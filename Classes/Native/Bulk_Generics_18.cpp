﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.Collections.Generic.LinkedList`1<System.Object>
struct LinkedList_1_t2045451540;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t2185721892;
// System.Array
struct Il2CppArray;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t1787714124;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Collections.Generic.LinkedListNode`1<System.Object>
struct LinkedListNode_1_t3787551078;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// System.Collections.Generic.List`1<Core.RpsChoice>
struct List_1_t1420982684;
// System.Collections.Generic.List`1<Core.RpsResult>
struct List_1_t1847780376;
// System.Collections.Generic.List`1<Entity.Behavior.EBehaviorID>
struct List_1_t2606718688;
// System.Collections.Generic.List`1<HatredCtrl/stValue>
struct List_1_t499163666;
// System.Collections.Generic.List`1<Mihua.Assets.SubAssetMgr/ErrorInfo>
struct List_1_t4002166762;
// System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JTokenType>
struct List_1_t990115817;
// System.Collections.Generic.List`1<Newtonsoft.Json.Schema.JsonSchemaType>
struct List_1_t3483601373;
// System.Collections.Generic.List`1<Pathfinding.AdvancedSmooth/Turn>
struct List_1_t1833380930;
// System.Collections.Generic.List`1<Pathfinding.ClipperLib.IntPoint>
struct List_1_t399344435;
// System.Collections.Generic.List`1<Pathfinding.Int2>
struct List_1_t3342231145;
// System.Collections.Generic.List`1<Pathfinding.Int3>
struct List_1_t3342231146;
// System.Collections.Generic.List`1<Pathfinding.IntRect>
struct List_1_t88276517;
// System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/IntersectionPair>
struct List_1_t4189505471;
// System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/VOLine>
struct List_1_t3398117353;
// System.Collections.Generic.List`1<Pathfinding.Voxels.ExtraMesh>
struct List_1_t1291247971;
// System.Collections.Generic.List`1<Pathfinding.Voxels.VoxelContour>
struct List_1_t670419272;
// System.Collections.Generic.List`1<PointCloudRegognizer/Point>
struct List_1_t3207017302;
// System.Collections.Generic.List`1<PushType>
struct List_1_t3208828004;
// System.Collections.Generic.List`1<System.Boolean>
struct List_1_t1844984270;
// System.Collections.Generic.List`1<System.Byte>
struct List_1_t4230795212;
// System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct List_1_t3312854530;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t2522024052;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t1244034627;
// System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>
struct List_1_t132831245;
// System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>
struct List_1_t374511678;
// System.Collections.Generic.List`1<System.Single>
struct List_1_t1365137228;
// System.Collections.Generic.List`1<System.UInt32>
struct List_1_t1392853533;
// System.Collections.Generic.List`1<UnityEngine.Bounds>
struct List_1_t4079827401;
// System.Collections.Generic.List`1<UnityEngine.Color>
struct List_1_t1267765161;
// System.Collections.Generic.List`1<UnityEngine.Color32>
struct List_1_t1967039240;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23529636437.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23529636437MethodDeclarations.h"
#include "AssemblyU2DCSharp_AIEnum_EAIEventtype3896289311.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Void2863195528.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_String7231557MethodDeclarations.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Object4170816371MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24204345545.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24204345545MethodDeclarations.h"
#include "AssemblyU2DCSharp_AnimationRunner_AniType1006238651.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21209901952.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21209901952MethodDeclarations.h"
#include "AssemblyU2DCSharp_Entity_Behavior_EBehaviorID1238533136.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_g25818750.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_g25818750MethodDeclarations.h"
#include "AssemblyU2DCSharp_FLOAT_TEXT_ID2199345770.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21341048298.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21341048298MethodDeclarations.h"
#include "AssemblyU2DCSharp_MScrollView_MoveWay3474941550.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24013503581.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24013503581MethodDeclarations.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_De2971844791.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22815902970.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22815902970MethodDeclarations.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_Convert866134174.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22144098788.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22144098788MethodDeclarations.h"
#include "AssemblyU2DCSharp_Pathfinding_Int21974045593.h"
#include "mscorlib_System_Int321153838500.h"
#include "AssemblyU2DCSharp_Pathfinding_Int21974045593MethodDeclarations.h"
#include "mscorlib_System_Int321153838500MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_866109363.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_866109363MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22630286527.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22630286527MethodDeclarations.h"
#include "AssemblyU2DCSharp_Pathfinding_Int31974045594.h"
#include "AssemblyU2DCSharp_Pathfinding_Int31974045594MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21352297102.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21352297102MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_519615340.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_519615340MethodDeclarations.h"
#include "AssemblyU2DCSharp_PushType1840642452.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21947212572.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21947212572MethodDeclarations.h"
#include "AssemblyU2DCSharp_SoundTypeID3626616740.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23488982890.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23488982890MethodDeclarations.h"
#include "AssemblyU2DCSharp_SoundStatus3592938945.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21049882445.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21049882445MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24066860316.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24066860316MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24187962917.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24187962917MethodDeclarations.h"
#include "mscorlib_System_Single4291918972.h"
#include "mscorlib_System_Single4291918972MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21199798016.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21199798016MethodDeclarations.h"
#include "AssemblyU2DCSharp_HatredCtrl_stValue3425945410.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_407833816.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_407833816MethodDeclarations.h"
#include "AssemblyU2DCSharp_Mihua_Assets_SubAssetMgr_ErrorIn2633981210.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24184235723.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24184235723MethodDeclarations.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Schema_JsonSchem2115415821.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_666182096.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_666182096MethodDeclarations.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_Js2892329490.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23374811140.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23374811140MethodDeclarations.h"
#include "AssemblyU2DCSharp_ProductsCfgMgr_ProductInfo1305991238.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22545618620.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22545618620MethodDeclarations.h"
#include "mscorlib_System_Boolean476798718.h"
#include "mscorlib_System_Boolean476798718MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22061784035.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22061784035MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24287931429.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24287931429MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23222658402.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23222658402MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21944668977.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21944668977MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22065771578.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22065771578MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22093487825.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22093487825MethodDeclarations.h"
#include "mscorlib_System_UInt1624667923.h"
#include "mscorlib_System_UInt1624667923MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21919813716.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21919813716MethodDeclarations.h"
#include "UnityEngine_UnityEngine_TextEditor_TextEditOp4145961110.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21637661969.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21637661969MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24066747055.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24066747055MethodDeclarations.h"
#include "mscorlib_System_UInt3224667981.h"
#include "mscorlib_System_UInt3224667981MethodDeclarations.h"
#include "mscorlib_System_UInt6424668076.h"
#include "mscorlib_System_UInt6424668076MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22851311006.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22851311006MethodDeclarations.h"
#include "AssemblyU2DCSharp_UIModelDisplayType1891752679.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21573321581.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21573321581MethodDeclarations.h"
#include "System_System_Collections_Generic_LinkedList_1_Enu3528606100.h"
#include "System_System_Collections_Generic_LinkedList_1_Enu3528606100MethodDeclarations.h"
#include "System_System_Collections_Generic_LinkedList_1_gen2045451540.h"
#include "System_System_Collections_Generic_LinkedListNode_13787551078.h"
#include "mscorlib_System_ObjectDisposedException1794727681MethodDeclarations.h"
#include "mscorlib_System_InvalidOperationException1589641621MethodDeclarations.h"
#include "mscorlib_System_ObjectDisposedException1794727681.h"
#include "mscorlib_System_InvalidOperationException1589641621.h"
#include "System_System_Collections_Generic_LinkedListNode_13787551078MethodDeclarations.h"
#include "System_System_Collections_Generic_LinkedList_1_gen2045451540MethodDeclarations.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2185721892.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon2761351129.h"
#include "mscorlib_System_ArgumentException928607144MethodDeclarations.h"
#include "mscorlib_System_ArgumentException928607144.h"
#include "mscorlib_System_ArgumentNullException3573189601MethodDeclarations.h"
#include "mscorlib_System_ArgumentNullException3573189601.h"
#include "mscorlib_System_Array1146569071MethodDeclarations.h"
#include "mscorlib_System_ArgumentOutOfRangeException3816648464MethodDeclarations.h"
#include "mscorlib_System_ArgumentOutOfRangeException3816648464.h"
#include "mscorlib_System_Type2863145774MethodDeclarations.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2185721892MethodDeclarations.h"
#include "mscorlib_System_Type2863145774.h"
#include "mscorlib_System_RuntimeTypeHandle2669177232.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1440655454.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1440655454MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1420982684.h"
#include "AssemblyU2DCSharp_Core_RpsChoice52797132.h"
#include "Assembly-CSharp_ArrayTypes.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1867453146.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1867453146MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1847780376.h"
#include "AssemblyU2DCSharp_Core_RpsResult479594824.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2626391458.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2626391458MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2606718688.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat518836436.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat518836436MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen499163666.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera4021839532.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera4021839532MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen4002166762.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1009788587.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1009788587MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen990115817.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JTokenType3916897561.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3503274143.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3503274143MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3483601373.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1853053700.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1853053700MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1833380930.h"
#include "AssemblyU2DCSharp_Pathfinding_AdvancedSmooth_Turn465195378.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat419017205.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat419017205MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen399344435.h"
#include "Pathfinding_ClipperLib_Pathfinding_ClipperLib_IntP3326126179.h"
#include "Pathfinding.ClipperLib_ArrayTypes.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3361903915.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3361903915MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3342231145.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3361903916.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3361903916MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3342231146.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat107949287.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat107949287MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen88276517.h"
#include "AssemblyU2DCSharp_Pathfinding_IntRect3015058261.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera4209178241.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera4209178241MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen4189505471.h"
#include "AssemblyU2DCSharp_Pathfinding_LocalAvoidance_Inter2821319919.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3417790123.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3417790123MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3398117353.h"
#include "AssemblyU2DCSharp_Pathfinding_LocalAvoidance_VOLin2029931801.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1310920741.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1310920741MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1291247971.h"
#include "AssemblyU2DCSharp_Pathfinding_Voxels_ExtraMesh4218029715.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat690092042.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat690092042MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen670419272.h"
#include "AssemblyU2DCSharp_Pathfinding_Voxels_VoxelContour3597201016.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3226690072.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3226690072MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3207017302.h"
#include "AssemblyU2DCSharp_PointCloudRegognizer_Point1838831750.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3228500774.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3228500774MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3208828004.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1864657040.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1864657040MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1844984270.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera4250467982.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera4250467982MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen4230795212.h"
#include "mscorlib_System_Byte2862609660.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3332527299.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3332527299MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3312854529.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2541696822.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2541696822MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2522024052.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1263707397.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1263707397MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1244034627.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat152504015.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat152504015MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen132831245.h"
#include "mscorlib_System_Reflection_CustomAttributeNamedArg3059612989.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat394184448.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat394184448MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen374511678.h"
#include "mscorlib_System_Reflection_CustomAttributeTypedArg3301293422.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1384809998.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1384809998MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1365137228.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1412526303.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1412526303MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1392853533.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera4099500171.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera4099500171MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen4079827401.h"
#include "UnityEngine_UnityEngine_Bounds2711641849.h"
#include "UnityEngine_ArrayTypes.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1287437931.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1287437931MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1267765161.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1986712010.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1986712010MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1967039240.h"
#include "UnityEngine_UnityEngine_Color32598853688.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Collections.Generic.KeyValuePair`2<AIEnum.EAIEventtype,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m1845025713_gshared (KeyValuePair_2_t3529636437 * __this, uint8_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		uint8_t L_0 = ___key0;
		((  void (*) (Il2CppObject *, uint8_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t3529636437 *)__this), (uint8_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject * L_1 = ___value1;
		((  void (*) (Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t3529636437 *)__this), (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m1845025713_AdjustorThunk (Il2CppObject * __this, uint8_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t3529636437 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3529636437 *>(__this + 1);
	KeyValuePair_2__ctor_m1845025713(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<AIEnum.EAIEventtype,System.Object>::get_Key()
extern "C"  uint8_t KeyValuePair_2_get_Key_m2103164055_gshared (KeyValuePair_2_t3529636437 * __this, const MethodInfo* method)
{
	{
		uint8_t L_0 = (uint8_t)__this->get_key_0();
		return L_0;
	}
}
extern "C"  uint8_t KeyValuePair_2_get_Key_m2103164055_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3529636437 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3529636437 *>(__this + 1);
	return KeyValuePair_2_get_Key_m2103164055(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<AIEnum.EAIEventtype,System.Object>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m1003893720_gshared (KeyValuePair_2_t3529636437 * __this, uint8_t ___value0, const MethodInfo* method)
{
	{
		uint8_t L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m1003893720_AdjustorThunk (Il2CppObject * __this, uint8_t ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t3529636437 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3529636437 *>(__this + 1);
	KeyValuePair_2_set_Key_m1003893720(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<AIEnum.EAIEventtype,System.Object>::get_Value()
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m3702007291_gshared (KeyValuePair_2_t3529636437 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_value_1();
		return L_0;
	}
}
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m3702007291_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3529636437 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3529636437 *>(__this + 1);
	return KeyValuePair_2_get_Value_m3702007291(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<AIEnum.EAIEventtype,System.Object>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m801364952_gshared (KeyValuePair_2_t3529636437 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m801364952_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t3529636437 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3529636437 *>(__this + 1);
	KeyValuePair_2_set_Value_m801364952(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<AIEnum.EAIEventtype,System.Object>::ToString()
extern Il2CppClass* StringU5BU5D_t4054002952_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral91;
extern Il2CppCodeGenString* _stringLiteral1396;
extern Il2CppCodeGenString* _stringLiteral93;
extern const uint32_t KeyValuePair_2_ToString_m2025062384_MetadataUsageId;
extern "C"  String_t* KeyValuePair_2_ToString_m2025062384_gshared (KeyValuePair_2_t3529636437 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m2025062384_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint8_t V_0 = 0;
	Il2CppObject * V_1 = NULL;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t4054002952* G_B2_1 = NULL;
	StringU5BU5D_t4054002952* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t4054002952* G_B1_1 = NULL;
	StringU5BU5D_t4054002952* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t4054002952* G_B3_2 = NULL;
	StringU5BU5D_t4054002952* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t4054002952* G_B5_1 = NULL;
	StringU5BU5D_t4054002952* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t4054002952* G_B4_1 = NULL;
	StringU5BU5D_t4054002952* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t4054002952* G_B6_2 = NULL;
	StringU5BU5D_t4054002952* G_B6_3 = NULL;
	{
		StringU5BU5D_t4054002952* L_0 = (StringU5BU5D_t4054002952*)((StringU5BU5D_t4054002952*)SZArrayNew(StringU5BU5D_t4054002952_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral91);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral91);
		StringU5BU5D_t4054002952* L_1 = (StringU5BU5D_t4054002952*)L_0;
		uint8_t L_2 = ((  uint8_t (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t3529636437 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
	}
	{
		uint8_t L_3 = ((  uint8_t (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t3529636437 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (uint8_t)L_3;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (&V_0));
		NullCheck((Il2CppObject *)L_4);
		String_t* L_5 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)L_4);
		G_B3_0 = L_5;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_6;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t4054002952* L_7 = (StringU5BU5D_t4054002952*)G_B3_3;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 2);
		ArrayElementTypeCheck (L_7, _stringLiteral1396);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral1396);
		StringU5BU5D_t4054002952* L_8 = (StringU5BU5D_t4054002952*)L_7;
		Il2CppObject * L_9 = ((  Il2CppObject * (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t3529636437 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_8;
		G_B4_2 = L_8;
		if (!L_9)
		{
			G_B5_0 = 3;
			G_B5_1 = L_8;
			G_B5_2 = L_8;
			goto IL_0072;
		}
	}
	{
		Il2CppObject * L_10 = ((  Il2CppObject * (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t3529636437 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (Il2CppObject *)L_10;
		NullCheck((Il2CppObject *)(*(&V_1)));
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_1)));
		G_B6_0 = L_11;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_12;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t4054002952* L_13 = (StringU5BU5D_t4054002952*)G_B6_3;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 4);
		ArrayElementTypeCheck (L_13, _stringLiteral93);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral93);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m21867311(NULL /*static, unused*/, (StringU5BU5D_t4054002952*)L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m2025062384_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3529636437 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3529636437 *>(__this + 1);
	return KeyValuePair_2_ToString_m2025062384(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<AnimationRunner/AniType,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m1933297218_gshared (KeyValuePair_2_t4204345545 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___key0;
		((  void (*) (Il2CppObject *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t4204345545 *)__this), (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject * L_1 = ___value1;
		((  void (*) (Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t4204345545 *)__this), (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m1933297218_AdjustorThunk (Il2CppObject * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t4204345545 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t4204345545 *>(__this + 1);
	KeyValuePair_2__ctor_m1933297218(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<AnimationRunner/AniType,System.Object>::get_Key()
extern "C"  int32_t KeyValuePair_2_get_Key_m3772685030_gshared (KeyValuePair_2_t4204345545 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_key_0();
		return L_0;
	}
}
extern "C"  int32_t KeyValuePair_2_get_Key_m3772685030_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t4204345545 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t4204345545 *>(__this + 1);
	return KeyValuePair_2_get_Key_m3772685030(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<AnimationRunner/AniType,System.Object>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m3195876007_gshared (KeyValuePair_2_t4204345545 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m3195876007_AdjustorThunk (Il2CppObject * __this, int32_t ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t4204345545 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t4204345545 *>(__this + 1);
	KeyValuePair_2_set_Key_m3195876007(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<AnimationRunner/AniType,System.Object>::get_Value()
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m3039564682_gshared (KeyValuePair_2_t4204345545 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_value_1();
		return L_0;
	}
}
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m3039564682_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t4204345545 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t4204345545 *>(__this + 1);
	return KeyValuePair_2_get_Value_m3039564682(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<AnimationRunner/AniType,System.Object>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m4129348391_gshared (KeyValuePair_2_t4204345545 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m4129348391_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t4204345545 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t4204345545 *>(__this + 1);
	KeyValuePair_2_set_Value_m4129348391(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<AnimationRunner/AniType,System.Object>::ToString()
extern Il2CppClass* StringU5BU5D_t4054002952_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral91;
extern Il2CppCodeGenString* _stringLiteral1396;
extern Il2CppCodeGenString* _stringLiteral93;
extern const uint32_t KeyValuePair_2_ToString_m3322470913_MetadataUsageId;
extern "C"  String_t* KeyValuePair_2_ToString_m3322470913_gshared (KeyValuePair_2_t4204345545 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m3322470913_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Il2CppObject * V_1 = NULL;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t4054002952* G_B2_1 = NULL;
	StringU5BU5D_t4054002952* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t4054002952* G_B1_1 = NULL;
	StringU5BU5D_t4054002952* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t4054002952* G_B3_2 = NULL;
	StringU5BU5D_t4054002952* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t4054002952* G_B5_1 = NULL;
	StringU5BU5D_t4054002952* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t4054002952* G_B4_1 = NULL;
	StringU5BU5D_t4054002952* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t4054002952* G_B6_2 = NULL;
	StringU5BU5D_t4054002952* G_B6_3 = NULL;
	{
		StringU5BU5D_t4054002952* L_0 = (StringU5BU5D_t4054002952*)((StringU5BU5D_t4054002952*)SZArrayNew(StringU5BU5D_t4054002952_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral91);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral91);
		StringU5BU5D_t4054002952* L_1 = (StringU5BU5D_t4054002952*)L_0;
		int32_t L_2 = ((  int32_t (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t4204345545 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
	}
	{
		int32_t L_3 = ((  int32_t (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t4204345545 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (int32_t)L_3;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (&V_0));
		NullCheck((Il2CppObject *)L_4);
		String_t* L_5 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)L_4);
		G_B3_0 = L_5;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_6;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t4054002952* L_7 = (StringU5BU5D_t4054002952*)G_B3_3;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 2);
		ArrayElementTypeCheck (L_7, _stringLiteral1396);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral1396);
		StringU5BU5D_t4054002952* L_8 = (StringU5BU5D_t4054002952*)L_7;
		Il2CppObject * L_9 = ((  Il2CppObject * (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t4204345545 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_8;
		G_B4_2 = L_8;
		if (!L_9)
		{
			G_B5_0 = 3;
			G_B5_1 = L_8;
			G_B5_2 = L_8;
			goto IL_0072;
		}
	}
	{
		Il2CppObject * L_10 = ((  Il2CppObject * (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t4204345545 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (Il2CppObject *)L_10;
		NullCheck((Il2CppObject *)(*(&V_1)));
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_1)));
		G_B6_0 = L_11;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_12;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t4054002952* L_13 = (StringU5BU5D_t4054002952*)G_B6_3;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 4);
		ArrayElementTypeCheck (L_13, _stringLiteral93);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral93);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m21867311(NULL /*static, unused*/, (StringU5BU5D_t4054002952*)L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m3322470913_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t4204345545 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t4204345545 *>(__this + 1);
	return KeyValuePair_2_ToString_m3322470913(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<Entity.Behavior.EBehaviorID,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m907882396_gshared (KeyValuePair_2_t1209901952 * __this, uint8_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		uint8_t L_0 = ___key0;
		((  void (*) (Il2CppObject *, uint8_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t1209901952 *)__this), (uint8_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject * L_1 = ___value1;
		((  void (*) (Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t1209901952 *)__this), (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m907882396_AdjustorThunk (Il2CppObject * __this, uint8_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t1209901952 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1209901952 *>(__this + 1);
	KeyValuePair_2__ctor_m907882396(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<Entity.Behavior.EBehaviorID,System.Object>::get_Key()
extern "C"  uint8_t KeyValuePair_2_get_Key_m3205006412_gshared (KeyValuePair_2_t1209901952 * __this, const MethodInfo* method)
{
	{
		uint8_t L_0 = (uint8_t)__this->get_key_0();
		return L_0;
	}
}
extern "C"  uint8_t KeyValuePair_2_get_Key_m3205006412_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1209901952 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1209901952 *>(__this + 1);
	return KeyValuePair_2_get_Key_m3205006412(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<Entity.Behavior.EBehaviorID,System.Object>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m614586637_gshared (KeyValuePair_2_t1209901952 * __this, uint8_t ___value0, const MethodInfo* method)
{
	{
		uint8_t L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m614586637_AdjustorThunk (Il2CppObject * __this, uint8_t ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t1209901952 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1209901952 *>(__this + 1);
	KeyValuePair_2_set_Key_m614586637(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<Entity.Behavior.EBehaviorID,System.Object>::get_Value()
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m1857364592_gshared (KeyValuePair_2_t1209901952 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_value_1();
		return L_0;
	}
}
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m1857364592_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1209901952 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1209901952 *>(__this + 1);
	return KeyValuePair_2_get_Value_m1857364592(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<Entity.Behavior.EBehaviorID,System.Object>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m3542081165_gshared (KeyValuePair_2_t1209901952 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m3542081165_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t1209901952 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1209901952 *>(__this + 1);
	KeyValuePair_2_set_Value_m3542081165(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<Entity.Behavior.EBehaviorID,System.Object>::ToString()
extern Il2CppClass* StringU5BU5D_t4054002952_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral91;
extern Il2CppCodeGenString* _stringLiteral1396;
extern Il2CppCodeGenString* _stringLiteral93;
extern const uint32_t KeyValuePair_2_ToString_m3384335323_MetadataUsageId;
extern "C"  String_t* KeyValuePair_2_ToString_m3384335323_gshared (KeyValuePair_2_t1209901952 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m3384335323_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint8_t V_0 = 0;
	Il2CppObject * V_1 = NULL;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t4054002952* G_B2_1 = NULL;
	StringU5BU5D_t4054002952* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t4054002952* G_B1_1 = NULL;
	StringU5BU5D_t4054002952* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t4054002952* G_B3_2 = NULL;
	StringU5BU5D_t4054002952* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t4054002952* G_B5_1 = NULL;
	StringU5BU5D_t4054002952* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t4054002952* G_B4_1 = NULL;
	StringU5BU5D_t4054002952* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t4054002952* G_B6_2 = NULL;
	StringU5BU5D_t4054002952* G_B6_3 = NULL;
	{
		StringU5BU5D_t4054002952* L_0 = (StringU5BU5D_t4054002952*)((StringU5BU5D_t4054002952*)SZArrayNew(StringU5BU5D_t4054002952_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral91);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral91);
		StringU5BU5D_t4054002952* L_1 = (StringU5BU5D_t4054002952*)L_0;
		uint8_t L_2 = ((  uint8_t (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t1209901952 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
	}
	{
		uint8_t L_3 = ((  uint8_t (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t1209901952 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (uint8_t)L_3;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (&V_0));
		NullCheck((Il2CppObject *)L_4);
		String_t* L_5 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)L_4);
		G_B3_0 = L_5;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_6;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t4054002952* L_7 = (StringU5BU5D_t4054002952*)G_B3_3;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 2);
		ArrayElementTypeCheck (L_7, _stringLiteral1396);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral1396);
		StringU5BU5D_t4054002952* L_8 = (StringU5BU5D_t4054002952*)L_7;
		Il2CppObject * L_9 = ((  Il2CppObject * (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t1209901952 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_8;
		G_B4_2 = L_8;
		if (!L_9)
		{
			G_B5_0 = 3;
			G_B5_1 = L_8;
			G_B5_2 = L_8;
			goto IL_0072;
		}
	}
	{
		Il2CppObject * L_10 = ((  Il2CppObject * (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t1209901952 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (Il2CppObject *)L_10;
		NullCheck((Il2CppObject *)(*(&V_1)));
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_1)));
		G_B6_0 = L_11;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_12;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t4054002952* L_13 = (StringU5BU5D_t4054002952*)G_B6_3;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 4);
		ArrayElementTypeCheck (L_13, _stringLiteral93);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral93);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m21867311(NULL /*static, unused*/, (StringU5BU5D_t4054002952*)L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m3384335323_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1209901952 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1209901952 *>(__this + 1);
	return KeyValuePair_2_ToString_m3384335323(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<FLOAT_TEXT_ID,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m3745503539_gshared (KeyValuePair_2_t25818750 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___key0;
		((  void (*) (Il2CppObject *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t25818750 *)__this), (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject * L_1 = ___value1;
		((  void (*) (Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t25818750 *)__this), (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m3745503539_AdjustorThunk (Il2CppObject * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t25818750 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t25818750 *>(__this + 1);
	KeyValuePair_2__ctor_m3745503539(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<FLOAT_TEXT_ID,System.Object>::get_Key()
extern "C"  int32_t KeyValuePair_2_get_Key_m2837181653_gshared (KeyValuePair_2_t25818750 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_key_0();
		return L_0;
	}
}
extern "C"  int32_t KeyValuePair_2_get_Key_m2837181653_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t25818750 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t25818750 *>(__this + 1);
	return KeyValuePair_2_get_Key_m2837181653(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<FLOAT_TEXT_ID,System.Object>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m4121120022_gshared (KeyValuePair_2_t25818750 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m4121120022_AdjustorThunk (Il2CppObject * __this, int32_t ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t25818750 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t25818750 *>(__this + 1);
	KeyValuePair_2_set_Key_m4121120022(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<FLOAT_TEXT_ID,System.Object>::get_Value()
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m389192761_gshared (KeyValuePair_2_t25818750 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_value_1();
		return L_0;
	}
}
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m389192761_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t25818750 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t25818750 *>(__this + 1);
	return KeyValuePair_2_get_Value_m389192761(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<FLOAT_TEXT_ID,System.Object>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m2663786006_gshared (KeyValuePair_2_t25818750 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m2663786006_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t25818750 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t25818750 *>(__this + 1);
	KeyValuePair_2_set_Value_m2663786006(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<FLOAT_TEXT_ID,System.Object>::ToString()
extern Il2CppClass* StringU5BU5D_t4054002952_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral91;
extern Il2CppCodeGenString* _stringLiteral1396;
extern Il2CppCodeGenString* _stringLiteral93;
extern const uint32_t KeyValuePair_2_ToString_m1200637362_MetadataUsageId;
extern "C"  String_t* KeyValuePair_2_ToString_m1200637362_gshared (KeyValuePair_2_t25818750 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m1200637362_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Il2CppObject * V_1 = NULL;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t4054002952* G_B2_1 = NULL;
	StringU5BU5D_t4054002952* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t4054002952* G_B1_1 = NULL;
	StringU5BU5D_t4054002952* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t4054002952* G_B3_2 = NULL;
	StringU5BU5D_t4054002952* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t4054002952* G_B5_1 = NULL;
	StringU5BU5D_t4054002952* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t4054002952* G_B4_1 = NULL;
	StringU5BU5D_t4054002952* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t4054002952* G_B6_2 = NULL;
	StringU5BU5D_t4054002952* G_B6_3 = NULL;
	{
		StringU5BU5D_t4054002952* L_0 = (StringU5BU5D_t4054002952*)((StringU5BU5D_t4054002952*)SZArrayNew(StringU5BU5D_t4054002952_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral91);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral91);
		StringU5BU5D_t4054002952* L_1 = (StringU5BU5D_t4054002952*)L_0;
		int32_t L_2 = ((  int32_t (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t25818750 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
	}
	{
		int32_t L_3 = ((  int32_t (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t25818750 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (int32_t)L_3;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (&V_0));
		NullCheck((Il2CppObject *)L_4);
		String_t* L_5 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)L_4);
		G_B3_0 = L_5;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_6;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t4054002952* L_7 = (StringU5BU5D_t4054002952*)G_B3_3;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 2);
		ArrayElementTypeCheck (L_7, _stringLiteral1396);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral1396);
		StringU5BU5D_t4054002952* L_8 = (StringU5BU5D_t4054002952*)L_7;
		Il2CppObject * L_9 = ((  Il2CppObject * (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t25818750 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_8;
		G_B4_2 = L_8;
		if (!L_9)
		{
			G_B5_0 = 3;
			G_B5_1 = L_8;
			G_B5_2 = L_8;
			goto IL_0072;
		}
	}
	{
		Il2CppObject * L_10 = ((  Il2CppObject * (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t25818750 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (Il2CppObject *)L_10;
		NullCheck((Il2CppObject *)(*(&V_1)));
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_1)));
		G_B6_0 = L_11;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_12;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t4054002952* L_13 = (StringU5BU5D_t4054002952*)G_B6_3;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 4);
		ArrayElementTypeCheck (L_13, _stringLiteral93);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral93);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m21867311(NULL /*static, unused*/, (StringU5BU5D_t4054002952*)L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m1200637362_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t25818750 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t25818750 *>(__this + 1);
	return KeyValuePair_2_ToString_m1200637362(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<MScrollView/MoveWay,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m3766851247_gshared (KeyValuePair_2_t1341048298 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___key0;
		((  void (*) (Il2CppObject *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t1341048298 *)__this), (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject * L_1 = ___value1;
		((  void (*) (Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t1341048298 *)__this), (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m3766851247_AdjustorThunk (Il2CppObject * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t1341048298 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1341048298 *>(__this + 1);
	KeyValuePair_2__ctor_m3766851247(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<MScrollView/MoveWay,System.Object>::get_Key()
extern "C"  int32_t KeyValuePair_2_get_Key_m2147945433_gshared (KeyValuePair_2_t1341048298 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_key_0();
		return L_0;
	}
}
extern "C"  int32_t KeyValuePair_2_get_Key_m2147945433_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1341048298 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1341048298 *>(__this + 1);
	return KeyValuePair_2_get_Key_m2147945433(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<MScrollView/MoveWay,System.Object>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m1306792474_gshared (KeyValuePair_2_t1341048298 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m1306792474_AdjustorThunk (Il2CppObject * __this, int32_t ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t1341048298 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1341048298 *>(__this + 1);
	KeyValuePair_2_set_Key_m1306792474(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<MScrollView/MoveWay,System.Object>::get_Value()
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m3787238589_gshared (KeyValuePair_2_t1341048298 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_value_1();
		return L_0;
	}
}
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m3787238589_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1341048298 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1341048298 *>(__this + 1);
	return KeyValuePair_2_get_Value_m3787238589(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<MScrollView/MoveWay,System.Object>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m2941569306_gshared (KeyValuePair_2_t1341048298 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m2941569306_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t1341048298 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1341048298 *>(__this + 1);
	KeyValuePair_2_set_Value_m2941569306(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<MScrollView/MoveWay,System.Object>::ToString()
extern Il2CppClass* StringU5BU5D_t4054002952_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral91;
extern Il2CppCodeGenString* _stringLiteral1396;
extern Il2CppCodeGenString* _stringLiteral93;
extern const uint32_t KeyValuePair_2_ToString_m3413285102_MetadataUsageId;
extern "C"  String_t* KeyValuePair_2_ToString_m3413285102_gshared (KeyValuePair_2_t1341048298 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m3413285102_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Il2CppObject * V_1 = NULL;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t4054002952* G_B2_1 = NULL;
	StringU5BU5D_t4054002952* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t4054002952* G_B1_1 = NULL;
	StringU5BU5D_t4054002952* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t4054002952* G_B3_2 = NULL;
	StringU5BU5D_t4054002952* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t4054002952* G_B5_1 = NULL;
	StringU5BU5D_t4054002952* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t4054002952* G_B4_1 = NULL;
	StringU5BU5D_t4054002952* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t4054002952* G_B6_2 = NULL;
	StringU5BU5D_t4054002952* G_B6_3 = NULL;
	{
		StringU5BU5D_t4054002952* L_0 = (StringU5BU5D_t4054002952*)((StringU5BU5D_t4054002952*)SZArrayNew(StringU5BU5D_t4054002952_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral91);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral91);
		StringU5BU5D_t4054002952* L_1 = (StringU5BU5D_t4054002952*)L_0;
		int32_t L_2 = ((  int32_t (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t1341048298 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
	}
	{
		int32_t L_3 = ((  int32_t (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t1341048298 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (int32_t)L_3;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (&V_0));
		NullCheck((Il2CppObject *)L_4);
		String_t* L_5 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)L_4);
		G_B3_0 = L_5;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_6;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t4054002952* L_7 = (StringU5BU5D_t4054002952*)G_B3_3;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 2);
		ArrayElementTypeCheck (L_7, _stringLiteral1396);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral1396);
		StringU5BU5D_t4054002952* L_8 = (StringU5BU5D_t4054002952*)L_7;
		Il2CppObject * L_9 = ((  Il2CppObject * (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t1341048298 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_8;
		G_B4_2 = L_8;
		if (!L_9)
		{
			G_B5_0 = 3;
			G_B5_1 = L_8;
			G_B5_2 = L_8;
			goto IL_0072;
		}
	}
	{
		Il2CppObject * L_10 = ((  Il2CppObject * (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t1341048298 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (Il2CppObject *)L_10;
		NullCheck((Il2CppObject *)(*(&V_1)));
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_1)));
		G_B6_0 = L_11;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_12;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t4054002952* L_13 = (StringU5BU5D_t4054002952*)G_B6_3;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 4);
		ArrayElementTypeCheck (L_13, _stringLiteral93);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral93);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m21867311(NULL /*static, unused*/, (StringU5BU5D_t4054002952*)L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m3413285102_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1341048298 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1341048298 *>(__this + 1);
	return KeyValuePair_2_ToString_m3413285102(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m1883880326_gshared (KeyValuePair_2_t4013503581 * __this, TypeNameKey_t2971844791  ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		TypeNameKey_t2971844791  L_0 = ___key0;
		((  void (*) (Il2CppObject *, TypeNameKey_t2971844791 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t4013503581 *)__this), (TypeNameKey_t2971844791 )L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject * L_1 = ___value1;
		((  void (*) (Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t4013503581 *)__this), (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m1883880326_AdjustorThunk (Il2CppObject * __this, TypeNameKey_t2971844791  ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t4013503581 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t4013503581 *>(__this + 1);
	KeyValuePair_2__ctor_m1883880326(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::get_Key()
extern "C"  TypeNameKey_t2971844791  KeyValuePair_2_get_Key_m2652238242_gshared (KeyValuePair_2_t4013503581 * __this, const MethodInfo* method)
{
	{
		TypeNameKey_t2971844791  L_0 = (TypeNameKey_t2971844791 )__this->get_key_0();
		return L_0;
	}
}
extern "C"  TypeNameKey_t2971844791  KeyValuePair_2_get_Key_m2652238242_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t4013503581 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t4013503581 *>(__this + 1);
	return KeyValuePair_2_get_Key_m2652238242(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m362021603_gshared (KeyValuePair_2_t4013503581 * __this, TypeNameKey_t2971844791  ___value0, const MethodInfo* method)
{
	{
		TypeNameKey_t2971844791  L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m362021603_AdjustorThunk (Il2CppObject * __this, TypeNameKey_t2971844791  ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t4013503581 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t4013503581 *>(__this + 1);
	KeyValuePair_2_set_Key_m362021603(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::get_Value()
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m3736459618_gshared (KeyValuePair_2_t4013503581 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_value_1();
		return L_0;
	}
}
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m3736459618_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t4013503581 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t4013503581 *>(__this + 1);
	return KeyValuePair_2_get_Value_m3736459618(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m1218260323_gshared (KeyValuePair_2_t4013503581 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m1218260323_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t4013503581 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t4013503581 *>(__this + 1);
	KeyValuePair_2_set_Value_m1218260323(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::ToString()
extern Il2CppClass* StringU5BU5D_t4054002952_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral91;
extern Il2CppCodeGenString* _stringLiteral1396;
extern Il2CppCodeGenString* _stringLiteral93;
extern const uint32_t KeyValuePair_2_ToString_m2354625887_MetadataUsageId;
extern "C"  String_t* KeyValuePair_2_ToString_m2354625887_gshared (KeyValuePair_2_t4013503581 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m2354625887_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	TypeNameKey_t2971844791  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Il2CppObject * V_1 = NULL;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t4054002952* G_B2_1 = NULL;
	StringU5BU5D_t4054002952* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t4054002952* G_B1_1 = NULL;
	StringU5BU5D_t4054002952* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t4054002952* G_B3_2 = NULL;
	StringU5BU5D_t4054002952* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t4054002952* G_B5_1 = NULL;
	StringU5BU5D_t4054002952* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t4054002952* G_B4_1 = NULL;
	StringU5BU5D_t4054002952* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t4054002952* G_B6_2 = NULL;
	StringU5BU5D_t4054002952* G_B6_3 = NULL;
	{
		StringU5BU5D_t4054002952* L_0 = (StringU5BU5D_t4054002952*)((StringU5BU5D_t4054002952*)SZArrayNew(StringU5BU5D_t4054002952_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral91);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral91);
		StringU5BU5D_t4054002952* L_1 = (StringU5BU5D_t4054002952*)L_0;
		TypeNameKey_t2971844791  L_2 = ((  TypeNameKey_t2971844791  (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t4013503581 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
	}
	{
		TypeNameKey_t2971844791  L_3 = ((  TypeNameKey_t2971844791  (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t4013503581 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (TypeNameKey_t2971844791 )L_3;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (&V_0));
		NullCheck((Il2CppObject *)L_4);
		String_t* L_5 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)L_4);
		G_B3_0 = L_5;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_6;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t4054002952* L_7 = (StringU5BU5D_t4054002952*)G_B3_3;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 2);
		ArrayElementTypeCheck (L_7, _stringLiteral1396);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral1396);
		StringU5BU5D_t4054002952* L_8 = (StringU5BU5D_t4054002952*)L_7;
		Il2CppObject * L_9 = ((  Il2CppObject * (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t4013503581 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_8;
		G_B4_2 = L_8;
		if (!L_9)
		{
			G_B5_0 = 3;
			G_B5_1 = L_8;
			G_B5_2 = L_8;
			goto IL_0072;
		}
	}
	{
		Il2CppObject * L_10 = ((  Il2CppObject * (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t4013503581 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (Il2CppObject *)L_10;
		NullCheck((Il2CppObject *)(*(&V_1)));
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_1)));
		G_B6_0 = L_11;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_12;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t4054002952* L_13 = (StringU5BU5D_t4054002952*)G_B6_3;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 4);
		ArrayElementTypeCheck (L_13, _stringLiteral93);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral93);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m21867311(NULL /*static, unused*/, (StringU5BU5D_t4054002952*)L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m2354625887_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t4013503581 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t4013503581 *>(__this + 1);
	return KeyValuePair_2_ToString_m2354625887(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m2411535999_gshared (KeyValuePair_2_t2815902970 * __this, TypeConvertKey_t866134174  ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		TypeConvertKey_t866134174  L_0 = ___key0;
		((  void (*) (Il2CppObject *, TypeConvertKey_t866134174 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t2815902970 *)__this), (TypeConvertKey_t866134174 )L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject * L_1 = ___value1;
		((  void (*) (Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t2815902970 *)__this), (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m2411535999_AdjustorThunk (Il2CppObject * __this, TypeConvertKey_t866134174  ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t2815902970 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2815902970 *>(__this + 1);
	KeyValuePair_2__ctor_m2411535999(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>::get_Key()
extern "C"  TypeConvertKey_t866134174  KeyValuePair_2_get_Key_m1364625929_gshared (KeyValuePair_2_t2815902970 * __this, const MethodInfo* method)
{
	{
		TypeConvertKey_t866134174  L_0 = (TypeConvertKey_t866134174 )__this->get_key_0();
		return L_0;
	}
}
extern "C"  TypeConvertKey_t866134174  KeyValuePair_2_get_Key_m1364625929_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t2815902970 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2815902970 *>(__this + 1);
	return KeyValuePair_2_get_Key_m1364625929(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m1634396234_gshared (KeyValuePair_2_t2815902970 * __this, TypeConvertKey_t866134174  ___value0, const MethodInfo* method)
{
	{
		TypeConvertKey_t866134174  L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m1634396234_AdjustorThunk (Il2CppObject * __this, TypeConvertKey_t866134174  ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t2815902970 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2815902970 *>(__this + 1);
	KeyValuePair_2_set_Key_m1634396234(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>::get_Value()
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m1894627949_gshared (KeyValuePair_2_t2815902970 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_value_1();
		return L_0;
	}
}
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m1894627949_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t2815902970 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2815902970 *>(__this + 1);
	return KeyValuePair_2_get_Value_m1894627949(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m1512376138_gshared (KeyValuePair_2_t2815902970 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m1512376138_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t2815902970 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2815902970 *>(__this + 1);
	KeyValuePair_2_set_Value_m1512376138(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>::ToString()
extern Il2CppClass* StringU5BU5D_t4054002952_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral91;
extern Il2CppCodeGenString* _stringLiteral1396;
extern Il2CppCodeGenString* _stringLiteral93;
extern const uint32_t KeyValuePair_2_ToString_m52328446_MetadataUsageId;
extern "C"  String_t* KeyValuePair_2_ToString_m52328446_gshared (KeyValuePair_2_t2815902970 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m52328446_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	TypeConvertKey_t866134174  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Il2CppObject * V_1 = NULL;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t4054002952* G_B2_1 = NULL;
	StringU5BU5D_t4054002952* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t4054002952* G_B1_1 = NULL;
	StringU5BU5D_t4054002952* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t4054002952* G_B3_2 = NULL;
	StringU5BU5D_t4054002952* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t4054002952* G_B5_1 = NULL;
	StringU5BU5D_t4054002952* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t4054002952* G_B4_1 = NULL;
	StringU5BU5D_t4054002952* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t4054002952* G_B6_2 = NULL;
	StringU5BU5D_t4054002952* G_B6_3 = NULL;
	{
		StringU5BU5D_t4054002952* L_0 = (StringU5BU5D_t4054002952*)((StringU5BU5D_t4054002952*)SZArrayNew(StringU5BU5D_t4054002952_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral91);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral91);
		StringU5BU5D_t4054002952* L_1 = (StringU5BU5D_t4054002952*)L_0;
		TypeConvertKey_t866134174  L_2 = ((  TypeConvertKey_t866134174  (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t2815902970 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
	}
	{
		TypeConvertKey_t866134174  L_3 = ((  TypeConvertKey_t866134174  (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t2815902970 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (TypeConvertKey_t866134174 )L_3;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (&V_0));
		NullCheck((Il2CppObject *)L_4);
		String_t* L_5 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)L_4);
		G_B3_0 = L_5;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_6;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t4054002952* L_7 = (StringU5BU5D_t4054002952*)G_B3_3;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 2);
		ArrayElementTypeCheck (L_7, _stringLiteral1396);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral1396);
		StringU5BU5D_t4054002952* L_8 = (StringU5BU5D_t4054002952*)L_7;
		Il2CppObject * L_9 = ((  Il2CppObject * (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t2815902970 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_8;
		G_B4_2 = L_8;
		if (!L_9)
		{
			G_B5_0 = 3;
			G_B5_1 = L_8;
			G_B5_2 = L_8;
			goto IL_0072;
		}
	}
	{
		Il2CppObject * L_10 = ((  Il2CppObject * (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t2815902970 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (Il2CppObject *)L_10;
		NullCheck((Il2CppObject *)(*(&V_1)));
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_1)));
		G_B6_0 = L_11;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_12;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t4054002952* L_13 = (StringU5BU5D_t4054002952*)G_B6_3;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 4);
		ArrayElementTypeCheck (L_13, _stringLiteral93);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral93);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m21867311(NULL /*static, unused*/, (StringU5BU5D_t4054002952*)L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m52328446_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t2815902970 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2815902970 *>(__this + 1);
	return KeyValuePair_2_ToString_m52328446(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<Pathfinding.Int2,System.Int32>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m2441266561_gshared (KeyValuePair_2_t2144098788 * __this, Int2_t1974045593  ___key0, int32_t ___value1, const MethodInfo* method)
{
	{
		Int2_t1974045593  L_0 = ___key0;
		((  void (*) (Il2CppObject *, Int2_t1974045593 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t2144098788 *)__this), (Int2_t1974045593 )L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = ___value1;
		((  void (*) (Il2CppObject *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t2144098788 *)__this), (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m2441266561_AdjustorThunk (Il2CppObject * __this, Int2_t1974045593  ___key0, int32_t ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t2144098788 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2144098788 *>(__this + 1);
	KeyValuePair_2__ctor_m2441266561(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<Pathfinding.Int2,System.Int32>::get_Key()
extern "C"  Int2_t1974045593  KeyValuePair_2_get_Key_m2231789767_gshared (KeyValuePair_2_t2144098788 * __this, const MethodInfo* method)
{
	{
		Int2_t1974045593  L_0 = (Int2_t1974045593 )__this->get_key_0();
		return L_0;
	}
}
extern "C"  Int2_t1974045593  KeyValuePair_2_get_Key_m2231789767_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t2144098788 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2144098788 *>(__this + 1);
	return KeyValuePair_2_get_Key_m2231789767(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<Pathfinding.Int2,System.Int32>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m1704290312_gshared (KeyValuePair_2_t2144098788 * __this, Int2_t1974045593  ___value0, const MethodInfo* method)
{
	{
		Int2_t1974045593  L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m1704290312_AdjustorThunk (Il2CppObject * __this, Int2_t1974045593  ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t2144098788 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2144098788 *>(__this + 1);
	KeyValuePair_2_set_Key_m1704290312(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<Pathfinding.Int2,System.Int32>::get_Value()
extern "C"  int32_t KeyValuePair_2_get_Value_m4232945451_gshared (KeyValuePair_2_t2144098788 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_value_1();
		return L_0;
	}
}
extern "C"  int32_t KeyValuePair_2_get_Value_m4232945451_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t2144098788 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2144098788 *>(__this + 1);
	return KeyValuePair_2_get_Value_m4232945451(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<Pathfinding.Int2,System.Int32>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m1097693192_gshared (KeyValuePair_2_t2144098788 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m1097693192_AdjustorThunk (Il2CppObject * __this, int32_t ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t2144098788 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2144098788 *>(__this + 1);
	KeyValuePair_2_set_Value_m1097693192(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<Pathfinding.Int2,System.Int32>::ToString()
extern Il2CppClass* StringU5BU5D_t4054002952_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral91;
extern Il2CppCodeGenString* _stringLiteral1396;
extern Il2CppCodeGenString* _stringLiteral93;
extern const uint32_t KeyValuePair_2_ToString_m2959319872_MetadataUsageId;
extern "C"  String_t* KeyValuePair_2_ToString_m2959319872_gshared (KeyValuePair_2_t2144098788 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m2959319872_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Int2_t1974045593  V_0;
	memset(&V_0, 0, sizeof(V_0));
	int32_t V_1 = 0;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t4054002952* G_B2_1 = NULL;
	StringU5BU5D_t4054002952* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t4054002952* G_B1_1 = NULL;
	StringU5BU5D_t4054002952* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t4054002952* G_B3_2 = NULL;
	StringU5BU5D_t4054002952* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t4054002952* G_B5_1 = NULL;
	StringU5BU5D_t4054002952* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t4054002952* G_B4_1 = NULL;
	StringU5BU5D_t4054002952* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t4054002952* G_B6_2 = NULL;
	StringU5BU5D_t4054002952* G_B6_3 = NULL;
	{
		StringU5BU5D_t4054002952* L_0 = (StringU5BU5D_t4054002952*)((StringU5BU5D_t4054002952*)SZArrayNew(StringU5BU5D_t4054002952_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral91);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral91);
		StringU5BU5D_t4054002952* L_1 = (StringU5BU5D_t4054002952*)L_0;
		Int2_t1974045593  L_2 = ((  Int2_t1974045593  (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t2144098788 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
	}
	{
		Int2_t1974045593  L_3 = ((  Int2_t1974045593  (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t2144098788 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (Int2_t1974045593 )L_3;
		String_t* L_4 = Int2_ToString_m331126053((Int2_t1974045593 *)(&V_0), /*hidden argument*/NULL);
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t4054002952* L_6 = (StringU5BU5D_t4054002952*)G_B3_3;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 2);
		ArrayElementTypeCheck (L_6, _stringLiteral1396);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral1396);
		StringU5BU5D_t4054002952* L_7 = (StringU5BU5D_t4054002952*)L_6;
		int32_t L_8 = ((  int32_t (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t2144098788 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
	}
	{
		int32_t L_9 = ((  int32_t (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t2144098788 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (int32_t)L_9;
		String_t* L_10 = Int32_ToString_m1286526384((int32_t*)(&V_1), /*hidden argument*/NULL);
		G_B6_0 = L_10;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_11;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t4054002952* L_12 = (StringU5BU5D_t4054002952*)G_B6_3;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 4);
		ArrayElementTypeCheck (L_12, _stringLiteral93);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral93);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m21867311(NULL /*static, unused*/, (StringU5BU5D_t4054002952*)L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m2959319872_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t2144098788 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2144098788 *>(__this + 1);
	return KeyValuePair_2_ToString_m2959319872(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<Pathfinding.Int2,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m3790321200_gshared (KeyValuePair_2_t866109363 * __this, Int2_t1974045593  ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		Int2_t1974045593  L_0 = ___key0;
		((  void (*) (Il2CppObject *, Int2_t1974045593 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t866109363 *)__this), (Int2_t1974045593 )L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject * L_1 = ___value1;
		((  void (*) (Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t866109363 *)__this), (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m3790321200_AdjustorThunk (Il2CppObject * __this, Int2_t1974045593  ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t866109363 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t866109363 *>(__this + 1);
	KeyValuePair_2__ctor_m3790321200(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<Pathfinding.Int2,System.Object>::get_Key()
extern "C"  Int2_t1974045593  KeyValuePair_2_get_Key_m4013756344_gshared (KeyValuePair_2_t866109363 * __this, const MethodInfo* method)
{
	{
		Int2_t1974045593  L_0 = (Int2_t1974045593 )__this->get_key_0();
		return L_0;
	}
}
extern "C"  Int2_t1974045593  KeyValuePair_2_get_Key_m4013756344_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t866109363 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t866109363 *>(__this + 1);
	return KeyValuePair_2_get_Key_m4013756344(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<Pathfinding.Int2,System.Object>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m93750777_gshared (KeyValuePair_2_t866109363 * __this, Int2_t1974045593  ___value0, const MethodInfo* method)
{
	{
		Int2_t1974045593  L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m93750777_AdjustorThunk (Il2CppObject * __this, Int2_t1974045593  ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t866109363 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t866109363 *>(__this + 1);
	KeyValuePair_2_set_Key_m93750777(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<Pathfinding.Int2,System.Object>::get_Value()
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m167978232_gshared (KeyValuePair_2_t866109363 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_value_1();
		return L_0;
	}
}
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m167978232_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t866109363 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t866109363 *>(__this + 1);
	return KeyValuePair_2_get_Value_m167978232(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<Pathfinding.Int2,System.Object>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m2665231737_gshared (KeyValuePair_2_t866109363 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m2665231737_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t866109363 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t866109363 *>(__this + 1);
	KeyValuePair_2_set_Value_m2665231737(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<Pathfinding.Int2,System.Object>::ToString()
extern Il2CppClass* StringU5BU5D_t4054002952_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral91;
extern Il2CppCodeGenString* _stringLiteral1396;
extern Il2CppCodeGenString* _stringLiteral93;
extern const uint32_t KeyValuePair_2_ToString_m3662222985_MetadataUsageId;
extern "C"  String_t* KeyValuePair_2_ToString_m3662222985_gshared (KeyValuePair_2_t866109363 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m3662222985_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Int2_t1974045593  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Il2CppObject * V_1 = NULL;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t4054002952* G_B2_1 = NULL;
	StringU5BU5D_t4054002952* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t4054002952* G_B1_1 = NULL;
	StringU5BU5D_t4054002952* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t4054002952* G_B3_2 = NULL;
	StringU5BU5D_t4054002952* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t4054002952* G_B5_1 = NULL;
	StringU5BU5D_t4054002952* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t4054002952* G_B4_1 = NULL;
	StringU5BU5D_t4054002952* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t4054002952* G_B6_2 = NULL;
	StringU5BU5D_t4054002952* G_B6_3 = NULL;
	{
		StringU5BU5D_t4054002952* L_0 = (StringU5BU5D_t4054002952*)((StringU5BU5D_t4054002952*)SZArrayNew(StringU5BU5D_t4054002952_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral91);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral91);
		StringU5BU5D_t4054002952* L_1 = (StringU5BU5D_t4054002952*)L_0;
		Int2_t1974045593  L_2 = ((  Int2_t1974045593  (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t866109363 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
	}
	{
		Int2_t1974045593  L_3 = ((  Int2_t1974045593  (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t866109363 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (Int2_t1974045593 )L_3;
		String_t* L_4 = Int2_ToString_m331126053((Int2_t1974045593 *)(&V_0), /*hidden argument*/NULL);
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t4054002952* L_6 = (StringU5BU5D_t4054002952*)G_B3_3;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 2);
		ArrayElementTypeCheck (L_6, _stringLiteral1396);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral1396);
		StringU5BU5D_t4054002952* L_7 = (StringU5BU5D_t4054002952*)L_6;
		Il2CppObject * L_8 = ((  Il2CppObject * (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t866109363 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
		if (!L_8)
		{
			G_B5_0 = 3;
			G_B5_1 = L_7;
			G_B5_2 = L_7;
			goto IL_0072;
		}
	}
	{
		Il2CppObject * L_9 = ((  Il2CppObject * (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t866109363 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (Il2CppObject *)L_9;
		NullCheck((Il2CppObject *)(*(&V_1)));
		String_t* L_10 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_1)));
		G_B6_0 = L_10;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_11;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t4054002952* L_12 = (StringU5BU5D_t4054002952*)G_B6_3;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 4);
		ArrayElementTypeCheck (L_12, _stringLiteral93);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral93);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m21867311(NULL /*static, unused*/, (StringU5BU5D_t4054002952*)L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m3662222985_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t866109363 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t866109363 *>(__this + 1);
	return KeyValuePair_2_ToString_m3662222985(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<Pathfinding.Int3,System.Int32>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m4166747458_gshared (KeyValuePair_2_t2630286527 * __this, Int3_t1974045594  ___key0, int32_t ___value1, const MethodInfo* method)
{
	{
		Int3_t1974045594  L_0 = ___key0;
		((  void (*) (Il2CppObject *, Int3_t1974045594 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t2630286527 *)__this), (Int3_t1974045594 )L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = ___value1;
		((  void (*) (Il2CppObject *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t2630286527 *)__this), (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m4166747458_AdjustorThunk (Il2CppObject * __this, Int3_t1974045594  ___key0, int32_t ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t2630286527 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2630286527 *>(__this + 1);
	KeyValuePair_2__ctor_m4166747458(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<Pathfinding.Int3,System.Int32>::get_Key()
extern "C"  Int3_t1974045594  KeyValuePair_2_get_Key_m2262809574_gshared (KeyValuePair_2_t2630286527 * __this, const MethodInfo* method)
{
	{
		Int3_t1974045594  L_0 = (Int3_t1974045594 )__this->get_key_0();
		return L_0;
	}
}
extern "C"  Int3_t1974045594  KeyValuePair_2_get_Key_m2262809574_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t2630286527 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2630286527 *>(__this + 1);
	return KeyValuePair_2_get_Key_m2262809574(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<Pathfinding.Int3,System.Int32>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m1715606439_gshared (KeyValuePair_2_t2630286527 * __this, Int3_t1974045594  ___value0, const MethodInfo* method)
{
	{
		Int3_t1974045594  L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m1715606439_AdjustorThunk (Il2CppObject * __this, Int3_t1974045594  ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t2630286527 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2630286527 *>(__this + 1);
	KeyValuePair_2_set_Key_m1715606439(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<Pathfinding.Int3,System.Int32>::get_Value()
extern "C"  int32_t KeyValuePair_2_get_Value_m3978208906_gshared (KeyValuePair_2_t2630286527 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_value_1();
		return L_0;
	}
}
extern "C"  int32_t KeyValuePair_2_get_Value_m3978208906_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t2630286527 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2630286527 *>(__this + 1);
	return KeyValuePair_2_get_Value_m3978208906(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<Pathfinding.Int3,System.Int32>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m2123185191_gshared (KeyValuePair_2_t2630286527 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m2123185191_AdjustorThunk (Il2CppObject * __this, int32_t ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t2630286527 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2630286527 *>(__this + 1);
	KeyValuePair_2_set_Value_m2123185191(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<Pathfinding.Int3,System.Int32>::ToString()
extern Il2CppClass* StringU5BU5D_t4054002952_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral91;
extern Il2CppCodeGenString* _stringLiteral1396;
extern Il2CppCodeGenString* _stringLiteral93;
extern const uint32_t KeyValuePair_2_ToString_m3920933889_MetadataUsageId;
extern "C"  String_t* KeyValuePair_2_ToString_m3920933889_gshared (KeyValuePair_2_t2630286527 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m3920933889_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Int3_t1974045594  V_0;
	memset(&V_0, 0, sizeof(V_0));
	int32_t V_1 = 0;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t4054002952* G_B2_1 = NULL;
	StringU5BU5D_t4054002952* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t4054002952* G_B1_1 = NULL;
	StringU5BU5D_t4054002952* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t4054002952* G_B3_2 = NULL;
	StringU5BU5D_t4054002952* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t4054002952* G_B5_1 = NULL;
	StringU5BU5D_t4054002952* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t4054002952* G_B4_1 = NULL;
	StringU5BU5D_t4054002952* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t4054002952* G_B6_2 = NULL;
	StringU5BU5D_t4054002952* G_B6_3 = NULL;
	{
		StringU5BU5D_t4054002952* L_0 = (StringU5BU5D_t4054002952*)((StringU5BU5D_t4054002952*)SZArrayNew(StringU5BU5D_t4054002952_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral91);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral91);
		StringU5BU5D_t4054002952* L_1 = (StringU5BU5D_t4054002952*)L_0;
		Int3_t1974045594  L_2 = ((  Int3_t1974045594  (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t2630286527 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
	}
	{
		Int3_t1974045594  L_3 = ((  Int3_t1974045594  (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t2630286527 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (Int3_t1974045594 )L_3;
		String_t* L_4 = Int3_ToString_m37723046((Int3_t1974045594 *)(&V_0), /*hidden argument*/NULL);
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t4054002952* L_6 = (StringU5BU5D_t4054002952*)G_B3_3;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 2);
		ArrayElementTypeCheck (L_6, _stringLiteral1396);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral1396);
		StringU5BU5D_t4054002952* L_7 = (StringU5BU5D_t4054002952*)L_6;
		int32_t L_8 = ((  int32_t (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t2630286527 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
	}
	{
		int32_t L_9 = ((  int32_t (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t2630286527 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (int32_t)L_9;
		String_t* L_10 = Int32_ToString_m1286526384((int32_t*)(&V_1), /*hidden argument*/NULL);
		G_B6_0 = L_10;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_11;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t4054002952* L_12 = (StringU5BU5D_t4054002952*)G_B6_3;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 4);
		ArrayElementTypeCheck (L_12, _stringLiteral93);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral93);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m21867311(NULL /*static, unused*/, (StringU5BU5D_t4054002952*)L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m3920933889_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t2630286527 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2630286527 *>(__this + 1);
	return KeyValuePair_2_ToString_m3920933889(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<Pathfinding.Int3,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m1445654159_gshared (KeyValuePair_2_t1352297102 * __this, Int3_t1974045594  ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		Int3_t1974045594  L_0 = ___key0;
		((  void (*) (Il2CppObject *, Int3_t1974045594 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t1352297102 *)__this), (Int3_t1974045594 )L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject * L_1 = ___value1;
		((  void (*) (Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t1352297102 *)__this), (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m1445654159_AdjustorThunk (Il2CppObject * __this, Int3_t1974045594  ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t1352297102 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1352297102 *>(__this + 1);
	KeyValuePair_2__ctor_m1445654159(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<Pathfinding.Int3,System.Object>::get_Key()
extern "C"  Int3_t1974045594  KeyValuePair_2_get_Key_m680403065_gshared (KeyValuePair_2_t1352297102 * __this, const MethodInfo* method)
{
	{
		Int3_t1974045594  L_0 = (Int3_t1974045594 )__this->get_key_0();
		return L_0;
	}
}
extern "C"  Int3_t1974045594  KeyValuePair_2_get_Key_m680403065_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1352297102 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1352297102 *>(__this + 1);
	return KeyValuePair_2_get_Key_m680403065(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<Pathfinding.Int3,System.Object>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m444550714_gshared (KeyValuePair_2_t1352297102 * __this, Int3_t1974045594  ___value0, const MethodInfo* method)
{
	{
		Int3_t1974045594  L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m444550714_AdjustorThunk (Il2CppObject * __this, Int3_t1974045594  ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t1352297102 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1352297102 *>(__this + 1);
	KeyValuePair_2_set_Key_m444550714(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<Pathfinding.Int3,System.Object>::get_Value()
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m861079929_gshared (KeyValuePair_2_t1352297102 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_value_1();
		return L_0;
	}
}
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m861079929_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1352297102 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1352297102 *>(__this + 1);
	return KeyValuePair_2_get_Value_m861079929(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<Pathfinding.Int3,System.Object>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m95745338_gshared (KeyValuePair_2_t1352297102 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m95745338_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t1352297102 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1352297102 *>(__this + 1);
	KeyValuePair_2_set_Value_m95745338(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<Pathfinding.Int3,System.Object>::ToString()
extern Il2CppClass* StringU5BU5D_t4054002952_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral91;
extern Il2CppCodeGenString* _stringLiteral1396;
extern Il2CppCodeGenString* _stringLiteral93;
extern const uint32_t KeyValuePair_2_ToString_m3407486440_MetadataUsageId;
extern "C"  String_t* KeyValuePair_2_ToString_m3407486440_gshared (KeyValuePair_2_t1352297102 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m3407486440_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Int3_t1974045594  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Il2CppObject * V_1 = NULL;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t4054002952* G_B2_1 = NULL;
	StringU5BU5D_t4054002952* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t4054002952* G_B1_1 = NULL;
	StringU5BU5D_t4054002952* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t4054002952* G_B3_2 = NULL;
	StringU5BU5D_t4054002952* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t4054002952* G_B5_1 = NULL;
	StringU5BU5D_t4054002952* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t4054002952* G_B4_1 = NULL;
	StringU5BU5D_t4054002952* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t4054002952* G_B6_2 = NULL;
	StringU5BU5D_t4054002952* G_B6_3 = NULL;
	{
		StringU5BU5D_t4054002952* L_0 = (StringU5BU5D_t4054002952*)((StringU5BU5D_t4054002952*)SZArrayNew(StringU5BU5D_t4054002952_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral91);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral91);
		StringU5BU5D_t4054002952* L_1 = (StringU5BU5D_t4054002952*)L_0;
		Int3_t1974045594  L_2 = ((  Int3_t1974045594  (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t1352297102 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
	}
	{
		Int3_t1974045594  L_3 = ((  Int3_t1974045594  (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t1352297102 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (Int3_t1974045594 )L_3;
		String_t* L_4 = Int3_ToString_m37723046((Int3_t1974045594 *)(&V_0), /*hidden argument*/NULL);
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t4054002952* L_6 = (StringU5BU5D_t4054002952*)G_B3_3;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 2);
		ArrayElementTypeCheck (L_6, _stringLiteral1396);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral1396);
		StringU5BU5D_t4054002952* L_7 = (StringU5BU5D_t4054002952*)L_6;
		Il2CppObject * L_8 = ((  Il2CppObject * (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t1352297102 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
		if (!L_8)
		{
			G_B5_0 = 3;
			G_B5_1 = L_7;
			G_B5_2 = L_7;
			goto IL_0072;
		}
	}
	{
		Il2CppObject * L_9 = ((  Il2CppObject * (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t1352297102 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (Il2CppObject *)L_9;
		NullCheck((Il2CppObject *)(*(&V_1)));
		String_t* L_10 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_1)));
		G_B6_0 = L_10;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_11;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t4054002952* L_12 = (StringU5BU5D_t4054002952*)G_B6_3;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 4);
		ArrayElementTypeCheck (L_12, _stringLiteral93);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral93);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m21867311(NULL /*static, unused*/, (StringU5BU5D_t4054002952*)L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m3407486440_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1352297102 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1352297102 *>(__this + 1);
	return KeyValuePair_2_ToString_m3407486440(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<PushType,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m1781480841_gshared (KeyValuePair_2_t519615340 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___key0;
		((  void (*) (Il2CppObject *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t519615340 *)__this), (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject * L_1 = ___value1;
		((  void (*) (Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t519615340 *)__this), (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m1781480841_AdjustorThunk (Il2CppObject * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t519615340 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t519615340 *>(__this + 1);
	KeyValuePair_2__ctor_m1781480841(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<PushType,System.Object>::get_Key()
extern "C"  int32_t KeyValuePair_2_get_Key_m1078751295_gshared (KeyValuePair_2_t519615340 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_key_0();
		return L_0;
	}
}
extern "C"  int32_t KeyValuePair_2_get_Key_m1078751295_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t519615340 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t519615340 *>(__this + 1);
	return KeyValuePair_2_get_Key_m1078751295(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<PushType,System.Object>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m2104494848_gshared (KeyValuePair_2_t519615340 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m2104494848_AdjustorThunk (Il2CppObject * __this, int32_t ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t519615340 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t519615340 *>(__this + 1);
	KeyValuePair_2_set_Key_m2104494848(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<PushType,System.Object>::get_Value()
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m1445662143_gshared (KeyValuePair_2_t519615340 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_value_1();
		return L_0;
	}
}
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m1445662143_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t519615340 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t519615340 *>(__this + 1);
	return KeyValuePair_2_get_Value_m1445662143(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<PushType,System.Object>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m3570261760_gshared (KeyValuePair_2_t519615340 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m3570261760_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t519615340 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t519615340 *>(__this + 1);
	KeyValuePair_2_set_Value_m3570261760(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<PushType,System.Object>::ToString()
extern Il2CppClass* StringU5BU5D_t4054002952_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral91;
extern Il2CppCodeGenString* _stringLiteral1396;
extern Il2CppCodeGenString* _stringLiteral93;
extern const uint32_t KeyValuePair_2_ToString_m1871991778_MetadataUsageId;
extern "C"  String_t* KeyValuePair_2_ToString_m1871991778_gshared (KeyValuePair_2_t519615340 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m1871991778_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Il2CppObject * V_1 = NULL;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t4054002952* G_B2_1 = NULL;
	StringU5BU5D_t4054002952* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t4054002952* G_B1_1 = NULL;
	StringU5BU5D_t4054002952* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t4054002952* G_B3_2 = NULL;
	StringU5BU5D_t4054002952* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t4054002952* G_B5_1 = NULL;
	StringU5BU5D_t4054002952* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t4054002952* G_B4_1 = NULL;
	StringU5BU5D_t4054002952* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t4054002952* G_B6_2 = NULL;
	StringU5BU5D_t4054002952* G_B6_3 = NULL;
	{
		StringU5BU5D_t4054002952* L_0 = (StringU5BU5D_t4054002952*)((StringU5BU5D_t4054002952*)SZArrayNew(StringU5BU5D_t4054002952_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral91);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral91);
		StringU5BU5D_t4054002952* L_1 = (StringU5BU5D_t4054002952*)L_0;
		int32_t L_2 = ((  int32_t (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t519615340 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
	}
	{
		int32_t L_3 = ((  int32_t (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t519615340 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (int32_t)L_3;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (&V_0));
		NullCheck((Il2CppObject *)L_4);
		String_t* L_5 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)L_4);
		G_B3_0 = L_5;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_6;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t4054002952* L_7 = (StringU5BU5D_t4054002952*)G_B3_3;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 2);
		ArrayElementTypeCheck (L_7, _stringLiteral1396);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral1396);
		StringU5BU5D_t4054002952* L_8 = (StringU5BU5D_t4054002952*)L_7;
		Il2CppObject * L_9 = ((  Il2CppObject * (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t519615340 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_8;
		G_B4_2 = L_8;
		if (!L_9)
		{
			G_B5_0 = 3;
			G_B5_1 = L_8;
			G_B5_2 = L_8;
			goto IL_0072;
		}
	}
	{
		Il2CppObject * L_10 = ((  Il2CppObject * (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t519615340 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (Il2CppObject *)L_10;
		NullCheck((Il2CppObject *)(*(&V_1)));
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_1)));
		G_B6_0 = L_11;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_12;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t4054002952* L_13 = (StringU5BU5D_t4054002952*)G_B6_3;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 4);
		ArrayElementTypeCheck (L_13, _stringLiteral93);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral93);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m21867311(NULL /*static, unused*/, (StringU5BU5D_t4054002952*)L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m1871991778_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t519615340 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t519615340 *>(__this + 1);
	return KeyValuePair_2_ToString_m1871991778(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<SoundTypeID,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m2265684409_gshared (KeyValuePair_2_t1947212572 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___key0;
		((  void (*) (Il2CppObject *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t1947212572 *)__this), (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject * L_1 = ___value1;
		((  void (*) (Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t1947212572 *)__this), (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m2265684409_AdjustorThunk (Il2CppObject * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t1947212572 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1947212572 *>(__this + 1);
	KeyValuePair_2__ctor_m2265684409(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<SoundTypeID,System.Object>::get_Key()
extern "C"  int32_t KeyValuePair_2_get_Key_m50681231_gshared (KeyValuePair_2_t1947212572 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_key_0();
		return L_0;
	}
}
extern "C"  int32_t KeyValuePair_2_get_Key_m50681231_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1947212572 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1947212572 *>(__this + 1);
	return KeyValuePair_2_get_Key_m50681231(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<SoundTypeID,System.Object>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m3522613456_gshared (KeyValuePair_2_t1947212572 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m3522613456_AdjustorThunk (Il2CppObject * __this, int32_t ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t1947212572 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1947212572 *>(__this + 1);
	KeyValuePair_2_set_Key_m3522613456(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<SoundTypeID,System.Object>::get_Value()
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m1051595507_gshared (KeyValuePair_2_t1947212572 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_value_1();
		return L_0;
	}
}
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m1051595507_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1947212572 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1947212572 *>(__this + 1);
	return KeyValuePair_2_get_Value_m1051595507(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<SoundTypeID,System.Object>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m1507671248_gshared (KeyValuePair_2_t1947212572 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m1507671248_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t1947212572 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1947212572 *>(__this + 1);
	KeyValuePair_2_set_Value_m1507671248(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<SoundTypeID,System.Object>::ToString()
extern Il2CppClass* StringU5BU5D_t4054002952_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral91;
extern Il2CppCodeGenString* _stringLiteral1396;
extern Il2CppCodeGenString* _stringLiteral93;
extern const uint32_t KeyValuePair_2_ToString_m2770852088_MetadataUsageId;
extern "C"  String_t* KeyValuePair_2_ToString_m2770852088_gshared (KeyValuePair_2_t1947212572 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m2770852088_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Il2CppObject * V_1 = NULL;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t4054002952* G_B2_1 = NULL;
	StringU5BU5D_t4054002952* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t4054002952* G_B1_1 = NULL;
	StringU5BU5D_t4054002952* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t4054002952* G_B3_2 = NULL;
	StringU5BU5D_t4054002952* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t4054002952* G_B5_1 = NULL;
	StringU5BU5D_t4054002952* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t4054002952* G_B4_1 = NULL;
	StringU5BU5D_t4054002952* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t4054002952* G_B6_2 = NULL;
	StringU5BU5D_t4054002952* G_B6_3 = NULL;
	{
		StringU5BU5D_t4054002952* L_0 = (StringU5BU5D_t4054002952*)((StringU5BU5D_t4054002952*)SZArrayNew(StringU5BU5D_t4054002952_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral91);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral91);
		StringU5BU5D_t4054002952* L_1 = (StringU5BU5D_t4054002952*)L_0;
		int32_t L_2 = ((  int32_t (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t1947212572 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
	}
	{
		int32_t L_3 = ((  int32_t (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t1947212572 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (int32_t)L_3;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (&V_0));
		NullCheck((Il2CppObject *)L_4);
		String_t* L_5 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)L_4);
		G_B3_0 = L_5;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_6;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t4054002952* L_7 = (StringU5BU5D_t4054002952*)G_B3_3;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 2);
		ArrayElementTypeCheck (L_7, _stringLiteral1396);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral1396);
		StringU5BU5D_t4054002952* L_8 = (StringU5BU5D_t4054002952*)L_7;
		Il2CppObject * L_9 = ((  Il2CppObject * (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t1947212572 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_8;
		G_B4_2 = L_8;
		if (!L_9)
		{
			G_B5_0 = 3;
			G_B5_1 = L_8;
			G_B5_2 = L_8;
			goto IL_0072;
		}
	}
	{
		Il2CppObject * L_10 = ((  Il2CppObject * (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t1947212572 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (Il2CppObject *)L_10;
		NullCheck((Il2CppObject *)(*(&V_1)));
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_1)));
		G_B6_0 = L_11;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_12;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t4054002952* L_13 = (StringU5BU5D_t4054002952*)G_B6_3;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 4);
		ArrayElementTypeCheck (L_13, _stringLiteral93);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral93);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m21867311(NULL /*static, unused*/, (StringU5BU5D_t4054002952*)L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m2770852088_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1947212572 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1947212572 *>(__this + 1);
	return KeyValuePair_2_ToString_m2770852088(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,SoundStatus>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m3861120107_gshared (KeyValuePair_2_t3488982890 * __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___key0;
		((  void (*) (Il2CppObject *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t3488982890 *)__this), (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = ___value1;
		((  void (*) (Il2CppObject *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t3488982890 *)__this), (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m3861120107_AdjustorThunk (Il2CppObject * __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t3488982890 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3488982890 *>(__this + 1);
	KeyValuePair_2__ctor_m3861120107(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,SoundStatus>::get_Key()
extern "C"  int32_t KeyValuePair_2_get_Key_m1294005021_gshared (KeyValuePair_2_t3488982890 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_key_0();
		return L_0;
	}
}
extern "C"  int32_t KeyValuePair_2_get_Key_m1294005021_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3488982890 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3488982890 *>(__this + 1);
	return KeyValuePair_2_get_Key_m1294005021(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,SoundStatus>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m4239326942_gshared (KeyValuePair_2_t3488982890 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m4239326942_AdjustorThunk (Il2CppObject * __this, int32_t ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t3488982890 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3488982890 *>(__this + 1);
	KeyValuePair_2_set_Key_m4239326942(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,SoundStatus>::get_Value()
extern "C"  int32_t KeyValuePair_2_get_Value_m1077960093_gshared (KeyValuePair_2_t3488982890 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_value_1();
		return L_0;
	}
}
extern "C"  int32_t KeyValuePair_2_get_Value_m1077960093_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3488982890 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3488982890 *>(__this + 1);
	return KeyValuePair_2_get_Value_m1077960093(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,SoundStatus>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m4052988894_gshared (KeyValuePair_2_t3488982890 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m4052988894_AdjustorThunk (Il2CppObject * __this, int32_t ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t3488982890 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3488982890 *>(__this + 1);
	KeyValuePair_2_set_Value_m4052988894(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,SoundStatus>::ToString()
extern Il2CppClass* StringU5BU5D_t4054002952_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral91;
extern Il2CppCodeGenString* _stringLiteral1396;
extern Il2CppCodeGenString* _stringLiteral93;
extern const uint32_t KeyValuePair_2_ToString_m266981764_MetadataUsageId;
extern "C"  String_t* KeyValuePair_2_ToString_m266981764_gshared (KeyValuePair_2_t3488982890 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m266981764_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t4054002952* G_B2_1 = NULL;
	StringU5BU5D_t4054002952* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t4054002952* G_B1_1 = NULL;
	StringU5BU5D_t4054002952* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t4054002952* G_B3_2 = NULL;
	StringU5BU5D_t4054002952* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t4054002952* G_B5_1 = NULL;
	StringU5BU5D_t4054002952* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t4054002952* G_B4_1 = NULL;
	StringU5BU5D_t4054002952* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t4054002952* G_B6_2 = NULL;
	StringU5BU5D_t4054002952* G_B6_3 = NULL;
	{
		StringU5BU5D_t4054002952* L_0 = (StringU5BU5D_t4054002952*)((StringU5BU5D_t4054002952*)SZArrayNew(StringU5BU5D_t4054002952_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral91);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral91);
		StringU5BU5D_t4054002952* L_1 = (StringU5BU5D_t4054002952*)L_0;
		int32_t L_2 = ((  int32_t (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t3488982890 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
	}
	{
		int32_t L_3 = ((  int32_t (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t3488982890 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (int32_t)L_3;
		String_t* L_4 = Int32_ToString_m1286526384((int32_t*)(&V_0), /*hidden argument*/NULL);
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t4054002952* L_6 = (StringU5BU5D_t4054002952*)G_B3_3;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 2);
		ArrayElementTypeCheck (L_6, _stringLiteral1396);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral1396);
		StringU5BU5D_t4054002952* L_7 = (StringU5BU5D_t4054002952*)L_6;
		int32_t L_8 = ((  int32_t (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t3488982890 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
	}
	{
		int32_t L_9 = ((  int32_t (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t3488982890 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (int32_t)L_9;
		Il2CppObject * L_10 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (&V_1));
		NullCheck((Il2CppObject *)L_10);
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)L_10);
		G_B6_0 = L_11;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_12;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t4054002952* L_13 = (StringU5BU5D_t4054002952*)G_B6_3;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 4);
		ArrayElementTypeCheck (L_13, _stringLiteral93);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral93);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m21867311(NULL /*static, unused*/, (StringU5BU5D_t4054002952*)L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m266981764_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3488982890 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3488982890 *>(__this + 1);
	return KeyValuePair_2_ToString_m266981764(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m1072433347_gshared (KeyValuePair_2_t1049882445 * __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___key0;
		((  void (*) (Il2CppObject *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t1049882445 *)__this), (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = ___value1;
		((  void (*) (Il2CppObject *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t1049882445 *)__this), (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m1072433347_AdjustorThunk (Il2CppObject * __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t1049882445 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1049882445 *>(__this + 1);
	KeyValuePair_2__ctor_m1072433347(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>::get_Key()
extern "C"  int32_t KeyValuePair_2_get_Key_m293730880_gshared (KeyValuePair_2_t1049882445 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_key_0();
		return L_0;
	}
}
extern "C"  int32_t KeyValuePair_2_get_Key_m293730880_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1049882445 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1049882445 *>(__this + 1);
	return KeyValuePair_2_get_Key_m293730880(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m3084624774_gshared (KeyValuePair_2_t1049882445 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m3084624774_AdjustorThunk (Il2CppObject * __this, int32_t ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t1049882445 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1049882445 *>(__this + 1);
	KeyValuePair_2_set_Key_m3084624774(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>::get_Value()
extern "C"  int32_t KeyValuePair_2_get_Value_m2665882259_gshared (KeyValuePair_2_t1049882445 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_value_1();
		return L_0;
	}
}
extern "C"  int32_t KeyValuePair_2_get_Value_m2665882259_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1049882445 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1049882445 *>(__this + 1);
	return KeyValuePair_2_get_Value_m2665882259(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m1192084614_gshared (KeyValuePair_2_t1049882445 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m1192084614_AdjustorThunk (Il2CppObject * __this, int32_t ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t1049882445 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1049882445 *>(__this + 1);
	KeyValuePair_2_set_Value_m1192084614(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>::ToString()
extern Il2CppClass* StringU5BU5D_t4054002952_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral91;
extern Il2CppCodeGenString* _stringLiteral1396;
extern Il2CppCodeGenString* _stringLiteral93;
extern const uint32_t KeyValuePair_2_ToString_m2441409026_MetadataUsageId;
extern "C"  String_t* KeyValuePair_2_ToString_m2441409026_gshared (KeyValuePair_2_t1049882445 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m2441409026_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t4054002952* G_B2_1 = NULL;
	StringU5BU5D_t4054002952* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t4054002952* G_B1_1 = NULL;
	StringU5BU5D_t4054002952* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t4054002952* G_B3_2 = NULL;
	StringU5BU5D_t4054002952* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t4054002952* G_B5_1 = NULL;
	StringU5BU5D_t4054002952* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t4054002952* G_B4_1 = NULL;
	StringU5BU5D_t4054002952* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t4054002952* G_B6_2 = NULL;
	StringU5BU5D_t4054002952* G_B6_3 = NULL;
	{
		StringU5BU5D_t4054002952* L_0 = (StringU5BU5D_t4054002952*)((StringU5BU5D_t4054002952*)SZArrayNew(StringU5BU5D_t4054002952_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral91);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral91);
		StringU5BU5D_t4054002952* L_1 = (StringU5BU5D_t4054002952*)L_0;
		int32_t L_2 = ((  int32_t (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t1049882445 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
	}
	{
		int32_t L_3 = ((  int32_t (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t1049882445 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (int32_t)L_3;
		String_t* L_4 = Int32_ToString_m1286526384((int32_t*)(&V_0), /*hidden argument*/NULL);
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t4054002952* L_6 = (StringU5BU5D_t4054002952*)G_B3_3;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 2);
		ArrayElementTypeCheck (L_6, _stringLiteral1396);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral1396);
		StringU5BU5D_t4054002952* L_7 = (StringU5BU5D_t4054002952*)L_6;
		int32_t L_8 = ((  int32_t (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t1049882445 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
	}
	{
		int32_t L_9 = ((  int32_t (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t1049882445 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (int32_t)L_9;
		String_t* L_10 = Int32_ToString_m1286526384((int32_t*)(&V_1), /*hidden argument*/NULL);
		G_B6_0 = L_10;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_11;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t4054002952* L_12 = (StringU5BU5D_t4054002952*)G_B6_3;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 4);
		ArrayElementTypeCheck (L_12, _stringLiteral93);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral93);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m21867311(NULL /*static, unused*/, (StringU5BU5D_t4054002952*)L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m2441409026_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1049882445 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1049882445 *>(__this + 1);
	return KeyValuePair_2_ToString_m2441409026(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m11197230_gshared (KeyValuePair_2_t4066860316 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___key0;
		((  void (*) (Il2CppObject *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t4066860316 *)__this), (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject * L_1 = ___value1;
		((  void (*) (Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t4066860316 *)__this), (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m11197230_AdjustorThunk (Il2CppObject * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t4066860316 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t4066860316 *>(__this + 1);
	KeyValuePair_2__ctor_m11197230(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::get_Key()
extern "C"  int32_t KeyValuePair_2_get_Key_m494458106_gshared (KeyValuePair_2_t4066860316 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_key_0();
		return L_0;
	}
}
extern "C"  int32_t KeyValuePair_2_get_Key_m494458106_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t4066860316 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t4066860316 *>(__this + 1);
	return KeyValuePair_2_get_Key_m494458106(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m4229413435_gshared (KeyValuePair_2_t4066860316 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m4229413435_AdjustorThunk (Il2CppObject * __this, int32_t ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t4066860316 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t4066860316 *>(__this + 1);
	KeyValuePair_2_set_Key_m4229413435(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::get_Value()
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m1563175098_gshared (KeyValuePair_2_t4066860316 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_value_1();
		return L_0;
	}
}
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m1563175098_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t4066860316 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t4066860316 *>(__this + 1);
	return KeyValuePair_2_get_Value_m1563175098(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m1296398523_gshared (KeyValuePair_2_t4066860316 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m1296398523_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t4066860316 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t4066860316 *>(__this + 1);
	KeyValuePair_2_set_Value_m1296398523(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::ToString()
extern Il2CppClass* StringU5BU5D_t4054002952_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral91;
extern Il2CppCodeGenString* _stringLiteral1396;
extern Il2CppCodeGenString* _stringLiteral93;
extern const uint32_t KeyValuePair_2_ToString_m491888647_MetadataUsageId;
extern "C"  String_t* KeyValuePair_2_ToString_m491888647_gshared (KeyValuePair_2_t4066860316 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m491888647_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Il2CppObject * V_1 = NULL;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t4054002952* G_B2_1 = NULL;
	StringU5BU5D_t4054002952* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t4054002952* G_B1_1 = NULL;
	StringU5BU5D_t4054002952* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t4054002952* G_B3_2 = NULL;
	StringU5BU5D_t4054002952* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t4054002952* G_B5_1 = NULL;
	StringU5BU5D_t4054002952* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t4054002952* G_B4_1 = NULL;
	StringU5BU5D_t4054002952* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t4054002952* G_B6_2 = NULL;
	StringU5BU5D_t4054002952* G_B6_3 = NULL;
	{
		StringU5BU5D_t4054002952* L_0 = (StringU5BU5D_t4054002952*)((StringU5BU5D_t4054002952*)SZArrayNew(StringU5BU5D_t4054002952_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral91);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral91);
		StringU5BU5D_t4054002952* L_1 = (StringU5BU5D_t4054002952*)L_0;
		int32_t L_2 = ((  int32_t (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t4066860316 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
	}
	{
		int32_t L_3 = ((  int32_t (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t4066860316 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (int32_t)L_3;
		String_t* L_4 = Int32_ToString_m1286526384((int32_t*)(&V_0), /*hidden argument*/NULL);
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t4054002952* L_6 = (StringU5BU5D_t4054002952*)G_B3_3;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 2);
		ArrayElementTypeCheck (L_6, _stringLiteral1396);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral1396);
		StringU5BU5D_t4054002952* L_7 = (StringU5BU5D_t4054002952*)L_6;
		Il2CppObject * L_8 = ((  Il2CppObject * (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t4066860316 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
		if (!L_8)
		{
			G_B5_0 = 3;
			G_B5_1 = L_7;
			G_B5_2 = L_7;
			goto IL_0072;
		}
	}
	{
		Il2CppObject * L_9 = ((  Il2CppObject * (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t4066860316 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (Il2CppObject *)L_9;
		NullCheck((Il2CppObject *)(*(&V_1)));
		String_t* L_10 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_1)));
		G_B6_0 = L_10;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_11;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t4054002952* L_12 = (StringU5BU5D_t4054002952*)G_B6_3;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 4);
		ArrayElementTypeCheck (L_12, _stringLiteral93);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral93);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m21867311(NULL /*static, unused*/, (StringU5BU5D_t4054002952*)L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m491888647_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t4066860316 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t4066860316 *>(__this + 1);
	return KeyValuePair_2_ToString_m491888647(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Single>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m3045927365_gshared (KeyValuePair_2_t4187962917 * __this, int32_t ___key0, float ___value1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___key0;
		((  void (*) (Il2CppObject *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t4187962917 *)__this), (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		float L_1 = ___value1;
		((  void (*) (Il2CppObject *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t4187962917 *)__this), (float)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m3045927365_AdjustorThunk (Il2CppObject * __this, int32_t ___key0, float ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t4187962917 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t4187962917 *>(__this + 1);
	KeyValuePair_2__ctor_m3045927365(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,System.Single>::get_Key()
extern "C"  int32_t KeyValuePair_2_get_Key_m2508353667_gshared (KeyValuePair_2_t4187962917 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_key_0();
		return L_0;
	}
}
extern "C"  int32_t KeyValuePair_2_get_Key_m2508353667_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t4187962917 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t4187962917 *>(__this + 1);
	return KeyValuePair_2_get_Key_m2508353667(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Single>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m3613813060_gshared (KeyValuePair_2_t4187962917 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m3613813060_AdjustorThunk (Il2CppObject * __this, int32_t ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t4187962917 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t4187962917 *>(__this + 1);
	KeyValuePair_2_set_Key_m3613813060(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,System.Single>::get_Value()
extern "C"  float KeyValuePair_2_get_Value_m4181526019_gshared (KeyValuePair_2_t4187962917 * __this, const MethodInfo* method)
{
	{
		float L_0 = (float)__this->get_value_1();
		return L_0;
	}
}
extern "C"  float KeyValuePair_2_get_Value_m4181526019_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t4187962917 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t4187962917 *>(__this + 1);
	return KeyValuePair_2_get_Value_m4181526019(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Single>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m1948482372_gshared (KeyValuePair_2_t4187962917 * __this, float ___value0, const MethodInfo* method)
{
	{
		float L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m1948482372_AdjustorThunk (Il2CppObject * __this, float ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t4187962917 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t4187962917 *>(__this + 1);
	KeyValuePair_2_set_Value_m1948482372(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,System.Single>::ToString()
extern Il2CppClass* StringU5BU5D_t4054002952_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral91;
extern Il2CppCodeGenString* _stringLiteral1396;
extern Il2CppCodeGenString* _stringLiteral93;
extern const uint32_t KeyValuePair_2_ToString_m2793108894_MetadataUsageId;
extern "C"  String_t* KeyValuePair_2_ToString_m2793108894_gshared (KeyValuePair_2_t4187962917 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m2793108894_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	float V_1 = 0.0f;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t4054002952* G_B2_1 = NULL;
	StringU5BU5D_t4054002952* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t4054002952* G_B1_1 = NULL;
	StringU5BU5D_t4054002952* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t4054002952* G_B3_2 = NULL;
	StringU5BU5D_t4054002952* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t4054002952* G_B5_1 = NULL;
	StringU5BU5D_t4054002952* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t4054002952* G_B4_1 = NULL;
	StringU5BU5D_t4054002952* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t4054002952* G_B6_2 = NULL;
	StringU5BU5D_t4054002952* G_B6_3 = NULL;
	{
		StringU5BU5D_t4054002952* L_0 = (StringU5BU5D_t4054002952*)((StringU5BU5D_t4054002952*)SZArrayNew(StringU5BU5D_t4054002952_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral91);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral91);
		StringU5BU5D_t4054002952* L_1 = (StringU5BU5D_t4054002952*)L_0;
		int32_t L_2 = ((  int32_t (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t4187962917 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
	}
	{
		int32_t L_3 = ((  int32_t (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t4187962917 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (int32_t)L_3;
		String_t* L_4 = Int32_ToString_m1286526384((int32_t*)(&V_0), /*hidden argument*/NULL);
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t4054002952* L_6 = (StringU5BU5D_t4054002952*)G_B3_3;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 2);
		ArrayElementTypeCheck (L_6, _stringLiteral1396);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral1396);
		StringU5BU5D_t4054002952* L_7 = (StringU5BU5D_t4054002952*)L_6;
		float L_8 = ((  float (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t4187962917 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
	}
	{
		float L_9 = ((  float (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t4187962917 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (float)L_9;
		String_t* L_10 = Single_ToString_m5736032((float*)(&V_1), /*hidden argument*/NULL);
		G_B6_0 = L_10;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_11;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t4054002952* L_12 = (StringU5BU5D_t4054002952*)G_B6_3;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 4);
		ArrayElementTypeCheck (L_12, _stringLiteral93);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral93);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m21867311(NULL /*static, unused*/, (StringU5BU5D_t4054002952*)L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m2793108894_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t4187962917 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t4187962917 *>(__this + 1);
	return KeyValuePair_2_ToString_m2793108894(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,HatredCtrl/stValue>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m923468223_gshared (KeyValuePair_2_t1199798016 * __this, Il2CppObject * ___key0, stValue_t3425945410  ___value1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___key0;
		((  void (*) (Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t1199798016 *)__this), (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		stValue_t3425945410  L_1 = ___value1;
		((  void (*) (Il2CppObject *, stValue_t3425945410 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t1199798016 *)__this), (stValue_t3425945410 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m923468223_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___key0, stValue_t3425945410  ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t1199798016 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1199798016 *>(__this + 1);
	KeyValuePair_2__ctor_m923468223(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,HatredCtrl/stValue>::get_Key()
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m2179442505_gshared (KeyValuePair_2_t1199798016 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_key_0();
		return L_0;
	}
}
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m2179442505_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1199798016 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1199798016 *>(__this + 1);
	return KeyValuePair_2_get_Key_m2179442505(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,HatredCtrl/stValue>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m2806775050_gshared (KeyValuePair_2_t1199798016 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m2806775050_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t1199798016 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1199798016 *>(__this + 1);
	KeyValuePair_2_set_Key_m2806775050(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,HatredCtrl/stValue>::get_Value()
extern "C"  stValue_t3425945410  KeyValuePair_2_get_Value_m4264011977_gshared (KeyValuePair_2_t1199798016 * __this, const MethodInfo* method)
{
	{
		stValue_t3425945410  L_0 = (stValue_t3425945410 )__this->get_value_1();
		return L_0;
	}
}
extern "C"  stValue_t3425945410  KeyValuePair_2_get_Value_m4264011977_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1199798016 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1199798016 *>(__this + 1);
	return KeyValuePair_2_get_Value_m4264011977(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,HatredCtrl/stValue>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m3958225930_gshared (KeyValuePair_2_t1199798016 * __this, stValue_t3425945410  ___value0, const MethodInfo* method)
{
	{
		stValue_t3425945410  L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m3958225930_AdjustorThunk (Il2CppObject * __this, stValue_t3425945410  ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t1199798016 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1199798016 *>(__this + 1);
	KeyValuePair_2_set_Value_m3958225930(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,HatredCtrl/stValue>::ToString()
extern Il2CppClass* StringU5BU5D_t4054002952_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral91;
extern Il2CppCodeGenString* _stringLiteral1396;
extern Il2CppCodeGenString* _stringLiteral93;
extern const uint32_t KeyValuePair_2_ToString_m284841432_MetadataUsageId;
extern "C"  String_t* KeyValuePair_2_ToString_m284841432_gshared (KeyValuePair_2_t1199798016 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m284841432_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	stValue_t3425945410  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t G_B2_0 = 0;
	StringU5BU5D_t4054002952* G_B2_1 = NULL;
	StringU5BU5D_t4054002952* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t4054002952* G_B1_1 = NULL;
	StringU5BU5D_t4054002952* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t4054002952* G_B3_2 = NULL;
	StringU5BU5D_t4054002952* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t4054002952* G_B5_1 = NULL;
	StringU5BU5D_t4054002952* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t4054002952* G_B4_1 = NULL;
	StringU5BU5D_t4054002952* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t4054002952* G_B6_2 = NULL;
	StringU5BU5D_t4054002952* G_B6_3 = NULL;
	{
		StringU5BU5D_t4054002952* L_0 = (StringU5BU5D_t4054002952*)((StringU5BU5D_t4054002952*)SZArrayNew(StringU5BU5D_t4054002952_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral91);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral91);
		StringU5BU5D_t4054002952* L_1 = (StringU5BU5D_t4054002952*)L_0;
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t1199798016 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
		if (!L_2)
		{
			G_B2_0 = 1;
			G_B2_1 = L_1;
			G_B2_2 = L_1;
			goto IL_0039;
		}
	}
	{
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t1199798016 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (Il2CppObject *)L_3;
		NullCheck((Il2CppObject *)(*(&V_0)));
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_0)));
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t4054002952* L_6 = (StringU5BU5D_t4054002952*)G_B3_3;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 2);
		ArrayElementTypeCheck (L_6, _stringLiteral1396);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral1396);
		StringU5BU5D_t4054002952* L_7 = (StringU5BU5D_t4054002952*)L_6;
		stValue_t3425945410  L_8 = ((  stValue_t3425945410  (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t1199798016 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
	}
	{
		stValue_t3425945410  L_9 = ((  stValue_t3425945410  (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t1199798016 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (stValue_t3425945410 )L_9;
		Il2CppObject * L_10 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (&V_1));
		NullCheck((Il2CppObject *)L_10);
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)L_10);
		G_B6_0 = L_11;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_12;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t4054002952* L_13 = (StringU5BU5D_t4054002952*)G_B6_3;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 4);
		ArrayElementTypeCheck (L_13, _stringLiteral93);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral93);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m21867311(NULL /*static, unused*/, (StringU5BU5D_t4054002952*)L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m284841432_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1199798016 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1199798016 *>(__this + 1);
	return KeyValuePair_2_ToString_m284841432(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m333815527_gshared (KeyValuePair_2_t407833816 * __this, Il2CppObject * ___key0, ErrorInfo_t2633981210  ___value1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___key0;
		((  void (*) (Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t407833816 *)__this), (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ErrorInfo_t2633981210  L_1 = ___value1;
		((  void (*) (Il2CppObject *, ErrorInfo_t2633981210 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t407833816 *)__this), (ErrorInfo_t2633981210 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m333815527_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___key0, ErrorInfo_t2633981210  ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t407833816 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t407833816 *>(__this + 1);
	KeyValuePair_2__ctor_m333815527(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::get_Key()
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m3798965537_gshared (KeyValuePair_2_t407833816 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_key_0();
		return L_0;
	}
}
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m3798965537_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t407833816 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t407833816 *>(__this + 1);
	return KeyValuePair_2_get_Key_m3798965537(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m3619538658_gshared (KeyValuePair_2_t407833816 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m3619538658_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t407833816 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t407833816 *>(__this + 1);
	KeyValuePair_2_set_Key_m3619538658(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::get_Value()
extern "C"  ErrorInfo_t2633981210  KeyValuePair_2_get_Value_m2907532449_gshared (KeyValuePair_2_t407833816 * __this, const MethodInfo* method)
{
	{
		ErrorInfo_t2633981210  L_0 = (ErrorInfo_t2633981210 )__this->get_value_1();
		return L_0;
	}
}
extern "C"  ErrorInfo_t2633981210  KeyValuePair_2_get_Value_m2907532449_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t407833816 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t407833816 *>(__this + 1);
	return KeyValuePair_2_get_Value_m2907532449(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m2553731554_gshared (KeyValuePair_2_t407833816 * __this, ErrorInfo_t2633981210  ___value0, const MethodInfo* method)
{
	{
		ErrorInfo_t2633981210  L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m2553731554_AdjustorThunk (Il2CppObject * __this, ErrorInfo_t2633981210  ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t407833816 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t407833816 *>(__this + 1);
	KeyValuePair_2_set_Value_m2553731554(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::ToString()
extern Il2CppClass* StringU5BU5D_t4054002952_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral91;
extern Il2CppCodeGenString* _stringLiteral1396;
extern Il2CppCodeGenString* _stringLiteral93;
extern const uint32_t KeyValuePair_2_ToString_m2605285632_MetadataUsageId;
extern "C"  String_t* KeyValuePair_2_ToString_m2605285632_gshared (KeyValuePair_2_t407833816 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m2605285632_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	ErrorInfo_t2633981210  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t G_B2_0 = 0;
	StringU5BU5D_t4054002952* G_B2_1 = NULL;
	StringU5BU5D_t4054002952* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t4054002952* G_B1_1 = NULL;
	StringU5BU5D_t4054002952* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t4054002952* G_B3_2 = NULL;
	StringU5BU5D_t4054002952* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t4054002952* G_B5_1 = NULL;
	StringU5BU5D_t4054002952* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t4054002952* G_B4_1 = NULL;
	StringU5BU5D_t4054002952* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t4054002952* G_B6_2 = NULL;
	StringU5BU5D_t4054002952* G_B6_3 = NULL;
	{
		StringU5BU5D_t4054002952* L_0 = (StringU5BU5D_t4054002952*)((StringU5BU5D_t4054002952*)SZArrayNew(StringU5BU5D_t4054002952_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral91);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral91);
		StringU5BU5D_t4054002952* L_1 = (StringU5BU5D_t4054002952*)L_0;
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t407833816 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
		if (!L_2)
		{
			G_B2_0 = 1;
			G_B2_1 = L_1;
			G_B2_2 = L_1;
			goto IL_0039;
		}
	}
	{
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t407833816 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (Il2CppObject *)L_3;
		NullCheck((Il2CppObject *)(*(&V_0)));
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_0)));
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t4054002952* L_6 = (StringU5BU5D_t4054002952*)G_B3_3;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 2);
		ArrayElementTypeCheck (L_6, _stringLiteral1396);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral1396);
		StringU5BU5D_t4054002952* L_7 = (StringU5BU5D_t4054002952*)L_6;
		ErrorInfo_t2633981210  L_8 = ((  ErrorInfo_t2633981210  (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t407833816 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
	}
	{
		ErrorInfo_t2633981210  L_9 = ((  ErrorInfo_t2633981210  (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t407833816 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (ErrorInfo_t2633981210 )L_9;
		Il2CppObject * L_10 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (&V_1));
		NullCheck((Il2CppObject *)L_10);
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)L_10);
		G_B6_0 = L_11;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_12;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t4054002952* L_13 = (StringU5BU5D_t4054002952*)G_B6_3;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 4);
		ArrayElementTypeCheck (L_13, _stringLiteral93);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral93);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m21867311(NULL /*static, unused*/, (StringU5BU5D_t4054002952*)L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m2605285632_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t407833816 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t407833816 *>(__this + 1);
	return KeyValuePair_2_ToString_m2605285632(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m814088226_gshared (KeyValuePair_2_t4184235723 * __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___key0;
		((  void (*) (Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t4184235723 *)__this), (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = ___value1;
		((  void (*) (Il2CppObject *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t4184235723 *)__this), (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m814088226_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t4184235723 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t4184235723 *>(__this + 1);
	KeyValuePair_2__ctor_m814088226(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>::get_Key()
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m2801349894_gshared (KeyValuePair_2_t4184235723 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_key_0();
		return L_0;
	}
}
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m2801349894_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t4184235723 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t4184235723 *>(__this + 1);
	return KeyValuePair_2_get_Key_m2801349894(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m2758692039_gshared (KeyValuePair_2_t4184235723 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m2758692039_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t4184235723 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t4184235723 *>(__this + 1);
	KeyValuePair_2_set_Key_m2758692039(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>::get_Value()
extern "C"  int32_t KeyValuePair_2_get_Value_m3410492970_gshared (KeyValuePair_2_t4184235723 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_value_1();
		return L_0;
	}
}
extern "C"  int32_t KeyValuePair_2_get_Value_m3410492970_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t4184235723 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t4184235723 *>(__this + 1);
	return KeyValuePair_2_get_Value_m3410492970(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m3954697543_gshared (KeyValuePair_2_t4184235723 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m3954697543_AdjustorThunk (Il2CppObject * __this, int32_t ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t4184235723 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t4184235723 *>(__this + 1);
	KeyValuePair_2_set_Value_m3954697543(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>::ToString()
extern Il2CppClass* StringU5BU5D_t4054002952_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral91;
extern Il2CppCodeGenString* _stringLiteral1396;
extern Il2CppCodeGenString* _stringLiteral93;
extern const uint32_t KeyValuePair_2_ToString_m2134856353_MetadataUsageId;
extern "C"  String_t* KeyValuePair_2_ToString_m2134856353_gshared (KeyValuePair_2_t4184235723 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m2134856353_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t4054002952* G_B2_1 = NULL;
	StringU5BU5D_t4054002952* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t4054002952* G_B1_1 = NULL;
	StringU5BU5D_t4054002952* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t4054002952* G_B3_2 = NULL;
	StringU5BU5D_t4054002952* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t4054002952* G_B5_1 = NULL;
	StringU5BU5D_t4054002952* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t4054002952* G_B4_1 = NULL;
	StringU5BU5D_t4054002952* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t4054002952* G_B6_2 = NULL;
	StringU5BU5D_t4054002952* G_B6_3 = NULL;
	{
		StringU5BU5D_t4054002952* L_0 = (StringU5BU5D_t4054002952*)((StringU5BU5D_t4054002952*)SZArrayNew(StringU5BU5D_t4054002952_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral91);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral91);
		StringU5BU5D_t4054002952* L_1 = (StringU5BU5D_t4054002952*)L_0;
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t4184235723 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
		if (!L_2)
		{
			G_B2_0 = 1;
			G_B2_1 = L_1;
			G_B2_2 = L_1;
			goto IL_0039;
		}
	}
	{
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t4184235723 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (Il2CppObject *)L_3;
		NullCheck((Il2CppObject *)(*(&V_0)));
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_0)));
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t4054002952* L_6 = (StringU5BU5D_t4054002952*)G_B3_3;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 2);
		ArrayElementTypeCheck (L_6, _stringLiteral1396);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral1396);
		StringU5BU5D_t4054002952* L_7 = (StringU5BU5D_t4054002952*)L_6;
		int32_t L_8 = ((  int32_t (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t4184235723 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
	}
	{
		int32_t L_9 = ((  int32_t (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t4184235723 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (int32_t)L_9;
		Il2CppObject * L_10 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (&V_1));
		NullCheck((Il2CppObject *)L_10);
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)L_10);
		G_B6_0 = L_11;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_12;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t4054002952* L_13 = (StringU5BU5D_t4054002952*)G_B6_3;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 4);
		ArrayElementTypeCheck (L_13, _stringLiteral93);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral93);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m21867311(NULL /*static, unused*/, (StringU5BU5D_t4054002952*)L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m2134856353_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t4184235723 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t4184235723 *>(__this + 1);
	return KeyValuePair_2_ToString_m2134856353(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m32323851_gshared (KeyValuePair_2_t666182096 * __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___key0;
		((  void (*) (Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t666182096 *)__this), (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = ___value1;
		((  void (*) (Il2CppObject *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t666182096 *)__this), (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m32323851_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t666182096 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t666182096 *>(__this + 1);
	KeyValuePair_2__ctor_m32323851(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::get_Key()
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m509596157_gshared (KeyValuePair_2_t666182096 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_key_0();
		return L_0;
	}
}
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m509596157_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t666182096 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t666182096 *>(__this + 1);
	return KeyValuePair_2_get_Key_m509596157(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m2246010430_gshared (KeyValuePair_2_t666182096 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m2246010430_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t666182096 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t666182096 *>(__this + 1);
	KeyValuePair_2_set_Key_m2246010430(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::get_Value()
extern "C"  int32_t KeyValuePair_2_get_Value_m1901960161_gshared (KeyValuePair_2_t666182096 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_value_1();
		return L_0;
	}
}
extern "C"  int32_t KeyValuePair_2_get_Value_m1901960161_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t666182096 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t666182096 *>(__this + 1);
	return KeyValuePair_2_get_Value_m1901960161(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m327248702_gshared (KeyValuePair_2_t666182096 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m327248702_AdjustorThunk (Il2CppObject * __this, int32_t ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t666182096 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t666182096 *>(__this + 1);
	KeyValuePair_2_set_Value_m327248702(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::ToString()
extern Il2CppClass* StringU5BU5D_t4054002952_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral91;
extern Il2CppCodeGenString* _stringLiteral1396;
extern Il2CppCodeGenString* _stringLiteral93;
extern const uint32_t KeyValuePair_2_ToString_m3189658186_MetadataUsageId;
extern "C"  String_t* KeyValuePair_2_ToString_m3189658186_gshared (KeyValuePair_2_t666182096 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m3189658186_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t4054002952* G_B2_1 = NULL;
	StringU5BU5D_t4054002952* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t4054002952* G_B1_1 = NULL;
	StringU5BU5D_t4054002952* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t4054002952* G_B3_2 = NULL;
	StringU5BU5D_t4054002952* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t4054002952* G_B5_1 = NULL;
	StringU5BU5D_t4054002952* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t4054002952* G_B4_1 = NULL;
	StringU5BU5D_t4054002952* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t4054002952* G_B6_2 = NULL;
	StringU5BU5D_t4054002952* G_B6_3 = NULL;
	{
		StringU5BU5D_t4054002952* L_0 = (StringU5BU5D_t4054002952*)((StringU5BU5D_t4054002952*)SZArrayNew(StringU5BU5D_t4054002952_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral91);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral91);
		StringU5BU5D_t4054002952* L_1 = (StringU5BU5D_t4054002952*)L_0;
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t666182096 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
		if (!L_2)
		{
			G_B2_0 = 1;
			G_B2_1 = L_1;
			G_B2_2 = L_1;
			goto IL_0039;
		}
	}
	{
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t666182096 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (Il2CppObject *)L_3;
		NullCheck((Il2CppObject *)(*(&V_0)));
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_0)));
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t4054002952* L_6 = (StringU5BU5D_t4054002952*)G_B3_3;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 2);
		ArrayElementTypeCheck (L_6, _stringLiteral1396);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral1396);
		StringU5BU5D_t4054002952* L_7 = (StringU5BU5D_t4054002952*)L_6;
		int32_t L_8 = ((  int32_t (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t666182096 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
	}
	{
		int32_t L_9 = ((  int32_t (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t666182096 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (int32_t)L_9;
		Il2CppObject * L_10 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (&V_1));
		NullCheck((Il2CppObject *)L_10);
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)L_10);
		G_B6_0 = L_11;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_12;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t4054002952* L_13 = (StringU5BU5D_t4054002952*)G_B6_3;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 4);
		ArrayElementTypeCheck (L_13, _stringLiteral93);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral93);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m21867311(NULL /*static, unused*/, (StringU5BU5D_t4054002952*)L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m3189658186_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t666182096 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t666182096 *>(__this + 1);
	return KeyValuePair_2_ToString_m3189658186(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,ProductsCfgMgr/ProductInfo>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m3143780667_gshared (KeyValuePair_2_t3374811140 * __this, Il2CppObject * ___key0, ProductInfo_t1305991238  ___value1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___key0;
		((  void (*) (Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t3374811140 *)__this), (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ProductInfo_t1305991238  L_1 = ___value1;
		((  void (*) (Il2CppObject *, ProductInfo_t1305991238 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t3374811140 *)__this), (ProductInfo_t1305991238 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m3143780667_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___key0, ProductInfo_t1305991238  ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t3374811140 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3374811140 *>(__this + 1);
	KeyValuePair_2__ctor_m3143780667(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,ProductsCfgMgr/ProductInfo>::get_Key()
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m2376238157_gshared (KeyValuePair_2_t3374811140 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_key_0();
		return L_0;
	}
}
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m2376238157_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3374811140 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3374811140 *>(__this + 1);
	return KeyValuePair_2_get_Key_m2376238157(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,ProductsCfgMgr/ProductInfo>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m3324835854_gshared (KeyValuePair_2_t3374811140 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m3324835854_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t3374811140 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3374811140 *>(__this + 1);
	KeyValuePair_2_set_Key_m3324835854(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,ProductsCfgMgr/ProductInfo>::get_Value()
extern "C"  ProductInfo_t1305991238  KeyValuePair_2_get_Value_m2609530573_gshared (KeyValuePair_2_t3374811140 * __this, const MethodInfo* method)
{
	{
		ProductInfo_t1305991238  L_0 = (ProductInfo_t1305991238 )__this->get_value_1();
		return L_0;
	}
}
extern "C"  ProductInfo_t1305991238  KeyValuePair_2_get_Value_m2609530573_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3374811140 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3374811140 *>(__this + 1);
	return KeyValuePair_2_get_Value_m2609530573(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,ProductsCfgMgr/ProductInfo>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m1813091598_gshared (KeyValuePair_2_t3374811140 * __this, ProductInfo_t1305991238  ___value0, const MethodInfo* method)
{
	{
		ProductInfo_t1305991238  L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m1813091598_AdjustorThunk (Il2CppObject * __this, ProductInfo_t1305991238  ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t3374811140 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3374811140 *>(__this + 1);
	KeyValuePair_2_set_Value_m1813091598(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,ProductsCfgMgr/ProductInfo>::ToString()
extern Il2CppClass* StringU5BU5D_t4054002952_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral91;
extern Il2CppCodeGenString* _stringLiteral1396;
extern Il2CppCodeGenString* _stringLiteral93;
extern const uint32_t KeyValuePair_2_ToString_m4080585812_MetadataUsageId;
extern "C"  String_t* KeyValuePair_2_ToString_m4080585812_gshared (KeyValuePair_2_t3374811140 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m4080585812_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	ProductInfo_t1305991238  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t G_B2_0 = 0;
	StringU5BU5D_t4054002952* G_B2_1 = NULL;
	StringU5BU5D_t4054002952* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t4054002952* G_B1_1 = NULL;
	StringU5BU5D_t4054002952* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t4054002952* G_B3_2 = NULL;
	StringU5BU5D_t4054002952* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t4054002952* G_B5_1 = NULL;
	StringU5BU5D_t4054002952* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t4054002952* G_B4_1 = NULL;
	StringU5BU5D_t4054002952* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t4054002952* G_B6_2 = NULL;
	StringU5BU5D_t4054002952* G_B6_3 = NULL;
	{
		StringU5BU5D_t4054002952* L_0 = (StringU5BU5D_t4054002952*)((StringU5BU5D_t4054002952*)SZArrayNew(StringU5BU5D_t4054002952_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral91);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral91);
		StringU5BU5D_t4054002952* L_1 = (StringU5BU5D_t4054002952*)L_0;
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t3374811140 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
		if (!L_2)
		{
			G_B2_0 = 1;
			G_B2_1 = L_1;
			G_B2_2 = L_1;
			goto IL_0039;
		}
	}
	{
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t3374811140 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (Il2CppObject *)L_3;
		NullCheck((Il2CppObject *)(*(&V_0)));
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_0)));
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t4054002952* L_6 = (StringU5BU5D_t4054002952*)G_B3_3;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 2);
		ArrayElementTypeCheck (L_6, _stringLiteral1396);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral1396);
		StringU5BU5D_t4054002952* L_7 = (StringU5BU5D_t4054002952*)L_6;
		ProductInfo_t1305991238  L_8 = ((  ProductInfo_t1305991238  (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t3374811140 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
	}
	{
		ProductInfo_t1305991238  L_9 = ((  ProductInfo_t1305991238  (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t3374811140 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (ProductInfo_t1305991238 )L_9;
		Il2CppObject * L_10 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (&V_1));
		NullCheck((Il2CppObject *)L_10);
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)L_10);
		G_B6_0 = L_11;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_12;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t4054002952* L_13 = (StringU5BU5D_t4054002952*)G_B6_3;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 4);
		ArrayElementTypeCheck (L_13, _stringLiteral93);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral93);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m21867311(NULL /*static, unused*/, (StringU5BU5D_t4054002952*)L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m4080585812_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3374811140 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3374811140 *>(__this + 1);
	return KeyValuePair_2_ToString_m4080585812(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m2040323320_gshared (KeyValuePair_2_t2545618620 * __this, Il2CppObject * ___key0, bool ___value1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___key0;
		((  void (*) (Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t2545618620 *)__this), (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		bool L_1 = ___value1;
		((  void (*) (Il2CppObject *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t2545618620 *)__this), (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m2040323320_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___key0, bool ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t2545618620 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2545618620 *>(__this + 1);
	KeyValuePair_2__ctor_m2040323320(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::get_Key()
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m700889072_gshared (KeyValuePair_2_t2545618620 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_key_0();
		return L_0;
	}
}
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m700889072_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t2545618620 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2545618620 *>(__this + 1);
	return KeyValuePair_2_get_Key_m700889072(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m1751794225_gshared (KeyValuePair_2_t2545618620 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m1751794225_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t2545618620 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2545618620 *>(__this + 1);
	KeyValuePair_2_set_Key_m1751794225(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::get_Value()
extern "C"  bool KeyValuePair_2_get_Value_m3809014448_gshared (KeyValuePair_2_t2545618620 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_value_1();
		return L_0;
	}
}
extern "C"  bool KeyValuePair_2_get_Value_m3809014448_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t2545618620 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2545618620 *>(__this + 1);
	return KeyValuePair_2_get_Value_m3809014448(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m3162969521_gshared (KeyValuePair_2_t2545618620 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m3162969521_AdjustorThunk (Il2CppObject * __this, bool ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t2545618620 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2545618620 *>(__this + 1);
	KeyValuePair_2_set_Value_m3162969521(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::ToString()
extern Il2CppClass* StringU5BU5D_t4054002952_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral91;
extern Il2CppCodeGenString* _stringLiteral1396;
extern Il2CppCodeGenString* _stringLiteral93;
extern const uint32_t KeyValuePair_2_ToString_m3396952209_MetadataUsageId;
extern "C"  String_t* KeyValuePair_2_ToString_m3396952209_gshared (KeyValuePair_2_t2545618620 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m3396952209_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	bool V_1 = false;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t4054002952* G_B2_1 = NULL;
	StringU5BU5D_t4054002952* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t4054002952* G_B1_1 = NULL;
	StringU5BU5D_t4054002952* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t4054002952* G_B3_2 = NULL;
	StringU5BU5D_t4054002952* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t4054002952* G_B5_1 = NULL;
	StringU5BU5D_t4054002952* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t4054002952* G_B4_1 = NULL;
	StringU5BU5D_t4054002952* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t4054002952* G_B6_2 = NULL;
	StringU5BU5D_t4054002952* G_B6_3 = NULL;
	{
		StringU5BU5D_t4054002952* L_0 = (StringU5BU5D_t4054002952*)((StringU5BU5D_t4054002952*)SZArrayNew(StringU5BU5D_t4054002952_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral91);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral91);
		StringU5BU5D_t4054002952* L_1 = (StringU5BU5D_t4054002952*)L_0;
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t2545618620 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
		if (!L_2)
		{
			G_B2_0 = 1;
			G_B2_1 = L_1;
			G_B2_2 = L_1;
			goto IL_0039;
		}
	}
	{
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t2545618620 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (Il2CppObject *)L_3;
		NullCheck((Il2CppObject *)(*(&V_0)));
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_0)));
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t4054002952* L_6 = (StringU5BU5D_t4054002952*)G_B3_3;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 2);
		ArrayElementTypeCheck (L_6, _stringLiteral1396);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral1396);
		StringU5BU5D_t4054002952* L_7 = (StringU5BU5D_t4054002952*)L_6;
		bool L_8 = ((  bool (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t2545618620 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
	}
	{
		bool L_9 = ((  bool (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t2545618620 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (bool)L_9;
		String_t* L_10 = Boolean_ToString_m2512358154((bool*)(&V_1), /*hidden argument*/NULL);
		G_B6_0 = L_10;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_11;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t4054002952* L_12 = (StringU5BU5D_t4054002952*)G_B6_3;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 4);
		ArrayElementTypeCheck (L_12, _stringLiteral93);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral93);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m21867311(NULL /*static, unused*/, (StringU5BU5D_t4054002952*)L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m3396952209_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t2545618620 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2545618620 *>(__this + 1);
	return KeyValuePair_2_ToString_m3396952209(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m1079219282_gshared (KeyValuePair_2_t2061784035 * __this, Il2CppObject * ___key0, KeyValuePair_2_t4287931429  ___value1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___key0;
		((  void (*) (Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t2061784035 *)__this), (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t4287931429  L_1 = ___value1;
		((  void (*) (Il2CppObject *, KeyValuePair_2_t4287931429 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t2061784035 *)__this), (KeyValuePair_2_t4287931429 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m1079219282_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___key0, KeyValuePair_2_t4287931429  ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t2061784035 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2061784035 *>(__this + 1);
	KeyValuePair_2__ctor_m1079219282(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::get_Key()
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m1584147158_gshared (KeyValuePair_2_t2061784035 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_key_0();
		return L_0;
	}
}
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m1584147158_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t2061784035 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2061784035 *>(__this + 1);
	return KeyValuePair_2_get_Key_m1584147158(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m1377884823_gshared (KeyValuePair_2_t2061784035 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m1377884823_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t2061784035 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2061784035 *>(__this + 1);
	KeyValuePair_2_set_Key_m1377884823(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::get_Value()
extern "C"  KeyValuePair_2_t4287931429  KeyValuePair_2_get_Value_m2108684282_gshared (KeyValuePair_2_t2061784035 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t4287931429  L_0 = (KeyValuePair_2_t4287931429 )__this->get_value_1();
		return L_0;
	}
}
extern "C"  KeyValuePair_2_t4287931429  KeyValuePair_2_get_Value_m2108684282_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t2061784035 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2061784035 *>(__this + 1);
	return KeyValuePair_2_get_Value_m2108684282(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m1053756183_gshared (KeyValuePair_2_t2061784035 * __this, KeyValuePair_2_t4287931429  ___value0, const MethodInfo* method)
{
	{
		KeyValuePair_2_t4287931429  L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m1053756183_AdjustorThunk (Il2CppObject * __this, KeyValuePair_2_t4287931429  ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t2061784035 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2061784035 *>(__this + 1);
	KeyValuePair_2_set_Value_m1053756183(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::ToString()
extern Il2CppClass* StringU5BU5D_t4054002952_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* KeyValuePair_2_ToString_m1712840292_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral91;
extern Il2CppCodeGenString* _stringLiteral1396;
extern Il2CppCodeGenString* _stringLiteral93;
extern const uint32_t KeyValuePair_2_ToString_m3764046545_MetadataUsageId;
extern "C"  String_t* KeyValuePair_2_ToString_m3764046545_gshared (KeyValuePair_2_t2061784035 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m3764046545_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	KeyValuePair_2_t4287931429  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t G_B2_0 = 0;
	StringU5BU5D_t4054002952* G_B2_1 = NULL;
	StringU5BU5D_t4054002952* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t4054002952* G_B1_1 = NULL;
	StringU5BU5D_t4054002952* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t4054002952* G_B3_2 = NULL;
	StringU5BU5D_t4054002952* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t4054002952* G_B5_1 = NULL;
	StringU5BU5D_t4054002952* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t4054002952* G_B4_1 = NULL;
	StringU5BU5D_t4054002952* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t4054002952* G_B6_2 = NULL;
	StringU5BU5D_t4054002952* G_B6_3 = NULL;
	{
		StringU5BU5D_t4054002952* L_0 = (StringU5BU5D_t4054002952*)((StringU5BU5D_t4054002952*)SZArrayNew(StringU5BU5D_t4054002952_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral91);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral91);
		StringU5BU5D_t4054002952* L_1 = (StringU5BU5D_t4054002952*)L_0;
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t2061784035 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
		if (!L_2)
		{
			G_B2_0 = 1;
			G_B2_1 = L_1;
			G_B2_2 = L_1;
			goto IL_0039;
		}
	}
	{
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t2061784035 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (Il2CppObject *)L_3;
		NullCheck((Il2CppObject *)(*(&V_0)));
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_0)));
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t4054002952* L_6 = (StringU5BU5D_t4054002952*)G_B3_3;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 2);
		ArrayElementTypeCheck (L_6, _stringLiteral1396);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral1396);
		StringU5BU5D_t4054002952* L_7 = (StringU5BU5D_t4054002952*)L_6;
		KeyValuePair_2_t4287931429  L_8 = ((  KeyValuePair_2_t4287931429  (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t2061784035 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
	}
	{
		KeyValuePair_2_t4287931429  L_9 = ((  KeyValuePair_2_t4287931429  (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t2061784035 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (KeyValuePair_2_t4287931429 )L_9;
		String_t* L_10 = KeyValuePair_2_ToString_m1712840292((KeyValuePair_2_t4287931429 *)(&V_1), /*hidden argument*/KeyValuePair_2_ToString_m1712840292_MethodInfo_var);
		G_B6_0 = L_10;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_11;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t4054002952* L_12 = (StringU5BU5D_t4054002952*)G_B6_3;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 4);
		ArrayElementTypeCheck (L_12, _stringLiteral93);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral93);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m21867311(NULL /*static, unused*/, (StringU5BU5D_t4054002952*)L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m3764046545_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t2061784035 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2061784035 *>(__this + 1);
	return KeyValuePair_2_ToString_m3764046545(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m2730552978_gshared (KeyValuePair_2_t3222658402 * __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___key0;
		((  void (*) (Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t3222658402 *)__this), (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = ___value1;
		((  void (*) (Il2CppObject *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t3222658402 *)__this), (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m2730552978_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t3222658402 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3222658402 *>(__this + 1);
	KeyValuePair_2__ctor_m2730552978(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::get_Key()
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m4285571350_gshared (KeyValuePair_2_t3222658402 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_key_0();
		return L_0;
	}
}
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m4285571350_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3222658402 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3222658402 *>(__this + 1);
	return KeyValuePair_2_get_Key_m4285571350(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m1188304983_gshared (KeyValuePair_2_t3222658402 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m1188304983_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t3222658402 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3222658402 *>(__this + 1);
	KeyValuePair_2_set_Key_m1188304983(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::get_Value()
extern "C"  int32_t KeyValuePair_2_get_Value_m2690735574_gshared (KeyValuePair_2_t3222658402 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_value_1();
		return L_0;
	}
}
extern "C"  int32_t KeyValuePair_2_get_Value_m2690735574_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3222658402 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3222658402 *>(__this + 1);
	return KeyValuePair_2_get_Value_m2690735574(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m137193687_gshared (KeyValuePair_2_t3222658402 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m137193687_AdjustorThunk (Il2CppObject * __this, int32_t ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t3222658402 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3222658402 *>(__this + 1);
	KeyValuePair_2_set_Value_m137193687(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::ToString()
extern Il2CppClass* StringU5BU5D_t4054002952_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral91;
extern Il2CppCodeGenString* _stringLiteral1396;
extern Il2CppCodeGenString* _stringLiteral93;
extern const uint32_t KeyValuePair_2_ToString_m2052282219_MetadataUsageId;
extern "C"  String_t* KeyValuePair_2_ToString_m2052282219_gshared (KeyValuePair_2_t3222658402 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m2052282219_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t4054002952* G_B2_1 = NULL;
	StringU5BU5D_t4054002952* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t4054002952* G_B1_1 = NULL;
	StringU5BU5D_t4054002952* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t4054002952* G_B3_2 = NULL;
	StringU5BU5D_t4054002952* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t4054002952* G_B5_1 = NULL;
	StringU5BU5D_t4054002952* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t4054002952* G_B4_1 = NULL;
	StringU5BU5D_t4054002952* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t4054002952* G_B6_2 = NULL;
	StringU5BU5D_t4054002952* G_B6_3 = NULL;
	{
		StringU5BU5D_t4054002952* L_0 = (StringU5BU5D_t4054002952*)((StringU5BU5D_t4054002952*)SZArrayNew(StringU5BU5D_t4054002952_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral91);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral91);
		StringU5BU5D_t4054002952* L_1 = (StringU5BU5D_t4054002952*)L_0;
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t3222658402 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
		if (!L_2)
		{
			G_B2_0 = 1;
			G_B2_1 = L_1;
			G_B2_2 = L_1;
			goto IL_0039;
		}
	}
	{
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t3222658402 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (Il2CppObject *)L_3;
		NullCheck((Il2CppObject *)(*(&V_0)));
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_0)));
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t4054002952* L_6 = (StringU5BU5D_t4054002952*)G_B3_3;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 2);
		ArrayElementTypeCheck (L_6, _stringLiteral1396);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral1396);
		StringU5BU5D_t4054002952* L_7 = (StringU5BU5D_t4054002952*)L_6;
		int32_t L_8 = ((  int32_t (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t3222658402 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
	}
	{
		int32_t L_9 = ((  int32_t (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t3222658402 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (int32_t)L_9;
		String_t* L_10 = Int32_ToString_m1286526384((int32_t*)(&V_1), /*hidden argument*/NULL);
		G_B6_0 = L_10;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_11;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t4054002952* L_12 = (StringU5BU5D_t4054002952*)G_B6_3;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 4);
		ArrayElementTypeCheck (L_12, _stringLiteral93);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral93);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m21867311(NULL /*static, unused*/, (StringU5BU5D_t4054002952*)L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m2052282219_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3222658402 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3222658402 *>(__this + 1);
	return KeyValuePair_2_ToString_m2052282219(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m4168265535_gshared (KeyValuePair_2_t1944668977 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___key0;
		((  void (*) (Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t1944668977 *)__this), (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject * L_1 = ___value1;
		((  void (*) (Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t1944668977 *)__this), (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m4168265535_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t1944668977 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1944668977 *>(__this + 1);
	KeyValuePair_2__ctor_m4168265535(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::get_Key()
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m3256475977_gshared (KeyValuePair_2_t1944668977 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_key_0();
		return L_0;
	}
}
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m3256475977_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1944668977 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1944668977 *>(__this + 1);
	return KeyValuePair_2_get_Key_m3256475977(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m1278074762_gshared (KeyValuePair_2_t1944668977 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m1278074762_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t1944668977 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1944668977 *>(__this + 1);
	KeyValuePair_2_set_Key_m1278074762(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::get_Value()
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m3899079597_gshared (KeyValuePair_2_t1944668977 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_value_1();
		return L_0;
	}
}
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m3899079597_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1944668977 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1944668977 *>(__this + 1);
	return KeyValuePair_2_get_Value_m3899079597(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m2954518154_gshared (KeyValuePair_2_t1944668977 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m2954518154_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t1944668977 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1944668977 *>(__this + 1);
	KeyValuePair_2_set_Value_m2954518154(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::ToString()
extern Il2CppClass* StringU5BU5D_t4054002952_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral91;
extern Il2CppCodeGenString* _stringLiteral1396;
extern Il2CppCodeGenString* _stringLiteral93;
extern const uint32_t KeyValuePair_2_ToString_m1313859518_MetadataUsageId;
extern "C"  String_t* KeyValuePair_2_ToString_m1313859518_gshared (KeyValuePair_2_t1944668977 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m1313859518_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t4054002952* G_B2_1 = NULL;
	StringU5BU5D_t4054002952* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t4054002952* G_B1_1 = NULL;
	StringU5BU5D_t4054002952* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t4054002952* G_B3_2 = NULL;
	StringU5BU5D_t4054002952* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t4054002952* G_B5_1 = NULL;
	StringU5BU5D_t4054002952* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t4054002952* G_B4_1 = NULL;
	StringU5BU5D_t4054002952* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t4054002952* G_B6_2 = NULL;
	StringU5BU5D_t4054002952* G_B6_3 = NULL;
	{
		StringU5BU5D_t4054002952* L_0 = (StringU5BU5D_t4054002952*)((StringU5BU5D_t4054002952*)SZArrayNew(StringU5BU5D_t4054002952_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral91);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral91);
		StringU5BU5D_t4054002952* L_1 = (StringU5BU5D_t4054002952*)L_0;
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t1944668977 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
		if (!L_2)
		{
			G_B2_0 = 1;
			G_B2_1 = L_1;
			G_B2_2 = L_1;
			goto IL_0039;
		}
	}
	{
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t1944668977 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (Il2CppObject *)L_3;
		NullCheck((Il2CppObject *)(*(&V_0)));
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_0)));
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t4054002952* L_6 = (StringU5BU5D_t4054002952*)G_B3_3;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 2);
		ArrayElementTypeCheck (L_6, _stringLiteral1396);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral1396);
		StringU5BU5D_t4054002952* L_7 = (StringU5BU5D_t4054002952*)L_6;
		Il2CppObject * L_8 = ((  Il2CppObject * (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t1944668977 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
		if (!L_8)
		{
			G_B5_0 = 3;
			G_B5_1 = L_7;
			G_B5_2 = L_7;
			goto IL_0072;
		}
	}
	{
		Il2CppObject * L_9 = ((  Il2CppObject * (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t1944668977 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (Il2CppObject *)L_9;
		NullCheck((Il2CppObject *)(*(&V_1)));
		String_t* L_10 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_1)));
		G_B6_0 = L_10;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_11;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t4054002952* L_12 = (StringU5BU5D_t4054002952*)G_B6_3;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 4);
		ArrayElementTypeCheck (L_12, _stringLiteral93);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral93);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m21867311(NULL /*static, unused*/, (StringU5BU5D_t4054002952*)L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m1313859518_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1944668977 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1944668977 *>(__this + 1);
	return KeyValuePair_2_ToString_m1313859518(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m2908028374_gshared (KeyValuePair_2_t2065771578 * __this, Il2CppObject * ___key0, float ___value1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___key0;
		((  void (*) (Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t2065771578 *)__this), (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		float L_1 = ___value1;
		((  void (*) (Il2CppObject *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t2065771578 *)__this), (float)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m2908028374_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___key0, float ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t2065771578 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2065771578 *>(__this + 1);
	KeyValuePair_2__ctor_m2908028374(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>::get_Key()
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m975404242_gshared (KeyValuePair_2_t2065771578 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_key_0();
		return L_0;
	}
}
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m975404242_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t2065771578 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2065771578 *>(__this + 1);
	return KeyValuePair_2_get_Key_m975404242(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m662474387_gshared (KeyValuePair_2_t2065771578 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m662474387_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t2065771578 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2065771578 *>(__this + 1);
	KeyValuePair_2_set_Key_m662474387(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>::get_Value()
extern "C"  float KeyValuePair_2_get_Value_m2222463222_gshared (KeyValuePair_2_t2065771578 * __this, const MethodInfo* method)
{
	{
		float L_0 = (float)__this->get_value_1();
		return L_0;
	}
}
extern "C"  float KeyValuePair_2_get_Value_m2222463222_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t2065771578 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2065771578 *>(__this + 1);
	return KeyValuePair_2_get_Value_m2222463222(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m3606602003_gshared (KeyValuePair_2_t2065771578 * __this, float ___value0, const MethodInfo* method)
{
	{
		float L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m3606602003_AdjustorThunk (Il2CppObject * __this, float ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t2065771578 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2065771578 *>(__this + 1);
	KeyValuePair_2_set_Value_m3606602003(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>::ToString()
extern Il2CppClass* StringU5BU5D_t4054002952_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral91;
extern Il2CppCodeGenString* _stringLiteral1396;
extern Il2CppCodeGenString* _stringLiteral93;
extern const uint32_t KeyValuePair_2_ToString_m3615079765_MetadataUsageId;
extern "C"  String_t* KeyValuePair_2_ToString_m3615079765_gshared (KeyValuePair_2_t2065771578 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m3615079765_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	float V_1 = 0.0f;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t4054002952* G_B2_1 = NULL;
	StringU5BU5D_t4054002952* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t4054002952* G_B1_1 = NULL;
	StringU5BU5D_t4054002952* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t4054002952* G_B3_2 = NULL;
	StringU5BU5D_t4054002952* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t4054002952* G_B5_1 = NULL;
	StringU5BU5D_t4054002952* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t4054002952* G_B4_1 = NULL;
	StringU5BU5D_t4054002952* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t4054002952* G_B6_2 = NULL;
	StringU5BU5D_t4054002952* G_B6_3 = NULL;
	{
		StringU5BU5D_t4054002952* L_0 = (StringU5BU5D_t4054002952*)((StringU5BU5D_t4054002952*)SZArrayNew(StringU5BU5D_t4054002952_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral91);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral91);
		StringU5BU5D_t4054002952* L_1 = (StringU5BU5D_t4054002952*)L_0;
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t2065771578 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
		if (!L_2)
		{
			G_B2_0 = 1;
			G_B2_1 = L_1;
			G_B2_2 = L_1;
			goto IL_0039;
		}
	}
	{
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t2065771578 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (Il2CppObject *)L_3;
		NullCheck((Il2CppObject *)(*(&V_0)));
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_0)));
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t4054002952* L_6 = (StringU5BU5D_t4054002952*)G_B3_3;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 2);
		ArrayElementTypeCheck (L_6, _stringLiteral1396);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral1396);
		StringU5BU5D_t4054002952* L_7 = (StringU5BU5D_t4054002952*)L_6;
		float L_8 = ((  float (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t2065771578 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
	}
	{
		float L_9 = ((  float (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t2065771578 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (float)L_9;
		String_t* L_10 = Single_ToString_m5736032((float*)(&V_1), /*hidden argument*/NULL);
		G_B6_0 = L_10;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_11;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t4054002952* L_12 = (StringU5BU5D_t4054002952*)G_B6_3;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 4);
		ArrayElementTypeCheck (L_12, _stringLiteral93);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral93);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m21867311(NULL /*static, unused*/, (StringU5BU5D_t4054002952*)L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m3615079765_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t2065771578 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2065771578 *>(__this + 1);
	return KeyValuePair_2_ToString_m3615079765(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m3840239519_gshared (KeyValuePair_2_t2093487825 * __this, Il2CppObject * ___key0, uint16_t ___value1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___key0;
		((  void (*) (Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t2093487825 *)__this), (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		uint16_t L_1 = ___value1;
		((  void (*) (Il2CppObject *, uint16_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t2093487825 *)__this), (uint16_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m3840239519_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___key0, uint16_t ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t2093487825 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2093487825 *>(__this + 1);
	KeyValuePair_2__ctor_m3840239519(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>::get_Key()
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m1711553769_gshared (KeyValuePair_2_t2093487825 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_key_0();
		return L_0;
	}
}
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m1711553769_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t2093487825 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2093487825 *>(__this + 1);
	return KeyValuePair_2_get_Key_m1711553769(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m4131482410_gshared (KeyValuePair_2_t2093487825 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m4131482410_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t2093487825 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2093487825 *>(__this + 1);
	KeyValuePair_2_set_Key_m4131482410(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>::get_Value()
extern "C"  uint16_t KeyValuePair_2_get_Value_m992554829_gshared (KeyValuePair_2_t2093487825 * __this, const MethodInfo* method)
{
	{
		uint16_t L_0 = (uint16_t)__this->get_value_1();
		return L_0;
	}
}
extern "C"  uint16_t KeyValuePair_2_get_Value_m992554829_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t2093487825 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2093487825 *>(__this + 1);
	return KeyValuePair_2_get_Value_m992554829(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m3359578666_gshared (KeyValuePair_2_t2093487825 * __this, uint16_t ___value0, const MethodInfo* method)
{
	{
		uint16_t L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m3359578666_AdjustorThunk (Il2CppObject * __this, uint16_t ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t2093487825 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2093487825 *>(__this + 1);
	KeyValuePair_2_set_Value_m3359578666(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>::ToString()
extern Il2CppClass* StringU5BU5D_t4054002952_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral91;
extern Il2CppCodeGenString* _stringLiteral1396;
extern Il2CppCodeGenString* _stringLiteral93;
extern const uint32_t KeyValuePair_2_ToString_m665911326_MetadataUsageId;
extern "C"  String_t* KeyValuePair_2_ToString_m665911326_gshared (KeyValuePair_2_t2093487825 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m665911326_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	uint16_t V_1 = 0;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t4054002952* G_B2_1 = NULL;
	StringU5BU5D_t4054002952* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t4054002952* G_B1_1 = NULL;
	StringU5BU5D_t4054002952* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t4054002952* G_B3_2 = NULL;
	StringU5BU5D_t4054002952* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t4054002952* G_B5_1 = NULL;
	StringU5BU5D_t4054002952* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t4054002952* G_B4_1 = NULL;
	StringU5BU5D_t4054002952* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t4054002952* G_B6_2 = NULL;
	StringU5BU5D_t4054002952* G_B6_3 = NULL;
	{
		StringU5BU5D_t4054002952* L_0 = (StringU5BU5D_t4054002952*)((StringU5BU5D_t4054002952*)SZArrayNew(StringU5BU5D_t4054002952_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral91);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral91);
		StringU5BU5D_t4054002952* L_1 = (StringU5BU5D_t4054002952*)L_0;
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t2093487825 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
		if (!L_2)
		{
			G_B2_0 = 1;
			G_B2_1 = L_1;
			G_B2_2 = L_1;
			goto IL_0039;
		}
	}
	{
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t2093487825 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (Il2CppObject *)L_3;
		NullCheck((Il2CppObject *)(*(&V_0)));
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_0)));
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t4054002952* L_6 = (StringU5BU5D_t4054002952*)G_B3_3;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 2);
		ArrayElementTypeCheck (L_6, _stringLiteral1396);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral1396);
		StringU5BU5D_t4054002952* L_7 = (StringU5BU5D_t4054002952*)L_6;
		uint16_t L_8 = ((  uint16_t (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t2093487825 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
	}
	{
		uint16_t L_9 = ((  uint16_t (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t2093487825 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (uint16_t)L_9;
		String_t* L_10 = UInt16_ToString_m741885559((uint16_t*)(&V_1), /*hidden argument*/NULL);
		G_B6_0 = L_10;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_11;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t4054002952* L_12 = (StringU5BU5D_t4054002952*)G_B6_3;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 4);
		ArrayElementTypeCheck (L_12, _stringLiteral93);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral93);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m21867311(NULL /*static, unused*/, (StringU5BU5D_t4054002952*)L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m665911326_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t2093487825 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2093487825 *>(__this + 1);
	return KeyValuePair_2_ToString_m665911326(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m2418427527_gshared (KeyValuePair_2_t1919813716 * __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___key0;
		((  void (*) (Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t1919813716 *)__this), (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = ___value1;
		((  void (*) (Il2CppObject *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t1919813716 *)__this), (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m2418427527_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t1919813716 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1919813716 *>(__this + 1);
	KeyValuePair_2__ctor_m2418427527(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>::get_Key()
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m1474304257_gshared (KeyValuePair_2_t1919813716 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_key_0();
		return L_0;
	}
}
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m1474304257_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1919813716 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1919813716 *>(__this + 1);
	return KeyValuePair_2_get_Key_m1474304257(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m979032898_gshared (KeyValuePair_2_t1919813716 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m979032898_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t1919813716 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1919813716 *>(__this + 1);
	KeyValuePair_2_set_Key_m979032898(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>::get_Value()
extern "C"  int32_t KeyValuePair_2_get_Value_m2789648485_gshared (KeyValuePair_2_t1919813716 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_value_1();
		return L_0;
	}
}
extern "C"  int32_t KeyValuePair_2_get_Value_m2789648485_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1919813716 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1919813716 *>(__this + 1);
	return KeyValuePair_2_get_Value_m2789648485(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m2205335106_gshared (KeyValuePair_2_t1919813716 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m2205335106_AdjustorThunk (Il2CppObject * __this, int32_t ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t1919813716 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1919813716 *>(__this + 1);
	KeyValuePair_2_set_Value_m2205335106(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>::ToString()
extern Il2CppClass* StringU5BU5D_t4054002952_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral91;
extern Il2CppCodeGenString* _stringLiteral1396;
extern Il2CppCodeGenString* _stringLiteral93;
extern const uint32_t KeyValuePair_2_ToString_m3626653574_MetadataUsageId;
extern "C"  String_t* KeyValuePair_2_ToString_m3626653574_gshared (KeyValuePair_2_t1919813716 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m3626653574_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t4054002952* G_B2_1 = NULL;
	StringU5BU5D_t4054002952* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t4054002952* G_B1_1 = NULL;
	StringU5BU5D_t4054002952* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t4054002952* G_B3_2 = NULL;
	StringU5BU5D_t4054002952* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t4054002952* G_B5_1 = NULL;
	StringU5BU5D_t4054002952* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t4054002952* G_B4_1 = NULL;
	StringU5BU5D_t4054002952* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t4054002952* G_B6_2 = NULL;
	StringU5BU5D_t4054002952* G_B6_3 = NULL;
	{
		StringU5BU5D_t4054002952* L_0 = (StringU5BU5D_t4054002952*)((StringU5BU5D_t4054002952*)SZArrayNew(StringU5BU5D_t4054002952_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral91);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral91);
		StringU5BU5D_t4054002952* L_1 = (StringU5BU5D_t4054002952*)L_0;
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t1919813716 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
		if (!L_2)
		{
			G_B2_0 = 1;
			G_B2_1 = L_1;
			G_B2_2 = L_1;
			goto IL_0039;
		}
	}
	{
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t1919813716 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (Il2CppObject *)L_3;
		NullCheck((Il2CppObject *)(*(&V_0)));
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_0)));
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t4054002952* L_6 = (StringU5BU5D_t4054002952*)G_B3_3;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 2);
		ArrayElementTypeCheck (L_6, _stringLiteral1396);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral1396);
		StringU5BU5D_t4054002952* L_7 = (StringU5BU5D_t4054002952*)L_6;
		int32_t L_8 = ((  int32_t (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t1919813716 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
	}
	{
		int32_t L_9 = ((  int32_t (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t1919813716 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (int32_t)L_9;
		Il2CppObject * L_10 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (&V_1));
		NullCheck((Il2CppObject *)L_10);
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)L_10);
		G_B6_0 = L_11;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_12;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t4054002952* L_13 = (StringU5BU5D_t4054002952*)G_B6_3;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 4);
		ArrayElementTypeCheck (L_13, _stringLiteral93);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral93);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m21867311(NULL /*static, unused*/, (StringU5BU5D_t4054002952*)L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m3626653574_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1919813716 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1919813716 *>(__this + 1);
	return KeyValuePair_2_ToString_m3626653574(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.UInt16,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m2617982879_gshared (KeyValuePair_2_t1637661969 * __this, uint16_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		uint16_t L_0 = ___key0;
		((  void (*) (Il2CppObject *, uint16_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t1637661969 *)__this), (uint16_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject * L_1 = ___value1;
		((  void (*) (Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t1637661969 *)__this), (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m2617982879_AdjustorThunk (Il2CppObject * __this, uint16_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t1637661969 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1637661969 *>(__this + 1);
	KeyValuePair_2__ctor_m2617982879(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.UInt16,System.Object>::get_Key()
extern "C"  uint16_t KeyValuePair_2_get_Key_m1818977513_gshared (KeyValuePair_2_t1637661969 * __this, const MethodInfo* method)
{
	{
		uint16_t L_0 = (uint16_t)__this->get_key_0();
		return L_0;
	}
}
extern "C"  uint16_t KeyValuePair_2_get_Key_m1818977513_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1637661969 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1637661969 *>(__this + 1);
	return KeyValuePair_2_get_Key_m1818977513(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.UInt16,System.Object>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m2765394730_gshared (KeyValuePair_2_t1637661969 * __this, uint16_t ___value0, const MethodInfo* method)
{
	{
		uint16_t L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m2765394730_AdjustorThunk (Il2CppObject * __this, uint16_t ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t1637661969 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1637661969 *>(__this + 1);
	KeyValuePair_2_set_Key_m2765394730(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.UInt16,System.Object>::get_Value()
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m1147557709_gshared (KeyValuePair_2_t1637661969 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_value_1();
		return L_0;
	}
}
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m1147557709_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1637661969 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1637661969 *>(__this + 1);
	return KeyValuePair_2_get_Value_m1147557709(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.UInt16,System.Object>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m687751722_gshared (KeyValuePair_2_t1637661969 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m687751722_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t1637661969 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1637661969 *>(__this + 1);
	KeyValuePair_2_set_Value_m687751722(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.UInt16,System.Object>::ToString()
extern Il2CppClass* StringU5BU5D_t4054002952_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral91;
extern Il2CppCodeGenString* _stringLiteral1396;
extern Il2CppCodeGenString* _stringLiteral93;
extern const uint32_t KeyValuePair_2_ToString_m3996047390_MetadataUsageId;
extern "C"  String_t* KeyValuePair_2_ToString_m3996047390_gshared (KeyValuePair_2_t1637661969 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m3996047390_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint16_t V_0 = 0;
	Il2CppObject * V_1 = NULL;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t4054002952* G_B2_1 = NULL;
	StringU5BU5D_t4054002952* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t4054002952* G_B1_1 = NULL;
	StringU5BU5D_t4054002952* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t4054002952* G_B3_2 = NULL;
	StringU5BU5D_t4054002952* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t4054002952* G_B5_1 = NULL;
	StringU5BU5D_t4054002952* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t4054002952* G_B4_1 = NULL;
	StringU5BU5D_t4054002952* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t4054002952* G_B6_2 = NULL;
	StringU5BU5D_t4054002952* G_B6_3 = NULL;
	{
		StringU5BU5D_t4054002952* L_0 = (StringU5BU5D_t4054002952*)((StringU5BU5D_t4054002952*)SZArrayNew(StringU5BU5D_t4054002952_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral91);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral91);
		StringU5BU5D_t4054002952* L_1 = (StringU5BU5D_t4054002952*)L_0;
		uint16_t L_2 = ((  uint16_t (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t1637661969 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
	}
	{
		uint16_t L_3 = ((  uint16_t (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t1637661969 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (uint16_t)L_3;
		String_t* L_4 = UInt16_ToString_m741885559((uint16_t*)(&V_0), /*hidden argument*/NULL);
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t4054002952* L_6 = (StringU5BU5D_t4054002952*)G_B3_3;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 2);
		ArrayElementTypeCheck (L_6, _stringLiteral1396);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral1396);
		StringU5BU5D_t4054002952* L_7 = (StringU5BU5D_t4054002952*)L_6;
		Il2CppObject * L_8 = ((  Il2CppObject * (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t1637661969 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
		if (!L_8)
		{
			G_B5_0 = 3;
			G_B5_1 = L_7;
			G_B5_2 = L_7;
			goto IL_0072;
		}
	}
	{
		Il2CppObject * L_9 = ((  Il2CppObject * (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t1637661969 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (Il2CppObject *)L_9;
		NullCheck((Il2CppObject *)(*(&V_1)));
		String_t* L_10 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_1)));
		G_B6_0 = L_10;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_11;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t4054002952* L_12 = (StringU5BU5D_t4054002952*)G_B6_3;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 4);
		ArrayElementTypeCheck (L_12, _stringLiteral93);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral93);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m21867311(NULL /*static, unused*/, (StringU5BU5D_t4054002952*)L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m3996047390_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1637661969 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1637661969 *>(__this + 1);
	return KeyValuePair_2_ToString_m3996047390(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.UInt32,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m4066247973_gshared (KeyValuePair_2_t4066747055 * __this, uint32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		uint32_t L_0 = ___key0;
		((  void (*) (Il2CppObject *, uint32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t4066747055 *)__this), (uint32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject * L_1 = ___value1;
		((  void (*) (Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t4066747055 *)__this), (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m4066247973_AdjustorThunk (Il2CppObject * __this, uint32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t4066747055 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t4066747055 *>(__this + 1);
	KeyValuePair_2__ctor_m4066247973(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.UInt32,System.Object>::get_Key()
extern "C"  uint32_t KeyValuePair_2_get_Key_m1758015651_gshared (KeyValuePair_2_t4066747055 * __this, const MethodInfo* method)
{
	{
		uint32_t L_0 = (uint32_t)__this->get_key_0();
		return L_0;
	}
}
extern "C"  uint32_t KeyValuePair_2_get_Key_m1758015651_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t4066747055 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t4066747055 *>(__this + 1);
	return KeyValuePair_2_get_Key_m1758015651(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.UInt32,System.Object>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m1636954596_gshared (KeyValuePair_2_t4066747055 * __this, uint32_t ___value0, const MethodInfo* method)
{
	{
		uint32_t L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m1636954596_AdjustorThunk (Il2CppObject * __this, uint32_t ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t4066747055 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t4066747055 *>(__this + 1);
	KeyValuePair_2_set_Key_m1636954596(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.UInt32,System.Object>::get_Value()
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m2692750471_gshared (KeyValuePair_2_t4066747055 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_value_1();
		return L_0;
	}
}
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m2692750471_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t4066747055 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t4066747055 *>(__this + 1);
	return KeyValuePair_2_get_Value_m2692750471(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.UInt32,System.Object>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m1981395940_gshared (KeyValuePair_2_t4066747055 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m1981395940_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t4066747055 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t4066747055 *>(__this + 1);
	KeyValuePair_2_set_Value_m1981395940(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.UInt32,System.Object>::ToString()
extern Il2CppClass* StringU5BU5D_t4054002952_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral91;
extern Il2CppCodeGenString* _stringLiteral1396;
extern Il2CppCodeGenString* _stringLiteral93;
extern const uint32_t KeyValuePair_2_ToString_m2106229668_MetadataUsageId;
extern "C"  String_t* KeyValuePair_2_ToString_m2106229668_gshared (KeyValuePair_2_t4066747055 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m2106229668_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	Il2CppObject * V_1 = NULL;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t4054002952* G_B2_1 = NULL;
	StringU5BU5D_t4054002952* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t4054002952* G_B1_1 = NULL;
	StringU5BU5D_t4054002952* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t4054002952* G_B3_2 = NULL;
	StringU5BU5D_t4054002952* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t4054002952* G_B5_1 = NULL;
	StringU5BU5D_t4054002952* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t4054002952* G_B4_1 = NULL;
	StringU5BU5D_t4054002952* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t4054002952* G_B6_2 = NULL;
	StringU5BU5D_t4054002952* G_B6_3 = NULL;
	{
		StringU5BU5D_t4054002952* L_0 = (StringU5BU5D_t4054002952*)((StringU5BU5D_t4054002952*)SZArrayNew(StringU5BU5D_t4054002952_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral91);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral91);
		StringU5BU5D_t4054002952* L_1 = (StringU5BU5D_t4054002952*)L_0;
		uint32_t L_2 = ((  uint32_t (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t4066747055 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
	}
	{
		uint32_t L_3 = ((  uint32_t (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t4066747055 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (uint32_t)L_3;
		String_t* L_4 = UInt32_ToString_m904380337((uint32_t*)(&V_0), /*hidden argument*/NULL);
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t4054002952* L_6 = (StringU5BU5D_t4054002952*)G_B3_3;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 2);
		ArrayElementTypeCheck (L_6, _stringLiteral1396);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral1396);
		StringU5BU5D_t4054002952* L_7 = (StringU5BU5D_t4054002952*)L_6;
		Il2CppObject * L_8 = ((  Il2CppObject * (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t4066747055 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
		if (!L_8)
		{
			G_B5_0 = 3;
			G_B5_1 = L_7;
			G_B5_2 = L_7;
			goto IL_0072;
		}
	}
	{
		Il2CppObject * L_9 = ((  Il2CppObject * (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t4066747055 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (Il2CppObject *)L_9;
		NullCheck((Il2CppObject *)(*(&V_1)));
		String_t* L_10 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_1)));
		G_B6_0 = L_10;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_11;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t4054002952* L_12 = (StringU5BU5D_t4054002952*)G_B6_3;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 4);
		ArrayElementTypeCheck (L_12, _stringLiteral93);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral93);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m21867311(NULL /*static, unused*/, (StringU5BU5D_t4054002952*)L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m2106229668_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t4066747055 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t4066747055 *>(__this + 1);
	return KeyValuePair_2_ToString_m2106229668(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m733106853_gshared (KeyValuePair_2_t4287931429 * __this, uint64_t ___key0, int32_t ___value1, const MethodInfo* method)
{
	{
		uint64_t L_0 = ___key0;
		((  void (*) (Il2CppObject *, uint64_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t4287931429 *)__this), (uint64_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = ___value1;
		((  void (*) (Il2CppObject *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t4287931429 *)__this), (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m733106853_AdjustorThunk (Il2CppObject * __this, uint64_t ___key0, int32_t ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t4287931429 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t4287931429 *>(__this + 1);
	KeyValuePair_2__ctor_m733106853(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>::get_Key()
extern "C"  uint64_t KeyValuePair_2_get_Key_m2024648546_gshared (KeyValuePair_2_t4287931429 * __this, const MethodInfo* method)
{
	{
		uint64_t L_0 = (uint64_t)__this->get_key_0();
		return L_0;
	}
}
extern "C"  uint64_t KeyValuePair_2_get_Key_m2024648546_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t4287931429 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t4287931429 *>(__this + 1);
	return KeyValuePair_2_get_Key_m2024648546(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m3521839806_gshared (KeyValuePair_2_t4287931429 * __this, uint64_t ___value0, const MethodInfo* method)
{
	{
		uint64_t L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m3521839806_AdjustorThunk (Il2CppObject * __this, uint64_t ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t4287931429 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t4287931429 *>(__this + 1);
	KeyValuePair_2_set_Key_m3521839806(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>::get_Value()
extern "C"  int32_t KeyValuePair_2_get_Value_m2323422163_gshared (KeyValuePair_2_t4287931429 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_value_1();
		return L_0;
	}
}
extern "C"  int32_t KeyValuePair_2_get_Value_m2323422163_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t4287931429 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t4287931429 *>(__this + 1);
	return KeyValuePair_2_get_Value_m2323422163(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m4285188030_gshared (KeyValuePair_2_t4287931429 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m4285188030_AdjustorThunk (Il2CppObject * __this, int32_t ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t4287931429 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t4287931429 *>(__this + 1);
	KeyValuePair_2_set_Value_m4285188030(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>::ToString()
extern Il2CppClass* StringU5BU5D_t4054002952_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral91;
extern Il2CppCodeGenString* _stringLiteral1396;
extern Il2CppCodeGenString* _stringLiteral93;
extern const uint32_t KeyValuePair_2_ToString_m1712840292_MetadataUsageId;
extern "C"  String_t* KeyValuePair_2_ToString_m1712840292_gshared (KeyValuePair_2_t4287931429 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m1712840292_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint64_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t4054002952* G_B2_1 = NULL;
	StringU5BU5D_t4054002952* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t4054002952* G_B1_1 = NULL;
	StringU5BU5D_t4054002952* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t4054002952* G_B3_2 = NULL;
	StringU5BU5D_t4054002952* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t4054002952* G_B5_1 = NULL;
	StringU5BU5D_t4054002952* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t4054002952* G_B4_1 = NULL;
	StringU5BU5D_t4054002952* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t4054002952* G_B6_2 = NULL;
	StringU5BU5D_t4054002952* G_B6_3 = NULL;
	{
		StringU5BU5D_t4054002952* L_0 = (StringU5BU5D_t4054002952*)((StringU5BU5D_t4054002952*)SZArrayNew(StringU5BU5D_t4054002952_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral91);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral91);
		StringU5BU5D_t4054002952* L_1 = (StringU5BU5D_t4054002952*)L_0;
		uint64_t L_2 = ((  uint64_t (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t4287931429 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
	}
	{
		uint64_t L_3 = ((  uint64_t (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t4287931429 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (uint64_t)L_3;
		String_t* L_4 = UInt64_ToString_m3095865744((uint64_t*)(&V_0), /*hidden argument*/NULL);
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t4054002952* L_6 = (StringU5BU5D_t4054002952*)G_B3_3;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 2);
		ArrayElementTypeCheck (L_6, _stringLiteral1396);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral1396);
		StringU5BU5D_t4054002952* L_7 = (StringU5BU5D_t4054002952*)L_6;
		int32_t L_8 = ((  int32_t (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t4287931429 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
	}
	{
		int32_t L_9 = ((  int32_t (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t4287931429 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (int32_t)L_9;
		String_t* L_10 = Int32_ToString_m1286526384((int32_t*)(&V_1), /*hidden argument*/NULL);
		G_B6_0 = L_10;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_11;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t4054002952* L_12 = (StringU5BU5D_t4054002952*)G_B6_3;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 4);
		ArrayElementTypeCheck (L_12, _stringLiteral93);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral93);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m21867311(NULL /*static, unused*/, (StringU5BU5D_t4054002952*)L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m1712840292_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t4287931429 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t4287931429 *>(__this + 1);
	return KeyValuePair_2_ToString_m1712840292(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<UIModelDisplayType,System.Int32>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m2384040859_gshared (KeyValuePair_2_t2851311006 * __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___key0;
		((  void (*) (Il2CppObject *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t2851311006 *)__this), (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = ___value1;
		((  void (*) (Il2CppObject *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t2851311006 *)__this), (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m2384040859_AdjustorThunk (Il2CppObject * __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t2851311006 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2851311006 *>(__this + 1);
	KeyValuePair_2__ctor_m2384040859(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<UIModelDisplayType,System.Int32>::get_Key()
extern "C"  int32_t KeyValuePair_2_get_Key_m100238701_gshared (KeyValuePair_2_t2851311006 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_key_0();
		return L_0;
	}
}
extern "C"  int32_t KeyValuePair_2_get_Key_m100238701_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t2851311006 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2851311006 *>(__this + 1);
	return KeyValuePair_2_get_Key_m100238701(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<UIModelDisplayType,System.Int32>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m3521687982_gshared (KeyValuePair_2_t2851311006 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m3521687982_AdjustorThunk (Il2CppObject * __this, int32_t ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t2851311006 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2851311006 *>(__this + 1);
	KeyValuePair_2_set_Key_m3521687982(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<UIModelDisplayType,System.Int32>::get_Value()
extern "C"  int32_t KeyValuePair_2_get_Value_m3733392849_gshared (KeyValuePair_2_t2851311006 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_value_1();
		return L_0;
	}
}
extern "C"  int32_t KeyValuePair_2_get_Value_m3733392849_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t2851311006 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2851311006 *>(__this + 1);
	return KeyValuePair_2_get_Value_m3733392849(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<UIModelDisplayType,System.Int32>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m1511489198_gshared (KeyValuePair_2_t2851311006 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m1511489198_AdjustorThunk (Il2CppObject * __this, int32_t ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t2851311006 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2851311006 *>(__this + 1);
	KeyValuePair_2_set_Value_m1511489198(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<UIModelDisplayType,System.Int32>::ToString()
extern Il2CppClass* StringU5BU5D_t4054002952_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral91;
extern Il2CppCodeGenString* _stringLiteral1396;
extern Il2CppCodeGenString* _stringLiteral93;
extern const uint32_t KeyValuePair_2_ToString_m4139490970_MetadataUsageId;
extern "C"  String_t* KeyValuePair_2_ToString_m4139490970_gshared (KeyValuePair_2_t2851311006 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m4139490970_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t4054002952* G_B2_1 = NULL;
	StringU5BU5D_t4054002952* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t4054002952* G_B1_1 = NULL;
	StringU5BU5D_t4054002952* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t4054002952* G_B3_2 = NULL;
	StringU5BU5D_t4054002952* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t4054002952* G_B5_1 = NULL;
	StringU5BU5D_t4054002952* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t4054002952* G_B4_1 = NULL;
	StringU5BU5D_t4054002952* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t4054002952* G_B6_2 = NULL;
	StringU5BU5D_t4054002952* G_B6_3 = NULL;
	{
		StringU5BU5D_t4054002952* L_0 = (StringU5BU5D_t4054002952*)((StringU5BU5D_t4054002952*)SZArrayNew(StringU5BU5D_t4054002952_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral91);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral91);
		StringU5BU5D_t4054002952* L_1 = (StringU5BU5D_t4054002952*)L_0;
		int32_t L_2 = ((  int32_t (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t2851311006 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
	}
	{
		int32_t L_3 = ((  int32_t (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t2851311006 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (int32_t)L_3;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (&V_0));
		NullCheck((Il2CppObject *)L_4);
		String_t* L_5 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)L_4);
		G_B3_0 = L_5;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_6;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t4054002952* L_7 = (StringU5BU5D_t4054002952*)G_B3_3;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 2);
		ArrayElementTypeCheck (L_7, _stringLiteral1396);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral1396);
		StringU5BU5D_t4054002952* L_8 = (StringU5BU5D_t4054002952*)L_7;
		int32_t L_9 = ((  int32_t (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t2851311006 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_8;
		G_B4_2 = L_8;
	}
	{
		int32_t L_10 = ((  int32_t (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t2851311006 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (int32_t)L_10;
		String_t* L_11 = Int32_ToString_m1286526384((int32_t*)(&V_1), /*hidden argument*/NULL);
		G_B6_0 = L_11;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_12;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t4054002952* L_13 = (StringU5BU5D_t4054002952*)G_B6_3;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 4);
		ArrayElementTypeCheck (L_13, _stringLiteral93);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral93);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m21867311(NULL /*static, unused*/, (StringU5BU5D_t4054002952*)L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m4139490970_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t2851311006 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2851311006 *>(__this + 1);
	return KeyValuePair_2_ToString_m4139490970(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<UIModelDisplayType,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m2016324438_gshared (KeyValuePair_2_t1573321581 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___key0;
		((  void (*) (Il2CppObject *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t1573321581 *)__this), (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject * L_1 = ___value1;
		((  void (*) (Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t1573321581 *)__this), (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m2016324438_AdjustorThunk (Il2CppObject * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t1573321581 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1573321581 *>(__this + 1);
	KeyValuePair_2__ctor_m2016324438(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<UIModelDisplayType,System.Object>::get_Key()
extern "C"  int32_t KeyValuePair_2_get_Key_m2360182738_gshared (KeyValuePair_2_t1573321581 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_key_0();
		return L_0;
	}
}
extern "C"  int32_t KeyValuePair_2_get_Key_m2360182738_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1573321581 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1573321581 *>(__this + 1);
	return KeyValuePair_2_get_Key_m2360182738(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<UIModelDisplayType,System.Object>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m598503699_gshared (KeyValuePair_2_t1573321581 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m598503699_AdjustorThunk (Il2CppObject * __this, int32_t ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t1573321581 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1573321581 *>(__this + 1);
	KeyValuePair_2_set_Key_m598503699(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<UIModelDisplayType,System.Object>::get_Value()
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m1861716754_gshared (KeyValuePair_2_t1573321581 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_value_1();
		return L_0;
	}
}
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m1861716754_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1573321581 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1573321581 *>(__this + 1);
	return KeyValuePair_2_get_Value_m1861716754(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<UIModelDisplayType,System.Object>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m2608006035_gshared (KeyValuePair_2_t1573321581 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m2608006035_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t1573321581 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1573321581 *>(__this + 1);
	KeyValuePair_2_set_Value_m2608006035(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<UIModelDisplayType,System.Object>::ToString()
extern Il2CppClass* StringU5BU5D_t4054002952_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral91;
extern Il2CppCodeGenString* _stringLiteral1396;
extern Il2CppCodeGenString* _stringLiteral93;
extern const uint32_t KeyValuePair_2_ToString_m1592821359_MetadataUsageId;
extern "C"  String_t* KeyValuePair_2_ToString_m1592821359_gshared (KeyValuePair_2_t1573321581 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m1592821359_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Il2CppObject * V_1 = NULL;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t4054002952* G_B2_1 = NULL;
	StringU5BU5D_t4054002952* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t4054002952* G_B1_1 = NULL;
	StringU5BU5D_t4054002952* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t4054002952* G_B3_2 = NULL;
	StringU5BU5D_t4054002952* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t4054002952* G_B5_1 = NULL;
	StringU5BU5D_t4054002952* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t4054002952* G_B4_1 = NULL;
	StringU5BU5D_t4054002952* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t4054002952* G_B6_2 = NULL;
	StringU5BU5D_t4054002952* G_B6_3 = NULL;
	{
		StringU5BU5D_t4054002952* L_0 = (StringU5BU5D_t4054002952*)((StringU5BU5D_t4054002952*)SZArrayNew(StringU5BU5D_t4054002952_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral91);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral91);
		StringU5BU5D_t4054002952* L_1 = (StringU5BU5D_t4054002952*)L_0;
		int32_t L_2 = ((  int32_t (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t1573321581 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
	}
	{
		int32_t L_3 = ((  int32_t (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t1573321581 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (int32_t)L_3;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (&V_0));
		NullCheck((Il2CppObject *)L_4);
		String_t* L_5 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)L_4);
		G_B3_0 = L_5;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_6;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t4054002952* L_7 = (StringU5BU5D_t4054002952*)G_B3_3;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 2);
		ArrayElementTypeCheck (L_7, _stringLiteral1396);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral1396);
		StringU5BU5D_t4054002952* L_8 = (StringU5BU5D_t4054002952*)L_7;
		Il2CppObject * L_9 = ((  Il2CppObject * (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t1573321581 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_8;
		G_B4_2 = L_8;
		if (!L_9)
		{
			G_B5_0 = 3;
			G_B5_1 = L_8;
			G_B5_2 = L_8;
			goto IL_0072;
		}
	}
	{
		Il2CppObject * L_10 = ((  Il2CppObject * (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t1573321581 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (Il2CppObject *)L_10;
		NullCheck((Il2CppObject *)(*(&V_1)));
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_1)));
		G_B6_0 = L_11;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_12;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t4054002952* L_13 = (StringU5BU5D_t4054002952*)G_B6_3;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 4);
		ArrayElementTypeCheck (L_13, _stringLiteral93);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral93);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m21867311(NULL /*static, unused*/, (StringU5BU5D_t4054002952*)L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m1592821359_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1573321581 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1573321581 *>(__this + 1);
	return KeyValuePair_2_ToString_m1592821359(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::.ctor(System.Collections.Generic.LinkedList`1<T>)
extern "C"  void Enumerator__ctor_m857368315_gshared (Enumerator_t3528606100 * __this, LinkedList_1_t2045451540 * ___parent0, const MethodInfo* method)
{
	{
		LinkedList_1_t2045451540 * L_0 = ___parent0;
		__this->set_list_0(L_0);
		__this->set_current_1((LinkedListNode_1_t3787551078 *)NULL);
		__this->set_index_2((-1));
		LinkedList_1_t2045451540 * L_1 = ___parent0;
		NullCheck(L_1);
		uint32_t L_2 = (uint32_t)L_1->get_version_1();
		__this->set_version_3(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m857368315_AdjustorThunk (Il2CppObject * __this, LinkedList_1_t2045451540 * ___parent0, const MethodInfo* method)
{
	Enumerator_t3528606100 * _thisAdjusted = reinterpret_cast<Enumerator_t3528606100 *>(__this + 1);
	Enumerator__ctor_m857368315(_thisAdjusted, ___parent0, method);
}
// System.Object System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1753810300_gshared (Enumerator_t3528606100 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ((  Il2CppObject * (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t3528606100 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_0;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1753810300_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3528606100 * _thisAdjusted = reinterpret_cast<Enumerator_t3528606100 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m1753810300(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::System.Collections.IEnumerator.Reset()
extern Il2CppClass* ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3204776971;
extern const uint32_t Enumerator_System_Collections_IEnumerator_Reset_m4062113552_MetadataUsageId;
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m4062113552_gshared (Enumerator_t3528606100 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_Reset_m4062113552_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		LinkedList_1_t2045451540 * L_0 = (LinkedList_1_t2045451540 *)__this->get_list_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t1794727681 * L_1 = (ObjectDisposedException_t1794727681 *)il2cpp_codegen_object_new(ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		uint32_t L_2 = (uint32_t)__this->get_version_3();
		LinkedList_1_t2045451540 * L_3 = (LinkedList_1_t2045451540 *)__this->get_list_0();
		NullCheck(L_3);
		uint32_t L_4 = (uint32_t)L_3->get_version_1();
		if ((((int32_t)L_2) == ((int32_t)L_4)))
		{
			goto IL_0033;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_5 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_5, (String_t*)_stringLiteral3204776971, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0033:
	{
		__this->set_current_1((LinkedListNode_1_t3787551078 *)NULL);
		__this->set_index_2((-1));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m4062113552_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3528606100 * _thisAdjusted = reinterpret_cast<Enumerator_t3528606100 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m4062113552(_thisAdjusted, method);
}
// T System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::get_Current()
extern Il2CppClass* ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_get_Current_m1124073047_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_get_Current_m1124073047_gshared (Enumerator_t3528606100 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_get_Current_m1124073047_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		LinkedList_1_t2045451540 * L_0 = (LinkedList_1_t2045451540 *)__this->get_list_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t1794727681 * L_1 = (ObjectDisposedException_t1794727681 *)il2cpp_codegen_object_new(ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		LinkedListNode_1_t3787551078 * L_2 = (LinkedListNode_1_t3787551078 *)__this->get_current_1();
		if (L_2)
		{
			goto IL_0023;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m355676978(L_3, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0023:
	{
		LinkedListNode_1_t3787551078 * L_4 = (LinkedListNode_1_t3787551078 *)__this->get_current_1();
		NullCheck((LinkedListNode_1_t3787551078 *)L_4);
		Il2CppObject * L_5 = ((  Il2CppObject * (*) (LinkedListNode_1_t3787551078 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((LinkedListNode_1_t3787551078 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_5;
	}
}
extern "C"  Il2CppObject * Enumerator_get_Current_m1124073047_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3528606100 * _thisAdjusted = reinterpret_cast<Enumerator_t3528606100 *>(__this + 1);
	return Enumerator_get_Current_m1124073047(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::MoveNext()
extern Il2CppClass* ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3204776971;
extern const uint32_t Enumerator_MoveNext_m2358966120_MetadataUsageId;
extern "C"  bool Enumerator_MoveNext_m2358966120_gshared (Enumerator_t3528606100 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_MoveNext_m2358966120_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		LinkedList_1_t2045451540 * L_0 = (LinkedList_1_t2045451540 *)__this->get_list_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t1794727681 * L_1 = (ObjectDisposedException_t1794727681 *)il2cpp_codegen_object_new(ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		uint32_t L_2 = (uint32_t)__this->get_version_3();
		LinkedList_1_t2045451540 * L_3 = (LinkedList_1_t2045451540 *)__this->get_list_0();
		NullCheck(L_3);
		uint32_t L_4 = (uint32_t)L_3->get_version_1();
		if ((((int32_t)L_2) == ((int32_t)L_4)))
		{
			goto IL_0033;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_5 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_5, (String_t*)_stringLiteral3204776971, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0033:
	{
		LinkedListNode_1_t3787551078 * L_6 = (LinkedListNode_1_t3787551078 *)__this->get_current_1();
		if (L_6)
		{
			goto IL_0054;
		}
	}
	{
		LinkedList_1_t2045451540 * L_7 = (LinkedList_1_t2045451540 *)__this->get_list_0();
		NullCheck(L_7);
		LinkedListNode_1_t3787551078 * L_8 = (LinkedListNode_1_t3787551078 *)L_7->get_first_3();
		__this->set_current_1(L_8);
		goto IL_0082;
	}

IL_0054:
	{
		LinkedListNode_1_t3787551078 * L_9 = (LinkedListNode_1_t3787551078 *)__this->get_current_1();
		NullCheck(L_9);
		LinkedListNode_1_t3787551078 * L_10 = (LinkedListNode_1_t3787551078 *)L_9->get_forward_2();
		__this->set_current_1(L_10);
		LinkedListNode_1_t3787551078 * L_11 = (LinkedListNode_1_t3787551078 *)__this->get_current_1();
		LinkedList_1_t2045451540 * L_12 = (LinkedList_1_t2045451540 *)__this->get_list_0();
		NullCheck(L_12);
		LinkedListNode_1_t3787551078 * L_13 = (LinkedListNode_1_t3787551078 *)L_12->get_first_3();
		if ((!(((Il2CppObject*)(LinkedListNode_1_t3787551078 *)L_11) == ((Il2CppObject*)(LinkedListNode_1_t3787551078 *)L_13))))
		{
			goto IL_0082;
		}
	}
	{
		__this->set_current_1((LinkedListNode_1_t3787551078 *)NULL);
	}

IL_0082:
	{
		LinkedListNode_1_t3787551078 * L_14 = (LinkedListNode_1_t3787551078 *)__this->get_current_1();
		if (L_14)
		{
			goto IL_0096;
		}
	}
	{
		__this->set_index_2((-1));
		return (bool)0;
	}

IL_0096:
	{
		int32_t L_15 = (int32_t)__this->get_index_2();
		__this->set_index_2(((int32_t)((int32_t)L_15+(int32_t)1)));
		return (bool)1;
	}
}
extern "C"  bool Enumerator_MoveNext_m2358966120_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3528606100 * _thisAdjusted = reinterpret_cast<Enumerator_t3528606100 *>(__this + 1);
	return Enumerator_MoveNext_m2358966120(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::Dispose()
extern Il2CppClass* ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_Dispose_m272587367_MetadataUsageId;
extern "C"  void Enumerator_Dispose_m272587367_gshared (Enumerator_t3528606100 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_Dispose_m272587367_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		LinkedList_1_t2045451540 * L_0 = (LinkedList_1_t2045451540 *)__this->get_list_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t1794727681 * L_1 = (ObjectDisposedException_t1794727681 *)il2cpp_codegen_object_new(ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		__this->set_current_1((LinkedListNode_1_t3787551078 *)NULL);
		__this->set_list_0((LinkedList_1_t2045451540 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m272587367_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3528606100 * _thisAdjusted = reinterpret_cast<Enumerator_t3528606100 *>(__this + 1);
	Enumerator_Dispose_m272587367(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::.ctor()
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t LinkedList_1__ctor_m2955457271_MetadataUsageId;
extern "C"  void LinkedList_1__ctor_m2955457271_gshared (LinkedList_1_t2045451540 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LinkedList_1__ctor_m2955457271_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		__this->set_syncRoot_2(L_0);
		__this->set_first_3((LinkedListNode_1_t3787551078 *)NULL);
		int32_t L_1 = (int32_t)0;
		V_0 = (uint32_t)L_1;
		__this->set_version_1(L_1);
		uint32_t L_2 = V_0;
		__this->set_count_0(L_2);
		return;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t LinkedList_1__ctor_m3369579448_MetadataUsageId;
extern "C"  void LinkedList_1__ctor_m3369579448_gshared (LinkedList_1_t2045451540 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LinkedList_1__ctor_m3369579448_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((LinkedList_1_t2045451540 *)__this);
		((  void (*) (LinkedList_1_t2045451540 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((LinkedList_1_t2045451540 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		SerializationInfo_t2185721892 * L_0 = ___info0;
		__this->set_si_4(L_0);
		Il2CppObject * L_1 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_1, /*hidden argument*/NULL);
		__this->set_syncRoot_2(L_1);
		return;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C"  void LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m3576108392_gshared (LinkedList_1_t2045451540 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		NullCheck((LinkedList_1_t2045451540 *)__this);
		((  LinkedListNode_1_t3787551078 * (*) (LinkedList_1_t2045451540 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((LinkedList_1_t2045451540 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern Il2CppClass* ArgumentException_t928607144_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral93090393;
extern const uint32_t LinkedList_1_System_Collections_ICollection_CopyTo_m2331638317_MetadataUsageId;
extern "C"  void LinkedList_1_System_Collections_ICollection_CopyTo_m2331638317_gshared (LinkedList_1_t2045451540 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LinkedList_1_System_Collections_ICollection_CopyTo_m2331638317_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ObjectU5BU5D_t1108656482* V_0 = NULL;
	{
		Il2CppArray * L_0 = ___array0;
		V_0 = (ObjectU5BU5D_t1108656482*)((ObjectU5BU5D_t1108656482*)IsInst(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2)));
		ObjectU5BU5D_t1108656482* L_1 = V_0;
		if (L_1)
		{
			goto IL_0018;
		}
	}
	{
		ArgumentException_t928607144 * L_2 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_2, (String_t*)_stringLiteral93090393, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0018:
	{
		ObjectU5BU5D_t1108656482* L_3 = V_0;
		int32_t L_4 = ___index1;
		NullCheck((LinkedList_1_t2045451540 *)__this);
		((  void (*) (LinkedList_1_t2045451540 *, ObjectU5BU5D_t1108656482*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((LinkedList_1_t2045451540 *)__this, (ObjectU5BU5D_t1108656482*)L_3, (int32_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* LinkedList_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2027502985_gshared (LinkedList_1_t2045451540 * __this, const MethodInfo* method)
{
	{
		NullCheck((LinkedList_1_t2045451540 *)__this);
		Enumerator_t3528606100  L_0 = ((  Enumerator_t3528606100  (*) (LinkedList_1_t2045451540 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((LinkedList_1_t2045451540 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		Enumerator_t3528606100  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), &L_1);
		return (Il2CppObject*)L_2;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * LinkedList_1_System_Collections_IEnumerable_GetEnumerator_m51916412_gshared (LinkedList_1_t2045451540 * __this, const MethodInfo* method)
{
	{
		NullCheck((LinkedList_1_t2045451540 *)__this);
		Enumerator_t3528606100  L_0 = ((  Enumerator_t3528606100  (*) (LinkedList_1_t2045451540 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((LinkedList_1_t2045451540 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		Enumerator_t3528606100  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), &L_1);
		return (Il2CppObject *)L_2;
	}
}
// System.Boolean System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2230847288_gshared (LinkedList_1_t2045451540 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Boolean System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool LinkedList_1_System_Collections_ICollection_get_IsSynchronized_m683991045_gshared (LinkedList_1_t2045451540 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * LinkedList_1_System_Collections_ICollection_get_SyncRoot_m573420165_gshared (LinkedList_1_t2045451540 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_syncRoot_2();
		return L_0;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::VerifyReferencedNode(System.Collections.Generic.LinkedListNode`1<T>)
extern Il2CppClass* ArgumentNullException_t3573189601_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3386882;
extern const uint32_t LinkedList_1_VerifyReferencedNode_m3939775124_MetadataUsageId;
extern "C"  void LinkedList_1_VerifyReferencedNode_m3939775124_gshared (LinkedList_1_t2045451540 * __this, LinkedListNode_1_t3787551078 * ___node0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LinkedList_1_VerifyReferencedNode_m3939775124_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		LinkedListNode_1_t3787551078 * L_0 = ___node0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t3573189601 * L_1 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral3386882, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		LinkedListNode_1_t3787551078 * L_2 = ___node0;
		NullCheck((LinkedListNode_1_t3787551078 *)L_2);
		LinkedList_1_t2045451540 * L_3 = ((  LinkedList_1_t2045451540 * (*) (LinkedListNode_1_t3787551078 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((LinkedListNode_1_t3787551078 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		if ((((Il2CppObject*)(LinkedList_1_t2045451540 *)L_3) == ((Il2CppObject*)(LinkedList_1_t2045451540 *)__this)))
		{
			goto IL_0023;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_4 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m355676978(L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_0023:
	{
		return;
	}
}
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<System.Object>::AddLast(T)
extern "C"  LinkedListNode_1_t3787551078 * LinkedList_1_AddLast_m4070107716_gshared (LinkedList_1_t2045451540 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	LinkedListNode_1_t3787551078 * V_0 = NULL;
	{
		LinkedListNode_1_t3787551078 * L_0 = (LinkedListNode_1_t3787551078 *)__this->get_first_3();
		if (L_0)
		{
			goto IL_001f;
		}
	}
	{
		Il2CppObject * L_1 = ___value0;
		LinkedListNode_1_t3787551078 * L_2 = (LinkedListNode_1_t3787551078 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (LinkedListNode_1_t3787551078 *, LinkedList_1_t2045451540 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_2, (LinkedList_1_t2045451540 *)__this, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		V_0 = (LinkedListNode_1_t3787551078 *)L_2;
		LinkedListNode_1_t3787551078 * L_3 = V_0;
		__this->set_first_3(L_3);
		goto IL_0038;
	}

IL_001f:
	{
		Il2CppObject * L_4 = ___value0;
		LinkedListNode_1_t3787551078 * L_5 = (LinkedListNode_1_t3787551078 *)__this->get_first_3();
		NullCheck(L_5);
		LinkedListNode_1_t3787551078 * L_6 = (LinkedListNode_1_t3787551078 *)L_5->get_back_3();
		LinkedListNode_1_t3787551078 * L_7 = (LinkedListNode_1_t3787551078 *)__this->get_first_3();
		LinkedListNode_1_t3787551078 * L_8 = (LinkedListNode_1_t3787551078 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (LinkedListNode_1_t3787551078 *, LinkedList_1_t2045451540 *, Il2CppObject *, LinkedListNode_1_t3787551078 *, LinkedListNode_1_t3787551078 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)(L_8, (LinkedList_1_t2045451540 *)__this, (Il2CppObject *)L_4, (LinkedListNode_1_t3787551078 *)L_6, (LinkedListNode_1_t3787551078 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		V_0 = (LinkedListNode_1_t3787551078 *)L_8;
	}

IL_0038:
	{
		uint32_t L_9 = (uint32_t)__this->get_count_0();
		__this->set_count_0(((int32_t)((int32_t)L_9+(int32_t)1)));
		uint32_t L_10 = (uint32_t)__this->get_version_1();
		__this->set_version_1(((int32_t)((int32_t)L_10+(int32_t)1)));
		LinkedListNode_1_t3787551078 * L_11 = V_0;
		return L_11;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::Clear()
extern "C"  void LinkedList_1_Clear_m361590562_gshared (LinkedList_1_t2045451540 * __this, const MethodInfo* method)
{
	{
		goto IL_000b;
	}

IL_0005:
	{
		NullCheck((LinkedList_1_t2045451540 *)__this);
		((  void (*) (LinkedList_1_t2045451540 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((LinkedList_1_t2045451540 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
	}

IL_000b:
	{
		LinkedListNode_1_t3787551078 * L_0 = (LinkedListNode_1_t3787551078 *)__this->get_first_3();
		if (L_0)
		{
			goto IL_0005;
		}
	}
	{
		return;
	}
}
// System.Boolean System.Collections.Generic.LinkedList`1<System.Object>::Contains(T)
extern "C"  bool LinkedList_1_Contains_m3484410556_gshared (LinkedList_1_t2045451540 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	LinkedListNode_1_t3787551078 * V_0 = NULL;
	{
		LinkedListNode_1_t3787551078 * L_0 = (LinkedListNode_1_t3787551078 *)__this->get_first_3();
		V_0 = (LinkedListNode_1_t3787551078 *)L_0;
		LinkedListNode_1_t3787551078 * L_1 = V_0;
		if (L_1)
		{
			goto IL_000f;
		}
	}
	{
		return (bool)0;
	}

IL_000f:
	{
		LinkedListNode_1_t3787551078 * L_2 = V_0;
		NullCheck((LinkedListNode_1_t3787551078 *)L_2);
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (LinkedListNode_1_t3787551078 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((LinkedListNode_1_t3787551078 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		NullCheck((Il2CppObject *)(*(&___value0)));
		bool L_4 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)(*(&___value0)), (Il2CppObject *)L_3);
		if (!L_4)
		{
			goto IL_002e;
		}
	}
	{
		return (bool)1;
	}

IL_002e:
	{
		LinkedListNode_1_t3787551078 * L_5 = V_0;
		NullCheck(L_5);
		LinkedListNode_1_t3787551078 * L_6 = (LinkedListNode_1_t3787551078 *)L_5->get_forward_2();
		V_0 = (LinkedListNode_1_t3787551078 *)L_6;
		LinkedListNode_1_t3787551078 * L_7 = V_0;
		LinkedListNode_1_t3787551078 * L_8 = (LinkedListNode_1_t3787551078 *)__this->get_first_3();
		if ((!(((Il2CppObject*)(LinkedListNode_1_t3787551078 *)L_7) == ((Il2CppObject*)(LinkedListNode_1_t3787551078 *)L_8))))
		{
			goto IL_000f;
		}
	}
	{
		return (bool)0;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::CopyTo(T[],System.Int32)
extern Il2CppClass* ArgumentNullException_t3573189601_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t928607144_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral93090393;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern Il2CppCodeGenString* _stringLiteral279574791;
extern Il2CppCodeGenString* _stringLiteral768919341;
extern const uint32_t LinkedList_1_CopyTo_m3470139544_MetadataUsageId;
extern "C"  void LinkedList_1_CopyTo_m3470139544_gshared (LinkedList_1_t2045451540 * __this, ObjectU5BU5D_t1108656482* ___array0, int32_t ___index1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LinkedList_1_CopyTo_m3470139544_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	LinkedListNode_1_t3787551078 * V_0 = NULL;
	{
		ObjectU5BU5D_t1108656482* L_0 = ___array0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t3573189601 * L_1 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral93090393, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		int32_t L_2 = ___index1;
		ObjectU5BU5D_t1108656482* L_3 = ___array0;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_3);
		int32_t L_4 = Array_GetLowerBound_m2369136542((Il2CppArray *)(Il2CppArray *)L_3, (int32_t)0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_2) < ((uint32_t)L_4))))
		{
			goto IL_0029;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_5 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_5, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0029:
	{
		ObjectU5BU5D_t1108656482* L_6 = ___array0;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_6);
		int32_t L_7 = Array_get_Rank_m1671008509((Il2CppArray *)(Il2CppArray *)L_6, /*hidden argument*/NULL);
		if ((((int32_t)L_7) == ((int32_t)1)))
		{
			goto IL_0045;
		}
	}
	{
		ArgumentException_t928607144 * L_8 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m732321503(L_8, (String_t*)_stringLiteral93090393, (String_t*)_stringLiteral279574791, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}

IL_0045:
	{
		ObjectU5BU5D_t1108656482* L_9 = ___array0;
		NullCheck(L_9);
		int32_t L_10 = ___index1;
		ObjectU5BU5D_t1108656482* L_11 = ___array0;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_11);
		int32_t L_12 = Array_GetLowerBound_m2369136542((Il2CppArray *)(Il2CppArray *)L_11, (int32_t)0, /*hidden argument*/NULL);
		uint32_t L_13 = (uint32_t)__this->get_count_0();
		if ((((int64_t)(((int64_t)((int64_t)((int32_t)((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_9)->max_length))))-(int32_t)L_10))+(int32_t)L_12)))))) >= ((int64_t)(((int64_t)((uint64_t)L_13))))))
		{
			goto IL_006a;
		}
	}
	{
		ArgumentException_t928607144 * L_14 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_14, (String_t*)_stringLiteral768919341, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_14);
	}

IL_006a:
	{
		LinkedListNode_1_t3787551078 * L_15 = (LinkedListNode_1_t3787551078 *)__this->get_first_3();
		V_0 = (LinkedListNode_1_t3787551078 *)L_15;
		LinkedListNode_1_t3787551078 * L_16 = (LinkedListNode_1_t3787551078 *)__this->get_first_3();
		if (L_16)
		{
			goto IL_007d;
		}
	}
	{
		return;
	}

IL_007d:
	{
		ObjectU5BU5D_t1108656482* L_17 = ___array0;
		int32_t L_18 = ___index1;
		LinkedListNode_1_t3787551078 * L_19 = V_0;
		NullCheck((LinkedListNode_1_t3787551078 *)L_19);
		Il2CppObject * L_20 = ((  Il2CppObject * (*) (LinkedListNode_1_t3787551078 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((LinkedListNode_1_t3787551078 *)L_19, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, L_18);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(L_18), (Il2CppObject *)L_20);
		int32_t L_21 = ___index1;
		___index1 = (int32_t)((int32_t)((int32_t)L_21+(int32_t)1));
		LinkedListNode_1_t3787551078 * L_22 = V_0;
		NullCheck(L_22);
		LinkedListNode_1_t3787551078 * L_23 = (LinkedListNode_1_t3787551078 *)L_22->get_forward_2();
		V_0 = (LinkedListNode_1_t3787551078 *)L_23;
		LinkedListNode_1_t3787551078 * L_24 = V_0;
		LinkedListNode_1_t3787551078 * L_25 = (LinkedListNode_1_t3787551078 *)__this->get_first_3();
		if ((!(((Il2CppObject*)(LinkedListNode_1_t3787551078 *)L_24) == ((Il2CppObject*)(LinkedListNode_1_t3787551078 *)L_25))))
		{
			goto IL_007d;
		}
	}
	{
		return;
	}
}
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<System.Object>::Find(T)
extern "C"  LinkedListNode_1_t3787551078 * LinkedList_1_Find_m2643247334_gshared (LinkedList_1_t2045451540 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	LinkedListNode_1_t3787551078 * V_0 = NULL;
	{
		LinkedListNode_1_t3787551078 * L_0 = (LinkedListNode_1_t3787551078 *)__this->get_first_3();
		V_0 = (LinkedListNode_1_t3787551078 *)L_0;
		LinkedListNode_1_t3787551078 * L_1 = V_0;
		if (L_1)
		{
			goto IL_000f;
		}
	}
	{
		return (LinkedListNode_1_t3787551078 *)NULL;
	}

IL_000f:
	{
		Il2CppObject * L_2 = ___value0;
		if (L_2)
		{
			goto IL_002a;
		}
	}
	{
		LinkedListNode_1_t3787551078 * L_3 = V_0;
		NullCheck((LinkedListNode_1_t3787551078 *)L_3);
		Il2CppObject * L_4 = ((  Il2CppObject * (*) (LinkedListNode_1_t3787551078 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((LinkedListNode_1_t3787551078 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		if (!L_4)
		{
			goto IL_0052;
		}
	}

IL_002a:
	{
		Il2CppObject * L_5 = ___value0;
		if (!L_5)
		{
			goto IL_0054;
		}
	}
	{
		LinkedListNode_1_t3787551078 * L_6 = V_0;
		NullCheck((LinkedListNode_1_t3787551078 *)L_6);
		Il2CppObject * L_7 = ((  Il2CppObject * (*) (LinkedListNode_1_t3787551078 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((LinkedListNode_1_t3787551078 *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		NullCheck((Il2CppObject *)(*(&___value0)));
		bool L_8 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)(*(&___value0)), (Il2CppObject *)L_7);
		if (!L_8)
		{
			goto IL_0054;
		}
	}

IL_0052:
	{
		LinkedListNode_1_t3787551078 * L_9 = V_0;
		return L_9;
	}

IL_0054:
	{
		LinkedListNode_1_t3787551078 * L_10 = V_0;
		NullCheck(L_10);
		LinkedListNode_1_t3787551078 * L_11 = (LinkedListNode_1_t3787551078 *)L_10->get_forward_2();
		V_0 = (LinkedListNode_1_t3787551078 *)L_11;
		LinkedListNode_1_t3787551078 * L_12 = V_0;
		LinkedListNode_1_t3787551078 * L_13 = (LinkedListNode_1_t3787551078 *)__this->get_first_3();
		if ((!(((Il2CppObject*)(LinkedListNode_1_t3787551078 *)L_12) == ((Il2CppObject*)(LinkedListNode_1_t3787551078 *)L_13))))
		{
			goto IL_000f;
		}
	}
	{
		return (LinkedListNode_1_t3787551078 *)NULL;
	}
}
// System.Collections.Generic.LinkedList`1/Enumerator<T> System.Collections.Generic.LinkedList`1<System.Object>::GetEnumerator()
extern "C"  Enumerator_t3528606100  LinkedList_1_GetEnumerator_m3713737734_gshared (LinkedList_1_t2045451540 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3528606100  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Enumerator__ctor_m857368315(&L_0, (LinkedList_1_t2045451540 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13));
		return L_0;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1612836015;
extern Il2CppCodeGenString* _stringLiteral351608024;
extern const uint32_t LinkedList_1_GetObjectData_m3974480661_MetadataUsageId;
extern "C"  void LinkedList_1_GetObjectData_m3974480661_gshared (LinkedList_1_t2045451540 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LinkedList_1_GetObjectData_m3974480661_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ObjectU5BU5D_t1108656482* V_0 = NULL;
	{
		uint32_t L_0 = (uint32_t)__this->get_count_0();
		V_0 = (ObjectU5BU5D_t1108656482*)((ObjectU5BU5D_t1108656482*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 14), (uint32_t)(((uintptr_t)L_0))));
		ObjectU5BU5D_t1108656482* L_1 = V_0;
		NullCheck((LinkedList_1_t2045451540 *)__this);
		((  void (*) (LinkedList_1_t2045451540 *, ObjectU5BU5D_t1108656482*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((LinkedList_1_t2045451540 *)__this, (ObjectU5BU5D_t1108656482*)L_1, (int32_t)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		SerializationInfo_t2185721892 * L_2 = ___info0;
		ObjectU5BU5D_t1108656482* L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_4 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 15)), /*hidden argument*/NULL);
		NullCheck((SerializationInfo_t2185721892 *)L_2);
		SerializationInfo_AddValue_m3341936982((SerializationInfo_t2185721892 *)L_2, (String_t*)_stringLiteral1612836015, (Il2CppObject *)(Il2CppObject *)L_3, (Type_t *)L_4, /*hidden argument*/NULL);
		SerializationInfo_t2185721892 * L_5 = ___info0;
		uint32_t L_6 = (uint32_t)__this->get_version_1();
		NullCheck((SerializationInfo_t2185721892 *)L_5);
		SerializationInfo_AddValue_m787539465((SerializationInfo_t2185721892 *)L_5, (String_t*)_stringLiteral351608024, (uint32_t)L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::OnDeserialization(System.Object)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1612836015;
extern Il2CppCodeGenString* _stringLiteral351608024;
extern const uint32_t LinkedList_1_OnDeserialization_m3445006959_MetadataUsageId;
extern "C"  void LinkedList_1_OnDeserialization_m3445006959_gshared (LinkedList_1_t2045451540 * __this, Il2CppObject * ___sender0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LinkedList_1_OnDeserialization_m3445006959_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ObjectU5BU5D_t1108656482* V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	ObjectU5BU5D_t1108656482* V_2 = NULL;
	int32_t V_3 = 0;
	{
		SerializationInfo_t2185721892 * L_0 = (SerializationInfo_t2185721892 *)__this->get_si_4();
		if (!L_0)
		{
			goto IL_0074;
		}
	}
	{
		SerializationInfo_t2185721892 * L_1 = (SerializationInfo_t2185721892 *)__this->get_si_4();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 15)), /*hidden argument*/NULL);
		NullCheck((SerializationInfo_t2185721892 *)L_1);
		Il2CppObject * L_3 = SerializationInfo_GetValue_m4125471336((SerializationInfo_t2185721892 *)L_1, (String_t*)_stringLiteral1612836015, (Type_t *)L_2, /*hidden argument*/NULL);
		V_0 = (ObjectU5BU5D_t1108656482*)((ObjectU5BU5D_t1108656482*)Castclass(L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2)));
		ObjectU5BU5D_t1108656482* L_4 = V_0;
		if (!L_4)
		{
			goto IL_0057;
		}
	}
	{
		ObjectU5BU5D_t1108656482* L_5 = V_0;
		V_2 = (ObjectU5BU5D_t1108656482*)L_5;
		V_3 = (int32_t)0;
		goto IL_004e;
	}

IL_003a:
	{
		ObjectU5BU5D_t1108656482* L_6 = V_2;
		int32_t L_7 = V_3;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		int32_t L_8 = L_7;
		Il2CppObject * L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		V_1 = (Il2CppObject *)L_9;
		Il2CppObject * L_10 = V_1;
		NullCheck((LinkedList_1_t2045451540 *)__this);
		((  LinkedListNode_1_t3787551078 * (*) (LinkedList_1_t2045451540 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((LinkedList_1_t2045451540 *)__this, (Il2CppObject *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		int32_t L_11 = V_3;
		V_3 = (int32_t)((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_004e:
	{
		int32_t L_12 = V_3;
		ObjectU5BU5D_t1108656482* L_13 = V_2;
		NullCheck(L_13);
		if ((((int32_t)L_12) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_13)->max_length)))))))
		{
			goto IL_003a;
		}
	}

IL_0057:
	{
		SerializationInfo_t2185721892 * L_14 = (SerializationInfo_t2185721892 *)__this->get_si_4();
		NullCheck((SerializationInfo_t2185721892 *)L_14);
		uint32_t L_15 = SerializationInfo_GetUInt32_m1908270281((SerializationInfo_t2185721892 *)L_14, (String_t*)_stringLiteral351608024, /*hidden argument*/NULL);
		__this->set_version_1(L_15);
		__this->set_si_4((SerializationInfo_t2185721892 *)NULL);
	}

IL_0074:
	{
		return;
	}
}
// System.Boolean System.Collections.Generic.LinkedList`1<System.Object>::Remove(T)
extern "C"  bool LinkedList_1_Remove_m3283493303_gshared (LinkedList_1_t2045451540 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	LinkedListNode_1_t3787551078 * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___value0;
		NullCheck((LinkedList_1_t2045451540 *)__this);
		LinkedListNode_1_t3787551078 * L_1 = ((  LinkedListNode_1_t3787551078 * (*) (LinkedList_1_t2045451540 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16)->methodPointer)((LinkedList_1_t2045451540 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16));
		V_0 = (LinkedListNode_1_t3787551078 *)L_1;
		LinkedListNode_1_t3787551078 * L_2 = V_0;
		if (L_2)
		{
			goto IL_0010;
		}
	}
	{
		return (bool)0;
	}

IL_0010:
	{
		LinkedListNode_1_t3787551078 * L_3 = V_0;
		NullCheck((LinkedList_1_t2045451540 *)__this);
		((  void (*) (LinkedList_1_t2045451540 *, LinkedListNode_1_t3787551078 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((LinkedList_1_t2045451540 *)__this, (LinkedListNode_1_t3787551078 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
		return (bool)1;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::Remove(System.Collections.Generic.LinkedListNode`1<T>)
extern "C"  void LinkedList_1_Remove_m4034790180_gshared (LinkedList_1_t2045451540 * __this, LinkedListNode_1_t3787551078 * ___node0, const MethodInfo* method)
{
	{
		LinkedListNode_1_t3787551078 * L_0 = ___node0;
		NullCheck((LinkedList_1_t2045451540 *)__this);
		((  void (*) (LinkedList_1_t2045451540 *, LinkedListNode_1_t3787551078 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18)->methodPointer)((LinkedList_1_t2045451540 *)__this, (LinkedListNode_1_t3787551078 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18));
		uint32_t L_1 = (uint32_t)__this->get_count_0();
		__this->set_count_0(((int32_t)((int32_t)L_1-(int32_t)1)));
		uint32_t L_2 = (uint32_t)__this->get_count_0();
		if (L_2)
		{
			goto IL_0027;
		}
	}
	{
		__this->set_first_3((LinkedListNode_1_t3787551078 *)NULL);
	}

IL_0027:
	{
		LinkedListNode_1_t3787551078 * L_3 = ___node0;
		LinkedListNode_1_t3787551078 * L_4 = (LinkedListNode_1_t3787551078 *)__this->get_first_3();
		if ((!(((Il2CppObject*)(LinkedListNode_1_t3787551078 *)L_3) == ((Il2CppObject*)(LinkedListNode_1_t3787551078 *)L_4))))
		{
			goto IL_0044;
		}
	}
	{
		LinkedListNode_1_t3787551078 * L_5 = (LinkedListNode_1_t3787551078 *)__this->get_first_3();
		NullCheck(L_5);
		LinkedListNode_1_t3787551078 * L_6 = (LinkedListNode_1_t3787551078 *)L_5->get_forward_2();
		__this->set_first_3(L_6);
	}

IL_0044:
	{
		uint32_t L_7 = (uint32_t)__this->get_version_1();
		__this->set_version_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		LinkedListNode_1_t3787551078 * L_8 = ___node0;
		NullCheck((LinkedListNode_1_t3787551078 *)L_8);
		((  void (*) (LinkedListNode_1_t3787551078 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19)->methodPointer)((LinkedListNode_1_t3787551078 *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19));
		return;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::RemoveLast()
extern "C"  void LinkedList_1_RemoveLast_m2573038887_gshared (LinkedList_1_t2045451540 * __this, const MethodInfo* method)
{
	{
		LinkedListNode_1_t3787551078 * L_0 = (LinkedListNode_1_t3787551078 *)__this->get_first_3();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		LinkedListNode_1_t3787551078 * L_1 = (LinkedListNode_1_t3787551078 *)__this->get_first_3();
		NullCheck(L_1);
		LinkedListNode_1_t3787551078 * L_2 = (LinkedListNode_1_t3787551078 *)L_1->get_back_3();
		NullCheck((LinkedList_1_t2045451540 *)__this);
		((  void (*) (LinkedList_1_t2045451540 *, LinkedListNode_1_t3787551078 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((LinkedList_1_t2045451540 *)__this, (LinkedListNode_1_t3787551078 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
	}

IL_001c:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.LinkedList`1<System.Object>::get_Count()
extern "C"  int32_t LinkedList_1_get_Count_m1368924491_gshared (LinkedList_1_t2045451540 * __this, const MethodInfo* method)
{
	{
		uint32_t L_0 = (uint32_t)__this->get_count_0();
		return L_0;
	}
}
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<System.Object>::get_First()
extern "C"  LinkedListNode_1_t3787551078 * LinkedList_1_get_First_m3278587786_gshared (LinkedList_1_t2045451540 * __this, const MethodInfo* method)
{
	{
		LinkedListNode_1_t3787551078 * L_0 = (LinkedListNode_1_t3787551078 *)__this->get_first_3();
		return L_0;
	}
}
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<System.Object>::get_Last()
extern "C"  LinkedListNode_1_t3787551078 * LinkedList_1_get_Last_m270176030_gshared (LinkedList_1_t2045451540 * __this, const MethodInfo* method)
{
	LinkedListNode_1_t3787551078 * G_B3_0 = NULL;
	{
		LinkedListNode_1_t3787551078 * L_0 = (LinkedListNode_1_t3787551078 *)__this->get_first_3();
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		LinkedListNode_1_t3787551078 * L_1 = (LinkedListNode_1_t3787551078 *)__this->get_first_3();
		NullCheck(L_1);
		LinkedListNode_1_t3787551078 * L_2 = (LinkedListNode_1_t3787551078 *)L_1->get_back_3();
		G_B3_0 = L_2;
		goto IL_001c;
	}

IL_001b:
	{
		G_B3_0 = ((LinkedListNode_1_t3787551078 *)(NULL));
	}

IL_001c:
	{
		return G_B3_0;
	}
}
// System.Void System.Collections.Generic.LinkedListNode`1<System.Object>::.ctor(System.Collections.Generic.LinkedList`1<T>,T)
extern "C"  void LinkedListNode_1__ctor_m648136130_gshared (LinkedListNode_1_t3787551078 * __this, LinkedList_1_t2045451540 * ___list0, Il2CppObject * ___value1, const MethodInfo* method)
{
	LinkedListNode_1_t3787551078 * V_0 = NULL;
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		LinkedList_1_t2045451540 * L_0 = ___list0;
		__this->set_container_1(L_0);
		Il2CppObject * L_1 = ___value1;
		__this->set_item_0(L_1);
		V_0 = (LinkedListNode_1_t3787551078 *)__this;
		__this->set_forward_2(__this);
		LinkedListNode_1_t3787551078 * L_2 = V_0;
		__this->set_back_3(L_2);
		return;
	}
}
// System.Void System.Collections.Generic.LinkedListNode`1<System.Object>::.ctor(System.Collections.Generic.LinkedList`1<T>,T,System.Collections.Generic.LinkedListNode`1<T>,System.Collections.Generic.LinkedListNode`1<T>)
extern "C"  void LinkedListNode_1__ctor_m448391458_gshared (LinkedListNode_1_t3787551078 * __this, LinkedList_1_t2045451540 * ___list0, Il2CppObject * ___value1, LinkedListNode_1_t3787551078 * ___previousNode2, LinkedListNode_1_t3787551078 * ___nextNode3, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		LinkedList_1_t2045451540 * L_0 = ___list0;
		__this->set_container_1(L_0);
		Il2CppObject * L_1 = ___value1;
		__this->set_item_0(L_1);
		LinkedListNode_1_t3787551078 * L_2 = ___previousNode2;
		__this->set_back_3(L_2);
		LinkedListNode_1_t3787551078 * L_3 = ___nextNode3;
		__this->set_forward_2(L_3);
		LinkedListNode_1_t3787551078 * L_4 = ___previousNode2;
		NullCheck(L_4);
		L_4->set_forward_2(__this);
		LinkedListNode_1_t3787551078 * L_5 = ___nextNode3;
		NullCheck(L_5);
		L_5->set_back_3(__this);
		return;
	}
}
// System.Void System.Collections.Generic.LinkedListNode`1<System.Object>::Detach()
extern "C"  void LinkedListNode_1_Detach_m3406254942_gshared (LinkedListNode_1_t3787551078 * __this, const MethodInfo* method)
{
	LinkedListNode_1_t3787551078 * V_0 = NULL;
	{
		LinkedListNode_1_t3787551078 * L_0 = (LinkedListNode_1_t3787551078 *)__this->get_back_3();
		LinkedListNode_1_t3787551078 * L_1 = (LinkedListNode_1_t3787551078 *)__this->get_forward_2();
		NullCheck(L_0);
		L_0->set_forward_2(L_1);
		LinkedListNode_1_t3787551078 * L_2 = (LinkedListNode_1_t3787551078 *)__this->get_forward_2();
		LinkedListNode_1_t3787551078 * L_3 = (LinkedListNode_1_t3787551078 *)__this->get_back_3();
		NullCheck(L_2);
		L_2->set_back_3(L_3);
		V_0 = (LinkedListNode_1_t3787551078 *)NULL;
		__this->set_back_3((LinkedListNode_1_t3787551078 *)NULL);
		LinkedListNode_1_t3787551078 * L_4 = V_0;
		__this->set_forward_2(L_4);
		__this->set_container_1((LinkedList_1_t2045451540 *)NULL);
		return;
	}
}
// System.Collections.Generic.LinkedList`1<T> System.Collections.Generic.LinkedListNode`1<System.Object>::get_List()
extern "C"  LinkedList_1_t2045451540 * LinkedListNode_1_get_List_m3467110818_gshared (LinkedListNode_1_t3787551078 * __this, const MethodInfo* method)
{
	{
		LinkedList_1_t2045451540 * L_0 = (LinkedList_1_t2045451540 *)__this->get_container_1();
		return L_0;
	}
}
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedListNode`1<System.Object>::get_Previous()
extern "C"  LinkedListNode_1_t3787551078 * LinkedListNode_1_get_Previous_m3755155549_gshared (LinkedListNode_1_t3787551078 * __this, const MethodInfo* method)
{
	LinkedListNode_1_t3787551078 * G_B4_0 = NULL;
	{
		LinkedList_1_t2045451540 * L_0 = (LinkedList_1_t2045451540 *)__this->get_container_1();
		if (!L_0)
		{
			goto IL_0027;
		}
	}
	{
		LinkedList_1_t2045451540 * L_1 = (LinkedList_1_t2045451540 *)__this->get_container_1();
		NullCheck(L_1);
		LinkedListNode_1_t3787551078 * L_2 = (LinkedListNode_1_t3787551078 *)L_1->get_first_3();
		if ((((Il2CppObject*)(LinkedListNode_1_t3787551078 *)__this) == ((Il2CppObject*)(LinkedListNode_1_t3787551078 *)L_2)))
		{
			goto IL_0027;
		}
	}
	{
		LinkedListNode_1_t3787551078 * L_3 = (LinkedListNode_1_t3787551078 *)__this->get_back_3();
		G_B4_0 = L_3;
		goto IL_0028;
	}

IL_0027:
	{
		G_B4_0 = ((LinkedListNode_1_t3787551078 *)(NULL));
	}

IL_0028:
	{
		return G_B4_0;
	}
}
// T System.Collections.Generic.LinkedListNode`1<System.Object>::get_Value()
extern "C"  Il2CppObject * LinkedListNode_1_get_Value_m702633824_gshared (LinkedListNode_1_t3787551078 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_item_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.List`1/Enumerator<Core.RpsChoice>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m347781690_gshared (Enumerator_t1440655454 * __this, List_1_t1420982684 * ___l0, const MethodInfo* method)
{
	{
		List_1_t1420982684 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t1420982684 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m347781690_AdjustorThunk (Il2CppObject * __this, List_1_t1420982684 * ___l0, const MethodInfo* method)
{
	Enumerator_t1440655454 * _thisAdjusted = reinterpret_cast<Enumerator_t1440655454 *>(__this + 1);
	Enumerator__ctor_m347781690(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Core.RpsChoice>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1411603928_gshared (Enumerator_t1440655454 * __this, const MethodInfo* method)
{
	{
		((  void (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t1440655454 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1411603928_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1440655454 * _thisAdjusted = reinterpret_cast<Enumerator_t1440655454 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m1411603928(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<Core.RpsChoice>::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m1771233294_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1771233294_gshared (Enumerator_t1440655454 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m1771233294_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		((  void (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t1440655454 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m355676978(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_current_3();
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1771233294_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1440655454 * _thisAdjusted = reinterpret_cast<Enumerator_t1440655454 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m1771233294(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Core.RpsChoice>::Dispose()
extern "C"  void Enumerator_Dispose_m1280702111_gshared (Enumerator_t1440655454 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t1420982684 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m1280702111_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1440655454 * _thisAdjusted = reinterpret_cast<Enumerator_t1440655454 *>(__this + 1);
	Enumerator_Dispose_m1280702111(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Core.RpsChoice>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3238255034;
extern const uint32_t Enumerator_VerifyState_m375344728_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m375344728_gshared (Enumerator_t1440655454 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m375344728_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t1420982684 * L_0 = (List_1_t1420982684 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t1440655454  L_1 = (*(Enumerator_t1440655454 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t1794727681 * L_5 = (ObjectDisposedException_t1794727681 *)il2cpp_codegen_object_new(ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t1420982684 * L_7 = (List_1_t1420982684 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_9 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_9, (String_t*)_stringLiteral3238255034, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m375344728_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1440655454 * _thisAdjusted = reinterpret_cast<Enumerator_t1440655454 *>(__this + 1);
	Enumerator_VerifyState_m375344728(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<Core.RpsChoice>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m4131464200_gshared (Enumerator_t1440655454 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		((  void (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t1440655454 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t1420982684 * L_2 = (List_1_t1420982684 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t1420982684 * L_4 = (List_1_t1420982684 *)__this->get_l_0();
		NullCheck(L_4);
		RpsChoiceU5BU5D_t3806589061* L_5 = (RpsChoiceU5BU5D_t3806589061*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_8);
		int32_t L_9 = L_8;
		int32_t L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m4131464200_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1440655454 * _thisAdjusted = reinterpret_cast<Enumerator_t1440655454 *>(__this + 1);
	return Enumerator_MoveNext_m4131464200(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<Core.RpsChoice>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m2760603185_gshared (Enumerator_t1440655454 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_current_3();
		return L_0;
	}
}
extern "C"  int32_t Enumerator_get_Current_m2760603185_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1440655454 * _thisAdjusted = reinterpret_cast<Enumerator_t1440655454 *>(__this + 1);
	return Enumerator_get_Current_m2760603185(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Core.RpsResult>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m2771135414_gshared (Enumerator_t1867453146 * __this, List_1_t1847780376 * ___l0, const MethodInfo* method)
{
	{
		List_1_t1847780376 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t1847780376 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m2771135414_AdjustorThunk (Il2CppObject * __this, List_1_t1847780376 * ___l0, const MethodInfo* method)
{
	Enumerator_t1867453146 * _thisAdjusted = reinterpret_cast<Enumerator_t1867453146 *>(__this + 1);
	Enumerator__ctor_m2771135414(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Core.RpsResult>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m364375772_gshared (Enumerator_t1867453146 * __this, const MethodInfo* method)
{
	{
		((  void (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t1867453146 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m364375772_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1867453146 * _thisAdjusted = reinterpret_cast<Enumerator_t1867453146 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m364375772(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<Core.RpsResult>::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m3880754706_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3880754706_gshared (Enumerator_t1867453146 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m3880754706_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		((  void (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t1867453146 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m355676978(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_current_3();
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3880754706_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1867453146 * _thisAdjusted = reinterpret_cast<Enumerator_t1867453146 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m3880754706(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Core.RpsResult>::Dispose()
extern "C"  void Enumerator_Dispose_m2247001883_gshared (Enumerator_t1867453146 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t1847780376 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m2247001883_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1867453146 * _thisAdjusted = reinterpret_cast<Enumerator_t1867453146 *>(__this + 1);
	Enumerator_Dispose_m2247001883(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Core.RpsResult>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3238255034;
extern const uint32_t Enumerator_VerifyState_m3087220948_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m3087220948_gshared (Enumerator_t1867453146 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m3087220948_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t1847780376 * L_0 = (List_1_t1847780376 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t1867453146  L_1 = (*(Enumerator_t1867453146 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t1794727681 * L_5 = (ObjectDisposedException_t1794727681 *)il2cpp_codegen_object_new(ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t1847780376 * L_7 = (List_1_t1847780376 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_9 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_9, (String_t*)_stringLiteral3238255034, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m3087220948_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1867453146 * _thisAdjusted = reinterpret_cast<Enumerator_t1867453146 *>(__this + 1);
	Enumerator_VerifyState_m3087220948(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<Core.RpsResult>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m4021986060_gshared (Enumerator_t1867453146 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		((  void (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t1867453146 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t1847780376 * L_2 = (List_1_t1847780376 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t1847780376 * L_4 = (List_1_t1847780376 *)__this->get_l_0();
		NullCheck(L_4);
		RpsResultU5BU5D_t1451851929* L_5 = (RpsResultU5BU5D_t1451851929*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_8);
		int32_t L_9 = L_8;
		int32_t L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m4021986060_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1867453146 * _thisAdjusted = reinterpret_cast<Enumerator_t1867453146 *>(__this + 1);
	return Enumerator_MoveNext_m4021986060(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<Core.RpsResult>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m1177512109_gshared (Enumerator_t1867453146 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_current_3();
		return L_0;
	}
}
extern "C"  int32_t Enumerator_get_Current_m1177512109_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1867453146 * _thisAdjusted = reinterpret_cast<Enumerator_t1867453146 *>(__this + 1);
	return Enumerator_get_Current_m1177512109(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Entity.Behavior.EBehaviorID>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m2118648904_gshared (Enumerator_t2626391458 * __this, List_1_t2606718688 * ___l0, const MethodInfo* method)
{
	{
		List_1_t2606718688 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t2606718688 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m2118648904_AdjustorThunk (Il2CppObject * __this, List_1_t2606718688 * ___l0, const MethodInfo* method)
{
	Enumerator_t2626391458 * _thisAdjusted = reinterpret_cast<Enumerator_t2626391458 *>(__this + 1);
	Enumerator__ctor_m2118648904(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Entity.Behavior.EBehaviorID>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2142733066_gshared (Enumerator_t2626391458 * __this, const MethodInfo* method)
{
	{
		((  void (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t2626391458 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2142733066_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2626391458 * _thisAdjusted = reinterpret_cast<Enumerator_t2626391458 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m2142733066(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<Entity.Behavior.EBehaviorID>::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m3565809014_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3565809014_gshared (Enumerator_t2626391458 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m3565809014_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		((  void (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t2626391458 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m355676978(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		uint8_t L_2 = (uint8_t)__this->get_current_3();
		uint8_t L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3565809014_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2626391458 * _thisAdjusted = reinterpret_cast<Enumerator_t2626391458 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m3565809014(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Entity.Behavior.EBehaviorID>::Dispose()
extern "C"  void Enumerator_Dispose_m3614712877_gshared (Enumerator_t2626391458 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t2606718688 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m3614712877_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2626391458 * _thisAdjusted = reinterpret_cast<Enumerator_t2626391458 *>(__this + 1);
	Enumerator_Dispose_m3614712877(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Entity.Behavior.EBehaviorID>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3238255034;
extern const uint32_t Enumerator_VerifyState_m1685062886_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m1685062886_gshared (Enumerator_t2626391458 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m1685062886_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t2606718688 * L_0 = (List_1_t2606718688 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t2626391458  L_1 = (*(Enumerator_t2626391458 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t1794727681 * L_5 = (ObjectDisposedException_t1794727681 *)il2cpp_codegen_object_new(ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t2606718688 * L_7 = (List_1_t2606718688 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_9 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_9, (String_t*)_stringLiteral3238255034, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m1685062886_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2626391458 * _thisAdjusted = reinterpret_cast<Enumerator_t2626391458 *>(__this + 1);
	Enumerator_VerifyState_m1685062886(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<Entity.Behavior.EBehaviorID>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1732968822_gshared (Enumerator_t2626391458 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		((  void (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t2626391458 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t2606718688 * L_2 = (List_1_t2606718688 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t2606718688 * L_4 = (List_1_t2606718688 *)__this->get_l_0();
		NullCheck(L_4);
		EBehaviorIDU5BU5D_t373889457* L_5 = (EBehaviorIDU5BU5D_t373889457*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_8);
		int32_t L_9 = L_8;
		uint8_t L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m1732968822_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2626391458 * _thisAdjusted = reinterpret_cast<Enumerator_t2626391458 *>(__this + 1);
	return Enumerator_MoveNext_m1732968822(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<Entity.Behavior.EBehaviorID>::get_Current()
extern "C"  uint8_t Enumerator_get_Current_m4294259357_gshared (Enumerator_t2626391458 * __this, const MethodInfo* method)
{
	{
		uint8_t L_0 = (uint8_t)__this->get_current_3();
		return L_0;
	}
}
extern "C"  uint8_t Enumerator_get_Current_m4294259357_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2626391458 * _thisAdjusted = reinterpret_cast<Enumerator_t2626391458 *>(__this + 1);
	return Enumerator_get_Current_m4294259357(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<HatredCtrl/stValue>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m3702358741_gshared (Enumerator_t518836436 * __this, List_1_t499163666 * ___l0, const MethodInfo* method)
{
	{
		List_1_t499163666 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t499163666 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m3702358741_AdjustorThunk (Il2CppObject * __this, List_1_t499163666 * ___l0, const MethodInfo* method)
{
	Enumerator_t518836436 * _thisAdjusted = reinterpret_cast<Enumerator_t518836436 *>(__this + 1);
	Enumerator__ctor_m3702358741(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<HatredCtrl/stValue>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1018585181_gshared (Enumerator_t518836436 * __this, const MethodInfo* method)
{
	{
		((  void (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t518836436 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1018585181_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t518836436 * _thisAdjusted = reinterpret_cast<Enumerator_t518836436 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m1018585181(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<HatredCtrl/stValue>::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m1220151379_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1220151379_gshared (Enumerator_t518836436 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m1220151379_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		((  void (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t518836436 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m355676978(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		stValue_t3425945410  L_2 = (stValue_t3425945410 )__this->get_current_3();
		stValue_t3425945410  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1220151379_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t518836436 * _thisAdjusted = reinterpret_cast<Enumerator_t518836436 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m1220151379(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<HatredCtrl/stValue>::Dispose()
extern "C"  void Enumerator_Dispose_m3851703802_gshared (Enumerator_t518836436 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t499163666 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m3851703802_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t518836436 * _thisAdjusted = reinterpret_cast<Enumerator_t518836436 *>(__this + 1);
	Enumerator_Dispose_m3851703802(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<HatredCtrl/stValue>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3238255034;
extern const uint32_t Enumerator_VerifyState_m542672947_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m542672947_gshared (Enumerator_t518836436 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m542672947_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t499163666 * L_0 = (List_1_t499163666 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t518836436  L_1 = (*(Enumerator_t518836436 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t1794727681 * L_5 = (ObjectDisposedException_t1794727681 *)il2cpp_codegen_object_new(ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t499163666 * L_7 = (List_1_t499163666 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_9 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_9, (String_t*)_stringLiteral3238255034, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m542672947_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t518836436 * _thisAdjusted = reinterpret_cast<Enumerator_t518836436 *>(__this + 1);
	Enumerator_VerifyState_m542672947(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<HatredCtrl/stValue>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2089574157_gshared (Enumerator_t518836436 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		((  void (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t518836436 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t499163666 * L_2 = (List_1_t499163666 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t499163666 * L_4 = (List_1_t499163666 *)__this->get_l_0();
		NullCheck(L_4);
		stValueU5BU5D_t1121401207* L_5 = (stValueU5BU5D_t1121401207*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_8);
		int32_t L_9 = L_8;
		stValue_t3425945410  L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m2089574157_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t518836436 * _thisAdjusted = reinterpret_cast<Enumerator_t518836436 *>(__this + 1);
	return Enumerator_MoveNext_m2089574157(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<HatredCtrl/stValue>::get_Current()
extern "C"  stValue_t3425945410  Enumerator_get_Current_m1131141004_gshared (Enumerator_t518836436 * __this, const MethodInfo* method)
{
	{
		stValue_t3425945410  L_0 = (stValue_t3425945410 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  stValue_t3425945410  Enumerator_get_Current_m1131141004_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t518836436 * _thisAdjusted = reinterpret_cast<Enumerator_t518836436 *>(__this + 1);
	return Enumerator_get_Current_m1131141004(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Mihua.Assets.SubAssetMgr/ErrorInfo>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m2381542061_gshared (Enumerator_t4021839532 * __this, List_1_t4002166762 * ___l0, const MethodInfo* method)
{
	{
		List_1_t4002166762 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t4002166762 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m2381542061_AdjustorThunk (Il2CppObject * __this, List_1_t4002166762 * ___l0, const MethodInfo* method)
{
	Enumerator_t4021839532 * _thisAdjusted = reinterpret_cast<Enumerator_t4021839532 *>(__this + 1);
	Enumerator__ctor_m2381542061(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2235558277_gshared (Enumerator_t4021839532 * __this, const MethodInfo* method)
{
	{
		((  void (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t4021839532 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2235558277_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4021839532 * _thisAdjusted = reinterpret_cast<Enumerator_t4021839532 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m2235558277(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m1649019259_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1649019259_gshared (Enumerator_t4021839532 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m1649019259_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		((  void (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t4021839532 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m355676978(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		ErrorInfo_t2633981210  L_2 = (ErrorInfo_t2633981210 )__this->get_current_3();
		ErrorInfo_t2633981210  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1649019259_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4021839532 * _thisAdjusted = reinterpret_cast<Enumerator_t4021839532 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m1649019259(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Mihua.Assets.SubAssetMgr/ErrorInfo>::Dispose()
extern "C"  void Enumerator_Dispose_m194100178_gshared (Enumerator_t4021839532 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t4002166762 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m194100178_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4021839532 * _thisAdjusted = reinterpret_cast<Enumerator_t4021839532 *>(__this + 1);
	Enumerator_Dispose_m194100178(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Mihua.Assets.SubAssetMgr/ErrorInfo>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3238255034;
extern const uint32_t Enumerator_VerifyState_m2600419851_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m2600419851_gshared (Enumerator_t4021839532 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m2600419851_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t4002166762 * L_0 = (List_1_t4002166762 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t4021839532  L_1 = (*(Enumerator_t4021839532 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t1794727681 * L_5 = (ObjectDisposedException_t1794727681 *)il2cpp_codegen_object_new(ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t4002166762 * L_7 = (List_1_t4002166762 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_9 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_9, (String_t*)_stringLiteral3238255034, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m2600419851_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4021839532 * _thisAdjusted = reinterpret_cast<Enumerator_t4021839532 *>(__this + 1);
	Enumerator_VerifyState_m2600419851(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<Mihua.Assets.SubAssetMgr/ErrorInfo>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m240611381_gshared (Enumerator_t4021839532 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		((  void (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t4021839532 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t4002166762 * L_2 = (List_1_t4002166762 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t4002166762 * L_4 = (List_1_t4002166762 *)__this->get_l_0();
		NullCheck(L_4);
		ErrorInfoU5BU5D_t2864912703* L_5 = (ErrorInfoU5BU5D_t2864912703*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_8);
		int32_t L_9 = L_8;
		ErrorInfo_t2633981210  L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m240611381_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4021839532 * _thisAdjusted = reinterpret_cast<Enumerator_t4021839532 *>(__this + 1);
	return Enumerator_MoveNext_m240611381(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<Mihua.Assets.SubAssetMgr/ErrorInfo>::get_Current()
extern "C"  ErrorInfo_t2633981210  Enumerator_get_Current_m3350310756_gshared (Enumerator_t4021839532 * __this, const MethodInfo* method)
{
	{
		ErrorInfo_t2633981210  L_0 = (ErrorInfo_t2633981210 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  ErrorInfo_t2633981210  Enumerator_get_Current_m3350310756_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4021839532 * _thisAdjusted = reinterpret_cast<Enumerator_t4021839532 *>(__this + 1);
	return Enumerator_get_Current_m3350310756(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Newtonsoft.Json.Linq.JTokenType>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m764303625_gshared (Enumerator_t1009788587 * __this, List_1_t990115817 * ___l0, const MethodInfo* method)
{
	{
		List_1_t990115817 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t990115817 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m764303625_AdjustorThunk (Il2CppObject * __this, List_1_t990115817 * ___l0, const MethodInfo* method)
{
	Enumerator_t1009788587 * _thisAdjusted = reinterpret_cast<Enumerator_t1009788587 *>(__this + 1);
	Enumerator__ctor_m764303625(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Newtonsoft.Json.Linq.JTokenType>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3037559209_gshared (Enumerator_t1009788587 * __this, const MethodInfo* method)
{
	{
		((  void (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t1009788587 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3037559209_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1009788587 * _thisAdjusted = reinterpret_cast<Enumerator_t1009788587 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m3037559209(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<Newtonsoft.Json.Linq.JTokenType>::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m54608725_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m54608725_gshared (Enumerator_t1009788587 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m54608725_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		((  void (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t1009788587 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m355676978(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_current_3();
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m54608725_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1009788587 * _thisAdjusted = reinterpret_cast<Enumerator_t1009788587 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m54608725(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Newtonsoft.Json.Linq.JTokenType>::Dispose()
extern "C"  void Enumerator_Dispose_m2698443054_gshared (Enumerator_t1009788587 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t990115817 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m2698443054_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1009788587 * _thisAdjusted = reinterpret_cast<Enumerator_t1009788587 *>(__this + 1);
	Enumerator_Dispose_m2698443054(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Newtonsoft.Json.Linq.JTokenType>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3238255034;
extern const uint32_t Enumerator_VerifyState_m1718514023_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m1718514023_gshared (Enumerator_t1009788587 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m1718514023_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t990115817 * L_0 = (List_1_t990115817 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t1009788587  L_1 = (*(Enumerator_t1009788587 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t1794727681 * L_5 = (ObjectDisposedException_t1794727681 *)il2cpp_codegen_object_new(ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t990115817 * L_7 = (List_1_t990115817 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_9 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_9, (String_t*)_stringLiteral3238255034, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m1718514023_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1009788587 * _thisAdjusted = reinterpret_cast<Enumerator_t1009788587 *>(__this + 1);
	Enumerator_VerifyState_m1718514023(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<Newtonsoft.Json.Linq.JTokenType>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2860088725_gshared (Enumerator_t1009788587 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		((  void (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t1009788587 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t990115817 * L_2 = (List_1_t990115817 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t990115817 * L_4 = (List_1_t990115817 *)__this->get_l_0();
		NullCheck(L_4);
		JTokenTypeU5BU5D_t1069135460* L_5 = (JTokenTypeU5BU5D_t1069135460*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_8);
		int32_t L_9 = L_8;
		int32_t L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m2860088725_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1009788587 * _thisAdjusted = reinterpret_cast<Enumerator_t1009788587 *>(__this + 1);
	return Enumerator_MoveNext_m2860088725(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<Newtonsoft.Json.Linq.JTokenType>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m2500372382_gshared (Enumerator_t1009788587 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_current_3();
		return L_0;
	}
}
extern "C"  int32_t Enumerator_get_Current_m2500372382_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1009788587 * _thisAdjusted = reinterpret_cast<Enumerator_t1009788587 *>(__this + 1);
	return Enumerator_get_Current_m2500372382(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Newtonsoft.Json.Schema.JsonSchemaType>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m3223797442_gshared (Enumerator_t3503274143 * __this, List_1_t3483601373 * ___l0, const MethodInfo* method)
{
	{
		List_1_t3483601373 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t3483601373 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m3223797442_AdjustorThunk (Il2CppObject * __this, List_1_t3483601373 * ___l0, const MethodInfo* method)
{
	Enumerator_t3503274143 * _thisAdjusted = reinterpret_cast<Enumerator_t3503274143 *>(__this + 1);
	Enumerator__ctor_m3223797442(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1401431120_gshared (Enumerator_t3503274143 * __this, const MethodInfo* method)
{
	{
		((  void (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t3503274143 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1401431120_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3503274143 * _thisAdjusted = reinterpret_cast<Enumerator_t3503274143 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m1401431120(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m2774718588_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2774718588_gshared (Enumerator_t3503274143 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m2774718588_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		((  void (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t3503274143 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m355676978(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_current_3();
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2774718588_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3503274143 * _thisAdjusted = reinterpret_cast<Enumerator_t3503274143 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m2774718588(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Newtonsoft.Json.Schema.JsonSchemaType>::Dispose()
extern "C"  void Enumerator_Dispose_m472259879_gshared (Enumerator_t3503274143 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t3483601373 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m472259879_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3503274143 * _thisAdjusted = reinterpret_cast<Enumerator_t3503274143 *>(__this + 1);
	Enumerator_Dispose_m472259879(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Newtonsoft.Json.Schema.JsonSchemaType>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3238255034;
extern const uint32_t Enumerator_VerifyState_m2636706016_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m2636706016_gshared (Enumerator_t3503274143 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m2636706016_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t3483601373 * L_0 = (List_1_t3483601373 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t3503274143  L_1 = (*(Enumerator_t3503274143 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t1794727681 * L_5 = (ObjectDisposedException_t1794727681 *)il2cpp_codegen_object_new(ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t3483601373 * L_7 = (List_1_t3483601373 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_9 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_9, (String_t*)_stringLiteral3238255034, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m2636706016_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3503274143 * _thisAdjusted = reinterpret_cast<Enumerator_t3503274143 *>(__this + 1);
	Enumerator_VerifyState_m2636706016(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<Newtonsoft.Json.Schema.JsonSchemaType>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2097934908_gshared (Enumerator_t3503274143 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		((  void (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t3503274143 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t3483601373 * L_2 = (List_1_t3483601373 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t3483601373 * L_4 = (List_1_t3483601373 *)__this->get_l_0();
		NullCheck(L_4);
		JsonSchemaTypeU5BU5D_t2531260960* L_5 = (JsonSchemaTypeU5BU5D_t2531260960*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_8);
		int32_t L_9 = L_8;
		int32_t L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m2097934908_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3503274143 * _thisAdjusted = reinterpret_cast<Enumerator_t3503274143 *>(__this + 1);
	return Enumerator_MoveNext_m2097934908(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<Newtonsoft.Json.Schema.JsonSchemaType>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m231947223_gshared (Enumerator_t3503274143 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_current_3();
		return L_0;
	}
}
extern "C"  int32_t Enumerator_get_Current_m231947223_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3503274143 * _thisAdjusted = reinterpret_cast<Enumerator_t3503274143 *>(__this + 1);
	return Enumerator_get_Current_m231947223(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.AdvancedSmooth/Turn>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m832639577_gshared (Enumerator_t1853053700 * __this, List_1_t1833380930 * ___l0, const MethodInfo* method)
{
	{
		List_1_t1833380930 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t1833380930 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m832639577_AdjustorThunk (Il2CppObject * __this, List_1_t1833380930 * ___l0, const MethodInfo* method)
{
	Enumerator_t1853053700 * _thisAdjusted = reinterpret_cast<Enumerator_t1853053700 *>(__this + 1);
	Enumerator__ctor_m832639577(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.AdvancedSmooth/Turn>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2564828761_gshared (Enumerator_t1853053700 * __this, const MethodInfo* method)
{
	{
		((  void (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t1853053700 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2564828761_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1853053700 * _thisAdjusted = reinterpret_cast<Enumerator_t1853053700 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m2564828761(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<Pathfinding.AdvancedSmooth/Turn>::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m2173023237_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2173023237_gshared (Enumerator_t1853053700 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m2173023237_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		((  void (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t1853053700 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m355676978(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		Turn_t465195378  L_2 = (Turn_t465195378 )__this->get_current_3();
		Turn_t465195378  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2173023237_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1853053700 * _thisAdjusted = reinterpret_cast<Enumerator_t1853053700 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m2173023237(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.AdvancedSmooth/Turn>::Dispose()
extern "C"  void Enumerator_Dispose_m1173423742_gshared (Enumerator_t1853053700 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t1833380930 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m1173423742_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1853053700 * _thisAdjusted = reinterpret_cast<Enumerator_t1853053700 *>(__this + 1);
	Enumerator_Dispose_m1173423742(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.AdvancedSmooth/Turn>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3238255034;
extern const uint32_t Enumerator_VerifyState_m2854311607_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m2854311607_gshared (Enumerator_t1853053700 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m2854311607_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t1833380930 * L_0 = (List_1_t1833380930 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t1853053700  L_1 = (*(Enumerator_t1853053700 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t1794727681 * L_5 = (ObjectDisposedException_t1794727681 *)il2cpp_codegen_object_new(ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t1833380930 * L_7 = (List_1_t1833380930 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_9 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_9, (String_t*)_stringLiteral3238255034, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m2854311607_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1853053700 * _thisAdjusted = reinterpret_cast<Enumerator_t1853053700 *>(__this + 1);
	Enumerator_VerifyState_m2854311607(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<Pathfinding.AdvancedSmooth/Turn>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2829130309_gshared (Enumerator_t1853053700 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		((  void (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t1853053700 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t1833380930 * L_2 = (List_1_t1833380930 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t1833380930 * L_4 = (List_1_t1833380930 *)__this->get_l_0();
		NullCheck(L_4);
		TurnU5BU5D_t2705444999* L_5 = (TurnU5BU5D_t2705444999*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_8);
		int32_t L_9 = L_8;
		Turn_t465195378  L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m2829130309_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1853053700 * _thisAdjusted = reinterpret_cast<Enumerator_t1853053700 *>(__this + 1);
	return Enumerator_MoveNext_m2829130309(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<Pathfinding.AdvancedSmooth/Turn>::get_Current()
extern "C"  Turn_t465195378  Enumerator_get_Current_m3636169966_gshared (Enumerator_t1853053700 * __this, const MethodInfo* method)
{
	{
		Turn_t465195378  L_0 = (Turn_t465195378 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  Turn_t465195378  Enumerator_get_Current_m3636169966_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1853053700 * _thisAdjusted = reinterpret_cast<Enumerator_t1853053700 *>(__this + 1);
	return Enumerator_get_Current_m3636169966(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.ClipperLib.IntPoint>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m3963183348_gshared (Enumerator_t419017205 * __this, List_1_t399344435 * ___l0, const MethodInfo* method)
{
	{
		List_1_t399344435 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t399344435 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m3963183348_AdjustorThunk (Il2CppObject * __this, List_1_t399344435 * ___l0, const MethodInfo* method)
{
	Enumerator_t419017205 * _thisAdjusted = reinterpret_cast<Enumerator_t419017205 *>(__this + 1);
	Enumerator__ctor_m3963183348(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.ClipperLib.IntPoint>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m854715102_gshared (Enumerator_t419017205 * __this, const MethodInfo* method)
{
	{
		((  void (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t419017205 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m854715102_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t419017205 * _thisAdjusted = reinterpret_cast<Enumerator_t419017205 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m854715102(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<Pathfinding.ClipperLib.IntPoint>::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m435632330_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m435632330_gshared (Enumerator_t419017205 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m435632330_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		((  void (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t419017205 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m355676978(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		IntPoint_t3326126179  L_2 = (IntPoint_t3326126179 )__this->get_current_3();
		IntPoint_t3326126179  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m435632330_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t419017205 * _thisAdjusted = reinterpret_cast<Enumerator_t419017205 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m435632330(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.ClipperLib.IntPoint>::Dispose()
extern "C"  void Enumerator_Dispose_m1997958105_gshared (Enumerator_t419017205 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t399344435 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m1997958105_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t419017205 * _thisAdjusted = reinterpret_cast<Enumerator_t419017205 *>(__this + 1);
	Enumerator_Dispose_m1997958105(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.ClipperLib.IntPoint>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3238255034;
extern const uint32_t Enumerator_VerifyState_m1427019410_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m1427019410_gshared (Enumerator_t419017205 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m1427019410_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t399344435 * L_0 = (List_1_t399344435 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t419017205  L_1 = (*(Enumerator_t419017205 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t1794727681 * L_5 = (ObjectDisposedException_t1794727681 *)il2cpp_codegen_object_new(ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t399344435 * L_7 = (List_1_t399344435 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_9 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_9, (String_t*)_stringLiteral3238255034, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m1427019410_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t419017205 * _thisAdjusted = reinterpret_cast<Enumerator_t419017205 *>(__this + 1);
	Enumerator_VerifyState_m1427019410(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<Pathfinding.ClipperLib.IntPoint>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2619891786_gshared (Enumerator_t419017205 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		((  void (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t419017205 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t399344435 * L_2 = (List_1_t399344435 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t399344435 * L_4 = (List_1_t399344435 *)__this->get_l_0();
		NullCheck(L_4);
		IntPointU5BU5D_t3364663986* L_5 = (IntPointU5BU5D_t3364663986*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_8);
		int32_t L_9 = L_8;
		IntPoint_t3326126179  L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m2619891786_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t419017205 * _thisAdjusted = reinterpret_cast<Enumerator_t419017205 *>(__this + 1);
	return Enumerator_MoveNext_m2619891786(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<Pathfinding.ClipperLib.IntPoint>::get_Current()
extern "C"  IntPoint_t3326126179  Enumerator_get_Current_m2208877769_gshared (Enumerator_t419017205 * __this, const MethodInfo* method)
{
	{
		IntPoint_t3326126179  L_0 = (IntPoint_t3326126179 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  IntPoint_t3326126179  Enumerator_get_Current_m2208877769_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t419017205 * _thisAdjusted = reinterpret_cast<Enumerator_t419017205 *>(__this + 1);
	return Enumerator_get_Current_m2208877769(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.Int2>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m44454944_gshared (Enumerator_t3361903915 * __this, List_1_t3342231145 * ___l0, const MethodInfo* method)
{
	{
		List_1_t3342231145 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t3342231145 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m44454944_AdjustorThunk (Il2CppObject * __this, List_1_t3342231145 * ___l0, const MethodInfo* method)
{
	Enumerator_t3361903915 * _thisAdjusted = reinterpret_cast<Enumerator_t3361903915 *>(__this + 1);
	Enumerator__ctor_m44454944(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.Int2>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3211570226_gshared (Enumerator_t3361903915 * __this, const MethodInfo* method)
{
	{
		((  void (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t3361903915 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3211570226_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3361903915 * _thisAdjusted = reinterpret_cast<Enumerator_t3361903915 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m3211570226(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<Pathfinding.Int2>::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m1063668136_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1063668136_gshared (Enumerator_t3361903915 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m1063668136_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		((  void (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t3361903915 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m355676978(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		Int2_t1974045593  L_2 = (Int2_t1974045593 )__this->get_current_3();
		Int2_t1974045593  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1063668136_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3361903915 * _thisAdjusted = reinterpret_cast<Enumerator_t3361903915 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m1063668136(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.Int2>::Dispose()
extern "C"  void Enumerator_Dispose_m1419897349_gshared (Enumerator_t3361903915 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t3342231145 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m1419897349_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3361903915 * _thisAdjusted = reinterpret_cast<Enumerator_t3361903915 *>(__this + 1);
	Enumerator_Dispose_m1419897349(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.Int2>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3238255034;
extern const uint32_t Enumerator_VerifyState_m1729568446_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m1729568446_gshared (Enumerator_t3361903915 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m1729568446_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t3342231145 * L_0 = (List_1_t3342231145 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t3361903915  L_1 = (*(Enumerator_t3361903915 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t1794727681 * L_5 = (ObjectDisposedException_t1794727681 *)il2cpp_codegen_object_new(ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t3342231145 * L_7 = (List_1_t3342231145 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_9 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_9, (String_t*)_stringLiteral3238255034, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m1729568446_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3361903915 * _thisAdjusted = reinterpret_cast<Enumerator_t3361903915 *>(__this + 1);
	Enumerator_VerifyState_m1729568446(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<Pathfinding.Int2>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m249324770_gshared (Enumerator_t3361903915 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		((  void (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t3361903915 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t3342231145 * L_2 = (List_1_t3342231145 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t3342231145 * L_4 = (List_1_t3342231145 *)__this->get_l_0();
		NullCheck(L_4);
		Int2U5BU5D_t30096868* L_5 = (Int2U5BU5D_t30096868*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_8);
		int32_t L_9 = L_8;
		Int2_t1974045593  L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m249324770_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3361903915 * _thisAdjusted = reinterpret_cast<Enumerator_t3361903915 *>(__this + 1);
	return Enumerator_MoveNext_m249324770(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<Pathfinding.Int2>::get_Current()
extern "C"  Int2_t1974045593  Enumerator_get_Current_m3327185495_gshared (Enumerator_t3361903915 * __this, const MethodInfo* method)
{
	{
		Int2_t1974045593  L_0 = (Int2_t1974045593 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  Int2_t1974045593  Enumerator_get_Current_m3327185495_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3361903915 * _thisAdjusted = reinterpret_cast<Enumerator_t3361903915 *>(__this + 1);
	return Enumerator_get_Current_m3327185495(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.Int3>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m2015394401_gshared (Enumerator_t3361903916 * __this, List_1_t3342231146 * ___l0, const MethodInfo* method)
{
	{
		List_1_t3342231146 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t3342231146 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m2015394401_AdjustorThunk (Il2CppObject * __this, List_1_t3342231146 * ___l0, const MethodInfo* method)
{
	Enumerator_t3361903916 * _thisAdjusted = reinterpret_cast<Enumerator_t3361903916 *>(__this + 1);
	Enumerator__ctor_m2015394401(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.Int3>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3335643473_gshared (Enumerator_t3361903916 * __this, const MethodInfo* method)
{
	{
		((  void (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t3361903916 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3335643473_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3361903916 * _thisAdjusted = reinterpret_cast<Enumerator_t3361903916 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m3335643473(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<Pathfinding.Int3>::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m2033249159_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2033249159_gshared (Enumerator_t3361903916 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m2033249159_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		((  void (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t3361903916 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m355676978(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		Int3_t1974045594  L_2 = (Int3_t1974045594 )__this->get_current_3();
		Int3_t1974045594  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2033249159_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3361903916 * _thisAdjusted = reinterpret_cast<Enumerator_t3361903916 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m2033249159(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.Int3>::Dispose()
extern "C"  void Enumerator_Dispose_m1126494342_gshared (Enumerator_t3361903916 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t3342231146 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m1126494342_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3361903916 * _thisAdjusted = reinterpret_cast<Enumerator_t3361903916 *>(__this + 1);
	Enumerator_Dispose_m1126494342(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.Int3>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3238255034;
extern const uint32_t Enumerator_VerifyState_m3082878143_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m3082878143_gshared (Enumerator_t3361903916 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m3082878143_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t3342231146 * L_0 = (List_1_t3342231146 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t3361903916  L_1 = (*(Enumerator_t3361903916 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t1794727681 * L_5 = (ObjectDisposedException_t1794727681 *)il2cpp_codegen_object_new(ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t3342231146 * L_7 = (List_1_t3342231146 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_9 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_9, (String_t*)_stringLiteral3238255034, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m3082878143_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3361903916 * _thisAdjusted = reinterpret_cast<Enumerator_t3361903916 *>(__this + 1);
	Enumerator_VerifyState_m3082878143(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<Pathfinding.Int3>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m4038733441_gshared (Enumerator_t3361903916 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		((  void (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t3361903916 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t3342231146 * L_2 = (List_1_t3342231146 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t3342231146 * L_4 = (List_1_t3342231146 *)__this->get_l_0();
		NullCheck(L_4);
		Int3U5BU5D_t516284607* L_5 = (Int3U5BU5D_t516284607*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_8);
		int32_t L_9 = L_8;
		Int3_t1974045594  L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m4038733441_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3361903916 * _thisAdjusted = reinterpret_cast<Enumerator_t3361903916 *>(__this + 1);
	return Enumerator_MoveNext_m4038733441(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<Pathfinding.Int3>::get_Current()
extern "C"  Int3_t1974045594  Enumerator_get_Current_m385527896_gshared (Enumerator_t3361903916 * __this, const MethodInfo* method)
{
	{
		Int3_t1974045594  L_0 = (Int3_t1974045594 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  Int3_t1974045594  Enumerator_get_Current_m385527896_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3361903916 * _thisAdjusted = reinterpret_cast<Enumerator_t3361903916 *>(__this + 1);
	return Enumerator_get_Current_m385527896(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.IntRect>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m1501480368_gshared (Enumerator_t107949287 * __this, List_1_t88276517 * ___l0, const MethodInfo* method)
{
	{
		List_1_t88276517 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t88276517 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m1501480368_AdjustorThunk (Il2CppObject * __this, List_1_t88276517 * ___l0, const MethodInfo* method)
{
	Enumerator_t107949287 * _thisAdjusted = reinterpret_cast<Enumerator_t107949287 *>(__this + 1);
	Enumerator__ctor_m1501480368(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.IntRect>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m223838370_gshared (Enumerator_t107949287 * __this, const MethodInfo* method)
{
	{
		((  void (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t107949287 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m223838370_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t107949287 * _thisAdjusted = reinterpret_cast<Enumerator_t107949287 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m223838370(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<Pathfinding.IntRect>::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m3705500686_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3705500686_gshared (Enumerator_t107949287 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m3705500686_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		((  void (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t107949287 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m355676978(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		IntRect_t3015058261  L_2 = (IntRect_t3015058261 )__this->get_current_3();
		IntRect_t3015058261  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3705500686_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t107949287 * _thisAdjusted = reinterpret_cast<Enumerator_t107949287 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m3705500686(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.IntRect>::Dispose()
extern "C"  void Enumerator_Dispose_m574865301_gshared (Enumerator_t107949287 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t88276517 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m574865301_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t107949287 * _thisAdjusted = reinterpret_cast<Enumerator_t107949287 *>(__this + 1);
	Enumerator_Dispose_m574865301(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.IntRect>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3238255034;
extern const uint32_t Enumerator_VerifyState_m1035185230_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m1035185230_gshared (Enumerator_t107949287 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m1035185230_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t88276517 * L_0 = (List_1_t88276517 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t107949287  L_1 = (*(Enumerator_t107949287 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t1794727681 * L_5 = (ObjectDisposedException_t1794727681 *)il2cpp_codegen_object_new(ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t88276517 * L_7 = (List_1_t88276517 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_9 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_9, (String_t*)_stringLiteral3238255034, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m1035185230_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t107949287 * _thisAdjusted = reinterpret_cast<Enumerator_t107949287 *>(__this + 1);
	Enumerator_VerifyState_m1035185230(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<Pathfinding.IntRect>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3737121038_gshared (Enumerator_t107949287 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		((  void (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t107949287 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t88276517 * L_2 = (List_1_t88276517 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t88276517 * L_4 = (List_1_t88276517 *)__this->get_l_0();
		NullCheck(L_4);
		IntRectU5BU5D_t3425567672* L_5 = (IntRectU5BU5D_t3425567672*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_8);
		int32_t L_9 = L_8;
		IntRect_t3015058261  L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m3737121038_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t107949287 * _thisAdjusted = reinterpret_cast<Enumerator_t107949287 *>(__this + 1);
	return Enumerator_MoveNext_m3737121038(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<Pathfinding.IntRect>::get_Current()
extern "C"  IntRect_t3015058261  Enumerator_get_Current_m3127249157_gshared (Enumerator_t107949287 * __this, const MethodInfo* method)
{
	{
		IntRect_t3015058261  L_0 = (IntRect_t3015058261 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  IntRect_t3015058261  Enumerator_get_Current_m3127249157_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t107949287 * _thisAdjusted = reinterpret_cast<Enumerator_t107949287 *>(__this + 1);
	return Enumerator_get_Current_m3127249157(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.LocalAvoidance/IntersectionPair>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m550476822_gshared (Enumerator_t4209178241 * __this, List_1_t4189505471 * ___l0, const MethodInfo* method)
{
	{
		List_1_t4189505471 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t4189505471 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m550476822_AdjustorThunk (Il2CppObject * __this, List_1_t4189505471 * ___l0, const MethodInfo* method)
{
	Enumerator_t4209178241 * _thisAdjusted = reinterpret_cast<Enumerator_t4209178241 *>(__this + 1);
	Enumerator__ctor_m550476822(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.LocalAvoidance/IntersectionPair>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3003630204_gshared (Enumerator_t4209178241 * __this, const MethodInfo* method)
{
	{
		((  void (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t4209178241 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3003630204_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4209178241 * _thisAdjusted = reinterpret_cast<Enumerator_t4209178241 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m3003630204(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<Pathfinding.LocalAvoidance/IntersectionPair>::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m3715242344_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3715242344_gshared (Enumerator_t4209178241 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m3715242344_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		((  void (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t4209178241 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m355676978(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		IntersectionPair_t2821319919  L_2 = (IntersectionPair_t2821319919 )__this->get_current_3();
		IntersectionPair_t2821319919  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3715242344_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4209178241 * _thisAdjusted = reinterpret_cast<Enumerator_t4209178241 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m3715242344(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.LocalAvoidance/IntersectionPair>::Dispose()
extern "C"  void Enumerator_Dispose_m2041319291_gshared (Enumerator_t4209178241 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t4189505471 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m2041319291_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4209178241 * _thisAdjusted = reinterpret_cast<Enumerator_t4209178241 *>(__this + 1);
	Enumerator_Dispose_m2041319291(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.LocalAvoidance/IntersectionPair>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3238255034;
extern const uint32_t Enumerator_VerifyState_m117807412_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m117807412_gshared (Enumerator_t4209178241 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m117807412_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t4189505471 * L_0 = (List_1_t4189505471 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t4209178241  L_1 = (*(Enumerator_t4209178241 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t1794727681 * L_5 = (ObjectDisposedException_t1794727681 *)il2cpp_codegen_object_new(ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t4189505471 * L_7 = (List_1_t4189505471 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_9 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_9, (String_t*)_stringLiteral3238255034, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m117807412_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4209178241 * _thisAdjusted = reinterpret_cast<Enumerator_t4209178241 *>(__this + 1);
	Enumerator_VerifyState_m117807412(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<Pathfinding.LocalAvoidance/IntersectionPair>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m4050142184_gshared (Enumerator_t4209178241 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		((  void (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t4209178241 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t4189505471 * L_2 = (List_1_t4189505471 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t4189505471 * L_4 = (List_1_t4189505471 *)__this->get_l_0();
		NullCheck(L_4);
		IntersectionPairU5BU5D_t3912429174* L_5 = (IntersectionPairU5BU5D_t3912429174*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_8);
		int32_t L_9 = L_8;
		IntersectionPair_t2821319919  L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m4050142184_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4209178241 * _thisAdjusted = reinterpret_cast<Enumerator_t4209178241 *>(__this + 1);
	return Enumerator_MoveNext_m4050142184(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<Pathfinding.LocalAvoidance/IntersectionPair>::get_Current()
extern "C"  IntersectionPair_t2821319919  Enumerator_get_Current_m2534238443_gshared (Enumerator_t4209178241 * __this, const MethodInfo* method)
{
	{
		IntersectionPair_t2821319919  L_0 = (IntersectionPair_t2821319919 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  IntersectionPair_t2821319919  Enumerator_get_Current_m2534238443_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4209178241 * _thisAdjusted = reinterpret_cast<Enumerator_t4209178241 *>(__this + 1);
	return Enumerator_get_Current_m2534238443(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.LocalAvoidance/VOLine>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m3001993024_gshared (Enumerator_t3417790123 * __this, List_1_t3398117353 * ___l0, const MethodInfo* method)
{
	{
		List_1_t3398117353 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t3398117353 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m3001993024_AdjustorThunk (Il2CppObject * __this, List_1_t3398117353 * ___l0, const MethodInfo* method)
{
	Enumerator_t3417790123 * _thisAdjusted = reinterpret_cast<Enumerator_t3417790123 *>(__this + 1);
	Enumerator__ctor_m3001993024(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.LocalAvoidance/VOLine>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3694183698_gshared (Enumerator_t3417790123 * __this, const MethodInfo* method)
{
	{
		((  void (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t3417790123 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3694183698_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3417790123 * _thisAdjusted = reinterpret_cast<Enumerator_t3417790123 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m3694183698(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<Pathfinding.LocalAvoidance/VOLine>::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m1699818814_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1699818814_gshared (Enumerator_t3417790123 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m1699818814_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		((  void (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t3417790123 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m355676978(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		VOLine_t2029931801  L_2 = (VOLine_t2029931801 )__this->get_current_3();
		VOLine_t2029931801  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1699818814_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3417790123 * _thisAdjusted = reinterpret_cast<Enumerator_t3417790123 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m1699818814(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.LocalAvoidance/VOLine>::Dispose()
extern "C"  void Enumerator_Dispose_m627123493_gshared (Enumerator_t3417790123 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t3398117353 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m627123493_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3417790123 * _thisAdjusted = reinterpret_cast<Enumerator_t3417790123 *>(__this + 1);
	Enumerator_Dispose_m627123493(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.LocalAvoidance/VOLine>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3238255034;
extern const uint32_t Enumerator_VerifyState_m25414110_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m25414110_gshared (Enumerator_t3417790123 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m25414110_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t3398117353 * L_0 = (List_1_t3398117353 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t3417790123  L_1 = (*(Enumerator_t3417790123 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t1794727681 * L_5 = (ObjectDisposedException_t1794727681 *)il2cpp_codegen_object_new(ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t3398117353 * L_7 = (List_1_t3398117353 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_9 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_9, (String_t*)_stringLiteral3238255034, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m25414110_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3417790123 * _thisAdjusted = reinterpret_cast<Enumerator_t3417790123 *>(__this + 1);
	Enumerator_VerifyState_m25414110(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<Pathfinding.LocalAvoidance/VOLine>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m209975550_gshared (Enumerator_t3417790123 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		((  void (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t3417790123 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t3398117353 * L_2 = (List_1_t3398117353 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t3398117353 * L_4 = (List_1_t3398117353 *)__this->get_l_0();
		NullCheck(L_4);
		VOLineU5BU5D_t1958725220* L_5 = (VOLineU5BU5D_t1958725220*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_8);
		int32_t L_9 = L_8;
		VOLine_t2029931801  L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m209975550_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3417790123 * _thisAdjusted = reinterpret_cast<Enumerator_t3417790123 *>(__this + 1);
	return Enumerator_MoveNext_m209975550(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<Pathfinding.LocalAvoidance/VOLine>::get_Current()
extern "C"  VOLine_t2029931801  Enumerator_get_Current_m2648796245_gshared (Enumerator_t3417790123 * __this, const MethodInfo* method)
{
	{
		VOLine_t2029931801  L_0 = (VOLine_t2029931801 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  VOLine_t2029931801  Enumerator_get_Current_m2648796245_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3417790123 * _thisAdjusted = reinterpret_cast<Enumerator_t3417790123 *>(__this + 1);
	return Enumerator_get_Current_m2648796245(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.Voxels.ExtraMesh>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m295747321_gshared (Enumerator_t1310920741 * __this, List_1_t1291247971 * ___l0, const MethodInfo* method)
{
	{
		List_1_t1291247971 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t1291247971 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m295747321_AdjustorThunk (Il2CppObject * __this, List_1_t1291247971 * ___l0, const MethodInfo* method)
{
	Enumerator_t1310920741 * _thisAdjusted = reinterpret_cast<Enumerator_t1310920741 *>(__this + 1);
	Enumerator__ctor_m295747321(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.Voxels.ExtraMesh>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3283033529_gshared (Enumerator_t1310920741 * __this, const MethodInfo* method)
{
	{
		((  void (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t1310920741 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3283033529_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1310920741 * _thisAdjusted = reinterpret_cast<Enumerator_t1310920741 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m3283033529(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<Pathfinding.Voxels.ExtraMesh>::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m1915187823_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1915187823_gshared (Enumerator_t1310920741 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m1915187823_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		((  void (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t1310920741 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m355676978(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		ExtraMesh_t4218029715  L_2 = (ExtraMesh_t4218029715 )__this->get_current_3();
		ExtraMesh_t4218029715  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1915187823_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1310920741 * _thisAdjusted = reinterpret_cast<Enumerator_t1310920741 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m1915187823(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.Voxels.ExtraMesh>::Dispose()
extern "C"  void Enumerator_Dispose_m2393758494_gshared (Enumerator_t1310920741 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t1291247971 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m2393758494_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1310920741 * _thisAdjusted = reinterpret_cast<Enumerator_t1310920741 *>(__this + 1);
	Enumerator_Dispose_m2393758494(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.Voxels.ExtraMesh>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3238255034;
extern const uint32_t Enumerator_VerifyState_m3911375703_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m3911375703_gshared (Enumerator_t1310920741 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m3911375703_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t1291247971 * L_0 = (List_1_t1291247971 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t1310920741  L_1 = (*(Enumerator_t1310920741 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t1794727681 * L_5 = (ObjectDisposedException_t1794727681 *)il2cpp_codegen_object_new(ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t1291247971 * L_7 = (List_1_t1291247971 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_9 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_9, (String_t*)_stringLiteral3238255034, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m3911375703_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1310920741 * _thisAdjusted = reinterpret_cast<Enumerator_t1310920741 *>(__this + 1);
	Enumerator_VerifyState_m3911375703(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<Pathfinding.Voxels.ExtraMesh>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1938416617_gshared (Enumerator_t1310920741 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		((  void (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t1310920741 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t1291247971 * L_2 = (List_1_t1291247971 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t1291247971 * L_4 = (List_1_t1291247971 *)__this->get_l_0();
		NullCheck(L_4);
		ExtraMeshU5BU5D_t2875893186* L_5 = (ExtraMeshU5BU5D_t2875893186*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_8);
		int32_t L_9 = L_8;
		ExtraMesh_t4218029715  L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m1938416617_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1310920741 * _thisAdjusted = reinterpret_cast<Enumerator_t1310920741 *>(__this + 1);
	return Enumerator_MoveNext_m1938416617(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<Pathfinding.Voxels.ExtraMesh>::get_Current()
extern "C"  ExtraMesh_t4218029715  Enumerator_get_Current_m4221511024_gshared (Enumerator_t1310920741 * __this, const MethodInfo* method)
{
	{
		ExtraMesh_t4218029715  L_0 = (ExtraMesh_t4218029715 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  ExtraMesh_t4218029715  Enumerator_get_Current_m4221511024_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1310920741 * _thisAdjusted = reinterpret_cast<Enumerator_t1310920741 *>(__this + 1);
	return Enumerator_get_Current_m4221511024(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.Voxels.VoxelContour>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m2835219186_gshared (Enumerator_t690092042 * __this, List_1_t670419272 * ___l0, const MethodInfo* method)
{
	{
		List_1_t670419272 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t670419272 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m2835219186_AdjustorThunk (Il2CppObject * __this, List_1_t670419272 * ___l0, const MethodInfo* method)
{
	Enumerator_t690092042 * _thisAdjusted = reinterpret_cast<Enumerator_t690092042 *>(__this + 1);
	Enumerator__ctor_m2835219186(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.Voxels.VoxelContour>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2469626400_gshared (Enumerator_t690092042 * __this, const MethodInfo* method)
{
	{
		((  void (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t690092042 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2469626400_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t690092042 * _thisAdjusted = reinterpret_cast<Enumerator_t690092042 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m2469626400(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<Pathfinding.Voxels.VoxelContour>::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m4123448972_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m4123448972_gshared (Enumerator_t690092042 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m4123448972_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		((  void (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t690092042 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m355676978(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		VoxelContour_t3597201016  L_2 = (VoxelContour_t3597201016 )__this->get_current_3();
		VoxelContour_t3597201016  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m4123448972_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t690092042 * _thisAdjusted = reinterpret_cast<Enumerator_t690092042 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m4123448972(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.Voxels.VoxelContour>::Dispose()
extern "C"  void Enumerator_Dispose_m2584538455_gshared (Enumerator_t690092042 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t670419272 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m2584538455_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t690092042 * _thisAdjusted = reinterpret_cast<Enumerator_t690092042 *>(__this + 1);
	Enumerator_Dispose_m2584538455(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.Voxels.VoxelContour>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3238255034;
extern const uint32_t Enumerator_VerifyState_m768354576_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m768354576_gshared (Enumerator_t690092042 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m768354576_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t670419272 * L_0 = (List_1_t670419272 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t690092042  L_1 = (*(Enumerator_t690092042 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t1794727681 * L_5 = (ObjectDisposedException_t1794727681 *)il2cpp_codegen_object_new(ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t670419272 * L_7 = (List_1_t670419272 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_9 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_9, (String_t*)_stringLiteral3238255034, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m768354576_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t690092042 * _thisAdjusted = reinterpret_cast<Enumerator_t690092042 *>(__this + 1);
	Enumerator_VerifyState_m768354576(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<Pathfinding.Voxels.VoxelContour>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3624013452_gshared (Enumerator_t690092042 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		((  void (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t690092042 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t670419272 * L_2 = (List_1_t670419272 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t670419272 * L_4 = (List_1_t670419272 *)__this->get_l_0();
		NullCheck(L_4);
		VoxelContourU5BU5D_t3554406569* L_5 = (VoxelContourU5BU5D_t3554406569*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_8);
		int32_t L_9 = L_8;
		VoxelContour_t3597201016  L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m3624013452_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t690092042 * _thisAdjusted = reinterpret_cast<Enumerator_t690092042 *>(__this + 1);
	return Enumerator_MoveNext_m3624013452(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<Pathfinding.Voxels.VoxelContour>::get_Current()
extern "C"  VoxelContour_t3597201016  Enumerator_get_Current_m1550212935_gshared (Enumerator_t690092042 * __this, const MethodInfo* method)
{
	{
		VoxelContour_t3597201016  L_0 = (VoxelContour_t3597201016 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  VoxelContour_t3597201016  Enumerator_get_Current_m1550212935_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t690092042 * _thisAdjusted = reinterpret_cast<Enumerator_t690092042 *>(__this + 1);
	return Enumerator_get_Current_m1550212935(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<PointCloudRegognizer/Point>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m3135825689_gshared (Enumerator_t3226690072 * __this, List_1_t3207017302 * ___l0, const MethodInfo* method)
{
	{
		List_1_t3207017302 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t3207017302 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m3135825689_AdjustorThunk (Il2CppObject * __this, List_1_t3207017302 * ___l0, const MethodInfo* method)
{
	Enumerator_t3226690072 * _thisAdjusted = reinterpret_cast<Enumerator_t3226690072 *>(__this + 1);
	Enumerator__ctor_m3135825689(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<PointCloudRegognizer/Point>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2578866585_gshared (Enumerator_t3226690072 * __this, const MethodInfo* method)
{
	{
		((  void (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t3226690072 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2578866585_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3226690072 * _thisAdjusted = reinterpret_cast<Enumerator_t3226690072 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m2578866585(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<PointCloudRegognizer/Point>::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m3402409871_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3402409871_gshared (Enumerator_t3226690072 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m3402409871_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		((  void (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t3226690072 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m355676978(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		Point_t1838831750  L_2 = (Point_t1838831750 )__this->get_current_3();
		Point_t1838831750  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3402409871_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3226690072 * _thisAdjusted = reinterpret_cast<Enumerator_t3226690072 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m3402409871(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<PointCloudRegognizer/Point>::Dispose()
extern "C"  void Enumerator_Dispose_m132778814_gshared (Enumerator_t3226690072 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t3207017302 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m132778814_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3226690072 * _thisAdjusted = reinterpret_cast<Enumerator_t3226690072 *>(__this + 1);
	Enumerator_Dispose_m132778814(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<PointCloudRegognizer/Point>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3238255034;
extern const uint32_t Enumerator_VerifyState_m176814967_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m176814967_gshared (Enumerator_t3226690072 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m176814967_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t3207017302 * L_0 = (List_1_t3207017302 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t3226690072  L_1 = (*(Enumerator_t3226690072 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t1794727681 * L_5 = (ObjectDisposedException_t1794727681 *)il2cpp_codegen_object_new(ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t3207017302 * L_7 = (List_1_t3207017302 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_9 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_9, (String_t*)_stringLiteral3238255034, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m176814967_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3226690072 * _thisAdjusted = reinterpret_cast<Enumerator_t3226690072 *>(__this + 1);
	Enumerator_VerifyState_m176814967(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<PointCloudRegognizer/Point>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3403296841_gshared (Enumerator_t3226690072 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		((  void (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t3226690072 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t3207017302 * L_2 = (List_1_t3207017302 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t3207017302 * L_4 = (List_1_t3207017302 *)__this->get_l_0();
		NullCheck(L_4);
		PointU5BU5D_t2313848483* L_5 = (PointU5BU5D_t2313848483*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_8);
		int32_t L_9 = L_8;
		Point_t1838831750  L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m3403296841_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3226690072 * _thisAdjusted = reinterpret_cast<Enumerator_t3226690072 *>(__this + 1);
	return Enumerator_MoveNext_m3403296841(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<PointCloudRegognizer/Point>::get_Current()
extern "C"  Point_t1838831750  Enumerator_get_Current_m1890343376_gshared (Enumerator_t3226690072 * __this, const MethodInfo* method)
{
	{
		Point_t1838831750  L_0 = (Point_t1838831750 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  Point_t1838831750  Enumerator_get_Current_m1890343376_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3226690072 * _thisAdjusted = reinterpret_cast<Enumerator_t3226690072 *>(__this + 1);
	return Enumerator_get_Current_m1890343376(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<PushType>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m2728911143_gshared (Enumerator_t3228500774 * __this, List_1_t3208828004 * ___l0, const MethodInfo* method)
{
	{
		List_1_t3208828004 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t3208828004 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m2728911143_AdjustorThunk (Il2CppObject * __this, List_1_t3208828004 * ___l0, const MethodInfo* method)
{
	Enumerator_t3228500774 * _thisAdjusted = reinterpret_cast<Enumerator_t3228500774 *>(__this + 1);
	Enumerator__ctor_m2728911143(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<PushType>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m986371787_gshared (Enumerator_t3228500774 * __this, const MethodInfo* method)
{
	{
		((  void (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t3228500774 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m986371787_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3228500774 * _thisAdjusted = reinterpret_cast<Enumerator_t3228500774 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m986371787(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<PushType>::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m3173458561_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3173458561_gshared (Enumerator_t3228500774 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m3173458561_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		((  void (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t3228500774 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m355676978(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_current_3();
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3173458561_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3228500774 * _thisAdjusted = reinterpret_cast<Enumerator_t3228500774 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m3173458561(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<PushType>::Dispose()
extern "C"  void Enumerator_Dispose_m3030771916_gshared (Enumerator_t3228500774 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t3208828004 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m3030771916_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3228500774 * _thisAdjusted = reinterpret_cast<Enumerator_t3228500774 *>(__this + 1);
	Enumerator_Dispose_m3030771916(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<PushType>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3238255034;
extern const uint32_t Enumerator_VerifyState_m333472261_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m333472261_gshared (Enumerator_t3228500774 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m333472261_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t3208828004 * L_0 = (List_1_t3208828004 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t3228500774  L_1 = (*(Enumerator_t3228500774 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t1794727681 * L_5 = (ObjectDisposedException_t1794727681 *)il2cpp_codegen_object_new(ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t3208828004 * L_7 = (List_1_t3208828004 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_9 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_9, (String_t*)_stringLiteral3238255034, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m333472261_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3228500774 * _thisAdjusted = reinterpret_cast<Enumerator_t3228500774 *>(__this + 1);
	Enumerator_VerifyState_m333472261(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<PushType>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2391662843_gshared (Enumerator_t3228500774 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		((  void (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t3228500774 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t3208828004 * L_2 = (List_1_t3208828004 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t3208828004 * L_4 = (List_1_t3208828004 *)__this->get_l_0();
		NullCheck(L_4);
		PushTypeU5BU5D_t3978570141* L_5 = (PushTypeU5BU5D_t3978570141*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_8);
		int32_t L_9 = L_8;
		int32_t L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m2391662843_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3228500774 * _thisAdjusted = reinterpret_cast<Enumerator_t3228500774 *>(__this + 1);
	return Enumerator_MoveNext_m2391662843(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<PushType>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m2857649822_gshared (Enumerator_t3228500774 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_current_3();
		return L_0;
	}
}
extern "C"  int32_t Enumerator_get_Current_m2857649822_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3228500774 * _thisAdjusted = reinterpret_cast<Enumerator_t3228500774 *>(__this + 1);
	return Enumerator_get_Current_m2857649822(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Boolean>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m1283739068_gshared (Enumerator_t1864657040 * __this, List_1_t1844984270 * ___l0, const MethodInfo* method)
{
	{
		List_1_t1844984270 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t1844984270 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m1283739068_AdjustorThunk (Il2CppObject * __this, List_1_t1844984270 * ___l0, const MethodInfo* method)
{
	Enumerator_t1864657040 * _thisAdjusted = reinterpret_cast<Enumerator_t1864657040 *>(__this + 1);
	Enumerator__ctor_m1283739068(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Boolean>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m430448918_gshared (Enumerator_t1864657040 * __this, const MethodInfo* method)
{
	{
		((  void (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t1864657040 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m430448918_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1864657040 * _thisAdjusted = reinterpret_cast<Enumerator_t1864657040 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m430448918(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<System.Boolean>::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m721140940_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m721140940_gshared (Enumerator_t1864657040 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m721140940_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		((  void (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t1864657040 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m355676978(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		bool L_2 = (bool)__this->get_current_3();
		bool L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m721140940_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1864657040 * _thisAdjusted = reinterpret_cast<Enumerator_t1864657040 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m721140940(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Boolean>::Dispose()
extern "C"  void Enumerator_Dispose_m2662533793_gshared (Enumerator_t1864657040 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t1844984270 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m2662533793_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1864657040 * _thisAdjusted = reinterpret_cast<Enumerator_t1864657040 *>(__this + 1);
	Enumerator_Dispose_m2662533793(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Boolean>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3238255034;
extern const uint32_t Enumerator_VerifyState_m204378458_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m204378458_gshared (Enumerator_t1864657040 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m204378458_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t1844984270 * L_0 = (List_1_t1844984270 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t1864657040  L_1 = (*(Enumerator_t1864657040 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t1794727681 * L_5 = (ObjectDisposedException_t1794727681 *)il2cpp_codegen_object_new(ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t1844984270 * L_7 = (List_1_t1844984270 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_9 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_9, (String_t*)_stringLiteral3238255034, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m204378458_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1864657040 * _thisAdjusted = reinterpret_cast<Enumerator_t1864657040 *>(__this + 1);
	Enumerator_VerifyState_m204378458(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Boolean>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m4018573382_gshared (Enumerator_t1864657040 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		((  void (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t1864657040 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t1844984270 * L_2 = (List_1_t1844984270 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t1844984270 * L_4 = (List_1_t1844984270 *)__this->get_l_0();
		NullCheck(L_4);
		BooleanU5BU5D_t3456302923* L_5 = (BooleanU5BU5D_t3456302923*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_8);
		int32_t L_9 = L_8;
		bool L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m4018573382_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1864657040 * _thisAdjusted = reinterpret_cast<Enumerator_t1864657040 *>(__this + 1);
	return Enumerator_MoveNext_m4018573382(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<System.Boolean>::get_Current()
extern "C"  bool Enumerator_get_Current_m2589636915_gshared (Enumerator_t1864657040 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_current_3();
		return L_0;
	}
}
extern "C"  bool Enumerator_get_Current_m2589636915_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1864657040 * _thisAdjusted = reinterpret_cast<Enumerator_t1864657040 *>(__this + 1);
	return Enumerator_get_Current_m2589636915(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Byte>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m2573176462_gshared (Enumerator_t4250467982 * __this, List_1_t4230795212 * ___l0, const MethodInfo* method)
{
	{
		List_1_t4230795212 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t4230795212 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m2573176462_AdjustorThunk (Il2CppObject * __this, List_1_t4230795212 * ___l0, const MethodInfo* method)
{
	Enumerator_t4250467982 * _thisAdjusted = reinterpret_cast<Enumerator_t4250467982 *>(__this + 1);
	Enumerator__ctor_m2573176462(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Byte>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m595076356_gshared (Enumerator_t4250467982 * __this, const MethodInfo* method)
{
	{
		((  void (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t4250467982 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m595076356_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4250467982 * _thisAdjusted = reinterpret_cast<Enumerator_t4250467982 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m595076356(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<System.Byte>::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m722657776_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m722657776_gshared (Enumerator_t4250467982 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m722657776_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		((  void (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t4250467982 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m355676978(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		uint8_t L_2 = (uint8_t)__this->get_current_3();
		uint8_t L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m722657776_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4250467982 * _thisAdjusted = reinterpret_cast<Enumerator_t4250467982 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m722657776(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Byte>::Dispose()
extern "C"  void Enumerator_Dispose_m254156787_gshared (Enumerator_t4250467982 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t4230795212 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m254156787_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4250467982 * _thisAdjusted = reinterpret_cast<Enumerator_t4250467982 *>(__this + 1);
	Enumerator_Dispose_m254156787(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Byte>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3238255034;
extern const uint32_t Enumerator_VerifyState_m932359596_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m932359596_gshared (Enumerator_t4250467982 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m932359596_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t4230795212 * L_0 = (List_1_t4230795212 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t4250467982  L_1 = (*(Enumerator_t4250467982 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t1794727681 * L_5 = (ObjectDisposedException_t1794727681 *)il2cpp_codegen_object_new(ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t4230795212 * L_7 = (List_1_t4230795212 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_9 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_9, (String_t*)_stringLiteral3238255034, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m932359596_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4250467982 * _thisAdjusted = reinterpret_cast<Enumerator_t4250467982 *>(__this + 1);
	Enumerator_VerifyState_m932359596(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Byte>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2960702064_gshared (Enumerator_t4250467982 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		((  void (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t4250467982 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t4230795212 * L_2 = (List_1_t4230795212 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t4230795212 * L_4 = (List_1_t4230795212 *)__this->get_l_0();
		NullCheck(L_4);
		ByteU5BU5D_t4260760469* L_5 = (ByteU5BU5D_t4260760469*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_8);
		int32_t L_9 = L_8;
		uint8_t L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m2960702064_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4250467982 * _thisAdjusted = reinterpret_cast<Enumerator_t4250467982 *>(__this + 1);
	return Enumerator_MoveNext_m2960702064(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<System.Byte>::get_Current()
extern "C"  uint8_t Enumerator_get_Current_m2433825123_gshared (Enumerator_t4250467982 * __this, const MethodInfo* method)
{
	{
		uint8_t L_0 = (uint8_t)__this->get_current_3();
		return L_0;
	}
}
extern "C"  uint8_t Enumerator_get_Current_m2433825123_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4250467982 * _thisAdjusted = reinterpret_cast<Enumerator_t4250467982 *>(__this + 1);
	return Enumerator_get_Current_m2433825123(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m2249551728_gshared (Enumerator_t3332527299 * __this, List_1_t3312854530 * ___l0, const MethodInfo* method)
{
	{
		List_1_t3312854530 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t3312854530 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m2249551728_AdjustorThunk (Il2CppObject * __this, List_1_t3312854530 * ___l0, const MethodInfo* method)
{
	Enumerator_t3332527299 * _thisAdjusted = reinterpret_cast<Enumerator_t3332527299 *>(__this + 1);
	Enumerator__ctor_m2249551728(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1882046178_gshared (Enumerator_t3332527299 * __this, const MethodInfo* method)
{
	{
		((  void (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t3332527299 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1882046178_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3332527299 * _thisAdjusted = reinterpret_cast<Enumerator_t3332527299 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m1882046178(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m602451608_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m602451608_gshared (Enumerator_t3332527299 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m602451608_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		((  void (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t3332527299 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m355676978(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		KeyValuePair_2_t1944668977  L_2 = (KeyValuePair_2_t1944668977 )__this->get_current_3();
		KeyValuePair_2_t1944668977  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m602451608_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3332527299 * _thisAdjusted = reinterpret_cast<Enumerator_t3332527299 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m602451608(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Dispose()
extern "C"  void Enumerator_Dispose_m3838892373_gshared (Enumerator_t3332527299 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t3312854530 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m3838892373_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3332527299 * _thisAdjusted = reinterpret_cast<Enumerator_t3332527299 *>(__this + 1);
	Enumerator_Dispose_m3838892373(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3238255034;
extern const uint32_t Enumerator_VerifyState_m1553851918_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m1553851918_gshared (Enumerator_t3332527299 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m1553851918_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t3312854530 * L_0 = (List_1_t3312854530 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t3332527299  L_1 = (*(Enumerator_t3332527299 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t1794727681 * L_5 = (ObjectDisposedException_t1794727681 *)il2cpp_codegen_object_new(ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t3312854530 * L_7 = (List_1_t3312854530 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_9 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_9, (String_t*)_stringLiteral3238255034, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m1553851918_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3332527299 * _thisAdjusted = reinterpret_cast<Enumerator_t3332527299 *>(__this + 1);
	Enumerator_VerifyState_m1553851918(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1524607506_gshared (Enumerator_t3332527299 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		((  void (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t3332527299 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t3312854530 * L_2 = (List_1_t3312854530 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t3312854530 * L_4 = (List_1_t3312854530 *)__this->get_l_0();
		NullCheck(L_4);
		KeyValuePair_2U5BU5D_t2483180780* L_5 = (KeyValuePair_2U5BU5D_t2483180780*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_8);
		int32_t L_9 = L_8;
		KeyValuePair_2_t1944668977  L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m1524607506_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3332527299 * _thisAdjusted = reinterpret_cast<Enumerator_t3332527299 *>(__this + 1);
	return Enumerator_MoveNext_m1524607506(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Current()
extern "C"  KeyValuePair_2_t1944668977  Enumerator_get_Current_m3227842279_gshared (Enumerator_t3332527299 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t1944668977  L_0 = (KeyValuePair_2_t1944668977 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  KeyValuePair_2_t1944668977  Enumerator_get_Current_m3227842279_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3332527299 * _thisAdjusted = reinterpret_cast<Enumerator_t3332527299 *>(__this + 1);
	return Enumerator_get_Current_m3227842279(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Int32>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m1242988386_gshared (Enumerator_t2541696822 * __this, List_1_t2522024052 * ___l0, const MethodInfo* method)
{
	{
		List_1_t2522024052 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t2522024052 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m1242988386_AdjustorThunk (Il2CppObject * __this, List_1_t2522024052 * ___l0, const MethodInfo* method)
{
	Enumerator_t2541696822 * _thisAdjusted = reinterpret_cast<Enumerator_t2541696822 *>(__this + 1);
	Enumerator__ctor_m1242988386(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Int32>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1509621680_gshared (Enumerator_t2541696822 * __this, const MethodInfo* method)
{
	{
		((  void (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t2541696822 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1509621680_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2541696822 * _thisAdjusted = reinterpret_cast<Enumerator_t2541696822 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m1509621680(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<System.Int32>::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m262228262_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m262228262_gshared (Enumerator_t2541696822 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m262228262_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		((  void (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t2541696822 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m355676978(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_current_3();
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m262228262_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2541696822 * _thisAdjusted = reinterpret_cast<Enumerator_t2541696822 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m262228262(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Int32>::Dispose()
extern "C"  void Enumerator_Dispose_m3304555975_gshared (Enumerator_t2541696822 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t2522024052 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m3304555975_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2541696822 * _thisAdjusted = reinterpret_cast<Enumerator_t2541696822 *>(__this + 1);
	Enumerator_Dispose_m3304555975(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Int32>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3238255034;
extern const uint32_t Enumerator_VerifyState_m936708480_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m936708480_gshared (Enumerator_t2541696822 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m936708480_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t2522024052 * L_0 = (List_1_t2522024052 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t2541696822  L_1 = (*(Enumerator_t2541696822 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t1794727681 * L_5 = (ObjectDisposedException_t1794727681 *)il2cpp_codegen_object_new(ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t2522024052 * L_7 = (List_1_t2522024052 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_9 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_9, (String_t*)_stringLiteral3238255034, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m936708480_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2541696822 * _thisAdjusted = reinterpret_cast<Enumerator_t2541696822 *>(__this + 1);
	Enumerator_VerifyState_m936708480(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Int32>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3459783625_gshared (Enumerator_t2541696822 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		((  void (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t2541696822 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t2522024052 * L_2 = (List_1_t2522024052 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t2522024052 * L_4 = (List_1_t2522024052 *)__this->get_l_0();
		NullCheck(L_4);
		Int32U5BU5D_t3230847821* L_5 = (Int32U5BU5D_t3230847821*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_8);
		int32_t L_9 = L_8;
		int32_t L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m3459783625_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2541696822 * _thisAdjusted = reinterpret_cast<Enumerator_t2541696822 *>(__this + 1);
	return Enumerator_MoveNext_m3459783625(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<System.Int32>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m1836990579_gshared (Enumerator_t2541696822 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_current_3();
		return L_0;
	}
}
extern "C"  int32_t Enumerator_get_Current_m1836990579_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2541696822 * _thisAdjusted = reinterpret_cast<Enumerator_t2541696822 *>(__this + 1);
	return Enumerator_get_Current_m1836990579(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m1029849669_gshared (Enumerator_t1263707397 * __this, List_1_t1244034627 * ___l0, const MethodInfo* method)
{
	{
		List_1_t1244034627 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t1244034627 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m1029849669_AdjustorThunk (Il2CppObject * __this, List_1_t1244034627 * ___l0, const MethodInfo* method)
{
	Enumerator_t1263707397 * _thisAdjusted = reinterpret_cast<Enumerator_t1263707397 *>(__this + 1);
	Enumerator__ctor_m1029849669(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m771996397_gshared (Enumerator_t1263707397 * __this, const MethodInfo* method)
{
	{
		((  void (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t1263707397 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m771996397_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1263707397 * _thisAdjusted = reinterpret_cast<Enumerator_t1263707397 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m771996397(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<System.Object>::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_gshared (Enumerator_t1263707397 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		((  void (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t1263707397 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m355676978(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		Il2CppObject * L_2 = (Il2CppObject *)__this->get_current_3();
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1263707397 * _thisAdjusted = reinterpret_cast<Enumerator_t1263707397 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m3561903705(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m2904289642_gshared (Enumerator_t1263707397 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t1244034627 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m2904289642_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1263707397 * _thisAdjusted = reinterpret_cast<Enumerator_t1263707397 *>(__this + 1);
	Enumerator_Dispose_m2904289642(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3238255034;
extern const uint32_t Enumerator_VerifyState_m1522854819_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m1522854819_gshared (Enumerator_t1263707397 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m1522854819_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t1244034627 * L_0 = (List_1_t1244034627 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t1263707397  L_1 = (*(Enumerator_t1263707397 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t1794727681 * L_5 = (ObjectDisposedException_t1794727681 *)il2cpp_codegen_object_new(ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t1244034627 * L_7 = (List_1_t1244034627 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_9 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_9, (String_t*)_stringLiteral3238255034, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m1522854819_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1263707397 * _thisAdjusted = reinterpret_cast<Enumerator_t1263707397 *>(__this + 1);
	Enumerator_VerifyState_m1522854819(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m4284703760_gshared (Enumerator_t1263707397 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		((  void (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t1263707397 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t1244034627 * L_2 = (List_1_t1244034627 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t1244034627 * L_4 = (List_1_t1244034627 *)__this->get_l_0();
		NullCheck(L_4);
		ObjectU5BU5D_t1108656482* L_5 = (ObjectU5BU5D_t1108656482*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_8);
		int32_t L_9 = L_8;
		Il2CppObject * L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m4284703760_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1263707397 * _thisAdjusted = reinterpret_cast<Enumerator_t1263707397 *>(__this + 1);
	return Enumerator_MoveNext_m4284703760(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<System.Object>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m396252160_gshared (Enumerator_t1263707397 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_current_3();
		return L_0;
	}
}
extern "C"  Il2CppObject * Enumerator_get_Current_m396252160_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1263707397 * _thisAdjusted = reinterpret_cast<Enumerator_t1263707397 *>(__this + 1);
	return Enumerator_get_Current_m396252160(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m256124610_gshared (Enumerator_t152504015 * __this, List_1_t132831245 * ___l0, const MethodInfo* method)
{
	{
		List_1_t132831245 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t132831245 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m256124610_AdjustorThunk (Il2CppObject * __this, List_1_t132831245 * ___l0, const MethodInfo* method)
{
	Enumerator_t152504015 * _thisAdjusted = reinterpret_cast<Enumerator_t152504015 *>(__this + 1);
	Enumerator__ctor_m256124610(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m4026154064_gshared (Enumerator_t152504015 * __this, const MethodInfo* method)
{
	{
		((  void (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t152504015 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m4026154064_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t152504015 * _thisAdjusted = reinterpret_cast<Enumerator_t152504015 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m4026154064(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m143716486_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m143716486_gshared (Enumerator_t152504015 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m143716486_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		((  void (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t152504015 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m355676978(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		CustomAttributeNamedArgument_t3059612989  L_2 = (CustomAttributeNamedArgument_t3059612989 )__this->get_current_3();
		CustomAttributeNamedArgument_t3059612989  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m143716486_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t152504015 * _thisAdjusted = reinterpret_cast<Enumerator_t152504015 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m143716486(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::Dispose()
extern "C"  void Enumerator_Dispose_m855442727_gshared (Enumerator_t152504015 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t132831245 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m855442727_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t152504015 * _thisAdjusted = reinterpret_cast<Enumerator_t152504015 *>(__this + 1);
	Enumerator_Dispose_m855442727(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3238255034;
extern const uint32_t Enumerator_VerifyState_m508287200_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m508287200_gshared (Enumerator_t152504015 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m508287200_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t132831245 * L_0 = (List_1_t132831245 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t152504015  L_1 = (*(Enumerator_t152504015 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t1794727681 * L_5 = (ObjectDisposedException_t1794727681 *)il2cpp_codegen_object_new(ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t132831245 * L_7 = (List_1_t132831245 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_9 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_9, (String_t*)_stringLiteral3238255034, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m508287200_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t152504015 * _thisAdjusted = reinterpret_cast<Enumerator_t152504015 *>(__this + 1);
	Enumerator_VerifyState_m508287200(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3090636416_gshared (Enumerator_t152504015 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		((  void (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t152504015 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t132831245 * L_2 = (List_1_t132831245 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t132831245 * L_4 = (List_1_t132831245 *)__this->get_l_0();
		NullCheck(L_4);
		CustomAttributeNamedArgumentU5BU5D_t1983528240* L_5 = (CustomAttributeNamedArgumentU5BU5D_t1983528240*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_8);
		int32_t L_9 = L_8;
		CustomAttributeNamedArgument_t3059612989  L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m3090636416_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t152504015 * _thisAdjusted = reinterpret_cast<Enumerator_t152504015 *>(__this + 1);
	return Enumerator_MoveNext_m3090636416(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::get_Current()
extern "C"  CustomAttributeNamedArgument_t3059612989  Enumerator_get_Current_m473447609_gshared (Enumerator_t152504015 * __this, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgument_t3059612989  L_0 = (CustomAttributeNamedArgument_t3059612989 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  CustomAttributeNamedArgument_t3059612989  Enumerator_get_Current_m473447609_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t152504015 * _thisAdjusted = reinterpret_cast<Enumerator_t152504015 *>(__this + 1);
	return Enumerator_get_Current_m473447609(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m444414259_gshared (Enumerator_t394184448 * __this, List_1_t374511678 * ___l0, const MethodInfo* method)
{
	{
		List_1_t374511678 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t374511678 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m444414259_AdjustorThunk (Il2CppObject * __this, List_1_t374511678 * ___l0, const MethodInfo* method)
{
	Enumerator_t394184448 * _thisAdjusted = reinterpret_cast<Enumerator_t394184448 *>(__this + 1);
	Enumerator__ctor_m444414259(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1403627327_gshared (Enumerator_t394184448 * __this, const MethodInfo* method)
{
	{
		((  void (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t394184448 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1403627327_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t394184448 * _thisAdjusted = reinterpret_cast<Enumerator_t394184448 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m1403627327(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m1685728309_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1685728309_gshared (Enumerator_t394184448 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m1685728309_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		((  void (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t394184448 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m355676978(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		CustomAttributeTypedArgument_t3301293422  L_2 = (CustomAttributeTypedArgument_t3301293422 )__this->get_current_3();
		CustomAttributeTypedArgument_t3301293422  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1685728309_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t394184448 * _thisAdjusted = reinterpret_cast<Enumerator_t394184448 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m1685728309(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::Dispose()
extern "C"  void Enumerator_Dispose_m3403219928_gshared (Enumerator_t394184448 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t374511678 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m3403219928_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t394184448 * _thisAdjusted = reinterpret_cast<Enumerator_t394184448 *>(__this + 1);
	Enumerator_Dispose_m3403219928(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3238255034;
extern const uint32_t Enumerator_VerifyState_m1438062353_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m1438062353_gshared (Enumerator_t394184448 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m1438062353_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t374511678 * L_0 = (List_1_t374511678 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t394184448  L_1 = (*(Enumerator_t394184448 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t1794727681 * L_5 = (ObjectDisposedException_t1794727681 *)il2cpp_codegen_object_new(ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t374511678 * L_7 = (List_1_t374511678 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_9 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_9, (String_t*)_stringLiteral3238255034, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m1438062353_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t394184448 * _thisAdjusted = reinterpret_cast<Enumerator_t394184448 *>(__this + 1);
	Enumerator_VerifyState_m1438062353(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m467351023_gshared (Enumerator_t394184448 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		((  void (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t394184448 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t374511678 * L_2 = (List_1_t374511678 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t374511678 * L_4 = (List_1_t374511678 *)__this->get_l_0();
		NullCheck(L_4);
		CustomAttributeTypedArgumentU5BU5D_t2088020251* L_5 = (CustomAttributeTypedArgumentU5BU5D_t2088020251*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_8);
		int32_t L_9 = L_8;
		CustomAttributeTypedArgument_t3301293422  L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m467351023_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t394184448 * _thisAdjusted = reinterpret_cast<Enumerator_t394184448 *>(__this + 1);
	return Enumerator_MoveNext_m467351023(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::get_Current()
extern "C"  CustomAttributeTypedArgument_t3301293422  Enumerator_get_Current_m1403222762_gshared (Enumerator_t394184448 * __this, const MethodInfo* method)
{
	{
		CustomAttributeTypedArgument_t3301293422  L_0 = (CustomAttributeTypedArgument_t3301293422 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  CustomAttributeTypedArgument_t3301293422  Enumerator_get_Current_m1403222762_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t394184448 * _thisAdjusted = reinterpret_cast<Enumerator_t394184448 *>(__this + 1);
	return Enumerator_get_Current_m1403222762(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Single>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m3108223886_gshared (Enumerator_t1384809998 * __this, List_1_t1365137228 * ___l0, const MethodInfo* method)
{
	{
		List_1_t1365137228 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t1365137228 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m3108223886_AdjustorThunk (Il2CppObject * __this, List_1_t1365137228 * ___l0, const MethodInfo* method)
{
	Enumerator_t1384809998 * _thisAdjusted = reinterpret_cast<Enumerator_t1384809998 *>(__this + 1);
	Enumerator__ctor_m3108223886(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Single>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2800474116_gshared (Enumerator_t1384809998 * __this, const MethodInfo* method)
{
	{
		((  void (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t1384809998 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2800474116_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1384809998 * _thisAdjusted = reinterpret_cast<Enumerator_t1384809998 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m2800474116(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<System.Single>::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m3566994992_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3566994992_gshared (Enumerator_t1384809998 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m3566994992_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		((  void (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t1384809998 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m355676978(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		float L_2 = (float)__this->get_current_3();
		float L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3566994992_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1384809998 * _thisAdjusted = reinterpret_cast<Enumerator_t1384809998 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m3566994992(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Single>::Dispose()
extern "C"  void Enumerator_Dispose_m623217907_gshared (Enumerator_t1384809998 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t1365137228 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m623217907_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1384809998 * _thisAdjusted = reinterpret_cast<Enumerator_t1384809998 *>(__this + 1);
	Enumerator_Dispose_m623217907(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Single>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3238255034;
extern const uint32_t Enumerator_VerifyState_m907254444_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m907254444_gshared (Enumerator_t1384809998 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m907254444_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t1365137228 * L_0 = (List_1_t1365137228 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t1384809998  L_1 = (*(Enumerator_t1384809998 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t1794727681 * L_5 = (ObjectDisposedException_t1794727681 *)il2cpp_codegen_object_new(ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t1365137228 * L_7 = (List_1_t1365137228 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_9 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_9, (String_t*)_stringLiteral3238255034, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m907254444_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1384809998 * _thisAdjusted = reinterpret_cast<Enumerator_t1384809998 *>(__this + 1);
	Enumerator_VerifyState_m907254444(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Single>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3145684464_gshared (Enumerator_t1384809998 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		((  void (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t1384809998 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t1365137228 * L_2 = (List_1_t1365137228 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t1365137228 * L_4 = (List_1_t1365137228 *)__this->get_l_0();
		NullCheck(L_4);
		SingleU5BU5D_t2316563989* L_5 = (SingleU5BU5D_t2316563989*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_8);
		int32_t L_9 = L_8;
		float L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m3145684464_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1384809998 * _thisAdjusted = reinterpret_cast<Enumerator_t1384809998 *>(__this + 1);
	return Enumerator_MoveNext_m3145684464(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<System.Single>::get_Current()
extern "C"  float Enumerator_get_Current_m3583390371_gshared (Enumerator_t1384809998 * __this, const MethodInfo* method)
{
	{
		float L_0 = (float)__this->get_current_3();
		return L_0;
	}
}
extern "C"  float Enumerator_get_Current_m3583390371_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1384809998 * _thisAdjusted = reinterpret_cast<Enumerator_t1384809998 *>(__this + 1);
	return Enumerator_get_Current_m3583390371(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.UInt32>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m1478123679_gshared (Enumerator_t1412526303 * __this, List_1_t1392853533 * ___l0, const MethodInfo* method)
{
	{
		List_1_t1392853533 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t1392853533 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m1478123679_AdjustorThunk (Il2CppObject * __this, List_1_t1392853533 * ___l0, const MethodInfo* method)
{
	Enumerator_t1412526303 * _thisAdjusted = reinterpret_cast<Enumerator_t1412526303 *>(__this + 1);
	Enumerator__ctor_m1478123679(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.UInt32>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3291778131_gshared (Enumerator_t1412526303 * __this, const MethodInfo* method)
{
	{
		((  void (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t1412526303 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3291778131_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1412526303 * _thisAdjusted = reinterpret_cast<Enumerator_t1412526303 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m3291778131(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<System.UInt32>::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m278528831_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m278528831_gshared (Enumerator_t1412526303 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m278528831_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		((  void (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t1412526303 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m355676978(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		uint32_t L_2 = (uint32_t)__this->get_current_3();
		uint32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m278528831_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1412526303 * _thisAdjusted = reinterpret_cast<Enumerator_t1412526303 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m278528831(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.UInt32>::Dispose()
extern "C"  void Enumerator_Dispose_m1521862212_gshared (Enumerator_t1412526303 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t1392853533 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m1521862212_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1412526303 * _thisAdjusted = reinterpret_cast<Enumerator_t1412526303 *>(__this + 1);
	Enumerator_Dispose_m1521862212(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.UInt32>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3238255034;
extern const uint32_t Enumerator_VerifyState_m1263846269_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m1263846269_gshared (Enumerator_t1412526303 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m1263846269_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t1392853533 * L_0 = (List_1_t1392853533 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t1412526303  L_1 = (*(Enumerator_t1412526303 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t1794727681 * L_5 = (ObjectDisposedException_t1794727681 *)il2cpp_codegen_object_new(ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t1392853533 * L_7 = (List_1_t1392853533 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_9 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_9, (String_t*)_stringLiteral3238255034, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m1263846269_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1412526303 * _thisAdjusted = reinterpret_cast<Enumerator_t1412526303 *>(__this + 1);
	Enumerator_VerifyState_m1263846269(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.UInt32>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m938886847_gshared (Enumerator_t1412526303 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		((  void (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t1412526303 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t1392853533 * L_2 = (List_1_t1392853533 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t1392853533 * L_4 = (List_1_t1392853533 *)__this->get_l_0();
		NullCheck(L_4);
		UInt32U5BU5D_t3230734560* L_5 = (UInt32U5BU5D_t3230734560*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_8);
		int32_t L_9 = L_8;
		uint32_t L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m938886847_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1412526303 * _thisAdjusted = reinterpret_cast<Enumerator_t1412526303 *>(__this + 1);
	return Enumerator_MoveNext_m938886847(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<System.UInt32>::get_Current()
extern "C"  uint32_t Enumerator_get_Current_m3939982196_gshared (Enumerator_t1412526303 * __this, const MethodInfo* method)
{
	{
		uint32_t L_0 = (uint32_t)__this->get_current_3();
		return L_0;
	}
}
extern "C"  uint32_t Enumerator_get_Current_m3939982196_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1412526303 * _thisAdjusted = reinterpret_cast<Enumerator_t1412526303 *>(__this + 1);
	return Enumerator_get_Current_m3939982196(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Bounds>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m3309784255_gshared (Enumerator_t4099500171 * __this, List_1_t4079827401 * ___l0, const MethodInfo* method)
{
	{
		List_1_t4079827401 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t4079827401 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m3309784255_AdjustorThunk (Il2CppObject * __this, List_1_t4079827401 * ___l0, const MethodInfo* method)
{
	Enumerator_t4099500171 * _thisAdjusted = reinterpret_cast<Enumerator_t4099500171 *>(__this + 1);
	Enumerator__ctor_m3309784255(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Bounds>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3815541811_gshared (Enumerator_t4099500171 * __this, const MethodInfo* method)
{
	{
		((  void (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t4099500171 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3815541811_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4099500171 * _thisAdjusted = reinterpret_cast<Enumerator_t4099500171 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m3815541811(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.Bounds>::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m1935244201_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1935244201_gshared (Enumerator_t4099500171 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m1935244201_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		((  void (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t4099500171 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m355676978(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		Bounds_t2711641849  L_2 = (Bounds_t2711641849 )__this->get_current_3();
		Bounds_t2711641849  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1935244201_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4099500171 * _thisAdjusted = reinterpret_cast<Enumerator_t4099500171 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m1935244201(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Bounds>::Dispose()
extern "C"  void Enumerator_Dispose_m2051521124_gshared (Enumerator_t4099500171 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t4079827401 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m2051521124_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4099500171 * _thisAdjusted = reinterpret_cast<Enumerator_t4099500171 *>(__this + 1);
	Enumerator_Dispose_m2051521124(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Bounds>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3238255034;
extern const uint32_t Enumerator_VerifyState_m2861541277_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m2861541277_gshared (Enumerator_t4099500171 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m2861541277_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t4079827401 * L_0 = (List_1_t4079827401 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t4099500171  L_1 = (*(Enumerator_t4099500171 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t1794727681 * L_5 = (ObjectDisposedException_t1794727681 *)il2cpp_codegen_object_new(ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t4079827401 * L_7 = (List_1_t4079827401 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_9 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_9, (String_t*)_stringLiteral3238255034, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m2861541277_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4099500171 * _thisAdjusted = reinterpret_cast<Enumerator_t4099500171 *>(__this + 1);
	Enumerator_VerifyState_m2861541277(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Bounds>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2118485987_gshared (Enumerator_t4099500171 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		((  void (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t4099500171 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t4079827401 * L_2 = (List_1_t4079827401 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t4079827401 * L_4 = (List_1_t4079827401 *)__this->get_l_0();
		NullCheck(L_4);
		BoundsU5BU5D_t3144995076* L_5 = (BoundsU5BU5D_t3144995076*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_8);
		int32_t L_9 = L_8;
		Bounds_t2711641849  L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m2118485987_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4099500171 * _thisAdjusted = reinterpret_cast<Enumerator_t4099500171 *>(__this + 1);
	return Enumerator_MoveNext_m2118485987(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.Bounds>::get_Current()
extern "C"  Bounds_t2711641849  Enumerator_get_Current_m3450009334_gshared (Enumerator_t4099500171 * __this, const MethodInfo* method)
{
	{
		Bounds_t2711641849  L_0 = (Bounds_t2711641849 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  Bounds_t2711641849  Enumerator_get_Current_m3450009334_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4099500171 * _thisAdjusted = reinterpret_cast<Enumerator_t4099500171 *>(__this + 1);
	return Enumerator_get_Current_m3450009334(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Color>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m1142033779_gshared (Enumerator_t1287437931 * __this, List_1_t1267765161 * ___l0, const MethodInfo* method)
{
	{
		List_1_t1267765161 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t1267765161 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m1142033779_AdjustorThunk (Il2CppObject * __this, List_1_t1267765161 * ___l0, const MethodInfo* method)
{
	Enumerator_t1287437931 * _thisAdjusted = reinterpret_cast<Enumerator_t1287437931 *>(__this + 1);
	Enumerator__ctor_m1142033779(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Color>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m821111551_gshared (Enumerator_t1287437931 * __this, const MethodInfo* method)
{
	{
		((  void (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t1287437931 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m821111551_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1287437931 * _thisAdjusted = reinterpret_cast<Enumerator_t1287437931 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m821111551(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.Color>::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m2558878571_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2558878571_gshared (Enumerator_t1287437931 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m2558878571_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		((  void (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t1287437931 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m355676978(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		Color_t4194546905  L_2 = (Color_t4194546905 )__this->get_current_3();
		Color_t4194546905  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2558878571_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1287437931 * _thisAdjusted = reinterpret_cast<Enumerator_t1287437931 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m2558878571(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Color>::Dispose()
extern "C"  void Enumerator_Dispose_m1790371864_gshared (Enumerator_t1287437931 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t1267765161 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m1790371864_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1287437931 * _thisAdjusted = reinterpret_cast<Enumerator_t1287437931 *>(__this + 1);
	Enumerator_Dispose_m1790371864(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Color>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3238255034;
extern const uint32_t Enumerator_VerifyState_m1334369105_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m1334369105_gshared (Enumerator_t1287437931 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m1334369105_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t1267765161 * L_0 = (List_1_t1267765161 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t1287437931  L_1 = (*(Enumerator_t1287437931 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t1794727681 * L_5 = (ObjectDisposedException_t1794727681 *)il2cpp_codegen_object_new(ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t1267765161 * L_7 = (List_1_t1267765161 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_9 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_9, (String_t*)_stringLiteral3238255034, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m1334369105_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1287437931 * _thisAdjusted = reinterpret_cast<Enumerator_t1287437931 *>(__this + 1);
	Enumerator_VerifyState_m1334369105(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Color>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2053754987_gshared (Enumerator_t1287437931 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		((  void (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t1287437931 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t1267765161 * L_2 = (List_1_t1267765161 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t1267765161 * L_4 = (List_1_t1267765161 *)__this->get_l_0();
		NullCheck(L_4);
		ColorU5BU5D_t2441545636* L_5 = (ColorU5BU5D_t2441545636*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_8);
		int32_t L_9 = L_8;
		Color_t4194546905  L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m2053754987_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1287437931 * _thisAdjusted = reinterpret_cast<Enumerator_t1287437931 *>(__this + 1);
	return Enumerator_MoveNext_m2053754987(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.Color>::get_Current()
extern "C"  Color_t4194546905  Enumerator_get_Current_m350313416_gshared (Enumerator_t1287437931 * __this, const MethodInfo* method)
{
	{
		Color_t4194546905  L_0 = (Color_t4194546905 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  Color_t4194546905  Enumerator_get_Current_m350313416_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1287437931 * _thisAdjusted = reinterpret_cast<Enumerator_t1287437931 *>(__this + 1);
	return Enumerator_get_Current_m350313416(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Color32>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m336902162_gshared (Enumerator_t1986712010 * __this, List_1_t1967039240 * ___l0, const MethodInfo* method)
{
	{
		List_1_t1967039240 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t1967039240 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m336902162_AdjustorThunk (Il2CppObject * __this, List_1_t1967039240 * ___l0, const MethodInfo* method)
{
	Enumerator_t1986712010 * _thisAdjusted = reinterpret_cast<Enumerator_t1986712010 *>(__this + 1);
	Enumerator__ctor_m336902162(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Color32>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2580027648_gshared (Enumerator_t1986712010 * __this, const MethodInfo* method)
{
	{
		((  void (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t1986712010 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2580027648_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1986712010 * _thisAdjusted = reinterpret_cast<Enumerator_t1986712010 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m2580027648(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.Color32>::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m1963314668_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1963314668_gshared (Enumerator_t1986712010 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m1963314668_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		((  void (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t1986712010 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m355676978(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		Color32_t598853688  L_2 = (Color32_t598853688 )__this->get_current_3();
		Color32_t598853688  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1963314668_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1986712010 * _thisAdjusted = reinterpret_cast<Enumerator_t1986712010 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m1963314668(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Color32>::Dispose()
extern "C"  void Enumerator_Dispose_m2747984503_gshared (Enumerator_t1986712010 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t1967039240 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m2747984503_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1986712010 * _thisAdjusted = reinterpret_cast<Enumerator_t1986712010 *>(__this + 1);
	Enumerator_Dispose_m2747984503(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Color32>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3238255034;
extern const uint32_t Enumerator_VerifyState_m431664_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m431664_gshared (Enumerator_t1986712010 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m431664_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t1967039240 * L_0 = (List_1_t1967039240 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t1986712010  L_1 = (*(Enumerator_t1986712010 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t1794727681 * L_5 = (ObjectDisposedException_t1794727681 *)il2cpp_codegen_object_new(ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t1967039240 * L_7 = (List_1_t1967039240 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_9 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_9, (String_t*)_stringLiteral3238255034, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m431664_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1986712010 * _thisAdjusted = reinterpret_cast<Enumerator_t1986712010 *>(__this + 1);
	Enumerator_VerifyState_m431664(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Color32>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2384339564_gshared (Enumerator_t1986712010 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		((  void (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t1986712010 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t1967039240 * L_2 = (List_1_t1967039240 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t1967039240 * L_4 = (List_1_t1967039240 *)__this->get_l_0();
		NullCheck(L_4);
		Color32U5BU5D_t2960766953* L_5 = (Color32U5BU5D_t2960766953*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_8);
		int32_t L_9 = L_8;
		Color32_t598853688  L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m2384339564_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1986712010 * _thisAdjusted = reinterpret_cast<Enumerator_t1986712010 *>(__this + 1);
	return Enumerator_MoveNext_m2384339564(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.Color32>::get_Current()
extern "C"  Color32_t598853688  Enumerator_get_Current_m2092495591_gshared (Enumerator_t1986712010 * __this, const MethodInfo* method)
{
	{
		Color32_t598853688  L_0 = (Color32_t598853688 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  Color32_t598853688  Enumerator_get_Current_m2092495591_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1986712010 * _thisAdjusted = reinterpret_cast<Enumerator_t1986712010 *>(__this + 1);
	return Enumerator_get_Current_m2092495591(_thisAdjusted, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
