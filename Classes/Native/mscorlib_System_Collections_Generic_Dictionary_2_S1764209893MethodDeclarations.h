﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<SoundTypeID,System.Object>
struct ShimEnumerator_t1764209893;
// System.Collections.Generic.Dictionary`2<SoundTypeID,System.Object>
struct Dictionary_2_t2048431866;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<SoundTypeID,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m993733952_gshared (ShimEnumerator_t1764209893 * __this, Dictionary_2_t2048431866 * ___host0, const MethodInfo* method);
#define ShimEnumerator__ctor_m993733952(__this, ___host0, method) ((  void (*) (ShimEnumerator_t1764209893 *, Dictionary_2_t2048431866 *, const MethodInfo*))ShimEnumerator__ctor_m993733952_gshared)(__this, ___host0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<SoundTypeID,System.Object>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m3711122017_gshared (ShimEnumerator_t1764209893 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m3711122017(__this, method) ((  bool (*) (ShimEnumerator_t1764209893 *, const MethodInfo*))ShimEnumerator_MoveNext_m3711122017_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<SoundTypeID,System.Object>::get_Entry()
extern "C"  DictionaryEntry_t1751606614  ShimEnumerator_get_Entry_m66325203_gshared (ShimEnumerator_t1764209893 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m66325203(__this, method) ((  DictionaryEntry_t1751606614  (*) (ShimEnumerator_t1764209893 *, const MethodInfo*))ShimEnumerator_get_Entry_m66325203_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<SoundTypeID,System.Object>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m3346012910_gshared (ShimEnumerator_t1764209893 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m3346012910(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t1764209893 *, const MethodInfo*))ShimEnumerator_get_Key_m3346012910_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<SoundTypeID,System.Object>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m3931305472_gshared (ShimEnumerator_t1764209893 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m3931305472(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t1764209893 *, const MethodInfo*))ShimEnumerator_get_Value_m3931305472_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<SoundTypeID,System.Object>::get_Current()
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m4132996232_gshared (ShimEnumerator_t1764209893 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m4132996232(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t1764209893 *, const MethodInfo*))ShimEnumerator_get_Current_m4132996232_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<SoundTypeID,System.Object>::Reset()
extern "C"  void ShimEnumerator_Reset_m3930056594_gshared (ShimEnumerator_t1764209893 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m3930056594(__this, method) ((  void (*) (ShimEnumerator_t1764209893 *, const MethodInfo*))ShimEnumerator_Reset_m3930056594_gshared)(__this, method)
