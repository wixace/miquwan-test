﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.Ionic.Zlib.InflateBlocks
struct InflateBlocks_t3613171323;
// Pathfinding.Ionic.Zlib.ZlibCodec
struct ZlibCodec_t3170755737;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "Pathfinding_Ionic_Zip_Reduced_Pathfinding_Ionic_Zl3170755737.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void Pathfinding.Ionic.Zlib.InflateBlocks::.ctor(Pathfinding.Ionic.Zlib.ZlibCodec,System.Object,System.Int32)
extern "C"  void InflateBlocks__ctor_m4207429407 (InflateBlocks_t3613171323 * __this, ZlibCodec_t3170755737 * ___codec0, Il2CppObject * ___checkfn1, int32_t ___w2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Ionic.Zlib.InflateBlocks::.cctor()
extern "C"  void InflateBlocks__cctor_m4127866853 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Pathfinding.Ionic.Zlib.InflateBlocks::Reset()
extern "C"  uint32_t InflateBlocks_Reset_m890362874 (InflateBlocks_t3613171323 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pathfinding.Ionic.Zlib.InflateBlocks::Process(System.Int32)
extern "C"  int32_t InflateBlocks_Process_m3397246750 (InflateBlocks_t3613171323 * __this, int32_t ___r0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Ionic.Zlib.InflateBlocks::Free()
extern "C"  void InflateBlocks_Free_m2855009000 (InflateBlocks_t3613171323 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pathfinding.Ionic.Zlib.InflateBlocks::Flush(System.Int32)
extern "C"  int32_t InflateBlocks_Flush_m75740723 (InflateBlocks_t3613171323 * __this, int32_t ___r0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
