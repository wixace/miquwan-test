﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_DebugGenerated
struct UnityEngine_DebugGenerated_t798295012;
// JSVCall
struct JSVCall_t3708497963;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void UnityEngine_DebugGenerated::.ctor()
extern "C"  void UnityEngine_DebugGenerated__ctor_m3714024759 (UnityEngine_DebugGenerated_t798295012 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_DebugGenerated::Debug_Debug1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_DebugGenerated_Debug_Debug1_m1116760891 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_DebugGenerated::Debug_logger(JSVCall)
extern "C"  void UnityEngine_DebugGenerated_Debug_logger_m3642581598 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_DebugGenerated::Debug_developerConsoleVisible(JSVCall)
extern "C"  void UnityEngine_DebugGenerated_Debug_developerConsoleVisible_m3074816569 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_DebugGenerated::Debug_isDebugBuild(JSVCall)
extern "C"  void UnityEngine_DebugGenerated_Debug_isDebugBuild_m4228765929 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_DebugGenerated::Debug_Assert__Boolean__String__UEObject(JSVCall,System.Int32)
extern "C"  bool UnityEngine_DebugGenerated_Debug_Assert__Boolean__String__UEObject_m2984792935 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_DebugGenerated::Debug_Assert__Boolean__Object__UEObject(JSVCall,System.Int32)
extern "C"  bool UnityEngine_DebugGenerated_Debug_Assert__Boolean__Object__UEObject_m1359699605 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_DebugGenerated::Debug_Assert__Boolean__UEObject(JSVCall,System.Int32)
extern "C"  bool UnityEngine_DebugGenerated_Debug_Assert__Boolean__UEObject_m3515022678 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_DebugGenerated::Debug_Assert__Boolean__Object(JSVCall,System.Int32)
extern "C"  bool UnityEngine_DebugGenerated_Debug_Assert__Boolean__Object_m3200604230 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_DebugGenerated::Debug_Assert__Boolean__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_DebugGenerated_Debug_Assert__Boolean__String_m3100953496 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_DebugGenerated::Debug_Assert__Boolean(JSVCall,System.Int32)
extern "C"  bool UnityEngine_DebugGenerated_Debug_Assert__Boolean_m214910023 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_DebugGenerated::Debug_AssertFormat__Boolean__UEObject__String__Object_Array(JSVCall,System.Int32)
extern "C"  bool UnityEngine_DebugGenerated_Debug_AssertFormat__Boolean__UEObject__String__Object_Array_m4030694953 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_DebugGenerated::Debug_AssertFormat__Boolean__String__Object_Array(JSVCall,System.Int32)
extern "C"  bool UnityEngine_DebugGenerated_Debug_AssertFormat__Boolean__String__Object_Array_m2003794330 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_DebugGenerated::Debug_Break(JSVCall,System.Int32)
extern "C"  bool UnityEngine_DebugGenerated_Debug_Break_m2342345444 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_DebugGenerated::Debug_ClearDeveloperConsole(JSVCall,System.Int32)
extern "C"  bool UnityEngine_DebugGenerated_Debug_ClearDeveloperConsole_m2359968319 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_DebugGenerated::Debug_DebugBreak(JSVCall,System.Int32)
extern "C"  bool UnityEngine_DebugGenerated_Debug_DebugBreak_m1672542441 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_DebugGenerated::Debug_DrawLine__Vector3__Vector3__Color__Single__Boolean(JSVCall,System.Int32)
extern "C"  bool UnityEngine_DebugGenerated_Debug_DrawLine__Vector3__Vector3__Color__Single__Boolean_m2236680434 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_DebugGenerated::Debug_DrawLine__Vector3__Vector3__Color__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_DebugGenerated_Debug_DrawLine__Vector3__Vector3__Color__Single_m2144301880 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_DebugGenerated::Debug_DrawLine__Vector3__Vector3__Color(JSVCall,System.Int32)
extern "C"  bool UnityEngine_DebugGenerated_Debug_DrawLine__Vector3__Vector3__Color_m1526961136 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_DebugGenerated::Debug_DrawLine__Vector3__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_DebugGenerated_Debug_DrawLine__Vector3__Vector3_m3116514517 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_DebugGenerated::Debug_DrawRay__Vector3__Vector3__Color__Single__Boolean(JSVCall,System.Int32)
extern "C"  bool UnityEngine_DebugGenerated_Debug_DrawRay__Vector3__Vector3__Color__Single__Boolean_m2072693640 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_DebugGenerated::Debug_DrawRay__Vector3__Vector3__Color__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_DebugGenerated_Debug_DrawRay__Vector3__Vector3__Color__Single_m30560226 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_DebugGenerated::Debug_DrawRay__Vector3__Vector3__Color(JSVCall,System.Int32)
extern "C"  bool UnityEngine_DebugGenerated_Debug_DrawRay__Vector3__Vector3__Color_m1176156826 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_DebugGenerated::Debug_DrawRay__Vector3__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_DebugGenerated_Debug_DrawRay__Vector3__Vector3_m2455429483 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_DebugGenerated::Debug_Log__Object__UEObject(JSVCall,System.Int32)
extern "C"  bool UnityEngine_DebugGenerated_Debug_Log__Object__UEObject_m815595063 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_DebugGenerated::Debug_Log__Object(JSVCall,System.Int32)
extern "C"  bool UnityEngine_DebugGenerated_Debug_Log__Object_m2961816168 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_DebugGenerated::Debug_LogAssertion__Object__UEObject(JSVCall,System.Int32)
extern "C"  bool UnityEngine_DebugGenerated_Debug_LogAssertion__Object__UEObject_m3898727273 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_DebugGenerated::Debug_LogAssertion__Object(JSVCall,System.Int32)
extern "C"  bool UnityEngine_DebugGenerated_Debug_LogAssertion__Object_m2320895514 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_DebugGenerated::Debug_LogAssertionFormat__UEObject__String__Object_Array(JSVCall,System.Int32)
extern "C"  bool UnityEngine_DebugGenerated_Debug_LogAssertionFormat__UEObject__String__Object_Array_m594127563 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_DebugGenerated::Debug_LogAssertionFormat__String__Object_Array(JSVCall,System.Int32)
extern "C"  bool UnityEngine_DebugGenerated_Debug_LogAssertionFormat__String__Object_Array_m3820921020 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_DebugGenerated::Debug_LogError__Object__UEObject(JSVCall,System.Int32)
extern "C"  bool UnityEngine_DebugGenerated_Debug_LogError__Object__UEObject_m1712495727 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_DebugGenerated::Debug_LogError__Object(JSVCall,System.Int32)
extern "C"  bool UnityEngine_DebugGenerated_Debug_LogError__Object_m260914848 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_DebugGenerated::Debug_LogErrorFormat__UEObject__String__Object_Array(JSVCall,System.Int32)
extern "C"  bool UnityEngine_DebugGenerated_Debug_LogErrorFormat__UEObject__String__Object_Array_m2244324561 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_DebugGenerated::Debug_LogErrorFormat__String__Object_Array(JSVCall,System.Int32)
extern "C"  bool UnityEngine_DebugGenerated_Debug_LogErrorFormat__String__Object_Array_m3985527362 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_DebugGenerated::Debug_LogException__Exception__UEObject(JSVCall,System.Int32)
extern "C"  bool UnityEngine_DebugGenerated_Debug_LogException__Exception__UEObject_m59043768 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_DebugGenerated::Debug_LogException__Exception(JSVCall,System.Int32)
extern "C"  bool UnityEngine_DebugGenerated_Debug_LogException__Exception_m1460169001 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_DebugGenerated::Debug_LogFormat__UEObject__String__Object_Array(JSVCall,System.Int32)
extern "C"  bool UnityEngine_DebugGenerated_Debug_LogFormat__UEObject__String__Object_Array_m1943362201 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_DebugGenerated::Debug_LogFormat__String__Object_Array(JSVCall,System.Int32)
extern "C"  bool UnityEngine_DebugGenerated_Debug_LogFormat__String__Object_Array_m2382305802 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_DebugGenerated::Debug_LogWarning__Object__UEObject(JSVCall,System.Int32)
extern "C"  bool UnityEngine_DebugGenerated_Debug_LogWarning__Object__UEObject_m1171919875 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_DebugGenerated::Debug_LogWarning__Object(JSVCall,System.Int32)
extern "C"  bool UnityEngine_DebugGenerated_Debug_LogWarning__Object_m3938803508 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_DebugGenerated::Debug_LogWarningFormat__UEObject__String__Object_Array(JSVCall,System.Int32)
extern "C"  bool UnityEngine_DebugGenerated_Debug_LogWarningFormat__UEObject__String__Object_Array_m513380965 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_DebugGenerated::Debug_LogWarningFormat__String__Object_Array(JSVCall,System.Int32)
extern "C"  bool UnityEngine_DebugGenerated_Debug_LogWarningFormat__String__Object_Array_m1675911382 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_DebugGenerated::__Register()
extern "C"  void UnityEngine_DebugGenerated___Register_m1577558512 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] UnityEngine_DebugGenerated::<Debug_AssertFormat__Boolean__UEObject__String__Object_Array>m__1A0()
extern "C"  ObjectU5BU5D_t1108656482* UnityEngine_DebugGenerated_U3CDebug_AssertFormat__Boolean__UEObject__String__Object_ArrayU3Em__1A0_m1917886775 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] UnityEngine_DebugGenerated::<Debug_AssertFormat__Boolean__String__Object_Array>m__1A1()
extern "C"  ObjectU5BU5D_t1108656482* UnityEngine_DebugGenerated_U3CDebug_AssertFormat__Boolean__String__Object_ArrayU3Em__1A1_m2334397991 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] UnityEngine_DebugGenerated::<Debug_LogAssertionFormat__UEObject__String__Object_Array>m__1A2()
extern "C"  ObjectU5BU5D_t1108656482* UnityEngine_DebugGenerated_U3CDebug_LogAssertionFormat__UEObject__String__Object_ArrayU3Em__1A2_m3068012541 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] UnityEngine_DebugGenerated::<Debug_LogAssertionFormat__String__Object_Array>m__1A3()
extern "C"  ObjectU5BU5D_t1108656482* UnityEngine_DebugGenerated_U3CDebug_LogAssertionFormat__String__Object_ArrayU3Em__1A3_m2280071661 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] UnityEngine_DebugGenerated::<Debug_LogErrorFormat__UEObject__String__Object_Array>m__1A4()
extern "C"  ObjectU5BU5D_t1108656482* UnityEngine_DebugGenerated_U3CDebug_LogErrorFormat__UEObject__String__Object_ArrayU3Em__1A4_m114812217 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] UnityEngine_DebugGenerated::<Debug_LogErrorFormat__String__Object_Array>m__1A5()
extern "C"  ObjectU5BU5D_t1108656482* UnityEngine_DebugGenerated_U3CDebug_LogErrorFormat__String__Object_ArrayU3Em__1A5_m234610601 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] UnityEngine_DebugGenerated::<Debug_LogFormat__UEObject__String__Object_Array>m__1A6()
extern "C"  ObjectU5BU5D_t1108656482* UnityEngine_DebugGenerated_U3CDebug_LogFormat__UEObject__String__Object_ArrayU3Em__1A6_m3933814989 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] UnityEngine_DebugGenerated::<Debug_LogFormat__String__Object_Array>m__1A7()
extern "C"  ObjectU5BU5D_t1108656482* UnityEngine_DebugGenerated_U3CDebug_LogFormat__String__Object_ArrayU3Em__1A7_m732023229 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] UnityEngine_DebugGenerated::<Debug_LogWarningFormat__UEObject__String__Object_Array>m__1A8()
extern "C"  ObjectU5BU5D_t1108656482* UnityEngine_DebugGenerated_U3CDebug_LogWarningFormat__UEObject__String__Object_ArrayU3Em__1A8_m4261491049 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] UnityEngine_DebugGenerated::<Debug_LogWarningFormat__String__Object_Array>m__1A9()
extern "C"  ObjectU5BU5D_t1108656482* UnityEngine_DebugGenerated_U3CDebug_LogWarningFormat__String__Object_ArrayU3Em__1A9_m2154525401 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_DebugGenerated::ilo_attachFinalizerObject1(System.Int32)
extern "C"  bool UnityEngine_DebugGenerated_ilo_attachFinalizerObject1_m2441733256 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_DebugGenerated::ilo_getObject2(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_DebugGenerated_ilo_getObject2_m1105518233 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_DebugGenerated::ilo_getBooleanS3(System.Int32)
extern "C"  bool UnityEngine_DebugGenerated_ilo_getBooleanS3_m3071494455 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine_DebugGenerated::ilo_getStringS4(System.Int32)
extern "C"  String_t* UnityEngine_DebugGenerated_ilo_getStringS4_m4119263544 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine_DebugGenerated::ilo_getVector3S5(System.Int32)
extern "C"  Vector3_t4282066566  UnityEngine_DebugGenerated_ilo_getVector3S5_m2051181607 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_DebugGenerated::ilo_getWhatever6(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_DebugGenerated_ilo_getWhatever6_m1250918618 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_DebugGenerated::ilo_getElement7(System.Int32,System.Int32)
extern "C"  int32_t UnityEngine_DebugGenerated_ilo_getElement7_m3051592193 (Il2CppObject * __this /* static, unused */, int32_t ___id0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_DebugGenerated::ilo_getObject8(System.Int32)
extern "C"  int32_t UnityEngine_DebugGenerated_ilo_getObject8_m2062262402 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_DebugGenerated::ilo_getArrayLength9(System.Int32)
extern "C"  int32_t UnityEngine_DebugGenerated_ilo_getArrayLength9_m4057940117 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
