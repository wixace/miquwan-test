﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UITextureGenerated
struct UITextureGenerated_t1798445416;
// JSVCall
struct JSVCall_t3708497963;
// UnityEngine.Texture
struct Texture_t2526458961;
// UITexture
struct UITexture_t3903132647;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;
// UnityEngine.Material
struct Material_t3870600107;
// BetterList`1<UnityEngine.Vector3>
struct BetterList_1_t1484067282;
// BetterList`1<UnityEngine.Vector2>
struct BetterList_1_t1484067281;
// BetterList`1<UnityEngine.Color32>
struct BetterList_1_t2095821700;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_UITexture3903132647.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_Material3870600107.h"

// System.Void UITextureGenerated::.ctor()
extern "C"  void UITextureGenerated__ctor_m1326840883 (UITextureGenerated_t1798445416 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UITextureGenerated::UITexture_UITexture1(JSVCall,System.Int32)
extern "C"  bool UITextureGenerated_UITexture_UITexture1_m84754775 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITextureGenerated::UITexture_IsMathConvert(JSVCall)
extern "C"  void UITextureGenerated_UITexture_IsMathConvert_m220342701 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITextureGenerated::UITexture_mainTexture(JSVCall)
extern "C"  void UITextureGenerated_UITexture_mainTexture_m499132812 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITextureGenerated::UITexture_material(JSVCall)
extern "C"  void UITextureGenerated_UITexture_material_m1174684247 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITextureGenerated::UITexture_shader(JSVCall)
extern "C"  void UITextureGenerated_UITexture_shader_m1858734937 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITextureGenerated::UITexture_premultipliedAlpha(JSVCall)
extern "C"  void UITextureGenerated_UITexture_premultipliedAlpha_m1828033590 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITextureGenerated::UITexture_border(JSVCall)
extern "C"  void UITextureGenerated_UITexture_border_m2063331058 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITextureGenerated::UITexture_uvRect(JSVCall)
extern "C"  void UITextureGenerated_UITexture_uvRect_m1232054041 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITextureGenerated::UITexture_drawingDimensions(JSVCall)
extern "C"  void UITextureGenerated_UITexture_drawingDimensions_m267080771 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITextureGenerated::UITexture_fixedAspect(JSVCall)
extern "C"  void UITextureGenerated_UITexture_fixedAspect_m1390969090 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UITextureGenerated::UITexture_MakePixelPerfect(JSVCall,System.Int32)
extern "C"  bool UITextureGenerated_UITexture_MakePixelPerfect_m1143684290 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UITextureGenerated::UITexture_OnFill__BetterListT1_Vector3__BetterListT1_Vector2__BetterListT1_Color32(JSVCall,System.Int32)
extern "C"  bool UITextureGenerated_UITexture_OnFill__BetterListT1_Vector3__BetterListT1_Vector2__BetterListT1_Color32_m3722544628 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UITextureGenerated::UITexture_UpdateTexture__String(JSVCall,System.Int32)
extern "C"  bool UITextureGenerated_UITexture_UpdateTexture__String_m3073576632 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITextureGenerated::__Register()
extern "C"  void UITextureGenerated___Register_m2549098612 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture UITextureGenerated::ilo_get_mainTexture1(UITexture)
extern "C"  Texture_t2526458961 * UITextureGenerated_ilo_get_mainTexture1_m1834163104 (Il2CppObject * __this /* static, unused */, UITexture_t3903132647 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UITextureGenerated::ilo_setObject2(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UITextureGenerated_ilo_setObject2_m437889784 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITextureGenerated::ilo_set_material3(UITexture,UnityEngine.Material)
extern "C"  void UITextureGenerated_ilo_set_material3_m13830346 (Il2CppObject * __this /* static, unused */, UITexture_t3903132647 * ____this0, Material_t3870600107 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UITextureGenerated::ilo_getObject4(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UITextureGenerated_ilo_getObject4_m2598535455 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITextureGenerated::ilo_setBooleanS5(System.Int32,System.Boolean)
extern "C"  void UITextureGenerated_ilo_setBooleanS5_m2834058976 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UITextureGenerated::ilo_getBooleanS6(System.Int32)
extern "C"  bool UITextureGenerated_ilo_getBooleanS6_m2682120574 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITextureGenerated::ilo_MakePixelPerfect7(UITexture)
extern "C"  void UITextureGenerated_ilo_MakePixelPerfect7_m1322365003 (Il2CppObject * __this /* static, unused */, UITexture_t3903132647 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITextureGenerated::ilo_OnFill8(UITexture,BetterList`1<UnityEngine.Vector3>,BetterList`1<UnityEngine.Vector2>,BetterList`1<UnityEngine.Color32>)
extern "C"  void UITextureGenerated_ilo_OnFill8_m3781796166 (Il2CppObject * __this /* static, unused */, UITexture_t3903132647 * ____this0, BetterList_1_t1484067282 * ___verts1, BetterList_1_t1484067281 * ___uvs2, BetterList_1_t2095821700 * ___cols3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
