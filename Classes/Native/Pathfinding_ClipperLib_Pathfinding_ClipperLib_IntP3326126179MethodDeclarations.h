﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// Pathfinding.ClipperLib.IntPoint
struct IntPoint_t3326126179;
struct IntPoint_t3326126179_marshaled_pinvoke;
struct IntPoint_t3326126179_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "Pathfinding_ClipperLib_Pathfinding_ClipperLib_IntP3326126179.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void Pathfinding.ClipperLib.IntPoint::.ctor(System.Int64,System.Int64)
extern "C"  void IntPoint__ctor_m1065894876 (IntPoint_t3326126179 * __this, int64_t ___X0, int64_t ___Y1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.ClipperLib.IntPoint::.ctor(Pathfinding.ClipperLib.IntPoint)
extern "C"  void IntPoint__ctor_m964253275 (IntPoint_t3326126179 * __this, IntPoint_t3326126179  ___pt0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.ClipperLib.IntPoint::Equals(System.Object)
extern "C"  bool IntPoint_Equals_m2369764343 (IntPoint_t3326126179 * __this, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pathfinding.ClipperLib.IntPoint::GetHashCode()
extern "C"  int32_t IntPoint_GetHashCode_m2934122383 (IntPoint_t3326126179 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.ClipperLib.IntPoint::op_Equality(Pathfinding.ClipperLib.IntPoint,Pathfinding.ClipperLib.IntPoint)
extern "C"  bool IntPoint_op_Equality_m1729684360 (Il2CppObject * __this /* static, unused */, IntPoint_t3326126179  ___a0, IntPoint_t3326126179  ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.ClipperLib.IntPoint::op_Inequality(Pathfinding.ClipperLib.IntPoint,Pathfinding.ClipperLib.IntPoint)
extern "C"  bool IntPoint_op_Inequality_m3996904515 (Il2CppObject * __this /* static, unused */, IntPoint_t3326126179  ___a0, IntPoint_t3326126179  ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct IntPoint_t3326126179;
struct IntPoint_t3326126179_marshaled_pinvoke;

extern "C" void IntPoint_t3326126179_marshal_pinvoke(const IntPoint_t3326126179& unmarshaled, IntPoint_t3326126179_marshaled_pinvoke& marshaled);
extern "C" void IntPoint_t3326126179_marshal_pinvoke_back(const IntPoint_t3326126179_marshaled_pinvoke& marshaled, IntPoint_t3326126179& unmarshaled);
extern "C" void IntPoint_t3326126179_marshal_pinvoke_cleanup(IntPoint_t3326126179_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct IntPoint_t3326126179;
struct IntPoint_t3326126179_marshaled_com;

extern "C" void IntPoint_t3326126179_marshal_com(const IntPoint_t3326126179& unmarshaled, IntPoint_t3326126179_marshaled_com& marshaled);
extern "C" void IntPoint_t3326126179_marshal_com_back(const IntPoint_t3326126179_marshaled_com& marshaled, IntPoint_t3326126179& unmarshaled);
extern "C" void IntPoint_t3326126179_marshal_com_cleanup(IntPoint_t3326126179_marshaled_com& marshaled);
