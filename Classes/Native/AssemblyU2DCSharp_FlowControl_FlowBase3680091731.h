﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FlowControl.FlowBase
struct  FlowBase_t3680091731  : public Il2CppObject
{
public:
	// System.Single FlowControl.FlowBase::_activeTime
	float ____activeTime_0;
	// System.Boolean FlowControl.FlowBase::_isActive
	bool ____isActive_1;
	// System.Boolean FlowControl.FlowBase::_isComplete
	bool ____isComplete_2;
	// System.Boolean FlowControl.FlowBase::_isOnComplete
	bool ____isOnComplete_3;
	// System.Boolean FlowControl.FlowBase::_isDestroy
	bool ____isDestroy_4;
	// System.String FlowControl.FlowBase::_name
	String_t* ____name_5;
	// System.Single FlowControl.FlowBase::passSATime
	float ___passSATime_6;

public:
	inline static int32_t get_offset_of__activeTime_0() { return static_cast<int32_t>(offsetof(FlowBase_t3680091731, ____activeTime_0)); }
	inline float get__activeTime_0() const { return ____activeTime_0; }
	inline float* get_address_of__activeTime_0() { return &____activeTime_0; }
	inline void set__activeTime_0(float value)
	{
		____activeTime_0 = value;
	}

	inline static int32_t get_offset_of__isActive_1() { return static_cast<int32_t>(offsetof(FlowBase_t3680091731, ____isActive_1)); }
	inline bool get__isActive_1() const { return ____isActive_1; }
	inline bool* get_address_of__isActive_1() { return &____isActive_1; }
	inline void set__isActive_1(bool value)
	{
		____isActive_1 = value;
	}

	inline static int32_t get_offset_of__isComplete_2() { return static_cast<int32_t>(offsetof(FlowBase_t3680091731, ____isComplete_2)); }
	inline bool get__isComplete_2() const { return ____isComplete_2; }
	inline bool* get_address_of__isComplete_2() { return &____isComplete_2; }
	inline void set__isComplete_2(bool value)
	{
		____isComplete_2 = value;
	}

	inline static int32_t get_offset_of__isOnComplete_3() { return static_cast<int32_t>(offsetof(FlowBase_t3680091731, ____isOnComplete_3)); }
	inline bool get__isOnComplete_3() const { return ____isOnComplete_3; }
	inline bool* get_address_of__isOnComplete_3() { return &____isOnComplete_3; }
	inline void set__isOnComplete_3(bool value)
	{
		____isOnComplete_3 = value;
	}

	inline static int32_t get_offset_of__isDestroy_4() { return static_cast<int32_t>(offsetof(FlowBase_t3680091731, ____isDestroy_4)); }
	inline bool get__isDestroy_4() const { return ____isDestroy_4; }
	inline bool* get_address_of__isDestroy_4() { return &____isDestroy_4; }
	inline void set__isDestroy_4(bool value)
	{
		____isDestroy_4 = value;
	}

	inline static int32_t get_offset_of__name_5() { return static_cast<int32_t>(offsetof(FlowBase_t3680091731, ____name_5)); }
	inline String_t* get__name_5() const { return ____name_5; }
	inline String_t** get_address_of__name_5() { return &____name_5; }
	inline void set__name_5(String_t* value)
	{
		____name_5 = value;
		Il2CppCodeGenWriteBarrier(&____name_5, value);
	}

	inline static int32_t get_offset_of_passSATime_6() { return static_cast<int32_t>(offsetof(FlowBase_t3680091731, ___passSATime_6)); }
	inline float get_passSATime_6() const { return ___passSATime_6; }
	inline float* get_address_of_passSATime_6() { return &___passSATime_6; }
	inline void set_passSATime_6(float value)
	{
		___passSATime_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
