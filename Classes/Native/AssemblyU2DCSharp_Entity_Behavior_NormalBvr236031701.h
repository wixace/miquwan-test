﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_Entity_Behavior_IBehavior770859129.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entity.Behavior.NormalBvr
struct  NormalBvr_t236031701  : public IBehavior_t770859129
{
public:
	// System.Boolean Entity.Behavior.NormalBvr::isRotePoint
	bool ___isRotePoint_3;
	// System.Single Entity.Behavior.NormalBvr::rotePointWaitTime
	float ___rotePointWaitTime_4;

public:
	inline static int32_t get_offset_of_isRotePoint_3() { return static_cast<int32_t>(offsetof(NormalBvr_t236031701, ___isRotePoint_3)); }
	inline bool get_isRotePoint_3() const { return ___isRotePoint_3; }
	inline bool* get_address_of_isRotePoint_3() { return &___isRotePoint_3; }
	inline void set_isRotePoint_3(bool value)
	{
		___isRotePoint_3 = value;
	}

	inline static int32_t get_offset_of_rotePointWaitTime_4() { return static_cast<int32_t>(offsetof(NormalBvr_t236031701, ___rotePointWaitTime_4)); }
	inline float get_rotePointWaitTime_4() const { return ___rotePointWaitTime_4; }
	inline float* get_address_of_rotePointWaitTime_4() { return &___rotePointWaitTime_4; }
	inline void set_rotePointWaitTime_4(float value)
	{
		___rotePointWaitTime_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
