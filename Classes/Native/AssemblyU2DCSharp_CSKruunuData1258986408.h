﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// CSAttrBuffData[]
struct CSAttrBuffDataU5BU5D_t2697436427;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CSKruunuData
struct  CSKruunuData_t1258986408  : public Il2CppObject
{
public:
	// CSAttrBuffData[] CSKruunuData::attrBuffs
	CSAttrBuffDataU5BU5D_t2697436427* ___attrBuffs_0;

public:
	inline static int32_t get_offset_of_attrBuffs_0() { return static_cast<int32_t>(offsetof(CSKruunuData_t1258986408, ___attrBuffs_0)); }
	inline CSAttrBuffDataU5BU5D_t2697436427* get_attrBuffs_0() const { return ___attrBuffs_0; }
	inline CSAttrBuffDataU5BU5D_t2697436427** get_address_of_attrBuffs_0() { return &___attrBuffs_0; }
	inline void set_attrBuffs_0(CSAttrBuffDataU5BU5D_t2697436427* value)
	{
		___attrBuffs_0 = value;
		Il2CppCodeGenWriteBarrier(&___attrBuffs_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
