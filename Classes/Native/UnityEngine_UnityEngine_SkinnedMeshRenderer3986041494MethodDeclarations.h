﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.SkinnedMeshRenderer
struct SkinnedMeshRenderer_t3986041494;
// UnityEngine.Transform[]
struct TransformU5BU5D_t3792884695;
// UnityEngine.Transform
struct Transform_t1659122786;
// UnityEngine.Mesh
struct Mesh_t4241756145;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Transform1659122786.h"
#include "UnityEngine_UnityEngine_SkinQuality1206517624.h"
#include "UnityEngine_UnityEngine_Mesh4241756145.h"
#include "UnityEngine_UnityEngine_Bounds2711641849.h"

// System.Void UnityEngine.SkinnedMeshRenderer::.ctor()
extern "C"  void SkinnedMeshRenderer__ctor_m847473785 (SkinnedMeshRenderer_t3986041494 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform[] UnityEngine.SkinnedMeshRenderer::get_bones()
extern "C"  TransformU5BU5D_t3792884695* SkinnedMeshRenderer_get_bones_m2029101177 (SkinnedMeshRenderer_t3986041494 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SkinnedMeshRenderer::set_bones(UnityEngine.Transform[])
extern "C"  void SkinnedMeshRenderer_set_bones_m3788446386 (SkinnedMeshRenderer_t3986041494 * __this, TransformU5BU5D_t3792884695* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.SkinnedMeshRenderer::get_rootBone()
extern "C"  Transform_t1659122786 * SkinnedMeshRenderer_get_rootBone_m2468735484 (SkinnedMeshRenderer_t3986041494 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SkinnedMeshRenderer::set_rootBone(UnityEngine.Transform)
extern "C"  void SkinnedMeshRenderer_set_rootBone_m104990511 (SkinnedMeshRenderer_t3986041494 * __this, Transform_t1659122786 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.SkinQuality UnityEngine.SkinnedMeshRenderer::get_quality()
extern "C"  int32_t SkinnedMeshRenderer_get_quality_m730302869 (SkinnedMeshRenderer_t3986041494 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SkinnedMeshRenderer::set_quality(UnityEngine.SkinQuality)
extern "C"  void SkinnedMeshRenderer_set_quality_m2719599022 (SkinnedMeshRenderer_t3986041494 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Mesh UnityEngine.SkinnedMeshRenderer::get_sharedMesh()
extern "C"  Mesh_t4241756145 * SkinnedMeshRenderer_get_sharedMesh_m1298086811 (SkinnedMeshRenderer_t3986041494 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SkinnedMeshRenderer::set_sharedMesh(UnityEngine.Mesh)
extern "C"  void SkinnedMeshRenderer_set_sharedMesh_m3249072150 (SkinnedMeshRenderer_t3986041494 * __this, Mesh_t4241756145 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.SkinnedMeshRenderer::get_updateWhenOffscreen()
extern "C"  bool SkinnedMeshRenderer_get_updateWhenOffscreen_m1513102252 (SkinnedMeshRenderer_t3986041494 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SkinnedMeshRenderer::set_updateWhenOffscreen(System.Boolean)
extern "C"  void SkinnedMeshRenderer_set_updateWhenOffscreen_m3438381641 (SkinnedMeshRenderer_t3986041494 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Bounds UnityEngine.SkinnedMeshRenderer::get_localBounds()
extern "C"  Bounds_t2711641849  SkinnedMeshRenderer_get_localBounds_m2234798417 (SkinnedMeshRenderer_t3986041494 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SkinnedMeshRenderer::set_localBounds(UnityEngine.Bounds)
extern "C"  void SkinnedMeshRenderer_set_localBounds_m1518546862 (SkinnedMeshRenderer_t3986041494 * __this, Bounds_t2711641849  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SkinnedMeshRenderer::INTERNAL_get_localBounds(UnityEngine.Bounds&)
extern "C"  void SkinnedMeshRenderer_INTERNAL_get_localBounds_m3779851992 (SkinnedMeshRenderer_t3986041494 * __this, Bounds_t2711641849 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SkinnedMeshRenderer::INTERNAL_set_localBounds(UnityEngine.Bounds&)
extern "C"  void SkinnedMeshRenderer_INTERNAL_set_localBounds_m1413651276 (SkinnedMeshRenderer_t3986041494 * __this, Bounds_t2711641849 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SkinnedMeshRenderer::BakeMesh(UnityEngine.Mesh)
extern "C"  void SkinnedMeshRenderer_BakeMesh_m3302464365 (SkinnedMeshRenderer_t3986041494 * __this, Mesh_t4241756145 * ___mesh0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.SkinnedMeshRenderer::GetBlendShapeWeight(System.Int32)
extern "C"  float SkinnedMeshRenderer_GetBlendShapeWeight_m2551512402 (SkinnedMeshRenderer_t3986041494 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SkinnedMeshRenderer::SetBlendShapeWeight(System.Int32,System.Single)
extern "C"  void SkinnedMeshRenderer_SetBlendShapeWeight_m2024253623 (SkinnedMeshRenderer_t3986041494 * __this, int32_t ___index0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
