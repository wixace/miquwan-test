﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BehaviourUtil/<JSDelayCallExec>c__Iterator3B
struct U3CJSDelayCallExecU3Ec__Iterator3B_t3648384767;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void BehaviourUtil/<JSDelayCallExec>c__Iterator3B::.ctor()
extern "C"  void U3CJSDelayCallExecU3Ec__Iterator3B__ctor_m2320962172 (U3CJSDelayCallExecU3Ec__Iterator3B_t3648384767 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object BehaviourUtil/<JSDelayCallExec>c__Iterator3B::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CJSDelayCallExecU3Ec__Iterator3B_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1440437600 (U3CJSDelayCallExecU3Ec__Iterator3B_t3648384767 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object BehaviourUtil/<JSDelayCallExec>c__Iterator3B::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CJSDelayCallExecU3Ec__Iterator3B_System_Collections_IEnumerator_get_Current_m2820937972 (U3CJSDelayCallExecU3Ec__Iterator3B_t3648384767 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BehaviourUtil/<JSDelayCallExec>c__Iterator3B::MoveNext()
extern "C"  bool U3CJSDelayCallExecU3Ec__Iterator3B_MoveNext_m2831614944 (U3CJSDelayCallExecU3Ec__Iterator3B_t3648384767 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BehaviourUtil/<JSDelayCallExec>c__Iterator3B::Dispose()
extern "C"  void U3CJSDelayCallExecU3Ec__Iterator3B_Dispose_m1254903737 (U3CJSDelayCallExecU3Ec__Iterator3B_t3648384767 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BehaviourUtil/<JSDelayCallExec>c__Iterator3B::Reset()
extern "C"  void U3CJSDelayCallExecU3Ec__Iterator3B_Reset_m4262362409 (U3CJSDelayCallExecU3Ec__Iterator3B_t3648384767 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
