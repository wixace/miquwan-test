﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_StaticBatchingUtilityGenerated
struct UnityEngine_StaticBatchingUtilityGenerated_t3293962241;
// JSVCall
struct JSVCall_t3708497963;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t2662109048;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"

// System.Void UnityEngine_StaticBatchingUtilityGenerated::.ctor()
extern "C"  void UnityEngine_StaticBatchingUtilityGenerated__ctor_m2723465658 (UnityEngine_StaticBatchingUtilityGenerated_t3293962241 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_StaticBatchingUtilityGenerated::StaticBatchingUtility_StaticBatchingUtility1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_StaticBatchingUtilityGenerated_StaticBatchingUtility_StaticBatchingUtility1_m985272274 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_StaticBatchingUtilityGenerated::StaticBatchingUtility_Combine__GameObject_Array__GameObject(JSVCall,System.Int32)
extern "C"  bool UnityEngine_StaticBatchingUtilityGenerated_StaticBatchingUtility_Combine__GameObject_Array__GameObject_m1491287814 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_StaticBatchingUtilityGenerated::StaticBatchingUtility_Combine__GameObject(JSVCall,System.Int32)
extern "C"  bool UnityEngine_StaticBatchingUtilityGenerated_StaticBatchingUtility_Combine__GameObject_m2324181947 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_StaticBatchingUtilityGenerated::__Register()
extern "C"  void UnityEngine_StaticBatchingUtilityGenerated___Register_m1397208909 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject[] UnityEngine_StaticBatchingUtilityGenerated::<StaticBatchingUtility_Combine__GameObject_Array__GameObject>m__2D8()
extern "C"  GameObjectU5BU5D_t2662109048* UnityEngine_StaticBatchingUtilityGenerated_U3CStaticBatchingUtility_Combine__GameObject_Array__GameObjectU3Em__2D8_m3629096646 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_StaticBatchingUtilityGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UnityEngine_StaticBatchingUtilityGenerated_ilo_getObject1_m304191512 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_StaticBatchingUtilityGenerated::ilo_getObject2(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_StaticBatchingUtilityGenerated_ilo_getObject2_m441701046 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
