﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FingerGestures/InputProviderEvent
struct InputProviderEvent_t3551969339;

#include "codegen/il2cpp-codegen.h"

// System.Void FingerGestures/InputProviderEvent::.ctor()
extern "C"  void InputProviderEvent__ctor_m4257951632 (InputProviderEvent_t3551969339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
