﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FingerClusterManager
struct FingerClusterManager_t3376029756;
// FingerGestures/IFingerList
struct IFingerList_t1026994100;
// System.Collections.Generic.List`1<FingerClusterManager/Cluster>
struct List_1_t943449559;
// FingerClusterManager/Cluster
struct Cluster_t3870231303;
// FingerGestures/Finger
struct Finger_t182428197;
// FingerGestures/FingerList
struct FingerList_t1886137443;
// FingerGestures
struct FingerGestures_t2907604723;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_FingerGestures_Finger182428197.h"
#include "AssemblyU2DCSharp_FingerGestures_FingerList1886137443.h"
#include "AssemblyU2DCSharp_FingerClusterManager3376029756.h"

// System.Void FingerClusterManager::.ctor()
extern "C"  void FingerClusterManager__ctor_m2122890719 (FingerClusterManager_t3376029756 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FingerGestures/IFingerList FingerClusterManager::get_FingersAdded()
extern "C"  Il2CppObject * FingerClusterManager_get_FingersAdded_m2512966733 (FingerClusterManager_t3376029756 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FingerGestures/IFingerList FingerClusterManager::get_FingersRemoved()
extern "C"  Il2CppObject * FingerClusterManager_get_FingersRemoved_m1972654253 (FingerClusterManager_t3376029756 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<FingerClusterManager/Cluster> FingerClusterManager::get_Clusters()
extern "C"  List_1_t943449559 * FingerClusterManager_get_Clusters_m3950141729 (FingerClusterManager_t3376029756 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<FingerClusterManager/Cluster> FingerClusterManager::GetClustersPool()
extern "C"  List_1_t943449559 * FingerClusterManager_GetClustersPool_m1079340366 (FingerClusterManager_t3376029756 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FingerClusterManager::Awake()
extern "C"  void FingerClusterManager_Awake_m2360495938 (FingerClusterManager_t3376029756 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FingerClusterManager::Update()
extern "C"  void FingerClusterManager_Update_m3111964942 (FingerClusterManager_t3376029756 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FingerClusterManager/Cluster FingerClusterManager::FindClusterById(System.Int32)
extern "C"  Cluster_t3870231303 * FingerClusterManager_FindClusterById_m1641108523 (FingerClusterManager_t3376029756 * __this, int32_t ___clusterId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FingerClusterManager/Cluster FingerClusterManager::NewCluster()
extern "C"  Cluster_t3870231303 * FingerClusterManager_NewCluster_m2942243541 (FingerClusterManager_t3376029756 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FingerClusterManager/Cluster FingerClusterManager::FindExistingCluster(FingerGestures/Finger)
extern "C"  Cluster_t3870231303 * FingerClusterManager_FindExistingCluster_m3357250480 (FingerClusterManager_t3376029756 * __this, Finger_t182428197 * ___finger0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FingerClusterManager::ilo_Clear1(FingerGestures/FingerList)
extern "C"  void FingerClusterManager_ilo_Clear1_m2254485875 (Il2CppObject * __this /* static, unused */, FingerList_t1886137443 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FingerGestures/Finger FingerClusterManager::ilo_GetFinger2(System.Int32)
extern "C"  Finger_t182428197 * FingerClusterManager_ilo_GetFinger2_m2915182282 (Il2CppObject * __this /* static, unused */, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FingerClusterManager::ilo_get_IsDown3(FingerGestures/Finger)
extern "C"  bool FingerClusterManager_ilo_get_IsDown3_m875854553 (Il2CppObject * __this /* static, unused */, Finger_t182428197 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FingerClusterManager::ilo_get_WasDown4(FingerGestures/Finger)
extern "C"  bool FingerClusterManager_ilo_get_WasDown4_m841298959 (Il2CppObject * __this /* static, unused */, Finger_t182428197 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FingerGestures FingerClusterManager::ilo_get_Instance5()
extern "C"  FingerGestures_t2907604723 * FingerClusterManager_ilo_get_Instance5_m632552471 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 FingerClusterManager::ilo_get_Count6(FingerGestures/FingerList)
extern "C"  int32_t FingerClusterManager_ilo_get_Count6_m4249529113 (Il2CppObject * __this /* static, unused */, FingerList_t1886137443 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FingerClusterManager/Cluster FingerClusterManager::ilo_FindExistingCluster7(FingerClusterManager,FingerGestures/Finger)
extern "C"  Cluster_t3870231303 * FingerClusterManager_ilo_FindExistingCluster7_m3507080122 (Il2CppObject * __this /* static, unused */, FingerClusterManager_t3376029756 * ____this0, Finger_t182428197 * ___finger1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FingerClusterManager::ilo_Add8(FingerGestures/FingerList,FingerGestures/Finger)
extern "C"  void FingerClusterManager_ilo_Add8_m1315165703 (Il2CppObject * __this /* static, unused */, FingerList_t1886137443 * ____this0, Finger_t182428197 * ___touch1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
