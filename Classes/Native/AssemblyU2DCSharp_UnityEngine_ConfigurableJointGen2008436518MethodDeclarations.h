﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_ConfigurableJointGenerated
struct UnityEngine_ConfigurableJointGenerated_t2008436518;
// JSVCall
struct JSVCall_t3708497963;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void UnityEngine_ConfigurableJointGenerated::.ctor()
extern "C"  void UnityEngine_ConfigurableJointGenerated__ctor_m3488243765 (UnityEngine_ConfigurableJointGenerated_t2008436518 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ConfigurableJointGenerated::ConfigurableJoint_ConfigurableJoint1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ConfigurableJointGenerated_ConfigurableJoint_ConfigurableJoint1_m550725249 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ConfigurableJointGenerated::ConfigurableJoint_secondaryAxis(JSVCall)
extern "C"  void UnityEngine_ConfigurableJointGenerated_ConfigurableJoint_secondaryAxis_m489635597 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ConfigurableJointGenerated::ConfigurableJoint_xMotion(JSVCall)
extern "C"  void UnityEngine_ConfigurableJointGenerated_ConfigurableJoint_xMotion_m4082413844 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ConfigurableJointGenerated::ConfigurableJoint_yMotion(JSVCall)
extern "C"  void UnityEngine_ConfigurableJointGenerated_ConfigurableJoint_yMotion_m3571879667 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ConfigurableJointGenerated::ConfigurableJoint_zMotion(JSVCall)
extern "C"  void UnityEngine_ConfigurableJointGenerated_ConfigurableJoint_zMotion_m3061345490 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ConfigurableJointGenerated::ConfigurableJoint_angularXMotion(JSVCall)
extern "C"  void UnityEngine_ConfigurableJointGenerated_ConfigurableJoint_angularXMotion_m1076980574 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ConfigurableJointGenerated::ConfigurableJoint_angularYMotion(JSVCall)
extern "C"  void UnityEngine_ConfigurableJointGenerated_ConfigurableJoint_angularYMotion_m566446397 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ConfigurableJointGenerated::ConfigurableJoint_angularZMotion(JSVCall)
extern "C"  void UnityEngine_ConfigurableJointGenerated_ConfigurableJoint_angularZMotion_m55912220 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ConfigurableJointGenerated::ConfigurableJoint_linearLimitSpring(JSVCall)
extern "C"  void UnityEngine_ConfigurableJointGenerated_ConfigurableJoint_linearLimitSpring_m4260123327 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ConfigurableJointGenerated::ConfigurableJoint_angularXLimitSpring(JSVCall)
extern "C"  void UnityEngine_ConfigurableJointGenerated_ConfigurableJoint_angularXLimitSpring_m1549986000 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ConfigurableJointGenerated::ConfigurableJoint_angularYZLimitSpring(JSVCall)
extern "C"  void UnityEngine_ConfigurableJointGenerated_ConfigurableJoint_angularYZLimitSpring_m1150522725 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ConfigurableJointGenerated::ConfigurableJoint_linearLimit(JSVCall)
extern "C"  void UnityEngine_ConfigurableJointGenerated_ConfigurableJoint_linearLimit_m3501664972 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ConfigurableJointGenerated::ConfigurableJoint_lowAngularXLimit(JSVCall)
extern "C"  void UnityEngine_ConfigurableJointGenerated_ConfigurableJoint_lowAngularXLimit_m2104222297 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ConfigurableJointGenerated::ConfigurableJoint_highAngularXLimit(JSVCall)
extern "C"  void UnityEngine_ConfigurableJointGenerated_ConfigurableJoint_highAngularXLimit_m747026399 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ConfigurableJointGenerated::ConfigurableJoint_angularYLimit(JSVCall)
extern "C"  void UnityEngine_ConfigurableJointGenerated_ConfigurableJoint_angularYLimit_m1928547294 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ConfigurableJointGenerated::ConfigurableJoint_angularZLimit(JSVCall)
extern "C"  void UnityEngine_ConfigurableJointGenerated_ConfigurableJoint_angularZLimit_m3436099103 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ConfigurableJointGenerated::ConfigurableJoint_targetPosition(JSVCall)
extern "C"  void UnityEngine_ConfigurableJointGenerated_ConfigurableJoint_targetPosition_m1558429520 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ConfigurableJointGenerated::ConfigurableJoint_targetVelocity(JSVCall)
extern "C"  void UnityEngine_ConfigurableJointGenerated_ConfigurableJoint_targetVelocity_m3628795420 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ConfigurableJointGenerated::ConfigurableJoint_xDrive(JSVCall)
extern "C"  void UnityEngine_ConfigurableJointGenerated_ConfigurableJoint_xDrive_m4268742712 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ConfigurableJointGenerated::ConfigurableJoint_yDrive(JSVCall)
extern "C"  void UnityEngine_ConfigurableJointGenerated_ConfigurableJoint_yDrive_m1481327225 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ConfigurableJointGenerated::ConfigurableJoint_zDrive(JSVCall)
extern "C"  void UnityEngine_ConfigurableJointGenerated_ConfigurableJoint_zDrive_m2988879034 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ConfigurableJointGenerated::ConfigurableJoint_targetRotation(JSVCall)
extern "C"  void UnityEngine_ConfigurableJointGenerated_ConfigurableJoint_targetRotation_m1227135131 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ConfigurableJointGenerated::ConfigurableJoint_targetAngularVelocity(JSVCall)
extern "C"  void UnityEngine_ConfigurableJointGenerated_ConfigurableJoint_targetAngularVelocity_m2325306196 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ConfigurableJointGenerated::ConfigurableJoint_rotationDriveMode(JSVCall)
extern "C"  void UnityEngine_ConfigurableJointGenerated_ConfigurableJoint_rotationDriveMode_m1425044755 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ConfigurableJointGenerated::ConfigurableJoint_angularXDrive(JSVCall)
extern "C"  void UnityEngine_ConfigurableJointGenerated_ConfigurableJoint_angularXDrive_m2370677934 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ConfigurableJointGenerated::ConfigurableJoint_angularYZDrive(JSVCall)
extern "C"  void UnityEngine_ConfigurableJointGenerated_ConfigurableJoint_angularYZDrive_m1615637763 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ConfigurableJointGenerated::ConfigurableJoint_slerpDrive(JSVCall)
extern "C"  void UnityEngine_ConfigurableJointGenerated_ConfigurableJoint_slerpDrive_m2855102122 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ConfigurableJointGenerated::ConfigurableJoint_projectionMode(JSVCall)
extern "C"  void UnityEngine_ConfigurableJointGenerated_ConfigurableJoint_projectionMode_m2459165592 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ConfigurableJointGenerated::ConfigurableJoint_projectionDistance(JSVCall)
extern "C"  void UnityEngine_ConfigurableJointGenerated_ConfigurableJoint_projectionDistance_m2158091014 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ConfigurableJointGenerated::ConfigurableJoint_projectionAngle(JSVCall)
extern "C"  void UnityEngine_ConfigurableJointGenerated_ConfigurableJoint_projectionAngle_m3252827230 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ConfigurableJointGenerated::ConfigurableJoint_configuredInWorldSpace(JSVCall)
extern "C"  void UnityEngine_ConfigurableJointGenerated_ConfigurableJoint_configuredInWorldSpace_m461367507 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ConfigurableJointGenerated::ConfigurableJoint_swapBodies(JSVCall)
extern "C"  void UnityEngine_ConfigurableJointGenerated_ConfigurableJoint_swapBodies_m1327801879 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ConfigurableJointGenerated::__Register()
extern "C"  void UnityEngine_ConfigurableJointGenerated___Register_m597947826 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_ConfigurableJointGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UnityEngine_ConfigurableJointGenerated_ilo_getObject1_m720806781 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ConfigurableJointGenerated::ilo_setVector3S2(System.Int32,UnityEngine.Vector3)
extern "C"  void UnityEngine_ConfigurableJointGenerated_ilo_setVector3S2_m891823795 (Il2CppObject * __this /* static, unused */, int32_t ___e0, Vector3_t4282066566  ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ConfigurableJointGenerated::ilo_setEnum3(System.Int32,System.Int32)
extern "C"  void UnityEngine_ConfigurableJointGenerated_ilo_setEnum3_m1209821332 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_ConfigurableJointGenerated::ilo_getEnum4(System.Int32)
extern "C"  int32_t UnityEngine_ConfigurableJointGenerated_ilo_getEnum4_m786658942 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_ConfigurableJointGenerated::ilo_setObject5(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UnityEngine_ConfigurableJointGenerated_ilo_setObject5_m2734138361 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_ConfigurableJointGenerated::ilo_getObject6(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_ConfigurableJointGenerated_ilo_getObject6_m2227022431 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine_ConfigurableJointGenerated::ilo_getSingle7(System.Int32)
extern "C"  float UnityEngine_ConfigurableJointGenerated_ilo_getSingle7_m4062537880 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
