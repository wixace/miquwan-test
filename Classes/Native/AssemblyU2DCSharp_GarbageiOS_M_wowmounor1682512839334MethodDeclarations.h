﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_wowmounor168
struct M_wowmounor168_t2512839334;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"

// System.Void GarbageiOS.M_wowmounor168::.ctor()
extern "C"  void M_wowmounor168__ctor_m2020862541 (M_wowmounor168_t2512839334 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_wowmounor168::M_bebaroNouba0(System.String[],System.Int32)
extern "C"  void M_wowmounor168_M_bebaroNouba0_m1219433852 (M_wowmounor168_t2512839334 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_wowmounor168::M_felsolereSafirjo1(System.String[],System.Int32)
extern "C"  void M_wowmounor168_M_felsolereSafirjo1_m2050767472 (M_wowmounor168_t2512839334 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_wowmounor168::M_zairnarRergo2(System.String[],System.Int32)
extern "C"  void M_wowmounor168_M_zairnarRergo2_m3183552010 (M_wowmounor168_t2512839334 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
