﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_jasuse121
struct  M_jasuse121_t2879802277  : public Il2CppObject
{
public:
	// System.Boolean GarbageiOS.M_jasuse121::_hiliXerketai
	bool ____hiliXerketai_0;
	// System.Boolean GarbageiOS.M_jasuse121::_sedalCalgair
	bool ____sedalCalgair_1;
	// System.UInt32 GarbageiOS.M_jasuse121::_bemouBirmur
	uint32_t ____bemouBirmur_2;
	// System.String GarbageiOS.M_jasuse121::_dajaForsoutra
	String_t* ____dajaForsoutra_3;
	// System.Int32 GarbageiOS.M_jasuse121::_demzuCisem
	int32_t ____demzuCisem_4;
	// System.String GarbageiOS.M_jasuse121::_dipasJoza
	String_t* ____dipasJoza_5;
	// System.Int32 GarbageiOS.M_jasuse121::_missisTowhelow
	int32_t ____missisTowhelow_6;
	// System.Boolean GarbageiOS.M_jasuse121::_kireatasMalllegoo
	bool ____kireatasMalllegoo_7;
	// System.String GarbageiOS.M_jasuse121::_xeabem
	String_t* ____xeabem_8;
	// System.Int32 GarbageiOS.M_jasuse121::_bawsemJeajairmou
	int32_t ____bawsemJeajairmou_9;
	// System.Int32 GarbageiOS.M_jasuse121::_searsuCasirbea
	int32_t ____searsuCasirbea_10;
	// System.Single GarbageiOS.M_jasuse121::_rassawsiKugime
	float ____rassawsiKugime_11;
	// System.UInt32 GarbageiOS.M_jasuse121::_dirjarmisDrugi
	uint32_t ____dirjarmisDrugi_12;
	// System.Boolean GarbageiOS.M_jasuse121::_maryairSemnay
	bool ____maryairSemnay_13;
	// System.Int32 GarbageiOS.M_jasuse121::_zowbouxaSeasoyoo
	int32_t ____zowbouxaSeasoyoo_14;
	// System.Boolean GarbageiOS.M_jasuse121::_taje
	bool ____taje_15;
	// System.Single GarbageiOS.M_jasuse121::_kealou
	float ____kealou_16;
	// System.Boolean GarbageiOS.M_jasuse121::_nimawWhakoro
	bool ____nimawWhakoro_17;
	// System.UInt32 GarbageiOS.M_jasuse121::_jaqissairBereargi
	uint32_t ____jaqissairBereargi_18;
	// System.Single GarbageiOS.M_jasuse121::_wemi
	float ____wemi_19;
	// System.Single GarbageiOS.M_jasuse121::_mataiPowroha
	float ____mataiPowroha_20;
	// System.String GarbageiOS.M_jasuse121::_dralrowNemkea
	String_t* ____dralrowNemkea_21;
	// System.Single GarbageiOS.M_jasuse121::_luborQarou
	float ____luborQarou_22;
	// System.UInt32 GarbageiOS.M_jasuse121::_rite
	uint32_t ____rite_23;
	// System.String GarbageiOS.M_jasuse121::_callheahaw
	String_t* ____callheahaw_24;
	// System.Single GarbageiOS.M_jasuse121::_pimaLoujisqaw
	float ____pimaLoujisqaw_25;
	// System.Boolean GarbageiOS.M_jasuse121::_leastor
	bool ____leastor_26;
	// System.UInt32 GarbageiOS.M_jasuse121::_cipelJearkere
	uint32_t ____cipelJearkere_27;
	// System.Single GarbageiOS.M_jasuse121::_xersiSuxe
	float ____xersiSuxe_28;

public:
	inline static int32_t get_offset_of__hiliXerketai_0() { return static_cast<int32_t>(offsetof(M_jasuse121_t2879802277, ____hiliXerketai_0)); }
	inline bool get__hiliXerketai_0() const { return ____hiliXerketai_0; }
	inline bool* get_address_of__hiliXerketai_0() { return &____hiliXerketai_0; }
	inline void set__hiliXerketai_0(bool value)
	{
		____hiliXerketai_0 = value;
	}

	inline static int32_t get_offset_of__sedalCalgair_1() { return static_cast<int32_t>(offsetof(M_jasuse121_t2879802277, ____sedalCalgair_1)); }
	inline bool get__sedalCalgair_1() const { return ____sedalCalgair_1; }
	inline bool* get_address_of__sedalCalgair_1() { return &____sedalCalgair_1; }
	inline void set__sedalCalgair_1(bool value)
	{
		____sedalCalgair_1 = value;
	}

	inline static int32_t get_offset_of__bemouBirmur_2() { return static_cast<int32_t>(offsetof(M_jasuse121_t2879802277, ____bemouBirmur_2)); }
	inline uint32_t get__bemouBirmur_2() const { return ____bemouBirmur_2; }
	inline uint32_t* get_address_of__bemouBirmur_2() { return &____bemouBirmur_2; }
	inline void set__bemouBirmur_2(uint32_t value)
	{
		____bemouBirmur_2 = value;
	}

	inline static int32_t get_offset_of__dajaForsoutra_3() { return static_cast<int32_t>(offsetof(M_jasuse121_t2879802277, ____dajaForsoutra_3)); }
	inline String_t* get__dajaForsoutra_3() const { return ____dajaForsoutra_3; }
	inline String_t** get_address_of__dajaForsoutra_3() { return &____dajaForsoutra_3; }
	inline void set__dajaForsoutra_3(String_t* value)
	{
		____dajaForsoutra_3 = value;
		Il2CppCodeGenWriteBarrier(&____dajaForsoutra_3, value);
	}

	inline static int32_t get_offset_of__demzuCisem_4() { return static_cast<int32_t>(offsetof(M_jasuse121_t2879802277, ____demzuCisem_4)); }
	inline int32_t get__demzuCisem_4() const { return ____demzuCisem_4; }
	inline int32_t* get_address_of__demzuCisem_4() { return &____demzuCisem_4; }
	inline void set__demzuCisem_4(int32_t value)
	{
		____demzuCisem_4 = value;
	}

	inline static int32_t get_offset_of__dipasJoza_5() { return static_cast<int32_t>(offsetof(M_jasuse121_t2879802277, ____dipasJoza_5)); }
	inline String_t* get__dipasJoza_5() const { return ____dipasJoza_5; }
	inline String_t** get_address_of__dipasJoza_5() { return &____dipasJoza_5; }
	inline void set__dipasJoza_5(String_t* value)
	{
		____dipasJoza_5 = value;
		Il2CppCodeGenWriteBarrier(&____dipasJoza_5, value);
	}

	inline static int32_t get_offset_of__missisTowhelow_6() { return static_cast<int32_t>(offsetof(M_jasuse121_t2879802277, ____missisTowhelow_6)); }
	inline int32_t get__missisTowhelow_6() const { return ____missisTowhelow_6; }
	inline int32_t* get_address_of__missisTowhelow_6() { return &____missisTowhelow_6; }
	inline void set__missisTowhelow_6(int32_t value)
	{
		____missisTowhelow_6 = value;
	}

	inline static int32_t get_offset_of__kireatasMalllegoo_7() { return static_cast<int32_t>(offsetof(M_jasuse121_t2879802277, ____kireatasMalllegoo_7)); }
	inline bool get__kireatasMalllegoo_7() const { return ____kireatasMalllegoo_7; }
	inline bool* get_address_of__kireatasMalllegoo_7() { return &____kireatasMalllegoo_7; }
	inline void set__kireatasMalllegoo_7(bool value)
	{
		____kireatasMalllegoo_7 = value;
	}

	inline static int32_t get_offset_of__xeabem_8() { return static_cast<int32_t>(offsetof(M_jasuse121_t2879802277, ____xeabem_8)); }
	inline String_t* get__xeabem_8() const { return ____xeabem_8; }
	inline String_t** get_address_of__xeabem_8() { return &____xeabem_8; }
	inline void set__xeabem_8(String_t* value)
	{
		____xeabem_8 = value;
		Il2CppCodeGenWriteBarrier(&____xeabem_8, value);
	}

	inline static int32_t get_offset_of__bawsemJeajairmou_9() { return static_cast<int32_t>(offsetof(M_jasuse121_t2879802277, ____bawsemJeajairmou_9)); }
	inline int32_t get__bawsemJeajairmou_9() const { return ____bawsemJeajairmou_9; }
	inline int32_t* get_address_of__bawsemJeajairmou_9() { return &____bawsemJeajairmou_9; }
	inline void set__bawsemJeajairmou_9(int32_t value)
	{
		____bawsemJeajairmou_9 = value;
	}

	inline static int32_t get_offset_of__searsuCasirbea_10() { return static_cast<int32_t>(offsetof(M_jasuse121_t2879802277, ____searsuCasirbea_10)); }
	inline int32_t get__searsuCasirbea_10() const { return ____searsuCasirbea_10; }
	inline int32_t* get_address_of__searsuCasirbea_10() { return &____searsuCasirbea_10; }
	inline void set__searsuCasirbea_10(int32_t value)
	{
		____searsuCasirbea_10 = value;
	}

	inline static int32_t get_offset_of__rassawsiKugime_11() { return static_cast<int32_t>(offsetof(M_jasuse121_t2879802277, ____rassawsiKugime_11)); }
	inline float get__rassawsiKugime_11() const { return ____rassawsiKugime_11; }
	inline float* get_address_of__rassawsiKugime_11() { return &____rassawsiKugime_11; }
	inline void set__rassawsiKugime_11(float value)
	{
		____rassawsiKugime_11 = value;
	}

	inline static int32_t get_offset_of__dirjarmisDrugi_12() { return static_cast<int32_t>(offsetof(M_jasuse121_t2879802277, ____dirjarmisDrugi_12)); }
	inline uint32_t get__dirjarmisDrugi_12() const { return ____dirjarmisDrugi_12; }
	inline uint32_t* get_address_of__dirjarmisDrugi_12() { return &____dirjarmisDrugi_12; }
	inline void set__dirjarmisDrugi_12(uint32_t value)
	{
		____dirjarmisDrugi_12 = value;
	}

	inline static int32_t get_offset_of__maryairSemnay_13() { return static_cast<int32_t>(offsetof(M_jasuse121_t2879802277, ____maryairSemnay_13)); }
	inline bool get__maryairSemnay_13() const { return ____maryairSemnay_13; }
	inline bool* get_address_of__maryairSemnay_13() { return &____maryairSemnay_13; }
	inline void set__maryairSemnay_13(bool value)
	{
		____maryairSemnay_13 = value;
	}

	inline static int32_t get_offset_of__zowbouxaSeasoyoo_14() { return static_cast<int32_t>(offsetof(M_jasuse121_t2879802277, ____zowbouxaSeasoyoo_14)); }
	inline int32_t get__zowbouxaSeasoyoo_14() const { return ____zowbouxaSeasoyoo_14; }
	inline int32_t* get_address_of__zowbouxaSeasoyoo_14() { return &____zowbouxaSeasoyoo_14; }
	inline void set__zowbouxaSeasoyoo_14(int32_t value)
	{
		____zowbouxaSeasoyoo_14 = value;
	}

	inline static int32_t get_offset_of__taje_15() { return static_cast<int32_t>(offsetof(M_jasuse121_t2879802277, ____taje_15)); }
	inline bool get__taje_15() const { return ____taje_15; }
	inline bool* get_address_of__taje_15() { return &____taje_15; }
	inline void set__taje_15(bool value)
	{
		____taje_15 = value;
	}

	inline static int32_t get_offset_of__kealou_16() { return static_cast<int32_t>(offsetof(M_jasuse121_t2879802277, ____kealou_16)); }
	inline float get__kealou_16() const { return ____kealou_16; }
	inline float* get_address_of__kealou_16() { return &____kealou_16; }
	inline void set__kealou_16(float value)
	{
		____kealou_16 = value;
	}

	inline static int32_t get_offset_of__nimawWhakoro_17() { return static_cast<int32_t>(offsetof(M_jasuse121_t2879802277, ____nimawWhakoro_17)); }
	inline bool get__nimawWhakoro_17() const { return ____nimawWhakoro_17; }
	inline bool* get_address_of__nimawWhakoro_17() { return &____nimawWhakoro_17; }
	inline void set__nimawWhakoro_17(bool value)
	{
		____nimawWhakoro_17 = value;
	}

	inline static int32_t get_offset_of__jaqissairBereargi_18() { return static_cast<int32_t>(offsetof(M_jasuse121_t2879802277, ____jaqissairBereargi_18)); }
	inline uint32_t get__jaqissairBereargi_18() const { return ____jaqissairBereargi_18; }
	inline uint32_t* get_address_of__jaqissairBereargi_18() { return &____jaqissairBereargi_18; }
	inline void set__jaqissairBereargi_18(uint32_t value)
	{
		____jaqissairBereargi_18 = value;
	}

	inline static int32_t get_offset_of__wemi_19() { return static_cast<int32_t>(offsetof(M_jasuse121_t2879802277, ____wemi_19)); }
	inline float get__wemi_19() const { return ____wemi_19; }
	inline float* get_address_of__wemi_19() { return &____wemi_19; }
	inline void set__wemi_19(float value)
	{
		____wemi_19 = value;
	}

	inline static int32_t get_offset_of__mataiPowroha_20() { return static_cast<int32_t>(offsetof(M_jasuse121_t2879802277, ____mataiPowroha_20)); }
	inline float get__mataiPowroha_20() const { return ____mataiPowroha_20; }
	inline float* get_address_of__mataiPowroha_20() { return &____mataiPowroha_20; }
	inline void set__mataiPowroha_20(float value)
	{
		____mataiPowroha_20 = value;
	}

	inline static int32_t get_offset_of__dralrowNemkea_21() { return static_cast<int32_t>(offsetof(M_jasuse121_t2879802277, ____dralrowNemkea_21)); }
	inline String_t* get__dralrowNemkea_21() const { return ____dralrowNemkea_21; }
	inline String_t** get_address_of__dralrowNemkea_21() { return &____dralrowNemkea_21; }
	inline void set__dralrowNemkea_21(String_t* value)
	{
		____dralrowNemkea_21 = value;
		Il2CppCodeGenWriteBarrier(&____dralrowNemkea_21, value);
	}

	inline static int32_t get_offset_of__luborQarou_22() { return static_cast<int32_t>(offsetof(M_jasuse121_t2879802277, ____luborQarou_22)); }
	inline float get__luborQarou_22() const { return ____luborQarou_22; }
	inline float* get_address_of__luborQarou_22() { return &____luborQarou_22; }
	inline void set__luborQarou_22(float value)
	{
		____luborQarou_22 = value;
	}

	inline static int32_t get_offset_of__rite_23() { return static_cast<int32_t>(offsetof(M_jasuse121_t2879802277, ____rite_23)); }
	inline uint32_t get__rite_23() const { return ____rite_23; }
	inline uint32_t* get_address_of__rite_23() { return &____rite_23; }
	inline void set__rite_23(uint32_t value)
	{
		____rite_23 = value;
	}

	inline static int32_t get_offset_of__callheahaw_24() { return static_cast<int32_t>(offsetof(M_jasuse121_t2879802277, ____callheahaw_24)); }
	inline String_t* get__callheahaw_24() const { return ____callheahaw_24; }
	inline String_t** get_address_of__callheahaw_24() { return &____callheahaw_24; }
	inline void set__callheahaw_24(String_t* value)
	{
		____callheahaw_24 = value;
		Il2CppCodeGenWriteBarrier(&____callheahaw_24, value);
	}

	inline static int32_t get_offset_of__pimaLoujisqaw_25() { return static_cast<int32_t>(offsetof(M_jasuse121_t2879802277, ____pimaLoujisqaw_25)); }
	inline float get__pimaLoujisqaw_25() const { return ____pimaLoujisqaw_25; }
	inline float* get_address_of__pimaLoujisqaw_25() { return &____pimaLoujisqaw_25; }
	inline void set__pimaLoujisqaw_25(float value)
	{
		____pimaLoujisqaw_25 = value;
	}

	inline static int32_t get_offset_of__leastor_26() { return static_cast<int32_t>(offsetof(M_jasuse121_t2879802277, ____leastor_26)); }
	inline bool get__leastor_26() const { return ____leastor_26; }
	inline bool* get_address_of__leastor_26() { return &____leastor_26; }
	inline void set__leastor_26(bool value)
	{
		____leastor_26 = value;
	}

	inline static int32_t get_offset_of__cipelJearkere_27() { return static_cast<int32_t>(offsetof(M_jasuse121_t2879802277, ____cipelJearkere_27)); }
	inline uint32_t get__cipelJearkere_27() const { return ____cipelJearkere_27; }
	inline uint32_t* get_address_of__cipelJearkere_27() { return &____cipelJearkere_27; }
	inline void set__cipelJearkere_27(uint32_t value)
	{
		____cipelJearkere_27 = value;
	}

	inline static int32_t get_offset_of__xersiSuxe_28() { return static_cast<int32_t>(offsetof(M_jasuse121_t2879802277, ____xersiSuxe_28)); }
	inline float get__xersiSuxe_28() const { return ____xersiSuxe_28; }
	inline float* get_address_of__xersiSuxe_28() { return &____xersiSuxe_28; }
	inline void set__xersiSuxe_28(float value)
	{
		____xersiSuxe_28 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
