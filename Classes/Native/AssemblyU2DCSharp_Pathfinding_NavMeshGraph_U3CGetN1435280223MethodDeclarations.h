﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.NavMeshGraph/<GetNearestForceBoth>c__AnonStorey117
struct U3CGetNearestForceBothU3Ec__AnonStorey117_t1435280223;
// Pathfinding.GraphNode
struct GraphNode_t23612370;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphNode23612370.h"

// System.Void Pathfinding.NavMeshGraph/<GetNearestForceBoth>c__AnonStorey117::.ctor()
extern "C"  void U3CGetNearestForceBothU3Ec__AnonStorey117__ctor_m1478489116 (U3CGetNearestForceBothU3Ec__AnonStorey117_t1435280223 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.NavMeshGraph/<GetNearestForceBoth>c__AnonStorey117::<>m__34C(Pathfinding.GraphNode)
extern "C"  bool U3CGetNearestForceBothU3Ec__AnonStorey117_U3CU3Em__34C_m2521683705 (U3CGetNearestForceBothU3Ec__AnonStorey117_t1435280223 * __this, GraphNode_t23612370 * ____node0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
