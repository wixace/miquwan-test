﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SevenZip.Compression.LZMA.Decoder/LenDecoder
struct LenDecoder_t4003695492;
// SevenZip.Compression.RangeCoder.Decoder
struct Decoder_t1102840654;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SevenZip_Compression_RangeCoder_1102840654.h"

// System.Void SevenZip.Compression.LZMA.Decoder/LenDecoder::.ctor()
extern "C"  void LenDecoder__ctor_m2732637079 (LenDecoder_t4003695492 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.LZMA.Decoder/LenDecoder::Create(System.UInt32)
extern "C"  void LenDecoder_Create_m1515433425 (LenDecoder_t4003695492 * __this, uint32_t ___numPosStates0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.LZMA.Decoder/LenDecoder::Init()
extern "C"  void LenDecoder_Init_m1009517533 (LenDecoder_t4003695492 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 SevenZip.Compression.LZMA.Decoder/LenDecoder::Decode(SevenZip.Compression.RangeCoder.Decoder,System.UInt32)
extern "C"  uint32_t LenDecoder_Decode_m4112036016 (LenDecoder_t4003695492 * __this, Decoder_t1102840654 * ___rangeDecoder0, uint32_t ___posState1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
