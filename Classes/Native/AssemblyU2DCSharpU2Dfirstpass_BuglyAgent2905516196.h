﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Func`1<System.Collections.Generic.Dictionary`2<System.String,System.String>>
struct Func_1_t1952811501;
// BuglyAgent/LogCallbackDelegate
struct LogCallbackDelegate_t45039171;

#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharpU2Dfirstpass_LogSeverity591216193.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BuglyAgent
struct  BuglyAgent_t2905516196  : public Il2CppObject
{
public:

public:
};

struct BuglyAgent_t2905516196_StaticFields
{
public:
	// System.Boolean BuglyAgent::_crashReporterTypeConfiged
	bool ____crashReporterTypeConfiged_0;
	// System.Boolean BuglyAgent::_isInitialized
	bool ____isInitialized_1;
	// LogSeverity BuglyAgent::_autoReportLogLevel
	int32_t ____autoReportLogLevel_2;
	// System.Int32 BuglyAgent::_crashReporterType
	int32_t ____crashReporterType_3;
	// System.Int32 BuglyAgent::_crashReproterCustomizedLogLevel
	int32_t ____crashReproterCustomizedLogLevel_4;
	// System.Boolean BuglyAgent::_debugMode
	bool ____debugMode_5;
	// System.Boolean BuglyAgent::_autoQuitApplicationAfterReport
	bool ____autoQuitApplicationAfterReport_6;
	// System.Int32 BuglyAgent::EXCEPTION_TYPE_UNCAUGHT
	int32_t ___EXCEPTION_TYPE_UNCAUGHT_7;
	// System.Int32 BuglyAgent::EXCEPTION_TYPE_CAUGHT
	int32_t ___EXCEPTION_TYPE_CAUGHT_8;
	// System.String BuglyAgent::_pluginVersion
	String_t* ____pluginVersion_9;
	// System.Func`1<System.Collections.Generic.Dictionary`2<System.String,System.String>> BuglyAgent::_LogCallbackExtrasHandler
	Func_1_t1952811501 * ____LogCallbackExtrasHandler_10;
	// System.Boolean BuglyAgent::_uncaughtAutoReportOnce
	bool ____uncaughtAutoReportOnce_11;
	// BuglyAgent/LogCallbackDelegate BuglyAgent::_LogCallbackEventHandler
	LogCallbackDelegate_t45039171 * ____LogCallbackEventHandler_12;

public:
	inline static int32_t get_offset_of__crashReporterTypeConfiged_0() { return static_cast<int32_t>(offsetof(BuglyAgent_t2905516196_StaticFields, ____crashReporterTypeConfiged_0)); }
	inline bool get__crashReporterTypeConfiged_0() const { return ____crashReporterTypeConfiged_0; }
	inline bool* get_address_of__crashReporterTypeConfiged_0() { return &____crashReporterTypeConfiged_0; }
	inline void set__crashReporterTypeConfiged_0(bool value)
	{
		____crashReporterTypeConfiged_0 = value;
	}

	inline static int32_t get_offset_of__isInitialized_1() { return static_cast<int32_t>(offsetof(BuglyAgent_t2905516196_StaticFields, ____isInitialized_1)); }
	inline bool get__isInitialized_1() const { return ____isInitialized_1; }
	inline bool* get_address_of__isInitialized_1() { return &____isInitialized_1; }
	inline void set__isInitialized_1(bool value)
	{
		____isInitialized_1 = value;
	}

	inline static int32_t get_offset_of__autoReportLogLevel_2() { return static_cast<int32_t>(offsetof(BuglyAgent_t2905516196_StaticFields, ____autoReportLogLevel_2)); }
	inline int32_t get__autoReportLogLevel_2() const { return ____autoReportLogLevel_2; }
	inline int32_t* get_address_of__autoReportLogLevel_2() { return &____autoReportLogLevel_2; }
	inline void set__autoReportLogLevel_2(int32_t value)
	{
		____autoReportLogLevel_2 = value;
	}

	inline static int32_t get_offset_of__crashReporterType_3() { return static_cast<int32_t>(offsetof(BuglyAgent_t2905516196_StaticFields, ____crashReporterType_3)); }
	inline int32_t get__crashReporterType_3() const { return ____crashReporterType_3; }
	inline int32_t* get_address_of__crashReporterType_3() { return &____crashReporterType_3; }
	inline void set__crashReporterType_3(int32_t value)
	{
		____crashReporterType_3 = value;
	}

	inline static int32_t get_offset_of__crashReproterCustomizedLogLevel_4() { return static_cast<int32_t>(offsetof(BuglyAgent_t2905516196_StaticFields, ____crashReproterCustomizedLogLevel_4)); }
	inline int32_t get__crashReproterCustomizedLogLevel_4() const { return ____crashReproterCustomizedLogLevel_4; }
	inline int32_t* get_address_of__crashReproterCustomizedLogLevel_4() { return &____crashReproterCustomizedLogLevel_4; }
	inline void set__crashReproterCustomizedLogLevel_4(int32_t value)
	{
		____crashReproterCustomizedLogLevel_4 = value;
	}

	inline static int32_t get_offset_of__debugMode_5() { return static_cast<int32_t>(offsetof(BuglyAgent_t2905516196_StaticFields, ____debugMode_5)); }
	inline bool get__debugMode_5() const { return ____debugMode_5; }
	inline bool* get_address_of__debugMode_5() { return &____debugMode_5; }
	inline void set__debugMode_5(bool value)
	{
		____debugMode_5 = value;
	}

	inline static int32_t get_offset_of__autoQuitApplicationAfterReport_6() { return static_cast<int32_t>(offsetof(BuglyAgent_t2905516196_StaticFields, ____autoQuitApplicationAfterReport_6)); }
	inline bool get__autoQuitApplicationAfterReport_6() const { return ____autoQuitApplicationAfterReport_6; }
	inline bool* get_address_of__autoQuitApplicationAfterReport_6() { return &____autoQuitApplicationAfterReport_6; }
	inline void set__autoQuitApplicationAfterReport_6(bool value)
	{
		____autoQuitApplicationAfterReport_6 = value;
	}

	inline static int32_t get_offset_of_EXCEPTION_TYPE_UNCAUGHT_7() { return static_cast<int32_t>(offsetof(BuglyAgent_t2905516196_StaticFields, ___EXCEPTION_TYPE_UNCAUGHT_7)); }
	inline int32_t get_EXCEPTION_TYPE_UNCAUGHT_7() const { return ___EXCEPTION_TYPE_UNCAUGHT_7; }
	inline int32_t* get_address_of_EXCEPTION_TYPE_UNCAUGHT_7() { return &___EXCEPTION_TYPE_UNCAUGHT_7; }
	inline void set_EXCEPTION_TYPE_UNCAUGHT_7(int32_t value)
	{
		___EXCEPTION_TYPE_UNCAUGHT_7 = value;
	}

	inline static int32_t get_offset_of_EXCEPTION_TYPE_CAUGHT_8() { return static_cast<int32_t>(offsetof(BuglyAgent_t2905516196_StaticFields, ___EXCEPTION_TYPE_CAUGHT_8)); }
	inline int32_t get_EXCEPTION_TYPE_CAUGHT_8() const { return ___EXCEPTION_TYPE_CAUGHT_8; }
	inline int32_t* get_address_of_EXCEPTION_TYPE_CAUGHT_8() { return &___EXCEPTION_TYPE_CAUGHT_8; }
	inline void set_EXCEPTION_TYPE_CAUGHT_8(int32_t value)
	{
		___EXCEPTION_TYPE_CAUGHT_8 = value;
	}

	inline static int32_t get_offset_of__pluginVersion_9() { return static_cast<int32_t>(offsetof(BuglyAgent_t2905516196_StaticFields, ____pluginVersion_9)); }
	inline String_t* get__pluginVersion_9() const { return ____pluginVersion_9; }
	inline String_t** get_address_of__pluginVersion_9() { return &____pluginVersion_9; }
	inline void set__pluginVersion_9(String_t* value)
	{
		____pluginVersion_9 = value;
		Il2CppCodeGenWriteBarrier(&____pluginVersion_9, value);
	}

	inline static int32_t get_offset_of__LogCallbackExtrasHandler_10() { return static_cast<int32_t>(offsetof(BuglyAgent_t2905516196_StaticFields, ____LogCallbackExtrasHandler_10)); }
	inline Func_1_t1952811501 * get__LogCallbackExtrasHandler_10() const { return ____LogCallbackExtrasHandler_10; }
	inline Func_1_t1952811501 ** get_address_of__LogCallbackExtrasHandler_10() { return &____LogCallbackExtrasHandler_10; }
	inline void set__LogCallbackExtrasHandler_10(Func_1_t1952811501 * value)
	{
		____LogCallbackExtrasHandler_10 = value;
		Il2CppCodeGenWriteBarrier(&____LogCallbackExtrasHandler_10, value);
	}

	inline static int32_t get_offset_of__uncaughtAutoReportOnce_11() { return static_cast<int32_t>(offsetof(BuglyAgent_t2905516196_StaticFields, ____uncaughtAutoReportOnce_11)); }
	inline bool get__uncaughtAutoReportOnce_11() const { return ____uncaughtAutoReportOnce_11; }
	inline bool* get_address_of__uncaughtAutoReportOnce_11() { return &____uncaughtAutoReportOnce_11; }
	inline void set__uncaughtAutoReportOnce_11(bool value)
	{
		____uncaughtAutoReportOnce_11 = value;
	}

	inline static int32_t get_offset_of__LogCallbackEventHandler_12() { return static_cast<int32_t>(offsetof(BuglyAgent_t2905516196_StaticFields, ____LogCallbackEventHandler_12)); }
	inline LogCallbackDelegate_t45039171 * get__LogCallbackEventHandler_12() const { return ____LogCallbackEventHandler_12; }
	inline LogCallbackDelegate_t45039171 ** get_address_of__LogCallbackEventHandler_12() { return &____LogCallbackEventHandler_12; }
	inline void set__LogCallbackEventHandler_12(LogCallbackDelegate_t45039171 * value)
	{
		____LogCallbackEventHandler_12 = value;
		Il2CppCodeGenWriteBarrier(&____LogCallbackEventHandler_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
