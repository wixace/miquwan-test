﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<PushType,System.Object>
struct Dictionary_2_t620834634;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K1235770688.h"
#include "AssemblyU2DCSharp_PushType1840642452.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<PushType,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m3730376003_gshared (Enumerator_t1235770688 * __this, Dictionary_2_t620834634 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m3730376003(__this, ___host0, method) ((  void (*) (Enumerator_t1235770688 *, Dictionary_2_t620834634 *, const MethodInfo*))Enumerator__ctor_m3730376003_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<PushType,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2753332424_gshared (Enumerator_t1235770688 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m2753332424(__this, method) ((  Il2CppObject * (*) (Enumerator_t1235770688 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2753332424_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<PushType,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m850159506_gshared (Enumerator_t1235770688 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m850159506(__this, method) ((  void (*) (Enumerator_t1235770688 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m850159506_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<PushType,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m393900709_gshared (Enumerator_t1235770688 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m393900709(__this, method) ((  void (*) (Enumerator_t1235770688 *, const MethodInfo*))Enumerator_Dispose_m393900709_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<PushType,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2616619458_gshared (Enumerator_t1235770688 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m2616619458(__this, method) ((  bool (*) (Enumerator_t1235770688 *, const MethodInfo*))Enumerator_MoveNext_m2616619458_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<PushType,System.Object>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m3967147798_gshared (Enumerator_t1235770688 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m3967147798(__this, method) ((  int32_t (*) (Enumerator_t1235770688 *, const MethodInfo*))Enumerator_get_Current_m3967147798_gshared)(__this, method)
