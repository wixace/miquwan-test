﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System_Collections_Generic_List77Generated/<ListA1_ForEach_GetDelegate_member22_arg0>c__AnonStorey91`1<System.Object>
struct U3CListA1_ForEach_GetDelegate_member22_arg0U3Ec__AnonStorey91_1_t3044851907;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System_Collections_Generic_List77Generated/<ListA1_ForEach_GetDelegate_member22_arg0>c__AnonStorey91`1<System.Object>::.ctor()
extern "C"  void U3CListA1_ForEach_GetDelegate_member22_arg0U3Ec__AnonStorey91_1__ctor_m3339397458_gshared (U3CListA1_ForEach_GetDelegate_member22_arg0U3Ec__AnonStorey91_1_t3044851907 * __this, const MethodInfo* method);
#define U3CListA1_ForEach_GetDelegate_member22_arg0U3Ec__AnonStorey91_1__ctor_m3339397458(__this, method) ((  void (*) (U3CListA1_ForEach_GetDelegate_member22_arg0U3Ec__AnonStorey91_1_t3044851907 *, const MethodInfo*))U3CListA1_ForEach_GetDelegate_member22_arg0U3Ec__AnonStorey91_1__ctor_m3339397458_gshared)(__this, method)
// System.Void System_Collections_Generic_List77Generated/<ListA1_ForEach_GetDelegate_member22_arg0>c__AnonStorey91`1<System.Object>::<>m__B6(T)
extern "C"  void U3CListA1_ForEach_GetDelegate_member22_arg0U3Ec__AnonStorey91_1_U3CU3Em__B6_m2507836527_gshared (U3CListA1_ForEach_GetDelegate_member22_arg0U3Ec__AnonStorey91_1_t3044851907 * __this, Il2CppObject * ___obj0, const MethodInfo* method);
#define U3CListA1_ForEach_GetDelegate_member22_arg0U3Ec__AnonStorey91_1_U3CU3Em__B6_m2507836527(__this, ___obj0, method) ((  void (*) (U3CListA1_ForEach_GetDelegate_member22_arg0U3Ec__AnonStorey91_1_t3044851907 *, Il2CppObject *, const MethodInfo*))U3CListA1_ForEach_GetDelegate_member22_arg0U3Ec__AnonStorey91_1_U3CU3Em__B6_m2507836527_gshared)(__this, ___obj0, method)
