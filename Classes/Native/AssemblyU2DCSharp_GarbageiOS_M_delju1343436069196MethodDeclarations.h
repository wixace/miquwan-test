﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_delju134
struct M_delju134_t3436069196;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_delju1343436069196.h"

// System.Void GarbageiOS.M_delju134::.ctor()
extern "C"  void M_delju134__ctor_m3211287143 (M_delju134_t3436069196 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_delju134::M_tawcal0(System.String[],System.Int32)
extern "C"  void M_delju134_M_tawcal0_m1853682870 (M_delju134_t3436069196 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_delju134::M_jehisrelBere1(System.String[],System.Int32)
extern "C"  void M_delju134_M_jehisrelBere1_m980209475 (M_delju134_t3436069196 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_delju134::M_taiwalti2(System.String[],System.Int32)
extern "C"  void M_delju134_M_taiwalti2_m2683372289 (M_delju134_t3436069196 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_delju134::M_trirtreawo3(System.String[],System.Int32)
extern "C"  void M_delju134_M_trirtreawo3_m676884196 (M_delju134_t3436069196 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_delju134::ilo_M_jehisrelBere11(GarbageiOS.M_delju134,System.String[],System.Int32)
extern "C"  void M_delju134_ilo_M_jehisrelBere11_m442449929 (Il2CppObject * __this /* static, unused */, M_delju134_t3436069196 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_delju134::ilo_M_taiwalti22(GarbageiOS.M_delju134,System.String[],System.Int32)
extern "C"  void M_delju134_ilo_M_taiwalti22_m371308940 (Il2CppObject * __this /* static, unused */, M_delju134_t3436069196 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
