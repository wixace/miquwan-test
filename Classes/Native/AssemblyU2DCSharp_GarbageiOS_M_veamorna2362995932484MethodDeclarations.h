﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_veamorna236
struct M_veamorna236_t2995932484;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"

// System.Void GarbageiOS.M_veamorna236::.ctor()
extern "C"  void M_veamorna236__ctor_m587258527 (M_veamorna236_t2995932484 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_veamorna236::M_cinalljir0(System.String[],System.Int32)
extern "C"  void M_veamorna236_M_cinalljir0_m3253368932 (M_veamorna236_t2995932484 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
