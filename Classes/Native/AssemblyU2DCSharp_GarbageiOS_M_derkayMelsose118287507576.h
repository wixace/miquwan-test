﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_derkayMelsose118
struct  M_derkayMelsose118_t287507576  : public Il2CppObject
{
public:
	// System.UInt32 GarbageiOS.M_derkayMelsose118::_joufor
	uint32_t ____joufor_0;
	// System.Int32 GarbageiOS.M_derkayMelsose118::_noubairTarleyaw
	int32_t ____noubairTarleyaw_1;
	// System.String GarbageiOS.M_derkayMelsose118::_jalballpor
	String_t* ____jalballpor_2;
	// System.String GarbageiOS.M_derkayMelsose118::_pairsteTrali
	String_t* ____pairsteTrali_3;
	// System.Boolean GarbageiOS.M_derkayMelsose118::_dalneaFunemis
	bool ____dalneaFunemis_4;
	// System.Int32 GarbageiOS.M_derkayMelsose118::_sterumeaLowjo
	int32_t ____sterumeaLowjo_5;
	// System.String GarbageiOS.M_derkayMelsose118::_hourowwhaGidru
	String_t* ____hourowwhaGidru_6;
	// System.String GarbageiOS.M_derkayMelsose118::_jowpelTearme
	String_t* ____jowpelTearme_7;
	// System.Boolean GarbageiOS.M_derkayMelsose118::_hearpirpa
	bool ____hearpirpa_8;
	// System.Int32 GarbageiOS.M_derkayMelsose118::_mearka
	int32_t ____mearka_9;
	// System.Boolean GarbageiOS.M_derkayMelsose118::_neremal
	bool ____neremal_10;
	// System.String GarbageiOS.M_derkayMelsose118::_wiscorkou
	String_t* ____wiscorkou_11;

public:
	inline static int32_t get_offset_of__joufor_0() { return static_cast<int32_t>(offsetof(M_derkayMelsose118_t287507576, ____joufor_0)); }
	inline uint32_t get__joufor_0() const { return ____joufor_0; }
	inline uint32_t* get_address_of__joufor_0() { return &____joufor_0; }
	inline void set__joufor_0(uint32_t value)
	{
		____joufor_0 = value;
	}

	inline static int32_t get_offset_of__noubairTarleyaw_1() { return static_cast<int32_t>(offsetof(M_derkayMelsose118_t287507576, ____noubairTarleyaw_1)); }
	inline int32_t get__noubairTarleyaw_1() const { return ____noubairTarleyaw_1; }
	inline int32_t* get_address_of__noubairTarleyaw_1() { return &____noubairTarleyaw_1; }
	inline void set__noubairTarleyaw_1(int32_t value)
	{
		____noubairTarleyaw_1 = value;
	}

	inline static int32_t get_offset_of__jalballpor_2() { return static_cast<int32_t>(offsetof(M_derkayMelsose118_t287507576, ____jalballpor_2)); }
	inline String_t* get__jalballpor_2() const { return ____jalballpor_2; }
	inline String_t** get_address_of__jalballpor_2() { return &____jalballpor_2; }
	inline void set__jalballpor_2(String_t* value)
	{
		____jalballpor_2 = value;
		Il2CppCodeGenWriteBarrier(&____jalballpor_2, value);
	}

	inline static int32_t get_offset_of__pairsteTrali_3() { return static_cast<int32_t>(offsetof(M_derkayMelsose118_t287507576, ____pairsteTrali_3)); }
	inline String_t* get__pairsteTrali_3() const { return ____pairsteTrali_3; }
	inline String_t** get_address_of__pairsteTrali_3() { return &____pairsteTrali_3; }
	inline void set__pairsteTrali_3(String_t* value)
	{
		____pairsteTrali_3 = value;
		Il2CppCodeGenWriteBarrier(&____pairsteTrali_3, value);
	}

	inline static int32_t get_offset_of__dalneaFunemis_4() { return static_cast<int32_t>(offsetof(M_derkayMelsose118_t287507576, ____dalneaFunemis_4)); }
	inline bool get__dalneaFunemis_4() const { return ____dalneaFunemis_4; }
	inline bool* get_address_of__dalneaFunemis_4() { return &____dalneaFunemis_4; }
	inline void set__dalneaFunemis_4(bool value)
	{
		____dalneaFunemis_4 = value;
	}

	inline static int32_t get_offset_of__sterumeaLowjo_5() { return static_cast<int32_t>(offsetof(M_derkayMelsose118_t287507576, ____sterumeaLowjo_5)); }
	inline int32_t get__sterumeaLowjo_5() const { return ____sterumeaLowjo_5; }
	inline int32_t* get_address_of__sterumeaLowjo_5() { return &____sterumeaLowjo_5; }
	inline void set__sterumeaLowjo_5(int32_t value)
	{
		____sterumeaLowjo_5 = value;
	}

	inline static int32_t get_offset_of__hourowwhaGidru_6() { return static_cast<int32_t>(offsetof(M_derkayMelsose118_t287507576, ____hourowwhaGidru_6)); }
	inline String_t* get__hourowwhaGidru_6() const { return ____hourowwhaGidru_6; }
	inline String_t** get_address_of__hourowwhaGidru_6() { return &____hourowwhaGidru_6; }
	inline void set__hourowwhaGidru_6(String_t* value)
	{
		____hourowwhaGidru_6 = value;
		Il2CppCodeGenWriteBarrier(&____hourowwhaGidru_6, value);
	}

	inline static int32_t get_offset_of__jowpelTearme_7() { return static_cast<int32_t>(offsetof(M_derkayMelsose118_t287507576, ____jowpelTearme_7)); }
	inline String_t* get__jowpelTearme_7() const { return ____jowpelTearme_7; }
	inline String_t** get_address_of__jowpelTearme_7() { return &____jowpelTearme_7; }
	inline void set__jowpelTearme_7(String_t* value)
	{
		____jowpelTearme_7 = value;
		Il2CppCodeGenWriteBarrier(&____jowpelTearme_7, value);
	}

	inline static int32_t get_offset_of__hearpirpa_8() { return static_cast<int32_t>(offsetof(M_derkayMelsose118_t287507576, ____hearpirpa_8)); }
	inline bool get__hearpirpa_8() const { return ____hearpirpa_8; }
	inline bool* get_address_of__hearpirpa_8() { return &____hearpirpa_8; }
	inline void set__hearpirpa_8(bool value)
	{
		____hearpirpa_8 = value;
	}

	inline static int32_t get_offset_of__mearka_9() { return static_cast<int32_t>(offsetof(M_derkayMelsose118_t287507576, ____mearka_9)); }
	inline int32_t get__mearka_9() const { return ____mearka_9; }
	inline int32_t* get_address_of__mearka_9() { return &____mearka_9; }
	inline void set__mearka_9(int32_t value)
	{
		____mearka_9 = value;
	}

	inline static int32_t get_offset_of__neremal_10() { return static_cast<int32_t>(offsetof(M_derkayMelsose118_t287507576, ____neremal_10)); }
	inline bool get__neremal_10() const { return ____neremal_10; }
	inline bool* get_address_of__neremal_10() { return &____neremal_10; }
	inline void set__neremal_10(bool value)
	{
		____neremal_10 = value;
	}

	inline static int32_t get_offset_of__wiscorkou_11() { return static_cast<int32_t>(offsetof(M_derkayMelsose118_t287507576, ____wiscorkou_11)); }
	inline String_t* get__wiscorkou_11() const { return ____wiscorkou_11; }
	inline String_t** get_address_of__wiscorkou_11() { return &____wiscorkou_11; }
	inline void set__wiscorkou_11(String_t* value)
	{
		____wiscorkou_11 = value;
		Il2CppCodeGenWriteBarrier(&____wiscorkou_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
