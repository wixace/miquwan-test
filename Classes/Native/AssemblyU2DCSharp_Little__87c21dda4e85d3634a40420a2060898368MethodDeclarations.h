﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._87c21dda4e85d3634a40420a7749a883
struct _87c21dda4e85d3634a40420a7749a883_t2060898368;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._87c21dda4e85d3634a40420a7749a883::.ctor()
extern "C"  void _87c21dda4e85d3634a40420a7749a883__ctor_m3488783853 (_87c21dda4e85d3634a40420a7749a883_t2060898368 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._87c21dda4e85d3634a40420a7749a883::_87c21dda4e85d3634a40420a7749a883m2(System.Int32)
extern "C"  int32_t _87c21dda4e85d3634a40420a7749a883__87c21dda4e85d3634a40420a7749a883m2_m2871732633 (_87c21dda4e85d3634a40420a7749a883_t2060898368 * __this, int32_t ____87c21dda4e85d3634a40420a7749a883a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._87c21dda4e85d3634a40420a7749a883::_87c21dda4e85d3634a40420a7749a883m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _87c21dda4e85d3634a40420a7749a883__87c21dda4e85d3634a40420a7749a883m_m3630464317 (_87c21dda4e85d3634a40420a7749a883_t2060898368 * __this, int32_t ____87c21dda4e85d3634a40420a7749a883a0, int32_t ____87c21dda4e85d3634a40420a7749a883271, int32_t ____87c21dda4e85d3634a40420a7749a883c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
