﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AMihuaPay
struct  AMihuaPay_t1319045905  : public Il2CppObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.String> AMihuaPay::payParams
	Dictionary_2_t827649927 * ___payParams_0;
	// System.String AMihuaPay::referer
	String_t* ___referer_1;
	// System.String AMihuaPay::callBackName
	String_t* ___callBackName_2;

public:
	inline static int32_t get_offset_of_payParams_0() { return static_cast<int32_t>(offsetof(AMihuaPay_t1319045905, ___payParams_0)); }
	inline Dictionary_2_t827649927 * get_payParams_0() const { return ___payParams_0; }
	inline Dictionary_2_t827649927 ** get_address_of_payParams_0() { return &___payParams_0; }
	inline void set_payParams_0(Dictionary_2_t827649927 * value)
	{
		___payParams_0 = value;
		Il2CppCodeGenWriteBarrier(&___payParams_0, value);
	}

	inline static int32_t get_offset_of_referer_1() { return static_cast<int32_t>(offsetof(AMihuaPay_t1319045905, ___referer_1)); }
	inline String_t* get_referer_1() const { return ___referer_1; }
	inline String_t** get_address_of_referer_1() { return &___referer_1; }
	inline void set_referer_1(String_t* value)
	{
		___referer_1 = value;
		Il2CppCodeGenWriteBarrier(&___referer_1, value);
	}

	inline static int32_t get_offset_of_callBackName_2() { return static_cast<int32_t>(offsetof(AMihuaPay_t1319045905, ___callBackName_2)); }
	inline String_t* get_callBackName_2() const { return ___callBackName_2; }
	inline String_t** get_address_of_callBackName_2() { return &___callBackName_2; }
	inline void set_callBackName_2(String_t* value)
	{
		___callBackName_2 = value;
		Il2CppCodeGenWriteBarrier(&___callBackName_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
