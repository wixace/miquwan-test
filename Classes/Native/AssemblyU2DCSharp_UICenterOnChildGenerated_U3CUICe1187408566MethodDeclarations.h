﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UICenterOnChildGenerated/<UICenterOnChild_onCenter_GetDelegate_member3_arg0>c__AnonStoreyB3
struct U3CUICenterOnChild_onCenter_GetDelegate_member3_arg0U3Ec__AnonStoreyB3_t1187408566;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"

// System.Void UICenterOnChildGenerated/<UICenterOnChild_onCenter_GetDelegate_member3_arg0>c__AnonStoreyB3::.ctor()
extern "C"  void U3CUICenterOnChild_onCenter_GetDelegate_member3_arg0U3Ec__AnonStoreyB3__ctor_m24423797 (U3CUICenterOnChild_onCenter_GetDelegate_member3_arg0U3Ec__AnonStoreyB3_t1187408566 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICenterOnChildGenerated/<UICenterOnChild_onCenter_GetDelegate_member3_arg0>c__AnonStoreyB3::<>m__12A(UnityEngine.GameObject)
extern "C"  void U3CUICenterOnChild_onCenter_GetDelegate_member3_arg0U3Ec__AnonStoreyB3_U3CU3Em__12A_m4282799452 (U3CUICenterOnChild_onCenter_GetDelegate_member3_arg0U3Ec__AnonStoreyB3_t1187408566 * __this, GameObject_t3674682005 * ___centeredObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
