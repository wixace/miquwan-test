﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// ReplayMgr
struct ReplayMgr_t1549183121;
// ReplayData
struct ReplayData_t779762769;
// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<ReplayBase>>
struct Dictionary_2_t2968307082;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1375417109;
// System.Collections.Generic.List`1<ReplayBase>
struct List_1_t2147888712;
// System.Action
struct Action_t3771233898;

#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_ReplayPlayType3102897589.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ReplayMgr
struct  ReplayMgr_t1549183121  : public Il2CppObject
{
public:
	// System.String ReplayMgr::curJson
	String_t* ___curJson_4;
	// ReplayPlayType ReplayMgr::playType
	int32_t ___playType_5;
	// ReplayData ReplayMgr::replayData
	ReplayData_t779762769 * ___replayData_6;
	// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<ReplayBase>> ReplayMgr::replayDataDics
	Dictionary_2_t2968307082 * ___replayDataDics_7;
	// System.Single ReplayMgr::time
	float ___time_8;
	// System.Int32 ReplayMgr::frameNum
	int32_t ___frameNum_9;
	// System.Boolean ReplayMgr::IsStartREC
	bool ___IsStartREC_10;
	// System.Collections.Generic.List`1<System.String> ReplayMgr::replayDicKeys
	List_1_t1375417109 * ___replayDicKeys_11;
	// System.Collections.Generic.List`1<ReplayBase> ReplayMgr::replayBaseList
	List_1_t2147888712 * ___replayBaseList_12;

public:
	inline static int32_t get_offset_of_curJson_4() { return static_cast<int32_t>(offsetof(ReplayMgr_t1549183121, ___curJson_4)); }
	inline String_t* get_curJson_4() const { return ___curJson_4; }
	inline String_t** get_address_of_curJson_4() { return &___curJson_4; }
	inline void set_curJson_4(String_t* value)
	{
		___curJson_4 = value;
		Il2CppCodeGenWriteBarrier(&___curJson_4, value);
	}

	inline static int32_t get_offset_of_playType_5() { return static_cast<int32_t>(offsetof(ReplayMgr_t1549183121, ___playType_5)); }
	inline int32_t get_playType_5() const { return ___playType_5; }
	inline int32_t* get_address_of_playType_5() { return &___playType_5; }
	inline void set_playType_5(int32_t value)
	{
		___playType_5 = value;
	}

	inline static int32_t get_offset_of_replayData_6() { return static_cast<int32_t>(offsetof(ReplayMgr_t1549183121, ___replayData_6)); }
	inline ReplayData_t779762769 * get_replayData_6() const { return ___replayData_6; }
	inline ReplayData_t779762769 ** get_address_of_replayData_6() { return &___replayData_6; }
	inline void set_replayData_6(ReplayData_t779762769 * value)
	{
		___replayData_6 = value;
		Il2CppCodeGenWriteBarrier(&___replayData_6, value);
	}

	inline static int32_t get_offset_of_replayDataDics_7() { return static_cast<int32_t>(offsetof(ReplayMgr_t1549183121, ___replayDataDics_7)); }
	inline Dictionary_2_t2968307082 * get_replayDataDics_7() const { return ___replayDataDics_7; }
	inline Dictionary_2_t2968307082 ** get_address_of_replayDataDics_7() { return &___replayDataDics_7; }
	inline void set_replayDataDics_7(Dictionary_2_t2968307082 * value)
	{
		___replayDataDics_7 = value;
		Il2CppCodeGenWriteBarrier(&___replayDataDics_7, value);
	}

	inline static int32_t get_offset_of_time_8() { return static_cast<int32_t>(offsetof(ReplayMgr_t1549183121, ___time_8)); }
	inline float get_time_8() const { return ___time_8; }
	inline float* get_address_of_time_8() { return &___time_8; }
	inline void set_time_8(float value)
	{
		___time_8 = value;
	}

	inline static int32_t get_offset_of_frameNum_9() { return static_cast<int32_t>(offsetof(ReplayMgr_t1549183121, ___frameNum_9)); }
	inline int32_t get_frameNum_9() const { return ___frameNum_9; }
	inline int32_t* get_address_of_frameNum_9() { return &___frameNum_9; }
	inline void set_frameNum_9(int32_t value)
	{
		___frameNum_9 = value;
	}

	inline static int32_t get_offset_of_IsStartREC_10() { return static_cast<int32_t>(offsetof(ReplayMgr_t1549183121, ___IsStartREC_10)); }
	inline bool get_IsStartREC_10() const { return ___IsStartREC_10; }
	inline bool* get_address_of_IsStartREC_10() { return &___IsStartREC_10; }
	inline void set_IsStartREC_10(bool value)
	{
		___IsStartREC_10 = value;
	}

	inline static int32_t get_offset_of_replayDicKeys_11() { return static_cast<int32_t>(offsetof(ReplayMgr_t1549183121, ___replayDicKeys_11)); }
	inline List_1_t1375417109 * get_replayDicKeys_11() const { return ___replayDicKeys_11; }
	inline List_1_t1375417109 ** get_address_of_replayDicKeys_11() { return &___replayDicKeys_11; }
	inline void set_replayDicKeys_11(List_1_t1375417109 * value)
	{
		___replayDicKeys_11 = value;
		Il2CppCodeGenWriteBarrier(&___replayDicKeys_11, value);
	}

	inline static int32_t get_offset_of_replayBaseList_12() { return static_cast<int32_t>(offsetof(ReplayMgr_t1549183121, ___replayBaseList_12)); }
	inline List_1_t2147888712 * get_replayBaseList_12() const { return ___replayBaseList_12; }
	inline List_1_t2147888712 ** get_address_of_replayBaseList_12() { return &___replayBaseList_12; }
	inline void set_replayBaseList_12(List_1_t2147888712 * value)
	{
		___replayBaseList_12 = value;
		Il2CppCodeGenWriteBarrier(&___replayBaseList_12, value);
	}
};

struct ReplayMgr_t1549183121_StaticFields
{
public:
	// ReplayMgr ReplayMgr::_inst
	ReplayMgr_t1549183121 * ____inst_3;
	// System.Action ReplayMgr::<>f__am$cacheA
	Action_t3771233898 * ___U3CU3Ef__amU24cacheA_13;

public:
	inline static int32_t get_offset_of__inst_3() { return static_cast<int32_t>(offsetof(ReplayMgr_t1549183121_StaticFields, ____inst_3)); }
	inline ReplayMgr_t1549183121 * get__inst_3() const { return ____inst_3; }
	inline ReplayMgr_t1549183121 ** get_address_of__inst_3() { return &____inst_3; }
	inline void set__inst_3(ReplayMgr_t1549183121 * value)
	{
		____inst_3 = value;
		Il2CppCodeGenWriteBarrier(&____inst_3, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheA_13() { return static_cast<int32_t>(offsetof(ReplayMgr_t1549183121_StaticFields, ___U3CU3Ef__amU24cacheA_13)); }
	inline Action_t3771233898 * get_U3CU3Ef__amU24cacheA_13() const { return ___U3CU3Ef__amU24cacheA_13; }
	inline Action_t3771233898 ** get_address_of_U3CU3Ef__amU24cacheA_13() { return &___U3CU3Ef__amU24cacheA_13; }
	inline void set_U3CU3Ef__amU24cacheA_13(Action_t3771233898 * value)
	{
		___U3CU3Ef__amU24cacheA_13 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheA_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
