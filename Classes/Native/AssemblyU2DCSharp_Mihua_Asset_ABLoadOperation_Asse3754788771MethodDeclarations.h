﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mihua.Asset.ABLoadOperation.AssetOperationLoad
struct AssetOperationLoad_t3754788771;
// System.String
struct String_t;
// Mihua.Asset.OnLoadAsset
struct OnLoadAsset_t4181543125;
// UnityEngine.Object
struct Object_t3071478659;
// Mihua.Asset.ABLoadOperation.AssetOperation
struct AssetOperation_t778728221;
// ConfigAssetMgr
struct ConfigAssetMgr_t4036193930;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_Mihua_Asset_OnLoadAsset4181543125.h"
#include "AssemblyU2DCSharp_Mihua_Asset_ABLoadOperation_Asset778728221.h"
#include "AssemblyU2DCSharp_ConfigAssetMgr4036193930.h"

// System.Void Mihua.Asset.ABLoadOperation.AssetOperationLoad::.ctor(System.String,Mihua.Asset.OnLoadAsset)
extern "C"  void AssetOperationLoad__ctor_m982547707 (AssetOperationLoad_t3754788771 * __this, String_t* ____assetName0, OnLoadAsset_t4181543125 * ____onLoadAsset1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua.Asset.ABLoadOperation.AssetOperationLoad::Init()
extern "C"  void AssetOperationLoad_Init_m314012888 (AssetOperationLoad_t3754788771 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object Mihua.Asset.ABLoadOperation.AssetOperationLoad::GetAsset()
extern "C"  Object_t3071478659 * AssetOperationLoad_GetAsset_m1688512073 (AssetOperationLoad_t3754788771 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mihua.Asset.ABLoadOperation.AssetOperationLoad::IsDone()
extern "C"  bool AssetOperationLoad_IsDone_m3708419464 (AssetOperationLoad_t3754788771 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Mihua.Asset.ABLoadOperation.AssetOperationLoad::get_progress()
extern "C"  float AssetOperationLoad_get_progress_m1277947882 (AssetOperationLoad_t3754788771 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua.Asset.ABLoadOperation.AssetOperationLoad::CacheAsset()
extern "C"  void AssetOperationLoad_CacheAsset_m4175192982 (AssetOperationLoad_t3754788771 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua.Asset.ABLoadOperation.AssetOperationLoad::ClearOnLoad()
extern "C"  void AssetOperationLoad_ClearOnLoad_m3730608108 (AssetOperationLoad_t3754788771 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua.Asset.ABLoadOperation.AssetOperationLoad::Clear()
extern "C"  void AssetOperationLoad_Clear_m52962855 (AssetOperationLoad_t3754788771 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua.Asset.ABLoadOperation.AssetOperationLoad::ilo_Init1(Mihua.Asset.ABLoadOperation.AssetOperation)
extern "C"  void AssetOperationLoad_ilo_Init1_m645015887 (Il2CppObject * __this /* static, unused */, AssetOperation_t778728221 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mihua.Asset.ABLoadOperation.AssetOperationLoad::ilo_LoadPathToAssetName2(ConfigAssetMgr,System.String)
extern "C"  String_t* AssetOperationLoad_ilo_LoadPathToAssetName2_m1154682285 (Il2CppObject * __this /* static, unused */, ConfigAssetMgr_t4036193930 * ____this0, String_t* ___path1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
