﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Pathfinding.RVO.Sampled.Agent
struct Agent_t1290054243;

#include "mscorlib_System_ValueType1744280289.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.RVO.RVOQuadtree/Node
struct  Node_t2108618958 
{
public:
	// System.Int32 Pathfinding.RVO.RVOQuadtree/Node::child00
	int32_t ___child00_0;
	// System.Int32 Pathfinding.RVO.RVOQuadtree/Node::child01
	int32_t ___child01_1;
	// System.Int32 Pathfinding.RVO.RVOQuadtree/Node::child10
	int32_t ___child10_2;
	// System.Int32 Pathfinding.RVO.RVOQuadtree/Node::child11
	int32_t ___child11_3;
	// System.Byte Pathfinding.RVO.RVOQuadtree/Node::count
	uint8_t ___count_4;
	// Pathfinding.RVO.Sampled.Agent Pathfinding.RVO.RVOQuadtree/Node::linkedList
	Agent_t1290054243 * ___linkedList_5;

public:
	inline static int32_t get_offset_of_child00_0() { return static_cast<int32_t>(offsetof(Node_t2108618958, ___child00_0)); }
	inline int32_t get_child00_0() const { return ___child00_0; }
	inline int32_t* get_address_of_child00_0() { return &___child00_0; }
	inline void set_child00_0(int32_t value)
	{
		___child00_0 = value;
	}

	inline static int32_t get_offset_of_child01_1() { return static_cast<int32_t>(offsetof(Node_t2108618958, ___child01_1)); }
	inline int32_t get_child01_1() const { return ___child01_1; }
	inline int32_t* get_address_of_child01_1() { return &___child01_1; }
	inline void set_child01_1(int32_t value)
	{
		___child01_1 = value;
	}

	inline static int32_t get_offset_of_child10_2() { return static_cast<int32_t>(offsetof(Node_t2108618958, ___child10_2)); }
	inline int32_t get_child10_2() const { return ___child10_2; }
	inline int32_t* get_address_of_child10_2() { return &___child10_2; }
	inline void set_child10_2(int32_t value)
	{
		___child10_2 = value;
	}

	inline static int32_t get_offset_of_child11_3() { return static_cast<int32_t>(offsetof(Node_t2108618958, ___child11_3)); }
	inline int32_t get_child11_3() const { return ___child11_3; }
	inline int32_t* get_address_of_child11_3() { return &___child11_3; }
	inline void set_child11_3(int32_t value)
	{
		___child11_3 = value;
	}

	inline static int32_t get_offset_of_count_4() { return static_cast<int32_t>(offsetof(Node_t2108618958, ___count_4)); }
	inline uint8_t get_count_4() const { return ___count_4; }
	inline uint8_t* get_address_of_count_4() { return &___count_4; }
	inline void set_count_4(uint8_t value)
	{
		___count_4 = value;
	}

	inline static int32_t get_offset_of_linkedList_5() { return static_cast<int32_t>(offsetof(Node_t2108618958, ___linkedList_5)); }
	inline Agent_t1290054243 * get_linkedList_5() const { return ___linkedList_5; }
	inline Agent_t1290054243 ** get_address_of_linkedList_5() { return &___linkedList_5; }
	inline void set_linkedList_5(Agent_t1290054243 * value)
	{
		___linkedList_5 = value;
		Il2CppCodeGenWriteBarrier(&___linkedList_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for marshalling of: Pathfinding.RVO.RVOQuadtree/Node
struct Node_t2108618958_marshaled_pinvoke
{
	int32_t ___child00_0;
	int32_t ___child01_1;
	int32_t ___child10_2;
	int32_t ___child11_3;
	uint8_t ___count_4;
	Agent_t1290054243 * ___linkedList_5;
};
// Native definition for marshalling of: Pathfinding.RVO.RVOQuadtree/Node
struct Node_t2108618958_marshaled_com
{
	int32_t ___child00_0;
	int32_t ___child01_1;
	int32_t ___child10_2;
	int32_t ___child11_3;
	uint8_t ___count_4;
	Agent_t1290054243 * ___linkedList_5;
};
