﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_pihou88
struct M_pihou88_t1935803061;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_pihou881935803061.h"

// System.Void GarbageiOS.M_pihou88::.ctor()
extern "C"  void M_pihou88__ctor_m845054030 (M_pihou88_t1935803061 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_pihou88::M_trirvereTalllar0(System.String[],System.Int32)
extern "C"  void M_pihou88_M_trirvereTalllar0_m3563467336 (M_pihou88_t1935803061 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_pihou88::M_drowlellor1(System.String[],System.Int32)
extern "C"  void M_pihou88_M_drowlellor1_m348625328 (M_pihou88_t1935803061 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_pihou88::ilo_M_trirvereTalllar01(GarbageiOS.M_pihou88,System.String[],System.Int32)
extern "C"  void M_pihou88_ilo_M_trirvereTalllar01_m3277226171 (Il2CppObject * __this /* static, unused */, M_pihou88_t1935803061 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
