﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.AssetBundle
struct AssetBundle_t2070959688;
// UnityEngine.AssetBundleCreateRequest
struct AssetBundleCreateRequest_t1416890373;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// UnityEngine.Object
struct Object_t3071478659;
// System.Type
struct Type_t;
// UnityEngine.AssetBundleRequest
struct AssetBundleRequest_t2154290273;
// UnityEngine.Object[]
struct ObjectU5BU5D_t1015136018;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Type2863145774.h"

// System.Void UnityEngine.AssetBundle::.ctor()
extern "C"  void AssetBundle__ctor_m1775753223 (AssetBundle_t2070959688 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AssetBundleCreateRequest UnityEngine.AssetBundle::LoadFromFileAsync(System.String,System.UInt32,System.UInt64)
extern "C"  AssetBundleCreateRequest_t1416890373 * AssetBundle_LoadFromFileAsync_m3702322495 (Il2CppObject * __this /* static, unused */, String_t* ___path0, uint32_t ___crc1, uint64_t ___offset2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AssetBundleCreateRequest UnityEngine.AssetBundle::LoadFromFileAsync(System.String,System.UInt32)
extern "C"  AssetBundleCreateRequest_t1416890373 * AssetBundle_LoadFromFileAsync_m3360524554 (Il2CppObject * __this /* static, unused */, String_t* ___path0, uint32_t ___crc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AssetBundleCreateRequest UnityEngine.AssetBundle::LoadFromFileAsync(System.String)
extern "C"  AssetBundleCreateRequest_t1416890373 * AssetBundle_LoadFromFileAsync_m1987943766 (Il2CppObject * __this /* static, unused */, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AssetBundle UnityEngine.AssetBundle::LoadFromFile(System.String,System.UInt32,System.UInt64)
extern "C"  AssetBundle_t2070959688 * AssetBundle_LoadFromFile_m2188469338 (Il2CppObject * __this /* static, unused */, String_t* ___path0, uint32_t ___crc1, uint64_t ___offset2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AssetBundle UnityEngine.AssetBundle::LoadFromFile(System.String,System.UInt32)
extern "C"  AssetBundle_t2070959688 * AssetBundle_LoadFromFile_m2447684453 (Il2CppObject * __this /* static, unused */, String_t* ___path0, uint32_t ___crc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AssetBundle UnityEngine.AssetBundle::LoadFromFile(System.String)
extern "C"  AssetBundle_t2070959688 * AssetBundle_LoadFromFile_m233059057 (Il2CppObject * __this /* static, unused */, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AssetBundleCreateRequest UnityEngine.AssetBundle::LoadFromMemoryAsync(System.Byte[],System.UInt32)
extern "C"  AssetBundleCreateRequest_t1416890373 * AssetBundle_LoadFromMemoryAsync_m4175799958 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___binary0, uint32_t ___crc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AssetBundleCreateRequest UnityEngine.AssetBundle::LoadFromMemoryAsync(System.Byte[])
extern "C"  AssetBundleCreateRequest_t1416890373 * AssetBundle_LoadFromMemoryAsync_m3957530594 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___binary0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AssetBundle UnityEngine.AssetBundle::LoadFromMemory(System.Byte[],System.UInt32)
extern "C"  AssetBundle_t2070959688 * AssetBundle_LoadFromMemory_m4021799815 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___binary0, uint32_t ___crc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AssetBundle UnityEngine.AssetBundle::LoadFromMemory(System.Byte[])
extern "C"  AssetBundle_t2070959688 * AssetBundle_LoadFromMemory_m4150873747 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___binary0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object UnityEngine.AssetBundle::get_mainAsset()
extern "C"  Object_t3071478659 * AssetBundle_get_mainAsset_m3597227404 (AssetBundle_t2070959688 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.AssetBundle::Contains(System.String)
extern "C"  bool AssetBundle_Contains_m3736170156 (AssetBundle_t2070959688 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object UnityEngine.AssetBundle::LoadAsset(System.String)
extern "C"  Object_t3071478659 * AssetBundle_LoadAsset_m2357653626 (AssetBundle_t2070959688 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object UnityEngine.AssetBundle::LoadAsset(System.String,System.Type)
extern "C"  Object_t3071478659 * AssetBundle_LoadAsset_m3749681325 (AssetBundle_t2070959688 * __this, String_t* ___name0, Type_t * ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object UnityEngine.AssetBundle::LoadAsset_Internal(System.String,System.Type)
extern "C"  Object_t3071478659 * AssetBundle_LoadAsset_Internal_m1258146911 (AssetBundle_t2070959688 * __this, String_t* ___name0, Type_t * ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AssetBundleRequest UnityEngine.AssetBundle::LoadAssetAsync(System.String)
extern "C"  AssetBundleRequest_t2154290273 * AssetBundle_LoadAssetAsync_m1390876910 (AssetBundle_t2070959688 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AssetBundleRequest UnityEngine.AssetBundle::LoadAssetAsync(System.String,System.Type)
extern "C"  AssetBundleRequest_t2154290273 * AssetBundle_LoadAssetAsync_m2722280225 (AssetBundle_t2070959688 * __this, String_t* ___name0, Type_t * ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AssetBundleRequest UnityEngine.AssetBundle::LoadAssetAsync_Internal(System.String,System.Type)
extern "C"  AssetBundleRequest_t2154290273 * AssetBundle_LoadAssetAsync_Internal_m442446955 (AssetBundle_t2070959688 * __this, String_t* ___name0, Type_t * ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object[] UnityEngine.AssetBundle::LoadAssetWithSubAssets(System.String)
extern "C"  ObjectU5BU5D_t1015136018* AssetBundle_LoadAssetWithSubAssets_m2040785033 (AssetBundle_t2070959688 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object[] UnityEngine.AssetBundle::LoadAssetWithSubAssets(System.String,System.Type)
extern "C"  ObjectU5BU5D_t1015136018* AssetBundle_LoadAssetWithSubAssets_m3935695420 (AssetBundle_t2070959688 * __this, String_t* ___name0, Type_t * ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object[] UnityEngine.AssetBundle::LoadAssetWithSubAssets_Internal(System.String,System.Type)
extern "C"  ObjectU5BU5D_t1015136018* AssetBundle_LoadAssetWithSubAssets_Internal_m1442956208 (AssetBundle_t2070959688 * __this, String_t* ___name0, Type_t * ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AssetBundleRequest UnityEngine.AssetBundle::LoadAssetWithSubAssetsAsync(System.String)
extern "C"  AssetBundleRequest_t2154290273 * AssetBundle_LoadAssetWithSubAssetsAsync_m2130282585 (AssetBundle_t2070959688 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AssetBundleRequest UnityEngine.AssetBundle::LoadAssetWithSubAssetsAsync(System.String,System.Type)
extern "C"  AssetBundleRequest_t2154290273 * AssetBundle_LoadAssetWithSubAssetsAsync_m3286915596 (AssetBundle_t2070959688 * __this, String_t* ___name0, Type_t * ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AssetBundleRequest UnityEngine.AssetBundle::LoadAssetWithSubAssetsAsync_Internal(System.String,System.Type)
extern "C"  AssetBundleRequest_t2154290273 * AssetBundle_LoadAssetWithSubAssetsAsync_Internal_m3277661664 (AssetBundle_t2070959688 * __this, String_t* ___name0, Type_t * ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object[] UnityEngine.AssetBundle::LoadAllAssets()
extern "C"  ObjectU5BU5D_t1015136018* AssetBundle_LoadAllAssets_m3038626170 (AssetBundle_t2070959688 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object[] UnityEngine.AssetBundle::LoadAllAssets(System.Type)
extern "C"  ObjectU5BU5D_t1015136018* AssetBundle_LoadAllAssets_m3817565631 (AssetBundle_t2070959688 * __this, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AssetBundleRequest UnityEngine.AssetBundle::LoadAllAssetsAsync()
extern "C"  AssetBundleRequest_t2154290273 * AssetBundle_LoadAllAssetsAsync_m273894144 (AssetBundle_t2070959688 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AssetBundleRequest UnityEngine.AssetBundle::LoadAllAssetsAsync(System.Type)
extern "C"  AssetBundleRequest_t2154290273 * AssetBundle_LoadAllAssetsAsync_m4256355449 (AssetBundle_t2070959688 * __this, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AssetBundle::Unload(System.Boolean)
extern "C"  void AssetBundle_Unload_m913727763 (AssetBundle_t2070959688 * __this, bool ___unloadAllLoadedObjects0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] UnityEngine.AssetBundle::GetAllAssetNames()
extern "C"  StringU5BU5D_t4054002952* AssetBundle_GetAllAssetNames_m3257385279 (AssetBundle_t2070959688 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] UnityEngine.AssetBundle::GetAllScenePaths()
extern "C"  StringU5BU5D_t4054002952* AssetBundle_GetAllScenePaths_m750568521 (AssetBundle_t2070959688 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
