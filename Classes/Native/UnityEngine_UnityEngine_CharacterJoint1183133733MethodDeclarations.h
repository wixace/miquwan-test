﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.CharacterJoint
struct CharacterJoint_t1183133733;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_SoftJointLimitSpring4258895628.h"
#include "UnityEngine_UnityEngine_SoftJointLimit2747465311.h"

// System.Void UnityEngine.CharacterJoint::.ctor()
extern "C"  void CharacterJoint__ctor_m2056130604 (CharacterJoint_t1183133733 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.CharacterJoint::get_swingAxis()
extern "C"  Vector3_t4282066566  CharacterJoint_get_swingAxis_m1020895814 (CharacterJoint_t1183133733 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.CharacterJoint::set_swingAxis(UnityEngine.Vector3)
extern "C"  void CharacterJoint_set_swingAxis_m3140864077 (CharacterJoint_t1183133733 * __this, Vector3_t4282066566  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.CharacterJoint::INTERNAL_get_swingAxis(UnityEngine.Vector3&)
extern "C"  void CharacterJoint_INTERNAL_get_swingAxis_m409001951 (CharacterJoint_t1183133733 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.CharacterJoint::INTERNAL_set_swingAxis(UnityEngine.Vector3&)
extern "C"  void CharacterJoint_INTERNAL_set_swingAxis_m3934903531 (CharacterJoint_t1183133733 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.SoftJointLimitSpring UnityEngine.CharacterJoint::get_twistLimitSpring()
extern "C"  SoftJointLimitSpring_t4258895628  CharacterJoint_get_twistLimitSpring_m1641353106 (CharacterJoint_t1183133733 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.CharacterJoint::set_twistLimitSpring(UnityEngine.SoftJointLimitSpring)
extern "C"  void CharacterJoint_set_twistLimitSpring_m75129175 (CharacterJoint_t1183133733 * __this, SoftJointLimitSpring_t4258895628  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.CharacterJoint::INTERNAL_get_twistLimitSpring(UnityEngine.SoftJointLimitSpring&)
extern "C"  void CharacterJoint_INTERNAL_get_twistLimitSpring_m3245115413 (CharacterJoint_t1183133733 * __this, SoftJointLimitSpring_t4258895628 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.CharacterJoint::INTERNAL_set_twistLimitSpring(UnityEngine.SoftJointLimitSpring&)
extern "C"  void CharacterJoint_INTERNAL_set_twistLimitSpring_m2975893281 (CharacterJoint_t1183133733 * __this, SoftJointLimitSpring_t4258895628 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.SoftJointLimitSpring UnityEngine.CharacterJoint::get_swingLimitSpring()
extern "C"  SoftJointLimitSpring_t4258895628  CharacterJoint_get_swingLimitSpring_m1672928539 (CharacterJoint_t1183133733 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.CharacterJoint::set_swingLimitSpring(UnityEngine.SoftJointLimitSpring)
extern "C"  void CharacterJoint_set_swingLimitSpring_m1549519584 (CharacterJoint_t1183133733 * __this, SoftJointLimitSpring_t4258895628  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.CharacterJoint::INTERNAL_get_swingLimitSpring(UnityEngine.SoftJointLimitSpring&)
extern "C"  void CharacterJoint_INTERNAL_get_swingLimitSpring_m1706577836 (CharacterJoint_t1183133733 * __this, SoftJointLimitSpring_t4258895628 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.CharacterJoint::INTERNAL_set_swingLimitSpring(UnityEngine.SoftJointLimitSpring&)
extern "C"  void CharacterJoint_INTERNAL_set_swingLimitSpring_m1437355704 (CharacterJoint_t1183133733 * __this, SoftJointLimitSpring_t4258895628 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.SoftJointLimit UnityEngine.CharacterJoint::get_lowTwistLimit()
extern "C"  SoftJointLimit_t2747465311  CharacterJoint_get_lowTwistLimit_m1680431180 (CharacterJoint_t1183133733 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.CharacterJoint::set_lowTwistLimit(UnityEngine.SoftJointLimit)
extern "C"  void CharacterJoint_set_lowTwistLimit_m3481696387 (CharacterJoint_t1183133733 * __this, SoftJointLimit_t2747465311  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.CharacterJoint::INTERNAL_get_lowTwistLimit(UnityEngine.SoftJointLimit&)
extern "C"  void CharacterJoint_INTERNAL_get_lowTwistLimit_m1637368041 (CharacterJoint_t1183133733 * __this, SoftJointLimit_t2747465311 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.CharacterJoint::INTERNAL_set_lowTwistLimit(UnityEngine.SoftJointLimit&)
extern "C"  void CharacterJoint_INTERNAL_set_lowTwistLimit_m322102365 (CharacterJoint_t1183133733 * __this, SoftJointLimit_t2747465311 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.SoftJointLimit UnityEngine.CharacterJoint::get_highTwistLimit()
extern "C"  SoftJointLimit_t2747465311  CharacterJoint_get_highTwistLimit_m847665652 (CharacterJoint_t1183133733 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.CharacterJoint::set_highTwistLimit(UnityEngine.SoftJointLimit)
extern "C"  void CharacterJoint_set_highTwistLimit_m4291597753 (CharacterJoint_t1183133733 * __this, SoftJointLimit_t2747465311  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.CharacterJoint::INTERNAL_get_highTwistLimit(UnityEngine.SoftJointLimit&)
extern "C"  void CharacterJoint_INTERNAL_get_highTwistLimit_m3278610419 (CharacterJoint_t1183133733 * __this, SoftJointLimit_t2747465311 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.CharacterJoint::INTERNAL_set_highTwistLimit(UnityEngine.SoftJointLimit&)
extern "C"  void CharacterJoint_INTERNAL_set_highTwistLimit_m1160080127 (CharacterJoint_t1183133733 * __this, SoftJointLimit_t2747465311 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.SoftJointLimit UnityEngine.CharacterJoint::get_swing1Limit()
extern "C"  SoftJointLimit_t2747465311  CharacterJoint_get_swing1Limit_m552713292 (CharacterJoint_t1183133733 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.CharacterJoint::set_swing1Limit(UnityEngine.SoftJointLimit)
extern "C"  void CharacterJoint_set_swing1Limit_m1768733379 (CharacterJoint_t1183133733 * __this, SoftJointLimit_t2747465311  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.CharacterJoint::INTERNAL_get_swing1Limit(UnityEngine.SoftJointLimit&)
extern "C"  void CharacterJoint_INTERNAL_get_swing1Limit_m181638825 (CharacterJoint_t1183133733 * __this, SoftJointLimit_t2747465311 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.CharacterJoint::INTERNAL_set_swing1Limit(UnityEngine.SoftJointLimit&)
extern "C"  void CharacterJoint_INTERNAL_set_swing1Limit_m770213661 (CharacterJoint_t1183133733 * __this, SoftJointLimit_t2747465311 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.SoftJointLimit UnityEngine.CharacterJoint::get_swing2Limit()
extern "C"  SoftJointLimit_t2747465311  CharacterJoint_get_swing2Limit_m2295523627 (CharacterJoint_t1183133733 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.CharacterJoint::set_swing2Limit(UnityEngine.SoftJointLimit)
extern "C"  void CharacterJoint_set_swing2Limit_m2794225378 (CharacterJoint_t1183133733 * __this, SoftJointLimit_t2747465311  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.CharacterJoint::INTERNAL_get_swing2Limit(UnityEngine.SoftJointLimit&)
extern "C"  void CharacterJoint_INTERNAL_get_swing2Limit_m1907119722 (CharacterJoint_t1183133733 * __this, SoftJointLimit_t2747465311 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.CharacterJoint::INTERNAL_set_swing2Limit(UnityEngine.SoftJointLimit&)
extern "C"  void CharacterJoint_INTERNAL_set_swing2Limit_m2495694558 (CharacterJoint_t1183133733 * __this, SoftJointLimit_t2747465311 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.CharacterJoint::get_enableProjection()
extern "C"  bool CharacterJoint_get_enableProjection_m1981282841 (CharacterJoint_t1183133733 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.CharacterJoint::set_enableProjection(System.Boolean)
extern "C"  void CharacterJoint_set_enableProjection_m1491725662 (CharacterJoint_t1183133733 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.CharacterJoint::get_projectionDistance()
extern "C"  float CharacterJoint_get_projectionDistance_m3900897393 (CharacterJoint_t1183133733 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.CharacterJoint::set_projectionDistance(System.Single)
extern "C"  void CharacterJoint_set_projectionDistance_m3343916146 (CharacterJoint_t1183133733 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.CharacterJoint::get_projectionAngle()
extern "C"  float CharacterJoint_get_projectionAngle_m3205248569 (CharacterJoint_t1183133733 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.CharacterJoint::set_projectionAngle(System.Single)
extern "C"  void CharacterJoint_set_projectionAngle_m4174627706 (CharacterJoint_t1183133733 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
