﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UISprite
struct UISprite_t661437049;
// UnityEngine.Material
struct Material_t3870600107;
// UIAtlas
struct UIAtlas_t281921111;
// System.String
struct String_t;
// UISpriteData
struct UISpriteData_t3578345923;
// BetterList`1<UnityEngine.Vector3>
struct BetterList_1_t1484067282;
// BetterList`1<UnityEngine.Vector2>
struct BetterList_1_t1484067281;
// BetterList`1<UnityEngine.Color32>
struct BetterList_1_t2095821700;
// UIWidget
struct UIWidget_t769069560;
// UIBasicSprite
struct UIBasicSprite_t2501337439;
// System.Collections.Generic.List`1<UISpriteData>
struct List_1_t651564179;
// UnityEngine.Texture
struct Texture_t2526458961;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_UIAtlas281921111.h"
#include "mscorlib_System_String7231557.h"
#include "UnityEngine_UnityEngine_Vector44282066567.h"
#include "AssemblyU2DCSharp_UISpriteData3578345923.h"
#include "AssemblyU2DCSharp_UISprite661437049.h"
#include "AssemblyU2DCSharp_UIWidget769069560.h"
#include "AssemblyU2DCSharp_UIBasicSprite_Type741864970.h"
#include "AssemblyU2DCSharp_UIBasicSprite2501337439.h"
#include "UnityEngine_UnityEngine_Rect4241904616.h"

// System.Void UISprite::.ctor()
extern "C"  void UISprite__ctor_m1299954754 (UISprite_t661437049 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material UISprite::get_material()
extern "C"  Material_t3870600107 * UISprite_get_material_m2331571249 (UISprite_t661437049 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIAtlas UISprite::get_atlas()
extern "C"  UIAtlas_t281921111 * UISprite_get_atlas_m2395763654 (UISprite_t661437049 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UISprite::set_atlas(UIAtlas)
extern "C"  void UISprite_set_atlas_m2780703821 (UISprite_t661437049 * __this, UIAtlas_t281921111 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UISprite::get_spriteName()
extern "C"  String_t* UISprite_get_spriteName_m373728510 (UISprite_t661437049 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UISprite::set_spriteName(System.String)
extern "C"  void UISprite_set_spriteName_m4264380499 (UISprite_t661437049 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UISprite::get_isValid()
extern "C"  bool UISprite_get_isValid_m1407144917 (UISprite_t661437049 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UISprite::get_fillCenter()
extern "C"  bool UISprite_get_fillCenter_m1707900183 (UISprite_t661437049 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UISprite::set_fillCenter(System.Boolean)
extern "C"  void UISprite_set_fillCenter_m2174279438 (UISprite_t661437049 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector4 UISprite::get_border()
extern "C"  Vector4_t4282066567  UISprite_get_border_m1305010352 (UISprite_t661437049 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UISprite::get_pixelSize()
extern "C"  float UISprite_get_pixelSize_m2516843954 (UISprite_t661437049 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UISprite::get_minWidth()
extern "C"  int32_t UISprite_get_minWidth_m518583757 (UISprite_t661437049 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UISprite::get_minHeight()
extern "C"  int32_t UISprite_get_minHeight_m3695008930 (UISprite_t661437049 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector4 UISprite::get_drawingDimensions()
extern "C"  Vector4_t4282066567  UISprite_get_drawingDimensions_m3780210281 (UISprite_t661437049 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UISprite::get_premultipliedAlpha()
extern "C"  bool UISprite_get_premultipliedAlpha_m3157737927 (UISprite_t661437049 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UISpriteData UISprite::GetAtlasSprite()
extern "C"  UISpriteData_t3578345923 * UISprite_GetAtlasSprite_m158078806 (UISprite_t661437049 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UISprite::SetAtlasSprite(UISpriteData)
extern "C"  void UISprite_SetAtlasSprite_m521282957 (UISprite_t661437049 * __this, UISpriteData_t3578345923 * ___sp0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UISprite::MakePixelPerfect()
extern "C"  void UISprite_MakePixelPerfect_m2230015415 (UISprite_t661437049 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UISprite::OnInit()
extern "C"  void UISprite_OnInit_m3262491729 (UISprite_t661437049 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UISprite::OnUpdate()
extern "C"  void UISprite_OnUpdate_m983950794 (UISprite_t661437049 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UISprite::OnFill(BetterList`1<UnityEngine.Vector3>,BetterList`1<UnityEngine.Vector2>,BetterList`1<UnityEngine.Color32>)
extern "C"  void UISprite_OnFill_m2052871057 (UISprite_t661437049 * __this, BetterList_1_t1484067282 * ___verts0, BetterList_1_t1484067281 * ___uvs1, BetterList_1_t2095821700 * ___cols2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UISpriteData UISprite::ilo_GetAtlasSprite1(UISprite)
extern "C"  UISpriteData_t3578345923 * UISprite_ilo_GetAtlasSprite1_m178310289 (Il2CppObject * __this /* static, unused */, UISprite_t661437049 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector4 UISprite::ilo_get_border2(UIWidget)
extern "C"  Vector4_t4282066567  UISprite_ilo_get_border2_m3851090329 (Il2CppObject * __this /* static, unused */, UIWidget_t769069560 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UISprite::ilo_get_pixelSize3(UIAtlas)
extern "C"  float UISprite_ilo_get_pixelSize3_m872162469 (Il2CppObject * __this /* static, unused */, UIAtlas_t281921111 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIBasicSprite/Type UISprite::ilo_get_type4(UIBasicSprite)
extern "C"  int32_t UISprite_ilo_get_type4_m2514545704 (Il2CppObject * __this /* static, unused */, UIBasicSprite_t2501337439 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UISprite::ilo_get_pixelSize5(UISprite)
extern "C"  float UISprite_ilo_get_pixelSize5_m654760127 (Il2CppObject * __this /* static, unused */, UISprite_t661437049 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UISprite::ilo_get_minWidth6(UIBasicSprite)
extern "C"  int32_t UISprite_ilo_get_minWidth6_m1472389595 (Il2CppObject * __this /* static, unused */, UIBasicSprite_t2501337439 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector4 UISprite::ilo_get_border7(UISprite)
extern "C"  Vector4_t4282066567  UISprite_ilo_get_border7_m119660285 (Il2CppObject * __this /* static, unused */, UISprite_t661437049 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<UISpriteData> UISprite::ilo_get_spriteList8(UIAtlas)
extern "C"  List_1_t651564179 * UISprite_ilo_get_spriteList8_m1614082072 (Il2CppObject * __this /* static, unused */, UIAtlas_t281921111 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UISprite::ilo_get_isValid9(UISprite)
extern "C"  bool UISprite_ilo_get_isValid9_m355099072 (Il2CppObject * __this /* static, unused */, UISprite_t661437049 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UISprite::ilo_MakePixelPerfect10(UIWidget)
extern "C"  void UISprite_ilo_MakePixelPerfect10_m4221760977 (Il2CppObject * __this /* static, unused */, UIWidget_t769069560 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture UISprite::ilo_get_mainTexture11(UIWidget)
extern "C"  Texture_t2526458961 * UISprite_ilo_get_mainTexture11_m437184927 (Il2CppObject * __this /* static, unused */, UIWidget_t769069560 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UISprite::ilo_get_hasBorder12(UISpriteData)
extern "C"  bool UISprite_ilo_get_hasBorder12_m2500942580 (Il2CppObject * __this /* static, unused */, UISpriteData_t3578345923 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UISprite::ilo_get_mult13(UIWidget)
extern "C"  float UISprite_ilo_get_mult13_m1209739908 (Il2CppObject * __this /* static, unused */, UIWidget_t769069560 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UISprite::ilo_set_height14(UIWidget,System.Int32)
extern "C"  void UISprite_ilo_set_height14_m716890675 (Il2CppObject * __this /* static, unused */, UIWidget_t769069560 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UISprite::ilo_OnInit15(UIWidget)
extern "C"  void UISprite_ilo_OnInit15_m3192694128 (Il2CppObject * __this /* static, unused */, UIWidget_t769069560 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UISpriteData UISprite::ilo_GetSprite16(UIAtlas,System.String)
extern "C"  UISpriteData_t3578345923 * UISprite_ilo_GetSprite16_m1469829252 (Il2CppObject * __this /* static, unused */, UIAtlas_t281921111 * ____this0, String_t* ___name1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect UISprite::ilo_ConvertToTexCoords17(UnityEngine.Rect,System.Int32,System.Int32,System.Boolean)
extern "C"  Rect_t4241904616  UISprite_ilo_ConvertToTexCoords17_m148073828 (Il2CppObject * __this /* static, unused */, Rect_t4241904616  ___rect0, int32_t ___width1, int32_t ___height2, bool ___isConvert3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
