﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_CircleCollider2DGenerated
struct UnityEngine_CircleCollider2DGenerated_t2592664465;
// JSVCall
struct JSVCall_t3708497963;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"

// System.Void UnityEngine_CircleCollider2DGenerated::.ctor()
extern "C"  void UnityEngine_CircleCollider2DGenerated__ctor_m3177230586 (UnityEngine_CircleCollider2DGenerated_t2592664465 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_CircleCollider2DGenerated::CircleCollider2D_CircleCollider2D1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_CircleCollider2DGenerated_CircleCollider2D_CircleCollider2D1_m71218424 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CircleCollider2DGenerated::CircleCollider2D_radius(JSVCall)
extern "C"  void UnityEngine_CircleCollider2DGenerated_CircleCollider2D_radius_m1556134900 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CircleCollider2DGenerated::__Register()
extern "C"  void UnityEngine_CircleCollider2DGenerated___Register_m63592461 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CircleCollider2DGenerated::ilo_setSingle1(System.Int32,System.Single)
extern "C"  void UnityEngine_CircleCollider2DGenerated_ilo_setSingle1_m2210755898 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
