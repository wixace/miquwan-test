﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SimplePlayerManager
struct SimplePlayerManager_t1523766490;

#include "codegen/il2cpp-codegen.h"

// System.Void SimplePlayerManager::.ctor()
extern "C"  void SimplePlayerManager__ctor_m734270417 (SimplePlayerManager_t1523766490 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SimplePlayerManager::Start()
extern "C"  void SimplePlayerManager_Start_m3976375505 (SimplePlayerManager_t1523766490 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SimplePlayerManager::AddMoney(System.Int32)
extern "C"  void SimplePlayerManager_AddMoney_m954508675 (Il2CppObject * __this /* static, unused */, int32_t ___v0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SimplePlayerManager::AddTicket(System.Int32)
extern "C"  void SimplePlayerManager_AddTicket_m2467713293 (Il2CppObject * __this /* static, unused */, int32_t ___v0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SimplePlayerManager::SetIcon(System.Int32)
extern "C"  void SimplePlayerManager_SetIcon_m4283798779 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SimplePlayerManager::UpdateText()
extern "C"  void SimplePlayerManager_UpdateText_m1955802601 (SimplePlayerManager_t1523766490 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SimplePlayerManager::ilo_Init1()
extern "C"  void SimplePlayerManager_ilo_Init1_m1194593597 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
