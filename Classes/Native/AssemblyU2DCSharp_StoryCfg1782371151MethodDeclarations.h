﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// StoryCfg
struct StoryCfg_t1782371151;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;

#include "codegen/il2cpp-codegen.h"

// System.Void StoryCfg::.ctor()
extern "C"  void StoryCfg__ctor_m1286288428 (StoryCfg_t1782371151 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StoryCfg::Init(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C"  void StoryCfg_Init_m2979828697 (StoryCfg_t1782371151 * __this, Dictionary_2_t827649927 * ____info0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
