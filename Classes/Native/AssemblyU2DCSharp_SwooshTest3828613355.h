﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.AnimationClip
struct AnimationClip_t2007702890;
// UnityEngine.AnimationState
struct AnimationState_t3682323633;
// MeleeWeaponTrail
struct MeleeWeaponTrail_t72048470;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SwooshTest
struct  SwooshTest_t3828613355  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.AnimationClip SwooshTest::_animation
	AnimationClip_t2007702890 * ____animation_2;
	// UnityEngine.AnimationState SwooshTest::_animationState
	AnimationState_t3682323633 * ____animationState_3;
	// System.Int32 SwooshTest::_start
	int32_t ____start_4;
	// System.Int32 SwooshTest::_end
	int32_t ____end_5;
	// System.Single SwooshTest::_startN
	float ____startN_6;
	// System.Single SwooshTest::_endN
	float ____endN_7;
	// System.Single SwooshTest::_time
	float ____time_8;
	// System.Single SwooshTest::_prevTime
	float ____prevTime_9;
	// System.Single SwooshTest::_prevAnimTime
	float ____prevAnimTime_10;
	// MeleeWeaponTrail SwooshTest::_trail
	MeleeWeaponTrail_t72048470 * ____trail_11;
	// System.Boolean SwooshTest::_firstFrame
	bool ____firstFrame_12;

public:
	inline static int32_t get_offset_of__animation_2() { return static_cast<int32_t>(offsetof(SwooshTest_t3828613355, ____animation_2)); }
	inline AnimationClip_t2007702890 * get__animation_2() const { return ____animation_2; }
	inline AnimationClip_t2007702890 ** get_address_of__animation_2() { return &____animation_2; }
	inline void set__animation_2(AnimationClip_t2007702890 * value)
	{
		____animation_2 = value;
		Il2CppCodeGenWriteBarrier(&____animation_2, value);
	}

	inline static int32_t get_offset_of__animationState_3() { return static_cast<int32_t>(offsetof(SwooshTest_t3828613355, ____animationState_3)); }
	inline AnimationState_t3682323633 * get__animationState_3() const { return ____animationState_3; }
	inline AnimationState_t3682323633 ** get_address_of__animationState_3() { return &____animationState_3; }
	inline void set__animationState_3(AnimationState_t3682323633 * value)
	{
		____animationState_3 = value;
		Il2CppCodeGenWriteBarrier(&____animationState_3, value);
	}

	inline static int32_t get_offset_of__start_4() { return static_cast<int32_t>(offsetof(SwooshTest_t3828613355, ____start_4)); }
	inline int32_t get__start_4() const { return ____start_4; }
	inline int32_t* get_address_of__start_4() { return &____start_4; }
	inline void set__start_4(int32_t value)
	{
		____start_4 = value;
	}

	inline static int32_t get_offset_of__end_5() { return static_cast<int32_t>(offsetof(SwooshTest_t3828613355, ____end_5)); }
	inline int32_t get__end_5() const { return ____end_5; }
	inline int32_t* get_address_of__end_5() { return &____end_5; }
	inline void set__end_5(int32_t value)
	{
		____end_5 = value;
	}

	inline static int32_t get_offset_of__startN_6() { return static_cast<int32_t>(offsetof(SwooshTest_t3828613355, ____startN_6)); }
	inline float get__startN_6() const { return ____startN_6; }
	inline float* get_address_of__startN_6() { return &____startN_6; }
	inline void set__startN_6(float value)
	{
		____startN_6 = value;
	}

	inline static int32_t get_offset_of__endN_7() { return static_cast<int32_t>(offsetof(SwooshTest_t3828613355, ____endN_7)); }
	inline float get__endN_7() const { return ____endN_7; }
	inline float* get_address_of__endN_7() { return &____endN_7; }
	inline void set__endN_7(float value)
	{
		____endN_7 = value;
	}

	inline static int32_t get_offset_of__time_8() { return static_cast<int32_t>(offsetof(SwooshTest_t3828613355, ____time_8)); }
	inline float get__time_8() const { return ____time_8; }
	inline float* get_address_of__time_8() { return &____time_8; }
	inline void set__time_8(float value)
	{
		____time_8 = value;
	}

	inline static int32_t get_offset_of__prevTime_9() { return static_cast<int32_t>(offsetof(SwooshTest_t3828613355, ____prevTime_9)); }
	inline float get__prevTime_9() const { return ____prevTime_9; }
	inline float* get_address_of__prevTime_9() { return &____prevTime_9; }
	inline void set__prevTime_9(float value)
	{
		____prevTime_9 = value;
	}

	inline static int32_t get_offset_of__prevAnimTime_10() { return static_cast<int32_t>(offsetof(SwooshTest_t3828613355, ____prevAnimTime_10)); }
	inline float get__prevAnimTime_10() const { return ____prevAnimTime_10; }
	inline float* get_address_of__prevAnimTime_10() { return &____prevAnimTime_10; }
	inline void set__prevAnimTime_10(float value)
	{
		____prevAnimTime_10 = value;
	}

	inline static int32_t get_offset_of__trail_11() { return static_cast<int32_t>(offsetof(SwooshTest_t3828613355, ____trail_11)); }
	inline MeleeWeaponTrail_t72048470 * get__trail_11() const { return ____trail_11; }
	inline MeleeWeaponTrail_t72048470 ** get_address_of__trail_11() { return &____trail_11; }
	inline void set__trail_11(MeleeWeaponTrail_t72048470 * value)
	{
		____trail_11 = value;
		Il2CppCodeGenWriteBarrier(&____trail_11, value);
	}

	inline static int32_t get_offset_of__firstFrame_12() { return static_cast<int32_t>(offsetof(SwooshTest_t3828613355, ____firstFrame_12)); }
	inline bool get__firstFrame_12() const { return ____firstFrame_12; }
	inline bool* get_address_of__firstFrame_12() { return &____firstFrame_12; }
	inline void set__firstFrame_12(bool value)
	{
		____firstFrame_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
