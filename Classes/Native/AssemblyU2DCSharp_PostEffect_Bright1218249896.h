﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Shader
struct Shader_t3191267369;
// UnityEngine.Material
struct Material_t3870600107;

#include "AssemblyU2DCSharp_PostEffectBase3382443234.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PostEffect_Bright
struct  PostEffect_Bright_t1218249896  : public PostEffectBase_t3382443234
{
public:
	// UnityEngine.Shader PostEffect_Bright::brightShader
	Shader_t3191267369 * ___brightShader_2;
	// UnityEngine.Material PostEffect_Bright::brightMaterial
	Material_t3870600107 * ___brightMaterial_3;
	// System.Single PostEffect_Bright::brightness
	float ___brightness_4;
	// System.Single PostEffect_Bright::saturation
	float ___saturation_5;
	// System.Single PostEffect_Bright::contrast
	float ___contrast_6;

public:
	inline static int32_t get_offset_of_brightShader_2() { return static_cast<int32_t>(offsetof(PostEffect_Bright_t1218249896, ___brightShader_2)); }
	inline Shader_t3191267369 * get_brightShader_2() const { return ___brightShader_2; }
	inline Shader_t3191267369 ** get_address_of_brightShader_2() { return &___brightShader_2; }
	inline void set_brightShader_2(Shader_t3191267369 * value)
	{
		___brightShader_2 = value;
		Il2CppCodeGenWriteBarrier(&___brightShader_2, value);
	}

	inline static int32_t get_offset_of_brightMaterial_3() { return static_cast<int32_t>(offsetof(PostEffect_Bright_t1218249896, ___brightMaterial_3)); }
	inline Material_t3870600107 * get_brightMaterial_3() const { return ___brightMaterial_3; }
	inline Material_t3870600107 ** get_address_of_brightMaterial_3() { return &___brightMaterial_3; }
	inline void set_brightMaterial_3(Material_t3870600107 * value)
	{
		___brightMaterial_3 = value;
		Il2CppCodeGenWriteBarrier(&___brightMaterial_3, value);
	}

	inline static int32_t get_offset_of_brightness_4() { return static_cast<int32_t>(offsetof(PostEffect_Bright_t1218249896, ___brightness_4)); }
	inline float get_brightness_4() const { return ___brightness_4; }
	inline float* get_address_of_brightness_4() { return &___brightness_4; }
	inline void set_brightness_4(float value)
	{
		___brightness_4 = value;
	}

	inline static int32_t get_offset_of_saturation_5() { return static_cast<int32_t>(offsetof(PostEffect_Bright_t1218249896, ___saturation_5)); }
	inline float get_saturation_5() const { return ___saturation_5; }
	inline float* get_address_of_saturation_5() { return &___saturation_5; }
	inline void set_saturation_5(float value)
	{
		___saturation_5 = value;
	}

	inline static int32_t get_offset_of_contrast_6() { return static_cast<int32_t>(offsetof(PostEffect_Bright_t1218249896, ___contrast_6)); }
	inline float get_contrast_6() const { return ___contrast_6; }
	inline float* get_address_of_contrast_6() { return &___contrast_6; }
	inline void set_contrast_6(float value)
	{
		___contrast_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
