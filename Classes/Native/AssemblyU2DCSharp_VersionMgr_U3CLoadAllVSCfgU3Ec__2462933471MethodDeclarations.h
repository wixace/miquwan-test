﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VersionMgr/<LoadAllVSCfg>c__AnonStorey15E
struct U3CLoadAllVSCfgU3Ec__AnonStorey15E_t2462933471;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t4260760469;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void VersionMgr/<LoadAllVSCfg>c__AnonStorey15E::.ctor()
extern "C"  void U3CLoadAllVSCfgU3Ec__AnonStorey15E__ctor_m2803285612 (U3CLoadAllVSCfgU3Ec__AnonStorey15E_t2462933471 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VersionMgr/<LoadAllVSCfg>c__AnonStorey15E::<>m__3F9(System.String,System.Byte[])
extern "C"  void U3CLoadAllVSCfgU3Ec__AnonStorey15E_U3CU3Em__3F9_m243921842 (U3CLoadAllVSCfgU3Ec__AnonStorey15E_t2462933471 * __this, String_t* ___name0, ByteU5BU5D_t4260760469* ___abBytes1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
