﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ProtoBuf.IExtension
struct IExtension_t1606339106;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraShotSkillCfg
struct  CameraShotSkillCfg_t260684114  : public Il2CppObject
{
public:
	// System.Int32 CameraShotSkillCfg::_id
	int32_t ____id_0;
	// System.Int32 CameraShotSkillCfg::_heroOrNpcId
	int32_t ____heroOrNpcId_1;
	// System.Int32 CameraShotSkillCfg::_isHero
	int32_t ____isHero_2;
	// System.Int32 CameraShotSkillCfg::_skillId
	int32_t ____skillId_3;
	// ProtoBuf.IExtension CameraShotSkillCfg::extensionObject
	Il2CppObject * ___extensionObject_4;

public:
	inline static int32_t get_offset_of__id_0() { return static_cast<int32_t>(offsetof(CameraShotSkillCfg_t260684114, ____id_0)); }
	inline int32_t get__id_0() const { return ____id_0; }
	inline int32_t* get_address_of__id_0() { return &____id_0; }
	inline void set__id_0(int32_t value)
	{
		____id_0 = value;
	}

	inline static int32_t get_offset_of__heroOrNpcId_1() { return static_cast<int32_t>(offsetof(CameraShotSkillCfg_t260684114, ____heroOrNpcId_1)); }
	inline int32_t get__heroOrNpcId_1() const { return ____heroOrNpcId_1; }
	inline int32_t* get_address_of__heroOrNpcId_1() { return &____heroOrNpcId_1; }
	inline void set__heroOrNpcId_1(int32_t value)
	{
		____heroOrNpcId_1 = value;
	}

	inline static int32_t get_offset_of__isHero_2() { return static_cast<int32_t>(offsetof(CameraShotSkillCfg_t260684114, ____isHero_2)); }
	inline int32_t get__isHero_2() const { return ____isHero_2; }
	inline int32_t* get_address_of__isHero_2() { return &____isHero_2; }
	inline void set__isHero_2(int32_t value)
	{
		____isHero_2 = value;
	}

	inline static int32_t get_offset_of__skillId_3() { return static_cast<int32_t>(offsetof(CameraShotSkillCfg_t260684114, ____skillId_3)); }
	inline int32_t get__skillId_3() const { return ____skillId_3; }
	inline int32_t* get_address_of__skillId_3() { return &____skillId_3; }
	inline void set__skillId_3(int32_t value)
	{
		____skillId_3 = value;
	}

	inline static int32_t get_offset_of_extensionObject_4() { return static_cast<int32_t>(offsetof(CameraShotSkillCfg_t260684114, ___extensionObject_4)); }
	inline Il2CppObject * get_extensionObject_4() const { return ___extensionObject_4; }
	inline Il2CppObject ** get_address_of_extensionObject_4() { return &___extensionObject_4; }
	inline void set_extensionObject_4(Il2CppObject * value)
	{
		___extensionObject_4 = value;
		Il2CppCodeGenWriteBarrier(&___extensionObject_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
