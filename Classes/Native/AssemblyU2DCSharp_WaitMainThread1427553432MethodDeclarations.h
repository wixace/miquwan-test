﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WaitMainThread
struct WaitMainThread_t1427553432;
// System.Action
struct Action_t3771233898;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action3771233898.h"

// System.Void WaitMainThread::.ctor()
extern "C"  void WaitMainThread__ctor_m1684912899 (WaitMainThread_t1427553432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WaitMainThread::.cctor()
extern "C"  void WaitMainThread__cctor_m210596106 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WaitMainThread::Wait(System.Action)
extern "C"  void WaitMainThread_Wait_m3098102567 (Il2CppObject * __this /* static, unused */, Action_t3771233898 * ___waitCallBack0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WaitMainThread::ZUpdate()
extern "C"  void WaitMainThread_ZUpdate_m3862121796 (WaitMainThread_t1427553432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
