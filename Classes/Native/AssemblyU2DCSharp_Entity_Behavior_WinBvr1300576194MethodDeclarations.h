﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Entity.Behavior.WinBvr
struct WinBvr_t1300576194;
// CombatEntity
struct CombatEntity_t684137495;
// Entity.Behavior.IBehavior
struct IBehavior_t770859129;
// AnimationRunner
struct AnimationRunner_t1015409588;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Entity_Behavior_EBehaviorID1238533136.h"
#include "AssemblyU2DCSharp_Entity_Behavior_IBehavior770859129.h"
#include "AssemblyU2DCSharp_CombatEntity684137495.h"
#include "AssemblyU2DCSharp_AnimationRunner1015409588.h"
#include "AssemblyU2DCSharp_AnimationRunner_AniType1006238651.h"

// System.Void Entity.Behavior.WinBvr::.ctor()
extern "C"  void WinBvr__ctor_m731252136 (WinBvr_t1300576194 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Entity.Behavior.EBehaviorID Entity.Behavior.WinBvr::get_id()
extern "C"  uint8_t WinBvr_get_id_m4104584818 (WinBvr_t1300576194 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.WinBvr::Reason()
extern "C"  void WinBvr_Reason_m745367840 (WinBvr_t1300576194 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.WinBvr::Action()
extern "C"  void WinBvr_Action_m4237041810 (WinBvr_t1300576194 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.WinBvr::DoEntering()
extern "C"  void WinBvr_DoEntering_m1820004561 (WinBvr_t1300576194 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.WinBvr::PlayWin()
extern "C"  void WinBvr_PlayWin_m415807374 (WinBvr_t1300576194 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.WinBvr::PlayIdle()
extern "C"  void WinBvr_PlayIdle_m3894667332 (WinBvr_t1300576194 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.WinBvr::PlayWinFinish()
extern "C"  void WinBvr_PlayWinFinish_m1987899841 (WinBvr_t1300576194 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.WinBvr::PlayIdleFinish()
extern "C"  void WinBvr_PlayIdleFinish_m3697426935 (WinBvr_t1300576194 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.WinBvr::DoLeaving()
extern "C"  void WinBvr_DoLeaving_m3413114031 (WinBvr_t1300576194 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CombatEntity Entity.Behavior.WinBvr::ilo_get_entity1(Entity.Behavior.IBehavior)
extern "C"  CombatEntity_t684137495 * WinBvr_ilo_get_entity1_m2342805014 (Il2CppObject * __this /* static, unused */, IBehavior_t770859129 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// AnimationRunner Entity.Behavior.WinBvr::ilo_get_characterAnim2(CombatEntity)
extern "C"  AnimationRunner_t1015409588 * WinBvr_ilo_get_characterAnim2_m3848635256 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Entity.Behavior.WinBvr::ilo_GetActionTime3(AnimationRunner,AnimationRunner/AniType)
extern "C"  float WinBvr_ilo_GetActionTime3_m3670382682 (Il2CppObject * __this /* static, unused */, AnimationRunner_t1015409588 * ____this0, int32_t ___aniType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.WinBvr::ilo_Play4(AnimationRunner,AnimationRunner/AniType,System.Single,System.Boolean)
extern "C"  void WinBvr_ilo_Play4_m2223716860 (Il2CppObject * __this /* static, unused */, AnimationRunner_t1015409588 * ____this0, int32_t ___aniType1, float ___playTime2, bool ___isskill3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
