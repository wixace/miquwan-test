﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_jaitisvalJarvo100
struct M_jaitisvalJarvo100_t2108255250;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_jaitisvalJarvo1002108255250.h"

// System.Void GarbageiOS.M_jaitisvalJarvo100::.ctor()
extern "C"  void M_jaitisvalJarvo100__ctor_m3990720913 (M_jaitisvalJarvo100_t2108255250 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_jaitisvalJarvo100::M_teseaWhuke0(System.String[],System.Int32)
extern "C"  void M_jaitisvalJarvo100_M_teseaWhuke0_m2428868548 (M_jaitisvalJarvo100_t2108255250 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_jaitisvalJarvo100::M_sikisgo1(System.String[],System.Int32)
extern "C"  void M_jaitisvalJarvo100_M_sikisgo1_m3641106054 (M_jaitisvalJarvo100_t2108255250 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_jaitisvalJarvo100::M_keatooFoudotra2(System.String[],System.Int32)
extern "C"  void M_jaitisvalJarvo100_M_keatooFoudotra2_m3593682157 (M_jaitisvalJarvo100_t2108255250 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_jaitisvalJarvo100::M_sisorGalerce3(System.String[],System.Int32)
extern "C"  void M_jaitisvalJarvo100_M_sisorGalerce3_m3737957350 (M_jaitisvalJarvo100_t2108255250 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_jaitisvalJarvo100::M_torrall4(System.String[],System.Int32)
extern "C"  void M_jaitisvalJarvo100_M_torrall4_m1254503306 (M_jaitisvalJarvo100_t2108255250 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_jaitisvalJarvo100::ilo_M_teseaWhuke01(GarbageiOS.M_jaitisvalJarvo100,System.String[],System.Int32)
extern "C"  void M_jaitisvalJarvo100_ilo_M_teseaWhuke01_m2689613198 (Il2CppObject * __this /* static, unused */, M_jaitisvalJarvo100_t2108255250 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_jaitisvalJarvo100::ilo_M_keatooFoudotra22(GarbageiOS.M_jaitisvalJarvo100,System.String[],System.Int32)
extern "C"  void M_jaitisvalJarvo100_ilo_M_keatooFoudotra22_m3501666262 (Il2CppObject * __this /* static, unused */, M_jaitisvalJarvo100_t2108255250 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
