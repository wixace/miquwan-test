﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Entity.Behavior.DodgeBvr
struct DodgeBvr_t2621377959;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// Pathfinding.Path
struct Path_t1974241691;
// Entity.Behavior.IBehavior
struct IBehavior_t770859129;
// CombatEntity
struct CombatEntity_t684137495;
// Entity.Behavior.IBehaviorCtrl
struct IBehaviorCtrl_t4225040900;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Entity_Behavior_EBehaviorID1238533136.h"
#include "AssemblyU2DCSharp_Pathfinding_Path1974241691.h"
#include "AssemblyU2DCSharp_Entity_Behavior_IBehavior770859129.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_CombatEntity684137495.h"
#include "AssemblyU2DCSharp_Entity_Behavior_DodgeBvr2621377959.h"

// System.Void Entity.Behavior.DodgeBvr::.ctor()
extern "C"  void DodgeBvr__ctor_m3759364963 (DodgeBvr_t2621377959 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Entity.Behavior.EBehaviorID Entity.Behavior.DodgeBvr::get_id()
extern "C"  uint8_t DodgeBvr_get_id_m3113640855 (DodgeBvr_t2621377959 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.DodgeBvr::Reason()
extern "C"  void DodgeBvr_Reason_m127584965 (DodgeBvr_t2621377959 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.DodgeBvr::Action()
extern "C"  void DodgeBvr_Action_m3619258935 (DodgeBvr_t2621377959 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.DodgeBvr::SetParams(System.Object[])
extern "C"  void DodgeBvr_SetParams_m3863744905 (DodgeBvr_t2621377959 * __this, ObjectU5BU5D_t1108656482* ___args0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.DodgeBvr::DoEntering()
extern "C"  void DodgeBvr_DoEntering_m1227167734 (DodgeBvr_t2621377959 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.DodgeBvr::OnPathComplete(Pathfinding.Path)
extern "C"  void DodgeBvr_OnPathComplete_m726245839 (DodgeBvr_t2621377959 * __this, Path_t1974241691 * ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.DodgeBvr::FinishDodgeBvr()
extern "C"  void DodgeBvr_FinishDodgeBvr_m1384762363 (DodgeBvr_t2621377959 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.DodgeBvr::DoLeaving()
extern "C"  void DodgeBvr_DoLeaving_m2978348266 (DodgeBvr_t2621377959 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Entity.Behavior.DodgeBvr::CanTran2OtherBehavior(Entity.Behavior.IBehavior)
extern "C"  bool DodgeBvr_CanTran2OtherBehavior_m2329758608 (DodgeBvr_t2621377959 * __this, IBehavior_t770859129 * ___behavior0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.DodgeBvr::Pause()
extern "C"  void DodgeBvr_Pause_m3813490935 (DodgeBvr_t2621377959 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.DodgeBvr::Play()
extern "C"  void DodgeBvr_Play_m3042076597 (DodgeBvr_t2621377959 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Entity.Behavior.DodgeBvr::IsLockingBehavior()
extern "C"  bool DodgeBvr_IsLockingBehavior_m2433413708 (DodgeBvr_t2621377959 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Entity.Behavior.DodgeBvr::ilo_get_position1(CombatEntity)
extern "C"  Vector3_t4282066566  DodgeBvr_ilo_get_position1_m98618716 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CombatEntity Entity.Behavior.DodgeBvr::ilo_get_entity2(Entity.Behavior.IBehavior)
extern "C"  CombatEntity_t684137495 * DodgeBvr_ilo_get_entity2_m1449153626 (Il2CppObject * __this /* static, unused */, IBehavior_t770859129 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Entity.Behavior.IBehaviorCtrl Entity.Behavior.DodgeBvr::ilo_get_bvrCtrl3(CombatEntity)
extern "C"  IBehaviorCtrl_t4225040900 * DodgeBvr_ilo_get_bvrCtrl3_m3962816046 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.DodgeBvr::ilo_set_canMove4(CombatEntity,System.Boolean)
extern "C"  void DodgeBvr_ilo_set_canMove4_m1261690736 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Entity.Behavior.EBehaviorID Entity.Behavior.DodgeBvr::ilo_get_id5(Entity.Behavior.DodgeBvr)
extern "C"  uint8_t DodgeBvr_ilo_get_id5_m1443937557 (Il2CppObject * __this /* static, unused */, DodgeBvr_t2621377959 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Entity.Behavior.EBehaviorID Entity.Behavior.DodgeBvr::ilo_get_id6(Entity.Behavior.IBehavior)
extern "C"  uint8_t DodgeBvr_ilo_get_id6_m2869592090 (Il2CppObject * __this /* static, unused */, IBehavior_t770859129 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.DodgeBvr::ilo_FinishDodgeBvr7(Entity.Behavior.DodgeBvr)
extern "C"  void DodgeBvr_ilo_FinishDodgeBvr7_m1467873459 (Il2CppObject * __this /* static, unused */, DodgeBvr_t2621377959 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
