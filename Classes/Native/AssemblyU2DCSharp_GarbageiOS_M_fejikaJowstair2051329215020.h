﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_fejikaJowstair205
struct  M_fejikaJowstair205_t1329215020  : public Il2CppObject
{
public:
	// System.Single GarbageiOS.M_fejikaJowstair205::_peecur
	float ____peecur_0;
	// System.Int32 GarbageiOS.M_fejikaJowstair205::_nearlarKemchagal
	int32_t ____nearlarKemchagal_1;
	// System.Boolean GarbageiOS.M_fejikaJowstair205::_dralzowjallJearje
	bool ____dralzowjallJearje_2;
	// System.String GarbageiOS.M_fejikaJowstair205::_nayyisSurpouse
	String_t* ____nayyisSurpouse_3;
	// System.String GarbageiOS.M_fejikaJowstair205::_taixar
	String_t* ____taixar_4;
	// System.UInt32 GarbageiOS.M_fejikaJowstair205::_derboBaisou
	uint32_t ____derboBaisou_5;
	// System.String GarbageiOS.M_fejikaJowstair205::_maljeDelair
	String_t* ____maljeDelair_6;

public:
	inline static int32_t get_offset_of__peecur_0() { return static_cast<int32_t>(offsetof(M_fejikaJowstair205_t1329215020, ____peecur_0)); }
	inline float get__peecur_0() const { return ____peecur_0; }
	inline float* get_address_of__peecur_0() { return &____peecur_0; }
	inline void set__peecur_0(float value)
	{
		____peecur_0 = value;
	}

	inline static int32_t get_offset_of__nearlarKemchagal_1() { return static_cast<int32_t>(offsetof(M_fejikaJowstair205_t1329215020, ____nearlarKemchagal_1)); }
	inline int32_t get__nearlarKemchagal_1() const { return ____nearlarKemchagal_1; }
	inline int32_t* get_address_of__nearlarKemchagal_1() { return &____nearlarKemchagal_1; }
	inline void set__nearlarKemchagal_1(int32_t value)
	{
		____nearlarKemchagal_1 = value;
	}

	inline static int32_t get_offset_of__dralzowjallJearje_2() { return static_cast<int32_t>(offsetof(M_fejikaJowstair205_t1329215020, ____dralzowjallJearje_2)); }
	inline bool get__dralzowjallJearje_2() const { return ____dralzowjallJearje_2; }
	inline bool* get_address_of__dralzowjallJearje_2() { return &____dralzowjallJearje_2; }
	inline void set__dralzowjallJearje_2(bool value)
	{
		____dralzowjallJearje_2 = value;
	}

	inline static int32_t get_offset_of__nayyisSurpouse_3() { return static_cast<int32_t>(offsetof(M_fejikaJowstair205_t1329215020, ____nayyisSurpouse_3)); }
	inline String_t* get__nayyisSurpouse_3() const { return ____nayyisSurpouse_3; }
	inline String_t** get_address_of__nayyisSurpouse_3() { return &____nayyisSurpouse_3; }
	inline void set__nayyisSurpouse_3(String_t* value)
	{
		____nayyisSurpouse_3 = value;
		Il2CppCodeGenWriteBarrier(&____nayyisSurpouse_3, value);
	}

	inline static int32_t get_offset_of__taixar_4() { return static_cast<int32_t>(offsetof(M_fejikaJowstair205_t1329215020, ____taixar_4)); }
	inline String_t* get__taixar_4() const { return ____taixar_4; }
	inline String_t** get_address_of__taixar_4() { return &____taixar_4; }
	inline void set__taixar_4(String_t* value)
	{
		____taixar_4 = value;
		Il2CppCodeGenWriteBarrier(&____taixar_4, value);
	}

	inline static int32_t get_offset_of__derboBaisou_5() { return static_cast<int32_t>(offsetof(M_fejikaJowstair205_t1329215020, ____derboBaisou_5)); }
	inline uint32_t get__derboBaisou_5() const { return ____derboBaisou_5; }
	inline uint32_t* get_address_of__derboBaisou_5() { return &____derboBaisou_5; }
	inline void set__derboBaisou_5(uint32_t value)
	{
		____derboBaisou_5 = value;
	}

	inline static int32_t get_offset_of__maljeDelair_6() { return static_cast<int32_t>(offsetof(M_fejikaJowstair205_t1329215020, ____maljeDelair_6)); }
	inline String_t* get__maljeDelair_6() const { return ____maljeDelair_6; }
	inline String_t** get_address_of__maljeDelair_6() { return &____maljeDelair_6; }
	inline void set__maljeDelair_6(String_t* value)
	{
		____maljeDelair_6 = value;
		Il2CppCodeGenWriteBarrier(&____maljeDelair_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
