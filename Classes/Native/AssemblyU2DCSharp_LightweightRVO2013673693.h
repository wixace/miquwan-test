﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Mesh
struct Mesh_t4241756145;
// Pathfinding.RVO.Simulator
struct Simulator_t2705969170;
// System.Collections.Generic.List`1<Pathfinding.RVO.IAgent>
struct List_1_t4170952948;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t1355284822;
// System.Collections.Generic.List`1<UnityEngine.Color>
struct List_1_t1267765161;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t215400611;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t4024180168;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// UnityEngine.Color[]
struct ColorU5BU5D_t2441545636;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "AssemblyU2DCSharp_LightweightRVO_RVOExampleType2792658859.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LightweightRVO
struct  LightweightRVO_t2013673693  : public MonoBehaviour_t667441552
{
public:
	// System.Int32 LightweightRVO::agentCount
	int32_t ___agentCount_2;
	// System.Single LightweightRVO::exampleScale
	float ___exampleScale_3;
	// LightweightRVO/RVOExampleType LightweightRVO::type
	int32_t ___type_4;
	// System.Single LightweightRVO::radius
	float ___radius_5;
	// System.Single LightweightRVO::maxSpeed
	float ___maxSpeed_6;
	// System.Single LightweightRVO::agentTimeHorizon
	float ___agentTimeHorizon_7;
	// System.Single LightweightRVO::obstacleTimeHorizon
	float ___obstacleTimeHorizon_8;
	// System.Int32 LightweightRVO::maxNeighbours
	int32_t ___maxNeighbours_9;
	// System.Single LightweightRVO::neighbourDist
	float ___neighbourDist_10;
	// UnityEngine.Vector3 LightweightRVO::renderingOffset
	Vector3_t4282066566  ___renderingOffset_11;
	// System.Boolean LightweightRVO::debug
	bool ___debug_12;
	// UnityEngine.Mesh LightweightRVO::mesh
	Mesh_t4241756145 * ___mesh_13;
	// Pathfinding.RVO.Simulator LightweightRVO::sim
	Simulator_t2705969170 * ___sim_14;
	// System.Collections.Generic.List`1<Pathfinding.RVO.IAgent> LightweightRVO::agents
	List_1_t4170952948 * ___agents_15;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> LightweightRVO::goals
	List_1_t1355284822 * ___goals_16;
	// System.Collections.Generic.List`1<UnityEngine.Color> LightweightRVO::colors
	List_1_t1267765161 * ___colors_17;
	// UnityEngine.Vector3[] LightweightRVO::verts
	Vector3U5BU5D_t215400611* ___verts_18;
	// UnityEngine.Vector2[] LightweightRVO::uv
	Vector2U5BU5D_t4024180168* ___uv_19;
	// System.Int32[] LightweightRVO::tris
	Int32U5BU5D_t3230847821* ___tris_20;
	// UnityEngine.Color[] LightweightRVO::meshColors
	ColorU5BU5D_t2441545636* ___meshColors_21;
	// UnityEngine.Vector3[] LightweightRVO::interpolatedVelocities
	Vector3U5BU5D_t215400611* ___interpolatedVelocities_22;

public:
	inline static int32_t get_offset_of_agentCount_2() { return static_cast<int32_t>(offsetof(LightweightRVO_t2013673693, ___agentCount_2)); }
	inline int32_t get_agentCount_2() const { return ___agentCount_2; }
	inline int32_t* get_address_of_agentCount_2() { return &___agentCount_2; }
	inline void set_agentCount_2(int32_t value)
	{
		___agentCount_2 = value;
	}

	inline static int32_t get_offset_of_exampleScale_3() { return static_cast<int32_t>(offsetof(LightweightRVO_t2013673693, ___exampleScale_3)); }
	inline float get_exampleScale_3() const { return ___exampleScale_3; }
	inline float* get_address_of_exampleScale_3() { return &___exampleScale_3; }
	inline void set_exampleScale_3(float value)
	{
		___exampleScale_3 = value;
	}

	inline static int32_t get_offset_of_type_4() { return static_cast<int32_t>(offsetof(LightweightRVO_t2013673693, ___type_4)); }
	inline int32_t get_type_4() const { return ___type_4; }
	inline int32_t* get_address_of_type_4() { return &___type_4; }
	inline void set_type_4(int32_t value)
	{
		___type_4 = value;
	}

	inline static int32_t get_offset_of_radius_5() { return static_cast<int32_t>(offsetof(LightweightRVO_t2013673693, ___radius_5)); }
	inline float get_radius_5() const { return ___radius_5; }
	inline float* get_address_of_radius_5() { return &___radius_5; }
	inline void set_radius_5(float value)
	{
		___radius_5 = value;
	}

	inline static int32_t get_offset_of_maxSpeed_6() { return static_cast<int32_t>(offsetof(LightweightRVO_t2013673693, ___maxSpeed_6)); }
	inline float get_maxSpeed_6() const { return ___maxSpeed_6; }
	inline float* get_address_of_maxSpeed_6() { return &___maxSpeed_6; }
	inline void set_maxSpeed_6(float value)
	{
		___maxSpeed_6 = value;
	}

	inline static int32_t get_offset_of_agentTimeHorizon_7() { return static_cast<int32_t>(offsetof(LightweightRVO_t2013673693, ___agentTimeHorizon_7)); }
	inline float get_agentTimeHorizon_7() const { return ___agentTimeHorizon_7; }
	inline float* get_address_of_agentTimeHorizon_7() { return &___agentTimeHorizon_7; }
	inline void set_agentTimeHorizon_7(float value)
	{
		___agentTimeHorizon_7 = value;
	}

	inline static int32_t get_offset_of_obstacleTimeHorizon_8() { return static_cast<int32_t>(offsetof(LightweightRVO_t2013673693, ___obstacleTimeHorizon_8)); }
	inline float get_obstacleTimeHorizon_8() const { return ___obstacleTimeHorizon_8; }
	inline float* get_address_of_obstacleTimeHorizon_8() { return &___obstacleTimeHorizon_8; }
	inline void set_obstacleTimeHorizon_8(float value)
	{
		___obstacleTimeHorizon_8 = value;
	}

	inline static int32_t get_offset_of_maxNeighbours_9() { return static_cast<int32_t>(offsetof(LightweightRVO_t2013673693, ___maxNeighbours_9)); }
	inline int32_t get_maxNeighbours_9() const { return ___maxNeighbours_9; }
	inline int32_t* get_address_of_maxNeighbours_9() { return &___maxNeighbours_9; }
	inline void set_maxNeighbours_9(int32_t value)
	{
		___maxNeighbours_9 = value;
	}

	inline static int32_t get_offset_of_neighbourDist_10() { return static_cast<int32_t>(offsetof(LightweightRVO_t2013673693, ___neighbourDist_10)); }
	inline float get_neighbourDist_10() const { return ___neighbourDist_10; }
	inline float* get_address_of_neighbourDist_10() { return &___neighbourDist_10; }
	inline void set_neighbourDist_10(float value)
	{
		___neighbourDist_10 = value;
	}

	inline static int32_t get_offset_of_renderingOffset_11() { return static_cast<int32_t>(offsetof(LightweightRVO_t2013673693, ___renderingOffset_11)); }
	inline Vector3_t4282066566  get_renderingOffset_11() const { return ___renderingOffset_11; }
	inline Vector3_t4282066566 * get_address_of_renderingOffset_11() { return &___renderingOffset_11; }
	inline void set_renderingOffset_11(Vector3_t4282066566  value)
	{
		___renderingOffset_11 = value;
	}

	inline static int32_t get_offset_of_debug_12() { return static_cast<int32_t>(offsetof(LightweightRVO_t2013673693, ___debug_12)); }
	inline bool get_debug_12() const { return ___debug_12; }
	inline bool* get_address_of_debug_12() { return &___debug_12; }
	inline void set_debug_12(bool value)
	{
		___debug_12 = value;
	}

	inline static int32_t get_offset_of_mesh_13() { return static_cast<int32_t>(offsetof(LightweightRVO_t2013673693, ___mesh_13)); }
	inline Mesh_t4241756145 * get_mesh_13() const { return ___mesh_13; }
	inline Mesh_t4241756145 ** get_address_of_mesh_13() { return &___mesh_13; }
	inline void set_mesh_13(Mesh_t4241756145 * value)
	{
		___mesh_13 = value;
		Il2CppCodeGenWriteBarrier(&___mesh_13, value);
	}

	inline static int32_t get_offset_of_sim_14() { return static_cast<int32_t>(offsetof(LightweightRVO_t2013673693, ___sim_14)); }
	inline Simulator_t2705969170 * get_sim_14() const { return ___sim_14; }
	inline Simulator_t2705969170 ** get_address_of_sim_14() { return &___sim_14; }
	inline void set_sim_14(Simulator_t2705969170 * value)
	{
		___sim_14 = value;
		Il2CppCodeGenWriteBarrier(&___sim_14, value);
	}

	inline static int32_t get_offset_of_agents_15() { return static_cast<int32_t>(offsetof(LightweightRVO_t2013673693, ___agents_15)); }
	inline List_1_t4170952948 * get_agents_15() const { return ___agents_15; }
	inline List_1_t4170952948 ** get_address_of_agents_15() { return &___agents_15; }
	inline void set_agents_15(List_1_t4170952948 * value)
	{
		___agents_15 = value;
		Il2CppCodeGenWriteBarrier(&___agents_15, value);
	}

	inline static int32_t get_offset_of_goals_16() { return static_cast<int32_t>(offsetof(LightweightRVO_t2013673693, ___goals_16)); }
	inline List_1_t1355284822 * get_goals_16() const { return ___goals_16; }
	inline List_1_t1355284822 ** get_address_of_goals_16() { return &___goals_16; }
	inline void set_goals_16(List_1_t1355284822 * value)
	{
		___goals_16 = value;
		Il2CppCodeGenWriteBarrier(&___goals_16, value);
	}

	inline static int32_t get_offset_of_colors_17() { return static_cast<int32_t>(offsetof(LightweightRVO_t2013673693, ___colors_17)); }
	inline List_1_t1267765161 * get_colors_17() const { return ___colors_17; }
	inline List_1_t1267765161 ** get_address_of_colors_17() { return &___colors_17; }
	inline void set_colors_17(List_1_t1267765161 * value)
	{
		___colors_17 = value;
		Il2CppCodeGenWriteBarrier(&___colors_17, value);
	}

	inline static int32_t get_offset_of_verts_18() { return static_cast<int32_t>(offsetof(LightweightRVO_t2013673693, ___verts_18)); }
	inline Vector3U5BU5D_t215400611* get_verts_18() const { return ___verts_18; }
	inline Vector3U5BU5D_t215400611** get_address_of_verts_18() { return &___verts_18; }
	inline void set_verts_18(Vector3U5BU5D_t215400611* value)
	{
		___verts_18 = value;
		Il2CppCodeGenWriteBarrier(&___verts_18, value);
	}

	inline static int32_t get_offset_of_uv_19() { return static_cast<int32_t>(offsetof(LightweightRVO_t2013673693, ___uv_19)); }
	inline Vector2U5BU5D_t4024180168* get_uv_19() const { return ___uv_19; }
	inline Vector2U5BU5D_t4024180168** get_address_of_uv_19() { return &___uv_19; }
	inline void set_uv_19(Vector2U5BU5D_t4024180168* value)
	{
		___uv_19 = value;
		Il2CppCodeGenWriteBarrier(&___uv_19, value);
	}

	inline static int32_t get_offset_of_tris_20() { return static_cast<int32_t>(offsetof(LightweightRVO_t2013673693, ___tris_20)); }
	inline Int32U5BU5D_t3230847821* get_tris_20() const { return ___tris_20; }
	inline Int32U5BU5D_t3230847821** get_address_of_tris_20() { return &___tris_20; }
	inline void set_tris_20(Int32U5BU5D_t3230847821* value)
	{
		___tris_20 = value;
		Il2CppCodeGenWriteBarrier(&___tris_20, value);
	}

	inline static int32_t get_offset_of_meshColors_21() { return static_cast<int32_t>(offsetof(LightweightRVO_t2013673693, ___meshColors_21)); }
	inline ColorU5BU5D_t2441545636* get_meshColors_21() const { return ___meshColors_21; }
	inline ColorU5BU5D_t2441545636** get_address_of_meshColors_21() { return &___meshColors_21; }
	inline void set_meshColors_21(ColorU5BU5D_t2441545636* value)
	{
		___meshColors_21 = value;
		Il2CppCodeGenWriteBarrier(&___meshColors_21, value);
	}

	inline static int32_t get_offset_of_interpolatedVelocities_22() { return static_cast<int32_t>(offsetof(LightweightRVO_t2013673693, ___interpolatedVelocities_22)); }
	inline Vector3U5BU5D_t215400611* get_interpolatedVelocities_22() const { return ___interpolatedVelocities_22; }
	inline Vector3U5BU5D_t215400611** get_address_of_interpolatedVelocities_22() { return &___interpolatedVelocities_22; }
	inline void set_interpolatedVelocities_22(Vector3U5BU5D_t215400611* value)
	{
		___interpolatedVelocities_22 = value;
		Il2CppCodeGenWriteBarrier(&___interpolatedVelocities_22, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
