﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Int3,System.Object>
struct KeyCollection_t3080275847;
// System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Object>
struct Dictionary_2_t1453516396;
// System.Collections.Generic.IEnumerator`1<Pathfinding.Int3>
struct IEnumerator_1_t3885910643;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// Pathfinding.Int3[]
struct Int3U5BU5D_t516284607;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_Int31974045594.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K2068452450.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Int3,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void KeyCollection__ctor_m2083279176_gshared (KeyCollection_t3080275847 * __this, Dictionary_2_t1453516396 * ___dictionary0, const MethodInfo* method);
#define KeyCollection__ctor_m2083279176(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t3080275847 *, Dictionary_2_t1453516396 *, const MethodInfo*))KeyCollection__ctor_m2083279176_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Int3,System.Object>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1015896846_gshared (KeyCollection_t3080275847 * __this, Int3_t1974045594  ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1015896846(__this, ___item0, method) ((  void (*) (KeyCollection_t3080275847 *, Int3_t1974045594 , const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1015896846_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Int3,System.Object>::System.Collections.Generic.ICollection<TKey>.Clear()
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1256909829_gshared (KeyCollection_t3080275847 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1256909829(__this, method) ((  void (*) (KeyCollection_t3080275847 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1256909829_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Int3,System.Object>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m177369728_gshared (KeyCollection_t3080275847 * __this, Int3_t1974045594  ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m177369728(__this, ___item0, method) ((  bool (*) (KeyCollection_t3080275847 *, Int3_t1974045594 , const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m177369728_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Int3,System.Object>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3151594469_gshared (KeyCollection_t3080275847 * __this, Int3_t1974045594  ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3151594469(__this, ___item0, method) ((  bool (*) (KeyCollection_t3080275847 *, Int3_t1974045594 , const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3151594469_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Int3,System.Object>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m3383333591_gshared (KeyCollection_t3080275847 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m3383333591(__this, method) ((  Il2CppObject* (*) (KeyCollection_t3080275847 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m3383333591_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Int3,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m1859776759_gshared (KeyCollection_t3080275847 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_CopyTo_m1859776759(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t3080275847 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m1859776759_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Int3,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m2111535686_gshared (KeyCollection_t3080275847 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m2111535686(__this, method) ((  Il2CppObject * (*) (KeyCollection_t3080275847 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m2111535686_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Int3,System.Object>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m1534675425_gshared (KeyCollection_t3080275847 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m1534675425(__this, method) ((  bool (*) (KeyCollection_t3080275847 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m1534675425_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Int3,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m389220307_gshared (KeyCollection_t3080275847 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m389220307(__this, method) ((  bool (*) (KeyCollection_t3080275847 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m389220307_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Int3,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m3741191109_gshared (KeyCollection_t3080275847 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m3741191109(__this, method) ((  Il2CppObject * (*) (KeyCollection_t3080275847 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m3741191109_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Int3,System.Object>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m259053565_gshared (KeyCollection_t3080275847 * __this, Int3U5BU5D_t516284607* ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_CopyTo_m259053565(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t3080275847 *, Int3U5BU5D_t516284607*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m259053565_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Int3,System.Object>::GetEnumerator()
extern "C"  Enumerator_t2068452450  KeyCollection_GetEnumerator_m1917886282_gshared (KeyCollection_t3080275847 * __this, const MethodInfo* method);
#define KeyCollection_GetEnumerator_m1917886282(__this, method) ((  Enumerator_t2068452450  (*) (KeyCollection_t3080275847 *, const MethodInfo*))KeyCollection_GetEnumerator_m1917886282_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Int3,System.Object>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m3630650253_gshared (KeyCollection_t3080275847 * __this, const MethodInfo* method);
#define KeyCollection_get_Count_m3630650253(__this, method) ((  int32_t (*) (KeyCollection_t3080275847 *, const MethodInfo*))KeyCollection_get_Count_m3630650253_gshared)(__this, method)
