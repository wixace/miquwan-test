﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.LocalAvoidance
struct LocalAvoidance_t2375621135;
// System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/VO>
struct List_1_t3540405845;
// Pathfinding.LocalAvoidance/VO
struct VO_t2172220293;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_Pathfinding_LocalAvoidance2375621135.h"
#include "AssemblyU2DCSharp_Pathfinding_LocalAvoidance_VO2172220293.h"

// System.Void Pathfinding.LocalAvoidance::.ctor()
extern "C"  void LocalAvoidance__ctor_m558540600 (LocalAvoidance_t2375621135 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.LocalAvoidance::Start()
extern "C"  void LocalAvoidance_Start_m3800645688 (LocalAvoidance_t2375621135 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.LocalAvoidance::Update()
extern "C"  void LocalAvoidance_Update_m1861751509 (LocalAvoidance_t2375621135 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Pathfinding.LocalAvoidance::GetVelocity()
extern "C"  Vector3_t4282066566  LocalAvoidance_GetVelocity_m951494127 (LocalAvoidance_t2375621135 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.LocalAvoidance::LateUpdate()
extern "C"  void LocalAvoidance_LateUpdate_m4004144667 (LocalAvoidance_t2375621135 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.LocalAvoidance::SimpleMove(UnityEngine.Vector3)
extern "C"  void LocalAvoidance_SimpleMove_m3314259146 (LocalAvoidance_t2375621135 * __this, Vector3_t4282066566  ___desiredMovement0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Pathfinding.LocalAvoidance::ClampMovement(UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  LocalAvoidance_ClampMovement_m112910451 (LocalAvoidance_t2375621135 * __this, Vector3_t4282066566  ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.LocalAvoidance::CheckSample(UnityEngine.Vector3,System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/VO>)
extern "C"  bool LocalAvoidance_CheckSample_m1587945034 (LocalAvoidance_t2375621135 * __this, Vector3_t4282066566  ___sample0, List_1_t3540405845 * ___vos1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Pathfinding.LocalAvoidance::ilo_ClampMovement1(Pathfinding.LocalAvoidance,UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  LocalAvoidance_ilo_ClampMovement1_m2866191504 (Il2CppObject * __this /* static, unused */, LocalAvoidance_t2375621135 * ____this0, Vector3_t4282066566  ___direction1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Pathfinding.LocalAvoidance::ilo_GetVelocity2(Pathfinding.LocalAvoidance)
extern "C"  Vector3_t4282066566  LocalAvoidance_ilo_GetVelocity2_m1734263509 (Il2CppObject * __this /* static, unused */, LocalAvoidance_t2375621135 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.LocalAvoidance::ilo_Contains3(Pathfinding.LocalAvoidance/VO,UnityEngine.Vector3)
extern "C"  bool LocalAvoidance_ilo_Contains3_m3361823549 (Il2CppObject * __this /* static, unused */, VO_t2172220293 * ____this0, Vector3_t4282066566  ___p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.LocalAvoidance::ilo_FinalInts4(Pathfinding.LocalAvoidance/VO,UnityEngine.Vector3,UnityEngine.Vector3,System.Boolean,UnityEngine.Vector3&)
extern "C"  bool LocalAvoidance_ilo_FinalInts4_m482545186 (Il2CppObject * __this /* static, unused */, VO_t2172220293 * ____this0, Vector3_t4282066566  ___target1, Vector3_t4282066566  ___closeEdgeConstraint2, bool ___drawGizmos3, Vector3_t4282066566 * ___closest4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.LocalAvoidance::ilo_CheckSample5(Pathfinding.LocalAvoidance,UnityEngine.Vector3,System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/VO>)
extern "C"  bool LocalAvoidance_ilo_CheckSample5_m965454217 (Il2CppObject * __this /* static, unused */, LocalAvoidance_t2375621135 * ____this0, Vector3_t4282066566  ___sample1, List_1_t3540405845 * ___vos2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
