﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Float_exchange_copper
struct Float_exchange_copper_t452915890;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// UILabel
struct UILabel_t291504320;
// System.String
struct String_t;
// FloatTextUnit
struct FloatTextUnit_t2362298029;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "AssemblyU2DCSharp_FLOAT_TEXT_ID2199345770.h"
#include "AssemblyU2DCSharp_UILabel291504320.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_FloatTextUnit2362298029.h"

// System.Void Float_exchange_copper::.ctor()
extern "C"  void Float_exchange_copper__ctor_m3995660025 (Float_exchange_copper_t452915890 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_exchange_copper::OnAwake(UnityEngine.GameObject)
extern "C"  void Float_exchange_copper_OnAwake_m114014133 (Float_exchange_copper_t452915890 * __this, GameObject_t3674682005 * ___viewGO0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_exchange_copper::OnDestroy()
extern "C"  void Float_exchange_copper_OnDestroy_m1367908210 (Float_exchange_copper_t452915890 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FLOAT_TEXT_ID Float_exchange_copper::FloatID()
extern "C"  int32_t Float_exchange_copper_FloatID_m1663600887 (Float_exchange_copper_t452915890 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_exchange_copper::Init()
extern "C"  void Float_exchange_copper_Init_m3544112187 (Float_exchange_copper_t452915890 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_exchange_copper::SetFile(System.Object[])
extern "C"  void Float_exchange_copper_SetFile_m3048366845 (Float_exchange_copper_t452915890 * __this, ObjectU5BU5D_t1108656482* ___args0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_exchange_copper::OnClickBG(UnityEngine.GameObject)
extern "C"  void Float_exchange_copper_OnClickBG_m415854653 (Float_exchange_copper_t452915890 * __this, GameObject_t3674682005 * ___go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_exchange_copper::ilo_set_text1(UILabel,System.String)
extern "C"  void Float_exchange_copper_ilo_set_text1_m779591267 (Il2CppObject * __this /* static, unused */, UILabel_t291504320 * ____this0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_exchange_copper::ilo_Death2(FloatTextUnit)
extern "C"  void Float_exchange_copper_ilo_Death2_m2244384393 (Il2CppObject * __this /* static, unused */, FloatTextUnit_t2362298029 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
