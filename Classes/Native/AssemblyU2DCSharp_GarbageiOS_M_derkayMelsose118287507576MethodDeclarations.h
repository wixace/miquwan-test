﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_derkayMelsose118
struct M_derkayMelsose118_t287507576;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"

// System.Void GarbageiOS.M_derkayMelsose118::.ctor()
extern "C"  void M_derkayMelsose118__ctor_m2582839483 (M_derkayMelsose118_t287507576 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_derkayMelsose118::M_wayhiTroselbair0(System.String[],System.Int32)
extern "C"  void M_derkayMelsose118_M_wayhiTroselbair0_m1790420609 (M_derkayMelsose118_t287507576 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_derkayMelsose118::M_curhorhearNeehal1(System.String[],System.Int32)
extern "C"  void M_derkayMelsose118_M_curhorhearNeehal1_m41977425 (M_derkayMelsose118_t287507576 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
