﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_nemkaku42
struct  M_nemkaku42_t1288500244  : public Il2CppObject
{
public:
	// System.Boolean GarbageiOS.M_nemkaku42::_toxem
	bool ____toxem_0;
	// System.Single GarbageiOS.M_nemkaku42::_rurmanel
	float ____rurmanel_1;
	// System.String GarbageiOS.M_nemkaku42::_yearcase
	String_t* ____yearcase_2;
	// System.String GarbageiOS.M_nemkaku42::_stowpaWiji
	String_t* ____stowpaWiji_3;
	// System.Boolean GarbageiOS.M_nemkaku42::_mokawHekurta
	bool ____mokawHekurta_4;
	// System.Single GarbageiOS.M_nemkaku42::_gaygerbe
	float ____gaygerbe_5;
	// System.Int32 GarbageiOS.M_nemkaku42::_yivisle
	int32_t ____yivisle_6;
	// System.UInt32 GarbageiOS.M_nemkaku42::_leewhorcasCejerere
	uint32_t ____leewhorcasCejerere_7;
	// System.UInt32 GarbageiOS.M_nemkaku42::_qere
	uint32_t ____qere_8;
	// System.UInt32 GarbageiOS.M_nemkaku42::_ballmeaChallbasqou
	uint32_t ____ballmeaChallbasqou_9;
	// System.String GarbageiOS.M_nemkaku42::_dodemkaMelereku
	String_t* ____dodemkaMelereku_10;
	// System.Int32 GarbageiOS.M_nemkaku42::_geyeeTorbirgu
	int32_t ____geyeeTorbirgu_11;
	// System.String GarbageiOS.M_nemkaku42::_salveWismee
	String_t* ____salveWismee_12;
	// System.UInt32 GarbageiOS.M_nemkaku42::_louhere
	uint32_t ____louhere_13;
	// System.Single GarbageiOS.M_nemkaku42::_joopounir
	float ____joopounir_14;
	// System.Single GarbageiOS.M_nemkaku42::_bairfir
	float ____bairfir_15;
	// System.String GarbageiOS.M_nemkaku42::_beci
	String_t* ____beci_16;
	// System.Int32 GarbageiOS.M_nemkaku42::_nayhair
	int32_t ____nayhair_17;
	// System.String GarbageiOS.M_nemkaku42::_gurxemjar
	String_t* ____gurxemjar_18;
	// System.String GarbageiOS.M_nemkaku42::_gaycheqoGogou
	String_t* ____gaycheqoGogou_19;
	// System.String GarbageiOS.M_nemkaku42::_silearjai
	String_t* ____silearjai_20;
	// System.Int32 GarbageiOS.M_nemkaku42::_cowall
	int32_t ____cowall_21;
	// System.Boolean GarbageiOS.M_nemkaku42::_nerloReajea
	bool ____nerloReajea_22;
	// System.String GarbageiOS.M_nemkaku42::_jugodas
	String_t* ____jugodas_23;
	// System.Int32 GarbageiOS.M_nemkaku42::_jeeposeTohooral
	int32_t ____jeeposeTohooral_24;
	// System.Int32 GarbageiOS.M_nemkaku42::_waihootea
	int32_t ____waihootea_25;
	// System.Single GarbageiOS.M_nemkaku42::_beagisNearrowstow
	float ____beagisNearrowstow_26;
	// System.Boolean GarbageiOS.M_nemkaku42::_pisair
	bool ____pisair_27;
	// System.Boolean GarbageiOS.M_nemkaku42::_jalkaiQostese
	bool ____jalkaiQostese_28;
	// System.String GarbageiOS.M_nemkaku42::_dawna
	String_t* ____dawna_29;
	// System.Int32 GarbageiOS.M_nemkaku42::_trisouReardege
	int32_t ____trisouReardege_30;
	// System.String GarbageiOS.M_nemkaku42::_zeticis
	String_t* ____zeticis_31;
	// System.String GarbageiOS.M_nemkaku42::_deldriBotay
	String_t* ____deldriBotay_32;
	// System.Single GarbageiOS.M_nemkaku42::_wumallirRitawsis
	float ____wumallirRitawsis_33;
	// System.Single GarbageiOS.M_nemkaku42::_trawlairpaTelboo
	float ____trawlairpaTelboo_34;
	// System.String GarbageiOS.M_nemkaku42::_gelvupi
	String_t* ____gelvupi_35;
	// System.String GarbageiOS.M_nemkaku42::_fallige
	String_t* ____fallige_36;
	// System.Boolean GarbageiOS.M_nemkaku42::_mearmowSeavurhou
	bool ____mearmowSeavurhou_37;

public:
	inline static int32_t get_offset_of__toxem_0() { return static_cast<int32_t>(offsetof(M_nemkaku42_t1288500244, ____toxem_0)); }
	inline bool get__toxem_0() const { return ____toxem_0; }
	inline bool* get_address_of__toxem_0() { return &____toxem_0; }
	inline void set__toxem_0(bool value)
	{
		____toxem_0 = value;
	}

	inline static int32_t get_offset_of__rurmanel_1() { return static_cast<int32_t>(offsetof(M_nemkaku42_t1288500244, ____rurmanel_1)); }
	inline float get__rurmanel_1() const { return ____rurmanel_1; }
	inline float* get_address_of__rurmanel_1() { return &____rurmanel_1; }
	inline void set__rurmanel_1(float value)
	{
		____rurmanel_1 = value;
	}

	inline static int32_t get_offset_of__yearcase_2() { return static_cast<int32_t>(offsetof(M_nemkaku42_t1288500244, ____yearcase_2)); }
	inline String_t* get__yearcase_2() const { return ____yearcase_2; }
	inline String_t** get_address_of__yearcase_2() { return &____yearcase_2; }
	inline void set__yearcase_2(String_t* value)
	{
		____yearcase_2 = value;
		Il2CppCodeGenWriteBarrier(&____yearcase_2, value);
	}

	inline static int32_t get_offset_of__stowpaWiji_3() { return static_cast<int32_t>(offsetof(M_nemkaku42_t1288500244, ____stowpaWiji_3)); }
	inline String_t* get__stowpaWiji_3() const { return ____stowpaWiji_3; }
	inline String_t** get_address_of__stowpaWiji_3() { return &____stowpaWiji_3; }
	inline void set__stowpaWiji_3(String_t* value)
	{
		____stowpaWiji_3 = value;
		Il2CppCodeGenWriteBarrier(&____stowpaWiji_3, value);
	}

	inline static int32_t get_offset_of__mokawHekurta_4() { return static_cast<int32_t>(offsetof(M_nemkaku42_t1288500244, ____mokawHekurta_4)); }
	inline bool get__mokawHekurta_4() const { return ____mokawHekurta_4; }
	inline bool* get_address_of__mokawHekurta_4() { return &____mokawHekurta_4; }
	inline void set__mokawHekurta_4(bool value)
	{
		____mokawHekurta_4 = value;
	}

	inline static int32_t get_offset_of__gaygerbe_5() { return static_cast<int32_t>(offsetof(M_nemkaku42_t1288500244, ____gaygerbe_5)); }
	inline float get__gaygerbe_5() const { return ____gaygerbe_5; }
	inline float* get_address_of__gaygerbe_5() { return &____gaygerbe_5; }
	inline void set__gaygerbe_5(float value)
	{
		____gaygerbe_5 = value;
	}

	inline static int32_t get_offset_of__yivisle_6() { return static_cast<int32_t>(offsetof(M_nemkaku42_t1288500244, ____yivisle_6)); }
	inline int32_t get__yivisle_6() const { return ____yivisle_6; }
	inline int32_t* get_address_of__yivisle_6() { return &____yivisle_6; }
	inline void set__yivisle_6(int32_t value)
	{
		____yivisle_6 = value;
	}

	inline static int32_t get_offset_of__leewhorcasCejerere_7() { return static_cast<int32_t>(offsetof(M_nemkaku42_t1288500244, ____leewhorcasCejerere_7)); }
	inline uint32_t get__leewhorcasCejerere_7() const { return ____leewhorcasCejerere_7; }
	inline uint32_t* get_address_of__leewhorcasCejerere_7() { return &____leewhorcasCejerere_7; }
	inline void set__leewhorcasCejerere_7(uint32_t value)
	{
		____leewhorcasCejerere_7 = value;
	}

	inline static int32_t get_offset_of__qere_8() { return static_cast<int32_t>(offsetof(M_nemkaku42_t1288500244, ____qere_8)); }
	inline uint32_t get__qere_8() const { return ____qere_8; }
	inline uint32_t* get_address_of__qere_8() { return &____qere_8; }
	inline void set__qere_8(uint32_t value)
	{
		____qere_8 = value;
	}

	inline static int32_t get_offset_of__ballmeaChallbasqou_9() { return static_cast<int32_t>(offsetof(M_nemkaku42_t1288500244, ____ballmeaChallbasqou_9)); }
	inline uint32_t get__ballmeaChallbasqou_9() const { return ____ballmeaChallbasqou_9; }
	inline uint32_t* get_address_of__ballmeaChallbasqou_9() { return &____ballmeaChallbasqou_9; }
	inline void set__ballmeaChallbasqou_9(uint32_t value)
	{
		____ballmeaChallbasqou_9 = value;
	}

	inline static int32_t get_offset_of__dodemkaMelereku_10() { return static_cast<int32_t>(offsetof(M_nemkaku42_t1288500244, ____dodemkaMelereku_10)); }
	inline String_t* get__dodemkaMelereku_10() const { return ____dodemkaMelereku_10; }
	inline String_t** get_address_of__dodemkaMelereku_10() { return &____dodemkaMelereku_10; }
	inline void set__dodemkaMelereku_10(String_t* value)
	{
		____dodemkaMelereku_10 = value;
		Il2CppCodeGenWriteBarrier(&____dodemkaMelereku_10, value);
	}

	inline static int32_t get_offset_of__geyeeTorbirgu_11() { return static_cast<int32_t>(offsetof(M_nemkaku42_t1288500244, ____geyeeTorbirgu_11)); }
	inline int32_t get__geyeeTorbirgu_11() const { return ____geyeeTorbirgu_11; }
	inline int32_t* get_address_of__geyeeTorbirgu_11() { return &____geyeeTorbirgu_11; }
	inline void set__geyeeTorbirgu_11(int32_t value)
	{
		____geyeeTorbirgu_11 = value;
	}

	inline static int32_t get_offset_of__salveWismee_12() { return static_cast<int32_t>(offsetof(M_nemkaku42_t1288500244, ____salveWismee_12)); }
	inline String_t* get__salveWismee_12() const { return ____salveWismee_12; }
	inline String_t** get_address_of__salveWismee_12() { return &____salveWismee_12; }
	inline void set__salveWismee_12(String_t* value)
	{
		____salveWismee_12 = value;
		Il2CppCodeGenWriteBarrier(&____salveWismee_12, value);
	}

	inline static int32_t get_offset_of__louhere_13() { return static_cast<int32_t>(offsetof(M_nemkaku42_t1288500244, ____louhere_13)); }
	inline uint32_t get__louhere_13() const { return ____louhere_13; }
	inline uint32_t* get_address_of__louhere_13() { return &____louhere_13; }
	inline void set__louhere_13(uint32_t value)
	{
		____louhere_13 = value;
	}

	inline static int32_t get_offset_of__joopounir_14() { return static_cast<int32_t>(offsetof(M_nemkaku42_t1288500244, ____joopounir_14)); }
	inline float get__joopounir_14() const { return ____joopounir_14; }
	inline float* get_address_of__joopounir_14() { return &____joopounir_14; }
	inline void set__joopounir_14(float value)
	{
		____joopounir_14 = value;
	}

	inline static int32_t get_offset_of__bairfir_15() { return static_cast<int32_t>(offsetof(M_nemkaku42_t1288500244, ____bairfir_15)); }
	inline float get__bairfir_15() const { return ____bairfir_15; }
	inline float* get_address_of__bairfir_15() { return &____bairfir_15; }
	inline void set__bairfir_15(float value)
	{
		____bairfir_15 = value;
	}

	inline static int32_t get_offset_of__beci_16() { return static_cast<int32_t>(offsetof(M_nemkaku42_t1288500244, ____beci_16)); }
	inline String_t* get__beci_16() const { return ____beci_16; }
	inline String_t** get_address_of__beci_16() { return &____beci_16; }
	inline void set__beci_16(String_t* value)
	{
		____beci_16 = value;
		Il2CppCodeGenWriteBarrier(&____beci_16, value);
	}

	inline static int32_t get_offset_of__nayhair_17() { return static_cast<int32_t>(offsetof(M_nemkaku42_t1288500244, ____nayhair_17)); }
	inline int32_t get__nayhair_17() const { return ____nayhair_17; }
	inline int32_t* get_address_of__nayhair_17() { return &____nayhair_17; }
	inline void set__nayhair_17(int32_t value)
	{
		____nayhair_17 = value;
	}

	inline static int32_t get_offset_of__gurxemjar_18() { return static_cast<int32_t>(offsetof(M_nemkaku42_t1288500244, ____gurxemjar_18)); }
	inline String_t* get__gurxemjar_18() const { return ____gurxemjar_18; }
	inline String_t** get_address_of__gurxemjar_18() { return &____gurxemjar_18; }
	inline void set__gurxemjar_18(String_t* value)
	{
		____gurxemjar_18 = value;
		Il2CppCodeGenWriteBarrier(&____gurxemjar_18, value);
	}

	inline static int32_t get_offset_of__gaycheqoGogou_19() { return static_cast<int32_t>(offsetof(M_nemkaku42_t1288500244, ____gaycheqoGogou_19)); }
	inline String_t* get__gaycheqoGogou_19() const { return ____gaycheqoGogou_19; }
	inline String_t** get_address_of__gaycheqoGogou_19() { return &____gaycheqoGogou_19; }
	inline void set__gaycheqoGogou_19(String_t* value)
	{
		____gaycheqoGogou_19 = value;
		Il2CppCodeGenWriteBarrier(&____gaycheqoGogou_19, value);
	}

	inline static int32_t get_offset_of__silearjai_20() { return static_cast<int32_t>(offsetof(M_nemkaku42_t1288500244, ____silearjai_20)); }
	inline String_t* get__silearjai_20() const { return ____silearjai_20; }
	inline String_t** get_address_of__silearjai_20() { return &____silearjai_20; }
	inline void set__silearjai_20(String_t* value)
	{
		____silearjai_20 = value;
		Il2CppCodeGenWriteBarrier(&____silearjai_20, value);
	}

	inline static int32_t get_offset_of__cowall_21() { return static_cast<int32_t>(offsetof(M_nemkaku42_t1288500244, ____cowall_21)); }
	inline int32_t get__cowall_21() const { return ____cowall_21; }
	inline int32_t* get_address_of__cowall_21() { return &____cowall_21; }
	inline void set__cowall_21(int32_t value)
	{
		____cowall_21 = value;
	}

	inline static int32_t get_offset_of__nerloReajea_22() { return static_cast<int32_t>(offsetof(M_nemkaku42_t1288500244, ____nerloReajea_22)); }
	inline bool get__nerloReajea_22() const { return ____nerloReajea_22; }
	inline bool* get_address_of__nerloReajea_22() { return &____nerloReajea_22; }
	inline void set__nerloReajea_22(bool value)
	{
		____nerloReajea_22 = value;
	}

	inline static int32_t get_offset_of__jugodas_23() { return static_cast<int32_t>(offsetof(M_nemkaku42_t1288500244, ____jugodas_23)); }
	inline String_t* get__jugodas_23() const { return ____jugodas_23; }
	inline String_t** get_address_of__jugodas_23() { return &____jugodas_23; }
	inline void set__jugodas_23(String_t* value)
	{
		____jugodas_23 = value;
		Il2CppCodeGenWriteBarrier(&____jugodas_23, value);
	}

	inline static int32_t get_offset_of__jeeposeTohooral_24() { return static_cast<int32_t>(offsetof(M_nemkaku42_t1288500244, ____jeeposeTohooral_24)); }
	inline int32_t get__jeeposeTohooral_24() const { return ____jeeposeTohooral_24; }
	inline int32_t* get_address_of__jeeposeTohooral_24() { return &____jeeposeTohooral_24; }
	inline void set__jeeposeTohooral_24(int32_t value)
	{
		____jeeposeTohooral_24 = value;
	}

	inline static int32_t get_offset_of__waihootea_25() { return static_cast<int32_t>(offsetof(M_nemkaku42_t1288500244, ____waihootea_25)); }
	inline int32_t get__waihootea_25() const { return ____waihootea_25; }
	inline int32_t* get_address_of__waihootea_25() { return &____waihootea_25; }
	inline void set__waihootea_25(int32_t value)
	{
		____waihootea_25 = value;
	}

	inline static int32_t get_offset_of__beagisNearrowstow_26() { return static_cast<int32_t>(offsetof(M_nemkaku42_t1288500244, ____beagisNearrowstow_26)); }
	inline float get__beagisNearrowstow_26() const { return ____beagisNearrowstow_26; }
	inline float* get_address_of__beagisNearrowstow_26() { return &____beagisNearrowstow_26; }
	inline void set__beagisNearrowstow_26(float value)
	{
		____beagisNearrowstow_26 = value;
	}

	inline static int32_t get_offset_of__pisair_27() { return static_cast<int32_t>(offsetof(M_nemkaku42_t1288500244, ____pisair_27)); }
	inline bool get__pisair_27() const { return ____pisair_27; }
	inline bool* get_address_of__pisair_27() { return &____pisair_27; }
	inline void set__pisair_27(bool value)
	{
		____pisair_27 = value;
	}

	inline static int32_t get_offset_of__jalkaiQostese_28() { return static_cast<int32_t>(offsetof(M_nemkaku42_t1288500244, ____jalkaiQostese_28)); }
	inline bool get__jalkaiQostese_28() const { return ____jalkaiQostese_28; }
	inline bool* get_address_of__jalkaiQostese_28() { return &____jalkaiQostese_28; }
	inline void set__jalkaiQostese_28(bool value)
	{
		____jalkaiQostese_28 = value;
	}

	inline static int32_t get_offset_of__dawna_29() { return static_cast<int32_t>(offsetof(M_nemkaku42_t1288500244, ____dawna_29)); }
	inline String_t* get__dawna_29() const { return ____dawna_29; }
	inline String_t** get_address_of__dawna_29() { return &____dawna_29; }
	inline void set__dawna_29(String_t* value)
	{
		____dawna_29 = value;
		Il2CppCodeGenWriteBarrier(&____dawna_29, value);
	}

	inline static int32_t get_offset_of__trisouReardege_30() { return static_cast<int32_t>(offsetof(M_nemkaku42_t1288500244, ____trisouReardege_30)); }
	inline int32_t get__trisouReardege_30() const { return ____trisouReardege_30; }
	inline int32_t* get_address_of__trisouReardege_30() { return &____trisouReardege_30; }
	inline void set__trisouReardege_30(int32_t value)
	{
		____trisouReardege_30 = value;
	}

	inline static int32_t get_offset_of__zeticis_31() { return static_cast<int32_t>(offsetof(M_nemkaku42_t1288500244, ____zeticis_31)); }
	inline String_t* get__zeticis_31() const { return ____zeticis_31; }
	inline String_t** get_address_of__zeticis_31() { return &____zeticis_31; }
	inline void set__zeticis_31(String_t* value)
	{
		____zeticis_31 = value;
		Il2CppCodeGenWriteBarrier(&____zeticis_31, value);
	}

	inline static int32_t get_offset_of__deldriBotay_32() { return static_cast<int32_t>(offsetof(M_nemkaku42_t1288500244, ____deldriBotay_32)); }
	inline String_t* get__deldriBotay_32() const { return ____deldriBotay_32; }
	inline String_t** get_address_of__deldriBotay_32() { return &____deldriBotay_32; }
	inline void set__deldriBotay_32(String_t* value)
	{
		____deldriBotay_32 = value;
		Il2CppCodeGenWriteBarrier(&____deldriBotay_32, value);
	}

	inline static int32_t get_offset_of__wumallirRitawsis_33() { return static_cast<int32_t>(offsetof(M_nemkaku42_t1288500244, ____wumallirRitawsis_33)); }
	inline float get__wumallirRitawsis_33() const { return ____wumallirRitawsis_33; }
	inline float* get_address_of__wumallirRitawsis_33() { return &____wumallirRitawsis_33; }
	inline void set__wumallirRitawsis_33(float value)
	{
		____wumallirRitawsis_33 = value;
	}

	inline static int32_t get_offset_of__trawlairpaTelboo_34() { return static_cast<int32_t>(offsetof(M_nemkaku42_t1288500244, ____trawlairpaTelboo_34)); }
	inline float get__trawlairpaTelboo_34() const { return ____trawlairpaTelboo_34; }
	inline float* get_address_of__trawlairpaTelboo_34() { return &____trawlairpaTelboo_34; }
	inline void set__trawlairpaTelboo_34(float value)
	{
		____trawlairpaTelboo_34 = value;
	}

	inline static int32_t get_offset_of__gelvupi_35() { return static_cast<int32_t>(offsetof(M_nemkaku42_t1288500244, ____gelvupi_35)); }
	inline String_t* get__gelvupi_35() const { return ____gelvupi_35; }
	inline String_t** get_address_of__gelvupi_35() { return &____gelvupi_35; }
	inline void set__gelvupi_35(String_t* value)
	{
		____gelvupi_35 = value;
		Il2CppCodeGenWriteBarrier(&____gelvupi_35, value);
	}

	inline static int32_t get_offset_of__fallige_36() { return static_cast<int32_t>(offsetof(M_nemkaku42_t1288500244, ____fallige_36)); }
	inline String_t* get__fallige_36() const { return ____fallige_36; }
	inline String_t** get_address_of__fallige_36() { return &____fallige_36; }
	inline void set__fallige_36(String_t* value)
	{
		____fallige_36 = value;
		Il2CppCodeGenWriteBarrier(&____fallige_36, value);
	}

	inline static int32_t get_offset_of__mearmowSeavurhou_37() { return static_cast<int32_t>(offsetof(M_nemkaku42_t1288500244, ____mearmowSeavurhou_37)); }
	inline bool get__mearmowSeavurhou_37() const { return ____mearmowSeavurhou_37; }
	inline bool* get_address_of__mearmowSeavurhou_37() { return &____mearmowSeavurhou_37; }
	inline void set__mearmowSeavurhou_37(bool value)
	{
		____mearmowSeavurhou_37 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
