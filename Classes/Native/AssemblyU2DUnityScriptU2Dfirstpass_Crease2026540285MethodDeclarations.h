﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Crease
struct Crease_t2026540285;
// UnityEngine.RenderTexture
struct RenderTexture_t1963041563;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture1963041563.h"

// System.Void Crease::.ctor()
extern "C"  void Crease__ctor_m4015790055 (Crease_t2026540285 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Crease::CheckResources()
extern "C"  bool Crease_CheckResources_m4147814048 (Crease_t2026540285 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Crease::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void Crease_OnRenderImage_m4140501015 (Crease_t2026540285 * __this, RenderTexture_t1963041563 * ___source0, RenderTexture_t1963041563 * ___destination1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Crease::Main()
extern "C"  void Crease_Main_m1707603958 (Crease_t2026540285 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
