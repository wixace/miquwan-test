﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Int32[]
struct Int32U5BU5D_t3230847821;

#include "AssemblyU2DCSharp_PropertyID1067426256.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MethodID
struct  MethodID_t3916401116  : public PropertyID_t1067426256
{
public:
	// System.Int32[] MethodID::genericParameterPositions
	Int32U5BU5D_t3230847821* ___genericParameterPositions_6;

public:
	inline static int32_t get_offset_of_genericParameterPositions_6() { return static_cast<int32_t>(offsetof(MethodID_t3916401116, ___genericParameterPositions_6)); }
	inline Int32U5BU5D_t3230847821* get_genericParameterPositions_6() const { return ___genericParameterPositions_6; }
	inline Int32U5BU5D_t3230847821** get_address_of_genericParameterPositions_6() { return &___genericParameterPositions_6; }
	inline void set_genericParameterPositions_6(Int32U5BU5D_t3230847821* value)
	{
		___genericParameterPositions_6 = value;
		Il2CppCodeGenWriteBarrier(&___genericParameterPositions_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
