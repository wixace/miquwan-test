﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>
struct Dictionary_2_t509053110;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1826376502.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_407833816.h"
#include "AssemblyU2DCSharp_Mihua_Assets_SubAssetMgr_ErrorIn2633981210.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m1912791287_gshared (Enumerator_t1826376502 * __this, Dictionary_2_t509053110 * ___dictionary0, const MethodInfo* method);
#define Enumerator__ctor_m1912791287(__this, ___dictionary0, method) ((  void (*) (Enumerator_t1826376502 *, Dictionary_2_t509053110 *, const MethodInfo*))Enumerator__ctor_m1912791287_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1259720724_gshared (Enumerator_t1826376502 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1259720724(__this, method) ((  Il2CppObject * (*) (Enumerator_t1826376502 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1259720724_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m310780766_gshared (Enumerator_t1826376502 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m310780766(__this, method) ((  void (*) (Enumerator_t1826376502 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m310780766_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t1751606614  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2971872853_gshared (Enumerator_t1826376502 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2971872853(__this, method) ((  DictionaryEntry_t1751606614  (*) (Enumerator_t1826376502 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2971872853_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3875909104_gshared (Enumerator_t1826376502 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3875909104(__this, method) ((  Il2CppObject * (*) (Enumerator_t1826376502 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3875909104_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2060439682_gshared (Enumerator_t1826376502 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2060439682(__this, method) ((  Il2CppObject * (*) (Enumerator_t1826376502 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2060439682_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m119841934_gshared (Enumerator_t1826376502 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m119841934(__this, method) ((  bool (*) (Enumerator_t1826376502 *, const MethodInfo*))Enumerator_MoveNext_m119841934_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::get_Current()
extern "C"  KeyValuePair_2_t407833816  Enumerator_get_Current_m1456259310_gshared (Enumerator_t1826376502 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m1456259310(__this, method) ((  KeyValuePair_2_t407833816  (*) (Enumerator_t1826376502 *, const MethodInfo*))Enumerator_get_Current_m1456259310_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::get_CurrentKey()
extern "C"  Il2CppObject * Enumerator_get_CurrentKey_m1563444759_gshared (Enumerator_t1826376502 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m1563444759(__this, method) ((  Il2CppObject * (*) (Enumerator_t1826376502 *, const MethodInfo*))Enumerator_get_CurrentKey_m1563444759_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::get_CurrentValue()
extern "C"  ErrorInfo_t2633981210  Enumerator_get_CurrentValue_m788763671_gshared (Enumerator_t1826376502 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m788763671(__this, method) ((  ErrorInfo_t2633981210  (*) (Enumerator_t1826376502 *, const MethodInfo*))Enumerator_get_CurrentValue_m788763671_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::Reset()
extern "C"  void Enumerator_Reset_m4037146825_gshared (Enumerator_t1826376502 * __this, const MethodInfo* method);
#define Enumerator_Reset_m4037146825(__this, method) ((  void (*) (Enumerator_t1826376502 *, const MethodInfo*))Enumerator_Reset_m4037146825_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::VerifyState()
extern "C"  void Enumerator_VerifyState_m4162833938_gshared (Enumerator_t1826376502 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m4162833938(__this, method) ((  void (*) (Enumerator_t1826376502 *, const MethodInfo*))Enumerator_VerifyState_m4162833938_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::VerifyCurrent()
extern "C"  void Enumerator_VerifyCurrent_m318766330_gshared (Enumerator_t1826376502 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m318766330(__this, method) ((  void (*) (Enumerator_t1826376502 *, const MethodInfo*))Enumerator_VerifyCurrent_m318766330_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::Dispose()
extern "C"  void Enumerator_Dispose_m3866059609_gshared (Enumerator_t1826376502 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m3866059609(__this, method) ((  void (*) (Enumerator_t1826376502 *, const MethodInfo*))Enumerator_Dispose_m3866059609_gshared)(__this, method)
