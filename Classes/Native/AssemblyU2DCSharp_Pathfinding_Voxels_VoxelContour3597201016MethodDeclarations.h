﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.Voxels.VoxelContour
struct VoxelContour_t3597201016;
struct VoxelContour_t3597201016_marshaled_pinvoke;
struct VoxelContour_t3597201016_marshaled_com;

#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct VoxelContour_t3597201016;
struct VoxelContour_t3597201016_marshaled_pinvoke;

extern "C" void VoxelContour_t3597201016_marshal_pinvoke(const VoxelContour_t3597201016& unmarshaled, VoxelContour_t3597201016_marshaled_pinvoke& marshaled);
extern "C" void VoxelContour_t3597201016_marshal_pinvoke_back(const VoxelContour_t3597201016_marshaled_pinvoke& marshaled, VoxelContour_t3597201016& unmarshaled);
extern "C" void VoxelContour_t3597201016_marshal_pinvoke_cleanup(VoxelContour_t3597201016_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct VoxelContour_t3597201016;
struct VoxelContour_t3597201016_marshaled_com;

extern "C" void VoxelContour_t3597201016_marshal_com(const VoxelContour_t3597201016& unmarshaled, VoxelContour_t3597201016_marshaled_com& marshaled);
extern "C" void VoxelContour_t3597201016_marshal_com_back(const VoxelContour_t3597201016_marshaled_com& marshaled, VoxelContour_t3597201016& unmarshaled);
extern "C" void VoxelContour_t3597201016_marshal_com_cleanup(VoxelContour_t3597201016_marshaled_com& marshaled);
