﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FlowControl.FlowSplashScreen
struct FlowSplashScreen_t1652246581;
// VersionMgr
struct VersionMgr_t1322950208;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_FlowControl_FlowSplashScreen1652246581.h"

// System.Void FlowControl.FlowSplashScreen::.ctor()
extern "C"  void FlowSplashScreen__ctor_m1053998235 (FlowSplashScreen_t1652246581 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlowControl.FlowSplashScreen::Activate()
extern "C"  void FlowSplashScreen_Activate_m3151131004 (FlowSplashScreen_t1652246581 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FlowControl.FlowSplashScreen::ShowNext()
extern "C"  bool FlowSplashScreen_ShowNext_m1992730509 (FlowSplashScreen_t1652246581 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlowControl.FlowSplashScreen::FlowUpdate(System.Single)
extern "C"  void FlowSplashScreen_FlowUpdate_m3045177483 (FlowSplashScreen_t1652246581 * __this, float ___duringTime0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlowControl.FlowSplashScreen::OnComplete()
extern "C"  void FlowSplashScreen_OnComplete_m4280435073 (FlowSplashScreen_t1652246581 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionMgr FlowControl.FlowSplashScreen::ilo_get_Instance1()
extern "C"  VersionMgr_t1322950208 * FlowSplashScreen_ilo_get_Instance1_m3799149404 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FlowControl.FlowSplashScreen::ilo_ShowNext2(FlowControl.FlowSplashScreen)
extern "C"  bool FlowSplashScreen_ilo_ShowNext2_m2373733652 (Il2CppObject * __this /* static, unused */, FlowSplashScreen_t1652246581 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
