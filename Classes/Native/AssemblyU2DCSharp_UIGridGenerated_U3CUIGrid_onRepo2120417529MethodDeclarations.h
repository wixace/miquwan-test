﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIGridGenerated/<UIGrid_onReposition_GetDelegate_member9_arg0>c__AnonStoreyC4
struct U3CUIGrid_onReposition_GetDelegate_member9_arg0U3Ec__AnonStoreyC4_t2120417529;

#include "codegen/il2cpp-codegen.h"

// System.Void UIGridGenerated/<UIGrid_onReposition_GetDelegate_member9_arg0>c__AnonStoreyC4::.ctor()
extern "C"  void U3CUIGrid_onReposition_GetDelegate_member9_arg0U3Ec__AnonStoreyC4__ctor_m2380998802 (U3CUIGrid_onReposition_GetDelegate_member9_arg0U3Ec__AnonStoreyC4_t2120417529 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIGridGenerated/<UIGrid_onReposition_GetDelegate_member9_arg0>c__AnonStoreyC4::<>m__14C()
extern "C"  void U3CUIGrid_onReposition_GetDelegate_member9_arg0U3Ec__AnonStoreyC4_U3CU3Em__14C_m3780324583 (U3CUIGrid_onReposition_GetDelegate_member9_arg0U3Ec__AnonStoreyC4_t2120417529 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
