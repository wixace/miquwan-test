﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GenericTypeCache/TypeMembers
struct TypeMembers_t2369396673;

#include "codegen/il2cpp-codegen.h"

// System.Void GenericTypeCache/TypeMembers::.ctor()
extern "C"  void TypeMembers__ctor_m3619146746 (TypeMembers_t2369396673 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
