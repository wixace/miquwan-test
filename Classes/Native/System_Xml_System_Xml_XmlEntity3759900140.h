﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Xml.XmlLinkedNode
struct XmlLinkedNode_t901819716;

#include "System_Xml_System_Xml_XmlNode856910923.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlEntity
struct  XmlEntity_t3759900140  : public XmlNode_t856910923
{
public:
	// System.String System.Xml.XmlEntity::name
	String_t* ___name_7;
	// System.String System.Xml.XmlEntity::NDATA
	String_t* ___NDATA_8;
	// System.String System.Xml.XmlEntity::publicId
	String_t* ___publicId_9;
	// System.String System.Xml.XmlEntity::systemId
	String_t* ___systemId_10;
	// System.String System.Xml.XmlEntity::baseUri
	String_t* ___baseUri_11;
	// System.Xml.XmlLinkedNode System.Xml.XmlEntity::lastLinkedChild
	XmlLinkedNode_t901819716 * ___lastLinkedChild_12;
	// System.Boolean System.Xml.XmlEntity::contentAlreadySet
	bool ___contentAlreadySet_13;

public:
	inline static int32_t get_offset_of_name_7() { return static_cast<int32_t>(offsetof(XmlEntity_t3759900140, ___name_7)); }
	inline String_t* get_name_7() const { return ___name_7; }
	inline String_t** get_address_of_name_7() { return &___name_7; }
	inline void set_name_7(String_t* value)
	{
		___name_7 = value;
		Il2CppCodeGenWriteBarrier(&___name_7, value);
	}

	inline static int32_t get_offset_of_NDATA_8() { return static_cast<int32_t>(offsetof(XmlEntity_t3759900140, ___NDATA_8)); }
	inline String_t* get_NDATA_8() const { return ___NDATA_8; }
	inline String_t** get_address_of_NDATA_8() { return &___NDATA_8; }
	inline void set_NDATA_8(String_t* value)
	{
		___NDATA_8 = value;
		Il2CppCodeGenWriteBarrier(&___NDATA_8, value);
	}

	inline static int32_t get_offset_of_publicId_9() { return static_cast<int32_t>(offsetof(XmlEntity_t3759900140, ___publicId_9)); }
	inline String_t* get_publicId_9() const { return ___publicId_9; }
	inline String_t** get_address_of_publicId_9() { return &___publicId_9; }
	inline void set_publicId_9(String_t* value)
	{
		___publicId_9 = value;
		Il2CppCodeGenWriteBarrier(&___publicId_9, value);
	}

	inline static int32_t get_offset_of_systemId_10() { return static_cast<int32_t>(offsetof(XmlEntity_t3759900140, ___systemId_10)); }
	inline String_t* get_systemId_10() const { return ___systemId_10; }
	inline String_t** get_address_of_systemId_10() { return &___systemId_10; }
	inline void set_systemId_10(String_t* value)
	{
		___systemId_10 = value;
		Il2CppCodeGenWriteBarrier(&___systemId_10, value);
	}

	inline static int32_t get_offset_of_baseUri_11() { return static_cast<int32_t>(offsetof(XmlEntity_t3759900140, ___baseUri_11)); }
	inline String_t* get_baseUri_11() const { return ___baseUri_11; }
	inline String_t** get_address_of_baseUri_11() { return &___baseUri_11; }
	inline void set_baseUri_11(String_t* value)
	{
		___baseUri_11 = value;
		Il2CppCodeGenWriteBarrier(&___baseUri_11, value);
	}

	inline static int32_t get_offset_of_lastLinkedChild_12() { return static_cast<int32_t>(offsetof(XmlEntity_t3759900140, ___lastLinkedChild_12)); }
	inline XmlLinkedNode_t901819716 * get_lastLinkedChild_12() const { return ___lastLinkedChild_12; }
	inline XmlLinkedNode_t901819716 ** get_address_of_lastLinkedChild_12() { return &___lastLinkedChild_12; }
	inline void set_lastLinkedChild_12(XmlLinkedNode_t901819716 * value)
	{
		___lastLinkedChild_12 = value;
		Il2CppCodeGenWriteBarrier(&___lastLinkedChild_12, value);
	}

	inline static int32_t get_offset_of_contentAlreadySet_13() { return static_cast<int32_t>(offsetof(XmlEntity_t3759900140, ___contentAlreadySet_13)); }
	inline bool get_contentAlreadySet_13() const { return ___contentAlreadySet_13; }
	inline bool* get_address_of_contentAlreadySet_13() { return &___contentAlreadySet_13; }
	inline void set_contentAlreadySet_13(bool value)
	{
		___contentAlreadySet_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
