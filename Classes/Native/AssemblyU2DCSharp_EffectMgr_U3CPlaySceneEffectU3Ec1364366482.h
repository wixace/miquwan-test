﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// EffectMgr
struct EffectMgr_t535289511;

#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EffectMgr/<PlaySceneEffect>c__AnonStorey154
struct  U3CPlaySceneEffectU3Ec__AnonStorey154_t1364366482  : public Il2CppObject
{
public:
	// UnityEngine.Vector3 EffectMgr/<PlaySceneEffect>c__AnonStorey154::pos
	Vector3_t4282066566  ___pos_0;
	// EffectMgr EffectMgr/<PlaySceneEffect>c__AnonStorey154::<>f__this
	EffectMgr_t535289511 * ___U3CU3Ef__this_1;

public:
	inline static int32_t get_offset_of_pos_0() { return static_cast<int32_t>(offsetof(U3CPlaySceneEffectU3Ec__AnonStorey154_t1364366482, ___pos_0)); }
	inline Vector3_t4282066566  get_pos_0() const { return ___pos_0; }
	inline Vector3_t4282066566 * get_address_of_pos_0() { return &___pos_0; }
	inline void set_pos_0(Vector3_t4282066566  value)
	{
		___pos_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_1() { return static_cast<int32_t>(offsetof(U3CPlaySceneEffectU3Ec__AnonStorey154_t1364366482, ___U3CU3Ef__this_1)); }
	inline EffectMgr_t535289511 * get_U3CU3Ef__this_1() const { return ___U3CU3Ef__this_1; }
	inline EffectMgr_t535289511 ** get_address_of_U3CU3Ef__this_1() { return &___U3CU3Ef__this_1; }
	inline void set_U3CU3Ef__this_1(EffectMgr_t535289511 * value)
	{
		___U3CU3Ef__this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
