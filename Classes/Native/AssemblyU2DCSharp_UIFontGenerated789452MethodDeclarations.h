﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIFontGenerated
struct UIFontGenerated_t789452;
// JSVCall
struct JSVCall_t3708497963;
// BMFont
struct BMFont_t1962830650;
// UIFont
struct UIFont_t2503090435;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// UnityEngine.Material
struct Material_t3870600107;
// UnityEngine.Texture2D
struct Texture2D_t3884108195;
// System.String
struct String_t;
// BMSymbol
struct BMSymbol_t1170982339;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_UIFont2503090435.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_Material3870600107.h"
#include "UnityEngine_UnityEngine_Rect4241904616.h"
#include "UnityEngine_UnityEngine_FontStyle3350479768.h"
#include "mscorlib_System_String7231557.h"

// System.Void UIFontGenerated::.ctor()
extern "C"  void UIFontGenerated__ctor_m4059971871 (UIFontGenerated_t789452 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIFontGenerated::UIFont_UIFont1(JSVCall,System.Int32)
extern "C"  bool UIFontGenerated_UIFont_UIFont1_m1199890899 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIFontGenerated::UIFont_bmFont(JSVCall)
extern "C"  void UIFontGenerated_UIFont_bmFont_m852419444 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIFontGenerated::UIFont_texWidth(JSVCall)
extern "C"  void UIFontGenerated_UIFont_texWidth_m3728286831 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIFontGenerated::UIFont_texHeight(JSVCall)
extern "C"  void UIFontGenerated_UIFont_texHeight_m268218416 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIFontGenerated::UIFont_hasSymbols(JSVCall)
extern "C"  void UIFontGenerated_UIFont_hasSymbols_m2207983789 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIFontGenerated::UIFont_symbols(JSVCall)
extern "C"  void UIFontGenerated_UIFont_symbols_m3192205091 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIFontGenerated::UIFont_atlas(JSVCall)
extern "C"  void UIFontGenerated_UIFont_atlas_m1279595635 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIFontGenerated::UIFont_material(JSVCall)
extern "C"  void UIFontGenerated_UIFont_material_m1665134119 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIFontGenerated::UIFont_premultipliedAlphaShader(JSVCall)
extern "C"  void UIFontGenerated_UIFont_premultipliedAlphaShader_m3369741729 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIFontGenerated::UIFont_packedFontShader(JSVCall)
extern "C"  void UIFontGenerated_UIFont_packedFontShader_m4144874818 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIFontGenerated::UIFont_texture(JSVCall)
extern "C"  void UIFontGenerated_UIFont_texture_m1676853219 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIFontGenerated::UIFont_uvRect(JSVCall)
extern "C"  void UIFontGenerated_UIFont_uvRect_m1245972201 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIFontGenerated::UIFont_spriteName(JSVCall)
extern "C"  void UIFontGenerated_UIFont_spriteName_m1259234430 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIFontGenerated::UIFont_isValid(JSVCall)
extern "C"  void UIFontGenerated_UIFont_isValid_m1944926700 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIFontGenerated::UIFont_defaultSize(JSVCall)
extern "C"  void UIFontGenerated_UIFont_defaultSize_m967218940 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIFontGenerated::UIFont_sprite(JSVCall)
extern "C"  void UIFontGenerated_UIFont_sprite_m3445890121 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIFontGenerated::UIFont_replacement(JSVCall)
extern "C"  void UIFontGenerated_UIFont_replacement_m1971462348 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIFontGenerated::UIFont_isDynamic(JSVCall)
extern "C"  void UIFontGenerated_UIFont_isDynamic_m1866617993 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIFontGenerated::UIFont_dynamicFont(JSVCall)
extern "C"  void UIFontGenerated_UIFont_dynamicFont_m3166799152 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIFontGenerated::UIFont_dynamicFontStyle(JSVCall)
extern "C"  void UIFontGenerated_UIFont_dynamicFontStyle_m618698155 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIFontGenerated::UIFont_AddSymbol__String__String(JSVCall,System.Int32)
extern "C"  bool UIFontGenerated_UIFont_AddSymbol__String__String_m4146122784 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIFontGenerated::UIFont_MarkAsChanged(JSVCall,System.Int32)
extern "C"  bool UIFontGenerated_UIFont_MarkAsChanged_m3974493626 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIFontGenerated::UIFont_MatchSymbol__String__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool UIFontGenerated_UIFont_MatchSymbol__String__Int32__Int32_m1533090739 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIFontGenerated::UIFont_RemoveSymbol__String(JSVCall,System.Int32)
extern "C"  bool UIFontGenerated_UIFont_RemoveSymbol__String_m1707885962 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIFontGenerated::UIFont_RenameSymbol__String__String(JSVCall,System.Int32)
extern "C"  bool UIFontGenerated_UIFont_RenameSymbol__String__String_m521300373 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIFontGenerated::UIFont_UpdateUVRect(JSVCall,System.Int32)
extern "C"  bool UIFontGenerated_UIFont_UpdateUVRect_m291153323 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIFontGenerated::UIFont_UsesSprite__String(JSVCall,System.Int32)
extern "C"  bool UIFontGenerated_UIFont_UsesSprite__String_m3509157951 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIFontGenerated::UIFont_CheckIfRelated__UIFont__UIFont(JSVCall,System.Int32)
extern "C"  bool UIFontGenerated_UIFont_CheckIfRelated__UIFont__UIFont_m4117957865 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIFontGenerated::__Register()
extern "C"  void UIFontGenerated___Register_m575280904 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// BMFont UIFontGenerated::ilo_get_bmFont1(UIFont)
extern "C"  BMFont_t1962830650 * UIFontGenerated_ilo_get_bmFont1_m1412004856 (Il2CppObject * __this /* static, unused */, UIFont_t2503090435 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIFontGenerated::ilo_getInt322(System.Int32)
extern "C"  int32_t UIFontGenerated_ilo_getInt322_m430412419 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UIFontGenerated::ilo_getObject3(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UIFontGenerated_ilo_getObject3_m3499693032 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIFontGenerated::ilo_setObject4(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UIFontGenerated_ilo_setObject4_m466988210 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIFontGenerated::ilo_set_material5(UIFont,UnityEngine.Material)
extern "C"  void UIFontGenerated_ilo_set_material5_m3665778608 (Il2CppObject * __this /* static, unused */, UIFont_t2503090435 * ____this0, Material_t3870600107 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture2D UIFontGenerated::ilo_get_texture6(UIFont)
extern "C"  Texture2D_t3884108195 * UIFontGenerated_ilo_get_texture6_m3325636060 (Il2CppObject * __this /* static, unused */, UIFont_t2503090435 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect UIFontGenerated::ilo_get_uvRect7(UIFont)
extern "C"  Rect_t4241904616  UIFontGenerated_ilo_get_uvRect7_m3084985330 (Il2CppObject * __this /* static, unused */, UIFont_t2503090435 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UIFontGenerated::ilo_getStringS8(System.Int32)
extern "C"  String_t* UIFontGenerated_ilo_getStringS8_m3767125450 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIFontGenerated::ilo_setBooleanS9(System.Int32,System.Boolean)
extern "C"  void UIFontGenerated_ilo_setBooleanS9_m1863856456 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIFontGenerated::ilo_setInt3210(System.Int32,System.Int32)
extern "C"  void UIFontGenerated_ilo_setInt3210_m2374753283 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIFontGenerated::ilo_set_defaultSize11(UIFont,System.Int32)
extern "C"  void UIFontGenerated_ilo_set_defaultSize11_m1891732235 (Il2CppObject * __this /* static, unused */, UIFont_t2503090435 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.FontStyle UIFontGenerated::ilo_get_dynamicFontStyle12(UIFont)
extern "C"  int32_t UIFontGenerated_ilo_get_dynamicFontStyle12_m1801032346 (Il2CppObject * __this /* static, unused */, UIFont_t2503090435 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIFontGenerated::ilo_setEnum13(System.Int32,System.Int32)
extern "C"  void UIFontGenerated_ilo_setEnum13_m1980831511 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIFontGenerated::ilo_getEnum14(System.Int32)
extern "C"  int32_t UIFontGenerated_ilo_getEnum14_m1481858563 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIFontGenerated::ilo_set_dynamicFontStyle15(UIFont,UnityEngine.FontStyle)
extern "C"  void UIFontGenerated_ilo_set_dynamicFontStyle15_m4275247418 (Il2CppObject * __this /* static, unused */, UIFont_t2503090435 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIFontGenerated::ilo_MarkAsChanged16(UIFont)
extern "C"  void UIFontGenerated_ilo_MarkAsChanged16_m3570737857 (Il2CppObject * __this /* static, unused */, UIFont_t2503090435 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// BMSymbol UIFontGenerated::ilo_MatchSymbol17(UIFont,System.String,System.Int32,System.Int32)
extern "C"  BMSymbol_t1170982339 * UIFontGenerated_ilo_MatchSymbol17_m1428372832 (Il2CppObject * __this /* static, unused */, UIFont_t2503090435 * ____this0, String_t* ___text1, int32_t ___offset2, int32_t ___textLength3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIFontGenerated::ilo_RemoveSymbol18(UIFont,System.String)
extern "C"  void UIFontGenerated_ilo_RemoveSymbol18_m1542555444 (Il2CppObject * __this /* static, unused */, UIFont_t2503090435 * ____this0, String_t* ___sequence1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIFontGenerated::ilo_RenameSymbol19(UIFont,System.String,System.String)
extern "C"  void UIFontGenerated_ilo_RenameSymbol19_m2211735467 (Il2CppObject * __this /* static, unused */, UIFont_t2503090435 * ____this0, String_t* ___before1, String_t* ___after2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIFontGenerated::ilo_CheckIfRelated20(UIFont,UIFont)
extern "C"  bool UIFontGenerated_ilo_CheckIfRelated20_m2130197502 (Il2CppObject * __this /* static, unused */, UIFont_t2503090435 * ___a0, UIFont_t2503090435 * ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
