﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System_Collections_Generic_List77Generated/<ListA1_FindIndex__PredicateT1_T>c__AnonStorey88
struct U3CListA1_FindIndex__PredicateT1_TU3Ec__AnonStorey88_t1064049838;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void System_Collections_Generic_List77Generated/<ListA1_FindIndex__PredicateT1_T>c__AnonStorey88::.ctor()
extern "C"  void U3CListA1_FindIndex__PredicateT1_TU3Ec__AnonStorey88__ctor_m1623962237 (U3CListA1_FindIndex__PredicateT1_TU3Ec__AnonStorey88_t1064049838 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System_Collections_Generic_List77Generated/<ListA1_FindIndex__PredicateT1_T>c__AnonStorey88::<>m__AD()
extern "C"  Il2CppObject * U3CListA1_FindIndex__PredicateT1_TU3Ec__AnonStorey88_U3CU3Em__AD_m1725760094 (U3CListA1_FindIndex__PredicateT1_TU3Ec__AnonStorey88_t1064049838 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
