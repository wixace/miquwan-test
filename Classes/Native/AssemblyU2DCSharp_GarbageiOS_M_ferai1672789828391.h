﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_ferai167
struct  M_ferai167_t2789828391  : public Il2CppObject
{
public:
	// System.UInt32 GarbageiOS.M_ferai167::_tradrawrir
	uint32_t ____tradrawrir_0;
	// System.UInt32 GarbageiOS.M_ferai167::_trerederSirnu
	uint32_t ____trerederSirnu_1;
	// System.Single GarbageiOS.M_ferai167::_qusar
	float ____qusar_2;
	// System.String GarbageiOS.M_ferai167::_nelporsa
	String_t* ____nelporsa_3;
	// System.String GarbageiOS.M_ferai167::_gibelsur
	String_t* ____gibelsur_4;
	// System.UInt32 GarbageiOS.M_ferai167::_calri
	uint32_t ____calri_5;
	// System.UInt32 GarbageiOS.M_ferai167::_piqatru
	uint32_t ____piqatru_6;
	// System.UInt32 GarbageiOS.M_ferai167::_hoogowfeRemi
	uint32_t ____hoogowfeRemi_7;
	// System.Boolean GarbageiOS.M_ferai167::_keelarwiBistu
	bool ____keelarwiBistu_8;
	// System.Int32 GarbageiOS.M_ferai167::_mairfaiGairrefal
	int32_t ____mairfaiGairrefal_9;
	// System.Single GarbageiOS.M_ferai167::_stormur
	float ____stormur_10;
	// System.Single GarbageiOS.M_ferai167::_tesasgaDremjere
	float ____tesasgaDremjere_11;
	// System.Int32 GarbageiOS.M_ferai167::_sastearTairtis
	int32_t ____sastearTairtis_12;
	// System.Boolean GarbageiOS.M_ferai167::_tira
	bool ____tira_13;
	// System.Int32 GarbageiOS.M_ferai167::_wokall
	int32_t ____wokall_14;
	// System.String GarbageiOS.M_ferai167::_bearroSulear
	String_t* ____bearroSulear_15;
	// System.String GarbageiOS.M_ferai167::_harcormaw
	String_t* ____harcormaw_16;
	// System.UInt32 GarbageiOS.M_ferai167::_nanoudeaSerusai
	uint32_t ____nanoudeaSerusai_17;
	// System.Single GarbageiOS.M_ferai167::_toudarLele
	float ____toudarLele_18;
	// System.Single GarbageiOS.M_ferai167::_sirmasDejor
	float ____sirmasDejor_19;
	// System.Int32 GarbageiOS.M_ferai167::_nerevetro
	int32_t ____nerevetro_20;
	// System.Int32 GarbageiOS.M_ferai167::_raceSirar
	int32_t ____raceSirar_21;
	// System.Boolean GarbageiOS.M_ferai167::_kaybeger
	bool ____kaybeger_22;
	// System.UInt32 GarbageiOS.M_ferai167::_baske
	uint32_t ____baske_23;

public:
	inline static int32_t get_offset_of__tradrawrir_0() { return static_cast<int32_t>(offsetof(M_ferai167_t2789828391, ____tradrawrir_0)); }
	inline uint32_t get__tradrawrir_0() const { return ____tradrawrir_0; }
	inline uint32_t* get_address_of__tradrawrir_0() { return &____tradrawrir_0; }
	inline void set__tradrawrir_0(uint32_t value)
	{
		____tradrawrir_0 = value;
	}

	inline static int32_t get_offset_of__trerederSirnu_1() { return static_cast<int32_t>(offsetof(M_ferai167_t2789828391, ____trerederSirnu_1)); }
	inline uint32_t get__trerederSirnu_1() const { return ____trerederSirnu_1; }
	inline uint32_t* get_address_of__trerederSirnu_1() { return &____trerederSirnu_1; }
	inline void set__trerederSirnu_1(uint32_t value)
	{
		____trerederSirnu_1 = value;
	}

	inline static int32_t get_offset_of__qusar_2() { return static_cast<int32_t>(offsetof(M_ferai167_t2789828391, ____qusar_2)); }
	inline float get__qusar_2() const { return ____qusar_2; }
	inline float* get_address_of__qusar_2() { return &____qusar_2; }
	inline void set__qusar_2(float value)
	{
		____qusar_2 = value;
	}

	inline static int32_t get_offset_of__nelporsa_3() { return static_cast<int32_t>(offsetof(M_ferai167_t2789828391, ____nelporsa_3)); }
	inline String_t* get__nelporsa_3() const { return ____nelporsa_3; }
	inline String_t** get_address_of__nelporsa_3() { return &____nelporsa_3; }
	inline void set__nelporsa_3(String_t* value)
	{
		____nelporsa_3 = value;
		Il2CppCodeGenWriteBarrier(&____nelporsa_3, value);
	}

	inline static int32_t get_offset_of__gibelsur_4() { return static_cast<int32_t>(offsetof(M_ferai167_t2789828391, ____gibelsur_4)); }
	inline String_t* get__gibelsur_4() const { return ____gibelsur_4; }
	inline String_t** get_address_of__gibelsur_4() { return &____gibelsur_4; }
	inline void set__gibelsur_4(String_t* value)
	{
		____gibelsur_4 = value;
		Il2CppCodeGenWriteBarrier(&____gibelsur_4, value);
	}

	inline static int32_t get_offset_of__calri_5() { return static_cast<int32_t>(offsetof(M_ferai167_t2789828391, ____calri_5)); }
	inline uint32_t get__calri_5() const { return ____calri_5; }
	inline uint32_t* get_address_of__calri_5() { return &____calri_5; }
	inline void set__calri_5(uint32_t value)
	{
		____calri_5 = value;
	}

	inline static int32_t get_offset_of__piqatru_6() { return static_cast<int32_t>(offsetof(M_ferai167_t2789828391, ____piqatru_6)); }
	inline uint32_t get__piqatru_6() const { return ____piqatru_6; }
	inline uint32_t* get_address_of__piqatru_6() { return &____piqatru_6; }
	inline void set__piqatru_6(uint32_t value)
	{
		____piqatru_6 = value;
	}

	inline static int32_t get_offset_of__hoogowfeRemi_7() { return static_cast<int32_t>(offsetof(M_ferai167_t2789828391, ____hoogowfeRemi_7)); }
	inline uint32_t get__hoogowfeRemi_7() const { return ____hoogowfeRemi_7; }
	inline uint32_t* get_address_of__hoogowfeRemi_7() { return &____hoogowfeRemi_7; }
	inline void set__hoogowfeRemi_7(uint32_t value)
	{
		____hoogowfeRemi_7 = value;
	}

	inline static int32_t get_offset_of__keelarwiBistu_8() { return static_cast<int32_t>(offsetof(M_ferai167_t2789828391, ____keelarwiBistu_8)); }
	inline bool get__keelarwiBistu_8() const { return ____keelarwiBistu_8; }
	inline bool* get_address_of__keelarwiBistu_8() { return &____keelarwiBistu_8; }
	inline void set__keelarwiBistu_8(bool value)
	{
		____keelarwiBistu_8 = value;
	}

	inline static int32_t get_offset_of__mairfaiGairrefal_9() { return static_cast<int32_t>(offsetof(M_ferai167_t2789828391, ____mairfaiGairrefal_9)); }
	inline int32_t get__mairfaiGairrefal_9() const { return ____mairfaiGairrefal_9; }
	inline int32_t* get_address_of__mairfaiGairrefal_9() { return &____mairfaiGairrefal_9; }
	inline void set__mairfaiGairrefal_9(int32_t value)
	{
		____mairfaiGairrefal_9 = value;
	}

	inline static int32_t get_offset_of__stormur_10() { return static_cast<int32_t>(offsetof(M_ferai167_t2789828391, ____stormur_10)); }
	inline float get__stormur_10() const { return ____stormur_10; }
	inline float* get_address_of__stormur_10() { return &____stormur_10; }
	inline void set__stormur_10(float value)
	{
		____stormur_10 = value;
	}

	inline static int32_t get_offset_of__tesasgaDremjere_11() { return static_cast<int32_t>(offsetof(M_ferai167_t2789828391, ____tesasgaDremjere_11)); }
	inline float get__tesasgaDremjere_11() const { return ____tesasgaDremjere_11; }
	inline float* get_address_of__tesasgaDremjere_11() { return &____tesasgaDremjere_11; }
	inline void set__tesasgaDremjere_11(float value)
	{
		____tesasgaDremjere_11 = value;
	}

	inline static int32_t get_offset_of__sastearTairtis_12() { return static_cast<int32_t>(offsetof(M_ferai167_t2789828391, ____sastearTairtis_12)); }
	inline int32_t get__sastearTairtis_12() const { return ____sastearTairtis_12; }
	inline int32_t* get_address_of__sastearTairtis_12() { return &____sastearTairtis_12; }
	inline void set__sastearTairtis_12(int32_t value)
	{
		____sastearTairtis_12 = value;
	}

	inline static int32_t get_offset_of__tira_13() { return static_cast<int32_t>(offsetof(M_ferai167_t2789828391, ____tira_13)); }
	inline bool get__tira_13() const { return ____tira_13; }
	inline bool* get_address_of__tira_13() { return &____tira_13; }
	inline void set__tira_13(bool value)
	{
		____tira_13 = value;
	}

	inline static int32_t get_offset_of__wokall_14() { return static_cast<int32_t>(offsetof(M_ferai167_t2789828391, ____wokall_14)); }
	inline int32_t get__wokall_14() const { return ____wokall_14; }
	inline int32_t* get_address_of__wokall_14() { return &____wokall_14; }
	inline void set__wokall_14(int32_t value)
	{
		____wokall_14 = value;
	}

	inline static int32_t get_offset_of__bearroSulear_15() { return static_cast<int32_t>(offsetof(M_ferai167_t2789828391, ____bearroSulear_15)); }
	inline String_t* get__bearroSulear_15() const { return ____bearroSulear_15; }
	inline String_t** get_address_of__bearroSulear_15() { return &____bearroSulear_15; }
	inline void set__bearroSulear_15(String_t* value)
	{
		____bearroSulear_15 = value;
		Il2CppCodeGenWriteBarrier(&____bearroSulear_15, value);
	}

	inline static int32_t get_offset_of__harcormaw_16() { return static_cast<int32_t>(offsetof(M_ferai167_t2789828391, ____harcormaw_16)); }
	inline String_t* get__harcormaw_16() const { return ____harcormaw_16; }
	inline String_t** get_address_of__harcormaw_16() { return &____harcormaw_16; }
	inline void set__harcormaw_16(String_t* value)
	{
		____harcormaw_16 = value;
		Il2CppCodeGenWriteBarrier(&____harcormaw_16, value);
	}

	inline static int32_t get_offset_of__nanoudeaSerusai_17() { return static_cast<int32_t>(offsetof(M_ferai167_t2789828391, ____nanoudeaSerusai_17)); }
	inline uint32_t get__nanoudeaSerusai_17() const { return ____nanoudeaSerusai_17; }
	inline uint32_t* get_address_of__nanoudeaSerusai_17() { return &____nanoudeaSerusai_17; }
	inline void set__nanoudeaSerusai_17(uint32_t value)
	{
		____nanoudeaSerusai_17 = value;
	}

	inline static int32_t get_offset_of__toudarLele_18() { return static_cast<int32_t>(offsetof(M_ferai167_t2789828391, ____toudarLele_18)); }
	inline float get__toudarLele_18() const { return ____toudarLele_18; }
	inline float* get_address_of__toudarLele_18() { return &____toudarLele_18; }
	inline void set__toudarLele_18(float value)
	{
		____toudarLele_18 = value;
	}

	inline static int32_t get_offset_of__sirmasDejor_19() { return static_cast<int32_t>(offsetof(M_ferai167_t2789828391, ____sirmasDejor_19)); }
	inline float get__sirmasDejor_19() const { return ____sirmasDejor_19; }
	inline float* get_address_of__sirmasDejor_19() { return &____sirmasDejor_19; }
	inline void set__sirmasDejor_19(float value)
	{
		____sirmasDejor_19 = value;
	}

	inline static int32_t get_offset_of__nerevetro_20() { return static_cast<int32_t>(offsetof(M_ferai167_t2789828391, ____nerevetro_20)); }
	inline int32_t get__nerevetro_20() const { return ____nerevetro_20; }
	inline int32_t* get_address_of__nerevetro_20() { return &____nerevetro_20; }
	inline void set__nerevetro_20(int32_t value)
	{
		____nerevetro_20 = value;
	}

	inline static int32_t get_offset_of__raceSirar_21() { return static_cast<int32_t>(offsetof(M_ferai167_t2789828391, ____raceSirar_21)); }
	inline int32_t get__raceSirar_21() const { return ____raceSirar_21; }
	inline int32_t* get_address_of__raceSirar_21() { return &____raceSirar_21; }
	inline void set__raceSirar_21(int32_t value)
	{
		____raceSirar_21 = value;
	}

	inline static int32_t get_offset_of__kaybeger_22() { return static_cast<int32_t>(offsetof(M_ferai167_t2789828391, ____kaybeger_22)); }
	inline bool get__kaybeger_22() const { return ____kaybeger_22; }
	inline bool* get_address_of__kaybeger_22() { return &____kaybeger_22; }
	inline void set__kaybeger_22(bool value)
	{
		____kaybeger_22 = value;
	}

	inline static int32_t get_offset_of__baske_23() { return static_cast<int32_t>(offsetof(M_ferai167_t2789828391, ____baske_23)); }
	inline uint32_t get__baske_23() const { return ____baske_23; }
	inline uint32_t* get_address_of__baske_23() { return &____baske_23; }
	inline void set__baske_23(uint32_t value)
	{
		____baske_23 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
