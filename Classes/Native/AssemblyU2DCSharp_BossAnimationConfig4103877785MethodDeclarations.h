﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BossAnimationConfig
struct BossAnimationConfig_t4103877785;
// BossAnimationInfo
struct BossAnimationInfo_t384335813;

#include "codegen/il2cpp-codegen.h"

// System.Void BossAnimationConfig::.ctor()
extern "C"  void BossAnimationConfig__ctor_m132184818 (BossAnimationConfig_t4103877785 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// BossAnimationConfig BossAnimationConfig::get_Instance()
extern "C"  BossAnimationConfig_t4103877785 * BossAnimationConfig_get_Instance_m1212038550 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BossAnimationConfig::InitLevelConfig()
extern "C"  void BossAnimationConfig_InitLevelConfig_m2357302406 (BossAnimationConfig_t4103877785 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// BossAnimationInfo BossAnimationConfig::GetBossCfg(System.Int32)
extern "C"  BossAnimationInfo_t384335813 * BossAnimationConfig_GetBossCfg_m3878482934 (BossAnimationConfig_t4103877785 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
