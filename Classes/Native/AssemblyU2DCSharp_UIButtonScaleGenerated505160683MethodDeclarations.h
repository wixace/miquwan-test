﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIButtonScaleGenerated
struct UIButtonScaleGenerated_t505160683;
// JSVCall
struct JSVCall_t3708497963;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void UIButtonScaleGenerated::.ctor()
extern "C"  void UIButtonScaleGenerated__ctor_m3930212624 (UIButtonScaleGenerated_t505160683 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIButtonScaleGenerated::UIButtonScale_UIButtonScale1(JSVCall,System.Int32)
extern "C"  bool UIButtonScaleGenerated_UIButtonScale_UIButtonScale1_m3357434784 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonScaleGenerated::UIButtonScale_tweenTarget(JSVCall)
extern "C"  void UIButtonScaleGenerated_UIButtonScale_tweenTarget_m1812696856 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonScaleGenerated::UIButtonScale_hover(JSVCall)
extern "C"  void UIButtonScaleGenerated_UIButtonScale_hover_m1945158712 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonScaleGenerated::UIButtonScale_pressed(JSVCall)
extern "C"  void UIButtonScaleGenerated_UIButtonScale_pressed_m626221202 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonScaleGenerated::UIButtonScale_duration(JSVCall)
extern "C"  void UIButtonScaleGenerated_UIButtonScale_duration_m2007149380 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonScaleGenerated::__Register()
extern "C"  void UIButtonScaleGenerated___Register_m1102208439 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIButtonScaleGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UIButtonScaleGenerated_ilo_getObject1_m3855236482 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIButtonScaleGenerated::ilo_setObject2(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UIButtonScaleGenerated_ilo_setObject2_m766211899 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UIButtonScaleGenerated::ilo_getObject3(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UIButtonScaleGenerated_ilo_getObject3_m2374313761 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonScaleGenerated::ilo_setVector3S4(System.Int32,UnityEngine.Vector3)
extern "C"  void UIButtonScaleGenerated_ilo_setVector3S4_m2011095674 (Il2CppObject * __this /* static, unused */, int32_t ___e0, Vector3_t4282066566  ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UIButtonScaleGenerated::ilo_getVector3S5(System.Int32)
extern "C"  Vector3_t4282066566  UIButtonScaleGenerated_ilo_getVector3S5_m2062946606 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonScaleGenerated::ilo_setSingle6(System.Int32,System.Single)
extern "C"  void UIButtonScaleGenerated_ilo_setSingle6_m3099274217 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
