﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_PushType1840642452.h"
#include "mscorlib_System_DateTime4283661327.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PluginPush/NotificationData
struct  NotificationData_t3289515031  : public Il2CppObject
{
public:
	// PushType PluginPush/NotificationData::pushType
	int32_t ___pushType_0;
	// System.String PluginPush/NotificationData::message
	String_t* ___message_1;
	// System.String PluginPush/NotificationData::integralTimes
	String_t* ___integralTimes_2;
	// System.DateTime PluginPush/NotificationData::delayDate
	DateTime_t4283661327  ___delayDate_3;
	// System.Boolean PluginPush/NotificationData::isLoopTimer
	bool ___isLoopTimer_4;
	// System.Boolean PluginPush/NotificationData::isClose
	bool ___isClose_5;

public:
	inline static int32_t get_offset_of_pushType_0() { return static_cast<int32_t>(offsetof(NotificationData_t3289515031, ___pushType_0)); }
	inline int32_t get_pushType_0() const { return ___pushType_0; }
	inline int32_t* get_address_of_pushType_0() { return &___pushType_0; }
	inline void set_pushType_0(int32_t value)
	{
		___pushType_0 = value;
	}

	inline static int32_t get_offset_of_message_1() { return static_cast<int32_t>(offsetof(NotificationData_t3289515031, ___message_1)); }
	inline String_t* get_message_1() const { return ___message_1; }
	inline String_t** get_address_of_message_1() { return &___message_1; }
	inline void set_message_1(String_t* value)
	{
		___message_1 = value;
		Il2CppCodeGenWriteBarrier(&___message_1, value);
	}

	inline static int32_t get_offset_of_integralTimes_2() { return static_cast<int32_t>(offsetof(NotificationData_t3289515031, ___integralTimes_2)); }
	inline String_t* get_integralTimes_2() const { return ___integralTimes_2; }
	inline String_t** get_address_of_integralTimes_2() { return &___integralTimes_2; }
	inline void set_integralTimes_2(String_t* value)
	{
		___integralTimes_2 = value;
		Il2CppCodeGenWriteBarrier(&___integralTimes_2, value);
	}

	inline static int32_t get_offset_of_delayDate_3() { return static_cast<int32_t>(offsetof(NotificationData_t3289515031, ___delayDate_3)); }
	inline DateTime_t4283661327  get_delayDate_3() const { return ___delayDate_3; }
	inline DateTime_t4283661327 * get_address_of_delayDate_3() { return &___delayDate_3; }
	inline void set_delayDate_3(DateTime_t4283661327  value)
	{
		___delayDate_3 = value;
	}

	inline static int32_t get_offset_of_isLoopTimer_4() { return static_cast<int32_t>(offsetof(NotificationData_t3289515031, ___isLoopTimer_4)); }
	inline bool get_isLoopTimer_4() const { return ___isLoopTimer_4; }
	inline bool* get_address_of_isLoopTimer_4() { return &___isLoopTimer_4; }
	inline void set_isLoopTimer_4(bool value)
	{
		___isLoopTimer_4 = value;
	}

	inline static int32_t get_offset_of_isClose_5() { return static_cast<int32_t>(offsetof(NotificationData_t3289515031, ___isClose_5)); }
	inline bool get_isClose_5() const { return ___isClose_5; }
	inline bool* get_address_of_isClose_5() { return &___isClose_5; }
	inline void set_isClose_5(bool value)
	{
		___isClose_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
