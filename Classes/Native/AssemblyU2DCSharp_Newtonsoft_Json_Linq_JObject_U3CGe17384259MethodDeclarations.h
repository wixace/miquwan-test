﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Linq.JObject/<GetEnumerator>c__Iterator1D
struct U3CGetEnumeratorU3Ec__Iterator1D_t17384259;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24131445027.h"

// System.Void Newtonsoft.Json.Linq.JObject/<GetEnumerator>c__Iterator1D::.ctor()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator1D__ctor_m1928420744 (U3CGetEnumeratorU3Ec__Iterator1D_t17384259 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.KeyValuePair`2<System.String,Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JObject/<GetEnumerator>c__Iterator1D::System.Collections.Generic.IEnumerator<System.Collections.Generic.KeyValuePair<string,Newtonsoft.Json.Linq.JToken>>.get_Current()
extern "C"  KeyValuePair_2_t4131445027  U3CGetEnumeratorU3Ec__Iterator1D_System_Collections_Generic_IEnumeratorU3CSystem_Collections_Generic_KeyValuePairU3CstringU2CNewtonsoft_Json_Linq_JTokenU3EU3E_get_Current_m1417251216 (U3CGetEnumeratorU3Ec__Iterator1D_t17384259 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Linq.JObject/<GetEnumerator>c__Iterator1D::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator1D_System_Collections_IEnumerator_get_Current_m2283246238 (U3CGetEnumeratorU3Ec__Iterator1D_t17384259 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Linq.JObject/<GetEnumerator>c__Iterator1D::MoveNext()
extern "C"  bool U3CGetEnumeratorU3Ec__Iterator1D_MoveNext_m3446328428 (U3CGetEnumeratorU3Ec__Iterator1D_t17384259 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JObject/<GetEnumerator>c__Iterator1D::Dispose()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator1D_Dispose_m1979713477 (U3CGetEnumeratorU3Ec__Iterator1D_t17384259 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JObject/<GetEnumerator>c__Iterator1D::Reset()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator1D_Reset_m3869820981 (U3CGetEnumeratorU3Ec__Iterator1D_t17384259 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
