﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>
struct Dictionary_2_t2952530300;
// System.Collections.Generic.Dictionary`2<UIModelDisplayType,UIModelDisplay>
struct Dictionary_2_t3529212389;
// UIModelDisplayMgr
struct UIModelDisplayMgr_t1446490315;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIModelDisplayMgr
struct  UIModelDisplayMgr_t1446490315  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.GameObject UIModelDisplayMgr::uiModelRoot
	GameObject_t3674682005 * ___uiModelRoot_2;
	// System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32> UIModelDisplayMgr::idDic
	Dictionary_2_t2952530300 * ___idDic_3;
	// System.Collections.Generic.Dictionary`2<UIModelDisplayType,UIModelDisplay> UIModelDisplayMgr::uiModelList
	Dictionary_2_t3529212389 * ___uiModelList_4;

public:
	inline static int32_t get_offset_of_uiModelRoot_2() { return static_cast<int32_t>(offsetof(UIModelDisplayMgr_t1446490315, ___uiModelRoot_2)); }
	inline GameObject_t3674682005 * get_uiModelRoot_2() const { return ___uiModelRoot_2; }
	inline GameObject_t3674682005 ** get_address_of_uiModelRoot_2() { return &___uiModelRoot_2; }
	inline void set_uiModelRoot_2(GameObject_t3674682005 * value)
	{
		___uiModelRoot_2 = value;
		Il2CppCodeGenWriteBarrier(&___uiModelRoot_2, value);
	}

	inline static int32_t get_offset_of_idDic_3() { return static_cast<int32_t>(offsetof(UIModelDisplayMgr_t1446490315, ___idDic_3)); }
	inline Dictionary_2_t2952530300 * get_idDic_3() const { return ___idDic_3; }
	inline Dictionary_2_t2952530300 ** get_address_of_idDic_3() { return &___idDic_3; }
	inline void set_idDic_3(Dictionary_2_t2952530300 * value)
	{
		___idDic_3 = value;
		Il2CppCodeGenWriteBarrier(&___idDic_3, value);
	}

	inline static int32_t get_offset_of_uiModelList_4() { return static_cast<int32_t>(offsetof(UIModelDisplayMgr_t1446490315, ___uiModelList_4)); }
	inline Dictionary_2_t3529212389 * get_uiModelList_4() const { return ___uiModelList_4; }
	inline Dictionary_2_t3529212389 ** get_address_of_uiModelList_4() { return &___uiModelList_4; }
	inline void set_uiModelList_4(Dictionary_2_t3529212389 * value)
	{
		___uiModelList_4 = value;
		Il2CppCodeGenWriteBarrier(&___uiModelList_4, value);
	}
};

struct UIModelDisplayMgr_t1446490315_StaticFields
{
public:
	// UIModelDisplayMgr UIModelDisplayMgr::_instance
	UIModelDisplayMgr_t1446490315 * ____instance_5;

public:
	inline static int32_t get_offset_of__instance_5() { return static_cast<int32_t>(offsetof(UIModelDisplayMgr_t1446490315_StaticFields, ____instance_5)); }
	inline UIModelDisplayMgr_t1446490315 * get__instance_5() const { return ____instance_5; }
	inline UIModelDisplayMgr_t1446490315 ** get_address_of__instance_5() { return &____instance_5; }
	inline void set__instance_5(UIModelDisplayMgr_t1446490315 * value)
	{
		____instance_5 = value;
		Il2CppCodeGenWriteBarrier(&____instance_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
