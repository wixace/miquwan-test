﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// BloodMgr
struct BloodMgr_t3705885150;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Singleton`1<BloodMgr>
struct  Singleton_1_t3958700543  : public Il2CppObject
{
public:

public:
};

struct Singleton_1_t3958700543_StaticFields
{
public:
	// T Singleton`1::_instance
	BloodMgr_t3705885150 * ____instance_0;

public:
	inline static int32_t get_offset_of__instance_0() { return static_cast<int32_t>(offsetof(Singleton_1_t3958700543_StaticFields, ____instance_0)); }
	inline BloodMgr_t3705885150 * get__instance_0() const { return ____instance_0; }
	inline BloodMgr_t3705885150 ** get_address_of__instance_0() { return &____instance_0; }
	inline void set__instance_0(BloodMgr_t3705885150 * value)
	{
		____instance_0 = value;
		Il2CppCodeGenWriteBarrier(&____instance_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
