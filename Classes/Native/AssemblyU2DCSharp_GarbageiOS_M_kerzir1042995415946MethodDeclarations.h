﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_kerzir104
struct M_kerzir104_t2995415946;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"

// System.Void GarbageiOS.M_kerzir104::.ctor()
extern "C"  void M_kerzir104__ctor_m521802009 (M_kerzir104_t2995415946 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_kerzir104::M_hemqeWoubor0(System.String[],System.Int32)
extern "C"  void M_kerzir104_M_hemqeWoubor0_m3215966392 (M_kerzir104_t2995415946 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
