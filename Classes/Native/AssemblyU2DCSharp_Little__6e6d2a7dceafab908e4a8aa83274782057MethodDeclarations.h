﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._6e6d2a7dceafab908e4a8aa82e2a8697
struct _6e6d2a7dceafab908e4a8aa82e2a8697_t3274782057;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__6e6d2a7dceafab908e4a8aa83274782057.h"

// System.Void Little._6e6d2a7dceafab908e4a8aa82e2a8697::.ctor()
extern "C"  void _6e6d2a7dceafab908e4a8aa82e2a8697__ctor_m1715271396 (_6e6d2a7dceafab908e4a8aa82e2a8697_t3274782057 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._6e6d2a7dceafab908e4a8aa82e2a8697::_6e6d2a7dceafab908e4a8aa82e2a8697m2(System.Int32)
extern "C"  int32_t _6e6d2a7dceafab908e4a8aa82e2a8697__6e6d2a7dceafab908e4a8aa82e2a8697m2_m3108917497 (_6e6d2a7dceafab908e4a8aa82e2a8697_t3274782057 * __this, int32_t ____6e6d2a7dceafab908e4a8aa82e2a8697a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._6e6d2a7dceafab908e4a8aa82e2a8697::_6e6d2a7dceafab908e4a8aa82e2a8697m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _6e6d2a7dceafab908e4a8aa82e2a8697__6e6d2a7dceafab908e4a8aa82e2a8697m_m3033162205 (_6e6d2a7dceafab908e4a8aa82e2a8697_t3274782057 * __this, int32_t ____6e6d2a7dceafab908e4a8aa82e2a8697a0, int32_t ____6e6d2a7dceafab908e4a8aa82e2a8697591, int32_t ____6e6d2a7dceafab908e4a8aa82e2a8697c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._6e6d2a7dceafab908e4a8aa82e2a8697::ilo__6e6d2a7dceafab908e4a8aa82e2a8697m21(Little._6e6d2a7dceafab908e4a8aa82e2a8697,System.Int32)
extern "C"  int32_t _6e6d2a7dceafab908e4a8aa82e2a8697_ilo__6e6d2a7dceafab908e4a8aa82e2a8697m21_m299189072 (Il2CppObject * __this /* static, unused */, _6e6d2a7dceafab908e4a8aa82e2a8697_t3274782057 * ____this0, int32_t ____6e6d2a7dceafab908e4a8aa82e2a8697a1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
