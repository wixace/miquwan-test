﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// JSMgr/<InitJSEngine>c__AnonStoreyFC
struct U3CInitJSEngineU3Ec__AnonStoreyFC_t1539097821;

#include "codegen/il2cpp-codegen.h"

// System.Void JSMgr/<InitJSEngine>c__AnonStoreyFC::.ctor()
extern "C"  void U3CInitJSEngineU3Ec__AnonStoreyFC__ctor_m3179390766 (U3CInitJSEngineU3Ec__AnonStoreyFC_t1539097821 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSMgr/<InitJSEngine>c__AnonStoreyFC::<>m__31F()
extern "C"  void U3CInitJSEngineU3Ec__AnonStoreyFC_U3CU3Em__31F_m3148199411 (U3CInitJSEngineU3Ec__AnonStoreyFC_t1539097821 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSMgr/<InitJSEngine>c__AnonStoreyFC::<>m__320()
extern "C"  void U3CInitJSEngineU3Ec__AnonStoreyFC_U3CU3Em__320_m3148208060 (U3CInitJSEngineU3Ec__AnonStoreyFC_t1539097821 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
