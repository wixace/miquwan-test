﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ProtoBuf.ProtoConverterAttribute
struct ProtoConverterAttribute_t2177037642;

#include "codegen/il2cpp-codegen.h"

// System.Void ProtoBuf.ProtoConverterAttribute::.ctor()
extern "C"  void ProtoConverterAttribute__ctor_m2572122202 (ProtoConverterAttribute_t2177037642 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
