﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_InputGenerated
struct UnityEngine_InputGenerated_t3472291917;
// JSVCall
struct JSVCall_t3708497963;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "mscorlib_System_String7231557.h"

// System.Void UnityEngine_InputGenerated::.ctor()
extern "C"  void UnityEngine_InputGenerated__ctor_m755988206 (UnityEngine_InputGenerated_t3472291917 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_InputGenerated::Input_Input1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_InputGenerated_Input_Input1_m1113607606 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_InputGenerated::Input_compensateSensors(JSVCall)
extern "C"  void UnityEngine_InputGenerated_Input_compensateSensors_m2480049646 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_InputGenerated::Input_gyro(JSVCall)
extern "C"  void UnityEngine_InputGenerated_Input_gyro_m623573645 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_InputGenerated::Input_mousePosition(JSVCall)
extern "C"  void UnityEngine_InputGenerated_Input_mousePosition_m593299010 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_InputGenerated::Input_mouseScrollDelta(JSVCall)
extern "C"  void UnityEngine_InputGenerated_Input_mouseScrollDelta_m572646166 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_InputGenerated::Input_mousePresent(JSVCall)
extern "C"  void UnityEngine_InputGenerated_Input_mousePresent_m113579718 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_InputGenerated::Input_simulateMouseWithTouches(JSVCall)
extern "C"  void UnityEngine_InputGenerated_Input_simulateMouseWithTouches_m1788949302 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_InputGenerated::Input_anyKey(JSVCall)
extern "C"  void UnityEngine_InputGenerated_Input_anyKey_m4002060137 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_InputGenerated::Input_anyKeyDown(JSVCall)
extern "C"  void UnityEngine_InputGenerated_Input_anyKeyDown_m2992574311 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_InputGenerated::Input_inputString(JSVCall)
extern "C"  void UnityEngine_InputGenerated_Input_inputString_m164644821 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_InputGenerated::Input_acceleration(JSVCall)
extern "C"  void UnityEngine_InputGenerated_Input_acceleration_m1957237212 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_InputGenerated::Input_accelerationEvents(JSVCall)
extern "C"  void UnityEngine_InputGenerated_Input_accelerationEvents_m2462710851 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_InputGenerated::Input_accelerationEventCount(JSVCall)
extern "C"  void UnityEngine_InputGenerated_Input_accelerationEventCount_m244383559 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_InputGenerated::Input_touches(JSVCall)
extern "C"  void UnityEngine_InputGenerated_Input_touches_m4262469987 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_InputGenerated::Input_touchCount(JSVCall)
extern "C"  void UnityEngine_InputGenerated_Input_touchCount_m3258946764 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_InputGenerated::Input_touchPressureSupported(JSVCall)
extern "C"  void UnityEngine_InputGenerated_Input_touchPressureSupported_m3221346226 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_InputGenerated::Input_stylusTouchSupported(JSVCall)
extern "C"  void UnityEngine_InputGenerated_Input_stylusTouchSupported_m3210004955 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_InputGenerated::Input_touchSupported(JSVCall)
extern "C"  void UnityEngine_InputGenerated_Input_touchSupported_m2373120397 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_InputGenerated::Input_multiTouchEnabled(JSVCall)
extern "C"  void UnityEngine_InputGenerated_Input_multiTouchEnabled_m2728433141 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_InputGenerated::Input_location(JSVCall)
extern "C"  void UnityEngine_InputGenerated_Input_location_m385183047 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_InputGenerated::Input_compass(JSVCall)
extern "C"  void UnityEngine_InputGenerated_Input_compass_m4290752030 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_InputGenerated::Input_deviceOrientation(JSVCall)
extern "C"  void UnityEngine_InputGenerated_Input_deviceOrientation_m932149782 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_InputGenerated::Input_imeCompositionMode(JSVCall)
extern "C"  void UnityEngine_InputGenerated_Input_imeCompositionMode_m2992520944 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_InputGenerated::Input_compositionString(JSVCall)
extern "C"  void UnityEngine_InputGenerated_Input_compositionString_m3761250933 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_InputGenerated::Input_imeIsSelected(JSVCall)
extern "C"  void UnityEngine_InputGenerated_Input_imeIsSelected_m3220325802 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_InputGenerated::Input_compositionCursorPos(JSVCall)
extern "C"  void UnityEngine_InputGenerated_Input_compositionCursorPos_m363352648 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_InputGenerated::Input_backButtonLeavesApp(JSVCall)
extern "C"  void UnityEngine_InputGenerated_Input_backButtonLeavesApp_m1466537092 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_InputGenerated::Input_GetAccelerationEvent__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_InputGenerated_Input_GetAccelerationEvent__Int32_m3595809501 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_InputGenerated::Input_GetAxis__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_InputGenerated_Input_GetAxis__String_m420859835 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_InputGenerated::Input_GetAxisRaw__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_InputGenerated_Input_GetAxisRaw__String_m3137926257 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_InputGenerated::Input_GetButton__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_InputGenerated_Input_GetButton__String_m1930476940 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_InputGenerated::Input_GetButtonDown__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_InputGenerated_Input_GetButtonDown__String_m2943578574 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_InputGenerated::Input_GetButtonUp__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_InputGenerated_Input_GetButtonUp__String_m822635655 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_InputGenerated::Input_GetJoystickNames(JSVCall,System.Int32)
extern "C"  bool UnityEngine_InputGenerated_Input_GetJoystickNames_m3293344005 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_InputGenerated::Input_GetKey__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_InputGenerated_Input_GetKey__String_m781882377 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_InputGenerated::Input_GetKey__KeyCode(JSVCall,System.Int32)
extern "C"  bool UnityEngine_InputGenerated_Input_GetKey__KeyCode_m787980342 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_InputGenerated::Input_GetKeyDown__KeyCode(JSVCall,System.Int32)
extern "C"  bool UnityEngine_InputGenerated_Input_GetKeyDown__KeyCode_m3719572148 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_InputGenerated::Input_GetKeyDown__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_InputGenerated_Input_GetKeyDown__String_m1292091851 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_InputGenerated::Input_GetKeyUp__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_InputGenerated_Input_GetKeyUp__String_m829855684 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_InputGenerated::Input_GetKeyUp__KeyCode(JSVCall,System.Int32)
extern "C"  bool UnityEngine_InputGenerated_Input_GetKeyUp__KeyCode_m2275152859 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_InputGenerated::Input_GetMouseButton__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_InputGenerated_Input_GetMouseButton__Int32_m1537593280 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_InputGenerated::Input_GetMouseButtonDown__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_InputGenerated_Input_GetMouseButtonDown__Int32_m3024096702 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_InputGenerated::Input_GetMouseButtonUp__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_InputGenerated_Input_GetMouseButtonUp__Int32_m1356611237 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_InputGenerated::Input_GetTouch__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_InputGenerated_Input_GetTouch__Int32_m786442168 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_InputGenerated::Input_ResetInputAxes(JSVCall,System.Int32)
extern "C"  bool UnityEngine_InputGenerated_Input_ResetInputAxes_m2381938383 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_InputGenerated::__Register()
extern "C"  void UnityEngine_InputGenerated___Register_m4226624153 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_InputGenerated::ilo_setBooleanS1(System.Int32,System.Boolean)
extern "C"  void UnityEngine_InputGenerated_ilo_setBooleanS1_m2331784927 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_InputGenerated::ilo_getBooleanS2(System.Int32)
extern "C"  bool UnityEngine_InputGenerated_ilo_getBooleanS2_m1967517535 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_InputGenerated::ilo_setObject3(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UnityEngine_InputGenerated_ilo_setObject3_m3610257246 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_InputGenerated::ilo_setVector3S4(System.Int32,UnityEngine.Vector3)
extern "C"  void UnityEngine_InputGenerated_ilo_setVector3S4_m4001035740 (Il2CppObject * __this /* static, unused */, int32_t ___e0, Vector3_t4282066566  ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_InputGenerated::ilo_setVector2S5(System.Int32,UnityEngine.Vector2)
extern "C"  void UnityEngine_InputGenerated_ilo_setVector2S5_m1101783549 (Il2CppObject * __this /* static, unused */, int32_t ___e0, Vector2_t4282066565  ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_InputGenerated::ilo_setStringS6(System.Int32,System.String)
extern "C"  void UnityEngine_InputGenerated_ilo_setStringS6_m3751469372 (Il2CppObject * __this /* static, unused */, int32_t ___e0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_InputGenerated::ilo_setArrayS7(System.Int32,System.Int32,System.Boolean)
extern "C"  void UnityEngine_InputGenerated_ilo_setArrayS7_m1753359451 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___count1, bool ___bClear2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_InputGenerated::ilo_setInt328(System.Int32,System.Int32)
extern "C"  void UnityEngine_InputGenerated_ilo_setInt328_m975595073 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_InputGenerated::ilo_moveSaveID2Arr9(System.Int32)
extern "C"  void UnityEngine_InputGenerated_ilo_moveSaveID2Arr9_m2675198347 (Il2CppObject * __this /* static, unused */, int32_t ___arrIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_InputGenerated::ilo_setEnum10(System.Int32,System.Int32)
extern "C"  void UnityEngine_InputGenerated_ilo_setEnum10_m3472294155 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine_InputGenerated::ilo_getStringS11(System.Int32)
extern "C"  String_t* UnityEngine_InputGenerated_ilo_getStringS11_m65692265 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_InputGenerated::ilo_getEnum12(System.Int32)
extern "C"  int32_t UnityEngine_InputGenerated_ilo_getEnum12_m4062454516 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_InputGenerated::ilo_getInt3213(System.Int32)
extern "C"  int32_t UnityEngine_InputGenerated_ilo_getInt3213_m638912322 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
