﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_MeshRendererGenerated
struct UnityEngine_MeshRendererGenerated_t1559440695;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"

// System.Void UnityEngine_MeshRendererGenerated::.ctor()
extern "C"  void UnityEngine_MeshRendererGenerated__ctor_m2350095892 (UnityEngine_MeshRendererGenerated_t1559440695 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MeshRendererGenerated::MeshRenderer_MeshRenderer1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MeshRendererGenerated_MeshRenderer_MeshRenderer1_m3716958558 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_MeshRendererGenerated::MeshRenderer_additionalVertexStreams(JSVCall)
extern "C"  void UnityEngine_MeshRendererGenerated_MeshRenderer_additionalVertexStreams_m487205566 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_MeshRendererGenerated::__Register()
extern "C"  void UnityEngine_MeshRendererGenerated___Register_m759270195 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_MeshRendererGenerated::ilo_getObject1(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_MeshRendererGenerated_ilo_getObject1_m2426638481 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
