﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Float_hero_shengji
struct Float_hero_shengji_t1963723590;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// FloatTextUnit
struct FloatTextUnit_t2362298029;
// EventDelegate/Callback
struct Callback_t1094463061;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_FLOAT_TEXT_ID2199345770.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "AssemblyU2DCSharp_FloatTextUnit2362298029.h"
#include "AssemblyU2DCSharp_EventDelegate_Callback1094463061.h"
#include "AssemblyU2DCSharp_UITweener_Method1078127180.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void Float_hero_shengji::.ctor()
extern "C"  void Float_hero_shengji__ctor_m1149977621 (Float_hero_shengji_t1963723590 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FLOAT_TEXT_ID Float_hero_shengji::FloatID()
extern "C"  int32_t Float_hero_shengji_FloatID_m1964871777 (Float_hero_shengji_t1963723590 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_hero_shengji::OnAwake(UnityEngine.GameObject)
extern "C"  void Float_hero_shengji_OnAwake_m1443330257 (Float_hero_shengji_t1963723590 * __this, GameObject_t3674682005 * ___viewGO0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_hero_shengji::OnDestroy()
extern "C"  void Float_hero_shengji_OnDestroy_m1447233166 (Float_hero_shengji_t1963723590 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_hero_shengji::Init()
extern "C"  void Float_hero_shengji_Init_m1928295327 (Float_hero_shengji_t1963723590 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_hero_shengji::SetFile(System.Object[])
extern "C"  void Float_hero_shengji_SetFile_m2677631329 (Float_hero_shengji_t1963723590 * __this, ObjectU5BU5D_t1108656482* ___args0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_hero_shengji::ilo_OnDestroy1(FloatTextUnit)
extern "C"  void Float_hero_shengji_ilo_OnDestroy1_m219191277 (Il2CppObject * __this /* static, unused */, FloatTextUnit_t2362298029 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_hero_shengji::ilo_MoveAlpha2(FloatTextUnit,System.Single,System.Single,System.Single,System.Single,EventDelegate/Callback,UnityEngine.GameObject,UITweener/Method)
extern "C"  void Float_hero_shengji_ilo_MoveAlpha2_m2211180027 (Il2CppObject * __this /* static, unused */, FloatTextUnit_t2362298029 * ____this0, float ___from1, float ___to2, float ___duration3, float ___delay4, Callback_t1094463061 * ___call5, GameObject_t3674682005 * ___go6, int32_t ___method7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_hero_shengji::ilo_MovePosition3(FloatTextUnit,UnityEngine.Vector3,System.Single,System.Single,System.Single,EventDelegate/Callback,UnityEngine.GameObject,UITweener/Method)
extern "C"  void Float_hero_shengji_ilo_MovePosition3_m1402078335 (Il2CppObject * __this /* static, unused */, FloatTextUnit_t2362298029 * ____this0, Vector3_t4282066566  ___from1, float ___toUp2, float ___duration3, float ___delay4, Callback_t1094463061 * ___call5, GameObject_t3674682005 * ___go6, int32_t ___method7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
