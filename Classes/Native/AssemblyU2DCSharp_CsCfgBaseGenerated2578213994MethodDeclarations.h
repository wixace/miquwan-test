﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CsCfgBaseGenerated
struct CsCfgBaseGenerated_t2578213994;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// CsCfgBase
struct CsCfgBase_t69924517;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_CsCfgBase69924517.h"

// System.Void CsCfgBaseGenerated::.ctor()
extern "C"  void CsCfgBaseGenerated__ctor_m2182867825 (CsCfgBaseGenerated_t2578213994 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CsCfgBaseGenerated::CsCfgBase_CsCfgBase1(JSVCall,System.Int32)
extern "C"  bool CsCfgBaseGenerated_CsCfgBase_CsCfgBase1_m4073614813 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CsCfgBaseGenerated::CsCfgBase_Init__DictionaryT2_String_String(JSVCall,System.Int32)
extern "C"  bool CsCfgBaseGenerated_CsCfgBase_Init__DictionaryT2_String_String_m935094101 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CsCfgBaseGenerated::__Register()
extern "C"  void CsCfgBaseGenerated___Register_m3222540278 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CsCfgBaseGenerated::ilo_attachFinalizerObject1(System.Int32)
extern "C"  bool CsCfgBaseGenerated_ilo_attachFinalizerObject1_m2882445710 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CsCfgBaseGenerated::ilo_addJSCSRel2(System.Int32,System.Object)
extern "C"  void CsCfgBaseGenerated_ilo_addJSCSRel2_m62276142 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CsCfgBaseGenerated::ilo_Init3(CsCfgBase,System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C"  void CsCfgBaseGenerated_ilo_Init3_m964570825 (Il2CppObject * __this /* static, unused */, CsCfgBase_t69924517 * ____this0, Dictionary_2_t827649927 * ___info1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
