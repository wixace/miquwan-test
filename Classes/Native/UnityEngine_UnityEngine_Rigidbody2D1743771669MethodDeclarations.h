﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Rigidbody2D
struct Rigidbody2D_t1743771669;
// UnityEngine.Collider2D
struct Collider2D_t1552025098;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "UnityEngine_UnityEngine_Rigidbody2D1743771669.h"
#include "UnityEngine_UnityEngine_RigidbodyConstraints2D774977535.h"
#include "UnityEngine_UnityEngine_RigidbodyInterpolation2D4161840493.h"
#include "UnityEngine_UnityEngine_RigidbodySleepMode2D3661767779.h"
#include "UnityEngine_UnityEngine_CollisionDetectionMode2D2714190092.h"
#include "UnityEngine_UnityEngine_Collider2D1552025098.h"
#include "UnityEngine_UnityEngine_ForceMode2D665452726.h"

// System.Void UnityEngine.Rigidbody2D::.ctor()
extern "C"  void Rigidbody2D__ctor_m3171486938 (Rigidbody2D_t1743771669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Rigidbody2D::get_position()
extern "C"  Vector2_t4282066565  Rigidbody2D_get_position_m3766784193 (Rigidbody2D_t1743771669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rigidbody2D::set_position(UnityEngine.Vector2)
extern "C"  void Rigidbody2D_set_position_m2296903370 (Rigidbody2D_t1743771669 * __this, Vector2_t4282066565  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rigidbody2D::INTERNAL_get_position(UnityEngine.Vector2&)
extern "C"  void Rigidbody2D_INTERNAL_get_position_m80650910 (Rigidbody2D_t1743771669 * __this, Vector2_t4282066565 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rigidbody2D::INTERNAL_set_position(UnityEngine.Vector2&)
extern "C"  void Rigidbody2D_INTERNAL_set_position_m3796620306 (Rigidbody2D_t1743771669 * __this, Vector2_t4282066565 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Rigidbody2D::get_rotation()
extern "C"  float Rigidbody2D_get_rotation_m3576148997 (Rigidbody2D_t1743771669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rigidbody2D::set_rotation(System.Single)
extern "C"  void Rigidbody2D_set_rotation_m4035430854 (Rigidbody2D_t1743771669 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rigidbody2D::MovePosition(UnityEngine.Vector2)
extern "C"  void Rigidbody2D_MovePosition_m816157558 (Rigidbody2D_t1743771669 * __this, Vector2_t4282066565  ___position0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rigidbody2D::INTERNAL_CALL_MovePosition(UnityEngine.Rigidbody2D,UnityEngine.Vector2&)
extern "C"  void Rigidbody2D_INTERNAL_CALL_MovePosition_m3017526385 (Il2CppObject * __this /* static, unused */, Rigidbody2D_t1743771669 * ___self0, Vector2_t4282066565 * ___position1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rigidbody2D::MoveRotation(System.Single)
extern "C"  void Rigidbody2D_MoveRotation_m724785010 (Rigidbody2D_t1743771669 * __this, float ___angle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rigidbody2D::INTERNAL_CALL_MoveRotation(UnityEngine.Rigidbody2D,System.Single)
extern "C"  void Rigidbody2D_INTERNAL_CALL_MoveRotation_m800044407 (Il2CppObject * __this /* static, unused */, Rigidbody2D_t1743771669 * ___self0, float ___angle1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Rigidbody2D::get_velocity()
extern "C"  Vector2_t4282066565  Rigidbody2D_get_velocity_m416159605 (Rigidbody2D_t1743771669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rigidbody2D::set_velocity(UnityEngine.Vector2)
extern "C"  void Rigidbody2D_set_velocity_m100625302 (Rigidbody2D_t1743771669 * __this, Vector2_t4282066565  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rigidbody2D::INTERNAL_get_velocity(UnityEngine.Vector2&)
extern "C"  void Rigidbody2D_INTERNAL_get_velocity_m715507538 (Rigidbody2D_t1743771669 * __this, Vector2_t4282066565 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rigidbody2D::INTERNAL_set_velocity(UnityEngine.Vector2&)
extern "C"  void Rigidbody2D_INTERNAL_set_velocity_m136509638 (Rigidbody2D_t1743771669 * __this, Vector2_t4282066565 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Rigidbody2D::get_angularVelocity()
extern "C"  float Rigidbody2D_get_angularVelocity_m3714473050 (Rigidbody2D_t1743771669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rigidbody2D::set_angularVelocity(System.Single)
extern "C"  void Rigidbody2D_set_angularVelocity_m1393720209 (Rigidbody2D_t1743771669 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Rigidbody2D::get_useAutoMass()
extern "C"  bool Rigidbody2D_get_useAutoMass_m3316137215 (Rigidbody2D_t1743771669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rigidbody2D::set_useAutoMass(System.Boolean)
extern "C"  void Rigidbody2D_set_useAutoMass_m673357404 (Rigidbody2D_t1743771669 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Rigidbody2D::get_mass()
extern "C"  float Rigidbody2D_get_mass_m3688503547 (Rigidbody2D_t1743771669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rigidbody2D::set_mass(System.Single)
extern "C"  void Rigidbody2D_set_mass_m610116752 (Rigidbody2D_t1743771669 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Rigidbody2D::get_centerOfMass()
extern "C"  Vector2_t4282066565  Rigidbody2D_get_centerOfMass_m4052188280 (Rigidbody2D_t1743771669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rigidbody2D::set_centerOfMass(UnityEngine.Vector2)
extern "C"  void Rigidbody2D_set_centerOfMass_m5458227 (Rigidbody2D_t1743771669 * __this, Vector2_t4282066565  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rigidbody2D::INTERNAL_get_centerOfMass(UnityEngine.Vector2&)
extern "C"  void Rigidbody2D_INTERNAL_get_centerOfMass_m3724414805 (Rigidbody2D_t1743771669 * __this, Vector2_t4282066565 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rigidbody2D::INTERNAL_set_centerOfMass(UnityEngine.Vector2&)
extern "C"  void Rigidbody2D_INTERNAL_set_centerOfMass_m1843226313 (Rigidbody2D_t1743771669 * __this, Vector2_t4282066565 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Rigidbody2D::get_worldCenterOfMass()
extern "C"  Vector2_t4282066565  Rigidbody2D_get_worldCenterOfMass_m2477707356 (Rigidbody2D_t1743771669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rigidbody2D::INTERNAL_get_worldCenterOfMass(UnityEngine.Vector2&)
extern "C"  void Rigidbody2D_INTERNAL_get_worldCenterOfMass_m3173980419 (Rigidbody2D_t1743771669 * __this, Vector2_t4282066565 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Rigidbody2D::get_inertia()
extern "C"  float Rigidbody2D_get_inertia_m2090858869 (Rigidbody2D_t1743771669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rigidbody2D::set_inertia(System.Single)
extern "C"  void Rigidbody2D_set_inertia_m2553823830 (Rigidbody2D_t1743771669 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Rigidbody2D::get_drag()
extern "C"  float Rigidbody2D_get_drag_m3445993275 (Rigidbody2D_t1743771669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rigidbody2D::set_drag(System.Single)
extern "C"  void Rigidbody2D_set_drag_m3091740240 (Rigidbody2D_t1743771669 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Rigidbody2D::get_angularDrag()
extern "C"  float Rigidbody2D_get_angularDrag_m957403377 (Rigidbody2D_t1743771669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rigidbody2D::set_angularDrag(System.Single)
extern "C"  void Rigidbody2D_set_angularDrag_m1521513562 (Rigidbody2D_t1743771669 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Rigidbody2D::get_gravityScale()
extern "C"  float Rigidbody2D_get_gravityScale_m1347094947 (Rigidbody2D_t1743771669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rigidbody2D::set_gravityScale(System.Single)
extern "C"  void Rigidbody2D_set_gravityScale_m2024998120 (Rigidbody2D_t1743771669 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Rigidbody2D::get_isKinematic()
extern "C"  bool Rigidbody2D_get_isKinematic_m957232848 (Rigidbody2D_t1743771669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rigidbody2D::set_isKinematic(System.Boolean)
extern "C"  void Rigidbody2D_set_isKinematic_m222467693 (Rigidbody2D_t1743771669 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Rigidbody2D::get_freezeRotation()
extern "C"  bool Rigidbody2D_get_freezeRotation_m48155618 (Rigidbody2D_t1743771669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rigidbody2D::set_freezeRotation(System.Boolean)
extern "C"  void Rigidbody2D_set_freezeRotation_m3797270003 (Rigidbody2D_t1743771669 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RigidbodyConstraints2D UnityEngine.Rigidbody2D::get_constraints()
extern "C"  int32_t Rigidbody2D_get_constraints_m3440684386 (Rigidbody2D_t1743771669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rigidbody2D::set_constraints(UnityEngine.RigidbodyConstraints2D)
extern "C"  void Rigidbody2D_set_constraints_m3508094335 (Rigidbody2D_t1743771669 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Rigidbody2D::IsSleeping()
extern "C"  bool Rigidbody2D_IsSleeping_m4134977273 (Rigidbody2D_t1743771669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Rigidbody2D::IsAwake()
extern "C"  bool Rigidbody2D_IsAwake_m3225469273 (Rigidbody2D_t1743771669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rigidbody2D::Sleep()
extern "C"  void Rigidbody2D_Sleep_m1892894479 (Rigidbody2D_t1743771669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rigidbody2D::WakeUp()
extern "C"  void Rigidbody2D_WakeUp_m224894601 (Rigidbody2D_t1743771669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Rigidbody2D::get_simulated()
extern "C"  bool Rigidbody2D_get_simulated_m1958222677 (Rigidbody2D_t1743771669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rigidbody2D::set_simulated(System.Boolean)
extern "C"  void Rigidbody2D_set_simulated_m1953955762 (Rigidbody2D_t1743771669 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RigidbodyInterpolation2D UnityEngine.Rigidbody2D::get_interpolation()
extern "C"  int32_t Rigidbody2D_get_interpolation_m3806763106 (Rigidbody2D_t1743771669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rigidbody2D::set_interpolation(UnityEngine.RigidbodyInterpolation2D)
extern "C"  void Rigidbody2D_set_interpolation_m360668735 (Rigidbody2D_t1743771669 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RigidbodySleepMode2D UnityEngine.Rigidbody2D::get_sleepMode()
extern "C"  int32_t Rigidbody2D_get_sleepMode_m3719802274 (Rigidbody2D_t1743771669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rigidbody2D::set_sleepMode(UnityEngine.RigidbodySleepMode2D)
extern "C"  void Rigidbody2D_set_sleepMode_m1448458239 (Rigidbody2D_t1743771669 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.CollisionDetectionMode2D UnityEngine.Rigidbody2D::get_collisionDetectionMode()
extern "C"  int32_t Rigidbody2D_get_collisionDetectionMode_m4178159481 (Rigidbody2D_t1743771669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rigidbody2D::set_collisionDetectionMode(UnityEngine.CollisionDetectionMode2D)
extern "C"  void Rigidbody2D_set_collisionDetectionMode_m4242997470 (Rigidbody2D_t1743771669 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Rigidbody2D::IsTouching(UnityEngine.Collider2D)
extern "C"  bool Rigidbody2D_IsTouching_m1617274740 (Rigidbody2D_t1743771669 * __this, Collider2D_t1552025098 * ___collider0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Rigidbody2D::IsTouchingLayers(System.Int32)
extern "C"  bool Rigidbody2D_IsTouchingLayers_m2262791492 (Rigidbody2D_t1743771669 * __this, int32_t ___layerMask0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Rigidbody2D::IsTouchingLayers()
extern "C"  bool Rigidbody2D_IsTouchingLayers_m244569203 (Rigidbody2D_t1743771669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rigidbody2D::AddForce(UnityEngine.Vector2,UnityEngine.ForceMode2D)
extern "C"  void Rigidbody2D_AddForce_m4161385513 (Rigidbody2D_t1743771669 * __this, Vector2_t4282066565  ___force0, int32_t ___mode1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rigidbody2D::AddForce(UnityEngine.Vector2)
extern "C"  void Rigidbody2D_AddForce_m312397382 (Rigidbody2D_t1743771669 * __this, Vector2_t4282066565  ___force0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rigidbody2D::INTERNAL_CALL_AddForce(UnityEngine.Rigidbody2D,UnityEngine.Vector2&,UnityEngine.ForceMode2D)
extern "C"  void Rigidbody2D_INTERNAL_CALL_AddForce_m2763823108 (Il2CppObject * __this /* static, unused */, Rigidbody2D_t1743771669 * ___self0, Vector2_t4282066565 * ___force1, int32_t ___mode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rigidbody2D::AddRelativeForce(UnityEngine.Vector2,UnityEngine.ForceMode2D)
extern "C"  void Rigidbody2D_AddRelativeForce_m3569051669 (Rigidbody2D_t1743771669 * __this, Vector2_t4282066565  ___relativeForce0, int32_t ___mode1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rigidbody2D::AddRelativeForce(UnityEngine.Vector2)
extern "C"  void Rigidbody2D_AddRelativeForce_m952010290 (Rigidbody2D_t1743771669 * __this, Vector2_t4282066565  ___relativeForce0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rigidbody2D::INTERNAL_CALL_AddRelativeForce(UnityEngine.Rigidbody2D,UnityEngine.Vector2&,UnityEngine.ForceMode2D)
extern "C"  void Rigidbody2D_INTERNAL_CALL_AddRelativeForce_m3603347096 (Il2CppObject * __this /* static, unused */, Rigidbody2D_t1743771669 * ___self0, Vector2_t4282066565 * ___relativeForce1, int32_t ___mode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rigidbody2D::AddForceAtPosition(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.ForceMode2D)
extern "C"  void Rigidbody2D_AddForceAtPosition_m174512961 (Rigidbody2D_t1743771669 * __this, Vector2_t4282066565  ___force0, Vector2_t4282066565  ___position1, int32_t ___mode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rigidbody2D::AddForceAtPosition(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  void Rigidbody2D_AddForceAtPosition_m3734093150 (Rigidbody2D_t1743771669 * __this, Vector2_t4282066565  ___force0, Vector2_t4282066565  ___position1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rigidbody2D::INTERNAL_CALL_AddForceAtPosition(UnityEngine.Rigidbody2D,UnityEngine.Vector2&,UnityEngine.Vector2&,UnityEngine.ForceMode2D)
extern "C"  void Rigidbody2D_INTERNAL_CALL_AddForceAtPosition_m1988757918 (Il2CppObject * __this /* static, unused */, Rigidbody2D_t1743771669 * ___self0, Vector2_t4282066565 * ___force1, Vector2_t4282066565 * ___position2, int32_t ___mode3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rigidbody2D::AddTorque(System.Single,UnityEngine.ForceMode2D)
extern "C"  void Rigidbody2D_AddTorque_m3138315435 (Rigidbody2D_t1743771669 * __this, float ___torque0, int32_t ___mode1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rigidbody2D::AddTorque(System.Single)
extern "C"  void Rigidbody2D_AddTorque_m780068040 (Rigidbody2D_t1743771669 * __this, float ___torque0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Rigidbody2D::GetPoint(UnityEngine.Vector2)
extern "C"  Vector2_t4282066565  Rigidbody2D_GetPoint_m311990097 (Rigidbody2D_t1743771669 * __this, Vector2_t4282066565  ___point0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rigidbody2D::Rigidbody2D_CUSTOM_INTERNAL_GetPoint(UnityEngine.Rigidbody2D,UnityEngine.Vector2,UnityEngine.Vector2&)
extern "C"  void Rigidbody2D_Rigidbody2D_CUSTOM_INTERNAL_GetPoint_m965507570 (Il2CppObject * __this /* static, unused */, Rigidbody2D_t1743771669 * ___rigidbody0, Vector2_t4282066565  ___point1, Vector2_t4282066565 * ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rigidbody2D::INTERNAL_CALL_Rigidbody2D_CUSTOM_INTERNAL_GetPoint(UnityEngine.Rigidbody2D,UnityEngine.Vector2&,UnityEngine.Vector2&)
extern "C"  void Rigidbody2D_INTERNAL_CALL_Rigidbody2D_CUSTOM_INTERNAL_GetPoint_m2094726099 (Il2CppObject * __this /* static, unused */, Rigidbody2D_t1743771669 * ___rigidbody0, Vector2_t4282066565 * ___point1, Vector2_t4282066565 * ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Rigidbody2D::GetRelativePoint(UnityEngine.Vector2)
extern "C"  Vector2_t4282066565  Rigidbody2D_GetRelativePoint_m143441725 (Rigidbody2D_t1743771669 * __this, Vector2_t4282066565  ___relativePoint0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rigidbody2D::Rigidbody2D_CUSTOM_INTERNAL_GetRelativePoint(UnityEngine.Rigidbody2D,UnityEngine.Vector2,UnityEngine.Vector2&)
extern "C"  void Rigidbody2D_Rigidbody2D_CUSTOM_INTERNAL_GetRelativePoint_m3486130822 (Il2CppObject * __this /* static, unused */, Rigidbody2D_t1743771669 * ___rigidbody0, Vector2_t4282066565  ___relativePoint1, Vector2_t4282066565 * ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rigidbody2D::INTERNAL_CALL_Rigidbody2D_CUSTOM_INTERNAL_GetRelativePoint(UnityEngine.Rigidbody2D,UnityEngine.Vector2&,UnityEngine.Vector2&)
extern "C"  void Rigidbody2D_INTERNAL_CALL_Rigidbody2D_CUSTOM_INTERNAL_GetRelativePoint_m2355868863 (Il2CppObject * __this /* static, unused */, Rigidbody2D_t1743771669 * ___rigidbody0, Vector2_t4282066565 * ___relativePoint1, Vector2_t4282066565 * ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Rigidbody2D::GetVector(UnityEngine.Vector2)
extern "C"  Vector2_t4282066565  Rigidbody2D_GetVector_m1217773294 (Rigidbody2D_t1743771669 * __this, Vector2_t4282066565  ___vector0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rigidbody2D::Rigidbody2D_CUSTOM_INTERNAL_GetVector(UnityEngine.Rigidbody2D,UnityEngine.Vector2,UnityEngine.Vector2&)
extern "C"  void Rigidbody2D_Rigidbody2D_CUSTOM_INTERNAL_GetVector_m2817843703 (Il2CppObject * __this /* static, unused */, Rigidbody2D_t1743771669 * ___rigidbody0, Vector2_t4282066565  ___vector1, Vector2_t4282066565 * ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rigidbody2D::INTERNAL_CALL_Rigidbody2D_CUSTOM_INTERNAL_GetVector(UnityEngine.Rigidbody2D,UnityEngine.Vector2&,UnityEngine.Vector2&)
extern "C"  void Rigidbody2D_INTERNAL_CALL_Rigidbody2D_CUSTOM_INTERNAL_GetVector_m2679858764 (Il2CppObject * __this /* static, unused */, Rigidbody2D_t1743771669 * ___rigidbody0, Vector2_t4282066565 * ___vector1, Vector2_t4282066565 * ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Rigidbody2D::GetRelativeVector(UnityEngine.Vector2)
extern "C"  Vector2_t4282066565  Rigidbody2D_GetRelativeVector_m287741058 (Rigidbody2D_t1743771669 * __this, Vector2_t4282066565  ___relativeVector0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rigidbody2D::Rigidbody2D_CUSTOM_INTERNAL_GetRelativeVector(UnityEngine.Rigidbody2D,UnityEngine.Vector2,UnityEngine.Vector2&)
extern "C"  void Rigidbody2D_Rigidbody2D_CUSTOM_INTERNAL_GetRelativeVector_m3647753187 (Il2CppObject * __this /* static, unused */, Rigidbody2D_t1743771669 * ___rigidbody0, Vector2_t4282066565  ___relativeVector1, Vector2_t4282066565 * ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rigidbody2D::INTERNAL_CALL_Rigidbody2D_CUSTOM_INTERNAL_GetRelativeVector(UnityEngine.Rigidbody2D,UnityEngine.Vector2&,UnityEngine.Vector2&)
extern "C"  void Rigidbody2D_INTERNAL_CALL_Rigidbody2D_CUSTOM_INTERNAL_GetRelativeVector_m2185349856 (Il2CppObject * __this /* static, unused */, Rigidbody2D_t1743771669 * ___rigidbody0, Vector2_t4282066565 * ___relativeVector1, Vector2_t4282066565 * ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Rigidbody2D::GetPointVelocity(UnityEngine.Vector2)
extern "C"  Vector2_t4282066565  Rigidbody2D_GetPointVelocity_m3610750580 (Rigidbody2D_t1743771669 * __this, Vector2_t4282066565  ___point0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rigidbody2D::Rigidbody2D_CUSTOM_INTERNAL_GetPointVelocity(UnityEngine.Rigidbody2D,UnityEngine.Vector2,UnityEngine.Vector2&)
extern "C"  void Rigidbody2D_Rigidbody2D_CUSTOM_INTERNAL_GetPointVelocity_m1128726703 (Il2CppObject * __this /* static, unused */, Rigidbody2D_t1743771669 * ___rigidbody0, Vector2_t4282066565  ___point1, Vector2_t4282066565 * ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rigidbody2D::INTERNAL_CALL_Rigidbody2D_CUSTOM_INTERNAL_GetPointVelocity(UnityEngine.Rigidbody2D,UnityEngine.Vector2&,UnityEngine.Vector2&)
extern "C"  void Rigidbody2D_INTERNAL_CALL_Rigidbody2D_CUSTOM_INTERNAL_GetPointVelocity_m2290785206 (Il2CppObject * __this /* static, unused */, Rigidbody2D_t1743771669 * ___rigidbody0, Vector2_t4282066565 * ___point1, Vector2_t4282066565 * ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Rigidbody2D::GetRelativePointVelocity(UnityEngine.Vector2)
extern "C"  Vector2_t4282066565  Rigidbody2D_GetRelativePointVelocity_m3039504992 (Rigidbody2D_t1743771669 * __this, Vector2_t4282066565  ___relativePoint0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rigidbody2D::Rigidbody2D_CUSTOM_INTERNAL_GetRelativePointVelocity(UnityEngine.Rigidbody2D,UnityEngine.Vector2,UnityEngine.Vector2&)
extern "C"  void Rigidbody2D_Rigidbody2D_CUSTOM_INTERNAL_GetRelativePointVelocity_m3974944067 (Il2CppObject * __this /* static, unused */, Rigidbody2D_t1743771669 * ___rigidbody0, Vector2_t4282066565  ___relativePoint1, Vector2_t4282066565 * ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rigidbody2D::INTERNAL_CALL_Rigidbody2D_CUSTOM_INTERNAL_GetRelativePointVelocity(UnityEngine.Rigidbody2D,UnityEngine.Vector2&,UnityEngine.Vector2&)
extern "C"  void Rigidbody2D_INTERNAL_CALL_Rigidbody2D_CUSTOM_INTERNAL_GetRelativePointVelocity_m3875121314 (Il2CppObject * __this /* static, unused */, Rigidbody2D_t1743771669 * ___rigidbody0, Vector2_t4282066565 * ___relativePoint1, Vector2_t4282066565 * ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
