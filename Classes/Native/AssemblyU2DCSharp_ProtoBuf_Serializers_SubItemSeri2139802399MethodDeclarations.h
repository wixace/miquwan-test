﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ProtoBuf.Serializers.SubItemSerializer
struct SubItemSerializer_t2139802399;
// System.Type
struct Type_t;
// ProtoBuf.Serializers.ISerializerProxy
struct ISerializerProxy_t1336599533;
// System.Object
struct Il2CppObject;
// ProtoBuf.SerializationContext
struct SerializationContext_t3997850667;
// ProtoBuf.ProtoReader
struct ProtoReader_t3962509489;
// ProtoBuf.ProtoWriter
struct ProtoWriter_t4117914721;
// ProtoBuf.Serializers.IProtoSerializer
struct IProtoSerializer_t3033312651;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Type2863145774.h"
#include "AssemblyU2DCSharp_ProtoBuf_Meta_TypeModel_Callback2866957669.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_ProtoBuf_SerializationContext3997850667.h"
#include "AssemblyU2DCSharp_ProtoBuf_ProtoReader3962509489.h"
#include "AssemblyU2DCSharp_ProtoBuf_ProtoWriter4117914721.h"

// System.Void ProtoBuf.Serializers.SubItemSerializer::.ctor(System.Type,System.Int32,ProtoBuf.Serializers.ISerializerProxy,System.Boolean)
extern "C"  void SubItemSerializer__ctor_m4216718864 (SubItemSerializer_t2139802399 * __this, Type_t * ___type0, int32_t ___key1, Il2CppObject * ___proxy2, bool ___recursionCheck3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Serializers.SubItemSerializer::ProtoBuf.Serializers.IProtoTypeSerializer.HasCallbacks(ProtoBuf.Meta.TypeModel/CallbackType)
extern "C"  bool SubItemSerializer_ProtoBuf_Serializers_IProtoTypeSerializer_HasCallbacks_m3700757460 (SubItemSerializer_t2139802399 * __this, int32_t ___callbackType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Serializers.SubItemSerializer::ProtoBuf.Serializers.IProtoTypeSerializer.CanCreateInstance()
extern "C"  bool SubItemSerializer_ProtoBuf_Serializers_IProtoTypeSerializer_CanCreateInstance_m2324864926 (SubItemSerializer_t2139802399 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Serializers.SubItemSerializer::ProtoBuf.Serializers.IProtoTypeSerializer.Callback(System.Object,ProtoBuf.Meta.TypeModel/CallbackType,ProtoBuf.SerializationContext)
extern "C"  void SubItemSerializer_ProtoBuf_Serializers_IProtoTypeSerializer_Callback_m3005223149 (SubItemSerializer_t2139802399 * __this, Il2CppObject * ___value0, int32_t ___callbackType1, SerializationContext_t3997850667 * ___context2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProtoBuf.Serializers.SubItemSerializer::ProtoBuf.Serializers.IProtoTypeSerializer.CreateInstance(ProtoBuf.ProtoReader)
extern "C"  Il2CppObject * SubItemSerializer_ProtoBuf_Serializers_IProtoTypeSerializer_CreateInstance_m3019329455 (SubItemSerializer_t2139802399 * __this, ProtoReader_t3962509489 * ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type ProtoBuf.Serializers.SubItemSerializer::ProtoBuf.Serializers.IProtoSerializer.get_ExpectedType()
extern "C"  Type_t * SubItemSerializer_ProtoBuf_Serializers_IProtoSerializer_get_ExpectedType_m2936394848 (SubItemSerializer_t2139802399 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Serializers.SubItemSerializer::ProtoBuf.Serializers.IProtoSerializer.get_RequiresOldValue()
extern "C"  bool SubItemSerializer_ProtoBuf_Serializers_IProtoSerializer_get_RequiresOldValue_m1062295104 (SubItemSerializer_t2139802399 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Serializers.SubItemSerializer::ProtoBuf.Serializers.IProtoSerializer.get_ReturnsValue()
extern "C"  bool SubItemSerializer_ProtoBuf_Serializers_IProtoSerializer_get_ReturnsValue_m3551684822 (SubItemSerializer_t2139802399 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Serializers.SubItemSerializer::ProtoBuf.Serializers.IProtoSerializer.Write(System.Object,ProtoBuf.ProtoWriter)
extern "C"  void SubItemSerializer_ProtoBuf_Serializers_IProtoSerializer_Write_m2447788720 (SubItemSerializer_t2139802399 * __this, Il2CppObject * ___value0, ProtoWriter_t4117914721 * ___dest1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProtoBuf.Serializers.SubItemSerializer::ProtoBuf.Serializers.IProtoSerializer.Read(System.Object,ProtoBuf.ProtoReader)
extern "C"  Il2CppObject * SubItemSerializer_ProtoBuf_Serializers_IProtoSerializer_Read_m2265120416 (SubItemSerializer_t2139802399 * __this, Il2CppObject * ___value0, ProtoReader_t3962509489 * ___source1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.Serializers.IProtoSerializer ProtoBuf.Serializers.SubItemSerializer::ilo_get_Serializer1(ProtoBuf.Serializers.ISerializerProxy)
extern "C"  Il2CppObject * SubItemSerializer_ilo_get_Serializer1_m3518307578 (Il2CppObject * __this /* static, unused */, Il2CppObject * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProtoBuf.Serializers.SubItemSerializer::ilo_ReadObject2(System.Object,System.Int32,ProtoBuf.ProtoReader)
extern "C"  Il2CppObject * SubItemSerializer_ilo_ReadObject2_m594409314 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, int32_t ___key1, ProtoReader_t3962509489 * ___reader2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
