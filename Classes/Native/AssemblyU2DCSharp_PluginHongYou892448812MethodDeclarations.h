﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PluginHongYou
struct PluginHongYou_t892448812;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// CEvent.EventFunc`1<CEvent.ZEvent>
struct EventFunc_1_t1114914310;
// VersionMgr
struct VersionMgr_t1322950208;
// PluginsSdkMgr
struct PluginsSdkMgr_t3884624670;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_PluginHongYou892448812.h"
#include "AssemblyU2DCSharp_PluginsSdkMgr3884624670.h"

// System.Void PluginHongYou::.ctor()
extern "C"  void PluginHongYou__ctor_m1930306239 (PluginHongYou_t892448812 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginHongYou::Init()
extern "C"  void PluginHongYou_Init_m983635893 (PluginHongYou_t892448812 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginHongYou::OnDestory()
extern "C"  void PluginHongYou_OnDestory_m3042547410 (PluginHongYou_t892448812 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginHongYou::Clear()
extern "C"  void PluginHongYou_Clear_m3631406826 (PluginHongYou_t892448812 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginHongYou::RoleEnterGame(CEvent.ZEvent)
extern "C"  void PluginHongYou_RoleEnterGame_m422948394 (PluginHongYou_t892448812 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginHongYou::UserPay(CEvent.ZEvent)
extern "C"  void PluginHongYou_UserPay_m304003137 (PluginHongYou_t892448812 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginHongYou::GetChannelIdSuccess(System.String)
extern "C"  void PluginHongYou_GetChannelIdSuccess_m2455771594 (PluginHongYou_t892448812 * __this, String_t* ___channelId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginHongYou::OnLoginHgSuccess(System.String)
extern "C"  void PluginHongYou_OnLoginHgSuccess_m4239365859 (PluginHongYou_t892448812 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginHongYou::OnPayHgSuccess(System.String)
extern "C"  void PluginHongYou_OnPayHgSuccess_m3317724770 (PluginHongYou_t892448812 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginHongYou::OnPayHgFail(System.String)
extern "C"  void PluginHongYou_OnPayHgFail_m2160944671 (PluginHongYou_t892448812 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginHongYou::OnExitGameSuccess(System.String)
extern "C"  void PluginHongYou_OnExitGameSuccess_m2470311409 (PluginHongYou_t892448812 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginHongYou::OnExitGameFail(System.String)
extern "C"  void PluginHongYou_OnExitGameFail_m1450158320 (PluginHongYou_t892448812 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginHongYou::OnLogoutSuccess(System.String)
extern "C"  void PluginHongYou_OnLogoutSuccess_m514600587 (PluginHongYou_t892448812 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginHongYou::OnLogoutHgSuccess(System.String)
extern "C"  void PluginHongYou_OnLogoutHgSuccess_m1891397418 (PluginHongYou_t892448812 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginHongYou::OnLogoutFail(System.String)
extern "C"  void PluginHongYou_OnLogoutFail_m3170616982 (PluginHongYou_t892448812 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginHongYou::ReqSDKHttpLogin()
extern "C"  Dictionary_2_t827649927 * PluginHongYou_ReqSDKHttpLogin_m4030421488 (PluginHongYou_t892448812 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginHongYou::IsLoginSuccess(System.Boolean)
extern "C"  bool PluginHongYou_IsLoginSuccess_m3275123404 (PluginHongYou_t892448812 * __this, bool ___isOpenUseLogin0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginHongYou::Hg_Login()
extern "C"  void PluginHongYou_Hg_Login_m2013911662 (PluginHongYou_t892448812 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginHongYou::Hg_Play(System.Int32,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.Int32,System.String,System.String)
extern "C"  void PluginHongYou_Hg_Play_m4024797225 (PluginHongYou_t892448812 * __this, int32_t ___price0, String_t* ___roleId1, String_t* ___roleName2, String_t* ___roleLevel3, String_t* ___serverId4, String_t* ___serverName5, String_t* ___productId6, String_t* ___productName7, int32_t ___buyNum8, String_t* ___exts9, String_t* ___exts110, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginHongYou::Hg_EnterGame(System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginHongYou_Hg_EnterGame_m2682470819 (PluginHongYou_t892448812 * __this, String_t* ___roleId0, String_t* ___roleName1, String_t* ___roleLevel2, String_t* ___serverId3, String_t* ___serverName4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginHongYou::Hg_GetChannelId()
extern "C"  void PluginHongYou_Hg_GetChannelId_m3305014533 (PluginHongYou_t892448812 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginHongYou::Hg_Logout()
extern "C"  void PluginHongYou_Hg_Logout_m2307542087 (PluginHongYou_t892448812 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginHongYou::Hg_Exit()
extern "C"  void PluginHongYou_Hg_Exit_m4167907803 (PluginHongYou_t892448812 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginHongYou::<OnLogoutHgSuccess>m__428()
extern "C"  void PluginHongYou_U3COnLogoutHgSuccessU3Em__428_m3697140297 (PluginHongYou_t892448812 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginHongYou::ilo_AddEventListener1(System.String,CEvent.EventFunc`1<CEvent.ZEvent>)
extern "C"  void PluginHongYou_ilo_AddEventListener1_m1265332821 (Il2CppObject * __this /* static, unused */, String_t* ___eventType0, EventFunc_1_t1114914310 * ___fun1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginHongYou::ilo_RemoveEventListener2(System.String,CEvent.EventFunc`1<CEvent.ZEvent>)
extern "C"  void PluginHongYou_ilo_RemoveEventListener2_m2816929443 (Il2CppObject * __this /* static, unused */, String_t* ___eventType0, EventFunc_1_t1114914310 * ___func1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginHongYou::ilo_Hg_Play3(PluginHongYou,System.Int32,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.Int32,System.String,System.String)
extern "C"  void PluginHongYou_ilo_Hg_Play3_m1281709155 (Il2CppObject * __this /* static, unused */, PluginHongYou_t892448812 * ____this0, int32_t ___price1, String_t* ___roleId2, String_t* ___roleName3, String_t* ___roleLevel4, String_t* ___serverId5, String_t* ___serverName6, String_t* ___productId7, String_t* ___productName8, int32_t ___buyNum9, String_t* ___exts10, String_t* ___exts111, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionMgr PluginHongYou::ilo_get_Instance4()
extern "C"  VersionMgr_t1322950208 * PluginHongYou_ilo_get_Instance4_m680498269 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginHongYou::ilo_DispatchEvent5(CEvent.ZEvent)
extern "C"  void PluginHongYou_ilo_DispatchEvent5_m3659713806 (Il2CppObject * __this /* static, unused */, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PluginHongYou::ilo_get_DeviceID6(PluginsSdkMgr)
extern "C"  String_t* PluginHongYou_ilo_get_DeviceID6_m3780456587 (Il2CppObject * __this /* static, unused */, PluginsSdkMgr_t3884624670 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
