﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_EventSystems_ISubmitHandlerGenerated
struct UnityEngine_EventSystems_ISubmitHandlerGenerated_t4074012057;
// JSVCall
struct JSVCall_t3708497963;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"

// System.Void UnityEngine_EventSystems_ISubmitHandlerGenerated::.ctor()
extern "C"  void UnityEngine_EventSystems_ISubmitHandlerGenerated__ctor_m2321777954 (UnityEngine_EventSystems_ISubmitHandlerGenerated_t4074012057 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_EventSystems_ISubmitHandlerGenerated::ISubmitHandler_OnSubmit__BaseEventData(JSVCall,System.Int32)
extern "C"  bool UnityEngine_EventSystems_ISubmitHandlerGenerated_ISubmitHandler_OnSubmit__BaseEventData_m843802124 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_ISubmitHandlerGenerated::__Register()
extern "C"  void UnityEngine_EventSystems_ISubmitHandlerGenerated___Register_m263052517 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
