﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._171615cd86d6f8acbbd196fc3294e5ea
struct _171615cd86d6f8acbbd196fc3294e5ea_t1926136730;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._171615cd86d6f8acbbd196fc3294e5ea::.ctor()
extern "C"  void _171615cd86d6f8acbbd196fc3294e5ea__ctor_m432410579 (_171615cd86d6f8acbbd196fc3294e5ea_t1926136730 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._171615cd86d6f8acbbd196fc3294e5ea::_171615cd86d6f8acbbd196fc3294e5eam2(System.Int32)
extern "C"  int32_t _171615cd86d6f8acbbd196fc3294e5ea__171615cd86d6f8acbbd196fc3294e5eam2_m601135961 (_171615cd86d6f8acbbd196fc3294e5ea_t1926136730 * __this, int32_t ____171615cd86d6f8acbbd196fc3294e5eaa0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._171615cd86d6f8acbbd196fc3294e5ea::_171615cd86d6f8acbbd196fc3294e5eam(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _171615cd86d6f8acbbd196fc3294e5ea__171615cd86d6f8acbbd196fc3294e5eam_m2424996733 (_171615cd86d6f8acbbd196fc3294e5ea_t1926136730 * __this, int32_t ____171615cd86d6f8acbbd196fc3294e5eaa0, int32_t ____171615cd86d6f8acbbd196fc3294e5ea531, int32_t ____171615cd86d6f8acbbd196fc3294e5eac2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
