﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// FingerGestures/IFingerList
struct IFingerList_t1026994100;
// ScreenRaycaster
struct ScreenRaycaster_t4188861866;
// FingerClusterManager
struct FingerClusterManager_t3376029756;
// GestureRecognizerDelegate
struct GestureRecognizerDelegate_t2078703698;
// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "AssemblyU2DCSharp_DistanceUnit3471059769.h"
#include "AssemblyU2DCSharp_GestureResetMode2954327145.h"
#include "AssemblyU2DCSharp_GestureRecognizer_SelectionType3992046564.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GestureRecognizer
struct  GestureRecognizer_t3512875949  : public MonoBehaviour_t667441552
{
public:
	// System.Int32 GestureRecognizer::requiredFingerCount
	int32_t ___requiredFingerCount_3;
	// DistanceUnit GestureRecognizer::DistanceUnit
	int32_t ___DistanceUnit_4;
	// System.Int32 GestureRecognizer::MaxSimultaneousGestures
	int32_t ___MaxSimultaneousGestures_5;
	// GestureResetMode GestureRecognizer::ResetMode
	int32_t ___ResetMode_6;
	// ScreenRaycaster GestureRecognizer::Raycaster
	ScreenRaycaster_t4188861866 * ___Raycaster_7;
	// FingerClusterManager GestureRecognizer::ClusterManager
	FingerClusterManager_t3376029756 * ___ClusterManager_8;
	// GestureRecognizerDelegate GestureRecognizer::Delegate
	GestureRecognizerDelegate_t2078703698 * ___Delegate_9;
	// System.Boolean GestureRecognizer::UseSendMessage
	bool ___UseSendMessage_10;
	// System.String GestureRecognizer::EventMessageName
	String_t* ___EventMessageName_11;
	// UnityEngine.GameObject GestureRecognizer::EventMessageTarget
	GameObject_t3674682005 * ___EventMessageTarget_12;
	// GestureRecognizer/SelectionType GestureRecognizer::SendMessageToSelection
	int32_t ___SendMessageToSelection_13;
	// System.Boolean GestureRecognizer::IsExclusive
	bool ___IsExclusive_14;

public:
	inline static int32_t get_offset_of_requiredFingerCount_3() { return static_cast<int32_t>(offsetof(GestureRecognizer_t3512875949, ___requiredFingerCount_3)); }
	inline int32_t get_requiredFingerCount_3() const { return ___requiredFingerCount_3; }
	inline int32_t* get_address_of_requiredFingerCount_3() { return &___requiredFingerCount_3; }
	inline void set_requiredFingerCount_3(int32_t value)
	{
		___requiredFingerCount_3 = value;
	}

	inline static int32_t get_offset_of_DistanceUnit_4() { return static_cast<int32_t>(offsetof(GestureRecognizer_t3512875949, ___DistanceUnit_4)); }
	inline int32_t get_DistanceUnit_4() const { return ___DistanceUnit_4; }
	inline int32_t* get_address_of_DistanceUnit_4() { return &___DistanceUnit_4; }
	inline void set_DistanceUnit_4(int32_t value)
	{
		___DistanceUnit_4 = value;
	}

	inline static int32_t get_offset_of_MaxSimultaneousGestures_5() { return static_cast<int32_t>(offsetof(GestureRecognizer_t3512875949, ___MaxSimultaneousGestures_5)); }
	inline int32_t get_MaxSimultaneousGestures_5() const { return ___MaxSimultaneousGestures_5; }
	inline int32_t* get_address_of_MaxSimultaneousGestures_5() { return &___MaxSimultaneousGestures_5; }
	inline void set_MaxSimultaneousGestures_5(int32_t value)
	{
		___MaxSimultaneousGestures_5 = value;
	}

	inline static int32_t get_offset_of_ResetMode_6() { return static_cast<int32_t>(offsetof(GestureRecognizer_t3512875949, ___ResetMode_6)); }
	inline int32_t get_ResetMode_6() const { return ___ResetMode_6; }
	inline int32_t* get_address_of_ResetMode_6() { return &___ResetMode_6; }
	inline void set_ResetMode_6(int32_t value)
	{
		___ResetMode_6 = value;
	}

	inline static int32_t get_offset_of_Raycaster_7() { return static_cast<int32_t>(offsetof(GestureRecognizer_t3512875949, ___Raycaster_7)); }
	inline ScreenRaycaster_t4188861866 * get_Raycaster_7() const { return ___Raycaster_7; }
	inline ScreenRaycaster_t4188861866 ** get_address_of_Raycaster_7() { return &___Raycaster_7; }
	inline void set_Raycaster_7(ScreenRaycaster_t4188861866 * value)
	{
		___Raycaster_7 = value;
		Il2CppCodeGenWriteBarrier(&___Raycaster_7, value);
	}

	inline static int32_t get_offset_of_ClusterManager_8() { return static_cast<int32_t>(offsetof(GestureRecognizer_t3512875949, ___ClusterManager_8)); }
	inline FingerClusterManager_t3376029756 * get_ClusterManager_8() const { return ___ClusterManager_8; }
	inline FingerClusterManager_t3376029756 ** get_address_of_ClusterManager_8() { return &___ClusterManager_8; }
	inline void set_ClusterManager_8(FingerClusterManager_t3376029756 * value)
	{
		___ClusterManager_8 = value;
		Il2CppCodeGenWriteBarrier(&___ClusterManager_8, value);
	}

	inline static int32_t get_offset_of_Delegate_9() { return static_cast<int32_t>(offsetof(GestureRecognizer_t3512875949, ___Delegate_9)); }
	inline GestureRecognizerDelegate_t2078703698 * get_Delegate_9() const { return ___Delegate_9; }
	inline GestureRecognizerDelegate_t2078703698 ** get_address_of_Delegate_9() { return &___Delegate_9; }
	inline void set_Delegate_9(GestureRecognizerDelegate_t2078703698 * value)
	{
		___Delegate_9 = value;
		Il2CppCodeGenWriteBarrier(&___Delegate_9, value);
	}

	inline static int32_t get_offset_of_UseSendMessage_10() { return static_cast<int32_t>(offsetof(GestureRecognizer_t3512875949, ___UseSendMessage_10)); }
	inline bool get_UseSendMessage_10() const { return ___UseSendMessage_10; }
	inline bool* get_address_of_UseSendMessage_10() { return &___UseSendMessage_10; }
	inline void set_UseSendMessage_10(bool value)
	{
		___UseSendMessage_10 = value;
	}

	inline static int32_t get_offset_of_EventMessageName_11() { return static_cast<int32_t>(offsetof(GestureRecognizer_t3512875949, ___EventMessageName_11)); }
	inline String_t* get_EventMessageName_11() const { return ___EventMessageName_11; }
	inline String_t** get_address_of_EventMessageName_11() { return &___EventMessageName_11; }
	inline void set_EventMessageName_11(String_t* value)
	{
		___EventMessageName_11 = value;
		Il2CppCodeGenWriteBarrier(&___EventMessageName_11, value);
	}

	inline static int32_t get_offset_of_EventMessageTarget_12() { return static_cast<int32_t>(offsetof(GestureRecognizer_t3512875949, ___EventMessageTarget_12)); }
	inline GameObject_t3674682005 * get_EventMessageTarget_12() const { return ___EventMessageTarget_12; }
	inline GameObject_t3674682005 ** get_address_of_EventMessageTarget_12() { return &___EventMessageTarget_12; }
	inline void set_EventMessageTarget_12(GameObject_t3674682005 * value)
	{
		___EventMessageTarget_12 = value;
		Il2CppCodeGenWriteBarrier(&___EventMessageTarget_12, value);
	}

	inline static int32_t get_offset_of_SendMessageToSelection_13() { return static_cast<int32_t>(offsetof(GestureRecognizer_t3512875949, ___SendMessageToSelection_13)); }
	inline int32_t get_SendMessageToSelection_13() const { return ___SendMessageToSelection_13; }
	inline int32_t* get_address_of_SendMessageToSelection_13() { return &___SendMessageToSelection_13; }
	inline void set_SendMessageToSelection_13(int32_t value)
	{
		___SendMessageToSelection_13 = value;
	}

	inline static int32_t get_offset_of_IsExclusive_14() { return static_cast<int32_t>(offsetof(GestureRecognizer_t3512875949, ___IsExclusive_14)); }
	inline bool get_IsExclusive_14() const { return ___IsExclusive_14; }
	inline bool* get_address_of_IsExclusive_14() { return &___IsExclusive_14; }
	inline void set_IsExclusive_14(bool value)
	{
		___IsExclusive_14 = value;
	}
};

struct GestureRecognizer_t3512875949_StaticFields
{
public:
	// FingerGestures/IFingerList GestureRecognizer::EmptyFingerList
	Il2CppObject * ___EmptyFingerList_2;

public:
	inline static int32_t get_offset_of_EmptyFingerList_2() { return static_cast<int32_t>(offsetof(GestureRecognizer_t3512875949_StaticFields, ___EmptyFingerList_2)); }
	inline Il2CppObject * get_EmptyFingerList_2() const { return ___EmptyFingerList_2; }
	inline Il2CppObject ** get_address_of_EmptyFingerList_2() { return &___EmptyFingerList_2; }
	inline void set_EmptyFingerList_2(Il2CppObject * value)
	{
		___EmptyFingerList_2 = value;
		Il2CppCodeGenWriteBarrier(&___EmptyFingerList_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
