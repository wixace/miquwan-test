﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_fowjouporTereqal25
struct  M_fowjouporTereqal25_t119778618  : public Il2CppObject
{
public:
	// System.String GarbageiOS.M_fowjouporTereqal25::_ketea
	String_t* ____ketea_0;
	// System.UInt32 GarbageiOS.M_fowjouporTereqal25::_mikeco
	uint32_t ____mikeco_1;
	// System.Single GarbageiOS.M_fowjouporTereqal25::_raska
	float ____raska_2;
	// System.String GarbageiOS.M_fowjouporTereqal25::_rairma
	String_t* ____rairma_3;
	// System.Int32 GarbageiOS.M_fowjouporTereqal25::_mordistuQaswaw
	int32_t ____mordistuQaswaw_4;
	// System.UInt32 GarbageiOS.M_fowjouporTereqal25::_kerexejaVemji
	uint32_t ____kerexejaVemji_5;
	// System.UInt32 GarbageiOS.M_fowjouporTereqal25::_kehallCoqi
	uint32_t ____kehallCoqi_6;
	// System.String GarbageiOS.M_fowjouporTereqal25::_qorpa
	String_t* ____qorpa_7;
	// System.Boolean GarbageiOS.M_fowjouporTereqal25::_nerjeasar
	bool ____nerjeasar_8;
	// System.String GarbageiOS.M_fowjouporTereqal25::_hexowGouti
	String_t* ____hexowGouti_9;
	// System.Boolean GarbageiOS.M_fowjouporTereqal25::_mairtearDairdir
	bool ____mairtearDairdir_10;
	// System.Int32 GarbageiOS.M_fowjouporTereqal25::_nayteldaTeramem
	int32_t ____nayteldaTeramem_11;
	// System.Boolean GarbageiOS.M_fowjouporTereqal25::_gowci
	bool ____gowci_12;
	// System.UInt32 GarbageiOS.M_fowjouporTereqal25::_cifejasPosorka
	uint32_t ____cifejasPosorka_13;
	// System.Boolean GarbageiOS.M_fowjouporTereqal25::_semasna
	bool ____semasna_14;
	// System.String GarbageiOS.M_fowjouporTereqal25::_wexekar
	String_t* ____wexekar_15;
	// System.UInt32 GarbageiOS.M_fowjouporTereqal25::_chijakeaYaime
	uint32_t ____chijakeaYaime_16;

public:
	inline static int32_t get_offset_of__ketea_0() { return static_cast<int32_t>(offsetof(M_fowjouporTereqal25_t119778618, ____ketea_0)); }
	inline String_t* get__ketea_0() const { return ____ketea_0; }
	inline String_t** get_address_of__ketea_0() { return &____ketea_0; }
	inline void set__ketea_0(String_t* value)
	{
		____ketea_0 = value;
		Il2CppCodeGenWriteBarrier(&____ketea_0, value);
	}

	inline static int32_t get_offset_of__mikeco_1() { return static_cast<int32_t>(offsetof(M_fowjouporTereqal25_t119778618, ____mikeco_1)); }
	inline uint32_t get__mikeco_1() const { return ____mikeco_1; }
	inline uint32_t* get_address_of__mikeco_1() { return &____mikeco_1; }
	inline void set__mikeco_1(uint32_t value)
	{
		____mikeco_1 = value;
	}

	inline static int32_t get_offset_of__raska_2() { return static_cast<int32_t>(offsetof(M_fowjouporTereqal25_t119778618, ____raska_2)); }
	inline float get__raska_2() const { return ____raska_2; }
	inline float* get_address_of__raska_2() { return &____raska_2; }
	inline void set__raska_2(float value)
	{
		____raska_2 = value;
	}

	inline static int32_t get_offset_of__rairma_3() { return static_cast<int32_t>(offsetof(M_fowjouporTereqal25_t119778618, ____rairma_3)); }
	inline String_t* get__rairma_3() const { return ____rairma_3; }
	inline String_t** get_address_of__rairma_3() { return &____rairma_3; }
	inline void set__rairma_3(String_t* value)
	{
		____rairma_3 = value;
		Il2CppCodeGenWriteBarrier(&____rairma_3, value);
	}

	inline static int32_t get_offset_of__mordistuQaswaw_4() { return static_cast<int32_t>(offsetof(M_fowjouporTereqal25_t119778618, ____mordistuQaswaw_4)); }
	inline int32_t get__mordistuQaswaw_4() const { return ____mordistuQaswaw_4; }
	inline int32_t* get_address_of__mordistuQaswaw_4() { return &____mordistuQaswaw_4; }
	inline void set__mordistuQaswaw_4(int32_t value)
	{
		____mordistuQaswaw_4 = value;
	}

	inline static int32_t get_offset_of__kerexejaVemji_5() { return static_cast<int32_t>(offsetof(M_fowjouporTereqal25_t119778618, ____kerexejaVemji_5)); }
	inline uint32_t get__kerexejaVemji_5() const { return ____kerexejaVemji_5; }
	inline uint32_t* get_address_of__kerexejaVemji_5() { return &____kerexejaVemji_5; }
	inline void set__kerexejaVemji_5(uint32_t value)
	{
		____kerexejaVemji_5 = value;
	}

	inline static int32_t get_offset_of__kehallCoqi_6() { return static_cast<int32_t>(offsetof(M_fowjouporTereqal25_t119778618, ____kehallCoqi_6)); }
	inline uint32_t get__kehallCoqi_6() const { return ____kehallCoqi_6; }
	inline uint32_t* get_address_of__kehallCoqi_6() { return &____kehallCoqi_6; }
	inline void set__kehallCoqi_6(uint32_t value)
	{
		____kehallCoqi_6 = value;
	}

	inline static int32_t get_offset_of__qorpa_7() { return static_cast<int32_t>(offsetof(M_fowjouporTereqal25_t119778618, ____qorpa_7)); }
	inline String_t* get__qorpa_7() const { return ____qorpa_7; }
	inline String_t** get_address_of__qorpa_7() { return &____qorpa_7; }
	inline void set__qorpa_7(String_t* value)
	{
		____qorpa_7 = value;
		Il2CppCodeGenWriteBarrier(&____qorpa_7, value);
	}

	inline static int32_t get_offset_of__nerjeasar_8() { return static_cast<int32_t>(offsetof(M_fowjouporTereqal25_t119778618, ____nerjeasar_8)); }
	inline bool get__nerjeasar_8() const { return ____nerjeasar_8; }
	inline bool* get_address_of__nerjeasar_8() { return &____nerjeasar_8; }
	inline void set__nerjeasar_8(bool value)
	{
		____nerjeasar_8 = value;
	}

	inline static int32_t get_offset_of__hexowGouti_9() { return static_cast<int32_t>(offsetof(M_fowjouporTereqal25_t119778618, ____hexowGouti_9)); }
	inline String_t* get__hexowGouti_9() const { return ____hexowGouti_9; }
	inline String_t** get_address_of__hexowGouti_9() { return &____hexowGouti_9; }
	inline void set__hexowGouti_9(String_t* value)
	{
		____hexowGouti_9 = value;
		Il2CppCodeGenWriteBarrier(&____hexowGouti_9, value);
	}

	inline static int32_t get_offset_of__mairtearDairdir_10() { return static_cast<int32_t>(offsetof(M_fowjouporTereqal25_t119778618, ____mairtearDairdir_10)); }
	inline bool get__mairtearDairdir_10() const { return ____mairtearDairdir_10; }
	inline bool* get_address_of__mairtearDairdir_10() { return &____mairtearDairdir_10; }
	inline void set__mairtearDairdir_10(bool value)
	{
		____mairtearDairdir_10 = value;
	}

	inline static int32_t get_offset_of__nayteldaTeramem_11() { return static_cast<int32_t>(offsetof(M_fowjouporTereqal25_t119778618, ____nayteldaTeramem_11)); }
	inline int32_t get__nayteldaTeramem_11() const { return ____nayteldaTeramem_11; }
	inline int32_t* get_address_of__nayteldaTeramem_11() { return &____nayteldaTeramem_11; }
	inline void set__nayteldaTeramem_11(int32_t value)
	{
		____nayteldaTeramem_11 = value;
	}

	inline static int32_t get_offset_of__gowci_12() { return static_cast<int32_t>(offsetof(M_fowjouporTereqal25_t119778618, ____gowci_12)); }
	inline bool get__gowci_12() const { return ____gowci_12; }
	inline bool* get_address_of__gowci_12() { return &____gowci_12; }
	inline void set__gowci_12(bool value)
	{
		____gowci_12 = value;
	}

	inline static int32_t get_offset_of__cifejasPosorka_13() { return static_cast<int32_t>(offsetof(M_fowjouporTereqal25_t119778618, ____cifejasPosorka_13)); }
	inline uint32_t get__cifejasPosorka_13() const { return ____cifejasPosorka_13; }
	inline uint32_t* get_address_of__cifejasPosorka_13() { return &____cifejasPosorka_13; }
	inline void set__cifejasPosorka_13(uint32_t value)
	{
		____cifejasPosorka_13 = value;
	}

	inline static int32_t get_offset_of__semasna_14() { return static_cast<int32_t>(offsetof(M_fowjouporTereqal25_t119778618, ____semasna_14)); }
	inline bool get__semasna_14() const { return ____semasna_14; }
	inline bool* get_address_of__semasna_14() { return &____semasna_14; }
	inline void set__semasna_14(bool value)
	{
		____semasna_14 = value;
	}

	inline static int32_t get_offset_of__wexekar_15() { return static_cast<int32_t>(offsetof(M_fowjouporTereqal25_t119778618, ____wexekar_15)); }
	inline String_t* get__wexekar_15() const { return ____wexekar_15; }
	inline String_t** get_address_of__wexekar_15() { return &____wexekar_15; }
	inline void set__wexekar_15(String_t* value)
	{
		____wexekar_15 = value;
		Il2CppCodeGenWriteBarrier(&____wexekar_15, value);
	}

	inline static int32_t get_offset_of__chijakeaYaime_16() { return static_cast<int32_t>(offsetof(M_fowjouporTereqal25_t119778618, ____chijakeaYaime_16)); }
	inline uint32_t get__chijakeaYaime_16() const { return ____chijakeaYaime_16; }
	inline uint32_t* get_address_of__chijakeaYaime_16() { return &____chijakeaYaime_16; }
	inline void set__chijakeaYaime_16(uint32_t value)
	{
		____chijakeaYaime_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
