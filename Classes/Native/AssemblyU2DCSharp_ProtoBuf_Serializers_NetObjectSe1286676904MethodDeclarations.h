﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ProtoBuf.Serializers.NetObjectSerializer
struct NetObjectSerializer_t1286676904;
// ProtoBuf.Meta.TypeModel
struct TypeModel_t2730011105;
// System.Type
struct Type_t;
// System.Object
struct Il2CppObject;
// ProtoBuf.ProtoReader
struct ProtoReader_t3962509489;
// ProtoBuf.ProtoWriter
struct ProtoWriter_t4117914721;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ProtoBuf_Meta_TypeModel2730011105.h"
#include "mscorlib_System_Type2863145774.h"
#include "AssemblyU2DCSharp_ProtoBuf_BclHelpers_NetObjectOpt2130952596.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_ProtoBuf_ProtoReader3962509489.h"
#include "AssemblyU2DCSharp_ProtoBuf_ProtoWriter4117914721.h"

// System.Void ProtoBuf.Serializers.NetObjectSerializer::.ctor(ProtoBuf.Meta.TypeModel,System.Type,System.Int32,ProtoBuf.BclHelpers/NetObjectOptions)
extern "C"  void NetObjectSerializer__ctor_m1048379738 (NetObjectSerializer_t1286676904 * __this, TypeModel_t2730011105 * ___model0, Type_t * ___type1, int32_t ___key2, uint8_t ___options3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type ProtoBuf.Serializers.NetObjectSerializer::get_ExpectedType()
extern "C"  Type_t * NetObjectSerializer_get_ExpectedType_m1622915324 (NetObjectSerializer_t1286676904 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Serializers.NetObjectSerializer::get_ReturnsValue()
extern "C"  bool NetObjectSerializer_get_ReturnsValue_m820318834 (NetObjectSerializer_t1286676904 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Serializers.NetObjectSerializer::get_RequiresOldValue()
extern "C"  bool NetObjectSerializer_get_RequiresOldValue_m161337820 (NetObjectSerializer_t1286676904 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProtoBuf.Serializers.NetObjectSerializer::Read(System.Object,ProtoBuf.ProtoReader)
extern "C"  Il2CppObject * NetObjectSerializer_Read_m2355845052 (NetObjectSerializer_t1286676904 * __this, Il2CppObject * ___value0, ProtoReader_t3962509489 * ___source1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Serializers.NetObjectSerializer::Write(System.Object,ProtoBuf.ProtoWriter)
extern "C"  void NetObjectSerializer_Write_m3087997844 (NetObjectSerializer_t1286676904 * __this, Il2CppObject * ___value0, ProtoWriter_t4117914721 * ___dest1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProtoBuf.Serializers.NetObjectSerializer::ilo_ReadNetObject1(System.Object,ProtoBuf.ProtoReader,System.Int32,System.Type,ProtoBuf.BclHelpers/NetObjectOptions)
extern "C"  Il2CppObject * NetObjectSerializer_ilo_ReadNetObject1_m3824000812 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, ProtoReader_t3962509489 * ___source1, int32_t ___key2, Type_t * ___type3, uint8_t ___options4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
