﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_vemjewai192
struct  M_vemjewai192_t1694127012  : public Il2CppObject
{
public:
	// System.String GarbageiOS.M_vemjewai192::_trallwhisZiwir
	String_t* ____trallwhisZiwir_0;
	// System.UInt32 GarbageiOS.M_vemjewai192::_morho
	uint32_t ____morho_1;
	// System.Int32 GarbageiOS.M_vemjewai192::_cinemairKasjame
	int32_t ____cinemairKasjame_2;
	// System.Int32 GarbageiOS.M_vemjewai192::_murjizoSearti
	int32_t ____murjizoSearti_3;
	// System.UInt32 GarbageiOS.M_vemjewai192::_rewhallcher
	uint32_t ____rewhallcher_4;
	// System.Boolean GarbageiOS.M_vemjewai192::_jaze
	bool ____jaze_5;
	// System.Boolean GarbageiOS.M_vemjewai192::_steseStisci
	bool ____steseStisci_6;
	// System.Boolean GarbageiOS.M_vemjewai192::_sirne
	bool ____sirne_7;
	// System.Boolean GarbageiOS.M_vemjewai192::_mougaydrea
	bool ____mougaydrea_8;
	// System.Boolean GarbageiOS.M_vemjewai192::_jadalSewehou
	bool ____jadalSewehou_9;
	// System.Boolean GarbageiOS.M_vemjewai192::_wetechuBasaymall
	bool ____wetechuBasaymall_10;
	// System.String GarbageiOS.M_vemjewai192::_chuta
	String_t* ____chuta_11;
	// System.Single GarbageiOS.M_vemjewai192::_mewheZallba
	float ____mewheZallba_12;
	// System.String GarbageiOS.M_vemjewai192::_kixeemasJoda
	String_t* ____kixeemasJoda_13;
	// System.Single GarbageiOS.M_vemjewai192::_zarkechouCouperma
	float ____zarkechouCouperma_14;
	// System.Single GarbageiOS.M_vemjewai192::_joulerenePalltrar
	float ____joulerenePalltrar_15;
	// System.Int32 GarbageiOS.M_vemjewai192::_bojikere
	int32_t ____bojikere_16;
	// System.String GarbageiOS.M_vemjewai192::_sarwirjor
	String_t* ____sarwirjor_17;
	// System.String GarbageiOS.M_vemjewai192::_trizeme
	String_t* ____trizeme_18;
	// System.String GarbageiOS.M_vemjewai192::_dorgasNaswhewhis
	String_t* ____dorgasNaswhewhis_19;
	// System.Int32 GarbageiOS.M_vemjewai192::_towmal
	int32_t ____towmal_20;
	// System.String GarbageiOS.M_vemjewai192::_challdaboFacu
	String_t* ____challdaboFacu_21;
	// System.Int32 GarbageiOS.M_vemjewai192::_deljeetairBerwermay
	int32_t ____deljeetairBerwermay_22;
	// System.UInt32 GarbageiOS.M_vemjewai192::_sadayRerel
	uint32_t ____sadayRerel_23;
	// System.Int32 GarbageiOS.M_vemjewai192::_rasmer
	int32_t ____rasmer_24;
	// System.UInt32 GarbageiOS.M_vemjewai192::_derewemSoocha
	uint32_t ____derewemSoocha_25;
	// System.Boolean GarbageiOS.M_vemjewai192::_searzasri
	bool ____searzasri_26;
	// System.String GarbageiOS.M_vemjewai192::_penemRalljaje
	String_t* ____penemRalljaje_27;
	// System.String GarbageiOS.M_vemjewai192::_hallwarLermur
	String_t* ____hallwarLermur_28;
	// System.UInt32 GarbageiOS.M_vemjewai192::_mileforXatirmaw
	uint32_t ____mileforXatirmaw_29;
	// System.String GarbageiOS.M_vemjewai192::_yonoosiJabem
	String_t* ____yonoosiJabem_30;
	// System.String GarbageiOS.M_vemjewai192::_nige
	String_t* ____nige_31;
	// System.Boolean GarbageiOS.M_vemjewai192::_whamanee
	bool ____whamanee_32;
	// System.Boolean GarbageiOS.M_vemjewai192::_whalljur
	bool ____whalljur_33;
	// System.Int32 GarbageiOS.M_vemjewai192::_cogou
	int32_t ____cogou_34;
	// System.UInt32 GarbageiOS.M_vemjewai192::_wawserayCoballker
	uint32_t ____wawserayCoballker_35;
	// System.Boolean GarbageiOS.M_vemjewai192::_zoxallmaSaihati
	bool ____zoxallmaSaihati_36;
	// System.UInt32 GarbageiOS.M_vemjewai192::_cemnouCavisla
	uint32_t ____cemnouCavisla_37;
	// System.Single GarbageiOS.M_vemjewai192::_lemtisis
	float ____lemtisis_38;
	// System.Int32 GarbageiOS.M_vemjewai192::_qouco
	int32_t ____qouco_39;
	// System.Int32 GarbageiOS.M_vemjewai192::_coubougirTrecarjai
	int32_t ____coubougirTrecarjai_40;
	// System.Int32 GarbageiOS.M_vemjewai192::_dasnosa
	int32_t ____dasnosa_41;
	// System.UInt32 GarbageiOS.M_vemjewai192::_rowhemKumaba
	uint32_t ____rowhemKumaba_42;
	// System.String GarbageiOS.M_vemjewai192::_sallwel
	String_t* ____sallwel_43;
	// System.Int32 GarbageiOS.M_vemjewai192::_saljibeeLeabi
	int32_t ____saljibeeLeabi_44;
	// System.String GarbageiOS.M_vemjewai192::_sawdallwhoJuhear
	String_t* ____sawdallwhoJuhear_45;
	// System.String GarbageiOS.M_vemjewai192::_tajoFerkor
	String_t* ____tajoFerkor_46;
	// System.Boolean GarbageiOS.M_vemjewai192::_kimocow
	bool ____kimocow_47;

public:
	inline static int32_t get_offset_of__trallwhisZiwir_0() { return static_cast<int32_t>(offsetof(M_vemjewai192_t1694127012, ____trallwhisZiwir_0)); }
	inline String_t* get__trallwhisZiwir_0() const { return ____trallwhisZiwir_0; }
	inline String_t** get_address_of__trallwhisZiwir_0() { return &____trallwhisZiwir_0; }
	inline void set__trallwhisZiwir_0(String_t* value)
	{
		____trallwhisZiwir_0 = value;
		Il2CppCodeGenWriteBarrier(&____trallwhisZiwir_0, value);
	}

	inline static int32_t get_offset_of__morho_1() { return static_cast<int32_t>(offsetof(M_vemjewai192_t1694127012, ____morho_1)); }
	inline uint32_t get__morho_1() const { return ____morho_1; }
	inline uint32_t* get_address_of__morho_1() { return &____morho_1; }
	inline void set__morho_1(uint32_t value)
	{
		____morho_1 = value;
	}

	inline static int32_t get_offset_of__cinemairKasjame_2() { return static_cast<int32_t>(offsetof(M_vemjewai192_t1694127012, ____cinemairKasjame_2)); }
	inline int32_t get__cinemairKasjame_2() const { return ____cinemairKasjame_2; }
	inline int32_t* get_address_of__cinemairKasjame_2() { return &____cinemairKasjame_2; }
	inline void set__cinemairKasjame_2(int32_t value)
	{
		____cinemairKasjame_2 = value;
	}

	inline static int32_t get_offset_of__murjizoSearti_3() { return static_cast<int32_t>(offsetof(M_vemjewai192_t1694127012, ____murjizoSearti_3)); }
	inline int32_t get__murjizoSearti_3() const { return ____murjizoSearti_3; }
	inline int32_t* get_address_of__murjizoSearti_3() { return &____murjizoSearti_3; }
	inline void set__murjizoSearti_3(int32_t value)
	{
		____murjizoSearti_3 = value;
	}

	inline static int32_t get_offset_of__rewhallcher_4() { return static_cast<int32_t>(offsetof(M_vemjewai192_t1694127012, ____rewhallcher_4)); }
	inline uint32_t get__rewhallcher_4() const { return ____rewhallcher_4; }
	inline uint32_t* get_address_of__rewhallcher_4() { return &____rewhallcher_4; }
	inline void set__rewhallcher_4(uint32_t value)
	{
		____rewhallcher_4 = value;
	}

	inline static int32_t get_offset_of__jaze_5() { return static_cast<int32_t>(offsetof(M_vemjewai192_t1694127012, ____jaze_5)); }
	inline bool get__jaze_5() const { return ____jaze_5; }
	inline bool* get_address_of__jaze_5() { return &____jaze_5; }
	inline void set__jaze_5(bool value)
	{
		____jaze_5 = value;
	}

	inline static int32_t get_offset_of__steseStisci_6() { return static_cast<int32_t>(offsetof(M_vemjewai192_t1694127012, ____steseStisci_6)); }
	inline bool get__steseStisci_6() const { return ____steseStisci_6; }
	inline bool* get_address_of__steseStisci_6() { return &____steseStisci_6; }
	inline void set__steseStisci_6(bool value)
	{
		____steseStisci_6 = value;
	}

	inline static int32_t get_offset_of__sirne_7() { return static_cast<int32_t>(offsetof(M_vemjewai192_t1694127012, ____sirne_7)); }
	inline bool get__sirne_7() const { return ____sirne_7; }
	inline bool* get_address_of__sirne_7() { return &____sirne_7; }
	inline void set__sirne_7(bool value)
	{
		____sirne_7 = value;
	}

	inline static int32_t get_offset_of__mougaydrea_8() { return static_cast<int32_t>(offsetof(M_vemjewai192_t1694127012, ____mougaydrea_8)); }
	inline bool get__mougaydrea_8() const { return ____mougaydrea_8; }
	inline bool* get_address_of__mougaydrea_8() { return &____mougaydrea_8; }
	inline void set__mougaydrea_8(bool value)
	{
		____mougaydrea_8 = value;
	}

	inline static int32_t get_offset_of__jadalSewehou_9() { return static_cast<int32_t>(offsetof(M_vemjewai192_t1694127012, ____jadalSewehou_9)); }
	inline bool get__jadalSewehou_9() const { return ____jadalSewehou_9; }
	inline bool* get_address_of__jadalSewehou_9() { return &____jadalSewehou_9; }
	inline void set__jadalSewehou_9(bool value)
	{
		____jadalSewehou_9 = value;
	}

	inline static int32_t get_offset_of__wetechuBasaymall_10() { return static_cast<int32_t>(offsetof(M_vemjewai192_t1694127012, ____wetechuBasaymall_10)); }
	inline bool get__wetechuBasaymall_10() const { return ____wetechuBasaymall_10; }
	inline bool* get_address_of__wetechuBasaymall_10() { return &____wetechuBasaymall_10; }
	inline void set__wetechuBasaymall_10(bool value)
	{
		____wetechuBasaymall_10 = value;
	}

	inline static int32_t get_offset_of__chuta_11() { return static_cast<int32_t>(offsetof(M_vemjewai192_t1694127012, ____chuta_11)); }
	inline String_t* get__chuta_11() const { return ____chuta_11; }
	inline String_t** get_address_of__chuta_11() { return &____chuta_11; }
	inline void set__chuta_11(String_t* value)
	{
		____chuta_11 = value;
		Il2CppCodeGenWriteBarrier(&____chuta_11, value);
	}

	inline static int32_t get_offset_of__mewheZallba_12() { return static_cast<int32_t>(offsetof(M_vemjewai192_t1694127012, ____mewheZallba_12)); }
	inline float get__mewheZallba_12() const { return ____mewheZallba_12; }
	inline float* get_address_of__mewheZallba_12() { return &____mewheZallba_12; }
	inline void set__mewheZallba_12(float value)
	{
		____mewheZallba_12 = value;
	}

	inline static int32_t get_offset_of__kixeemasJoda_13() { return static_cast<int32_t>(offsetof(M_vemjewai192_t1694127012, ____kixeemasJoda_13)); }
	inline String_t* get__kixeemasJoda_13() const { return ____kixeemasJoda_13; }
	inline String_t** get_address_of__kixeemasJoda_13() { return &____kixeemasJoda_13; }
	inline void set__kixeemasJoda_13(String_t* value)
	{
		____kixeemasJoda_13 = value;
		Il2CppCodeGenWriteBarrier(&____kixeemasJoda_13, value);
	}

	inline static int32_t get_offset_of__zarkechouCouperma_14() { return static_cast<int32_t>(offsetof(M_vemjewai192_t1694127012, ____zarkechouCouperma_14)); }
	inline float get__zarkechouCouperma_14() const { return ____zarkechouCouperma_14; }
	inline float* get_address_of__zarkechouCouperma_14() { return &____zarkechouCouperma_14; }
	inline void set__zarkechouCouperma_14(float value)
	{
		____zarkechouCouperma_14 = value;
	}

	inline static int32_t get_offset_of__joulerenePalltrar_15() { return static_cast<int32_t>(offsetof(M_vemjewai192_t1694127012, ____joulerenePalltrar_15)); }
	inline float get__joulerenePalltrar_15() const { return ____joulerenePalltrar_15; }
	inline float* get_address_of__joulerenePalltrar_15() { return &____joulerenePalltrar_15; }
	inline void set__joulerenePalltrar_15(float value)
	{
		____joulerenePalltrar_15 = value;
	}

	inline static int32_t get_offset_of__bojikere_16() { return static_cast<int32_t>(offsetof(M_vemjewai192_t1694127012, ____bojikere_16)); }
	inline int32_t get__bojikere_16() const { return ____bojikere_16; }
	inline int32_t* get_address_of__bojikere_16() { return &____bojikere_16; }
	inline void set__bojikere_16(int32_t value)
	{
		____bojikere_16 = value;
	}

	inline static int32_t get_offset_of__sarwirjor_17() { return static_cast<int32_t>(offsetof(M_vemjewai192_t1694127012, ____sarwirjor_17)); }
	inline String_t* get__sarwirjor_17() const { return ____sarwirjor_17; }
	inline String_t** get_address_of__sarwirjor_17() { return &____sarwirjor_17; }
	inline void set__sarwirjor_17(String_t* value)
	{
		____sarwirjor_17 = value;
		Il2CppCodeGenWriteBarrier(&____sarwirjor_17, value);
	}

	inline static int32_t get_offset_of__trizeme_18() { return static_cast<int32_t>(offsetof(M_vemjewai192_t1694127012, ____trizeme_18)); }
	inline String_t* get__trizeme_18() const { return ____trizeme_18; }
	inline String_t** get_address_of__trizeme_18() { return &____trizeme_18; }
	inline void set__trizeme_18(String_t* value)
	{
		____trizeme_18 = value;
		Il2CppCodeGenWriteBarrier(&____trizeme_18, value);
	}

	inline static int32_t get_offset_of__dorgasNaswhewhis_19() { return static_cast<int32_t>(offsetof(M_vemjewai192_t1694127012, ____dorgasNaswhewhis_19)); }
	inline String_t* get__dorgasNaswhewhis_19() const { return ____dorgasNaswhewhis_19; }
	inline String_t** get_address_of__dorgasNaswhewhis_19() { return &____dorgasNaswhewhis_19; }
	inline void set__dorgasNaswhewhis_19(String_t* value)
	{
		____dorgasNaswhewhis_19 = value;
		Il2CppCodeGenWriteBarrier(&____dorgasNaswhewhis_19, value);
	}

	inline static int32_t get_offset_of__towmal_20() { return static_cast<int32_t>(offsetof(M_vemjewai192_t1694127012, ____towmal_20)); }
	inline int32_t get__towmal_20() const { return ____towmal_20; }
	inline int32_t* get_address_of__towmal_20() { return &____towmal_20; }
	inline void set__towmal_20(int32_t value)
	{
		____towmal_20 = value;
	}

	inline static int32_t get_offset_of__challdaboFacu_21() { return static_cast<int32_t>(offsetof(M_vemjewai192_t1694127012, ____challdaboFacu_21)); }
	inline String_t* get__challdaboFacu_21() const { return ____challdaboFacu_21; }
	inline String_t** get_address_of__challdaboFacu_21() { return &____challdaboFacu_21; }
	inline void set__challdaboFacu_21(String_t* value)
	{
		____challdaboFacu_21 = value;
		Il2CppCodeGenWriteBarrier(&____challdaboFacu_21, value);
	}

	inline static int32_t get_offset_of__deljeetairBerwermay_22() { return static_cast<int32_t>(offsetof(M_vemjewai192_t1694127012, ____deljeetairBerwermay_22)); }
	inline int32_t get__deljeetairBerwermay_22() const { return ____deljeetairBerwermay_22; }
	inline int32_t* get_address_of__deljeetairBerwermay_22() { return &____deljeetairBerwermay_22; }
	inline void set__deljeetairBerwermay_22(int32_t value)
	{
		____deljeetairBerwermay_22 = value;
	}

	inline static int32_t get_offset_of__sadayRerel_23() { return static_cast<int32_t>(offsetof(M_vemjewai192_t1694127012, ____sadayRerel_23)); }
	inline uint32_t get__sadayRerel_23() const { return ____sadayRerel_23; }
	inline uint32_t* get_address_of__sadayRerel_23() { return &____sadayRerel_23; }
	inline void set__sadayRerel_23(uint32_t value)
	{
		____sadayRerel_23 = value;
	}

	inline static int32_t get_offset_of__rasmer_24() { return static_cast<int32_t>(offsetof(M_vemjewai192_t1694127012, ____rasmer_24)); }
	inline int32_t get__rasmer_24() const { return ____rasmer_24; }
	inline int32_t* get_address_of__rasmer_24() { return &____rasmer_24; }
	inline void set__rasmer_24(int32_t value)
	{
		____rasmer_24 = value;
	}

	inline static int32_t get_offset_of__derewemSoocha_25() { return static_cast<int32_t>(offsetof(M_vemjewai192_t1694127012, ____derewemSoocha_25)); }
	inline uint32_t get__derewemSoocha_25() const { return ____derewemSoocha_25; }
	inline uint32_t* get_address_of__derewemSoocha_25() { return &____derewemSoocha_25; }
	inline void set__derewemSoocha_25(uint32_t value)
	{
		____derewemSoocha_25 = value;
	}

	inline static int32_t get_offset_of__searzasri_26() { return static_cast<int32_t>(offsetof(M_vemjewai192_t1694127012, ____searzasri_26)); }
	inline bool get__searzasri_26() const { return ____searzasri_26; }
	inline bool* get_address_of__searzasri_26() { return &____searzasri_26; }
	inline void set__searzasri_26(bool value)
	{
		____searzasri_26 = value;
	}

	inline static int32_t get_offset_of__penemRalljaje_27() { return static_cast<int32_t>(offsetof(M_vemjewai192_t1694127012, ____penemRalljaje_27)); }
	inline String_t* get__penemRalljaje_27() const { return ____penemRalljaje_27; }
	inline String_t** get_address_of__penemRalljaje_27() { return &____penemRalljaje_27; }
	inline void set__penemRalljaje_27(String_t* value)
	{
		____penemRalljaje_27 = value;
		Il2CppCodeGenWriteBarrier(&____penemRalljaje_27, value);
	}

	inline static int32_t get_offset_of__hallwarLermur_28() { return static_cast<int32_t>(offsetof(M_vemjewai192_t1694127012, ____hallwarLermur_28)); }
	inline String_t* get__hallwarLermur_28() const { return ____hallwarLermur_28; }
	inline String_t** get_address_of__hallwarLermur_28() { return &____hallwarLermur_28; }
	inline void set__hallwarLermur_28(String_t* value)
	{
		____hallwarLermur_28 = value;
		Il2CppCodeGenWriteBarrier(&____hallwarLermur_28, value);
	}

	inline static int32_t get_offset_of__mileforXatirmaw_29() { return static_cast<int32_t>(offsetof(M_vemjewai192_t1694127012, ____mileforXatirmaw_29)); }
	inline uint32_t get__mileforXatirmaw_29() const { return ____mileforXatirmaw_29; }
	inline uint32_t* get_address_of__mileforXatirmaw_29() { return &____mileforXatirmaw_29; }
	inline void set__mileforXatirmaw_29(uint32_t value)
	{
		____mileforXatirmaw_29 = value;
	}

	inline static int32_t get_offset_of__yonoosiJabem_30() { return static_cast<int32_t>(offsetof(M_vemjewai192_t1694127012, ____yonoosiJabem_30)); }
	inline String_t* get__yonoosiJabem_30() const { return ____yonoosiJabem_30; }
	inline String_t** get_address_of__yonoosiJabem_30() { return &____yonoosiJabem_30; }
	inline void set__yonoosiJabem_30(String_t* value)
	{
		____yonoosiJabem_30 = value;
		Il2CppCodeGenWriteBarrier(&____yonoosiJabem_30, value);
	}

	inline static int32_t get_offset_of__nige_31() { return static_cast<int32_t>(offsetof(M_vemjewai192_t1694127012, ____nige_31)); }
	inline String_t* get__nige_31() const { return ____nige_31; }
	inline String_t** get_address_of__nige_31() { return &____nige_31; }
	inline void set__nige_31(String_t* value)
	{
		____nige_31 = value;
		Il2CppCodeGenWriteBarrier(&____nige_31, value);
	}

	inline static int32_t get_offset_of__whamanee_32() { return static_cast<int32_t>(offsetof(M_vemjewai192_t1694127012, ____whamanee_32)); }
	inline bool get__whamanee_32() const { return ____whamanee_32; }
	inline bool* get_address_of__whamanee_32() { return &____whamanee_32; }
	inline void set__whamanee_32(bool value)
	{
		____whamanee_32 = value;
	}

	inline static int32_t get_offset_of__whalljur_33() { return static_cast<int32_t>(offsetof(M_vemjewai192_t1694127012, ____whalljur_33)); }
	inline bool get__whalljur_33() const { return ____whalljur_33; }
	inline bool* get_address_of__whalljur_33() { return &____whalljur_33; }
	inline void set__whalljur_33(bool value)
	{
		____whalljur_33 = value;
	}

	inline static int32_t get_offset_of__cogou_34() { return static_cast<int32_t>(offsetof(M_vemjewai192_t1694127012, ____cogou_34)); }
	inline int32_t get__cogou_34() const { return ____cogou_34; }
	inline int32_t* get_address_of__cogou_34() { return &____cogou_34; }
	inline void set__cogou_34(int32_t value)
	{
		____cogou_34 = value;
	}

	inline static int32_t get_offset_of__wawserayCoballker_35() { return static_cast<int32_t>(offsetof(M_vemjewai192_t1694127012, ____wawserayCoballker_35)); }
	inline uint32_t get__wawserayCoballker_35() const { return ____wawserayCoballker_35; }
	inline uint32_t* get_address_of__wawserayCoballker_35() { return &____wawserayCoballker_35; }
	inline void set__wawserayCoballker_35(uint32_t value)
	{
		____wawserayCoballker_35 = value;
	}

	inline static int32_t get_offset_of__zoxallmaSaihati_36() { return static_cast<int32_t>(offsetof(M_vemjewai192_t1694127012, ____zoxallmaSaihati_36)); }
	inline bool get__zoxallmaSaihati_36() const { return ____zoxallmaSaihati_36; }
	inline bool* get_address_of__zoxallmaSaihati_36() { return &____zoxallmaSaihati_36; }
	inline void set__zoxallmaSaihati_36(bool value)
	{
		____zoxallmaSaihati_36 = value;
	}

	inline static int32_t get_offset_of__cemnouCavisla_37() { return static_cast<int32_t>(offsetof(M_vemjewai192_t1694127012, ____cemnouCavisla_37)); }
	inline uint32_t get__cemnouCavisla_37() const { return ____cemnouCavisla_37; }
	inline uint32_t* get_address_of__cemnouCavisla_37() { return &____cemnouCavisla_37; }
	inline void set__cemnouCavisla_37(uint32_t value)
	{
		____cemnouCavisla_37 = value;
	}

	inline static int32_t get_offset_of__lemtisis_38() { return static_cast<int32_t>(offsetof(M_vemjewai192_t1694127012, ____lemtisis_38)); }
	inline float get__lemtisis_38() const { return ____lemtisis_38; }
	inline float* get_address_of__lemtisis_38() { return &____lemtisis_38; }
	inline void set__lemtisis_38(float value)
	{
		____lemtisis_38 = value;
	}

	inline static int32_t get_offset_of__qouco_39() { return static_cast<int32_t>(offsetof(M_vemjewai192_t1694127012, ____qouco_39)); }
	inline int32_t get__qouco_39() const { return ____qouco_39; }
	inline int32_t* get_address_of__qouco_39() { return &____qouco_39; }
	inline void set__qouco_39(int32_t value)
	{
		____qouco_39 = value;
	}

	inline static int32_t get_offset_of__coubougirTrecarjai_40() { return static_cast<int32_t>(offsetof(M_vemjewai192_t1694127012, ____coubougirTrecarjai_40)); }
	inline int32_t get__coubougirTrecarjai_40() const { return ____coubougirTrecarjai_40; }
	inline int32_t* get_address_of__coubougirTrecarjai_40() { return &____coubougirTrecarjai_40; }
	inline void set__coubougirTrecarjai_40(int32_t value)
	{
		____coubougirTrecarjai_40 = value;
	}

	inline static int32_t get_offset_of__dasnosa_41() { return static_cast<int32_t>(offsetof(M_vemjewai192_t1694127012, ____dasnosa_41)); }
	inline int32_t get__dasnosa_41() const { return ____dasnosa_41; }
	inline int32_t* get_address_of__dasnosa_41() { return &____dasnosa_41; }
	inline void set__dasnosa_41(int32_t value)
	{
		____dasnosa_41 = value;
	}

	inline static int32_t get_offset_of__rowhemKumaba_42() { return static_cast<int32_t>(offsetof(M_vemjewai192_t1694127012, ____rowhemKumaba_42)); }
	inline uint32_t get__rowhemKumaba_42() const { return ____rowhemKumaba_42; }
	inline uint32_t* get_address_of__rowhemKumaba_42() { return &____rowhemKumaba_42; }
	inline void set__rowhemKumaba_42(uint32_t value)
	{
		____rowhemKumaba_42 = value;
	}

	inline static int32_t get_offset_of__sallwel_43() { return static_cast<int32_t>(offsetof(M_vemjewai192_t1694127012, ____sallwel_43)); }
	inline String_t* get__sallwel_43() const { return ____sallwel_43; }
	inline String_t** get_address_of__sallwel_43() { return &____sallwel_43; }
	inline void set__sallwel_43(String_t* value)
	{
		____sallwel_43 = value;
		Il2CppCodeGenWriteBarrier(&____sallwel_43, value);
	}

	inline static int32_t get_offset_of__saljibeeLeabi_44() { return static_cast<int32_t>(offsetof(M_vemjewai192_t1694127012, ____saljibeeLeabi_44)); }
	inline int32_t get__saljibeeLeabi_44() const { return ____saljibeeLeabi_44; }
	inline int32_t* get_address_of__saljibeeLeabi_44() { return &____saljibeeLeabi_44; }
	inline void set__saljibeeLeabi_44(int32_t value)
	{
		____saljibeeLeabi_44 = value;
	}

	inline static int32_t get_offset_of__sawdallwhoJuhear_45() { return static_cast<int32_t>(offsetof(M_vemjewai192_t1694127012, ____sawdallwhoJuhear_45)); }
	inline String_t* get__sawdallwhoJuhear_45() const { return ____sawdallwhoJuhear_45; }
	inline String_t** get_address_of__sawdallwhoJuhear_45() { return &____sawdallwhoJuhear_45; }
	inline void set__sawdallwhoJuhear_45(String_t* value)
	{
		____sawdallwhoJuhear_45 = value;
		Il2CppCodeGenWriteBarrier(&____sawdallwhoJuhear_45, value);
	}

	inline static int32_t get_offset_of__tajoFerkor_46() { return static_cast<int32_t>(offsetof(M_vemjewai192_t1694127012, ____tajoFerkor_46)); }
	inline String_t* get__tajoFerkor_46() const { return ____tajoFerkor_46; }
	inline String_t** get_address_of__tajoFerkor_46() { return &____tajoFerkor_46; }
	inline void set__tajoFerkor_46(String_t* value)
	{
		____tajoFerkor_46 = value;
		Il2CppCodeGenWriteBarrier(&____tajoFerkor_46, value);
	}

	inline static int32_t get_offset_of__kimocow_47() { return static_cast<int32_t>(offsetof(M_vemjewai192_t1694127012, ____kimocow_47)); }
	inline bool get__kimocow_47() const { return ____kimocow_47; }
	inline bool* get_address_of__kimocow_47() { return &____kimocow_47; }
	inline void set__kimocow_47(bool value)
	{
		____kimocow_47 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
