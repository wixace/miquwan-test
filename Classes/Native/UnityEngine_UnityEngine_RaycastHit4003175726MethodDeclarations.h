﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Collider
struct Collider_t2939674232;
// UnityEngine.Rigidbody
struct Rigidbody_t3346577219;
// UnityEngine.Transform
struct Transform_t1659122786;
// UnityEngine.RaycastHit
struct RaycastHit_t4003175726;
struct RaycastHit_t4003175726_marshaled_pinvoke;
struct RaycastHit_t4003175726_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "UnityEngine_UnityEngine_Collider2939674232.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_RaycastHit4003175726.h"

// System.Void UnityEngine.RaycastHit::CalculateRaycastTexCoord(UnityEngine.Vector2&,UnityEngine.Collider,UnityEngine.Vector2,UnityEngine.Vector3,System.Int32,System.Int32)
extern "C"  void RaycastHit_CalculateRaycastTexCoord_m1670734366 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565 * ___output0, Collider_t2939674232 * ___col1, Vector2_t4282066565  ___uv2, Vector3_t4282066566  ___point3, int32_t ___face4, int32_t ___index5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RaycastHit::INTERNAL_CALL_CalculateRaycastTexCoord(UnityEngine.Vector2&,UnityEngine.Collider,UnityEngine.Vector2&,UnityEngine.Vector3&,System.Int32,System.Int32)
extern "C"  void RaycastHit_INTERNAL_CALL_CalculateRaycastTexCoord_m1112610553 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565 * ___output0, Collider_t2939674232 * ___col1, Vector2_t4282066565 * ___uv2, Vector3_t4282066566 * ___point3, int32_t ___face4, int32_t ___index5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.RaycastHit::get_point()
extern "C"  Vector3_t4282066566  RaycastHit_get_point_m4165497838 (RaycastHit_t4003175726 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RaycastHit::set_point(UnityEngine.Vector3)
extern "C"  void RaycastHit_set_point_m4242621861 (RaycastHit_t4003175726 * __this, Vector3_t4282066566  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.RaycastHit::get_normal()
extern "C"  Vector3_t4282066566  RaycastHit_get_normal_m1346998891 (RaycastHit_t4003175726 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RaycastHit::set_normal(UnityEngine.Vector3)
extern "C"  void RaycastHit_set_normal_m2487773780 (RaycastHit_t4003175726 * __this, Vector3_t4282066566  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.RaycastHit::get_barycentricCoordinate()
extern "C"  Vector3_t4282066566  RaycastHit_get_barycentricCoordinate_m1133032404 (RaycastHit_t4003175726 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RaycastHit::set_barycentricCoordinate(UnityEngine.Vector3)
extern "C"  void RaycastHit_set_barycentricCoordinate_m1040518143 (RaycastHit_t4003175726 * __this, Vector3_t4282066566  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.RaycastHit::get_distance()
extern "C"  float RaycastHit_get_distance_m800944203 (RaycastHit_t4003175726 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RaycastHit::set_distance(System.Single)
extern "C"  void RaycastHit_set_distance_m1076755160 (RaycastHit_t4003175726 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.RaycastHit::get_triangleIndex()
extern "C"  int32_t RaycastHit_get_triangleIndex_m2720652482 (RaycastHit_t4003175726 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.RaycastHit::get_textureCoord()
extern "C"  Vector2_t4282066565  RaycastHit_get_textureCoord_m2319334335 (RaycastHit_t4003175726 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.RaycastHit::get_textureCoord2()
extern "C"  Vector2_t4282066565  RaycastHit_get_textureCoord2_m3179897269 (RaycastHit_t4003175726 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.RaycastHit::get_lightmapCoord()
extern "C"  Vector2_t4282066565  RaycastHit_get_lightmapCoord_m3264735532 (RaycastHit_t4003175726 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider UnityEngine.RaycastHit::get_collider()
extern "C"  Collider_t2939674232 * RaycastHit_get_collider_m3116882274 (RaycastHit_t4003175726 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rigidbody UnityEngine.RaycastHit::get_rigidbody()
extern "C"  Rigidbody_t3346577219 * RaycastHit_get_rigidbody_m4137883432 (RaycastHit_t4003175726 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.RaycastHit::get_transform()
extern "C"  Transform_t1659122786 * RaycastHit_get_transform_m905149094 (RaycastHit_t4003175726 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct RaycastHit_t4003175726;
struct RaycastHit_t4003175726_marshaled_pinvoke;

extern "C" void RaycastHit_t4003175726_marshal_pinvoke(const RaycastHit_t4003175726& unmarshaled, RaycastHit_t4003175726_marshaled_pinvoke& marshaled);
extern "C" void RaycastHit_t4003175726_marshal_pinvoke_back(const RaycastHit_t4003175726_marshaled_pinvoke& marshaled, RaycastHit_t4003175726& unmarshaled);
extern "C" void RaycastHit_t4003175726_marshal_pinvoke_cleanup(RaycastHit_t4003175726_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct RaycastHit_t4003175726;
struct RaycastHit_t4003175726_marshaled_com;

extern "C" void RaycastHit_t4003175726_marshal_com(const RaycastHit_t4003175726& unmarshaled, RaycastHit_t4003175726_marshaled_com& marshaled);
extern "C" void RaycastHit_t4003175726_marshal_com_back(const RaycastHit_t4003175726_marshaled_com& marshaled, RaycastHit_t4003175726& unmarshaled);
extern "C" void RaycastHit_t4003175726_marshal_com_cleanup(RaycastHit_t4003175726_marshaled_com& marshaled);
