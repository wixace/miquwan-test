﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<System.String>
struct List_1_t1375417109;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t2522024052;
// System.Collections.Generic.List`1<ByteReadArray>
struct List_1_t2119379179;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>
struct Dictionary_2_t1151101739;
// System.Text.StringBuilder
struct StringBuilder_t243639308;
// ByteReadArray
struct ByteReadArray_t751193627;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TabData
struct  TabData_t110553279  : public Il2CppObject
{
public:
	// System.Collections.Generic.List`1<System.String> TabData::fieldNameList
	List_1_t1375417109 * ___fieldNameList_0;
	// System.Collections.Generic.List`1<System.Int32> TabData::fieldTypeList
	List_1_t2522024052 * ___fieldTypeList_1;
	// System.Collections.Generic.List`1<System.Int32> TabData::keys
	List_1_t2522024052 * ___keys_2;
	// System.Collections.Generic.List`1<ByteReadArray> TabData::infos
	List_1_t2119379179 * ___infos_3;
	// System.Int32 TabData::colNum
	int32_t ___colNum_4;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32> TabData::tabDic
	Dictionary_2_t1151101739 * ___tabDic_5;
	// ByteReadArray TabData::curRow
	ByteReadArray_t751193627 * ___curRow_7;

public:
	inline static int32_t get_offset_of_fieldNameList_0() { return static_cast<int32_t>(offsetof(TabData_t110553279, ___fieldNameList_0)); }
	inline List_1_t1375417109 * get_fieldNameList_0() const { return ___fieldNameList_0; }
	inline List_1_t1375417109 ** get_address_of_fieldNameList_0() { return &___fieldNameList_0; }
	inline void set_fieldNameList_0(List_1_t1375417109 * value)
	{
		___fieldNameList_0 = value;
		Il2CppCodeGenWriteBarrier(&___fieldNameList_0, value);
	}

	inline static int32_t get_offset_of_fieldTypeList_1() { return static_cast<int32_t>(offsetof(TabData_t110553279, ___fieldTypeList_1)); }
	inline List_1_t2522024052 * get_fieldTypeList_1() const { return ___fieldTypeList_1; }
	inline List_1_t2522024052 ** get_address_of_fieldTypeList_1() { return &___fieldTypeList_1; }
	inline void set_fieldTypeList_1(List_1_t2522024052 * value)
	{
		___fieldTypeList_1 = value;
		Il2CppCodeGenWriteBarrier(&___fieldTypeList_1, value);
	}

	inline static int32_t get_offset_of_keys_2() { return static_cast<int32_t>(offsetof(TabData_t110553279, ___keys_2)); }
	inline List_1_t2522024052 * get_keys_2() const { return ___keys_2; }
	inline List_1_t2522024052 ** get_address_of_keys_2() { return &___keys_2; }
	inline void set_keys_2(List_1_t2522024052 * value)
	{
		___keys_2 = value;
		Il2CppCodeGenWriteBarrier(&___keys_2, value);
	}

	inline static int32_t get_offset_of_infos_3() { return static_cast<int32_t>(offsetof(TabData_t110553279, ___infos_3)); }
	inline List_1_t2119379179 * get_infos_3() const { return ___infos_3; }
	inline List_1_t2119379179 ** get_address_of_infos_3() { return &___infos_3; }
	inline void set_infos_3(List_1_t2119379179 * value)
	{
		___infos_3 = value;
		Il2CppCodeGenWriteBarrier(&___infos_3, value);
	}

	inline static int32_t get_offset_of_colNum_4() { return static_cast<int32_t>(offsetof(TabData_t110553279, ___colNum_4)); }
	inline int32_t get_colNum_4() const { return ___colNum_4; }
	inline int32_t* get_address_of_colNum_4() { return &___colNum_4; }
	inline void set_colNum_4(int32_t value)
	{
		___colNum_4 = value;
	}

	inline static int32_t get_offset_of_tabDic_5() { return static_cast<int32_t>(offsetof(TabData_t110553279, ___tabDic_5)); }
	inline Dictionary_2_t1151101739 * get_tabDic_5() const { return ___tabDic_5; }
	inline Dictionary_2_t1151101739 ** get_address_of_tabDic_5() { return &___tabDic_5; }
	inline void set_tabDic_5(Dictionary_2_t1151101739 * value)
	{
		___tabDic_5 = value;
		Il2CppCodeGenWriteBarrier(&___tabDic_5, value);
	}

	inline static int32_t get_offset_of_curRow_7() { return static_cast<int32_t>(offsetof(TabData_t110553279, ___curRow_7)); }
	inline ByteReadArray_t751193627 * get_curRow_7() const { return ___curRow_7; }
	inline ByteReadArray_t751193627 ** get_address_of_curRow_7() { return &___curRow_7; }
	inline void set_curRow_7(ByteReadArray_t751193627 * value)
	{
		___curRow_7 = value;
		Il2CppCodeGenWriteBarrier(&___curRow_7, value);
	}
};

struct TabData_t110553279_StaticFields
{
public:
	// System.Text.StringBuilder TabData::sb
	StringBuilder_t243639308 * ___sb_6;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> TabData::infoDic
	Dictionary_2_t827649927 * ___infoDic_8;

public:
	inline static int32_t get_offset_of_sb_6() { return static_cast<int32_t>(offsetof(TabData_t110553279_StaticFields, ___sb_6)); }
	inline StringBuilder_t243639308 * get_sb_6() const { return ___sb_6; }
	inline StringBuilder_t243639308 ** get_address_of_sb_6() { return &___sb_6; }
	inline void set_sb_6(StringBuilder_t243639308 * value)
	{
		___sb_6 = value;
		Il2CppCodeGenWriteBarrier(&___sb_6, value);
	}

	inline static int32_t get_offset_of_infoDic_8() { return static_cast<int32_t>(offsetof(TabData_t110553279_StaticFields, ___infoDic_8)); }
	inline Dictionary_2_t827649927 * get_infoDic_8() const { return ___infoDic_8; }
	inline Dictionary_2_t827649927 ** get_address_of_infoDic_8() { return &___infoDic_8; }
	inline void set_infoDic_8(Dictionary_2_t827649927 * value)
	{
		___infoDic_8 = value;
		Il2CppCodeGenWriteBarrier(&___infoDic_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
