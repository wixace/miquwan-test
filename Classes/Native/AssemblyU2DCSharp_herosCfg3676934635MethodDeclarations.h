﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// herosCfg
struct herosCfg_t3676934635;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;

#include "codegen/il2cpp-codegen.h"

// System.Void herosCfg::.ctor()
extern "C"  void herosCfg__ctor_m2213881104 (herosCfg_t3676934635 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void herosCfg::Init(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C"  void herosCfg_Init_m2151770229 (herosCfg_t3676934635 * __this, Dictionary_2_t827649927 * ____info0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
