﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K2660824325MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.String,UnityEngine.AssetBundle>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m780888546(__this, ___host0, method) ((  void (*) (Enumerator_t3506314112 *, Dictionary_2_t2891378058 *, const MethodInfo*))Enumerator__ctor_m2661607283_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.String,UnityEngine.AssetBundle>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m181415935(__this, method) ((  Il2CppObject * (*) (Enumerator_t3506314112 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2640325710_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.String,UnityEngine.AssetBundle>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2267643283(__this, method) ((  void (*) (Enumerator_t3506314112 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1606518626_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.String,UnityEngine.AssetBundle>::Dispose()
#define Enumerator_Dispose_m2797030660(__this, method) ((  void (*) (Enumerator_t3506314112 *, const MethodInfo*))Enumerator_Dispose_m2264940757_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.String,UnityEngine.AssetBundle>::MoveNext()
#define Enumerator_MoveNext_m2052022326(__this, method) ((  bool (*) (Enumerator_t3506314112 *, const MethodInfo*))Enumerator_MoveNext_m3041849038_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.String,UnityEngine.AssetBundle>::get_Current()
#define Enumerator_get_Current_m2412711706(__this, method) ((  String_t* (*) (Enumerator_t3506314112 *, const MethodInfo*))Enumerator_get_Current_m3451690438_gshared)(__this, method)
