﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System_IDisposableGenerated
struct System_IDisposableGenerated_t4084720886;
// JSVCall
struct JSVCall_t3708497963;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"

// System.Void System_IDisposableGenerated::.ctor()
extern "C"  void System_IDisposableGenerated__ctor_m2863921973 (System_IDisposableGenerated_t4084720886 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IDisposableGenerated::IDisposable_Dispose(JSVCall,System.Int32)
extern "C"  bool System_IDisposableGenerated_IDisposable_Dispose_m188295488 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_IDisposableGenerated::__Register()
extern "C"  void System_IDisposableGenerated___Register_m497236658 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
