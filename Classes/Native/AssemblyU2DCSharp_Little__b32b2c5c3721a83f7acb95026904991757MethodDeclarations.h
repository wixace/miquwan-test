﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._b32b2c5c3721a83f7acb95026d6b9a75
struct _b32b2c5c3721a83f7acb95026d6b9a75_t904991757;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__b32b2c5c3721a83f7acb95026904991757.h"

// System.Void Little._b32b2c5c3721a83f7acb95026d6b9a75::.ctor()
extern "C"  void _b32b2c5c3721a83f7acb95026d6b9a75__ctor_m3380826304 (_b32b2c5c3721a83f7acb95026d6b9a75_t904991757 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._b32b2c5c3721a83f7acb95026d6b9a75::_b32b2c5c3721a83f7acb95026d6b9a75m2(System.Int32)
extern "C"  int32_t _b32b2c5c3721a83f7acb95026d6b9a75__b32b2c5c3721a83f7acb95026d6b9a75m2_m2028383865 (_b32b2c5c3721a83f7acb95026d6b9a75_t904991757 * __this, int32_t ____b32b2c5c3721a83f7acb95026d6b9a75a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._b32b2c5c3721a83f7acb95026d6b9a75::_b32b2c5c3721a83f7acb95026d6b9a75m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _b32b2c5c3721a83f7acb95026d6b9a75__b32b2c5c3721a83f7acb95026d6b9a75m_m269366365 (_b32b2c5c3721a83f7acb95026d6b9a75_t904991757 * __this, int32_t ____b32b2c5c3721a83f7acb95026d6b9a75a0, int32_t ____b32b2c5c3721a83f7acb95026d6b9a75231, int32_t ____b32b2c5c3721a83f7acb95026d6b9a75c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._b32b2c5c3721a83f7acb95026d6b9a75::ilo__b32b2c5c3721a83f7acb95026d6b9a75m21(Little._b32b2c5c3721a83f7acb95026d6b9a75,System.Int32)
extern "C"  int32_t _b32b2c5c3721a83f7acb95026d6b9a75_ilo__b32b2c5c3721a83f7acb95026d6b9a75m21_m1786700404 (Il2CppObject * __this /* static, unused */, _b32b2c5c3721a83f7acb95026d6b9a75_t904991757 * ____this0, int32_t ____b32b2c5c3721a83f7acb95026d6b9a75a1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
