﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Pathfinding.Path
struct Path_t1974241691;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.Util.LockFreeStack
struct  LockFreeStack_t746776019  : public Il2CppObject
{
public:
	// Pathfinding.Path Pathfinding.Util.LockFreeStack::head
	Path_t1974241691 * ___head_0;
	// System.Object Pathfinding.Util.LockFreeStack::lockObj
	Il2CppObject * ___lockObj_1;

public:
	inline static int32_t get_offset_of_head_0() { return static_cast<int32_t>(offsetof(LockFreeStack_t746776019, ___head_0)); }
	inline Path_t1974241691 * get_head_0() const { return ___head_0; }
	inline Path_t1974241691 ** get_address_of_head_0() { return &___head_0; }
	inline void set_head_0(Path_t1974241691 * value)
	{
		___head_0 = value;
		Il2CppCodeGenWriteBarrier(&___head_0, value);
	}

	inline static int32_t get_offset_of_lockObj_1() { return static_cast<int32_t>(offsetof(LockFreeStack_t746776019, ___lockObj_1)); }
	inline Il2CppObject * get_lockObj_1() const { return ___lockObj_1; }
	inline Il2CppObject ** get_address_of_lockObj_1() { return &___lockObj_1; }
	inline void set_lockObj_1(Il2CppObject * value)
	{
		___lockObj_1 = value;
		Il2CppCodeGenWriteBarrier(&___lockObj_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
