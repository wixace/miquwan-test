﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PluginSamsung/<SendHttpLogin>c__Iterator34
struct U3CSendHttpLoginU3Ec__Iterator34_t3547970329;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void PluginSamsung/<SendHttpLogin>c__Iterator34::.ctor()
extern "C"  void U3CSendHttpLoginU3Ec__Iterator34__ctor_m2772036002 (U3CSendHttpLoginU3Ec__Iterator34_t3547970329 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PluginSamsung/<SendHttpLogin>c__Iterator34::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CSendHttpLoginU3Ec__Iterator34_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m176014138 (U3CSendHttpLoginU3Ec__Iterator34_t3547970329 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PluginSamsung/<SendHttpLogin>c__Iterator34::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CSendHttpLoginU3Ec__Iterator34_System_Collections_IEnumerator_get_Current_m2835446478 (U3CSendHttpLoginU3Ec__Iterator34_t3547970329 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginSamsung/<SendHttpLogin>c__Iterator34::MoveNext()
extern "C"  bool U3CSendHttpLoginU3Ec__Iterator34_MoveNext_m1321938938 (U3CSendHttpLoginU3Ec__Iterator34_t3547970329 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginSamsung/<SendHttpLogin>c__Iterator34::Dispose()
extern "C"  void U3CSendHttpLoginU3Ec__Iterator34_Dispose_m945157471 (U3CSendHttpLoginU3Ec__Iterator34_t3547970329 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginSamsung/<SendHttpLogin>c__Iterator34::Reset()
extern "C"  void U3CSendHttpLoginU3Ec__Iterator34_Reset_m418468943 (U3CSendHttpLoginU3Ec__Iterator34_t3547970329 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
