﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SevenZip.Compression.LZ.BinTree
struct BinTree_t161206647;
// System.IO.Stream
struct Stream_t1561764144;
// System.UInt32[]
struct UInt32U5BU5D_t3230734560;
// SevenZip.Compression.LZ.InWindow
struct InWindow_t3196891427;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IO_Stream1561764144.h"
#include "AssemblyU2DCSharp_SevenZip_Compression_LZ_InWindow3196891427.h"
#include "AssemblyU2DCSharp_SevenZip_Compression_LZ_BinTree161206647.h"

// System.Void SevenZip.Compression.LZ.BinTree::.ctor()
extern "C"  void BinTree__ctor_m3018519972 (BinTree_t161206647 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.LZ.BinTree::SetType(System.Int32)
extern "C"  void BinTree_SetType_m1324365455 (BinTree_t161206647 * __this, int32_t ___numHashBytes0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.LZ.BinTree::SetStream(System.IO.Stream)
extern "C"  void BinTree_SetStream_m3887914715 (BinTree_t161206647 * __this, Stream_t1561764144 * ___stream0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.LZ.BinTree::ReleaseStream()
extern "C"  void BinTree_ReleaseStream_m646123113 (BinTree_t161206647 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.LZ.BinTree::Init()
extern "C"  void BinTree_Init_m3374044208 (BinTree_t161206647 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.LZ.BinTree::MovePos()
extern "C"  void BinTree_MovePos_m2396646021 (BinTree_t161206647 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte SevenZip.Compression.LZ.BinTree::GetIndexByte(System.Int32)
extern "C"  uint8_t BinTree_GetIndexByte_m761537065 (BinTree_t161206647 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 SevenZip.Compression.LZ.BinTree::GetMatchLen(System.Int32,System.UInt32,System.UInt32)
extern "C"  uint32_t BinTree_GetMatchLen_m1905735964 (BinTree_t161206647 * __this, int32_t ___index0, uint32_t ___distance1, uint32_t ___limit2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 SevenZip.Compression.LZ.BinTree::GetNumAvailableBytes()
extern "C"  uint32_t BinTree_GetNumAvailableBytes_m1009934775 (BinTree_t161206647 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.LZ.BinTree::Create(System.UInt32,System.UInt32,System.UInt32,System.UInt32)
extern "C"  void BinTree_Create_m2822108474 (BinTree_t161206647 * __this, uint32_t ___historySize0, uint32_t ___keepAddBufferBefore1, uint32_t ___matchMaxLen2, uint32_t ___keepAddBufferAfter3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 SevenZip.Compression.LZ.BinTree::GetMatches(System.UInt32[])
extern "C"  uint32_t BinTree_GetMatches_m2222502902 (BinTree_t161206647 * __this, UInt32U5BU5D_t3230734560* ___distances0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.LZ.BinTree::Skip(System.UInt32)
extern "C"  void BinTree_Skip_m1251384955 (BinTree_t161206647 * __this, uint32_t ___num0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.LZ.BinTree::NormalizeLinks(System.UInt32[],System.UInt32,System.UInt32)
extern "C"  void BinTree_NormalizeLinks_m4147890548 (BinTree_t161206647 * __this, UInt32U5BU5D_t3230734560* ___items0, uint32_t ___numItems1, uint32_t ___subValue2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.LZ.BinTree::Normalize()
extern "C"  void BinTree_Normalize_m1084908047 (BinTree_t161206647 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.LZ.BinTree::SetCutValue(System.UInt32)
extern "C"  void BinTree_SetCutValue_m520948551 (BinTree_t161206647 * __this, uint32_t ___cutValue0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.LZ.BinTree::ilo_ReduceOffsets1(SevenZip.Compression.LZ.InWindow,System.Int32)
extern "C"  void BinTree_ilo_ReduceOffsets1_m2864829536 (Il2CppObject * __this /* static, unused */, InWindow_t3196891427 * ____this0, int32_t ___subValue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.LZ.BinTree::ilo_Normalize2(SevenZip.Compression.LZ.BinTree)
extern "C"  void BinTree_ilo_Normalize2_m3943424531 (Il2CppObject * __this /* static, unused */, BinTree_t161206647 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte SevenZip.Compression.LZ.BinTree::ilo_GetIndexByte3(SevenZip.Compression.LZ.InWindow,System.Int32)
extern "C"  uint8_t BinTree_ilo_GetIndexByte3_m3575182016 (Il2CppObject * __this /* static, unused */, InWindow_t3196891427 * ____this0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.LZ.BinTree::ilo_MovePos4(SevenZip.Compression.LZ.BinTree)
extern "C"  void BinTree_ilo_MovePos4_m2128646663 (Il2CppObject * __this /* static, unused */, BinTree_t161206647 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.LZ.BinTree::ilo_NormalizeLinks5(SevenZip.Compression.LZ.BinTree,System.UInt32[],System.UInt32,System.UInt32)
extern "C"  void BinTree_ilo_NormalizeLinks5_m3887915501 (Il2CppObject * __this /* static, unused */, BinTree_t161206647 * ____this0, UInt32U5BU5D_t3230734560* ___items1, uint32_t ___numItems2, uint32_t ___subValue3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
