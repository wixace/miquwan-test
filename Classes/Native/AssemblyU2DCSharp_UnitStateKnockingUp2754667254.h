﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_UnitStateBase1040218558.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnitStateKnockingUp
struct  UnitStateKnockingUp_t2754667254  : public UnitStateBase_t1040218558
{
public:
	// System.Single UnitStateKnockingUp::hight
	float ___hight_5;
	// System.Single UnitStateKnockingUp::avgtime
	float ___avgtime_7;
	// System.Boolean UnitStateKnockingUp::isInitPath
	bool ___isInitPath_8;
	// System.Int32 UnitStateKnockingUp::state
	int32_t ___state_9;
	// UnityEngine.Vector3 UnitStateKnockingUp::targetPos
	Vector3_t4282066566  ___targetPos_10;
	// System.Single UnitStateKnockingUp::mTime
	float ___mTime_11;
	// System.Single UnitStateKnockingUp::maxtime
	float ___maxtime_12;

public:
	inline static int32_t get_offset_of_hight_5() { return static_cast<int32_t>(offsetof(UnitStateKnockingUp_t2754667254, ___hight_5)); }
	inline float get_hight_5() const { return ___hight_5; }
	inline float* get_address_of_hight_5() { return &___hight_5; }
	inline void set_hight_5(float value)
	{
		___hight_5 = value;
	}

	inline static int32_t get_offset_of_avgtime_7() { return static_cast<int32_t>(offsetof(UnitStateKnockingUp_t2754667254, ___avgtime_7)); }
	inline float get_avgtime_7() const { return ___avgtime_7; }
	inline float* get_address_of_avgtime_7() { return &___avgtime_7; }
	inline void set_avgtime_7(float value)
	{
		___avgtime_7 = value;
	}

	inline static int32_t get_offset_of_isInitPath_8() { return static_cast<int32_t>(offsetof(UnitStateKnockingUp_t2754667254, ___isInitPath_8)); }
	inline bool get_isInitPath_8() const { return ___isInitPath_8; }
	inline bool* get_address_of_isInitPath_8() { return &___isInitPath_8; }
	inline void set_isInitPath_8(bool value)
	{
		___isInitPath_8 = value;
	}

	inline static int32_t get_offset_of_state_9() { return static_cast<int32_t>(offsetof(UnitStateKnockingUp_t2754667254, ___state_9)); }
	inline int32_t get_state_9() const { return ___state_9; }
	inline int32_t* get_address_of_state_9() { return &___state_9; }
	inline void set_state_9(int32_t value)
	{
		___state_9 = value;
	}

	inline static int32_t get_offset_of_targetPos_10() { return static_cast<int32_t>(offsetof(UnitStateKnockingUp_t2754667254, ___targetPos_10)); }
	inline Vector3_t4282066566  get_targetPos_10() const { return ___targetPos_10; }
	inline Vector3_t4282066566 * get_address_of_targetPos_10() { return &___targetPos_10; }
	inline void set_targetPos_10(Vector3_t4282066566  value)
	{
		___targetPos_10 = value;
	}

	inline static int32_t get_offset_of_mTime_11() { return static_cast<int32_t>(offsetof(UnitStateKnockingUp_t2754667254, ___mTime_11)); }
	inline float get_mTime_11() const { return ___mTime_11; }
	inline float* get_address_of_mTime_11() { return &___mTime_11; }
	inline void set_mTime_11(float value)
	{
		___mTime_11 = value;
	}

	inline static int32_t get_offset_of_maxtime_12() { return static_cast<int32_t>(offsetof(UnitStateKnockingUp_t2754667254, ___maxtime_12)); }
	inline float get_maxtime_12() const { return ___maxtime_12; }
	inline float* get_address_of_maxtime_12() { return &___maxtime_12; }
	inline void set_maxtime_12(float value)
	{
		___maxtime_12 = value;
	}
};

struct UnitStateKnockingUp_t2754667254_StaticFields
{
public:
	// System.Single UnitStateKnockingUp::speed
	float ___speed_6;

public:
	inline static int32_t get_offset_of_speed_6() { return static_cast<int32_t>(offsetof(UnitStateKnockingUp_t2754667254_StaticFields, ___speed_6)); }
	inline float get_speed_6() const { return ___speed_6; }
	inline float* get_address_of_speed_6() { return &___speed_6; }
	inline void set_speed_6(float value)
	{
		___speed_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
