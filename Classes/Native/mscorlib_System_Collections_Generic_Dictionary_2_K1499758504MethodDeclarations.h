﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/KeyCollection<System.UInt32,System.Object>
struct KeyCollection_t1499758504;
// System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>
struct Dictionary_2_t4167966349;
// System.Collections.Generic.IEnumerator`1<System.UInt32>
struct IEnumerator_1_t1936533030;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// System.UInt32[]
struct UInt32U5BU5D_t3230734560;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Ke487935107.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.UInt32,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void KeyCollection__ctor_m4137106658_gshared (KeyCollection_t1499758504 * __this, Dictionary_2_t4167966349 * ___dictionary0, const MethodInfo* method);
#define KeyCollection__ctor_m4137106658(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t1499758504 *, Dictionary_2_t4167966349 *, const MethodInfo*))KeyCollection__ctor_m4137106658_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.UInt32,System.Object>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3142588212_gshared (KeyCollection_t1499758504 * __this, uint32_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3142588212(__this, ___item0, method) ((  void (*) (KeyCollection_t1499758504 *, uint32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3142588212_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.UInt32,System.Object>::System.Collections.Generic.ICollection<TKey>.Clear()
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m660240811_gshared (KeyCollection_t1499758504 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m660240811(__this, method) ((  void (*) (KeyCollection_t1499758504 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m660240811_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.UInt32,System.Object>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m3107174486_gshared (KeyCollection_t1499758504 * __this, uint32_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m3107174486(__this, ___item0, method) ((  bool (*) (KeyCollection_t1499758504 *, uint32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m3107174486_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.UInt32,System.Object>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2292074299_gshared (KeyCollection_t1499758504 * __this, uint32_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2292074299(__this, ___item0, method) ((  bool (*) (KeyCollection_t1499758504 *, uint32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2292074299_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.UInt32,System.Object>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m3531386983_gshared (KeyCollection_t1499758504 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m3531386983(__this, method) ((  Il2CppObject* (*) (KeyCollection_t1499758504 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m3531386983_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.UInt32,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m4032050589_gshared (KeyCollection_t1499758504 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_CopyTo_m4032050589(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t1499758504 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m4032050589_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.UInt32,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m2531852888_gshared (KeyCollection_t1499758504 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m2531852888(__this, method) ((  Il2CppObject * (*) (KeyCollection_t1499758504 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m2531852888_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.UInt32,System.Object>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m3873468983_gshared (KeyCollection_t1499758504 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m3873468983(__this, method) ((  bool (*) (KeyCollection_t1499758504 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m3873468983_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.UInt32,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m4199187369_gshared (KeyCollection_t1499758504 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m4199187369(__this, method) ((  bool (*) (KeyCollection_t1499758504 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m4199187369_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.UInt32,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m3281150229_gshared (KeyCollection_t1499758504 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m3281150229(__this, method) ((  Il2CppObject * (*) (KeyCollection_t1499758504 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m3281150229_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.UInt32,System.Object>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m2183179159_gshared (KeyCollection_t1499758504 * __this, UInt32U5BU5D_t3230734560* ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_CopyTo_m2183179159(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t1499758504 *, UInt32U5BU5D_t3230734560*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m2183179159_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.UInt32,System.Object>::GetEnumerator()
extern "C"  Enumerator_t487935107  KeyCollection_GetEnumerator_m4267000826_gshared (KeyCollection_t1499758504 * __this, const MethodInfo* method);
#define KeyCollection_GetEnumerator_m4267000826(__this, method) ((  Enumerator_t487935107  (*) (KeyCollection_t1499758504 *, const MethodInfo*))KeyCollection_GetEnumerator_m4267000826_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.UInt32,System.Object>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m168011375_gshared (KeyCollection_t1499758504 * __this, const MethodInfo* method);
#define KeyCollection_get_Count_m168011375(__this, method) ((  int32_t (*) (KeyCollection_t1499758504 *, const MethodInfo*))KeyCollection_get_Count_m168011375_gshared)(__this, method)
