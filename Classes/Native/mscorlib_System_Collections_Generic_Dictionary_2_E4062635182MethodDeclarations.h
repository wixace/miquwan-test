﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3363211663MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,ACData>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m2719760183(__this, ___dictionary0, method) ((  void (*) (Enumerator_t4062635182 *, Dictionary_2_t2745311790 *, const MethodInfo*))Enumerator__ctor_m3920831137_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,ACData>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m114371412(__this, method) ((  Il2CppObject * (*) (Enumerator_t4062635182 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3262087712_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,ACData>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3309722910(__this, method) ((  void (*) (Enumerator_t4062635182 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2959141748_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,ACData>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1258305685(__this, method) ((  DictionaryEntry_t1751606614  (*) (Enumerator_t4062635182 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2279524093_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,ACData>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3139410224(__this, method) ((  Il2CppObject * (*) (Enumerator_t4062635182 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1201448700_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,ACData>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2954619842(__this, method) ((  Il2CppObject * (*) (Enumerator_t4062635182 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m294434446_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,ACData>::MoveNext()
#define Enumerator_MoveNext_m1801729358(__this, method) ((  bool (*) (Enumerator_t4062635182 *, const MethodInfo*))Enumerator_MoveNext_m217327200_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,ACData>::get_Current()
#define Enumerator_get_Current_m512068910(__this, method) ((  KeyValuePair_2_t2644092496  (*) (Enumerator_t4062635182 *, const MethodInfo*))Enumerator_get_Current_m4240003024_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,ACData>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m1937846231(__this, method) ((  String_t* (*) (Enumerator_t4062635182 *, const MethodInfo*))Enumerator_get_CurrentKey_m3062159917_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,ACData>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m1010368215(__this, method) ((  ACData_t1924893420 * (*) (Enumerator_t4062635182 *, const MethodInfo*))Enumerator_get_CurrentValue_m592783249_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,ACData>::Reset()
#define Enumerator_Reset_m259127049(__this, method) ((  void (*) (Enumerator_t4062635182 *, const MethodInfo*))Enumerator_Reset_m3001375603_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,ACData>::VerifyState()
#define Enumerator_VerifyState_m671620178(__this, method) ((  void (*) (Enumerator_t4062635182 *, const MethodInfo*))Enumerator_VerifyState_m4290054460_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,ACData>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m3926768442(__this, method) ((  void (*) (Enumerator_t4062635182 *, const MethodInfo*))Enumerator_VerifyCurrent_m2318603684_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,ACData>::Dispose()
#define Enumerator_Dispose_m2436419993(__this, method) ((  void (*) (Enumerator_t4062635182 *, const MethodInfo*))Enumerator_Dispose_m627360643_gshared)(__this, method)
