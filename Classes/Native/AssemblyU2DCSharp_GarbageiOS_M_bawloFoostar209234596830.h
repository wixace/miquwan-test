﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_bawloFoostar209
struct  M_bawloFoostar209_t234596830  : public Il2CppObject
{
public:
	// System.Int32 GarbageiOS.M_bawloFoostar209::_kearmuTiheaga
	int32_t ____kearmuTiheaga_0;
	// System.Boolean GarbageiOS.M_bawloFoostar209::_qowyairderDouwi
	bool ____qowyairderDouwi_1;
	// System.Int32 GarbageiOS.M_bawloFoostar209::_fawlasa
	int32_t ____fawlasa_2;
	// System.Boolean GarbageiOS.M_bawloFoostar209::_ralceredere
	bool ____ralceredere_3;
	// System.Int32 GarbageiOS.M_bawloFoostar209::_latror
	int32_t ____latror_4;
	// System.Int32 GarbageiOS.M_bawloFoostar209::_powfi
	int32_t ____powfi_5;
	// System.UInt32 GarbageiOS.M_bawloFoostar209::_statou
	uint32_t ____statou_6;
	// System.UInt32 GarbageiOS.M_bawloFoostar209::_caidrasraw
	uint32_t ____caidrasraw_7;
	// System.UInt32 GarbageiOS.M_bawloFoostar209::_yelpeeqeKeajeajoo
	uint32_t ____yelpeeqeKeajeajoo_8;
	// System.String GarbageiOS.M_bawloFoostar209::_neeverBaitanee
	String_t* ____neeverBaitanee_9;
	// System.UInt32 GarbageiOS.M_bawloFoostar209::_magaiPetrousas
	uint32_t ____magaiPetrousas_10;
	// System.Single GarbageiOS.M_bawloFoostar209::_derordremWelou
	float ____derordremWelou_11;
	// System.Boolean GarbageiOS.M_bawloFoostar209::_needeejir
	bool ____needeejir_12;
	// System.Int32 GarbageiOS.M_bawloFoostar209::_vorloLallwihur
	int32_t ____vorloLallwihur_13;
	// System.Boolean GarbageiOS.M_bawloFoostar209::_cexas
	bool ____cexas_14;

public:
	inline static int32_t get_offset_of__kearmuTiheaga_0() { return static_cast<int32_t>(offsetof(M_bawloFoostar209_t234596830, ____kearmuTiheaga_0)); }
	inline int32_t get__kearmuTiheaga_0() const { return ____kearmuTiheaga_0; }
	inline int32_t* get_address_of__kearmuTiheaga_0() { return &____kearmuTiheaga_0; }
	inline void set__kearmuTiheaga_0(int32_t value)
	{
		____kearmuTiheaga_0 = value;
	}

	inline static int32_t get_offset_of__qowyairderDouwi_1() { return static_cast<int32_t>(offsetof(M_bawloFoostar209_t234596830, ____qowyairderDouwi_1)); }
	inline bool get__qowyairderDouwi_1() const { return ____qowyairderDouwi_1; }
	inline bool* get_address_of__qowyairderDouwi_1() { return &____qowyairderDouwi_1; }
	inline void set__qowyairderDouwi_1(bool value)
	{
		____qowyairderDouwi_1 = value;
	}

	inline static int32_t get_offset_of__fawlasa_2() { return static_cast<int32_t>(offsetof(M_bawloFoostar209_t234596830, ____fawlasa_2)); }
	inline int32_t get__fawlasa_2() const { return ____fawlasa_2; }
	inline int32_t* get_address_of__fawlasa_2() { return &____fawlasa_2; }
	inline void set__fawlasa_2(int32_t value)
	{
		____fawlasa_2 = value;
	}

	inline static int32_t get_offset_of__ralceredere_3() { return static_cast<int32_t>(offsetof(M_bawloFoostar209_t234596830, ____ralceredere_3)); }
	inline bool get__ralceredere_3() const { return ____ralceredere_3; }
	inline bool* get_address_of__ralceredere_3() { return &____ralceredere_3; }
	inline void set__ralceredere_3(bool value)
	{
		____ralceredere_3 = value;
	}

	inline static int32_t get_offset_of__latror_4() { return static_cast<int32_t>(offsetof(M_bawloFoostar209_t234596830, ____latror_4)); }
	inline int32_t get__latror_4() const { return ____latror_4; }
	inline int32_t* get_address_of__latror_4() { return &____latror_4; }
	inline void set__latror_4(int32_t value)
	{
		____latror_4 = value;
	}

	inline static int32_t get_offset_of__powfi_5() { return static_cast<int32_t>(offsetof(M_bawloFoostar209_t234596830, ____powfi_5)); }
	inline int32_t get__powfi_5() const { return ____powfi_5; }
	inline int32_t* get_address_of__powfi_5() { return &____powfi_5; }
	inline void set__powfi_5(int32_t value)
	{
		____powfi_5 = value;
	}

	inline static int32_t get_offset_of__statou_6() { return static_cast<int32_t>(offsetof(M_bawloFoostar209_t234596830, ____statou_6)); }
	inline uint32_t get__statou_6() const { return ____statou_6; }
	inline uint32_t* get_address_of__statou_6() { return &____statou_6; }
	inline void set__statou_6(uint32_t value)
	{
		____statou_6 = value;
	}

	inline static int32_t get_offset_of__caidrasraw_7() { return static_cast<int32_t>(offsetof(M_bawloFoostar209_t234596830, ____caidrasraw_7)); }
	inline uint32_t get__caidrasraw_7() const { return ____caidrasraw_7; }
	inline uint32_t* get_address_of__caidrasraw_7() { return &____caidrasraw_7; }
	inline void set__caidrasraw_7(uint32_t value)
	{
		____caidrasraw_7 = value;
	}

	inline static int32_t get_offset_of__yelpeeqeKeajeajoo_8() { return static_cast<int32_t>(offsetof(M_bawloFoostar209_t234596830, ____yelpeeqeKeajeajoo_8)); }
	inline uint32_t get__yelpeeqeKeajeajoo_8() const { return ____yelpeeqeKeajeajoo_8; }
	inline uint32_t* get_address_of__yelpeeqeKeajeajoo_8() { return &____yelpeeqeKeajeajoo_8; }
	inline void set__yelpeeqeKeajeajoo_8(uint32_t value)
	{
		____yelpeeqeKeajeajoo_8 = value;
	}

	inline static int32_t get_offset_of__neeverBaitanee_9() { return static_cast<int32_t>(offsetof(M_bawloFoostar209_t234596830, ____neeverBaitanee_9)); }
	inline String_t* get__neeverBaitanee_9() const { return ____neeverBaitanee_9; }
	inline String_t** get_address_of__neeverBaitanee_9() { return &____neeverBaitanee_9; }
	inline void set__neeverBaitanee_9(String_t* value)
	{
		____neeverBaitanee_9 = value;
		Il2CppCodeGenWriteBarrier(&____neeverBaitanee_9, value);
	}

	inline static int32_t get_offset_of__magaiPetrousas_10() { return static_cast<int32_t>(offsetof(M_bawloFoostar209_t234596830, ____magaiPetrousas_10)); }
	inline uint32_t get__magaiPetrousas_10() const { return ____magaiPetrousas_10; }
	inline uint32_t* get_address_of__magaiPetrousas_10() { return &____magaiPetrousas_10; }
	inline void set__magaiPetrousas_10(uint32_t value)
	{
		____magaiPetrousas_10 = value;
	}

	inline static int32_t get_offset_of__derordremWelou_11() { return static_cast<int32_t>(offsetof(M_bawloFoostar209_t234596830, ____derordremWelou_11)); }
	inline float get__derordremWelou_11() const { return ____derordremWelou_11; }
	inline float* get_address_of__derordremWelou_11() { return &____derordremWelou_11; }
	inline void set__derordremWelou_11(float value)
	{
		____derordremWelou_11 = value;
	}

	inline static int32_t get_offset_of__needeejir_12() { return static_cast<int32_t>(offsetof(M_bawloFoostar209_t234596830, ____needeejir_12)); }
	inline bool get__needeejir_12() const { return ____needeejir_12; }
	inline bool* get_address_of__needeejir_12() { return &____needeejir_12; }
	inline void set__needeejir_12(bool value)
	{
		____needeejir_12 = value;
	}

	inline static int32_t get_offset_of__vorloLallwihur_13() { return static_cast<int32_t>(offsetof(M_bawloFoostar209_t234596830, ____vorloLallwihur_13)); }
	inline int32_t get__vorloLallwihur_13() const { return ____vorloLallwihur_13; }
	inline int32_t* get_address_of__vorloLallwihur_13() { return &____vorloLallwihur_13; }
	inline void set__vorloLallwihur_13(int32_t value)
	{
		____vorloLallwihur_13 = value;
	}

	inline static int32_t get_offset_of__cexas_14() { return static_cast<int32_t>(offsetof(M_bawloFoostar209_t234596830, ____cexas_14)); }
	inline bool get__cexas_14() const { return ____cexas_14; }
	inline bool* get_address_of__cexas_14() { return &____cexas_14; }
	inline void set__cexas_14(bool value)
	{
		____cexas_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
