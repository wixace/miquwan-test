﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_waynaygo161
struct  M_waynaygo161_t3952817293  : public Il2CppObject
{
public:
	// System.Int32 GarbageiOS.M_waynaygo161::_geadea
	int32_t ____geadea_0;
	// System.Int32 GarbageiOS.M_waynaygo161::_vereray
	int32_t ____vereray_1;
	// System.Boolean GarbageiOS.M_waynaygo161::_callhouCeje
	bool ____callhouCeje_2;
	// System.String GarbageiOS.M_waynaygo161::_sasbawhayDani
	String_t* ____sasbawhayDani_3;
	// System.UInt32 GarbageiOS.M_waynaygo161::_mutray
	uint32_t ____mutray_4;
	// System.String GarbageiOS.M_waynaygo161::_mokepeeFawcay
	String_t* ____mokepeeFawcay_5;
	// System.UInt32 GarbageiOS.M_waynaygo161::_nerenem
	uint32_t ____nerenem_6;
	// System.String GarbageiOS.M_waynaygo161::_soohoMougowu
	String_t* ____soohoMougowu_7;
	// System.Int32 GarbageiOS.M_waynaygo161::_tasinou
	int32_t ____tasinou_8;
	// System.String GarbageiOS.M_waynaygo161::_herallJijapor
	String_t* ____herallJijapor_9;
	// System.String GarbageiOS.M_waynaygo161::_calbi
	String_t* ____calbi_10;
	// System.UInt32 GarbageiOS.M_waynaygo161::_sayfeaKurtrou
	uint32_t ____sayfeaKurtrou_11;
	// System.String GarbageiOS.M_waynaygo161::_berepallkaDrirniwere
	String_t* ____berepallkaDrirniwere_12;
	// System.String GarbageiOS.M_waynaygo161::_jalmay
	String_t* ____jalmay_13;
	// System.Single GarbageiOS.M_waynaygo161::_ferimirFemraw
	float ____ferimirFemraw_14;
	// System.Boolean GarbageiOS.M_waynaygo161::_wowenay
	bool ____wowenay_15;
	// System.String GarbageiOS.M_waynaygo161::_koutriliTisterqou
	String_t* ____koutriliTisterqou_16;
	// System.String GarbageiOS.M_waynaygo161::_sirhekiSaitreakis
	String_t* ____sirhekiSaitreakis_17;
	// System.Single GarbageiOS.M_waynaygo161::_pemteweVoulakal
	float ____pemteweVoulakal_18;
	// System.String GarbageiOS.M_waynaygo161::_nairdir
	String_t* ____nairdir_19;
	// System.Boolean GarbageiOS.M_waynaygo161::_silunePalcay
	bool ____silunePalcay_20;
	// System.Single GarbageiOS.M_waynaygo161::_reltaste
	float ____reltaste_21;
	// System.Boolean GarbageiOS.M_waynaygo161::_qeras
	bool ____qeras_22;
	// System.String GarbageiOS.M_waynaygo161::_howyowce
	String_t* ____howyowce_23;
	// System.Single GarbageiOS.M_waynaygo161::_lairwhemooChaskoo
	float ____lairwhemooChaskoo_24;
	// System.UInt32 GarbageiOS.M_waynaygo161::_chowgu
	uint32_t ____chowgu_25;
	// System.UInt32 GarbageiOS.M_waynaygo161::_keldreRemnur
	uint32_t ____keldreRemnur_26;
	// System.Single GarbageiOS.M_waynaygo161::_pihestuLakow
	float ____pihestuLakow_27;
	// System.Int32 GarbageiOS.M_waynaygo161::_racoumowNearmousa
	int32_t ____racoumowNearmousa_28;
	// System.String GarbageiOS.M_waynaygo161::_fownabair
	String_t* ____fownabair_29;
	// System.UInt32 GarbageiOS.M_waynaygo161::_zairdear
	uint32_t ____zairdear_30;
	// System.UInt32 GarbageiOS.M_waynaygo161::_teacaw
	uint32_t ____teacaw_31;
	// System.Boolean GarbageiOS.M_waynaygo161::_gairdairhu
	bool ____gairdairhu_32;
	// System.String GarbageiOS.M_waynaygo161::_dajineMeayostu
	String_t* ____dajineMeayostu_33;
	// System.Int32 GarbageiOS.M_waynaygo161::_yertursearHoufiste
	int32_t ____yertursearHoufiste_34;
	// System.Single GarbageiOS.M_waynaygo161::_masger
	float ____masger_35;
	// System.Single GarbageiOS.M_waynaygo161::_jallsa
	float ____jallsa_36;
	// System.Boolean GarbageiOS.M_waynaygo161::_seco
	bool ____seco_37;
	// System.Single GarbageiOS.M_waynaygo161::_rawtral
	float ____rawtral_38;
	// System.UInt32 GarbageiOS.M_waynaygo161::_dairjiskoLoucawai
	uint32_t ____dairjiskoLoucawai_39;
	// System.String GarbageiOS.M_waynaygo161::_semnow
	String_t* ____semnow_40;
	// System.Single GarbageiOS.M_waynaygo161::_rairmal
	float ____rairmal_41;

public:
	inline static int32_t get_offset_of__geadea_0() { return static_cast<int32_t>(offsetof(M_waynaygo161_t3952817293, ____geadea_0)); }
	inline int32_t get__geadea_0() const { return ____geadea_0; }
	inline int32_t* get_address_of__geadea_0() { return &____geadea_0; }
	inline void set__geadea_0(int32_t value)
	{
		____geadea_0 = value;
	}

	inline static int32_t get_offset_of__vereray_1() { return static_cast<int32_t>(offsetof(M_waynaygo161_t3952817293, ____vereray_1)); }
	inline int32_t get__vereray_1() const { return ____vereray_1; }
	inline int32_t* get_address_of__vereray_1() { return &____vereray_1; }
	inline void set__vereray_1(int32_t value)
	{
		____vereray_1 = value;
	}

	inline static int32_t get_offset_of__callhouCeje_2() { return static_cast<int32_t>(offsetof(M_waynaygo161_t3952817293, ____callhouCeje_2)); }
	inline bool get__callhouCeje_2() const { return ____callhouCeje_2; }
	inline bool* get_address_of__callhouCeje_2() { return &____callhouCeje_2; }
	inline void set__callhouCeje_2(bool value)
	{
		____callhouCeje_2 = value;
	}

	inline static int32_t get_offset_of__sasbawhayDani_3() { return static_cast<int32_t>(offsetof(M_waynaygo161_t3952817293, ____sasbawhayDani_3)); }
	inline String_t* get__sasbawhayDani_3() const { return ____sasbawhayDani_3; }
	inline String_t** get_address_of__sasbawhayDani_3() { return &____sasbawhayDani_3; }
	inline void set__sasbawhayDani_3(String_t* value)
	{
		____sasbawhayDani_3 = value;
		Il2CppCodeGenWriteBarrier(&____sasbawhayDani_3, value);
	}

	inline static int32_t get_offset_of__mutray_4() { return static_cast<int32_t>(offsetof(M_waynaygo161_t3952817293, ____mutray_4)); }
	inline uint32_t get__mutray_4() const { return ____mutray_4; }
	inline uint32_t* get_address_of__mutray_4() { return &____mutray_4; }
	inline void set__mutray_4(uint32_t value)
	{
		____mutray_4 = value;
	}

	inline static int32_t get_offset_of__mokepeeFawcay_5() { return static_cast<int32_t>(offsetof(M_waynaygo161_t3952817293, ____mokepeeFawcay_5)); }
	inline String_t* get__mokepeeFawcay_5() const { return ____mokepeeFawcay_5; }
	inline String_t** get_address_of__mokepeeFawcay_5() { return &____mokepeeFawcay_5; }
	inline void set__mokepeeFawcay_5(String_t* value)
	{
		____mokepeeFawcay_5 = value;
		Il2CppCodeGenWriteBarrier(&____mokepeeFawcay_5, value);
	}

	inline static int32_t get_offset_of__nerenem_6() { return static_cast<int32_t>(offsetof(M_waynaygo161_t3952817293, ____nerenem_6)); }
	inline uint32_t get__nerenem_6() const { return ____nerenem_6; }
	inline uint32_t* get_address_of__nerenem_6() { return &____nerenem_6; }
	inline void set__nerenem_6(uint32_t value)
	{
		____nerenem_6 = value;
	}

	inline static int32_t get_offset_of__soohoMougowu_7() { return static_cast<int32_t>(offsetof(M_waynaygo161_t3952817293, ____soohoMougowu_7)); }
	inline String_t* get__soohoMougowu_7() const { return ____soohoMougowu_7; }
	inline String_t** get_address_of__soohoMougowu_7() { return &____soohoMougowu_7; }
	inline void set__soohoMougowu_7(String_t* value)
	{
		____soohoMougowu_7 = value;
		Il2CppCodeGenWriteBarrier(&____soohoMougowu_7, value);
	}

	inline static int32_t get_offset_of__tasinou_8() { return static_cast<int32_t>(offsetof(M_waynaygo161_t3952817293, ____tasinou_8)); }
	inline int32_t get__tasinou_8() const { return ____tasinou_8; }
	inline int32_t* get_address_of__tasinou_8() { return &____tasinou_8; }
	inline void set__tasinou_8(int32_t value)
	{
		____tasinou_8 = value;
	}

	inline static int32_t get_offset_of__herallJijapor_9() { return static_cast<int32_t>(offsetof(M_waynaygo161_t3952817293, ____herallJijapor_9)); }
	inline String_t* get__herallJijapor_9() const { return ____herallJijapor_9; }
	inline String_t** get_address_of__herallJijapor_9() { return &____herallJijapor_9; }
	inline void set__herallJijapor_9(String_t* value)
	{
		____herallJijapor_9 = value;
		Il2CppCodeGenWriteBarrier(&____herallJijapor_9, value);
	}

	inline static int32_t get_offset_of__calbi_10() { return static_cast<int32_t>(offsetof(M_waynaygo161_t3952817293, ____calbi_10)); }
	inline String_t* get__calbi_10() const { return ____calbi_10; }
	inline String_t** get_address_of__calbi_10() { return &____calbi_10; }
	inline void set__calbi_10(String_t* value)
	{
		____calbi_10 = value;
		Il2CppCodeGenWriteBarrier(&____calbi_10, value);
	}

	inline static int32_t get_offset_of__sayfeaKurtrou_11() { return static_cast<int32_t>(offsetof(M_waynaygo161_t3952817293, ____sayfeaKurtrou_11)); }
	inline uint32_t get__sayfeaKurtrou_11() const { return ____sayfeaKurtrou_11; }
	inline uint32_t* get_address_of__sayfeaKurtrou_11() { return &____sayfeaKurtrou_11; }
	inline void set__sayfeaKurtrou_11(uint32_t value)
	{
		____sayfeaKurtrou_11 = value;
	}

	inline static int32_t get_offset_of__berepallkaDrirniwere_12() { return static_cast<int32_t>(offsetof(M_waynaygo161_t3952817293, ____berepallkaDrirniwere_12)); }
	inline String_t* get__berepallkaDrirniwere_12() const { return ____berepallkaDrirniwere_12; }
	inline String_t** get_address_of__berepallkaDrirniwere_12() { return &____berepallkaDrirniwere_12; }
	inline void set__berepallkaDrirniwere_12(String_t* value)
	{
		____berepallkaDrirniwere_12 = value;
		Il2CppCodeGenWriteBarrier(&____berepallkaDrirniwere_12, value);
	}

	inline static int32_t get_offset_of__jalmay_13() { return static_cast<int32_t>(offsetof(M_waynaygo161_t3952817293, ____jalmay_13)); }
	inline String_t* get__jalmay_13() const { return ____jalmay_13; }
	inline String_t** get_address_of__jalmay_13() { return &____jalmay_13; }
	inline void set__jalmay_13(String_t* value)
	{
		____jalmay_13 = value;
		Il2CppCodeGenWriteBarrier(&____jalmay_13, value);
	}

	inline static int32_t get_offset_of__ferimirFemraw_14() { return static_cast<int32_t>(offsetof(M_waynaygo161_t3952817293, ____ferimirFemraw_14)); }
	inline float get__ferimirFemraw_14() const { return ____ferimirFemraw_14; }
	inline float* get_address_of__ferimirFemraw_14() { return &____ferimirFemraw_14; }
	inline void set__ferimirFemraw_14(float value)
	{
		____ferimirFemraw_14 = value;
	}

	inline static int32_t get_offset_of__wowenay_15() { return static_cast<int32_t>(offsetof(M_waynaygo161_t3952817293, ____wowenay_15)); }
	inline bool get__wowenay_15() const { return ____wowenay_15; }
	inline bool* get_address_of__wowenay_15() { return &____wowenay_15; }
	inline void set__wowenay_15(bool value)
	{
		____wowenay_15 = value;
	}

	inline static int32_t get_offset_of__koutriliTisterqou_16() { return static_cast<int32_t>(offsetof(M_waynaygo161_t3952817293, ____koutriliTisterqou_16)); }
	inline String_t* get__koutriliTisterqou_16() const { return ____koutriliTisterqou_16; }
	inline String_t** get_address_of__koutriliTisterqou_16() { return &____koutriliTisterqou_16; }
	inline void set__koutriliTisterqou_16(String_t* value)
	{
		____koutriliTisterqou_16 = value;
		Il2CppCodeGenWriteBarrier(&____koutriliTisterqou_16, value);
	}

	inline static int32_t get_offset_of__sirhekiSaitreakis_17() { return static_cast<int32_t>(offsetof(M_waynaygo161_t3952817293, ____sirhekiSaitreakis_17)); }
	inline String_t* get__sirhekiSaitreakis_17() const { return ____sirhekiSaitreakis_17; }
	inline String_t** get_address_of__sirhekiSaitreakis_17() { return &____sirhekiSaitreakis_17; }
	inline void set__sirhekiSaitreakis_17(String_t* value)
	{
		____sirhekiSaitreakis_17 = value;
		Il2CppCodeGenWriteBarrier(&____sirhekiSaitreakis_17, value);
	}

	inline static int32_t get_offset_of__pemteweVoulakal_18() { return static_cast<int32_t>(offsetof(M_waynaygo161_t3952817293, ____pemteweVoulakal_18)); }
	inline float get__pemteweVoulakal_18() const { return ____pemteweVoulakal_18; }
	inline float* get_address_of__pemteweVoulakal_18() { return &____pemteweVoulakal_18; }
	inline void set__pemteweVoulakal_18(float value)
	{
		____pemteweVoulakal_18 = value;
	}

	inline static int32_t get_offset_of__nairdir_19() { return static_cast<int32_t>(offsetof(M_waynaygo161_t3952817293, ____nairdir_19)); }
	inline String_t* get__nairdir_19() const { return ____nairdir_19; }
	inline String_t** get_address_of__nairdir_19() { return &____nairdir_19; }
	inline void set__nairdir_19(String_t* value)
	{
		____nairdir_19 = value;
		Il2CppCodeGenWriteBarrier(&____nairdir_19, value);
	}

	inline static int32_t get_offset_of__silunePalcay_20() { return static_cast<int32_t>(offsetof(M_waynaygo161_t3952817293, ____silunePalcay_20)); }
	inline bool get__silunePalcay_20() const { return ____silunePalcay_20; }
	inline bool* get_address_of__silunePalcay_20() { return &____silunePalcay_20; }
	inline void set__silunePalcay_20(bool value)
	{
		____silunePalcay_20 = value;
	}

	inline static int32_t get_offset_of__reltaste_21() { return static_cast<int32_t>(offsetof(M_waynaygo161_t3952817293, ____reltaste_21)); }
	inline float get__reltaste_21() const { return ____reltaste_21; }
	inline float* get_address_of__reltaste_21() { return &____reltaste_21; }
	inline void set__reltaste_21(float value)
	{
		____reltaste_21 = value;
	}

	inline static int32_t get_offset_of__qeras_22() { return static_cast<int32_t>(offsetof(M_waynaygo161_t3952817293, ____qeras_22)); }
	inline bool get__qeras_22() const { return ____qeras_22; }
	inline bool* get_address_of__qeras_22() { return &____qeras_22; }
	inline void set__qeras_22(bool value)
	{
		____qeras_22 = value;
	}

	inline static int32_t get_offset_of__howyowce_23() { return static_cast<int32_t>(offsetof(M_waynaygo161_t3952817293, ____howyowce_23)); }
	inline String_t* get__howyowce_23() const { return ____howyowce_23; }
	inline String_t** get_address_of__howyowce_23() { return &____howyowce_23; }
	inline void set__howyowce_23(String_t* value)
	{
		____howyowce_23 = value;
		Il2CppCodeGenWriteBarrier(&____howyowce_23, value);
	}

	inline static int32_t get_offset_of__lairwhemooChaskoo_24() { return static_cast<int32_t>(offsetof(M_waynaygo161_t3952817293, ____lairwhemooChaskoo_24)); }
	inline float get__lairwhemooChaskoo_24() const { return ____lairwhemooChaskoo_24; }
	inline float* get_address_of__lairwhemooChaskoo_24() { return &____lairwhemooChaskoo_24; }
	inline void set__lairwhemooChaskoo_24(float value)
	{
		____lairwhemooChaskoo_24 = value;
	}

	inline static int32_t get_offset_of__chowgu_25() { return static_cast<int32_t>(offsetof(M_waynaygo161_t3952817293, ____chowgu_25)); }
	inline uint32_t get__chowgu_25() const { return ____chowgu_25; }
	inline uint32_t* get_address_of__chowgu_25() { return &____chowgu_25; }
	inline void set__chowgu_25(uint32_t value)
	{
		____chowgu_25 = value;
	}

	inline static int32_t get_offset_of__keldreRemnur_26() { return static_cast<int32_t>(offsetof(M_waynaygo161_t3952817293, ____keldreRemnur_26)); }
	inline uint32_t get__keldreRemnur_26() const { return ____keldreRemnur_26; }
	inline uint32_t* get_address_of__keldreRemnur_26() { return &____keldreRemnur_26; }
	inline void set__keldreRemnur_26(uint32_t value)
	{
		____keldreRemnur_26 = value;
	}

	inline static int32_t get_offset_of__pihestuLakow_27() { return static_cast<int32_t>(offsetof(M_waynaygo161_t3952817293, ____pihestuLakow_27)); }
	inline float get__pihestuLakow_27() const { return ____pihestuLakow_27; }
	inline float* get_address_of__pihestuLakow_27() { return &____pihestuLakow_27; }
	inline void set__pihestuLakow_27(float value)
	{
		____pihestuLakow_27 = value;
	}

	inline static int32_t get_offset_of__racoumowNearmousa_28() { return static_cast<int32_t>(offsetof(M_waynaygo161_t3952817293, ____racoumowNearmousa_28)); }
	inline int32_t get__racoumowNearmousa_28() const { return ____racoumowNearmousa_28; }
	inline int32_t* get_address_of__racoumowNearmousa_28() { return &____racoumowNearmousa_28; }
	inline void set__racoumowNearmousa_28(int32_t value)
	{
		____racoumowNearmousa_28 = value;
	}

	inline static int32_t get_offset_of__fownabair_29() { return static_cast<int32_t>(offsetof(M_waynaygo161_t3952817293, ____fownabair_29)); }
	inline String_t* get__fownabair_29() const { return ____fownabair_29; }
	inline String_t** get_address_of__fownabair_29() { return &____fownabair_29; }
	inline void set__fownabair_29(String_t* value)
	{
		____fownabair_29 = value;
		Il2CppCodeGenWriteBarrier(&____fownabair_29, value);
	}

	inline static int32_t get_offset_of__zairdear_30() { return static_cast<int32_t>(offsetof(M_waynaygo161_t3952817293, ____zairdear_30)); }
	inline uint32_t get__zairdear_30() const { return ____zairdear_30; }
	inline uint32_t* get_address_of__zairdear_30() { return &____zairdear_30; }
	inline void set__zairdear_30(uint32_t value)
	{
		____zairdear_30 = value;
	}

	inline static int32_t get_offset_of__teacaw_31() { return static_cast<int32_t>(offsetof(M_waynaygo161_t3952817293, ____teacaw_31)); }
	inline uint32_t get__teacaw_31() const { return ____teacaw_31; }
	inline uint32_t* get_address_of__teacaw_31() { return &____teacaw_31; }
	inline void set__teacaw_31(uint32_t value)
	{
		____teacaw_31 = value;
	}

	inline static int32_t get_offset_of__gairdairhu_32() { return static_cast<int32_t>(offsetof(M_waynaygo161_t3952817293, ____gairdairhu_32)); }
	inline bool get__gairdairhu_32() const { return ____gairdairhu_32; }
	inline bool* get_address_of__gairdairhu_32() { return &____gairdairhu_32; }
	inline void set__gairdairhu_32(bool value)
	{
		____gairdairhu_32 = value;
	}

	inline static int32_t get_offset_of__dajineMeayostu_33() { return static_cast<int32_t>(offsetof(M_waynaygo161_t3952817293, ____dajineMeayostu_33)); }
	inline String_t* get__dajineMeayostu_33() const { return ____dajineMeayostu_33; }
	inline String_t** get_address_of__dajineMeayostu_33() { return &____dajineMeayostu_33; }
	inline void set__dajineMeayostu_33(String_t* value)
	{
		____dajineMeayostu_33 = value;
		Il2CppCodeGenWriteBarrier(&____dajineMeayostu_33, value);
	}

	inline static int32_t get_offset_of__yertursearHoufiste_34() { return static_cast<int32_t>(offsetof(M_waynaygo161_t3952817293, ____yertursearHoufiste_34)); }
	inline int32_t get__yertursearHoufiste_34() const { return ____yertursearHoufiste_34; }
	inline int32_t* get_address_of__yertursearHoufiste_34() { return &____yertursearHoufiste_34; }
	inline void set__yertursearHoufiste_34(int32_t value)
	{
		____yertursearHoufiste_34 = value;
	}

	inline static int32_t get_offset_of__masger_35() { return static_cast<int32_t>(offsetof(M_waynaygo161_t3952817293, ____masger_35)); }
	inline float get__masger_35() const { return ____masger_35; }
	inline float* get_address_of__masger_35() { return &____masger_35; }
	inline void set__masger_35(float value)
	{
		____masger_35 = value;
	}

	inline static int32_t get_offset_of__jallsa_36() { return static_cast<int32_t>(offsetof(M_waynaygo161_t3952817293, ____jallsa_36)); }
	inline float get__jallsa_36() const { return ____jallsa_36; }
	inline float* get_address_of__jallsa_36() { return &____jallsa_36; }
	inline void set__jallsa_36(float value)
	{
		____jallsa_36 = value;
	}

	inline static int32_t get_offset_of__seco_37() { return static_cast<int32_t>(offsetof(M_waynaygo161_t3952817293, ____seco_37)); }
	inline bool get__seco_37() const { return ____seco_37; }
	inline bool* get_address_of__seco_37() { return &____seco_37; }
	inline void set__seco_37(bool value)
	{
		____seco_37 = value;
	}

	inline static int32_t get_offset_of__rawtral_38() { return static_cast<int32_t>(offsetof(M_waynaygo161_t3952817293, ____rawtral_38)); }
	inline float get__rawtral_38() const { return ____rawtral_38; }
	inline float* get_address_of__rawtral_38() { return &____rawtral_38; }
	inline void set__rawtral_38(float value)
	{
		____rawtral_38 = value;
	}

	inline static int32_t get_offset_of__dairjiskoLoucawai_39() { return static_cast<int32_t>(offsetof(M_waynaygo161_t3952817293, ____dairjiskoLoucawai_39)); }
	inline uint32_t get__dairjiskoLoucawai_39() const { return ____dairjiskoLoucawai_39; }
	inline uint32_t* get_address_of__dairjiskoLoucawai_39() { return &____dairjiskoLoucawai_39; }
	inline void set__dairjiskoLoucawai_39(uint32_t value)
	{
		____dairjiskoLoucawai_39 = value;
	}

	inline static int32_t get_offset_of__semnow_40() { return static_cast<int32_t>(offsetof(M_waynaygo161_t3952817293, ____semnow_40)); }
	inline String_t* get__semnow_40() const { return ____semnow_40; }
	inline String_t** get_address_of__semnow_40() { return &____semnow_40; }
	inline void set__semnow_40(String_t* value)
	{
		____semnow_40 = value;
		Il2CppCodeGenWriteBarrier(&____semnow_40, value);
	}

	inline static int32_t get_offset_of__rairmal_41() { return static_cast<int32_t>(offsetof(M_waynaygo161_t3952817293, ____rairmal_41)); }
	inline float get__rairmal_41() const { return ____rairmal_41; }
	inline float* get_address_of__rairmal_41() { return &____rairmal_41; }
	inline void set__rairmal_41(float value)
	{
		____rairmal_41 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
