﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_serejamir44
struct M_serejamir44_t1742413338;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_serejamir441742413338.h"

// System.Void GarbageiOS.M_serejamir44::.ctor()
extern "C"  void M_serejamir44__ctor_m3221658249 (M_serejamir44_t1742413338 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_serejamir44::M_meesekaiFemwi0(System.String[],System.Int32)
extern "C"  void M_serejamir44_M_meesekaiFemwi0_m2207085032 (M_serejamir44_t1742413338 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_serejamir44::M_jeejal1(System.String[],System.Int32)
extern "C"  void M_serejamir44_M_jeejal1_m2931455186 (M_serejamir44_t1742413338 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_serejamir44::M_doumaw2(System.String[],System.Int32)
extern "C"  void M_serejamir44_M_doumaw2_m3609887429 (M_serejamir44_t1742413338 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_serejamir44::M_budaVoubearmu3(System.String[],System.Int32)
extern "C"  void M_serejamir44_M_budaVoubearmu3_m124221391 (M_serejamir44_t1742413338 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_serejamir44::M_moustealereGassur4(System.String[],System.Int32)
extern "C"  void M_serejamir44_M_moustealereGassur4_m1340310789 (M_serejamir44_t1742413338 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_serejamir44::M_kubawchiRallmeedou5(System.String[],System.Int32)
extern "C"  void M_serejamir44_M_kubawchiRallmeedou5_m1102915359 (M_serejamir44_t1742413338 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_serejamir44::M_gastair6(System.String[],System.Int32)
extern "C"  void M_serejamir44_M_gastair6_m1973499915 (M_serejamir44_t1742413338 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_serejamir44::M_mistashiJomi7(System.String[],System.Int32)
extern "C"  void M_serejamir44_M_mistashiJomi7_m998109426 (M_serejamir44_t1742413338 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_serejamir44::M_dekigearLege8(System.String[],System.Int32)
extern "C"  void M_serejamir44_M_dekigearLege8_m1191324351 (M_serejamir44_t1742413338 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_serejamir44::ilo_M_gastair61(GarbageiOS.M_serejamir44,System.String[],System.Int32)
extern "C"  void M_serejamir44_ilo_M_gastair61_m1227313379 (Il2CppObject * __this /* static, unused */, M_serejamir44_t1742413338 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
