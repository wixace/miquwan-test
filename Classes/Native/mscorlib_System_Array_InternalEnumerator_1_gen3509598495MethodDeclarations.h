﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2953159047MethodDeclarations.h"

// System.Void System.Array/InternalEnumerator`1<CSSkillData>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m3762584272(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t3509598495 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m2616641763_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<CSSkillData>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2435950800(__this, method) ((  void (*) (InternalEnumerator_1_t3509598495 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2224260061_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<CSSkillData>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2102223302(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t3509598495 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m390763987_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<CSSkillData>::Dispose()
#define InternalEnumerator_1_Dispose_m2779138215(__this, method) ((  void (*) (InternalEnumerator_1_t3509598495 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2760671866_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<CSSkillData>::MoveNext()
#define InternalEnumerator_1_MoveNext_m898702976(__this, method) ((  bool (*) (InternalEnumerator_1_t3509598495 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m3716548237_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<CSSkillData>::get_Current()
#define InternalEnumerator_1_get_Current_m2870374905(__this, method) ((  CSSkillData_t432288523 * (*) (InternalEnumerator_1_t3509598495 *, const MethodInfo*))InternalEnumerator_1_get_Current_m2178852364_gshared)(__this, method)
