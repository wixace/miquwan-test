﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UISprite
struct UISprite_t661437049;
// System.Collections.Generic.List`1<UISpriteData>
struct List_1_t651564179;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIFrameAni
struct  UIFrameAni_t95674339  : public MonoBehaviour_t667441552
{
public:
	// UISprite UIFrameAni::ui
	UISprite_t661437049 * ___ui_2;
	// System.Collections.Generic.List`1<UISpriteData> UIFrameAni::list
	List_1_t651564179 * ___list_3;
	// System.Single UIFrameAni::time
	float ___time_4;
	// System.Int32 UIFrameAni::index
	int32_t ___index_5;
	// System.Single UIFrameAni::spacetime
	float ___spacetime_6;
	// System.Single UIFrameAni::lastTime
	float ___lastTime_7;

public:
	inline static int32_t get_offset_of_ui_2() { return static_cast<int32_t>(offsetof(UIFrameAni_t95674339, ___ui_2)); }
	inline UISprite_t661437049 * get_ui_2() const { return ___ui_2; }
	inline UISprite_t661437049 ** get_address_of_ui_2() { return &___ui_2; }
	inline void set_ui_2(UISprite_t661437049 * value)
	{
		___ui_2 = value;
		Il2CppCodeGenWriteBarrier(&___ui_2, value);
	}

	inline static int32_t get_offset_of_list_3() { return static_cast<int32_t>(offsetof(UIFrameAni_t95674339, ___list_3)); }
	inline List_1_t651564179 * get_list_3() const { return ___list_3; }
	inline List_1_t651564179 ** get_address_of_list_3() { return &___list_3; }
	inline void set_list_3(List_1_t651564179 * value)
	{
		___list_3 = value;
		Il2CppCodeGenWriteBarrier(&___list_3, value);
	}

	inline static int32_t get_offset_of_time_4() { return static_cast<int32_t>(offsetof(UIFrameAni_t95674339, ___time_4)); }
	inline float get_time_4() const { return ___time_4; }
	inline float* get_address_of_time_4() { return &___time_4; }
	inline void set_time_4(float value)
	{
		___time_4 = value;
	}

	inline static int32_t get_offset_of_index_5() { return static_cast<int32_t>(offsetof(UIFrameAni_t95674339, ___index_5)); }
	inline int32_t get_index_5() const { return ___index_5; }
	inline int32_t* get_address_of_index_5() { return &___index_5; }
	inline void set_index_5(int32_t value)
	{
		___index_5 = value;
	}

	inline static int32_t get_offset_of_spacetime_6() { return static_cast<int32_t>(offsetof(UIFrameAni_t95674339, ___spacetime_6)); }
	inline float get_spacetime_6() const { return ___spacetime_6; }
	inline float* get_address_of_spacetime_6() { return &___spacetime_6; }
	inline void set_spacetime_6(float value)
	{
		___spacetime_6 = value;
	}

	inline static int32_t get_offset_of_lastTime_7() { return static_cast<int32_t>(offsetof(UIFrameAni_t95674339, ___lastTime_7)); }
	inline float get_lastTime_7() const { return ___lastTime_7; }
	inline float* get_address_of_lastTime_7() { return &___lastTime_7; }
	inline void set_lastTime_7(float value)
	{
		___lastTime_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
