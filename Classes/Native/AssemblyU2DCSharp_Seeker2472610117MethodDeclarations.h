﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Seeker
struct Seeker_t2472610117;
// Pathfinding.Path
struct Path_t1974241691;
// Pathfinding.IPathModifier
struct IPathModifier_t1088723335;
// Pathfinding.ABPath
struct ABPath_t1187561148;
// OnPathDelegate
struct OnPathDelegate_t598607977;
// Pathfinding.MultiTargetPath
struct MultiTargetPath_t4131434065;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t215400611;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// OnPathDelegate[]
struct OnPathDelegateU5BU5D_t1505600468;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_Path1974241691.h"
#include "AssemblyU2DCSharp_Seeker_ModifierPass242289714.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_OnPathDelegate598607977.h"
#include "AssemblyU2DCSharp_Pathfinding_MultiTargetPath4131434065.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_Seeker2472610117.h"
#include "AssemblyU2DCSharp_Pathfinding_Int31974045594.h"

// System.Void Seeker::.ctor()
extern "C"  void Seeker__ctor_m1657534710 (Seeker_t2472610117 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Path Seeker::GetCurrentPath()
extern "C"  Path_t1974241691 * Seeker_GetCurrentPath_m130564772 (Seeker_t2472610117 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Seeker::Awake()
extern "C"  void Seeker_Awake_m1895139929 (Seeker_t2472610117 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Seeker::OnDestroy()
extern "C"  void Seeker_OnDestroy_m1231839983 (Seeker_t2472610117 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Seeker::ReleaseClaimedPath()
extern "C"  void Seeker_ReleaseClaimedPath_m1845309095 (Seeker_t2472610117 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Seeker::RegisterModifier(Pathfinding.IPathModifier)
extern "C"  void Seeker_RegisterModifier_m2981737007 (Seeker_t2472610117 * __this, Il2CppObject * ___mod0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Seeker::DeregisterModifier(Pathfinding.IPathModifier)
extern "C"  void Seeker_DeregisterModifier_m2266408526 (Seeker_t2472610117 * __this, Il2CppObject * ___mod0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Seeker::PostProcess(Pathfinding.Path)
extern "C"  void Seeker_PostProcess_m3710039892 (Seeker_t2472610117 * __this, Path_t1974241691 * ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Seeker::RunModifiers(Seeker/ModifierPass,Pathfinding.Path)
extern "C"  void Seeker_RunModifiers_m2777234966 (Seeker_t2472610117 * __this, int32_t ___pass0, Path_t1974241691 * ___p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Seeker::IsDone()
extern "C"  bool Seeker_IsDone_m2507346382 (Seeker_t2472610117 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Seeker::OnPathComplete(Pathfinding.Path)
extern "C"  void Seeker_OnPathComplete_m4283850652 (Seeker_t2472610117 * __this, Path_t1974241691 * ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Seeker::OnPathComplete(Pathfinding.Path,System.Boolean,System.Boolean)
extern "C"  void Seeker_OnPathComplete_m2888375548 (Seeker_t2472610117 * __this, Path_t1974241691 * ___p0, bool ___runModifiers1, bool ___sendCallbacks2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Seeker::OnPartialPathComplete(Pathfinding.Path)
extern "C"  void Seeker_OnPartialPathComplete_m2353987557 (Seeker_t2472610117 * __this, Path_t1974241691 * ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Seeker::OnMultiPathComplete(Pathfinding.Path)
extern "C"  void Seeker_OnMultiPathComplete_m1902167709 (Seeker_t2472610117 * __this, Path_t1974241691 * ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.ABPath Seeker::GetNewPath(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  ABPath_t1187561148 * Seeker_GetNewPath_m1245565794 (Seeker_t2472610117 * __this, Vector3_t4282066566  ___start0, Vector3_t4282066566  ___end1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Path Seeker::StartPath(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Path_t1974241691 * Seeker_StartPath_m677111775 (Seeker_t2472610117 * __this, Vector3_t4282066566  ___start0, Vector3_t4282066566  ___end1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Path Seeker::StartPath(UnityEngine.Vector3,UnityEngine.Vector3,OnPathDelegate)
extern "C"  Path_t1974241691 * Seeker_StartPath_m3394378942 (Seeker_t2472610117 * __this, Vector3_t4282066566  ___start0, Vector3_t4282066566  ___end1, OnPathDelegate_t598607977 * ___callback2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Path Seeker::StartPath(UnityEngine.Vector3,UnityEngine.Vector3,OnPathDelegate,System.Int32)
extern "C"  Path_t1974241691 * Seeker_StartPath_m3736741081 (Seeker_t2472610117 * __this, Vector3_t4282066566  ___start0, Vector3_t4282066566  ___end1, OnPathDelegate_t598607977 * ___callback2, int32_t ___graphMask3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Path Seeker::StartPath(Pathfinding.Path,OnPathDelegate,System.Int32)
extern "C"  Path_t1974241691 * Seeker_StartPath_m3235751032 (Seeker_t2472610117 * __this, Path_t1974241691 * ___p0, OnPathDelegate_t598607977 * ___callback1, int32_t ___graphMask2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.MultiTargetPath Seeker::StartMultiTargetPath(UnityEngine.Vector3,UnityEngine.Vector3[],System.Boolean,OnPathDelegate,System.Int32)
extern "C"  MultiTargetPath_t4131434065 * Seeker_StartMultiTargetPath_m4186258262 (Seeker_t2472610117 * __this, Vector3_t4282066566  ___start0, Vector3U5BU5D_t215400611* ___endPoints1, bool ___pathsForAll2, OnPathDelegate_t598607977 * ___callback3, int32_t ___graphMask4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.MultiTargetPath Seeker::StartMultiTargetPath(UnityEngine.Vector3[],UnityEngine.Vector3,System.Boolean,OnPathDelegate,System.Int32)
extern "C"  MultiTargetPath_t4131434065 * Seeker_StartMultiTargetPath_m1188414870 (Seeker_t2472610117 * __this, Vector3U5BU5D_t215400611* ___startPoints0, Vector3_t4282066566  ___end1, bool ___pathsForAll2, OnPathDelegate_t598607977 * ___callback3, int32_t ___graphMask4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.MultiTargetPath Seeker::StartMultiTargetPath(Pathfinding.MultiTargetPath,OnPathDelegate,System.Int32)
extern "C"  MultiTargetPath_t4131434065 * Seeker_StartMultiTargetPath_m1111411646 (Seeker_t2472610117 * __this, MultiTargetPath_t4131434065 * ___p0, OnPathDelegate_t598607977 * ___callback1, int32_t ___graphMask2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Seeker::DelayPathStart(Pathfinding.Path)
extern "C"  Il2CppObject * Seeker_DelayPathStart_m1023060385 (Seeker_t2472610117 * __this, Path_t1974241691 * ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Seeker::OnDrawGizmos()
extern "C"  void Seeker_OnDrawGizmos_m3501733738 (Seeker_t2472610117 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Seeker::ilo_Invoke1(OnPathDelegate,Pathfinding.Path)
extern "C"  void Seeker_ilo_Invoke1_m376349126 (Il2CppObject * __this /* static, unused */, OnPathDelegate_t598607977 * ____this0, Path_t1974241691 * ___p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Seeker::ilo_PreProcess2(Pathfinding.IPathModifier,Pathfinding.Path)
extern "C"  void Seeker_ilo_PreProcess2_m2932294087 (Il2CppObject * __this /* static, unused */, Il2CppObject * ____this0, Path_t1974241691 * ___p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Seeker::ilo_Claim3(Pathfinding.Path,System.Object)
extern "C"  void Seeker_ilo_Claim3_m1246662487 (Il2CppObject * __this /* static, unused */, Path_t1974241691 * ____this0, Il2CppObject * ___o1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Seeker::ilo_OnPathComplete4(Seeker,Pathfinding.Path,System.Boolean,System.Boolean)
extern "C"  void Seeker_ilo_OnPathComplete4_m3595944612 (Il2CppObject * __this /* static, unused */, Seeker_t2472610117 * ____this0, Path_t1974241691 * ___p1, bool ___runModifiers2, bool ___sendCallbacks3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Path Seeker::ilo_StartPath5(Seeker,Pathfinding.Path,OnPathDelegate,System.Int32)
extern "C"  Path_t1974241691 * Seeker_ilo_StartPath5_m705480569 (Il2CppObject * __this /* static, unused */, Seeker_t2472610117 * ____this0, Path_t1974241691 * ___p1, OnPathDelegate_t598607977 * ___callback2, int32_t ___graphMask3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Seeker::ilo_Error6(Pathfinding.Path)
extern "C"  void Seeker_ilo_Error6_m3873777728 (Il2CppObject * __this /* static, unused */, Path_t1974241691 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.MultiTargetPath Seeker::ilo_Construct7(UnityEngine.Vector3,UnityEngine.Vector3[],OnPathDelegate[],OnPathDelegate)
extern "C"  MultiTargetPath_t4131434065 * Seeker_ilo_Construct7_m578510141 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___start0, Vector3U5BU5D_t215400611* ___targets1, OnPathDelegateU5BU5D_t1505600468* ___callbackDelegates2, OnPathDelegate_t598607977 * ___callback3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.MultiTargetPath Seeker::ilo_StartMultiTargetPath8(Seeker,Pathfinding.MultiTargetPath,OnPathDelegate,System.Int32)
extern "C"  MultiTargetPath_t4131434065 * Seeker_ilo_StartMultiTargetPath8_m3523977818 (Il2CppObject * __this /* static, unused */, Seeker_t2472610117 * ____this0, MultiTargetPath_t4131434065 * ___p1, OnPathDelegate_t598607977 * ___callback2, int32_t ___graphMask3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Seeker::ilo_StartPath9(Pathfinding.Path,System.Boolean)
extern "C"  void Seeker_ilo_StartPath9_m1048285561 (Il2CppObject * __this /* static, unused */, Path_t1974241691 * ___p0, bool ___pushToFront1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Seeker::ilo_op_Explicit10(Pathfinding.Int3)
extern "C"  Vector3_t4282066566  Seeker_ilo_op_Explicit10_m820195114 (Il2CppObject * __this /* static, unused */, Int3_t1974045594  ___ob0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
