﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_vurde23
struct M_vurde23_t3318299605;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_vurde233318299605.h"

// System.Void GarbageiOS.M_vurde23::.ctor()
extern "C"  void M_vurde23__ctor_m2268433198 (M_vurde23_t3318299605 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_vurde23::M_jemaircouGalwa0(System.String[],System.Int32)
extern "C"  void M_vurde23_M_jemaircouGalwa0_m3265134838 (M_vurde23_t3318299605 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_vurde23::M_vicheremelCowhal1(System.String[],System.Int32)
extern "C"  void M_vurde23_M_vicheremelCowhal1_m1585093446 (M_vurde23_t3318299605 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_vurde23::M_coubaw2(System.String[],System.Int32)
extern "C"  void M_vurde23_M_coubaw2_m245390996 (M_vurde23_t3318299605 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_vurde23::M_sekibel3(System.String[],System.Int32)
extern "C"  void M_vurde23_M_sekibel3_m2695057753 (M_vurde23_t3318299605 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_vurde23::ilo_M_jemaircouGalwa01(GarbageiOS.M_vurde23,System.String[],System.Int32)
extern "C"  void M_vurde23_ilo_M_jemaircouGalwa01_m2441317571 (Il2CppObject * __this /* static, unused */, M_vurde23_t3318299605 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
