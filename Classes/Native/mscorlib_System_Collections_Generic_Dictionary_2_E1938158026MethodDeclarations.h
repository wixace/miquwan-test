﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<PushType,System.Object>
struct Dictionary_2_t620834634;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1938158026.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_519615340.h"
#include "AssemblyU2DCSharp_PushType1840642452.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<PushType,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m396494165_gshared (Enumerator_t1938158026 * __this, Dictionary_2_t620834634 * ___dictionary0, const MethodInfo* method);
#define Enumerator__ctor_m396494165(__this, ___dictionary0, method) ((  void (*) (Enumerator_t1938158026 *, Dictionary_2_t620834634 *, const MethodInfo*))Enumerator__ctor_m396494165_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<PushType,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m817845174_gshared (Enumerator_t1938158026 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m817845174(__this, method) ((  Il2CppObject * (*) (Enumerator_t1938158026 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m817845174_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<PushType,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1157578816_gshared (Enumerator_t1938158026 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1157578816(__this, method) ((  void (*) (Enumerator_t1938158026 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1157578816_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<PushType,System.Object>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t1751606614  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1960616567_gshared (Enumerator_t1938158026 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1960616567(__this, method) ((  DictionaryEntry_t1751606614  (*) (Enumerator_t1938158026 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1960616567_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<PushType,System.Object>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1945268242_gshared (Enumerator_t1938158026 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1945268242(__this, method) ((  Il2CppObject * (*) (Enumerator_t1938158026 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1945268242_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<PushType,System.Object>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2140443172_gshared (Enumerator_t1938158026 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2140443172(__this, method) ((  Il2CppObject * (*) (Enumerator_t1938158026 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2140443172_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<PushType,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2697807600_gshared (Enumerator_t1938158026 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m2697807600(__this, method) ((  bool (*) (Enumerator_t1938158026 *, const MethodInfo*))Enumerator_MoveNext_m2697807600_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<PushType,System.Object>::get_Current()
extern "C"  KeyValuePair_2_t519615340  Enumerator_get_Current_m912297676_gshared (Enumerator_t1938158026 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m912297676(__this, method) ((  KeyValuePair_2_t519615340  (*) (Enumerator_t1938158026 *, const MethodInfo*))Enumerator_get_Current_m912297676_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<PushType,System.Object>::get_CurrentKey()
extern "C"  int32_t Enumerator_get_CurrentKey_m3381004409_gshared (Enumerator_t1938158026 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m3381004409(__this, method) ((  int32_t (*) (Enumerator_t1938158026 *, const MethodInfo*))Enumerator_get_CurrentKey_m3381004409_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<PushType,System.Object>::get_CurrentValue()
extern "C"  Il2CppObject * Enumerator_get_CurrentValue_m3438306169_gshared (Enumerator_t1938158026 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m3438306169(__this, method) ((  Il2CppObject * (*) (Enumerator_t1938158026 *, const MethodInfo*))Enumerator_get_CurrentValue_m3438306169_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<PushType,System.Object>::Reset()
extern "C"  void Enumerator_Reset_m2343661607_gshared (Enumerator_t1938158026 * __this, const MethodInfo* method);
#define Enumerator_Reset_m2343661607(__this, method) ((  void (*) (Enumerator_t1938158026 *, const MethodInfo*))Enumerator_Reset_m2343661607_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<PushType,System.Object>::VerifyState()
extern "C"  void Enumerator_VerifyState_m1040434928_gshared (Enumerator_t1938158026 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m1040434928(__this, method) ((  void (*) (Enumerator_t1938158026 *, const MethodInfo*))Enumerator_VerifyState_m1040434928_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<PushType,System.Object>::VerifyCurrent()
extern "C"  void Enumerator_VerifyCurrent_m1875457624_gshared (Enumerator_t1938158026 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m1875457624(__this, method) ((  void (*) (Enumerator_t1938158026 *, const MethodInfo*))Enumerator_VerifyCurrent_m1875457624_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<PushType,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m4219370295_gshared (Enumerator_t1938158026 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m4219370295(__this, method) ((  void (*) (Enumerator_t1938158026 *, const MethodInfo*))Enumerator_Dispose_m4219370295_gshared)(__this, method)
