﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.List`1<JSCWaveNpcConfig>
struct List_1_t1294722912;
// JSCLevelHeroConfig
struct JSCLevelHeroConfig_t1953226502;
// ProtoBuf.IExtension
struct IExtension_t1606339106;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JSCLevelConfig
struct  JSCLevelConfig_t1411099500  : public Il2CppObject
{
public:
	// System.Int32 JSCLevelConfig::_id
	int32_t ____id_0;
	// System.String JSCLevelConfig::_resName
	String_t* ____resName_1;
	// System.Int32 JSCLevelConfig::_type
	int32_t ____type_2;
	// System.Int32 JSCLevelConfig::_mapPathId
	int32_t ____mapPathId_3;
	// System.Collections.Generic.List`1<JSCWaveNpcConfig> JSCLevelConfig::_monsters
	List_1_t1294722912 * ____monsters_4;
	// System.Collections.Generic.List`1<JSCWaveNpcConfig> JSCLevelConfig::_others
	List_1_t1294722912 * ____others_5;
	// System.Collections.Generic.List`1<JSCWaveNpcConfig> JSCLevelConfig::_guide
	List_1_t1294722912 * ____guide_6;
	// JSCLevelHeroConfig JSCLevelConfig::_hero
	JSCLevelHeroConfig_t1953226502 * ____hero_7;
	// JSCLevelHeroConfig JSCLevelConfig::_guideHero
	JSCLevelHeroConfig_t1953226502 * ____guideHero_8;
	// System.Collections.Generic.List`1<JSCWaveNpcConfig> JSCLevelConfig::_plot
	List_1_t1294722912 * ____plot_9;
	// ProtoBuf.IExtension JSCLevelConfig::extensionObject
	Il2CppObject * ___extensionObject_10;

public:
	inline static int32_t get_offset_of__id_0() { return static_cast<int32_t>(offsetof(JSCLevelConfig_t1411099500, ____id_0)); }
	inline int32_t get__id_0() const { return ____id_0; }
	inline int32_t* get_address_of__id_0() { return &____id_0; }
	inline void set__id_0(int32_t value)
	{
		____id_0 = value;
	}

	inline static int32_t get_offset_of__resName_1() { return static_cast<int32_t>(offsetof(JSCLevelConfig_t1411099500, ____resName_1)); }
	inline String_t* get__resName_1() const { return ____resName_1; }
	inline String_t** get_address_of__resName_1() { return &____resName_1; }
	inline void set__resName_1(String_t* value)
	{
		____resName_1 = value;
		Il2CppCodeGenWriteBarrier(&____resName_1, value);
	}

	inline static int32_t get_offset_of__type_2() { return static_cast<int32_t>(offsetof(JSCLevelConfig_t1411099500, ____type_2)); }
	inline int32_t get__type_2() const { return ____type_2; }
	inline int32_t* get_address_of__type_2() { return &____type_2; }
	inline void set__type_2(int32_t value)
	{
		____type_2 = value;
	}

	inline static int32_t get_offset_of__mapPathId_3() { return static_cast<int32_t>(offsetof(JSCLevelConfig_t1411099500, ____mapPathId_3)); }
	inline int32_t get__mapPathId_3() const { return ____mapPathId_3; }
	inline int32_t* get_address_of__mapPathId_3() { return &____mapPathId_3; }
	inline void set__mapPathId_3(int32_t value)
	{
		____mapPathId_3 = value;
	}

	inline static int32_t get_offset_of__monsters_4() { return static_cast<int32_t>(offsetof(JSCLevelConfig_t1411099500, ____monsters_4)); }
	inline List_1_t1294722912 * get__monsters_4() const { return ____monsters_4; }
	inline List_1_t1294722912 ** get_address_of__monsters_4() { return &____monsters_4; }
	inline void set__monsters_4(List_1_t1294722912 * value)
	{
		____monsters_4 = value;
		Il2CppCodeGenWriteBarrier(&____monsters_4, value);
	}

	inline static int32_t get_offset_of__others_5() { return static_cast<int32_t>(offsetof(JSCLevelConfig_t1411099500, ____others_5)); }
	inline List_1_t1294722912 * get__others_5() const { return ____others_5; }
	inline List_1_t1294722912 ** get_address_of__others_5() { return &____others_5; }
	inline void set__others_5(List_1_t1294722912 * value)
	{
		____others_5 = value;
		Il2CppCodeGenWriteBarrier(&____others_5, value);
	}

	inline static int32_t get_offset_of__guide_6() { return static_cast<int32_t>(offsetof(JSCLevelConfig_t1411099500, ____guide_6)); }
	inline List_1_t1294722912 * get__guide_6() const { return ____guide_6; }
	inline List_1_t1294722912 ** get_address_of__guide_6() { return &____guide_6; }
	inline void set__guide_6(List_1_t1294722912 * value)
	{
		____guide_6 = value;
		Il2CppCodeGenWriteBarrier(&____guide_6, value);
	}

	inline static int32_t get_offset_of__hero_7() { return static_cast<int32_t>(offsetof(JSCLevelConfig_t1411099500, ____hero_7)); }
	inline JSCLevelHeroConfig_t1953226502 * get__hero_7() const { return ____hero_7; }
	inline JSCLevelHeroConfig_t1953226502 ** get_address_of__hero_7() { return &____hero_7; }
	inline void set__hero_7(JSCLevelHeroConfig_t1953226502 * value)
	{
		____hero_7 = value;
		Il2CppCodeGenWriteBarrier(&____hero_7, value);
	}

	inline static int32_t get_offset_of__guideHero_8() { return static_cast<int32_t>(offsetof(JSCLevelConfig_t1411099500, ____guideHero_8)); }
	inline JSCLevelHeroConfig_t1953226502 * get__guideHero_8() const { return ____guideHero_8; }
	inline JSCLevelHeroConfig_t1953226502 ** get_address_of__guideHero_8() { return &____guideHero_8; }
	inline void set__guideHero_8(JSCLevelHeroConfig_t1953226502 * value)
	{
		____guideHero_8 = value;
		Il2CppCodeGenWriteBarrier(&____guideHero_8, value);
	}

	inline static int32_t get_offset_of__plot_9() { return static_cast<int32_t>(offsetof(JSCLevelConfig_t1411099500, ____plot_9)); }
	inline List_1_t1294722912 * get__plot_9() const { return ____plot_9; }
	inline List_1_t1294722912 ** get_address_of__plot_9() { return &____plot_9; }
	inline void set__plot_9(List_1_t1294722912 * value)
	{
		____plot_9 = value;
		Il2CppCodeGenWriteBarrier(&____plot_9, value);
	}

	inline static int32_t get_offset_of_extensionObject_10() { return static_cast<int32_t>(offsetof(JSCLevelConfig_t1411099500, ___extensionObject_10)); }
	inline Il2CppObject * get_extensionObject_10() const { return ___extensionObject_10; }
	inline Il2CppObject ** get_address_of_extensionObject_10() { return &___extensionObject_10; }
	inline void set_extensionObject_10(Il2CppObject * value)
	{
		___extensionObject_10 = value;
		Il2CppCodeGenWriteBarrier(&___extensionObject_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
