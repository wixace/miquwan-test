﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._c23d2b5e66936cf1fc222fc988b3f30e
struct _c23d2b5e66936cf1fc222fc988b3f30e_t2185101989;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._c23d2b5e66936cf1fc222fc988b3f30e::.ctor()
extern "C"  void _c23d2b5e66936cf1fc222fc988b3f30e__ctor_m3850194216 (_c23d2b5e66936cf1fc222fc988b3f30e_t2185101989 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._c23d2b5e66936cf1fc222fc988b3f30e::_c23d2b5e66936cf1fc222fc988b3f30em2(System.Int32)
extern "C"  int32_t _c23d2b5e66936cf1fc222fc988b3f30e__c23d2b5e66936cf1fc222fc988b3f30em2_m516384633 (_c23d2b5e66936cf1fc222fc988b3f30e_t2185101989 * __this, int32_t ____c23d2b5e66936cf1fc222fc988b3f30ea0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._c23d2b5e66936cf1fc222fc988b3f30e::_c23d2b5e66936cf1fc222fc988b3f30em(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _c23d2b5e66936cf1fc222fc988b3f30e__c23d2b5e66936cf1fc222fc988b3f30em_m3544161117 (_c23d2b5e66936cf1fc222fc988b3f30e_t2185101989 * __this, int32_t ____c23d2b5e66936cf1fc222fc988b3f30ea0, int32_t ____c23d2b5e66936cf1fc222fc988b3f30e871, int32_t ____c23d2b5e66936cf1fc222fc988b3f30ec2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
