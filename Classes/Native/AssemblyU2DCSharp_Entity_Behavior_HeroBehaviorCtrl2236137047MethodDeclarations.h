﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Entity.Behavior.HeroBehaviorCtrl
struct HeroBehaviorCtrl_t2236137047;
// CombatEntity
struct CombatEntity_t684137495;
// Entity.Behavior.IBehaviorCtrl
struct IBehaviorCtrl_t4225040900;
// Entity.Behavior.IBehavior
struct IBehavior_t770859129;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CombatEntity684137495.h"
#include "AssemblyU2DCSharp_Entity_Behavior_IBehaviorCtrl4225040900.h"
#include "AssemblyU2DCSharp_Entity_Behavior_IBehavior770859129.h"

// System.Void Entity.Behavior.HeroBehaviorCtrl::.ctor(CombatEntity)
extern "C"  void HeroBehaviorCtrl__ctor_m2854994044 (HeroBehaviorCtrl_t2236137047 * __this, CombatEntity_t684137495 * ___entity0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.HeroBehaviorCtrl::Init()
extern "C"  void HeroBehaviorCtrl_Init_m3031749441 (HeroBehaviorCtrl_t2236137047 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.HeroBehaviorCtrl::ilo_AddBehavior1(Entity.Behavior.IBehaviorCtrl,Entity.Behavior.IBehavior)
extern "C"  void HeroBehaviorCtrl_ilo_AddBehavior1_m721984957 (Il2CppObject * __this /* static, unused */, IBehaviorCtrl_t4225040900 * ____this0, IBehavior_t770859129 * ___behavior1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
