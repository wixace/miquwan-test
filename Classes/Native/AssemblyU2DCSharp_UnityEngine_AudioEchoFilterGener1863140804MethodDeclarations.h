﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_AudioEchoFilterGenerated
struct UnityEngine_AudioEchoFilterGenerated_t1863140804;
// JSVCall
struct JSVCall_t3708497963;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"

// System.Void UnityEngine_AudioEchoFilterGenerated::.ctor()
extern "C"  void UnityEngine_AudioEchoFilterGenerated__ctor_m3900449111 (UnityEngine_AudioEchoFilterGenerated_t1863140804 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AudioEchoFilterGenerated::AudioEchoFilter_AudioEchoFilter1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AudioEchoFilterGenerated_AudioEchoFilter_AudioEchoFilter1_m3690070747 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioEchoFilterGenerated::AudioEchoFilter_delay(JSVCall)
extern "C"  void UnityEngine_AudioEchoFilterGenerated_AudioEchoFilter_delay_m2199754427 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioEchoFilterGenerated::AudioEchoFilter_decayRatio(JSVCall)
extern "C"  void UnityEngine_AudioEchoFilterGenerated_AudioEchoFilter_decayRatio_m566282205 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioEchoFilterGenerated::AudioEchoFilter_dryMix(JSVCall)
extern "C"  void UnityEngine_AudioEchoFilterGenerated_AudioEchoFilter_dryMix_m206793629 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioEchoFilterGenerated::AudioEchoFilter_wetMix(JSVCall)
extern "C"  void UnityEngine_AudioEchoFilterGenerated_AudioEchoFilter_wetMix_m2529816792 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioEchoFilterGenerated::__Register()
extern "C"  void UnityEngine_AudioEchoFilterGenerated___Register_m1325898192 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AudioEchoFilterGenerated::ilo_attachFinalizerObject1(System.Int32)
extern "C"  bool UnityEngine_AudioEchoFilterGenerated_ilo_attachFinalizerObject1_m1518205800 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine_AudioEchoFilterGenerated::ilo_getSingle2(System.Int32)
extern "C"  float UnityEngine_AudioEchoFilterGenerated_ilo_getSingle2_m1965506673 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioEchoFilterGenerated::ilo_setSingle3(System.Int32,System.Single)
extern "C"  void UnityEngine_AudioEchoFilterGenerated_ilo_setSingle3_m2701871295 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
