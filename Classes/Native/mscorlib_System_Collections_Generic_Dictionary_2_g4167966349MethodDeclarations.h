﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>
struct Dictionary_2_t4167966349;
// System.Collections.Generic.IEqualityComparer`1<System.UInt32>
struct IEqualityComparer_1_t815702385;
// System.Collections.Generic.IDictionary`2<System.UInt32,System.Object>
struct IDictionary_2_t3745839694;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t2185721892;
// System.Collections.Generic.ICollection`1<System.UInt32>
struct ICollection_1_t919257968;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t770439062;
// System.Collections.ICollection
struct ICollection_t2643922881;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.KeyValuePair`2<System.UInt32,System.Object>[]
struct KeyValuePair_2U5BU5D_t2966088118;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.UInt32,System.Object>>
struct IEnumerator_1_t1683644808;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t951828701;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.UInt32,System.Object>
struct KeyCollection_t1499758504;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.UInt32,System.Object>
struct ValueCollection_t2868572062;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2185721892.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon2761351129.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24066747055.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1190322445.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"

// System.Void System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>::.ctor()
extern "C"  void Dictionary_2__ctor_m2403136537_gshared (Dictionary_2_t4167966349 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m2403136537(__this, method) ((  void (*) (Dictionary_2_t4167966349 *, const MethodInfo*))Dictionary_2__ctor_m2403136537_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m978935824_gshared (Dictionary_2_t4167966349 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define Dictionary_2__ctor_m978935824(__this, ___comparer0, method) ((  void (*) (Dictionary_2_t4167966349 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m978935824_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
extern "C"  void Dictionary_2__ctor_m297965343_gshared (Dictionary_2_t4167966349 * __this, Il2CppObject* ___dictionary0, const MethodInfo* method);
#define Dictionary_2__ctor_m297965343(__this, ___dictionary0, method) ((  void (*) (Dictionary_2_t4167966349 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m297965343_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>::.ctor(System.Int32)
extern "C"  void Dictionary_2__ctor_m3637005290_gshared (Dictionary_2_t4167966349 * __this, int32_t ___capacity0, const MethodInfo* method);
#define Dictionary_2__ctor_m3637005290(__this, ___capacity0, method) ((  void (*) (Dictionary_2_t4167966349 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m3637005290_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m2792234558_gshared (Dictionary_2_t4167966349 * __this, Il2CppObject* ___dictionary0, Il2CppObject* ___comparer1, const MethodInfo* method);
#define Dictionary_2__ctor_m2792234558(__this, ___dictionary0, ___comparer1, method) ((  void (*) (Dictionary_2_t4167966349 *, Il2CppObject*, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m2792234558_gshared)(__this, ___dictionary0, ___comparer1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2__ctor_m1697299930_gshared (Dictionary_2_t4167966349 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method);
#define Dictionary_2__ctor_m1697299930(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t4167966349 *, SerializationInfo_t2185721892 *, StreamingContext_t2761351129 , const MethodInfo*))Dictionary_2__ctor_m1697299930_gshared)(__this, ___info0, ___context1, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m2412434197_gshared (Dictionary_2_t4167966349 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m2412434197(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t4167966349 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m2412434197_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m3273778453_gshared (Dictionary_2_t4167966349 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m3273778453(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t4167966349 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m3273778453_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>::System.Collections.IDictionary.get_Keys()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Keys_m1705758111_gshared (Dictionary_2_t4167966349 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m1705758111(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t4167966349 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m1705758111_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>::System.Collections.IDictionary.get_Values()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Values_m923592397_gshared (Dictionary_2_t4167966349 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Values_m923592397(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t4167966349 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Values_m923592397_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>::System.Collections.IDictionary.get_IsFixedSize()
extern "C"  bool Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m1366614120_gshared (Dictionary_2_t4167966349 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m1366614120(__this, method) ((  bool (*) (Dictionary_2_t4167966349 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m1366614120_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>::System.Collections.IDictionary.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m1883566481_gshared (Dictionary_2_t4167966349 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m1883566481(__this, method) ((  bool (*) (Dictionary_2_t4167966349 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m1883566481_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>::System.Collections.IDictionary.get_Item(System.Object)
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Item_m1591938383_gshared (Dictionary_2_t4167966349 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m1591938383(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t4167966349 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m1591938383_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_set_Item_m663767988_gshared (Dictionary_2_t4167966349 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m663767988(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t4167966349 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m663767988_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Add_m704279069_gshared (Dictionary_2_t4167966349 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m704279069(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t4167966349 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m704279069_gshared)(__this, ___key0, ___value1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>::System.Collections.IDictionary.Contains(System.Object)
extern "C"  bool Dictionary_2_System_Collections_IDictionary_Contains_m1824051769_gshared (Dictionary_2_t4167966349 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m1824051769(__this, ___key0, method) ((  bool (*) (Dictionary_2_t4167966349 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m1824051769_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>::System.Collections.IDictionary.Remove(System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Remove_m2711137778_gshared (Dictionary_2_t4167966349 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m2711137778(__this, ___key0, method) ((  void (*) (Dictionary_2_t4167966349 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m2711137778_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m318222267_gshared (Dictionary_2_t4167966349 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m318222267(__this, method) ((  bool (*) (Dictionary_2_t4167966349 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m318222267_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1781101927_gshared (Dictionary_2_t4167966349 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1781101927(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t4167966349 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1781101927_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1658812479_gshared (Dictionary_2_t4167966349 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1658812479(__this, method) ((  bool (*) (Dictionary_2_t4167966349 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1658812479_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m4166465928_gshared (Dictionary_2_t4167966349 * __this, KeyValuePair_2_t4066747055  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m4166465928(__this, ___keyValuePair0, method) ((  void (*) (Dictionary_2_t4167966349 *, KeyValuePair_2_t4066747055 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m4166465928_gshared)(__this, ___keyValuePair0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m2944884410_gshared (Dictionary_2_t4167966349 * __this, KeyValuePair_2_t4066747055  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m2944884410(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t4167966349 *, KeyValuePair_2_t4066747055 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m2944884410_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m805033836_gshared (Dictionary_2_t4167966349 * __this, KeyValuePair_2U5BU5D_t2966088118* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m805033836(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t4167966349 *, KeyValuePair_2U5BU5D_t2966088118*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m805033836_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1827794015_gshared (Dictionary_2_t4167966349 * __this, KeyValuePair_2_t4066747055  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1827794015(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t4167966349 *, KeyValuePair_2_t4066747055 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1827794015_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Dictionary_2_System_Collections_ICollection_CopyTo_m1153559371_gshared (Dictionary_2_t4167966349 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m1153559371(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t4167966349 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m1153559371_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m3579962886_gshared (Dictionary_2_t4167966349 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m3579962886(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t4167966349 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m3579962886_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m1860656259_gshared (Dictionary_2_t4167966349 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m1860656259(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t4167966349 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m1860656259_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>::System.Collections.IDictionary.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m859982238_gshared (Dictionary_2_t4167966349 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m859982238(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t4167966349 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m859982238_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>::get_Count()
extern "C"  int32_t Dictionary_2_get_Count_m25921281_gshared (Dictionary_2_t4167966349 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m25921281(__this, method) ((  int32_t (*) (Dictionary_2_t4167966349 *, const MethodInfo*))Dictionary_2_get_Count_m25921281_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>::get_Item(TKey)
extern "C"  Il2CppObject * Dictionary_2_get_Item_m525730250_gshared (Dictionary_2_t4167966349 * __this, uint32_t ___key0, const MethodInfo* method);
#define Dictionary_2_get_Item_m525730250(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t4167966349 *, uint32_t, const MethodInfo*))Dictionary_2_get_Item_m525730250_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>::set_Item(TKey,TValue)
extern "C"  void Dictionary_2_set_Item_m4259547673_gshared (Dictionary_2_t4167966349 * __this, uint32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_set_Item_m4259547673(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t4167966349 *, uint32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_set_Item_m4259547673_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2_Init_m696550289_gshared (Dictionary_2_t4167966349 * __this, int32_t ___capacity0, Il2CppObject* ___hcp1, const MethodInfo* method);
#define Dictionary_2_Init_m696550289(__this, ___capacity0, ___hcp1, method) ((  void (*) (Dictionary_2_t4167966349 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2_Init_m696550289_gshared)(__this, ___capacity0, ___hcp1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>::InitArrays(System.Int32)
extern "C"  void Dictionary_2_InitArrays_m344112198_gshared (Dictionary_2_t4167966349 * __this, int32_t ___size0, const MethodInfo* method);
#define Dictionary_2_InitArrays_m344112198(__this, ___size0, method) ((  void (*) (Dictionary_2_t4167966349 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m344112198_gshared)(__this, ___size0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>::CopyToCheck(System.Array,System.Int32)
extern "C"  void Dictionary_2_CopyToCheck_m2290853314_gshared (Dictionary_2_t4167966349 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m2290853314(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t4167966349 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m2290853314_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>::make_pair(TKey,TValue)
extern "C"  KeyValuePair_2_t4066747055  Dictionary_2_make_pair_m1125136654_gshared (Il2CppObject * __this /* static, unused */, uint32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_make_pair_m1125136654(__this /* static, unused */, ___key0, ___value1, method) ((  KeyValuePair_2_t4066747055  (*) (Il2CppObject * /* static, unused */, uint32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_make_pair_m1125136654_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TKey System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>::pick_key(TKey,TValue)
extern "C"  uint32_t Dictionary_2_pick_key_m1245782312_gshared (Il2CppObject * __this /* static, unused */, uint32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_pick_key_m1245782312(__this /* static, unused */, ___key0, ___value1, method) ((  uint32_t (*) (Il2CppObject * /* static, unused */, uint32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_pick_key_m1245782312_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TValue System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>::pick_value(TKey,TValue)
extern "C"  Il2CppObject * Dictionary_2_pick_value_m3835972072_gshared (Il2CppObject * __this /* static, unused */, uint32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_pick_value_m3835972072(__this /* static, unused */, ___key0, ___value1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, uint32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_pick_value_m3835972072_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_CopyTo_m2346653645_gshared (Dictionary_2_t4167966349 * __this, KeyValuePair_2U5BU5D_t2966088118* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyTo_m2346653645(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t4167966349 *, KeyValuePair_2U5BU5D_t2966088118*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m2346653645_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>::Resize()
extern "C"  void Dictionary_2_Resize_m1540585279_gshared (Dictionary_2_t4167966349 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m1540585279(__this, method) ((  void (*) (Dictionary_2_t4167966349 *, const MethodInfo*))Dictionary_2_Resize_m1540585279_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>::Add(TKey,TValue)
extern "C"  void Dictionary_2_Add_m457755836_gshared (Dictionary_2_t4167966349 * __this, uint32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_Add_m457755836(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t4167966349 *, uint32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_Add_m457755836_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>::Clear()
extern "C"  void Dictionary_2_Clear_m4104237124_gshared (Dictionary_2_t4167966349 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m4104237124(__this, method) ((  void (*) (Dictionary_2_t4167966349 *, const MethodInfo*))Dictionary_2_Clear_m4104237124_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>::ContainsKey(TKey)
extern "C"  bool Dictionary_2_ContainsKey_m2033868842_gshared (Dictionary_2_t4167966349 * __this, uint32_t ___key0, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m2033868842(__this, ___key0, method) ((  bool (*) (Dictionary_2_t4167966349 *, uint32_t, const MethodInfo*))Dictionary_2_ContainsKey_m2033868842_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>::ContainsValue(TValue)
extern "C"  bool Dictionary_2_ContainsValue_m2343931690_gshared (Dictionary_2_t4167966349 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m2343931690(__this, ___value0, method) ((  bool (*) (Dictionary_2_t4167966349 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ContainsValue_m2343931690_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2_GetObjectData_m3819277111_gshared (Dictionary_2_t4167966349 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m3819277111(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t4167966349 *, SerializationInfo_t2185721892 *, StreamingContext_t2761351129 , const MethodInfo*))Dictionary_2_GetObjectData_m3819277111_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>::OnDeserialization(System.Object)
extern "C"  void Dictionary_2_OnDeserialization_m3116010381_gshared (Dictionary_2_t4167966349 * __this, Il2CppObject * ___sender0, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m3116010381(__this, ___sender0, method) ((  void (*) (Dictionary_2_t4167966349 *, Il2CppObject *, const MethodInfo*))Dictionary_2_OnDeserialization_m3116010381_gshared)(__this, ___sender0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>::Remove(TKey)
extern "C"  bool Dictionary_2_Remove_m3414222470_gshared (Dictionary_2_t4167966349 * __this, uint32_t ___key0, const MethodInfo* method);
#define Dictionary_2_Remove_m3414222470(__this, ___key0, method) ((  bool (*) (Dictionary_2_t4167966349 *, uint32_t, const MethodInfo*))Dictionary_2_Remove_m3414222470_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>::TryGetValue(TKey,TValue&)
extern "C"  bool Dictionary_2_TryGetValue_m2879450755_gshared (Dictionary_2_t4167966349 * __this, uint32_t ___key0, Il2CppObject ** ___value1, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m2879450755(__this, ___key0, ___value1, method) ((  bool (*) (Dictionary_2_t4167966349 *, uint32_t, Il2CppObject **, const MethodInfo*))Dictionary_2_TryGetValue_m2879450755_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>::get_Keys()
extern "C"  KeyCollection_t1499758504 * Dictionary_2_get_Keys_m3416980060_gshared (Dictionary_2_t4167966349 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m3416980060(__this, method) ((  KeyCollection_t1499758504 * (*) (Dictionary_2_t4167966349 *, const MethodInfo*))Dictionary_2_get_Keys_m3416980060_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>::get_Values()
extern "C"  ValueCollection_t2868572062 * Dictionary_2_get_Values_m3329104860_gshared (Dictionary_2_t4167966349 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m3329104860(__this, method) ((  ValueCollection_t2868572062 * (*) (Dictionary_2_t4167966349 *, const MethodInfo*))Dictionary_2_get_Values_m3329104860_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>::ToTKey(System.Object)
extern "C"  uint32_t Dictionary_2_ToTKey_m695641219_gshared (Dictionary_2_t4167966349 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_ToTKey_m695641219(__this, ___key0, method) ((  uint32_t (*) (Dictionary_2_t4167966349 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTKey_m695641219_gshared)(__this, ___key0, method)
// TValue System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>::ToTValue(System.Object)
extern "C"  Il2CppObject * Dictionary_2_ToTValue_m2148286211_gshared (Dictionary_2_t4167966349 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ToTValue_m2148286211(__this, ___value0, method) ((  Il2CppObject * (*) (Dictionary_2_t4167966349 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTValue_m2148286211_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_ContainsKeyValuePair_m3316311145_gshared (Dictionary_2_t4167966349 * __this, KeyValuePair_2_t4066747055  ___pair0, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m3316311145(__this, ___pair0, method) ((  bool (*) (Dictionary_2_t4167966349 *, KeyValuePair_2_t4066747055 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m3316311145_gshared)(__this, ___pair0, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>::GetEnumerator()
extern "C"  Enumerator_t1190322445  Dictionary_2_GetEnumerator_m1351811870_gshared (Dictionary_2_t4167966349 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m1351811870(__this, method) ((  Enumerator_t1190322445  (*) (Dictionary_2_t4167966349 *, const MethodInfo*))Dictionary_2_GetEnumerator_m1351811870_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>::<CopyTo>m__0(TKey,TValue)
extern "C"  DictionaryEntry_t1751606614  Dictionary_2_U3CCopyToU3Em__0_m327585261_gshared (Il2CppObject * __this /* static, unused */, uint32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m327585261(__this /* static, unused */, ___key0, ___value1, method) ((  DictionaryEntry_t1751606614  (*) (Il2CppObject * /* static, unused */, uint32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m327585261_gshared)(__this /* static, unused */, ___key0, ___value1, method)
