﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.AstarEnumFlagAttribute
struct AstarEnumFlagAttribute_t3239786002;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void Pathfinding.AstarEnumFlagAttribute::.ctor()
extern "C"  void AstarEnumFlagAttribute__ctor_m4023305877 (AstarEnumFlagAttribute_t3239786002 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.AstarEnumFlagAttribute::.ctor(System.String)
extern "C"  void AstarEnumFlagAttribute__ctor_m2237330061 (AstarEnumFlagAttribute_t3239786002 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
