﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_laswhisbo74
struct  M_laswhisbo74_t179941123  : public Il2CppObject
{
public:
	// System.Int32 GarbageiOS.M_laswhisbo74::_rase
	int32_t ____rase_0;
	// System.UInt32 GarbageiOS.M_laswhisbo74::_kecawMoche
	uint32_t ____kecawMoche_1;
	// System.Boolean GarbageiOS.M_laswhisbo74::_saslerzuStezis
	bool ____saslerzuStezis_2;
	// System.Boolean GarbageiOS.M_laswhisbo74::_mirteraCepaysea
	bool ____mirteraCepaysea_3;
	// System.Int32 GarbageiOS.M_laswhisbo74::_pechouhere
	int32_t ____pechouhere_4;
	// System.UInt32 GarbageiOS.M_laswhisbo74::_mesemHafur
	uint32_t ____mesemHafur_5;
	// System.UInt32 GarbageiOS.M_laswhisbo74::_narmuCereyasdrou
	uint32_t ____narmuCereyasdrou_6;
	// System.String GarbageiOS.M_laswhisbo74::_soudeSesay
	String_t* ____soudeSesay_7;
	// System.Boolean GarbageiOS.M_laswhisbo74::_sabouWelselma
	bool ____sabouWelselma_8;
	// System.Boolean GarbageiOS.M_laswhisbo74::_tesihall
	bool ____tesihall_9;
	// System.Int32 GarbageiOS.M_laswhisbo74::_seceeste
	int32_t ____seceeste_10;
	// System.UInt32 GarbageiOS.M_laswhisbo74::_tepeteeKaltay
	uint32_t ____tepeteeKaltay_11;
	// System.Single GarbageiOS.M_laswhisbo74::_lawteeNexea
	float ____lawteeNexea_12;
	// System.Boolean GarbageiOS.M_laswhisbo74::_koulamaBigo
	bool ____koulamaBigo_13;
	// System.Boolean GarbageiOS.M_laswhisbo74::_sapee
	bool ____sapee_14;
	// System.Int32 GarbageiOS.M_laswhisbo74::_firdraiYaytrosir
	int32_t ____firdraiYaytrosir_15;

public:
	inline static int32_t get_offset_of__rase_0() { return static_cast<int32_t>(offsetof(M_laswhisbo74_t179941123, ____rase_0)); }
	inline int32_t get__rase_0() const { return ____rase_0; }
	inline int32_t* get_address_of__rase_0() { return &____rase_0; }
	inline void set__rase_0(int32_t value)
	{
		____rase_0 = value;
	}

	inline static int32_t get_offset_of__kecawMoche_1() { return static_cast<int32_t>(offsetof(M_laswhisbo74_t179941123, ____kecawMoche_1)); }
	inline uint32_t get__kecawMoche_1() const { return ____kecawMoche_1; }
	inline uint32_t* get_address_of__kecawMoche_1() { return &____kecawMoche_1; }
	inline void set__kecawMoche_1(uint32_t value)
	{
		____kecawMoche_1 = value;
	}

	inline static int32_t get_offset_of__saslerzuStezis_2() { return static_cast<int32_t>(offsetof(M_laswhisbo74_t179941123, ____saslerzuStezis_2)); }
	inline bool get__saslerzuStezis_2() const { return ____saslerzuStezis_2; }
	inline bool* get_address_of__saslerzuStezis_2() { return &____saslerzuStezis_2; }
	inline void set__saslerzuStezis_2(bool value)
	{
		____saslerzuStezis_2 = value;
	}

	inline static int32_t get_offset_of__mirteraCepaysea_3() { return static_cast<int32_t>(offsetof(M_laswhisbo74_t179941123, ____mirteraCepaysea_3)); }
	inline bool get__mirteraCepaysea_3() const { return ____mirteraCepaysea_3; }
	inline bool* get_address_of__mirteraCepaysea_3() { return &____mirteraCepaysea_3; }
	inline void set__mirteraCepaysea_3(bool value)
	{
		____mirteraCepaysea_3 = value;
	}

	inline static int32_t get_offset_of__pechouhere_4() { return static_cast<int32_t>(offsetof(M_laswhisbo74_t179941123, ____pechouhere_4)); }
	inline int32_t get__pechouhere_4() const { return ____pechouhere_4; }
	inline int32_t* get_address_of__pechouhere_4() { return &____pechouhere_4; }
	inline void set__pechouhere_4(int32_t value)
	{
		____pechouhere_4 = value;
	}

	inline static int32_t get_offset_of__mesemHafur_5() { return static_cast<int32_t>(offsetof(M_laswhisbo74_t179941123, ____mesemHafur_5)); }
	inline uint32_t get__mesemHafur_5() const { return ____mesemHafur_5; }
	inline uint32_t* get_address_of__mesemHafur_5() { return &____mesemHafur_5; }
	inline void set__mesemHafur_5(uint32_t value)
	{
		____mesemHafur_5 = value;
	}

	inline static int32_t get_offset_of__narmuCereyasdrou_6() { return static_cast<int32_t>(offsetof(M_laswhisbo74_t179941123, ____narmuCereyasdrou_6)); }
	inline uint32_t get__narmuCereyasdrou_6() const { return ____narmuCereyasdrou_6; }
	inline uint32_t* get_address_of__narmuCereyasdrou_6() { return &____narmuCereyasdrou_6; }
	inline void set__narmuCereyasdrou_6(uint32_t value)
	{
		____narmuCereyasdrou_6 = value;
	}

	inline static int32_t get_offset_of__soudeSesay_7() { return static_cast<int32_t>(offsetof(M_laswhisbo74_t179941123, ____soudeSesay_7)); }
	inline String_t* get__soudeSesay_7() const { return ____soudeSesay_7; }
	inline String_t** get_address_of__soudeSesay_7() { return &____soudeSesay_7; }
	inline void set__soudeSesay_7(String_t* value)
	{
		____soudeSesay_7 = value;
		Il2CppCodeGenWriteBarrier(&____soudeSesay_7, value);
	}

	inline static int32_t get_offset_of__sabouWelselma_8() { return static_cast<int32_t>(offsetof(M_laswhisbo74_t179941123, ____sabouWelselma_8)); }
	inline bool get__sabouWelselma_8() const { return ____sabouWelselma_8; }
	inline bool* get_address_of__sabouWelselma_8() { return &____sabouWelselma_8; }
	inline void set__sabouWelselma_8(bool value)
	{
		____sabouWelselma_8 = value;
	}

	inline static int32_t get_offset_of__tesihall_9() { return static_cast<int32_t>(offsetof(M_laswhisbo74_t179941123, ____tesihall_9)); }
	inline bool get__tesihall_9() const { return ____tesihall_9; }
	inline bool* get_address_of__tesihall_9() { return &____tesihall_9; }
	inline void set__tesihall_9(bool value)
	{
		____tesihall_9 = value;
	}

	inline static int32_t get_offset_of__seceeste_10() { return static_cast<int32_t>(offsetof(M_laswhisbo74_t179941123, ____seceeste_10)); }
	inline int32_t get__seceeste_10() const { return ____seceeste_10; }
	inline int32_t* get_address_of__seceeste_10() { return &____seceeste_10; }
	inline void set__seceeste_10(int32_t value)
	{
		____seceeste_10 = value;
	}

	inline static int32_t get_offset_of__tepeteeKaltay_11() { return static_cast<int32_t>(offsetof(M_laswhisbo74_t179941123, ____tepeteeKaltay_11)); }
	inline uint32_t get__tepeteeKaltay_11() const { return ____tepeteeKaltay_11; }
	inline uint32_t* get_address_of__tepeteeKaltay_11() { return &____tepeteeKaltay_11; }
	inline void set__tepeteeKaltay_11(uint32_t value)
	{
		____tepeteeKaltay_11 = value;
	}

	inline static int32_t get_offset_of__lawteeNexea_12() { return static_cast<int32_t>(offsetof(M_laswhisbo74_t179941123, ____lawteeNexea_12)); }
	inline float get__lawteeNexea_12() const { return ____lawteeNexea_12; }
	inline float* get_address_of__lawteeNexea_12() { return &____lawteeNexea_12; }
	inline void set__lawteeNexea_12(float value)
	{
		____lawteeNexea_12 = value;
	}

	inline static int32_t get_offset_of__koulamaBigo_13() { return static_cast<int32_t>(offsetof(M_laswhisbo74_t179941123, ____koulamaBigo_13)); }
	inline bool get__koulamaBigo_13() const { return ____koulamaBigo_13; }
	inline bool* get_address_of__koulamaBigo_13() { return &____koulamaBigo_13; }
	inline void set__koulamaBigo_13(bool value)
	{
		____koulamaBigo_13 = value;
	}

	inline static int32_t get_offset_of__sapee_14() { return static_cast<int32_t>(offsetof(M_laswhisbo74_t179941123, ____sapee_14)); }
	inline bool get__sapee_14() const { return ____sapee_14; }
	inline bool* get_address_of__sapee_14() { return &____sapee_14; }
	inline void set__sapee_14(bool value)
	{
		____sapee_14 = value;
	}

	inline static int32_t get_offset_of__firdraiYaytrosir_15() { return static_cast<int32_t>(offsetof(M_laswhisbo74_t179941123, ____firdraiYaytrosir_15)); }
	inline int32_t get__firdraiYaytrosir_15() const { return ____firdraiYaytrosir_15; }
	inline int32_t* get_address_of__firdraiYaytrosir_15() { return &____firdraiYaytrosir_15; }
	inline void set__firdraiYaytrosir_15(int32_t value)
	{
		____firdraiYaytrosir_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
