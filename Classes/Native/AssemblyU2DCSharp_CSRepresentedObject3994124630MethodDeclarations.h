﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CSRepresentedObject
struct CSRepresentedObject_t3994124630;
// System.Action
struct Action_t3771233898;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CSRepresentedObject3994124630.h"

// System.Void CSRepresentedObject::.ctor(System.Int32,System.Boolean)
extern "C"  void CSRepresentedObject__ctor_m900424215 (CSRepresentedObject_t3994124630 * __this, int32_t ___jsObjID0, bool ___bFunction1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSRepresentedObject::.cctor()
extern "C"  void CSRepresentedObject__cctor_m346059640 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSRepresentedObject::Finalize()
extern "C"  void CSRepresentedObject_Finalize_m1391329645 (CSRepresentedObject_t3994124630 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Action CSRepresentedObject::CreateDestructAction(System.Boolean,System.Int32,System.Int32)
extern "C"  Action_t3771233898 * CSRepresentedObject_CreateDestructAction_m2066093840 (CSRepresentedObject_t3994124630 * __this, bool ___bFunction0, int32_t ___jsObjID1, int32_t ___round2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CSRepresentedObject::get_jsobjID()
extern "C"  int32_t CSRepresentedObject_get_jsobjID_m2070218209 (CSRepresentedObject_t3994124630 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Action CSRepresentedObject::ilo_CreateDestructAction1(CSRepresentedObject,System.Boolean,System.Int32,System.Int32)
extern "C"  Action_t3771233898 * CSRepresentedObject_ilo_CreateDestructAction1_m2346540712 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ____this0, bool ___bFunction1, int32_t ___jsObjID2, int32_t ___round3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
