﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FPSCounter
struct FPSCounter_t1808341236;

#include "codegen/il2cpp-codegen.h"

// System.Void FPSCounter::.ctor()
extern "C"  void FPSCounter__ctor_m269244989 (FPSCounter_t1808341236 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FPSCounter::Awake()
extern "C"  void FPSCounter_Awake_m506850208 (FPSCounter_t1808341236 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FPSCounter::OnGUI()
extern "C"  void FPSCounter_OnGUI_m4059610935 (FPSCounter_t1808341236 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FPSCounter::Start()
extern "C"  void FPSCounter_Start_m3511350077 (FPSCounter_t1808341236 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FPSCounter::Update()
extern "C"  void FPSCounter_Update_m1483522160 (FPSCounter_t1808341236 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FPSCounter::Main()
extern "C"  void FPSCounter_Main_m4219146976 (FPSCounter_t1808341236 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
