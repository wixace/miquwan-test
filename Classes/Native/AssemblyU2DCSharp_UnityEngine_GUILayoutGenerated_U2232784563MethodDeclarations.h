﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_GUILayoutGenerated/<GUILayout_Window_GetDelegate_member101_arg2>c__AnonStoreyF3
struct U3CGUILayout_Window_GetDelegate_member101_arg2U3Ec__AnonStoreyF3_t2232784563;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine_GUILayoutGenerated/<GUILayout_Window_GetDelegate_member101_arg2>c__AnonStoreyF3::.ctor()
extern "C"  void U3CGUILayout_Window_GetDelegate_member101_arg2U3Ec__AnonStoreyF3__ctor_m781889048 (U3CGUILayout_Window_GetDelegate_member101_arg2U3Ec__AnonStoreyF3_t2232784563 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUILayoutGenerated/<GUILayout_Window_GetDelegate_member101_arg2>c__AnonStoreyF3::<>m__236(System.Int32)
extern "C"  void U3CGUILayout_Window_GetDelegate_member101_arg2U3Ec__AnonStoreyF3_U3CU3Em__236_m364730439 (U3CGUILayout_Window_GetDelegate_member101_arg2U3Ec__AnonStoreyF3_t2232784563 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
