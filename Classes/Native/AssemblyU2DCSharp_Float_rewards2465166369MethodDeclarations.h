﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Float_rewards
struct Float_rewards_t2465166369;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// FloatTextUnit
struct FloatTextUnit_t2362298029;
// EventDelegate/Callback
struct Callback_t1094463061;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_FLOAT_TEXT_ID2199345770.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "AssemblyU2DCSharp_FloatTextUnit2362298029.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_EventDelegate_Callback1094463061.h"
#include "AssemblyU2DCSharp_UITweener_Method1078127180.h"

// System.Void Float_rewards::.ctor()
extern "C"  void Float_rewards__ctor_m4117037674 (Float_rewards_t2465166369 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FLOAT_TEXT_ID Float_rewards::FloatID()
extern "C"  int32_t Float_rewards_FloatID_m345521448 (Float_rewards_t2465166369 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_rewards::OnAwake(UnityEngine.GameObject)
extern "C"  void Float_rewards_OnAwake_m152094758 (Float_rewards_t2465166369 * __this, GameObject_t3674682005 * ___viewGO0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_rewards::OnDestroy()
extern "C"  void Float_rewards_OnDestroy_m1824232035 (Float_rewards_t2465166369 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_rewards::Init()
extern "C"  void Float_rewards_Init_m2439648938 (Float_rewards_t2465166369 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_rewards::SetFile(System.Object[])
extern "C"  void Float_rewards_SetFile_m639860652 (Float_rewards_t2465166369 * __this, ObjectU5BU5D_t1108656482* ___args0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_rewards::ilo_OnAwake1(FloatTextUnit,UnityEngine.GameObject)
extern "C"  void Float_rewards_ilo_OnAwake1_m3885106065 (Il2CppObject * __this /* static, unused */, FloatTextUnit_t2362298029 * ____this0, GameObject_t3674682005 * ___viewGO1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_rewards::ilo_OnDestroy2(FloatTextUnit)
extern "C"  void Float_rewards_ilo_OnDestroy2_m4176582241 (Il2CppObject * __this /* static, unused */, FloatTextUnit_t2362298029 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_rewards::ilo_MovePosition3(FloatTextUnit,UnityEngine.Vector3,System.Single,System.Single,System.Single,EventDelegate/Callback,UnityEngine.GameObject,UITweener/Method)
extern "C"  void Float_rewards_ilo_MovePosition3_m3657618708 (Il2CppObject * __this /* static, unused */, FloatTextUnit_t2362298029 * ____this0, Vector3_t4282066566  ___from1, float ___toUp2, float ___duration3, float ___delay4, Callback_t1094463061 * ___call5, GameObject_t3674682005 * ___go6, int32_t ___method7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
