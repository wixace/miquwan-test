﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// JJCCoefficientCfg
struct JJCCoefficientCfg_t2755051730;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;

#include "codegen/il2cpp-codegen.h"

// System.Void JJCCoefficientCfg::.ctor()
extern "C"  void JJCCoefficientCfg__ctor_m3379804377 (JJCCoefficientCfg_t2755051730 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JJCCoefficientCfg::Init(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C"  void JJCCoefficientCfg_Init_m1279859724 (JJCCoefficientCfg_t2755051730 * __this, Dictionary_2_t827649927 * ____info0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
