﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,ProductsCfgMgr/ProductInfo,ProductsCfgMgr/ProductInfo>
struct Transform_1_t3750066325;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "AssemblyU2DCSharp_ProductsCfgMgr_ProductInfo1305991238.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,ProductsCfgMgr/ProductInfo,ProductsCfgMgr/ProductInfo>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m1888013799_gshared (Transform_1_t3750066325 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define Transform_1__ctor_m1888013799(__this, ___object0, ___method1, method) ((  void (*) (Transform_1_t3750066325 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m1888013799_gshared)(__this, ___object0, ___method1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,ProductsCfgMgr/ProductInfo,ProductsCfgMgr/ProductInfo>::Invoke(TKey,TValue)
extern "C"  ProductInfo_t1305991238  Transform_1_Invoke_m1325702581_gshared (Transform_1_t3750066325 * __this, Il2CppObject * ___key0, ProductInfo_t1305991238  ___value1, const MethodInfo* method);
#define Transform_1_Invoke_m1325702581(__this, ___key0, ___value1, method) ((  ProductInfo_t1305991238  (*) (Transform_1_t3750066325 *, Il2CppObject *, ProductInfo_t1305991238 , const MethodInfo*))Transform_1_Invoke_m1325702581_gshared)(__this, ___key0, ___value1, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,ProductsCfgMgr/ProductInfo,ProductsCfgMgr/ProductInfo>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m2753670292_gshared (Transform_1_t3750066325 * __this, Il2CppObject * ___key0, ProductInfo_t1305991238  ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method);
#define Transform_1_BeginInvoke_m2753670292(__this, ___key0, ___value1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Transform_1_t3750066325 *, Il2CppObject *, ProductInfo_t1305991238 , AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))Transform_1_BeginInvoke_m2753670292_gshared)(__this, ___key0, ___value1, ___callback2, ___object3, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,ProductsCfgMgr/ProductInfo,ProductsCfgMgr/ProductInfo>::EndInvoke(System.IAsyncResult)
extern "C"  ProductInfo_t1305991238  Transform_1_EndInvoke_m1412157365_gshared (Transform_1_t3750066325 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define Transform_1_EndInvoke_m1412157365(__this, ___result0, method) ((  ProductInfo_t1305991238  (*) (Transform_1_t3750066325 *, Il2CppObject *, const MethodInfo*))Transform_1_EndInvoke_m1412157365_gshared)(__this, ___result0, method)
