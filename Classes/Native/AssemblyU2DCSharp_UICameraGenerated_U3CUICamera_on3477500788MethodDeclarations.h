﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UICameraGenerated/<UICamera_onTooltip_GetDelegate_member55_arg0>c__AnonStoreyB0
struct U3CUICamera_onTooltip_GetDelegate_member55_arg0U3Ec__AnonStoreyB0_t3477500788;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"

// System.Void UICameraGenerated/<UICamera_onTooltip_GetDelegate_member55_arg0>c__AnonStoreyB0::.ctor()
extern "C"  void U3CUICamera_onTooltip_GetDelegate_member55_arg0U3Ec__AnonStoreyB0__ctor_m3981358711 (U3CUICamera_onTooltip_GetDelegate_member55_arg0U3Ec__AnonStoreyB0_t3477500788 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICameraGenerated/<UICamera_onTooltip_GetDelegate_member55_arg0>c__AnonStoreyB0::<>m__124(UnityEngine.GameObject,System.Boolean)
extern "C"  void U3CUICamera_onTooltip_GetDelegate_member55_arg0U3Ec__AnonStoreyB0_U3CU3Em__124_m778649168 (U3CUICamera_onTooltip_GetDelegate_member55_arg0U3Ec__AnonStoreyB0_t3477500788 * __this, GameObject_t3674682005 * ___go0, bool ___state1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
