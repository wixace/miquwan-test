﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BehaviourUtil/<DelayCallExec>c__Iterator42`4<System.Object,System.Int32,System.Object,System.Single>
struct U3CDelayCallExecU3Ec__Iterator42_4_t1747052450;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void BehaviourUtil/<DelayCallExec>c__Iterator42`4<System.Object,System.Int32,System.Object,System.Single>::.ctor()
extern "C"  void U3CDelayCallExecU3Ec__Iterator42_4__ctor_m2276885432_gshared (U3CDelayCallExecU3Ec__Iterator42_4_t1747052450 * __this, const MethodInfo* method);
#define U3CDelayCallExecU3Ec__Iterator42_4__ctor_m2276885432(__this, method) ((  void (*) (U3CDelayCallExecU3Ec__Iterator42_4_t1747052450 *, const MethodInfo*))U3CDelayCallExecU3Ec__Iterator42_4__ctor_m2276885432_gshared)(__this, method)
// System.Object BehaviourUtil/<DelayCallExec>c__Iterator42`4<System.Object,System.Int32,System.Object,System.Single>::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDelayCallExecU3Ec__Iterator42_4_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3907800740_gshared (U3CDelayCallExecU3Ec__Iterator42_4_t1747052450 * __this, const MethodInfo* method);
#define U3CDelayCallExecU3Ec__Iterator42_4_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3907800740(__this, method) ((  Il2CppObject * (*) (U3CDelayCallExecU3Ec__Iterator42_4_t1747052450 *, const MethodInfo*))U3CDelayCallExecU3Ec__Iterator42_4_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3907800740_gshared)(__this, method)
// System.Object BehaviourUtil/<DelayCallExec>c__Iterator42`4<System.Object,System.Int32,System.Object,System.Single>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDelayCallExecU3Ec__Iterator42_4_System_Collections_IEnumerator_get_Current_m3426228792_gshared (U3CDelayCallExecU3Ec__Iterator42_4_t1747052450 * __this, const MethodInfo* method);
#define U3CDelayCallExecU3Ec__Iterator42_4_System_Collections_IEnumerator_get_Current_m3426228792(__this, method) ((  Il2CppObject * (*) (U3CDelayCallExecU3Ec__Iterator42_4_t1747052450 *, const MethodInfo*))U3CDelayCallExecU3Ec__Iterator42_4_System_Collections_IEnumerator_get_Current_m3426228792_gshared)(__this, method)
// System.Boolean BehaviourUtil/<DelayCallExec>c__Iterator42`4<System.Object,System.Int32,System.Object,System.Single>::MoveNext()
extern "C"  bool U3CDelayCallExecU3Ec__Iterator42_4_MoveNext_m3672739108_gshared (U3CDelayCallExecU3Ec__Iterator42_4_t1747052450 * __this, const MethodInfo* method);
#define U3CDelayCallExecU3Ec__Iterator42_4_MoveNext_m3672739108(__this, method) ((  bool (*) (U3CDelayCallExecU3Ec__Iterator42_4_t1747052450 *, const MethodInfo*))U3CDelayCallExecU3Ec__Iterator42_4_MoveNext_m3672739108_gshared)(__this, method)
// System.Void BehaviourUtil/<DelayCallExec>c__Iterator42`4<System.Object,System.Int32,System.Object,System.Single>::Dispose()
extern "C"  void U3CDelayCallExecU3Ec__Iterator42_4_Dispose_m1846829557_gshared (U3CDelayCallExecU3Ec__Iterator42_4_t1747052450 * __this, const MethodInfo* method);
#define U3CDelayCallExecU3Ec__Iterator42_4_Dispose_m1846829557(__this, method) ((  void (*) (U3CDelayCallExecU3Ec__Iterator42_4_t1747052450 *, const MethodInfo*))U3CDelayCallExecU3Ec__Iterator42_4_Dispose_m1846829557_gshared)(__this, method)
// System.Void BehaviourUtil/<DelayCallExec>c__Iterator42`4<System.Object,System.Int32,System.Object,System.Single>::Reset()
extern "C"  void U3CDelayCallExecU3Ec__Iterator42_4_Reset_m4218285669_gshared (U3CDelayCallExecU3Ec__Iterator42_4_t1747052450 * __this, const MethodInfo* method);
#define U3CDelayCallExecU3Ec__Iterator42_4_Reset_m4218285669(__this, method) ((  void (*) (U3CDelayCallExecU3Ec__Iterator42_4_t1747052450 *, const MethodInfo*))U3CDelayCallExecU3Ec__Iterator42_4_Reset_m4218285669_gshared)(__this, method)
