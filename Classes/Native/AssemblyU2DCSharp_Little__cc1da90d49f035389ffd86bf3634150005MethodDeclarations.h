﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._cc1da90d49f035389ffd86bf263ae690
struct _cc1da90d49f035389ffd86bf263ae690_t3634150005;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._cc1da90d49f035389ffd86bf263ae690::.ctor()
extern "C"  void _cc1da90d49f035389ffd86bf263ae690__ctor_m3563609944 (_cc1da90d49f035389ffd86bf263ae690_t3634150005 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._cc1da90d49f035389ffd86bf263ae690::_cc1da90d49f035389ffd86bf263ae690m2(System.Int32)
extern "C"  int32_t _cc1da90d49f035389ffd86bf263ae690__cc1da90d49f035389ffd86bf263ae690m2_m2685785465 (_cc1da90d49f035389ffd86bf263ae690_t3634150005 * __this, int32_t ____cc1da90d49f035389ffd86bf263ae690a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._cc1da90d49f035389ffd86bf263ae690::_cc1da90d49f035389ffd86bf263ae690m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _cc1da90d49f035389ffd86bf263ae690__cc1da90d49f035389ffd86bf263ae690m_m661187933 (_cc1da90d49f035389ffd86bf263ae690_t3634150005 * __this, int32_t ____cc1da90d49f035389ffd86bf263ae690a0, int32_t ____cc1da90d49f035389ffd86bf263ae690771, int32_t ____cc1da90d49f035389ffd86bf263ae690c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
