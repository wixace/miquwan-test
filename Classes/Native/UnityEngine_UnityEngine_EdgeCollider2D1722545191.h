﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_Collider2D1552025098.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EdgeCollider2D
struct  EdgeCollider2D_t1722545191  : public Collider2D_t1552025098
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
