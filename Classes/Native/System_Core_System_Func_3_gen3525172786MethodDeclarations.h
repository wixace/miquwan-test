﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Func_3_gen3956694567MethodDeclarations.h"

// System.Void System.Func`3<System.Type,System.Reflection.MethodInfo,System.Reflection.MethodInfo>::.ctor(System.Object,System.IntPtr)
#define Func_3__ctor_m2548455442(__this, ___object0, ___method1, method) ((  void (*) (Func_3_t3525172786 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_3__ctor_m1533793193_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`3<System.Type,System.Reflection.MethodInfo,System.Reflection.MethodInfo>::Invoke(T1,T2)
#define Func_3_Invoke_m2049450215(__this, ___arg10, ___arg21, method) ((  MethodInfo_t * (*) (Func_3_t3525172786 *, Type_t *, MethodInfo_t *, const MethodInfo*))Func_3_Invoke_m4101486064_gshared)(__this, ___arg10, ___arg21, method)
// System.IAsyncResult System.Func`3<System.Type,System.Reflection.MethodInfo,System.Reflection.MethodInfo>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
#define Func_3_BeginInvoke_m1813110760(__this, ___arg10, ___arg21, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Func_3_t3525172786 *, Type_t *, MethodInfo_t *, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))Func_3_BeginInvoke_m69685489_gshared)(__this, ___arg10, ___arg21, ___callback2, ___object3, method)
// TResult System.Func`3<System.Type,System.Reflection.MethodInfo,System.Reflection.MethodInfo>::EndInvoke(System.IAsyncResult)
#define Func_3_EndInvoke_m4015816916(__this, ___result0, method) ((  MethodInfo_t * (*) (Func_3_t3525172786 *, Il2CppObject *, const MethodInfo*))Func_3_EndInvoke_m1319796459_gshared)(__this, ___result0, method)
