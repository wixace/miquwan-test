﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.LocalAvoidance/IntersectionPair>
struct ReadOnlyCollection_1_t83430159;
// System.Collections.Generic.IList`1<Pathfinding.LocalAvoidance/IntersectionPair>
struct IList_1_t1220999826;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// Pathfinding.LocalAvoidance/IntersectionPair[]
struct IntersectionPairU5BU5D_t3912429174;
// System.Collections.Generic.IEnumerator`1<Pathfinding.LocalAvoidance/IntersectionPair>
struct IEnumerator_1_t438217672;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_LocalAvoidance_Inter2821319919.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.LocalAvoidance/IntersectionPair>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C"  void ReadOnlyCollection_1__ctor_m514447265_gshared (ReadOnlyCollection_1_t83430159 * __this, Il2CppObject* ___list0, const MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m514447265(__this, ___list0, method) ((  void (*) (ReadOnlyCollection_1_t83430159 *, Il2CppObject*, const MethodInfo*))ReadOnlyCollection_1__ctor_m514447265_gshared)(__this, ___list0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.LocalAvoidance/IntersectionPair>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1990676747_gshared (ReadOnlyCollection_1_t83430159 * __this, IntersectionPair_t2821319919  ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1990676747(__this, ___item0, method) ((  void (*) (ReadOnlyCollection_1_t83430159 *, IntersectionPair_t2821319919 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1990676747_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.LocalAvoidance/IntersectionPair>::System.Collections.Generic.ICollection<T>.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m3588128031_gshared (ReadOnlyCollection_1_t83430159 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m3588128031(__this, method) ((  void (*) (ReadOnlyCollection_1_t83430159 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m3588128031_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.LocalAvoidance/IntersectionPair>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m4123410994_gshared (ReadOnlyCollection_1_t83430159 * __this, int32_t ___index0, IntersectionPair_t2821319919  ___item1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m4123410994(__this, ___index0, ___item1, method) ((  void (*) (ReadOnlyCollection_1_t83430159 *, int32_t, IntersectionPair_t2821319919 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m4123410994_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.LocalAvoidance/IntersectionPair>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m3439222920_gshared (ReadOnlyCollection_1_t83430159 * __this, IntersectionPair_t2821319919  ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m3439222920(__this, ___item0, method) ((  bool (*) (ReadOnlyCollection_1_t83430159 *, IntersectionPair_t2821319919 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m3439222920_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.LocalAvoidance/IntersectionPair>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1997263864_gshared (ReadOnlyCollection_1_t83430159 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1997263864(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t83430159 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1997263864_gshared)(__this, ___index0, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.LocalAvoidance/IntersectionPair>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C"  IntersectionPair_t2821319919  ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m2822393852_gshared (ReadOnlyCollection_1_t83430159 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m2822393852(__this, ___index0, method) ((  IntersectionPair_t2821319919  (*) (ReadOnlyCollection_1_t83430159 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m2822393852_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.LocalAvoidance/IntersectionPair>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m152747657_gshared (ReadOnlyCollection_1_t83430159 * __this, int32_t ___index0, IntersectionPair_t2821319919  ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m152747657(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t83430159 *, int32_t, IntersectionPair_t2821319919 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m152747657_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.LocalAvoidance/IntersectionPair>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1185756167_gshared (ReadOnlyCollection_1_t83430159 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1185756167(__this, method) ((  bool (*) (ReadOnlyCollection_1_t83430159 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1185756167_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.LocalAvoidance/IntersectionPair>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m2932641744_gshared (ReadOnlyCollection_1_t83430159 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m2932641744(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t83430159 *, Il2CppArray *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m2932641744_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.LocalAvoidance/IntersectionPair>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m1244689611_gshared (ReadOnlyCollection_1_t83430159 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m1244689611(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t83430159 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m1244689611_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.LocalAvoidance/IntersectionPair>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m2282161158_gshared (ReadOnlyCollection_1_t83430159 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m2282161158(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t83430159 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m2282161158_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.LocalAvoidance/IntersectionPair>::System.Collections.IList.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Clear_m4075609822_gshared (ReadOnlyCollection_1_t83430159 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m4075609822(__this, method) ((  void (*) (ReadOnlyCollection_1_t83430159 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m4075609822_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.LocalAvoidance/IntersectionPair>::System.Collections.IList.Contains(System.Object)
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_Contains_m3720311686_gshared (ReadOnlyCollection_1_t83430159 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m3720311686(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t83430159 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m3720311686_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.LocalAvoidance/IntersectionPair>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m343525214_gshared (ReadOnlyCollection_1_t83430159 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m343525214(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t83430159 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m343525214_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.LocalAvoidance/IntersectionPair>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Insert_m786682505_gshared (ReadOnlyCollection_1_t83430159 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m786682505(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t83430159 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m786682505_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.LocalAvoidance/IntersectionPair>::System.Collections.IList.Remove(System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Remove_m3856624639_gshared (ReadOnlyCollection_1_t83430159 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m3856624639(__this, ___value0, method) ((  void (*) (ReadOnlyCollection_1_t83430159 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m3856624639_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.LocalAvoidance/IntersectionPair>::System.Collections.IList.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m881635225_gshared (ReadOnlyCollection_1_t83430159 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m881635225(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t83430159 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m881635225_gshared)(__this, ___index0, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.LocalAvoidance/IntersectionPair>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m169340438_gshared (ReadOnlyCollection_1_t83430159 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m169340438(__this, method) ((  bool (*) (ReadOnlyCollection_1_t83430159 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m169340438_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.LocalAvoidance/IntersectionPair>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m2001485442_gshared (ReadOnlyCollection_1_t83430159 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m2001485442(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t83430159 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m2001485442_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.LocalAvoidance/IntersectionPair>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m2939010933_gshared (ReadOnlyCollection_1_t83430159 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m2939010933(__this, method) ((  bool (*) (ReadOnlyCollection_1_t83430159 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m2939010933_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.LocalAvoidance/IntersectionPair>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m3181214948_gshared (ReadOnlyCollection_1_t83430159 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m3181214948(__this, method) ((  bool (*) (ReadOnlyCollection_1_t83430159 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m3181214948_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.LocalAvoidance/IntersectionPair>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IList_get_Item_m347772809_gshared (ReadOnlyCollection_1_t83430159 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m347772809(__this, ___index0, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t83430159 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m347772809_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.LocalAvoidance/IntersectionPair>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_set_Item_m701595680_gshared (ReadOnlyCollection_1_t83430159 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m701595680(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t83430159 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m701595680_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.LocalAvoidance/IntersectionPair>::Contains(T)
extern "C"  bool ReadOnlyCollection_1_Contains_m1020204493_gshared (ReadOnlyCollection_1_t83430159 * __this, IntersectionPair_t2821319919  ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m1020204493(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t83430159 *, IntersectionPair_t2821319919 , const MethodInfo*))ReadOnlyCollection_1_Contains_m1020204493_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.LocalAvoidance/IntersectionPair>::CopyTo(T[],System.Int32)
extern "C"  void ReadOnlyCollection_1_CopyTo_m1458656571_gshared (ReadOnlyCollection_1_t83430159 * __this, IntersectionPairU5BU5D_t3912429174* ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m1458656571(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t83430159 *, IntersectionPairU5BU5D_t3912429174*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m1458656571_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.LocalAvoidance/IntersectionPair>::GetEnumerator()
extern "C"  Il2CppObject* ReadOnlyCollection_1_GetEnumerator_m793321904_gshared (ReadOnlyCollection_1_t83430159 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m793321904(__this, method) ((  Il2CppObject* (*) (ReadOnlyCollection_1_t83430159 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m793321904_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.LocalAvoidance/IntersectionPair>::IndexOf(T)
extern "C"  int32_t ReadOnlyCollection_1_IndexOf_m3382947327_gshared (ReadOnlyCollection_1_t83430159 * __this, IntersectionPair_t2821319919  ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m3382947327(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t83430159 *, IntersectionPair_t2821319919 , const MethodInfo*))ReadOnlyCollection_1_IndexOf_m3382947327_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.LocalAvoidance/IntersectionPair>::get_Count()
extern "C"  int32_t ReadOnlyCollection_1_get_Count_m1609459548_gshared (ReadOnlyCollection_1_t83430159 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m1609459548(__this, method) ((  int32_t (*) (ReadOnlyCollection_1_t83430159 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m1609459548_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.LocalAvoidance/IntersectionPair>::get_Item(System.Int32)
extern "C"  IntersectionPair_t2821319919  ReadOnlyCollection_1_get_Item_m3359326140_gshared (ReadOnlyCollection_1_t83430159 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m3359326140(__this, ___index0, method) ((  IntersectionPair_t2821319919  (*) (ReadOnlyCollection_1_t83430159 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m3359326140_gshared)(__this, ___index0, method)
