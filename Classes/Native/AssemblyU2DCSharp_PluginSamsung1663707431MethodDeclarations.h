﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PluginSamsung
struct PluginSamsung_t1663707431;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// System.String
struct String_t;
// UnityEngine.WWWForm
struct WWWForm_t461342257;
// Mihua.SDK.PayInfo
struct PayInfo_t1775308120;
// CEvent.EventFunc`1<CEvent.ZEvent>
struct EventFunc_1_t1114914310;
// PluginsSdkMgr
struct PluginsSdkMgr_t3884624670;
// VersionMgr
struct VersionMgr_t1322950208;
// System.Action
struct Action_t3771233898;
// FloatTextMgr
struct FloatTextMgr_t630384591;
// Mihua.SDK.SdkwwwCallBack
struct SdkwwwCallBack_t3004559384;
// System.Object
struct Il2CppObject;
// Newtonsoft.Json.Linq.JToken
struct JToken_t3412245951;
// Newtonsoft.Json.Linq.JObject
struct JObject_t1798544199;
// VersionInfo
struct VersionInfo_t2356638086;
// UnityEngine.AndroidJavaObject
struct AndroidJavaObject_t2362096582;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_Mihua_SDK_PayInfo1775308120.h"
#include "AssemblyU2DCSharp_PluginSamsung1663707431.h"
#include "AssemblyU2DCSharp_VersionMgr1322950208.h"
#include "AssemblyU2DCSharp_PluginsSdkMgr3884624670.h"
#include "System_Core_System_Action3771233898.h"
#include "UnityEngine_UnityEngine_WWWForm461342257.h"
#include "AssemblyU2DCSharp_Mihua_SDK_SdkwwwCallBack3004559384.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JObject1798544199.h"

// System.Void PluginSamsung::.ctor()
extern "C"  void PluginSamsung__ctor_m1830720548 (PluginSamsung_t1663707431 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginSamsung::Init()
extern "C"  void PluginSamsung_Init_m1673160112 (PluginSamsung_t1663707431 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PluginSamsung::SendHttpLogin(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C"  Il2CppObject * PluginSamsung_SendHttpLogin_m2878123284 (PluginSamsung_t1663707431 * __this, Dictionary_2_t827649927 * ___parameters0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginSamsung::ReqSDKHttpLoginPre()
extern "C"  Dictionary_2_t827649927 * PluginSamsung_ReqSDKHttpLoginPre_m4019009072 (PluginSamsung_t1663707431 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginSamsung::ReqSDKHttpLogin()
extern "C"  Dictionary_2_t827649927 * PluginSamsung_ReqSDKHttpLogin_m1198330133 (PluginSamsung_t1663707431 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginSamsung::IsLoginSuccess(System.Boolean)
extern "C"  bool PluginSamsung_IsLoginSuccess_m2480158663 (PluginSamsung_t1663707431 * __this, bool ___isOpenUseLogin0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginSamsung::OpenUserLogin()
extern "C"  void PluginSamsung_OpenUserLogin_m312693366 (PluginSamsung_t1663707431 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginSamsung::UserPay(CEvent.ZEvent)
extern "C"  void PluginSamsung_UserPay_m394965052 (PluginSamsung_t1663707431 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginSamsung::SignCallBack(System.Boolean,System.String)
extern "C"  void PluginSamsung_SignCallBack_m3469701333 (PluginSamsung_t1663707431 * __this, bool ___success0, String_t* ___arg1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.WWWForm PluginSamsung::BuildOrderParam2WWWForm(Mihua.SDK.PayInfo)
extern "C"  WWWForm_t461342257 * PluginSamsung_BuildOrderParam2WWWForm_m916595519 (PluginSamsung_t1663707431 * __this, PayInfo_t1775308120 * ___info0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginSamsung::<OpenUserLogin>m__451()
extern "C"  void PluginSamsung_U3COpenUserLoginU3Em__451_m4189109547 (PluginSamsung_t1663707431 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginSamsung::ilo_AddEventListener1(System.String,CEvent.EventFunc`1<CEvent.ZEvent>)
extern "C"  void PluginSamsung_ilo_AddEventListener1_m4267141200 (Il2CppObject * __this /* static, unused */, String_t* ___eventType0, EventFunc_1_t1114914310 * ___fun1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginSamsung::ilo_ReqSDKHttpLoginPre2(PluginSamsung)
extern "C"  Dictionary_2_t827649927 * PluginSamsung_ilo_ReqSDKHttpLoginPre2_m4264370042 (Il2CppObject * __this /* static, unused */, PluginSamsung_t1663707431 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PluginsSdkMgr PluginSamsung::ilo_get_PluginsSdkMgr3()
extern "C"  PluginsSdkMgr_t3884624670 * PluginSamsung_ilo_get_PluginsSdkMgr3_m4176939964 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionMgr PluginSamsung::ilo_get_Instance4()
extern "C"  VersionMgr_t1322950208 * PluginSamsung_ilo_get_Instance4_m2049971778 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginSamsung::ilo_get_isSdkLogin5(VersionMgr)
extern "C"  bool PluginSamsung_ilo_get_isSdkLogin5_m2528101526 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PluginSamsung::ilo_get_DeviceID6(PluginsSdkMgr)
extern "C"  String_t* PluginSamsung_ilo_get_DeviceID6_m28551878 (Il2CppObject * __this /* static, unused */, PluginsSdkMgr_t3884624670 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 PluginSamsung::ilo_DelayCall7(System.Single,System.Action)
extern "C"  uint32_t PluginSamsung_ilo_DelayCall7_m2224552948 (Il2CppObject * __this /* static, unused */, float ___delaytime0, Action_t3771233898 * ___act1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FloatTextMgr PluginSamsung::ilo_get_FloatTextMgr8()
extern "C"  FloatTextMgr_t630384591 * PluginSamsung_ilo_get_FloatTextMgr8_m3114138941 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginSamsung::ilo_Request9(System.String,UnityEngine.WWWForm,Mihua.SDK.SdkwwwCallBack)
extern "C"  void PluginSamsung_ilo_Request9_m728156365 (Il2CppObject * __this /* static, unused */, String_t* ___url0, WWWForm_t461342257 * ___wwwForm1, SdkwwwCallBack_t3004559384 * ___callBack2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginSamsung::ilo_Log10(System.Object,System.Boolean)
extern "C"  void PluginSamsung_ilo_Log10_m4044437307 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken PluginSamsung::ilo_get_Item11(Newtonsoft.Json.Linq.JObject,System.String)
extern "C"  JToken_t3412245951 * PluginSamsung_ilo_get_Item11_m2015043920 (Il2CppObject * __this /* static, unused */, JObject_t1798544199 * ____this0, String_t* ___propertyName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionInfo PluginSamsung::ilo_get_currentVS12(VersionMgr)
extern "C"  VersionInfo_t2356638086 * PluginSamsung_ilo_get_currentVS12_m1600566858 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AndroidJavaObject PluginSamsung::ilo_get_androidJavaObject13(PluginsSdkMgr)
extern "C"  AndroidJavaObject_t2362096582 * PluginSamsung_ilo_get_androidJavaObject13_m665104130 (Il2CppObject * __this /* static, unused */, PluginsSdkMgr_t3884624670 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
