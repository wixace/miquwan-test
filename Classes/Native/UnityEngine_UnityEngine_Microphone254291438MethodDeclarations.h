﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Microphone
struct Microphone_t254291438;
// UnityEngine.AudioClip
struct AudioClip_t794140988;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void UnityEngine.Microphone::.ctor()
extern "C"  void Microphone__ctor_m2444225795 (Microphone_t254291438 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AudioClip UnityEngine.Microphone::Start(System.String,System.Boolean,System.Int32,System.Int32)
extern "C"  AudioClip_t794140988 * Microphone_Start_m3251913754 (Il2CppObject * __this /* static, unused */, String_t* ___deviceName0, bool ___loop1, int32_t ___lengthSec2, int32_t ___frequency3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Microphone::End(System.String)
extern "C"  void Microphone_End_m1908738118 (Il2CppObject * __this /* static, unused */, String_t* ___deviceName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] UnityEngine.Microphone::get_devices()
extern "C"  StringU5BU5D_t4054002952* Microphone_get_devices_m2367555092 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Microphone::IsRecording(System.String)
extern "C"  bool Microphone_IsRecording_m2779654432 (Il2CppObject * __this /* static, unused */, String_t* ___deviceName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Microphone::GetPosition(System.String)
extern "C"  int32_t Microphone_GetPosition_m3193481858 (Il2CppObject * __this /* static, unused */, String_t* ___deviceName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Microphone::GetDeviceCaps(System.String,System.Int32&,System.Int32&)
extern "C"  void Microphone_GetDeviceCaps_m1050884478 (Il2CppObject * __this /* static, unused */, String_t* ___deviceName0, int32_t* ___minFreq1, int32_t* ___maxFreq2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
