﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Int2,System.Object>
struct KeyCollection_t2594088108;
// System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Object>
struct Dictionary_2_t967328657;
// System.Collections.Generic.IEnumerator`1<Pathfinding.Int2>
struct IEnumerator_1_t3885910642;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// Pathfinding.Int2[]
struct Int2U5BU5D_t30096868;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_Int21974045593.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K1582264711.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Int2,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void KeyCollection__ctor_m2069746375_gshared (KeyCollection_t2594088108 * __this, Dictionary_2_t967328657 * ___dictionary0, const MethodInfo* method);
#define KeyCollection__ctor_m2069746375(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t2594088108 *, Dictionary_2_t967328657 *, const MethodInfo*))KeyCollection__ctor_m2069746375_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Int2,System.Object>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3124930095_gshared (KeyCollection_t2594088108 * __this, Int2_t1974045593  ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3124930095(__this, ___item0, method) ((  void (*) (KeyCollection_t2594088108 *, Int2_t1974045593 , const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3124930095_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Int2,System.Object>::System.Collections.Generic.ICollection<TKey>.Clear()
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m302680934_gshared (KeyCollection_t2594088108 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m302680934(__this, method) ((  void (*) (KeyCollection_t2594088108 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m302680934_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Int2,System.Object>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m163836927_gshared (KeyCollection_t2594088108 * __this, Int2_t1974045593  ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m163836927(__this, ___item0, method) ((  bool (*) (KeyCollection_t2594088108 *, Int2_t1974045593 , const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m163836927_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Int2,System.Object>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2284542244_gshared (KeyCollection_t2594088108 * __this, Int2_t1974045593  ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2284542244(__this, ___item0, method) ((  bool (*) (KeyCollection_t2594088108 *, Int2_t1974045593 , const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2284542244_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Int2,System.Object>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m2963816760_gshared (KeyCollection_t2594088108 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m2963816760(__this, method) ((  Il2CppObject* (*) (KeyCollection_t2594088108 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m2963816760_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Int2,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m1076397784_gshared (KeyCollection_t2594088108 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_CopyTo_m1076397784(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t2594088108 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m1076397784_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Int2,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m4286215783_gshared (KeyCollection_t2594088108 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m4286215783(__this, method) ((  Il2CppObject * (*) (KeyCollection_t2594088108 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m4286215783_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Int2,System.Object>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m1414555552_gshared (KeyCollection_t2594088108 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m1414555552(__this, method) ((  bool (*) (KeyCollection_t2594088108 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m1414555552_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Int2,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m635533394_gshared (KeyCollection_t2594088108 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m635533394(__this, method) ((  bool (*) (KeyCollection_t2594088108 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m635533394_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Int2,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m3949889412_gshared (KeyCollection_t2594088108 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m3949889412(__this, method) ((  Il2CppObject * (*) (KeyCollection_t2594088108 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m3949889412_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Int2,System.Object>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m1959794044_gshared (KeyCollection_t2594088108 * __this, Int2U5BU5D_t30096868* ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_CopyTo_m1959794044(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t2594088108 *, Int2U5BU5D_t30096868*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m1959794044_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Int2,System.Object>::GetEnumerator()
extern "C"  Enumerator_t1582264711  KeyCollection_GetEnumerator_m4101563209_gshared (KeyCollection_t2594088108 * __this, const MethodInfo* method);
#define KeyCollection_GetEnumerator_m4101563209(__this, method) ((  Enumerator_t1582264711  (*) (KeyCollection_t2594088108 *, const MethodInfo*))KeyCollection_GetEnumerator_m4101563209_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Int2,System.Object>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m2937548556_gshared (KeyCollection_t2594088108 * __this, const MethodInfo* method);
#define KeyCollection_get_Count_m2937548556(__this, method) ((  int32_t (*) (KeyCollection_t2594088108 *, const MethodInfo*))KeyCollection_get_Count_m2937548556_gshared)(__this, method)
