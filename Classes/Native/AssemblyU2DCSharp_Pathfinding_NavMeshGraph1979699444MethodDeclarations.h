﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.NavMeshGraph
struct NavMeshGraph_t1979699444;
// Pathfinding.GraphNodeDelegateCancelable
struct GraphNodeDelegateCancelable_t3591372971;
// Pathfinding.TriangleMeshNode[]
struct TriangleMeshNodeU5BU5D_t2064970368;
// Pathfinding.BBTree
struct BBTree_t1216325332;
// Pathfinding.Int3[]
struct Int3U5BU5D_t516284607;
// Pathfinding.GraphNode[]
struct GraphNodeU5BU5D_t927449255;
// Pathfinding.NNConstraint
struct NNConstraint_t758567699;
// Pathfinding.GraphNode
struct GraphNode_t23612370;
// Pathfinding.NavGraph
struct NavGraph_t1254319713;
// Pathfinding.INavmeshHolder
struct INavmeshHolder_t1019551337;
// System.Collections.Generic.List`1<Pathfinding.GraphNode>
struct List_1_t1391797922;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t1355284822;
// Pathfinding.INavmesh
struct INavmesh_t889550173;
// Pathfinding.GraphUpdateObject
struct GraphUpdateObject_t430843704;
// Pathfinding.TriangleMeshNode
struct TriangleMeshNode_t1626248749;
// System.String
struct String_t;
// OnScanStatus
struct OnScanStatus_t2412749870;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t215400611;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// Pathfinding.Serialization.GraphSerializationContext
struct GraphSerializationContext_t3256954663;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// Pathfinding.MeshNode
struct MeshNode_t3005053445;
// UnityEngine.Mesh
struct Mesh_t4241756145;
// Pathfinding.PathHandler
struct PathHandler_t918952263;
// AstarPath
struct AstarPath_t4090270936;
// Pathfinding.PathNode
struct PathNode_t417131581;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphNodeDelegateCan3591372971.h"
#include "AssemblyU2DCSharp_Pathfinding_Int31974045594.h"
#include "AssemblyU2DCSharp_Pathfinding_BBTree1216325332.h"
#include "UnityEngine_UnityEngine_Matrix4x41651859333.h"
#include "AssemblyU2DCSharp_Pathfinding_NNInfo1570625892.h"
#include "AssemblyU2DCSharp_Pathfinding_NavMeshGraph1979699444.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_Pathfinding_NNConstraint758567699.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphNode23612370.h"
#include "AssemblyU2DCSharp_Pathfinding_NavGraph1254319713.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphHitInfo3082627369.h"
#include "AssemblyU2DCSharp_GraphUpdateThreading3958092737.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphUpdateObject430843704.h"
#include "AssemblyU2DCSharp_Pathfinding_TriangleMeshNode1626248749.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_OnScanStatus2412749870.h"
#include "AssemblyU2DCSharp_Pathfinding_Serialization_GraphS3256954663.h"
#include "AssemblyU2DCSharp_Pathfinding_MeshNode3005053445.h"
#include "UnityEngine_UnityEngine_Rect4241904616.h"
#include "AssemblyU2DCSharp_AstarPath4090270936.h"
#include "AssemblyU2DCSharp_Pathfinding_PathHandler918952263.h"

// System.Void Pathfinding.NavMeshGraph::.ctor()
extern "C"  void NavMeshGraph__ctor_m2708211699 (NavMeshGraph_t1979699444 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NavMeshGraph::Pathfinding.INavmesh.GetNodes(Pathfinding.GraphNodeDelegateCancelable)
extern "C"  void NavMeshGraph_Pathfinding_INavmesh_GetNodes_m3089396554 (NavMeshGraph_t1979699444 * __this, GraphNodeDelegateCancelable_t3591372971 * ___del0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NavMeshGraph::CreateNodes(System.Int32)
extern "C"  void NavMeshGraph_CreateNodes_m435485495 (NavMeshGraph_t1979699444 * __this, int32_t ___number0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.TriangleMeshNode[] Pathfinding.NavMeshGraph::get_TriNodes()
extern "C"  TriangleMeshNodeU5BU5D_t2064970368* NavMeshGraph_get_TriNodes_m484651802 (NavMeshGraph_t1979699444 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NavMeshGraph::GetNodes(Pathfinding.GraphNodeDelegateCancelable)
extern "C"  void NavMeshGraph_GetNodes_m1610646087 (NavMeshGraph_t1979699444 * __this, GraphNodeDelegateCancelable_t3591372971 * ___del0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NavMeshGraph::OnDestroy()
extern "C"  void NavMeshGraph_OnDestroy_m188918636 (NavMeshGraph_t1979699444 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Int3 Pathfinding.NavMeshGraph::GetVertex(System.Int32)
extern "C"  Int3_t1974045594  NavMeshGraph_GetVertex_m1163060397 (NavMeshGraph_t1979699444 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pathfinding.NavMeshGraph::GetVertexArrayIndex(System.Int32)
extern "C"  int32_t NavMeshGraph_GetVertexArrayIndex_m2985339655 (NavMeshGraph_t1979699444 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NavMeshGraph::GetTileCoordinates(System.Int32,System.Int32&,System.Int32&)
extern "C"  void NavMeshGraph_GetTileCoordinates_m2304387619 (NavMeshGraph_t1979699444 * __this, int32_t ___tileIndex0, int32_t* ___x1, int32_t* ___z2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.BBTree Pathfinding.NavMeshGraph::get_bbTree()
extern "C"  BBTree_t1216325332 * NavMeshGraph_get_bbTree_m62719053 (NavMeshGraph_t1979699444 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NavMeshGraph::set_bbTree(Pathfinding.BBTree)
extern "C"  void NavMeshGraph_set_bbTree_m1974101828 (NavMeshGraph_t1979699444 * __this, BBTree_t1216325332 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Int3[] Pathfinding.NavMeshGraph::get_vertices()
extern "C"  Int3U5BU5D_t516284607* NavMeshGraph_get_vertices_m3140538976 (NavMeshGraph_t1979699444 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NavMeshGraph::set_vertices(Pathfinding.Int3[])
extern "C"  void NavMeshGraph_set_vertices_m1921282263 (NavMeshGraph_t1979699444 * __this, Int3U5BU5D_t516284607* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NavMeshGraph::GenerateMatrix()
extern "C"  void NavMeshGraph_GenerateMatrix_m2287439623 (NavMeshGraph_t1979699444 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NavMeshGraph::RelocateNodes(UnityEngine.Matrix4x4,UnityEngine.Matrix4x4)
extern "C"  void NavMeshGraph_RelocateNodes_m1337544721 (NavMeshGraph_t1979699444 * __this, Matrix4x4_t1651859333  ___oldMatrix0, Matrix4x4_t1651859333  ___newMatrix1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.NNInfo Pathfinding.NavMeshGraph::GetNearest(Pathfinding.NavMeshGraph,Pathfinding.GraphNode[],UnityEngine.Vector3,Pathfinding.NNConstraint,System.Boolean)
extern "C"  NNInfo_t1570625892  NavMeshGraph_GetNearest_m691569515 (Il2CppObject * __this /* static, unused */, NavMeshGraph_t1979699444 * ___graph0, GraphNodeU5BU5D_t927449255* ___nodes1, Vector3_t4282066566  ___position2, NNConstraint_t758567699 * ___constraint3, bool ___accurateNearestNode4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.NNInfo Pathfinding.NavMeshGraph::GetNearest(UnityEngine.Vector3,Pathfinding.NNConstraint,Pathfinding.GraphNode)
extern "C"  NNInfo_t1570625892  NavMeshGraph_GetNearest_m1700351946 (NavMeshGraph_t1979699444 * __this, Vector3_t4282066566  ___position0, NNConstraint_t758567699 * ___constraint1, GraphNode_t23612370 * ___hint2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.NNInfo Pathfinding.NavMeshGraph::GetNearestForce(UnityEngine.Vector3,Pathfinding.NNConstraint)
extern "C"  NNInfo_t1570625892  NavMeshGraph_GetNearestForce_m463478229 (NavMeshGraph_t1979699444 * __this, Vector3_t4282066566  ___position0, NNConstraint_t758567699 * ___constraint1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.NNInfo Pathfinding.NavMeshGraph::GetNearestForce(Pathfinding.NavGraph,Pathfinding.INavmeshHolder,UnityEngine.Vector3,Pathfinding.NNConstraint,System.Boolean)
extern "C"  NNInfo_t1570625892  NavMeshGraph_GetNearestForce_m4237479840 (Il2CppObject * __this /* static, unused */, NavGraph_t1254319713 * ___graph0, Il2CppObject * ___navmesh1, Vector3_t4282066566  ___position2, NNConstraint_t758567699 * ___constraint3, bool ___accurateNearestNode4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.NNInfo Pathfinding.NavMeshGraph::GetNearestForceBoth(Pathfinding.NavGraph,Pathfinding.INavmeshHolder,UnityEngine.Vector3,Pathfinding.NNConstraint,System.Boolean)
extern "C"  NNInfo_t1570625892  NavMeshGraph_GetNearestForceBoth_m235954335 (Il2CppObject * __this /* static, unused */, NavGraph_t1254319713 * ___graph0, Il2CppObject * ___navmesh1, Vector3_t4282066566  ___position2, NNConstraint_t758567699 * ___constraint3, bool ___accurateNearestNode4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NavMeshGraph::BuildFunnelCorridor(System.Collections.Generic.List`1<Pathfinding.GraphNode>,System.Int32,System.Int32,System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Collections.Generic.List`1<UnityEngine.Vector3>)
extern "C"  void NavMeshGraph_BuildFunnelCorridor_m2859837161 (NavMeshGraph_t1979699444 * __this, List_1_t1391797922 * ___path0, int32_t ___startIndex1, int32_t ___endIndex2, List_1_t1355284822 * ___left3, List_1_t1355284822 * ___right4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NavMeshGraph::BuildFunnelCorridor(Pathfinding.INavmesh,System.Collections.Generic.List`1<Pathfinding.GraphNode>,System.Int32,System.Int32,System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Collections.Generic.List`1<UnityEngine.Vector3>)
extern "C"  void NavMeshGraph_BuildFunnelCorridor_m2449013602 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___graph0, List_1_t1391797922 * ___path1, int32_t ___startIndex2, int32_t ___endIndex3, List_1_t1355284822 * ___left4, List_1_t1355284822 * ___right5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NavMeshGraph::AddPortal(Pathfinding.GraphNode,Pathfinding.GraphNode,System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Collections.Generic.List`1<UnityEngine.Vector3>)
extern "C"  void NavMeshGraph_AddPortal_m2618386612 (NavMeshGraph_t1979699444 * __this, GraphNode_t23612370 * ___n10, GraphNode_t23612370 * ___n21, List_1_t1355284822 * ___left2, List_1_t1355284822 * ___right3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.NavMeshGraph::Linecast(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  bool NavMeshGraph_Linecast_m1015000020 (NavMeshGraph_t1979699444 * __this, Vector3_t4282066566  ___origin0, Vector3_t4282066566  ___end1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.NavMeshGraph::Linecast(UnityEngine.Vector3,UnityEngine.Vector3,Pathfinding.GraphNode,Pathfinding.GraphHitInfo&)
extern "C"  bool NavMeshGraph_Linecast_m798580253 (NavMeshGraph_t1979699444 * __this, Vector3_t4282066566  ___origin0, Vector3_t4282066566  ___end1, GraphNode_t23612370 * ___hint2, GraphHitInfo_t3082627369 * ___hit3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.NavMeshGraph::Linecast(UnityEngine.Vector3,UnityEngine.Vector3,Pathfinding.GraphNode)
extern "C"  bool NavMeshGraph_Linecast_m1819366138 (NavMeshGraph_t1979699444 * __this, Vector3_t4282066566  ___origin0, Vector3_t4282066566  ___end1, GraphNode_t23612370 * ___hint2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.NavMeshGraph::Linecast(UnityEngine.Vector3,UnityEngine.Vector3,Pathfinding.GraphNode,Pathfinding.GraphHitInfo&,System.Collections.Generic.List`1<Pathfinding.GraphNode>)
extern "C"  bool NavMeshGraph_Linecast_m416035475 (NavMeshGraph_t1979699444 * __this, Vector3_t4282066566  ___origin0, Vector3_t4282066566  ___end1, GraphNode_t23612370 * ___hint2, GraphHitInfo_t3082627369 * ___hit3, List_1_t1391797922 * ___trace4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.NavMeshGraph::Linecast(Pathfinding.INavmesh,UnityEngine.Vector3,UnityEngine.Vector3,Pathfinding.GraphNode,Pathfinding.GraphHitInfo&)
extern "C"  bool NavMeshGraph_Linecast_m1878249878 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___graph0, Vector3_t4282066566  ___tmp_origin1, Vector3_t4282066566  ___tmp_end2, GraphNode_t23612370 * ___hint3, GraphHitInfo_t3082627369 * ___hit4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.NavMeshGraph::Linecast(Pathfinding.INavmesh,UnityEngine.Vector3,UnityEngine.Vector3,Pathfinding.GraphNode,Pathfinding.GraphHitInfo&,System.Collections.Generic.List`1<Pathfinding.GraphNode>)
extern "C"  bool NavMeshGraph_Linecast_m388304186 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___graph0, Vector3_t4282066566  ___tmp_origin1, Vector3_t4282066566  ___tmp_end2, GraphNode_t23612370 * ___hint3, GraphHitInfo_t3082627369 * ___hit4, List_1_t1391797922 * ___trace5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GraphUpdateThreading Pathfinding.NavMeshGraph::CanUpdateAsync(Pathfinding.GraphUpdateObject)
extern "C"  int32_t NavMeshGraph_CanUpdateAsync_m960213814 (NavMeshGraph_t1979699444 * __this, GraphUpdateObject_t430843704 * ___o0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NavMeshGraph::UpdateAreaInit(Pathfinding.GraphUpdateObject)
extern "C"  void NavMeshGraph_UpdateAreaInit_m1798710799 (NavMeshGraph_t1979699444 * __this, GraphUpdateObject_t430843704 * ___o0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NavMeshGraph::UpdateArea(Pathfinding.GraphUpdateObject)
extern "C"  void NavMeshGraph_UpdateArea_m2919096895 (NavMeshGraph_t1979699444 * __this, GraphUpdateObject_t430843704 * ___o0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NavMeshGraph::UpdateArea(Pathfinding.GraphUpdateObject,Pathfinding.INavmesh)
extern "C"  void NavMeshGraph_UpdateArea_m2542908726 (Il2CppObject * __this /* static, unused */, GraphUpdateObject_t430843704 * ___o0, Il2CppObject * ___graph1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Pathfinding.NavMeshGraph::ClosestPointOnNode(Pathfinding.TriangleMeshNode,Pathfinding.Int3[],UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  NavMeshGraph_ClosestPointOnNode_m2485949479 (Il2CppObject * __this /* static, unused */, TriangleMeshNode_t1626248749 * ___node0, Int3U5BU5D_t516284607* ___vertices1, Vector3_t4282066566  ___pos2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.NavMeshGraph::ContainsPoint(Pathfinding.TriangleMeshNode,UnityEngine.Vector3)
extern "C"  bool NavMeshGraph_ContainsPoint_m3726746144 (NavMeshGraph_t1979699444 * __this, TriangleMeshNode_t1626248749 * ___node0, Vector3_t4282066566  ___pos1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.NavMeshGraph::ContainsPoint(Pathfinding.TriangleMeshNode,UnityEngine.Vector3,Pathfinding.Int3[])
extern "C"  bool NavMeshGraph_ContainsPoint_m739157238 (Il2CppObject * __this /* static, unused */, TriangleMeshNode_t1626248749 * ___node0, Vector3_t4282066566  ___pos1, Int3U5BU5D_t516284607* ___vertices2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NavMeshGraph::ScanInternal(System.String)
extern "C"  void NavMeshGraph_ScanInternal_m576823319 (NavMeshGraph_t1979699444 * __this, String_t* ___objMeshPath0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NavMeshGraph::ScanInternal(OnScanStatus)
extern "C"  void NavMeshGraph_ScanInternal_m4014595933 (NavMeshGraph_t1979699444 * __this, OnScanStatus_t2412749870 * ___statusCallback0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NavMeshGraph::GenerateNodes(UnityEngine.Vector3[],System.Int32[],UnityEngine.Vector3[]&,Pathfinding.Int3[]&)
extern "C"  void NavMeshGraph_GenerateNodes_m1509476006 (NavMeshGraph_t1979699444 * __this, Vector3U5BU5D_t215400611* ___vectorVertices0, Int32U5BU5D_t3230847821* ___triangles1, Vector3U5BU5D_t215400611** ___originalVertices2, Int3U5BU5D_t516284607** ___vertices3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NavMeshGraph::RebuildBBTree(Pathfinding.NavMeshGraph)
extern "C"  void NavMeshGraph_RebuildBBTree_m670406914 (Il2CppObject * __this /* static, unused */, NavMeshGraph_t1979699444 * ___graph0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NavMeshGraph::PostProcess()
extern "C"  void NavMeshGraph_PostProcess_m411278080 (NavMeshGraph_t1979699444 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NavMeshGraph::Sort(UnityEngine.Vector3[])
extern "C"  void NavMeshGraph_Sort_m31298312 (NavMeshGraph_t1979699444 * __this, Vector3U5BU5D_t215400611* ___a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NavMeshGraph::OnDrawGizmos(System.Boolean)
extern "C"  void NavMeshGraph_OnDrawGizmos_m4052405380 (NavMeshGraph_t1979699444 * __this, bool ___drawNodes0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NavMeshGraph::DeserializeExtraInfo(Pathfinding.Serialization.GraphSerializationContext)
extern "C"  void NavMeshGraph_DeserializeExtraInfo_m1396531517 (NavMeshGraph_t1979699444 * __this, GraphSerializationContext_t3256954663 * ___ctx0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NavMeshGraph::SerializeExtraInfo(Pathfinding.Serialization.GraphSerializationContext)
extern "C"  void NavMeshGraph_SerializeExtraInfo_m1832357086 (NavMeshGraph_t1979699444 * __this, GraphSerializationContext_t3256954663 * ___ctx0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NavMeshGraph::DeserializeMeshNodes(Pathfinding.NavMeshGraph,Pathfinding.GraphNode[],System.Byte[])
extern "C"  void NavMeshGraph_DeserializeMeshNodes_m1705164629 (Il2CppObject * __this /* static, unused */, NavMeshGraph_t1979699444 * ___graph0, GraphNodeU5BU5D_t927449255* ___nodes1, ByteU5BU5D_t4260760469* ___bytes2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NavMeshGraph::ilo_SetNavmeshHolder1(System.Int32,Pathfinding.INavmeshHolder)
extern "C"  void NavMeshGraph_ilo_SetNavmeshHolder1_m3778077603 (Il2CppObject * __this /* static, unused */, int32_t ___graphIndex0, Il2CppObject * ___graph1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Int3[] Pathfinding.NavMeshGraph::ilo_get_vertices2(Pathfinding.NavMeshGraph)
extern "C"  Int3U5BU5D_t516284607* NavMeshGraph_ilo_get_vertices2_m820749433 (Il2CppObject * __this /* static, unused */, NavMeshGraph_t1979699444 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Int3 Pathfinding.NavMeshGraph::ilo_op_Subtraction3(Pathfinding.Int3,Pathfinding.Int3)
extern "C"  Int3_t1974045594  NavMeshGraph_ilo_op_Subtraction3_m4040933178 (Il2CppObject * __this /* static, unused */, Int3_t1974045594  ___lhs0, Int3_t1974045594  ___rhs1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NavMeshGraph::ilo_set_bbTree4(Pathfinding.NavMeshGraph,Pathfinding.BBTree)
extern "C"  void NavMeshGraph_ilo_set_bbTree4_m4151554639 (Il2CppObject * __this /* static, unused */, NavMeshGraph_t1979699444 * ____this0, BBTree_t1216325332 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NavMeshGraph::ilo_Insert5(Pathfinding.BBTree,Pathfinding.MeshNode)
extern "C"  void NavMeshGraph_ilo_Insert5_m1292571291 (Il2CppObject * __this /* static, unused */, BBTree_t1216325332 * ____this0, MeshNode_t3005053445 * ___node1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Int3[] Pathfinding.NavMeshGraph::ilo_get_vertices6(Pathfinding.NavMeshGraph)
extern "C"  Int3U5BU5D_t516284607* NavMeshGraph_ilo_get_vertices6_m372238205 (Il2CppObject * __this /* static, unused */, NavMeshGraph_t1979699444 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.NNInfo Pathfinding.NavMeshGraph::ilo_GetNearestForce7(Pathfinding.NavGraph,Pathfinding.INavmeshHolder,UnityEngine.Vector3,Pathfinding.NNConstraint,System.Boolean)
extern "C"  NNInfo_t1570625892  NavMeshGraph_ilo_GetNearestForce7_m3853396626 (Il2CppObject * __this /* static, unused */, NavGraph_t1254319713 * ___graph0, Il2CppObject * ___navmesh1, Vector3_t4282066566  ___position2, NNConstraint_t758567699 * ___constraint3, bool ___accurateNearestNode4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.BBTree Pathfinding.NavMeshGraph::ilo_get_bbTree8(Pathfinding.NavMeshGraph)
extern "C"  BBTree_t1216325332 * NavMeshGraph_ilo_get_bbTree8_m3341407986 (Il2CppObject * __this /* static, unused */, NavMeshGraph_t1979699444 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect Pathfinding.NavMeshGraph::ilo_get_Size9(Pathfinding.BBTree)
extern "C"  Rect_t4241904616  NavMeshGraph_ilo_get_Size9_m3443029283 (Il2CppObject * __this /* static, unused */, BBTree_t1216325332 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Pathfinding.NavMeshGraph::ilo_ClosestPointOnNode10(Pathfinding.TriangleMeshNode,Pathfinding.Int3[],UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  NavMeshGraph_ilo_ClosestPointOnNode10_m1825843221 (Il2CppObject * __this /* static, unused */, TriangleMeshNode_t1626248749 * ___node0, Int3U5BU5D_t516284607* ___vertices1, Vector3_t4282066566  ___pos2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.NNInfo Pathfinding.NavMeshGraph::ilo_GetNearest11(Pathfinding.NavMeshGraph,Pathfinding.GraphNode[],UnityEngine.Vector3,Pathfinding.NNConstraint,System.Boolean)
extern "C"  NNInfo_t1570625892  NavMeshGraph_ilo_GetNearest11_m1786012734 (Il2CppObject * __this /* static, unused */, NavMeshGraph_t1979699444 * ___graph0, GraphNodeU5BU5D_t927449255* ___nodes1, Vector3_t4282066566  ___position2, NNConstraint_t758567699 * ___constraint3, bool ___accurateNearestNode4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NavMeshGraph::ilo_GetNodes12(Pathfinding.NavGraph,Pathfinding.GraphNodeDelegateCancelable)
extern "C"  void NavMeshGraph_ilo_GetNodes12_m3638540472 (Il2CppObject * __this /* static, unused */, NavGraph_t1254319713 * ____this0, GraphNodeDelegateCancelable_t3591372971 * ___del1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Pathfinding.NavMeshGraph::ilo_ClosestPointOnNode13(Pathfinding.TriangleMeshNode,UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  NavMeshGraph_ilo_ClosestPointOnNode13_m246116330 (Il2CppObject * __this /* static, unused */, TriangleMeshNode_t1626248749 * ____this0, Vector3_t4282066566  ___p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pathfinding.NavMeshGraph::ilo_GetVertexIndex14(Pathfinding.TriangleMeshNode,System.Int32)
extern "C"  int32_t NavMeshGraph_ilo_GetVertexIndex14_m3442083051 (Il2CppObject * __this /* static, unused */, TriangleMeshNode_t1626248749 * ____this0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Pathfinding.NavMeshGraph::ilo_op_Explicit15(Pathfinding.Int3)
extern "C"  Vector3_t4282066566  NavMeshGraph_ilo_op_Explicit15_m379401196 (Il2CppObject * __this /* static, unused */, Int3_t1974045594  ___ob0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Int3 Pathfinding.NavMeshGraph::ilo_GetVertex16(Pathfinding.TriangleMeshNode,System.Int32)
extern "C"  Int3_t1974045594  NavMeshGraph_ilo_GetVertex16_m591419530 (Il2CppObject * __this /* static, unused */, TriangleMeshNode_t1626248749 * ____this0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.NavMeshGraph::ilo_Linecast17(Pathfinding.INavmesh,UnityEngine.Vector3,UnityEngine.Vector3,Pathfinding.GraphNode,Pathfinding.GraphHitInfo&,System.Collections.Generic.List`1<Pathfinding.GraphNode>)
extern "C"  bool NavMeshGraph_ilo_Linecast17_m1232963137 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___graph0, Vector3_t4282066566  ___tmp_origin1, Vector3_t4282066566  ___tmp_end2, GraphNode_t23612370 * ___hint3, GraphHitInfo_t3082627369 * ___hit4, List_1_t1391797922 * ___trace5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Int3 Pathfinding.NavMeshGraph::ilo_op_Explicit18(UnityEngine.Vector3)
extern "C"  Int3_t1974045594  NavMeshGraph_ilo_op_Explicit18_m3726206129 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___ob0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.NNConstraint Pathfinding.NavMeshGraph::ilo_get_None19()
extern "C"  NNConstraint_t758567699 * NavMeshGraph_ilo_get_None19_m1105483043 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.NNInfo Pathfinding.NavMeshGraph::ilo_GetNearest20(Pathfinding.NavGraph,UnityEngine.Vector3,Pathfinding.NNConstraint)
extern "C"  NNInfo_t1570625892  NavMeshGraph_ilo_GetNearest20_m2987236722 (Il2CppObject * __this /* static, unused */, NavGraph_t1254319713 * ____this0, Vector3_t4282066566  ___position1, NNConstraint_t758567699 * ___constraint2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.NavMeshGraph::ilo_ContainsPoint21(Pathfinding.TriangleMeshNode,Pathfinding.Int3)
extern "C"  bool NavMeshGraph_ilo_ContainsPoint21_m921003167 (Il2CppObject * __this /* static, unused */, TriangleMeshNode_t1626248749 * ____this0, Int3_t1974045594  ___p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.NavMeshGraph::ilo_GetPortal22(Pathfinding.TriangleMeshNode,Pathfinding.GraphNode,System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Boolean)
extern "C"  bool NavMeshGraph_ilo_GetPortal22_m3112631468 (Il2CppObject * __this /* static, unused */, TriangleMeshNode_t1626248749 * ____this0, GraphNode_t23612370 * ____other1, List_1_t1355284822 * ___left2, List_1_t1355284822 * ___right3, bool ___backwards4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.NavMeshGraph::ilo_LeftNotColinear23(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  bool NavMeshGraph_ilo_LeftNotColinear23_m3537755703 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___a0, Vector3_t4282066566  ___b1, Vector3_t4282066566  ___p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.NavMeshGraph::ilo_IntersectionFactor24(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,System.Single&,System.Single&)
extern "C"  bool NavMeshGraph_ilo_IntersectionFactor24_m2494555360 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___start10, Vector3_t4282066566  ___end11, Vector3_t4282066566  ___start22, Vector3_t4282066566  ___end23, float* ___factor14, float* ___factor25, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.NavMeshGraph::ilo_IsClockwise25(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  bool NavMeshGraph_ilo_IsClockwise25_m4059165674 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___a0, Vector3_t4282066566  ___b1, Vector3_t4282066566  ___c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.NavMeshGraph::ilo_IsClockwiseMargin26(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  bool NavMeshGraph_ilo_IsClockwiseMargin26_m2684952763 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___a0, Vector3_t4282066566  ___b1, Vector3_t4282066566  ___c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Mesh Pathfinding.NavMeshGraph::ilo_ImportFile27(System.String)
extern "C"  Mesh_t4241756145 * NavMeshGraph_ilo_ImportFile27_m1452078495 (Il2CppObject * __this /* static, unused */, String_t* ___filePath0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.NavMeshGraph::ilo_IsClockwise28(Pathfinding.Int3,Pathfinding.Int3,Pathfinding.Int3)
extern "C"  bool NavMeshGraph_ilo_IsClockwise28_m259252906 (Il2CppObject * __this /* static, unused */, Int3_t1974045594  ___a0, Int3_t1974045594  ___b1, Int3_t1974045594  ___c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.NavMeshGraph::ilo_IsColinear29(Pathfinding.Int3,Pathfinding.Int3,Pathfinding.Int3)
extern "C"  bool NavMeshGraph_ilo_IsColinear29_m1823262444 (Il2CppObject * __this /* static, unused */, Int3_t1974045594  ___a0, Int3_t1974045594  ___b1, Int3_t1974045594  ___c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pathfinding.NavMeshGraph::ilo_get_costMagnitude30(Pathfinding.Int3&)
extern "C"  int32_t NavMeshGraph_ilo_get_costMagnitude30_m3004620211 (Il2CppObject * __this /* static, unused */, Int3_t1974045594 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NavMeshGraph::ilo_RebuildBBTree31(Pathfinding.NavMeshGraph)
extern "C"  void NavMeshGraph_ilo_RebuildBBTree31_m397582029 (Il2CppObject * __this /* static, unused */, NavMeshGraph_t1979699444 * ___graph0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.TriangleMeshNode[] Pathfinding.NavMeshGraph::ilo_get_TriNodes32(Pathfinding.NavMeshGraph)
extern "C"  TriangleMeshNodeU5BU5D_t2064970368* NavMeshGraph_ilo_get_TriNodes32_m1764943620 (Il2CppObject * __this /* static, unused */, NavMeshGraph_t1979699444 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NavMeshGraph::ilo_set_bbTree33(Pathfinding.NavMeshGraph,Pathfinding.BBTree)
extern "C"  void NavMeshGraph_ilo_set_bbTree33_m4057175783 (Il2CppObject * __this /* static, unused */, NavMeshGraph_t1979699444 * ____this0, BBTree_t1216325332 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.BBTree Pathfinding.NavMeshGraph::ilo_get_bbTree34(Pathfinding.NavMeshGraph)
extern "C"  BBTree_t1216325332 * NavMeshGraph_ilo_get_bbTree34_m1075714873 (Il2CppObject * __this /* static, unused */, NavMeshGraph_t1979699444 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NavMeshGraph::ilo_RelocateNodes35(Pathfinding.NavMeshGraph,UnityEngine.Matrix4x4,UnityEngine.Matrix4x4)
extern "C"  void NavMeshGraph_ilo_RelocateNodes35_m3944352844 (Il2CppObject * __this /* static, unused */, NavMeshGraph_t1979699444 * ____this0, Matrix4x4_t1651859333  ___oldMatrix1, Matrix4x4_t1651859333  ___newMatrix2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.PathHandler Pathfinding.NavMeshGraph::ilo_get_debugPathData36(AstarPath)
extern "C"  PathHandler_t918952263 * NavMeshGraph_ilo_get_debugPathData36_m2829912 (Il2CppObject * __this /* static, unused */, AstarPath_t4090270936 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.PathNode Pathfinding.NavMeshGraph::ilo_GetPathNode37(Pathfinding.PathHandler,Pathfinding.GraphNode)
extern "C"  PathNode_t417131581 * NavMeshGraph_ilo_GetPathNode37_m2690850282 (Il2CppObject * __this /* static, unused */, PathHandler_t918952263 * ____this0, GraphNode_t23612370 * ___node1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
