﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<Core.RpsResult>
struct DefaultComparer_t3222338787;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Core_RpsResult479594824.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<Core.RpsResult>::.ctor()
extern "C"  void DefaultComparer__ctor_m1829866703_gshared (DefaultComparer_t3222338787 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m1829866703(__this, method) ((  void (*) (DefaultComparer_t3222338787 *, const MethodInfo*))DefaultComparer__ctor_m1829866703_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<Core.RpsResult>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m1936948292_gshared (DefaultComparer_t3222338787 * __this, int32_t ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m1936948292(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t3222338787 *, int32_t, const MethodInfo*))DefaultComparer_GetHashCode_m1936948292_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<Core.RpsResult>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m2889634276_gshared (DefaultComparer_t3222338787 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m2889634276(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t3222338787 *, int32_t, int32_t, const MethodInfo*))DefaultComparer_Equals_m2889634276_gshared)(__this, ___x0, ___y1, method)
