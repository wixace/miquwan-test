﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIFBXDisplaySprite
struct UIFBXDisplaySprite_t2233392895;
// UnityEngine.Texture
struct Texture_t2526458961;
// UITexture
struct UITexture_t3903132647;
// UnityEngine.Shader
struct Shader_t3191267369;
// UIModelDisplayMgr
struct UIModelDisplayMgr_t1446490315;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_UIModelDisplayType1891752679.h"
#include "AssemblyU2DCSharp_UITexture3903132647.h"
#include "UnityEngine_UnityEngine_Shader3191267369.h"
#include "AssemblyU2DCSharp_UIModelDisplayMgr1446490315.h"
#include "AssemblyU2DCSharp_UIFBXDisplaySprite2233392895.h"

// System.Void UIFBXDisplaySprite::.ctor()
extern "C"  void UIFBXDisplaySprite__ctor_m973639804 (UIFBXDisplaySprite_t2233392895 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture UIFBXDisplaySprite::get_mainTexture()
extern "C"  Texture_t2526458961 * UIFBXDisplaySprite_get_mainTexture_m577338948 (UIFBXDisplaySprite_t2233392895 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIFBXDisplaySprite::Bind(UIModelDisplayType)
extern "C"  bool UIFBXDisplaySprite_Bind_m3777595890 (UIFBXDisplaySprite_t2233392895 * __this, int32_t ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIFBXDisplaySprite::UnBind()
extern "C"  void UIFBXDisplaySprite_UnBind_m3398702206 (UIFBXDisplaySprite_t2233392895 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIFBXDisplaySprite::RefreshDrawCall()
extern "C"  void UIFBXDisplaySprite_RefreshDrawCall_m131667159 (UIFBXDisplaySprite_t2233392895 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIFBXDisplaySprite::ilo_set_shader1(UITexture,UnityEngine.Shader)
extern "C"  void UIFBXDisplaySprite_ilo_set_shader1_m1018847829 (Il2CppObject * __this /* static, unused */, UITexture_t3903132647 * ____this0, Shader_t3191267369 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIFBXDisplaySprite::ilo_BindUIFBXSprite2(UIModelDisplayMgr,UIModelDisplayType,UIFBXDisplaySprite)
extern "C"  bool UIFBXDisplaySprite_ilo_BindUIFBXSprite2_m444536926 (Il2CppObject * __this /* static, unused */, UIModelDisplayMgr_t1446490315 * ____this0, int32_t ___type1, UIFBXDisplaySprite_t2233392895 * ___sp2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIModelDisplayMgr UIFBXDisplaySprite::ilo_get_Instance3()
extern "C"  UIModelDisplayMgr_t1446490315 * UIFBXDisplaySprite_ilo_get_Instance3_m920963060 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
