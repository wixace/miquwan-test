﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UISceneCamera
struct  UISceneCamera_t2586519453  : public MonoBehaviour_t667441552
{
public:
	// System.Single UISceneCamera::defaultZ
	float ___defaultZ_2;
	// System.Int32 UISceneCamera::mWidth
	int32_t ___mWidth_3;
	// System.Int32 UISceneCamera::mHeight
	int32_t ___mHeight_4;

public:
	inline static int32_t get_offset_of_defaultZ_2() { return static_cast<int32_t>(offsetof(UISceneCamera_t2586519453, ___defaultZ_2)); }
	inline float get_defaultZ_2() const { return ___defaultZ_2; }
	inline float* get_address_of_defaultZ_2() { return &___defaultZ_2; }
	inline void set_defaultZ_2(float value)
	{
		___defaultZ_2 = value;
	}

	inline static int32_t get_offset_of_mWidth_3() { return static_cast<int32_t>(offsetof(UISceneCamera_t2586519453, ___mWidth_3)); }
	inline int32_t get_mWidth_3() const { return ___mWidth_3; }
	inline int32_t* get_address_of_mWidth_3() { return &___mWidth_3; }
	inline void set_mWidth_3(int32_t value)
	{
		___mWidth_3 = value;
	}

	inline static int32_t get_offset_of_mHeight_4() { return static_cast<int32_t>(offsetof(UISceneCamera_t2586519453, ___mHeight_4)); }
	inline int32_t get_mHeight_4() const { return ___mHeight_4; }
	inline int32_t* get_address_of_mHeight_4() { return &___mHeight_4; }
	inline void set_mHeight_4(int32_t value)
	{
		___mHeight_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
