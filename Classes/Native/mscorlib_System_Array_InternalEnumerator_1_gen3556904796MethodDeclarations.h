﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3556904796.h"
#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DCSharp_Core_RpsResult479594824.h"

// System.Void System.Array/InternalEnumerator`1<Core.RpsResult>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m4258094388_gshared (InternalEnumerator_1_t3556904796 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m4258094388(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t3556904796 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m4258094388_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<Core.RpsResult>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2434876396_gshared (InternalEnumerator_1_t3556904796 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2434876396(__this, method) ((  void (*) (InternalEnumerator_1_t3556904796 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2434876396_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<Core.RpsResult>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m64703960_gshared (InternalEnumerator_1_t3556904796 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m64703960(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t3556904796 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m64703960_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Core.RpsResult>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2089818123_gshared (InternalEnumerator_1_t3556904796 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m2089818123(__this, method) ((  void (*) (InternalEnumerator_1_t3556904796 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2089818123_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Core.RpsResult>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2862277464_gshared (InternalEnumerator_1_t3556904796 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m2862277464(__this, method) ((  bool (*) (InternalEnumerator_1_t3556904796 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m2862277464_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Core.RpsResult>::get_Current()
extern "C"  int32_t InternalEnumerator_1_get_Current_m2977731707_gshared (InternalEnumerator_1_t3556904796 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m2977731707(__this, method) ((  int32_t (*) (InternalEnumerator_1_t3556904796 *, const MethodInfo*))InternalEnumerator_1_get_Current_m2977731707_gshared)(__this, method)
