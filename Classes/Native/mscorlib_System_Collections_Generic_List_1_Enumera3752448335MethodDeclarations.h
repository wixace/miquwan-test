﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1263707397MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.Util.TileHandler/TileType>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m4029033956(__this, ___l0, method) ((  void (*) (Enumerator_t3752448335 *, List_1_t3732775565 *, const MethodInfo*))Enumerator__ctor_m1029849669_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.Util.TileHandler/TileType>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1586808814(__this, method) ((  void (*) (Enumerator_t3752448335 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m771996397_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Pathfinding.Util.TileHandler/TileType>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1967246746(__this, method) ((  Il2CppObject * (*) (Enumerator_t3752448335 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.Util.TileHandler/TileType>::Dispose()
#define Enumerator_Dispose_m1754363081(__this, method) ((  void (*) (Enumerator_t3752448335 *, const MethodInfo*))Enumerator_Dispose_m2904289642_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.Util.TileHandler/TileType>::VerifyState()
#define Enumerator_VerifyState_m2398857090(__this, method) ((  void (*) (Enumerator_t3752448335 *, const MethodInfo*))Enumerator_VerifyState_m1522854819_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Pathfinding.Util.TileHandler/TileType>::MoveNext()
#define Enumerator_MoveNext_m3188428506(__this, method) ((  bool (*) (Enumerator_t3752448335 *, const MethodInfo*))Enumerator_MoveNext_m4284703760_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Pathfinding.Util.TileHandler/TileType>::get_Current()
#define Enumerator_get_Current_m4289065593(__this, method) ((  TileType_t2364590013 * (*) (Enumerator_t3752448335 *, const MethodInfo*))Enumerator_get_Current_m396252160_gshared)(__this, method)
