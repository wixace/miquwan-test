﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen88333914.h"
#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DCSharp_ProductsCfgMgr_ProductInfo1305991238.h"

// System.Void System.Array/InternalEnumerator`1<ProductsCfgMgr/ProductInfo>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2658113879_gshared (InternalEnumerator_1_t88333914 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m2658113879(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t88333914 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m2658113879_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<ProductsCfgMgr/ProductInfo>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m849204969_gshared (InternalEnumerator_1_t88333914 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m849204969(__this, method) ((  void (*) (InternalEnumerator_1_t88333914 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m849204969_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<ProductsCfgMgr/ProductInfo>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m168486549_gshared (InternalEnumerator_1_t88333914 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m168486549(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t88333914 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m168486549_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<ProductsCfgMgr/ProductInfo>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2999292910_gshared (InternalEnumerator_1_t88333914 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m2999292910(__this, method) ((  void (*) (InternalEnumerator_1_t88333914 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2999292910_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<ProductsCfgMgr/ProductInfo>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1585477333_gshared (InternalEnumerator_1_t88333914 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m1585477333(__this, method) ((  bool (*) (InternalEnumerator_1_t88333914 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1585477333_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<ProductsCfgMgr/ProductInfo>::get_Current()
extern "C"  ProductInfo_t1305991238  InternalEnumerator_1_get_Current_m3401990238_gshared (InternalEnumerator_1_t88333914 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m3401990238(__this, method) ((  ProductInfo_t1305991238  (*) (InternalEnumerator_1_t88333914 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3401990238_gshared)(__this, method)
