﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// JSAssetMgr/<RunLoadJavascript>c__Iterator0
struct U3CRunLoadJavascriptU3Ec__Iterator0_t3544505757;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void JSAssetMgr/<RunLoadJavascript>c__Iterator0::.ctor()
extern "C"  void U3CRunLoadJavascriptU3Ec__Iterator0__ctor_m4038286238 (U3CRunLoadJavascriptU3Ec__Iterator0_t3544505757 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object JSAssetMgr/<RunLoadJavascript>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CRunLoadJavascriptU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1318688446 (U3CRunLoadJavascriptU3Ec__Iterator0_t3544505757 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object JSAssetMgr/<RunLoadJavascript>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CRunLoadJavascriptU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2308757074 (U3CRunLoadJavascriptU3Ec__Iterator0_t3544505757 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JSAssetMgr/<RunLoadJavascript>c__Iterator0::MoveNext()
extern "C"  bool U3CRunLoadJavascriptU3Ec__Iterator0_MoveNext_m1484958846 (U3CRunLoadJavascriptU3Ec__Iterator0_t3544505757 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSAssetMgr/<RunLoadJavascript>c__Iterator0::Dispose()
extern "C"  void U3CRunLoadJavascriptU3Ec__Iterator0_Dispose_m2335889499 (U3CRunLoadJavascriptU3Ec__Iterator0_t3544505757 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSAssetMgr/<RunLoadJavascript>c__Iterator0::Reset()
extern "C"  void U3CRunLoadJavascriptU3Ec__Iterator0_Reset_m1684719179 (U3CRunLoadJavascriptU3Ec__Iterator0_t3544505757 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
