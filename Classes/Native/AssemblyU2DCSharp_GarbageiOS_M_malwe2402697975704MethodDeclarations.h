﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_malwe240
struct M_malwe240_t2697975704;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_malwe2402697975704.h"

// System.Void GarbageiOS.M_malwe240::.ctor()
extern "C"  void M_malwe240__ctor_m1788680603 (M_malwe240_t2697975704 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_malwe240::M_sirdowRarhaw0(System.String[],System.Int32)
extern "C"  void M_malwe240_M_sirdowRarhaw0_m3564292035 (M_malwe240_t2697975704 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_malwe240::M_saynuDaste1(System.String[],System.Int32)
extern "C"  void M_malwe240_M_saynuDaste1_m528594490 (M_malwe240_t2697975704 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_malwe240::M_feredrorrePeartearfea2(System.String[],System.Int32)
extern "C"  void M_malwe240_M_feredrorrePeartearfea2_m1360536320 (M_malwe240_t2697975704 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_malwe240::M_fomorTumijo3(System.String[],System.Int32)
extern "C"  void M_malwe240_M_fomorTumijo3_m1847307900 (M_malwe240_t2697975704 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_malwe240::M_sewousere4(System.String[],System.Int32)
extern "C"  void M_malwe240_M_sewousere4_m2314084150 (M_malwe240_t2697975704 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_malwe240::ilo_M_sirdowRarhaw01(GarbageiOS.M_malwe240,System.String[],System.Int32)
extern "C"  void M_malwe240_ilo_M_sirdowRarhaw01_m2497493717 (Il2CppObject * __this /* static, unused */, M_malwe240_t2697975704 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
