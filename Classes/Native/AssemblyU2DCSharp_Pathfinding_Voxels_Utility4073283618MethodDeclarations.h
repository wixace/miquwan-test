﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.Voxels.Utility
struct Utility_t4073283618;
// System.String
struct String_t;
// System.Single[]
struct SingleU5BU5D_t2316563989;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t215400611;
// Pathfinding.Int3[]
struct Int3U5BU5D_t516284607;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_Pathfinding_Int31974045594.h"

// System.Void Pathfinding.Voxels.Utility::.ctor()
extern "C"  void Utility__ctor_m3524105414 (Utility_t4073283618 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Voxels.Utility::.cctor()
extern "C"  void Utility__cctor_m1390989223 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color Pathfinding.Voxels.Utility::GetColor(System.Int32)
extern "C"  Color_t4194546905  Utility_GetColor_m1925576963 (Il2CppObject * __this /* static, unused */, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pathfinding.Voxels.Utility::Bit(System.Int32,System.Int32)
extern "C"  int32_t Utility_Bit_m1444090979 (Il2CppObject * __this /* static, unused */, int32_t ___a0, int32_t ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color Pathfinding.Voxels.Utility::IntToColor(System.Int32,System.Single)
extern "C"  Color_t4194546905  Utility_IntToColor_m2323770548 (Il2CppObject * __this /* static, unused */, int32_t ___i0, float ___a1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Pathfinding.Voxels.Utility::TriangleArea2(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  float Utility_TriangleArea2_m4231167466 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___a0, Vector3_t4282066566  ___b1, Vector3_t4282066566  ___c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Pathfinding.Voxels.Utility::TriangleArea(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  float Utility_TriangleArea_m3508334880 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___a0, Vector3_t4282066566  ___b1, Vector3_t4282066566  ___c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Pathfinding.Voxels.Utility::Min(System.Single,System.Single,System.Single)
extern "C"  float Utility_Min_m2421243467 (Il2CppObject * __this /* static, unused */, float ___a0, float ___b1, float ___c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Pathfinding.Voxels.Utility::Max(System.Single,System.Single,System.Single)
extern "C"  float Utility_Max_m1485260793 (Il2CppObject * __this /* static, unused */, float ___a0, float ___b1, float ___c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pathfinding.Voxels.Utility::Max(System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t Utility_Max_m2739815436 (Il2CppObject * __this /* static, unused */, int32_t ___a0, int32_t ___b1, int32_t ___c2, int32_t ___d3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pathfinding.Voxels.Utility::Min(System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t Utility_Min_m3906862046 (Il2CppObject * __this /* static, unused */, int32_t ___a0, int32_t ___b1, int32_t ___c2, int32_t ___d3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Pathfinding.Voxels.Utility::Max(System.Single,System.Single,System.Single,System.Single)
extern "C"  float Utility_Max_m2661542686 (Il2CppObject * __this /* static, unused */, float ___a0, float ___b1, float ___c2, float ___d3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Pathfinding.Voxels.Utility::Min(System.Single,System.Single,System.Single,System.Single)
extern "C"  float Utility_Min_m2735696368 (Il2CppObject * __this /* static, unused */, float ___a0, float ___b1, float ___c2, float ___d3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Pathfinding.Voxels.Utility::ToMillis(System.Single)
extern "C"  String_t* Utility_ToMillis_m3406975849 (Il2CppObject * __this /* static, unused */, float ___v0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Voxels.Utility::StartTimer()
extern "C"  void Utility_StartTimer_m1126134081 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Voxels.Utility::EndTimer(System.String)
extern "C"  void Utility_EndTimer_m312670746 (Il2CppObject * __this /* static, unused */, String_t* ___label0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Voxels.Utility::StartTimerAdditive(System.Boolean)
extern "C"  void Utility_StartTimerAdditive_m494594276 (Il2CppObject * __this /* static, unused */, bool ___reset0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Voxels.Utility::EndTimerAdditive(System.String,System.Boolean)
extern "C"  void Utility_EndTimerAdditive_m1246873103 (Il2CppObject * __this /* static, unused */, String_t* ___label0, bool ___log1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Voxels.Utility::CopyVector(System.Single[],System.Int32,UnityEngine.Vector3)
extern "C"  void Utility_CopyVector_m188094743 (Il2CppObject * __this /* static, unused */, SingleU5BU5D_t2316563989* ___a0, int32_t ___i1, Vector3_t4282066566  ___v2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pathfinding.Voxels.Utility::ClipPoly(System.Single[],System.Int32,System.Single[],System.Single,System.Single,System.Single)
extern "C"  int32_t Utility_ClipPoly_m2760350312 (Il2CppObject * __this /* static, unused */, SingleU5BU5D_t2316563989* ___vIn0, int32_t ___n1, SingleU5BU5D_t2316563989* ___vOut2, float ___pnx3, float ___pnz4, float ___pd5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pathfinding.Voxels.Utility::ClipPolygon(System.Single[],System.Int32,System.Single[],System.Single,System.Single,System.Int32)
extern "C"  int32_t Utility_ClipPolygon_m1217798748 (Il2CppObject * __this /* static, unused */, SingleU5BU5D_t2316563989* ___vIn0, int32_t ___n1, SingleU5BU5D_t2316563989* ___vOut2, float ___multi3, float ___offset4, int32_t ___axis5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pathfinding.Voxels.Utility::ClipPolygonY(System.Single[],System.Int32,System.Single[],System.Single,System.Single,System.Int32)
extern "C"  int32_t Utility_ClipPolygonY_m3596967553 (Il2CppObject * __this /* static, unused */, SingleU5BU5D_t2316563989* ___vIn0, int32_t ___n1, SingleU5BU5D_t2316563989* ___vOut2, float ___multi3, float ___offset4, int32_t ___axis5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pathfinding.Voxels.Utility::ClipPolygon(UnityEngine.Vector3[],System.Int32,UnityEngine.Vector3[],System.Single,System.Single,System.Int32)
extern "C"  int32_t Utility_ClipPolygon_m3909642012 (Il2CppObject * __this /* static, unused */, Vector3U5BU5D_t215400611* ___vIn0, int32_t ___n1, Vector3U5BU5D_t215400611* ___vOut2, float ___multi3, float ___offset4, int32_t ___axis5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pathfinding.Voxels.Utility::ClipPolygon(Pathfinding.Int3[],System.Int32,Pathfinding.Int3[],System.Int32,System.Int32,System.Int32)
extern "C"  int32_t Utility_ClipPolygon_m1402171656 (Il2CppObject * __this /* static, unused */, Int3U5BU5D_t516284607* ___vIn0, int32_t ___n1, Int3U5BU5D_t516284607* ___vOut2, int32_t ___multi3, int32_t ___offset4, int32_t ___axis5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.Voxels.Utility::IntersectXAxis(UnityEngine.Vector3&,UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern "C"  bool Utility_IntersectXAxis_m778968042 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566 * ___intersection0, Vector3_t4282066566  ___start11, Vector3_t4282066566  ___dir12, float ___x3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.Voxels.Utility::IntersectZAxis(UnityEngine.Vector3&,UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern "C"  bool Utility_IntersectZAxis_m4016097260 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566 * ___intersection0, Vector3_t4282066566  ___start11, Vector3_t4282066566  ___dir12, float ___z3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pathfinding.Voxels.Utility::ilo_Bit1(System.Int32,System.Int32)
extern "C"  int32_t Utility_ilo_Bit1_m2302555139 (Il2CppObject * __this /* static, unused */, int32_t ___a0, int32_t ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Pathfinding.Voxels.Utility::ilo_ToMillis2(System.Single)
extern "C"  String_t* Utility_ilo_ToMillis2_m1574774732 (Il2CppObject * __this /* static, unused */, float ___v0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Int3 Pathfinding.Voxels.Utility::ilo_op_Subtraction3(Pathfinding.Int3,Pathfinding.Int3)
extern "C"  Int3_t1974045594  Utility_ilo_op_Subtraction3_m991268423 (Il2CppObject * __this /* static, unused */, Int3_t1974045594  ___lhs0, Int3_t1974045594  ___rhs1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Int3 Pathfinding.Voxels.Utility::ilo_op_Addition4(Pathfinding.Int3,Pathfinding.Int3)
extern "C"  Int3_t1974045594  Utility_ilo_op_Addition4_m977283788 (Il2CppObject * __this /* static, unused */, Int3_t1974045594  ___lhs0, Int3_t1974045594  ___rhs1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
