﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._34526536dcea38a8e5c83a5471d62607
struct _34526536dcea38a8e5c83a5471d62607_t2491235641;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._34526536dcea38a8e5c83a5471d62607::.ctor()
extern "C"  void _34526536dcea38a8e5c83a5471d62607__ctor_m3358898452 (_34526536dcea38a8e5c83a5471d62607_t2491235641 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._34526536dcea38a8e5c83a5471d62607::_34526536dcea38a8e5c83a5471d62607m2(System.Int32)
extern "C"  int32_t _34526536dcea38a8e5c83a5471d62607__34526536dcea38a8e5c83a5471d62607m2_m823836409 (_34526536dcea38a8e5c83a5471d62607_t2491235641 * __this, int32_t ____34526536dcea38a8e5c83a5471d62607a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._34526536dcea38a8e5c83a5471d62607::_34526536dcea38a8e5c83a5471d62607m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _34526536dcea38a8e5c83a5471d62607__34526536dcea38a8e5c83a5471d62607m_m3157636061 (_34526536dcea38a8e5c83a5471d62607_t2491235641 * __this, int32_t ____34526536dcea38a8e5c83a5471d62607a0, int32_t ____34526536dcea38a8e5c83a5471d62607581, int32_t ____34526536dcea38a8e5c83a5471d62607c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
