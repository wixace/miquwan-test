﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Formula/PlusAtt
struct PlusAtt_t2698014398;

#include "codegen/il2cpp-codegen.h"

// System.Void Formula/PlusAtt::.ctor()
extern "C"  void PlusAtt__ctor_m4168420973 (PlusAtt_t2698014398 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
