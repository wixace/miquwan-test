﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<Pathfinding.NavmeshAdd>
struct List_1_t1669175735;
// UnityEngine.Mesh
struct Mesh_t4241756145;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t215400611;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// UnityEngine.Transform
struct Transform_t1659122786;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "AssemblyU2DCSharp_Pathfinding_NavmeshAdd_MeshType4058623195.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_Bounds2711641849.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.NavmeshAdd
struct  NavmeshAdd_t300990183  : public MonoBehaviour_t667441552
{
public:
	// Pathfinding.NavmeshAdd/MeshType Pathfinding.NavmeshAdd::type
	int32_t ___type_3;
	// UnityEngine.Mesh Pathfinding.NavmeshAdd::mesh
	Mesh_t4241756145 * ___mesh_4;
	// UnityEngine.Vector3[] Pathfinding.NavmeshAdd::verts
	Vector3U5BU5D_t215400611* ___verts_5;
	// System.Int32[] Pathfinding.NavmeshAdd::tris
	Int32U5BU5D_t3230847821* ___tris_6;
	// UnityEngine.Vector2 Pathfinding.NavmeshAdd::rectangleSize
	Vector2_t4282066565  ___rectangleSize_7;
	// System.Single Pathfinding.NavmeshAdd::meshScale
	float ___meshScale_8;
	// UnityEngine.Vector3 Pathfinding.NavmeshAdd::center
	Vector3_t4282066566  ___center_9;
	// UnityEngine.Bounds Pathfinding.NavmeshAdd::bounds
	Bounds_t2711641849  ___bounds_10;
	// System.Boolean Pathfinding.NavmeshAdd::useRotation
	bool ___useRotation_11;
	// UnityEngine.Transform Pathfinding.NavmeshAdd::tr
	Transform_t1659122786 * ___tr_12;

public:
	inline static int32_t get_offset_of_type_3() { return static_cast<int32_t>(offsetof(NavmeshAdd_t300990183, ___type_3)); }
	inline int32_t get_type_3() const { return ___type_3; }
	inline int32_t* get_address_of_type_3() { return &___type_3; }
	inline void set_type_3(int32_t value)
	{
		___type_3 = value;
	}

	inline static int32_t get_offset_of_mesh_4() { return static_cast<int32_t>(offsetof(NavmeshAdd_t300990183, ___mesh_4)); }
	inline Mesh_t4241756145 * get_mesh_4() const { return ___mesh_4; }
	inline Mesh_t4241756145 ** get_address_of_mesh_4() { return &___mesh_4; }
	inline void set_mesh_4(Mesh_t4241756145 * value)
	{
		___mesh_4 = value;
		Il2CppCodeGenWriteBarrier(&___mesh_4, value);
	}

	inline static int32_t get_offset_of_verts_5() { return static_cast<int32_t>(offsetof(NavmeshAdd_t300990183, ___verts_5)); }
	inline Vector3U5BU5D_t215400611* get_verts_5() const { return ___verts_5; }
	inline Vector3U5BU5D_t215400611** get_address_of_verts_5() { return &___verts_5; }
	inline void set_verts_5(Vector3U5BU5D_t215400611* value)
	{
		___verts_5 = value;
		Il2CppCodeGenWriteBarrier(&___verts_5, value);
	}

	inline static int32_t get_offset_of_tris_6() { return static_cast<int32_t>(offsetof(NavmeshAdd_t300990183, ___tris_6)); }
	inline Int32U5BU5D_t3230847821* get_tris_6() const { return ___tris_6; }
	inline Int32U5BU5D_t3230847821** get_address_of_tris_6() { return &___tris_6; }
	inline void set_tris_6(Int32U5BU5D_t3230847821* value)
	{
		___tris_6 = value;
		Il2CppCodeGenWriteBarrier(&___tris_6, value);
	}

	inline static int32_t get_offset_of_rectangleSize_7() { return static_cast<int32_t>(offsetof(NavmeshAdd_t300990183, ___rectangleSize_7)); }
	inline Vector2_t4282066565  get_rectangleSize_7() const { return ___rectangleSize_7; }
	inline Vector2_t4282066565 * get_address_of_rectangleSize_7() { return &___rectangleSize_7; }
	inline void set_rectangleSize_7(Vector2_t4282066565  value)
	{
		___rectangleSize_7 = value;
	}

	inline static int32_t get_offset_of_meshScale_8() { return static_cast<int32_t>(offsetof(NavmeshAdd_t300990183, ___meshScale_8)); }
	inline float get_meshScale_8() const { return ___meshScale_8; }
	inline float* get_address_of_meshScale_8() { return &___meshScale_8; }
	inline void set_meshScale_8(float value)
	{
		___meshScale_8 = value;
	}

	inline static int32_t get_offset_of_center_9() { return static_cast<int32_t>(offsetof(NavmeshAdd_t300990183, ___center_9)); }
	inline Vector3_t4282066566  get_center_9() const { return ___center_9; }
	inline Vector3_t4282066566 * get_address_of_center_9() { return &___center_9; }
	inline void set_center_9(Vector3_t4282066566  value)
	{
		___center_9 = value;
	}

	inline static int32_t get_offset_of_bounds_10() { return static_cast<int32_t>(offsetof(NavmeshAdd_t300990183, ___bounds_10)); }
	inline Bounds_t2711641849  get_bounds_10() const { return ___bounds_10; }
	inline Bounds_t2711641849 * get_address_of_bounds_10() { return &___bounds_10; }
	inline void set_bounds_10(Bounds_t2711641849  value)
	{
		___bounds_10 = value;
	}

	inline static int32_t get_offset_of_useRotation_11() { return static_cast<int32_t>(offsetof(NavmeshAdd_t300990183, ___useRotation_11)); }
	inline bool get_useRotation_11() const { return ___useRotation_11; }
	inline bool* get_address_of_useRotation_11() { return &___useRotation_11; }
	inline void set_useRotation_11(bool value)
	{
		___useRotation_11 = value;
	}

	inline static int32_t get_offset_of_tr_12() { return static_cast<int32_t>(offsetof(NavmeshAdd_t300990183, ___tr_12)); }
	inline Transform_t1659122786 * get_tr_12() const { return ___tr_12; }
	inline Transform_t1659122786 ** get_address_of_tr_12() { return &___tr_12; }
	inline void set_tr_12(Transform_t1659122786 * value)
	{
		___tr_12 = value;
		Il2CppCodeGenWriteBarrier(&___tr_12, value);
	}
};

struct NavmeshAdd_t300990183_StaticFields
{
public:
	// System.Collections.Generic.List`1<Pathfinding.NavmeshAdd> Pathfinding.NavmeshAdd::allCuts
	List_1_t1669175735 * ___allCuts_2;
	// UnityEngine.Color Pathfinding.NavmeshAdd::GizmoColor
	Color_t4194546905  ___GizmoColor_13;

public:
	inline static int32_t get_offset_of_allCuts_2() { return static_cast<int32_t>(offsetof(NavmeshAdd_t300990183_StaticFields, ___allCuts_2)); }
	inline List_1_t1669175735 * get_allCuts_2() const { return ___allCuts_2; }
	inline List_1_t1669175735 ** get_address_of_allCuts_2() { return &___allCuts_2; }
	inline void set_allCuts_2(List_1_t1669175735 * value)
	{
		___allCuts_2 = value;
		Il2CppCodeGenWriteBarrier(&___allCuts_2, value);
	}

	inline static int32_t get_offset_of_GizmoColor_13() { return static_cast<int32_t>(offsetof(NavmeshAdd_t300990183_StaticFields, ___GizmoColor_13)); }
	inline Color_t4194546905  get_GizmoColor_13() const { return ___GizmoColor_13; }
	inline Color_t4194546905 * get_address_of_GizmoColor_13() { return &___GizmoColor_13; }
	inline void set_GizmoColor_13(Color_t4194546905  value)
	{
		___GizmoColor_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
