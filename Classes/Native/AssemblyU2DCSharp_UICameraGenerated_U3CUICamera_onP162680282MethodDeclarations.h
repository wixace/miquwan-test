﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UICameraGenerated/<UICamera_onPress_GetDelegate_member45_arg0>c__AnonStoreyA6
struct U3CUICamera_onPress_GetDelegate_member45_arg0U3Ec__AnonStoreyA6_t162680282;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"

// System.Void UICameraGenerated/<UICamera_onPress_GetDelegate_member45_arg0>c__AnonStoreyA6::.ctor()
extern "C"  void U3CUICamera_onPress_GetDelegate_member45_arg0U3Ec__AnonStoreyA6__ctor_m3449669841 (U3CUICamera_onPress_GetDelegate_member45_arg0U3Ec__AnonStoreyA6_t162680282 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICameraGenerated/<UICamera_onPress_GetDelegate_member45_arg0>c__AnonStoreyA6::<>m__110(UnityEngine.GameObject,System.Boolean)
extern "C"  void U3CUICamera_onPress_GetDelegate_member45_arg0U3Ec__AnonStoreyA6_U3CU3Em__110_m502628397 (U3CUICamera_onPress_GetDelegate_member45_arg0U3Ec__AnonStoreyA6_t162680282 * __this, GameObject_t3674682005 * ___go0, bool ___state1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
