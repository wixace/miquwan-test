﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>
struct DefaultComparer_t2735708096;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24287931429.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::.ctor()
extern "C"  void DefaultComparer__ctor_m967913385_gshared (DefaultComparer_t2735708096 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m967913385(__this, method) ((  void (*) (DefaultComparer_t2735708096 *, const MethodInfo*))DefaultComparer__ctor_m967913385_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m3547266274_gshared (DefaultComparer_t2735708096 * __this, KeyValuePair_2_t4287931429  ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m3547266274(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t2735708096 *, KeyValuePair_2_t4287931429 , const MethodInfo*))DefaultComparer_GetHashCode_m3547266274_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m3411651706_gshared (DefaultComparer_t2735708096 * __this, KeyValuePair_2_t4287931429  ___x0, KeyValuePair_2_t4287931429  ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m3411651706(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t2735708096 *, KeyValuePair_2_t4287931429 , KeyValuePair_2_t4287931429 , const MethodInfo*))DefaultComparer_Equals_m3411651706_gshared)(__this, ___x0, ___y1, method)
