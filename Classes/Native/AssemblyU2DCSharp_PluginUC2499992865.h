﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.String,System.Object>,System.String>
struct Func_2_t4224953495;
// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.String,System.Object>,System.Object>
struct Func_2_t4093571013;
// System.Action
struct Action_t3771233898;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PluginUC
struct  PluginUC_t2499992865  : public MonoBehaviour_t667441552
{
public:
	// System.String PluginUC::sid
	String_t* ___sid_2;

public:
	inline static int32_t get_offset_of_sid_2() { return static_cast<int32_t>(offsetof(PluginUC_t2499992865, ___sid_2)); }
	inline String_t* get_sid_2() const { return ___sid_2; }
	inline String_t** get_address_of_sid_2() { return &___sid_2; }
	inline void set_sid_2(String_t* value)
	{
		___sid_2 = value;
		Il2CppCodeGenWriteBarrier(&___sid_2, value);
	}
};

struct PluginUC_t2499992865_StaticFields
{
public:
	// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.String,System.Object>,System.String> PluginUC::<>f__am$cache1
	Func_2_t4224953495 * ___U3CU3Ef__amU24cache1_3;
	// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.String,System.Object>,System.String> PluginUC::<>f__am$cache2
	Func_2_t4224953495 * ___U3CU3Ef__amU24cache2_4;
	// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.String,System.Object>,System.Object> PluginUC::<>f__am$cache3
	Func_2_t4093571013 * ___U3CU3Ef__amU24cache3_5;
	// System.Action PluginUC::<>f__am$cache4
	Action_t3771233898 * ___U3CU3Ef__amU24cache4_6;
	// System.Action PluginUC::<>f__am$cache5
	Action_t3771233898 * ___U3CU3Ef__amU24cache5_7;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_3() { return static_cast<int32_t>(offsetof(PluginUC_t2499992865_StaticFields, ___U3CU3Ef__amU24cache1_3)); }
	inline Func_2_t4224953495 * get_U3CU3Ef__amU24cache1_3() const { return ___U3CU3Ef__amU24cache1_3; }
	inline Func_2_t4224953495 ** get_address_of_U3CU3Ef__amU24cache1_3() { return &___U3CU3Ef__amU24cache1_3; }
	inline void set_U3CU3Ef__amU24cache1_3(Func_2_t4224953495 * value)
	{
		___U3CU3Ef__amU24cache1_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1_3, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_4() { return static_cast<int32_t>(offsetof(PluginUC_t2499992865_StaticFields, ___U3CU3Ef__amU24cache2_4)); }
	inline Func_2_t4224953495 * get_U3CU3Ef__amU24cache2_4() const { return ___U3CU3Ef__amU24cache2_4; }
	inline Func_2_t4224953495 ** get_address_of_U3CU3Ef__amU24cache2_4() { return &___U3CU3Ef__amU24cache2_4; }
	inline void set_U3CU3Ef__amU24cache2_4(Func_2_t4224953495 * value)
	{
		___U3CU3Ef__amU24cache2_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache2_4, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_5() { return static_cast<int32_t>(offsetof(PluginUC_t2499992865_StaticFields, ___U3CU3Ef__amU24cache3_5)); }
	inline Func_2_t4093571013 * get_U3CU3Ef__amU24cache3_5() const { return ___U3CU3Ef__amU24cache3_5; }
	inline Func_2_t4093571013 ** get_address_of_U3CU3Ef__amU24cache3_5() { return &___U3CU3Ef__amU24cache3_5; }
	inline void set_U3CU3Ef__amU24cache3_5(Func_2_t4093571013 * value)
	{
		___U3CU3Ef__amU24cache3_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache3_5, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache4_6() { return static_cast<int32_t>(offsetof(PluginUC_t2499992865_StaticFields, ___U3CU3Ef__amU24cache4_6)); }
	inline Action_t3771233898 * get_U3CU3Ef__amU24cache4_6() const { return ___U3CU3Ef__amU24cache4_6; }
	inline Action_t3771233898 ** get_address_of_U3CU3Ef__amU24cache4_6() { return &___U3CU3Ef__amU24cache4_6; }
	inline void set_U3CU3Ef__amU24cache4_6(Action_t3771233898 * value)
	{
		___U3CU3Ef__amU24cache4_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache4_6, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache5_7() { return static_cast<int32_t>(offsetof(PluginUC_t2499992865_StaticFields, ___U3CU3Ef__amU24cache5_7)); }
	inline Action_t3771233898 * get_U3CU3Ef__amU24cache5_7() const { return ___U3CU3Ef__amU24cache5_7; }
	inline Action_t3771233898 ** get_address_of_U3CU3Ef__amU24cache5_7() { return &___U3CU3Ef__amU24cache5_7; }
	inline void set_U3CU3Ef__amU24cache5_7(Action_t3771233898 * value)
	{
		___U3CU3Ef__amU24cache5_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache5_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
