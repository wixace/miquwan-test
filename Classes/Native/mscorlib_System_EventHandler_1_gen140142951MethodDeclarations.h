﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_EventHandler_1_gen20799621MethodDeclarations.h"

// System.Void System.EventHandler`1<Pathfinding.Ionic.Zip.AddProgressEventArgs>::.ctor(System.Object,System.IntPtr)
#define EventHandler_1__ctor_m2010312068(__this, ___object0, ___method1, method) ((  void (*) (EventHandler_1_t140142951 *, Il2CppObject *, IntPtr_t, const MethodInfo*))EventHandler_1__ctor_m1337593804_gshared)(__this, ___object0, ___method1, method)
// System.Void System.EventHandler`1<Pathfinding.Ionic.Zip.AddProgressEventArgs>::Invoke(System.Object,TEventArgs)
#define EventHandler_1_Invoke_m1934394199(__this, ___sender0, ___e1, method) ((  void (*) (EventHandler_1_t140142951 *, Il2CppObject *, AddProgressEventArgs_t4290159701 *, const MethodInfo*))EventHandler_1_Invoke_m2623239957_gshared)(__this, ___sender0, ___e1, method)
// System.IAsyncResult System.EventHandler`1<Pathfinding.Ionic.Zip.AddProgressEventArgs>::BeginInvoke(System.Object,TEventArgs,System.AsyncCallback,System.Object)
#define EventHandler_1_BeginInvoke_m3273291842(__this, ___sender0, ___e1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (EventHandler_1_t140142951 *, Il2CppObject *, AddProgressEventArgs_t4290159701 *, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))EventHandler_1_BeginInvoke_m996893970_gshared)(__this, ___sender0, ___e1, ___callback2, ___object3, method)
// System.Void System.EventHandler`1<Pathfinding.Ionic.Zip.AddProgressEventArgs>::EndInvoke(System.IAsyncResult)
#define EventHandler_1_EndInvoke_m3600925076(__this, ___result0, method) ((  void (*) (EventHandler_1_t140142951 *, Il2CppObject *, const MethodInfo*))EventHandler_1_EndInvoke_m2479179740_gshared)(__this, ___result0, method)
