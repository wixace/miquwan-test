﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GestureRecognizerTS_1_GestureEve2002909991MethodDeclarations.h"

// System.Void GestureRecognizerTS`1/GestureEventHandler<TwistGesture>::.ctor(System.Object,System.IntPtr)
#define GestureEventHandler__ctor_m2235551070(__this, ___object0, ___method1, method) ((  void (*) (GestureEventHandler_t2030454774 *, Il2CppObject *, IntPtr_t, const MethodInfo*))GestureEventHandler__ctor_m3273015516_gshared)(__this, ___object0, ___method1, method)
// System.Void GestureRecognizerTS`1/GestureEventHandler<TwistGesture>::Invoke(T)
#define GestureEventHandler_Invoke_m706148646(__this, ___gesture0, method) ((  void (*) (GestureEventHandler_t2030454774 *, TwistGesture_t4198361154 *, const MethodInfo*))GestureEventHandler_Invoke_m61391848_gshared)(__this, ___gesture0, method)
// System.IAsyncResult GestureRecognizerTS`1/GestureEventHandler<TwistGesture>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define GestureEventHandler_BeginInvoke_m4142126203(__this, ___gesture0, ___callback1, ___object2, method) ((  Il2CppObject * (*) (GestureEventHandler_t2030454774 *, TwistGesture_t4198361154 *, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))GestureEventHandler_BeginInvoke_m2633461941_gshared)(__this, ___gesture0, ___callback1, ___object2, method)
// System.Void GestureRecognizerTS`1/GestureEventHandler<TwistGesture>::EndInvoke(System.IAsyncResult)
#define GestureEventHandler_EndInvoke_m1759288942(__this, ___result0, method) ((  void (*) (GestureEventHandler_t2030454774 *, Il2CppObject *, const MethodInfo*))GestureEventHandler_EndInvoke_m1518633196_gshared)(__this, ___result0, method)
