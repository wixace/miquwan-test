﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_chemeWhehibea102
struct M_chemeWhehibea102_t4136819874;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_chemeWhehibea1024136819874.h"

// System.Void GarbageiOS.M_chemeWhehibea102::.ctor()
extern "C"  void M_chemeWhehibea102__ctor_m1826797521 (M_chemeWhehibea102_t4136819874 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_chemeWhehibea102::M_mornisZelbernall0(System.String[],System.Int32)
extern "C"  void M_chemeWhehibea102_M_mornisZelbernall0_m2036287867 (M_chemeWhehibea102_t4136819874 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_chemeWhehibea102::M_celjayJunu1(System.String[],System.Int32)
extern "C"  void M_chemeWhehibea102_M_celjayJunu1_m168496507 (M_chemeWhehibea102_t4136819874 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_chemeWhehibea102::M_nowdaikay2(System.String[],System.Int32)
extern "C"  void M_chemeWhehibea102_M_nowdaikay2_m3319670657 (M_chemeWhehibea102_t4136819874 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_chemeWhehibea102::M_vibearWahas3(System.String[],System.Int32)
extern "C"  void M_chemeWhehibea102_M_vibearWahas3_m3589640582 (M_chemeWhehibea102_t4136819874 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_chemeWhehibea102::M_jisabowHestutee4(System.String[],System.Int32)
extern "C"  void M_chemeWhehibea102_M_jisabowHestutee4_m3043582454 (M_chemeWhehibea102_t4136819874 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_chemeWhehibea102::M_yiwherPisto5(System.String[],System.Int32)
extern "C"  void M_chemeWhehibea102_M_yiwherPisto5_m4217614666 (M_chemeWhehibea102_t4136819874 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_chemeWhehibea102::ilo_M_mornisZelbernall01(GarbageiOS.M_chemeWhehibea102,System.String[],System.Int32)
extern "C"  void M_chemeWhehibea102_ilo_M_mornisZelbernall01_m3149222439 (Il2CppObject * __this /* static, unused */, M_chemeWhehibea102_t4136819874 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_chemeWhehibea102::ilo_M_celjayJunu12(GarbageiOS.M_chemeWhehibea102,System.String[],System.Int32)
extern "C"  void M_chemeWhehibea102_ilo_M_celjayJunu12_m3919106280 (Il2CppObject * __this /* static, unused */, M_chemeWhehibea102_t4136819874 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
