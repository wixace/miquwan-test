﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UISprite
struct UISprite_t661437049;

#include "AssemblyU2DCSharp_FloatTextUnit2362298029.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Float_teamSkill_lei
struct  Float_teamSkill_lei_t2639309954  : public FloatTextUnit_t2362298029
{
public:
	// UISprite Float_teamSkill_lei::grade
	UISprite_t661437049 * ___grade_3;

public:
	inline static int32_t get_offset_of_grade_3() { return static_cast<int32_t>(offsetof(Float_teamSkill_lei_t2639309954, ___grade_3)); }
	inline UISprite_t661437049 * get_grade_3() const { return ___grade_3; }
	inline UISprite_t661437049 ** get_address_of_grade_3() { return &___grade_3; }
	inline void set_grade_3(UISprite_t661437049 * value)
	{
		___grade_3 = value;
		Il2CppCodeGenWriteBarrier(&___grade_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
