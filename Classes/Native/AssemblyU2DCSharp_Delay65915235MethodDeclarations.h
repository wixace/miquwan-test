﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Delay
struct Delay_t65915235;

#include "codegen/il2cpp-codegen.h"

// System.Void Delay::.ctor()
extern "C"  void Delay__ctor_m1625349992 (Delay_t65915235 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Delay::OnEnable()
extern "C"  void Delay_OnEnable_m1411895070 (Delay_t65915235 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Delay::DelayFunc()
extern "C"  void Delay_DelayFunc_m1253618477 (Delay_t65915235 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
