﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Utilities.TypeExtensions/<AllInterfaces>c__Iterator20
struct U3CAllInterfacesU3Ec__Iterator20_t3433998680;
// System.Type
struct Type_t;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Collections.Generic.IEnumerator`1<System.Type>
struct IEnumerator_1_t480043527;

#include "codegen/il2cpp-codegen.h"

// System.Void Newtonsoft.Json.Utilities.TypeExtensions/<AllInterfaces>c__Iterator20::.ctor()
extern "C"  void U3CAllInterfacesU3Ec__Iterator20__ctor_m2943935251 (U3CAllInterfacesU3Ec__Iterator20_t3433998680 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Newtonsoft.Json.Utilities.TypeExtensions/<AllInterfaces>c__Iterator20::System.Collections.Generic.IEnumerator<System.Type>.get_Current()
extern "C"  Type_t * U3CAllInterfacesU3Ec__Iterator20_System_Collections_Generic_IEnumeratorU3CSystem_TypeU3E_get_Current_m2806106448 (U3CAllInterfacesU3Ec__Iterator20_t3433998680 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Utilities.TypeExtensions/<AllInterfaces>c__Iterator20::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CAllInterfacesU3Ec__Iterator20_System_Collections_IEnumerator_get_Current_m3813805939 (U3CAllInterfacesU3Ec__Iterator20_t3433998680 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Newtonsoft.Json.Utilities.TypeExtensions/<AllInterfaces>c__Iterator20::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CAllInterfacesU3Ec__Iterator20_System_Collections_IEnumerable_GetEnumerator_m2106908398 (U3CAllInterfacesU3Ec__Iterator20_t3433998680 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<System.Type> Newtonsoft.Json.Utilities.TypeExtensions/<AllInterfaces>c__Iterator20::System.Collections.Generic.IEnumerable<System.Type>.GetEnumerator()
extern "C"  Il2CppObject* U3CAllInterfacesU3Ec__Iterator20_System_Collections_Generic_IEnumerableU3CSystem_TypeU3E_GetEnumerator_m2378831741 (U3CAllInterfacesU3Ec__Iterator20_t3433998680 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Utilities.TypeExtensions/<AllInterfaces>c__Iterator20::MoveNext()
extern "C"  bool U3CAllInterfacesU3Ec__Iterator20_MoveNext_m3951559937 (U3CAllInterfacesU3Ec__Iterator20_t3433998680 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Utilities.TypeExtensions/<AllInterfaces>c__Iterator20::Dispose()
extern "C"  void U3CAllInterfacesU3Ec__Iterator20_Dispose_m2931578512 (U3CAllInterfacesU3Ec__Iterator20_t3433998680 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Utilities.TypeExtensions/<AllInterfaces>c__Iterator20::Reset()
extern "C"  void U3CAllInterfacesU3Ec__Iterator20_Reset_m590368192 (U3CAllInterfacesU3Ec__Iterator20_t3433998680 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
