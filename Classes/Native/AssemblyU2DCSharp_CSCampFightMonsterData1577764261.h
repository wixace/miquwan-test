﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CSCampFightMonsterData
struct  CSCampFightMonsterData_t1577764261  : public Il2CppObject
{
public:
	// System.Int32 CSCampFightMonsterData::id
	int32_t ___id_0;
	// System.Int32 CSCampFightMonsterData::level
	int32_t ___level_1;
	// System.Int32 CSCampFightMonsterData::attLevel
	int32_t ___attLevel_2;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(CSCampFightMonsterData_t1577764261, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_level_1() { return static_cast<int32_t>(offsetof(CSCampFightMonsterData_t1577764261, ___level_1)); }
	inline int32_t get_level_1() const { return ___level_1; }
	inline int32_t* get_address_of_level_1() { return &___level_1; }
	inline void set_level_1(int32_t value)
	{
		___level_1 = value;
	}

	inline static int32_t get_offset_of_attLevel_2() { return static_cast<int32_t>(offsetof(CSCampFightMonsterData_t1577764261, ___attLevel_2)); }
	inline int32_t get_attLevel_2() const { return ___attLevel_2; }
	inline int32_t* get_address_of_attLevel_2() { return &___attLevel_2; }
	inline void set_attLevel_2(int32_t value)
	{
		___attLevel_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
