﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraShotNodeCfg
struct CameraShotNodeCfg_t3577740835;
// ProtoBuf.IExtension
struct IExtension_t1606339106;

#include "codegen/il2cpp-codegen.h"

// System.Void CameraShotNodeCfg::.ctor()
extern "C"  void CameraShotNodeCfg__ctor_m2109466280 (CameraShotNodeCfg_t3577740835 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.IExtension CameraShotNodeCfg::ProtoBuf.IExtensible.GetExtensionObject(System.Boolean)
extern "C"  Il2CppObject * CameraShotNodeCfg_ProtoBuf_IExtensible_GetExtensionObject_m904288684 (CameraShotNodeCfg_t3577740835 * __this, bool ___createIfMissing0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CameraShotNodeCfg::get_posX()
extern "C"  float CameraShotNodeCfg_get_posX_m1028063037 (CameraShotNodeCfg_t3577740835 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotNodeCfg::set_posX(System.Single)
extern "C"  void CameraShotNodeCfg_set_posX_m2240167630 (CameraShotNodeCfg_t3577740835 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CameraShotNodeCfg::get_posY()
extern "C"  float CameraShotNodeCfg_get_posY_m1028063998 (CameraShotNodeCfg_t3577740835 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotNodeCfg::set_posY(System.Single)
extern "C"  void CameraShotNodeCfg_set_posY_m1729633453 (CameraShotNodeCfg_t3577740835 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CameraShotNodeCfg::get_posZ()
extern "C"  float CameraShotNodeCfg_get_posZ_m1028064959 (CameraShotNodeCfg_t3577740835 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotNodeCfg::set_posZ(System.Single)
extern "C"  void CameraShotNodeCfg_set_posZ_m1219099276 (CameraShotNodeCfg_t3577740835 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CameraShotNodeCfg::get_rotX()
extern "C"  float CameraShotNodeCfg_get_rotX_m1085351130 (CameraShotNodeCfg_t3577740835 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotNodeCfg::set_rotX(System.Single)
extern "C"  void CameraShotNodeCfg_set_rotX_m1904533585 (CameraShotNodeCfg_t3577740835 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CameraShotNodeCfg::get_rotY()
extern "C"  float CameraShotNodeCfg_get_rotY_m1085352091 (CameraShotNodeCfg_t3577740835 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotNodeCfg::set_rotY(System.Single)
extern "C"  void CameraShotNodeCfg_set_rotY_m1393999408 (CameraShotNodeCfg_t3577740835 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CameraShotNodeCfg::get_rotZ()
extern "C"  float CameraShotNodeCfg_get_rotZ_m1085353052 (CameraShotNodeCfg_t3577740835 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotNodeCfg::set_rotZ(System.Single)
extern "C"  void CameraShotNodeCfg_set_rotZ_m883465231 (CameraShotNodeCfg_t3577740835 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CameraShotNodeCfg::get_speed()
extern "C"  float CameraShotNodeCfg_get_speed_m188871632 (CameraShotNodeCfg_t3577740835 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotNodeCfg::set_speed(System.Single)
extern "C"  void CameraShotNodeCfg_set_speed_m3551991451 (CameraShotNodeCfg_t3577740835 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CameraShotNodeCfg::get_focusId()
extern "C"  int32_t CameraShotNodeCfg_get_focusId_m1058292702 (CameraShotNodeCfg_t3577740835 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotNodeCfg::set_focusId(System.Int32)
extern "C"  void CameraShotNodeCfg_set_focusId_m873853741 (CameraShotNodeCfg_t3577740835 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CameraShotNodeCfg::get_smooth()
extern "C"  float CameraShotNodeCfg_get_smooth_m3488574439 (CameraShotNodeCfg_t3577740835 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotNodeCfg::set_smooth(System.Single)
extern "C"  void CameraShotNodeCfg_set_smooth_m4274382436 (CameraShotNodeCfg_t3577740835 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CameraShotNodeCfg::get_view()
extern "C"  float CameraShotNodeCfg_get_view_m1193909534 (CameraShotNodeCfg_t3577740835 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotNodeCfg::set_view(System.Single)
extern "C"  void CameraShotNodeCfg_set_view_m2742613645 (CameraShotNodeCfg_t3577740835 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CameraShotNodeCfg::get_bright()
extern "C"  float CameraShotNodeCfg_get_bright_m3888611891 (CameraShotNodeCfg_t3577740835 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotNodeCfg::set_bright(System.Single)
extern "C"  void CameraShotNodeCfg_set_bright_m2199481240 (CameraShotNodeCfg_t3577740835 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CameraShotNodeCfg::get_orthographic()
extern "C"  float CameraShotNodeCfg_get_orthographic_m290178025 (CameraShotNodeCfg_t3577740835 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotNodeCfg::set_orthographic(System.Single)
extern "C"  void CameraShotNodeCfg_set_orthographic_m1025388962 (CameraShotNodeCfg_t3577740835 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CameraShotNodeCfg::get_isHero()
extern "C"  int32_t CameraShotNodeCfg_get_isHero_m1651648379 (CameraShotNodeCfg_t3577740835 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotNodeCfg::set_isHero(System.Int32)
extern "C"  void CameraShotNodeCfg_set_isHero_m3522874766 (CameraShotNodeCfg_t3577740835 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.IExtension CameraShotNodeCfg::ilo_GetExtensionObject1(ProtoBuf.IExtension&,System.Boolean)
extern "C"  Il2CppObject * CameraShotNodeCfg_ilo_GetExtensionObject1_m2501295316 (Il2CppObject * __this /* static, unused */, Il2CppObject ** ___extensionObject0, bool ___createIfMissing1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
