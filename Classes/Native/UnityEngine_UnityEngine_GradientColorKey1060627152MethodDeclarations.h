﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.GradientColorKey
struct GradientColorKey_t1060627152;
struct GradientColorKey_t1060627152_marshaled_pinvoke;
struct GradientColorKey_t1060627152_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GradientColorKey1060627152.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"

// System.Void UnityEngine.GradientColorKey::.ctor(UnityEngine.Color,System.Single)
extern "C"  void GradientColorKey__ctor_m2887996618 (GradientColorKey_t1060627152 * __this, Color_t4194546905  ___col0, float ___time1, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct GradientColorKey_t1060627152;
struct GradientColorKey_t1060627152_marshaled_pinvoke;

extern "C" void GradientColorKey_t1060627152_marshal_pinvoke(const GradientColorKey_t1060627152& unmarshaled, GradientColorKey_t1060627152_marshaled_pinvoke& marshaled);
extern "C" void GradientColorKey_t1060627152_marshal_pinvoke_back(const GradientColorKey_t1060627152_marshaled_pinvoke& marshaled, GradientColorKey_t1060627152& unmarshaled);
extern "C" void GradientColorKey_t1060627152_marshal_pinvoke_cleanup(GradientColorKey_t1060627152_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct GradientColorKey_t1060627152;
struct GradientColorKey_t1060627152_marshaled_com;

extern "C" void GradientColorKey_t1060627152_marshal_com(const GradientColorKey_t1060627152& unmarshaled, GradientColorKey_t1060627152_marshaled_com& marshaled);
extern "C" void GradientColorKey_t1060627152_marshal_com_back(const GradientColorKey_t1060627152_marshaled_com& marshaled, GradientColorKey_t1060627152& unmarshaled);
extern "C" void GradientColorKey_t1060627152_marshal_com_cleanup(GradientColorKey_t1060627152_marshaled_com& marshaled);
