﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._8f197250ac2f11e38cb110526a6e310d
struct _8f197250ac2f11e38cb110526a6e310d_t3325302827;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__8f197250ac2f11e38cb110523325302827.h"

// System.Void Little._8f197250ac2f11e38cb110526a6e310d::.ctor()
extern "C"  void _8f197250ac2f11e38cb110526a6e310d__ctor_m2600472162 (_8f197250ac2f11e38cb110526a6e310d_t3325302827 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._8f197250ac2f11e38cb110526a6e310d::_8f197250ac2f11e38cb110526a6e310dm2(System.Int32)
extern "C"  int32_t _8f197250ac2f11e38cb110526a6e310d__8f197250ac2f11e38cb110526a6e310dm2_m3610397113 (_8f197250ac2f11e38cb110526a6e310d_t3325302827 * __this, int32_t ____8f197250ac2f11e38cb110526a6e310da0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._8f197250ac2f11e38cb110526a6e310d::_8f197250ac2f11e38cb110526a6e310dm(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _8f197250ac2f11e38cb110526a6e310d__8f197250ac2f11e38cb110526a6e310dm_m3620454685 (_8f197250ac2f11e38cb110526a6e310d_t3325302827 * __this, int32_t ____8f197250ac2f11e38cb110526a6e310da0, int32_t ____8f197250ac2f11e38cb110526a6e310d651, int32_t ____8f197250ac2f11e38cb110526a6e310dc2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._8f197250ac2f11e38cb110526a6e310d::ilo__8f197250ac2f11e38cb110526a6e310dm21(Little._8f197250ac2f11e38cb110526a6e310d,System.Int32)
extern "C"  int32_t _8f197250ac2f11e38cb110526a6e310d_ilo__8f197250ac2f11e38cb110526a6e310dm21_m582861906 (Il2CppObject * __this /* static, unused */, _8f197250ac2f11e38cb110526a6e310d_t3325302827 * ____this0, int32_t ____8f197250ac2f11e38cb110526a6e310da1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
