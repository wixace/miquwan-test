﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UITexture
struct UITexture_t3903132647;
// System.String
struct String_t;
// UnityEngine.Texture
struct Texture_t2526458961;
// UnityEngine.Material
struct Material_t3870600107;
// UnityEngine.Shader
struct Shader_t3191267369;
// BetterList`1<UnityEngine.Vector3>
struct BetterList_1_t1484067282;
// BetterList`1<UnityEngine.Vector2>
struct BetterList_1_t1484067281;
// BetterList`1<UnityEngine.Color32>
struct BetterList_1_t2095821700;
// UIDrawCall
struct UIDrawCall_t913273974;
// UIWidget
struct UIWidget_t769069560;
// UIWidget/OnPostFillCallback
struct OnPostFillCallback_t3030491454;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "UnityEngine_UnityEngine_Texture2526458961.h"
#include "UnityEngine_UnityEngine_Material3870600107.h"
#include "UnityEngine_UnityEngine_Shader3191267369.h"
#include "UnityEngine_UnityEngine_Vector44282066567.h"
#include "UnityEngine_UnityEngine_Rect4241904616.h"
#include "AssemblyU2DCSharp_UIDrawCall913273974.h"
#include "AssemblyU2DCSharp_UIWidget769069560.h"
#include "AssemblyU2DCSharp_UITexture3903132647.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "AssemblyU2DCSharp_UIWidget_OnPostFillCallback3030491454.h"

// System.Void UITexture::.ctor()
extern "C"  void UITexture__ctor_m1606448484 (UITexture_t3903132647 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITexture::UpdateTexture(System.String)
extern "C"  void UITexture_UpdateTexture_m2907046510 (UITexture_t3903132647 * __this, String_t* ___texName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture UITexture::get_mainTexture()
extern "C"  Texture_t2526458961 * UITexture_get_mainTexture_m3400120682 (UITexture_t3903132647 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITexture::set_mainTexture(UnityEngine.Texture)
extern "C"  void UITexture_set_mainTexture_m4209454887 (UITexture_t3903132647 * __this, Texture_t2526458961 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material UITexture::get_material()
extern "C"  Material_t3870600107 * UITexture_get_material_m1341168497 (UITexture_t3903132647 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITexture::set_material(UnityEngine.Material)
extern "C"  void UITexture_set_material_m2912196742 (UITexture_t3903132647 * __this, Material_t3870600107 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Shader UITexture::get_shader()
extern "C"  Shader_t3191267369 * UITexture_get_shader_m3259374125 (UITexture_t3903132647 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITexture::set_shader(UnityEngine.Shader)
extern "C"  void UITexture_set_shader_m265018694 (UITexture_t3903132647 * __this, Shader_t3191267369 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UITexture::get_premultipliedAlpha()
extern "C"  bool UITexture_get_premultipliedAlpha_m1638491133 (UITexture_t3903132647 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector4 UITexture::get_border()
extern "C"  Vector4_t4282066567  UITexture_get_border_m2382663228 (UITexture_t3903132647 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITexture::set_border(UnityEngine.Vector4)
extern "C"  void UITexture_set_border_m2360646895 (UITexture_t3903132647 * __this, Vector4_t4282066567  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect UITexture::get_uvRect()
extern "C"  Rect_t4241904616  UITexture_get_uvRect_m2135926764 (UITexture_t3903132647 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITexture::set_uvRect(UnityEngine.Rect)
extern "C"  void UITexture_set_uvRect_m3913517031 (UITexture_t3903132647 * __this, Rect_t4241904616  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector4 UITexture::get_drawingDimensions()
extern "C"  Vector4_t4282066567  UITexture_get_drawingDimensions_m1970525 (UITexture_t3903132647 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UITexture::get_fixedAspect()
extern "C"  bool UITexture_get_fixedAspect_m1097060057 (UITexture_t3903132647 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITexture::set_fixedAspect(System.Boolean)
extern "C"  void UITexture_set_fixedAspect_m3934661096 (UITexture_t3903132647 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITexture::MakePixelPerfect()
extern "C"  void UITexture_MakePixelPerfect_m2252403541 (UITexture_t3903132647 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITexture::OnUpdate()
extern "C"  void UITexture_OnUpdate_m638189928 (UITexture_t3903132647 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITexture::OnFill(BetterList`1<UnityEngine.Vector3>,BetterList`1<UnityEngine.Vector2>,BetterList`1<UnityEngine.Color32>)
extern "C"  void UITexture_OnFill_m2476806963 (UITexture_t3903132647 * __this, BetterList_1_t1484067282 * ___verts0, BetterList_1_t1484067281 * ___uvs1, BetterList_1_t2095821700 * ___cols2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITexture::ilo_set_mainTexture1(UIDrawCall,UnityEngine.Texture)
extern "C"  void UITexture_ilo_set_mainTexture1_m444173105 (Il2CppObject * __this /* static, unused */, UIDrawCall_t913273974 * ____this0, Texture_t2526458961 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITexture::ilo_MarkAsChanged2(UIWidget)
extern "C"  void UITexture_ilo_MarkAsChanged2_m3679270168 (Il2CppObject * __this /* static, unused */, UIWidget_t769069560 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITexture::ilo_RemoveFromPanel3(UIWidget)
extern "C"  void UITexture_ilo_RemoveFromPanel3_m2370705976 (Il2CppObject * __this /* static, unused */, UIWidget_t769069560 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITexture::ilo_set_shader4(UIDrawCall,UnityEngine.Shader)
extern "C"  void UITexture_ilo_set_shader4_m1965401375 (Il2CppObject * __this /* static, unused */, UIDrawCall_t913273974 * ____this0, Shader_t3191267369 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material UITexture::ilo_get_material5(UITexture)
extern "C"  Material_t3870600107 * UITexture_ilo_get_material5_m3815330712 (Il2CppObject * __this /* static, unused */, UITexture_t3903132647 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UITexture::ilo_get_pivotOffset6(UIWidget)
extern "C"  Vector2_t4282066565  UITexture_ilo_get_pivotOffset6_m1265438186 (Il2CppObject * __this /* static, unused */, UIWidget_t769069560 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture UITexture::ilo_get_mainTexture7(UITexture)
extern "C"  Texture_t2526458961 * UITexture_ilo_get_mainTexture7_m252200393 (Il2CppObject * __this /* static, unused */, UITexture_t3903132647 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UITexture::ilo_get_mult8(UIWidget)
extern "C"  float UITexture_ilo_get_mult8_m2899499650 (Il2CppObject * __this /* static, unused */, UIWidget_t769069560 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITexture::ilo_set_width9(UIWidget,System.Int32)
extern "C"  void UITexture_ilo_set_width9_m4179106764 (Il2CppObject * __this /* static, unused */, UIWidget_t769069560 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITexture::ilo_set_height10(UIWidget,System.Int32)
extern "C"  void UITexture_ilo_set_height10_m3665582041 (Il2CppObject * __this /* static, unused */, UIWidget_t769069560 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITexture::ilo_set_drawRegion11(UIWidget,UnityEngine.Vector4)
extern "C"  void UITexture_ilo_set_drawRegion11_m4216972642 (Il2CppObject * __this /* static, unused */, UIWidget_t769069560 * ____this0, Vector4_t4282066567  ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITexture::ilo_Invoke12(UIWidget/OnPostFillCallback,UIWidget,System.Int32,BetterList`1<UnityEngine.Vector3>,BetterList`1<UnityEngine.Vector2>,BetterList`1<UnityEngine.Color32>)
extern "C"  void UITexture_ilo_Invoke12_m1203836000 (Il2CppObject * __this /* static, unused */, OnPostFillCallback_t3030491454 * ____this0, UIWidget_t769069560 * ___widget1, int32_t ___bufferOffset2, BetterList_1_t1484067282 * ___verts3, BetterList_1_t1484067281 * ___uvs4, BetterList_1_t2095821700 * ___cols5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
