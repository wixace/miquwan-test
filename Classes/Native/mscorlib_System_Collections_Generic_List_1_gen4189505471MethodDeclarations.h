﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/IntersectionPair>
struct List_1_t4189505471;
// System.Collections.Generic.IEnumerable`1<Pathfinding.LocalAvoidance/IntersectionPair>
struct IEnumerable_1_t1827265580;
// System.Collections.Generic.IEnumerator`1<Pathfinding.LocalAvoidance/IntersectionPair>
struct IEnumerator_1_t438217672;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.ICollection`1<Pathfinding.LocalAvoidance/IntersectionPair>
struct ICollection_1_t3715909906;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.LocalAvoidance/IntersectionPair>
struct ReadOnlyCollection_1_t83430159;
// Pathfinding.LocalAvoidance/IntersectionPair[]
struct IntersectionPairU5BU5D_t3912429174;
// System.Predicate`1<Pathfinding.LocalAvoidance/IntersectionPair>
struct Predicate_1_t2432376802;
// System.Collections.Generic.IComparer`1<Pathfinding.LocalAvoidance/IntersectionPair>
struct IComparer_1_t1101366665;
// System.Comparison`1<Pathfinding.LocalAvoidance/IntersectionPair>
struct Comparison_1_t1537681106;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_Pathfinding_LocalAvoidance_Inter2821319919.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera4209178241.h"

// System.Void System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/IntersectionPair>::.ctor()
extern "C"  void List_1__ctor_m3904413902_gshared (List_1_t4189505471 * __this, const MethodInfo* method);
#define List_1__ctor_m3904413902(__this, method) ((  void (*) (List_1_t4189505471 *, const MethodInfo*))List_1__ctor_m3904413902_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/IntersectionPair>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1__ctor_m3620389946_gshared (List_1_t4189505471 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1__ctor_m3620389946(__this, ___collection0, method) ((  void (*) (List_1_t4189505471 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m3620389946_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/IntersectionPair>::.ctor(System.Int32)
extern "C"  void List_1__ctor_m1780721910_gshared (List_1_t4189505471 * __this, int32_t ___capacity0, const MethodInfo* method);
#define List_1__ctor_m1780721910(__this, ___capacity0, method) ((  void (*) (List_1_t4189505471 *, int32_t, const MethodInfo*))List_1__ctor_m1780721910_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/IntersectionPair>::.cctor()
extern "C"  void List_1__cctor_m3561073960_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m3561073960(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m3561073960_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/IntersectionPair>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2835391031_gshared (List_1_t4189505471 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2835391031(__this, method) ((  Il2CppObject* (*) (List_1_t4189505471 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2835391031_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/IntersectionPair>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m3411300799_gshared (List_1_t4189505471 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m3411300799(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t4189505471 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m3411300799_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/IntersectionPair>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m3233299962_gshared (List_1_t4189505471 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m3233299962(__this, method) ((  Il2CppObject * (*) (List_1_t4189505471 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m3233299962_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/IntersectionPair>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_Add_m3783482295_gshared (List_1_t4189505471 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m3783482295(__this, ___item0, method) ((  int32_t (*) (List_1_t4189505471 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m3783482295_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/IntersectionPair>::System.Collections.IList.Contains(System.Object)
extern "C"  bool List_1_System_Collections_IList_Contains_m1672212469_gshared (List_1_t4189505471 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m1672212469(__this, ___item0, method) ((  bool (*) (List_1_t4189505471 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m1672212469_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/IntersectionPair>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m598793871_gshared (List_1_t4189505471 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m598793871(__this, ___item0, method) ((  int32_t (*) (List_1_t4189505471 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m598793871_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/IntersectionPair>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_Insert_m1889031994_gshared (List_1_t4189505471 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m1889031994(__this, ___index0, ___item1, method) ((  void (*) (List_1_t4189505471 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m1889031994_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/IntersectionPair>::System.Collections.IList.Remove(System.Object)
extern "C"  void List_1_System_Collections_IList_Remove_m3374062510_gshared (List_1_t4189505471 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m3374062510(__this, ___item0, method) ((  void (*) (List_1_t4189505471 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m3374062510_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/IntersectionPair>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2874683830_gshared (List_1_t4189505471 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2874683830(__this, method) ((  bool (*) (List_1_t4189505471 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2874683830_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/IntersectionPair>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool List_1_System_Collections_ICollection_get_IsSynchronized_m3845941063_gshared (List_1_t4189505471 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m3845941063(__this, method) ((  bool (*) (List_1_t4189505471 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m3845941063_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/IntersectionPair>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m3185398195_gshared (List_1_t4189505471 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m3185398195(__this, method) ((  Il2CppObject * (*) (List_1_t4189505471 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m3185398195_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/IntersectionPair>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool List_1_System_Collections_IList_get_IsFixedSize_m3448765220_gshared (List_1_t4189505471 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m3448765220(__this, method) ((  bool (*) (List_1_t4189505471 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m3448765220_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/IntersectionPair>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_IList_get_IsReadOnly_m1396543317_gshared (List_1_t4189505471 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m1396543317(__this, method) ((  bool (*) (List_1_t4189505471 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m1396543317_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/IntersectionPair>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m4216562298_gshared (List_1_t4189505471 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m4216562298(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t4189505471 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m4216562298_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/IntersectionPair>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_set_Item_m3497499793_gshared (List_1_t4189505471 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m3497499793(__this, ___index0, ___value1, method) ((  void (*) (List_1_t4189505471 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m3497499793_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/IntersectionPair>::Add(T)
extern "C"  void List_1_Add_m3598774718_gshared (List_1_t4189505471 * __this, IntersectionPair_t2821319919  ___item0, const MethodInfo* method);
#define List_1_Add_m3598774718(__this, ___item0, method) ((  void (*) (List_1_t4189505471 *, IntersectionPair_t2821319919 , const MethodInfo*))List_1_Add_m3598774718_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/IntersectionPair>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m1269862901_gshared (List_1_t4189505471 * __this, int32_t ___newCount0, const MethodInfo* method);
#define List_1_GrowIfNeeded_m1269862901(__this, ___newCount0, method) ((  void (*) (List_1_t4189505471 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m1269862901_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/IntersectionPair>::CheckRange(System.Int32,System.Int32)
extern "C"  void List_1_CheckRange_m1382561490_gshared (List_1_t4189505471 * __this, int32_t ___idx0, int32_t ___count1, const MethodInfo* method);
#define List_1_CheckRange_m1382561490(__this, ___idx0, ___count1, method) ((  void (*) (List_1_t4189505471 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m1382561490_gshared)(__this, ___idx0, ___count1, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/IntersectionPair>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m1502860531_gshared (List_1_t4189505471 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddCollection_m1502860531(__this, ___collection0, method) ((  void (*) (List_1_t4189505471 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m1502860531_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/IntersectionPair>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddEnumerable_m763832755_gshared (List_1_t4189505471 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method);
#define List_1_AddEnumerable_m763832755(__this, ___enumerable0, method) ((  void (*) (List_1_t4189505471 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m763832755_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/IntersectionPair>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m3869065476_gshared (List_1_t4189505471 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddRange_m3869065476(__this, ___collection0, method) ((  void (*) (List_1_t4189505471 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m3869065476_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/IntersectionPair>::AsReadOnly()
extern "C"  ReadOnlyCollection_1_t83430159 * List_1_AsReadOnly_m3327649061_gshared (List_1_t4189505471 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m3327649061(__this, method) ((  ReadOnlyCollection_1_t83430159 * (*) (List_1_t4189505471 *, const MethodInfo*))List_1_AsReadOnly_m3327649061_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/IntersectionPair>::BinarySearch(T)
extern "C"  int32_t List_1_BinarySearch_m570998906_gshared (List_1_t4189505471 * __this, IntersectionPair_t2821319919  ___item0, const MethodInfo* method);
#define List_1_BinarySearch_m570998906(__this, ___item0, method) ((  int32_t (*) (List_1_t4189505471 *, IntersectionPair_t2821319919 , const MethodInfo*))List_1_BinarySearch_m570998906_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/IntersectionPair>::Clear()
extern "C"  void List_1_Clear_m2662809424_gshared (List_1_t4189505471 * __this, const MethodInfo* method);
#define List_1_Clear_m2662809424(__this, method) ((  void (*) (List_1_t4189505471 *, const MethodInfo*))List_1_Clear_m2662809424_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/IntersectionPair>::Contains(T)
extern "C"  bool List_1_Contains_m1558270974_gshared (List_1_t4189505471 * __this, IntersectionPair_t2821319919  ___item0, const MethodInfo* method);
#define List_1_Contains_m1558270974(__this, ___item0, method) ((  bool (*) (List_1_t4189505471 *, IntersectionPair_t2821319919 , const MethodInfo*))List_1_Contains_m1558270974_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/IntersectionPair>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m1992094250_gshared (List_1_t4189505471 * __this, IntersectionPairU5BU5D_t3912429174* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_CopyTo_m1992094250(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t4189505471 *, IntersectionPairU5BU5D_t3912429174*, int32_t, const MethodInfo*))List_1_CopyTo_m1992094250_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/IntersectionPair>::Find(System.Predicate`1<T>)
extern "C"  IntersectionPair_t2821319919  List_1_Find_m3506752958_gshared (List_1_t4189505471 * __this, Predicate_1_t2432376802 * ___match0, const MethodInfo* method);
#define List_1_Find_m3506752958(__this, ___match0, method) ((  IntersectionPair_t2821319919  (*) (List_1_t4189505471 *, Predicate_1_t2432376802 *, const MethodInfo*))List_1_Find_m3506752958_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/IntersectionPair>::CheckMatch(System.Predicate`1<T>)
extern "C"  void List_1_CheckMatch_m1275198457_gshared (Il2CppObject * __this /* static, unused */, Predicate_1_t2432376802 * ___match0, const MethodInfo* method);
#define List_1_CheckMatch_m1275198457(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t2432376802 *, const MethodInfo*))List_1_CheckMatch_m1275198457_gshared)(__this /* static, unused */, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/IntersectionPair>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C"  int32_t List_1_GetIndex_m2764482846_gshared (List_1_t4189505471 * __this, int32_t ___startIndex0, int32_t ___count1, Predicate_1_t2432376802 * ___match2, const MethodInfo* method);
#define List_1_GetIndex_m2764482846(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t4189505471 *, int32_t, int32_t, Predicate_1_t2432376802 *, const MethodInfo*))List_1_GetIndex_m2764482846_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/IntersectionPair>::GetEnumerator()
extern "C"  Enumerator_t4209178241  List_1_GetEnumerator_m2595232315_gshared (List_1_t4189505471 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m2595232315(__this, method) ((  Enumerator_t4209178241  (*) (List_1_t4189505471 *, const MethodInfo*))List_1_GetEnumerator_m2595232315_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/IntersectionPair>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m3990984750_gshared (List_1_t4189505471 * __this, IntersectionPair_t2821319919  ___item0, const MethodInfo* method);
#define List_1_IndexOf_m3990984750(__this, ___item0, method) ((  int32_t (*) (List_1_t4189505471 *, IntersectionPair_t2821319919 , const MethodInfo*))List_1_IndexOf_m3990984750_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/IntersectionPair>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m1799159425_gshared (List_1_t4189505471 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method);
#define List_1_Shift_m1799159425(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t4189505471 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m1799159425_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/IntersectionPair>::CheckIndex(System.Int32)
extern "C"  void List_1_CheckIndex_m1738461434_gshared (List_1_t4189505471 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_CheckIndex_m1738461434(__this, ___index0, method) ((  void (*) (List_1_t4189505471 *, int32_t, const MethodInfo*))List_1_CheckIndex_m1738461434_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/IntersectionPair>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m3764751137_gshared (List_1_t4189505471 * __this, int32_t ___index0, IntersectionPair_t2821319919  ___item1, const MethodInfo* method);
#define List_1_Insert_m3764751137(__this, ___index0, ___item1, method) ((  void (*) (List_1_t4189505471 *, int32_t, IntersectionPair_t2821319919 , const MethodInfo*))List_1_Insert_m3764751137_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/IntersectionPair>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_CheckCollection_m1666627286_gshared (List_1_t4189505471 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_CheckCollection_m1666627286(__this, ___collection0, method) ((  void (*) (List_1_t4189505471 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m1666627286_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/IntersectionPair>::Remove(T)
extern "C"  bool List_1_Remove_m3451321209_gshared (List_1_t4189505471 * __this, IntersectionPair_t2821319919  ___item0, const MethodInfo* method);
#define List_1_Remove_m3451321209(__this, ___item0, method) ((  bool (*) (List_1_t4189505471 *, IntersectionPair_t2821319919 , const MethodInfo*))List_1_Remove_m3451321209_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/IntersectionPair>::RemoveAll(System.Predicate`1<T>)
extern "C"  int32_t List_1_RemoveAll_m3827558769_gshared (List_1_t4189505471 * __this, Predicate_1_t2432376802 * ___match0, const MethodInfo* method);
#define List_1_RemoveAll_m3827558769(__this, ___match0, method) ((  int32_t (*) (List_1_t4189505471 *, Predicate_1_t2432376802 *, const MethodInfo*))List_1_RemoveAll_m3827558769_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/IntersectionPair>::RemoveAt(System.Int32)
extern "C"  void List_1_RemoveAt_m1638604007_gshared (List_1_t4189505471 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_RemoveAt_m1638604007(__this, ___index0, method) ((  void (*) (List_1_t4189505471 *, int32_t, const MethodInfo*))List_1_RemoveAt_m1638604007_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/IntersectionPair>::RemoveRange(System.Int32,System.Int32)
extern "C"  void List_1_RemoveRange_m3939912458_gshared (List_1_t4189505471 * __this, int32_t ___index0, int32_t ___count1, const MethodInfo* method);
#define List_1_RemoveRange_m3939912458(__this, ___index0, ___count1, method) ((  void (*) (List_1_t4189505471 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m3939912458_gshared)(__this, ___index0, ___count1, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/IntersectionPair>::Reverse()
extern "C"  void List_1_Reverse_m822034981_gshared (List_1_t4189505471 * __this, const MethodInfo* method);
#define List_1_Reverse_m822034981(__this, method) ((  void (*) (List_1_t4189505471 *, const MethodInfo*))List_1_Reverse_m822034981_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/IntersectionPair>::Sort()
extern "C"  void List_1_Sort_m2027536596_gshared (List_1_t4189505471 * __this, const MethodInfo* method);
#define List_1_Sort_m2027536596(__this, method) ((  void (*) (List_1_t4189505471 *, const MethodInfo*))List_1_Sort_m2027536596_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/IntersectionPair>::Sort(System.Collections.Generic.IComparer`1<T>)
extern "C"  void List_1_Sort_m1519518439_gshared (List_1_t4189505471 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define List_1_Sort_m1519518439(__this, ___comparer0, method) ((  void (*) (List_1_t4189505471 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m1519518439_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/IntersectionPair>::Sort(System.Comparison`1<T>)
extern "C"  void List_1_Sort_m2900131888_gshared (List_1_t4189505471 * __this, Comparison_1_t1537681106 * ___comparison0, const MethodInfo* method);
#define List_1_Sort_m2900131888(__this, ___comparison0, method) ((  void (*) (List_1_t4189505471 *, Comparison_1_t1537681106 *, const MethodInfo*))List_1_Sort_m2900131888_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/IntersectionPair>::ToArray()
extern "C"  IntersectionPairU5BU5D_t3912429174* List_1_ToArray_m718949348_gshared (List_1_t4189505471 * __this, const MethodInfo* method);
#define List_1_ToArray_m718949348(__this, method) ((  IntersectionPairU5BU5D_t3912429174* (*) (List_1_t4189505471 *, const MethodInfo*))List_1_ToArray_m718949348_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/IntersectionPair>::TrimExcess()
extern "C"  void List_1_TrimExcess_m2015030646_gshared (List_1_t4189505471 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m2015030646(__this, method) ((  void (*) (List_1_t4189505471 *, const MethodInfo*))List_1_TrimExcess_m2015030646_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/IntersectionPair>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m1442695518_gshared (List_1_t4189505471 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m1442695518(__this, method) ((  int32_t (*) (List_1_t4189505471 *, const MethodInfo*))List_1_get_Capacity_m1442695518_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/IntersectionPair>::set_Capacity(System.Int32)
extern "C"  void List_1_set_Capacity_m2775019015_gshared (List_1_t4189505471 * __this, int32_t ___value0, const MethodInfo* method);
#define List_1_set_Capacity_m2775019015(__this, ___value0, method) ((  void (*) (List_1_t4189505471 *, int32_t, const MethodInfo*))List_1_set_Capacity_m2775019015_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/IntersectionPair>::get_Count()
extern "C"  int32_t List_1_get_Count_m1622460164_gshared (List_1_t4189505471 * __this, const MethodInfo* method);
#define List_1_get_Count_m1622460164(__this, method) ((  int32_t (*) (List_1_t4189505471 *, const MethodInfo*))List_1_get_Count_m1622460164_gshared)(__this, method)
// T System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/IntersectionPair>::get_Item(System.Int32)
extern "C"  IntersectionPair_t2821319919  List_1_get_Item_m2769512849_gshared (List_1_t4189505471 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_get_Item_m2769512849(__this, ___index0, method) ((  IntersectionPair_t2821319919  (*) (List_1_t4189505471 *, int32_t, const MethodInfo*))List_1_get_Item_m2769512849_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/IntersectionPair>::set_Item(System.Int32,T)
extern "C"  void List_1_set_Item_m3372976056_gshared (List_1_t4189505471 * __this, int32_t ___index0, IntersectionPair_t2821319919  ___value1, const MethodInfo* method);
#define List_1_set_Item_m3372976056(__this, ___index0, ___value1, method) ((  void (*) (List_1_t4189505471 *, int32_t, IntersectionPair_t2821319919 , const MethodInfo*))List_1_set_Item_m3372976056_gshared)(__this, ___index0, ___value1, method)
