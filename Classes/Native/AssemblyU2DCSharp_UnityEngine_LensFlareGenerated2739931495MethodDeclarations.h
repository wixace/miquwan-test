﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_LensFlareGenerated
struct UnityEngine_LensFlareGenerated_t2739931495;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"

// System.Void UnityEngine_LensFlareGenerated::.ctor()
extern "C"  void UnityEngine_LensFlareGenerated__ctor_m1412511508 (UnityEngine_LensFlareGenerated_t2739931495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_LensFlareGenerated::LensFlare_LensFlare1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_LensFlareGenerated_LensFlare_LensFlare1_m2689711364 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_LensFlareGenerated::LensFlare_flare(JSVCall)
extern "C"  void UnityEngine_LensFlareGenerated_LensFlare_flare_m2290567446 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_LensFlareGenerated::LensFlare_brightness(JSVCall)
extern "C"  void UnityEngine_LensFlareGenerated_LensFlare_brightness_m3607018359 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_LensFlareGenerated::LensFlare_fadeSpeed(JSVCall)
extern "C"  void UnityEngine_LensFlareGenerated_LensFlare_fadeSpeed_m544164697 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_LensFlareGenerated::LensFlare_color(JSVCall)
extern "C"  void UnityEngine_LensFlareGenerated_LensFlare_color_m2888155425 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_LensFlareGenerated::__Register()
extern "C"  void UnityEngine_LensFlareGenerated___Register_m79401523 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_LensFlareGenerated::ilo_addJSCSRel1(System.Int32,System.Object)
extern "C"  void UnityEngine_LensFlareGenerated_ilo_addJSCSRel1_m3033387984 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_LensFlareGenerated::ilo_getObject2(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_LensFlareGenerated_ilo_getObject2_m4241275804 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_LensFlareGenerated::ilo_setSingle3(System.Int32,System.Single)
extern "C"  void UnityEngine_LensFlareGenerated_ilo_setSingle3_m864203234 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
