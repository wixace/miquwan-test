﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<System.Action>
struct List_1_t844452154;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WaitMainThread
struct  WaitMainThread_t1427553432  : public Il2CppObject
{
public:

public:
};

struct WaitMainThread_t1427553432_StaticFields
{
public:
	// System.Collections.Generic.List`1<System.Action> WaitMainThread::waitList
	List_1_t844452154 * ___waitList_0;

public:
	inline static int32_t get_offset_of_waitList_0() { return static_cast<int32_t>(offsetof(WaitMainThread_t1427553432_StaticFields, ___waitList_0)); }
	inline List_1_t844452154 * get_waitList_0() const { return ___waitList_0; }
	inline List_1_t844452154 ** get_address_of_waitList_0() { return &___waitList_0; }
	inline void set_waitList_0(List_1_t844452154 * value)
	{
		___waitList_0 = value;
		Il2CppCodeGenWriteBarrier(&___waitList_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
