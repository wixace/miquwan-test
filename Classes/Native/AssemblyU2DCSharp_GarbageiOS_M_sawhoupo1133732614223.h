﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_sawhoupo113
struct  M_sawhoupo113_t3732614223  : public Il2CppObject
{
public:
	// System.Single GarbageiOS.M_sawhoupo113::_sallgaNitrou
	float ____sallgaNitrou_0;
	// System.Int32 GarbageiOS.M_sawhoupo113::_qeteSemmudi
	int32_t ____qeteSemmudi_1;
	// System.Int32 GarbageiOS.M_sawhoupo113::_mesawTesaytere
	int32_t ____mesawTesaytere_2;
	// System.String GarbageiOS.M_sawhoupo113::_tejepelKowkitou
	String_t* ____tejepelKowkitou_3;
	// System.Single GarbageiOS.M_sawhoupo113::_najas
	float ____najas_4;
	// System.String GarbageiOS.M_sawhoupo113::_wemballkowLiqi
	String_t* ____wemballkowLiqi_5;
	// System.String GarbageiOS.M_sawhoupo113::_cisdoozemSotremsere
	String_t* ____cisdoozemSotremsere_6;
	// System.Int32 GarbageiOS.M_sawhoupo113::_whorrejere
	int32_t ____whorrejere_7;
	// System.UInt32 GarbageiOS.M_sawhoupo113::_salrir
	uint32_t ____salrir_8;
	// System.Boolean GarbageiOS.M_sawhoupo113::_genouke
	bool ____genouke_9;
	// System.String GarbageiOS.M_sawhoupo113::_geamodree
	String_t* ____geamodree_10;
	// System.Single GarbageiOS.M_sawhoupo113::_heejixe
	float ____heejixe_11;
	// System.Boolean GarbageiOS.M_sawhoupo113::_nurgear
	bool ____nurgear_12;
	// System.Int32 GarbageiOS.M_sawhoupo113::_bowheSalldemco
	int32_t ____bowheSalldemco_13;
	// System.UInt32 GarbageiOS.M_sawhoupo113::_basbayjaWheti
	uint32_t ____basbayjaWheti_14;
	// System.String GarbageiOS.M_sawhoupo113::_torcal
	String_t* ____torcal_15;
	// System.Single GarbageiOS.M_sawhoupo113::_rormiqo
	float ____rormiqo_16;
	// System.Int32 GarbageiOS.M_sawhoupo113::_zemcas
	int32_t ____zemcas_17;
	// System.Int32 GarbageiOS.M_sawhoupo113::_fatekaTeelachem
	int32_t ____fatekaTeelachem_18;
	// System.Boolean GarbageiOS.M_sawhoupo113::_nelrerparGelti
	bool ____nelrerparGelti_19;
	// System.UInt32 GarbageiOS.M_sawhoupo113::_bordeaPejouja
	uint32_t ____bordeaPejouja_20;
	// System.String GarbageiOS.M_sawhoupo113::_serzea
	String_t* ____serzea_21;
	// System.String GarbageiOS.M_sawhoupo113::_zupowCetallku
	String_t* ____zupowCetallku_22;
	// System.String GarbageiOS.M_sawhoupo113::_leredel
	String_t* ____leredel_23;
	// System.UInt32 GarbageiOS.M_sawhoupo113::_vumouMaiki
	uint32_t ____vumouMaiki_24;
	// System.Single GarbageiOS.M_sawhoupo113::_chairpa
	float ____chairpa_25;
	// System.String GarbageiOS.M_sawhoupo113::_bemhirmeSadas
	String_t* ____bemhirmeSadas_26;
	// System.Int32 GarbageiOS.M_sawhoupo113::_jisjaihe
	int32_t ____jisjaihe_27;
	// System.String GarbageiOS.M_sawhoupo113::_cascirmarNassere
	String_t* ____cascirmarNassere_28;
	// System.Int32 GarbageiOS.M_sawhoupo113::_birteeme
	int32_t ____birteeme_29;
	// System.Int32 GarbageiOS.M_sawhoupo113::_lemhurWhehini
	int32_t ____lemhurWhehini_30;
	// System.Single GarbageiOS.M_sawhoupo113::_berforaXuhe
	float ____berforaXuhe_31;
	// System.Single GarbageiOS.M_sawhoupo113::_taymasHasstearla
	float ____taymasHasstearla_32;
	// System.Int32 GarbageiOS.M_sawhoupo113::_qichelte
	int32_t ____qichelte_33;

public:
	inline static int32_t get_offset_of__sallgaNitrou_0() { return static_cast<int32_t>(offsetof(M_sawhoupo113_t3732614223, ____sallgaNitrou_0)); }
	inline float get__sallgaNitrou_0() const { return ____sallgaNitrou_0; }
	inline float* get_address_of__sallgaNitrou_0() { return &____sallgaNitrou_0; }
	inline void set__sallgaNitrou_0(float value)
	{
		____sallgaNitrou_0 = value;
	}

	inline static int32_t get_offset_of__qeteSemmudi_1() { return static_cast<int32_t>(offsetof(M_sawhoupo113_t3732614223, ____qeteSemmudi_1)); }
	inline int32_t get__qeteSemmudi_1() const { return ____qeteSemmudi_1; }
	inline int32_t* get_address_of__qeteSemmudi_1() { return &____qeteSemmudi_1; }
	inline void set__qeteSemmudi_1(int32_t value)
	{
		____qeteSemmudi_1 = value;
	}

	inline static int32_t get_offset_of__mesawTesaytere_2() { return static_cast<int32_t>(offsetof(M_sawhoupo113_t3732614223, ____mesawTesaytere_2)); }
	inline int32_t get__mesawTesaytere_2() const { return ____mesawTesaytere_2; }
	inline int32_t* get_address_of__mesawTesaytere_2() { return &____mesawTesaytere_2; }
	inline void set__mesawTesaytere_2(int32_t value)
	{
		____mesawTesaytere_2 = value;
	}

	inline static int32_t get_offset_of__tejepelKowkitou_3() { return static_cast<int32_t>(offsetof(M_sawhoupo113_t3732614223, ____tejepelKowkitou_3)); }
	inline String_t* get__tejepelKowkitou_3() const { return ____tejepelKowkitou_3; }
	inline String_t** get_address_of__tejepelKowkitou_3() { return &____tejepelKowkitou_3; }
	inline void set__tejepelKowkitou_3(String_t* value)
	{
		____tejepelKowkitou_3 = value;
		Il2CppCodeGenWriteBarrier(&____tejepelKowkitou_3, value);
	}

	inline static int32_t get_offset_of__najas_4() { return static_cast<int32_t>(offsetof(M_sawhoupo113_t3732614223, ____najas_4)); }
	inline float get__najas_4() const { return ____najas_4; }
	inline float* get_address_of__najas_4() { return &____najas_4; }
	inline void set__najas_4(float value)
	{
		____najas_4 = value;
	}

	inline static int32_t get_offset_of__wemballkowLiqi_5() { return static_cast<int32_t>(offsetof(M_sawhoupo113_t3732614223, ____wemballkowLiqi_5)); }
	inline String_t* get__wemballkowLiqi_5() const { return ____wemballkowLiqi_5; }
	inline String_t** get_address_of__wemballkowLiqi_5() { return &____wemballkowLiqi_5; }
	inline void set__wemballkowLiqi_5(String_t* value)
	{
		____wemballkowLiqi_5 = value;
		Il2CppCodeGenWriteBarrier(&____wemballkowLiqi_5, value);
	}

	inline static int32_t get_offset_of__cisdoozemSotremsere_6() { return static_cast<int32_t>(offsetof(M_sawhoupo113_t3732614223, ____cisdoozemSotremsere_6)); }
	inline String_t* get__cisdoozemSotremsere_6() const { return ____cisdoozemSotremsere_6; }
	inline String_t** get_address_of__cisdoozemSotremsere_6() { return &____cisdoozemSotremsere_6; }
	inline void set__cisdoozemSotremsere_6(String_t* value)
	{
		____cisdoozemSotremsere_6 = value;
		Il2CppCodeGenWriteBarrier(&____cisdoozemSotremsere_6, value);
	}

	inline static int32_t get_offset_of__whorrejere_7() { return static_cast<int32_t>(offsetof(M_sawhoupo113_t3732614223, ____whorrejere_7)); }
	inline int32_t get__whorrejere_7() const { return ____whorrejere_7; }
	inline int32_t* get_address_of__whorrejere_7() { return &____whorrejere_7; }
	inline void set__whorrejere_7(int32_t value)
	{
		____whorrejere_7 = value;
	}

	inline static int32_t get_offset_of__salrir_8() { return static_cast<int32_t>(offsetof(M_sawhoupo113_t3732614223, ____salrir_8)); }
	inline uint32_t get__salrir_8() const { return ____salrir_8; }
	inline uint32_t* get_address_of__salrir_8() { return &____salrir_8; }
	inline void set__salrir_8(uint32_t value)
	{
		____salrir_8 = value;
	}

	inline static int32_t get_offset_of__genouke_9() { return static_cast<int32_t>(offsetof(M_sawhoupo113_t3732614223, ____genouke_9)); }
	inline bool get__genouke_9() const { return ____genouke_9; }
	inline bool* get_address_of__genouke_9() { return &____genouke_9; }
	inline void set__genouke_9(bool value)
	{
		____genouke_9 = value;
	}

	inline static int32_t get_offset_of__geamodree_10() { return static_cast<int32_t>(offsetof(M_sawhoupo113_t3732614223, ____geamodree_10)); }
	inline String_t* get__geamodree_10() const { return ____geamodree_10; }
	inline String_t** get_address_of__geamodree_10() { return &____geamodree_10; }
	inline void set__geamodree_10(String_t* value)
	{
		____geamodree_10 = value;
		Il2CppCodeGenWriteBarrier(&____geamodree_10, value);
	}

	inline static int32_t get_offset_of__heejixe_11() { return static_cast<int32_t>(offsetof(M_sawhoupo113_t3732614223, ____heejixe_11)); }
	inline float get__heejixe_11() const { return ____heejixe_11; }
	inline float* get_address_of__heejixe_11() { return &____heejixe_11; }
	inline void set__heejixe_11(float value)
	{
		____heejixe_11 = value;
	}

	inline static int32_t get_offset_of__nurgear_12() { return static_cast<int32_t>(offsetof(M_sawhoupo113_t3732614223, ____nurgear_12)); }
	inline bool get__nurgear_12() const { return ____nurgear_12; }
	inline bool* get_address_of__nurgear_12() { return &____nurgear_12; }
	inline void set__nurgear_12(bool value)
	{
		____nurgear_12 = value;
	}

	inline static int32_t get_offset_of__bowheSalldemco_13() { return static_cast<int32_t>(offsetof(M_sawhoupo113_t3732614223, ____bowheSalldemco_13)); }
	inline int32_t get__bowheSalldemco_13() const { return ____bowheSalldemco_13; }
	inline int32_t* get_address_of__bowheSalldemco_13() { return &____bowheSalldemco_13; }
	inline void set__bowheSalldemco_13(int32_t value)
	{
		____bowheSalldemco_13 = value;
	}

	inline static int32_t get_offset_of__basbayjaWheti_14() { return static_cast<int32_t>(offsetof(M_sawhoupo113_t3732614223, ____basbayjaWheti_14)); }
	inline uint32_t get__basbayjaWheti_14() const { return ____basbayjaWheti_14; }
	inline uint32_t* get_address_of__basbayjaWheti_14() { return &____basbayjaWheti_14; }
	inline void set__basbayjaWheti_14(uint32_t value)
	{
		____basbayjaWheti_14 = value;
	}

	inline static int32_t get_offset_of__torcal_15() { return static_cast<int32_t>(offsetof(M_sawhoupo113_t3732614223, ____torcal_15)); }
	inline String_t* get__torcal_15() const { return ____torcal_15; }
	inline String_t** get_address_of__torcal_15() { return &____torcal_15; }
	inline void set__torcal_15(String_t* value)
	{
		____torcal_15 = value;
		Il2CppCodeGenWriteBarrier(&____torcal_15, value);
	}

	inline static int32_t get_offset_of__rormiqo_16() { return static_cast<int32_t>(offsetof(M_sawhoupo113_t3732614223, ____rormiqo_16)); }
	inline float get__rormiqo_16() const { return ____rormiqo_16; }
	inline float* get_address_of__rormiqo_16() { return &____rormiqo_16; }
	inline void set__rormiqo_16(float value)
	{
		____rormiqo_16 = value;
	}

	inline static int32_t get_offset_of__zemcas_17() { return static_cast<int32_t>(offsetof(M_sawhoupo113_t3732614223, ____zemcas_17)); }
	inline int32_t get__zemcas_17() const { return ____zemcas_17; }
	inline int32_t* get_address_of__zemcas_17() { return &____zemcas_17; }
	inline void set__zemcas_17(int32_t value)
	{
		____zemcas_17 = value;
	}

	inline static int32_t get_offset_of__fatekaTeelachem_18() { return static_cast<int32_t>(offsetof(M_sawhoupo113_t3732614223, ____fatekaTeelachem_18)); }
	inline int32_t get__fatekaTeelachem_18() const { return ____fatekaTeelachem_18; }
	inline int32_t* get_address_of__fatekaTeelachem_18() { return &____fatekaTeelachem_18; }
	inline void set__fatekaTeelachem_18(int32_t value)
	{
		____fatekaTeelachem_18 = value;
	}

	inline static int32_t get_offset_of__nelrerparGelti_19() { return static_cast<int32_t>(offsetof(M_sawhoupo113_t3732614223, ____nelrerparGelti_19)); }
	inline bool get__nelrerparGelti_19() const { return ____nelrerparGelti_19; }
	inline bool* get_address_of__nelrerparGelti_19() { return &____nelrerparGelti_19; }
	inline void set__nelrerparGelti_19(bool value)
	{
		____nelrerparGelti_19 = value;
	}

	inline static int32_t get_offset_of__bordeaPejouja_20() { return static_cast<int32_t>(offsetof(M_sawhoupo113_t3732614223, ____bordeaPejouja_20)); }
	inline uint32_t get__bordeaPejouja_20() const { return ____bordeaPejouja_20; }
	inline uint32_t* get_address_of__bordeaPejouja_20() { return &____bordeaPejouja_20; }
	inline void set__bordeaPejouja_20(uint32_t value)
	{
		____bordeaPejouja_20 = value;
	}

	inline static int32_t get_offset_of__serzea_21() { return static_cast<int32_t>(offsetof(M_sawhoupo113_t3732614223, ____serzea_21)); }
	inline String_t* get__serzea_21() const { return ____serzea_21; }
	inline String_t** get_address_of__serzea_21() { return &____serzea_21; }
	inline void set__serzea_21(String_t* value)
	{
		____serzea_21 = value;
		Il2CppCodeGenWriteBarrier(&____serzea_21, value);
	}

	inline static int32_t get_offset_of__zupowCetallku_22() { return static_cast<int32_t>(offsetof(M_sawhoupo113_t3732614223, ____zupowCetallku_22)); }
	inline String_t* get__zupowCetallku_22() const { return ____zupowCetallku_22; }
	inline String_t** get_address_of__zupowCetallku_22() { return &____zupowCetallku_22; }
	inline void set__zupowCetallku_22(String_t* value)
	{
		____zupowCetallku_22 = value;
		Il2CppCodeGenWriteBarrier(&____zupowCetallku_22, value);
	}

	inline static int32_t get_offset_of__leredel_23() { return static_cast<int32_t>(offsetof(M_sawhoupo113_t3732614223, ____leredel_23)); }
	inline String_t* get__leredel_23() const { return ____leredel_23; }
	inline String_t** get_address_of__leredel_23() { return &____leredel_23; }
	inline void set__leredel_23(String_t* value)
	{
		____leredel_23 = value;
		Il2CppCodeGenWriteBarrier(&____leredel_23, value);
	}

	inline static int32_t get_offset_of__vumouMaiki_24() { return static_cast<int32_t>(offsetof(M_sawhoupo113_t3732614223, ____vumouMaiki_24)); }
	inline uint32_t get__vumouMaiki_24() const { return ____vumouMaiki_24; }
	inline uint32_t* get_address_of__vumouMaiki_24() { return &____vumouMaiki_24; }
	inline void set__vumouMaiki_24(uint32_t value)
	{
		____vumouMaiki_24 = value;
	}

	inline static int32_t get_offset_of__chairpa_25() { return static_cast<int32_t>(offsetof(M_sawhoupo113_t3732614223, ____chairpa_25)); }
	inline float get__chairpa_25() const { return ____chairpa_25; }
	inline float* get_address_of__chairpa_25() { return &____chairpa_25; }
	inline void set__chairpa_25(float value)
	{
		____chairpa_25 = value;
	}

	inline static int32_t get_offset_of__bemhirmeSadas_26() { return static_cast<int32_t>(offsetof(M_sawhoupo113_t3732614223, ____bemhirmeSadas_26)); }
	inline String_t* get__bemhirmeSadas_26() const { return ____bemhirmeSadas_26; }
	inline String_t** get_address_of__bemhirmeSadas_26() { return &____bemhirmeSadas_26; }
	inline void set__bemhirmeSadas_26(String_t* value)
	{
		____bemhirmeSadas_26 = value;
		Il2CppCodeGenWriteBarrier(&____bemhirmeSadas_26, value);
	}

	inline static int32_t get_offset_of__jisjaihe_27() { return static_cast<int32_t>(offsetof(M_sawhoupo113_t3732614223, ____jisjaihe_27)); }
	inline int32_t get__jisjaihe_27() const { return ____jisjaihe_27; }
	inline int32_t* get_address_of__jisjaihe_27() { return &____jisjaihe_27; }
	inline void set__jisjaihe_27(int32_t value)
	{
		____jisjaihe_27 = value;
	}

	inline static int32_t get_offset_of__cascirmarNassere_28() { return static_cast<int32_t>(offsetof(M_sawhoupo113_t3732614223, ____cascirmarNassere_28)); }
	inline String_t* get__cascirmarNassere_28() const { return ____cascirmarNassere_28; }
	inline String_t** get_address_of__cascirmarNassere_28() { return &____cascirmarNassere_28; }
	inline void set__cascirmarNassere_28(String_t* value)
	{
		____cascirmarNassere_28 = value;
		Il2CppCodeGenWriteBarrier(&____cascirmarNassere_28, value);
	}

	inline static int32_t get_offset_of__birteeme_29() { return static_cast<int32_t>(offsetof(M_sawhoupo113_t3732614223, ____birteeme_29)); }
	inline int32_t get__birteeme_29() const { return ____birteeme_29; }
	inline int32_t* get_address_of__birteeme_29() { return &____birteeme_29; }
	inline void set__birteeme_29(int32_t value)
	{
		____birteeme_29 = value;
	}

	inline static int32_t get_offset_of__lemhurWhehini_30() { return static_cast<int32_t>(offsetof(M_sawhoupo113_t3732614223, ____lemhurWhehini_30)); }
	inline int32_t get__lemhurWhehini_30() const { return ____lemhurWhehini_30; }
	inline int32_t* get_address_of__lemhurWhehini_30() { return &____lemhurWhehini_30; }
	inline void set__lemhurWhehini_30(int32_t value)
	{
		____lemhurWhehini_30 = value;
	}

	inline static int32_t get_offset_of__berforaXuhe_31() { return static_cast<int32_t>(offsetof(M_sawhoupo113_t3732614223, ____berforaXuhe_31)); }
	inline float get__berforaXuhe_31() const { return ____berforaXuhe_31; }
	inline float* get_address_of__berforaXuhe_31() { return &____berforaXuhe_31; }
	inline void set__berforaXuhe_31(float value)
	{
		____berforaXuhe_31 = value;
	}

	inline static int32_t get_offset_of__taymasHasstearla_32() { return static_cast<int32_t>(offsetof(M_sawhoupo113_t3732614223, ____taymasHasstearla_32)); }
	inline float get__taymasHasstearla_32() const { return ____taymasHasstearla_32; }
	inline float* get_address_of__taymasHasstearla_32() { return &____taymasHasstearla_32; }
	inline void set__taymasHasstearla_32(float value)
	{
		____taymasHasstearla_32 = value;
	}

	inline static int32_t get_offset_of__qichelte_33() { return static_cast<int32_t>(offsetof(M_sawhoupo113_t3732614223, ____qichelte_33)); }
	inline int32_t get__qichelte_33() const { return ____qichelte_33; }
	inline int32_t* get_address_of__qichelte_33() { return &____qichelte_33; }
	inline void set__qichelte_33(int32_t value)
	{
		____qichelte_33 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
