﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_ApplicationGenerated/<Application_RequestAdvertisingIdentifierAsync_GetDelegate_member12_arg0>c__AnonStoreyDA
struct U3CApplication_RequestAdvertisingIdentifierAsync_GetDelegate_member12_arg0U3Ec__AnonStoreyDA_t320088476;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void UnityEngine_ApplicationGenerated/<Application_RequestAdvertisingIdentifierAsync_GetDelegate_member12_arg0>c__AnonStoreyDA::.ctor()
extern "C"  void U3CApplication_RequestAdvertisingIdentifierAsync_GetDelegate_member12_arg0U3Ec__AnonStoreyDA__ctor_m2079738703 (U3CApplication_RequestAdvertisingIdentifierAsync_GetDelegate_member12_arg0U3Ec__AnonStoreyDA_t320088476 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ApplicationGenerated/<Application_RequestAdvertisingIdentifierAsync_GetDelegate_member12_arg0>c__AnonStoreyDA::<>m__17E(System.String,System.Boolean,System.String)
extern "C"  void U3CApplication_RequestAdvertisingIdentifierAsync_GetDelegate_member12_arg0U3Ec__AnonStoreyDA_U3CU3Em__17E_m1588997024 (U3CApplication_RequestAdvertisingIdentifierAsync_GetDelegate_member12_arg0U3Ec__AnonStoreyDA_t320088476 * __this, String_t* ___advertisingId0, bool ___trackingEnabled1, String_t* ___errorMsg2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
