﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_ralldonawMirse221
struct  M_ralldonawMirse221_t3501742419  : public Il2CppObject
{
public:
	// System.Boolean GarbageiOS.M_ralldonawMirse221::_lallsodea
	bool ____lallsodea_0;
	// System.String GarbageiOS.M_ralldonawMirse221::_cuser
	String_t* ____cuser_1;
	// System.Single GarbageiOS.M_ralldonawMirse221::_marhoRougite
	float ____marhoRougite_2;
	// System.String GarbageiOS.M_ralldonawMirse221::_nerpisDirqir
	String_t* ____nerpisDirqir_3;
	// System.Single GarbageiOS.M_ralldonawMirse221::_gersu
	float ____gersu_4;
	// System.Int32 GarbageiOS.M_ralldonawMirse221::_joostu
	int32_t ____joostu_5;
	// System.Int32 GarbageiOS.M_ralldonawMirse221::_kejejarMotalqa
	int32_t ____kejejarMotalqa_6;

public:
	inline static int32_t get_offset_of__lallsodea_0() { return static_cast<int32_t>(offsetof(M_ralldonawMirse221_t3501742419, ____lallsodea_0)); }
	inline bool get__lallsodea_0() const { return ____lallsodea_0; }
	inline bool* get_address_of__lallsodea_0() { return &____lallsodea_0; }
	inline void set__lallsodea_0(bool value)
	{
		____lallsodea_0 = value;
	}

	inline static int32_t get_offset_of__cuser_1() { return static_cast<int32_t>(offsetof(M_ralldonawMirse221_t3501742419, ____cuser_1)); }
	inline String_t* get__cuser_1() const { return ____cuser_1; }
	inline String_t** get_address_of__cuser_1() { return &____cuser_1; }
	inline void set__cuser_1(String_t* value)
	{
		____cuser_1 = value;
		Il2CppCodeGenWriteBarrier(&____cuser_1, value);
	}

	inline static int32_t get_offset_of__marhoRougite_2() { return static_cast<int32_t>(offsetof(M_ralldonawMirse221_t3501742419, ____marhoRougite_2)); }
	inline float get__marhoRougite_2() const { return ____marhoRougite_2; }
	inline float* get_address_of__marhoRougite_2() { return &____marhoRougite_2; }
	inline void set__marhoRougite_2(float value)
	{
		____marhoRougite_2 = value;
	}

	inline static int32_t get_offset_of__nerpisDirqir_3() { return static_cast<int32_t>(offsetof(M_ralldonawMirse221_t3501742419, ____nerpisDirqir_3)); }
	inline String_t* get__nerpisDirqir_3() const { return ____nerpisDirqir_3; }
	inline String_t** get_address_of__nerpisDirqir_3() { return &____nerpisDirqir_3; }
	inline void set__nerpisDirqir_3(String_t* value)
	{
		____nerpisDirqir_3 = value;
		Il2CppCodeGenWriteBarrier(&____nerpisDirqir_3, value);
	}

	inline static int32_t get_offset_of__gersu_4() { return static_cast<int32_t>(offsetof(M_ralldonawMirse221_t3501742419, ____gersu_4)); }
	inline float get__gersu_4() const { return ____gersu_4; }
	inline float* get_address_of__gersu_4() { return &____gersu_4; }
	inline void set__gersu_4(float value)
	{
		____gersu_4 = value;
	}

	inline static int32_t get_offset_of__joostu_5() { return static_cast<int32_t>(offsetof(M_ralldonawMirse221_t3501742419, ____joostu_5)); }
	inline int32_t get__joostu_5() const { return ____joostu_5; }
	inline int32_t* get_address_of__joostu_5() { return &____joostu_5; }
	inline void set__joostu_5(int32_t value)
	{
		____joostu_5 = value;
	}

	inline static int32_t get_offset_of__kejejarMotalqa_6() { return static_cast<int32_t>(offsetof(M_ralldonawMirse221_t3501742419, ____kejejarMotalqa_6)); }
	inline int32_t get__kejejarMotalqa_6() const { return ____kejejarMotalqa_6; }
	inline int32_t* get_address_of__kejejarMotalqa_6() { return &____kejejarMotalqa_6; }
	inline void set__kejejarMotalqa_6(int32_t value)
	{
		____kejejarMotalqa_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
