﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._f130256568377340245d9eeb71b3cca7
struct _f130256568377340245d9eeb71b3cca7_t1643888636;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._f130256568377340245d9eeb71b3cca7::.ctor()
extern "C"  void _f130256568377340245d9eeb71b3cca7__ctor_m2816057777 (_f130256568377340245d9eeb71b3cca7_t1643888636 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._f130256568377340245d9eeb71b3cca7::_f130256568377340245d9eeb71b3cca7m2(System.Int32)
extern "C"  int32_t _f130256568377340245d9eeb71b3cca7__f130256568377340245d9eeb71b3cca7m2_m4007473177 (_f130256568377340245d9eeb71b3cca7_t1643888636 * __this, int32_t ____f130256568377340245d9eeb71b3cca7a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._f130256568377340245d9eeb71b3cca7::_f130256568377340245d9eeb71b3cca7m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _f130256568377340245d9eeb71b3cca7__f130256568377340245d9eeb71b3cca7m_m3314468541 (_f130256568377340245d9eeb71b3cca7_t1643888636 * __this, int32_t ____f130256568377340245d9eeb71b3cca7a0, int32_t ____f130256568377340245d9eeb71b3cca751, int32_t ____f130256568377340245d9eeb71b3cca7c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
