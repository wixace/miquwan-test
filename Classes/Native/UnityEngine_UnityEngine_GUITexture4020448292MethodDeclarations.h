﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.GUITexture
struct GUITexture_t4020448292;
// UnityEngine.Texture
struct Texture_t2526458961;
// UnityEngine.RectOffset
struct RectOffset_t3056157787;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"
#include "UnityEngine_UnityEngine_Texture2526458961.h"
#include "UnityEngine_UnityEngine_Rect4241904616.h"
#include "UnityEngine_UnityEngine_RectOffset3056157787.h"

// System.Void UnityEngine.GUITexture::.ctor()
extern "C"  void GUITexture__ctor_m3181453 (GUITexture_t4020448292 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.GUITexture::get_color()
extern "C"  Color_t4194546905  GUITexture_get_color_m683993630 (GUITexture_t4020448292 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUITexture::set_color(UnityEngine.Color)
extern "C"  void GUITexture_set_color_m3589887669 (GUITexture_t4020448292 * __this, Color_t4194546905  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUITexture::INTERNAL_get_color(UnityEngine.Color&)
extern "C"  void GUITexture_INTERNAL_get_color_m2531539541 (GUITexture_t4020448292 * __this, Color_t4194546905 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUITexture::INTERNAL_set_color(UnityEngine.Color&)
extern "C"  void GUITexture_INTERNAL_set_color_m2258825313 (GUITexture_t4020448292 * __this, Color_t4194546905 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture UnityEngine.GUITexture::get_texture()
extern "C"  Texture_t2526458961 * GUITexture_get_texture_m814751054 (GUITexture_t4020448292 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUITexture::set_texture(UnityEngine.Texture)
extern "C"  void GUITexture_set_texture_m1714285765 (GUITexture_t4020448292 * __this, Texture_t2526458961 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect UnityEngine.GUITexture::get_pixelInset()
extern "C"  Rect_t4241904616  GUITexture_get_pixelInset_m2745549995 (GUITexture_t4020448292 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUITexture::set_pixelInset(UnityEngine.Rect)
extern "C"  void GUITexture_set_pixelInset_m594215280 (GUITexture_t4020448292 * __this, Rect_t4241904616  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUITexture::INTERNAL_get_pixelInset(UnityEngine.Rect&)
extern "C"  void GUITexture_INTERNAL_get_pixelInset_m342714362 (GUITexture_t4020448292 * __this, Rect_t4241904616 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUITexture::INTERNAL_set_pixelInset(UnityEngine.Rect&)
extern "C"  void GUITexture_INTERNAL_set_pixelInset_m4203362310 (GUITexture_t4020448292 * __this, Rect_t4241904616 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RectOffset UnityEngine.GUITexture::get_border()
extern "C"  RectOffset_t3056157787 * GUITexture_get_border_m4232826829 (GUITexture_t4020448292 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUITexture::set_border(UnityEngine.RectOffset)
extern "C"  void GUITexture_set_border_m769542738 (GUITexture_t4020448292 * __this, RectOffset_t3056157787 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
