﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// System.IFormatProvider
struct IFormatProvider_t192740775;
// System.Char[]
struct CharU5BU5D_t3324145743;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Object4170816371.h"

// System.String EString::EFormat(System.String,System.Object)
extern "C"  String_t* EString_EFormat_m1422808458 (Il2CppObject * __this /* static, unused */, String_t* ___format0, Il2CppObject * ___arg01, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String EString::EFormat(System.String,System.Object[])
extern "C"  String_t* EString_EFormat_m1519378984 (Il2CppObject * __this /* static, unused */, String_t* ___format0, ObjectU5BU5D_t1108656482* ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String EString::EFormat(System.IFormatProvider,System.String,System.Object[])
extern "C"  String_t* EString_EFormat_m785528092 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___provider0, String_t* ___format1, ObjectU5BU5D_t1108656482* ___args2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String EString::EFormat(System.String,System.Object,System.Object)
extern "C"  String_t* EString_EFormat_m1016276696 (Il2CppObject * __this /* static, unused */, String_t* ___format0, Il2CppObject * ___arg01, Il2CppObject * ___arg12, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String EString::EFormat(System.String,System.Object,System.Object,System.Object)
extern "C"  String_t* EString_EFormat_m3220215718 (Il2CppObject * __this /* static, unused */, String_t* ___format0, Il2CppObject * ___arg01, Il2CppObject * ___arg12, Il2CppObject * ___arg23, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String EString::EReplace(System.String,System.String,System.String)
extern "C"  String_t* EString_EReplace_m3544730803 (Il2CppObject * __this /* static, unused */, String_t* ___t0, String_t* ___oldString1, String_t* ___newString2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String EString::EReplace(System.String,System.String,System.String,System.String)
extern "C"  String_t* EString_EReplace_m2109194159 (Il2CppObject * __this /* static, unused */, String_t* ___mainStr0, String_t* ___startStr1, String_t* ___endStr2, String_t* ___newStr3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 EString::ECompareOrdinal(System.String,System.String,System.Char[])
extern "C"  int32_t EString_ECompareOrdinal_m131616597 (Il2CppObject * __this /* static, unused */, String_t* ___strA0, String_t* ___strB1, CharU5BU5D_t3324145743* ___separator2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
