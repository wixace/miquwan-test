﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIButtonMessage
struct UIButtonMessage_t4052372033;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "AssemblyU2DCSharp_UIButtonMessage4052372033.h"

// System.Void UIButtonMessage::.ctor()
extern "C"  void UIButtonMessage__ctor_m1026979402 (UIButtonMessage_t4052372033 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonMessage::Start()
extern "C"  void UIButtonMessage_Start_m4269084490 (UIButtonMessage_t4052372033 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonMessage::OnEnable()
extern "C"  void UIButtonMessage_OnEnable_m3762894076 (UIButtonMessage_t4052372033 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonMessage::OnHover(System.Boolean)
extern "C"  void UIButtonMessage_OnHover_m2410697148 (UIButtonMessage_t4052372033 * __this, bool ___isOver0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonMessage::OnPress(System.Boolean)
extern "C"  void UIButtonMessage_OnPress_m330383043 (UIButtonMessage_t4052372033 * __this, bool ___isPressed0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonMessage::OnSelect(System.Boolean)
extern "C"  void UIButtonMessage_OnSelect_m1278705388 (UIButtonMessage_t4052372033 * __this, bool ___isSelected0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonMessage::OnClick()
extern "C"  void UIButtonMessage_OnClick_m928932433 (UIButtonMessage_t4052372033 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonMessage::OnDoubleClick()
extern "C"  void UIButtonMessage_OnDoubleClick_m385633088 (UIButtonMessage_t4052372033 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonMessage::Send()
extern "C"  void UIButtonMessage_Send_m1648251714 (UIButtonMessage_t4052372033 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIButtonMessage::ilo_IsHighlighted1(UnityEngine.GameObject)
extern "C"  bool UIButtonMessage_ilo_IsHighlighted1_m2160201529 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonMessage::ilo_OnHover2(UIButtonMessage,System.Boolean)
extern "C"  void UIButtonMessage_ilo_OnHover2_m642289070 (Il2CppObject * __this /* static, unused */, UIButtonMessage_t4052372033 * ____this0, bool ___isOver1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonMessage::ilo_Send3(UIButtonMessage)
extern "C"  void UIButtonMessage_ilo_Send3_m1796266001 (Il2CppObject * __this /* static, unused */, UIButtonMessage_t4052372033 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
