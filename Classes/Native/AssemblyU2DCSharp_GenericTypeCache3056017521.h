﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Dictionary`2<System.Type,GenericTypeCache/TypeMembers>
struct Dictionary_2_t2474819158;

#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Reflection_BindingFlags1523912596.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GenericTypeCache
struct  GenericTypeCache_t3056017521  : public Il2CppObject
{
public:

public:
};

struct GenericTypeCache_t3056017521_StaticFields
{
public:
	// System.Reflection.BindingFlags GenericTypeCache::BindingFlagsMethod
	int32_t ___BindingFlagsMethod_0;
	// System.Reflection.BindingFlags GenericTypeCache::BindingFlagsProperty
	int32_t ___BindingFlagsProperty_1;
	// System.Reflection.BindingFlags GenericTypeCache::BindingFlagsField
	int32_t ___BindingFlagsField_2;
	// System.Collections.Generic.Dictionary`2<System.Type,GenericTypeCache/TypeMembers> GenericTypeCache::dict
	Dictionary_2_t2474819158 * ___dict_3;

public:
	inline static int32_t get_offset_of_BindingFlagsMethod_0() { return static_cast<int32_t>(offsetof(GenericTypeCache_t3056017521_StaticFields, ___BindingFlagsMethod_0)); }
	inline int32_t get_BindingFlagsMethod_0() const { return ___BindingFlagsMethod_0; }
	inline int32_t* get_address_of_BindingFlagsMethod_0() { return &___BindingFlagsMethod_0; }
	inline void set_BindingFlagsMethod_0(int32_t value)
	{
		___BindingFlagsMethod_0 = value;
	}

	inline static int32_t get_offset_of_BindingFlagsProperty_1() { return static_cast<int32_t>(offsetof(GenericTypeCache_t3056017521_StaticFields, ___BindingFlagsProperty_1)); }
	inline int32_t get_BindingFlagsProperty_1() const { return ___BindingFlagsProperty_1; }
	inline int32_t* get_address_of_BindingFlagsProperty_1() { return &___BindingFlagsProperty_1; }
	inline void set_BindingFlagsProperty_1(int32_t value)
	{
		___BindingFlagsProperty_1 = value;
	}

	inline static int32_t get_offset_of_BindingFlagsField_2() { return static_cast<int32_t>(offsetof(GenericTypeCache_t3056017521_StaticFields, ___BindingFlagsField_2)); }
	inline int32_t get_BindingFlagsField_2() const { return ___BindingFlagsField_2; }
	inline int32_t* get_address_of_BindingFlagsField_2() { return &___BindingFlagsField_2; }
	inline void set_BindingFlagsField_2(int32_t value)
	{
		___BindingFlagsField_2 = value;
	}

	inline static int32_t get_offset_of_dict_3() { return static_cast<int32_t>(offsetof(GenericTypeCache_t3056017521_StaticFields, ___dict_3)); }
	inline Dictionary_2_t2474819158 * get_dict_3() const { return ___dict_3; }
	inline Dictionary_2_t2474819158 ** get_address_of_dict_3() { return &___dict_3; }
	inline void set_dict_3(Dictionary_2_t2474819158 * value)
	{
		___dict_3 = value;
		Il2CppCodeGenWriteBarrier(&___dict_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
