﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_rerehichiRinouke191
struct M_rerehichiRinouke191_t2673755535;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"

// System.Void GarbageiOS.M_rerehichiRinouke191::.ctor()
extern "C"  void M_rerehichiRinouke191__ctor_m3686035124 (M_rerehichiRinouke191_t2673755535 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_rerehichiRinouke191::M_taivai0(System.String[],System.Int32)
extern "C"  void M_rerehichiRinouke191_M_taivai0_m1615652645 (M_rerehichiRinouke191_t2673755535 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_rerehichiRinouke191::M_leanifoSeresturdis1(System.String[],System.Int32)
extern "C"  void M_rerehichiRinouke191_M_leanifoSeresturdis1_m1226634569 (M_rerehichiRinouke191_t2673755535 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_rerehichiRinouke191::M_binearhay2(System.String[],System.Int32)
extern "C"  void M_rerehichiRinouke191_M_binearhay2_m1713527578 (M_rerehichiRinouke191_t2673755535 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_rerehichiRinouke191::M_terraseeJeecor3(System.String[],System.Int32)
extern "C"  void M_rerehichiRinouke191_M_terraseeJeecor3_m3203268203 (M_rerehichiRinouke191_t2673755535 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_rerehichiRinouke191::M_jouhur4(System.String[],System.Int32)
extern "C"  void M_rerehichiRinouke191_M_jouhur4_m1674579094 (M_rerehichiRinouke191_t2673755535 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
