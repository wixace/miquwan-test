﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Newtonsoft.Json.Serialization.JsonSerializerProxy
struct JsonSerializerProxy_t3893567258;
// Newtonsoft.Json.Serialization.JsonFormatterConverter
struct JsonFormatterConverter_t4063403306;
// System.Func`2<System.Reflection.ParameterInfo,System.Reflection.ParameterInfo>
struct Func_2_t3450826049;
// System.Func`2<System.Reflection.ParameterInfo,System.Object>
struct Func_2_t1091201075;
// System.Func`2<Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonProperty>
struct Func_2_t1140624161;
// System.Func`2<Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>
struct Func_2_t3130298474;

#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_Js2068678036.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.JsonSerializerInternalReader
struct  JsonSerializerInternalReader_t3659144454  : public JsonSerializerInternalBase_t2068678036
{
public:
	// Newtonsoft.Json.Serialization.JsonSerializerProxy Newtonsoft.Json.Serialization.JsonSerializerInternalReader::_internalSerializer
	JsonSerializerProxy_t3893567258 * ____internalSerializer_3;
	// Newtonsoft.Json.Serialization.JsonFormatterConverter Newtonsoft.Json.Serialization.JsonSerializerInternalReader::_formatterConverter
	JsonFormatterConverter_t4063403306 * ____formatterConverter_4;

public:
	inline static int32_t get_offset_of__internalSerializer_3() { return static_cast<int32_t>(offsetof(JsonSerializerInternalReader_t3659144454, ____internalSerializer_3)); }
	inline JsonSerializerProxy_t3893567258 * get__internalSerializer_3() const { return ____internalSerializer_3; }
	inline JsonSerializerProxy_t3893567258 ** get_address_of__internalSerializer_3() { return &____internalSerializer_3; }
	inline void set__internalSerializer_3(JsonSerializerProxy_t3893567258 * value)
	{
		____internalSerializer_3 = value;
		Il2CppCodeGenWriteBarrier(&____internalSerializer_3, value);
	}

	inline static int32_t get_offset_of__formatterConverter_4() { return static_cast<int32_t>(offsetof(JsonSerializerInternalReader_t3659144454, ____formatterConverter_4)); }
	inline JsonFormatterConverter_t4063403306 * get__formatterConverter_4() const { return ____formatterConverter_4; }
	inline JsonFormatterConverter_t4063403306 ** get_address_of__formatterConverter_4() { return &____formatterConverter_4; }
	inline void set__formatterConverter_4(JsonFormatterConverter_t4063403306 * value)
	{
		____formatterConverter_4 = value;
		Il2CppCodeGenWriteBarrier(&____formatterConverter_4, value);
	}
};

struct JsonSerializerInternalReader_t3659144454_StaticFields
{
public:
	// System.Func`2<System.Reflection.ParameterInfo,System.Reflection.ParameterInfo> Newtonsoft.Json.Serialization.JsonSerializerInternalReader::<>f__am$cache2
	Func_2_t3450826049 * ___U3CU3Ef__amU24cache2_5;
	// System.Func`2<System.Reflection.ParameterInfo,System.Object> Newtonsoft.Json.Serialization.JsonSerializerInternalReader::<>f__am$cache3
	Func_2_t1091201075 * ___U3CU3Ef__amU24cache3_6;
	// System.Func`2<Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonProperty> Newtonsoft.Json.Serialization.JsonSerializerInternalReader::<>f__am$cache4
	Func_2_t1140624161 * ___U3CU3Ef__amU24cache4_7;
	// System.Func`2<Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence> Newtonsoft.Json.Serialization.JsonSerializerInternalReader::<>f__am$cache5
	Func_2_t3130298474 * ___U3CU3Ef__amU24cache5_8;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_5() { return static_cast<int32_t>(offsetof(JsonSerializerInternalReader_t3659144454_StaticFields, ___U3CU3Ef__amU24cache2_5)); }
	inline Func_2_t3450826049 * get_U3CU3Ef__amU24cache2_5() const { return ___U3CU3Ef__amU24cache2_5; }
	inline Func_2_t3450826049 ** get_address_of_U3CU3Ef__amU24cache2_5() { return &___U3CU3Ef__amU24cache2_5; }
	inline void set_U3CU3Ef__amU24cache2_5(Func_2_t3450826049 * value)
	{
		___U3CU3Ef__amU24cache2_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache2_5, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_6() { return static_cast<int32_t>(offsetof(JsonSerializerInternalReader_t3659144454_StaticFields, ___U3CU3Ef__amU24cache3_6)); }
	inline Func_2_t1091201075 * get_U3CU3Ef__amU24cache3_6() const { return ___U3CU3Ef__amU24cache3_6; }
	inline Func_2_t1091201075 ** get_address_of_U3CU3Ef__amU24cache3_6() { return &___U3CU3Ef__amU24cache3_6; }
	inline void set_U3CU3Ef__amU24cache3_6(Func_2_t1091201075 * value)
	{
		___U3CU3Ef__amU24cache3_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache3_6, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache4_7() { return static_cast<int32_t>(offsetof(JsonSerializerInternalReader_t3659144454_StaticFields, ___U3CU3Ef__amU24cache4_7)); }
	inline Func_2_t1140624161 * get_U3CU3Ef__amU24cache4_7() const { return ___U3CU3Ef__amU24cache4_7; }
	inline Func_2_t1140624161 ** get_address_of_U3CU3Ef__amU24cache4_7() { return &___U3CU3Ef__amU24cache4_7; }
	inline void set_U3CU3Ef__amU24cache4_7(Func_2_t1140624161 * value)
	{
		___U3CU3Ef__amU24cache4_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache4_7, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache5_8() { return static_cast<int32_t>(offsetof(JsonSerializerInternalReader_t3659144454_StaticFields, ___U3CU3Ef__amU24cache5_8)); }
	inline Func_2_t3130298474 * get_U3CU3Ef__amU24cache5_8() const { return ___U3CU3Ef__amU24cache5_8; }
	inline Func_2_t3130298474 ** get_address_of_U3CU3Ef__amU24cache5_8() { return &___U3CU3Ef__amU24cache5_8; }
	inline void set_U3CU3Ef__amU24cache5_8(Func_2_t3130298474 * value)
	{
		___U3CU3Ef__amU24cache5_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache5_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
