﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._29a464b56d93ac5eafcb6fb41727ea3f
struct _29a464b56d93ac5eafcb6fb41727ea3f_t3843496446;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__29a464b56d93ac5eafcb6fb43843496446.h"

// System.Void Little._29a464b56d93ac5eafcb6fb41727ea3f::.ctor()
extern "C"  void _29a464b56d93ac5eafcb6fb41727ea3f__ctor_m762735087 (_29a464b56d93ac5eafcb6fb41727ea3f_t3843496446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._29a464b56d93ac5eafcb6fb41727ea3f::_29a464b56d93ac5eafcb6fb41727ea3fm2(System.Int32)
extern "C"  int32_t _29a464b56d93ac5eafcb6fb41727ea3f__29a464b56d93ac5eafcb6fb41727ea3fm2_m2622742745 (_29a464b56d93ac5eafcb6fb41727ea3f_t3843496446 * __this, int32_t ____29a464b56d93ac5eafcb6fb41727ea3fa0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._29a464b56d93ac5eafcb6fb41727ea3f::_29a464b56d93ac5eafcb6fb41727ea3fm(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _29a464b56d93ac5eafcb6fb41727ea3f__29a464b56d93ac5eafcb6fb41727ea3fm_m3573313021 (_29a464b56d93ac5eafcb6fb41727ea3f_t3843496446 * __this, int32_t ____29a464b56d93ac5eafcb6fb41727ea3fa0, int32_t ____29a464b56d93ac5eafcb6fb41727ea3f541, int32_t ____29a464b56d93ac5eafcb6fb41727ea3fc2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._29a464b56d93ac5eafcb6fb41727ea3f::ilo__29a464b56d93ac5eafcb6fb41727ea3fm21(Little._29a464b56d93ac5eafcb6fb41727ea3f,System.Int32)
extern "C"  int32_t _29a464b56d93ac5eafcb6fb41727ea3f_ilo__29a464b56d93ac5eafcb6fb41727ea3fm21_m154139013 (Il2CppObject * __this /* static, unused */, _29a464b56d93ac5eafcb6fb41727ea3f_t3843496446 * ____this0, int32_t ____29a464b56d93ac5eafcb6fb41727ea3fa1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
