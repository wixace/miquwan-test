﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3485143788.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_407833816.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2146424652_gshared (InternalEnumerator_1_t3485143788 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m2146424652(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t3485143788 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m2146424652_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m521446100_gshared (InternalEnumerator_1_t3485143788 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m521446100(__this, method) ((  void (*) (InternalEnumerator_1_t3485143788 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m521446100_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m417142474_gshared (InternalEnumerator_1_t3485143788 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m417142474(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t3485143788 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m417142474_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m830984227_gshared (InternalEnumerator_1_t3485143788 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m830984227(__this, method) ((  void (*) (InternalEnumerator_1_t3485143788 *, const MethodInfo*))InternalEnumerator_1_Dispose_m830984227_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1615737476_gshared (InternalEnumerator_1_t3485143788 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m1615737476(__this, method) ((  bool (*) (InternalEnumerator_1_t3485143788 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1615737476_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>>::get_Current()
extern "C"  KeyValuePair_2_t407833816  InternalEnumerator_1_get_Current_m550302069_gshared (InternalEnumerator_1_t3485143788 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m550302069(__this, method) ((  KeyValuePair_2_t407833816  (*) (InternalEnumerator_1_t3485143788 *, const MethodInfo*))InternalEnumerator_1_get_Current_m550302069_gshared)(__this, method)
