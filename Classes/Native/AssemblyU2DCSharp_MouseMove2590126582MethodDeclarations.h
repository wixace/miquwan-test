﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MouseMove
struct MouseMove_t2590126582;

#include "codegen/il2cpp-codegen.h"

// System.Void MouseMove::.ctor()
extern "C"  void MouseMove__ctor_m1043511349 (MouseMove_t2590126582 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MouseMove::Start()
extern "C"  void MouseMove_Start_m4285616437 (MouseMove_t2590126582 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MouseMove::Update()
extern "C"  void MouseMove_Update_m4010942840 (MouseMove_t2590126582 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
