﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraHelper
struct CameraHelper_t3196871507;
// CameraSmoothFollow
struct CameraSmoothFollow_t2624612068;
// UnityEngine.Transform
struct Transform_t1659122786;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// System.String
struct String_t;
// TimeMgr
struct TimeMgr_t350708715;
// GlobalGOMgr
struct GlobalGOMgr_t803081773;
// CameraMove
struct CameraMove_t4276106422;
// GameMgr
struct GameMgr_t1469029542;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// EffectMgr
struct EffectMgr_t535289511;
// SoundMgr
struct SoundMgr_t1807284905;
// HeroMgr
struct HeroMgr_t2475965342;
// CombatEntity
struct CombatEntity_t684137495;
// CEvent.EventFunc`1<CEvent.ZEvent>
struct EventFunc_1_t1114914310;
// System.Collections.Generic.List`1<CombatEntity>
struct List_1_t2052323047;
// NpcMgr
struct NpcMgr_t2339534743;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CameraSmoothFollow2624612068.h"
#include "UnityEngine_UnityEngine_Transform1659122786.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_CameraMove4276106422.h"
#include "AssemblyU2DCSharp_GameMgr1469029542.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "AssemblyU2DCSharp_CameraHelper3196871507.h"
#include "AssemblyU2DCSharp_EffectMgr535289511.h"
#include "AssemblyU2DCSharp_CombatEntity684137495.h"
#include "AssemblyU2DCSharp_NpcMgr2339534743.h"

// System.Void CameraHelper::.ctor()
extern "C"  void CameraHelper__ctor_m21666984 (CameraHelper_t3196871507 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraHelper::.cctor()
extern "C"  void CameraHelper__cctor_m189580293 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraHelper::set_csf(CameraSmoothFollow)
extern "C"  void CameraHelper_set_csf_m3696696091 (CameraHelper_t3196871507 * __this, CameraSmoothFollow_t2624612068 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CameraSmoothFollow CameraHelper::get_csf()
extern "C"  CameraSmoothFollow_t2624612068 * CameraHelper_get_csf_m4243058362 (CameraHelper_t3196871507 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CameraHelper::get_inScaler()
extern "C"  bool CameraHelper_get_inScaler_m2386621542 (CameraHelper_t3196871507 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraHelper::Start()
extern "C"  void CameraHelper_Start_m3263772072 (CameraHelper_t3196871507 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraHelper::Update()
extern "C"  void CameraHelper_Update_m2398538597 (CameraHelper_t3196871507 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraHelper::ScalerClear()
extern "C"  void CameraHelper_ScalerClear_m2434704267 (CameraHelper_t3196871507 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraHelper::Scaler(UnityEngine.Transform,System.Single,System.Single,System.Single,UnityEngine.Vector3)
extern "C"  void CameraHelper_Scaler_m3946735419 (CameraHelper_t3196871507 * __this, Transform_t1659122786 * ___target0, float ___zoomInTime1, float ___waitTime2, float ___zoomOutTime3, Vector3_t4282066566  ___zoominoffset4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraHelper::MotionBlur(UnityEngine.Transform,System.Single)
extern "C"  void CameraHelper_MotionBlur_m605317993 (CameraHelper_t3196871507 * __this, Transform_t1659122786 * ___target0, float ___distance1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraHelper::PlayTeamSkillCamera()
extern "C"  void CameraHelper_PlayTeamSkillCamera_m858853419 (CameraHelper_t3196871507 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraHelper::StopTeamSkillCamera()
extern "C"  void CameraHelper_StopTeamSkillCamera_m3963291933 (CameraHelper_t3196871507 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraHelper::OnCameraRXFixed(CEvent.ZEvent)
extern "C"  void CameraHelper_OnCameraRXFixed_m1667262219 (CameraHelper_t3196871507 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraHelper::BreakTeamSkillCamera()
extern "C"  void CameraHelper_BreakTeamSkillCamera_m558477910 (CameraHelper_t3196871507 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraHelper::MoveCamera(System.Boolean,System.Int32,System.Boolean,System.Single,System.Int32)
extern "C"  void CameraHelper_MoveCamera_m4115643459 (CameraHelper_t3196871507 * __this, bool ___isHero0, int32_t ___id1, bool ___isMove2, float ___time3, int32_t ___AIid4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraHelper::ChangeCameraTarget(System.Boolean,System.Int32,System.Boolean,System.Single,System.String)
extern "C"  void CameraHelper_ChangeCameraTarget_m1517150688 (CameraHelper_t3196871507 * __this, bool ___isHero0, int32_t ___id1, bool ___isMove2, float ___time3, String_t* ___objName4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraHelper::MotionBlur(UnityEngine.Transform)
extern "C"  void CameraHelper_MotionBlur_m731600772 (CameraHelper_t3196871507 * __this, Transform_t1659122786 * ___target0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraHelper::ScreenShake(System.Single,System.Single)
extern "C"  void CameraHelper_ScreenShake_m3516682192 (CameraHelper_t3196871507 * __this, float ____fps0, float ____shakeTime1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraHelper::ScreenShake(System.Single,System.Single,System.Single)
extern "C"  void CameraHelper_ScreenShake_m2250698421 (CameraHelper_t3196871507 * __this, float ____fps0, float ____shakeTime1, float ____shakeDelta2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraHelper::LockView(System.Boolean,System.Int32,System.Single,System.Single,System.Single,System.Single,System.String)
extern "C"  void CameraHelper_LockView_m4147648004 (CameraHelper_t3196871507 * __this, bool ___isHero0, int32_t ___id1, float ___fov2, float ___dis3, float ___rx4, float ___ry5, String_t* ___objName6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraHelper::UnLockView()
extern "C"  void CameraHelper_UnLockView_m182486341 (CameraHelper_t3196871507 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraHelper::<MotionBlur>m__3BD()
extern "C"  void CameraHelper_U3CMotionBlurU3Em__3BD_m3698198921 (CameraHelper_t3196871507 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TimeMgr CameraHelper::ilo_get_TimeMgr1()
extern "C"  TimeMgr_t350708715 * CameraHelper_ilo_get_TimeMgr1_m1846062886 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GlobalGOMgr CameraHelper::ilo_get_GlobalGOMgr2()
extern "C"  GlobalGOMgr_t803081773 * CameraHelper_ilo_get_GlobalGOMgr2_m1076888419 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraHelper::ilo_FarAway3(CameraMove)
extern "C"  void CameraHelper_ilo_FarAway3_m1762498247 (Il2CppObject * __this /* static, unused */, CameraMove_t4276106422 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraHelper::ilo_ReActiveGameSpeedBtn4(GameMgr,System.Boolean)
extern "C"  void CameraHelper_ilo_ReActiveGameSpeedBtn4_m4107282104 (Il2CppObject * __this /* static, unused */, GameMgr_t1469029542 * ____this0, bool ____IsActive1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraHelper::ilo_SetLayer5(UnityEngine.GameObject,System.Int32)
extern "C"  void CameraHelper_ilo_SetLayer5_m1554762374 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___go0, int32_t ___layer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// EffectMgr CameraHelper::ilo_get_EffectMgr6()
extern "C"  EffectMgr_t535289511 * CameraHelper_ilo_get_EffectMgr6_m4136225523 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SoundMgr CameraHelper::ilo_get_SoundMgr7()
extern "C"  SoundMgr_t1807284905 * CameraHelper_ilo_get_SoundMgr7_m478282468 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 CameraHelper::ilo_OffsetPos8(CameraSmoothFollow)
extern "C"  Vector3_t4282066566  CameraHelper_ilo_OffsetPos8_m2794098492 (Il2CppObject * __this /* static, unused */, CameraSmoothFollow_t2624612068 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CameraSmoothFollow CameraHelper::ilo_get_csf9(CameraHelper)
extern "C"  CameraSmoothFollow_t2624612068 * CameraHelper_ilo_get_csf9_m4267169633 (Il2CppObject * __this /* static, unused */, CameraHelper_t3196871507 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HeroMgr CameraHelper::ilo_get_instance10()
extern "C"  HeroMgr_t2475965342 * CameraHelper_ilo_get_instance10_m2112508705 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraHelper::ilo_PlayEffect11(EffectMgr,System.Int32,CombatEntity)
extern "C"  void CameraHelper_ilo_PlayEffect11_m961932015 (Il2CppObject * __this /* static, unused */, EffectMgr_t535289511 * ____this0, int32_t ___id1, CombatEntity_t684137495 * ___srcTf2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraHelper::ilo_OnCameraRXFixed12(CameraHelper,CEvent.ZEvent)
extern "C"  void CameraHelper_ilo_OnCameraRXFixed12_m2523578368 (Il2CppObject * __this /* static, unused */, CameraHelper_t3196871507 * ____this0, ZEvent_t3638018500 * ___ev1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraHelper::ilo_RemoveEventListener13(System.String,CEvent.EventFunc`1<CEvent.ZEvent>)
extern "C"  void CameraHelper_ilo_RemoveEventListener13_m261684708 (Il2CppObject * __this /* static, unused */, String_t* ___eventType0, EventFunc_1_t1114914310 * ___func1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraHelper::ilo_DispatchEvent14(CEvent.ZEvent)
extern "C"  void CameraHelper_ilo_DispatchEvent14_m2108545381 (Il2CppObject * __this /* static, unused */, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CombatEntity> CameraHelper::ilo_GetCurRoundMonster15(NpcMgr)
extern "C"  List_1_t2052323047 * CameraHelper_ilo_GetCurRoundMonster15_m1442584124 (Il2CppObject * __this /* static, unused */, NpcMgr_t2339534743 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// NpcMgr CameraHelper::ilo_get_instance16()
extern "C"  NpcMgr_t2339534743 * CameraHelper_ilo_get_instance16_m2631127416 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CombatEntity> CameraHelper::ilo_GetCurRoundOther17(NpcMgr)
extern "C"  List_1_t2052323047 * CameraHelper_ilo_GetCurRoundOther17_m1825189108 (Il2CppObject * __this /* static, unused */, NpcMgr_t2339534743 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
