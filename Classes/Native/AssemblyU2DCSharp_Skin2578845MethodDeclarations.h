﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Skin
struct Skin_t2578845;
// UnityEngine.SkinnedMeshRenderer
struct SkinnedMeshRenderer_t3986041494;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_SkinnedMeshRenderer3986041494.h"

// System.Void Skin::.ctor()
extern "C"  void Skin__ctor_m2459349406 (Skin_t2578845 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Skin::Awake()
extern "C"  void Skin_Awake_m2696954625 (Skin_t2578845 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Skin::InitSkin(System.Int32,UnityEngine.SkinnedMeshRenderer)
extern "C"  void Skin_InitSkin_m2098187431 (Skin_t2578845 * __this, int32_t ___skinID0, SkinnedMeshRenderer_t3986041494 * ___mrenderer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
