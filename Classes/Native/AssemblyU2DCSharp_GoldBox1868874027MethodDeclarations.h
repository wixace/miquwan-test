﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GoldBox
struct GoldBox_t1868874027;
// CombatEntity
struct CombatEntity_t684137495;
// Entity.Behavior.IBehaviorCtrl
struct IBehaviorCtrl_t4225040900;
// AnimationRunner
struct AnimationRunner_t1015409588;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CombatEntity684137495.h"
#include "AssemblyU2DCSharp_Entity_Behavior_IBehaviorCtrl4225040900.h"
#include "AssemblyU2DCSharp_AnimationRunner1015409588.h"
#include "AssemblyU2DCSharp_AnimationRunner_AniType1006238651.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"

// System.Void GoldBox::.ctor()
extern "C"  void GoldBox__ctor_m3118698144 (GoldBox_t1868874027 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoldBox::OnTriggerBehaivir(CombatEntity)
extern "C"  void GoldBox_OnTriggerBehaivir_m4131777490 (GoldBox_t1868874027 * __this, CombatEntity_t684137495 * ___main0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoldBox::Init()
extern "C"  void GoldBox_Init_m1299065780 (GoldBox_t1868874027 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoldBox::UpdateHpBar()
extern "C"  void GoldBox_UpdateHpBar_m1813149728 (GoldBox_t1868874027 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoldBox::Update()
extern "C"  void GoldBox_Update_m3917224045 (GoldBox_t1868874027 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoldBox::OnTargetReached()
extern "C"  void GoldBox_OnTargetReached_m356902368 (GoldBox_t1868874027 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoldBox::ilo_Init1(Entity.Behavior.IBehaviorCtrl)
extern "C"  void GoldBox_ilo_Init1_m742359729 (Il2CppObject * __this /* static, unused */, IBehaviorCtrl_t4225040900 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoldBox::ilo_Play2(AnimationRunner,AnimationRunner/AniType,System.Single,System.Boolean)
extern "C"  void GoldBox_ilo_Play2_m3988857970 (Il2CppObject * __this /* static, unused */, AnimationRunner_t1015409588 * ____this0, int32_t ___aniType1, float ___playTime2, bool ___isskill3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject GoldBox::ilo_Instantiate3(UnityEngine.GameObject)
extern "C"  GameObject_t3674682005 * GoldBox_ilo_Instantiate3_m2557584579 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoldBox::ilo_ChangeParent4(UnityEngine.GameObject,UnityEngine.GameObject,System.Boolean)
extern "C"  void GoldBox_ilo_ChangeParent4_m1712276060 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ____this0, GameObject_t3674682005 * ___parent1, bool ___resetMat2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
