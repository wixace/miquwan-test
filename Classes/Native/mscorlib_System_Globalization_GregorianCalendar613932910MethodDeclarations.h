﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Globalization.GregorianCalendar
struct GregorianCalendar_t613932910;
// System.Int32[]
struct Int32U5BU5D_t3230847821;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Globalization_GregorianCalendarTyp1953288343.h"
#include "mscorlib_System_DateTime4283661327.h"
#include "mscorlib_System_DayOfWeek1779421117.h"

// System.Void System.Globalization.GregorianCalendar::.ctor(System.Globalization.GregorianCalendarTypes)
extern "C"  void GregorianCalendar__ctor_m2553712055 (GregorianCalendar_t613932910 * __this, int32_t ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.GregorianCalendar::.ctor()
extern "C"  void GregorianCalendar__ctor_m1867739610 (GregorianCalendar_t613932910 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] System.Globalization.GregorianCalendar::get_Eras()
extern "C"  Int32U5BU5D_t3230847821* GregorianCalendar_get_Eras_m1588180428 (GregorianCalendar_t613932910 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.GregorianCalendar::set_CalendarType(System.Globalization.GregorianCalendarTypes)
extern "C"  void GregorianCalendar_set_CalendarType_m158688434 (GregorianCalendar_t613932910 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.GregorianCalendar::M_CheckEra(System.Int32&)
extern "C"  void GregorianCalendar_M_CheckEra_m4280612051 (GregorianCalendar_t613932910 * __this, int32_t* ___era0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.GregorianCalendar::M_CheckYE(System.Int32,System.Int32&)
extern "C"  void GregorianCalendar_M_CheckYE_m296343552 (GregorianCalendar_t613932910 * __this, int32_t ___year0, int32_t* ___era1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.GregorianCalendar::M_CheckYME(System.Int32,System.Int32,System.Int32&)
extern "C"  void GregorianCalendar_M_CheckYME_m2875091670 (GregorianCalendar_t613932910 * __this, int32_t ___year0, int32_t ___month1, int32_t* ___era2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.GregorianCalendar::M_CheckYMDE(System.Int32,System.Int32,System.Int32,System.Int32&)
extern "C"  void GregorianCalendar_M_CheckYMDE_m4074428713 (GregorianCalendar_t613932910 * __this, int32_t ___year0, int32_t ___month1, int32_t ___day2, int32_t* ___era3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.GregorianCalendar::GetDayOfMonth(System.DateTime)
extern "C"  int32_t GregorianCalendar_GetDayOfMonth_m4091932953 (GregorianCalendar_t613932910 * __this, DateTime_t4283661327  ___time0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DayOfWeek System.Globalization.GregorianCalendar::GetDayOfWeek(System.DateTime)
extern "C"  int32_t GregorianCalendar_GetDayOfWeek_m3196705338 (GregorianCalendar_t613932910 * __this, DateTime_t4283661327  ___time0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.GregorianCalendar::GetDaysInMonth(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t GregorianCalendar_GetDaysInMonth_m1450053829 (GregorianCalendar_t613932910 * __this, int32_t ___year0, int32_t ___month1, int32_t ___era2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.GregorianCalendar::GetEra(System.DateTime)
extern "C"  int32_t GregorianCalendar_GetEra_m1048758548 (GregorianCalendar_t613932910 * __this, DateTime_t4283661327  ___time0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.GregorianCalendar::GetMonth(System.DateTime)
extern "C"  int32_t GregorianCalendar_GetMonth_m2408004904 (GregorianCalendar_t613932910 * __this, DateTime_t4283661327  ___time0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.GregorianCalendar::GetYear(System.DateTime)
extern "C"  int32_t GregorianCalendar_GetYear_m3821818313 (GregorianCalendar_t613932910 * __this, DateTime_t4283661327  ___time0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.Globalization.GregorianCalendar::ToDateTime(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  DateTime_t4283661327  GregorianCalendar_ToDateTime_m2478382509 (GregorianCalendar_t613932910 * __this, int32_t ___year0, int32_t ___month1, int32_t ___day2, int32_t ___hour3, int32_t ___minute4, int32_t ___second5, int32_t ___millisecond6, int32_t ___era7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
