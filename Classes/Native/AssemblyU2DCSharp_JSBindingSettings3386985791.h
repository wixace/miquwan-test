﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Dictionary`2<System.Type,System.String>
struct Dictionary_2_t112654042;
// System.Type[]
struct TypeU5BU5D_t3339007067;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JSBindingSettings
struct  JSBindingSettings_t3386985791  : public Il2CppObject
{
public:

public:
};

struct JSBindingSettings_t3386985791_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.Type,System.String> JSBindingSettings::classesFullNameDic
	Dictionary_2_t112654042 * ___classesFullNameDic_0;
	// System.Type[] JSBindingSettings::enums
	TypeU5BU5D_t3339007067* ___enums_1;
	// System.Type[] JSBindingSettings::classes
	TypeU5BU5D_t3339007067* ___classes_2;
	// System.String JSBindingSettings::streamingAssetsPath
	String_t* ___streamingAssetsPath_3;
	// System.String JSBindingSettings::dataPath
	String_t* ___dataPath_4;
	// System.String JSBindingSettings::jsExtension
	String_t* ___jsExtension_5;
	// System.String JSBindingSettings::jsNewExtension
	String_t* ___jsNewExtension_6;
	// System.String JSBindingSettings::jscExtension
	String_t* ___jscExtension_7;
	// System.String JSBindingSettings::jsDirName
	String_t* ___jsDirName_8;
	// System.String JSBindingSettings::jsDirNewName
	String_t* ___jsDirNewName_9;
	// System.String JSBindingSettings::jsRelDir
	String_t* ___jsRelDir_10;
	// System.String JSBindingSettings::JsTypeSection
	String_t* ___JsTypeSection_11;
	// System.String JSBindingSettings::jscRelDir
	String_t* ___jscRelDir_12;
	// System.String JSBindingSettings::sharpKitGenFileDir
	String_t* ___sharpKitGenFileDir_13;
	// System.String[] JSBindingSettings::PathsNotToJavaScript
	StringU5BU5D_t4054002952* ___PathsNotToJavaScript_14;
	// System.String[] JSBindingSettings::PathsToJavaScript
	StringU5BU5D_t4054002952* ___PathsToJavaScript_15;
	// System.String[] JSBindingSettings::PathsNotToCheckOrReplace
	StringU5BU5D_t4054002952* ___PathsNotToCheckOrReplace_16;

public:
	inline static int32_t get_offset_of_classesFullNameDic_0() { return static_cast<int32_t>(offsetof(JSBindingSettings_t3386985791_StaticFields, ___classesFullNameDic_0)); }
	inline Dictionary_2_t112654042 * get_classesFullNameDic_0() const { return ___classesFullNameDic_0; }
	inline Dictionary_2_t112654042 ** get_address_of_classesFullNameDic_0() { return &___classesFullNameDic_0; }
	inline void set_classesFullNameDic_0(Dictionary_2_t112654042 * value)
	{
		___classesFullNameDic_0 = value;
		Il2CppCodeGenWriteBarrier(&___classesFullNameDic_0, value);
	}

	inline static int32_t get_offset_of_enums_1() { return static_cast<int32_t>(offsetof(JSBindingSettings_t3386985791_StaticFields, ___enums_1)); }
	inline TypeU5BU5D_t3339007067* get_enums_1() const { return ___enums_1; }
	inline TypeU5BU5D_t3339007067** get_address_of_enums_1() { return &___enums_1; }
	inline void set_enums_1(TypeU5BU5D_t3339007067* value)
	{
		___enums_1 = value;
		Il2CppCodeGenWriteBarrier(&___enums_1, value);
	}

	inline static int32_t get_offset_of_classes_2() { return static_cast<int32_t>(offsetof(JSBindingSettings_t3386985791_StaticFields, ___classes_2)); }
	inline TypeU5BU5D_t3339007067* get_classes_2() const { return ___classes_2; }
	inline TypeU5BU5D_t3339007067** get_address_of_classes_2() { return &___classes_2; }
	inline void set_classes_2(TypeU5BU5D_t3339007067* value)
	{
		___classes_2 = value;
		Il2CppCodeGenWriteBarrier(&___classes_2, value);
	}

	inline static int32_t get_offset_of_streamingAssetsPath_3() { return static_cast<int32_t>(offsetof(JSBindingSettings_t3386985791_StaticFields, ___streamingAssetsPath_3)); }
	inline String_t* get_streamingAssetsPath_3() const { return ___streamingAssetsPath_3; }
	inline String_t** get_address_of_streamingAssetsPath_3() { return &___streamingAssetsPath_3; }
	inline void set_streamingAssetsPath_3(String_t* value)
	{
		___streamingAssetsPath_3 = value;
		Il2CppCodeGenWriteBarrier(&___streamingAssetsPath_3, value);
	}

	inline static int32_t get_offset_of_dataPath_4() { return static_cast<int32_t>(offsetof(JSBindingSettings_t3386985791_StaticFields, ___dataPath_4)); }
	inline String_t* get_dataPath_4() const { return ___dataPath_4; }
	inline String_t** get_address_of_dataPath_4() { return &___dataPath_4; }
	inline void set_dataPath_4(String_t* value)
	{
		___dataPath_4 = value;
		Il2CppCodeGenWriteBarrier(&___dataPath_4, value);
	}

	inline static int32_t get_offset_of_jsExtension_5() { return static_cast<int32_t>(offsetof(JSBindingSettings_t3386985791_StaticFields, ___jsExtension_5)); }
	inline String_t* get_jsExtension_5() const { return ___jsExtension_5; }
	inline String_t** get_address_of_jsExtension_5() { return &___jsExtension_5; }
	inline void set_jsExtension_5(String_t* value)
	{
		___jsExtension_5 = value;
		Il2CppCodeGenWriteBarrier(&___jsExtension_5, value);
	}

	inline static int32_t get_offset_of_jsNewExtension_6() { return static_cast<int32_t>(offsetof(JSBindingSettings_t3386985791_StaticFields, ___jsNewExtension_6)); }
	inline String_t* get_jsNewExtension_6() const { return ___jsNewExtension_6; }
	inline String_t** get_address_of_jsNewExtension_6() { return &___jsNewExtension_6; }
	inline void set_jsNewExtension_6(String_t* value)
	{
		___jsNewExtension_6 = value;
		Il2CppCodeGenWriteBarrier(&___jsNewExtension_6, value);
	}

	inline static int32_t get_offset_of_jscExtension_7() { return static_cast<int32_t>(offsetof(JSBindingSettings_t3386985791_StaticFields, ___jscExtension_7)); }
	inline String_t* get_jscExtension_7() const { return ___jscExtension_7; }
	inline String_t** get_address_of_jscExtension_7() { return &___jscExtension_7; }
	inline void set_jscExtension_7(String_t* value)
	{
		___jscExtension_7 = value;
		Il2CppCodeGenWriteBarrier(&___jscExtension_7, value);
	}

	inline static int32_t get_offset_of_jsDirName_8() { return static_cast<int32_t>(offsetof(JSBindingSettings_t3386985791_StaticFields, ___jsDirName_8)); }
	inline String_t* get_jsDirName_8() const { return ___jsDirName_8; }
	inline String_t** get_address_of_jsDirName_8() { return &___jsDirName_8; }
	inline void set_jsDirName_8(String_t* value)
	{
		___jsDirName_8 = value;
		Il2CppCodeGenWriteBarrier(&___jsDirName_8, value);
	}

	inline static int32_t get_offset_of_jsDirNewName_9() { return static_cast<int32_t>(offsetof(JSBindingSettings_t3386985791_StaticFields, ___jsDirNewName_9)); }
	inline String_t* get_jsDirNewName_9() const { return ___jsDirNewName_9; }
	inline String_t** get_address_of_jsDirNewName_9() { return &___jsDirNewName_9; }
	inline void set_jsDirNewName_9(String_t* value)
	{
		___jsDirNewName_9 = value;
		Il2CppCodeGenWriteBarrier(&___jsDirNewName_9, value);
	}

	inline static int32_t get_offset_of_jsRelDir_10() { return static_cast<int32_t>(offsetof(JSBindingSettings_t3386985791_StaticFields, ___jsRelDir_10)); }
	inline String_t* get_jsRelDir_10() const { return ___jsRelDir_10; }
	inline String_t** get_address_of_jsRelDir_10() { return &___jsRelDir_10; }
	inline void set_jsRelDir_10(String_t* value)
	{
		___jsRelDir_10 = value;
		Il2CppCodeGenWriteBarrier(&___jsRelDir_10, value);
	}

	inline static int32_t get_offset_of_JsTypeSection_11() { return static_cast<int32_t>(offsetof(JSBindingSettings_t3386985791_StaticFields, ___JsTypeSection_11)); }
	inline String_t* get_JsTypeSection_11() const { return ___JsTypeSection_11; }
	inline String_t** get_address_of_JsTypeSection_11() { return &___JsTypeSection_11; }
	inline void set_JsTypeSection_11(String_t* value)
	{
		___JsTypeSection_11 = value;
		Il2CppCodeGenWriteBarrier(&___JsTypeSection_11, value);
	}

	inline static int32_t get_offset_of_jscRelDir_12() { return static_cast<int32_t>(offsetof(JSBindingSettings_t3386985791_StaticFields, ___jscRelDir_12)); }
	inline String_t* get_jscRelDir_12() const { return ___jscRelDir_12; }
	inline String_t** get_address_of_jscRelDir_12() { return &___jscRelDir_12; }
	inline void set_jscRelDir_12(String_t* value)
	{
		___jscRelDir_12 = value;
		Il2CppCodeGenWriteBarrier(&___jscRelDir_12, value);
	}

	inline static int32_t get_offset_of_sharpKitGenFileDir_13() { return static_cast<int32_t>(offsetof(JSBindingSettings_t3386985791_StaticFields, ___sharpKitGenFileDir_13)); }
	inline String_t* get_sharpKitGenFileDir_13() const { return ___sharpKitGenFileDir_13; }
	inline String_t** get_address_of_sharpKitGenFileDir_13() { return &___sharpKitGenFileDir_13; }
	inline void set_sharpKitGenFileDir_13(String_t* value)
	{
		___sharpKitGenFileDir_13 = value;
		Il2CppCodeGenWriteBarrier(&___sharpKitGenFileDir_13, value);
	}

	inline static int32_t get_offset_of_PathsNotToJavaScript_14() { return static_cast<int32_t>(offsetof(JSBindingSettings_t3386985791_StaticFields, ___PathsNotToJavaScript_14)); }
	inline StringU5BU5D_t4054002952* get_PathsNotToJavaScript_14() const { return ___PathsNotToJavaScript_14; }
	inline StringU5BU5D_t4054002952** get_address_of_PathsNotToJavaScript_14() { return &___PathsNotToJavaScript_14; }
	inline void set_PathsNotToJavaScript_14(StringU5BU5D_t4054002952* value)
	{
		___PathsNotToJavaScript_14 = value;
		Il2CppCodeGenWriteBarrier(&___PathsNotToJavaScript_14, value);
	}

	inline static int32_t get_offset_of_PathsToJavaScript_15() { return static_cast<int32_t>(offsetof(JSBindingSettings_t3386985791_StaticFields, ___PathsToJavaScript_15)); }
	inline StringU5BU5D_t4054002952* get_PathsToJavaScript_15() const { return ___PathsToJavaScript_15; }
	inline StringU5BU5D_t4054002952** get_address_of_PathsToJavaScript_15() { return &___PathsToJavaScript_15; }
	inline void set_PathsToJavaScript_15(StringU5BU5D_t4054002952* value)
	{
		___PathsToJavaScript_15 = value;
		Il2CppCodeGenWriteBarrier(&___PathsToJavaScript_15, value);
	}

	inline static int32_t get_offset_of_PathsNotToCheckOrReplace_16() { return static_cast<int32_t>(offsetof(JSBindingSettings_t3386985791_StaticFields, ___PathsNotToCheckOrReplace_16)); }
	inline StringU5BU5D_t4054002952* get_PathsNotToCheckOrReplace_16() const { return ___PathsNotToCheckOrReplace_16; }
	inline StringU5BU5D_t4054002952** get_address_of_PathsNotToCheckOrReplace_16() { return &___PathsNotToCheckOrReplace_16; }
	inline void set_PathsNotToCheckOrReplace_16(StringU5BU5D_t4054002952* value)
	{
		___PathsNotToCheckOrReplace_16 = value;
		Il2CppCodeGenWriteBarrier(&___PathsNotToCheckOrReplace_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
