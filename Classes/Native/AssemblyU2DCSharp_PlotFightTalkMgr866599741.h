﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// UnityEngine.Transform
struct Transform_t1659122786;
// CombatEntity
struct CombatEntity_t684137495;
// PlotFightTalk
struct PlotFightTalk_t789215451;
// System.Collections.Generic.List`1<PlotFightTalkMgr/plotData>
struct List_1_t2013455757;
// System.Collections.Generic.List`1<System.UInt32>
struct List_1_t1392853533;
// PlotFightTalkMgr/plotData
struct plotData_t645270205;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlotFightTalkMgr
struct  PlotFightTalkMgr_t866599741  : public Il2CppObject
{
public:
	// UnityEngine.GameObject PlotFightTalkMgr::_parentGO
	GameObject_t3674682005 * ____parentGO_1;
	// UnityEngine.Transform PlotFightTalkMgr::_targe
	Transform_t1659122786 * ____targe_2;
	// CombatEntity PlotFightTalkMgr::entity
	CombatEntity_t684137495 * ___entity_3;
	// PlotFightTalk PlotFightTalkMgr::_curFightTalk
	PlotFightTalk_t789215451 * ____curFightTalk_4;
	// System.Collections.Generic.List`1<PlotFightTalkMgr/plotData> PlotFightTalkMgr::_plotList
	List_1_t2013455757 * ____plotList_5;
	// System.Collections.Generic.List`1<System.UInt32> PlotFightTalkMgr::beUtilKeyList
	List_1_t1392853533 * ___beUtilKeyList_6;
	// PlotFightTalkMgr/plotData PlotFightTalkMgr::pData
	plotData_t645270205 * ___pData_7;
	// System.Single PlotFightTalkMgr::doTime
	float ___doTime_8;
	// System.Single PlotFightTalkMgr::time
	float ___time_9;

public:
	inline static int32_t get_offset_of__parentGO_1() { return static_cast<int32_t>(offsetof(PlotFightTalkMgr_t866599741, ____parentGO_1)); }
	inline GameObject_t3674682005 * get__parentGO_1() const { return ____parentGO_1; }
	inline GameObject_t3674682005 ** get_address_of__parentGO_1() { return &____parentGO_1; }
	inline void set__parentGO_1(GameObject_t3674682005 * value)
	{
		____parentGO_1 = value;
		Il2CppCodeGenWriteBarrier(&____parentGO_1, value);
	}

	inline static int32_t get_offset_of__targe_2() { return static_cast<int32_t>(offsetof(PlotFightTalkMgr_t866599741, ____targe_2)); }
	inline Transform_t1659122786 * get__targe_2() const { return ____targe_2; }
	inline Transform_t1659122786 ** get_address_of__targe_2() { return &____targe_2; }
	inline void set__targe_2(Transform_t1659122786 * value)
	{
		____targe_2 = value;
		Il2CppCodeGenWriteBarrier(&____targe_2, value);
	}

	inline static int32_t get_offset_of_entity_3() { return static_cast<int32_t>(offsetof(PlotFightTalkMgr_t866599741, ___entity_3)); }
	inline CombatEntity_t684137495 * get_entity_3() const { return ___entity_3; }
	inline CombatEntity_t684137495 ** get_address_of_entity_3() { return &___entity_3; }
	inline void set_entity_3(CombatEntity_t684137495 * value)
	{
		___entity_3 = value;
		Il2CppCodeGenWriteBarrier(&___entity_3, value);
	}

	inline static int32_t get_offset_of__curFightTalk_4() { return static_cast<int32_t>(offsetof(PlotFightTalkMgr_t866599741, ____curFightTalk_4)); }
	inline PlotFightTalk_t789215451 * get__curFightTalk_4() const { return ____curFightTalk_4; }
	inline PlotFightTalk_t789215451 ** get_address_of__curFightTalk_4() { return &____curFightTalk_4; }
	inline void set__curFightTalk_4(PlotFightTalk_t789215451 * value)
	{
		____curFightTalk_4 = value;
		Il2CppCodeGenWriteBarrier(&____curFightTalk_4, value);
	}

	inline static int32_t get_offset_of__plotList_5() { return static_cast<int32_t>(offsetof(PlotFightTalkMgr_t866599741, ____plotList_5)); }
	inline List_1_t2013455757 * get__plotList_5() const { return ____plotList_5; }
	inline List_1_t2013455757 ** get_address_of__plotList_5() { return &____plotList_5; }
	inline void set__plotList_5(List_1_t2013455757 * value)
	{
		____plotList_5 = value;
		Il2CppCodeGenWriteBarrier(&____plotList_5, value);
	}

	inline static int32_t get_offset_of_beUtilKeyList_6() { return static_cast<int32_t>(offsetof(PlotFightTalkMgr_t866599741, ___beUtilKeyList_6)); }
	inline List_1_t1392853533 * get_beUtilKeyList_6() const { return ___beUtilKeyList_6; }
	inline List_1_t1392853533 ** get_address_of_beUtilKeyList_6() { return &___beUtilKeyList_6; }
	inline void set_beUtilKeyList_6(List_1_t1392853533 * value)
	{
		___beUtilKeyList_6 = value;
		Il2CppCodeGenWriteBarrier(&___beUtilKeyList_6, value);
	}

	inline static int32_t get_offset_of_pData_7() { return static_cast<int32_t>(offsetof(PlotFightTalkMgr_t866599741, ___pData_7)); }
	inline plotData_t645270205 * get_pData_7() const { return ___pData_7; }
	inline plotData_t645270205 ** get_address_of_pData_7() { return &___pData_7; }
	inline void set_pData_7(plotData_t645270205 * value)
	{
		___pData_7 = value;
		Il2CppCodeGenWriteBarrier(&___pData_7, value);
	}

	inline static int32_t get_offset_of_doTime_8() { return static_cast<int32_t>(offsetof(PlotFightTalkMgr_t866599741, ___doTime_8)); }
	inline float get_doTime_8() const { return ___doTime_8; }
	inline float* get_address_of_doTime_8() { return &___doTime_8; }
	inline void set_doTime_8(float value)
	{
		___doTime_8 = value;
	}

	inline static int32_t get_offset_of_time_9() { return static_cast<int32_t>(offsetof(PlotFightTalkMgr_t866599741, ___time_9)); }
	inline float get_time_9() const { return ___time_9; }
	inline float* get_address_of_time_9() { return &___time_9; }
	inline void set_time_9(float value)
	{
		___time_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
