﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_AnimationClipGenerated
struct UnityEngine_AnimationClipGenerated_t265766499;
// JSVCall
struct JSVCall_t3708497963;
// UnityEngine.AnimationEvent[]
struct AnimationEventU5BU5D_t1749266719;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void UnityEngine_AnimationClipGenerated::.ctor()
extern "C"  void UnityEngine_AnimationClipGenerated__ctor_m2441152920 (UnityEngine_AnimationClipGenerated_t265766499 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimationClipGenerated::AnimationClip_AnimationClip1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimationClipGenerated_AnimationClip_AnimationClip1_m1476016760 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimationClipGenerated::AnimationClip_length(JSVCall)
extern "C"  void UnityEngine_AnimationClipGenerated_AnimationClip_length_m1519294474 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimationClipGenerated::AnimationClip_frameRate(JSVCall)
extern "C"  void UnityEngine_AnimationClipGenerated_AnimationClip_frameRate_m407871823 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimationClipGenerated::AnimationClip_wrapMode(JSVCall)
extern "C"  void UnityEngine_AnimationClipGenerated_AnimationClip_wrapMode_m976192323 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimationClipGenerated::AnimationClip_localBounds(JSVCall)
extern "C"  void UnityEngine_AnimationClipGenerated_AnimationClip_localBounds_m2629304092 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimationClipGenerated::AnimationClip_legacy(JSVCall)
extern "C"  void UnityEngine_AnimationClipGenerated_AnimationClip_legacy_m4236258951 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimationClipGenerated::AnimationClip_humanMotion(JSVCall)
extern "C"  void UnityEngine_AnimationClipGenerated_AnimationClip_humanMotion_m2833008953 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimationClipGenerated::AnimationClip_events(JSVCall)
extern "C"  void UnityEngine_AnimationClipGenerated_AnimationClip_events_m934361911 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimationClipGenerated::AnimationClip_AddEvent__AnimationEvent(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimationClipGenerated_AnimationClip_AddEvent__AnimationEvent_m962976938 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimationClipGenerated::AnimationClip_ClearCurves(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimationClipGenerated_AnimationClip_ClearCurves_m874492216 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimationClipGenerated::AnimationClip_EnsureQuaternionContinuity(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimationClipGenerated_AnimationClip_EnsureQuaternionContinuity_m2252825607 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimationClipGenerated::AnimationClip_SampleAnimation__GameObject__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimationClipGenerated_AnimationClip_SampleAnimation__GameObject__Single_m3650098362 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimationClipGenerated::AnimationClip_SetCurve__String__Type__String__AnimationCurve(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimationClipGenerated_AnimationClip_SetCurve__String__Type__String__AnimationCurve_m1997345999 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimationClipGenerated::__Register()
extern "C"  void UnityEngine_AnimationClipGenerated___Register_m4223578671 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AnimationEvent[] UnityEngine_AnimationClipGenerated::<AnimationClip_events>m__179()
extern "C"  AnimationEventU5BU5D_t1749266719* UnityEngine_AnimationClipGenerated_U3CAnimationClip_eventsU3Em__179_m826084830 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_AnimationClipGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UnityEngine_AnimationClipGenerated_ilo_getObject1_m292453626 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimationClipGenerated::ilo_setSingle2(System.Int32,System.Single)
extern "C"  void UnityEngine_AnimationClipGenerated_ilo_setSingle2_m3747269725 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimationClipGenerated::ilo_setEnum3(System.Int32,System.Int32)
extern "C"  void UnityEngine_AnimationClipGenerated_ilo_setEnum3_m1195165175 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_AnimationClipGenerated::ilo_getEnum4(System.Int32)
extern "C"  int32_t UnityEngine_AnimationClipGenerated_ilo_getEnum4_m2542635835 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_AnimationClipGenerated::ilo_setObject5(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UnityEngine_AnimationClipGenerated_ilo_setObject5_m1042434422 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_AnimationClipGenerated::ilo_getObject6(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_AnimationClipGenerated_ilo_getObject6_m1969146396 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimationClipGenerated::ilo_setBooleanS7(System.Int32,System.Boolean)
extern "C"  void UnityEngine_AnimationClipGenerated_ilo_setBooleanS7_m509640643 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_AnimationClipGenerated::ilo_getElement8(System.Int32,System.Int32)
extern "C"  int32_t UnityEngine_AnimationClipGenerated_ilo_getElement8_m2165279583 (Il2CppObject * __this /* static, unused */, int32_t ___id0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
