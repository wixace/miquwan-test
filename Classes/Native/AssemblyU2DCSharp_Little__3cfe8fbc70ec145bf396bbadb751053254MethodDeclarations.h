﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._3cfe8fbc70ec145bf396bbadbc6b2b97
struct _3cfe8fbc70ec145bf396bbadbc6b2b97_t751053254;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._3cfe8fbc70ec145bf396bbadbc6b2b97::.ctor()
extern "C"  void _3cfe8fbc70ec145bf396bbadbc6b2b97__ctor_m1600822055 (_3cfe8fbc70ec145bf396bbadbc6b2b97_t751053254 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._3cfe8fbc70ec145bf396bbadbc6b2b97::_3cfe8fbc70ec145bf396bbadbc6b2b97m2(System.Int32)
extern "C"  int32_t _3cfe8fbc70ec145bf396bbadbc6b2b97__3cfe8fbc70ec145bf396bbadbc6b2b97m2_m271551449 (_3cfe8fbc70ec145bf396bbadbc6b2b97_t751053254 * __this, int32_t ____3cfe8fbc70ec145bf396bbadbc6b2b97a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._3cfe8fbc70ec145bf396bbadbc6b2b97::_3cfe8fbc70ec145bf396bbadbc6b2b97m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _3cfe8fbc70ec145bf396bbadbc6b2b97__3cfe8fbc70ec145bf396bbadbc6b2b97m_m134685437 (_3cfe8fbc70ec145bf396bbadbc6b2b97_t751053254 * __this, int32_t ____3cfe8fbc70ec145bf396bbadbc6b2b97a0, int32_t ____3cfe8fbc70ec145bf396bbadbc6b2b97991, int32_t ____3cfe8fbc70ec145bf396bbadbc6b2b97c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
