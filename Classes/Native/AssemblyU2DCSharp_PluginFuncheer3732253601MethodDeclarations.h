﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PluginFuncheer
struct PluginFuncheer_t3732253601;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// System.String
struct String_t;
// UnityEngine.WWWForm
struct WWWForm_t461342257;
// Mihua.SDK.PayInfo
struct PayInfo_t1775308120;
// VersionInfo
struct VersionInfo_t2356638086;
// VersionMgr
struct VersionMgr_t1322950208;
// FloatTextMgr
struct FloatTextMgr_t630384591;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// Mihua.SDK.SdkwwwCallBack
struct SdkwwwCallBack_t3004559384;
// System.Object
struct Il2CppObject;
// PluginsSdkMgr
struct PluginsSdkMgr_t3884624670;
// System.Action
struct Action_t3771233898;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_Mihua_SDK_PayInfo1775308120.h"
#include "AssemblyU2DCSharp_VersionMgr1322950208.h"
#include "AssemblyU2DCSharp_PluginFuncheer3732253601.h"
#include "AssemblyU2DCSharp_FloatTextMgr630384591.h"
#include "AssemblyU2DCSharp_FLOAT_TEXT_ID2199345770.h"
#include "UnityEngine_UnityEngine_WWWForm461342257.h"
#include "AssemblyU2DCSharp_Mihua_SDK_SdkwwwCallBack3004559384.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_PluginsSdkMgr3884624670.h"
#include "System_Core_System_Action3771233898.h"

// System.Void PluginFuncheer::.ctor()
extern "C"  void PluginFuncheer__ctor_m2885723674 (PluginFuncheer_t3732253601 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginFuncheer::Init()
extern "C"  void PluginFuncheer_Init_m737361146 (PluginFuncheer_t3732253601 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginFuncheer::ReqSDKHttpLogin()
extern "C"  Dictionary_2_t827649927 * PluginFuncheer_ReqSDKHttpLogin_m951597423 (PluginFuncheer_t3732253601 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginFuncheer::IsLoginSuccess(System.Boolean)
extern "C"  bool PluginFuncheer_IsLoginSuccess_m342507257 (PluginFuncheer_t3732253601 * __this, bool ___isOpenUseLogin0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginFuncheer::OpenUserLogin()
extern "C"  void PluginFuncheer_OpenUserLogin_m1420234348 (PluginFuncheer_t3732253601 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginFuncheer::UserPay(CEvent.ZEvent)
extern "C"  void PluginFuncheer_UserPay_m1783608198 (PluginFuncheer_t3732253601 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginFuncheer::SignCallBack(System.Boolean,System.String)
extern "C"  void PluginFuncheer_SignCallBack_m2083253023 (PluginFuncheer_t3732253601 * __this, bool ___success0, String_t* ___arg1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.WWWForm PluginFuncheer::BuildOrderParam2WWWForm(Mihua.SDK.PayInfo)
extern "C"  WWWForm_t461342257 * PluginFuncheer_BuildOrderParam2WWWForm_m3291540551 (PluginFuncheer_t3732253601 * __this, PayInfo_t1775308120 * ___info0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginFuncheer::CreatRole(CEvent.ZEvent)
extern "C"  void PluginFuncheer_CreatRole_m3401489732 (PluginFuncheer_t3732253601 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginFuncheer::OnLoginSuccess(System.String)
extern "C"  void PluginFuncheer_OnLoginSuccess_m185052575 (PluginFuncheer_t3732253601 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginFuncheer::OnSwitchSuccess(System.String)
extern "C"  void PluginFuncheer_OnSwitchSuccess_m479771386 (PluginFuncheer_t3732253601 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginFuncheer::OnLogoutSuccess(System.String)
extern "C"  void PluginFuncheer_OnLogoutSuccess_m1445205200 (PluginFuncheer_t3732253601 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginFuncheer::initSdk()
extern "C"  void PluginFuncheer_initSdk_m300753954 (PluginFuncheer_t3732253601 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginFuncheer::login()
extern "C"  void PluginFuncheer_login_m2407738497 (PluginFuncheer_t3732253601 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginFuncheer::logout()
extern "C"  void PluginFuncheer_logout_m1631272084 (PluginFuncheer_t3732253601 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginFuncheer::pay(System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginFuncheer_pay_m2972556578 (PluginFuncheer_t3732253601 * __this, String_t* ___orderid0, String_t* ___ProductId1, String_t* ___amount2, String_t* ___productname3, String_t* ___serverid4, String_t* ___rolename5, String_t* ___rolelevel6, String_t* ___extra7, String_t* ___gold8, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginFuncheer::roleinfo(System.String,System.String,System.String)
extern "C"  void PluginFuncheer_roleinfo_m2792246988 (PluginFuncheer_t3732253601 * __this, String_t* ___rolename0, String_t* ___rolelevel1, String_t* ___serverid2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginFuncheer::<OnLogoutSuccess>m__41F()
extern "C"  void PluginFuncheer_U3COnLogoutSuccessU3Em__41F_m3402587700 (PluginFuncheer_t3732253601 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionInfo PluginFuncheer::ilo_get_currentVS1(VersionMgr)
extern "C"  VersionInfo_t2356638086 * PluginFuncheer_ilo_get_currentVS1_m4001345678 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginFuncheer::ilo_get_isSdkLogin2(VersionMgr)
extern "C"  bool PluginFuncheer_ilo_get_isSdkLogin2_m1565626401 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionMgr PluginFuncheer::ilo_get_Instance3()
extern "C"  VersionMgr_t1322950208 * PluginFuncheer_ilo_get_Instance3_m2442342173 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginFuncheer::ilo_OpenUserLogin4(PluginFuncheer)
extern "C"  void PluginFuncheer_ilo_OpenUserLogin4_m1760220508 (Il2CppObject * __this /* static, unused */, PluginFuncheer_t3732253601 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FloatTextMgr PluginFuncheer::ilo_get_FloatTextMgr5()
extern "C"  FloatTextMgr_t630384591 * PluginFuncheer_ilo_get_FloatTextMgr5_m465763860 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginFuncheer::ilo_FloatText6(FloatTextMgr,FLOAT_TEXT_ID,System.Int32,System.Object[])
extern "C"  void PluginFuncheer_ilo_FloatText6_m3856046214 (Il2CppObject * __this /* static, unused */, FloatTextMgr_t630384591 * ____this0, int32_t ___textId1, int32_t ___offsetY2, ObjectU5BU5D_t1108656482* ___args3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mihua.SDK.PayInfo PluginFuncheer::ilo_Parse7(System.Object[])
extern "C"  PayInfo_t1775308120 * PluginFuncheer_ilo_Parse7_m3139250026 (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t1108656482* ___payOjs0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginFuncheer::ilo_Request8(System.String,UnityEngine.WWWForm,Mihua.SDK.SdkwwwCallBack)
extern "C"  void PluginFuncheer_ilo_Request8_m1342571798 (Il2CppObject * __this /* static, unused */, String_t* ___url0, WWWForm_t461342257 * ___wwwForm1, SdkwwwCallBack_t3004559384 * ___callBack2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginFuncheer::ilo_Log9(System.Object,System.Boolean)
extern "C"  void PluginFuncheer_ilo_Log9_m2807939067 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginFuncheer::ilo_roleinfo10(PluginFuncheer,System.String,System.String,System.String)
extern "C"  void PluginFuncheer_ilo_roleinfo10_m264972275 (Il2CppObject * __this /* static, unused */, PluginFuncheer_t3732253601 * ____this0, String_t* ___rolename1, String_t* ___rolelevel2, String_t* ___serverid3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PluginsSdkMgr PluginFuncheer::ilo_get_PluginsSdkMgr11()
extern "C"  PluginsSdkMgr_t3884624670 * PluginFuncheer_ilo_get_PluginsSdkMgr11_m2426999557 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginFuncheer::ilo_ReqSDKHttpLogin12(PluginsSdkMgr)
extern "C"  void PluginFuncheer_ilo_ReqSDKHttpLogin12_m2437428297 (Il2CppObject * __this /* static, unused */, PluginsSdkMgr_t3884624670 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginFuncheer::ilo_Logout13(System.Action)
extern "C"  void PluginFuncheer_ilo_Logout13_m2241419412 (Il2CppObject * __this /* static, unused */, Action_t3771233898 * ___call0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
