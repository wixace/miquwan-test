﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21947212572MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<SoundTypeID,ISound>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m253037313(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t4241366511 *, int32_t, ISound_t2170003014 *, const MethodInfo*))KeyValuePair_2__ctor_m2265684409_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<SoundTypeID,ISound>::get_Key()
#define KeyValuePair_2_get_Key_m1089816876(__this, method) ((  int32_t (*) (KeyValuePair_2_t4241366511 *, const MethodInfo*))KeyValuePair_2_get_Key_m50681231_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<SoundTypeID,ISound>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m1410183816(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t4241366511 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m3522613456_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<SoundTypeID,ISound>::get_Value()
#define KeyValuePair_2_get_Value_m1499249181(__this, method) ((  ISound_t2170003014 * (*) (KeyValuePair_2_t4241366511 *, const MethodInfo*))KeyValuePair_2_get_Value_m1051595507_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<SoundTypeID,ISound>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m1304199816(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t4241366511 *, ISound_t2170003014 *, const MethodInfo*))KeyValuePair_2_set_Value_m1507671248_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<SoundTypeID,ISound>::ToString()
#define KeyValuePair_2_ToString_m2385977306(__this, method) ((  String_t* (*) (KeyValuePair_2_t4241366511 *, const MethodInfo*))KeyValuePair_2_ToString_m2770852088_gshared)(__this, method)
