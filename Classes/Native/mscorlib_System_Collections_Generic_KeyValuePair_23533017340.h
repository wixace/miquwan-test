﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Pathfinding.GraphNode
struct GraphNode_t23612370;
// Pathfinding.NodeLink3
struct NodeLink3_t1645404665;

#include "mscorlib_System_ValueType1744280289.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<Pathfinding.GraphNode,Pathfinding.NodeLink3>
struct  KeyValuePair_2_t3533017340 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	GraphNode_t23612370 * ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	NodeLink3_t1645404665 * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t3533017340, ___key_0)); }
	inline GraphNode_t23612370 * get_key_0() const { return ___key_0; }
	inline GraphNode_t23612370 ** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(GraphNode_t23612370 * value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier(&___key_0, value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t3533017340, ___value_1)); }
	inline NodeLink3_t1645404665 * get_value_1() const { return ___value_1; }
	inline NodeLink3_t1645404665 ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(NodeLink3_t1645404665 * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier(&___value_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
