﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BMFontGenerated
struct BMFontGenerated_t3694808949;
// JSVCall
struct JSVCall_t3708497963;
// BMFont
struct BMFont_t1962830650;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_BMFont1962830650.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void BMFontGenerated::.ctor()
extern "C"  void BMFontGenerated__ctor_m1722223510 (BMFontGenerated_t3694808949 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BMFontGenerated::BMFont_BMFont1(JSVCall,System.Int32)
extern "C"  bool BMFontGenerated_BMFont_BMFont1_m815011484 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BMFontGenerated::BMFont_isValid(JSVCall)
extern "C"  void BMFontGenerated_BMFont_isValid_m3105931532 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BMFontGenerated::BMFont_charSize(JSVCall)
extern "C"  void BMFontGenerated_BMFont_charSize_m2420612535 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BMFontGenerated::BMFont_baseOffset(JSVCall)
extern "C"  void BMFontGenerated_BMFont_baseOffset_m1694722058 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BMFontGenerated::BMFont_texWidth(JSVCall)
extern "C"  void BMFontGenerated_BMFont_texWidth_m1064730959 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BMFontGenerated::BMFont_texHeight(JSVCall)
extern "C"  void BMFontGenerated_BMFont_texHeight_m3597332304 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BMFontGenerated::BMFont_glyphCount(JSVCall)
extern "C"  void BMFontGenerated_BMFont_glyphCount_m67068523 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BMFontGenerated::BMFont_spriteName(JSVCall)
extern "C"  void BMFontGenerated_BMFont_spriteName_m1382549854 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BMFontGenerated::BMFont_glyphs(JSVCall)
extern "C"  void BMFontGenerated_BMFont_glyphs_m4245655911 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BMFontGenerated::BMFont_Clear(JSVCall,System.Int32)
extern "C"  bool BMFontGenerated_BMFont_Clear_m3043522834 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BMFontGenerated::BMFont_GetGlyph__Int32__Boolean(JSVCall,System.Int32)
extern "C"  bool BMFontGenerated_BMFont_GetGlyph__Int32__Boolean_m2230188013 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BMFontGenerated::BMFont_GetGlyph__Int32(JSVCall,System.Int32)
extern "C"  bool BMFontGenerated_BMFont_GetGlyph__Int32_m610905565 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BMFontGenerated::BMFont_Trim__Int32__Int32__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool BMFontGenerated_BMFont_Trim__Int32__Int32__Int32__Int32_m26713919 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BMFontGenerated::__Register()
extern "C"  void BMFontGenerated___Register_m3567271665 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BMFontGenerated::ilo_set_charSize1(BMFont,System.Int32)
extern "C"  void BMFontGenerated_ilo_set_charSize1_m1989430675 (Il2CppObject * __this /* static, unused */, BMFont_t1962830650 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BMFontGenerated::ilo_get_baseOffset2(BMFont)
extern "C"  int32_t BMFontGenerated_ilo_get_baseOffset2_m3735532122 (Il2CppObject * __this /* static, unused */, BMFont_t1962830650 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BMFontGenerated::ilo_set_texWidth3(BMFont,System.Int32)
extern "C"  void BMFontGenerated_ilo_set_texWidth3_m883521081 (Il2CppObject * __this /* static, unused */, BMFont_t1962830650 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BMFontGenerated::ilo_setInt324(System.Int32,System.Int32)
extern "C"  void BMFontGenerated_ilo_setInt324_m2254825757 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BMFontGenerated::ilo_getInt325(System.Int32)
extern "C"  int32_t BMFontGenerated_ilo_getInt325_m1152629309 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BMFontGenerated::ilo_Clear6(BMFont)
extern "C"  void BMFontGenerated_ilo_Clear6_m2377443568 (Il2CppObject * __this /* static, unused */, BMFont_t1962830650 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BMFontGenerated::ilo_setObject7(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t BMFontGenerated_ilo_setObject7_m1968009566 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
