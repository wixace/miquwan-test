﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.ClipperLib.TEdge
struct TEdge_t3950806139;

#include "codegen/il2cpp-codegen.h"

// System.Void Pathfinding.ClipperLib.TEdge::.ctor()
extern "C"  void TEdge__ctor_m2332074730 (TEdge_t3950806139 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
