﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_getowce218
struct  M_getowce218_t3995948169  : public Il2CppObject
{
public:
	// System.Boolean GarbageiOS.M_getowce218::_jenuJaici
	bool ____jenuJaici_0;
	// System.Boolean GarbageiOS.M_getowce218::_haltastereLawla
	bool ____haltastereLawla_1;
	// System.Boolean GarbageiOS.M_getowce218::_trasde
	bool ____trasde_2;
	// System.Int32 GarbageiOS.M_getowce218::_rayhem
	int32_t ____rayhem_3;
	// System.String GarbageiOS.M_getowce218::_tepembor
	String_t* ____tepembor_4;
	// System.String GarbageiOS.M_getowce218::_nelmal
	String_t* ____nelmal_5;
	// System.Int32 GarbageiOS.M_getowce218::_caycouTekexer
	int32_t ____caycouTekexer_6;
	// System.Boolean GarbageiOS.M_getowce218::_wilayba
	bool ____wilayba_7;
	// System.Single GarbageiOS.M_getowce218::_toserjee
	float ____toserjee_8;
	// System.Int32 GarbageiOS.M_getowce218::_facowCirrere
	int32_t ____facowCirrere_9;
	// System.String GarbageiOS.M_getowce218::_trewearseTaichaito
	String_t* ____trewearseTaichaito_10;
	// System.String GarbageiOS.M_getowce218::_sayrihe
	String_t* ____sayrihe_11;
	// System.Boolean GarbageiOS.M_getowce218::_burjorte
	bool ____burjorte_12;
	// System.Boolean GarbageiOS.M_getowce218::_feenasirHoopis
	bool ____feenasirHoopis_13;
	// System.String GarbageiOS.M_getowce218::_curowhaw
	String_t* ____curowhaw_14;
	// System.UInt32 GarbageiOS.M_getowce218::_mayqime
	uint32_t ____mayqime_15;
	// System.Boolean GarbageiOS.M_getowce218::_kurseeKexelair
	bool ____kurseeKexelair_16;
	// System.Int32 GarbageiOS.M_getowce218::_darbor
	int32_t ____darbor_17;
	// System.UInt32 GarbageiOS.M_getowce218::_wudouDacer
	uint32_t ____wudouDacer_18;
	// System.Int32 GarbageiOS.M_getowce218::_nereresturHeremeryaw
	int32_t ____nereresturHeremeryaw_19;
	// System.UInt32 GarbageiOS.M_getowce218::_mouwe
	uint32_t ____mouwe_20;
	// System.Boolean GarbageiOS.M_getowce218::_kitalMujoubir
	bool ____kitalMujoubir_21;
	// System.String GarbageiOS.M_getowce218::_dorsayjo
	String_t* ____dorsayjo_22;
	// System.Boolean GarbageiOS.M_getowce218::_drunel
	bool ____drunel_23;
	// System.Boolean GarbageiOS.M_getowce218::_duhikeKairmowri
	bool ____duhikeKairmowri_24;
	// System.Single GarbageiOS.M_getowce218::_fehawcearSawpouba
	float ____fehawcearSawpouba_25;

public:
	inline static int32_t get_offset_of__jenuJaici_0() { return static_cast<int32_t>(offsetof(M_getowce218_t3995948169, ____jenuJaici_0)); }
	inline bool get__jenuJaici_0() const { return ____jenuJaici_0; }
	inline bool* get_address_of__jenuJaici_0() { return &____jenuJaici_0; }
	inline void set__jenuJaici_0(bool value)
	{
		____jenuJaici_0 = value;
	}

	inline static int32_t get_offset_of__haltastereLawla_1() { return static_cast<int32_t>(offsetof(M_getowce218_t3995948169, ____haltastereLawla_1)); }
	inline bool get__haltastereLawla_1() const { return ____haltastereLawla_1; }
	inline bool* get_address_of__haltastereLawla_1() { return &____haltastereLawla_1; }
	inline void set__haltastereLawla_1(bool value)
	{
		____haltastereLawla_1 = value;
	}

	inline static int32_t get_offset_of__trasde_2() { return static_cast<int32_t>(offsetof(M_getowce218_t3995948169, ____trasde_2)); }
	inline bool get__trasde_2() const { return ____trasde_2; }
	inline bool* get_address_of__trasde_2() { return &____trasde_2; }
	inline void set__trasde_2(bool value)
	{
		____trasde_2 = value;
	}

	inline static int32_t get_offset_of__rayhem_3() { return static_cast<int32_t>(offsetof(M_getowce218_t3995948169, ____rayhem_3)); }
	inline int32_t get__rayhem_3() const { return ____rayhem_3; }
	inline int32_t* get_address_of__rayhem_3() { return &____rayhem_3; }
	inline void set__rayhem_3(int32_t value)
	{
		____rayhem_3 = value;
	}

	inline static int32_t get_offset_of__tepembor_4() { return static_cast<int32_t>(offsetof(M_getowce218_t3995948169, ____tepembor_4)); }
	inline String_t* get__tepembor_4() const { return ____tepembor_4; }
	inline String_t** get_address_of__tepembor_4() { return &____tepembor_4; }
	inline void set__tepembor_4(String_t* value)
	{
		____tepembor_4 = value;
		Il2CppCodeGenWriteBarrier(&____tepembor_4, value);
	}

	inline static int32_t get_offset_of__nelmal_5() { return static_cast<int32_t>(offsetof(M_getowce218_t3995948169, ____nelmal_5)); }
	inline String_t* get__nelmal_5() const { return ____nelmal_5; }
	inline String_t** get_address_of__nelmal_5() { return &____nelmal_5; }
	inline void set__nelmal_5(String_t* value)
	{
		____nelmal_5 = value;
		Il2CppCodeGenWriteBarrier(&____nelmal_5, value);
	}

	inline static int32_t get_offset_of__caycouTekexer_6() { return static_cast<int32_t>(offsetof(M_getowce218_t3995948169, ____caycouTekexer_6)); }
	inline int32_t get__caycouTekexer_6() const { return ____caycouTekexer_6; }
	inline int32_t* get_address_of__caycouTekexer_6() { return &____caycouTekexer_6; }
	inline void set__caycouTekexer_6(int32_t value)
	{
		____caycouTekexer_6 = value;
	}

	inline static int32_t get_offset_of__wilayba_7() { return static_cast<int32_t>(offsetof(M_getowce218_t3995948169, ____wilayba_7)); }
	inline bool get__wilayba_7() const { return ____wilayba_7; }
	inline bool* get_address_of__wilayba_7() { return &____wilayba_7; }
	inline void set__wilayba_7(bool value)
	{
		____wilayba_7 = value;
	}

	inline static int32_t get_offset_of__toserjee_8() { return static_cast<int32_t>(offsetof(M_getowce218_t3995948169, ____toserjee_8)); }
	inline float get__toserjee_8() const { return ____toserjee_8; }
	inline float* get_address_of__toserjee_8() { return &____toserjee_8; }
	inline void set__toserjee_8(float value)
	{
		____toserjee_8 = value;
	}

	inline static int32_t get_offset_of__facowCirrere_9() { return static_cast<int32_t>(offsetof(M_getowce218_t3995948169, ____facowCirrere_9)); }
	inline int32_t get__facowCirrere_9() const { return ____facowCirrere_9; }
	inline int32_t* get_address_of__facowCirrere_9() { return &____facowCirrere_9; }
	inline void set__facowCirrere_9(int32_t value)
	{
		____facowCirrere_9 = value;
	}

	inline static int32_t get_offset_of__trewearseTaichaito_10() { return static_cast<int32_t>(offsetof(M_getowce218_t3995948169, ____trewearseTaichaito_10)); }
	inline String_t* get__trewearseTaichaito_10() const { return ____trewearseTaichaito_10; }
	inline String_t** get_address_of__trewearseTaichaito_10() { return &____trewearseTaichaito_10; }
	inline void set__trewearseTaichaito_10(String_t* value)
	{
		____trewearseTaichaito_10 = value;
		Il2CppCodeGenWriteBarrier(&____trewearseTaichaito_10, value);
	}

	inline static int32_t get_offset_of__sayrihe_11() { return static_cast<int32_t>(offsetof(M_getowce218_t3995948169, ____sayrihe_11)); }
	inline String_t* get__sayrihe_11() const { return ____sayrihe_11; }
	inline String_t** get_address_of__sayrihe_11() { return &____sayrihe_11; }
	inline void set__sayrihe_11(String_t* value)
	{
		____sayrihe_11 = value;
		Il2CppCodeGenWriteBarrier(&____sayrihe_11, value);
	}

	inline static int32_t get_offset_of__burjorte_12() { return static_cast<int32_t>(offsetof(M_getowce218_t3995948169, ____burjorte_12)); }
	inline bool get__burjorte_12() const { return ____burjorte_12; }
	inline bool* get_address_of__burjorte_12() { return &____burjorte_12; }
	inline void set__burjorte_12(bool value)
	{
		____burjorte_12 = value;
	}

	inline static int32_t get_offset_of__feenasirHoopis_13() { return static_cast<int32_t>(offsetof(M_getowce218_t3995948169, ____feenasirHoopis_13)); }
	inline bool get__feenasirHoopis_13() const { return ____feenasirHoopis_13; }
	inline bool* get_address_of__feenasirHoopis_13() { return &____feenasirHoopis_13; }
	inline void set__feenasirHoopis_13(bool value)
	{
		____feenasirHoopis_13 = value;
	}

	inline static int32_t get_offset_of__curowhaw_14() { return static_cast<int32_t>(offsetof(M_getowce218_t3995948169, ____curowhaw_14)); }
	inline String_t* get__curowhaw_14() const { return ____curowhaw_14; }
	inline String_t** get_address_of__curowhaw_14() { return &____curowhaw_14; }
	inline void set__curowhaw_14(String_t* value)
	{
		____curowhaw_14 = value;
		Il2CppCodeGenWriteBarrier(&____curowhaw_14, value);
	}

	inline static int32_t get_offset_of__mayqime_15() { return static_cast<int32_t>(offsetof(M_getowce218_t3995948169, ____mayqime_15)); }
	inline uint32_t get__mayqime_15() const { return ____mayqime_15; }
	inline uint32_t* get_address_of__mayqime_15() { return &____mayqime_15; }
	inline void set__mayqime_15(uint32_t value)
	{
		____mayqime_15 = value;
	}

	inline static int32_t get_offset_of__kurseeKexelair_16() { return static_cast<int32_t>(offsetof(M_getowce218_t3995948169, ____kurseeKexelair_16)); }
	inline bool get__kurseeKexelair_16() const { return ____kurseeKexelair_16; }
	inline bool* get_address_of__kurseeKexelair_16() { return &____kurseeKexelair_16; }
	inline void set__kurseeKexelair_16(bool value)
	{
		____kurseeKexelair_16 = value;
	}

	inline static int32_t get_offset_of__darbor_17() { return static_cast<int32_t>(offsetof(M_getowce218_t3995948169, ____darbor_17)); }
	inline int32_t get__darbor_17() const { return ____darbor_17; }
	inline int32_t* get_address_of__darbor_17() { return &____darbor_17; }
	inline void set__darbor_17(int32_t value)
	{
		____darbor_17 = value;
	}

	inline static int32_t get_offset_of__wudouDacer_18() { return static_cast<int32_t>(offsetof(M_getowce218_t3995948169, ____wudouDacer_18)); }
	inline uint32_t get__wudouDacer_18() const { return ____wudouDacer_18; }
	inline uint32_t* get_address_of__wudouDacer_18() { return &____wudouDacer_18; }
	inline void set__wudouDacer_18(uint32_t value)
	{
		____wudouDacer_18 = value;
	}

	inline static int32_t get_offset_of__nereresturHeremeryaw_19() { return static_cast<int32_t>(offsetof(M_getowce218_t3995948169, ____nereresturHeremeryaw_19)); }
	inline int32_t get__nereresturHeremeryaw_19() const { return ____nereresturHeremeryaw_19; }
	inline int32_t* get_address_of__nereresturHeremeryaw_19() { return &____nereresturHeremeryaw_19; }
	inline void set__nereresturHeremeryaw_19(int32_t value)
	{
		____nereresturHeremeryaw_19 = value;
	}

	inline static int32_t get_offset_of__mouwe_20() { return static_cast<int32_t>(offsetof(M_getowce218_t3995948169, ____mouwe_20)); }
	inline uint32_t get__mouwe_20() const { return ____mouwe_20; }
	inline uint32_t* get_address_of__mouwe_20() { return &____mouwe_20; }
	inline void set__mouwe_20(uint32_t value)
	{
		____mouwe_20 = value;
	}

	inline static int32_t get_offset_of__kitalMujoubir_21() { return static_cast<int32_t>(offsetof(M_getowce218_t3995948169, ____kitalMujoubir_21)); }
	inline bool get__kitalMujoubir_21() const { return ____kitalMujoubir_21; }
	inline bool* get_address_of__kitalMujoubir_21() { return &____kitalMujoubir_21; }
	inline void set__kitalMujoubir_21(bool value)
	{
		____kitalMujoubir_21 = value;
	}

	inline static int32_t get_offset_of__dorsayjo_22() { return static_cast<int32_t>(offsetof(M_getowce218_t3995948169, ____dorsayjo_22)); }
	inline String_t* get__dorsayjo_22() const { return ____dorsayjo_22; }
	inline String_t** get_address_of__dorsayjo_22() { return &____dorsayjo_22; }
	inline void set__dorsayjo_22(String_t* value)
	{
		____dorsayjo_22 = value;
		Il2CppCodeGenWriteBarrier(&____dorsayjo_22, value);
	}

	inline static int32_t get_offset_of__drunel_23() { return static_cast<int32_t>(offsetof(M_getowce218_t3995948169, ____drunel_23)); }
	inline bool get__drunel_23() const { return ____drunel_23; }
	inline bool* get_address_of__drunel_23() { return &____drunel_23; }
	inline void set__drunel_23(bool value)
	{
		____drunel_23 = value;
	}

	inline static int32_t get_offset_of__duhikeKairmowri_24() { return static_cast<int32_t>(offsetof(M_getowce218_t3995948169, ____duhikeKairmowri_24)); }
	inline bool get__duhikeKairmowri_24() const { return ____duhikeKairmowri_24; }
	inline bool* get_address_of__duhikeKairmowri_24() { return &____duhikeKairmowri_24; }
	inline void set__duhikeKairmowri_24(bool value)
	{
		____duhikeKairmowri_24 = value;
	}

	inline static int32_t get_offset_of__fehawcearSawpouba_25() { return static_cast<int32_t>(offsetof(M_getowce218_t3995948169, ____fehawcearSawpouba_25)); }
	inline float get__fehawcearSawpouba_25() const { return ____fehawcearSawpouba_25; }
	inline float* get_address_of__fehawcearSawpouba_25() { return &____fehawcearSawpouba_25; }
	inline void set__fehawcearSawpouba_25(float value)
	{
		____fehawcearSawpouba_25 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
