﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CSStoryData
struct CSStoryData_t151547823;

#include "codegen/il2cpp-codegen.h"

// System.Void CSStoryData::.ctor()
extern "C"  void CSStoryData__ctor_m3378790556 (CSStoryData_t151547823 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
