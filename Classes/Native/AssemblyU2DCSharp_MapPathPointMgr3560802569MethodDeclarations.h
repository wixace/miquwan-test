﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MapPathPointMgr
struct MapPathPointMgr_t3560802569;

#include "codegen/il2cpp-codegen.h"

// System.Void MapPathPointMgr::.ctor()
extern "C"  void MapPathPointMgr__ctor_m1453938818 (MapPathPointMgr_t3560802569 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MapPathPointMgr::Start()
extern "C"  void MapPathPointMgr_Start_m401076610 (MapPathPointMgr_t3560802569 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
