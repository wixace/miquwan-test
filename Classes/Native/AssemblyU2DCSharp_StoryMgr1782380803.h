﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// StoryMgr/StoryVO
struct StoryVO_t3225531714;
// System.Collections.Generic.List`1<StoryMgr/StoryVO>
struct List_1_t298749970;
// System.Comparison`1<StoryMgr/StoryVO>
struct Comparison_1_t1941892901;
// System.Action
struct Action_t3771233898;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StoryMgr
struct  StoryMgr_t1782380803  : public Il2CppObject
{
public:
	// StoryMgr/StoryVO StoryMgr::curStory
	StoryVO_t3225531714 * ___curStory_3;
	// System.Collections.Generic.List`1<StoryMgr/StoryVO> StoryMgr::storyQueue
	List_1_t298749970 * ___storyQueue_4;

public:
	inline static int32_t get_offset_of_curStory_3() { return static_cast<int32_t>(offsetof(StoryMgr_t1782380803, ___curStory_3)); }
	inline StoryVO_t3225531714 * get_curStory_3() const { return ___curStory_3; }
	inline StoryVO_t3225531714 ** get_address_of_curStory_3() { return &___curStory_3; }
	inline void set_curStory_3(StoryVO_t3225531714 * value)
	{
		___curStory_3 = value;
		Il2CppCodeGenWriteBarrier(&___curStory_3, value);
	}

	inline static int32_t get_offset_of_storyQueue_4() { return static_cast<int32_t>(offsetof(StoryMgr_t1782380803, ___storyQueue_4)); }
	inline List_1_t298749970 * get_storyQueue_4() const { return ___storyQueue_4; }
	inline List_1_t298749970 ** get_address_of_storyQueue_4() { return &___storyQueue_4; }
	inline void set_storyQueue_4(List_1_t298749970 * value)
	{
		___storyQueue_4 = value;
		Il2CppCodeGenWriteBarrier(&___storyQueue_4, value);
	}
};

struct StoryMgr_t1782380803_StaticFields
{
public:
	// System.Comparison`1<StoryMgr/StoryVO> StoryMgr::<>f__am$cache2
	Comparison_1_t1941892901 * ___U3CU3Ef__amU24cache2_5;
	// System.Action StoryMgr::<>f__am$cache3
	Action_t3771233898 * ___U3CU3Ef__amU24cache3_6;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_5() { return static_cast<int32_t>(offsetof(StoryMgr_t1782380803_StaticFields, ___U3CU3Ef__amU24cache2_5)); }
	inline Comparison_1_t1941892901 * get_U3CU3Ef__amU24cache2_5() const { return ___U3CU3Ef__amU24cache2_5; }
	inline Comparison_1_t1941892901 ** get_address_of_U3CU3Ef__amU24cache2_5() { return &___U3CU3Ef__amU24cache2_5; }
	inline void set_U3CU3Ef__amU24cache2_5(Comparison_1_t1941892901 * value)
	{
		___U3CU3Ef__amU24cache2_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache2_5, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_6() { return static_cast<int32_t>(offsetof(StoryMgr_t1782380803_StaticFields, ___U3CU3Ef__amU24cache3_6)); }
	inline Action_t3771233898 * get_U3CU3Ef__amU24cache3_6() const { return ___U3CU3Ef__amU24cache3_6; }
	inline Action_t3771233898 ** get_address_of_U3CU3Ef__amU24cache3_6() { return &___U3CU3Ef__amU24cache3_6; }
	inline void set_U3CU3Ef__amU24cache3_6(Action_t3771233898 * value)
	{
		___U3CU3Ef__amU24cache3_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache3_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
