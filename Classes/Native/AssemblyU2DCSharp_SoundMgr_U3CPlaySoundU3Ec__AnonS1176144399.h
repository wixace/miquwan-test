﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// SoundCfg
struct SoundCfg_t1807275253;
// SoundMgr
struct SoundMgr_t1807284905;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SoundMgr/<PlaySound>c__AnonStorey15A
struct  U3CPlaySoundU3Ec__AnonStorey15A_t1176144399  : public Il2CppObject
{
public:
	// SoundCfg SoundMgr/<PlaySound>c__AnonStorey15A::soundcfg
	SoundCfg_t1807275253 * ___soundcfg_0;
	// System.Int32 SoundMgr/<PlaySound>c__AnonStorey15A::id
	int32_t ___id_1;
	// SoundMgr SoundMgr/<PlaySound>c__AnonStorey15A::<>f__this
	SoundMgr_t1807284905 * ___U3CU3Ef__this_2;

public:
	inline static int32_t get_offset_of_soundcfg_0() { return static_cast<int32_t>(offsetof(U3CPlaySoundU3Ec__AnonStorey15A_t1176144399, ___soundcfg_0)); }
	inline SoundCfg_t1807275253 * get_soundcfg_0() const { return ___soundcfg_0; }
	inline SoundCfg_t1807275253 ** get_address_of_soundcfg_0() { return &___soundcfg_0; }
	inline void set_soundcfg_0(SoundCfg_t1807275253 * value)
	{
		___soundcfg_0 = value;
		Il2CppCodeGenWriteBarrier(&___soundcfg_0, value);
	}

	inline static int32_t get_offset_of_id_1() { return static_cast<int32_t>(offsetof(U3CPlaySoundU3Ec__AnonStorey15A_t1176144399, ___id_1)); }
	inline int32_t get_id_1() const { return ___id_1; }
	inline int32_t* get_address_of_id_1() { return &___id_1; }
	inline void set_id_1(int32_t value)
	{
		___id_1 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_2() { return static_cast<int32_t>(offsetof(U3CPlaySoundU3Ec__AnonStorey15A_t1176144399, ___U3CU3Ef__this_2)); }
	inline SoundMgr_t1807284905 * get_U3CU3Ef__this_2() const { return ___U3CU3Ef__this_2; }
	inline SoundMgr_t1807284905 ** get_address_of_U3CU3Ef__this_2() { return &___U3CU3Ef__this_2; }
	inline void set_U3CU3Ef__this_2(SoundMgr_t1807284905 * value)
	{
		___U3CU3Ef__this_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
