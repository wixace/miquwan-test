﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// taixuTipCfg
struct taixuTipCfg_t1268722370;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;

#include "codegen/il2cpp-codegen.h"

// System.Void taixuTipCfg::.ctor()
extern "C"  void taixuTipCfg__ctor_m3601025769 (taixuTipCfg_t1268722370 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void taixuTipCfg::Init(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C"  void taixuTipCfg_Init_m4119334908 (taixuTipCfg_t1268722370 * __this, Dictionary_2_t827649927 * ____info0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
