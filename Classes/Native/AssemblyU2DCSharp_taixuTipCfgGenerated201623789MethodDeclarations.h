﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// taixuTipCfgGenerated
struct taixuTipCfgGenerated_t201623789;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_String7231557.h"

// System.Void taixuTipCfgGenerated::.ctor()
extern "C"  void taixuTipCfgGenerated__ctor_m2075823182 (taixuTipCfgGenerated_t201623789 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean taixuTipCfgGenerated::taixuTipCfg_taixuTipCfg1(JSVCall,System.Int32)
extern "C"  bool taixuTipCfgGenerated_taixuTipCfg_taixuTipCfg1_m3340765670 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void taixuTipCfgGenerated::taixuTipCfg_id(JSVCall)
extern "C"  void taixuTipCfgGenerated_taixuTipCfg_id_m3856270169 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void taixuTipCfgGenerated::taixuTipCfg_order(JSVCall)
extern "C"  void taixuTipCfgGenerated_taixuTipCfg_order_m2835527338 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void taixuTipCfgGenerated::taixuTipCfg_des(JSVCall)
extern "C"  void taixuTipCfgGenerated_taixuTipCfg_des_m4251146982 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void taixuTipCfgGenerated::taixuTipCfg_image(JSVCall)
extern "C"  void taixuTipCfgGenerated_taixuTipCfg_image_m1614451517 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean taixuTipCfgGenerated::taixuTipCfg_Init__DictionaryT2_String_String(JSVCall,System.Int32)
extern "C"  bool taixuTipCfgGenerated_taixuTipCfg_Init__DictionaryT2_String_String_m2865942811 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void taixuTipCfgGenerated::__Register()
extern "C"  void taixuTipCfgGenerated___Register_m2579001657 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 taixuTipCfgGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t taixuTipCfgGenerated_ilo_getObject1_m4011030660 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void taixuTipCfgGenerated::ilo_addJSCSRel2(System.Int32,System.Object)
extern "C"  void taixuTipCfgGenerated_ilo_addJSCSRel2_m3798366347 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void taixuTipCfgGenerated::ilo_setStringS3(System.Int32,System.String)
extern "C"  void taixuTipCfgGenerated_ilo_setStringS3_m471800089 (Il2CppObject * __this /* static, unused */, int32_t ___e0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
