﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_zalbijouCapar172
struct  M_zalbijouCapar172_t1042014461  : public Il2CppObject
{
public:
	// System.Boolean GarbageiOS.M_zalbijouCapar172::_loukewhearTacai
	bool ____loukewhearTacai_0;
	// System.Int32 GarbageiOS.M_zalbijouCapar172::_vuyemnorLacoulee
	int32_t ____vuyemnorLacoulee_1;
	// System.Single GarbageiOS.M_zalbijouCapar172::_zowpeLorjaypir
	float ____zowpeLorjaypir_2;
	// System.Single GarbageiOS.M_zalbijouCapar172::_rujarwooPoolaybere
	float ____rujarwooPoolaybere_3;
	// System.Boolean GarbageiOS.M_zalbijouCapar172::_mereceaqallMerwousear
	bool ____mereceaqallMerwousear_4;
	// System.Single GarbageiOS.M_zalbijouCapar172::_birnifow
	float ____birnifow_5;
	// System.UInt32 GarbageiOS.M_zalbijouCapar172::_gasfe
	uint32_t ____gasfe_6;
	// System.UInt32 GarbageiOS.M_zalbijouCapar172::_whairjiMerwow
	uint32_t ____whairjiMerwow_7;
	// System.UInt32 GarbageiOS.M_zalbijouCapar172::_jeejoolo
	uint32_t ____jeejoolo_8;
	// System.Int32 GarbageiOS.M_zalbijouCapar172::_tigirqow
	int32_t ____tigirqow_9;
	// System.Single GarbageiOS.M_zalbijouCapar172::_cemyalllow
	float ____cemyalllow_10;
	// System.UInt32 GarbageiOS.M_zalbijouCapar172::_peezoyaWhirparhu
	uint32_t ____peezoyaWhirparhu_11;
	// System.Single GarbageiOS.M_zalbijouCapar172::_xassemPisall
	float ____xassemPisall_12;
	// System.Boolean GarbageiOS.M_zalbijouCapar172::_sasowHeatre
	bool ____sasowHeatre_13;
	// System.String GarbageiOS.M_zalbijouCapar172::_rallnuse
	String_t* ____rallnuse_14;
	// System.Boolean GarbageiOS.M_zalbijouCapar172::_neehurStarcel
	bool ____neehurStarcel_15;
	// System.String GarbageiOS.M_zalbijouCapar172::_telcenaw
	String_t* ____telcenaw_16;
	// System.Int32 GarbageiOS.M_zalbijouCapar172::_keestuTela
	int32_t ____keestuTela_17;
	// System.Int32 GarbageiOS.M_zalbijouCapar172::_chonaw
	int32_t ____chonaw_18;
	// System.UInt32 GarbageiOS.M_zalbijouCapar172::_recelVowpeere
	uint32_t ____recelVowpeere_19;
	// System.Int32 GarbageiOS.M_zalbijouCapar172::_mekay
	int32_t ____mekay_20;
	// System.Boolean GarbageiOS.M_zalbijouCapar172::_lerwel
	bool ____lerwel_21;
	// System.Single GarbageiOS.M_zalbijouCapar172::_yallyustoJeejee
	float ____yallyustoJeejee_22;
	// System.Single GarbageiOS.M_zalbijouCapar172::_seerouRewallsa
	float ____seerouRewallsa_23;

public:
	inline static int32_t get_offset_of__loukewhearTacai_0() { return static_cast<int32_t>(offsetof(M_zalbijouCapar172_t1042014461, ____loukewhearTacai_0)); }
	inline bool get__loukewhearTacai_0() const { return ____loukewhearTacai_0; }
	inline bool* get_address_of__loukewhearTacai_0() { return &____loukewhearTacai_0; }
	inline void set__loukewhearTacai_0(bool value)
	{
		____loukewhearTacai_0 = value;
	}

	inline static int32_t get_offset_of__vuyemnorLacoulee_1() { return static_cast<int32_t>(offsetof(M_zalbijouCapar172_t1042014461, ____vuyemnorLacoulee_1)); }
	inline int32_t get__vuyemnorLacoulee_1() const { return ____vuyemnorLacoulee_1; }
	inline int32_t* get_address_of__vuyemnorLacoulee_1() { return &____vuyemnorLacoulee_1; }
	inline void set__vuyemnorLacoulee_1(int32_t value)
	{
		____vuyemnorLacoulee_1 = value;
	}

	inline static int32_t get_offset_of__zowpeLorjaypir_2() { return static_cast<int32_t>(offsetof(M_zalbijouCapar172_t1042014461, ____zowpeLorjaypir_2)); }
	inline float get__zowpeLorjaypir_2() const { return ____zowpeLorjaypir_2; }
	inline float* get_address_of__zowpeLorjaypir_2() { return &____zowpeLorjaypir_2; }
	inline void set__zowpeLorjaypir_2(float value)
	{
		____zowpeLorjaypir_2 = value;
	}

	inline static int32_t get_offset_of__rujarwooPoolaybere_3() { return static_cast<int32_t>(offsetof(M_zalbijouCapar172_t1042014461, ____rujarwooPoolaybere_3)); }
	inline float get__rujarwooPoolaybere_3() const { return ____rujarwooPoolaybere_3; }
	inline float* get_address_of__rujarwooPoolaybere_3() { return &____rujarwooPoolaybere_3; }
	inline void set__rujarwooPoolaybere_3(float value)
	{
		____rujarwooPoolaybere_3 = value;
	}

	inline static int32_t get_offset_of__mereceaqallMerwousear_4() { return static_cast<int32_t>(offsetof(M_zalbijouCapar172_t1042014461, ____mereceaqallMerwousear_4)); }
	inline bool get__mereceaqallMerwousear_4() const { return ____mereceaqallMerwousear_4; }
	inline bool* get_address_of__mereceaqallMerwousear_4() { return &____mereceaqallMerwousear_4; }
	inline void set__mereceaqallMerwousear_4(bool value)
	{
		____mereceaqallMerwousear_4 = value;
	}

	inline static int32_t get_offset_of__birnifow_5() { return static_cast<int32_t>(offsetof(M_zalbijouCapar172_t1042014461, ____birnifow_5)); }
	inline float get__birnifow_5() const { return ____birnifow_5; }
	inline float* get_address_of__birnifow_5() { return &____birnifow_5; }
	inline void set__birnifow_5(float value)
	{
		____birnifow_5 = value;
	}

	inline static int32_t get_offset_of__gasfe_6() { return static_cast<int32_t>(offsetof(M_zalbijouCapar172_t1042014461, ____gasfe_6)); }
	inline uint32_t get__gasfe_6() const { return ____gasfe_6; }
	inline uint32_t* get_address_of__gasfe_6() { return &____gasfe_6; }
	inline void set__gasfe_6(uint32_t value)
	{
		____gasfe_6 = value;
	}

	inline static int32_t get_offset_of__whairjiMerwow_7() { return static_cast<int32_t>(offsetof(M_zalbijouCapar172_t1042014461, ____whairjiMerwow_7)); }
	inline uint32_t get__whairjiMerwow_7() const { return ____whairjiMerwow_7; }
	inline uint32_t* get_address_of__whairjiMerwow_7() { return &____whairjiMerwow_7; }
	inline void set__whairjiMerwow_7(uint32_t value)
	{
		____whairjiMerwow_7 = value;
	}

	inline static int32_t get_offset_of__jeejoolo_8() { return static_cast<int32_t>(offsetof(M_zalbijouCapar172_t1042014461, ____jeejoolo_8)); }
	inline uint32_t get__jeejoolo_8() const { return ____jeejoolo_8; }
	inline uint32_t* get_address_of__jeejoolo_8() { return &____jeejoolo_8; }
	inline void set__jeejoolo_8(uint32_t value)
	{
		____jeejoolo_8 = value;
	}

	inline static int32_t get_offset_of__tigirqow_9() { return static_cast<int32_t>(offsetof(M_zalbijouCapar172_t1042014461, ____tigirqow_9)); }
	inline int32_t get__tigirqow_9() const { return ____tigirqow_9; }
	inline int32_t* get_address_of__tigirqow_9() { return &____tigirqow_9; }
	inline void set__tigirqow_9(int32_t value)
	{
		____tigirqow_9 = value;
	}

	inline static int32_t get_offset_of__cemyalllow_10() { return static_cast<int32_t>(offsetof(M_zalbijouCapar172_t1042014461, ____cemyalllow_10)); }
	inline float get__cemyalllow_10() const { return ____cemyalllow_10; }
	inline float* get_address_of__cemyalllow_10() { return &____cemyalllow_10; }
	inline void set__cemyalllow_10(float value)
	{
		____cemyalllow_10 = value;
	}

	inline static int32_t get_offset_of__peezoyaWhirparhu_11() { return static_cast<int32_t>(offsetof(M_zalbijouCapar172_t1042014461, ____peezoyaWhirparhu_11)); }
	inline uint32_t get__peezoyaWhirparhu_11() const { return ____peezoyaWhirparhu_11; }
	inline uint32_t* get_address_of__peezoyaWhirparhu_11() { return &____peezoyaWhirparhu_11; }
	inline void set__peezoyaWhirparhu_11(uint32_t value)
	{
		____peezoyaWhirparhu_11 = value;
	}

	inline static int32_t get_offset_of__xassemPisall_12() { return static_cast<int32_t>(offsetof(M_zalbijouCapar172_t1042014461, ____xassemPisall_12)); }
	inline float get__xassemPisall_12() const { return ____xassemPisall_12; }
	inline float* get_address_of__xassemPisall_12() { return &____xassemPisall_12; }
	inline void set__xassemPisall_12(float value)
	{
		____xassemPisall_12 = value;
	}

	inline static int32_t get_offset_of__sasowHeatre_13() { return static_cast<int32_t>(offsetof(M_zalbijouCapar172_t1042014461, ____sasowHeatre_13)); }
	inline bool get__sasowHeatre_13() const { return ____sasowHeatre_13; }
	inline bool* get_address_of__sasowHeatre_13() { return &____sasowHeatre_13; }
	inline void set__sasowHeatre_13(bool value)
	{
		____sasowHeatre_13 = value;
	}

	inline static int32_t get_offset_of__rallnuse_14() { return static_cast<int32_t>(offsetof(M_zalbijouCapar172_t1042014461, ____rallnuse_14)); }
	inline String_t* get__rallnuse_14() const { return ____rallnuse_14; }
	inline String_t** get_address_of__rallnuse_14() { return &____rallnuse_14; }
	inline void set__rallnuse_14(String_t* value)
	{
		____rallnuse_14 = value;
		Il2CppCodeGenWriteBarrier(&____rallnuse_14, value);
	}

	inline static int32_t get_offset_of__neehurStarcel_15() { return static_cast<int32_t>(offsetof(M_zalbijouCapar172_t1042014461, ____neehurStarcel_15)); }
	inline bool get__neehurStarcel_15() const { return ____neehurStarcel_15; }
	inline bool* get_address_of__neehurStarcel_15() { return &____neehurStarcel_15; }
	inline void set__neehurStarcel_15(bool value)
	{
		____neehurStarcel_15 = value;
	}

	inline static int32_t get_offset_of__telcenaw_16() { return static_cast<int32_t>(offsetof(M_zalbijouCapar172_t1042014461, ____telcenaw_16)); }
	inline String_t* get__telcenaw_16() const { return ____telcenaw_16; }
	inline String_t** get_address_of__telcenaw_16() { return &____telcenaw_16; }
	inline void set__telcenaw_16(String_t* value)
	{
		____telcenaw_16 = value;
		Il2CppCodeGenWriteBarrier(&____telcenaw_16, value);
	}

	inline static int32_t get_offset_of__keestuTela_17() { return static_cast<int32_t>(offsetof(M_zalbijouCapar172_t1042014461, ____keestuTela_17)); }
	inline int32_t get__keestuTela_17() const { return ____keestuTela_17; }
	inline int32_t* get_address_of__keestuTela_17() { return &____keestuTela_17; }
	inline void set__keestuTela_17(int32_t value)
	{
		____keestuTela_17 = value;
	}

	inline static int32_t get_offset_of__chonaw_18() { return static_cast<int32_t>(offsetof(M_zalbijouCapar172_t1042014461, ____chonaw_18)); }
	inline int32_t get__chonaw_18() const { return ____chonaw_18; }
	inline int32_t* get_address_of__chonaw_18() { return &____chonaw_18; }
	inline void set__chonaw_18(int32_t value)
	{
		____chonaw_18 = value;
	}

	inline static int32_t get_offset_of__recelVowpeere_19() { return static_cast<int32_t>(offsetof(M_zalbijouCapar172_t1042014461, ____recelVowpeere_19)); }
	inline uint32_t get__recelVowpeere_19() const { return ____recelVowpeere_19; }
	inline uint32_t* get_address_of__recelVowpeere_19() { return &____recelVowpeere_19; }
	inline void set__recelVowpeere_19(uint32_t value)
	{
		____recelVowpeere_19 = value;
	}

	inline static int32_t get_offset_of__mekay_20() { return static_cast<int32_t>(offsetof(M_zalbijouCapar172_t1042014461, ____mekay_20)); }
	inline int32_t get__mekay_20() const { return ____mekay_20; }
	inline int32_t* get_address_of__mekay_20() { return &____mekay_20; }
	inline void set__mekay_20(int32_t value)
	{
		____mekay_20 = value;
	}

	inline static int32_t get_offset_of__lerwel_21() { return static_cast<int32_t>(offsetof(M_zalbijouCapar172_t1042014461, ____lerwel_21)); }
	inline bool get__lerwel_21() const { return ____lerwel_21; }
	inline bool* get_address_of__lerwel_21() { return &____lerwel_21; }
	inline void set__lerwel_21(bool value)
	{
		____lerwel_21 = value;
	}

	inline static int32_t get_offset_of__yallyustoJeejee_22() { return static_cast<int32_t>(offsetof(M_zalbijouCapar172_t1042014461, ____yallyustoJeejee_22)); }
	inline float get__yallyustoJeejee_22() const { return ____yallyustoJeejee_22; }
	inline float* get_address_of__yallyustoJeejee_22() { return &____yallyustoJeejee_22; }
	inline void set__yallyustoJeejee_22(float value)
	{
		____yallyustoJeejee_22 = value;
	}

	inline static int32_t get_offset_of__seerouRewallsa_23() { return static_cast<int32_t>(offsetof(M_zalbijouCapar172_t1042014461, ____seerouRewallsa_23)); }
	inline float get__seerouRewallsa_23() const { return ____seerouRewallsa_23; }
	inline float* get_address_of__seerouRewallsa_23() { return &____seerouRewallsa_23; }
	inline void set__seerouRewallsa_23(float value)
	{
		____seerouRewallsa_23 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
