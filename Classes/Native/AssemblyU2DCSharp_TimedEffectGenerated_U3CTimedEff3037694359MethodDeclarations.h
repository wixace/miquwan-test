﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TimedEffectGenerated/<TimedEffect_SetData_GetDelegate_member1_arg0>c__AnonStorey9C
struct U3CTimedEffect_SetData_GetDelegate_member1_arg0U3Ec__AnonStorey9C_t3037694359;

#include "codegen/il2cpp-codegen.h"

// System.Void TimedEffectGenerated/<TimedEffect_SetData_GetDelegate_member1_arg0>c__AnonStorey9C::.ctor()
extern "C"  void U3CTimedEffect_SetData_GetDelegate_member1_arg0U3Ec__AnonStorey9C__ctor_m2063962340 (U3CTimedEffect_SetData_GetDelegate_member1_arg0U3Ec__AnonStorey9C_t3037694359 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TimedEffectGenerated/<TimedEffect_SetData_GetDelegate_member1_arg0>c__AnonStorey9C::<>m__FB()
extern "C"  void U3CTimedEffect_SetData_GetDelegate_member1_arg0U3Ec__AnonStorey9C_U3CU3Em__FB_m1501600873 (U3CTimedEffect_SetData_GetDelegate_member1_arg0U3Ec__AnonStorey9C_t3037694359 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
