﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mihua.Asset.AssetMgr
struct AssetMgr_t543080634;
// System.String
struct String_t;
// UnityEngine.Object
struct Object_t3071478659;
// UnityEngine.AssetBundle
struct AssetBundle_t2070959688;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1375417109;
// Mihua.Asset.ABLoadOperation.ABOperation
struct ABOperation_t1894014760;
// Mihua.Asset.ABLoadOperation.AssetOperation
struct AssetOperation_t778728221;
// Mihua.Asset.OnLoadAsset
struct OnLoadAsset_t4181543125;
// System.Object
struct Il2CppObject;
// ConfigAssetMgr
struct ConfigAssetMgr_t4036193930;
// Newtonsoft.Json.Linq.JToken
struct JToken_t3412245951;
// Newtonsoft.Json.Linq.JObject
struct JObject_t1798544199;
// ByteReadArray
struct ByteReadArray_t751193627;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Mihua_Asset_AssetMgr_LogType3546132203.h"
#include "mscorlib_System_String7231557.h"
#include "UnityEngine_UnityEngine_Object3071478659.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24287931429.h"
#include "AssemblyU2DCSharp_Mihua_Asset_OnLoadAsset4181543125.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_ConfigAssetMgr4036193930.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JObject1798544199.h"
#include "AssemblyU2DCSharp_ByteReadArray751193627.h"
#include "AssemblyU2DCSharp_Mihua_Asset_ABLoadOperation_ABOp1894014760.h"

// System.Void Mihua.Asset.AssetMgr::.ctor()
extern "C"  void AssetMgr__ctor_m4222063775 (AssetMgr_t543080634 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua.Asset.AssetMgr::.cctor()
extern "C"  void AssetMgr__cctor_m1552861934 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mihua.Asset.AssetMgr::get_unloading()
extern "C"  bool AssetMgr_get_unloading_m1335632899 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua.Asset.AssetMgr::set_unloading(System.Boolean)
extern "C"  void AssetMgr_set_unloading_m3167023482 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua.Asset.AssetMgr::Log(Mihua.Asset.AssetMgr/LogType,System.String)
extern "C"  void AssetMgr_Log_m3521656626 (Il2CppObject * __this /* static, unused */, int32_t ___logType0, String_t* ___text1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua.Asset.AssetMgr::UnloadUnusedAssets()
extern "C"  void AssetMgr_UnloadUnusedAssets_m3956962653 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua.Asset.AssetMgr::AddCache(System.String,UnityEngine.Object)
extern "C"  void AssetMgr_AddCache_m2523500148 (Il2CppObject * __this /* static, unused */, String_t* ____assetName0, Object_t3071478659 * ___asset1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AssetBundle Mihua.Asset.AssetMgr::GetAssetBundles(System.String)
extern "C"  AssetBundle_t2070959688 * AssetMgr_GetAssetBundles_m1285657958 (Il2CppObject * __this /* static, unused */, String_t* ____assetName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua.Asset.AssetMgr::Initialize()
extern "C"  void AssetMgr_Initialize_m95669589 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua.Asset.AssetMgr::LoadAssetBundles(System.String)
extern "C"  void AssetMgr_LoadAssetBundles_m2459345494 (Il2CppObject * __this /* static, unused */, String_t* ___assetBundleName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua.Asset.AssetMgr::LoadAssetBundle(System.String)
extern "C"  void AssetMgr_LoadAssetBundle_m3157163801 (Il2CppObject * __this /* static, unused */, String_t* ___assetBundleName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mihua.Asset.AssetMgr::LoadAssetBundleInternal(System.String)
extern "C"  bool AssetMgr_LoadAssetBundleInternal_m3312349008 (Il2CppObject * __this /* static, unused */, String_t* ___assetBundleName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mihua.Asset.AssetMgr::GetAssetsFilePath(System.Int32)
extern "C"  String_t* AssetMgr_GetAssetsFilePath_m3057013541 (Il2CppObject * __this /* static, unused */, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32> Mihua.Asset.AssetMgr::GetAssetsIndex(System.String)
extern "C"  KeyValuePair_2_t4287931429  AssetMgr_GetAssetsIndex_m787234076 (Il2CppObject * __this /* static, unused */, String_t* ___abpath0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua.Asset.AssetMgr::LoadDependencies(System.String)
extern "C"  void AssetMgr_LoadDependencies_m3552588622 (Il2CppObject * __this /* static, unused */, String_t* ___assetBundleName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mihua.Asset.AssetMgr::Exists(System.String)
extern "C"  bool AssetMgr_Exists_m3121623117 (Il2CppObject * __this /* static, unused */, String_t* ___assetName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mihua.Asset.AssetMgr::Exists(System.Collections.Generic.List`1<System.String>)
extern "C"  bool AssetMgr_Exists_m2655380233 (Il2CppObject * __this /* static, unused */, List_1_t1375417109 * ___assetNames0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua.Asset.AssetMgr::DownLoad(System.String)
extern "C"  void AssetMgr_DownLoad_m3568262837 (Il2CppObject * __this /* static, unused */, String_t* ___assetName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua.Asset.AssetMgr::DownLoad(System.Collections.Generic.List`1<System.String>)
extern "C"  void AssetMgr_DownLoad_m3709262753 (Il2CppObject * __this /* static, unused */, List_1_t1375417109 * ___assetNames0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mihua.Asset.ABLoadOperation.ABOperation Mihua.Asset.AssetMgr::PreloadingAsync(System.Collections.Generic.List`1<System.String>)
extern "C"  ABOperation_t1894014760 * AssetMgr_PreloadingAsync_m2847318405 (Il2CppObject * __this /* static, unused */, List_1_t1375417109 * ___list0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mihua.Asset.ABLoadOperation.AssetOperation Mihua.Asset.AssetMgr::LoadAssetAsync(System.String)
extern "C"  AssetOperation_t778728221 * AssetMgr_LoadAssetAsync_m2746356231 (Il2CppObject * __this /* static, unused */, String_t* ___assetName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mihua.Asset.ABLoadOperation.AssetOperation Mihua.Asset.AssetMgr::LoadAssetAsync(System.String,Mihua.Asset.OnLoadAsset)
extern "C"  AssetOperation_t778728221 * AssetMgr_LoadAssetAsync_m3753708924 (Il2CppObject * __this /* static, unused */, String_t* ___assetName0, OnLoadAsset_t4181543125 * ___onLoadAsset1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mihua.Asset.ABLoadOperation.AssetOperation Mihua.Asset.AssetMgr::LoadAssetAsync(System.String,Mihua.Asset.OnLoadAsset,System.Boolean)
extern "C"  AssetOperation_t778728221 * AssetMgr_LoadAssetAsync_m3301908097 (Il2CppObject * __this /* static, unused */, String_t* ___assetName0, OnLoadAsset_t4181543125 * ___onLoadAsset1, bool ___isAsync2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mihua.Asset.ABLoadOperation.ABOperation Mihua.Asset.AssetMgr::LoadLevelAsync(System.String,System.String)
extern "C"  ABOperation_t1894014760 * AssetMgr_LoadLevelAsync_m4184581132 (Il2CppObject * __this /* static, unused */, String_t* ___levelPath0, String_t* ___levelName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua.Asset.AssetMgr::SetNextNeedAssets(System.Collections.Generic.List`1<System.String>,System.Boolean)
extern "C"  void AssetMgr_SetNextNeedAssets_m2323979038 (Il2CppObject * __this /* static, unused */, List_1_t1375417109 * ___needList0, bool ___is3D1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua.Asset.AssetMgr::Update()
extern "C"  void AssetMgr_Update_m3761820238 (AssetMgr_t543080634 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua.Asset.AssetMgr::ClearAll()
extern "C"  void AssetMgr_ClearAll_m2578308569 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua.Asset.AssetMgr::Clear()
extern "C"  void AssetMgr_Clear_m1628197066 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua.Asset.AssetMgr::ilo_Log1(System.Object,System.Boolean)
extern "C"  void AssetMgr_ilo_Log1_m3626237646 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mihua.Asset.AssetMgr::ilo_LoadPathToABPath2(ConfigAssetMgr,System.String)
extern "C"  String_t* AssetMgr_ilo_LoadPathToABPath2_m1622153439 (Il2CppObject * __this /* static, unused */, ConfigAssetMgr_t4036193930 * ____this0, String_t* ___path1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua.Asset.AssetMgr::ilo_LoadAssetBundle3(System.String)
extern "C"  void AssetMgr_ilo_LoadAssetBundle3_m1366751235 (Il2CppObject * __this /* static, unused */, String_t* ___assetBundleName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Mihua.Asset.AssetMgr::ilo_get_Item4(Newtonsoft.Json.Linq.JObject,System.String)
extern "C"  JToken_t3412245951 * AssetMgr_ilo_get_Item4_m2536778947 (Il2CppObject * __this /* static, unused */, JObject_t1798544199 * ____this0, String_t* ___propertyName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mihua.Asset.AssetMgr::ilo_get_Length5(ByteReadArray)
extern "C"  int32_t AssetMgr_ilo_get_Length5_m2274476021 (Il2CppObject * __this /* static, unused */, ByteReadArray_t751193627 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mihua.Asset.AssetMgr::ilo_ReadUTF6(ByteReadArray)
extern "C"  String_t* AssetMgr_ilo_ReadUTF6_m794402359 (Il2CppObject * __this /* static, unused */, ByteReadArray_t751193627 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mihua.Asset.AssetMgr::ilo_get_Postion7(ByteReadArray)
extern "C"  int32_t AssetMgr_ilo_get_Postion7_m4022656345 (Il2CppObject * __this /* static, unused */, ByteReadArray_t751193627 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua.Asset.AssetMgr::ilo_LogError8(System.Object,System.Boolean)
extern "C"  void AssetMgr_ilo_LogError8_m1973499559 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mihua.Asset.AssetMgr::ilo_GetDepend9(System.String,System.String[]&)
extern "C"  bool AssetMgr_ilo_GetDepend9_m2179999539 (Il2CppObject * __this /* static, unused */, String_t* ___key0, StringU5BU5D_t4054002952** ___depends1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua.Asset.AssetMgr::ilo_DownLoad10(System.String)
extern "C"  void AssetMgr_ilo_DownLoad10_m1860674787 (Il2CppObject * __this /* static, unused */, String_t* ___assetName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua.Asset.AssetMgr::ilo_Log11(Mihua.Asset.AssetMgr/LogType,System.String)
extern "C"  void AssetMgr_ilo_Log11_m746338687 (Il2CppObject * __this /* static, unused */, int32_t ___logType0, String_t* ___text1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mihua.Asset.AssetMgr::ilo_get_unloading12()
extern "C"  bool AssetMgr_ilo_get_unloading12_m1775847313 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mihua.Asset.AssetMgr::ilo_IsDone13(Mihua.Asset.ABLoadOperation.ABOperation)
extern "C"  bool AssetMgr_ilo_IsDone13_m3600928596 (Il2CppObject * __this /* static, unused */, ABOperation_t1894014760 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua.Asset.AssetMgr::ilo_CacheAsset14(Mihua.Asset.ABLoadOperation.ABOperation)
extern "C"  void AssetMgr_ilo_CacheAsset14_m2939182373 (Il2CppObject * __this /* static, unused */, ABOperation_t1894014760 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua.Asset.AssetMgr::ilo_Clear15(Mihua.Asset.ABLoadOperation.ABOperation)
extern "C"  void AssetMgr_ilo_Clear15_m249353907 (Il2CppObject * __this /* static, unused */, ABOperation_t1894014760 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
