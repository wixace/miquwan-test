﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HttpsData
struct HttpsData_t2200837909;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void HttpsData::.ctor()
extern "C"  void HttpsData__ctor_m2623938038 (HttpsData_t2200837909 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HttpsData::.ctor(System.String)
extern "C"  void HttpsData__ctor_m984768716 (HttpsData_t2200837909 * __this, String_t* ___url0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
