﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1190435706MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,monstersCfg>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m2701368189(__this, ___dictionary0, method) ((  void (*) (Enumerator_t2856982994 *, Dictionary_2_t1539659602 *, const MethodInfo*))Enumerator__ctor_m2377115088_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,monstersCfg>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1199712590(__this, method) ((  Il2CppObject * (*) (Enumerator_t2856982994 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1037642267_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,monstersCfg>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1495212312(__this, method) ((  void (*) (Enumerator_t2856982994 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2809374949_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,monstersCfg>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m4078781327(__this, method) ((  DictionaryEntry_t1751606614  (*) (Enumerator_t2856982994 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2434214620_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,monstersCfg>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3912545194(__this, method) ((  Il2CppObject * (*) (Enumerator_t2856982994 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3735627447_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,monstersCfg>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2907983804(__this, method) ((  Il2CppObject * (*) (Enumerator_t2856982994 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m393753481_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,monstersCfg>::MoveNext()
#define Enumerator_MoveNext_m400556872(__this, method) ((  bool (*) (Enumerator_t2856982994 *, const MethodInfo*))Enumerator_MoveNext_m1213995029_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,monstersCfg>::get_Current()
#define Enumerator_get_Current_m2512568820(__this, method) ((  KeyValuePair_2_t1438440308  (*) (Enumerator_t2856982994 *, const MethodInfo*))Enumerator_get_Current_m1399860359_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,monstersCfg>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m987433809(__this, method) ((  int32_t (*) (Enumerator_t2856982994 *, const MethodInfo*))Enumerator_get_CurrentKey_m1767398110_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,monstersCfg>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m3703381201(__this, method) ((  monstersCfg_t1542396363 * (*) (Enumerator_t2856982994 *, const MethodInfo*))Enumerator_get_CurrentValue_m3384846750_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,monstersCfg>::Reset()
#define Enumerator_Reset_m1405110863(__this, method) ((  void (*) (Enumerator_t2856982994 *, const MethodInfo*))Enumerator_Reset_m1080084514_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,monstersCfg>::VerifyState()
#define Enumerator_VerifyState_m2516607768(__this, method) ((  void (*) (Enumerator_t2856982994 *, const MethodInfo*))Enumerator_VerifyState_m2404513451_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,monstersCfg>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m3138349184(__this, method) ((  void (*) (Enumerator_t2856982994 *, const MethodInfo*))Enumerator_VerifyCurrent_m2789892947_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,monstersCfg>::Dispose()
#define Enumerator_Dispose_m4215237471(__this, method) ((  void (*) (Enumerator_t2856982994 *, const MethodInfo*))Enumerator_Dispose_m1102561394_gshared)(__this, method)
