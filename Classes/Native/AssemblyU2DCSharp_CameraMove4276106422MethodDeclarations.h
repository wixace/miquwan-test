﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraMove
struct CameraMove_t4276106422;
// UnityEngine.Transform
struct Transform_t1659122786;
// HeroMgr
struct HeroMgr_t2475965342;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Transform1659122786.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void CameraMove::.ctor()
extern "C"  void CameraMove__ctor_m4028387493 (CameraMove_t4276106422 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraMove::LateUpdate()
extern "C"  void CameraMove_LateUpdate_m3615135950 (CameraMove_t4276106422 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraMove::NearBy(UnityEngine.Transform,System.Single,System.Single,UnityEngine.Vector3)
extern "C"  void CameraMove_NearBy_m4234711612 (CameraMove_t4276106422 * __this, Transform_t1659122786 * ___target0, float ___nearTime1, float ___farTime2, Vector3_t4282066566  ___zoominoffset3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraMove::FarAway()
extern "C"  void CameraMove_FarAway_m3104372392 (CameraMove_t4276106422 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HeroMgr CameraMove::ilo_get_instance1()
extern "C"  HeroMgr_t2475965342 * CameraMove_ilo_get_instance1_m3602738254 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
