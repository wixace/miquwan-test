﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_tremperGerege187
struct M_tremperGerege187_t3246597050;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"

// System.Void GarbageiOS.M_tremperGerege187::.ctor()
extern "C"  void M_tremperGerege187__ctor_m892660665 (M_tremperGerege187_t3246597050 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_tremperGerege187::M_woso0(System.String[],System.Int32)
extern "C"  void M_tremperGerege187_M_woso0_m3888727480 (M_tremperGerege187_t3246597050 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_tremperGerege187::M_chearsagoNinaiwhi1(System.String[],System.Int32)
extern "C"  void M_tremperGerege187_M_chearsagoNinaiwhi1_m2210239265 (M_tremperGerege187_t3246597050 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
