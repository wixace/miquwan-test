﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_muvurBalllay4
struct M_muvurBalllay4_t3808086234;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"

// System.Void GarbageiOS.M_muvurBalllay4::.ctor()
extern "C"  void M_muvurBalllay4__ctor_m3125928393 (M_muvurBalllay4_t3808086234 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_muvurBalllay4::M_nenowFainellou0(System.String[],System.Int32)
extern "C"  void M_muvurBalllay4_M_nenowFainellou0_m1749567440 (M_muvurBalllay4_t3808086234 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_muvurBalllay4::M_rowcha1(System.String[],System.Int32)
extern "C"  void M_muvurBalllay4_M_rowcha1_m1442975739 (M_muvurBalllay4_t3808086234 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
