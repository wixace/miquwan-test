﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<CreateDefaultConstructor>c__AnonStorey142`1<System.Object>
struct U3CCreateDefaultConstructorU3Ec__AnonStorey142_1_t983517794;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<CreateDefaultConstructor>c__AnonStorey142`1<System.Object>::.ctor()
extern "C"  void U3CCreateDefaultConstructorU3Ec__AnonStorey142_1__ctor_m960944953_gshared (U3CCreateDefaultConstructorU3Ec__AnonStorey142_1_t983517794 * __this, const MethodInfo* method);
#define U3CCreateDefaultConstructorU3Ec__AnonStorey142_1__ctor_m960944953(__this, method) ((  void (*) (U3CCreateDefaultConstructorU3Ec__AnonStorey142_1_t983517794 *, const MethodInfo*))U3CCreateDefaultConstructorU3Ec__AnonStorey142_1__ctor_m960944953_gshared)(__this, method)
// T Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<CreateDefaultConstructor>c__AnonStorey142`1<System.Object>::<>m__3A8()
extern "C"  Il2CppObject * U3CCreateDefaultConstructorU3Ec__AnonStorey142_1_U3CU3Em__3A8_m2535618409_gshared (U3CCreateDefaultConstructorU3Ec__AnonStorey142_1_t983517794 * __this, const MethodInfo* method);
#define U3CCreateDefaultConstructorU3Ec__AnonStorey142_1_U3CU3Em__3A8_m2535618409(__this, method) ((  Il2CppObject * (*) (U3CCreateDefaultConstructorU3Ec__AnonStorey142_1_t983517794 *, const MethodInfo*))U3CCreateDefaultConstructorU3Ec__AnonStorey142_1_U3CU3Em__3A8_m2535618409_gshared)(__this, method)
// T Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<CreateDefaultConstructor>c__AnonStorey142`1<System.Object>::<>m__3A9()
extern "C"  Il2CppObject * U3CCreateDefaultConstructorU3Ec__AnonStorey142_1_U3CU3Em__3A9_m2535619370_gshared (U3CCreateDefaultConstructorU3Ec__AnonStorey142_1_t983517794 * __this, const MethodInfo* method);
#define U3CCreateDefaultConstructorU3Ec__AnonStorey142_1_U3CU3Em__3A9_m2535619370(__this, method) ((  Il2CppObject * (*) (U3CCreateDefaultConstructorU3Ec__AnonStorey142_1_t983517794 *, const MethodInfo*))U3CCreateDefaultConstructorU3Ec__AnonStorey142_1_U3CU3Em__3A9_m2535619370_gshared)(__this, method)
