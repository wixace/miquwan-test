﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.AudioChorusFilter
struct AudioChorusFilter_t1588581290;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.AudioChorusFilter::.ctor()
extern "C"  void AudioChorusFilter__ctor_m2670605733 (AudioChorusFilter_t1588581290 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.AudioChorusFilter::get_dryMix()
extern "C"  float AudioChorusFilter_get_dryMix_m3552635149 (AudioChorusFilter_t1588581290 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioChorusFilter::set_dryMix(System.Single)
extern "C"  void AudioChorusFilter_set_dryMix_m3945740990 (AudioChorusFilter_t1588581290 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.AudioChorusFilter::get_wetMix1()
extern "C"  float AudioChorusFilter_get_wetMix1_m1451370465 (AudioChorusFilter_t1588581290 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioChorusFilter::set_wetMix1(System.Single)
extern "C"  void AudioChorusFilter_set_wetMix1_m669502058 (AudioChorusFilter_t1588581290 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.AudioChorusFilter::get_wetMix2()
extern "C"  float AudioChorusFilter_get_wetMix2_m1451371426 (AudioChorusFilter_t1588581290 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioChorusFilter::set_wetMix2(System.Single)
extern "C"  void AudioChorusFilter_set_wetMix2_m158967881 (AudioChorusFilter_t1588581290 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.AudioChorusFilter::get_wetMix3()
extern "C"  float AudioChorusFilter_get_wetMix3_m1451372387 (AudioChorusFilter_t1588581290 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioChorusFilter::set_wetMix3(System.Single)
extern "C"  void AudioChorusFilter_set_wetMix3_m3943401000 (AudioChorusFilter_t1588581290 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.AudioChorusFilter::get_delay()
extern "C"  float AudioChorusFilter_get_delay_m3471803081 (AudioChorusFilter_t1588581290 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioChorusFilter::set_delay(System.Single)
extern "C"  void AudioChorusFilter_set_delay_m1897277570 (AudioChorusFilter_t1588581290 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.AudioChorusFilter::get_rate()
extern "C"  float AudioChorusFilter_get_rate_m2171915324 (AudioChorusFilter_t1588581290 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioChorusFilter::set_rate(System.Single)
extern "C"  void AudioChorusFilter_set_rate_m2632547311 (AudioChorusFilter_t1588581290 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.AudioChorusFilter::get_depth()
extern "C"  float AudioChorusFilter_get_depth_m3476046857 (AudioChorusFilter_t1588581290 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioChorusFilter::set_depth(System.Single)
extern "C"  void AudioChorusFilter_set_depth_m2236182338 (AudioChorusFilter_t1588581290 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
