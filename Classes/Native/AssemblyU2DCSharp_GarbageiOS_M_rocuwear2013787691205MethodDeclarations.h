﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_rocuwear201
struct M_rocuwear201_t3787691205;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_rocuwear2013787691205.h"

// System.Void GarbageiOS.M_rocuwear201::.ctor()
extern "C"  void M_rocuwear201__ctor_m4065424446 (M_rocuwear201_t3787691205 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_rocuwear201::M_dorgoocayHelrecha0(System.String[],System.Int32)
extern "C"  void M_rocuwear201_M_dorgoocayHelrecha0_m1855639978 (M_rocuwear201_t3787691205 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_rocuwear201::M_rijemhowWarhay1(System.String[],System.Int32)
extern "C"  void M_rocuwear201_M_rijemhowWarhay1_m1105777669 (M_rocuwear201_t3787691205 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_rocuwear201::M_heetreeFirnirdaw2(System.String[],System.Int32)
extern "C"  void M_rocuwear201_M_heetreeFirnirdaw2_m2517436359 (M_rocuwear201_t3787691205 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_rocuwear201::M_rearche3(System.String[],System.Int32)
extern "C"  void M_rocuwear201_M_rearche3_m2945315014 (M_rocuwear201_t3787691205 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_rocuwear201::M_lecawhoo4(System.String[],System.Int32)
extern "C"  void M_rocuwear201_M_lecawhoo4_m4112490541 (M_rocuwear201_t3787691205 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_rocuwear201::M_keqolem5(System.String[],System.Int32)
extern "C"  void M_rocuwear201_M_keqolem5_m765407144 (M_rocuwear201_t3787691205 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_rocuwear201::ilo_M_dorgoocayHelrecha01(GarbageiOS.M_rocuwear201,System.String[],System.Int32)
extern "C"  void M_rocuwear201_ilo_M_dorgoocayHelrecha01_m1246194157 (Il2CppObject * __this /* static, unused */, M_rocuwear201_t3787691205 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
