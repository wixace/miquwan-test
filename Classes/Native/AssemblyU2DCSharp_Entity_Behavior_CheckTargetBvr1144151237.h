﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<CombatEntity>
struct List_1_t2052323047;
// CombatEntity
struct CombatEntity_t684137495;

#include "AssemblyU2DCSharp_Entity_Behavior_IBehavior770859129.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entity.Behavior.CheckTargetBvr
struct  CheckTargetBvr_t1144151237  : public IBehavior_t770859129
{
public:
	// System.Collections.Generic.List`1<CombatEntity> Entity.Behavior.CheckTargetBvr::targetMonsters
	List_1_t2052323047 * ___targetMonsters_2;
	// CombatEntity Entity.Behavior.CheckTargetBvr::targetMonster
	CombatEntity_t684137495 * ___targetMonster_3;

public:
	inline static int32_t get_offset_of_targetMonsters_2() { return static_cast<int32_t>(offsetof(CheckTargetBvr_t1144151237, ___targetMonsters_2)); }
	inline List_1_t2052323047 * get_targetMonsters_2() const { return ___targetMonsters_2; }
	inline List_1_t2052323047 ** get_address_of_targetMonsters_2() { return &___targetMonsters_2; }
	inline void set_targetMonsters_2(List_1_t2052323047 * value)
	{
		___targetMonsters_2 = value;
		Il2CppCodeGenWriteBarrier(&___targetMonsters_2, value);
	}

	inline static int32_t get_offset_of_targetMonster_3() { return static_cast<int32_t>(offsetof(CheckTargetBvr_t1144151237, ___targetMonster_3)); }
	inline CombatEntity_t684137495 * get_targetMonster_3() const { return ___targetMonster_3; }
	inline CombatEntity_t684137495 ** get_address_of_targetMonster_3() { return &___targetMonster_3; }
	inline void set_targetMonster_3(CombatEntity_t684137495 * value)
	{
		___targetMonster_3 = value;
		Il2CppCodeGenWriteBarrier(&___targetMonster_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
