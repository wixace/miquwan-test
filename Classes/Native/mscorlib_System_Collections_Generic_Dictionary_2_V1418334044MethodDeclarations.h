﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V3962901666MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Pathfinding.Int2,Pathfinding.TriangleMeshNode>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define ValueCollection__ctor_m1214544682(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t1418334044 *, Dictionary_2_t2717728331 *, const MethodInfo*))ValueCollection__ctor_m1322936629_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Pathfinding.Int2,Pathfinding.TriangleMeshNode>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1709749192(__this, ___item0, method) ((  void (*) (ValueCollection_t1418334044 *, TriangleMeshNode_t1626248749 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m189225053_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Pathfinding.Int2,Pathfinding.TriangleMeshNode>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m505398417(__this, method) ((  void (*) (ValueCollection_t1418334044 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m3762944934_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<Pathfinding.Int2,Pathfinding.TriangleMeshNode>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3077710302(__this, ___item0, method) ((  bool (*) (ValueCollection_t1418334044 *, TriangleMeshNode_t1626248749 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3159435437_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<Pathfinding.Int2,Pathfinding.TriangleMeshNode>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m965089795(__this, ___item0, method) ((  bool (*) (ValueCollection_t1418334044 *, TriangleMeshNode_t1626248749 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m214337682_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<Pathfinding.Int2,Pathfinding.TriangleMeshNode>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1874026719(__this, method) ((  Il2CppObject* (*) (ValueCollection_t1418334044 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m2368026598_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Pathfinding.Int2,Pathfinding.TriangleMeshNode>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m4261061717(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t1418334044 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m3972973866_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<Pathfinding.Int2,Pathfinding.TriangleMeshNode>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m496617488(__this, method) ((  Il2CppObject * (*) (ValueCollection_t1418334044 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m3407700665_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<Pathfinding.Int2,Pathfinding.TriangleMeshNode>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1352886161(__this, method) ((  bool (*) (ValueCollection_t1418334044 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1434611296_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<Pathfinding.Int2,Pathfinding.TriangleMeshNode>::System.Collections.ICollection.get_IsSynchronized()
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m937672433(__this, method) ((  bool (*) (ValueCollection_t1418334044 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m4000531520_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<Pathfinding.Int2,Pathfinding.TriangleMeshNode>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m925628893(__this, method) ((  Il2CppObject * (*) (ValueCollection_t1418334044 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m1893444914_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Pathfinding.Int2,Pathfinding.TriangleMeshNode>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m2158904561(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t1418334044 *, TriangleMeshNodeU5BU5D_t2064970368*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m637061180_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<Pathfinding.Int2,Pathfinding.TriangleMeshNode>::GetEnumerator()
#define ValueCollection_GetEnumerator_m3371616852(__this, method) ((  Enumerator_t649561739  (*) (ValueCollection_t1418334044 *, const MethodInfo*))ValueCollection_GetEnumerator_m2862149669_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<Pathfinding.Int2,Pathfinding.TriangleMeshNode>::get_Count()
#define ValueCollection_get_Count_m3075570359(__this, method) ((  int32_t (*) (ValueCollection_t1418334044 *, const MethodInfo*))ValueCollection_get_Count_m3583443578_gshared)(__this, method)
