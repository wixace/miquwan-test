﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_rebecasBejooxal138
struct M_rebecasBejooxal138_t1831173035;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"

// System.Void GarbageiOS.M_rebecasBejooxal138::.ctor()
extern "C"  void M_rebecasBejooxal138__ctor_m2387171560 (M_rebecasBejooxal138_t1831173035 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_rebecasBejooxal138::M_howta0(System.String[],System.Int32)
extern "C"  void M_rebecasBejooxal138_M_howta0_m2493560184 (M_rebecasBejooxal138_t1831173035 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_rebecasBejooxal138::M_herhemhel1(System.String[],System.Int32)
extern "C"  void M_rebecasBejooxal138_M_herhemhel1_m3743573442 (M_rebecasBejooxal138_t1831173035 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_rebecasBejooxal138::M_zeser2(System.String[],System.Int32)
extern "C"  void M_rebecasBejooxal138_M_zeser2_m2642274402 (M_rebecasBejooxal138_t1831173035 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
