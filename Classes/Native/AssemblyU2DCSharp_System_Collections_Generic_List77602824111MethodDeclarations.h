﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System_Collections_Generic_List77Generated/<ListA1_FindLastIndex__Int32__Int32__PredicateT1_T>c__AnonStorey8C
struct U3CListA1_FindLastIndex__Int32__Int32__PredicateT1_TU3Ec__AnonStorey8C_t602824111;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void System_Collections_Generic_List77Generated/<ListA1_FindLastIndex__Int32__Int32__PredicateT1_T>c__AnonStorey8C::.ctor()
extern "C"  void U3CListA1_FindLastIndex__Int32__Int32__PredicateT1_TU3Ec__AnonStorey8C__ctor_m3636467868 (U3CListA1_FindLastIndex__Int32__Int32__PredicateT1_TU3Ec__AnonStorey8C_t602824111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System_Collections_Generic_List77Generated/<ListA1_FindLastIndex__Int32__Int32__PredicateT1_T>c__AnonStorey8C::<>m__B1()
extern "C"  Il2CppObject * U3CListA1_FindLastIndex__Int32__Int32__PredicateT1_TU3Ec__AnonStorey8C_U3CU3Em__B1_m793187977 (U3CListA1_FindLastIndex__Int32__Int32__PredicateT1_TU3Ec__AnonStorey8C_t602824111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
