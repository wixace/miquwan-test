﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MScrollViewGenerated/<MScrollView_Register_GetDelegate_member4_arg1>c__AnonStorey6D
struct U3CMScrollView_Register_GetDelegate_member4_arg1U3Ec__AnonStorey6D_t2331258234;

#include "codegen/il2cpp-codegen.h"

// System.Void MScrollViewGenerated/<MScrollView_Register_GetDelegate_member4_arg1>c__AnonStorey6D::.ctor()
extern "C"  void U3CMScrollView_Register_GetDelegate_member4_arg1U3Ec__AnonStorey6D__ctor_m297873201 (U3CMScrollView_Register_GetDelegate_member4_arg1U3Ec__AnonStorey6D_t2331258234 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MScrollViewGenerated/<MScrollView_Register_GetDelegate_member4_arg1>c__AnonStorey6D::<>m__74()
extern "C"  bool U3CMScrollView_Register_GetDelegate_member4_arg1U3Ec__AnonStorey6D_U3CU3Em__74_m2295951627 (U3CMScrollView_Register_GetDelegate_member4_arg1U3Ec__AnonStorey6D_t2331258234 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
