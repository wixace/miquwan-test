﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1203005159.h"
#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DCSharp_Pathfinding_PathThreadInfo2420662483.h"

// System.Void System.Array/InternalEnumerator`1<Pathfinding.PathThreadInfo>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2947620312_gshared (InternalEnumerator_1_t1203005159 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m2947620312(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t1203005159 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m2947620312_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<Pathfinding.PathThreadInfo>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m450025672_gshared (InternalEnumerator_1_t1203005159 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m450025672(__this, method) ((  void (*) (InternalEnumerator_1_t1203005159 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m450025672_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<Pathfinding.PathThreadInfo>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m826852660_gshared (InternalEnumerator_1_t1203005159 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m826852660(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t1203005159 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m826852660_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Pathfinding.PathThreadInfo>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3152647599_gshared (InternalEnumerator_1_t1203005159 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m3152647599(__this, method) ((  void (*) (InternalEnumerator_1_t1203005159 *, const MethodInfo*))InternalEnumerator_1_Dispose_m3152647599_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Pathfinding.PathThreadInfo>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2044505396_gshared (InternalEnumerator_1_t1203005159 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m2044505396(__this, method) ((  bool (*) (InternalEnumerator_1_t1203005159 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m2044505396_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Pathfinding.PathThreadInfo>::get_Current()
extern "C"  PathThreadInfo_t2420662483  InternalEnumerator_1_get_Current_m3131144607_gshared (InternalEnumerator_1_t1203005159 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m3131144607(__this, method) ((  PathThreadInfo_t2420662483  (*) (InternalEnumerator_1_t1203005159 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3131144607_gshared)(__this, method)
