﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_haltiKose165
struct M_haltiKose165_t1903042594;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_haltiKose1651903042594.h"

// System.Void GarbageiOS.M_haltiKose165::.ctor()
extern "C"  void M_haltiKose165__ctor_m2620659793 (M_haltiKose165_t1903042594 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_haltiKose165::M_whemnalRejowzou0(System.String[],System.Int32)
extern "C"  void M_haltiKose165_M_whemnalRejowzou0_m3861619723 (M_haltiKose165_t1903042594 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_haltiKose165::M_nepemneeBoobar1(System.String[],System.Int32)
extern "C"  void M_haltiKose165_M_nepemneeBoobar1_m2441064487 (M_haltiKose165_t1903042594 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_haltiKose165::M_hihaWhayfem2(System.String[],System.Int32)
extern "C"  void M_haltiKose165_M_hihaWhayfem2_m2358005347 (M_haltiKose165_t1903042594 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_haltiKose165::M_sawrojur3(System.String[],System.Int32)
extern "C"  void M_haltiKose165_M_sawrojur3_m413859270 (M_haltiKose165_t1903042594 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_haltiKose165::M_rorwhowKabe4(System.String[],System.Int32)
extern "C"  void M_haltiKose165_M_rorwhowKabe4_m2403018601 (M_haltiKose165_t1903042594 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_haltiKose165::M_hicasviChahalmo5(System.String[],System.Int32)
extern "C"  void M_haltiKose165_M_hicasviChahalmo5_m2454384945 (M_haltiKose165_t1903042594 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_haltiKose165::M_bistere6(System.String[],System.Int32)
extern "C"  void M_haltiKose165_M_bistere6_m65769058 (M_haltiKose165_t1903042594 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_haltiKose165::ilo_M_nepemneeBoobar11(GarbageiOS.M_haltiKose165,System.String[],System.Int32)
extern "C"  void M_haltiKose165_ilo_M_nepemneeBoobar11_m2078656699 (Il2CppObject * __this /* static, unused */, M_haltiKose165_t1903042594 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_haltiKose165::ilo_M_hihaWhayfem22(GarbageiOS.M_haltiKose165,System.String[],System.Int32)
extern "C"  void M_haltiKose165_ilo_M_hihaWhayfem22_m98002938 (Il2CppObject * __this /* static, unused */, M_haltiKose165_t1903042594 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_haltiKose165::ilo_M_rorwhowKabe43(GarbageiOS.M_haltiKose165,System.String[],System.Int32)
extern "C"  void M_haltiKose165_ilo_M_rorwhowKabe43_m3223528501 (Il2CppObject * __this /* static, unused */, M_haltiKose165_t1903042594 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
