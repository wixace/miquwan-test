﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"

// System.Int32 System.GC::get_MaxGeneration()
extern "C"  int32_t GC_get_MaxGeneration_m589573414 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.GC::InternalCollect(System.Int32)
extern "C"  void GC_InternalCollect_m3176899477 (Il2CppObject * __this /* static, unused */, int32_t ___generation0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.GC::Collect()
extern "C"  void GC_Collect_m1459080321 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.GC::GetTotalMemory(System.Boolean)
extern "C"  int64_t GC_GetTotalMemory_m1271477460 (Il2CppObject * __this /* static, unused */, bool ___forceFullCollection0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.GC::SuppressFinalize(System.Object)
extern "C"  void GC_SuppressFinalize_m1160635446 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.GC::CollectionCount(System.Int32)
extern "C"  int32_t GC_CollectionCount_m2814002709 (Il2CppObject * __this /* static, unused */, int32_t ___generation0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
