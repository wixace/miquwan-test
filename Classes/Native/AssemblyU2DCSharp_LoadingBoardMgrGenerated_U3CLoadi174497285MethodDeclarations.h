﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LoadingBoardMgrGenerated/<LoadingBoardMgr_OpenDialog_GetDelegate_member5_arg0>c__AnonStorey6B
struct U3CLoadingBoardMgr_OpenDialog_GetDelegate_member5_arg0U3Ec__AnonStorey6B_t174497285;

#include "codegen/il2cpp-codegen.h"

// System.Void LoadingBoardMgrGenerated/<LoadingBoardMgr_OpenDialog_GetDelegate_member5_arg0>c__AnonStorey6B::.ctor()
extern "C"  void U3CLoadingBoardMgr_OpenDialog_GetDelegate_member5_arg0U3Ec__AnonStorey6B__ctor_m2698684678 (U3CLoadingBoardMgr_OpenDialog_GetDelegate_member5_arg0U3Ec__AnonStorey6B_t174497285 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadingBoardMgrGenerated/<LoadingBoardMgr_OpenDialog_GetDelegate_member5_arg0>c__AnonStorey6B::<>m__6C()
extern "C"  void U3CLoadingBoardMgr_OpenDialog_GetDelegate_member5_arg0U3Ec__AnonStorey6B_U3CU3Em__6C_m1583935964 (U3CLoadingBoardMgr_OpenDialog_GetDelegate_member5_arg0U3Ec__AnonStorey6B_t174497285 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
