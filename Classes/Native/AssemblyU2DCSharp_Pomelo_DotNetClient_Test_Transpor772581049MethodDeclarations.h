﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pomelo.DotNetClient.Test.TransportTest
struct TransportTest_t772581049;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// System.Collections.Generic.List`1<System.Byte[]>
struct List_1_t1333978725;
// Pomelo.DotNetClient.Transporter
struct Transporter_t1342168412;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pomelo_DotNetClient_Transporter1342168412.h"

// System.Void Pomelo.DotNetClient.Test.TransportTest::.ctor()
extern "C"  void TransportTest__ctor_m1738991457 (TransportTest_t772581049 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.DotNetClient.Test.TransportTest::.cctor()
extern "C"  void TransportTest__cctor_m1887031404 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Pomelo.DotNetClient.Test.TransportTest::genBuffer(System.Int32)
extern "C"  ByteU5BU5D_t4260760469* TransportTest_genBuffer_m264848374 (Il2CppObject * __this /* static, unused */, int32_t ___count0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Pomelo.DotNetClient.Test.TransportTest::generateBuffers(System.Int32,System.Collections.Generic.List`1<System.Byte[]>&)
extern "C"  ByteU5BU5D_t4260760469* TransportTest_generateBuffers_m690470429 (Il2CppObject * __this /* static, unused */, int32_t ___num0, List_1_t1333978725 ** ___list1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.DotNetClient.Test.TransportTest::Run()
extern "C"  void TransportTest_Run_m683628490 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.DotNetClient.Test.TransportTest::process(System.Byte[])
extern "C"  void TransportTest_process_m1567333051 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___bytes0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.DotNetClient.Test.TransportTest::protocolProcess(System.Byte[])
extern "C"  void TransportTest_protocolProcess_m983667059 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___bytes0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pomelo.DotNetClient.Test.TransportTest::check(System.Collections.Generic.List`1<System.Byte[]>)
extern "C"  bool TransportTest_check_m781127872 (Il2CppObject * __this /* static, unused */, List_1_t1333978725 * ___list0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Pomelo.DotNetClient.Test.TransportTest::ilo_genBuffer1(System.Int32)
extern "C"  ByteU5BU5D_t4260760469* TransportTest_ilo_genBuffer1_m1050363858 (Il2CppObject * __this /* static, unused */, int32_t ___count0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.DotNetClient.Test.TransportTest::ilo_processBytes2(Pomelo.DotNetClient.Transporter,System.Byte[],System.Int32,System.Int32)
extern "C"  void TransportTest_ilo_processBytes2_m2707283416 (Il2CppObject * __this /* static, unused */, Transporter_t1342168412 * ____this0, ByteU5BU5D_t4260760469* ___bytes1, int32_t ___offset2, int32_t ___limit3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
