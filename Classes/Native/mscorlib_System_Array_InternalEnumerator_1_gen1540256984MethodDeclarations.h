﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1540256984.h"
#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DCSharp_Pathfinding_RVO_Sampled_Agent_VO2757914308.h"

// System.Void System.Array/InternalEnumerator`1<Pathfinding.RVO.Sampled.Agent/VO>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m798484693_gshared (InternalEnumerator_1_t1540256984 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m798484693(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t1540256984 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m798484693_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<Pathfinding.RVO.Sampled.Agent/VO>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1933879979_gshared (InternalEnumerator_1_t1540256984 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1933879979(__this, method) ((  void (*) (InternalEnumerator_1_t1540256984 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1933879979_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<Pathfinding.RVO.Sampled.Agent/VO>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1453171863_gshared (InternalEnumerator_1_t1540256984 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1453171863(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t1540256984 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1453171863_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Pathfinding.RVO.Sampled.Agent/VO>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m157649132_gshared (InternalEnumerator_1_t1540256984 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m157649132(__this, method) ((  void (*) (InternalEnumerator_1_t1540256984 *, const MethodInfo*))InternalEnumerator_1_Dispose_m157649132_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Pathfinding.RVO.Sampled.Agent/VO>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2487171863_gshared (InternalEnumerator_1_t1540256984 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m2487171863(__this, method) ((  bool (*) (InternalEnumerator_1_t1540256984 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m2487171863_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Pathfinding.RVO.Sampled.Agent/VO>::get_Current()
extern "C"  VO_t2757914308  InternalEnumerator_1_get_Current_m2258180892_gshared (InternalEnumerator_1_t1540256984 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m2258180892(__this, method) ((  VO_t2757914308  (*) (InternalEnumerator_1_t1540256984 *, const MethodInfo*))InternalEnumerator_1_get_Current_m2258180892_gshared)(__this, method)
