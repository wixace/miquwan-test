﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._7890be7d69530365880453b6c981ff8b
struct _7890be7d69530365880453b6c981ff8b_t1300758547;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._7890be7d69530365880453b6c981ff8b::.ctor()
extern "C"  void _7890be7d69530365880453b6c981ff8b__ctor_m1219398522 (_7890be7d69530365880453b6c981ff8b_t1300758547 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._7890be7d69530365880453b6c981ff8b::_7890be7d69530365880453b6c981ff8bm2(System.Int32)
extern "C"  int32_t _7890be7d69530365880453b6c981ff8b__7890be7d69530365880453b6c981ff8bm2_m3744334009 (_7890be7d69530365880453b6c981ff8b_t1300758547 * __this, int32_t ____7890be7d69530365880453b6c981ff8ba0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._7890be7d69530365880453b6c981ff8b::_7890be7d69530365880453b6c981ff8bm(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _7890be7d69530365880453b6c981ff8b__7890be7d69530365880453b6c981ff8bm_m2325473821 (_7890be7d69530365880453b6c981ff8b_t1300758547 * __this, int32_t ____7890be7d69530365880453b6c981ff8ba0, int32_t ____7890be7d69530365880453b6c981ff8b401, int32_t ____7890be7d69530365880453b6c981ff8bc2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
