﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DamageResult
struct DamageResult_t3811312780;
struct DamageResult_t3811312780_marshaled_pinvoke;
struct DamageResult_t3811312780_marshaled_com;

#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct DamageResult_t3811312780;
struct DamageResult_t3811312780_marshaled_pinvoke;

extern "C" void DamageResult_t3811312780_marshal_pinvoke(const DamageResult_t3811312780& unmarshaled, DamageResult_t3811312780_marshaled_pinvoke& marshaled);
extern "C" void DamageResult_t3811312780_marshal_pinvoke_back(const DamageResult_t3811312780_marshaled_pinvoke& marshaled, DamageResult_t3811312780& unmarshaled);
extern "C" void DamageResult_t3811312780_marshal_pinvoke_cleanup(DamageResult_t3811312780_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct DamageResult_t3811312780;
struct DamageResult_t3811312780_marshaled_com;

extern "C" void DamageResult_t3811312780_marshal_com(const DamageResult_t3811312780& unmarshaled, DamageResult_t3811312780_marshaled_com& marshaled);
extern "C" void DamageResult_t3811312780_marshal_com_back(const DamageResult_t3811312780_marshaled_com& marshaled, DamageResult_t3811312780& unmarshaled);
extern "C" void DamageResult_t3811312780_marshal_com_cleanup(DamageResult_t3811312780_marshaled_com& marshaled);
