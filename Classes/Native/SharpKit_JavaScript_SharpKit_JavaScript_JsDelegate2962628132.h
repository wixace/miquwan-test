﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Attribute2523058482.h"
#include "mscorlib_System_Nullable_1_gen560925241.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SharpKit.JavaScript.JsDelegateAttribute
struct  JsDelegateAttribute_t2962628132  : public Attribute_t2523058482
{
public:
	// System.Nullable`1<System.Boolean> SharpKit.JavaScript.JsDelegateAttribute::_NativeDelegates
	Nullable_1_t560925241  ____NativeDelegates_0;

public:
	inline static int32_t get_offset_of__NativeDelegates_0() { return static_cast<int32_t>(offsetof(JsDelegateAttribute_t2962628132, ____NativeDelegates_0)); }
	inline Nullable_1_t560925241  get__NativeDelegates_0() const { return ____NativeDelegates_0; }
	inline Nullable_1_t560925241 * get_address_of__NativeDelegates_0() { return &____NativeDelegates_0; }
	inline void set__NativeDelegates_0(Nullable_1_t560925241  value)
	{
		____NativeDelegates_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
