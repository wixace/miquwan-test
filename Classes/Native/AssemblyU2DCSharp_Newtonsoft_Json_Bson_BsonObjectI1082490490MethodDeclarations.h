﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Bson.BsonObjectId
struct BsonObjectId_t1082490490;
// System.Byte[]
struct ByteU5BU5D_t4260760469;

#include "codegen/il2cpp-codegen.h"

// System.Void Newtonsoft.Json.Bson.BsonObjectId::.ctor(System.Byte[])
extern "C"  void BsonObjectId__ctor_m2060971313 (BsonObjectId_t1082490490 * __this, ByteU5BU5D_t4260760469* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Newtonsoft.Json.Bson.BsonObjectId::get_Value()
extern "C"  ByteU5BU5D_t4260760469* BsonObjectId_get_Value_m2925913832 (BsonObjectId_t1082490490 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Bson.BsonObjectId::set_Value(System.Byte[])
extern "C"  void BsonObjectId_set_Value_m1736030591 (BsonObjectId_t1082490490 * __this, ByteU5BU5D_t4260760469* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
