﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_ConstantForceGenerated
struct UnityEngine_ConstantForceGenerated_t713118480;
// JSVCall
struct JSVCall_t3708497963;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void UnityEngine_ConstantForceGenerated::.ctor()
extern "C"  void UnityEngine_ConstantForceGenerated__ctor_m1560670091 (UnityEngine_ConstantForceGenerated_t713118480 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ConstantForceGenerated::ConstantForce_ConstantForce1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ConstantForceGenerated_ConstantForce_ConstantForce1_m795446207 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ConstantForceGenerated::ConstantForce_force(JSVCall)
extern "C"  void UnityEngine_ConstantForceGenerated_ConstantForce_force_m2256040427 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ConstantForceGenerated::ConstantForce_relativeForce(JSVCall)
extern "C"  void UnityEngine_ConstantForceGenerated_ConstantForce_relativeForce_m1696625623 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ConstantForceGenerated::ConstantForce_torque(JSVCall)
extern "C"  void UnityEngine_ConstantForceGenerated_ConstantForce_torque_m1243880908 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ConstantForceGenerated::ConstantForce_relativeTorque(JSVCall)
extern "C"  void UnityEngine_ConstantForceGenerated_ConstantForce_relativeTorque_m1081891168 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ConstantForceGenerated::__Register()
extern "C"  void UnityEngine_ConstantForceGenerated___Register_m657099804 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_ConstantForceGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UnityEngine_ConstantForceGenerated_ilo_getObject1_m2974473447 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine_ConstantForceGenerated::ilo_getVector3S2(System.Int32)
extern "C"  Vector3_t4282066566  UnityEngine_ConstantForceGenerated_ilo_getVector3S2_m1694624400 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
