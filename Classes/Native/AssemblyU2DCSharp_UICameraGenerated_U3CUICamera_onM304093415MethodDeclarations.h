﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UICameraGenerated/<UICamera_onMouseMove_GetDelegate_member56_arg0>c__AnonStoreyB1
struct U3CUICamera_onMouseMove_GetDelegate_member56_arg0U3Ec__AnonStoreyB1_t304093415;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"

// System.Void UICameraGenerated/<UICamera_onMouseMove_GetDelegate_member56_arg0>c__AnonStoreyB1::.ctor()
extern "C"  void U3CUICamera_onMouseMove_GetDelegate_member56_arg0U3Ec__AnonStoreyB1__ctor_m3573049444 (U3CUICamera_onMouseMove_GetDelegate_member56_arg0U3Ec__AnonStoreyB1_t304093415 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICameraGenerated/<UICamera_onMouseMove_GetDelegate_member56_arg0>c__AnonStoreyB1::<>m__126(UnityEngine.Vector2)
extern "C"  void U3CUICamera_onMouseMove_GetDelegate_member56_arg0U3Ec__AnonStoreyB1_U3CU3Em__126_m478870832 (U3CUICamera_onMouseMove_GetDelegate_member56_arg0U3Ec__AnonStoreyB1_t304093415 * __this, Vector2_t4282066565  ___delta0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
