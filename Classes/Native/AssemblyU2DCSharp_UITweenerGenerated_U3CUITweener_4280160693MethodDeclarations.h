﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UITweenerGenerated/<UITweener_SetOnFinished_GetDelegate_member10_arg0>c__AnonStoreyD3
struct U3CUITweener_SetOnFinished_GetDelegate_member10_arg0U3Ec__AnonStoreyD3_t4280160693;

#include "codegen/il2cpp-codegen.h"

// System.Void UITweenerGenerated/<UITweener_SetOnFinished_GetDelegate_member10_arg0>c__AnonStoreyD3::.ctor()
extern "C"  void U3CUITweener_SetOnFinished_GetDelegate_member10_arg0U3Ec__AnonStoreyD3__ctor_m1484637014 (U3CUITweener_SetOnFinished_GetDelegate_member10_arg0U3Ec__AnonStoreyD3_t4280160693 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITweenerGenerated/<UITweener_SetOnFinished_GetDelegate_member10_arg0>c__AnonStoreyD3::<>m__16B()
extern "C"  void U3CUITweener_SetOnFinished_GetDelegate_member10_arg0U3Ec__AnonStoreyD3_U3CU3Em__16B_m2078036128 (U3CUITweener_SetOnFinished_GetDelegate_member10_arg0U3Ec__AnonStoreyD3_t4280160693 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
