﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System_Collections_Generic_List77Generated/<ListA1_ForEach__ActionT1_T>c__AnonStorey92
struct U3CListA1_ForEach__ActionT1_TU3Ec__AnonStorey92_t3474186975;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void System_Collections_Generic_List77Generated/<ListA1_ForEach__ActionT1_T>c__AnonStorey92::.ctor()
extern "C"  void U3CListA1_ForEach__ActionT1_TU3Ec__AnonStorey92__ctor_m2645777564 (U3CListA1_ForEach__ActionT1_TU3Ec__AnonStorey92_t3474186975 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System_Collections_Generic_List77Generated/<ListA1_ForEach__ActionT1_T>c__AnonStorey92::<>m__B7()
extern "C"  Il2CppObject * U3CListA1_ForEach__ActionT1_TU3Ec__AnonStorey92_U3CU3Em__B7_m4243707205 (U3CListA1_ForEach__ActionT1_TU3Ec__AnonStorey92_t3474186975 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
