﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Hero
struct Hero_t2245658;
// HeroMgr
struct HeroMgr_t2475965342;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HeroMgr/<CreatGuideHeroNpc>c__AnonStorey153
struct  U3CCreatGuideHeroNpcU3Ec__AnonStorey153_t3324627133  : public Il2CppObject
{
public:
	// Hero HeroMgr/<CreatGuideHeroNpc>c__AnonStorey153::npc
	Hero_t2245658 * ___npc_0;
	// HeroMgr HeroMgr/<CreatGuideHeroNpc>c__AnonStorey153::<>f__this
	HeroMgr_t2475965342 * ___U3CU3Ef__this_1;

public:
	inline static int32_t get_offset_of_npc_0() { return static_cast<int32_t>(offsetof(U3CCreatGuideHeroNpcU3Ec__AnonStorey153_t3324627133, ___npc_0)); }
	inline Hero_t2245658 * get_npc_0() const { return ___npc_0; }
	inline Hero_t2245658 ** get_address_of_npc_0() { return &___npc_0; }
	inline void set_npc_0(Hero_t2245658 * value)
	{
		___npc_0 = value;
		Il2CppCodeGenWriteBarrier(&___npc_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_1() { return static_cast<int32_t>(offsetof(U3CCreatGuideHeroNpcU3Ec__AnonStorey153_t3324627133, ___U3CU3Ef__this_1)); }
	inline HeroMgr_t2475965342 * get_U3CU3Ef__this_1() const { return ___U3CU3Ef__this_1; }
	inline HeroMgr_t2475965342 ** get_address_of_U3CU3Ef__this_1() { return &___U3CU3Ef__this_1; }
	inline void set_U3CU3Ef__this_1(HeroMgr_t2475965342 * value)
	{
		___U3CU3Ef__this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
