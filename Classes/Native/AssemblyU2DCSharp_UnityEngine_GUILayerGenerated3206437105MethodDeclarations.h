﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_GUILayerGenerated
struct UnityEngine_GUILayerGenerated_t3206437105;
// JSVCall
struct JSVCall_t3708497963;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void UnityEngine_GUILayerGenerated::.ctor()
extern "C"  void UnityEngine_GUILayerGenerated__ctor_m1889777562 (UnityEngine_GUILayerGenerated_t3206437105 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUILayerGenerated::GUILayer_GUILayer1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUILayerGenerated_GUILayer_GUILayer1_m3544918872 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUILayerGenerated::GUILayer_HitTest__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUILayerGenerated_GUILayer_HitTest__Vector3_m1360891152 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUILayerGenerated::__Register()
extern "C"  void UnityEngine_GUILayerGenerated___Register_m813856109 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_GUILayerGenerated::ilo_setObject1(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UnityEngine_GUILayerGenerated_ilo_setObject1_m317864660 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
