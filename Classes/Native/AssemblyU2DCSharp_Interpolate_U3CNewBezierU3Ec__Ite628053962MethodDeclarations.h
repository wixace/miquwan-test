﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Interpolate/<NewBezier>c__Iterator24`1<UnityEngine.Vector3>
struct U3CNewBezierU3Ec__Iterator24_1_t628053962;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Vector3>
struct IEnumerator_1_t1898964319;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void Interpolate/<NewBezier>c__Iterator24`1<UnityEngine.Vector3>::.ctor()
extern "C"  void U3CNewBezierU3Ec__Iterator24_1__ctor_m1422335169_gshared (U3CNewBezierU3Ec__Iterator24_1_t628053962 * __this, const MethodInfo* method);
#define U3CNewBezierU3Ec__Iterator24_1__ctor_m1422335169(__this, method) ((  void (*) (U3CNewBezierU3Ec__Iterator24_1_t628053962 *, const MethodInfo*))U3CNewBezierU3Ec__Iterator24_1__ctor_m1422335169_gshared)(__this, method)
// UnityEngine.Vector3 Interpolate/<NewBezier>c__Iterator24`1<UnityEngine.Vector3>::System.Collections.Generic.IEnumerator<UnityEngine.Vector3>.get_Current()
extern "C"  Vector3_t4282066566  U3CNewBezierU3Ec__Iterator24_1_System_Collections_Generic_IEnumeratorU3CUnityEngine_Vector3U3E_get_Current_m2238162878_gshared (U3CNewBezierU3Ec__Iterator24_1_t628053962 * __this, const MethodInfo* method);
#define U3CNewBezierU3Ec__Iterator24_1_System_Collections_Generic_IEnumeratorU3CUnityEngine_Vector3U3E_get_Current_m2238162878(__this, method) ((  Vector3_t4282066566  (*) (U3CNewBezierU3Ec__Iterator24_1_t628053962 *, const MethodInfo*))U3CNewBezierU3Ec__Iterator24_1_System_Collections_Generic_IEnumeratorU3CUnityEngine_Vector3U3E_get_Current_m2238162878_gshared)(__this, method)
// System.Object Interpolate/<NewBezier>c__Iterator24`1<UnityEngine.Vector3>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CNewBezierU3Ec__Iterator24_1_System_Collections_IEnumerator_get_Current_m276976709_gshared (U3CNewBezierU3Ec__Iterator24_1_t628053962 * __this, const MethodInfo* method);
#define U3CNewBezierU3Ec__Iterator24_1_System_Collections_IEnumerator_get_Current_m276976709(__this, method) ((  Il2CppObject * (*) (U3CNewBezierU3Ec__Iterator24_1_t628053962 *, const MethodInfo*))U3CNewBezierU3Ec__Iterator24_1_System_Collections_IEnumerator_get_Current_m276976709_gshared)(__this, method)
// System.Collections.IEnumerator Interpolate/<NewBezier>c__Iterator24`1<UnityEngine.Vector3>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CNewBezierU3Ec__Iterator24_1_System_Collections_IEnumerable_GetEnumerator_m631161728_gshared (U3CNewBezierU3Ec__Iterator24_1_t628053962 * __this, const MethodInfo* method);
#define U3CNewBezierU3Ec__Iterator24_1_System_Collections_IEnumerable_GetEnumerator_m631161728(__this, method) ((  Il2CppObject * (*) (U3CNewBezierU3Ec__Iterator24_1_t628053962 *, const MethodInfo*))U3CNewBezierU3Ec__Iterator24_1_System_Collections_IEnumerable_GetEnumerator_m631161728_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<UnityEngine.Vector3> Interpolate/<NewBezier>c__Iterator24`1<UnityEngine.Vector3>::System.Collections.Generic.IEnumerable<UnityEngine.Vector3>.GetEnumerator()
extern "C"  Il2CppObject* U3CNewBezierU3Ec__Iterator24_1_System_Collections_Generic_IEnumerableU3CUnityEngine_Vector3U3E_GetEnumerator_m2141655659_gshared (U3CNewBezierU3Ec__Iterator24_1_t628053962 * __this, const MethodInfo* method);
#define U3CNewBezierU3Ec__Iterator24_1_System_Collections_Generic_IEnumerableU3CUnityEngine_Vector3U3E_GetEnumerator_m2141655659(__this, method) ((  Il2CppObject* (*) (U3CNewBezierU3Ec__Iterator24_1_t628053962 *, const MethodInfo*))U3CNewBezierU3Ec__Iterator24_1_System_Collections_Generic_IEnumerableU3CUnityEngine_Vector3U3E_GetEnumerator_m2141655659_gshared)(__this, method)
// System.Boolean Interpolate/<NewBezier>c__Iterator24`1<UnityEngine.Vector3>::MoveNext()
extern "C"  bool U3CNewBezierU3Ec__Iterator24_1_MoveNext_m4021900179_gshared (U3CNewBezierU3Ec__Iterator24_1_t628053962 * __this, const MethodInfo* method);
#define U3CNewBezierU3Ec__Iterator24_1_MoveNext_m4021900179(__this, method) ((  bool (*) (U3CNewBezierU3Ec__Iterator24_1_t628053962 *, const MethodInfo*))U3CNewBezierU3Ec__Iterator24_1_MoveNext_m4021900179_gshared)(__this, method)
// System.Void Interpolate/<NewBezier>c__Iterator24`1<UnityEngine.Vector3>::Dispose()
extern "C"  void U3CNewBezierU3Ec__Iterator24_1_Dispose_m962780350_gshared (U3CNewBezierU3Ec__Iterator24_1_t628053962 * __this, const MethodInfo* method);
#define U3CNewBezierU3Ec__Iterator24_1_Dispose_m962780350(__this, method) ((  void (*) (U3CNewBezierU3Ec__Iterator24_1_t628053962 *, const MethodInfo*))U3CNewBezierU3Ec__Iterator24_1_Dispose_m962780350_gshared)(__this, method)
// System.Void Interpolate/<NewBezier>c__Iterator24`1<UnityEngine.Vector3>::Reset()
extern "C"  void U3CNewBezierU3Ec__Iterator24_1_Reset_m3363735406_gshared (U3CNewBezierU3Ec__Iterator24_1_t628053962 * __this, const MethodInfo* method);
#define U3CNewBezierU3Ec__Iterator24_1_Reset_m3363735406(__this, method) ((  void (*) (U3CNewBezierU3Ec__Iterator24_1_t628053962 *, const MethodInfo*))U3CNewBezierU3Ec__Iterator24_1_Reset_m3363735406_gshared)(__this, method)
