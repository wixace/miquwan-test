﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ReplayHeroUnit
struct  ReplayHeroUnit_t605237701  : public Il2CppObject
{
public:
	// System.UInt32 ReplayHeroUnit::id
	uint32_t ___id_0;
	// System.Single ReplayHeroUnit::hp
	float ___hp_1;
	// System.UInt32 ReplayHeroUnit::lv
	uint32_t ___lv_2;
	// System.Single ReplayHeroUnit::movespeed
	float ___movespeed_3;
	// System.Single ReplayHeroUnit::hurt
	float ___hurt_4;
	// System.Int32 ReplayHeroUnit::anger
	int32_t ___anger_5;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(ReplayHeroUnit_t605237701, ___id_0)); }
	inline uint32_t get_id_0() const { return ___id_0; }
	inline uint32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(uint32_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_hp_1() { return static_cast<int32_t>(offsetof(ReplayHeroUnit_t605237701, ___hp_1)); }
	inline float get_hp_1() const { return ___hp_1; }
	inline float* get_address_of_hp_1() { return &___hp_1; }
	inline void set_hp_1(float value)
	{
		___hp_1 = value;
	}

	inline static int32_t get_offset_of_lv_2() { return static_cast<int32_t>(offsetof(ReplayHeroUnit_t605237701, ___lv_2)); }
	inline uint32_t get_lv_2() const { return ___lv_2; }
	inline uint32_t* get_address_of_lv_2() { return &___lv_2; }
	inline void set_lv_2(uint32_t value)
	{
		___lv_2 = value;
	}

	inline static int32_t get_offset_of_movespeed_3() { return static_cast<int32_t>(offsetof(ReplayHeroUnit_t605237701, ___movespeed_3)); }
	inline float get_movespeed_3() const { return ___movespeed_3; }
	inline float* get_address_of_movespeed_3() { return &___movespeed_3; }
	inline void set_movespeed_3(float value)
	{
		___movespeed_3 = value;
	}

	inline static int32_t get_offset_of_hurt_4() { return static_cast<int32_t>(offsetof(ReplayHeroUnit_t605237701, ___hurt_4)); }
	inline float get_hurt_4() const { return ___hurt_4; }
	inline float* get_address_of_hurt_4() { return &___hurt_4; }
	inline void set_hurt_4(float value)
	{
		___hurt_4 = value;
	}

	inline static int32_t get_offset_of_anger_5() { return static_cast<int32_t>(offsetof(ReplayHeroUnit_t605237701, ___anger_5)); }
	inline int32_t get_anger_5() const { return ___anger_5; }
	inline int32_t* get_address_of_anger_5() { return &___anger_5; }
	inline void set_anger_5(int32_t value)
	{
		___anger_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
