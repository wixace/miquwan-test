﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1349036564.h"
#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DCSharp_AstarPath_AstarWorkItem2566693888.h"

// System.Void System.Array/InternalEnumerator`1<AstarPath/AstarWorkItem>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1550221061_gshared (InternalEnumerator_1_t1349036564 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m1550221061(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t1349036564 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m1550221061_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<AstarPath/AstarWorkItem>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2309654139_gshared (InternalEnumerator_1_t1349036564 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2309654139(__this, method) ((  void (*) (InternalEnumerator_1_t1349036564 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2309654139_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<AstarPath/AstarWorkItem>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4121250737_gshared (InternalEnumerator_1_t1349036564 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4121250737(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t1349036564 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4121250737_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<AstarPath/AstarWorkItem>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2334202652_gshared (InternalEnumerator_1_t1349036564 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m2334202652(__this, method) ((  void (*) (InternalEnumerator_1_t1349036564 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2334202652_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<AstarPath/AstarWorkItem>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3471173547_gshared (InternalEnumerator_1_t1349036564 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m3471173547(__this, method) ((  bool (*) (InternalEnumerator_1_t1349036564 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m3471173547_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<AstarPath/AstarWorkItem>::get_Current()
extern "C"  AstarWorkItem_t2566693888  InternalEnumerator_1_get_Current_m227055726_gshared (InternalEnumerator_1_t1349036564 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m227055726(__this, method) ((  AstarWorkItem_t2566693888  (*) (InternalEnumerator_1_t1349036564 *, const MethodInfo*))InternalEnumerator_1_get_Current_m227055726_gshared)(__this, method)
