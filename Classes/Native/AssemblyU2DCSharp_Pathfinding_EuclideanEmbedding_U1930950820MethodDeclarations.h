﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.EuclideanEmbedding/<RecalculateCosts>c__AnonStorey120/<RecalculateCosts>c__AnonStorey11E/<RecalculateCosts>c__AnonStorey11F
struct U3CRecalculateCostsU3Ec__AnonStorey11F_t1930950820;
// Pathfinding.GraphNode
struct GraphNode_t23612370;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphNode23612370.h"

// System.Void Pathfinding.EuclideanEmbedding/<RecalculateCosts>c__AnonStorey120/<RecalculateCosts>c__AnonStorey11E/<RecalculateCosts>c__AnonStorey11F::.ctor()
extern "C"  void U3CRecalculateCostsU3Ec__AnonStorey11F__ctor_m3258855239 (U3CRecalculateCostsU3Ec__AnonStorey11F_t1930950820 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.EuclideanEmbedding/<RecalculateCosts>c__AnonStorey120/<RecalculateCosts>c__AnonStorey11E/<RecalculateCosts>c__AnonStorey11F::<>m__354(Pathfinding.GraphNode)
extern "C"  bool U3CRecalculateCostsU3Ec__AnonStorey11F_U3CU3Em__354_m940244732 (U3CRecalculateCostsU3Ec__AnonStorey11F_t1930950820 * __this, GraphNode_t23612370 * ___node0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
