﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AstarPath
struct AstarPath_t4090270936;
// System.Version
struct Version_t763695022;
// System.Type[]
struct TypeU5BU5D_t3339007067;
// Pathfinding.NavGraph[]
struct NavGraphU5BU5D_t850130684;
// Pathfinding.PathHandler
struct PathHandler_t918952263;
// System.String[]
struct StringU5BU5D_t4054002952;
// Pathfinding.GraphNode
struct GraphNode_t23612370;
// System.String
struct String_t;
// Pathfinding.Path
struct Path_t1974241691;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// Pathfinding.GraphUpdateObject
struct GraphUpdateObject_t430843704;
// System.Object
struct Il2CppObject;
// OnScanStatus
struct OnScanStatus_t2412749870;
// OnVoidDelegate
struct OnVoidDelegate_t2787120856;
// Pathfinding.NNConstraint
struct NNConstraint_t758567699;
// Pathfinding.NavGraph
struct NavGraph_t1254319713;
// Pathfinding.GraphNodeDelegateCancelable
struct GraphNodeDelegateCancelable_t3591372971;
// Pathfinding.ThreadControlQueue
struct ThreadControlQueue_t1865010388;
// System.Collections.IEnumerable
struct IEnumerable_t3464557803;
// Pathfinding.AstarData
struct AstarData_t3283402719;
// Pathfinding.IUpdatableGraph
struct IUpdatableGraph_t4229287971;
// OnScanDelegate
struct OnScanDelegate_t3165885121;
// OnGraphDelegate
struct OnGraphDelegate_t381382964;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// OnPathDelegate
struct OnPathDelegate_t598607977;
// Pathfinding.Util.LockFreeStack
struct LockFreeStack_t746776019;
// Pathfinding.PathNode
struct PathNode_t417131581;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphNode23612370.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_Pathfinding_Path1974241691.h"
#include "AssemblyU2DCSharp_AstarPath_AstarWorkItem2566693888.h"
#include "UnityEngine_UnityEngine_Bounds2711641849.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphUpdateObject430843704.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_ThreadCount3572904197.h"
#include "AssemblyU2DCSharp_OnScanStatus2412749870.h"
#include "AssemblyU2DCSharp_OnVoidDelegate2787120856.h"
#include "AssemblyU2DCSharp_Pathfinding_NNInfo1570625892.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_Pathfinding_NNConstraint758567699.h"
#include "UnityEngine_UnityEngine_Ray3134616544.h"
#include "AssemblyU2DCSharp_AstarPath4090270936.h"
#include "AssemblyU2DCSharp_Pathfinding_NavGraph1254319713.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphNodeDelegateCan3591372971.h"
#include "AssemblyU2DCSharp_PathLog873181375.h"
#include "AssemblyU2DCSharp_Pathfinding_ThreadControlQueue1865010388.h"
#include "AssemblyU2DCSharp_Pathfinding_AstarData3283402719.h"
#include "AssemblyU2DCSharp_OnScanDelegate3165885121.h"
#include "AssemblyU2DCSharp_Pathfinding_PathHandler918952263.h"
#include "AssemblyU2DCSharp_Pathfinding_Progress2476786339.h"
#include "AssemblyU2DCSharp_OnGraphDelegate381382964.h"
#include "AssemblyU2DCSharp_Pathfinding_Int31974045594.h"
#include "AssemblyU2DCSharp_PathState1615290188.h"
#include "AssemblyU2DCSharp_OnPathDelegate598607977.h"
#include "AssemblyU2DCSharp_Pathfinding_Util_LockFreeStack746776019.h"
#include "AssemblyU2DCSharp_Pathfinding_PathNode417131581.h"

// System.Void AstarPath::.ctor()
extern "C"  void AstarPath__ctor_m2720822163 (AstarPath_t4090270936 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AstarPath::.cctor()
extern "C"  void AstarPath__cctor_m2259012218 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Version AstarPath::get_Version()
extern "C"  Version_t763695022 * AstarPath_get_Version_m3939887588 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type[] AstarPath::get_graphTypes()
extern "C"  TypeU5BU5D_t3339007067* AstarPath_get_graphTypes_m2164504589 (AstarPath_t4090270936 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.NavGraph[] AstarPath::get_graphs()
extern "C"  NavGraphU5BU5D_t850130684* AstarPath_get_graphs_m1329136441 (AstarPath_t4090270936 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AstarPath::set_graphs(Pathfinding.NavGraph[])
extern "C"  void AstarPath_set_graphs_m3275201084 (AstarPath_t4090270936 * __this, NavGraphU5BU5D_t850130684* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single AstarPath::get_maxNearestNodeDistanceSqr()
extern "C"  float AstarPath_get_maxNearestNodeDistanceSqr_m3317355767 (AstarPath_t4090270936 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.PathHandler AstarPath::get_debugPathData()
extern "C"  PathHandler_t918952263 * AstarPath_get_debugPathData_m1345630914 (AstarPath_t4090270936 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 AstarPath::get_NumParallelThreads()
extern "C"  int32_t AstarPath_get_NumParallelThreads_m1627516936 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AstarPath::get_IsUsingMultithreading()
extern "C"  bool AstarPath_get_IsUsingMultithreading_m774394069 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AstarPath::get_IsAnyGraphUpdatesQueued()
extern "C"  bool AstarPath_get_IsAnyGraphUpdatesQueued_m3951572909 (AstarPath_t4090270936 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] AstarPath::GetTagNames()
extern "C"  StringU5BU5D_t4054002952* AstarPath_GetTagNames_m2289517110 (AstarPath_t4090270936 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] AstarPath::FindTagNames()
extern "C"  StringU5BU5D_t4054002952* AstarPath_FindTagNames_m3242431767 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt16 AstarPath::GetNextPathID()
extern "C"  uint16_t AstarPath_GetNextPathID_m629842959 (AstarPath_t4090270936 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AstarPath::OnDrawGizmos()
extern "C"  void AstarPath_OnDrawGizmos_m2312307245 (AstarPath_t4090270936 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AstarPath::DrawUnwalkableNode(Pathfinding.GraphNode)
extern "C"  bool AstarPath_DrawUnwalkableNode_m3399530477 (AstarPath_t4090270936 * __this, GraphNode_t23612370 * ___node0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AstarPath::OnGUI()
extern "C"  void AstarPath_OnGUI_m2216220813 (AstarPath_t4090270936 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AstarPath::AstarLog(System.String)
extern "C"  void AstarPath_AstarLog_m729253664 (Il2CppObject * __this /* static, unused */, String_t* ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AstarPath::AstarLogError(System.String)
extern "C"  void AstarPath_AstarLogError_m2369382554 (Il2CppObject * __this /* static, unused */, String_t* ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AstarPath::LogPathResults(Pathfinding.Path)
extern "C"  void AstarPath_LogPathResults_m3109937615 (AstarPath_t4090270936 * __this, Path_t1974241691 * ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AstarPath::Update()
extern "C"  void AstarPath_Update_m173003226 (AstarPath_t4090270936 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AstarPath::PerformBlockingActions(System.Boolean,System.Boolean)
extern "C"  void AstarPath_PerformBlockingActions_m3513456942 (AstarPath_t4090270936 * __this, bool ___force0, bool ___unblockOnComplete1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AstarPath::QueueWorkItemFloodFill()
extern "C"  void AstarPath_QueueWorkItemFloodFill_m2235952253 (AstarPath_t4090270936 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AstarPath::EnsureValidFloodFill()
extern "C"  void AstarPath_EnsureValidFloodFill_m601406580 (AstarPath_t4090270936 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AstarPath::AddWorkItem(AstarPath/AstarWorkItem)
extern "C"  void AstarPath_AddWorkItem_m2504772796 (AstarPath_t4090270936 * __this, AstarWorkItem_t2566693888  ___itm0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 AstarPath::ProcessWorkItems(System.Boolean)
extern "C"  int32_t AstarPath_ProcessWorkItems_m2992716218 (AstarPath_t4090270936 * __this, bool ___force0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AstarPath::QueueGraphUpdates()
extern "C"  void AstarPath_QueueGraphUpdates_m769970366 (AstarPath_t4090270936 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator AstarPath::DelayedGraphUpdate()
extern "C"  Il2CppObject * AstarPath_DelayedGraphUpdate_m2604013502 (AstarPath_t4090270936 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AstarPath::UpdateGraphs(UnityEngine.Bounds,System.Single)
extern "C"  void AstarPath_UpdateGraphs_m1771258328 (AstarPath_t4090270936 * __this, Bounds_t2711641849  ___bounds0, float ___t1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AstarPath::UpdateGraphs(Pathfinding.GraphUpdateObject,System.Single)
extern "C"  void AstarPath_UpdateGraphs_m476444524 (AstarPath_t4090270936 * __this, GraphUpdateObject_t430843704 * ___ob0, float ___t1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator AstarPath::UpdateGraphsInteral(Pathfinding.GraphUpdateObject,System.Single)
extern "C"  Il2CppObject * AstarPath_UpdateGraphsInteral_m537218201 (AstarPath_t4090270936 * __this, GraphUpdateObject_t430843704 * ___ob0, float ___t1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AstarPath::UpdateGraphs(UnityEngine.Bounds)
extern "C"  void AstarPath_UpdateGraphs_m3213331507 (AstarPath_t4090270936 * __this, Bounds_t2711641849  ___bounds0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AstarPath::UpdateGraphs(Pathfinding.GraphUpdateObject)
extern "C"  void AstarPath_UpdateGraphs_m769578183 (AstarPath_t4090270936 * __this, GraphUpdateObject_t430843704 * ___ob0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AstarPath::FlushGraphUpdates()
extern "C"  void AstarPath_FlushGraphUpdates_m3279818097 (AstarPath_t4090270936 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AstarPath::FlushWorkItems(System.Boolean,System.Boolean)
extern "C"  void AstarPath_FlushWorkItems_m160475530 (AstarPath_t4090270936 * __this, bool ___unblockOnComplete0, bool ___block1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AstarPath::QueueGraphUpdatesInternal()
extern "C"  void AstarPath_QueueGraphUpdatesInternal_m3697978203 (AstarPath_t4090270936 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AstarPath::ProcessGraphUpdates(System.Boolean)
extern "C"  bool AstarPath_ProcessGraphUpdates_m3972003271 (AstarPath_t4090270936 * __this, bool ___force0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AstarPath::ProcessGraphUpdatesAsync(System.Object)
extern "C"  void AstarPath_ProcessGraphUpdatesAsync_m1762273330 (AstarPath_t4090270936 * __this, Il2CppObject * ____astar0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AstarPath::FlushThreadSafeCallbacks()
extern "C"  void AstarPath_FlushThreadSafeCallbacks_m4167414468 (AstarPath_t4090270936 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AstarPath::LogProfiler()
extern "C"  void AstarPath_LogProfiler_m1825105086 (AstarPath_t4090270936 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AstarPath::ResetProfiler()
extern "C"  void AstarPath_ResetProfiler_m921395465 (AstarPath_t4090270936 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 AstarPath::CalculateThreadCount(ThreadCount)
extern "C"  int32_t AstarPath_CalculateThreadCount_m2970509611 (Il2CppObject * __this /* static, unused */, int32_t ___count0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AstarPath::Awake()
extern "C"  void AstarPath_Awake_m2958427382 (AstarPath_t4090270936 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AstarPath::VerifyIntegrity()
extern "C"  void AstarPath_VerifyIntegrity_m2261231237 (AstarPath_t4090270936 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AstarPath::SetUpReferences()
extern "C"  void AstarPath_SetUpReferences_m2489625718 (AstarPath_t4090270936 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AstarPath::Initialize()
extern "C"  void AstarPath_Initialize_m1172842209 (AstarPath_t4090270936 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AstarPath::OnDestroy()
extern "C"  void AstarPath_OnDestroy_m2560902924 (AstarPath_t4090270936 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AstarPath::FloodFill(Pathfinding.GraphNode)
extern "C"  void AstarPath_FloodFill_m2082902810 (AstarPath_t4090270936 * __this, GraphNode_t23612370 * ___seed0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AstarPath::FloodFill(Pathfinding.GraphNode,System.UInt32)
extern "C"  void AstarPath_FloodFill_m2426607566 (AstarPath_t4090270936 * __this, GraphNode_t23612370 * ___seed0, uint32_t ___area1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AstarPath::FloodFill()
extern "C"  void AstarPath_FloodFill_m30718738 (AstarPath_t4090270936 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 AstarPath::GetNewNodeIndex()
extern "C"  int32_t AstarPath_GetNewNodeIndex_m4208518373 (AstarPath_t4090270936 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AstarPath::InitializeNode(Pathfinding.GraphNode)
extern "C"  void AstarPath_InitializeNode_m4098717257 (AstarPath_t4090270936 * __this, GraphNode_t23612370 * ___node0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AstarPath::DestroyNode(Pathfinding.GraphNode)
extern "C"  void AstarPath_DestroyNode_m121720383 (AstarPath_t4090270936 * __this, GraphNode_t23612370 * ___node0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AstarPath::BlockUntilPathQueueBlocked()
extern "C"  void AstarPath_BlockUntilPathQueueBlocked_m3476508770 (AstarPath_t4090270936 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AstarPath::Scan()
extern "C"  void AstarPath_Scan_m3778877070 (AstarPath_t4090270936 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AstarPath::ScanLoop(OnScanStatus)
extern "C"  void AstarPath_ScanLoop_m2760560100 (AstarPath_t4090270936 * __this, OnScanStatus_t2412749870 * ___statusCallback0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AstarPath::ApplyLinks()
extern "C"  void AstarPath_ApplyLinks_m1616134524 (AstarPath_t4090270936 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AstarPath::WaitForPath(Pathfinding.Path)
extern "C"  void AstarPath_WaitForPath_m3560501083 (Il2CppObject * __this /* static, unused */, Path_t1974241691 * ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AstarPath::RegisterSafeUpdate(OnVoidDelegate,System.Boolean)
extern "C"  void AstarPath_RegisterSafeUpdate_m3561565195 (Il2CppObject * __this /* static, unused */, OnVoidDelegate_t2787120856 * ___callback0, bool ___threadSafe1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AstarPath::RegisterSafeUpdate(OnVoidDelegate)
extern "C"  void AstarPath_RegisterSafeUpdate_m775871794 (Il2CppObject * __this /* static, unused */, OnVoidDelegate_t2787120856 * ___callback0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AstarPath::InterruptPathfinding()
extern "C"  void AstarPath_InterruptPathfinding_m865233010 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AstarPath::StartPath(Pathfinding.Path,System.Boolean)
extern "C"  void AstarPath_StartPath_m107241012 (Il2CppObject * __this /* static, unused */, Path_t1974241691 * ___p0, bool ___pushToFront1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AstarPath::OnApplicationQuit()
extern "C"  void AstarPath_OnApplicationQuit_m2120031377 (AstarPath_t4090270936 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AstarPath::ReturnPaths(System.Boolean)
extern "C"  void AstarPath_ReturnPaths_m666449414 (AstarPath_t4090270936 * __this, bool ___timeSlice0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AstarPath::CalculatePathsThreaded(System.Object)
extern "C"  void AstarPath_CalculatePathsThreaded_m187948114 (Il2CppObject * __this /* static, unused */, Il2CppObject * ____threadInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator AstarPath::CalculatePaths(System.Object)
extern "C"  Il2CppObject * AstarPath_CalculatePaths_m3958168067 (Il2CppObject * __this /* static, unused */, Il2CppObject * ____threadInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.NNInfo AstarPath::GetNearest(UnityEngine.Vector3)
extern "C"  NNInfo_t1570625892  AstarPath_GetNearest_m3242620133 (AstarPath_t4090270936 * __this, Vector3_t4282066566  ___position0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.NNInfo AstarPath::GetNearest(UnityEngine.Vector3,Pathfinding.NNConstraint)
extern "C"  NNInfo_t1570625892  AstarPath_GetNearest_m1178989402 (AstarPath_t4090270936 * __this, Vector3_t4282066566  ___position0, NNConstraint_t758567699 * ___constraint1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.NNInfo AstarPath::GetNearest(UnityEngine.Vector3,Pathfinding.NNConstraint,Pathfinding.GraphNode)
extern "C"  NNInfo_t1570625892  AstarPath_GetNearest_m3576599040 (AstarPath_t4090270936 * __this, Vector3_t4282066566  ___position0, NNConstraint_t758567699 * ___constraint1, GraphNode_t23612370 * ___hint2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.GraphNode AstarPath::GetNearest(UnityEngine.Ray)
extern "C"  GraphNode_t23612370 * AstarPath_GetNearest_m3605192179 (AstarPath_t4090270936 * __this, Ray_t3134616544  ___ray0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AstarPath::<OnDrawGizmos>m__331(Pathfinding.GraphNode)
extern "C"  bool AstarPath_U3COnDrawGizmosU3Em__331_m1157835941 (AstarPath_t4090270936 * __this, GraphNode_t23612370 * ___node0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AstarPath::<FloodFill>m__332(Pathfinding.GraphNode)
extern "C"  bool AstarPath_U3CFloodFillU3Em__332_m3228792777 (Il2CppObject * __this /* static, unused */, GraphNode_t23612370 * ___node0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AstarPath::<ScanLoop>m__334(Pathfinding.GraphNode)
extern "C"  bool AstarPath_U3CScanLoopU3Em__334_m3207628647 (Il2CppObject * __this /* static, unused */, GraphNode_t23612370 * ___node0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] AstarPath::ilo_GetTagNames1(AstarPath)
extern "C"  StringU5BU5D_t4054002952* AstarPath_ilo_GetTagNames1_m3093078666 (Il2CppObject * __this /* static, unused */, AstarPath_t4090270936 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AstarPath::ilo_Invoke2(OnVoidDelegate)
extern "C"  void AstarPath_ilo_Invoke2_m2030004736 (Il2CppObject * __this /* static, unused */, OnVoidDelegate_t2787120856 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.NavGraph[] AstarPath::ilo_get_graphs3(AstarPath)
extern "C"  NavGraphU5BU5D_t850130684* AstarPath_ilo_get_graphs3_m779235217 (Il2CppObject * __this /* static, unused */, AstarPath_t4090270936 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AstarPath::ilo_GetNodes4(Pathfinding.NavGraph,Pathfinding.GraphNodeDelegateCancelable)
extern "C"  void AstarPath_ilo_GetNodes4_m4051990319 (Il2CppObject * __this /* static, unused */, NavGraph_t1254319713 * ____this0, GraphNodeDelegateCancelable_t3591372971 * ___del1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 AstarPath::ilo_get_id5(Pathfinding.GraphNode)
extern "C"  uint32_t AstarPath_ilo_get_id5_m2959552578 (Il2CppObject * __this /* static, unused */, GraphNode_t23612370 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AstarPath::ilo_get_error6(Pathfinding.Path)
extern "C"  bool AstarPath_ilo_get_error6_m771952056 (Il2CppObject * __this /* static, unused */, Path_t1974241691 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String AstarPath::ilo_DebugString7(Pathfinding.Path,PathLog)
extern "C"  String_t* AstarPath_ilo_DebugString7_m2715553682 (Il2CppObject * __this /* static, unused */, Path_t1974241691 * ____this0, int32_t ___logMode1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AstarPath::ilo_get_AllReceiversBlocked8(Pathfinding.ThreadControlQueue)
extern "C"  bool AstarPath_ilo_get_AllReceiversBlocked8_m399243456 (Il2CppObject * __this /* static, unused */, ThreadControlQueue_t1865010388 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 AstarPath::ilo_ProcessWorkItems9(AstarPath,System.Boolean)
extern "C"  int32_t AstarPath_ilo_ProcessWorkItems9_m1027919752 (Il2CppObject * __this /* static, unused */, AstarPath_t4090270936 * ____this0, bool ___force1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AstarPath::ilo_FloodFill10(AstarPath)
extern "C"  void AstarPath_ilo_FloodFill10_m1548126268 (Il2CppObject * __this /* static, unused */, AstarPath_t4090270936 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AstarPath::ilo_EnsureValidFloodFill11(AstarPath)
extern "C"  void AstarPath_ilo_EnsureValidFloodFill11_m152639795 (Il2CppObject * __this /* static, unused */, AstarPath_t4090270936 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AstarPath::ilo_AddWorkItem12(AstarPath,AstarPath/AstarWorkItem)
extern "C"  void AstarPath_ilo_AddWorkItem12_m3600789250 (Il2CppObject * __this /* static, unused */, AstarPath_t4090270936 * ____this0, AstarWorkItem_t2566693888  ___itm1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator AstarPath::ilo_UpdateGraphsInteral13(AstarPath,Pathfinding.GraphUpdateObject,System.Single)
extern "C"  Il2CppObject * AstarPath_ilo_UpdateGraphsInteral13_m1159187902 (Il2CppObject * __this /* static, unused */, AstarPath_t4090270936 * ____this0, GraphUpdateObject_t430843704 * ___ob1, float ___t2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AstarPath::ilo_UpdateGraphs14(AstarPath,Pathfinding.GraphUpdateObject)
extern "C"  void AstarPath_ilo_UpdateGraphs14_m3252958405 (Il2CppObject * __this /* static, unused */, AstarPath_t4090270936 * ____this0, GraphUpdateObject_t430843704 * ___ob1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator AstarPath::ilo_DelayedGraphUpdate15(AstarPath)
extern "C"  Il2CppObject * AstarPath_ilo_DelayedGraphUpdate15_m3091792997 (Il2CppObject * __this /* static, unused */, AstarPath_t4090270936 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AstarPath::ilo_get_IsAnyGraphUpdatesQueued16(AstarPath)
extern "C"  bool AstarPath_ilo_get_IsAnyGraphUpdatesQueued16_m2293551163 (Il2CppObject * __this /* static, unused */, AstarPath_t4090270936 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AstarPath::ilo_FlushWorkItems17(AstarPath,System.Boolean,System.Boolean)
extern "C"  void AstarPath_ilo_FlushWorkItems17_m4110419237 (Il2CppObject * __this /* static, unused */, AstarPath_t4090270936 * ____this0, bool ___unblockOnComplete1, bool ___block2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AstarPath::ilo_BlockUntilPathQueueBlocked18(AstarPath)
extern "C"  void AstarPath_ilo_BlockUntilPathQueueBlocked18_m2494780510 (Il2CppObject * __this /* static, unused */, AstarPath_t4090270936 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerable AstarPath::ilo_GetUpdateableGraphs19(Pathfinding.AstarData)
extern "C"  Il2CppObject * AstarPath_ilo_GetUpdateableGraphs19_m68169623 (Il2CppObject * __this /* static, unused */, AstarData_t3283402719 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AstarPath::ilo_SuitableGraph20(Pathfinding.NNConstraint,System.Int32,Pathfinding.NavGraph)
extern "C"  bool AstarPath_ilo_SuitableGraph20_m4011285162 (Il2CppObject * __this /* static, unused */, NNConstraint_t758567699 * ____this0, int32_t ___graphIndex1, NavGraph_t1254319713 * ___graph2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AstarPath::ilo_UpdateAreaInit21(Pathfinding.IUpdatableGraph,Pathfinding.GraphUpdateObject)
extern "C"  void AstarPath_ilo_UpdateAreaInit21_m2807285746 (Il2CppObject * __this /* static, unused */, Il2CppObject * ____this0, GraphUpdateObject_t430843704 * ___o1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AstarPath::ilo_Invoke22(OnScanDelegate,AstarPath)
extern "C"  void AstarPath_ilo_Invoke22_m3293152623 (Il2CppObject * __this /* static, unused */, OnScanDelegate_t3165885121 * ____this0, AstarPath_t4090270936 * ___script1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AstarPath::ilo_FindGraphTypes23(Pathfinding.AstarData)
extern "C"  void AstarPath_ilo_FindGraphTypes23_m1510174454 (Il2CppObject * __this /* static, unused */, AstarData_t3283402719 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AstarPath::ilo_Awake24(Pathfinding.AstarData)
extern "C"  void AstarPath_ilo_Awake24_m4042737914 (Il2CppObject * __this /* static, unused */, AstarData_t3283402719 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AstarPath::ilo_UpdateShortcuts25(Pathfinding.AstarData)
extern "C"  void AstarPath_ilo_UpdateShortcuts25_m2803143770 (Il2CppObject * __this /* static, unused */, AstarData_t3283402719 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AstarPath::ilo_AstarLog26(System.String)
extern "C"  void AstarPath_ilo_AstarLog26_m1600017865 (Il2CppObject * __this /* static, unused */, String_t* ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 AstarPath::ilo_get_NodeIndex27(Pathfinding.GraphNode)
extern "C"  int32_t AstarPath_ilo_get_NodeIndex27_m2810426676 (Il2CppObject * __this /* static, unused */, GraphNode_t23612370 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AstarPath::ilo_DestroyNode28(Pathfinding.PathHandler,Pathfinding.GraphNode)
extern "C"  void AstarPath_ilo_DestroyNode28_m2956573373 (Il2CppObject * __this /* static, unused */, PathHandler_t918952263 * ____this0, GraphNode_t23612370 * ___node1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AstarPath::ilo_Block29(Pathfinding.ThreadControlQueue)
extern "C"  void AstarPath_ilo_Block29_m1262805546 (Il2CppObject * __this /* static, unused */, ThreadControlQueue_t1865010388 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AstarPath::ilo_get_IsUsingMultithreading30()
extern "C"  bool AstarPath_ilo_get_IsUsingMultithreading30_m2073378719 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AstarPath::ilo_ScanLoop31(AstarPath,OnScanStatus)
extern "C"  void AstarPath_ilo_ScanLoop31_m2982008417 (Il2CppObject * __this /* static, unused */, AstarPath_t4090270936 * ____this0, OnScanStatus_t2412749870 * ___statusCallback1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AstarPath::ilo_FindAllModifiers32()
extern "C"  void AstarPath_ilo_FindAllModifiers32_m1295240919 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AstarPath::ilo_Invoke33(OnScanStatus,Pathfinding.Progress)
extern "C"  void AstarPath_ilo_Invoke33_m3989439649 (Il2CppObject * __this /* static, unused */, OnScanStatus_t2412749870 * ____this0, Progress_t2476786339  ___progress1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single AstarPath::ilo_MapToRange34(System.Single,System.Single,System.Single)
extern "C"  float AstarPath_ilo_MapToRange34_m3752672886 (Il2CppObject * __this /* static, unused */, float ___targetMin0, float ___targetMax1, float ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AstarPath::ilo_Invoke35(OnGraphDelegate,Pathfinding.NavGraph)
extern "C"  void AstarPath_ilo_Invoke35_m620866737 (Il2CppObject * __this /* static, unused */, OnGraphDelegate_t381382964 * ____this0, NavGraph_t1254319713 * ___graph1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AstarPath::ilo_ApplyLinks36(AstarPath)
extern "C"  void AstarPath_ilo_ApplyLinks36_m369085256 (Il2CppObject * __this /* static, unused */, AstarPath_t4090270936 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AstarPath::ilo_VerifyIntegrity37(AstarPath)
extern "C"  void AstarPath_ilo_VerifyIntegrity37_m2102269764 (Il2CppObject * __this /* static, unused */, AstarPath_t4090270936 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AstarPath::ilo_PerformBlockingActions38(AstarPath,System.Boolean,System.Boolean)
extern "C"  void AstarPath_ilo_PerformBlockingActions38_m958837866 (Il2CppObject * __this /* static, unused */, AstarPath_t4090270936 * ____this0, bool ___force1, bool ___unblockOnComplete2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Int3 AstarPath::ilo_op_Explicit39(UnityEngine.Vector3)
extern "C"  Int3_t1974045594  AstarPath_ilo_op_Explicit39_m259048372 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___ob0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] AstarPath::ilo_GetData40(Pathfinding.AstarData)
extern "C"  ByteU5BU5D_t4260760469* AstarPath_ilo_GetData40_m1965044827 (Il2CppObject * __this /* static, unused */, AstarData_t3283402719 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AstarPath::ilo_get_IsTerminating41(Pathfinding.ThreadControlQueue)
extern "C"  bool AstarPath_ilo_get_IsTerminating41_m1993487026 (Il2CppObject * __this /* static, unused */, ThreadControlQueue_t1865010388 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PathState AstarPath::ilo_GetState42(Pathfinding.Path)
extern "C"  int32_t AstarPath_ilo_GetState42_m1523313671 (Il2CppObject * __this /* static, unused */, Path_t1974241691 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AstarPath::ilo_PerformBlockingActions43(AstarPath,System.Boolean,System.Boolean)
extern "C"  void AstarPath_ilo_PerformBlockingActions43_m4184742288 (Il2CppObject * __this /* static, unused */, AstarPath_t4090270936 * ____this0, bool ___force1, bool ___unblockOnComplete2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AstarPath::ilo_get_IsEmpty44(Pathfinding.ThreadControlQueue)
extern "C"  bool AstarPath_ilo_get_IsEmpty44_m1866262500 (Il2CppObject * __this /* static, unused */, ThreadControlQueue_t1865010388 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AstarPath::ilo_Error45(Pathfinding.Path)
extern "C"  void AstarPath_ilo_Error45_m4104919480 (Il2CppObject * __this /* static, unused */, Path_t1974241691 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.NavGraph[] AstarPath::ilo_get_graphs46(AstarPath)
extern "C"  NavGraphU5BU5D_t850130684* AstarPath_ilo_get_graphs46_m2425685228 (Il2CppObject * __this /* static, unused */, AstarPath_t4090270936 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AstarPath::ilo_Push47(Pathfinding.ThreadControlQueue,Pathfinding.Path)
extern "C"  void AstarPath_ilo_Push47_m317846366 (Il2CppObject * __this /* static, unused */, ThreadControlQueue_t1865010388 * ____this0, Path_t1974241691 * ___p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AstarPath::ilo_Invoke48(OnPathDelegate,Pathfinding.Path)
extern "C"  void AstarPath_ilo_Invoke48_m362415296 (Il2CppObject * __this /* static, unused */, OnPathDelegate_t598607977 * ____this0, Path_t1974241691 * ___p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AstarPath::ilo_IsDone49(Pathfinding.Path)
extern "C"  bool AstarPath_ilo_IsDone49_m2410932786 (Il2CppObject * __this /* static, unused */, Path_t1974241691 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AstarPath::ilo_Cleanup50(Pathfinding.Path)
extern "C"  void AstarPath_ilo_Cleanup50_m3412157102 (Il2CppObject * __this /* static, unused */, Path_t1974241691 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AstarPath::ilo_Push51(Pathfinding.Util.LockFreeStack,Pathfinding.Path)
extern "C"  void AstarPath_ilo_Push51_m2337784140 (Il2CppObject * __this /* static, unused */, LockFreeStack_t746776019 * ____this0, Path_t1974241691 * ___p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.NNInfo AstarPath::ilo_GetNearest52(AstarPath,UnityEngine.Vector3,Pathfinding.NNConstraint)
extern "C"  NNInfo_t1570625892  AstarPath_ilo_GetNearest52_m3065157078 (Il2CppObject * __this /* static, unused */, AstarPath_t4090270936 * ____this0, Vector3_t4282066566  ___position1, NNConstraint_t758567699 * ___constraint2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AstarPath::ilo_Suitable53(Pathfinding.NNConstraint,Pathfinding.GraphNode)
extern "C"  bool AstarPath_ilo_Suitable53_m679319060 (Il2CppObject * __this /* static, unused */, NNConstraint_t758567699 * ____this0, GraphNode_t23612370 * ___node1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AstarPath::ilo_InSearchTree54(Pathfinding.GraphNode,Pathfinding.Path)
extern "C"  bool AstarPath_ilo_InSearchTree54_m3318959941 (Il2CppObject * __this /* static, unused */, GraphNode_t23612370 * ___node0, Path_t1974241691 * ___path1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.PathNode AstarPath::ilo_GetPathNode55(Pathfinding.PathHandler,Pathfinding.GraphNode)
extern "C"  PathNode_t417131581 * AstarPath_ilo_GetPathNode55_m3036025142 (Il2CppObject * __this /* static, unused */, PathHandler_t918952263 * ____this0, GraphNode_t23612370 * ___node1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 AstarPath::ilo_get_H56(Pathfinding.PathNode)
extern "C"  uint32_t AstarPath_ilo_get_H56_m3297270472 (Il2CppObject * __this /* static, unused */, PathNode_t417131581 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 AstarPath::ilo_get_Penalty57(Pathfinding.GraphNode)
extern "C"  uint32_t AstarPath_ilo_get_Penalty57_m1714045457 (Il2CppObject * __this /* static, unused */, GraphNode_t23612370 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AstarPath::ilo_Destroy58(Pathfinding.GraphNode)
extern "C"  void AstarPath_ilo_Destroy58_m2630463569 (Il2CppObject * __this /* static, unused */, GraphNode_t23612370 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
