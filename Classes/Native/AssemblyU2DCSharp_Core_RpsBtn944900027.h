﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "AssemblyU2DCSharp_Core_RpsChoice52797132.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Core.RpsBtn
struct  RpsBtn_t944900027  : public MonoBehaviour_t667441552
{
public:
	// Core.RpsChoice Core.RpsBtn::RpsChoice
	int32_t ___RpsChoice_2;

public:
	inline static int32_t get_offset_of_RpsChoice_2() { return static_cast<int32_t>(offsetof(RpsBtn_t944900027, ___RpsChoice_2)); }
	inline int32_t get_RpsChoice_2() const { return ___RpsChoice_2; }
	inline int32_t* get_address_of_RpsChoice_2() { return &___RpsChoice_2; }
	inline void set_RpsChoice_2(int32_t value)
	{
		___RpsChoice_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
