﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<Pathfinding.Int2>
struct List_1_t3342231145;

#include "codegen/il2cpp-codegen.h"

// System.Void Pathfinding.Util.ListPool`1<Pathfinding.Int2>::.cctor()
extern "C"  void ListPool_1__cctor_m136990980_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define ListPool_1__cctor_m136990980(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ListPool_1__cctor_m136990980_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.List`1<T> Pathfinding.Util.ListPool`1<Pathfinding.Int2>::Claim()
extern "C"  List_1_t3342231145 * ListPool_1_Claim_m1156262776_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define ListPool_1_Claim_m1156262776(__this /* static, unused */, method) ((  List_1_t3342231145 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ListPool_1_Claim_m1156262776_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.List`1<T> Pathfinding.Util.ListPool`1<Pathfinding.Int2>::Claim(System.Int32)
extern "C"  List_1_t3342231145 * ListPool_1_Claim_m578514121_gshared (Il2CppObject * __this /* static, unused */, int32_t ___capacity0, const MethodInfo* method);
#define ListPool_1_Claim_m578514121(__this /* static, unused */, ___capacity0, method) ((  List_1_t3342231145 * (*) (Il2CppObject * /* static, unused */, int32_t, const MethodInfo*))ListPool_1_Claim_m578514121_gshared)(__this /* static, unused */, ___capacity0, method)
// System.Void Pathfinding.Util.ListPool`1<Pathfinding.Int2>::Warmup(System.Int32,System.Int32)
extern "C"  void ListPool_1_Warmup_m3029164491_gshared (Il2CppObject * __this /* static, unused */, int32_t ___count0, int32_t ___size1, const MethodInfo* method);
#define ListPool_1_Warmup_m3029164491(__this /* static, unused */, ___count0, ___size1, method) ((  void (*) (Il2CppObject * /* static, unused */, int32_t, int32_t, const MethodInfo*))ListPool_1_Warmup_m3029164491_gshared)(__this /* static, unused */, ___count0, ___size1, method)
// System.Void Pathfinding.Util.ListPool`1<Pathfinding.Int2>::Release(System.Collections.Generic.List`1<T>)
extern "C"  void ListPool_1_Release_m2370061062_gshared (Il2CppObject * __this /* static, unused */, List_1_t3342231145 * ___list0, const MethodInfo* method);
#define ListPool_1_Release_m2370061062(__this /* static, unused */, ___list0, method) ((  void (*) (Il2CppObject * /* static, unused */, List_1_t3342231145 *, const MethodInfo*))ListPool_1_Release_m2370061062_gshared)(__this /* static, unused */, ___list0, method)
// System.Void Pathfinding.Util.ListPool`1<Pathfinding.Int2>::Clear()
extern "C"  void ListPool_1_Clear_m58503156_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define ListPool_1_Clear_m58503156(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ListPool_1_Clear_m58503156_gshared)(__this /* static, unused */, method)
// System.Int32 Pathfinding.Util.ListPool`1<Pathfinding.Int2>::GetSize()
extern "C"  int32_t ListPool_1_GetSize_m3297287724_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define ListPool_1_GetSize_m3297287724(__this /* static, unused */, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ListPool_1_GetSize_m3297287724_gshared)(__this /* static, unused */, method)
