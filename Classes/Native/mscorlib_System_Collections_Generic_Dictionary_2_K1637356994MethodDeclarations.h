﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/KeyCollection<AnimationRunner/AniType,System.Object>
struct KeyCollection_t1637356994;
// System.Collections.Generic.Dictionary`2<AnimationRunner/AniType,System.Object>
struct Dictionary_2_t10597543;
// System.Collections.Generic.IEnumerator`1<AnimationRunner/AniType>
struct IEnumerator_1_t2918103700;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// AnimationRunner/AniType[]
struct AniTypeU5BU5D_t3368333050;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_AnimationRunner_AniType1006238651.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Ke625533597.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<AnimationRunner/AniType,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void KeyCollection__ctor_m4044290661_gshared (KeyCollection_t1637356994 * __this, Dictionary_2_t10597543 * ___dictionary0, const MethodInfo* method);
#define KeyCollection__ctor_m4044290661(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t1637356994 *, Dictionary_2_t10597543 *, const MethodInfo*))KeyCollection__ctor_m4044290661_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<AnimationRunner/AniType,System.Object>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m537048017_gshared (KeyCollection_t1637356994 * __this, int32_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m537048017(__this, ___item0, method) ((  void (*) (KeyCollection_t1637356994 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m537048017_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<AnimationRunner/AniType,System.Object>::System.Collections.Generic.ICollection<TKey>.Clear()
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1582668168_gshared (KeyCollection_t1637356994 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1582668168(__this, method) ((  void (*) (KeyCollection_t1637356994 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1582668168_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<AnimationRunner/AniType,System.Object>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m278208601_gshared (KeyCollection_t1637356994 * __this, int32_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m278208601(__this, ___item0, method) ((  bool (*) (KeyCollection_t1637356994 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m278208601_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<AnimationRunner/AniType,System.Object>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m4264547326_gshared (KeyCollection_t1637356994 * __this, int32_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m4264547326(__this, ___item0, method) ((  bool (*) (KeyCollection_t1637356994 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m4264547326_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<AnimationRunner/AniType,System.Object>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m3773548804_gshared (KeyCollection_t1637356994 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m3773548804(__this, method) ((  Il2CppObject* (*) (KeyCollection_t1637356994 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m3773548804_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<AnimationRunner/AniType,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m1270182394_gshared (KeyCollection_t1637356994 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_CopyTo_m1270182394(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t1637356994 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m1270182394_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<AnimationRunner/AniType,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1983916149_gshared (KeyCollection_t1637356994 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1983916149(__this, method) ((  Il2CppObject * (*) (KeyCollection_t1637356994 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1983916149_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<AnimationRunner/AniType,System.Object>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m3951551866_gshared (KeyCollection_t1637356994 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m3951551866(__this, method) ((  bool (*) (KeyCollection_t1637356994 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m3951551866_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<AnimationRunner/AniType,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m2888408748_gshared (KeyCollection_t1637356994 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m2888408748(__this, method) ((  bool (*) (KeyCollection_t1637356994 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m2888408748_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<AnimationRunner/AniType,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m1903343128_gshared (KeyCollection_t1637356994 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m1903343128(__this, method) ((  Il2CppObject * (*) (KeyCollection_t1637356994 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m1903343128_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<AnimationRunner/AniType,System.Object>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m4220990746_gshared (KeyCollection_t1637356994 * __this, AniTypeU5BU5D_t3368333050* ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_CopyTo_m4220990746(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t1637356994 *, AniTypeU5BU5D_t3368333050*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m4220990746_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<AnimationRunner/AniType,System.Object>::GetEnumerator()
extern "C"  Enumerator_t625533597  KeyCollection_GetEnumerator_m165844029_gshared (KeyCollection_t1637356994 * __this, const MethodInfo* method);
#define KeyCollection_GetEnumerator_m165844029(__this, method) ((  Enumerator_t625533597  (*) (KeyCollection_t1637356994 *, const MethodInfo*))KeyCollection_GetEnumerator_m165844029_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<AnimationRunner/AniType,System.Object>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m2255579378_gshared (KeyCollection_t1637356994 * __this, const MethodInfo* method);
#define KeyCollection_get_Count_m2255579378(__this, method) ((  int32_t (*) (KeyCollection_t1637356994 *, const MethodInfo*))KeyCollection_get_Count_m2255579378_gshared)(__this, method)
