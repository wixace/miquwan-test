﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TapRecognizer
struct TapRecognizer_t3788301703;
// TapGesture
struct TapGesture_t659145798;
// FingerGestures/IFingerList
struct IFingerList_t1026994100;
// System.String
struct String_t;
// GestureRecognizer
struct GestureRecognizer_t3512875949;
// Gesture
struct Gesture_t1589572905;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_TapGesture659145798.h"
#include "AssemblyU2DCSharp_GestureRecognitionState3604239971.h"
#include "AssemblyU2DCSharp_TapRecognizer3788301703.h"
#include "AssemblyU2DCSharp_GestureRecognizer3512875949.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "AssemblyU2DCSharp_Gesture1589572905.h"

// System.Void TapRecognizer::.ctor()
extern "C"  void TapRecognizer__ctor_m3478766532 (TapRecognizer_t3788301703 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TapRecognizer::get_IsMultiTap()
extern "C"  bool TapRecognizer_get_IsMultiTap_m1126032713 (TapRecognizer_t3788301703 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TapRecognizer::HasTimedOut(TapGesture)
extern "C"  bool TapRecognizer_HasTimedOut_m1637105889 (TapRecognizer_t3788301703 * __this, TapGesture_t659145798 * ___gesture0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TapRecognizer::Reset(TapGesture)
extern "C"  void TapRecognizer_Reset_m472955115 (TapRecognizer_t3788301703 * __this, TapGesture_t659145798 * ___gesture0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TapRecognizer::get_SupportFingerClustering()
extern "C"  bool TapRecognizer_get_SupportFingerClustering_m666193165 (TapRecognizer_t3788301703 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GestureRecognitionState TapRecognizer::RecognizeSingleTap(TapGesture,FingerGestures/IFingerList)
extern "C"  int32_t TapRecognizer_RecognizeSingleTap_m2831951227 (TapRecognizer_t3788301703 * __this, TapGesture_t659145798 * ___gesture0, Il2CppObject * ___touches1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GestureRecognitionState TapRecognizer::RecognizeMultiTap(TapGesture,FingerGestures/IFingerList)
extern "C"  int32_t TapRecognizer_RecognizeMultiTap_m4263406222 (TapRecognizer_t3788301703 * __this, TapGesture_t659145798 * ___gesture0, Il2CppObject * ___touches1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String TapRecognizer::GetDefaultEventMessageName()
extern "C"  String_t* TapRecognizer_GetDefaultEventMessageName_m2171243872 (TapRecognizer_t3788301703 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TapRecognizer::OnBegin(TapGesture,FingerGestures/IFingerList)
extern "C"  void TapRecognizer_OnBegin_m1898903660 (TapRecognizer_t3788301703 * __this, TapGesture_t659145798 * ___gesture0, Il2CppObject * ___touches1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GestureRecognitionState TapRecognizer::OnRecognize(TapGesture,FingerGestures/IFingerList)
extern "C"  int32_t TapRecognizer_OnRecognize_m808350423 (TapRecognizer_t3788301703 * __this, TapGesture_t659145798 * ___gesture0, Il2CppObject * ___touches1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TapRecognizer::ilo_get_IsMultiTap1(TapRecognizer)
extern "C"  bool TapRecognizer_ilo_get_IsMultiTap1_m1637750036 (Il2CppObject * __this /* static, unused */, TapRecognizer_t3788301703 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TapRecognizer::ilo_get_SupportFingerClustering2(GestureRecognizer)
extern "C"  bool TapRecognizer_ilo_get_SupportFingerClustering2_m321059531 (Il2CppObject * __this /* static, unused */, GestureRecognizer_t3512875949 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 TapRecognizer::ilo_get_Count3(FingerGestures/IFingerList)
extern "C"  int32_t TapRecognizer_ilo_get_Count3_m584623230 (Il2CppObject * __this /* static, unused */, Il2CppObject * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 TapRecognizer::ilo_get_RequiredFingerCount4(GestureRecognizer)
extern "C"  int32_t TapRecognizer_ilo_get_RequiredFingerCount4_m1281465482 (Il2CppObject * __this /* static, unused */, GestureRecognizer_t3512875949 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 TapRecognizer::ilo_GetAveragePosition5(FingerGestures/IFingerList)
extern "C"  Vector2_t4282066565  TapRecognizer_ilo_GetAveragePosition5_m3763602619 (Il2CppObject * __this /* static, unused */, Il2CppObject * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 TapRecognizer::ilo_get_StartPosition6(Gesture)
extern "C"  Vector2_t4282066565  TapRecognizer_ilo_get_StartPosition6_m2632652221 (Il2CppObject * __this /* static, unused */, Gesture_t1589572905 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TapRecognizer::ilo_HasTimedOut7(TapRecognizer,TapGesture)
extern "C"  bool TapRecognizer_ilo_HasTimedOut7_m124784282 (Il2CppObject * __this /* static, unused */, TapRecognizer_t3788301703 * ____this0, TapGesture_t659145798 * ___gesture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 TapRecognizer::ilo_get_Taps8(TapGesture)
extern "C"  int32_t TapRecognizer_ilo_get_Taps8_m1281394998 (Il2CppObject * __this /* static, unused */, TapGesture_t659145798 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 TapRecognizer::ilo_get_Position9(Gesture)
extern "C"  Vector2_t4282066565  TapRecognizer_ilo_get_Position9_m1444611864 (Il2CppObject * __this /* static, unused */, Gesture_t1589572905 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TapRecognizer::ilo_set_StartPosition10(Gesture,UnityEngine.Vector2)
extern "C"  void TapRecognizer_ilo_set_StartPosition10_m1671893409 (Il2CppObject * __this /* static, unused */, Gesture_t1589572905 * ____this0, Vector2_t4282066565  ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
