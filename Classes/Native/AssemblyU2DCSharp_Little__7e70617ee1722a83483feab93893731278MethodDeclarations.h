﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._7e70617ee1722a83483feab981ba06d0
struct _7e70617ee1722a83483feab981ba06d0_t3893731278;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._7e70617ee1722a83483feab981ba06d0::.ctor()
extern "C"  void _7e70617ee1722a83483feab981ba06d0__ctor_m1269394975 (_7e70617ee1722a83483feab981ba06d0_t3893731278 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._7e70617ee1722a83483feab981ba06d0::_7e70617ee1722a83483feab981ba06d0m2(System.Int32)
extern "C"  int32_t _7e70617ee1722a83483feab981ba06d0__7e70617ee1722a83483feab981ba06d0m2_m1579126489 (_7e70617ee1722a83483feab981ba06d0_t3893731278 * __this, int32_t ____7e70617ee1722a83483feab981ba06d0a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._7e70617ee1722a83483feab981ba06d0::_7e70617ee1722a83483feab981ba06d0m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _7e70617ee1722a83483feab981ba06d0__7e70617ee1722a83483feab981ba06d0m_m3557851133 (_7e70617ee1722a83483feab981ba06d0_t3893731278 * __this, int32_t ____7e70617ee1722a83483feab981ba06d0a0, int32_t ____7e70617ee1722a83483feab981ba06d0501, int32_t ____7e70617ee1722a83483feab981ba06d0c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
