﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_lowmoutouDroneemar149
struct M_lowmoutouDroneemar149_t3142486912;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"

// System.Void GarbageiOS.M_lowmoutouDroneemar149::.ctor()
extern "C"  void M_lowmoutouDroneemar149__ctor_m3913425379 (M_lowmoutouDroneemar149_t3142486912 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_lowmoutouDroneemar149::M_nohaimi0(System.String[],System.Int32)
extern "C"  void M_lowmoutouDroneemar149_M_nohaimi0_m2453001871 (M_lowmoutouDroneemar149_t3142486912 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_lowmoutouDroneemar149::M_narnema1(System.String[],System.Int32)
extern "C"  void M_lowmoutouDroneemar149_M_narnema1_m560590545 (M_lowmoutouDroneemar149_t3142486912 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_lowmoutouDroneemar149::M_nugirCatou2(System.String[],System.Int32)
extern "C"  void M_lowmoutouDroneemar149_M_nugirCatou2_m1822224037 (M_lowmoutouDroneemar149_t3142486912 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
