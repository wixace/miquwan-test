﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_GUIStyleStateGenerated
struct UnityEngine_GUIStyleStateGenerated_t3615700540;
// JSVCall
struct JSVCall_t3708497963;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void UnityEngine_GUIStyleStateGenerated::.ctor()
extern "C"  void UnityEngine_GUIStyleStateGenerated__ctor_m3709001183 (UnityEngine_GUIStyleStateGenerated_t3615700540 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIStyleStateGenerated::GUIStyleState_GUIStyleState1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIStyleStateGenerated_GUIStyleState_GUIStyleState1_m3127902275 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUIStyleStateGenerated::GUIStyleState_background(JSVCall)
extern "C"  void UnityEngine_GUIStyleStateGenerated_GUIStyleState_background_m4046356944 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUIStyleStateGenerated::GUIStyleState_textColor(JSVCall)
extern "C"  void UnityEngine_GUIStyleStateGenerated_GUIStyleState_textColor_m509628248 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUIStyleStateGenerated::__Register()
extern "C"  void UnityEngine_GUIStyleStateGenerated___Register_m2136568392 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIStyleStateGenerated::ilo_attachFinalizerObject1(System.Int32)
extern "C"  bool UnityEngine_GUIStyleStateGenerated_ilo_attachFinalizerObject1_m3850652896 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_GUIStyleStateGenerated::ilo_setObject2(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UnityEngine_GUIStyleStateGenerated_ilo_setObject2_m759079116 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
