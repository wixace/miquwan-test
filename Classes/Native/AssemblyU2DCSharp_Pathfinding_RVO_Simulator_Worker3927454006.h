﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Threading.Thread
struct Thread_t1973216770;
// System.Threading.AutoResetEvent
struct AutoResetEvent_t874642578;
// System.Threading.ManualResetEvent
struct ManualResetEvent_t924017833;
// Pathfinding.RVO.Simulator
struct Simulator_t2705969170;
// Pathfinding.RVO.Simulator/WorkerContext
struct WorkerContext_t2850943897;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.RVO.Simulator/Worker
struct  Worker_t3927454006  : public Il2CppObject
{
public:
	// System.Threading.Thread Pathfinding.RVO.Simulator/Worker::thread
	Thread_t1973216770 * ___thread_0;
	// System.Int32 Pathfinding.RVO.Simulator/Worker::start
	int32_t ___start_1;
	// System.Int32 Pathfinding.RVO.Simulator/Worker::end
	int32_t ___end_2;
	// System.Int32 Pathfinding.RVO.Simulator/Worker::task
	int32_t ___task_3;
	// System.Threading.AutoResetEvent Pathfinding.RVO.Simulator/Worker::runFlag
	AutoResetEvent_t874642578 * ___runFlag_4;
	// System.Threading.ManualResetEvent Pathfinding.RVO.Simulator/Worker::waitFlag
	ManualResetEvent_t924017833 * ___waitFlag_5;
	// Pathfinding.RVO.Simulator Pathfinding.RVO.Simulator/Worker::simulator
	Simulator_t2705969170 * ___simulator_6;
	// System.Boolean Pathfinding.RVO.Simulator/Worker::terminate
	bool ___terminate_7;
	// Pathfinding.RVO.Simulator/WorkerContext Pathfinding.RVO.Simulator/Worker::context
	WorkerContext_t2850943897 * ___context_8;

public:
	inline static int32_t get_offset_of_thread_0() { return static_cast<int32_t>(offsetof(Worker_t3927454006, ___thread_0)); }
	inline Thread_t1973216770 * get_thread_0() const { return ___thread_0; }
	inline Thread_t1973216770 ** get_address_of_thread_0() { return &___thread_0; }
	inline void set_thread_0(Thread_t1973216770 * value)
	{
		___thread_0 = value;
		Il2CppCodeGenWriteBarrier(&___thread_0, value);
	}

	inline static int32_t get_offset_of_start_1() { return static_cast<int32_t>(offsetof(Worker_t3927454006, ___start_1)); }
	inline int32_t get_start_1() const { return ___start_1; }
	inline int32_t* get_address_of_start_1() { return &___start_1; }
	inline void set_start_1(int32_t value)
	{
		___start_1 = value;
	}

	inline static int32_t get_offset_of_end_2() { return static_cast<int32_t>(offsetof(Worker_t3927454006, ___end_2)); }
	inline int32_t get_end_2() const { return ___end_2; }
	inline int32_t* get_address_of_end_2() { return &___end_2; }
	inline void set_end_2(int32_t value)
	{
		___end_2 = value;
	}

	inline static int32_t get_offset_of_task_3() { return static_cast<int32_t>(offsetof(Worker_t3927454006, ___task_3)); }
	inline int32_t get_task_3() const { return ___task_3; }
	inline int32_t* get_address_of_task_3() { return &___task_3; }
	inline void set_task_3(int32_t value)
	{
		___task_3 = value;
	}

	inline static int32_t get_offset_of_runFlag_4() { return static_cast<int32_t>(offsetof(Worker_t3927454006, ___runFlag_4)); }
	inline AutoResetEvent_t874642578 * get_runFlag_4() const { return ___runFlag_4; }
	inline AutoResetEvent_t874642578 ** get_address_of_runFlag_4() { return &___runFlag_4; }
	inline void set_runFlag_4(AutoResetEvent_t874642578 * value)
	{
		___runFlag_4 = value;
		Il2CppCodeGenWriteBarrier(&___runFlag_4, value);
	}

	inline static int32_t get_offset_of_waitFlag_5() { return static_cast<int32_t>(offsetof(Worker_t3927454006, ___waitFlag_5)); }
	inline ManualResetEvent_t924017833 * get_waitFlag_5() const { return ___waitFlag_5; }
	inline ManualResetEvent_t924017833 ** get_address_of_waitFlag_5() { return &___waitFlag_5; }
	inline void set_waitFlag_5(ManualResetEvent_t924017833 * value)
	{
		___waitFlag_5 = value;
		Il2CppCodeGenWriteBarrier(&___waitFlag_5, value);
	}

	inline static int32_t get_offset_of_simulator_6() { return static_cast<int32_t>(offsetof(Worker_t3927454006, ___simulator_6)); }
	inline Simulator_t2705969170 * get_simulator_6() const { return ___simulator_6; }
	inline Simulator_t2705969170 ** get_address_of_simulator_6() { return &___simulator_6; }
	inline void set_simulator_6(Simulator_t2705969170 * value)
	{
		___simulator_6 = value;
		Il2CppCodeGenWriteBarrier(&___simulator_6, value);
	}

	inline static int32_t get_offset_of_terminate_7() { return static_cast<int32_t>(offsetof(Worker_t3927454006, ___terminate_7)); }
	inline bool get_terminate_7() const { return ___terminate_7; }
	inline bool* get_address_of_terminate_7() { return &___terminate_7; }
	inline void set_terminate_7(bool value)
	{
		___terminate_7 = value;
	}

	inline static int32_t get_offset_of_context_8() { return static_cast<int32_t>(offsetof(Worker_t3927454006, ___context_8)); }
	inline WorkerContext_t2850943897 * get_context_8() const { return ___context_8; }
	inline WorkerContext_t2850943897 ** get_address_of_context_8() { return &___context_8; }
	inline void set_context_8(WorkerContext_t2850943897 * value)
	{
		___context_8 = value;
		Il2CppCodeGenWriteBarrier(&___context_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
