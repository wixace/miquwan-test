﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Linq.JArray
struct JArray_t3394795039;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// System.Object
struct Il2CppObject;
// Newtonsoft.Json.Linq.JToken[]
struct JTokenU5BU5D_t2853253222;
// System.Collections.Generic.IList`1<Newtonsoft.Json.Linq.JToken>
struct IList_1_t1811925858;
// Newtonsoft.Json.Linq.JToken
struct JToken_t3412245951;
// Newtonsoft.Json.JsonReader
struct JsonReader_t816925123;
// System.String
struct String_t;
// Newtonsoft.Json.JsonSerializer
struct JsonSerializer_t251850770;
// Newtonsoft.Json.JsonWriter
struct JsonWriter_t972330355;
// Newtonsoft.Json.JsonConverter[]
struct JsonConverterU5BU5D_t638349667;
// Newtonsoft.Json.Linq.JContainer
struct JContainer_t3364442311;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JArray3394795039.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JTokenType3916897561.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JToken3412245951.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonReader816925123.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonSerializer251850770.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonWriter972330355.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JContainer3364442311.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonToken4173078175.h"

// System.Void Newtonsoft.Json.Linq.JArray::.ctor()
extern "C"  void JArray__ctor_m2818036547 (JArray_t3394795039 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JArray::.ctor(Newtonsoft.Json.Linq.JArray)
extern "C"  void JArray__ctor_m505908391 (JArray_t3394795039 * __this, JArray_t3394795039 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JArray::.ctor(System.Object[])
extern "C"  void JArray__ctor_m1160401423 (JArray_t3394795039 * __this, ObjectU5BU5D_t1108656482* ___content0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JArray::.ctor(System.Object)
extern "C"  void JArray__ctor_m255955761 (JArray_t3394795039 * __this, Il2CppObject * ___content0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JArray::System.Collections.Generic.ICollection<Newtonsoft.Json.Linq.JToken>.CopyTo(Newtonsoft.Json.Linq.JToken[],System.Int32)
extern "C"  void JArray_System_Collections_Generic_ICollectionU3CNewtonsoft_Json_Linq_JTokenU3E_CopyTo_m3256419700 (JArray_t3394795039 * __this, JTokenU5BU5D_t2853253222* ___array0, int32_t ___arrayIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Linq.JArray::System.Collections.Generic.ICollection<Newtonsoft.Json.Linq.JToken>.get_IsReadOnly()
extern "C"  bool JArray_System_Collections_Generic_ICollectionU3CNewtonsoft_Json_Linq_JTokenU3E_get_IsReadOnly_m1315846294 (JArray_t3394795039 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IList`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JArray::get_ChildrenTokens()
extern "C"  Il2CppObject* JArray_get_ChildrenTokens_m2090035513 (JArray_t3394795039 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JTokenType Newtonsoft.Json.Linq.JArray::get_Type()
extern "C"  int32_t JArray_get_Type_m2052067859 (JArray_t3394795039 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Linq.JArray::DeepEquals(Newtonsoft.Json.Linq.JToken)
extern "C"  bool JArray_DeepEquals_m880007346 (JArray_t3394795039 * __this, JToken_t3412245951 * ___node0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JArray::CloneToken()
extern "C"  JToken_t3412245951 * JArray_CloneToken_m3470042066 (JArray_t3394795039 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JArray Newtonsoft.Json.Linq.JArray::Load(Newtonsoft.Json.JsonReader)
extern "C"  JArray_t3394795039 * JArray_Load_m1319409372 (Il2CppObject * __this /* static, unused */, JsonReader_t816925123 * ___reader0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JArray Newtonsoft.Json.Linq.JArray::Parse(System.String)
extern "C"  JArray_t3394795039 * JArray_Parse_m139214627 (Il2CppObject * __this /* static, unused */, String_t* ___json0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JArray Newtonsoft.Json.Linq.JArray::FromObject(System.Object)
extern "C"  JArray_t3394795039 * JArray_FromObject_m3927320085 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___o0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JArray Newtonsoft.Json.Linq.JArray::FromObject(System.Object,Newtonsoft.Json.JsonSerializer)
extern "C"  JArray_t3394795039 * JArray_FromObject_m1263001474 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___o0, JsonSerializer_t251850770 * ___jsonSerializer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JArray::WriteTo(Newtonsoft.Json.JsonWriter,Newtonsoft.Json.JsonConverter[])
extern "C"  void JArray_WriteTo_m944467282 (JArray_t3394795039 * __this, JsonWriter_t972330355 * ___writer0, JsonConverterU5BU5D_t638349667* ___converters1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JArray::get_Item(System.Object)
extern "C"  JToken_t3412245951 * JArray_get_Item_m138481154 (JArray_t3394795039 * __this, Il2CppObject * ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JArray::set_Item(System.Object,Newtonsoft.Json.Linq.JToken)
extern "C"  void JArray_set_Item_m807916071 (JArray_t3394795039 * __this, Il2CppObject * ___key0, JToken_t3412245951 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JArray::get_Item(System.Int32)
extern "C"  JToken_t3412245951 * JArray_get_Item_m2337874723 (JArray_t3394795039 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JArray::set_Item(System.Int32,Newtonsoft.Json.Linq.JToken)
extern "C"  void JArray_set_Item_m3760123174 (JArray_t3394795039 * __this, int32_t ___index0, JToken_t3412245951 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Newtonsoft.Json.Linq.JArray::IndexOf(Newtonsoft.Json.Linq.JToken)
extern "C"  int32_t JArray_IndexOf_m2136073010 (JArray_t3394795039 * __this, JToken_t3412245951 * ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JArray::Insert(System.Int32,Newtonsoft.Json.Linq.JToken)
extern "C"  void JArray_Insert_m687796175 (JArray_t3394795039 * __this, int32_t ___index0, JToken_t3412245951 * ___item1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JArray::RemoveAt(System.Int32)
extern "C"  void JArray_RemoveAt_m953404681 (JArray_t3394795039 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JArray::Add(Newtonsoft.Json.Linq.JToken)
extern "C"  void JArray_Add_m2763501608 (JArray_t3394795039 * __this, JToken_t3412245951 * ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JArray::Clear()
extern "C"  void JArray_Clear_m224169838 (JArray_t3394795039 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Linq.JArray::Contains(Newtonsoft.Json.Linq.JToken)
extern "C"  bool JArray_Contains_m2493935582 (JArray_t3394795039 * __this, JToken_t3412245951 * ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Linq.JArray::Remove(Newtonsoft.Json.Linq.JToken)
extern "C"  bool JArray_Remove_m3058964633 (JArray_t3394795039 * __this, JToken_t3412245951 * ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Newtonsoft.Json.Linq.JArray::GetDeepHashCode()
extern "C"  int32_t JArray_GetDeepHashCode_m2305145420 (JArray_t3394795039 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Linq.JArray::ilo_ContentsEqual1(Newtonsoft.Json.Linq.JContainer,Newtonsoft.Json.Linq.JContainer)
extern "C"  bool JArray_ilo_ContentsEqual1_m3514904583 (Il2CppObject * __this /* static, unused */, JContainer_t3364442311 * ____this0, JContainer_t3364442311 * ___container1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.JsonToken Newtonsoft.Json.Linq.JArray::ilo_get_TokenType2(Newtonsoft.Json.JsonReader)
extern "C"  int32_t JArray_ilo_get_TokenType2_m409429253 (Il2CppObject * __this /* static, unused */, JsonReader_t816925123 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JArray::ilo_ReadTokenFrom3(Newtonsoft.Json.Linq.JContainer,Newtonsoft.Json.JsonReader)
extern "C"  void JArray_ilo_ReadTokenFrom3_m3732031390 (Il2CppObject * __this /* static, unused */, JContainer_t3364442311 * ____this0, JsonReader_t816925123 * ___r1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JArray Newtonsoft.Json.Linq.JArray::ilo_FromObject4(System.Object,Newtonsoft.Json.JsonSerializer)
extern "C"  JArray_t3394795039 * JArray_ilo_FromObject4_m592496743 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___o0, JsonSerializer_t251850770 * ___jsonSerializer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JTokenType Newtonsoft.Json.Linq.JArray::ilo_get_Type5(Newtonsoft.Json.Linq.JToken)
extern "C"  int32_t JArray_ilo_get_Type5_m3029087993 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JArray::ilo_SetItem6(Newtonsoft.Json.Linq.JContainer,System.Int32,Newtonsoft.Json.Linq.JToken)
extern "C"  void JArray_ilo_SetItem6_m3499752430 (Il2CppObject * __this /* static, unused */, JContainer_t3364442311 * ____this0, int32_t ___index1, JToken_t3412245951 * ___item2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JArray::ilo_InsertItem7(Newtonsoft.Json.Linq.JContainer,System.Int32,Newtonsoft.Json.Linq.JToken)
extern "C"  void JArray_ilo_InsertItem7_m946782834 (Il2CppObject * __this /* static, unused */, JContainer_t3364442311 * ____this0, int32_t ___index1, JToken_t3412245951 * ___item2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JArray::ilo_RemoveItemAt8(Newtonsoft.Json.Linq.JContainer,System.Int32)
extern "C"  void JArray_ilo_RemoveItemAt8_m3886920177 (Il2CppObject * __this /* static, unused */, JContainer_t3364442311 * ____this0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JArray::ilo_Add9(Newtonsoft.Json.Linq.JContainer,System.Object)
extern "C"  void JArray_ilo_Add9_m738824996 (Il2CppObject * __this /* static, unused */, JContainer_t3364442311 * ____this0, Il2CppObject * ___content1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
