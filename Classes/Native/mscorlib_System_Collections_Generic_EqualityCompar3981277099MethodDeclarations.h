﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<Entity.Behavior.EBehaviorID>
struct DefaultComparer_t3981277099;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Entity_Behavior_EBehaviorID1238533136.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<Entity.Behavior.EBehaviorID>::.ctor()
extern "C"  void DefaultComparer__ctor_m1819205919_gshared (DefaultComparer_t3981277099 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m1819205919(__this, method) ((  void (*) (DefaultComparer_t3981277099 *, const MethodInfo*))DefaultComparer__ctor_m1819205919_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<Entity.Behavior.EBehaviorID>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m3059350956_gshared (DefaultComparer_t3981277099 * __this, uint8_t ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m3059350956(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t3981277099 *, uint8_t, const MethodInfo*))DefaultComparer_GetHashCode_m3059350956_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<Entity.Behavior.EBehaviorID>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m3439591536_gshared (DefaultComparer_t3981277099 * __this, uint8_t ___x0, uint8_t ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m3439591536(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t3981277099 *, uint8_t, uint8_t, const MethodInfo*))DefaultComparer_Equals_m3439591536_gshared)(__this, ___x0, ___y1, method)
