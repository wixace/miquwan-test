﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t4054002952;
// System.Action
struct Action_t3771233898;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PluginIqiyi
struct  PluginIqiyi_t2544921470  : public MonoBehaviour_t667441552
{
public:
	// System.String PluginIqiyi::uid
	String_t* ___uid_2;
	// System.String PluginIqiyi::sign
	String_t* ___sign_3;
	// System.String PluginIqiyi::gameid
	String_t* ___gameid_4;
	// System.String PluginIqiyi::uname
	String_t* ___uname_5;
	// System.String PluginIqiyi::timestamp
	String_t* ___timestamp_6;
	// System.String PluginIqiyi::isAdult
	String_t* ___isAdult_7;
	// System.String[] PluginIqiyi::sdkInfos
	StringU5BU5D_t4054002952* ___sdkInfos_8;
	// System.String PluginIqiyi::time
	String_t* ___time_9;
	// System.Boolean PluginIqiyi::isOut
	bool ___isOut_10;

public:
	inline static int32_t get_offset_of_uid_2() { return static_cast<int32_t>(offsetof(PluginIqiyi_t2544921470, ___uid_2)); }
	inline String_t* get_uid_2() const { return ___uid_2; }
	inline String_t** get_address_of_uid_2() { return &___uid_2; }
	inline void set_uid_2(String_t* value)
	{
		___uid_2 = value;
		Il2CppCodeGenWriteBarrier(&___uid_2, value);
	}

	inline static int32_t get_offset_of_sign_3() { return static_cast<int32_t>(offsetof(PluginIqiyi_t2544921470, ___sign_3)); }
	inline String_t* get_sign_3() const { return ___sign_3; }
	inline String_t** get_address_of_sign_3() { return &___sign_3; }
	inline void set_sign_3(String_t* value)
	{
		___sign_3 = value;
		Il2CppCodeGenWriteBarrier(&___sign_3, value);
	}

	inline static int32_t get_offset_of_gameid_4() { return static_cast<int32_t>(offsetof(PluginIqiyi_t2544921470, ___gameid_4)); }
	inline String_t* get_gameid_4() const { return ___gameid_4; }
	inline String_t** get_address_of_gameid_4() { return &___gameid_4; }
	inline void set_gameid_4(String_t* value)
	{
		___gameid_4 = value;
		Il2CppCodeGenWriteBarrier(&___gameid_4, value);
	}

	inline static int32_t get_offset_of_uname_5() { return static_cast<int32_t>(offsetof(PluginIqiyi_t2544921470, ___uname_5)); }
	inline String_t* get_uname_5() const { return ___uname_5; }
	inline String_t** get_address_of_uname_5() { return &___uname_5; }
	inline void set_uname_5(String_t* value)
	{
		___uname_5 = value;
		Il2CppCodeGenWriteBarrier(&___uname_5, value);
	}

	inline static int32_t get_offset_of_timestamp_6() { return static_cast<int32_t>(offsetof(PluginIqiyi_t2544921470, ___timestamp_6)); }
	inline String_t* get_timestamp_6() const { return ___timestamp_6; }
	inline String_t** get_address_of_timestamp_6() { return &___timestamp_6; }
	inline void set_timestamp_6(String_t* value)
	{
		___timestamp_6 = value;
		Il2CppCodeGenWriteBarrier(&___timestamp_6, value);
	}

	inline static int32_t get_offset_of_isAdult_7() { return static_cast<int32_t>(offsetof(PluginIqiyi_t2544921470, ___isAdult_7)); }
	inline String_t* get_isAdult_7() const { return ___isAdult_7; }
	inline String_t** get_address_of_isAdult_7() { return &___isAdult_7; }
	inline void set_isAdult_7(String_t* value)
	{
		___isAdult_7 = value;
		Il2CppCodeGenWriteBarrier(&___isAdult_7, value);
	}

	inline static int32_t get_offset_of_sdkInfos_8() { return static_cast<int32_t>(offsetof(PluginIqiyi_t2544921470, ___sdkInfos_8)); }
	inline StringU5BU5D_t4054002952* get_sdkInfos_8() const { return ___sdkInfos_8; }
	inline StringU5BU5D_t4054002952** get_address_of_sdkInfos_8() { return &___sdkInfos_8; }
	inline void set_sdkInfos_8(StringU5BU5D_t4054002952* value)
	{
		___sdkInfos_8 = value;
		Il2CppCodeGenWriteBarrier(&___sdkInfos_8, value);
	}

	inline static int32_t get_offset_of_time_9() { return static_cast<int32_t>(offsetof(PluginIqiyi_t2544921470, ___time_9)); }
	inline String_t* get_time_9() const { return ___time_9; }
	inline String_t** get_address_of_time_9() { return &___time_9; }
	inline void set_time_9(String_t* value)
	{
		___time_9 = value;
		Il2CppCodeGenWriteBarrier(&___time_9, value);
	}

	inline static int32_t get_offset_of_isOut_10() { return static_cast<int32_t>(offsetof(PluginIqiyi_t2544921470, ___isOut_10)); }
	inline bool get_isOut_10() const { return ___isOut_10; }
	inline bool* get_address_of_isOut_10() { return &___isOut_10; }
	inline void set_isOut_10(bool value)
	{
		___isOut_10 = value;
	}
};

struct PluginIqiyi_t2544921470_StaticFields
{
public:
	// System.Action PluginIqiyi::<>f__am$cache9
	Action_t3771233898 * ___U3CU3Ef__amU24cache9_11;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache9_11() { return static_cast<int32_t>(offsetof(PluginIqiyi_t2544921470_StaticFields, ___U3CU3Ef__amU24cache9_11)); }
	inline Action_t3771233898 * get_U3CU3Ef__amU24cache9_11() const { return ___U3CU3Ef__amU24cache9_11; }
	inline Action_t3771233898 ** get_address_of_U3CU3Ef__amU24cache9_11() { return &___U3CU3Ef__amU24cache9_11; }
	inline void set_U3CU3Ef__amU24cache9_11(Action_t3771233898 * value)
	{
		___U3CU3Ef__amU24cache9_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache9_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
