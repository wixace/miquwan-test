﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._ed2dc351e9b4038bb73be748a1c7da2a
struct _ed2dc351e9b4038bb73be748a1c7da2a_t3572811411;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._ed2dc351e9b4038bb73be748a1c7da2a::.ctor()
extern "C"  void _ed2dc351e9b4038bb73be748a1c7da2a__ctor_m1291392250 (_ed2dc351e9b4038bb73be748a1c7da2a_t3572811411 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._ed2dc351e9b4038bb73be748a1c7da2a::_ed2dc351e9b4038bb73be748a1c7da2am2(System.Int32)
extern "C"  int32_t _ed2dc351e9b4038bb73be748a1c7da2a__ed2dc351e9b4038bb73be748a1c7da2am2_m1912893625 (_ed2dc351e9b4038bb73be748a1c7da2a_t3572811411 * __this, int32_t ____ed2dc351e9b4038bb73be748a1c7da2aa0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._ed2dc351e9b4038bb73be748a1c7da2a::_ed2dc351e9b4038bb73be748a1c7da2am(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _ed2dc351e9b4038bb73be748a1c7da2a__ed2dc351e9b4038bb73be748a1c7da2am_m3370351133 (_ed2dc351e9b4038bb73be748a1c7da2a_t3572811411 * __this, int32_t ____ed2dc351e9b4038bb73be748a1c7da2aa0, int32_t ____ed2dc351e9b4038bb73be748a1c7da2a391, int32_t ____ed2dc351e9b4038bb73be748a1c7da2ac2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
