﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.RVO.RVOController
struct RVOController_t1639282485;
// System.Object
struct Il2CppObject;
// Pathfinding.RVO.IAgent
struct IAgent_t2802767396;
// System.Collections.Generic.List`1<Pathfinding.RVO.ObstacleVertex>
struct List_1_t1243525355;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_Pathfinding_RVO_RVOLayer1386980398.h"
#include "AssemblyU2DCSharp_Pathfinding_RVO_RVOController1639282485.h"

// System.Void Pathfinding.RVO.RVOController::.ctor()
extern "C"  void RVOController__ctor_m856738833 (RVOController_t1639282485 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RVO.RVOController::.cctor()
extern "C"  void RVOController__cctor_m307003836 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Pathfinding.RVO.RVOController::get_position()
extern "C"  Vector3_t4282066566  RVOController_get_position_m171650347 (RVOController_t1639282485 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Pathfinding.RVO.RVOController::get_velocity()
extern "C"  Vector3_t4282066566  RVOController_get_velocity_m1115993055 (RVOController_t1639282485 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RVO.RVOController::OnDisable()
extern "C"  void RVOController_OnDisable_m1958850552 (RVOController_t1639282485 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RVO.RVOController::Awake()
extern "C"  void RVOController_Awake_m1094344052 (RVOController_t1639282485 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RVO.RVOController::OnEnable()
extern "C"  void RVOController_OnEnable_m187512277 (RVOController_t1639282485 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RVO.RVOController::UpdateAgentProperties()
extern "C"  void RVOController_UpdateAgentProperties_m2794187646 (RVOController_t1639282485 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RVO.RVOController::Move(UnityEngine.Vector3)
extern "C"  void RVOController_Move_m1188339381 (RVOController_t1639282485 * __this, Vector3_t4282066566  ___vel0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RVO.RVOController::Teleport(UnityEngine.Vector3)
extern "C"  void RVOController_Teleport_m4226242715 (RVOController_t1639282485 * __this, Vector3_t4282066566  ___pos0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RVO.RVOController::Update()
extern "C"  void RVOController_Update_m2515962140 (RVOController_t1639282485 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RVO.RVOController::OnDrawGizmos()
extern "C"  void RVOController_OnDrawGizmos_m2024783855 (RVOController_t1639282485 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RVO.RVOController::ilo_Log1(System.Object,System.Boolean)
extern "C"  void RVOController_ilo_Log1_m4140849436 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Pathfinding.RVO.RVOController::ilo_get_Position2(Pathfinding.RVO.IAgent)
extern "C"  Vector3_t4282066566  RVOController_ilo_get_Position2_m1453070061 (Il2CppObject * __this /* static, unused */, Il2CppObject * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RVO.RVOController::ilo_set_Radius3(Pathfinding.RVO.IAgent,System.Single)
extern "C"  void RVOController_ilo_set_Radius3_m2435857756 (Il2CppObject * __this /* static, unused */, Il2CppObject * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RVO.RVOController::ilo_set_NeighbourDist4(Pathfinding.RVO.IAgent,System.Single)
extern "C"  void RVOController_ilo_set_NeighbourDist4_m973757808 (Il2CppObject * __this /* static, unused */, Il2CppObject * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RVO.RVOController::ilo_set_CollidesWith5(Pathfinding.RVO.IAgent,Pathfinding.RVO.RVOLayer)
extern "C"  void RVOController_ilo_set_CollidesWith5_m326264259 (Il2CppObject * __this /* static, unused */, Il2CppObject * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RVO.RVOController::ilo_Teleport6(Pathfinding.RVO.IAgent,UnityEngine.Vector3)
extern "C"  void RVOController_ilo_Teleport6_m3631318641 (Il2CppObject * __this /* static, unused */, Il2CppObject * ____this0, Vector3_t4282066566  ___pos1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RVO.RVOController::ilo_UpdateAgentProperties7(Pathfinding.RVO.RVOController)
extern "C"  void RVOController_ilo_UpdateAgentProperties7_m1455060554 (Il2CppObject * __this /* static, unused */, RVOController_t1639282485 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RVO.RVOController::ilo_SetYPosition8(Pathfinding.RVO.IAgent,System.Single)
extern "C"  void RVOController_ilo_SetYPosition8_m1603976784 (Il2CppObject * __this /* static, unused */, Il2CppObject * ____this0, float ___yCoordinate1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<Pathfinding.RVO.ObstacleVertex> Pathfinding.RVO.RVOController::ilo_get_NeighbourObstacles9(Pathfinding.RVO.IAgent)
extern "C"  List_1_t1243525355 * RVOController_ilo_get_NeighbourObstacles9_m3342374031 (Il2CppObject * __this /* static, unused */, Il2CppObject * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Pathfinding.RVO.RVOController::ilo_NearestPointStrict10(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  RVOController_ilo_NearestPointStrict10_m2742956249 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___lineStart0, Vector3_t4282066566  ___lineEnd1, Vector3_t4282066566  ___point2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Pathfinding.RVO.RVOController::ilo_get_velocity11(Pathfinding.RVO.RVOController)
extern "C"  Vector3_t4282066566  RVOController_ilo_get_velocity11_m1421601510 (Il2CppObject * __this /* static, unused */, RVOController_t1639282485 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
