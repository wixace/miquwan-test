﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Utilities.CollectionUtils/<CreateListWrapper>c__AnonStorey138
struct U3CCreateListWrapperU3Ec__AnonStorey138_t173782235;
// System.Object
struct Il2CppObject;
// System.Type
struct Type_t;
// System.Collections.Generic.IList`1<System.Object>
struct IList_1_t2570496278;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Type2863145774.h"

// System.Void Newtonsoft.Json.Utilities.CollectionUtils/<CreateListWrapper>c__AnonStorey138::.ctor()
extern "C"  void U3CCreateListWrapperU3Ec__AnonStorey138__ctor_m4225872624 (U3CCreateListWrapperU3Ec__AnonStorey138_t173782235 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Utilities.CollectionUtils/<CreateListWrapper>c__AnonStorey138::<>m__394(System.Type,System.Collections.Generic.IList`1<System.Object>)
extern "C"  Il2CppObject * U3CCreateListWrapperU3Ec__AnonStorey138_U3CU3Em__394_m2085382800 (U3CCreateListWrapperU3Ec__AnonStorey138_t173782235 * __this, Type_t * ___t0, Il2CppObject* ___a1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
