﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Gizmos
struct Gizmos_t2849394813;
// UnityEngine.Mesh
struct Mesh_t4241756145;
// System.String
struct String_t;
// UnityEngine.Texture
struct Texture_t2526458961;
// UnityEngine.Material
struct Material_t3870600107;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Ray3134616544.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_Mesh4241756145.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882.h"
#include "mscorlib_System_String7231557.h"
#include "UnityEngine_UnityEngine_Rect4241904616.h"
#include "UnityEngine_UnityEngine_Texture2526458961.h"
#include "UnityEngine_UnityEngine_Material3870600107.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"
#include "UnityEngine_UnityEngine_Matrix4x41651859333.h"

// System.Void UnityEngine.Gizmos::.ctor()
extern "C"  void Gizmos__ctor_m2892293076 (Gizmos_t2849394813 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gizmos::DrawRay(UnityEngine.Ray)
extern "C"  void Gizmos_DrawRay_m926484423 (Il2CppObject * __this /* static, unused */, Ray_t3134616544  ___r0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gizmos::DrawRay(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  void Gizmos_DrawRay_m3654665332 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___from0, Vector3_t4282066566  ___direction1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gizmos::DrawLine(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  void Gizmos_DrawLine_m4199765284 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___from0, Vector3_t4282066566  ___to1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gizmos::INTERNAL_CALL_DrawLine(UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C"  void Gizmos_INTERNAL_CALL_DrawLine_m2181283263 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566 * ___from0, Vector3_t4282066566 * ___to1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gizmos::DrawWireSphere(UnityEngine.Vector3,System.Single)
extern "C"  void Gizmos_DrawWireSphere_m2407913464 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___center0, float ___radius1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gizmos::INTERNAL_CALL_DrawWireSphere(UnityEngine.Vector3&,System.Single)
extern "C"  void Gizmos_INTERNAL_CALL_DrawWireSphere_m1463220413 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566 * ___center0, float ___radius1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gizmos::DrawSphere(UnityEngine.Vector3,System.Single)
extern "C"  void Gizmos_DrawSphere_m3958783357 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___center0, float ___radius1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gizmos::INTERNAL_CALL_DrawSphere(UnityEngine.Vector3&,System.Single)
extern "C"  void Gizmos_INTERNAL_CALL_DrawSphere_m3742759768 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566 * ___center0, float ___radius1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gizmos::DrawWireCube(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  void Gizmos_DrawWireCube_m3014140670 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___center0, Vector3_t4282066566  ___size1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gizmos::INTERNAL_CALL_DrawWireCube(UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C"  void Gizmos_INTERNAL_CALL_DrawWireCube_m3575153817 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566 * ___center0, Vector3_t4282066566 * ___size1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gizmos::DrawCube(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  void Gizmos_DrawCube_m4004527619 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___center0, Vector3_t4282066566  ___size1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gizmos::INTERNAL_CALL_DrawCube(UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C"  void Gizmos_INTERNAL_CALL_DrawCube_m3536448222 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566 * ___center0, Vector3_t4282066566 * ___size1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gizmos::DrawMesh(UnityEngine.Mesh,UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C"  void Gizmos_DrawMesh_m1982014305 (Il2CppObject * __this /* static, unused */, Mesh_t4241756145 * ___mesh0, Vector3_t4282066566  ___position1, Quaternion_t1553702882  ___rotation2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gizmos::DrawMesh(UnityEngine.Mesh,UnityEngine.Vector3)
extern "C"  void Gizmos_DrawMesh_m868156016 (Il2CppObject * __this /* static, unused */, Mesh_t4241756145 * ___mesh0, Vector3_t4282066566  ___position1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gizmos::DrawMesh(UnityEngine.Mesh)
extern "C"  void Gizmos_DrawMesh_m3503877469 (Il2CppObject * __this /* static, unused */, Mesh_t4241756145 * ___mesh0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gizmos::DrawMesh(UnityEngine.Mesh,UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Vector3)
extern "C"  void Gizmos_DrawMesh_m3325431412 (Il2CppObject * __this /* static, unused */, Mesh_t4241756145 * ___mesh0, Vector3_t4282066566  ___position1, Quaternion_t1553702882  ___rotation2, Vector3_t4282066566  ___scale3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gizmos::DrawMesh(UnityEngine.Mesh,System.Int32,UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Vector3)
extern "C"  void Gizmos_DrawMesh_m328677783 (Il2CppObject * __this /* static, unused */, Mesh_t4241756145 * ___mesh0, int32_t ___submeshIndex1, Vector3_t4282066566  ___position2, Quaternion_t1553702882  ___rotation3, Vector3_t4282066566  ___scale4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gizmos::DrawMesh(UnityEngine.Mesh,System.Int32,UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C"  void Gizmos_DrawMesh_m1972436484 (Il2CppObject * __this /* static, unused */, Mesh_t4241756145 * ___mesh0, int32_t ___submeshIndex1, Vector3_t4282066566  ___position2, Quaternion_t1553702882  ___rotation3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gizmos::DrawMesh(UnityEngine.Mesh,System.Int32,UnityEngine.Vector3)
extern "C"  void Gizmos_DrawMesh_m2789450541 (Il2CppObject * __this /* static, unused */, Mesh_t4241756145 * ___mesh0, int32_t ___submeshIndex1, Vector3_t4282066566  ___position2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gizmos::DrawMesh(UnityEngine.Mesh,System.Int32)
extern "C"  void Gizmos_DrawMesh_m2659552922 (Il2CppObject * __this /* static, unused */, Mesh_t4241756145 * ___mesh0, int32_t ___submeshIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gizmos::INTERNAL_CALL_DrawMesh(UnityEngine.Mesh,System.Int32,UnityEngine.Vector3&,UnityEngine.Quaternion&,UnityEngine.Vector3&)
extern "C"  void Gizmos_INTERNAL_CALL_DrawMesh_m3193242848 (Il2CppObject * __this /* static, unused */, Mesh_t4241756145 * ___mesh0, int32_t ___submeshIndex1, Vector3_t4282066566 * ___position2, Quaternion_t1553702882 * ___rotation3, Vector3_t4282066566 * ___scale4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gizmos::DrawWireMesh(UnityEngine.Mesh,UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C"  void Gizmos_DrawWireMesh_m2923019484 (Il2CppObject * __this /* static, unused */, Mesh_t4241756145 * ___mesh0, Vector3_t4282066566  ___position1, Quaternion_t1553702882  ___rotation2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gizmos::DrawWireMesh(UnityEngine.Mesh,UnityEngine.Vector3)
extern "C"  void Gizmos_DrawWireMesh_m35829589 (Il2CppObject * __this /* static, unused */, Mesh_t4241756145 * ___mesh0, Vector3_t4282066566  ___position1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gizmos::DrawWireMesh(UnityEngine.Mesh)
extern "C"  void Gizmos_DrawWireMesh_m4228531906 (Il2CppObject * __this /* static, unused */, Mesh_t4241756145 * ___mesh0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gizmos::DrawWireMesh(UnityEngine.Mesh,UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Vector3)
extern "C"  void Gizmos_DrawWireMesh_m2407799919 (Il2CppObject * __this /* static, unused */, Mesh_t4241756145 * ___mesh0, Vector3_t4282066566  ___position1, Quaternion_t1553702882  ___rotation2, Vector3_t4282066566  ___scale3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gizmos::DrawWireMesh(UnityEngine.Mesh,System.Int32,UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Vector3)
extern "C"  void Gizmos_DrawWireMesh_m280689532 (Il2CppObject * __this /* static, unused */, Mesh_t4241756145 * ___mesh0, int32_t ___submeshIndex1, Vector3_t4282066566  ___position2, Quaternion_t1553702882  ___rotation3, Vector3_t4282066566  ___scale4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gizmos::DrawWireMesh(UnityEngine.Mesh,System.Int32,UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C"  void Gizmos_DrawWireMesh_m2756371561 (Il2CppObject * __this /* static, unused */, Mesh_t4241756145 * ___mesh0, int32_t ___submeshIndex1, Vector3_t4282066566  ___position2, Quaternion_t1553702882  ___rotation3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gizmos::DrawWireMesh(UnityEngine.Mesh,System.Int32,UnityEngine.Vector3)
extern "C"  void Gizmos_DrawWireMesh_m2380909160 (Il2CppObject * __this /* static, unused */, Mesh_t4241756145 * ___mesh0, int32_t ___submeshIndex1, Vector3_t4282066566  ___position2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gizmos::DrawWireMesh(UnityEngine.Mesh,System.Int32)
extern "C"  void Gizmos_DrawWireMesh_m579690837 (Il2CppObject * __this /* static, unused */, Mesh_t4241756145 * ___mesh0, int32_t ___submeshIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gizmos::INTERNAL_CALL_DrawWireMesh(UnityEngine.Mesh,System.Int32,UnityEngine.Vector3&,UnityEngine.Quaternion&,UnityEngine.Vector3&)
extern "C"  void Gizmos_INTERNAL_CALL_DrawWireMesh_m3518047323 (Il2CppObject * __this /* static, unused */, Mesh_t4241756145 * ___mesh0, int32_t ___submeshIndex1, Vector3_t4282066566 * ___position2, Quaternion_t1553702882 * ___rotation3, Vector3_t4282066566 * ___scale4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gizmos::DrawIcon(UnityEngine.Vector3,System.String,System.Boolean)
extern "C"  void Gizmos_DrawIcon_m874262677 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___center0, String_t* ___name1, bool ___allowScaling2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gizmos::DrawIcon(UnityEngine.Vector3,System.String)
extern "C"  void Gizmos_DrawIcon_m3933771240 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___center0, String_t* ___name1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gizmos::INTERNAL_CALL_DrawIcon(UnityEngine.Vector3&,System.String,System.Boolean)
extern "C"  void Gizmos_INTERNAL_CALL_DrawIcon_m3074702914 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566 * ___center0, String_t* ___name1, bool ___allowScaling2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gizmos::DrawGUITexture(UnityEngine.Rect,UnityEngine.Texture)
extern "C"  void Gizmos_DrawGUITexture_m3601965921 (Il2CppObject * __this /* static, unused */, Rect_t4241904616  ___screenRect0, Texture_t2526458961 * ___texture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gizmos::DrawGUITexture(UnityEngine.Rect,UnityEngine.Texture,UnityEngine.Material)
extern "C"  void Gizmos_DrawGUITexture_m2516109223 (Il2CppObject * __this /* static, unused */, Rect_t4241904616  ___screenRect0, Texture_t2526458961 * ___texture1, Material_t3870600107 * ___mat2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gizmos::DrawGUITexture(UnityEngine.Rect,UnityEngine.Texture,System.Int32,System.Int32,System.Int32,System.Int32,UnityEngine.Material)
extern "C"  void Gizmos_DrawGUITexture_m2291956583 (Il2CppObject * __this /* static, unused */, Rect_t4241904616  ___screenRect0, Texture_t2526458961 * ___texture1, int32_t ___leftBorder2, int32_t ___rightBorder3, int32_t ___topBorder4, int32_t ___bottomBorder5, Material_t3870600107 * ___mat6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gizmos::DrawGUITexture(UnityEngine.Rect,UnityEngine.Texture,System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  void Gizmos_DrawGUITexture_m1626150817 (Il2CppObject * __this /* static, unused */, Rect_t4241904616  ___screenRect0, Texture_t2526458961 * ___texture1, int32_t ___leftBorder2, int32_t ___rightBorder3, int32_t ___topBorder4, int32_t ___bottomBorder5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gizmos::INTERNAL_CALL_DrawGUITexture(UnityEngine.Rect&,UnityEngine.Texture,System.Int32,System.Int32,System.Int32,System.Int32,UnityEngine.Material)
extern "C"  void Gizmos_INTERNAL_CALL_DrawGUITexture_m394788716 (Il2CppObject * __this /* static, unused */, Rect_t4241904616 * ___screenRect0, Texture_t2526458961 * ___texture1, int32_t ___leftBorder2, int32_t ___rightBorder3, int32_t ___topBorder4, int32_t ___bottomBorder5, Material_t3870600107 * ___mat6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.Gizmos::get_color()
extern "C"  Color_t4194546905  Gizmos_get_color_m1497463653 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gizmos::set_color(UnityEngine.Color)
extern "C"  void Gizmos_set_color_m3649224910 (Il2CppObject * __this /* static, unused */, Color_t4194546905  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gizmos::INTERNAL_get_color(UnityEngine.Color&)
extern "C"  void Gizmos_INTERNAL_get_color_m3213269294 (Il2CppObject * __this /* static, unused */, Color_t4194546905 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gizmos::INTERNAL_set_color(UnityEngine.Color&)
extern "C"  void Gizmos_INTERNAL_set_color_m2940555066 (Il2CppObject * __this /* static, unused */, Color_t4194546905 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Matrix4x4 UnityEngine.Gizmos::get_matrix()
extern "C"  Matrix4x4_t1651859333  Gizmos_get_matrix_m2681280181 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gizmos::set_matrix(UnityEngine.Matrix4x4)
extern "C"  void Gizmos_set_matrix_m3443030764 (Il2CppObject * __this /* static, unused */, Matrix4x4_t1651859333  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gizmos::INTERNAL_get_matrix(UnityEngine.Matrix4x4&)
extern "C"  void Gizmos_INTERNAL_get_matrix_m755787760 (Il2CppObject * __this /* static, unused */, Matrix4x4_t1651859333 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gizmos::INTERNAL_set_matrix(UnityEngine.Matrix4x4&)
extern "C"  void Gizmos_INTERNAL_set_matrix_m176789860 (Il2CppObject * __this /* static, unused */, Matrix4x4_t1651859333 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gizmos::DrawFrustum(UnityEngine.Vector3,System.Single,System.Single,System.Single,System.Single)
extern "C"  void Gizmos_DrawFrustum_m1806993853 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___center0, float ___fov1, float ___maxRange2, float ___minRange3, float ___aspect4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gizmos::INTERNAL_CALL_DrawFrustum(UnityEngine.Vector3&,System.Single,System.Single,System.Single,System.Single)
extern "C"  void Gizmos_INTERNAL_CALL_DrawFrustum_m1743830712 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566 * ___center0, float ___fov1, float ___maxRange2, float ___minRange3, float ___aspect4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
