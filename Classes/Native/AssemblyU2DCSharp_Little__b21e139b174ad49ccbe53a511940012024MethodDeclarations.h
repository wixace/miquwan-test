﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._b21e139b174ad49ccbe53a5134523e71
struct _b21e139b174ad49ccbe53a5134523e71_t1940012024;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._b21e139b174ad49ccbe53a5134523e71::.ctor()
extern "C"  void _b21e139b174ad49ccbe53a5134523e71__ctor_m3533234485 (_b21e139b174ad49ccbe53a5134523e71_t1940012024 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._b21e139b174ad49ccbe53a5134523e71::_b21e139b174ad49ccbe53a5134523e71m2(System.Int32)
extern "C"  int32_t _b21e139b174ad49ccbe53a5134523e71__b21e139b174ad49ccbe53a5134523e71m2_m2506215065 (_b21e139b174ad49ccbe53a5134523e71_t1940012024 * __this, int32_t ____b21e139b174ad49ccbe53a5134523e71a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._b21e139b174ad49ccbe53a5134523e71::_b21e139b174ad49ccbe53a5134523e71m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _b21e139b174ad49ccbe53a5134523e71__b21e139b174ad49ccbe53a5134523e71m_m3380705341 (_b21e139b174ad49ccbe53a5134523e71_t1940012024 * __this, int32_t ____b21e139b174ad49ccbe53a5134523e71a0, int32_t ____b21e139b174ad49ccbe53a5134523e71501, int32_t ____b21e139b174ad49ccbe53a5134523e71c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
