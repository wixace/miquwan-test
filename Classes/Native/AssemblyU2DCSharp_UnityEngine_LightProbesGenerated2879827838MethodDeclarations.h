﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_LightProbesGenerated
struct UnityEngine_LightProbesGenerated_t2879827838;
// JSVCall
struct JSVCall_t3708497963;
// UnityEngine.Rendering.SphericalHarmonicsL2[]
struct SphericalHarmonicsL2U5BU5D_t3690964838;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"

// System.Void UnityEngine_LightProbesGenerated::.ctor()
extern "C"  void UnityEngine_LightProbesGenerated__ctor_m3134837981 (UnityEngine_LightProbesGenerated_t2879827838 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_LightProbesGenerated::LightProbes_LightProbes1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_LightProbesGenerated_LightProbes_LightProbes1_m530849289 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_LightProbesGenerated::LightProbes_positions(JSVCall)
extern "C"  void UnityEngine_LightProbesGenerated_LightProbes_positions_m355622056 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_LightProbesGenerated::LightProbes_bakedProbes(JSVCall)
extern "C"  void UnityEngine_LightProbesGenerated_LightProbes_bakedProbes_m2147418692 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_LightProbesGenerated::LightProbes_count(JSVCall)
extern "C"  void UnityEngine_LightProbesGenerated_LightProbes_count_m387188643 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_LightProbesGenerated::LightProbes_cellCount(JSVCall)
extern "C"  void UnityEngine_LightProbesGenerated_LightProbes_cellCount_m1406026661 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_LightProbesGenerated::LightProbes_GetInterpolatedProbe__Vector3__Renderer__SphericalHarmonicsL2(JSVCall,System.Int32)
extern "C"  bool UnityEngine_LightProbesGenerated_LightProbes_GetInterpolatedProbe__Vector3__Renderer__SphericalHarmonicsL2_m1648648540 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_LightProbesGenerated::__Register()
extern "C"  void UnityEngine_LightProbesGenerated___Register_m3337300490 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rendering.SphericalHarmonicsL2[] UnityEngine_LightProbesGenerated::<LightProbes_bakedProbes>m__25F()
extern "C"  SphericalHarmonicsL2U5BU5D_t3690964838* UnityEngine_LightProbesGenerated_U3CLightProbes_bakedProbesU3Em__25F_m1982999760 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_LightProbesGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UnityEngine_LightProbesGenerated_ilo_getObject1_m3554609749 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_LightProbesGenerated::ilo_incArgIndex2()
extern "C"  int32_t UnityEngine_LightProbesGenerated_ilo_incArgIndex2_m1515011840 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_LightProbesGenerated::ilo_getArrayLength3(System.Int32)
extern "C"  int32_t UnityEngine_LightProbesGenerated_ilo_getArrayLength3_m133980597 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
