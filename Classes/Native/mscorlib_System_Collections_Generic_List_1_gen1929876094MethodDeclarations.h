﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1244034627MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1<JSCLevelHeroPointConfig>::.ctor()
#define List_1__ctor_m295639501(__this, method) ((  void (*) (List_1_t1929876094 *, const MethodInfo*))List_1__ctor_m1239231859_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<JSCLevelHeroPointConfig>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m3503358043(__this, ___collection0, method) ((  void (*) (List_1_t1929876094 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m1160795371_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<JSCLevelHeroPointConfig>::.ctor(System.Int32)
#define List_1__ctor_m2496441589(__this, ___capacity0, method) ((  void (*) (List_1_t1929876094 *, int32_t, const MethodInfo*))List_1__ctor_m3643386469_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<JSCLevelHeroPointConfig>::.cctor()
#define List_1__cctor_m3996332681(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m3826137881_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<JSCLevelHeroPointConfig>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1026476278(__this, method) ((  Il2CppObject* (*) (List_1_t1929876094 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2808422246_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<JSCLevelHeroPointConfig>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m2215264288(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t1929876094 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m4034025648_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<JSCLevelHeroPointConfig>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m2620594587(__this, method) ((  Il2CppObject * (*) (List_1_t1929876094 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m1841330603_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<JSCLevelHeroPointConfig>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m3088460982(__this, ___item0, method) ((  int32_t (*) (List_1_t1929876094 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m3794749222_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<JSCLevelHeroPointConfig>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m2598688086(__this, ___item0, method) ((  bool (*) (List_1_t1929876094 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m2659633254_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<JSCLevelHeroPointConfig>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m3798276110(__this, ___item0, method) ((  int32_t (*) (List_1_t1929876094 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m3431692926_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<JSCLevelHeroPointConfig>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m4065144889(__this, ___index0, ___item1, method) ((  void (*) (List_1_t1929876094 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m2067529129_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<JSCLevelHeroPointConfig>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m2417987663(__this, ___item0, method) ((  void (*) (List_1_t1929876094 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m1644145887_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<JSCLevelHeroPointConfig>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m991859159(__this, method) ((  bool (*) (List_1_t1929876094 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1299706087_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<JSCLevelHeroPointConfig>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m726235718(__this, method) ((  bool (*) (List_1_t1929876094 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m3867536694_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<JSCLevelHeroPointConfig>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m758745202(__this, method) ((  Il2CppObject * (*) (List_1_t1929876094 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m4244374434_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<JSCLevelHeroPointConfig>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m87342405(__this, method) ((  bool (*) (List_1_t1929876094 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m432946261_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<JSCLevelHeroPointConfig>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m2396488980(__this, method) ((  bool (*) (List_1_t1929876094 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m2961826820_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<JSCLevelHeroPointConfig>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m75310457(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t1929876094 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m3985478825_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<JSCLevelHeroPointConfig>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m3092918736(__this, ___index0, ___value1, method) ((  void (*) (List_1_t1929876094 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m3234554688_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<JSCLevelHeroPointConfig>::Add(T)
#define List_1_Add_m4146753115(__this, ___item0, method) ((  void (*) (List_1_t1929876094 *, JSCLevelHeroPointConfig_t561690542 *, const MethodInfo*))List_1_Add_m933592675_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<JSCLevelHeroPointConfig>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m2646204694(__this, ___newCount0, method) ((  void (*) (List_1_t1929876094 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m4122600870_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<JSCLevelHeroPointConfig>::CheckRange(System.Int32,System.Int32)
#define List_1_CheckRange_m1622006033(__this, ___idx0, ___count1, method) ((  void (*) (List_1_t1929876094 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m590371457_gshared)(__this, ___idx0, ___count1, method)
// System.Void System.Collections.Generic.List`1<JSCLevelHeroPointConfig>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m326131732(__this, ___collection0, method) ((  void (*) (List_1_t1929876094 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m2478449828_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<JSCLevelHeroPointConfig>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m3882071252(__this, ___enumerable0, method) ((  void (*) (List_1_t1929876094 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m1739422052_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<JSCLevelHeroPointConfig>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m590120259(__this, ___collection0, method) ((  void (*) (List_1_t1929876094 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m2229151411_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<JSCLevelHeroPointConfig>::AsReadOnly()
#define List_1_AsReadOnly_m1819803398(__this, method) ((  ReadOnlyCollection_1_t2118768078 * (*) (List_1_t1929876094 *, const MethodInfo*))List_1_AsReadOnly_m769820182_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<JSCLevelHeroPointConfig>::BinarySearch(T)
#define List_1_BinarySearch_m1070249465(__this, ___item0, method) ((  int32_t (*) (List_1_t1929876094 *, JSCLevelHeroPointConfig_t561690542 *, const MethodInfo*))List_1_BinarySearch_m2761349225_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<JSCLevelHeroPointConfig>::Clear()
#define List_1_Clear_m1707018703(__this, method) ((  void (*) (List_1_t1929876094 *, const MethodInfo*))List_1_Clear_m2940332446_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<JSCLevelHeroPointConfig>::Contains(T)
#define List_1_Contains_m606376445(__this, ___item0, method) ((  bool (*) (List_1_t1929876094 *, JSCLevelHeroPointConfig_t561690542 *, const MethodInfo*))List_1_Contains_m4186092781_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<JSCLevelHeroPointConfig>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m1600230795(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t1929876094 *, JSCLevelHeroPointConfigU5BU5D_t3022208475*, int32_t, const MethodInfo*))List_1_CopyTo_m3988356635_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<JSCLevelHeroPointConfig>::Find(System.Predicate`1<T>)
#define List_1_Find_m677076285(__this, ___match0, method) ((  JSCLevelHeroPointConfig_t561690542 * (*) (List_1_t1929876094 *, Predicate_1_t172747425 *, const MethodInfo*))List_1_Find_m3379773421_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<JSCLevelHeroPointConfig>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m783848888(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t172747425 *, const MethodInfo*))List_1_CheckMatch_m3390394152_gshared)(__this /* static, unused */, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<JSCLevelHeroPointConfig>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m1539067869(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t1929876094 *, int32_t, int32_t, Predicate_1_t172747425 *, const MethodInfo*))List_1_GetIndex_m4275988045_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<JSCLevelHeroPointConfig>::GetEnumerator()
#define List_1_GetEnumerator_m3434561722(__this, method) ((  Enumerator_t1949548864  (*) (List_1_t1929876094 *, const MethodInfo*))List_1_GetEnumerator_m1919240000_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<JSCLevelHeroPointConfig>::IndexOf(T)
#define List_1_IndexOf_m3918393167(__this, ___item0, method) ((  int32_t (*) (List_1_t1929876094 *, JSCLevelHeroPointConfig_t561690542 *, const MethodInfo*))List_1_IndexOf_m2776798407_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<JSCLevelHeroPointConfig>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m813478114(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t1929876094 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m3807054194_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<JSCLevelHeroPointConfig>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m1346597979(__this, ___index0, method) ((  void (*) (List_1_t1929876094 *, int32_t, const MethodInfo*))List_1_CheckIndex_m3734723819_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<JSCLevelHeroPointConfig>::Insert(System.Int32,T)
#define List_1_Insert_m1257083586(__this, ___index0, ___item1, method) ((  void (*) (List_1_t1929876094 *, int32_t, JSCLevelHeroPointConfig_t561690542 *, const MethodInfo*))List_1_Insert_m3427163986_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<JSCLevelHeroPointConfig>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m406650295(__this, ___collection0, method) ((  void (*) (List_1_t1929876094 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m2905071175_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<JSCLevelHeroPointConfig>::Remove(T)
#define List_1_Remove_m2623515960(__this, ___item0, method) ((  bool (*) (List_1_t1929876094 *, JSCLevelHeroPointConfig_t561690542 *, const MethodInfo*))List_1_Remove_m2747911208_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<JSCLevelHeroPointConfig>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m38388178(__this, ___match0, method) ((  int32_t (*) (List_1_t1929876094 *, Predicate_1_t172747425 *, const MethodInfo*))List_1_RemoveAll_m2933443938_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<JSCLevelHeroPointConfig>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m3425903752(__this, ___index0, method) ((  void (*) (List_1_t1929876094 *, int32_t, const MethodInfo*))List_1_RemoveAt_m2208877785_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<JSCLevelHeroPointConfig>::RemoveRange(System.Int32,System.Int32)
#define List_1_RemoveRange_m2772758699(__this, ___index0, ___count1, method) ((  void (*) (List_1_t1929876094 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m856857915_gshared)(__this, ___index0, ___count1, method)
// System.Void System.Collections.Generic.List`1<JSCLevelHeroPointConfig>::Reverse()
#define List_1_Reverse_m1430153444(__this, method) ((  void (*) (List_1_t1929876094 *, const MethodInfo*))List_1_Reverse_m449081940_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<JSCLevelHeroPointConfig>::Sort()
#define List_1_Sort_m377758014(__this, method) ((  void (*) (List_1_t1929876094 *, const MethodInfo*))List_1_Sort_m1168641486_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<JSCLevelHeroPointConfig>::Sort(System.Collections.Generic.IComparer`1<T>)
#define List_1_Sort_m1945969254(__this, ___comparer0, method) ((  void (*) (List_1_t1929876094 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m3726677974_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<JSCLevelHeroPointConfig>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m2714667793(__this, ___comparison0, method) ((  void (*) (List_1_t1929876094 *, Comparison_1_t3573019025 *, const MethodInfo*))List_1_Sort_m4192185249_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<JSCLevelHeroPointConfig>::ToArray()
#define List_1_ToArray_m4240495139(__this, method) ((  JSCLevelHeroPointConfigU5BU5D_t3022208475* (*) (List_1_t1929876094 *, const MethodInfo*))List_1_ToArray_m1449333879_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<JSCLevelHeroPointConfig>::TrimExcess()
#define List_1_TrimExcess_m2300107351(__this, method) ((  void (*) (List_1_t1929876094 *, const MethodInfo*))List_1_TrimExcess_m2451380967_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<JSCLevelHeroPointConfig>::get_Capacity()
#define List_1_get_Capacity_m1735895039(__this, method) ((  int32_t (*) (List_1_t1929876094 *, const MethodInfo*))List_1_get_Capacity_m543520655_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<JSCLevelHeroPointConfig>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m4151360808(__this, ___value0, method) ((  void (*) (List_1_t1929876094 *, int32_t, const MethodInfo*))List_1_set_Capacity_m1332789688_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<JSCLevelHeroPointConfig>::get_Count()
#define List_1_get_Count_m3105731971(__this, method) ((  int32_t (*) (List_1_t1929876094 *, const MethodInfo*))List_1_get_Count_m3549598589_gshared)(__this, method)
// T System.Collections.Generic.List`1<JSCLevelHeroPointConfig>::get_Item(System.Int32)
#define List_1_get_Item_m2754221490(__this, ___index0, method) ((  JSCLevelHeroPointConfig_t561690542 * (*) (List_1_t1929876094 *, int32_t, const MethodInfo*))List_1_get_Item_m404118264_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<JSCLevelHeroPointConfig>::set_Item(System.Int32,T)
#define List_1_set_Item_m2981112601(__this, ___index0, ___value1, method) ((  void (*) (List_1_t1929876094 *, int32_t, JSCLevelHeroPointConfig_t561690542 *, const MethodInfo*))List_1_set_Item_m1074271145_gshared)(__this, ___index0, ___value1, method)
