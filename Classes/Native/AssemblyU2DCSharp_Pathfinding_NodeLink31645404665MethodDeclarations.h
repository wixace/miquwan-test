﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.NodeLink3
struct NodeLink3_t1645404665;
// Pathfinding.GraphNode
struct GraphNode_t23612370;
// UnityEngine.Transform
struct Transform_t1659122786;
// Pathfinding.AstarData
struct AstarData_t3283402719;
// Pathfinding.NavGraph
struct NavGraph_t1254319713;
// Pathfinding.GraphModifier
struct GraphModifier_t2555428519;
// Pathfinding.MeshNode
struct MeshNode_t3005053445;
// Pathfinding.PointNode
struct PointNode_t2761813780;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphNode23612370.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"
#include "AssemblyU2DCSharp_Pathfinding_NodeLink31645404665.h"
#include "AssemblyU2DCSharp_Pathfinding_AstarData3283402719.h"
#include "AssemblyU2DCSharp_Pathfinding_NavGraph1254319713.h"
#include "AssemblyU2DCSharp_Pathfinding_Int31974045594.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphModifier2555428519.h"
#include "AssemblyU2DCSharp_Pathfinding_MeshNode3005053445.h"
#include "AssemblyU2DCSharp_Pathfinding_PointNode2761813780.h"

// System.Void Pathfinding.NodeLink3::.ctor()
extern "C"  void NodeLink3__ctor_m47232862 (NodeLink3_t1645404665 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NodeLink3::.cctor()
extern "C"  void NodeLink3__cctor_m982122511 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.NodeLink3 Pathfinding.NodeLink3::GetNodeLink(Pathfinding.GraphNode)
extern "C"  NodeLink3_t1645404665 * NodeLink3_GetNodeLink_m976351576 (Il2CppObject * __this /* static, unused */, GraphNode_t23612370 * ___node0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform Pathfinding.NodeLink3::get_StartTransform()
extern "C"  Transform_t1659122786 * NodeLink3_get_StartTransform_m524964539 (NodeLink3_t1645404665 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform Pathfinding.NodeLink3::get_EndTransform()
extern "C"  Transform_t1659122786 * NodeLink3_get_EndTransform_m3890461858 (NodeLink3_t1645404665 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.GraphNode Pathfinding.NodeLink3::get_StartNode()
extern "C"  GraphNode_t23612370 * NodeLink3_get_StartNode_m2702072036 (NodeLink3_t1645404665 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.GraphNode Pathfinding.NodeLink3::get_EndNode()
extern "C"  GraphNode_t23612370 * NodeLink3_get_EndNode_m692742749 (NodeLink3_t1645404665 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NodeLink3::OnPostScan()
extern "C"  void NodeLink3_OnPostScan_m2573101474 (NodeLink3_t1645404665 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NodeLink3::InternalOnPostScan()
extern "C"  void NodeLink3_InternalOnPostScan_m1908349951 (NodeLink3_t1645404665 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NodeLink3::OnGraphsPostUpdate()
extern "C"  void NodeLink3_OnGraphsPostUpdate_m3676041843 (NodeLink3_t1645404665 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NodeLink3::OnEnable()
extern "C"  void NodeLink3_OnEnable_m436497256 (NodeLink3_t1645404665 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NodeLink3::OnDisable()
extern "C"  void NodeLink3_OnDisable_m1087450309 (NodeLink3_t1645404665 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NodeLink3::RemoveConnections(Pathfinding.GraphNode)
extern "C"  void NodeLink3_RemoveConnections_m2109861663 (NodeLink3_t1645404665 * __this, GraphNode_t23612370 * ___node0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NodeLink3::ContextApplyForce()
extern "C"  void NodeLink3_ContextApplyForce_m3778654248 (NodeLink3_t1645404665 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NodeLink3::Apply(System.Boolean)
extern "C"  void NodeLink3_Apply_m328478817 (NodeLink3_t1645404665 * __this, bool ___forceNewCheck0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NodeLink3::DrawCircle(UnityEngine.Vector3,System.Single,System.Int32,UnityEngine.Color)
extern "C"  void NodeLink3_DrawCircle_m1686378835 (NodeLink3_t1645404665 * __this, Vector3_t4282066566  ___o0, float ___r1, int32_t ___detail2, Color_t4194546905  ___col3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NodeLink3::DrawGizmoBezier(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  void NodeLink3_DrawGizmoBezier_m1447287099 (NodeLink3_t1645404665 * __this, Vector3_t4282066566  ___p10, Vector3_t4282066566  ___p21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NodeLink3::OnDrawGizmosSelected()
extern "C"  void NodeLink3_OnDrawGizmosSelected_m2550593277 (NodeLink3_t1645404665 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NodeLink3::OnDrawGizmos()
extern "C"  void NodeLink3_OnDrawGizmos_m922481666 (NodeLink3_t1645404665 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NodeLink3::OnDrawGizmos(System.Boolean)
extern "C"  void NodeLink3_OnDrawGizmos_m3139337721 (NodeLink3_t1645404665 * __this, bool ___selected0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.NodeLink3::<OnPostScan>m__33E(System.Boolean)
extern "C"  bool NodeLink3_U3COnPostScanU3Em__33E_m1748514023 (NodeLink3_t1645404665 * __this, bool ___force0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NodeLink3::ilo_InternalOnPostScan1(Pathfinding.NodeLink3)
extern "C"  void NodeLink3_ilo_InternalOnPostScan1_m3343348836 (Il2CppObject * __this /* static, unused */, NodeLink3_t1645404665 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NodeLink3::ilo_AddGraph2(Pathfinding.AstarData,Pathfinding.NavGraph)
extern "C"  void NodeLink3_ilo_AddGraph2_m1130701248 (Il2CppObject * __this /* static, unused */, AstarData_t3283402719 * ____this0, NavGraph_t1254319713 * ___graph1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform Pathfinding.NodeLink3::ilo_get_StartTransform3(Pathfinding.NodeLink3)
extern "C"  Transform_t1659122786 * NodeLink3_ilo_get_StartTransform3_m2325828318 (Il2CppObject * __this /* static, unused */, NodeLink3_t1645404665 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Int3 Pathfinding.NodeLink3::ilo_op_Explicit4(UnityEngine.Vector3)
extern "C"  Int3_t1974045594  NodeLink3_ilo_op_Explicit4_m679497773 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___ob0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NodeLink3::ilo_Apply5(Pathfinding.NodeLink3,System.Boolean)
extern "C"  void NodeLink3_ilo_Apply5_m4176402840 (Il2CppObject * __this /* static, unused */, NodeLink3_t1645404665 * ____this0, bool ___forceNewCheck1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NodeLink3::ilo_OnDisable6(Pathfinding.GraphModifier)
extern "C"  void NodeLink3_ilo_OnDisable6_m4131221841 (Il2CppObject * __this /* static, unused */, GraphModifier_t2555428519 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NodeLink3::ilo_RemoveConnection7(Pathfinding.MeshNode,Pathfinding.GraphNode)
extern "C"  void NodeLink3_ilo_RemoveConnection7_m3900125291 (Il2CppObject * __this /* static, unused */, MeshNode_t3005053445 * ____this0, GraphNode_t23612370 * ___node1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NodeLink3::ilo_ClearConnections8(Pathfinding.GraphNode,System.Boolean)
extern "C"  void NodeLink3_ilo_ClearConnections8_m3192902762 (Il2CppObject * __this /* static, unused */, GraphNode_t23612370 * ____this0, bool ___alsoReverse1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform Pathfinding.NodeLink3::ilo_get_EndTransform9(Pathfinding.NodeLink3)
extern "C"  Transform_t1659122786 * NodeLink3_ilo_get_EndTransform9_m2063777663 (Il2CppObject * __this /* static, unused */, NodeLink3_t1645404665 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NodeLink3::ilo_SetPosition10(Pathfinding.PointNode,Pathfinding.Int3)
extern "C"  void NodeLink3_ilo_SetPosition10_m2324014849 (Il2CppObject * __this /* static, unused */, PointNode_t2761813780 * ____this0, Int3_t1974045594  ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NodeLink3::ilo_RemoveConnections11(Pathfinding.NodeLink3,Pathfinding.GraphNode)
extern "C"  void NodeLink3_ilo_RemoveConnections11_m201842321 (Il2CppObject * __this /* static, unused */, NodeLink3_t1645404665 * ____this0, GraphNode_t23612370 * ___node1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Int3 Pathfinding.NodeLink3::ilo_GetVertex12(Pathfinding.MeshNode,System.Int32)
extern "C"  Int3_t1974045594  NodeLink3_ilo_GetVertex12_m4290241693 (Il2CppObject * __this /* static, unused */, MeshNode_t3005053445 * ____this0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pathfinding.NodeLink3::ilo_GetVertexCount13(Pathfinding.MeshNode)
extern "C"  int32_t NodeLink3_ilo_GetVertexCount13_m3905576937 (Il2CppObject * __this /* static, unused */, MeshNode_t3005053445 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Int3 Pathfinding.NodeLink3::ilo_op_Subtraction14(Pathfinding.Int3,Pathfinding.Int3)
extern "C"  Int3_t1974045594  NodeLink3_ilo_op_Subtraction14_m2080834851 (Il2CppObject * __this /* static, unused */, Int3_t1974045594  ___lhs0, Int3_t1974045594  ___rhs1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Int3 Pathfinding.NodeLink3::ilo_Normal2D15(Pathfinding.Int3&)
extern "C"  Int3_t1974045594  NodeLink3_ilo_Normal2D15_m3324587475 (Il2CppObject * __this /* static, unused */, Int3_t1974045594 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Pathfinding.NodeLink3::ilo_op_Explicit16(Pathfinding.Int3)
extern "C"  Vector3_t4282066566  NodeLink3_ilo_op_Explicit16_m2180133580 (Il2CppObject * __this /* static, unused */, Int3_t1974045594  ___ob0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pathfinding.NodeLink3::ilo_get_costMagnitude17(Pathfinding.Int3&)
extern "C"  int32_t NodeLink3_ilo_get_costMagnitude17_m40232099 (Il2CppObject * __this /* static, unused */, Int3_t1974045594 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NodeLink3::ilo_AddConnection18(Pathfinding.MeshNode,Pathfinding.GraphNode,System.UInt32)
extern "C"  void NodeLink3_ilo_AddConnection18_m1347882224 (Il2CppObject * __this /* static, unused */, MeshNode_t3005053445 * ____this0, GraphNode_t23612370 * ___node1, uint32_t ___cost2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NodeLink3::ilo_AddConnection19(Pathfinding.PointNode,Pathfinding.GraphNode,System.UInt32)
extern "C"  void NodeLink3_ilo_AddConnection19_m1902520404 (Il2CppObject * __this /* static, unused */, PointNode_t2761813780 * ____this0, GraphNode_t23612370 * ___node1, uint32_t ___cost2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NodeLink3::ilo_OnDrawGizmos20(Pathfinding.NodeLink3,System.Boolean)
extern "C"  void NodeLink3_ilo_OnDrawGizmos20_m268414443 (Il2CppObject * __this /* static, unused */, NodeLink3_t1645404665 * ____this0, bool ___selected1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NodeLink3::ilo_DrawCircle21(Pathfinding.NodeLink3,UnityEngine.Vector3,System.Single,System.Int32,UnityEngine.Color)
extern "C"  void NodeLink3_ilo_DrawCircle21_m3271419526 (Il2CppObject * __this /* static, unused */, NodeLink3_t1645404665 * ____this0, Vector3_t4282066566  ___o1, float ___r2, int32_t ___detail3, Color_t4194546905  ___col4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NodeLink3::ilo_DrawGizmoBezier22(Pathfinding.NodeLink3,UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  void NodeLink3_ilo_DrawGizmoBezier22_m4239186125 (Il2CppObject * __this /* static, unused */, NodeLink3_t1645404665 * ____this0, Vector3_t4282066566  ___p11, Vector3_t4282066566  ___p22, const MethodInfo* method) IL2CPP_METHOD_ATTR;
