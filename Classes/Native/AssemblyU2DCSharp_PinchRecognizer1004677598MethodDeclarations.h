﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PinchRecognizer
struct PinchRecognizer_t1004677598;
// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// PinchGesture
struct PinchGesture_t1502590799;
// FingerGestures/IFingerList
struct IFingerList_t1026994100;
// FingerGestures/Finger
struct Finger_t182428197;
// Gesture
struct Gesture_t1589572905;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PinchGesture1502590799.h"
#include "AssemblyU2DCSharp_GestureResetMode2954327145.h"
#include "AssemblyU2DCSharp_GestureRecognitionState3604239971.h"
#include "AssemblyU2DCSharp_FingerGestures_Finger182428197.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "AssemblyU2DCSharp_Gesture1589572905.h"
#include "AssemblyU2DCSharp_PinchRecognizer1004677598.h"

// System.Void PinchRecognizer::.ctor()
extern "C"  void PinchRecognizer__ctor_m1867765581 (PinchRecognizer_t1004677598 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PinchRecognizer::GetDefaultEventMessageName()
extern "C"  String_t* PinchRecognizer_GetDefaultEventMessageName_m2729946679 (PinchRecognizer_t1004677598 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PinchRecognizer::get_RequiredFingerCount()
extern "C"  int32_t PinchRecognizer_get_RequiredFingerCount_m3201705623 (PinchRecognizer_t1004677598 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PinchRecognizer::set_RequiredFingerCount(System.Int32)
extern "C"  void PinchRecognizer_set_RequiredFingerCount_m2093961190 (PinchRecognizer_t1004677598 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PinchRecognizer::get_SupportFingerClustering()
extern "C"  bool PinchRecognizer_get_SupportFingerClustering_m3329279702 (PinchRecognizer_t1004677598 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject PinchRecognizer::GetDefaultSelectionForSendMessage(PinchGesture)
extern "C"  GameObject_t3674682005 * PinchRecognizer_GetDefaultSelectionForSendMessage_m3620807784 (PinchRecognizer_t1004677598 * __this, PinchGesture_t1502590799 * ___gesture0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GestureResetMode PinchRecognizer::GetDefaultResetMode()
extern "C"  int32_t PinchRecognizer_GetDefaultResetMode_m3360150630 (PinchRecognizer_t1004677598 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PinchRecognizer::CanBegin(PinchGesture,FingerGestures/IFingerList)
extern "C"  bool PinchRecognizer_CanBegin_m606141509 (PinchRecognizer_t1004677598 * __this, PinchGesture_t1502590799 * ___gesture0, Il2CppObject * ___touches1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PinchRecognizer::OnBegin(PinchGesture,FingerGestures/IFingerList)
extern "C"  void PinchRecognizer_OnBegin_m305539916 (PinchRecognizer_t1004677598 * __this, PinchGesture_t1502590799 * ___gesture0, Il2CppObject * ___touches1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GestureRecognitionState PinchRecognizer::OnRecognize(PinchGesture,FingerGestures/IFingerList)
extern "C"  int32_t PinchRecognizer_OnRecognize_m2505971575 (PinchRecognizer_t1004677598 * __this, PinchGesture_t1502590799 * ___gesture0, Il2CppObject * ___touches1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PinchRecognizer::FingersMovedInOppositeDirections(FingerGestures/Finger,FingerGestures/Finger)
extern "C"  bool PinchRecognizer_FingersMovedInOppositeDirections_m3249159944 (PinchRecognizer_t1004677598 * __this, Finger_t182428197 * ___finger00, Finger_t182428197 * ___finger11, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FingerGestures/Finger PinchRecognizer::ilo_get_Item1(FingerGestures/IFingerList,System.Int32)
extern "C"  Finger_t182428197 * PinchRecognizer_ilo_get_Item1_m3161838896 (Il2CppObject * __this /* static, unused */, Il2CppObject * ____this0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 PinchRecognizer::ilo_get_StartPosition2(FingerGestures/Finger)
extern "C"  Vector2_t4282066565  PinchRecognizer_ilo_get_StartPosition2_m4032303118 (Il2CppObject * __this /* static, unused */, Finger_t182428197 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PinchRecognizer::ilo_set_StartPosition3(Gesture,UnityEngine.Vector2)
extern "C"  void PinchRecognizer_ilo_set_StartPosition3_m3769825870 (Il2CppObject * __this /* static, unused */, Gesture_t1589572905 * ____this0, Vector2_t4282066565  ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 PinchRecognizer::ilo_get_Position4(FingerGestures/Finger)
extern "C"  Vector2_t4282066565  PinchRecognizer_ilo_get_Position4_m3001409944 (Il2CppObject * __this /* static, unused */, Finger_t182428197 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PinchRecognizer::ilo_set_Position5(Gesture,UnityEngine.Vector2)
extern "C"  void PinchRecognizer_ilo_set_Position5_m2622380886 (Il2CppObject * __this /* static, unused */, Gesture_t1589572905 * ____this0, Vector2_t4282066565  ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PinchRecognizer::ilo_set_Gap6(PinchGesture,System.Single)
extern "C"  void PinchRecognizer_ilo_set_Gap6_m3134819805 (Il2CppObject * __this /* static, unused */, PinchGesture_t1502590799 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PinchRecognizer::ilo_set_Delta7(PinchGesture,System.Single)
extern "C"  void PinchRecognizer_ilo_set_Delta7_m2133700412 (Il2CppObject * __this /* static, unused */, PinchGesture_t1502590799 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PinchRecognizer::ilo_get_RequiredFingerCount8(PinchRecognizer)
extern "C"  int32_t PinchRecognizer_ilo_get_RequiredFingerCount8_m1515903422 (Il2CppObject * __this /* static, unused */, PinchRecognizer_t1004677598 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
