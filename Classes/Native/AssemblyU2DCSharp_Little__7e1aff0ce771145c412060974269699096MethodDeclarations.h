﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._7e1aff0ce771145c412060970704861f
struct _7e1aff0ce771145c412060970704861f_t4269699096;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._7e1aff0ce771145c412060970704861f::.ctor()
extern "C"  void _7e1aff0ce771145c412060970704861f__ctor_m862277909 (_7e1aff0ce771145c412060970704861f_t4269699096 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._7e1aff0ce771145c412060970704861f::_7e1aff0ce771145c412060970704861fm2(System.Int32)
extern "C"  int32_t _7e1aff0ce771145c412060970704861f__7e1aff0ce771145c412060970704861fm2_m413424281 (_7e1aff0ce771145c412060970704861f_t4269699096 * __this, int32_t ____7e1aff0ce771145c412060970704861fa0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._7e1aff0ce771145c412060970704861f::_7e1aff0ce771145c412060970704861fm(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _7e1aff0ce771145c412060970704861f__7e1aff0ce771145c412060970704861fm_m3992218685 (_7e1aff0ce771145c412060970704861f_t4269699096 * __this, int32_t ____7e1aff0ce771145c412060970704861fa0, int32_t ____7e1aff0ce771145c412060970704861f711, int32_t ____7e1aff0ce771145c412060970704861fc2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
