﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Pathfinding.ClipperLib.OutRec
struct OutRec_t3245805284;
// Pathfinding.ClipperLib.OutPt
struct OutPt_t3947633180;
// Pathfinding.ClipperLib.PolyNode
struct PolyNode_t3336253808;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.ClipperLib.OutRec
struct  OutRec_t3245805284  : public Il2CppObject
{
public:
	// System.Int32 Pathfinding.ClipperLib.OutRec::Idx
	int32_t ___Idx_0;
	// System.Boolean Pathfinding.ClipperLib.OutRec::IsHole
	bool ___IsHole_1;
	// System.Boolean Pathfinding.ClipperLib.OutRec::IsOpen
	bool ___IsOpen_2;
	// Pathfinding.ClipperLib.OutRec Pathfinding.ClipperLib.OutRec::FirstLeft
	OutRec_t3245805284 * ___FirstLeft_3;
	// Pathfinding.ClipperLib.OutPt Pathfinding.ClipperLib.OutRec::Pts
	OutPt_t3947633180 * ___Pts_4;
	// Pathfinding.ClipperLib.OutPt Pathfinding.ClipperLib.OutRec::BottomPt
	OutPt_t3947633180 * ___BottomPt_5;
	// Pathfinding.ClipperLib.PolyNode Pathfinding.ClipperLib.OutRec::PolyNode
	PolyNode_t3336253808 * ___PolyNode_6;

public:
	inline static int32_t get_offset_of_Idx_0() { return static_cast<int32_t>(offsetof(OutRec_t3245805284, ___Idx_0)); }
	inline int32_t get_Idx_0() const { return ___Idx_0; }
	inline int32_t* get_address_of_Idx_0() { return &___Idx_0; }
	inline void set_Idx_0(int32_t value)
	{
		___Idx_0 = value;
	}

	inline static int32_t get_offset_of_IsHole_1() { return static_cast<int32_t>(offsetof(OutRec_t3245805284, ___IsHole_1)); }
	inline bool get_IsHole_1() const { return ___IsHole_1; }
	inline bool* get_address_of_IsHole_1() { return &___IsHole_1; }
	inline void set_IsHole_1(bool value)
	{
		___IsHole_1 = value;
	}

	inline static int32_t get_offset_of_IsOpen_2() { return static_cast<int32_t>(offsetof(OutRec_t3245805284, ___IsOpen_2)); }
	inline bool get_IsOpen_2() const { return ___IsOpen_2; }
	inline bool* get_address_of_IsOpen_2() { return &___IsOpen_2; }
	inline void set_IsOpen_2(bool value)
	{
		___IsOpen_2 = value;
	}

	inline static int32_t get_offset_of_FirstLeft_3() { return static_cast<int32_t>(offsetof(OutRec_t3245805284, ___FirstLeft_3)); }
	inline OutRec_t3245805284 * get_FirstLeft_3() const { return ___FirstLeft_3; }
	inline OutRec_t3245805284 ** get_address_of_FirstLeft_3() { return &___FirstLeft_3; }
	inline void set_FirstLeft_3(OutRec_t3245805284 * value)
	{
		___FirstLeft_3 = value;
		Il2CppCodeGenWriteBarrier(&___FirstLeft_3, value);
	}

	inline static int32_t get_offset_of_Pts_4() { return static_cast<int32_t>(offsetof(OutRec_t3245805284, ___Pts_4)); }
	inline OutPt_t3947633180 * get_Pts_4() const { return ___Pts_4; }
	inline OutPt_t3947633180 ** get_address_of_Pts_4() { return &___Pts_4; }
	inline void set_Pts_4(OutPt_t3947633180 * value)
	{
		___Pts_4 = value;
		Il2CppCodeGenWriteBarrier(&___Pts_4, value);
	}

	inline static int32_t get_offset_of_BottomPt_5() { return static_cast<int32_t>(offsetof(OutRec_t3245805284, ___BottomPt_5)); }
	inline OutPt_t3947633180 * get_BottomPt_5() const { return ___BottomPt_5; }
	inline OutPt_t3947633180 ** get_address_of_BottomPt_5() { return &___BottomPt_5; }
	inline void set_BottomPt_5(OutPt_t3947633180 * value)
	{
		___BottomPt_5 = value;
		Il2CppCodeGenWriteBarrier(&___BottomPt_5, value);
	}

	inline static int32_t get_offset_of_PolyNode_6() { return static_cast<int32_t>(offsetof(OutRec_t3245805284, ___PolyNode_6)); }
	inline PolyNode_t3336253808 * get_PolyNode_6() const { return ___PolyNode_6; }
	inline PolyNode_t3336253808 ** get_address_of_PolyNode_6() { return &___PolyNode_6; }
	inline void set_PolyNode_6(PolyNode_t3336253808 * value)
	{
		___PolyNode_6 = value;
		Il2CppCodeGenWriteBarrier(&___PolyNode_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
