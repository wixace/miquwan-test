﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Int32,System.Single>
struct Dictionary_2_t4289182211;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Ke609150969.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Single>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m69797383_gshared (Enumerator_t609150969 * __this, Dictionary_2_t4289182211 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m69797383(__this, ___host0, method) ((  void (*) (Enumerator_t609150969 *, Dictionary_2_t4289182211 *, const MethodInfo*))Enumerator__ctor_m69797383_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Single>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1853960708_gshared (Enumerator_t609150969 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1853960708(__this, method) ((  Il2CppObject * (*) (Enumerator_t609150969 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1853960708_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Single>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2577462350_gshared (Enumerator_t609150969 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m2577462350(__this, method) ((  void (*) (Enumerator_t609150969 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2577462350_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Single>::Dispose()
extern "C"  void Enumerator_Dispose_m4277660777_gshared (Enumerator_t609150969 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m4277660777(__this, method) ((  void (*) (Enumerator_t609150969 *, const MethodInfo*))Enumerator_Dispose_m4277660777_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Single>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1805213566_gshared (Enumerator_t609150969 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m1805213566(__this, method) ((  bool (*) (Enumerator_t609150969 *, const MethodInfo*))Enumerator_MoveNext_m1805213566_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Single>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m1035925210_gshared (Enumerator_t609150969 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m1035925210(__this, method) ((  int32_t (*) (Enumerator_t609150969 *, const MethodInfo*))Enumerator_get_Current_m1035925210_gshared)(__this, method)
