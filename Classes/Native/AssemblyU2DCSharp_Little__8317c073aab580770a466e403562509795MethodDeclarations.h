﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._8317c073aab580770a466e401b2aaeb5
struct _8317c073aab580770a466e401b2aaeb5_t3562509795;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__8317c073aab580770a466e403562509795.h"

// System.Void Little._8317c073aab580770a466e401b2aaeb5::.ctor()
extern "C"  void _8317c073aab580770a466e401b2aaeb5__ctor_m893550506 (_8317c073aab580770a466e401b2aaeb5_t3562509795 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._8317c073aab580770a466e401b2aaeb5::_8317c073aab580770a466e401b2aaeb5m2(System.Int32)
extern "C"  int32_t _8317c073aab580770a466e401b2aaeb5__8317c073aab580770a466e401b2aaeb5m2_m786411193 (_8317c073aab580770a466e401b2aaeb5_t3562509795 * __this, int32_t ____8317c073aab580770a466e401b2aaeb5a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._8317c073aab580770a466e401b2aaeb5::_8317c073aab580770a466e401b2aaeb5m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _8317c073aab580770a466e401b2aaeb5__8317c073aab580770a466e401b2aaeb5m_m1250556957 (_8317c073aab580770a466e401b2aaeb5_t3562509795 * __this, int32_t ____8317c073aab580770a466e401b2aaeb5a0, int32_t ____8317c073aab580770a466e401b2aaeb5891, int32_t ____8317c073aab580770a466e401b2aaeb5c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._8317c073aab580770a466e401b2aaeb5::ilo__8317c073aab580770a466e401b2aaeb5m21(Little._8317c073aab580770a466e401b2aaeb5,System.Int32)
extern "C"  int32_t _8317c073aab580770a466e401b2aaeb5_ilo__8317c073aab580770a466e401b2aaeb5m21_m2695241994 (Il2CppObject * __this /* static, unused */, _8317c073aab580770a466e401b2aaeb5_t3562509795 * ____this0, int32_t ____8317c073aab580770a466e401b2aaeb5a1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
