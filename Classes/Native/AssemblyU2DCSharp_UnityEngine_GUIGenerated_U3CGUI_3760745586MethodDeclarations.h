﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_GUIGenerated/<GUI_ModalWindow_GetDelegate_member57_arg2>c__AnonStoreyEB
struct U3CGUI_ModalWindow_GetDelegate_member57_arg2U3Ec__AnonStoreyEB_t3760745586;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine_GUIGenerated/<GUI_ModalWindow_GetDelegate_member57_arg2>c__AnonStoreyEB::.ctor()
extern "C"  void U3CGUI_ModalWindow_GetDelegate_member57_arg2U3Ec__AnonStoreyEB__ctor_m3348645689 (U3CGUI_ModalWindow_GetDelegate_member57_arg2U3Ec__AnonStoreyEB_t3760745586 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUIGenerated/<GUI_ModalWindow_GetDelegate_member57_arg2>c__AnonStoreyEB::<>m__1C0(System.Int32)
extern "C"  void U3CGUI_ModalWindow_GetDelegate_member57_arg2U3Ec__AnonStoreyEB_U3CU3Em__1C0_m2983705455 (U3CGUI_ModalWindow_GetDelegate_member57_arg2U3Ec__AnonStoreyEB_t3760745586 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
