﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UI3DWidgetMgrGenerated
struct UI3DWidgetMgrGenerated_t3919198112;
// JSVCall
struct JSVCall_t3708497963;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void UI3DWidgetMgrGenerated::.ctor()
extern "C"  void UI3DWidgetMgrGenerated__ctor_m1473679611 (UI3DWidgetMgrGenerated_t3919198112 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UI3DWidgetMgrGenerated::UI3DWidgetMgr_UI3DWidgetMgr1(JSVCall,System.Int32)
extern "C"  bool UI3DWidgetMgrGenerated_UI3DWidgetMgr_UI3DWidgetMgr1_m876317695 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UI3DWidgetMgrGenerated::UI3DWidgetMgr_GetRoot(JSVCall,System.Int32)
extern "C"  bool UI3DWidgetMgrGenerated_UI3DWidgetMgr_GetRoot_m152923325 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UI3DWidgetMgrGenerated::UI3DWidgetMgr_NguiToWorld__Vector3__Single(JSVCall,System.Int32)
extern "C"  bool UI3DWidgetMgrGenerated_UI3DWidgetMgr_NguiToWorld__Vector3__Single_m1651907723 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UI3DWidgetMgrGenerated::UI3DWidgetMgr_WorldToNgui__Vector3(JSVCall,System.Int32)
extern "C"  bool UI3DWidgetMgrGenerated_UI3DWidgetMgr_WorldToNgui__Vector3_m1270612979 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UI3DWidgetMgrGenerated::__Register()
extern "C"  void UI3DWidgetMgrGenerated___Register_m3920973996 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UI3DWidgetMgrGenerated::ilo_setObject1(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UI3DWidgetMgrGenerated_ilo_setObject1_m913876463 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
