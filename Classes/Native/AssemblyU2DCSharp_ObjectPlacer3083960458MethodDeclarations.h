﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ObjectPlacer
struct ObjectPlacer_t3083960458;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ObjectPlacer3083960458.h"

// System.Void ObjectPlacer::.ctor()
extern "C"  void ObjectPlacer__ctor_m1933405521 (ObjectPlacer_t3083960458 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ObjectPlacer::Start()
extern "C"  void ObjectPlacer_Start_m880543313 (ObjectPlacer_t3083960458 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ObjectPlacer::Update()
extern "C"  void ObjectPlacer_Update_m1532891100 (ObjectPlacer_t3083960458 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ObjectPlacer::PlaceObject()
extern "C"  void ObjectPlacer_PlaceObject_m1434623381 (ObjectPlacer_t3083960458 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ObjectPlacer::RemoveObject()
extern "C"  void ObjectPlacer_RemoveObject_m2457552886 (ObjectPlacer_t3083960458 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ObjectPlacer::ilo_PlaceObject1(ObjectPlacer)
extern "C"  void ObjectPlacer_ilo_PlaceObject1_m540765063 (Il2CppObject * __this /* static, unused */, ObjectPlacer_t3083960458 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
