﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// Mihua.SDK.PayInfo
struct PayInfo_t1775308120;
// System.Action
struct Action_t3771233898;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PluginMoHe
struct  PluginMoHe_t1606231090  : public MonoBehaviour_t667441552
{
public:
	// System.String PluginMoHe::token
	String_t* ___token_2;
	// System.String PluginMoHe::userID
	String_t* ___userID_3;
	// System.Boolean PluginMoHe::isOut
	bool ___isOut_4;
	// System.String PluginMoHe::configId
	String_t* ___configId_5;
	// System.String PluginMoHe::switchAccount
	String_t* ___switchAccount_6;
	// Mihua.SDK.PayInfo PluginMoHe::payInfo
	PayInfo_t1775308120 * ___payInfo_7;

public:
	inline static int32_t get_offset_of_token_2() { return static_cast<int32_t>(offsetof(PluginMoHe_t1606231090, ___token_2)); }
	inline String_t* get_token_2() const { return ___token_2; }
	inline String_t** get_address_of_token_2() { return &___token_2; }
	inline void set_token_2(String_t* value)
	{
		___token_2 = value;
		Il2CppCodeGenWriteBarrier(&___token_2, value);
	}

	inline static int32_t get_offset_of_userID_3() { return static_cast<int32_t>(offsetof(PluginMoHe_t1606231090, ___userID_3)); }
	inline String_t* get_userID_3() const { return ___userID_3; }
	inline String_t** get_address_of_userID_3() { return &___userID_3; }
	inline void set_userID_3(String_t* value)
	{
		___userID_3 = value;
		Il2CppCodeGenWriteBarrier(&___userID_3, value);
	}

	inline static int32_t get_offset_of_isOut_4() { return static_cast<int32_t>(offsetof(PluginMoHe_t1606231090, ___isOut_4)); }
	inline bool get_isOut_4() const { return ___isOut_4; }
	inline bool* get_address_of_isOut_4() { return &___isOut_4; }
	inline void set_isOut_4(bool value)
	{
		___isOut_4 = value;
	}

	inline static int32_t get_offset_of_configId_5() { return static_cast<int32_t>(offsetof(PluginMoHe_t1606231090, ___configId_5)); }
	inline String_t* get_configId_5() const { return ___configId_5; }
	inline String_t** get_address_of_configId_5() { return &___configId_5; }
	inline void set_configId_5(String_t* value)
	{
		___configId_5 = value;
		Il2CppCodeGenWriteBarrier(&___configId_5, value);
	}

	inline static int32_t get_offset_of_switchAccount_6() { return static_cast<int32_t>(offsetof(PluginMoHe_t1606231090, ___switchAccount_6)); }
	inline String_t* get_switchAccount_6() const { return ___switchAccount_6; }
	inline String_t** get_address_of_switchAccount_6() { return &___switchAccount_6; }
	inline void set_switchAccount_6(String_t* value)
	{
		___switchAccount_6 = value;
		Il2CppCodeGenWriteBarrier(&___switchAccount_6, value);
	}

	inline static int32_t get_offset_of_payInfo_7() { return static_cast<int32_t>(offsetof(PluginMoHe_t1606231090, ___payInfo_7)); }
	inline PayInfo_t1775308120 * get_payInfo_7() const { return ___payInfo_7; }
	inline PayInfo_t1775308120 ** get_address_of_payInfo_7() { return &___payInfo_7; }
	inline void set_payInfo_7(PayInfo_t1775308120 * value)
	{
		___payInfo_7 = value;
		Il2CppCodeGenWriteBarrier(&___payInfo_7, value);
	}
};

struct PluginMoHe_t1606231090_StaticFields
{
public:
	// System.Action PluginMoHe::<>f__am$cache6
	Action_t3771233898 * ___U3CU3Ef__amU24cache6_8;
	// System.Action PluginMoHe::<>f__am$cache7
	Action_t3771233898 * ___U3CU3Ef__amU24cache7_9;
	// System.Action PluginMoHe::<>f__am$cache8
	Action_t3771233898 * ___U3CU3Ef__amU24cache8_10;
	// System.Action PluginMoHe::<>f__am$cache9
	Action_t3771233898 * ___U3CU3Ef__amU24cache9_11;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache6_8() { return static_cast<int32_t>(offsetof(PluginMoHe_t1606231090_StaticFields, ___U3CU3Ef__amU24cache6_8)); }
	inline Action_t3771233898 * get_U3CU3Ef__amU24cache6_8() const { return ___U3CU3Ef__amU24cache6_8; }
	inline Action_t3771233898 ** get_address_of_U3CU3Ef__amU24cache6_8() { return &___U3CU3Ef__amU24cache6_8; }
	inline void set_U3CU3Ef__amU24cache6_8(Action_t3771233898 * value)
	{
		___U3CU3Ef__amU24cache6_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache6_8, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache7_9() { return static_cast<int32_t>(offsetof(PluginMoHe_t1606231090_StaticFields, ___U3CU3Ef__amU24cache7_9)); }
	inline Action_t3771233898 * get_U3CU3Ef__amU24cache7_9() const { return ___U3CU3Ef__amU24cache7_9; }
	inline Action_t3771233898 ** get_address_of_U3CU3Ef__amU24cache7_9() { return &___U3CU3Ef__amU24cache7_9; }
	inline void set_U3CU3Ef__amU24cache7_9(Action_t3771233898 * value)
	{
		___U3CU3Ef__amU24cache7_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache7_9, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache8_10() { return static_cast<int32_t>(offsetof(PluginMoHe_t1606231090_StaticFields, ___U3CU3Ef__amU24cache8_10)); }
	inline Action_t3771233898 * get_U3CU3Ef__amU24cache8_10() const { return ___U3CU3Ef__amU24cache8_10; }
	inline Action_t3771233898 ** get_address_of_U3CU3Ef__amU24cache8_10() { return &___U3CU3Ef__amU24cache8_10; }
	inline void set_U3CU3Ef__amU24cache8_10(Action_t3771233898 * value)
	{
		___U3CU3Ef__amU24cache8_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache8_10, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache9_11() { return static_cast<int32_t>(offsetof(PluginMoHe_t1606231090_StaticFields, ___U3CU3Ef__amU24cache9_11)); }
	inline Action_t3771233898 * get_U3CU3Ef__amU24cache9_11() const { return ___U3CU3Ef__amU24cache9_11; }
	inline Action_t3771233898 ** get_address_of_U3CU3Ef__amU24cache9_11() { return &___U3CU3Ef__amU24cache9_11; }
	inline void set_U3CU3Ef__amU24cache9_11(Action_t3771233898 * value)
	{
		___U3CU3Ef__amU24cache9_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache9_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
