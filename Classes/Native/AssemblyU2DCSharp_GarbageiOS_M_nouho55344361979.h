﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_nouho55
struct  M_nouho55_t344361979  : public Il2CppObject
{
public:
	// System.String GarbageiOS.M_nouho55::_zigallNoukir
	String_t* ____zigallNoukir_0;
	// System.String GarbageiOS.M_nouho55::_dedasNodamu
	String_t* ____dedasNodamu_1;
	// System.Single GarbageiOS.M_nouho55::_derekasSemhidro
	float ____derekasSemhidro_2;
	// System.Single GarbageiOS.M_nouho55::_cursowTrakuro
	float ____cursowTrakuro_3;
	// System.Boolean GarbageiOS.M_nouho55::_nirpear
	bool ____nirpear_4;
	// System.String GarbageiOS.M_nouho55::_rapem
	String_t* ____rapem_5;
	// System.Boolean GarbageiOS.M_nouho55::_conislou
	bool ____conislou_6;
	// System.Single GarbageiOS.M_nouho55::_resirVurxer
	float ____resirVurxer_7;
	// System.Int32 GarbageiOS.M_nouho55::_pardesoo
	int32_t ____pardesoo_8;
	// System.Single GarbageiOS.M_nouho55::_tetaBaymas
	float ____tetaBaymas_9;
	// System.Int32 GarbageiOS.M_nouho55::_maychayis
	int32_t ____maychayis_10;
	// System.Boolean GarbageiOS.M_nouho55::_rooreeteePoowaga
	bool ____rooreeteePoowaga_11;
	// System.String GarbageiOS.M_nouho55::_nairfou
	String_t* ____nairfou_12;
	// System.String GarbageiOS.M_nouho55::_sowhi
	String_t* ____sowhi_13;
	// System.Boolean GarbageiOS.M_nouho55::_salcis
	bool ____salcis_14;
	// System.Single GarbageiOS.M_nouho55::_towxo
	float ____towxo_15;
	// System.UInt32 GarbageiOS.M_nouho55::_rerha
	uint32_t ____rerha_16;
	// System.Single GarbageiOS.M_nouho55::_wealerYalka
	float ____wealerYalka_17;
	// System.UInt32 GarbageiOS.M_nouho55::_junir
	uint32_t ____junir_18;

public:
	inline static int32_t get_offset_of__zigallNoukir_0() { return static_cast<int32_t>(offsetof(M_nouho55_t344361979, ____zigallNoukir_0)); }
	inline String_t* get__zigallNoukir_0() const { return ____zigallNoukir_0; }
	inline String_t** get_address_of__zigallNoukir_0() { return &____zigallNoukir_0; }
	inline void set__zigallNoukir_0(String_t* value)
	{
		____zigallNoukir_0 = value;
		Il2CppCodeGenWriteBarrier(&____zigallNoukir_0, value);
	}

	inline static int32_t get_offset_of__dedasNodamu_1() { return static_cast<int32_t>(offsetof(M_nouho55_t344361979, ____dedasNodamu_1)); }
	inline String_t* get__dedasNodamu_1() const { return ____dedasNodamu_1; }
	inline String_t** get_address_of__dedasNodamu_1() { return &____dedasNodamu_1; }
	inline void set__dedasNodamu_1(String_t* value)
	{
		____dedasNodamu_1 = value;
		Il2CppCodeGenWriteBarrier(&____dedasNodamu_1, value);
	}

	inline static int32_t get_offset_of__derekasSemhidro_2() { return static_cast<int32_t>(offsetof(M_nouho55_t344361979, ____derekasSemhidro_2)); }
	inline float get__derekasSemhidro_2() const { return ____derekasSemhidro_2; }
	inline float* get_address_of__derekasSemhidro_2() { return &____derekasSemhidro_2; }
	inline void set__derekasSemhidro_2(float value)
	{
		____derekasSemhidro_2 = value;
	}

	inline static int32_t get_offset_of__cursowTrakuro_3() { return static_cast<int32_t>(offsetof(M_nouho55_t344361979, ____cursowTrakuro_3)); }
	inline float get__cursowTrakuro_3() const { return ____cursowTrakuro_3; }
	inline float* get_address_of__cursowTrakuro_3() { return &____cursowTrakuro_3; }
	inline void set__cursowTrakuro_3(float value)
	{
		____cursowTrakuro_3 = value;
	}

	inline static int32_t get_offset_of__nirpear_4() { return static_cast<int32_t>(offsetof(M_nouho55_t344361979, ____nirpear_4)); }
	inline bool get__nirpear_4() const { return ____nirpear_4; }
	inline bool* get_address_of__nirpear_4() { return &____nirpear_4; }
	inline void set__nirpear_4(bool value)
	{
		____nirpear_4 = value;
	}

	inline static int32_t get_offset_of__rapem_5() { return static_cast<int32_t>(offsetof(M_nouho55_t344361979, ____rapem_5)); }
	inline String_t* get__rapem_5() const { return ____rapem_5; }
	inline String_t** get_address_of__rapem_5() { return &____rapem_5; }
	inline void set__rapem_5(String_t* value)
	{
		____rapem_5 = value;
		Il2CppCodeGenWriteBarrier(&____rapem_5, value);
	}

	inline static int32_t get_offset_of__conislou_6() { return static_cast<int32_t>(offsetof(M_nouho55_t344361979, ____conislou_6)); }
	inline bool get__conislou_6() const { return ____conislou_6; }
	inline bool* get_address_of__conislou_6() { return &____conislou_6; }
	inline void set__conislou_6(bool value)
	{
		____conislou_6 = value;
	}

	inline static int32_t get_offset_of__resirVurxer_7() { return static_cast<int32_t>(offsetof(M_nouho55_t344361979, ____resirVurxer_7)); }
	inline float get__resirVurxer_7() const { return ____resirVurxer_7; }
	inline float* get_address_of__resirVurxer_7() { return &____resirVurxer_7; }
	inline void set__resirVurxer_7(float value)
	{
		____resirVurxer_7 = value;
	}

	inline static int32_t get_offset_of__pardesoo_8() { return static_cast<int32_t>(offsetof(M_nouho55_t344361979, ____pardesoo_8)); }
	inline int32_t get__pardesoo_8() const { return ____pardesoo_8; }
	inline int32_t* get_address_of__pardesoo_8() { return &____pardesoo_8; }
	inline void set__pardesoo_8(int32_t value)
	{
		____pardesoo_8 = value;
	}

	inline static int32_t get_offset_of__tetaBaymas_9() { return static_cast<int32_t>(offsetof(M_nouho55_t344361979, ____tetaBaymas_9)); }
	inline float get__tetaBaymas_9() const { return ____tetaBaymas_9; }
	inline float* get_address_of__tetaBaymas_9() { return &____tetaBaymas_9; }
	inline void set__tetaBaymas_9(float value)
	{
		____tetaBaymas_9 = value;
	}

	inline static int32_t get_offset_of__maychayis_10() { return static_cast<int32_t>(offsetof(M_nouho55_t344361979, ____maychayis_10)); }
	inline int32_t get__maychayis_10() const { return ____maychayis_10; }
	inline int32_t* get_address_of__maychayis_10() { return &____maychayis_10; }
	inline void set__maychayis_10(int32_t value)
	{
		____maychayis_10 = value;
	}

	inline static int32_t get_offset_of__rooreeteePoowaga_11() { return static_cast<int32_t>(offsetof(M_nouho55_t344361979, ____rooreeteePoowaga_11)); }
	inline bool get__rooreeteePoowaga_11() const { return ____rooreeteePoowaga_11; }
	inline bool* get_address_of__rooreeteePoowaga_11() { return &____rooreeteePoowaga_11; }
	inline void set__rooreeteePoowaga_11(bool value)
	{
		____rooreeteePoowaga_11 = value;
	}

	inline static int32_t get_offset_of__nairfou_12() { return static_cast<int32_t>(offsetof(M_nouho55_t344361979, ____nairfou_12)); }
	inline String_t* get__nairfou_12() const { return ____nairfou_12; }
	inline String_t** get_address_of__nairfou_12() { return &____nairfou_12; }
	inline void set__nairfou_12(String_t* value)
	{
		____nairfou_12 = value;
		Il2CppCodeGenWriteBarrier(&____nairfou_12, value);
	}

	inline static int32_t get_offset_of__sowhi_13() { return static_cast<int32_t>(offsetof(M_nouho55_t344361979, ____sowhi_13)); }
	inline String_t* get__sowhi_13() const { return ____sowhi_13; }
	inline String_t** get_address_of__sowhi_13() { return &____sowhi_13; }
	inline void set__sowhi_13(String_t* value)
	{
		____sowhi_13 = value;
		Il2CppCodeGenWriteBarrier(&____sowhi_13, value);
	}

	inline static int32_t get_offset_of__salcis_14() { return static_cast<int32_t>(offsetof(M_nouho55_t344361979, ____salcis_14)); }
	inline bool get__salcis_14() const { return ____salcis_14; }
	inline bool* get_address_of__salcis_14() { return &____salcis_14; }
	inline void set__salcis_14(bool value)
	{
		____salcis_14 = value;
	}

	inline static int32_t get_offset_of__towxo_15() { return static_cast<int32_t>(offsetof(M_nouho55_t344361979, ____towxo_15)); }
	inline float get__towxo_15() const { return ____towxo_15; }
	inline float* get_address_of__towxo_15() { return &____towxo_15; }
	inline void set__towxo_15(float value)
	{
		____towxo_15 = value;
	}

	inline static int32_t get_offset_of__rerha_16() { return static_cast<int32_t>(offsetof(M_nouho55_t344361979, ____rerha_16)); }
	inline uint32_t get__rerha_16() const { return ____rerha_16; }
	inline uint32_t* get_address_of__rerha_16() { return &____rerha_16; }
	inline void set__rerha_16(uint32_t value)
	{
		____rerha_16 = value;
	}

	inline static int32_t get_offset_of__wealerYalka_17() { return static_cast<int32_t>(offsetof(M_nouho55_t344361979, ____wealerYalka_17)); }
	inline float get__wealerYalka_17() const { return ____wealerYalka_17; }
	inline float* get_address_of__wealerYalka_17() { return &____wealerYalka_17; }
	inline void set__wealerYalka_17(float value)
	{
		____wealerYalka_17 = value;
	}

	inline static int32_t get_offset_of__junir_18() { return static_cast<int32_t>(offsetof(M_nouho55_t344361979, ____junir_18)); }
	inline uint32_t get__junir_18() const { return ____junir_18; }
	inline uint32_t* get_address_of__junir_18() { return &____junir_18; }
	inline void set__junir_18(uint32_t value)
	{
		____junir_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
