﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1157182362.h"
#include "mscorlib_System_Array1146569071.h"
#include "UnityEngine_UnityEngine_MeshSubsetCombineUtility_S2374839686.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1664744215_gshared (InternalEnumerator_1_t1157182362 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m1664744215(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t1157182362 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m1664744215_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m556703017_gshared (InternalEnumerator_1_t1157182362 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m556703017(__this, method) ((  void (*) (InternalEnumerator_1_t1157182362 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m556703017_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3117776149_gshared (InternalEnumerator_1_t1157182362 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3117776149(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t1157182362 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3117776149_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2500535726_gshared (InternalEnumerator_1_t1157182362 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m2500535726(__this, method) ((  void (*) (InternalEnumerator_1_t1157182362 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2500535726_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3574950805_gshared (InternalEnumerator_1_t1157182362 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m3574950805(__this, method) ((  bool (*) (InternalEnumerator_1_t1157182362 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m3574950805_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::get_Current()
extern "C"  SubMeshInstance_t2374839686  InternalEnumerator_1_get_Current_m421990750_gshared (InternalEnumerator_1_t1157182362 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m421990750(__this, method) ((  SubMeshInstance_t2374839686  (*) (InternalEnumerator_1_t1157182362 *, const MethodInfo*))InternalEnumerator_1_get_Current_m421990750_gshared)(__this, method)
