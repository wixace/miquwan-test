﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Type
struct Type_t;

#include "mscorlib_System_Attribute2523058482.h"
#include "mscorlib_System_Nullable_1_gen560925241.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SharpKit.JavaScript.JsMethodAttribute
struct  JsMethodAttribute_t1501046856  : public Attribute_t2523058482
{
public:
	// System.Nullable`1<System.Boolean> SharpKit.JavaScript.JsMethodAttribute::_NativeParams
	Nullable_1_t560925241  ____NativeParams_0;
	// System.Nullable`1<System.Boolean> SharpKit.JavaScript.JsMethodAttribute::_NativeOverloads
	Nullable_1_t560925241  ____NativeOverloads_1;
	// System.Nullable`1<System.Boolean> SharpKit.JavaScript.JsMethodAttribute::_IgnoreGenericArguments
	Nullable_1_t560925241  ____IgnoreGenericArguments_2;
	// System.Nullable`1<System.Boolean> SharpKit.JavaScript.JsMethodAttribute::_Global
	Nullable_1_t560925241  ____Global_3;
	// System.Nullable`1<System.Boolean> SharpKit.JavaScript.JsMethodAttribute::_Export
	Nullable_1_t560925241  ____Export_4;
	// System.String SharpKit.JavaScript.JsMethodAttribute::<TargetMethod>k__BackingField
	String_t* ___U3CTargetMethodU3Ek__BackingField_5;
	// System.Type SharpKit.JavaScript.JsMethodAttribute::<TargetType>k__BackingField
	Type_t * ___U3CTargetTypeU3Ek__BackingField_6;
	// System.String SharpKit.JavaScript.JsMethodAttribute::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_7;

public:
	inline static int32_t get_offset_of__NativeParams_0() { return static_cast<int32_t>(offsetof(JsMethodAttribute_t1501046856, ____NativeParams_0)); }
	inline Nullable_1_t560925241  get__NativeParams_0() const { return ____NativeParams_0; }
	inline Nullable_1_t560925241 * get_address_of__NativeParams_0() { return &____NativeParams_0; }
	inline void set__NativeParams_0(Nullable_1_t560925241  value)
	{
		____NativeParams_0 = value;
	}

	inline static int32_t get_offset_of__NativeOverloads_1() { return static_cast<int32_t>(offsetof(JsMethodAttribute_t1501046856, ____NativeOverloads_1)); }
	inline Nullable_1_t560925241  get__NativeOverloads_1() const { return ____NativeOverloads_1; }
	inline Nullable_1_t560925241 * get_address_of__NativeOverloads_1() { return &____NativeOverloads_1; }
	inline void set__NativeOverloads_1(Nullable_1_t560925241  value)
	{
		____NativeOverloads_1 = value;
	}

	inline static int32_t get_offset_of__IgnoreGenericArguments_2() { return static_cast<int32_t>(offsetof(JsMethodAttribute_t1501046856, ____IgnoreGenericArguments_2)); }
	inline Nullable_1_t560925241  get__IgnoreGenericArguments_2() const { return ____IgnoreGenericArguments_2; }
	inline Nullable_1_t560925241 * get_address_of__IgnoreGenericArguments_2() { return &____IgnoreGenericArguments_2; }
	inline void set__IgnoreGenericArguments_2(Nullable_1_t560925241  value)
	{
		____IgnoreGenericArguments_2 = value;
	}

	inline static int32_t get_offset_of__Global_3() { return static_cast<int32_t>(offsetof(JsMethodAttribute_t1501046856, ____Global_3)); }
	inline Nullable_1_t560925241  get__Global_3() const { return ____Global_3; }
	inline Nullable_1_t560925241 * get_address_of__Global_3() { return &____Global_3; }
	inline void set__Global_3(Nullable_1_t560925241  value)
	{
		____Global_3 = value;
	}

	inline static int32_t get_offset_of__Export_4() { return static_cast<int32_t>(offsetof(JsMethodAttribute_t1501046856, ____Export_4)); }
	inline Nullable_1_t560925241  get__Export_4() const { return ____Export_4; }
	inline Nullable_1_t560925241 * get_address_of__Export_4() { return &____Export_4; }
	inline void set__Export_4(Nullable_1_t560925241  value)
	{
		____Export_4 = value;
	}

	inline static int32_t get_offset_of_U3CTargetMethodU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(JsMethodAttribute_t1501046856, ___U3CTargetMethodU3Ek__BackingField_5)); }
	inline String_t* get_U3CTargetMethodU3Ek__BackingField_5() const { return ___U3CTargetMethodU3Ek__BackingField_5; }
	inline String_t** get_address_of_U3CTargetMethodU3Ek__BackingField_5() { return &___U3CTargetMethodU3Ek__BackingField_5; }
	inline void set_U3CTargetMethodU3Ek__BackingField_5(String_t* value)
	{
		___U3CTargetMethodU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CTargetMethodU3Ek__BackingField_5, value);
	}

	inline static int32_t get_offset_of_U3CTargetTypeU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(JsMethodAttribute_t1501046856, ___U3CTargetTypeU3Ek__BackingField_6)); }
	inline Type_t * get_U3CTargetTypeU3Ek__BackingField_6() const { return ___U3CTargetTypeU3Ek__BackingField_6; }
	inline Type_t ** get_address_of_U3CTargetTypeU3Ek__BackingField_6() { return &___U3CTargetTypeU3Ek__BackingField_6; }
	inline void set_U3CTargetTypeU3Ek__BackingField_6(Type_t * value)
	{
		___U3CTargetTypeU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CTargetTypeU3Ek__BackingField_6, value);
	}

	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(JsMethodAttribute_t1501046856, ___U3CNameU3Ek__BackingField_7)); }
	inline String_t* get_U3CNameU3Ek__BackingField_7() const { return ___U3CNameU3Ek__BackingField_7; }
	inline String_t** get_address_of_U3CNameU3Ek__BackingField_7() { return &___U3CNameU3Ek__BackingField_7; }
	inline void set_U3CNameU3Ek__BackingField_7(String_t* value)
	{
		___U3CNameU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CNameU3Ek__BackingField_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
