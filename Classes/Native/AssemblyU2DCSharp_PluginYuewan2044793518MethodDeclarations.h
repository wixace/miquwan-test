﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PluginYuewan
struct PluginYuewan_t2044793518;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// System.String
struct String_t;
// UnityEngine.WWWForm
struct WWWForm_t461342257;
// Mihua.SDK.PayInfo
struct PayInfo_t1775308120;
// VersionMgr
struct VersionMgr_t1322950208;
// VersionInfo
struct VersionInfo_t2356638086;
// ProductsCfgMgr
struct ProductsCfgMgr_t2493714872;
// FloatTextMgr
struct FloatTextMgr_t630384591;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// PluginsSdkMgr
struct PluginsSdkMgr_t3884624670;
// System.Object
struct Il2CppObject;
// Newtonsoft.Json.Linq.JObject
struct JObject_t1798544199;
// Newtonsoft.Json.Linq.JToken
struct JToken_t3412245951;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_Mihua_SDK_PayInfo1775308120.h"
#include "AssemblyU2DCSharp_VersionMgr1322950208.h"
#include "AssemblyU2DCSharp_PluginYuewan2044793518.h"
#include "AssemblyU2DCSharp_ProductsCfgMgr2493714872.h"
#include "AssemblyU2DCSharp_FloatTextMgr630384591.h"
#include "AssemblyU2DCSharp_FLOAT_TEXT_ID2199345770.h"
#include "AssemblyU2DCSharp_PluginsSdkMgr3884624670.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JObject1798544199.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JToken3412245951.h"

// System.Void PluginYuewan::.ctor()
extern "C"  void PluginYuewan__ctor_m3471180717 (PluginYuewan_t2044793518 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYuewan::Init()
extern "C"  void PluginYuewan_Init_m2834456839 (PluginYuewan_t2044793518 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginYuewan::ReqSDKHttpLogin()
extern "C"  Dictionary_2_t827649927 * PluginYuewan_ReqSDKHttpLogin_m3354118082 (PluginYuewan_t2044793518 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginYuewan::IsLoginSuccess(System.Boolean)
extern "C"  bool PluginYuewan_IsLoginSuccess_m2455415046 (PluginYuewan_t2044793518 * __this, bool ___isOpenUseLogin0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYuewan::OpenUserLogin()
extern "C"  void PluginYuewan_OpenUserLogin_m3257608447 (PluginYuewan_t2044793518 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYuewan::UserPay(CEvent.ZEvent)
extern "C"  void PluginYuewan_UserPay_m86449555 (PluginYuewan_t2044793518 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYuewan::SignCallBack(System.Boolean,System.String)
extern "C"  void PluginYuewan_SignCallBack_m1055837356 (PluginYuewan_t2044793518 * __this, bool ___success0, String_t* ___arg1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.WWWForm PluginYuewan::BuildOrderParam2WWWForm(Mihua.SDK.PayInfo)
extern "C"  WWWForm_t461342257 * PluginYuewan_BuildOrderParam2WWWForm_m1706532244 (PluginYuewan_t2044793518 * __this, PayInfo_t1775308120 * ___info0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYuewan::OnLoginSuccess(System.String)
extern "C"  void PluginYuewan_OnLoginSuccess_m1666468594 (PluginYuewan_t2044793518 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYuewan::OnLoginFail(System.String)
extern "C"  void PluginYuewan_OnLoginFail_m3792460687 (PluginYuewan_t2044793518 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYuewan::GameLoginSuccess(CEvent.ZEvent)
extern "C"  void PluginYuewan_GameLoginSuccess_m3768902648 (PluginYuewan_t2044793518 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYuewan::JsonParse(System.String)
extern "C"  void PluginYuewan_JsonParse_m2302366476 (PluginYuewan_t2044793518 * __this, String_t* ___arg0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYuewan::OnUserSwitchSuccess(System.String)
extern "C"  void PluginYuewan_OnUserSwitchSuccess_m1480537842 (PluginYuewan_t2044793518 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYuewan::OnPaySuccess(System.String)
extern "C"  void PluginYuewan_OnPaySuccess_m1817493937 (PluginYuewan_t2044793518 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYuewan::OnPayFail(System.String)
extern "C"  void PluginYuewan_OnPayFail_m1928492336 (PluginYuewan_t2044793518 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYuewan::OnLogoutSuccess(System.String)
extern "C"  void PluginYuewan_OnLogoutSuccess_m124461533 (PluginYuewan_t2044793518 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYuewan::OnLogout(System.String)
extern "C"  void PluginYuewan_OnLogout_m123857922 (PluginYuewan_t2044793518 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYuewan::initSdk(System.String,System.String)
extern "C"  void PluginYuewan_initSdk_m3880321449 (PluginYuewan_t2044793518 * __this, String_t* ___appkey0, String_t* ___appsecret1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYuewan::login()
extern "C"  void PluginYuewan_login_m2993195540 (PluginYuewan_t2044793518 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYuewan::logout()
extern "C"  void PluginYuewan_logout_m2600571233 (PluginYuewan_t2044793518 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYuewan::confirmlogin(System.String,System.String,System.String,System.String)
extern "C"  void PluginYuewan_confirmlogin_m1046858230 (PluginYuewan_t2044793518 * __this, String_t* ___accesstoken0, String_t* ___expires1, String_t* ___userid2, String_t* ___username3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYuewan::pay(System.String,System.Int32,System.String,System.String,System.String,System.String,System.String,System.String,System.Int32,System.String,System.String)
extern "C"  void PluginYuewan_pay_m3371808255 (PluginYuewan_t2044793518 * __this, String_t* ___OrderId0, int32_t ___ZfAmount1, String_t* ___producttype2, String_t* ___ProductName3, String_t* ___extra4, String_t* ___NotifyUrl5, String_t* ___RoleId6, String_t* ___ServerName7, int32_t ___ServerId8, String_t* ___RoleName9, String_t* ___ProductId10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYuewan::<OnLogout>m__47F()
extern "C"  void PluginYuewan_U3COnLogoutU3Em__47F_m2090648198 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginYuewan::ilo_get_isSdkLogin1(VersionMgr)
extern "C"  bool PluginYuewan_ilo_get_isSdkLogin1_m2934661427 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionMgr PluginYuewan::ilo_get_Instance2()
extern "C"  VersionMgr_t1322950208 * PluginYuewan_ilo_get_Instance2_m775737647 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYuewan::ilo_login3(PluginYuewan)
extern "C"  void PluginYuewan_ilo_login3_m432516230 (Il2CppObject * __this /* static, unused */, PluginYuewan_t2044793518 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionInfo PluginYuewan::ilo_get_currentVS4(VersionMgr)
extern "C"  VersionInfo_t2356638086 * PluginYuewan_ilo_get_currentVS4_m4139872350 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PluginYuewan::ilo_GetProductID5(ProductsCfgMgr,System.String)
extern "C"  String_t* PluginYuewan_ilo_GetProductID5_m1275325082 (Il2CppObject * __this /* static, unused */, ProductsCfgMgr_t2493714872 * ____this0, String_t* ___name1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FloatTextMgr PluginYuewan::ilo_get_FloatTextMgr6()
extern "C"  FloatTextMgr_t630384591 * PluginYuewan_ilo_get_FloatTextMgr6_m150482408 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYuewan::ilo_FloatText7(FloatTextMgr,FLOAT_TEXT_ID,System.Int32,System.Object[])
extern "C"  void PluginYuewan_ilo_FloatText7_m556987704 (Il2CppObject * __this /* static, unused */, FloatTextMgr_t630384591 * ____this0, int32_t ___textId1, int32_t ___offsetY2, ObjectU5BU5D_t1108656482* ___args3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PluginsSdkMgr PluginYuewan::ilo_get_PluginsSdkMgr8()
extern "C"  PluginsSdkMgr_t3884624670 * PluginYuewan_ilo_get_PluginsSdkMgr8_m2301357794 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYuewan::ilo_ReqSDKHttpLogin9(PluginsSdkMgr)
extern "C"  void PluginYuewan_ilo_ReqSDKHttpLogin9_m850276406 (Il2CppObject * __this /* static, unused */, PluginsSdkMgr_t3884624670 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYuewan::ilo_JsonParse10(PluginYuewan,System.String)
extern "C"  void PluginYuewan_ilo_JsonParse10_m502228816 (Il2CppObject * __this /* static, unused */, PluginYuewan_t2044793518 * ____this0, String_t* ___arg1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYuewan::ilo_confirmlogin11(PluginYuewan,System.String,System.String,System.String,System.String)
extern "C"  void PluginYuewan_ilo_confirmlogin11_m4103400405 (Il2CppObject * __this /* static, unused */, PluginYuewan_t2044793518 * ____this0, String_t* ___accesstoken1, String_t* ___expires2, String_t* ___userid3, String_t* ___username4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYuewan::ilo_LogError12(System.Object,System.Boolean)
extern "C"  void PluginYuewan_ilo_LogError12_m886743576 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginYuewan::ilo_ContainsKey13(Newtonsoft.Json.Linq.JObject,System.String)
extern "C"  bool PluginYuewan_ilo_ContainsKey13_m1873694802 (Il2CppObject * __this /* static, unused */, JObject_t1798544199 * ____this0, String_t* ___key1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken PluginYuewan::ilo_get_Item14(Newtonsoft.Json.Linq.JToken,System.Object)
extern "C"  JToken_t3412245951 * PluginYuewan_ilo_get_Item14_m2875251076 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ____this0, Il2CppObject * ___key1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PluginYuewan::ilo_ToString15(Newtonsoft.Json.Linq.JToken)
extern "C"  String_t* PluginYuewan_ilo_ToString15_m3293340013 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYuewan::ilo_Log16(System.Object,System.Boolean)
extern "C"  void PluginYuewan_ilo_Log16_m3611620426 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYuewan::ilo_DispatchEvent17(CEvent.ZEvent)
extern "C"  void PluginYuewan_ilo_DispatchEvent17_m2046087389 (Il2CppObject * __this /* static, unused */, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
