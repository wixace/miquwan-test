﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_JointMotorGenerated
struct UnityEngine_JointMotorGenerated_t1563000156;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void UnityEngine_JointMotorGenerated::.ctor()
extern "C"  void UnityEngine_JointMotorGenerated__ctor_m4252287375 (UnityEngine_JointMotorGenerated_t1563000156 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_JointMotorGenerated::.cctor()
extern "C"  void UnityEngine_JointMotorGenerated__cctor_m2489793534 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_JointMotorGenerated::JointMotor_JointMotor1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_JointMotorGenerated_JointMotor_JointMotor1_m2459518563 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_JointMotorGenerated::JointMotor_targetVelocity(JSVCall)
extern "C"  void UnityEngine_JointMotorGenerated_JointMotor_targetVelocity_m3288983640 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_JointMotorGenerated::JointMotor_force(JSVCall)
extern "C"  void UnityEngine_JointMotorGenerated_JointMotor_force_m1326922331 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_JointMotorGenerated::JointMotor_freeSpin(JSVCall)
extern "C"  void UnityEngine_JointMotorGenerated_JointMotor_freeSpin_m253824280 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_JointMotorGenerated::__Register()
extern "C"  void UnityEngine_JointMotorGenerated___Register_m4228013208 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_JointMotorGenerated::ilo_attachFinalizerObject1(System.Int32)
extern "C"  bool UnityEngine_JointMotorGenerated_ilo_attachFinalizerObject1_m3950819912 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_JointMotorGenerated::ilo_addJSCSRel2(System.Int32,System.Object)
extern "C"  void UnityEngine_JointMotorGenerated_ilo_addJSCSRel2_m1267130700 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_JointMotorGenerated::ilo_setSingle3(System.Int32,System.Single)
extern "C"  void UnityEngine_JointMotorGenerated_ilo_setSingle3_m2585984391 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine_JointMotorGenerated::ilo_getSingle4(System.Int32)
extern "C"  float UnityEngine_JointMotorGenerated_ilo_getSingle4_m3799704739 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
