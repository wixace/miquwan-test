﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<Pathfinding.ClipperLib.PolyNode>
struct List_1_t409472064;

#include "Pathfinding_ClipperLib_Pathfinding_ClipperLib_Poly3336253808.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.ClipperLib.PolyTree
struct  PolyTree_t3336435468  : public PolyNode_t3336253808
{
public:
	// System.Collections.Generic.List`1<Pathfinding.ClipperLib.PolyNode> Pathfinding.ClipperLib.PolyTree::m_AllPolys
	List_1_t409472064 * ___m_AllPolys_5;

public:
	inline static int32_t get_offset_of_m_AllPolys_5() { return static_cast<int32_t>(offsetof(PolyTree_t3336435468, ___m_AllPolys_5)); }
	inline List_1_t409472064 * get_m_AllPolys_5() const { return ___m_AllPolys_5; }
	inline List_1_t409472064 ** get_address_of_m_AllPolys_5() { return &___m_AllPolys_5; }
	inline void set_m_AllPolys_5(List_1_t409472064 * value)
	{
		___m_AllPolys_5 = value;
		Il2CppCodeGenWriteBarrier(&___m_AllPolys_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
