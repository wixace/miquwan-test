﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VersionMgrGenerated
struct VersionMgrGenerated_t3038782255;
// JSVCall
struct JSVCall_t3708497963;
// System.Action
struct Action_t3771233898;
// CSRepresentedObject
struct CSRepresentedObject_t3994124630;
// System.String
struct String_t;
// VersionMgr
struct VersionMgr_t1322950208;
// VersionInfo[]
struct VersionInfoU5BU5D_t1155590563;
// System.Collections.Generic.Dictionary`2<System.String,VersionInfo>
struct Dictionary_2_t3177056456;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// VersionInfo
struct VersionInfo_t2356638086;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_CSRepresentedObject3994124630.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_VersionMgr1322950208.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "AssemblyU2DCSharp_VersionInfo2356638086.h"

// System.Void VersionMgrGenerated::.ctor()
extern "C"  void VersionMgrGenerated__ctor_m2498827548 (VersionMgrGenerated_t3038782255 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VersionMgrGenerated::VersionMgr_enabled(JSVCall)
extern "C"  void VersionMgrGenerated_VersionMgr_enabled_m1376681565 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VersionMgrGenerated::VersionMgr_vsTag(JSVCall)
extern "C"  void VersionMgrGenerated_VersionMgr_vsTag_m1044753281 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VersionMgrGenerated::VersionMgr_pkgSavePath(JSVCall)
extern "C"  void VersionMgrGenerated_VersionMgr_pkgSavePath_m831294224 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VersionMgrGenerated::VersionMgr_rzText(JSVCall)
extern "C"  void VersionMgrGenerated_VersionMgr_rzText_m4293066649 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VersionMgrGenerated::VersionMgr_app_id(JSVCall)
extern "C"  void VersionMgrGenerated_VersionMgr_app_id_m534010805 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VersionMgrGenerated::VersionMgr_cch_id(JSVCall)
extern "C"  void VersionMgrGenerated_VersionMgr_cch_id_m3878666012 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VersionMgrGenerated::VersionMgr_md_id(JSVCall)
extern "C"  void VersionMgrGenerated_VersionMgr_md_id_m1924632859 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VersionMgrGenerated::VersionMgr_reyun_app_id(JSVCall)
extern "C"  void VersionMgrGenerated_VersionMgr_reyun_app_id_m1004135221 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VersionMgrGenerated::VersionMgr_reyun_track_app_id(JSVCall)
extern "C"  void VersionMgrGenerated_VersionMgr_reyun_track_app_id_m749530689 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VersionMgrGenerated::VersionMgr_userType(JSVCall)
extern "C"  void VersionMgrGenerated_VersionMgr_userType_m2103427465 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VersionMgrGenerated::VersionMgr_pidurl(JSVCall)
extern "C"  void VersionMgrGenerated_VersionMgr_pidurl_m2417282058 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VersionMgrGenerated::VersionMgr_sdkAppKey(JSVCall)
extern "C"  void VersionMgrGenerated_VersionMgr_sdkAppKey_m2855244838 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VersionMgrGenerated::VersionMgr_sdkAppid(JSVCall)
extern "C"  void VersionMgrGenerated_VersionMgr_sdkAppid_m1320975596 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VersionMgrGenerated::VersionMgr_sdkCchid(JSVCall)
extern "C"  void VersionMgrGenerated_VersionMgr_sdkCchid_m3091435685 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VersionMgrGenerated::VersionMgr_sdkMdid(JSVCall)
extern "C"  void VersionMgrGenerated_VersionMgr_sdkMdid_m2103865778 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VersionMgrGenerated::VersionMgr_Instance(JSVCall)
extern "C"  void VersionMgrGenerated_VersionMgr_Instance_m511167609 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VersionMgrGenerated::VersionMgr_isSdkLogin(JSVCall)
extern "C"  void VersionMgrGenerated_VersionMgr_isSdkLogin_m433636629 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VersionMgrGenerated::VersionMgr_isWaiwang(JSVCall)
extern "C"  void VersionMgrGenerated_VersionMgr_isWaiwang_m1663166630 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VersionMgrGenerated::VersionMgr_isHide2000(JSVCall)
extern "C"  void VersionMgrGenerated_VersionMgr_isHide2000_m3606450820 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VersionMgrGenerated::VersionMgr_loadingbgName(JSVCall)
extern "C"  void VersionMgrGenerated_VersionMgr_loadingbgName_m231539538 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VersionMgrGenerated::VersionMgr_vsArr(JSVCall)
extern "C"  void VersionMgrGenerated_VersionMgr_vsArr_m242098330 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VersionMgrGenerated::VersionMgr_vsDic(JSVCall)
extern "C"  void VersionMgrGenerated_VersionMgr_vsDic_m2569742109 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VersionMgrGenerated::VersionMgr_currentVS(JSVCall)
extern "C"  void VersionMgrGenerated_VersionMgr_currentVS_m3354270024 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VersionMgrGenerated::VersionMgr_isSame(JSVCall)
extern "C"  void VersionMgrGenerated_VersionMgr_isSame_m3870348510 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VersionMgrGenerated::VersionMgr_isReview(JSVCall)
extern "C"  void VersionMgrGenerated_VersionMgr_isReview_m2340423340 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VersionMgrGenerated::VersionMgr_isHideGuide(JSVCall)
extern "C"  void VersionMgrGenerated_VersionMgr_isHideGuide_m719123886 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VersionMgrGenerated::VersionMgr_GetvsInfoByName__String(JSVCall,System.Int32)
extern "C"  bool VersionMgrGenerated_VersionMgr_GetvsInfoByName__String_m4231041945 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VersionMgrGenerated::VersionMgr_Init__String(JSVCall,System.Int32)
extern "C"  bool VersionMgrGenerated_VersionMgr_Init__String_m1537393214 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Action VersionMgrGenerated::VersionMgr_LoadAllVSCfg_GetDelegate_member2_arg1(CSRepresentedObject)
extern "C"  Action_t3771233898 * VersionMgrGenerated_VersionMgr_LoadAllVSCfg_GetDelegate_member2_arg1_m412195480 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VersionMgrGenerated::VersionMgr_LoadAllVSCfg__Int32__Action(JSVCall,System.Int32)
extern "C"  bool VersionMgrGenerated_VersionMgr_LoadAllVSCfg__Int32__Action_m2228055453 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VersionMgrGenerated::VersionMgr_NeedDownloadSize__VersionInfo(JSVCall,System.Int32)
extern "C"  bool VersionMgrGenerated_VersionMgr_NeedDownloadSize__VersionInfo_m1677010988 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VersionMgrGenerated::__Register()
extern "C"  void VersionMgrGenerated___Register_m1155348779 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Action VersionMgrGenerated::<VersionMgr_LoadAllVSCfg__Int32__Action>m__2FA()
extern "C"  Action_t3771233898 * VersionMgrGenerated_U3CVersionMgr_LoadAllVSCfg__Int32__ActionU3Em__2FA_m3816898451 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VersionMgrGenerated::ilo_setStringS1(System.Int32,System.String)
extern "C"  void VersionMgrGenerated_ilo_setStringS1_m4179495397 (Il2CppObject * __this /* static, unused */, int32_t ___e0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VersionMgrGenerated::ilo_getStringS2(System.Int32)
extern "C"  String_t* VersionMgrGenerated_ilo_getStringS2_m3906021121 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VersionMgrGenerated::ilo_get_isHide20003(VersionMgr)
extern "C"  bool VersionMgrGenerated_ilo_get_isHide20003_m648623995 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VersionMgrGenerated::ilo_get_loadingbgName4(VersionMgr)
extern "C"  String_t* VersionMgrGenerated_ilo_get_loadingbgName4_m2507040969 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionInfo[] VersionMgrGenerated::ilo_get_vsArr5(VersionMgr)
extern "C"  VersionInfoU5BU5D_t1155590563* VersionMgrGenerated_ilo_get_vsArr5_m3471992650 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,VersionInfo> VersionMgrGenerated::ilo_get_vsDic6(VersionMgr)
extern "C"  Dictionary_2_t3177056456 * VersionMgrGenerated_ilo_get_vsDic6_m2739525077 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object VersionMgrGenerated::ilo_getObject7(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * VersionMgrGenerated_ilo_getObject7_m3164508111 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VersionMgrGenerated::ilo_set_currentVS8(VersionMgr,VersionInfo)
extern "C"  void VersionMgrGenerated_ilo_set_currentVS8_m1921319456 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, VersionInfo_t2356638086 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VersionMgrGenerated::ilo_get_isReview9(VersionMgr)
extern "C"  bool VersionMgrGenerated_ilo_get_isReview9_m2817174761 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VersionMgrGenerated::ilo_setBooleanS10(System.Int32,System.Boolean)
extern "C"  void VersionMgrGenerated_ilo_setBooleanS10_m1647335513 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionInfo VersionMgrGenerated::ilo_GetvsInfoByName11(VersionMgr,System.String)
extern "C"  VersionInfo_t2356638086 * VersionMgrGenerated_ilo_GetvsInfoByName11_m2115541523 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, String_t* ___name1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VersionMgrGenerated::ilo_setInt6412(System.Int32,System.Int64)
extern "C"  void VersionMgrGenerated_ilo_setInt6412_m1609906944 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int64_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Action VersionMgrGenerated::ilo_VersionMgr_LoadAllVSCfg_GetDelegate_member2_arg113(CSRepresentedObject)
extern "C"  Action_t3771233898 * VersionMgrGenerated_ilo_VersionMgr_LoadAllVSCfg_GetDelegate_member2_arg113_m353366371 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
