﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_EventSystems_IPointerClickHandlerGenerated
struct UnityEngine_EventSystems_IPointerClickHandlerGenerated_t2267512780;
// JSVCall
struct JSVCall_t3708497963;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"

// System.Void UnityEngine_EventSystems_IPointerClickHandlerGenerated::.ctor()
extern "C"  void UnityEngine_EventSystems_IPointerClickHandlerGenerated__ctor_m2279823951 (UnityEngine_EventSystems_IPointerClickHandlerGenerated_t2267512780 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_EventSystems_IPointerClickHandlerGenerated::IPointerClickHandler_OnPointerClick__PointerEventData(JSVCall,System.Int32)
extern "C"  bool UnityEngine_EventSystems_IPointerClickHandlerGenerated_IPointerClickHandler_OnPointerClick__PointerEventData_m3642456995 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_IPointerClickHandlerGenerated::__Register()
extern "C"  void UnityEngine_EventSystems_IPointerClickHandlerGenerated___Register_m1855273944 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
