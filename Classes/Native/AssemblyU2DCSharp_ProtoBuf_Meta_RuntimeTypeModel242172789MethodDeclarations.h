﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ProtoBuf.Meta.RuntimeTypeModel
struct RuntimeTypeModel_t242172789;
// ProtoBuf.Meta.LockContentedEventHandler
struct LockContentedEventHandler_t2719505327;
// System.Collections.IEnumerable
struct IEnumerable_t3464557803;
// System.String
struct String_t;
// System.Type
struct Type_t;
// ProtoBuf.Meta.BasicList
struct BasicList_t528018366;
// ProtoBuf.Meta.MetaType
struct MetaType_t448283965;
// System.Object
struct Il2CppObject;
// ProtoBuf.Serializers.IProtoSerializer
struct IProtoSerializer_t3033312651;
// ProtoBuf.ProtoWriter
struct ProtoWriter_t4117914721;
// ProtoBuf.ProtoReader
struct ProtoReader_t3962509489;
// ProtoBuf.Serializers.EnumSerializer/EnumPair[]
struct EnumPairU5BU5D_t754245438;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Text.StringBuilder
struct StringBuilder_t243639308;
// System.Reflection.ConstructorInfo
struct ConstructorInfo_t4136801618;
// System.Reflection.MemberInfo[]
struct MemberInfoU5BU5D_t674955999;
// ProtoBuf.Meta.ValueMember
struct ValueMember_t110398141;
// ProtoBuf.Meta.RuntimeTypeModel/BasicType
struct BasicType_t2317730454;
// ProtoBuf.Meta.BasicList/MatchPredicate
struct MatchPredicate_t1402151099;
// ProtoBuf.Meta.TypeModel
struct TypeModel_t2730011105;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ProtoBuf_Meta_LockContentedEvent2719505327.h"
#include "mscorlib_System_Type2863145774.h"
#include "AssemblyU2DCSharp_ProtoBuf_Meta_BasicList528018366.h"
#include "AssemblyU2DCSharp_ProtoBuf_Meta_MetaType448283965.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_ProtoBuf_ProtoWriter4117914721.h"
#include "AssemblyU2DCSharp_ProtoBuf_ProtoReader3962509489.h"
#include "AssemblyU2DCSharp_ProtoBuf_DataFormat274207885.h"
#include "mscorlib_System_Reflection_MethodInfo318736065.h"
#include "AssemblyU2DCSharp_ProtoBuf_Meta_RuntimeTypeModel242172789.h"
#include "AssemblyU2DCSharp_ProtoBuf_Meta_BasicList_NodeEnum3394063023.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Text_StringBuilder243639308.h"
#include "AssemblyU2DCSharp_ProtoBuf_WireType2355646059.h"
#include "AssemblyU2DCSharp_ProtoBuf_Meta_ValueMember110398141.h"
#include "AssemblyU2DCSharp_ProtoBuf_Meta_RuntimeTypeModel_B2317730454.h"
#include "AssemblyU2DCSharp_ProtoBuf_Meta_BasicList_MatchPre1402151099.h"
#include "AssemblyU2DCSharp_ProtoBuf_Meta_TypeModel2730011105.h"

// System.Void ProtoBuf.Meta.RuntimeTypeModel::.ctor(System.Boolean)
extern "C"  void RuntimeTypeModel__ctor_m41643125 (RuntimeTypeModel_t242172789 * __this, bool ___isDefault0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.RuntimeTypeModel::.cctor()
extern "C"  void RuntimeTypeModel__cctor_m35360751 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.RuntimeTypeModel::add_LockContended(ProtoBuf.Meta.LockContentedEventHandler)
extern "C"  void RuntimeTypeModel_add_LockContended_m3768509920 (RuntimeTypeModel_t242172789 * __this, LockContentedEventHandler_t2719505327 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.RuntimeTypeModel::remove_LockContended(ProtoBuf.Meta.LockContentedEventHandler)
extern "C"  void RuntimeTypeModel_remove_LockContended_m1237519283 (RuntimeTypeModel_t242172789 * __this, LockContentedEventHandler_t2719505327 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Meta.RuntimeTypeModel::GetOption(System.Byte)
extern "C"  bool RuntimeTypeModel_GetOption_m4052331768 (RuntimeTypeModel_t242172789 * __this, uint8_t ___option0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.RuntimeTypeModel::SetOption(System.Byte,System.Boolean)
extern "C"  void RuntimeTypeModel_SetOption_m1850687429 (RuntimeTypeModel_t242172789 * __this, uint8_t ___option0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Meta.RuntimeTypeModel::get_InferTagFromNameDefault()
extern "C"  bool RuntimeTypeModel_get_InferTagFromNameDefault_m1804382623 (RuntimeTypeModel_t242172789 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.RuntimeTypeModel::set_InferTagFromNameDefault(System.Boolean)
extern "C"  void RuntimeTypeModel_set_InferTagFromNameDefault_m2476252438 (RuntimeTypeModel_t242172789 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Meta.RuntimeTypeModel::get_AutoAddProtoContractTypesOnly()
extern "C"  bool RuntimeTypeModel_get_AutoAddProtoContractTypesOnly_m1092513692 (RuntimeTypeModel_t242172789 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.RuntimeTypeModel::set_AutoAddProtoContractTypesOnly(System.Boolean)
extern "C"  void RuntimeTypeModel_set_AutoAddProtoContractTypesOnly_m3697830995 (RuntimeTypeModel_t242172789 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Meta.RuntimeTypeModel::get_UseImplicitZeroDefaults()
extern "C"  bool RuntimeTypeModel_get_UseImplicitZeroDefaults_m1826657989 (RuntimeTypeModel_t242172789 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.RuntimeTypeModel::set_UseImplicitZeroDefaults(System.Boolean)
extern "C"  void RuntimeTypeModel_set_UseImplicitZeroDefaults_m3830024124 (RuntimeTypeModel_t242172789 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Meta.RuntimeTypeModel::get_AllowParseableTypes()
extern "C"  bool RuntimeTypeModel_get_AllowParseableTypes_m3667743700 (RuntimeTypeModel_t242172789 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.RuntimeTypeModel::set_AllowParseableTypes(System.Boolean)
extern "C"  void RuntimeTypeModel_set_AllowParseableTypes_m1268309643 (RuntimeTypeModel_t242172789 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.Meta.RuntimeTypeModel ProtoBuf.Meta.RuntimeTypeModel::get_Default()
extern "C"  RuntimeTypeModel_t242172789 * RuntimeTypeModel_get_Default_m173113300 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerable ProtoBuf.Meta.RuntimeTypeModel::GetTypes()
extern "C"  Il2CppObject * RuntimeTypeModel_GetTypes_m890533709 (RuntimeTypeModel_t242172789 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ProtoBuf.Meta.RuntimeTypeModel::GetSchema(System.Type)
extern "C"  String_t* RuntimeTypeModel_GetSchema_m1285480841 (RuntimeTypeModel_t242172789 * __this, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.RuntimeTypeModel::CascadeDependents(ProtoBuf.Meta.BasicList,ProtoBuf.Meta.MetaType)
extern "C"  void RuntimeTypeModel_CascadeDependents_m131103097 (RuntimeTypeModel_t242172789 * __this, BasicList_t528018366 * ___list0, MetaType_t448283965 * ___metaType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.Meta.MetaType ProtoBuf.Meta.RuntimeTypeModel::get_Item(System.Type)
extern "C"  MetaType_t448283965 * RuntimeTypeModel_get_Item_m2870509887 (RuntimeTypeModel_t242172789 * __this, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.Meta.MetaType ProtoBuf.Meta.RuntimeTypeModel::FindWithoutAdd(System.Type)
extern "C"  MetaType_t448283965 * RuntimeTypeModel_FindWithoutAdd_m1253651401 (RuntimeTypeModel_t242172789 * __this, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Meta.RuntimeTypeModel::MetaTypeFinderImpl(System.Object,System.Object)
extern "C"  bool RuntimeTypeModel_MetaTypeFinderImpl_m368227747 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, Il2CppObject * ___ctx1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Meta.RuntimeTypeModel::BasicTypeFinderImpl(System.Object,System.Object)
extern "C"  bool RuntimeTypeModel_BasicTypeFinderImpl_m3232626156 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, Il2CppObject * ___ctx1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.RuntimeTypeModel::WaitOnLock(ProtoBuf.Meta.MetaType)
extern "C"  void RuntimeTypeModel_WaitOnLock_m2220817440 (RuntimeTypeModel_t242172789 * __this, MetaType_t448283965 * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.Serializers.IProtoSerializer ProtoBuf.Meta.RuntimeTypeModel::TryGetBasicTypeSerializer(System.Type)
extern "C"  Il2CppObject * RuntimeTypeModel_TryGetBasicTypeSerializer_m1483623984 (RuntimeTypeModel_t242172789 * __this, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ProtoBuf.Meta.RuntimeTypeModel::FindOrAddAuto(System.Type,System.Boolean,System.Boolean,System.Boolean)
extern "C"  int32_t RuntimeTypeModel_FindOrAddAuto_m3637149158 (RuntimeTypeModel_t242172789 * __this, Type_t * ___type0, bool ___demand1, bool ___addWithContractOnly2, bool ___addEvenIfAutoDisabled3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.Meta.MetaType ProtoBuf.Meta.RuntimeTypeModel::RecogniseCommonTypes(System.Type)
extern "C"  MetaType_t448283965 * RuntimeTypeModel_RecogniseCommonTypes_m1113803746 (RuntimeTypeModel_t242172789 * __this, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.Meta.MetaType ProtoBuf.Meta.RuntimeTypeModel::Create(System.Type)
extern "C"  MetaType_t448283965 * RuntimeTypeModel_Create_m546386783 (RuntimeTypeModel_t242172789 * __this, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.Meta.MetaType ProtoBuf.Meta.RuntimeTypeModel::Add(System.Type,System.Boolean)
extern "C"  MetaType_t448283965 * RuntimeTypeModel_Add_m1027327049 (RuntimeTypeModel_t242172789 * __this, Type_t * ___type0, bool ___applyDefaultBehaviour1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Meta.RuntimeTypeModel::get_AutoAddMissingTypes()
extern "C"  bool RuntimeTypeModel_get_AutoAddMissingTypes_m2846896772 (RuntimeTypeModel_t242172789 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.RuntimeTypeModel::set_AutoAddMissingTypes(System.Boolean)
extern "C"  void RuntimeTypeModel_set_AutoAddMissingTypes_m1613252923 (RuntimeTypeModel_t242172789 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.RuntimeTypeModel::ThrowIfFrozen()
extern "C"  void RuntimeTypeModel_ThrowIfFrozen_m1463341055 (RuntimeTypeModel_t242172789 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.RuntimeTypeModel::Freeze()
extern "C"  void RuntimeTypeModel_Freeze_m3679477149 (RuntimeTypeModel_t242172789 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ProtoBuf.Meta.RuntimeTypeModel::GetKeyImpl(System.Type)
extern "C"  int32_t RuntimeTypeModel_GetKeyImpl_m1178809692 (RuntimeTypeModel_t242172789 * __this, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ProtoBuf.Meta.RuntimeTypeModel::GetKey(System.Type,System.Boolean,System.Boolean)
extern "C"  int32_t RuntimeTypeModel_GetKey_m3915162364 (RuntimeTypeModel_t242172789 * __this, Type_t * ___type0, bool ___demand1, bool ___getBaseKey2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.RuntimeTypeModel::Serialize(System.Int32,System.Object,ProtoBuf.ProtoWriter)
extern "C"  void RuntimeTypeModel_Serialize_m119322131 (RuntimeTypeModel_t242172789 * __this, int32_t ___key0, Il2CppObject * ___value1, ProtoWriter_t4117914721 * ___dest2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProtoBuf.Meta.RuntimeTypeModel::Deserialize(System.Int32,System.Object,ProtoBuf.ProtoReader)
extern "C"  Il2CppObject * RuntimeTypeModel_Deserialize_m460585721 (RuntimeTypeModel_t242172789 * __this, int32_t ___key0, Il2CppObject * ___value1, ProtoReader_t3962509489 * ___source2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Meta.RuntimeTypeModel::IsPrepared(System.Type)
extern "C"  bool RuntimeTypeModel_IsPrepared_m3107619864 (RuntimeTypeModel_t242172789 * __this, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.Serializers.EnumSerializer/EnumPair[] ProtoBuf.Meta.RuntimeTypeModel::GetEnumMap(System.Type)
extern "C"  EnumPairU5BU5D_t754245438* RuntimeTypeModel_GetEnumMap_m719703034 (RuntimeTypeModel_t242172789 * __this, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ProtoBuf.Meta.RuntimeTypeModel::get_MetadataTimeoutMilliseconds()
extern "C"  int32_t RuntimeTypeModel_get_MetadataTimeoutMilliseconds_m771787465 (RuntimeTypeModel_t242172789 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.RuntimeTypeModel::set_MetadataTimeoutMilliseconds(System.Int32)
extern "C"  void RuntimeTypeModel_set_MetadataTimeoutMilliseconds_m1253172212 (RuntimeTypeModel_t242172789 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.RuntimeTypeModel::TakeLock(System.Int32&)
extern "C"  void RuntimeTypeModel_TakeLock_m1140280899 (RuntimeTypeModel_t242172789 * __this, int32_t* ___opaqueToken0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ProtoBuf.Meta.RuntimeTypeModel::GetContention()
extern "C"  int32_t RuntimeTypeModel_GetContention_m3483940211 (RuntimeTypeModel_t242172789 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.RuntimeTypeModel::AddContention()
extern "C"  void RuntimeTypeModel_AddContention_m1380224524 (RuntimeTypeModel_t242172789 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.RuntimeTypeModel::ReleaseLock(System.Int32)
extern "C"  void RuntimeTypeModel_ReleaseLock_m3527617023 (RuntimeTypeModel_t242172789 * __this, int32_t ___opaqueToken0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.RuntimeTypeModel::ResolveListTypes(System.Type,System.Type&,System.Type&)
extern "C"  void RuntimeTypeModel_ResolveListTypes_m4013905732 (RuntimeTypeModel_t242172789 * __this, Type_t * ___type0, Type_t ** ___itemType1, Type_t ** ___defaultType2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ProtoBuf.Meta.RuntimeTypeModel::GetSchemaTypeName(System.Type,ProtoBuf.DataFormat,System.Boolean,System.Boolean,System.Boolean&)
extern "C"  String_t* RuntimeTypeModel_GetSchemaTypeName_m3533177211 (RuntimeTypeModel_t242172789 * __this, Type_t * ___effectiveType0, int32_t ___dataFormat1, bool ___asReference2, bool ___dynamicType3, bool* ___requiresBclImport4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.RuntimeTypeModel::SetDefaultFactory(System.Reflection.MethodInfo)
extern "C"  void RuntimeTypeModel_SetDefaultFactory_m2995966676 (RuntimeTypeModel_t242172789 * __this, MethodInfo_t * ___methodInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.RuntimeTypeModel::VerifyFactory(System.Reflection.MethodInfo,System.Type)
extern "C"  void RuntimeTypeModel_VerifyFactory_m4205845453 (RuntimeTypeModel_t242172789 * __this, MethodInfo_t * ___factory0, Type_t * ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Meta.RuntimeTypeModel::ilo_GetOption1(ProtoBuf.Meta.RuntimeTypeModel,System.Byte)
extern "C"  bool RuntimeTypeModel_ilo_GetOption1_m1221659483 (Il2CppObject * __this /* static, unused */, RuntimeTypeModel_t242172789 * ____this0, uint8_t ___option1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.RuntimeTypeModel::ilo_SetOption2(ProtoBuf.Meta.RuntimeTypeModel,System.Byte,System.Boolean)
extern "C"  void RuntimeTypeModel_ilo_SetOption2_m265388129 (Il2CppObject * __this /* static, unused */, RuntimeTypeModel_t242172789 * ____this0, uint8_t ___option1, bool ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Meta.RuntimeTypeModel::ilo_Contains3(ProtoBuf.Meta.BasicList,System.Object)
extern "C"  bool RuntimeTypeModel_ilo_Contains3_m594996049 (Il2CppObject * __this /* static, unused */, BasicList_t528018366 * ____this0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Meta.RuntimeTypeModel::ilo_MoveNext4(ProtoBuf.Meta.BasicList/NodeEnumerator&)
extern "C"  bool RuntimeTypeModel_ilo_MoveNext4_m1134596534 (Il2CppObject * __this /* static, unused */, NodeEnumerator_t3394063023 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.Meta.MetaType ProtoBuf.Meta.RuntimeTypeModel::ilo_GetSurrogateOrBaseOrSelf5(ProtoBuf.Meta.MetaType,System.Boolean)
extern "C"  MetaType_t448283965 * RuntimeTypeModel_ilo_GetSurrogateOrBaseOrSelf5_m3254643745 (Il2CppObject * __this /* static, unused */, MetaType_t448283965 * ____this0, bool ___deep1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.RuntimeTypeModel::ilo_CascadeDependents6(ProtoBuf.Meta.RuntimeTypeModel,ProtoBuf.Meta.BasicList,ProtoBuf.Meta.MetaType)
extern "C"  void RuntimeTypeModel_ilo_CascadeDependents6_m2952981137 (Il2CppObject * __this /* static, unused */, RuntimeTypeModel_t242172789 * ____this0, BasicList_t528018366 * ___list1, MetaType_t448283965 * ___metaType2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type ProtoBuf.Meta.RuntimeTypeModel::ilo_get_Type7(ProtoBuf.Meta.MetaType)
extern "C"  Type_t * RuntimeTypeModel_ilo_get_Type7_m2709666270 (Il2CppObject * __this /* static, unused */, MetaType_t448283965 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Meta.RuntimeTypeModel::ilo_IsNullOrEmpty8(System.String)
extern "C"  bool RuntimeTypeModel_ilo_IsNullOrEmpty8_m4223232726 (Il2CppObject * __this /* static, unused */, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.RuntimeTypeModel::ilo_WriteSchema9(ProtoBuf.Meta.MetaType,System.Text.StringBuilder,System.Int32,System.Boolean&)
extern "C"  void RuntimeTypeModel_ilo_WriteSchema9_m1881384213 (Il2CppObject * __this /* static, unused */, MetaType_t448283965 * ____this0, StringBuilder_t243639308 * ___builder1, int32_t ___indent2, bool* ___requiresBclImport3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.Serializers.IProtoSerializer ProtoBuf.Meta.RuntimeTypeModel::ilo_TryGetCoreSerializer10(ProtoBuf.Meta.RuntimeTypeModel,ProtoBuf.DataFormat,System.Type,ProtoBuf.WireType&,System.Boolean,System.Boolean,System.Boolean,System.Boolean)
extern "C"  Il2CppObject * RuntimeTypeModel_ilo_TryGetCoreSerializer10_m1567758670 (Il2CppObject * __this /* static, unused */, RuntimeTypeModel_t242172789 * ___model0, int32_t ___dataFormat1, Type_t * ___type2, int32_t* ___defaultWireType3, bool ___asReference4, bool ___dynamicType5, bool ___overwriteList6, bool ___allowComplexTypes7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ProtoBuf.Meta.RuntimeTypeModel::ilo_Add11(ProtoBuf.Meta.BasicList,System.Object)
extern "C"  int32_t RuntimeTypeModel_ilo_Add11_m3197857662 (Il2CppObject * __this /* static, unused */, BasicList_t528018366 * ____this0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.ConstructorInfo ProtoBuf.Meta.RuntimeTypeModel::ilo_ResolveTupleConstructor12(System.Type,System.Reflection.MemberInfo[]&)
extern "C"  ConstructorInfo_t4136801618 * RuntimeTypeModel_ilo_ResolveTupleConstructor12_m2557382740 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, MemberInfoU5BU5D_t674955999** ___mappedMembers1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ProtoBuf.Meta.RuntimeTypeModel::ilo_FindOrAddAuto13(ProtoBuf.Meta.RuntimeTypeModel,System.Type,System.Boolean,System.Boolean,System.Boolean)
extern "C"  int32_t RuntimeTypeModel_ilo_FindOrAddAuto13_m3162740918 (Il2CppObject * __this /* static, unused */, RuntimeTypeModel_t242172789 * ____this0, Type_t * ___type1, bool ___demand2, bool ___addWithContractOnly3, bool ___addEvenIfAutoDisabled4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProtoBuf.Meta.RuntimeTypeModel::ilo_get_Item14(ProtoBuf.Meta.BasicList,System.Int32)
extern "C"  Il2CppObject * RuntimeTypeModel_ilo_get_Item14_m3430561432 (Il2CppObject * __this /* static, unused */, BasicList_t528018366 * ____this0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerable ProtoBuf.Meta.RuntimeTypeModel::ilo_get_Fields15(ProtoBuf.Meta.MetaType)
extern "C"  Il2CppObject * RuntimeTypeModel_ilo_get_Fields15_m3705334462 (Il2CppObject * __this /* static, unused */, MetaType_t448283965 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type ProtoBuf.Meta.RuntimeTypeModel::ilo_get_ItemType16(ProtoBuf.Meta.ValueMember)
extern "C"  Type_t * RuntimeTypeModel_ilo_get_ItemType16_m3270367839 (Il2CppObject * __this /* static, unused */, ValueMember_t110398141 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.Meta.MetaType ProtoBuf.Meta.RuntimeTypeModel::ilo_GetSurrogateOrSelf17(ProtoBuf.Meta.MetaType)
extern "C"  MetaType_t448283965 * RuntimeTypeModel_ilo_GetSurrogateOrSelf17_m3172363555 (Il2CppObject * __this /* static, unused */, MetaType_t448283965 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type ProtoBuf.Meta.RuntimeTypeModel::ilo_ResolveProxies18(System.Type)
extern "C"  Type_t * RuntimeTypeModel_ilo_ResolveProxies18_m1364562879 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.RuntimeTypeModel::ilo_ReleaseLock19(ProtoBuf.Meta.RuntimeTypeModel,System.Int32)
extern "C"  void RuntimeTypeModel_ilo_ReleaseLock19_m3184266161 (Il2CppObject * __this /* static, unused */, RuntimeTypeModel_t242172789 * ____this0, int32_t ___opaqueToken1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.Serializers.IProtoSerializer ProtoBuf.Meta.RuntimeTypeModel::ilo_get_Serializer20(ProtoBuf.Meta.RuntimeTypeModel/BasicType)
extern "C"  Il2CppObject * RuntimeTypeModel_ilo_get_Serializer20_m4223221646 (Il2CppObject * __this /* static, unused */, BasicType_t2317730454 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ProtoBuf.Meta.RuntimeTypeModel::ilo_IndexOf21(ProtoBuf.Meta.BasicList,ProtoBuf.Meta.BasicList/MatchPredicate,System.Object)
extern "C"  int32_t RuntimeTypeModel_ilo_IndexOf21_m50030800 (Il2CppObject * __this /* static, unused */, BasicList_t528018366 * ____this0, MatchPredicate_t1402151099 * ___predicate1, Il2CppObject * ___ctx2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Meta.RuntimeTypeModel::ilo_get_Pending22(ProtoBuf.Meta.MetaType)
extern "C"  bool RuntimeTypeModel_ilo_get_Pending22_m3846468830 (Il2CppObject * __this /* static, unused */, MetaType_t448283965 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.Meta.MetaType ProtoBuf.Meta.RuntimeTypeModel::ilo_FindWithoutAdd23(ProtoBuf.Meta.RuntimeTypeModel,System.Type)
extern "C"  MetaType_t448283965 * RuntimeTypeModel_ilo_FindWithoutAdd23_m492478010 (Il2CppObject * __this /* static, unused */, RuntimeTypeModel_t242172789 * ____this0, Type_t * ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type ProtoBuf.Meta.RuntimeTypeModel::ilo_GetListItemType24(ProtoBuf.Meta.TypeModel,System.Type)
extern "C"  Type_t * RuntimeTypeModel_ilo_GetListItemType24_m3719268170 (Il2CppObject * __this /* static, unused */, TypeModel_t2730011105 * ___model0, Type_t * ___listType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.Meta.MetaType ProtoBuf.Meta.RuntimeTypeModel::ilo_RecogniseCommonTypes25(ProtoBuf.Meta.RuntimeTypeModel,System.Type)
extern "C"  MetaType_t448283965 * RuntimeTypeModel_ilo_RecogniseCommonTypes25_m2966700035 (Il2CppObject * __this /* static, unused */, RuntimeTypeModel_t242172789 * ____this0, Type_t * ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.Meta.MetaType ProtoBuf.Meta.RuntimeTypeModel::ilo_Create26(ProtoBuf.Meta.RuntimeTypeModel,System.Type)
extern "C"  MetaType_t448283965 * RuntimeTypeModel_ilo_Create26_m28283879 (Il2CppObject * __this /* static, unused */, RuntimeTypeModel_t242172789 * ____this0, Type_t * ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.RuntimeTypeModel::ilo_set_Pending27(ProtoBuf.Meta.MetaType,System.Boolean)
extern "C"  void RuntimeTypeModel_ilo_set_Pending27_m821183738 (Il2CppObject * __this /* static, unused */, MetaType_t448283965 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.RuntimeTypeModel::ilo_ApplyDefaultBehaviour28(ProtoBuf.Meta.MetaType)
extern "C"  void RuntimeTypeModel_ilo_ApplyDefaultBehaviour28_m4234153844 (Il2CppObject * __this /* static, unused */, MetaType_t448283965 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.RuntimeTypeModel::ilo_Write29(ProtoBuf.Serializers.IProtoSerializer,System.Object,ProtoBuf.ProtoWriter)
extern "C"  void RuntimeTypeModel_ilo_Write29_m3722540638 (Il2CppObject * __this /* static, unused */, Il2CppObject * ____this0, Il2CppObject * ___value1, ProtoWriter_t4117914721 * ___dest2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type ProtoBuf.Meta.RuntimeTypeModel::ilo_get_ExpectedType30(ProtoBuf.Serializers.IProtoSerializer)
extern "C"  Type_t * RuntimeTypeModel_ilo_get_ExpectedType30_m743597638 (Il2CppObject * __this /* static, unused */, Il2CppObject * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProtoBuf.Meta.RuntimeTypeModel::ilo_Read31(ProtoBuf.Serializers.IProtoSerializer,System.Object,ProtoBuf.ProtoReader)
extern "C"  Il2CppObject * RuntimeTypeModel_ilo_Read31_m2582698177 (Il2CppObject * __this /* static, unused */, Il2CppObject * ____this0, Il2CppObject * ___value1, ProtoReader_t3962509489 * ___source2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.RuntimeTypeModel::ilo_AddContention32(ProtoBuf.Meta.RuntimeTypeModel)
extern "C"  void RuntimeTypeModel_ilo_AddContention32_m2693156923 (Il2CppObject * __this /* static, unused */, RuntimeTypeModel_t242172789 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.Meta.MetaType ProtoBuf.Meta.RuntimeTypeModel::ilo_get_Item33(ProtoBuf.Meta.RuntimeTypeModel,System.Type)
extern "C"  MetaType_t448283965 * RuntimeTypeModel_ilo_get_Item33_m629311331 (Il2CppObject * __this /* static, unused */, RuntimeTypeModel_t242172789 * ____this0, Type_t * ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Meta.RuntimeTypeModel::ilo_get_IgnoreListHandling34(ProtoBuf.Meta.MetaType)
extern "C"  bool RuntimeTypeModel_ilo_get_IgnoreListHandling34_m3628154183 (Il2CppObject * __this /* static, unused */, MetaType_t448283965 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type ProtoBuf.Meta.RuntimeTypeModel::ilo_MapType35(ProtoBuf.Meta.TypeModel,System.Type)
extern "C"  Type_t * RuntimeTypeModel_ilo_MapType35_m3161279221 (Il2CppObject * __this /* static, unused */, TypeModel_t2730011105 * ____this0, Type_t * ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Meta.RuntimeTypeModel::ilo_IsValueType36(System.Type)
extern "C"  bool RuntimeTypeModel_ilo_IsValueType36_m69654560 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Meta.RuntimeTypeModel::ilo_CheckCallbackParameters37(ProtoBuf.Meta.TypeModel,System.Reflection.MethodInfo)
extern "C"  bool RuntimeTypeModel_ilo_CheckCallbackParameters37_m4014086042 (Il2CppObject * __this /* static, unused */, TypeModel_t2730011105 * ___model0, MethodInfo_t * ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
