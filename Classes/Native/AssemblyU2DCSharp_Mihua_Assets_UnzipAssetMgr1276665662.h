﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<System.Byte[]>
struct List_1_t1333978725;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1375417109;
// System.Collections.Generic.List`1<System.Action`2<System.Byte[],System.String>>
struct List_1_t2810314695;
// System.Collections.Generic.List`1<System.Boolean>
struct List_1_t1844984270;
// System.Object
struct Il2CppObject;

#include "AssemblyU2DCSharp_Singleton_1_gen1529481055.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mihua.Assets.UnzipAssetMgr
struct  UnzipAssetMgr_t1276665662  : public Singleton_1_t1529481055
{
public:
	// System.Collections.Generic.List`1<System.Byte[]> Mihua.Assets.UnzipAssetMgr::byteList
	List_1_t1333978725 * ___byteList_1;
	// System.Collections.Generic.List`1<System.String> Mihua.Assets.UnzipAssetMgr::keyList
	List_1_t1375417109 * ___keyList_2;
	// System.Collections.Generic.List`1<System.Action`2<System.Byte[],System.String>> Mihua.Assets.UnzipAssetMgr::callBackList
	List_1_t2810314695 * ___callBackList_3;
	// System.Collections.Generic.List`1<System.Boolean> Mihua.Assets.UnzipAssetMgr::isCompressList
	List_1_t1844984270 * ___isCompressList_4;
	// System.Object Mihua.Assets.UnzipAssetMgr::lockd
	Il2CppObject * ___lockd_5;

public:
	inline static int32_t get_offset_of_byteList_1() { return static_cast<int32_t>(offsetof(UnzipAssetMgr_t1276665662, ___byteList_1)); }
	inline List_1_t1333978725 * get_byteList_1() const { return ___byteList_1; }
	inline List_1_t1333978725 ** get_address_of_byteList_1() { return &___byteList_1; }
	inline void set_byteList_1(List_1_t1333978725 * value)
	{
		___byteList_1 = value;
		Il2CppCodeGenWriteBarrier(&___byteList_1, value);
	}

	inline static int32_t get_offset_of_keyList_2() { return static_cast<int32_t>(offsetof(UnzipAssetMgr_t1276665662, ___keyList_2)); }
	inline List_1_t1375417109 * get_keyList_2() const { return ___keyList_2; }
	inline List_1_t1375417109 ** get_address_of_keyList_2() { return &___keyList_2; }
	inline void set_keyList_2(List_1_t1375417109 * value)
	{
		___keyList_2 = value;
		Il2CppCodeGenWriteBarrier(&___keyList_2, value);
	}

	inline static int32_t get_offset_of_callBackList_3() { return static_cast<int32_t>(offsetof(UnzipAssetMgr_t1276665662, ___callBackList_3)); }
	inline List_1_t2810314695 * get_callBackList_3() const { return ___callBackList_3; }
	inline List_1_t2810314695 ** get_address_of_callBackList_3() { return &___callBackList_3; }
	inline void set_callBackList_3(List_1_t2810314695 * value)
	{
		___callBackList_3 = value;
		Il2CppCodeGenWriteBarrier(&___callBackList_3, value);
	}

	inline static int32_t get_offset_of_isCompressList_4() { return static_cast<int32_t>(offsetof(UnzipAssetMgr_t1276665662, ___isCompressList_4)); }
	inline List_1_t1844984270 * get_isCompressList_4() const { return ___isCompressList_4; }
	inline List_1_t1844984270 ** get_address_of_isCompressList_4() { return &___isCompressList_4; }
	inline void set_isCompressList_4(List_1_t1844984270 * value)
	{
		___isCompressList_4 = value;
		Il2CppCodeGenWriteBarrier(&___isCompressList_4, value);
	}

	inline static int32_t get_offset_of_lockd_5() { return static_cast<int32_t>(offsetof(UnzipAssetMgr_t1276665662, ___lockd_5)); }
	inline Il2CppObject * get_lockd_5() const { return ___lockd_5; }
	inline Il2CppObject ** get_address_of_lockd_5() { return &___lockd_5; }
	inline void set_lockd_5(Il2CppObject * value)
	{
		___lockd_5 = value;
		Il2CppCodeGenWriteBarrier(&___lockd_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
