﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_serpeMembaw6
struct M_serpeMembaw6_t992743342;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_serpeMembaw6992743342.h"

// System.Void GarbageiOS.M_serpeMembaw6::.ctor()
extern "C"  void M_serpeMembaw6__ctor_m2682054213 (M_serpeMembaw6_t992743342 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_serpeMembaw6::M_peyur0(System.String[],System.Int32)
extern "C"  void M_serpeMembaw6_M_peyur0_m3190850551 (M_serpeMembaw6_t992743342 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_serpeMembaw6::M_normawma1(System.String[],System.Int32)
extern "C"  void M_serpeMembaw6_M_normawma1_m3748829811 (M_serpeMembaw6_t992743342 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_serpeMembaw6::M_jica2(System.String[],System.Int32)
extern "C"  void M_serpeMembaw6_M_jica2_m1761558525 (M_serpeMembaw6_t992743342 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_serpeMembaw6::M_qobeedriJiter3(System.String[],System.Int32)
extern "C"  void M_serpeMembaw6_M_qobeedriJiter3_m1456007920 (M_serpeMembaw6_t992743342 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_serpeMembaw6::M_jerenastel4(System.String[],System.Int32)
extern "C"  void M_serpeMembaw6_M_jerenastel4_m3716014003 (M_serpeMembaw6_t992743342 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_serpeMembaw6::ilo_M_peyur01(GarbageiOS.M_serpeMembaw6,System.String[],System.Int32)
extern "C"  void M_serpeMembaw6_ilo_M_peyur01_m2471770929 (Il2CppObject * __this /* static, unused */, M_serpeMembaw6_t992743342 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
