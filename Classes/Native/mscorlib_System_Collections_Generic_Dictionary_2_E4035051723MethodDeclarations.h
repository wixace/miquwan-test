﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2284652049MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.Int2,Pathfinding.TriangleMeshNode>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m3183628721(__this, ___dictionary0, method) ((  void (*) (Enumerator_t4035051723 *, Dictionary_2_t2717728331 *, const MethodInfo*))Enumerator__ctor_m2839623054_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.Int2,Pathfinding.TriangleMeshNode>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m796926032(__this, method) ((  Il2CppObject * (*) (Enumerator_t4035051723 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m4140471197_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.Int2,Pathfinding.TriangleMeshNode>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2059432548(__this, method) ((  void (*) (Enumerator_t4035051723 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3207216743_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.Int2,Pathfinding.TriangleMeshNode>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1699308973(__this, method) ((  DictionaryEntry_t1751606614  (*) (Enumerator_t4035051723 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1172003166_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.Int2,Pathfinding.TriangleMeshNode>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2892620076(__this, method) ((  Il2CppObject * (*) (Enumerator_t4035051723 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2853299897_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.Int2,Pathfinding.TriangleMeshNode>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2012488894(__this, method) ((  Il2CppObject * (*) (Enumerator_t4035051723 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2880502539_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.Int2,Pathfinding.TriangleMeshNode>::MoveNext()
#define Enumerator_MoveNext_m1900626384(__this, method) ((  bool (*) (Enumerator_t4035051723 *, const MethodInfo*))Enumerator_MoveNext_m14324631_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.Int2,Pathfinding.TriangleMeshNode>::get_Current()
#define Enumerator_get_Current_m1481075680(__this, method) ((  KeyValuePair_2_t2616509037  (*) (Enumerator_t4035051723 *, const MethodInfo*))Enumerator_get_Current_m2428217029_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.Int2,Pathfinding.TriangleMeshNode>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m1224590621(__this, method) ((  Int2_t1974045593  (*) (Enumerator_t4035051723 *, const MethodInfo*))Enumerator_get_CurrentKey_m1838538208_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.Int2,Pathfinding.TriangleMeshNode>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m1072985601(__this, method) ((  TriangleMeshNode_t1626248749 * (*) (Enumerator_t4035051723 *, const MethodInfo*))Enumerator_get_CurrentValue_m231599392_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.Int2,Pathfinding.TriangleMeshNode>::Reset()
#define Enumerator_Reset_m454488707(__this, method) ((  void (*) (Enumerator_t4035051723 *, const MethodInfo*))Enumerator_Reset_m1210538720_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.Int2,Pathfinding.TriangleMeshNode>::VerifyState()
#define Enumerator_VerifyState_m3665630284(__this, method) ((  void (*) (Enumerator_t4035051723 *, const MethodInfo*))Enumerator_VerifyState_m3141731049_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.Int2,Pathfinding.TriangleMeshNode>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m3542391988(__this, method) ((  void (*) (Enumerator_t4035051723 *, const MethodInfo*))Enumerator_VerifyCurrent_m2586400785_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.Int2,Pathfinding.TriangleMeshNode>::Dispose()
#define Enumerator_Dispose_m1200412307(__this, method) ((  void (*) (Enumerator_t4035051723 *, const MethodInfo*))Enumerator_Dispose_m1915001776_gshared)(__this, method)
