﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_DateTime4283661327.h"
#include "mscorlib_System_Decimal1954350631.h"
#include "mscorlib_System_String7231557.h"

// System.Double System.Xml.XQueryConvert::BooleanToDouble(System.Boolean)
extern "C"  double XQueryConvert_BooleanToDouble_m631491486 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Xml.XQueryConvert::BooleanToInt(System.Boolean)
extern "C"  int32_t XQueryConvert_BooleanToInt_m125604721 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Xml.XQueryConvert::BooleanToInteger(System.Boolean)
extern "C"  int64_t XQueryConvert_BooleanToInteger_m90636801 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.XQueryConvert::BooleanToString(System.Boolean)
extern "C"  String_t* XQueryConvert_BooleanToString_m220021790 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.XQueryConvert::DateTimeToString(System.DateTime)
extern "C"  String_t* XQueryConvert_DateTimeToString_m1761709826 (Il2CppObject * __this /* static, unused */, DateTime_t4283661327  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.XQueryConvert::DecimalToBoolean(System.Decimal)
extern "C"  bool XQueryConvert_DecimalToBoolean_m1215196734 (Il2CppObject * __this /* static, unused */, Decimal_t1954350631  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double System.Xml.XQueryConvert::DecimalToDouble(System.Decimal)
extern "C"  double XQueryConvert_DecimalToDouble_m4073719230 (Il2CppObject * __this /* static, unused */, Decimal_t1954350631  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Xml.XQueryConvert::DecimalToInt(System.Decimal)
extern "C"  int32_t XQueryConvert_DecimalToInt_m2253876703 (Il2CppObject * __this /* static, unused */, Decimal_t1954350631  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Xml.XQueryConvert::DecimalToInteger(System.Decimal)
extern "C"  int64_t XQueryConvert_DecimalToInteger_m3986737391 (Il2CppObject * __this /* static, unused */, Decimal_t1954350631  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.XQueryConvert::DecimalToString(System.Decimal)
extern "C"  String_t* XQueryConvert_DecimalToString_m3662249534 (Il2CppObject * __this /* static, unused */, Decimal_t1954350631  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.XQueryConvert::DoubleToBoolean(System.Double)
extern "C"  bool XQueryConvert_DoubleToBoolean_m1770524536 (Il2CppObject * __this /* static, unused */, double ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Xml.XQueryConvert::DoubleToInt(System.Double)
extern "C"  int32_t XQueryConvert_DoubleToInt_m771455275 (Il2CppObject * __this /* static, unused */, double ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Xml.XQueryConvert::DoubleToInteger(System.Double)
extern "C"  int64_t XQueryConvert_DoubleToInteger_m100222973 (Il2CppObject * __this /* static, unused */, double ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.XQueryConvert::DoubleToString(System.Double)
extern "C"  String_t* XQueryConvert_DoubleToString_m2120243734 (Il2CppObject * __this /* static, unused */, double ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.XQueryConvert::FloatToBoolean(System.Single)
extern "C"  bool XQueryConvert_FloatToBoolean_m369631782 (Il2CppObject * __this /* static, unused */, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double System.Xml.XQueryConvert::FloatToDouble(System.Single)
extern "C"  double XQueryConvert_FloatToDouble_m509515120 (Il2CppObject * __this /* static, unused */, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Xml.XQueryConvert::FloatToInt(System.Single)
extern "C"  int32_t XQueryConvert_FloatToInt_m1411049829 (Il2CppObject * __this /* static, unused */, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Xml.XQueryConvert::FloatToInteger(System.Single)
extern "C"  int64_t XQueryConvert_FloatToInteger_m3987810965 (Il2CppObject * __this /* static, unused */, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.XQueryConvert::FloatToString(System.Single)
extern "C"  String_t* XQueryConvert_FloatToString_m3185179888 (Il2CppObject * __this /* static, unused */, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.XQueryConvert::IntegerToBoolean(System.Int64)
extern "C"  bool XQueryConvert_IntegerToBoolean_m4117817109 (Il2CppObject * __this /* static, unused */, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double System.Xml.XQueryConvert::IntegerToDouble(System.Int64)
extern "C"  double XQueryConvert_IntegerToDouble_m1976022351 (Il2CppObject * __this /* static, unused */, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Xml.XQueryConvert::IntegerToInt(System.Int64)
extern "C"  int32_t XQueryConvert_IntegerToInt_m83561590 (Il2CppObject * __this /* static, unused */, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.XQueryConvert::IntegerToString(System.Int64)
extern "C"  String_t* XQueryConvert_IntegerToString_m2252688847 (Il2CppObject * __this /* static, unused */, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.XQueryConvert::IntToBoolean(System.Int32)
extern "C"  bool XQueryConvert_IntToBoolean_m1128496963 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double System.Xml.XQueryConvert::IntToDouble(System.Int32)
extern "C"  double XQueryConvert_IntToDouble_m1748663999 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.XQueryConvert::IntToString(System.Int32)
extern "C"  String_t* XQueryConvert_IntToString_m3337893695 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.XQueryConvert::StringToBoolean(System.String)
extern "C"  bool XQueryConvert_StringToBoolean_m787208056 (Il2CppObject * __this /* static, unused */, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.Xml.XQueryConvert::StringToDateTime(System.String)
extern "C"  DateTime_t4283661327  XQueryConvert_StringToDateTime_m4151105238 (Il2CppObject * __this /* static, unused */, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double System.Xml.XQueryConvert::StringToDouble(System.String)
extern "C"  double XQueryConvert_StringToDouble_m1726683542 (Il2CppObject * __this /* static, unused */, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Xml.XQueryConvert::StringToInt(System.String)
extern "C"  int32_t XQueryConvert_StringToInt_m974872875 (Il2CppObject * __this /* static, unused */, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Xml.XQueryConvert::StringToInteger(System.String)
extern "C"  int64_t XQueryConvert_StringToInteger_m3411873789 (Il2CppObject * __this /* static, unused */, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
