﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._fae995a476d99337380fd567b994a551
struct _fae995a476d99337380fd567b994a551_t4088872568;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__fae995a476d99337380fd5674088872568.h"

// System.Void Little._fae995a476d99337380fd567b994a551::.ctor()
extern "C"  void _fae995a476d99337380fd567b994a551__ctor_m1371451061 (_fae995a476d99337380fd567b994a551_t4088872568 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._fae995a476d99337380fd567b994a551::_fae995a476d99337380fd567b994a551m2(System.Int32)
extern "C"  int32_t _fae995a476d99337380fd567b994a551__fae995a476d99337380fd567b994a551m2_m3144597145 (_fae995a476d99337380fd567b994a551_t4088872568 * __this, int32_t ____fae995a476d99337380fd567b994a551a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._fae995a476d99337380fd567b994a551::_fae995a476d99337380fd567b994a551m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _fae995a476d99337380fd567b994a551__fae995a476d99337380fd567b994a551m_m115509309 (_fae995a476d99337380fd567b994a551_t4088872568 * __this, int32_t ____fae995a476d99337380fd567b994a551a0, int32_t ____fae995a476d99337380fd567b994a551471, int32_t ____fae995a476d99337380fd567b994a551c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._fae995a476d99337380fd567b994a551::ilo__fae995a476d99337380fd567b994a551m21(Little._fae995a476d99337380fd567b994a551,System.Int32)
extern "C"  int32_t _fae995a476d99337380fd567b994a551_ilo__fae995a476d99337380fd567b994a551m21_m2482969407 (Il2CppObject * __this /* static, unused */, _fae995a476d99337380fd567b994a551_t4088872568 * ____this0, int32_t ____fae995a476d99337380fd567b994a551a1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
