﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


// System.Action
struct Action_t3771233898;
// System.Func`2<System.Object,System.Object>
struct Func_2_t184564025;
// System.Action`2<System.Byte[],System.String>
struct Action_2_t1442129143;

#include "mscorlib_System_Array1146569071.h"
#include "System_Core_System_Collections_Generic_HashSet_1_L2122599155.h"
#include "System_Core_System_Action3771233898.h"
#include "System_Core_System_Collections_Generic_HashSet_1_L2270362450.h"
#include "System_Core_System_Func_2_gen184564025.h"
#include "System_Core_System_Action_2_gen1442129143.h"

#pragma once
// System.Collections.Generic.HashSet`1/Link<System.Object>[]
struct LinkU5BU5D_t3085427682  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Link_t2122599155  m_Items[1];

public:
	inline Link_t2122599155  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Link_t2122599155 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Link_t2122599155  value)
	{
		m_Items[index] = value;
	}
};
// System.Action[]
struct ActionU5BU5D_t1643143343  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Action_t3771233898 * m_Items[1];

public:
	inline Action_t3771233898 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Action_t3771233898 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Action_t3771233898 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.HashSet`1/Link<Pathfinding.GraphNode>[]
struct LinkU5BU5D_t2904220455  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Link_t2270362450  m_Items[1];

public:
	inline Link_t2270362450  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Link_t2270362450 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Link_t2270362450  value)
	{
		m_Items[index] = value;
	}
};
// System.Func`2<System.Object,System.Object>[]
struct Func_2U5BU5D_t2870019524  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Func_2_t184564025 * m_Items[1];

public:
	inline Func_2_t184564025 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Func_2_t184564025 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Func_2_t184564025 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Action`2<System.Byte[],System.String>[]
struct Action_2U5BU5D_t3343969614  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Action_2_t1442129143 * m_Items[1];

public:
	inline Action_2_t1442129143 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Action_2_t1442129143 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Action_2_t1442129143 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
