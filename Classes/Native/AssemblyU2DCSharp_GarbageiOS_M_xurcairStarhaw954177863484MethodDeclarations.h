﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_xurcairStarhaw95
struct M_xurcairStarhaw95_t4177863484;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_xurcairStarhaw954177863484.h"

// System.Void GarbageiOS.M_xurcairStarhaw95::.ctor()
extern "C"  void M_xurcairStarhaw95__ctor_m332217975 (M_xurcairStarhaw95_t4177863484 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_xurcairStarhaw95::M_kaylajel0(System.String[],System.Int32)
extern "C"  void M_xurcairStarhaw95_M_kaylajel0_m323068049 (M_xurcairStarhaw95_t4177863484 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_xurcairStarhaw95::M_coumallTormu1(System.String[],System.Int32)
extern "C"  void M_xurcairStarhaw95_M_coumallTormu1_m2349241225 (M_xurcairStarhaw95_t4177863484 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_xurcairStarhaw95::M_sistiTewhefoo2(System.String[],System.Int32)
extern "C"  void M_xurcairStarhaw95_M_sistiTewhefoo2_m1552581587 (M_xurcairStarhaw95_t4177863484 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_xurcairStarhaw95::M_mascairpe3(System.String[],System.Int32)
extern "C"  void M_xurcairStarhaw95_M_mascairpe3_m3988577902 (M_xurcairStarhaw95_t4177863484 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_xurcairStarhaw95::M_pikairmur4(System.String[],System.Int32)
extern "C"  void M_xurcairStarhaw95_M_pikairmur4_m2345024984 (M_xurcairStarhaw95_t4177863484 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_xurcairStarhaw95::M_feasowcaJerreejear5(System.String[],System.Int32)
extern "C"  void M_xurcairStarhaw95_M_feasowcaJerreejear5_m1771103121 (M_xurcairStarhaw95_t4177863484 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_xurcairStarhaw95::M_bumochere6(System.String[],System.Int32)
extern "C"  void M_xurcairStarhaw95_M_bumochere6_m686268526 (M_xurcairStarhaw95_t4177863484 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_xurcairStarhaw95::M_parkurKiswhorsoo7(System.String[],System.Int32)
extern "C"  void M_xurcairStarhaw95_M_parkurKiswhorsoo7_m1932293824 (M_xurcairStarhaw95_t4177863484 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_xurcairStarhaw95::ilo_M_kaylajel01(GarbageiOS.M_xurcairStarhaw95,System.String[],System.Int32)
extern "C"  void M_xurcairStarhaw95_ilo_M_kaylajel01_m2304842603 (Il2CppObject * __this /* static, unused */, M_xurcairStarhaw95_t4177863484 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_xurcairStarhaw95::ilo_M_coumallTormu12(GarbageiOS.M_xurcairStarhaw95,System.String[],System.Int32)
extern "C"  void M_xurcairStarhaw95_ilo_M_coumallTormu12_m3060373620 (Il2CppObject * __this /* static, unused */, M_xurcairStarhaw95_t4177863484 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_xurcairStarhaw95::ilo_M_mascairpe33(GarbageiOS.M_xurcairStarhaw95,System.String[],System.Int32)
extern "C"  void M_xurcairStarhaw95_ilo_M_mascairpe33_m694750954 (Il2CppObject * __this /* static, unused */, M_xurcairStarhaw95_t4177863484 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_xurcairStarhaw95::ilo_M_pikairmur44(GarbageiOS.M_xurcairStarhaw95,System.String[],System.Int32)
extern "C"  void M_xurcairStarhaw95_ilo_M_pikairmur44_m274981569 (Il2CppObject * __this /* static, unused */, M_xurcairStarhaw95_t4177863484 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_xurcairStarhaw95::ilo_M_feasowcaJerreejear55(GarbageiOS.M_xurcairStarhaw95,System.String[],System.Int32)
extern "C"  void M_xurcairStarhaw95_ilo_M_feasowcaJerreejear55_m1118787247 (Il2CppObject * __this /* static, unused */, M_xurcairStarhaw95_t4177863484 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
