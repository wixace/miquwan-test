﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_zallsemmaJemitar141
struct M_zallsemmaJemitar141_t3421718408;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_zallsemmaJemitar1413421718408.h"

// System.Void GarbageiOS.M_zallsemmaJemitar141::.ctor()
extern "C"  void M_zallsemmaJemitar141__ctor_m3953104091 (M_zallsemmaJemitar141_t3421718408 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_zallsemmaJemitar141::M_monaMootu0(System.String[],System.Int32)
extern "C"  void M_zallsemmaJemitar141_M_monaMootu0_m1658518761 (M_zallsemmaJemitar141_t3421718408 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_zallsemmaJemitar141::M_werefoolair1(System.String[],System.Int32)
extern "C"  void M_zallsemmaJemitar141_M_werefoolair1_m263018336 (M_zallsemmaJemitar141_t3421718408 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_zallsemmaJemitar141::M_jicaiYemel2(System.String[],System.Int32)
extern "C"  void M_zallsemmaJemitar141_M_jicaiYemel2_m480968980 (M_zallsemmaJemitar141_t3421718408 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_zallsemmaJemitar141::ilo_M_monaMootu01(GarbageiOS.M_zallsemmaJemitar141,System.String[],System.Int32)
extern "C"  void M_zallsemmaJemitar141_ilo_M_monaMootu01_m703131247 (Il2CppObject * __this /* static, unused */, M_zallsemmaJemitar141_t3421718408 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_zallsemmaJemitar141::ilo_M_werefoolair12(GarbageiOS.M_zallsemmaJemitar141,System.String[],System.Int32)
extern "C"  void M_zallsemmaJemitar141_ilo_M_werefoolair12_m2233880133 (Il2CppObject * __this /* static, unused */, M_zallsemmaJemitar141_t3421718408 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
