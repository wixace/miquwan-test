﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ICSDataGenerated
struct ICSDataGenerated_t3876350732;
// JSVCall
struct JSVCall_t3708497963;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"

// System.Void ICSDataGenerated::.ctor()
extern "C"  void ICSDataGenerated__ctor_m1417832719 (ICSDataGenerated_t3876350732 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSDataGenerated::ICSData_LoadData__String(JSVCall,System.Int32)
extern "C"  bool ICSDataGenerated_ICSData_LoadData__String_m2570081814 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSDataGenerated::ICSData_UnLoadData(JSVCall,System.Int32)
extern "C"  bool ICSDataGenerated_ICSData_UnLoadData_m2038269118 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSDataGenerated::__Register()
extern "C"  void ICSDataGenerated___Register_m3637601560 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
