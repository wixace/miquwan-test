﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_secouGeesis20
struct  M_secouGeesis20_t3296485195  : public Il2CppObject
{
public:
	// System.UInt32 GarbageiOS.M_secouGeesis20::_mesea
	uint32_t ____mesea_0;
	// System.Int32 GarbageiOS.M_secouGeesis20::_pihall
	int32_t ____pihall_1;
	// System.Int32 GarbageiOS.M_secouGeesis20::_xara
	int32_t ____xara_2;
	// System.Boolean GarbageiOS.M_secouGeesis20::_xetallLikai
	bool ____xetallLikai_3;
	// System.Boolean GarbageiOS.M_secouGeesis20::_sachu
	bool ____sachu_4;
	// System.Int32 GarbageiOS.M_secouGeesis20::_drurtiSiserejur
	int32_t ____drurtiSiserejur_5;
	// System.Single GarbageiOS.M_secouGeesis20::_nayzepa
	float ____nayzepa_6;
	// System.Int32 GarbageiOS.M_secouGeesis20::_salpalcem
	int32_t ____salpalcem_7;
	// System.String GarbageiOS.M_secouGeesis20::_mairbaCawru
	String_t* ____mairbaCawru_8;
	// System.Single GarbageiOS.M_secouGeesis20::_fearcoDenar
	float ____fearcoDenar_9;
	// System.String GarbageiOS.M_secouGeesis20::_dosipere
	String_t* ____dosipere_10;

public:
	inline static int32_t get_offset_of__mesea_0() { return static_cast<int32_t>(offsetof(M_secouGeesis20_t3296485195, ____mesea_0)); }
	inline uint32_t get__mesea_0() const { return ____mesea_0; }
	inline uint32_t* get_address_of__mesea_0() { return &____mesea_0; }
	inline void set__mesea_0(uint32_t value)
	{
		____mesea_0 = value;
	}

	inline static int32_t get_offset_of__pihall_1() { return static_cast<int32_t>(offsetof(M_secouGeesis20_t3296485195, ____pihall_1)); }
	inline int32_t get__pihall_1() const { return ____pihall_1; }
	inline int32_t* get_address_of__pihall_1() { return &____pihall_1; }
	inline void set__pihall_1(int32_t value)
	{
		____pihall_1 = value;
	}

	inline static int32_t get_offset_of__xara_2() { return static_cast<int32_t>(offsetof(M_secouGeesis20_t3296485195, ____xara_2)); }
	inline int32_t get__xara_2() const { return ____xara_2; }
	inline int32_t* get_address_of__xara_2() { return &____xara_2; }
	inline void set__xara_2(int32_t value)
	{
		____xara_2 = value;
	}

	inline static int32_t get_offset_of__xetallLikai_3() { return static_cast<int32_t>(offsetof(M_secouGeesis20_t3296485195, ____xetallLikai_3)); }
	inline bool get__xetallLikai_3() const { return ____xetallLikai_3; }
	inline bool* get_address_of__xetallLikai_3() { return &____xetallLikai_3; }
	inline void set__xetallLikai_3(bool value)
	{
		____xetallLikai_3 = value;
	}

	inline static int32_t get_offset_of__sachu_4() { return static_cast<int32_t>(offsetof(M_secouGeesis20_t3296485195, ____sachu_4)); }
	inline bool get__sachu_4() const { return ____sachu_4; }
	inline bool* get_address_of__sachu_4() { return &____sachu_4; }
	inline void set__sachu_4(bool value)
	{
		____sachu_4 = value;
	}

	inline static int32_t get_offset_of__drurtiSiserejur_5() { return static_cast<int32_t>(offsetof(M_secouGeesis20_t3296485195, ____drurtiSiserejur_5)); }
	inline int32_t get__drurtiSiserejur_5() const { return ____drurtiSiserejur_5; }
	inline int32_t* get_address_of__drurtiSiserejur_5() { return &____drurtiSiserejur_5; }
	inline void set__drurtiSiserejur_5(int32_t value)
	{
		____drurtiSiserejur_5 = value;
	}

	inline static int32_t get_offset_of__nayzepa_6() { return static_cast<int32_t>(offsetof(M_secouGeesis20_t3296485195, ____nayzepa_6)); }
	inline float get__nayzepa_6() const { return ____nayzepa_6; }
	inline float* get_address_of__nayzepa_6() { return &____nayzepa_6; }
	inline void set__nayzepa_6(float value)
	{
		____nayzepa_6 = value;
	}

	inline static int32_t get_offset_of__salpalcem_7() { return static_cast<int32_t>(offsetof(M_secouGeesis20_t3296485195, ____salpalcem_7)); }
	inline int32_t get__salpalcem_7() const { return ____salpalcem_7; }
	inline int32_t* get_address_of__salpalcem_7() { return &____salpalcem_7; }
	inline void set__salpalcem_7(int32_t value)
	{
		____salpalcem_7 = value;
	}

	inline static int32_t get_offset_of__mairbaCawru_8() { return static_cast<int32_t>(offsetof(M_secouGeesis20_t3296485195, ____mairbaCawru_8)); }
	inline String_t* get__mairbaCawru_8() const { return ____mairbaCawru_8; }
	inline String_t** get_address_of__mairbaCawru_8() { return &____mairbaCawru_8; }
	inline void set__mairbaCawru_8(String_t* value)
	{
		____mairbaCawru_8 = value;
		Il2CppCodeGenWriteBarrier(&____mairbaCawru_8, value);
	}

	inline static int32_t get_offset_of__fearcoDenar_9() { return static_cast<int32_t>(offsetof(M_secouGeesis20_t3296485195, ____fearcoDenar_9)); }
	inline float get__fearcoDenar_9() const { return ____fearcoDenar_9; }
	inline float* get_address_of__fearcoDenar_9() { return &____fearcoDenar_9; }
	inline void set__fearcoDenar_9(float value)
	{
		____fearcoDenar_9 = value;
	}

	inline static int32_t get_offset_of__dosipere_10() { return static_cast<int32_t>(offsetof(M_secouGeesis20_t3296485195, ____dosipere_10)); }
	inline String_t* get__dosipere_10() const { return ____dosipere_10; }
	inline String_t** get_address_of__dosipere_10() { return &____dosipere_10; }
	inline void set__dosipere_10(String_t* value)
	{
		____dosipere_10 = value;
		Il2CppCodeGenWriteBarrier(&____dosipere_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
