﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PluginOppo
struct  PluginOppo_t1606292883  : public MonoBehaviour_t667441552
{
public:
	// System.String PluginOppo::roleId
	String_t* ___roleId_2;
	// System.String PluginOppo::roleName
	String_t* ___roleName_3;
	// System.String PluginOppo::roleLevel
	String_t* ___roleLevel_4;
	// System.String PluginOppo::serverId
	String_t* ___serverId_5;
	// System.String PluginOppo::serverName
	String_t* ___serverName_6;
	// System.String PluginOppo::tokenId
	String_t* ___tokenId_7;
	// System.String PluginOppo::ssoid
	String_t* ___ssoid_8;
	// System.String PluginOppo::userId
	String_t* ___userId_9;
	// System.String PluginOppo::channelId
	String_t* ___channelId_10;
	// System.Single PluginOppo::lastSdkLoginOkTime
	float ___lastSdkLoginOkTime_11;

public:
	inline static int32_t get_offset_of_roleId_2() { return static_cast<int32_t>(offsetof(PluginOppo_t1606292883, ___roleId_2)); }
	inline String_t* get_roleId_2() const { return ___roleId_2; }
	inline String_t** get_address_of_roleId_2() { return &___roleId_2; }
	inline void set_roleId_2(String_t* value)
	{
		___roleId_2 = value;
		Il2CppCodeGenWriteBarrier(&___roleId_2, value);
	}

	inline static int32_t get_offset_of_roleName_3() { return static_cast<int32_t>(offsetof(PluginOppo_t1606292883, ___roleName_3)); }
	inline String_t* get_roleName_3() const { return ___roleName_3; }
	inline String_t** get_address_of_roleName_3() { return &___roleName_3; }
	inline void set_roleName_3(String_t* value)
	{
		___roleName_3 = value;
		Il2CppCodeGenWriteBarrier(&___roleName_3, value);
	}

	inline static int32_t get_offset_of_roleLevel_4() { return static_cast<int32_t>(offsetof(PluginOppo_t1606292883, ___roleLevel_4)); }
	inline String_t* get_roleLevel_4() const { return ___roleLevel_4; }
	inline String_t** get_address_of_roleLevel_4() { return &___roleLevel_4; }
	inline void set_roleLevel_4(String_t* value)
	{
		___roleLevel_4 = value;
		Il2CppCodeGenWriteBarrier(&___roleLevel_4, value);
	}

	inline static int32_t get_offset_of_serverId_5() { return static_cast<int32_t>(offsetof(PluginOppo_t1606292883, ___serverId_5)); }
	inline String_t* get_serverId_5() const { return ___serverId_5; }
	inline String_t** get_address_of_serverId_5() { return &___serverId_5; }
	inline void set_serverId_5(String_t* value)
	{
		___serverId_5 = value;
		Il2CppCodeGenWriteBarrier(&___serverId_5, value);
	}

	inline static int32_t get_offset_of_serverName_6() { return static_cast<int32_t>(offsetof(PluginOppo_t1606292883, ___serverName_6)); }
	inline String_t* get_serverName_6() const { return ___serverName_6; }
	inline String_t** get_address_of_serverName_6() { return &___serverName_6; }
	inline void set_serverName_6(String_t* value)
	{
		___serverName_6 = value;
		Il2CppCodeGenWriteBarrier(&___serverName_6, value);
	}

	inline static int32_t get_offset_of_tokenId_7() { return static_cast<int32_t>(offsetof(PluginOppo_t1606292883, ___tokenId_7)); }
	inline String_t* get_tokenId_7() const { return ___tokenId_7; }
	inline String_t** get_address_of_tokenId_7() { return &___tokenId_7; }
	inline void set_tokenId_7(String_t* value)
	{
		___tokenId_7 = value;
		Il2CppCodeGenWriteBarrier(&___tokenId_7, value);
	}

	inline static int32_t get_offset_of_ssoid_8() { return static_cast<int32_t>(offsetof(PluginOppo_t1606292883, ___ssoid_8)); }
	inline String_t* get_ssoid_8() const { return ___ssoid_8; }
	inline String_t** get_address_of_ssoid_8() { return &___ssoid_8; }
	inline void set_ssoid_8(String_t* value)
	{
		___ssoid_8 = value;
		Il2CppCodeGenWriteBarrier(&___ssoid_8, value);
	}

	inline static int32_t get_offset_of_userId_9() { return static_cast<int32_t>(offsetof(PluginOppo_t1606292883, ___userId_9)); }
	inline String_t* get_userId_9() const { return ___userId_9; }
	inline String_t** get_address_of_userId_9() { return &___userId_9; }
	inline void set_userId_9(String_t* value)
	{
		___userId_9 = value;
		Il2CppCodeGenWriteBarrier(&___userId_9, value);
	}

	inline static int32_t get_offset_of_channelId_10() { return static_cast<int32_t>(offsetof(PluginOppo_t1606292883, ___channelId_10)); }
	inline String_t* get_channelId_10() const { return ___channelId_10; }
	inline String_t** get_address_of_channelId_10() { return &___channelId_10; }
	inline void set_channelId_10(String_t* value)
	{
		___channelId_10 = value;
		Il2CppCodeGenWriteBarrier(&___channelId_10, value);
	}

	inline static int32_t get_offset_of_lastSdkLoginOkTime_11() { return static_cast<int32_t>(offsetof(PluginOppo_t1606292883, ___lastSdkLoginOkTime_11)); }
	inline float get_lastSdkLoginOkTime_11() const { return ___lastSdkLoginOkTime_11; }
	inline float* get_address_of_lastSdkLoginOkTime_11() { return &___lastSdkLoginOkTime_11; }
	inline void set_lastSdkLoginOkTime_11(float value)
	{
		___lastSdkLoginOkTime_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
