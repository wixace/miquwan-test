﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21573321581MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<UIModelDisplayType,UIModelDisplay>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m652033949(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t3427993095 *, int32_t, UIModelDisplay_t1730520589 *, const MethodInfo*))KeyValuePair_2__ctor_m2016324438_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<UIModelDisplayType,UIModelDisplay>::get_Key()
#define KeyValuePair_2_get_Key_m3161915179(__this, method) ((  int32_t (*) (KeyValuePair_2_t3427993095 *, const MethodInfo*))KeyValuePair_2_get_Key_m2360182738_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UIModelDisplayType,UIModelDisplay>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m530975852(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3427993095 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m598503699_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<UIModelDisplayType,UIModelDisplay>::get_Value()
#define KeyValuePair_2_get_Value_m3025015033(__this, method) ((  UIModelDisplay_t1730520589 * (*) (KeyValuePair_2_t3427993095 *, const MethodInfo*))KeyValuePair_2_get_Value_m1861716754_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UIModelDisplayType,UIModelDisplay>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m2148354668(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3427993095 *, UIModelDisplay_t1730520589 *, const MethodInfo*))KeyValuePair_2_set_Value_m2608006035_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<UIModelDisplayType,UIModelDisplay>::ToString()
#define KeyValuePair_2_ToString_m486608860(__this, method) ((  String_t* (*) (KeyValuePair_2_t3427993095 *, const MethodInfo*))KeyValuePair_2_ToString_m1592821359_gshared)(__this, method)
