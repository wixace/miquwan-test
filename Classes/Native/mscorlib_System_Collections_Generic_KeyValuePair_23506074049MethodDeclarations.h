﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21944668977MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,Pathfinding.Ionic.Zip.ZipEntry>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m1604498655(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t3506074049 *, String_t*, ZipEntry_t2786874973 *, const MethodInfo*))KeyValuePair_2__ctor_m4168265535_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,Pathfinding.Ionic.Zip.ZipEntry>::get_Key()
#define KeyValuePair_2_get_Key_m1029017641(__this, method) ((  String_t* (*) (KeyValuePair_2_t3506074049 *, const MethodInfo*))KeyValuePair_2_get_Key_m3256475977_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,Pathfinding.Ionic.Zip.ZipEntry>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m2805519850(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3506074049 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m1278074762_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,Pathfinding.Ionic.Zip.ZipEntry>::get_Value()
#define KeyValuePair_2_get_Value_m1150846121(__this, method) ((  ZipEntry_t2786874973 * (*) (KeyValuePair_2_t3506074049 *, const MethodInfo*))KeyValuePair_2_get_Value_m3899079597_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,Pathfinding.Ionic.Zip.ZipEntry>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m100869354(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3506074049 *, ZipEntry_t2786874973 *, const MethodInfo*))KeyValuePair_2_set_Value_m2954518154_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,Pathfinding.Ionic.Zip.ZipEntry>::ToString()
#define KeyValuePair_2_ToString_m3670078840(__this, method) ((  String_t* (*) (KeyValuePair_2_t3506074049 *, const MethodInfo*))KeyValuePair_2_ToString_m1313859518_gshared)(__this, method)
