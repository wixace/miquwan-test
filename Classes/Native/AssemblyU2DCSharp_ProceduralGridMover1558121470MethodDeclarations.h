﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ProceduralGridMover
struct ProceduralGridMover_t1558121470;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void ProceduralGridMover::.ctor()
extern "C"  void ProceduralGridMover__ctor_m3638142253 (ProceduralGridMover_t1558121470 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProceduralGridMover::Start()
extern "C"  void ProceduralGridMover_Start_m2585280045 (ProceduralGridMover_t1558121470 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProceduralGridMover::Update()
extern "C"  void ProceduralGridMover_Update_m2840122240 (ProceduralGridMover_t1558121470 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProceduralGridMover::UpdateGraph()
extern "C"  void ProceduralGridMover_UpdateGraph_m3064834960 (ProceduralGridMover_t1558121470 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ProceduralGridMover::UpdateGraphCoroutine()
extern "C"  Il2CppObject * ProceduralGridMover_UpdateGraphCoroutine_m2400635074 (ProceduralGridMover_t1558121470 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single ProceduralGridMover::ilo_SqrMagnitudeXZ1(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  float ProceduralGridMover_ilo_SqrMagnitudeXZ1_m2782017973 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___a0, Vector3_t4282066566  ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
