﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.Profile
struct Profile_t748148363;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_Pathfinding_Profile748148363.h"

// System.Void Pathfinding.Profile::.ctor(System.String)
extern "C"  void Profile__ctor_m1327812982 (Profile_t748148363 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pathfinding.Profile::ControlValue()
extern "C"  int32_t Profile_ControlValue_m2273927166 (Profile_t748148363 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Profile::Start()
extern "C"  void Profile_Start_m1047951884 (Profile_t748148363 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Profile::Stop()
extern "C"  void Profile_Stop_m3082259002 (Profile_t748148363 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Profile::Log()
extern "C"  void Profile_Log_m2863750830 (Profile_t748148363 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Profile::ConsoleLog()
extern "C"  void Profile_ConsoleLog_m2817843365 (Profile_t748148363 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Profile::Stop(System.Int32)
extern "C"  void Profile_Stop_m2666903691 (Profile_t748148363 * __this, int32_t ___control0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Profile::Control(Pathfinding.Profile)
extern "C"  void Profile_Control_m1087710252 (Profile_t748148363 * __this, Profile_t748148363 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Pathfinding.Profile::ToString()
extern "C"  String_t* Profile_ToString_m680194081 (Profile_t748148363 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Pathfinding.Profile::ilo_ToString1(Pathfinding.Profile)
extern "C"  String_t* Profile_ilo_ToString1_m2111768436 (Il2CppObject * __this /* static, unused */, Profile_t748148363 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pathfinding.Profile::ilo_ControlValue2(Pathfinding.Profile)
extern "C"  int32_t Profile_ilo_ControlValue2_m2972650864 (Il2CppObject * __this /* static, unused */, Profile_t748148363 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
