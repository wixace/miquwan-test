﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.EuclideanEmbedding
struct EuclideanEmbedding_t908139023;
// UnityEngine.Transform
struct Transform_t1659122786;
// System.Collections.Generic.List`1<Pathfinding.GraphNode>
struct List_1_t1391797922;
// Pathfinding.NNConstraint
struct NNConstraint_t758567699;
// Pathfinding.NavGraph
struct NavGraph_t1254319713;
// Pathfinding.GraphNodeDelegateCancelable
struct GraphNodeDelegateCancelable_t3591372971;
// Pathfinding.GraphNode
struct GraphNode_t23612370;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Transform1659122786.h"
#include "AssemblyU2DCSharp_Pathfinding_EuclideanEmbedding908139023.h"
#include "AssemblyU2DCSharp_Pathfinding_NavGraph1254319713.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphNodeDelegateCan3591372971.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphNode23612370.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_Pathfinding_Int31974045594.h"

// System.Void Pathfinding.EuclideanEmbedding::.ctor()
extern "C"  void EuclideanEmbedding__ctor_m153410360 (EuclideanEmbedding_t908139023 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Pathfinding.EuclideanEmbedding::GetRandom()
extern "C"  uint32_t EuclideanEmbedding_GetRandom_m783916116 (EuclideanEmbedding_t908139023 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.EuclideanEmbedding::EnsureCapacity(System.Int32)
extern "C"  void EuclideanEmbedding_EnsureCapacity_m2560395701 (EuclideanEmbedding_t908139023 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Pathfinding.EuclideanEmbedding::GetHeuristic(System.Int32,System.Int32)
extern "C"  uint32_t EuclideanEmbedding_GetHeuristic_m1284663115 (EuclideanEmbedding_t908139023 * __this, int32_t ___nodeIndex10, int32_t ___nodeIndex21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.EuclideanEmbedding::GetClosestWalkableNodesToChildrenRecursively(UnityEngine.Transform,System.Collections.Generic.List`1<Pathfinding.GraphNode>)
extern "C"  void EuclideanEmbedding_GetClosestWalkableNodesToChildrenRecursively_m3172085785 (EuclideanEmbedding_t908139023 * __this, Transform_t1659122786 * ___tr0, List_1_t1391797922 * ___nodes1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.EuclideanEmbedding::RecalculatePivots()
extern "C"  void EuclideanEmbedding_RecalculatePivots_m242199738 (EuclideanEmbedding_t908139023 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.EuclideanEmbedding::RecalculateCosts()
extern "C"  void EuclideanEmbedding_RecalculateCosts_m3186884927 (EuclideanEmbedding_t908139023 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.EuclideanEmbedding::OnDrawGizmos()
extern "C"  void EuclideanEmbedding_OnDrawGizmos_m1762305768 (EuclideanEmbedding_t908139023 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.NNConstraint Pathfinding.EuclideanEmbedding::ilo_get_Default1()
extern "C"  NNConstraint_t758567699 * EuclideanEmbedding_ilo_get_Default1_m145543790 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.EuclideanEmbedding::ilo_GetClosestWalkableNodesToChildrenRecursively2(Pathfinding.EuclideanEmbedding,UnityEngine.Transform,System.Collections.Generic.List`1<Pathfinding.GraphNode>)
extern "C"  void EuclideanEmbedding_ilo_GetClosestWalkableNodesToChildrenRecursively2_m670150721 (Il2CppObject * __this /* static, unused */, EuclideanEmbedding_t908139023 * ____this0, Transform_t1659122786 * ___tr1, List_1_t1391797922 * ___nodes2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.EuclideanEmbedding::ilo_GetNodes3(Pathfinding.NavGraph,Pathfinding.GraphNodeDelegateCancelable)
extern "C"  void EuclideanEmbedding_ilo_GetNodes3_m3275446291 (Il2CppObject * __this /* static, unused */, NavGraph_t1254319713 * ____this0, GraphNodeDelegateCancelable_t3591372971 * ___del1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.EuclideanEmbedding::ilo_RecalculatePivots4(Pathfinding.EuclideanEmbedding)
extern "C"  void EuclideanEmbedding_ilo_RecalculatePivots4_m2931889644 (Il2CppObject * __this /* static, unused */, EuclideanEmbedding_t908139023 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.EuclideanEmbedding::ilo_get_Destroyed5(Pathfinding.GraphNode)
extern "C"  bool EuclideanEmbedding_ilo_get_Destroyed5_m478571700 (Il2CppObject * __this /* static, unused */, GraphNode_t23612370 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Pathfinding.EuclideanEmbedding::ilo_op_Explicit6(Pathfinding.Int3)
extern "C"  Vector3_t4282066566  EuclideanEmbedding_ilo_op_Explicit6_m2687735887 (Il2CppObject * __this /* static, unused */, Int3_t1974045594  ___ob0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
