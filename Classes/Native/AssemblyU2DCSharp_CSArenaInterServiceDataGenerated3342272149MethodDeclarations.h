﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CSArenaInterServiceDataGenerated
struct CSArenaInterServiceDataGenerated_t3342272149;
// JSVCall
struct JSVCall_t3708497963;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// CSArenaInterServiceData
struct CSArenaInterServiceData_t1260466202;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_CSArenaInterServiceData1260466202.h"
#include "mscorlib_System_String7231557.h"

// System.Void CSArenaInterServiceDataGenerated::.ctor()
extern "C"  void CSArenaInterServiceDataGenerated__ctor_m2304018342 (CSArenaInterServiceDataGenerated_t3342272149 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CSArenaInterServiceDataGenerated::CSArenaInterServiceData_CSArenaInterServiceData1(JSVCall,System.Int32)
extern "C"  bool CSArenaInterServiceDataGenerated_CSArenaInterServiceData_CSArenaInterServiceData1_m2983493214 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSArenaInterServiceDataGenerated::CSArenaInterServiceData_enemyPlayerData(JSVCall)
extern "C"  void CSArenaInterServiceDataGenerated_CSArenaInterServiceData_enemyPlayerData_m578962197 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSArenaInterServiceDataGenerated::CSArenaInterServiceData_ourPlayerData(JSVCall)
extern "C"  void CSArenaInterServiceDataGenerated_CSArenaInterServiceData_ourPlayerData_m2712485201 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CSArenaInterServiceDataGenerated::CSArenaInterServiceData_LoadData__String(JSVCall,System.Int32)
extern "C"  bool CSArenaInterServiceDataGenerated_CSArenaInterServiceData_LoadData__String_m1146300264 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSArenaInterServiceDataGenerated::__Register()
extern "C"  void CSArenaInterServiceDataGenerated___Register_m172870881 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CSArenaInterServiceDataGenerated::ilo_setObject1(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t CSArenaInterServiceDataGenerated_ilo_setObject1_m3763145380 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String CSArenaInterServiceDataGenerated::ilo_getStringS2(System.Int32)
extern "C"  String_t* CSArenaInterServiceDataGenerated_ilo_getStringS2_m3746953253 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSArenaInterServiceDataGenerated::ilo_LoadData3(CSArenaInterServiceData,System.String)
extern "C"  void CSArenaInterServiceDataGenerated_ilo_LoadData3_m3356323552 (Il2CppObject * __this /* static, unused */, CSArenaInterServiceData_t1260466202 * ____this0, String_t* ___jsonStr1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
