﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// CombatEntity
struct CombatEntity_t684137495;

#include "AssemblyU2DCSharp_ReplayBase779703160.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ReplayBlood
struct  ReplayBlood_t2696285747  : public ReplayBase_t779703160
{
public:
	// System.Int32 ReplayBlood::isReduce
	int32_t ___isReduce_8;
	// System.Single ReplayBlood::delta
	float ___delta_9;
	// System.Int32 ReplayBlood::srcId
	int32_t ___srcId_10;
	// System.Int32 ReplayBlood::srcIsEnemy
	int32_t ___srcIsEnemy_11;
	// System.Int32 ReplayBlood::damageType
	int32_t ___damageType_12;
	// System.Int32 ReplayBlood::type
	int32_t ___type_13;
	// System.Int32 ReplayBlood::NearDeathProtect
	int32_t ___NearDeathProtect_14;
	// System.Int32 ReplayBlood::buffId
	int32_t ___buffId_15;
	// CombatEntity ReplayBlood::_src
	CombatEntity_t684137495 * ____src_16;

public:
	inline static int32_t get_offset_of_isReduce_8() { return static_cast<int32_t>(offsetof(ReplayBlood_t2696285747, ___isReduce_8)); }
	inline int32_t get_isReduce_8() const { return ___isReduce_8; }
	inline int32_t* get_address_of_isReduce_8() { return &___isReduce_8; }
	inline void set_isReduce_8(int32_t value)
	{
		___isReduce_8 = value;
	}

	inline static int32_t get_offset_of_delta_9() { return static_cast<int32_t>(offsetof(ReplayBlood_t2696285747, ___delta_9)); }
	inline float get_delta_9() const { return ___delta_9; }
	inline float* get_address_of_delta_9() { return &___delta_9; }
	inline void set_delta_9(float value)
	{
		___delta_9 = value;
	}

	inline static int32_t get_offset_of_srcId_10() { return static_cast<int32_t>(offsetof(ReplayBlood_t2696285747, ___srcId_10)); }
	inline int32_t get_srcId_10() const { return ___srcId_10; }
	inline int32_t* get_address_of_srcId_10() { return &___srcId_10; }
	inline void set_srcId_10(int32_t value)
	{
		___srcId_10 = value;
	}

	inline static int32_t get_offset_of_srcIsEnemy_11() { return static_cast<int32_t>(offsetof(ReplayBlood_t2696285747, ___srcIsEnemy_11)); }
	inline int32_t get_srcIsEnemy_11() const { return ___srcIsEnemy_11; }
	inline int32_t* get_address_of_srcIsEnemy_11() { return &___srcIsEnemy_11; }
	inline void set_srcIsEnemy_11(int32_t value)
	{
		___srcIsEnemy_11 = value;
	}

	inline static int32_t get_offset_of_damageType_12() { return static_cast<int32_t>(offsetof(ReplayBlood_t2696285747, ___damageType_12)); }
	inline int32_t get_damageType_12() const { return ___damageType_12; }
	inline int32_t* get_address_of_damageType_12() { return &___damageType_12; }
	inline void set_damageType_12(int32_t value)
	{
		___damageType_12 = value;
	}

	inline static int32_t get_offset_of_type_13() { return static_cast<int32_t>(offsetof(ReplayBlood_t2696285747, ___type_13)); }
	inline int32_t get_type_13() const { return ___type_13; }
	inline int32_t* get_address_of_type_13() { return &___type_13; }
	inline void set_type_13(int32_t value)
	{
		___type_13 = value;
	}

	inline static int32_t get_offset_of_NearDeathProtect_14() { return static_cast<int32_t>(offsetof(ReplayBlood_t2696285747, ___NearDeathProtect_14)); }
	inline int32_t get_NearDeathProtect_14() const { return ___NearDeathProtect_14; }
	inline int32_t* get_address_of_NearDeathProtect_14() { return &___NearDeathProtect_14; }
	inline void set_NearDeathProtect_14(int32_t value)
	{
		___NearDeathProtect_14 = value;
	}

	inline static int32_t get_offset_of_buffId_15() { return static_cast<int32_t>(offsetof(ReplayBlood_t2696285747, ___buffId_15)); }
	inline int32_t get_buffId_15() const { return ___buffId_15; }
	inline int32_t* get_address_of_buffId_15() { return &___buffId_15; }
	inline void set_buffId_15(int32_t value)
	{
		___buffId_15 = value;
	}

	inline static int32_t get_offset_of__src_16() { return static_cast<int32_t>(offsetof(ReplayBlood_t2696285747, ____src_16)); }
	inline CombatEntity_t684137495 * get__src_16() const { return ____src_16; }
	inline CombatEntity_t684137495 ** get_address_of__src_16() { return &____src_16; }
	inline void set__src_16(CombatEntity_t684137495 * value)
	{
		____src_16 = value;
		Il2CppCodeGenWriteBarrier(&____src_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
