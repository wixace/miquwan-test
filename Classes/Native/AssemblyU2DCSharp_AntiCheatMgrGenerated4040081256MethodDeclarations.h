﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AntiCheatMgrGenerated
struct AntiCheatMgrGenerated_t4040081256;
// JSVCall
struct JSVCall_t3708497963;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;
// AntiCheatMgr
struct AntiCheatMgr_t3474304487;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_AntiCheatMgr3474304487.h"
#include "mscorlib_System_String7231557.h"

// System.Void AntiCheatMgrGenerated::.ctor()
extern "C"  void AntiCheatMgrGenerated__ctor_m1670713091 (AntiCheatMgrGenerated_t4040081256 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AntiCheatMgrGenerated::AntiCheatMgr_AntiCheatMgr1(JSVCall,System.Int32)
extern "C"  bool AntiCheatMgrGenerated_AntiCheatMgr_AntiCheatMgr1_m390024687 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AntiCheatMgrGenerated::AntiCheatMgr_heroAntiCheatCSData(JSVCall)
extern "C"  void AntiCheatMgrGenerated_AntiCheatMgr_heroAntiCheatCSData_m3654799501 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AntiCheatMgrGenerated::AntiCheatMgr_heroAntiCheatData(JSVCall)
extern "C"  void AntiCheatMgrGenerated_AntiCheatMgr_heroAntiCheatData_m2657759037 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AntiCheatMgrGenerated::AntiCheatMgr_Clear(JSVCall,System.Int32)
extern "C"  bool AntiCheatMgrGenerated_AntiCheatMgr_Clear_m339870898 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AntiCheatMgrGenerated::AntiCheatMgr_ClearHeroAntiCheat(JSVCall,System.Int32)
extern "C"  bool AntiCheatMgrGenerated_AntiCheatMgr_ClearHeroAntiCheat_m39245671 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AntiCheatMgrGenerated::AntiCheatMgr_ClearHeroAntiCheatCS(JSVCall,System.Int32)
extern "C"  bool AntiCheatMgrGenerated_AntiCheatMgr_ClearHeroAntiCheatCS_m3556617815 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AntiCheatMgrGenerated::AntiCheatMgr_GetHeroAntiCheat__String__String__Int32(JSVCall,System.Int32)
extern "C"  bool AntiCheatMgrGenerated_AntiCheatMgr_GetHeroAntiCheat__String__String__Int32_m1608668912 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AntiCheatMgrGenerated::AntiCheatMgr_GetHeroAntiCheat__String__String(JSVCall,System.Int32)
extern "C"  bool AntiCheatMgrGenerated_AntiCheatMgr_GetHeroAntiCheat__String__String_m641770592 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AntiCheatMgrGenerated::AntiCheatMgr_GetHeroAntiCheatCS__String__String__Int32(JSVCall,System.Int32)
extern "C"  bool AntiCheatMgrGenerated_AntiCheatMgr_GetHeroAntiCheatCS__String__String__Int32_m3410207680 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AntiCheatMgrGenerated::AntiCheatMgr_GetHeroAntiCheatCS__String__String(JSVCall,System.Int32)
extern "C"  bool AntiCheatMgrGenerated_AntiCheatMgr_GetHeroAntiCheatCS__String__String_m2952864144 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AntiCheatMgrGenerated::AntiCheatMgr_RemoveHeroAntiCheat__String(JSVCall,System.Int32)
extern "C"  bool AntiCheatMgrGenerated_AntiCheatMgr_RemoveHeroAntiCheat__String_m1559830217 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AntiCheatMgrGenerated::AntiCheatMgr_RemoveHeroAntiCheatCS__String(JSVCall,System.Int32)
extern "C"  bool AntiCheatMgrGenerated_AntiCheatMgr_RemoveHeroAntiCheatCS__String_m1352287609 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AntiCheatMgrGenerated::AntiCheatMgr_SetHeroAntiCheat__String__String__UInt32__UInt32(JSVCall,System.Int32)
extern "C"  bool AntiCheatMgrGenerated_AntiCheatMgr_SetHeroAntiCheat__String__String__UInt32__UInt32_m3144987014 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AntiCheatMgrGenerated::AntiCheatMgr_SetHeroAntiCheatCS__String__String__UInt32__UInt32(JSVCall,System.Int32)
extern "C"  bool AntiCheatMgrGenerated_AntiCheatMgr_SetHeroAntiCheatCS__String__String__UInt32__UInt32_m1198571958 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AntiCheatMgrGenerated::__Register()
extern "C"  void AntiCheatMgrGenerated___Register_m2499565476 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 AntiCheatMgrGenerated::ilo_setObject1(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t AntiCheatMgrGenerated_ilo_setObject1_m3595142411 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AntiCheatMgrGenerated::ilo_ClearHeroAntiCheatCS2(AntiCheatMgr)
extern "C"  void AntiCheatMgrGenerated_ilo_ClearHeroAntiCheatCS2_m553791775 (Il2CppObject * __this /* static, unused */, AntiCheatMgr_t3474304487 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String AntiCheatMgrGenerated::ilo_getStringS3(System.Int32)
extern "C"  String_t* AntiCheatMgrGenerated_ilo_getStringS3_m2835072809 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 AntiCheatMgrGenerated::ilo_GetHeroAntiCheat4(AntiCheatMgr,System.String,System.String)
extern "C"  uint32_t AntiCheatMgrGenerated_ilo_GetHeroAntiCheat4_m1996993517 (Il2CppObject * __this /* static, unused */, AntiCheatMgr_t3474304487 * ____this0, String_t* ___uuid1, String_t* ___attrName2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 AntiCheatMgrGenerated::ilo_GetHeroAntiCheatCS5(AntiCheatMgr,System.String,System.String,System.Int32)
extern "C"  int32_t AntiCheatMgrGenerated_ilo_GetHeroAntiCheatCS5_m1399335174 (Il2CppObject * __this /* static, unused */, AntiCheatMgr_t3474304487 * ____this0, String_t* ___uuid1, String_t* ___attrName2, int32_t ___defaultValue3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AntiCheatMgrGenerated::ilo_setInt326(System.Int32,System.Int32)
extern "C"  void AntiCheatMgrGenerated_ilo_setInt326_m2796344846 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AntiCheatMgrGenerated::ilo_RemoveHeroAntiCheat7(AntiCheatMgr,System.String)
extern "C"  void AntiCheatMgrGenerated_ilo_RemoveHeroAntiCheat7_m3924749005 (Il2CppObject * __this /* static, unused */, AntiCheatMgr_t3474304487 * ____this0, String_t* ___uuid1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AntiCheatMgrGenerated::ilo_RemoveHeroAntiCheatCS8(AntiCheatMgr,System.String)
extern "C"  void AntiCheatMgrGenerated_ilo_RemoveHeroAntiCheatCS8_m2908898526 (Il2CppObject * __this /* static, unused */, AntiCheatMgr_t3474304487 * ____this0, String_t* ___uuid1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 AntiCheatMgrGenerated::ilo_getUInt329(System.Int32)
extern "C"  uint32_t AntiCheatMgrGenerated_ilo_getUInt329_m4067703124 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AntiCheatMgrGenerated::ilo_SetHeroAntiCheatCS10(AntiCheatMgr,System.String,System.String,System.UInt32,System.UInt32)
extern "C"  void AntiCheatMgrGenerated_ilo_SetHeroAntiCheatCS10_m3485734833 (Il2CppObject * __this /* static, unused */, AntiCheatMgr_t3474304487 * ____this0, String_t* ___uuid1, String_t* ___attrName2, uint32_t ___value3, uint32_t ___acValue4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
