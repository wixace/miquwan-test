﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// RealTimeGenerated
struct RealTimeGenerated_t3832243236;
// JSVCall
struct JSVCall_t3708497963;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"

// System.Void RealTimeGenerated::.ctor()
extern "C"  void RealTimeGenerated__ctor_m2253173191 (RealTimeGenerated_t3832243236 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean RealTimeGenerated::RealTime_RealTime1(JSVCall,System.Int32)
extern "C"  bool RealTimeGenerated_RealTime_RealTime1_m3805031723 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RealTimeGenerated::RealTime_time(JSVCall)
extern "C"  void RealTimeGenerated_RealTime_time_m3343187777 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RealTimeGenerated::RealTime_deltaTime(JSVCall)
extern "C"  void RealTimeGenerated_RealTime_deltaTime_m4203352985 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RealTimeGenerated::__Register()
extern "C"  void RealTimeGenerated___Register_m1278201696 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean RealTimeGenerated::ilo_attachFinalizerObject1(System.Int32)
extern "C"  bool RealTimeGenerated_ilo_attachFinalizerObject1_m2169046544 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RealTimeGenerated::ilo_setSingle2(System.Int32,System.Single)
extern "C"  void RealTimeGenerated_ilo_setSingle2_m4081679822 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
