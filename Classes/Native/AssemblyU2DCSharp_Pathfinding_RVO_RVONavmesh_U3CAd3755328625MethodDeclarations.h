﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.RVO.RVONavmesh/<AddGraphObstacles>c__AnonStorey123
struct U3CAddGraphObstaclesU3Ec__AnonStorey123_t3755328625;
// Pathfinding.GraphNode
struct GraphNode_t23612370;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphNode23612370.h"

// System.Void Pathfinding.RVO.RVONavmesh/<AddGraphObstacles>c__AnonStorey123::.ctor()
extern "C"  void U3CAddGraphObstaclesU3Ec__AnonStorey123__ctor_m4105561930 (U3CAddGraphObstaclesU3Ec__AnonStorey123_t3755328625 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.RVO.RVONavmesh/<AddGraphObstacles>c__AnonStorey123::<>m__359(Pathfinding.GraphNode)
extern "C"  bool U3CAddGraphObstaclesU3Ec__AnonStorey123_U3CU3Em__359_m1237932146 (U3CAddGraphObstaclesU3Ec__AnonStorey123_t3755328625 * __this, GraphNode_t23612370 * ____node0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
