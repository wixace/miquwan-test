﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Pathfinding.ConstantPath
struct ConstantPath_t275096927;
// System.Object
struct Il2CppObject;
// PathTypesDemo
struct PathTypesDemo_t3223563063;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PathTypesDemo/<Constant>c__Iterator14
struct  U3CConstantU3Ec__Iterator14_t2423148702  : public Il2CppObject
{
public:
	// Pathfinding.ConstantPath PathTypesDemo/<Constant>c__Iterator14::<constPath>__0
	ConstantPath_t275096927 * ___U3CconstPathU3E__0_0;
	// System.Int32 PathTypesDemo/<Constant>c__Iterator14::$PC
	int32_t ___U24PC_1;
	// System.Object PathTypesDemo/<Constant>c__Iterator14::$current
	Il2CppObject * ___U24current_2;
	// PathTypesDemo PathTypesDemo/<Constant>c__Iterator14::<>f__this
	PathTypesDemo_t3223563063 * ___U3CU3Ef__this_3;

public:
	inline static int32_t get_offset_of_U3CconstPathU3E__0_0() { return static_cast<int32_t>(offsetof(U3CConstantU3Ec__Iterator14_t2423148702, ___U3CconstPathU3E__0_0)); }
	inline ConstantPath_t275096927 * get_U3CconstPathU3E__0_0() const { return ___U3CconstPathU3E__0_0; }
	inline ConstantPath_t275096927 ** get_address_of_U3CconstPathU3E__0_0() { return &___U3CconstPathU3E__0_0; }
	inline void set_U3CconstPathU3E__0_0(ConstantPath_t275096927 * value)
	{
		___U3CconstPathU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CconstPathU3E__0_0, value);
	}

	inline static int32_t get_offset_of_U24PC_1() { return static_cast<int32_t>(offsetof(U3CConstantU3Ec__Iterator14_t2423148702, ___U24PC_1)); }
	inline int32_t get_U24PC_1() const { return ___U24PC_1; }
	inline int32_t* get_address_of_U24PC_1() { return &___U24PC_1; }
	inline void set_U24PC_1(int32_t value)
	{
		___U24PC_1 = value;
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CConstantU3Ec__Iterator14_t2423148702, ___U24current_2)); }
	inline Il2CppObject * get_U24current_2() const { return ___U24current_2; }
	inline Il2CppObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(Il2CppObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_2, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_3() { return static_cast<int32_t>(offsetof(U3CConstantU3Ec__Iterator14_t2423148702, ___U3CU3Ef__this_3)); }
	inline PathTypesDemo_t3223563063 * get_U3CU3Ef__this_3() const { return ___U3CU3Ef__this_3; }
	inline PathTypesDemo_t3223563063 ** get_address_of_U3CU3Ef__this_3() { return &___U3CU3Ef__this_3; }
	inline void set_U3CU3Ef__this_3(PathTypesDemo_t3223563063 * value)
	{
		___U3CU3Ef__this_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
