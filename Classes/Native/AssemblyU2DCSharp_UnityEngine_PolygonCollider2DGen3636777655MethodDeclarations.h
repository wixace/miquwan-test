﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_PolygonCollider2DGenerated
struct UnityEngine_PolygonCollider2DGenerated_t3636777655;
// JSVCall
struct JSVCall_t3708497963;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t4024180168;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"

// System.Void UnityEngine_PolygonCollider2DGenerated::.ctor()
extern "C"  void UnityEngine_PolygonCollider2DGenerated__ctor_m2971160516 (UnityEngine_PolygonCollider2DGenerated_t3636777655 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PolygonCollider2DGenerated::PolygonCollider2D_PolygonCollider2D1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PolygonCollider2DGenerated_PolygonCollider2D_PolygonCollider2D1_m2303443956 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_PolygonCollider2DGenerated::PolygonCollider2D_points(JSVCall)
extern "C"  void UnityEngine_PolygonCollider2DGenerated_PolygonCollider2D_points_m1601781893 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_PolygonCollider2DGenerated::PolygonCollider2D_pathCount(JSVCall)
extern "C"  void UnityEngine_PolygonCollider2DGenerated_PolygonCollider2D_pathCount_m2067046682 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PolygonCollider2DGenerated::PolygonCollider2D_CreatePrimitive__Int32__Vector2__Vector2(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PolygonCollider2DGenerated_PolygonCollider2D_CreatePrimitive__Int32__Vector2__Vector2_m4096133606 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PolygonCollider2DGenerated::PolygonCollider2D_CreatePrimitive__Int32__Vector2(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PolygonCollider2DGenerated_PolygonCollider2D_CreatePrimitive__Int32__Vector2_m3280802699 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PolygonCollider2DGenerated::PolygonCollider2D_CreatePrimitive__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PolygonCollider2DGenerated_PolygonCollider2D_CreatePrimitive__Int32_m1796648006 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PolygonCollider2DGenerated::PolygonCollider2D_GetPath__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PolygonCollider2DGenerated_PolygonCollider2D_GetPath__Int32_m3133815542 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PolygonCollider2DGenerated::PolygonCollider2D_GetTotalPointCount(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PolygonCollider2DGenerated_PolygonCollider2D_GetTotalPointCount_m3619333744 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PolygonCollider2DGenerated::PolygonCollider2D_SetPath__Int32__Vector2_Array(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PolygonCollider2DGenerated_PolygonCollider2D_SetPath__Int32__Vector2_Array_m1896304161 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_PolygonCollider2DGenerated::__Register()
extern "C"  void UnityEngine_PolygonCollider2DGenerated___Register_m3235407747 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2[] UnityEngine_PolygonCollider2DGenerated::<PolygonCollider2D_points>m__2C9()
extern "C"  Vector2U5BU5D_t4024180168* UnityEngine_PolygonCollider2DGenerated_U3CPolygonCollider2D_pointsU3Em__2C9_m3175277428 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2[] UnityEngine_PolygonCollider2DGenerated::<PolygonCollider2D_SetPath__Int32__Vector2_Array>m__2CA()
extern "C"  Vector2U5BU5D_t4024180168* UnityEngine_PolygonCollider2DGenerated_U3CPolygonCollider2D_SetPath__Int32__Vector2_ArrayU3Em__2CA_m3372366371 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_PolygonCollider2DGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UnityEngine_PolygonCollider2DGenerated_ilo_getObject1_m3050787662 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_PolygonCollider2DGenerated::ilo_setArrayS2(System.Int32,System.Int32,System.Boolean)
extern "C"  void UnityEngine_PolygonCollider2DGenerated_ilo_setArrayS2_m1127088256 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___count1, bool ___bClear2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine_PolygonCollider2DGenerated::ilo_getVector2S3(System.Int32)
extern "C"  Vector2_t4282066565  UnityEngine_PolygonCollider2DGenerated_ilo_getVector2S3_m2596218008 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_PolygonCollider2DGenerated::ilo_getInt324(System.Int32)
extern "C"  int32_t UnityEngine_PolygonCollider2DGenerated_ilo_getInt324_m3221116750 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_PolygonCollider2DGenerated::ilo_setInt325(System.Int32,System.Int32)
extern "C"  void UnityEngine_PolygonCollider2DGenerated_ilo_setInt325_m1790142990 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_PolygonCollider2DGenerated::ilo_getElement6(System.Int32,System.Int32)
extern "C"  int32_t UnityEngine_PolygonCollider2DGenerated_ilo_getElement6_m1420210165 (Il2CppObject * __this /* static, unused */, int32_t ___id0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_PolygonCollider2DGenerated::ilo_getArrayLength7(System.Int32)
extern "C"  int32_t UnityEngine_PolygonCollider2DGenerated_ilo_getArrayLength7_m2386916704 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
