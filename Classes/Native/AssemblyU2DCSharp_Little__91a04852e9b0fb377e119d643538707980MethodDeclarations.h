﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._91a04852e9b0fb377e119d646dbf2486
struct _91a04852e9b0fb377e119d646dbf2486_t3538707980;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__91a04852e9b0fb377e119d643538707980.h"

// System.Void Little._91a04852e9b0fb377e119d646dbf2486::.ctor()
extern "C"  void _91a04852e9b0fb377e119d646dbf2486__ctor_m685428129 (_91a04852e9b0fb377e119d646dbf2486_t3538707980 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._91a04852e9b0fb377e119d646dbf2486::_91a04852e9b0fb377e119d646dbf2486m2(System.Int32)
extern "C"  int32_t _91a04852e9b0fb377e119d646dbf2486__91a04852e9b0fb377e119d646dbf2486m2_m1259533849 (_91a04852e9b0fb377e119d646dbf2486_t3538707980 * __this, int32_t ____91a04852e9b0fb377e119d646dbf2486a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._91a04852e9b0fb377e119d646dbf2486::_91a04852e9b0fb377e119d646dbf2486m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _91a04852e9b0fb377e119d646dbf2486__91a04852e9b0fb377e119d646dbf2486m_m893632701 (_91a04852e9b0fb377e119d646dbf2486_t3538707980 * __this, int32_t ____91a04852e9b0fb377e119d646dbf2486a0, int32_t ____91a04852e9b0fb377e119d646dbf2486461, int32_t ____91a04852e9b0fb377e119d646dbf2486c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._91a04852e9b0fb377e119d646dbf2486::ilo__91a04852e9b0fb377e119d646dbf2486m21(Little._91a04852e9b0fb377e119d646dbf2486,System.Int32)
extern "C"  int32_t _91a04852e9b0fb377e119d646dbf2486_ilo__91a04852e9b0fb377e119d646dbf2486m21_m2417619283 (Il2CppObject * __this /* static, unused */, _91a04852e9b0fb377e119d646dbf2486_t3538707980 * ____this0, int32_t ____91a04852e9b0fb377e119d646dbf2486a1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
