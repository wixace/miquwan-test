﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ProtoBuf.IExtension
struct IExtension_t1606339106;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraShotHeroOrNpcCfg
struct  CameraShotHeroOrNpcCfg_t2707952927  : public Il2CppObject
{
public:
	// System.Int32 CameraShotHeroOrNpcCfg::_id
	int32_t ____id_0;
	// System.Int32 CameraShotHeroOrNpcCfg::_npcType
	int32_t ____npcType_1;
	// System.Int32 CameraShotHeroOrNpcCfg::_npcRoundIndex
	int32_t ____npcRoundIndex_2;
	// System.Int32 CameraShotHeroOrNpcCfg::_npcId
	int32_t ____npcId_3;
	// ProtoBuf.IExtension CameraShotHeroOrNpcCfg::extensionObject
	Il2CppObject * ___extensionObject_4;

public:
	inline static int32_t get_offset_of__id_0() { return static_cast<int32_t>(offsetof(CameraShotHeroOrNpcCfg_t2707952927, ____id_0)); }
	inline int32_t get__id_0() const { return ____id_0; }
	inline int32_t* get_address_of__id_0() { return &____id_0; }
	inline void set__id_0(int32_t value)
	{
		____id_0 = value;
	}

	inline static int32_t get_offset_of__npcType_1() { return static_cast<int32_t>(offsetof(CameraShotHeroOrNpcCfg_t2707952927, ____npcType_1)); }
	inline int32_t get__npcType_1() const { return ____npcType_1; }
	inline int32_t* get_address_of__npcType_1() { return &____npcType_1; }
	inline void set__npcType_1(int32_t value)
	{
		____npcType_1 = value;
	}

	inline static int32_t get_offset_of__npcRoundIndex_2() { return static_cast<int32_t>(offsetof(CameraShotHeroOrNpcCfg_t2707952927, ____npcRoundIndex_2)); }
	inline int32_t get__npcRoundIndex_2() const { return ____npcRoundIndex_2; }
	inline int32_t* get_address_of__npcRoundIndex_2() { return &____npcRoundIndex_2; }
	inline void set__npcRoundIndex_2(int32_t value)
	{
		____npcRoundIndex_2 = value;
	}

	inline static int32_t get_offset_of__npcId_3() { return static_cast<int32_t>(offsetof(CameraShotHeroOrNpcCfg_t2707952927, ____npcId_3)); }
	inline int32_t get__npcId_3() const { return ____npcId_3; }
	inline int32_t* get_address_of__npcId_3() { return &____npcId_3; }
	inline void set__npcId_3(int32_t value)
	{
		____npcId_3 = value;
	}

	inline static int32_t get_offset_of_extensionObject_4() { return static_cast<int32_t>(offsetof(CameraShotHeroOrNpcCfg_t2707952927, ___extensionObject_4)); }
	inline Il2CppObject * get_extensionObject_4() const { return ___extensionObject_4; }
	inline Il2CppObject ** get_address_of_extensionObject_4() { return &___extensionObject_4; }
	inline void set_extensionObject_4(Il2CppObject * value)
	{
		___extensionObject_4 = value;
		Il2CppCodeGenWriteBarrier(&___extensionObject_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
