﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._62a1c1379d2b981c3250a6ea5de6565b
struct _62a1c1379d2b981c3250a6ea5de6565b_t322878076;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._62a1c1379d2b981c3250a6ea5de6565b::.ctor()
extern "C"  void _62a1c1379d2b981c3250a6ea5de6565b__ctor_m503871281 (_62a1c1379d2b981c3250a6ea5de6565b_t322878076 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._62a1c1379d2b981c3250a6ea5de6565b::_62a1c1379d2b981c3250a6ea5de6565bm2(System.Int32)
extern "C"  int32_t _62a1c1379d2b981c3250a6ea5de6565b__62a1c1379d2b981c3250a6ea5de6565bm2_m40312857 (_62a1c1379d2b981c3250a6ea5de6565b_t322878076 * __this, int32_t ____62a1c1379d2b981c3250a6ea5de6565ba0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._62a1c1379d2b981c3250a6ea5de6565b::_62a1c1379d2b981c3250a6ea5de6565bm(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _62a1c1379d2b981c3250a6ea5de6565b__62a1c1379d2b981c3250a6ea5de6565bm_m1276303037 (_62a1c1379d2b981c3250a6ea5de6565b_t322878076 * __this, int32_t ____62a1c1379d2b981c3250a6ea5de6565ba0, int32_t ____62a1c1379d2b981c3250a6ea5de6565b311, int32_t ____62a1c1379d2b981c3250a6ea5de6565bc2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
