﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// JSApi/jsval
struct jsval_t1585251770;
struct jsval_t1585251770_marshaled_pinvoke;
struct jsval_t1585251770_marshaled_com;

#include "codegen/il2cpp-codegen.h"

// System.Boolean JSApi/jsval::isUndefined(System.UInt32)
extern "C"  bool jsval_isUndefined_m1135067761 (Il2CppObject * __this /* static, unused */, uint32_t ___tag0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JSApi/jsval::isNull(System.UInt32)
extern "C"  bool jsval_isNull_m606068746 (Il2CppObject * __this /* static, unused */, uint32_t ___tag0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JSApi/jsval::isInt32(System.UInt32)
extern "C"  bool jsval_isInt32_m623392115 (Il2CppObject * __this /* static, unused */, uint32_t ___tag0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JSApi/jsval::isDouble(System.UInt32)
extern "C"  bool jsval_isDouble_m4293080704 (Il2CppObject * __this /* static, unused */, uint32_t ___tag0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JSApi/jsval::isBoolean(System.UInt32)
extern "C"  bool jsval_isBoolean_m2185336473 (Il2CppObject * __this /* static, unused */, uint32_t ___tag0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JSApi/jsval::isString(System.UInt32)
extern "C"  bool jsval_isString_m2658683456 (Il2CppObject * __this /* static, unused */, uint32_t ___tag0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JSApi/jsval::isNumber(System.UInt32)
extern "C"  bool jsval_isNumber_m3189241864 (Il2CppObject * __this /* static, unused */, uint32_t ___tag0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JSApi/jsval::isObject(System.UInt32)
extern "C"  bool jsval_isObject_m2687796562 (Il2CppObject * __this /* static, unused */, uint32_t ___tag0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JSApi/jsval::isNullOrUndefined(System.UInt32)
extern "C"  bool jsval_isNullOrUndefined_m172455003 (Il2CppObject * __this /* static, unused */, uint32_t ___tag0, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct jsval_t1585251770;
struct jsval_t1585251770_marshaled_pinvoke;

extern "C" void jsval_t1585251770_marshal_pinvoke(const jsval_t1585251770& unmarshaled, jsval_t1585251770_marshaled_pinvoke& marshaled);
extern "C" void jsval_t1585251770_marshal_pinvoke_back(const jsval_t1585251770_marshaled_pinvoke& marshaled, jsval_t1585251770& unmarshaled);
extern "C" void jsval_t1585251770_marshal_pinvoke_cleanup(jsval_t1585251770_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct jsval_t1585251770;
struct jsval_t1585251770_marshaled_com;

extern "C" void jsval_t1585251770_marshal_com(const jsval_t1585251770& unmarshaled, jsval_t1585251770_marshaled_com& marshaled);
extern "C" void jsval_t1585251770_marshal_com_back(const jsval_t1585251770_marshaled_com& marshaled, jsval_t1585251770& unmarshaled);
extern "C" void jsval_t1585251770_marshal_com_cleanup(jsval_t1585251770_marshaled_com& marshaled);
