﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_ProceduralTextureGenerated
struct UnityEngine_ProceduralTextureGenerated_t2564000697;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"

// System.Void UnityEngine_ProceduralTextureGenerated::.ctor()
extern "C"  void UnityEngine_ProceduralTextureGenerated__ctor_m471636226 (UnityEngine_ProceduralTextureGenerated_t2564000697 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ProceduralTextureGenerated::ProceduralTexture_ProceduralTexture1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ProceduralTextureGenerated_ProceduralTexture_ProceduralTexture1_m1139424378 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ProceduralTextureGenerated::ProceduralTexture_hasAlpha(JSVCall)
extern "C"  void UnityEngine_ProceduralTextureGenerated_ProceduralTexture_hasAlpha_m518447264 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ProceduralTextureGenerated::ProceduralTexture_format(JSVCall)
extern "C"  void UnityEngine_ProceduralTextureGenerated_ProceduralTexture_format_m1240429677 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ProceduralTextureGenerated::ProceduralTexture_GetPixels32__Int32__Int32__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ProceduralTextureGenerated_ProceduralTexture_GetPixels32__Int32__Int32__Int32__Int32_m2749504381 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ProceduralTextureGenerated::ProceduralTexture_GetProceduralOutputType(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ProceduralTextureGenerated_ProceduralTexture_GetProceduralOutputType_m3361936745 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ProceduralTextureGenerated::__Register()
extern "C"  void UnityEngine_ProceduralTextureGenerated___Register_m2481337605 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ProceduralTextureGenerated::ilo_attachFinalizerObject1(System.Int32)
extern "C"  bool UnityEngine_ProceduralTextureGenerated_ilo_attachFinalizerObject1_m2469283869 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ProceduralTextureGenerated::ilo_addJSCSRel2(System.Int32,System.Object)
extern "C"  void UnityEngine_ProceduralTextureGenerated_ilo_addJSCSRel2_m699611967 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_ProceduralTextureGenerated::ilo_setObject3(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UnityEngine_ProceduralTextureGenerated_ilo_setObject3_m2462614730 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ProceduralTextureGenerated::ilo_setEnum4(System.Int32,System.Int32)
extern "C"  void UnityEngine_ProceduralTextureGenerated_ilo_setEnum4_m872494272 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
