﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24066860316MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,towerbuffCfg>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m2404228234(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t1651900817 *, int32_t, towerbuffCfg_t1755856872 *, const MethodInfo*))KeyValuePair_2__ctor_m11197230_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,towerbuffCfg>::get_Key()
#define KeyValuePair_2_get_Key_m238728094(__this, method) ((  int32_t (*) (KeyValuePair_2_t1651900817 *, const MethodInfo*))KeyValuePair_2_get_Key_m494458106_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,towerbuffCfg>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m1746964319(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t1651900817 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m4229413435_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,towerbuffCfg>::get_Value()
#define KeyValuePair_2_get_Value_m1376004418(__this, method) ((  towerbuffCfg_t1755856872 * (*) (KeyValuePair_2_t1651900817 *, const MethodInfo*))KeyValuePair_2_get_Value_m1563175098_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,towerbuffCfg>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m819403743(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t1651900817 *, towerbuffCfg_t1755856872 *, const MethodInfo*))KeyValuePair_2_set_Value_m1296398523_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,towerbuffCfg>::ToString()
#define KeyValuePair_2_ToString_m10370249(__this, method) ((  String_t* (*) (KeyValuePair_2_t1651900817 *, const MethodInfo*))KeyValuePair_2_ToString_m491888647_gshared)(__this, method)
