﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ProceduralWorld
struct ProceduralWorld_t114635253;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// AstarPath
struct AstarPath_t4090270936;
// ProceduralWorld/ProceduralTile
struct ProceduralTile_t3586714437;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_AstarPath4090270936.h"
#include "AssemblyU2DCSharp_ProceduralWorld_ProceduralTile3586714437.h"

// System.Void ProceduralWorld::.ctor()
extern "C"  void ProceduralWorld__ctor_m2212690198 (ProceduralWorld_t114635253 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProceduralWorld::Start()
extern "C"  void ProceduralWorld_Start_m1159827990 (ProceduralWorld_t114635253 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProceduralWorld::Update()
extern "C"  void ProceduralWorld_Update_m1600781495 (ProceduralWorld_t114635253 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ProceduralWorld::GenerateTiles()
extern "C"  Il2CppObject * ProceduralWorld_GenerateTiles_m205157516 (ProceduralWorld_t114635253 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProceduralWorld::ilo_Scan1(AstarPath)
extern "C"  void ProceduralWorld_ilo_Scan1_m3314985093 (Il2CppObject * __this /* static, unused */, AstarPath_t4090270936 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ProceduralWorld::ilo_Generate2(ProceduralWorld/ProceduralTile)
extern "C"  Il2CppObject * ProceduralWorld_ilo_Generate2_m1807700001 (Il2CppObject * __this /* static, unused */, ProceduralTile_t3586714437 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
