﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.IO.Stream
struct Stream_t1561764144;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// ProtoBuf.Meta.RuntimeTypeModel
struct RuntimeTypeModel_t242172789;
// System.Type
struct Type_t;
// ProtoBuf.Meta.TypeModel
struct TypeModel_t2730011105;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IO_Stream1561764144.h"
#include "AssemblyU2DCSharp_ProtoBuf_PrefixStyle1974492709.h"
#include "AssemblyU2DCSharp_ProtoBuf_Meta_TypeModel2730011105.h"
#include "mscorlib_System_Type2863145774.h"
#include "mscorlib_System_Object4170816371.h"

// System.Boolean ProtoBuf.Serializer::TryReadLengthPrefix(System.IO.Stream,ProtoBuf.PrefixStyle,System.Int32&)
extern "C"  bool Serializer_TryReadLengthPrefix_m2665256439 (Il2CppObject * __this /* static, unused */, Stream_t1561764144 * ___source0, int32_t ___style1, int32_t* ___length2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Serializer::TryReadLengthPrefix(System.Byte[],System.Int32,System.Int32,ProtoBuf.PrefixStyle,System.Int32&)
extern "C"  bool Serializer_TryReadLengthPrefix_m2583854567 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___buffer0, int32_t ___index1, int32_t ___count2, int32_t ___style3, int32_t* ___length4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Serializer::FlushPool()
extern "C"  void Serializer_FlushPool_m2181479412 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.Meta.RuntimeTypeModel ProtoBuf.Serializer::ilo_get_Default1()
extern "C"  RuntimeTypeModel_t242172789 * Serializer_ilo_get_Default1_m2035467930 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type ProtoBuf.Serializer::ilo_MapType2(ProtoBuf.Meta.TypeModel,System.Type)
extern "C"  Type_t * Serializer_ilo_MapType2_m3919603065 (Il2CppObject * __this /* static, unused */, TypeModel_t2730011105 * ____this0, Type_t * ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProtoBuf.Serializer::ilo_DeepClone3(ProtoBuf.Meta.TypeModel,System.Object)
extern "C"  Il2CppObject * Serializer_ilo_DeepClone3_m3923659753 (Il2CppObject * __this /* static, unused */, TypeModel_t2730011105 * ____this0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProtoBuf.Serializer::ilo_Deserialize4(ProtoBuf.Meta.TypeModel,System.IO.Stream,System.Object,System.Type)
extern "C"  Il2CppObject * Serializer_ilo_Deserialize4_m2850364534 (Il2CppObject * __this /* static, unused */, TypeModel_t2730011105 * ____this0, Stream_t1561764144 * ___source1, Il2CppObject * ___value2, Type_t * ___type3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProtoBuf.Serializer::ilo_DeserializeWithLengthPrefix5(ProtoBuf.Meta.TypeModel,System.IO.Stream,System.Object,System.Type,ProtoBuf.PrefixStyle,System.Int32)
extern "C"  Il2CppObject * Serializer_ilo_DeserializeWithLengthPrefix5_m2370792742 (Il2CppObject * __this /* static, unused */, TypeModel_t2730011105 * ____this0, Stream_t1561764144 * ___source1, Il2CppObject * ___value2, Type_t * ___type3, int32_t ___style4, int32_t ___fieldNumber5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Serializer::ilo_TryReadLengthPrefix6(System.IO.Stream,ProtoBuf.PrefixStyle,System.Int32&)
extern "C"  bool Serializer_ilo_TryReadLengthPrefix6_m2894852942 (Il2CppObject * __this /* static, unused */, Stream_t1561764144 * ___source0, int32_t ___style1, int32_t* ___length2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
