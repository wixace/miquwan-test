﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Core.RpsHandler
struct RpsHandler_t115594153;
// UnityEngine.UI.Text
struct Text_t9039225;
// UnityEngine.UI.Image
struct Image_t538875265;
// UnityEngine.Sprite
struct Sprite_t3199167241;
// UnityEngine.UI.Button
struct Button_t3896396478;

#include "AssemblyU2DCSharp_Core_USingleton_1_gen1690052985.h"
#include "AssemblyU2DCSharp_Core_RpsChoice52797132.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Core.GameManager
struct  GameManager_t3683759601  : public USingleton_1_t1690052985
{
public:
	// Core.RpsHandler Core.GameManager::rpsHandler
	RpsHandler_t115594153 * ___rpsHandler_5;
	// UnityEngine.UI.Text Core.GameManager::RoundText
	Text_t9039225 * ___RoundText_6;
	// UnityEngine.UI.Text Core.GameManager::PlayerWinText
	Text_t9039225 * ___PlayerWinText_7;
	// UnityEngine.UI.Text Core.GameManager::BotWinText
	Text_t9039225 * ___BotWinText_8;
	// UnityEngine.UI.Text Core.GameManager::TieText
	Text_t9039225 * ___TieText_9;
	// UnityEngine.UI.Text Core.GameManager::ResultText
	Text_t9039225 * ___ResultText_10;
	// UnityEngine.UI.Image Core.GameManager::PlayerChoice
	Image_t538875265 * ___PlayerChoice_11;
	// UnityEngine.UI.Image Core.GameManager::BotChoice
	Image_t538875265 * ___BotChoice_12;
	// UnityEngine.Sprite Core.GameManager::Scissors
	Sprite_t3199167241 * ___Scissors_13;
	// UnityEngine.Sprite Core.GameManager::Rock
	Sprite_t3199167241 * ___Rock_14;
	// UnityEngine.Sprite Core.GameManager::Paper
	Sprite_t3199167241 * ___Paper_15;
	// UnityEngine.Sprite Core.GameManager::RandomSpr
	Sprite_t3199167241 * ___RandomSpr_16;
	// UnityEngine.UI.Button Core.GameManager::R
	Button_t3896396478 * ___R_17;
	// UnityEngine.UI.Button Core.GameManager::P
	Button_t3896396478 * ___P_18;
	// UnityEngine.UI.Button Core.GameManager::S
	Button_t3896396478 * ___S_19;
	// System.Boolean Core.GameManager::_selected
	bool ____selected_20;
	// Core.RpsChoice Core.GameManager::_currentChoice
	int32_t ____currentChoice_21;

public:
	inline static int32_t get_offset_of_rpsHandler_5() { return static_cast<int32_t>(offsetof(GameManager_t3683759601, ___rpsHandler_5)); }
	inline RpsHandler_t115594153 * get_rpsHandler_5() const { return ___rpsHandler_5; }
	inline RpsHandler_t115594153 ** get_address_of_rpsHandler_5() { return &___rpsHandler_5; }
	inline void set_rpsHandler_5(RpsHandler_t115594153 * value)
	{
		___rpsHandler_5 = value;
		Il2CppCodeGenWriteBarrier(&___rpsHandler_5, value);
	}

	inline static int32_t get_offset_of_RoundText_6() { return static_cast<int32_t>(offsetof(GameManager_t3683759601, ___RoundText_6)); }
	inline Text_t9039225 * get_RoundText_6() const { return ___RoundText_6; }
	inline Text_t9039225 ** get_address_of_RoundText_6() { return &___RoundText_6; }
	inline void set_RoundText_6(Text_t9039225 * value)
	{
		___RoundText_6 = value;
		Il2CppCodeGenWriteBarrier(&___RoundText_6, value);
	}

	inline static int32_t get_offset_of_PlayerWinText_7() { return static_cast<int32_t>(offsetof(GameManager_t3683759601, ___PlayerWinText_7)); }
	inline Text_t9039225 * get_PlayerWinText_7() const { return ___PlayerWinText_7; }
	inline Text_t9039225 ** get_address_of_PlayerWinText_7() { return &___PlayerWinText_7; }
	inline void set_PlayerWinText_7(Text_t9039225 * value)
	{
		___PlayerWinText_7 = value;
		Il2CppCodeGenWriteBarrier(&___PlayerWinText_7, value);
	}

	inline static int32_t get_offset_of_BotWinText_8() { return static_cast<int32_t>(offsetof(GameManager_t3683759601, ___BotWinText_8)); }
	inline Text_t9039225 * get_BotWinText_8() const { return ___BotWinText_8; }
	inline Text_t9039225 ** get_address_of_BotWinText_8() { return &___BotWinText_8; }
	inline void set_BotWinText_8(Text_t9039225 * value)
	{
		___BotWinText_8 = value;
		Il2CppCodeGenWriteBarrier(&___BotWinText_8, value);
	}

	inline static int32_t get_offset_of_TieText_9() { return static_cast<int32_t>(offsetof(GameManager_t3683759601, ___TieText_9)); }
	inline Text_t9039225 * get_TieText_9() const { return ___TieText_9; }
	inline Text_t9039225 ** get_address_of_TieText_9() { return &___TieText_9; }
	inline void set_TieText_9(Text_t9039225 * value)
	{
		___TieText_9 = value;
		Il2CppCodeGenWriteBarrier(&___TieText_9, value);
	}

	inline static int32_t get_offset_of_ResultText_10() { return static_cast<int32_t>(offsetof(GameManager_t3683759601, ___ResultText_10)); }
	inline Text_t9039225 * get_ResultText_10() const { return ___ResultText_10; }
	inline Text_t9039225 ** get_address_of_ResultText_10() { return &___ResultText_10; }
	inline void set_ResultText_10(Text_t9039225 * value)
	{
		___ResultText_10 = value;
		Il2CppCodeGenWriteBarrier(&___ResultText_10, value);
	}

	inline static int32_t get_offset_of_PlayerChoice_11() { return static_cast<int32_t>(offsetof(GameManager_t3683759601, ___PlayerChoice_11)); }
	inline Image_t538875265 * get_PlayerChoice_11() const { return ___PlayerChoice_11; }
	inline Image_t538875265 ** get_address_of_PlayerChoice_11() { return &___PlayerChoice_11; }
	inline void set_PlayerChoice_11(Image_t538875265 * value)
	{
		___PlayerChoice_11 = value;
		Il2CppCodeGenWriteBarrier(&___PlayerChoice_11, value);
	}

	inline static int32_t get_offset_of_BotChoice_12() { return static_cast<int32_t>(offsetof(GameManager_t3683759601, ___BotChoice_12)); }
	inline Image_t538875265 * get_BotChoice_12() const { return ___BotChoice_12; }
	inline Image_t538875265 ** get_address_of_BotChoice_12() { return &___BotChoice_12; }
	inline void set_BotChoice_12(Image_t538875265 * value)
	{
		___BotChoice_12 = value;
		Il2CppCodeGenWriteBarrier(&___BotChoice_12, value);
	}

	inline static int32_t get_offset_of_Scissors_13() { return static_cast<int32_t>(offsetof(GameManager_t3683759601, ___Scissors_13)); }
	inline Sprite_t3199167241 * get_Scissors_13() const { return ___Scissors_13; }
	inline Sprite_t3199167241 ** get_address_of_Scissors_13() { return &___Scissors_13; }
	inline void set_Scissors_13(Sprite_t3199167241 * value)
	{
		___Scissors_13 = value;
		Il2CppCodeGenWriteBarrier(&___Scissors_13, value);
	}

	inline static int32_t get_offset_of_Rock_14() { return static_cast<int32_t>(offsetof(GameManager_t3683759601, ___Rock_14)); }
	inline Sprite_t3199167241 * get_Rock_14() const { return ___Rock_14; }
	inline Sprite_t3199167241 ** get_address_of_Rock_14() { return &___Rock_14; }
	inline void set_Rock_14(Sprite_t3199167241 * value)
	{
		___Rock_14 = value;
		Il2CppCodeGenWriteBarrier(&___Rock_14, value);
	}

	inline static int32_t get_offset_of_Paper_15() { return static_cast<int32_t>(offsetof(GameManager_t3683759601, ___Paper_15)); }
	inline Sprite_t3199167241 * get_Paper_15() const { return ___Paper_15; }
	inline Sprite_t3199167241 ** get_address_of_Paper_15() { return &___Paper_15; }
	inline void set_Paper_15(Sprite_t3199167241 * value)
	{
		___Paper_15 = value;
		Il2CppCodeGenWriteBarrier(&___Paper_15, value);
	}

	inline static int32_t get_offset_of_RandomSpr_16() { return static_cast<int32_t>(offsetof(GameManager_t3683759601, ___RandomSpr_16)); }
	inline Sprite_t3199167241 * get_RandomSpr_16() const { return ___RandomSpr_16; }
	inline Sprite_t3199167241 ** get_address_of_RandomSpr_16() { return &___RandomSpr_16; }
	inline void set_RandomSpr_16(Sprite_t3199167241 * value)
	{
		___RandomSpr_16 = value;
		Il2CppCodeGenWriteBarrier(&___RandomSpr_16, value);
	}

	inline static int32_t get_offset_of_R_17() { return static_cast<int32_t>(offsetof(GameManager_t3683759601, ___R_17)); }
	inline Button_t3896396478 * get_R_17() const { return ___R_17; }
	inline Button_t3896396478 ** get_address_of_R_17() { return &___R_17; }
	inline void set_R_17(Button_t3896396478 * value)
	{
		___R_17 = value;
		Il2CppCodeGenWriteBarrier(&___R_17, value);
	}

	inline static int32_t get_offset_of_P_18() { return static_cast<int32_t>(offsetof(GameManager_t3683759601, ___P_18)); }
	inline Button_t3896396478 * get_P_18() const { return ___P_18; }
	inline Button_t3896396478 ** get_address_of_P_18() { return &___P_18; }
	inline void set_P_18(Button_t3896396478 * value)
	{
		___P_18 = value;
		Il2CppCodeGenWriteBarrier(&___P_18, value);
	}

	inline static int32_t get_offset_of_S_19() { return static_cast<int32_t>(offsetof(GameManager_t3683759601, ___S_19)); }
	inline Button_t3896396478 * get_S_19() const { return ___S_19; }
	inline Button_t3896396478 ** get_address_of_S_19() { return &___S_19; }
	inline void set_S_19(Button_t3896396478 * value)
	{
		___S_19 = value;
		Il2CppCodeGenWriteBarrier(&___S_19, value);
	}

	inline static int32_t get_offset_of__selected_20() { return static_cast<int32_t>(offsetof(GameManager_t3683759601, ____selected_20)); }
	inline bool get__selected_20() const { return ____selected_20; }
	inline bool* get_address_of__selected_20() { return &____selected_20; }
	inline void set__selected_20(bool value)
	{
		____selected_20 = value;
	}

	inline static int32_t get_offset_of__currentChoice_21() { return static_cast<int32_t>(offsetof(GameManager_t3683759601, ____currentChoice_21)); }
	inline int32_t get__currentChoice_21() const { return ____currentChoice_21; }
	inline int32_t* get_address_of__currentChoice_21() { return &____currentChoice_21; }
	inline void set__currentChoice_21(int32_t value)
	{
		____currentChoice_21 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
