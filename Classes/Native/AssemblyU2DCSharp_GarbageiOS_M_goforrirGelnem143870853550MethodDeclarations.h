﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_goforrirGelnem143
struct M_goforrirGelnem143_t870853550;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"

// System.Void GarbageiOS.M_goforrirGelnem143::.ctor()
extern "C"  void M_goforrirGelnem143__ctor_m92709237 (M_goforrirGelnem143_t870853550 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_goforrirGelnem143::M_roudeaRore0(System.String[],System.Int32)
extern "C"  void M_goforrirGelnem143_M_roudeaRore0_m2718203056 (M_goforrirGelnem143_t870853550 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
