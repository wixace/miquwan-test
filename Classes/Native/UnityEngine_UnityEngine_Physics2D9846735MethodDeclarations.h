﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Physics2D
struct Physics2D_t9846735;
// UnityEngine.Collider2D
struct Collider2D_t1552025098;
// UnityEngine.RaycastHit2D[]
struct RaycastHit2DU5BU5D_t889400257;
// UnityEngine.Collider2D[]
struct Collider2DU5BU5D_t1758559887;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "UnityEngine_UnityEngine_Collider2D1552025098.h"
#include "UnityEngine_UnityEngine_RaycastHit2D1374744384.h"
#include "UnityEngine_UnityEngine_Ray3134616544.h"

// System.Void UnityEngine.Physics2D::.ctor()
extern "C"  void Physics2D__ctor_m2438197792 (Physics2D_t9846735 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Physics2D::.cctor()
extern "C"  void Physics2D__cctor_m2087591309 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Physics2D::get_velocityIterations()
extern "C"  int32_t Physics2D_get_velocityIterations_m3725341248 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Physics2D::set_velocityIterations(System.Int32)
extern "C"  void Physics2D_set_velocityIterations_m122563717 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Physics2D::get_positionIterations()
extern "C"  int32_t Physics2D_get_positionIterations_m1185184908 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Physics2D::set_positionIterations(System.Int32)
extern "C"  void Physics2D_set_positionIterations_m4205667537 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Physics2D::get_gravity()
extern "C"  Vector2_t4282066565  Physics2D_get_gravity_m1913302942 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Physics2D::set_gravity(UnityEngine.Vector2)
extern "C"  void Physics2D_set_gravity_m2019264139 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Physics2D::INTERNAL_get_gravity(UnityEngine.Vector2&)
extern "C"  void Physics2D_INTERNAL_get_gravity_m11614713 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Physics2D::INTERNAL_set_gravity(UnityEngine.Vector2&)
extern "C"  void Physics2D_INTERNAL_set_gravity_m3872262661 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics2D::get_queriesHitTriggers()
extern "C"  bool Physics2D_get_queriesHitTriggers_m3204826895 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Physics2D::set_queriesHitTriggers(System.Boolean)
extern "C"  void Physics2D_set_queriesHitTriggers_m3044555488 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics2D::get_queriesStartInColliders()
extern "C"  bool Physics2D_get_queriesStartInColliders_m2308704633 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Physics2D::set_queriesStartInColliders(System.Boolean)
extern "C"  void Physics2D_set_queriesStartInColliders_m2160083670 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics2D::get_changeStopsCallbacks()
extern "C"  bool Physics2D_get_changeStopsCallbacks_m2926039060 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Physics2D::set_changeStopsCallbacks(System.Boolean)
extern "C"  void Physics2D_set_changeStopsCallbacks_m2520832165 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Physics2D::get_velocityThreshold()
extern "C"  float Physics2D_get_velocityThreshold_m3459323279 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Physics2D::set_velocityThreshold(System.Single)
extern "C"  void Physics2D_set_velocityThreshold_m429953916 (Il2CppObject * __this /* static, unused */, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Physics2D::get_maxLinearCorrection()
extern "C"  float Physics2D_get_maxLinearCorrection_m3702084904 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Physics2D::set_maxLinearCorrection(System.Single)
extern "C"  void Physics2D_set_maxLinearCorrection_m1270248579 (Il2CppObject * __this /* static, unused */, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Physics2D::get_maxAngularCorrection()
extern "C"  float Physics2D_get_maxAngularCorrection_m426801117 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Physics2D::set_maxAngularCorrection(System.Single)
extern "C"  void Physics2D_set_maxAngularCorrection_m834974190 (Il2CppObject * __this /* static, unused */, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Physics2D::get_maxTranslationSpeed()
extern "C"  float Physics2D_get_maxTranslationSpeed_m2741155867 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Physics2D::set_maxTranslationSpeed(System.Single)
extern "C"  void Physics2D_set_maxTranslationSpeed_m1179825264 (Il2CppObject * __this /* static, unused */, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Physics2D::get_maxRotationSpeed()
extern "C"  float Physics2D_get_maxRotationSpeed_m3384377382 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Physics2D::set_maxRotationSpeed(System.Single)
extern "C"  void Physics2D_set_maxRotationSpeed_m3597379653 (Il2CppObject * __this /* static, unused */, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Physics2D::get_minPenetrationForPenalty()
extern "C"  float Physics2D_get_minPenetrationForPenalty_m292516026 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Physics2D::set_minPenetrationForPenalty(System.Single)
extern "C"  void Physics2D_set_minPenetrationForPenalty_m1959479345 (Il2CppObject * __this /* static, unused */, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Physics2D::get_baumgarteScale()
extern "C"  float Physics2D_get_baumgarteScale_m475058393 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Physics2D::set_baumgarteScale(System.Single)
extern "C"  void Physics2D_set_baumgarteScale_m344232562 (Il2CppObject * __this /* static, unused */, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Physics2D::get_baumgarteTOIScale()
extern "C"  float Physics2D_get_baumgarteTOIScale_m2547129039 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Physics2D::set_baumgarteTOIScale(System.Single)
extern "C"  void Physics2D_set_baumgarteTOIScale_m2315392572 (Il2CppObject * __this /* static, unused */, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Physics2D::get_timeToSleep()
extern "C"  float Physics2D_get_timeToSleep_m773964496 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Physics2D::set_timeToSleep(System.Single)
extern "C"  void Physics2D_set_timeToSleep_m3535435739 (Il2CppObject * __this /* static, unused */, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Physics2D::get_linearSleepTolerance()
extern "C"  float Physics2D_get_linearSleepTolerance_m295747516 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Physics2D::set_linearSleepTolerance(System.Single)
extern "C"  void Physics2D_set_linearSleepTolerance_m1253207151 (Il2CppObject * __this /* static, unused */, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Physics2D::get_angularSleepTolerance()
extern "C"  float Physics2D_get_angularSleepTolerance_m2777037945 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Physics2D::set_angularSleepTolerance(System.Single)
extern "C"  void Physics2D_set_angularSleepTolerance_m2085091538 (Il2CppObject * __this /* static, unused */, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Physics2D::IgnoreCollision(UnityEngine.Collider2D,UnityEngine.Collider2D,System.Boolean)
extern "C"  void Physics2D_IgnoreCollision_m2198021525 (Il2CppObject * __this /* static, unused */, Collider2D_t1552025098 * ___collider10, Collider2D_t1552025098 * ___collider21, bool ___ignore2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Physics2D::IgnoreCollision(UnityEngine.Collider2D,UnityEngine.Collider2D)
extern "C"  void Physics2D_IgnoreCollision_m4213588200 (Il2CppObject * __this /* static, unused */, Collider2D_t1552025098 * ___collider10, Collider2D_t1552025098 * ___collider21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics2D::GetIgnoreCollision(UnityEngine.Collider2D,UnityEngine.Collider2D)
extern "C"  bool Physics2D_GetIgnoreCollision_m3312905630 (Il2CppObject * __this /* static, unused */, Collider2D_t1552025098 * ___collider10, Collider2D_t1552025098 * ___collider21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Physics2D::IgnoreLayerCollision(System.Int32,System.Int32,System.Boolean)
extern "C"  void Physics2D_IgnoreLayerCollision_m348341326 (Il2CppObject * __this /* static, unused */, int32_t ___layer10, int32_t ___layer21, bool ___ignore2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Physics2D::IgnoreLayerCollision(System.Int32,System.Int32)
extern "C"  void Physics2D_IgnoreLayerCollision_m1285400399 (Il2CppObject * __this /* static, unused */, int32_t ___layer10, int32_t ___layer21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics2D::GetIgnoreLayerCollision(System.Int32,System.Int32)
extern "C"  bool Physics2D_GetIgnoreLayerCollision_m4261143897 (Il2CppObject * __this /* static, unused */, int32_t ___layer10, int32_t ___layer21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics2D::IsTouching(UnityEngine.Collider2D,UnityEngine.Collider2D)
extern "C"  bool Physics2D_IsTouching_m1953784315 (Il2CppObject * __this /* static, unused */, Collider2D_t1552025098 * ___collider10, Collider2D_t1552025098 * ___collider21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics2D::IsTouchingLayers(UnityEngine.Collider2D,System.Int32)
extern "C"  bool Physics2D_IsTouchingLayers_m2066689543 (Il2CppObject * __this /* static, unused */, Collider2D_t1552025098 * ___collider0, int32_t ___layerMask1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics2D::IsTouchingLayers(UnityEngine.Collider2D)
extern "C"  bool Physics2D_IsTouchingLayers_m3720331472 (Il2CppObject * __this /* static, unused */, Collider2D_t1552025098 * ___collider0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Physics2D::Internal_Linecast(UnityEngine.Vector2,UnityEngine.Vector2,System.Int32,System.Single,System.Single,UnityEngine.RaycastHit2D&)
extern "C"  void Physics2D_Internal_Linecast_m3653471167 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___start0, Vector2_t4282066565  ___end1, int32_t ___layerMask2, float ___minDepth3, float ___maxDepth4, RaycastHit2D_t1374744384 * ___raycastHit5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Physics2D::INTERNAL_CALL_Internal_Linecast(UnityEngine.Vector2&,UnityEngine.Vector2&,System.Int32,System.Single,System.Single,UnityEngine.RaycastHit2D&)
extern "C"  void Physics2D_INTERNAL_CALL_Internal_Linecast_m2627767048 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565 * ___start0, Vector2_t4282066565 * ___end1, int32_t ___layerMask2, float ___minDepth3, float ___maxDepth4, RaycastHit2D_t1374744384 * ___raycastHit5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit2D UnityEngine.Physics2D::Linecast(UnityEngine.Vector2,UnityEngine.Vector2,System.Int32,System.Single)
extern "C"  RaycastHit2D_t1374744384  Physics2D_Linecast_m1037081161 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___start0, Vector2_t4282066565  ___end1, int32_t ___layerMask2, float ___minDepth3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit2D UnityEngine.Physics2D::Linecast(UnityEngine.Vector2,UnityEngine.Vector2,System.Int32)
extern "C"  RaycastHit2D_t1374744384  Physics2D_Linecast_m4170255972 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___start0, Vector2_t4282066565  ___end1, int32_t ___layerMask2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit2D UnityEngine.Physics2D::Linecast(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  RaycastHit2D_t1374744384  Physics2D_Linecast_m1223134547 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___start0, Vector2_t4282066565  ___end1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit2D UnityEngine.Physics2D::Linecast(UnityEngine.Vector2,UnityEngine.Vector2,System.Int32,System.Single,System.Single)
extern "C"  RaycastHit2D_t1374744384  Physics2D_Linecast_m1824977262 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___start0, Vector2_t4282066565  ___end1, int32_t ___layerMask2, float ___minDepth3, float ___maxDepth4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::LinecastAll(UnityEngine.Vector2,UnityEngine.Vector2,System.Int32,System.Single,System.Single)
extern "C"  RaycastHit2DU5BU5D_t889400257* Physics2D_LinecastAll_m2367926937 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___start0, Vector2_t4282066565  ___end1, int32_t ___layerMask2, float ___minDepth3, float ___maxDepth4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::LinecastAll(UnityEngine.Vector2,UnityEngine.Vector2,System.Int32,System.Single)
extern "C"  RaycastHit2DU5BU5D_t889400257* Physics2D_LinecastAll_m2159145652 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___start0, Vector2_t4282066565  ___end1, int32_t ___layerMask2, float ___minDepth3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::LinecastAll(UnityEngine.Vector2,UnityEngine.Vector2,System.Int32)
extern "C"  RaycastHit2DU5BU5D_t889400257* Physics2D_LinecastAll_m740601359 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___start0, Vector2_t4282066565  ___end1, int32_t ___layerMask2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::LinecastAll(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  RaycastHit2DU5BU5D_t889400257* Physics2D_LinecastAll_m4138914760 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___start0, Vector2_t4282066565  ___end1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::INTERNAL_CALL_LinecastAll(UnityEngine.Vector2&,UnityEngine.Vector2&,System.Int32,System.Single,System.Single)
extern "C"  RaycastHit2DU5BU5D_t889400257* Physics2D_INTERNAL_CALL_LinecastAll_m3511439778 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565 * ___start0, Vector2_t4282066565 * ___end1, int32_t ___layerMask2, float ___minDepth3, float ___maxDepth4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Physics2D::LinecastNonAlloc(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.RaycastHit2D[],System.Int32,System.Single,System.Single)
extern "C"  int32_t Physics2D_LinecastNonAlloc_m3375406815 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___start0, Vector2_t4282066565  ___end1, RaycastHit2DU5BU5D_t889400257* ___results2, int32_t ___layerMask3, float ___minDepth4, float ___maxDepth5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Physics2D::LinecastNonAlloc(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.RaycastHit2D[],System.Int32,System.Single)
extern "C"  int32_t Physics2D_LinecastNonAlloc_m3222221178 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___start0, Vector2_t4282066565  ___end1, RaycastHit2DU5BU5D_t889400257* ___results2, int32_t ___layerMask3, float ___minDepth4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Physics2D::LinecastNonAlloc(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.RaycastHit2D[],System.Int32)
extern "C"  int32_t Physics2D_LinecastNonAlloc_m515678037 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___start0, Vector2_t4282066565  ___end1, RaycastHit2DU5BU5D_t889400257* ___results2, int32_t ___layerMask3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Physics2D::LinecastNonAlloc(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.RaycastHit2D[])
extern "C"  int32_t Physics2D_LinecastNonAlloc_m3991816386 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___start0, Vector2_t4282066565  ___end1, RaycastHit2DU5BU5D_t889400257* ___results2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Physics2D::INTERNAL_CALL_LinecastNonAlloc(UnityEngine.Vector2&,UnityEngine.Vector2&,UnityEngine.RaycastHit2D[],System.Int32,System.Single,System.Single)
extern "C"  int32_t Physics2D_INTERNAL_CALL_LinecastNonAlloc_m1714777816 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565 * ___start0, Vector2_t4282066565 * ___end1, RaycastHit2DU5BU5D_t889400257* ___results2, int32_t ___layerMask3, float ___minDepth4, float ___maxDepth5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Physics2D::Internal_Raycast(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Int32,System.Single,System.Single,UnityEngine.RaycastHit2D&)
extern "C"  void Physics2D_Internal_Raycast_m4294843026 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___origin0, Vector2_t4282066565  ___direction1, float ___distance2, int32_t ___layerMask3, float ___minDepth4, float ___maxDepth5, RaycastHit2D_t1374744384 * ___raycastHit6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Physics2D::INTERNAL_CALL_Internal_Raycast(UnityEngine.Vector2&,UnityEngine.Vector2&,System.Single,System.Int32,System.Single,System.Single,UnityEngine.RaycastHit2D&)
extern "C"  void Physics2D_INTERNAL_CALL_Internal_Raycast_m1210233913 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565 * ___origin0, Vector2_t4282066565 * ___direction1, float ___distance2, int32_t ___layerMask3, float ___minDepth4, float ___maxDepth5, RaycastHit2D_t1374744384 * ___raycastHit6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit2D UnityEngine.Physics2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Int32,System.Single)
extern "C"  RaycastHit2D_t1374744384  Physics2D_Raycast_m590929484 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___origin0, Vector2_t4282066565  ___direction1, float ___distance2, int32_t ___layerMask3, float ___minDepth4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit2D UnityEngine.Physics2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Int32)
extern "C"  RaycastHit2D_t1374744384  Physics2D_Raycast_m1435321255 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___origin0, Vector2_t4282066565  ___direction1, float ___distance2, int32_t ___layerMask3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit2D UnityEngine.Physics2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2,System.Single)
extern "C"  RaycastHit2D_t1374744384  Physics2D_Raycast_m1410271024 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___origin0, Vector2_t4282066565  ___direction1, float ___distance2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit2D UnityEngine.Physics2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  RaycastHit2D_t1374744384  Physics2D_Raycast_m1285622667 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___origin0, Vector2_t4282066565  ___direction1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit2D UnityEngine.Physics2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Int32,System.Single,System.Single)
extern "C"  RaycastHit2D_t1374744384  Physics2D_Raycast_m301626417 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___origin0, Vector2_t4282066565  ___direction1, float ___distance2, int32_t ___layerMask3, float ___minDepth4, float ___maxDepth5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::RaycastAll(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Int32,System.Single,System.Single)
extern "C"  RaycastHit2DU5BU5D_t889400257* Physics2D_RaycastAll_m1583954576 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___origin0, Vector2_t4282066565  ___direction1, float ___distance2, int32_t ___layerMask3, float ___minDepth4, float ___maxDepth5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::RaycastAll(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Int32,System.Single)
extern "C"  RaycastHit2DU5BU5D_t889400257* Physics2D_RaycastAll_m2854135531 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___origin0, Vector2_t4282066565  ___direction1, float ___distance2, int32_t ___layerMask3, float ___minDepth4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::RaycastAll(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Int32)
extern "C"  RaycastHit2DU5BU5D_t889400257* Physics2D_RaycastAll_m3437166214 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___origin0, Vector2_t4282066565  ___direction1, float ___distance2, int32_t ___layerMask3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::RaycastAll(UnityEngine.Vector2,UnityEngine.Vector2,System.Single)
extern "C"  RaycastHit2DU5BU5D_t889400257* Physics2D_RaycastAll_m4073459185 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___origin0, Vector2_t4282066565  ___direction1, float ___distance2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::RaycastAll(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  RaycastHit2DU5BU5D_t889400257* Physics2D_RaycastAll_m3440805900 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___origin0, Vector2_t4282066565  ___direction1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::INTERNAL_CALL_RaycastAll(UnityEngine.Vector2&,UnityEngine.Vector2&,System.Single,System.Int32,System.Single,System.Single)
extern "C"  RaycastHit2DU5BU5D_t889400257* Physics2D_INTERNAL_CALL_RaycastAll_m765742583 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565 * ___origin0, Vector2_t4282066565 * ___direction1, float ___distance2, int32_t ___layerMask3, float ___minDepth4, float ___maxDepth5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Physics2D::RaycastNonAlloc(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.RaycastHit2D[],System.Single,System.Int32,System.Single,System.Single)
extern "C"  int32_t Physics2D_RaycastNonAlloc_m4157136906 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___origin0, Vector2_t4282066565  ___direction1, RaycastHit2DU5BU5D_t889400257* ___results2, float ___distance3, int32_t ___layerMask4, float ___minDepth5, float ___maxDepth6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Physics2D::RaycastNonAlloc(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.RaycastHit2D[],System.Single,System.Int32,System.Single)
extern "C"  int32_t Physics2D_RaycastNonAlloc_m2538342885 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___origin0, Vector2_t4282066565  ___direction1, RaycastHit2DU5BU5D_t889400257* ___results2, float ___distance3, int32_t ___layerMask4, float ___minDepth5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Physics2D::RaycastNonAlloc(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.RaycastHit2D[],System.Single,System.Int32)
extern "C"  int32_t Physics2D_RaycastNonAlloc_m2764816128 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___origin0, Vector2_t4282066565  ___direction1, RaycastHit2DU5BU5D_t889400257* ___results2, float ___distance3, int32_t ___layerMask4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Physics2D::RaycastNonAlloc(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.RaycastHit2D[],System.Single)
extern "C"  int32_t Physics2D_RaycastNonAlloc_m1720586039 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___origin0, Vector2_t4282066565  ___direction1, RaycastHit2DU5BU5D_t889400257* ___results2, float ___distance3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Physics2D::RaycastNonAlloc(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.RaycastHit2D[])
extern "C"  int32_t Physics2D_RaycastNonAlloc_m2407635922 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___origin0, Vector2_t4282066565  ___direction1, RaycastHit2DU5BU5D_t889400257* ___results2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Physics2D::INTERNAL_CALL_RaycastNonAlloc(UnityEngine.Vector2&,UnityEngine.Vector2&,UnityEngine.RaycastHit2D[],System.Single,System.Int32,System.Single,System.Single)
extern "C"  int32_t Physics2D_INTERNAL_CALL_RaycastNonAlloc_m1388437601 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565 * ___origin0, Vector2_t4282066565 * ___direction1, RaycastHit2DU5BU5D_t889400257* ___results2, float ___distance3, int32_t ___layerMask4, float ___minDepth5, float ___maxDepth6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Physics2D::Internal_CircleCast(UnityEngine.Vector2,System.Single,UnityEngine.Vector2,System.Single,System.Int32,System.Single,System.Single,UnityEngine.RaycastHit2D&)
extern "C"  void Physics2D_Internal_CircleCast_m2382954929 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___origin0, float ___radius1, Vector2_t4282066565  ___direction2, float ___distance3, int32_t ___layerMask4, float ___minDepth5, float ___maxDepth6, RaycastHit2D_t1374744384 * ___raycastHit7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Physics2D::INTERNAL_CALL_Internal_CircleCast(UnityEngine.Vector2&,System.Single,UnityEngine.Vector2&,System.Single,System.Int32,System.Single,System.Single,UnityEngine.RaycastHit2D&)
extern "C"  void Physics2D_INTERNAL_CALL_Internal_CircleCast_m922648100 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565 * ___origin0, float ___radius1, Vector2_t4282066565 * ___direction2, float ___distance3, int32_t ___layerMask4, float ___minDepth5, float ___maxDepth6, RaycastHit2D_t1374744384 * ___raycastHit7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit2D UnityEngine.Physics2D::CircleCast(UnityEngine.Vector2,System.Single,UnityEngine.Vector2,System.Single,System.Int32,System.Single)
extern "C"  RaycastHit2D_t1374744384  Physics2D_CircleCast_m32375867 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___origin0, float ___radius1, Vector2_t4282066565  ___direction2, float ___distance3, int32_t ___layerMask4, float ___minDepth5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit2D UnityEngine.Physics2D::CircleCast(UnityEngine.Vector2,System.Single,UnityEngine.Vector2,System.Single,System.Int32)
extern "C"  RaycastHit2D_t1374744384  Physics2D_CircleCast_m1344858070 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___origin0, float ___radius1, Vector2_t4282066565  ___direction2, float ___distance3, int32_t ___layerMask4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit2D UnityEngine.Physics2D::CircleCast(UnityEngine.Vector2,System.Single,UnityEngine.Vector2,System.Single)
extern "C"  RaycastHit2D_t1374744384  Physics2D_CircleCast_m1561412257 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___origin0, float ___radius1, Vector2_t4282066565  ___direction2, float ___distance3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit2D UnityEngine.Physics2D::CircleCast(UnityEngine.Vector2,System.Single,UnityEngine.Vector2)
extern "C"  RaycastHit2D_t1374744384  Physics2D_CircleCast_m2674438332 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___origin0, float ___radius1, Vector2_t4282066565  ___direction2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit2D UnityEngine.Physics2D::CircleCast(UnityEngine.Vector2,System.Single,UnityEngine.Vector2,System.Single,System.Int32,System.Single,System.Single)
extern "C"  RaycastHit2D_t1374744384  Physics2D_CircleCast_m706939872 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___origin0, float ___radius1, Vector2_t4282066565  ___direction2, float ___distance3, int32_t ___layerMask4, float ___minDepth5, float ___maxDepth6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::CircleCastAll(UnityEngine.Vector2,System.Single,UnityEngine.Vector2,System.Single,System.Int32,System.Single,System.Single)
extern "C"  RaycastHit2DU5BU5D_t889400257* Physics2D_CircleCastAll_m3716119827 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___origin0, float ___radius1, Vector2_t4282066565  ___direction2, float ___distance3, int32_t ___layerMask4, float ___minDepth5, float ___maxDepth6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::CircleCastAll(UnityEngine.Vector2,System.Single,UnityEngine.Vector2,System.Single,System.Int32,System.Single)
extern "C"  RaycastHit2DU5BU5D_t889400257* Physics2D_CircleCastAll_m1120942766 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___origin0, float ___radius1, Vector2_t4282066565  ___direction2, float ___distance3, int32_t ___layerMask4, float ___minDepth5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::CircleCastAll(UnityEngine.Vector2,System.Single,UnityEngine.Vector2,System.Single,System.Int32)
extern "C"  RaycastHit2DU5BU5D_t889400257* Physics2D_CircleCastAll_m3997635465 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___origin0, float ___radius1, Vector2_t4282066565  ___direction2, float ___distance3, int32_t ___layerMask4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::CircleCastAll(UnityEngine.Vector2,System.Single,UnityEngine.Vector2,System.Single)
extern "C"  RaycastHit2DU5BU5D_t889400257* Physics2D_CircleCastAll_m3237416974 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___origin0, float ___radius1, Vector2_t4282066565  ___direction2, float ___distance3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::CircleCastAll(UnityEngine.Vector2,System.Single,UnityEngine.Vector2)
extern "C"  RaycastHit2DU5BU5D_t889400257* Physics2D_CircleCastAll_m3480539369 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___origin0, float ___radius1, Vector2_t4282066565  ___direction2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::INTERNAL_CALL_CircleCastAll(UnityEngine.Vector2&,System.Single,UnityEngine.Vector2&,System.Single,System.Int32,System.Single,System.Single)
extern "C"  RaycastHit2DU5BU5D_t889400257* Physics2D_INTERNAL_CALL_CircleCastAll_m3978373062 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565 * ___origin0, float ___radius1, Vector2_t4282066565 * ___direction2, float ___distance3, int32_t ___layerMask4, float ___minDepth5, float ___maxDepth6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Physics2D::CircleCastNonAlloc(UnityEngine.Vector2,System.Single,UnityEngine.Vector2,UnityEngine.RaycastHit2D[],System.Single,System.Int32,System.Single,System.Single)
extern "C"  int32_t Physics2D_CircleCastNonAlloc_m169286211 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___origin0, float ___radius1, Vector2_t4282066565  ___direction2, RaycastHit2DU5BU5D_t889400257* ___results3, float ___distance4, int32_t ___layerMask5, float ___minDepth6, float ___maxDepth7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Physics2D::CircleCastNonAlloc(UnityEngine.Vector2,System.Single,UnityEngine.Vector2,UnityEngine.RaycastHit2D[],System.Single,System.Int32,System.Single)
extern "C"  int32_t Physics2D_CircleCastNonAlloc_m3340610526 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___origin0, float ___radius1, Vector2_t4282066565  ___direction2, RaycastHit2DU5BU5D_t889400257* ___results3, float ___distance4, int32_t ___layerMask5, float ___minDepth6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Physics2D::CircleCastNonAlloc(UnityEngine.Vector2,System.Single,UnityEngine.Vector2,UnityEngine.RaycastHit2D[],System.Single,System.Int32)
extern "C"  int32_t Physics2D_CircleCastNonAlloc_m2018334905 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___origin0, float ___radius1, Vector2_t4282066565  ___direction2, RaycastHit2DU5BU5D_t889400257* ___results3, float ___distance4, int32_t ___layerMask5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Physics2D::CircleCastNonAlloc(UnityEngine.Vector2,System.Single,UnityEngine.Vector2,UnityEngine.RaycastHit2D[],System.Single)
extern "C"  int32_t Physics2D_CircleCastNonAlloc_m4063763678 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___origin0, float ___radius1, Vector2_t4282066565  ___direction2, RaycastHit2DU5BU5D_t889400257* ___results3, float ___distance4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Physics2D::CircleCastNonAlloc(UnityEngine.Vector2,System.Single,UnityEngine.Vector2,UnityEngine.RaycastHit2D[])
extern "C"  int32_t Physics2D_CircleCastNonAlloc_m3406924217 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___origin0, float ___radius1, Vector2_t4282066565  ___direction2, RaycastHit2DU5BU5D_t889400257* ___results3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Physics2D::INTERNAL_CALL_CircleCastNonAlloc(UnityEngine.Vector2&,System.Single,UnityEngine.Vector2&,UnityEngine.RaycastHit2D[],System.Single,System.Int32,System.Single,System.Single)
extern "C"  int32_t Physics2D_INTERNAL_CALL_CircleCastNonAlloc_m1673571026 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565 * ___origin0, float ___radius1, Vector2_t4282066565 * ___direction2, RaycastHit2DU5BU5D_t889400257* ___results3, float ___distance4, int32_t ___layerMask5, float ___minDepth6, float ___maxDepth7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Physics2D::Internal_BoxCast(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,UnityEngine.Vector2,System.Single,System.Int32,System.Single,System.Single,UnityEngine.RaycastHit2D&)
extern "C"  void Physics2D_Internal_BoxCast_m155355738 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___origin0, Vector2_t4282066565  ___size1, float ___angle2, Vector2_t4282066565  ___direction3, float ___distance4, int32_t ___layerMask5, float ___minDepth6, float ___maxDepth7, RaycastHit2D_t1374744384 * ___raycastHit8, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Physics2D::INTERNAL_CALL_Internal_BoxCast(UnityEngine.Vector2&,UnityEngine.Vector2&,System.Single,UnityEngine.Vector2&,System.Single,System.Int32,System.Single,System.Single,UnityEngine.RaycastHit2D&)
extern "C"  void Physics2D_INTERNAL_CALL_Internal_BoxCast_m3566003677 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565 * ___origin0, Vector2_t4282066565 * ___size1, float ___angle2, Vector2_t4282066565 * ___direction3, float ___distance4, int32_t ___layerMask5, float ___minDepth6, float ___maxDepth7, RaycastHit2D_t1374744384 * ___raycastHit8, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit2D UnityEngine.Physics2D::BoxCast(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,UnityEngine.Vector2,System.Single,System.Int32,System.Single)
extern "C"  RaycastHit2D_t1374744384  Physics2D_BoxCast_m3690647572 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___origin0, Vector2_t4282066565  ___size1, float ___angle2, Vector2_t4282066565  ___direction3, float ___distance4, int32_t ___layerMask5, float ___minDepth6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit2D UnityEngine.Physics2D::BoxCast(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,UnityEngine.Vector2,System.Single,System.Int32)
extern "C"  RaycastHit2D_t1374744384  Physics2D_BoxCast_m2065855343 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___origin0, Vector2_t4282066565  ___size1, float ___angle2, Vector2_t4282066565  ___direction3, float ___distance4, int32_t ___layerMask5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit2D UnityEngine.Physics2D::BoxCast(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,UnityEngine.Vector2,System.Single)
extern "C"  RaycastHit2D_t1374744384  Physics2D_BoxCast_m668979816 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___origin0, Vector2_t4282066565  ___size1, float ___angle2, Vector2_t4282066565  ___direction3, float ___distance4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit2D UnityEngine.Physics2D::BoxCast(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,UnityEngine.Vector2)
extern "C"  RaycastHit2D_t1374744384  Physics2D_BoxCast_m3934083267 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___origin0, Vector2_t4282066565  ___size1, float ___angle2, Vector2_t4282066565  ___direction3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit2D UnityEngine.Physics2D::BoxCast(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,UnityEngine.Vector2,System.Single,System.Int32,System.Single,System.Single)
extern "C"  RaycastHit2D_t1374744384  Physics2D_BoxCast_m2808981497 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___origin0, Vector2_t4282066565  ___size1, float ___angle2, Vector2_t4282066565  ___direction3, float ___distance4, int32_t ___layerMask5, float ___minDepth6, float ___maxDepth7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::BoxCastAll(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,UnityEngine.Vector2,System.Single,System.Int32,System.Single,System.Single)
extern "C"  RaycastHit2DU5BU5D_t889400257* Physics2D_BoxCastAll_m211689718 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___origin0, Vector2_t4282066565  ___size1, float ___angle2, Vector2_t4282066565  ___direction3, float ___distance4, int32_t ___layerMask5, float ___minDepth6, float ___maxDepth7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::BoxCastAll(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,UnityEngine.Vector2,System.Single,System.Int32,System.Single)
extern "C"  RaycastHit2DU5BU5D_t889400257* Physics2D_BoxCastAll_m1660295121 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___origin0, Vector2_t4282066565  ___size1, float ___angle2, Vector2_t4282066565  ___direction3, float ___distance4, int32_t ___layerMask5, float ___minDepth6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::BoxCastAll(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,UnityEngine.Vector2,System.Single,System.Int32)
extern "C"  RaycastHit2DU5BU5D_t889400257* Physics2D_BoxCastAll_m872765932 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___origin0, Vector2_t4282066565  ___size1, float ___angle2, Vector2_t4282066565  ___direction3, float ___distance4, int32_t ___layerMask5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::BoxCastAll(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,UnityEngine.Vector2,System.Single)
extern "C"  RaycastHit2DU5BU5D_t889400257* Physics2D_BoxCastAll_m2276926667 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___origin0, Vector2_t4282066565  ___size1, float ___angle2, Vector2_t4282066565  ___direction3, float ___distance4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::BoxCastAll(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,UnityEngine.Vector2)
extern "C"  RaycastHit2DU5BU5D_t889400257* Physics2D_BoxCastAll_m953631846 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___origin0, Vector2_t4282066565  ___size1, float ___angle2, Vector2_t4282066565  ___direction3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::INTERNAL_CALL_BoxCastAll(UnityEngine.Vector2&,UnityEngine.Vector2&,System.Single,UnityEngine.Vector2&,System.Single,System.Int32,System.Single,System.Single)
extern "C"  RaycastHit2DU5BU5D_t889400257* Physics2D_INTERNAL_CALL_BoxCastAll_m2342618159 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565 * ___origin0, Vector2_t4282066565 * ___size1, float ___angle2, Vector2_t4282066565 * ___direction3, float ___distance4, int32_t ___layerMask5, float ___minDepth6, float ___maxDepth7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Physics2D::BoxCastNonAlloc(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,UnityEngine.Vector2,UnityEngine.RaycastHit2D[],System.Single,System.Int32,System.Single,System.Single)
extern "C"  int32_t Physics2D_BoxCastNonAlloc_m3181173570 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___origin0, Vector2_t4282066565  ___size1, float ___angle2, Vector2_t4282066565  ___direction3, RaycastHit2DU5BU5D_t889400257* ___results4, float ___distance5, int32_t ___layerMask6, float ___minDepth7, float ___maxDepth8, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Physics2D::BoxCastNonAlloc(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,UnityEngine.Vector2,UnityEngine.RaycastHit2D[],System.Single,System.Int32,System.Single)
extern "C"  int32_t Physics2D_BoxCastNonAlloc_m1736410909 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___origin0, Vector2_t4282066565  ___size1, float ___angle2, Vector2_t4282066565  ___direction3, RaycastHit2DU5BU5D_t889400257* ___results4, float ___distance5, int32_t ___layerMask6, float ___minDepth7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Physics2D::BoxCastNonAlloc(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,UnityEngine.Vector2,UnityEngine.RaycastHit2D[],System.Single,System.Int32)
extern "C"  int32_t Physics2D_BoxCastNonAlloc_m130694712 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___origin0, Vector2_t4282066565  ___size1, float ___angle2, Vector2_t4282066565  ___direction3, RaycastHit2DU5BU5D_t889400257* ___results4, float ___distance5, int32_t ___layerMask6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Physics2D::BoxCastNonAlloc(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,UnityEngine.Vector2,UnityEngine.RaycastHit2D[],System.Single)
extern "C"  int32_t Physics2D_BoxCastNonAlloc_m3638113535 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___origin0, Vector2_t4282066565  ___size1, float ___angle2, Vector2_t4282066565  ___direction3, RaycastHit2DU5BU5D_t889400257* ___results4, float ___distance5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Physics2D::BoxCastNonAlloc(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,UnityEngine.Vector2,UnityEngine.RaycastHit2D[])
extern "C"  int32_t Physics2D_BoxCastNonAlloc_m403373978 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___origin0, Vector2_t4282066565  ___size1, float ___angle2, Vector2_t4282066565  ___direction3, RaycastHit2DU5BU5D_t889400257* ___results4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Physics2D::INTERNAL_CALL_BoxCastNonAlloc(UnityEngine.Vector2&,UnityEngine.Vector2&,System.Single,UnityEngine.Vector2&,UnityEngine.RaycastHit2D[],System.Single,System.Int32,System.Single,System.Single)
extern "C"  int32_t Physics2D_INTERNAL_CALL_BoxCastNonAlloc_m2636633033 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565 * ___origin0, Vector2_t4282066565 * ___size1, float ___angle2, Vector2_t4282066565 * ___direction3, RaycastHit2DU5BU5D_t889400257* ___results4, float ___distance5, int32_t ___layerMask6, float ___minDepth7, float ___maxDepth8, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Physics2D::Internal_GetRayIntersection(UnityEngine.Ray,System.Single,System.Int32,UnityEngine.RaycastHit2D&)
extern "C"  void Physics2D_Internal_GetRayIntersection_m2828566441 (Il2CppObject * __this /* static, unused */, Ray_t3134616544  ___ray0, float ___distance1, int32_t ___layerMask2, RaycastHit2D_t1374744384 * ___raycastHit3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Physics2D::INTERNAL_CALL_Internal_GetRayIntersection(UnityEngine.Ray&,System.Single,System.Int32,UnityEngine.RaycastHit2D&)
extern "C"  void Physics2D_INTERNAL_CALL_Internal_GetRayIntersection_m1206199840 (Il2CppObject * __this /* static, unused */, Ray_t3134616544 * ___ray0, float ___distance1, int32_t ___layerMask2, RaycastHit2D_t1374744384 * ___raycastHit3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit2D UnityEngine.Physics2D::GetRayIntersection(UnityEngine.Ray,System.Single)
extern "C"  RaycastHit2D_t1374744384  Physics2D_GetRayIntersection_m2375445855 (Il2CppObject * __this /* static, unused */, Ray_t3134616544  ___ray0, float ___distance1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit2D UnityEngine.Physics2D::GetRayIntersection(UnityEngine.Ray)
extern "C"  RaycastHit2D_t1374744384  Physics2D_GetRayIntersection_m4184367098 (Il2CppObject * __this /* static, unused */, Ray_t3134616544  ___ray0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit2D UnityEngine.Physics2D::GetRayIntersection(UnityEngine.Ray,System.Single,System.Int32)
extern "C"  RaycastHit2D_t1374744384  Physics2D_GetRayIntersection_m3849560536 (Il2CppObject * __this /* static, unused */, Ray_t3134616544  ___ray0, float ___distance1, int32_t ___layerMask2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::GetRayIntersectionAll(UnityEngine.Ray,System.Single,System.Int32)
extern "C"  RaycastHit2DU5BU5D_t889400257* Physics2D_GetRayIntersectionAll_m2520210479 (Il2CppObject * __this /* static, unused */, Ray_t3134616544  ___ray0, float ___distance1, int32_t ___layerMask2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::GetRayIntersectionAll(UnityEngine.Ray,System.Single)
extern "C"  RaycastHit2DU5BU5D_t889400257* Physics2D_GetRayIntersectionAll_m692040104 (Il2CppObject * __this /* static, unused */, Ray_t3134616544  ___ray0, float ___distance1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::GetRayIntersectionAll(UnityEngine.Ray)
extern "C"  RaycastHit2DU5BU5D_t889400257* Physics2D_GetRayIntersectionAll_m1005119491 (Il2CppObject * __this /* static, unused */, Ray_t3134616544  ___ray0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::INTERNAL_CALL_GetRayIntersectionAll(UnityEngine.Ray&,System.Single,System.Int32)
extern "C"  RaycastHit2DU5BU5D_t889400257* Physics2D_INTERNAL_CALL_GetRayIntersectionAll_m2968135304 (Il2CppObject * __this /* static, unused */, Ray_t3134616544 * ___ray0, float ___distance1, int32_t ___layerMask2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Physics2D::GetRayIntersectionNonAlloc(UnityEngine.Ray,UnityEngine.RaycastHit2D[],System.Single,System.Int32)
extern "C"  int32_t Physics2D_GetRayIntersectionNonAlloc_m283974775 (Il2CppObject * __this /* static, unused */, Ray_t3134616544  ___ray0, RaycastHit2DU5BU5D_t889400257* ___results1, float ___distance2, int32_t ___layerMask3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Physics2D::GetRayIntersectionNonAlloc(UnityEngine.Ray,UnityEngine.RaycastHit2D[],System.Single)
extern "C"  int32_t Physics2D_GetRayIntersectionNonAlloc_m3150912608 (Il2CppObject * __this /* static, unused */, Ray_t3134616544  ___ray0, RaycastHit2DU5BU5D_t889400257* ___results1, float ___distance2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Physics2D::GetRayIntersectionNonAlloc(UnityEngine.Ray,UnityEngine.RaycastHit2D[])
extern "C"  int32_t Physics2D_GetRayIntersectionNonAlloc_m935916731 (Il2CppObject * __this /* static, unused */, Ray_t3134616544  ___ray0, RaycastHit2DU5BU5D_t889400257* ___results1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Physics2D::INTERNAL_CALL_GetRayIntersectionNonAlloc(UnityEngine.Ray&,UnityEngine.RaycastHit2D[],System.Single,System.Int32)
extern "C"  int32_t Physics2D_INTERNAL_CALL_GetRayIntersectionNonAlloc_m1223337494 (Il2CppObject * __this /* static, unused */, Ray_t3134616544 * ___ray0, RaycastHit2DU5BU5D_t889400257* ___results1, float ___distance2, int32_t ___layerMask3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider2D UnityEngine.Physics2D::OverlapPoint(UnityEngine.Vector2,System.Int32,System.Single,System.Single)
extern "C"  Collider2D_t1552025098 * Physics2D_OverlapPoint_m1033704962 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___point0, int32_t ___layerMask1, float ___minDepth2, float ___maxDepth3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider2D UnityEngine.Physics2D::OverlapPoint(UnityEngine.Vector2,System.Int32,System.Single)
extern "C"  Collider2D_t1552025098 * Physics2D_OverlapPoint_m2588512733 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___point0, int32_t ___layerMask1, float ___minDepth2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider2D UnityEngine.Physics2D::OverlapPoint(UnityEngine.Vector2,System.Int32)
extern "C"  Collider2D_t1552025098 * Physics2D_OverlapPoint_m1741297912 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___point0, int32_t ___layerMask1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider2D UnityEngine.Physics2D::OverlapPoint(UnityEngine.Vector2)
extern "C"  Collider2D_t1552025098 * Physics2D_OverlapPoint_m4167885375 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___point0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider2D UnityEngine.Physics2D::INTERNAL_CALL_OverlapPoint(UnityEngine.Vector2&,System.Int32,System.Single,System.Single)
extern "C"  Collider2D_t1552025098 * Physics2D_INTERNAL_CALL_OverlapPoint_m467114293 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565 * ___point0, int32_t ___layerMask1, float ___minDepth2, float ___maxDepth3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider2D[] UnityEngine.Physics2D::OverlapPointAll(UnityEngine.Vector2,System.Int32,System.Single,System.Single)
extern "C"  Collider2DU5BU5D_t1758559887* Physics2D_OverlapPointAll_m4265382893 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___point0, int32_t ___layerMask1, float ___minDepth2, float ___maxDepth3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider2D[] UnityEngine.Physics2D::OverlapPointAll(UnityEngine.Vector2,System.Int32,System.Single)
extern "C"  Collider2DU5BU5D_t1758559887* Physics2D_OverlapPointAll_m1910699784 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___point0, int32_t ___layerMask1, float ___minDepth2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider2D[] UnityEngine.Physics2D::OverlapPointAll(UnityEngine.Vector2,System.Int32)
extern "C"  Collider2DU5BU5D_t1758559887* Physics2D_OverlapPointAll_m3910604131 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___point0, int32_t ___layerMask1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider2D[] UnityEngine.Physics2D::OverlapPointAll(UnityEngine.Vector2)
extern "C"  Collider2DU5BU5D_t1758559887* Physics2D_OverlapPointAll_m1053234164 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___point0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider2D[] UnityEngine.Physics2D::INTERNAL_CALL_OverlapPointAll(UnityEngine.Vector2&,System.Int32,System.Single,System.Single)
extern "C"  Collider2DU5BU5D_t1758559887* Physics2D_INTERNAL_CALL_OverlapPointAll_m1965895752 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565 * ___point0, int32_t ___layerMask1, float ___minDepth2, float ___maxDepth3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Physics2D::OverlapPointNonAlloc(UnityEngine.Vector2,UnityEngine.Collider2D[],System.Int32,System.Single,System.Single)
extern "C"  int32_t Physics2D_OverlapPointNonAlloc_m1559543967 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___point0, Collider2DU5BU5D_t1758559887* ___results1, int32_t ___layerMask2, float ___minDepth3, float ___maxDepth4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Physics2D::OverlapPointNonAlloc(UnityEngine.Vector2,UnityEngine.Collider2D[],System.Int32,System.Single)
extern "C"  int32_t Physics2D_OverlapPointNonAlloc_m3966591802 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___point0, Collider2DU5BU5D_t1758559887* ___results1, int32_t ___layerMask2, float ___minDepth3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Physics2D::OverlapPointNonAlloc(UnityEngine.Vector2,UnityEngine.Collider2D[],System.Int32)
extern "C"  int32_t Physics2D_OverlapPointNonAlloc_m865132821 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___point0, Collider2DU5BU5D_t1758559887* ___results1, int32_t ___layerMask2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Physics2D::OverlapPointNonAlloc(UnityEngine.Vector2,UnityEngine.Collider2D[])
extern "C"  int32_t Physics2D_OverlapPointNonAlloc_m3029479170 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___point0, Collider2DU5BU5D_t1758559887* ___results1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Physics2D::INTERNAL_CALL_OverlapPointNonAlloc(UnityEngine.Vector2&,UnityEngine.Collider2D[],System.Int32,System.Single,System.Single)
extern "C"  int32_t Physics2D_INTERNAL_CALL_OverlapPointNonAlloc_m3323423296 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565 * ___point0, Collider2DU5BU5D_t1758559887* ___results1, int32_t ___layerMask2, float ___minDepth3, float ___maxDepth4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider2D UnityEngine.Physics2D::OverlapCircle(UnityEngine.Vector2,System.Single,System.Int32,System.Single,System.Single)
extern "C"  Collider2D_t1552025098 * Physics2D_OverlapCircle_m2018451049 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___point0, float ___radius1, int32_t ___layerMask2, float ___minDepth3, float ___maxDepth4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider2D UnityEngine.Physics2D::OverlapCircle(UnityEngine.Vector2,System.Single,System.Int32,System.Single)
extern "C"  Collider2D_t1552025098 * Physics2D_OverlapCircle_m1010518660 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___point0, float ___radius1, int32_t ___layerMask2, float ___minDepth3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider2D UnityEngine.Physics2D::OverlapCircle(UnityEngine.Vector2,System.Single,System.Int32)
extern "C"  Collider2D_t1552025098 * Physics2D_OverlapCircle_m2476274143 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___point0, float ___radius1, int32_t ___layerMask2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider2D UnityEngine.Physics2D::OverlapCircle(UnityEngine.Vector2,System.Single)
extern "C"  Collider2D_t1552025098 * Physics2D_OverlapCircle_m2995559928 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___point0, float ___radius1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider2D UnityEngine.Physics2D::INTERNAL_CALL_OverlapCircle(UnityEngine.Vector2&,System.Single,System.Int32,System.Single,System.Single)
extern "C"  Collider2D_t1552025098 * Physics2D_INTERNAL_CALL_OverlapCircle_m1802552674 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565 * ___point0, float ___radius1, int32_t ___layerMask2, float ___minDepth3, float ___maxDepth4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider2D[] UnityEngine.Physics2D::OverlapCircleAll(UnityEngine.Vector2,System.Single,System.Int32,System.Single,System.Single)
extern "C"  Collider2DU5BU5D_t1758559887* Physics2D_OverlapCircleAll_m2585342016 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___point0, float ___radius1, int32_t ___layerMask2, float ___minDepth3, float ___maxDepth4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider2D[] UnityEngine.Physics2D::OverlapCircleAll(UnityEngine.Vector2,System.Single,System.Int32,System.Single)
extern "C"  Collider2DU5BU5D_t1758559887* Physics2D_OverlapCircleAll_m3310440603 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___point0, float ___radius1, int32_t ___layerMask2, float ___minDepth3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider2D[] UnityEngine.Physics2D::OverlapCircleAll(UnityEngine.Vector2,System.Single,System.Int32)
extern "C"  Collider2DU5BU5D_t1758559887* Physics2D_OverlapCircleAll_m1000299574 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___point0, float ___radius1, int32_t ___layerMask2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider2D[] UnityEngine.Physics2D::OverlapCircleAll(UnityEngine.Vector2,System.Single)
extern "C"  Collider2DU5BU5D_t1758559887* Physics2D_OverlapCircleAll_m1347760705 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___point0, float ___radius1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider2D[] UnityEngine.Physics2D::INTERNAL_CALL_OverlapCircleAll(UnityEngine.Vector2&,System.Single,System.Int32,System.Single,System.Single)
extern "C"  Collider2DU5BU5D_t1758559887* Physics2D_INTERNAL_CALL_OverlapCircleAll_m3702630157 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565 * ___point0, float ___radius1, int32_t ___layerMask2, float ___minDepth3, float ___maxDepth4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Physics2D::OverlapCircleNonAlloc(UnityEngine.Vector2,System.Single,UnityEngine.Collider2D[],System.Int32,System.Single,System.Single)
extern "C"  int32_t Physics2D_OverlapCircleNonAlloc_m3023321724 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___point0, float ___radius1, Collider2DU5BU5D_t1758559887* ___results2, int32_t ___layerMask3, float ___minDepth4, float ___maxDepth5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Physics2D::OverlapCircleNonAlloc(UnityEngine.Vector2,System.Single,UnityEngine.Collider2D[],System.Int32,System.Single)
extern "C"  int32_t Physics2D_OverlapCircleNonAlloc_m3123229655 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___point0, float ___radius1, Collider2DU5BU5D_t1758559887* ___results2, int32_t ___layerMask3, float ___minDepth4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Physics2D::OverlapCircleNonAlloc(UnityEngine.Vector2,System.Single,UnityEngine.Collider2D[],System.Int32)
extern "C"  int32_t Physics2D_OverlapCircleNonAlloc_m4006795378 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___point0, float ___radius1, Collider2DU5BU5D_t1758559887* ___results2, int32_t ___layerMask3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Physics2D::OverlapCircleNonAlloc(UnityEngine.Vector2,System.Single,UnityEngine.Collider2D[])
extern "C"  int32_t Physics2D_OverlapCircleNonAlloc_m75531397 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___point0, float ___radius1, Collider2DU5BU5D_t1758559887* ___results2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Physics2D::INTERNAL_CALL_OverlapCircleNonAlloc(UnityEngine.Vector2&,System.Single,UnityEngine.Collider2D[],System.Int32,System.Single,System.Single)
extern "C"  int32_t Physics2D_INTERNAL_CALL_OverlapCircleNonAlloc_m564933583 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565 * ___point0, float ___radius1, Collider2DU5BU5D_t1758559887* ___results2, int32_t ___layerMask3, float ___minDepth4, float ___maxDepth5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider2D UnityEngine.Physics2D::OverlapArea(UnityEngine.Vector2,UnityEngine.Vector2,System.Int32,System.Single,System.Single)
extern "C"  Collider2D_t1552025098 * Physics2D_OverlapArea_m3660361463 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___pointA0, Vector2_t4282066565  ___pointB1, int32_t ___layerMask2, float ___minDepth3, float ___maxDepth4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider2D UnityEngine.Physics2D::OverlapArea(UnityEngine.Vector2,UnityEngine.Vector2,System.Int32,System.Single)
extern "C"  Collider2D_t1552025098 * Physics2D_OverlapArea_m550423442 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___pointA0, Vector2_t4282066565  ___pointB1, int32_t ___layerMask2, float ___minDepth3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider2D UnityEngine.Physics2D::OverlapArea(UnityEngine.Vector2,UnityEngine.Vector2,System.Int32)
extern "C"  Collider2D_t1552025098 * Physics2D_OverlapArea_m2748924781 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___pointA0, Vector2_t4282066565  ___pointB1, int32_t ___layerMask2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider2D UnityEngine.Physics2D::OverlapArea(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  Collider2D_t1552025098 * Physics2D_OverlapArea_m2745228714 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___pointA0, Vector2_t4282066565  ___pointB1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider2D UnityEngine.Physics2D::INTERNAL_CALL_OverlapArea(UnityEngine.Vector2&,UnityEngine.Vector2&,System.Int32,System.Single,System.Single)
extern "C"  Collider2D_t1552025098 * Physics2D_INTERNAL_CALL_OverlapArea_m3162150016 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565 * ___pointA0, Vector2_t4282066565 * ___pointB1, int32_t ___layerMask2, float ___minDepth3, float ___maxDepth4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider2D[] UnityEngine.Physics2D::OverlapAreaAll(UnityEngine.Vector2,UnityEngine.Vector2,System.Int32,System.Single,System.Single)
extern "C"  Collider2DU5BU5D_t1758559887* Physics2D_OverlapAreaAll_m2584047348 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___pointA0, Vector2_t4282066565  ___pointB1, int32_t ___layerMask2, float ___minDepth3, float ___maxDepth4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider2D[] UnityEngine.Physics2D::OverlapAreaAll(UnityEngine.Vector2,UnityEngine.Vector2,System.Int32,System.Single)
extern "C"  Collider2DU5BU5D_t1758559887* Physics2D_OverlapAreaAll_m2775226959 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___pointA0, Vector2_t4282066565  ___pointB1, int32_t ___layerMask2, float ___minDepth3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider2D[] UnityEngine.Physics2D::OverlapAreaAll(UnityEngine.Vector2,UnityEngine.Vector2,System.Int32)
extern "C"  Collider2DU5BU5D_t1758559887* Physics2D_OverlapAreaAll_m2026172650 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___pointA0, Vector2_t4282066565  ___pointB1, int32_t ___layerMask2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider2D[] UnityEngine.Physics2D::OverlapAreaAll(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  Collider2DU5BU5D_t1758559887* Physics2D_OverlapAreaAll_m3317172493 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___pointA0, Vector2_t4282066565  ___pointB1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider2D[] UnityEngine.Physics2D::INTERNAL_CALL_OverlapAreaAll(UnityEngine.Vector2&,UnityEngine.Vector2&,System.Int32,System.Single,System.Single)
extern "C"  Collider2DU5BU5D_t1758559887* Physics2D_INTERNAL_CALL_OverlapAreaAll_m2088674779 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565 * ___pointA0, Vector2_t4282066565 * ___pointB1, int32_t ___layerMask2, float ___minDepth3, float ___maxDepth4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Physics2D::OverlapAreaNonAlloc(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Collider2D[],System.Int32,System.Single,System.Single)
extern "C"  int32_t Physics2D_OverlapAreaNonAlloc_m4186783662 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___pointA0, Vector2_t4282066565  ___pointB1, Collider2DU5BU5D_t1758559887* ___results2, int32_t ___layerMask3, float ___minDepth4, float ___maxDepth5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Physics2D::OverlapAreaNonAlloc(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Collider2D[],System.Int32,System.Single)
extern "C"  int32_t Physics2D_OverlapAreaNonAlloc_m3288389769 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___pointA0, Vector2_t4282066565  ___pointB1, Collider2DU5BU5D_t1758559887* ___results2, int32_t ___layerMask3, float ___minDepth4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Physics2D::OverlapAreaNonAlloc(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Collider2D[],System.Int32)
extern "C"  int32_t Physics2D_OverlapAreaNonAlloc_m1062157476 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___pointA0, Vector2_t4282066565  ___pointB1, Collider2DU5BU5D_t1758559887* ___results2, int32_t ___layerMask3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Physics2D::OverlapAreaNonAlloc(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Collider2D[])
extern "C"  int32_t Physics2D_OverlapAreaNonAlloc_m2574989075 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___pointA0, Vector2_t4282066565  ___pointB1, Collider2DU5BU5D_t1758559887* ___results2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Physics2D::INTERNAL_CALL_OverlapAreaNonAlloc(UnityEngine.Vector2&,UnityEngine.Vector2&,UnityEngine.Collider2D[],System.Int32,System.Single,System.Single)
extern "C"  int32_t Physics2D_INTERNAL_CALL_OverlapAreaNonAlloc_m4198999365 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565 * ___pointA0, Vector2_t4282066565 * ___pointB1, Collider2DU5BU5D_t1758559887* ___results2, int32_t ___layerMask3, float ___minDepth4, float ___maxDepth5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
