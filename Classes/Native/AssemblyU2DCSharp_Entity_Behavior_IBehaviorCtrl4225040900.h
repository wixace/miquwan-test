﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// CombatEntity
struct CombatEntity_t684137495;
// System.Collections.Generic.Dictionary`2<Entity.Behavior.EBehaviorID,Entity.Behavior.IBehavior>
struct Dictionary_2_t2206131300;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// System.Collections.Generic.List`1<Entity.Behavior.EBehaviorID>
struct List_1_t2606718688;
// Entity.Behavior.IBehavior
struct IBehavior_t770859129;

#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_Entity_Behavior_EBehaviorID1238533136.h"
#include "AssemblyU2DCSharp_Entity_Behavior_EGamePattern238230393.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entity.Behavior.IBehaviorCtrl
struct  IBehaviorCtrl_t4225040900  : public Il2CppObject
{
public:
	// CombatEntity Entity.Behavior.IBehaviorCtrl::entity
	CombatEntity_t684137495 * ___entity_0;
	// System.Collections.Generic.Dictionary`2<Entity.Behavior.EBehaviorID,Entity.Behavior.IBehavior> Entity.Behavior.IBehaviorCtrl::behaviors
	Dictionary_2_t2206131300 * ___behaviors_1;
	// System.Boolean Entity.Behavior.IBehaviorCtrl::isNotChangeBehavior
	bool ___isNotChangeBehavior_2;
	// System.Boolean Entity.Behavior.IBehaviorCtrl::IsDo
	bool ___IsDo_3;
	// Entity.Behavior.EBehaviorID Entity.Behavior.IBehaviorCtrl::id
	uint8_t ___id_4;
	// System.Object[] Entity.Behavior.IBehaviorCtrl::args
	ObjectU5BU5D_t1108656482* ___args_5;
	// System.Collections.Generic.List`1<Entity.Behavior.EBehaviorID> Entity.Behavior.IBehaviorCtrl::ignoreLockBehaviors
	List_1_t2606718688 * ___ignoreLockBehaviors_6;
	// Entity.Behavior.IBehavior Entity.Behavior.IBehaviorCtrl::<lastBvr>k__BackingField
	IBehavior_t770859129 * ___U3ClastBvrU3Ek__BackingField_8;
	// Entity.Behavior.IBehavior Entity.Behavior.IBehaviorCtrl::<curBehavior>k__BackingField
	IBehavior_t770859129 * ___U3CcurBehaviorU3Ek__BackingField_9;
	// Entity.Behavior.EBehaviorID Entity.Behavior.IBehaviorCtrl::<curBehaviorID>k__BackingField
	uint8_t ___U3CcurBehaviorIDU3Ek__BackingField_10;

public:
	inline static int32_t get_offset_of_entity_0() { return static_cast<int32_t>(offsetof(IBehaviorCtrl_t4225040900, ___entity_0)); }
	inline CombatEntity_t684137495 * get_entity_0() const { return ___entity_0; }
	inline CombatEntity_t684137495 ** get_address_of_entity_0() { return &___entity_0; }
	inline void set_entity_0(CombatEntity_t684137495 * value)
	{
		___entity_0 = value;
		Il2CppCodeGenWriteBarrier(&___entity_0, value);
	}

	inline static int32_t get_offset_of_behaviors_1() { return static_cast<int32_t>(offsetof(IBehaviorCtrl_t4225040900, ___behaviors_1)); }
	inline Dictionary_2_t2206131300 * get_behaviors_1() const { return ___behaviors_1; }
	inline Dictionary_2_t2206131300 ** get_address_of_behaviors_1() { return &___behaviors_1; }
	inline void set_behaviors_1(Dictionary_2_t2206131300 * value)
	{
		___behaviors_1 = value;
		Il2CppCodeGenWriteBarrier(&___behaviors_1, value);
	}

	inline static int32_t get_offset_of_isNotChangeBehavior_2() { return static_cast<int32_t>(offsetof(IBehaviorCtrl_t4225040900, ___isNotChangeBehavior_2)); }
	inline bool get_isNotChangeBehavior_2() const { return ___isNotChangeBehavior_2; }
	inline bool* get_address_of_isNotChangeBehavior_2() { return &___isNotChangeBehavior_2; }
	inline void set_isNotChangeBehavior_2(bool value)
	{
		___isNotChangeBehavior_2 = value;
	}

	inline static int32_t get_offset_of_IsDo_3() { return static_cast<int32_t>(offsetof(IBehaviorCtrl_t4225040900, ___IsDo_3)); }
	inline bool get_IsDo_3() const { return ___IsDo_3; }
	inline bool* get_address_of_IsDo_3() { return &___IsDo_3; }
	inline void set_IsDo_3(bool value)
	{
		___IsDo_3 = value;
	}

	inline static int32_t get_offset_of_id_4() { return static_cast<int32_t>(offsetof(IBehaviorCtrl_t4225040900, ___id_4)); }
	inline uint8_t get_id_4() const { return ___id_4; }
	inline uint8_t* get_address_of_id_4() { return &___id_4; }
	inline void set_id_4(uint8_t value)
	{
		___id_4 = value;
	}

	inline static int32_t get_offset_of_args_5() { return static_cast<int32_t>(offsetof(IBehaviorCtrl_t4225040900, ___args_5)); }
	inline ObjectU5BU5D_t1108656482* get_args_5() const { return ___args_5; }
	inline ObjectU5BU5D_t1108656482** get_address_of_args_5() { return &___args_5; }
	inline void set_args_5(ObjectU5BU5D_t1108656482* value)
	{
		___args_5 = value;
		Il2CppCodeGenWriteBarrier(&___args_5, value);
	}

	inline static int32_t get_offset_of_ignoreLockBehaviors_6() { return static_cast<int32_t>(offsetof(IBehaviorCtrl_t4225040900, ___ignoreLockBehaviors_6)); }
	inline List_1_t2606718688 * get_ignoreLockBehaviors_6() const { return ___ignoreLockBehaviors_6; }
	inline List_1_t2606718688 ** get_address_of_ignoreLockBehaviors_6() { return &___ignoreLockBehaviors_6; }
	inline void set_ignoreLockBehaviors_6(List_1_t2606718688 * value)
	{
		___ignoreLockBehaviors_6 = value;
		Il2CppCodeGenWriteBarrier(&___ignoreLockBehaviors_6, value);
	}

	inline static int32_t get_offset_of_U3ClastBvrU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(IBehaviorCtrl_t4225040900, ___U3ClastBvrU3Ek__BackingField_8)); }
	inline IBehavior_t770859129 * get_U3ClastBvrU3Ek__BackingField_8() const { return ___U3ClastBvrU3Ek__BackingField_8; }
	inline IBehavior_t770859129 ** get_address_of_U3ClastBvrU3Ek__BackingField_8() { return &___U3ClastBvrU3Ek__BackingField_8; }
	inline void set_U3ClastBvrU3Ek__BackingField_8(IBehavior_t770859129 * value)
	{
		___U3ClastBvrU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3ClastBvrU3Ek__BackingField_8, value);
	}

	inline static int32_t get_offset_of_U3CcurBehaviorU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(IBehaviorCtrl_t4225040900, ___U3CcurBehaviorU3Ek__BackingField_9)); }
	inline IBehavior_t770859129 * get_U3CcurBehaviorU3Ek__BackingField_9() const { return ___U3CcurBehaviorU3Ek__BackingField_9; }
	inline IBehavior_t770859129 ** get_address_of_U3CcurBehaviorU3Ek__BackingField_9() { return &___U3CcurBehaviorU3Ek__BackingField_9; }
	inline void set_U3CcurBehaviorU3Ek__BackingField_9(IBehavior_t770859129 * value)
	{
		___U3CcurBehaviorU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CcurBehaviorU3Ek__BackingField_9, value);
	}

	inline static int32_t get_offset_of_U3CcurBehaviorIDU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(IBehaviorCtrl_t4225040900, ___U3CcurBehaviorIDU3Ek__BackingField_10)); }
	inline uint8_t get_U3CcurBehaviorIDU3Ek__BackingField_10() const { return ___U3CcurBehaviorIDU3Ek__BackingField_10; }
	inline uint8_t* get_address_of_U3CcurBehaviorIDU3Ek__BackingField_10() { return &___U3CcurBehaviorIDU3Ek__BackingField_10; }
	inline void set_U3CcurBehaviorIDU3Ek__BackingField_10(uint8_t value)
	{
		___U3CcurBehaviorIDU3Ek__BackingField_10 = value;
	}
};

struct IBehaviorCtrl_t4225040900_StaticFields
{
public:
	// Entity.Behavior.EGamePattern Entity.Behavior.IBehaviorCtrl::<GamePattern>k__BackingField
	int32_t ___U3CGamePatternU3Ek__BackingField_7;

public:
	inline static int32_t get_offset_of_U3CGamePatternU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(IBehaviorCtrl_t4225040900_StaticFields, ___U3CGamePatternU3Ek__BackingField_7)); }
	inline int32_t get_U3CGamePatternU3Ek__BackingField_7() const { return ___U3CGamePatternU3Ek__BackingField_7; }
	inline int32_t* get_address_of_U3CGamePatternU3Ek__BackingField_7() { return &___U3CGamePatternU3Ek__BackingField_7; }
	inline void set_U3CGamePatternU3Ek__BackingField_7(int32_t value)
	{
		___U3CGamePatternU3Ek__BackingField_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
