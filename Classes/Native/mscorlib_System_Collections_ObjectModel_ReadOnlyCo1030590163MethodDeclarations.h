﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCo1432926611MethodDeclarations.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<JSMgr/CallbackInfo>::.ctor(System.Collections.Generic.IList`1<T>)
#define ReadOnlyCollection_1__ctor_m2373346013(__this, ___list0, method) ((  void (*) (ReadOnlyCollection_1_t1030590163 *, Il2CppObject*, const MethodInfo*))ReadOnlyCollection_1__ctor_m1366664402_gshared)(__this, ___list0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<JSMgr/CallbackInfo>::System.Collections.Generic.ICollection<T>.Add(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1233378887(__this, ___item0, method) ((  void (*) (ReadOnlyCollection_1_t1030590163 *, CallbackInfo_t3768479923 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2541166012_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<JSMgr/CallbackInfo>::System.Collections.Generic.ICollection<T>.Clear()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m1586730851(__this, method) ((  void (*) (ReadOnlyCollection_1_t1030590163 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m3473426062_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<JSMgr/CallbackInfo>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m3249566830(__this, ___index0, ___item1, method) ((  void (*) (ReadOnlyCollection_1_t1030590163 *, int32_t, CallbackInfo_t3768479923 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m3496388003_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<JSMgr/CallbackInfo>::System.Collections.Generic.ICollection<T>.Remove(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m3664606096(__this, ___item0, method) ((  bool (*) (ReadOnlyCollection_1_t1030590163 *, CallbackInfo_t3768479923 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m348744375_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<JSMgr/CallbackInfo>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1123419700(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t1030590163 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1370240873_gshared)(__this, ___index0, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<JSMgr/CallbackInfo>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m2766329050(__this, ___index0, method) ((  CallbackInfo_t3768479923 * (*) (ReadOnlyCollection_1_t1030590163 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m3534609325_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<JSMgr/CallbackInfo>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m2202096069(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t1030590163 *, int32_t, CallbackInfo_t3768479923 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m3174042042_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<JSMgr/CallbackInfo>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m62662655(__this, method) ((  bool (*) (ReadOnlyCollection_1_t1030590163 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2459576056_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<JSMgr/CallbackInfo>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m3233752332(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t1030590163 *, Il2CppArray *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1945557633_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<JSMgr/CallbackInfo>::System.Collections.IEnumerable.GetEnumerator()
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m840428315(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t1030590163 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m3330065468_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<JSMgr/CallbackInfo>::System.Collections.IList.Add(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Add_m4140340738(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t1030590163 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m1628967861_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<JSMgr/CallbackInfo>::System.Collections.IList.Clear()
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m3382928410(__this, method) ((  void (*) (ReadOnlyCollection_1_t1030590163 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m514207119_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<JSMgr/CallbackInfo>::System.Collections.IList.Contains(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m3594026366(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t1030590163 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m736178103_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<JSMgr/CallbackInfo>::System.Collections.IList.IndexOf(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m1139407706(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t1030590163 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m3658311565_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<JSMgr/CallbackInfo>::System.Collections.IList.Insert(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m3762284493(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t1030590163 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m2823806264_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<JSMgr/CallbackInfo>::System.Collections.IList.Remove(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m3551926331(__this, ___value0, method) ((  void (*) (ReadOnlyCollection_1_t1030590163 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m2498539760_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<JSMgr/CallbackInfo>::System.Collections.IList.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m25922269(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t1030590163 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1730676936_gshared)(__this, ___index0, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<JSMgr/CallbackInfo>::System.Collections.ICollection.get_IsSynchronized()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m394723614(__this, method) ((  bool (*) (ReadOnlyCollection_1_t1030590163 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1373829189_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<JSMgr/CallbackInfo>::System.Collections.ICollection.get_SyncRoot()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m1160556176(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t1030590163 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m918746289_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<JSMgr/CallbackInfo>::System.Collections.IList.get_IsFixedSize()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m2446566253(__this, method) ((  bool (*) (ReadOnlyCollection_1_t1030590163 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m932754534_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<JSMgr/CallbackInfo>::System.Collections.IList.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m3442424300(__this, method) ((  bool (*) (ReadOnlyCollection_1_t1030590163 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m2423760339_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<JSMgr/CallbackInfo>::System.Collections.IList.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m3963567127(__this, ___index0, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t1030590163 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m3512499704_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<JSMgr/CallbackInfo>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m4101854308(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t1030590163 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m4167408399_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<JSMgr/CallbackInfo>::Contains(T)
#define ReadOnlyCollection_1_Contains_m970182869(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t1030590163 *, CallbackInfo_t3768479923 *, const MethodInfo*))ReadOnlyCollection_1_Contains_m687553276_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<JSMgr/CallbackInfo>::CopyTo(T[],System.Int32)
#define ReadOnlyCollection_1_CopyTo_m4272871031(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t1030590163 *, CallbackInfoU5BU5D_t229224226*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m475587820_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<JSMgr/CallbackInfo>::GetEnumerator()
#define ReadOnlyCollection_1_GetEnumerator_m3052850092(__this, method) ((  Il2CppObject* (*) (ReadOnlyCollection_1_t1030590163 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m809369055_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<JSMgr/CallbackInfo>::IndexOf(T)
#define ReadOnlyCollection_1_IndexOf_m1092942979(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t1030590163 *, CallbackInfo_t3768479923 *, const MethodInfo*))ReadOnlyCollection_1_IndexOf_m817393776_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<JSMgr/CallbackInfo>::get_Count()
#define ReadOnlyCollection_1_get_Count_m3633768792(__this, method) ((  int32_t (*) (ReadOnlyCollection_1_t1030590163 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m3681678091_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<JSMgr/CallbackInfo>::get_Item(System.Int32)
#define ReadOnlyCollection_1_get_Item_m3830176666(__this, ___index0, method) ((  CallbackInfo_t3768479923 * (*) (ReadOnlyCollection_1_t1030590163 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m2421641197_gshared)(__this, ___index0, method)
