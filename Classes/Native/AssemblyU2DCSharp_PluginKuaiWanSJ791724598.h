﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// Mihua.SDK.PayInfo
struct PayInfo_t1775308120;
// System.Action
struct Action_t3771233898;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PluginKuaiWanSJ
struct  PluginKuaiWanSJ_t791724598  : public MonoBehaviour_t667441552
{
public:
	// System.String PluginKuaiWanSJ::token
	String_t* ___token_2;
	// System.String PluginKuaiWanSJ::username
	String_t* ___username_3;
	// System.String PluginKuaiWanSJ::userid
	String_t* ___userid_4;
	// System.Boolean PluginKuaiWanSJ::isOut
	bool ___isOut_5;
	// System.String PluginKuaiWanSJ::configId
	String_t* ___configId_6;
	// System.String PluginKuaiWanSJ::appid
	String_t* ___appid_7;
	// System.String PluginKuaiWanSJ::codeversion
	String_t* ___codeversion_8;
	// System.String PluginKuaiWanSJ::os
	String_t* ___os_9;
	// System.String PluginKuaiWanSJ::YDAppId
	String_t* ___YDAppId_10;
	// System.String PluginKuaiWanSJ::YDAppKey
	String_t* ___YDAppKey_11;
	// System.String PluginKuaiWanSJ::YDChannel
	String_t* ___YDChannel_12;
	// Mihua.SDK.PayInfo PluginKuaiWanSJ::payInfo
	PayInfo_t1775308120 * ___payInfo_13;

public:
	inline static int32_t get_offset_of_token_2() { return static_cast<int32_t>(offsetof(PluginKuaiWanSJ_t791724598, ___token_2)); }
	inline String_t* get_token_2() const { return ___token_2; }
	inline String_t** get_address_of_token_2() { return &___token_2; }
	inline void set_token_2(String_t* value)
	{
		___token_2 = value;
		Il2CppCodeGenWriteBarrier(&___token_2, value);
	}

	inline static int32_t get_offset_of_username_3() { return static_cast<int32_t>(offsetof(PluginKuaiWanSJ_t791724598, ___username_3)); }
	inline String_t* get_username_3() const { return ___username_3; }
	inline String_t** get_address_of_username_3() { return &___username_3; }
	inline void set_username_3(String_t* value)
	{
		___username_3 = value;
		Il2CppCodeGenWriteBarrier(&___username_3, value);
	}

	inline static int32_t get_offset_of_userid_4() { return static_cast<int32_t>(offsetof(PluginKuaiWanSJ_t791724598, ___userid_4)); }
	inline String_t* get_userid_4() const { return ___userid_4; }
	inline String_t** get_address_of_userid_4() { return &___userid_4; }
	inline void set_userid_4(String_t* value)
	{
		___userid_4 = value;
		Il2CppCodeGenWriteBarrier(&___userid_4, value);
	}

	inline static int32_t get_offset_of_isOut_5() { return static_cast<int32_t>(offsetof(PluginKuaiWanSJ_t791724598, ___isOut_5)); }
	inline bool get_isOut_5() const { return ___isOut_5; }
	inline bool* get_address_of_isOut_5() { return &___isOut_5; }
	inline void set_isOut_5(bool value)
	{
		___isOut_5 = value;
	}

	inline static int32_t get_offset_of_configId_6() { return static_cast<int32_t>(offsetof(PluginKuaiWanSJ_t791724598, ___configId_6)); }
	inline String_t* get_configId_6() const { return ___configId_6; }
	inline String_t** get_address_of_configId_6() { return &___configId_6; }
	inline void set_configId_6(String_t* value)
	{
		___configId_6 = value;
		Il2CppCodeGenWriteBarrier(&___configId_6, value);
	}

	inline static int32_t get_offset_of_appid_7() { return static_cast<int32_t>(offsetof(PluginKuaiWanSJ_t791724598, ___appid_7)); }
	inline String_t* get_appid_7() const { return ___appid_7; }
	inline String_t** get_address_of_appid_7() { return &___appid_7; }
	inline void set_appid_7(String_t* value)
	{
		___appid_7 = value;
		Il2CppCodeGenWriteBarrier(&___appid_7, value);
	}

	inline static int32_t get_offset_of_codeversion_8() { return static_cast<int32_t>(offsetof(PluginKuaiWanSJ_t791724598, ___codeversion_8)); }
	inline String_t* get_codeversion_8() const { return ___codeversion_8; }
	inline String_t** get_address_of_codeversion_8() { return &___codeversion_8; }
	inline void set_codeversion_8(String_t* value)
	{
		___codeversion_8 = value;
		Il2CppCodeGenWriteBarrier(&___codeversion_8, value);
	}

	inline static int32_t get_offset_of_os_9() { return static_cast<int32_t>(offsetof(PluginKuaiWanSJ_t791724598, ___os_9)); }
	inline String_t* get_os_9() const { return ___os_9; }
	inline String_t** get_address_of_os_9() { return &___os_9; }
	inline void set_os_9(String_t* value)
	{
		___os_9 = value;
		Il2CppCodeGenWriteBarrier(&___os_9, value);
	}

	inline static int32_t get_offset_of_YDAppId_10() { return static_cast<int32_t>(offsetof(PluginKuaiWanSJ_t791724598, ___YDAppId_10)); }
	inline String_t* get_YDAppId_10() const { return ___YDAppId_10; }
	inline String_t** get_address_of_YDAppId_10() { return &___YDAppId_10; }
	inline void set_YDAppId_10(String_t* value)
	{
		___YDAppId_10 = value;
		Il2CppCodeGenWriteBarrier(&___YDAppId_10, value);
	}

	inline static int32_t get_offset_of_YDAppKey_11() { return static_cast<int32_t>(offsetof(PluginKuaiWanSJ_t791724598, ___YDAppKey_11)); }
	inline String_t* get_YDAppKey_11() const { return ___YDAppKey_11; }
	inline String_t** get_address_of_YDAppKey_11() { return &___YDAppKey_11; }
	inline void set_YDAppKey_11(String_t* value)
	{
		___YDAppKey_11 = value;
		Il2CppCodeGenWriteBarrier(&___YDAppKey_11, value);
	}

	inline static int32_t get_offset_of_YDChannel_12() { return static_cast<int32_t>(offsetof(PluginKuaiWanSJ_t791724598, ___YDChannel_12)); }
	inline String_t* get_YDChannel_12() const { return ___YDChannel_12; }
	inline String_t** get_address_of_YDChannel_12() { return &___YDChannel_12; }
	inline void set_YDChannel_12(String_t* value)
	{
		___YDChannel_12 = value;
		Il2CppCodeGenWriteBarrier(&___YDChannel_12, value);
	}

	inline static int32_t get_offset_of_payInfo_13() { return static_cast<int32_t>(offsetof(PluginKuaiWanSJ_t791724598, ___payInfo_13)); }
	inline PayInfo_t1775308120 * get_payInfo_13() const { return ___payInfo_13; }
	inline PayInfo_t1775308120 ** get_address_of_payInfo_13() { return &___payInfo_13; }
	inline void set_payInfo_13(PayInfo_t1775308120 * value)
	{
		___payInfo_13 = value;
		Il2CppCodeGenWriteBarrier(&___payInfo_13, value);
	}
};

struct PluginKuaiWanSJ_t791724598_StaticFields
{
public:
	// System.Action PluginKuaiWanSJ::<>f__am$cacheC
	Action_t3771233898 * ___U3CU3Ef__amU24cacheC_14;
	// System.Action PluginKuaiWanSJ::<>f__am$cacheD
	Action_t3771233898 * ___U3CU3Ef__amU24cacheD_15;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheC_14() { return static_cast<int32_t>(offsetof(PluginKuaiWanSJ_t791724598_StaticFields, ___U3CU3Ef__amU24cacheC_14)); }
	inline Action_t3771233898 * get_U3CU3Ef__amU24cacheC_14() const { return ___U3CU3Ef__amU24cacheC_14; }
	inline Action_t3771233898 ** get_address_of_U3CU3Ef__amU24cacheC_14() { return &___U3CU3Ef__amU24cacheC_14; }
	inline void set_U3CU3Ef__amU24cacheC_14(Action_t3771233898 * value)
	{
		___U3CU3Ef__amU24cacheC_14 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheC_14, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheD_15() { return static_cast<int32_t>(offsetof(PluginKuaiWanSJ_t791724598_StaticFields, ___U3CU3Ef__amU24cacheD_15)); }
	inline Action_t3771233898 * get_U3CU3Ef__amU24cacheD_15() const { return ___U3CU3Ef__amU24cacheD_15; }
	inline Action_t3771233898 ** get_address_of_U3CU3Ef__amU24cacheD_15() { return &___U3CU3Ef__amU24cacheD_15; }
	inline void set_U3CU3Ef__amU24cacheD_15(Action_t3771233898 * value)
	{
		___U3CU3Ef__amU24cacheD_15 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheD_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
