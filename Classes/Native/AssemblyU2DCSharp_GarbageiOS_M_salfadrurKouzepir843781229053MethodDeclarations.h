﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_salfadrurKouzepir84
struct M_salfadrurKouzepir84_t3781229053;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"

// System.Void GarbageiOS.M_salfadrurKouzepir84::.ctor()
extern "C"  void M_salfadrurKouzepir84__ctor_m1460327942 (M_salfadrurKouzepir84_t3781229053 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_salfadrurKouzepir84::M_dasvearmall0(System.String[],System.Int32)
extern "C"  void M_salfadrurKouzepir84_M_dasvearmall0_m3246713773 (M_salfadrurKouzepir84_t3781229053 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_salfadrurKouzepir84::M_berjirLerlis1(System.String[],System.Int32)
extern "C"  void M_salfadrurKouzepir84_M_berjirLerlis1_m162602233 (M_salfadrurKouzepir84_t3781229053 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
