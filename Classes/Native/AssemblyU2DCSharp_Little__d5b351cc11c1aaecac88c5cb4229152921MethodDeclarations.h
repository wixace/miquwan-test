﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._d5b351cc11c1aaecac88c5cb156578cb
struct _d5b351cc11c1aaecac88c5cb156578cb_t4229152921;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._d5b351cc11c1aaecac88c5cb156578cb::.ctor()
extern "C"  void _d5b351cc11c1aaecac88c5cb156578cb__ctor_m3117154740 (_d5b351cc11c1aaecac88c5cb156578cb_t4229152921 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._d5b351cc11c1aaecac88c5cb156578cb::_d5b351cc11c1aaecac88c5cb156578cbm2(System.Int32)
extern "C"  int32_t _d5b351cc11c1aaecac88c5cb156578cb__d5b351cc11c1aaecac88c5cb156578cbm2_m2380333817 (_d5b351cc11c1aaecac88c5cb156578cb_t4229152921 * __this, int32_t ____d5b351cc11c1aaecac88c5cb156578cba0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._d5b351cc11c1aaecac88c5cb156578cb::_d5b351cc11c1aaecac88c5cb156578cbm(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _d5b351cc11c1aaecac88c5cb156578cb__d5b351cc11c1aaecac88c5cb156578cbm_m3829657565 (_d5b351cc11c1aaecac88c5cb156578cb_t4229152921 * __this, int32_t ____d5b351cc11c1aaecac88c5cb156578cba0, int32_t ____d5b351cc11c1aaecac88c5cb156578cb341, int32_t ____d5b351cc11c1aaecac88c5cb156578cbc2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
