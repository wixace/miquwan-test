﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<CameraShotsCfg>
struct List_1_t3108632096;
// ProtoBuf.IExtension
struct IExtension_t1606339106;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraShotConfig
struct  CameraShotConfig_t493527041  : public Il2CppObject
{
public:
	// System.Int32 CameraShotConfig::_id
	int32_t ____id_0;
	// System.Collections.Generic.List`1<CameraShotsCfg> CameraShotConfig::_cameraShots
	List_1_t3108632096 * ____cameraShots_1;
	// ProtoBuf.IExtension CameraShotConfig::extensionObject
	Il2CppObject * ___extensionObject_2;

public:
	inline static int32_t get_offset_of__id_0() { return static_cast<int32_t>(offsetof(CameraShotConfig_t493527041, ____id_0)); }
	inline int32_t get__id_0() const { return ____id_0; }
	inline int32_t* get_address_of__id_0() { return &____id_0; }
	inline void set__id_0(int32_t value)
	{
		____id_0 = value;
	}

	inline static int32_t get_offset_of__cameraShots_1() { return static_cast<int32_t>(offsetof(CameraShotConfig_t493527041, ____cameraShots_1)); }
	inline List_1_t3108632096 * get__cameraShots_1() const { return ____cameraShots_1; }
	inline List_1_t3108632096 ** get_address_of__cameraShots_1() { return &____cameraShots_1; }
	inline void set__cameraShots_1(List_1_t3108632096 * value)
	{
		____cameraShots_1 = value;
		Il2CppCodeGenWriteBarrier(&____cameraShots_1, value);
	}

	inline static int32_t get_offset_of_extensionObject_2() { return static_cast<int32_t>(offsetof(CameraShotConfig_t493527041, ___extensionObject_2)); }
	inline Il2CppObject * get_extensionObject_2() const { return ___extensionObject_2; }
	inline Il2CppObject ** get_address_of_extensionObject_2() { return &___extensionObject_2; }
	inline void set_extensionObject_2(Il2CppObject * value)
	{
		___extensionObject_2 = value;
		Il2CppCodeGenWriteBarrier(&___extensionObject_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
