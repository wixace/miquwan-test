﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ShakeCamera
struct ShakeCamera_t1941686699;

#include "AssemblyU2DCSharp_TweenPosition3684358292.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ShakeCamera
struct  ShakeCamera_t1941686699  : public TweenPosition_t3684358292
{
public:

public:
};

struct ShakeCamera_t1941686699_StaticFields
{
public:
	// ShakeCamera ShakeCamera::shakeCamera
	ShakeCamera_t1941686699 * ___shakeCamera_25;
	// System.Boolean ShakeCamera::<isshake>k__BackingField
	bool ___U3CisshakeU3Ek__BackingField_26;

public:
	inline static int32_t get_offset_of_shakeCamera_25() { return static_cast<int32_t>(offsetof(ShakeCamera_t1941686699_StaticFields, ___shakeCamera_25)); }
	inline ShakeCamera_t1941686699 * get_shakeCamera_25() const { return ___shakeCamera_25; }
	inline ShakeCamera_t1941686699 ** get_address_of_shakeCamera_25() { return &___shakeCamera_25; }
	inline void set_shakeCamera_25(ShakeCamera_t1941686699 * value)
	{
		___shakeCamera_25 = value;
		Il2CppCodeGenWriteBarrier(&___shakeCamera_25, value);
	}

	inline static int32_t get_offset_of_U3CisshakeU3Ek__BackingField_26() { return static_cast<int32_t>(offsetof(ShakeCamera_t1941686699_StaticFields, ___U3CisshakeU3Ek__BackingField_26)); }
	inline bool get_U3CisshakeU3Ek__BackingField_26() const { return ___U3CisshakeU3Ek__BackingField_26; }
	inline bool* get_address_of_U3CisshakeU3Ek__BackingField_26() { return &___U3CisshakeU3Ek__BackingField_26; }
	inline void set_U3CisshakeU3Ek__BackingField_26(bool value)
	{
		___U3CisshakeU3Ek__BackingField_26 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
