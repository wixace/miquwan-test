﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_EventSystems_EventSystemGenerated
struct UnityEngine_EventSystems_EventSystemGenerated_t2824389331;
// JSVCall
struct JSVCall_t3708497963;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void UnityEngine_EventSystems_EventSystemGenerated::.ctor()
extern "C"  void UnityEngine_EventSystems_EventSystemGenerated__ctor_m935754744 (UnityEngine_EventSystems_EventSystemGenerated_t2824389331 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_EventSystemGenerated::EventSystem_current(JSVCall)
extern "C"  void UnityEngine_EventSystems_EventSystemGenerated_EventSystem_current_m3977856846 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_EventSystemGenerated::EventSystem_sendNavigationEvents(JSVCall)
extern "C"  void UnityEngine_EventSystems_EventSystemGenerated_EventSystem_sendNavigationEvents_m669873072 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_EventSystemGenerated::EventSystem_pixelDragThreshold(JSVCall)
extern "C"  void UnityEngine_EventSystems_EventSystemGenerated_EventSystem_pixelDragThreshold_m2371288628 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_EventSystemGenerated::EventSystem_currentInputModule(JSVCall)
extern "C"  void UnityEngine_EventSystems_EventSystemGenerated_EventSystem_currentInputModule_m1236680904 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_EventSystemGenerated::EventSystem_firstSelectedGameObject(JSVCall)
extern "C"  void UnityEngine_EventSystems_EventSystemGenerated_EventSystem_firstSelectedGameObject_m4090769547 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_EventSystemGenerated::EventSystem_currentSelectedGameObject(JSVCall)
extern "C"  void UnityEngine_EventSystems_EventSystemGenerated_EventSystem_currentSelectedGameObject_m1844513762 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_EventSystemGenerated::EventSystem_alreadySelecting(JSVCall)
extern "C"  void UnityEngine_EventSystems_EventSystemGenerated_EventSystem_alreadySelecting_m2225974103 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_EventSystems_EventSystemGenerated::EventSystem_IsPointerOverGameObject__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_EventSystems_EventSystemGenerated_EventSystem_IsPointerOverGameObject__Int32_m1079674708 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_EventSystems_EventSystemGenerated::EventSystem_IsPointerOverGameObject(JSVCall,System.Int32)
extern "C"  bool UnityEngine_EventSystems_EventSystemGenerated_EventSystem_IsPointerOverGameObject_m1183472764 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_EventSystems_EventSystemGenerated::EventSystem_RaycastAll__PointerEventData__ListT1_RaycastResult(JSVCall,System.Int32)
extern "C"  bool UnityEngine_EventSystems_EventSystemGenerated_EventSystem_RaycastAll__PointerEventData__ListT1_RaycastResult_m3873825855 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_EventSystems_EventSystemGenerated::EventSystem_SetSelectedGameObject__GameObject__BaseEventData(JSVCall,System.Int32)
extern "C"  bool UnityEngine_EventSystems_EventSystemGenerated_EventSystem_SetSelectedGameObject__GameObject__BaseEventData_m3858283122 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_EventSystems_EventSystemGenerated::EventSystem_SetSelectedGameObject__GameObject(JSVCall,System.Int32)
extern "C"  bool UnityEngine_EventSystems_EventSystemGenerated_EventSystem_SetSelectedGameObject__GameObject_m2711452355 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_EventSystems_EventSystemGenerated::EventSystem_ToString(JSVCall,System.Int32)
extern "C"  bool UnityEngine_EventSystems_EventSystemGenerated_EventSystem_ToString_m3775902666 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_EventSystems_EventSystemGenerated::EventSystem_UpdateModules(JSVCall,System.Int32)
extern "C"  bool UnityEngine_EventSystems_EventSystemGenerated_EventSystem_UpdateModules_m2721411042 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_EventSystemGenerated::__Register()
extern "C"  void UnityEngine_EventSystems_EventSystemGenerated___Register_m2766257103 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_EventSystems_EventSystemGenerated::ilo_setObject1(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UnityEngine_EventSystems_EventSystemGenerated_ilo_setObject1_m2646462774 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_EventSystems_EventSystemGenerated::ilo_getBooleanS2(System.Int32)
extern "C"  bool UnityEngine_EventSystems_EventSystemGenerated_ilo_getBooleanS2_m2661723949 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_EventSystemGenerated::ilo_setInt323(System.Int32,System.Int32)
extern "C"  void UnityEngine_EventSystems_EventSystemGenerated_ilo_setInt323_m4031546780 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_EventSystems_EventSystemGenerated::ilo_getInt324(System.Int32)
extern "C"  int32_t UnityEngine_EventSystems_EventSystemGenerated_ilo_getInt324_m331476702 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_EventSystemGenerated::ilo_setBooleanS5(System.Int32,System.Boolean)
extern "C"  void UnityEngine_EventSystems_EventSystemGenerated_ilo_setBooleanS5_m3053703909 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_EventSystems_EventSystemGenerated::ilo_getObject6(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_EventSystems_EventSystemGenerated_ilo_getObject6_m622968242 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
