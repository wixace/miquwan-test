﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// Mihua.SDK.PayInfo
struct PayInfo_t1775308120;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PluginSamsung
struct  PluginSamsung_t1663707431  : public MonoBehaviour_t667441552
{
public:
	// System.String PluginSamsung::transid
	String_t* ___transid_2;
	// System.String PluginSamsung::loginParams
	String_t* ___loginParams_3;
	// Mihua.SDK.PayInfo PluginSamsung::info
	PayInfo_t1775308120 * ___info_4;
	// System.Boolean PluginSamsung::isOut
	bool ___isOut_5;

public:
	inline static int32_t get_offset_of_transid_2() { return static_cast<int32_t>(offsetof(PluginSamsung_t1663707431, ___transid_2)); }
	inline String_t* get_transid_2() const { return ___transid_2; }
	inline String_t** get_address_of_transid_2() { return &___transid_2; }
	inline void set_transid_2(String_t* value)
	{
		___transid_2 = value;
		Il2CppCodeGenWriteBarrier(&___transid_2, value);
	}

	inline static int32_t get_offset_of_loginParams_3() { return static_cast<int32_t>(offsetof(PluginSamsung_t1663707431, ___loginParams_3)); }
	inline String_t* get_loginParams_3() const { return ___loginParams_3; }
	inline String_t** get_address_of_loginParams_3() { return &___loginParams_3; }
	inline void set_loginParams_3(String_t* value)
	{
		___loginParams_3 = value;
		Il2CppCodeGenWriteBarrier(&___loginParams_3, value);
	}

	inline static int32_t get_offset_of_info_4() { return static_cast<int32_t>(offsetof(PluginSamsung_t1663707431, ___info_4)); }
	inline PayInfo_t1775308120 * get_info_4() const { return ___info_4; }
	inline PayInfo_t1775308120 ** get_address_of_info_4() { return &___info_4; }
	inline void set_info_4(PayInfo_t1775308120 * value)
	{
		___info_4 = value;
		Il2CppCodeGenWriteBarrier(&___info_4, value);
	}

	inline static int32_t get_offset_of_isOut_5() { return static_cast<int32_t>(offsetof(PluginSamsung_t1663707431, ___isOut_5)); }
	inline bool get_isOut_5() const { return ___isOut_5; }
	inline bool* get_address_of_isOut_5() { return &___isOut_5; }
	inline void set_isOut_5(bool value)
	{
		___isOut_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
