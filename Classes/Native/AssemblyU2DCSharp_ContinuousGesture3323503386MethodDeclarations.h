﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ContinuousGesture
struct ContinuousGesture_t3323503386;
// Gesture
struct Gesture_t1589572905;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ContinuousGesturePhase2680903297.h"
#include "AssemblyU2DCSharp_GestureRecognitionState3604239971.h"
#include "AssemblyU2DCSharp_Gesture1589572905.h"

// System.Void ContinuousGesture::.ctor()
extern "C"  void ContinuousGesture__ctor_m3208112017 (ContinuousGesture_t3323503386 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ContinuousGesturePhase ContinuousGesture::get_Phase()
extern "C"  int32_t ContinuousGesture_get_Phase_m4202573085 (ContinuousGesture_t3323503386 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GestureRecognitionState ContinuousGesture::ilo_get_State1(Gesture)
extern "C"  int32_t ContinuousGesture_ilo_get_State1_m3343268682 (Il2CppObject * __this /* static, unused */, Gesture_t1589572905 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
