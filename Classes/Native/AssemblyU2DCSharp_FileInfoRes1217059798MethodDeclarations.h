﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FileInfoRes
struct FileInfoRes_t1217059798;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"

// System.Void FileInfoRes::.ctor()
extern "C"  void FileInfoRes__ctor_m252416085 (FileInfoRes_t1217059798 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String FileInfoRes::ToString()
extern "C"  String_t* FileInfoRes_ToString_m3582582072 (FileInfoRes_t1217059798 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FileInfoRes::LoadByString(System.String[])
extern "C"  void FileInfoRes_LoadByString_m1614109923 (FileInfoRes_t1217059798 * __this, StringU5BU5D_t4054002952* ___infoArr0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
