﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PluginXiaoMai
struct PluginXiaoMai_t2024051491;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// UnityEngine.WWWForm
struct WWWForm_t461342257;
// Mihua.SDK.PayInfo
struct PayInfo_t1775308120;
// System.String
struct String_t;
// CEvent.EventFunc`1<CEvent.ZEvent>
struct EventFunc_1_t1114914310;
// VersionMgr
struct VersionMgr_t1322950208;
// Mihua.SDK.LvUpInfo
struct LvUpInfo_t411687177;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// System.Action
struct Action_t3771233898;
// PluginsSdkMgr
struct PluginsSdkMgr_t3884624670;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "AssemblyU2DCSharp_Mihua_SDK_PayInfo1775308120.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_VersionMgr1322950208.h"
#include "AssemblyU2DCSharp_PluginXiaoMai2024051491.h"
#include "System_Core_System_Action3771233898.h"

// System.Void PluginXiaoMai::.ctor()
extern "C"  void PluginXiaoMai__ctor_m2023073192 (PluginXiaoMai_t2024051491 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiaoMai::Init()
extern "C"  void PluginXiaoMai_Init_m1679365036 (PluginXiaoMai_t2024051491 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginXiaoMai::ReqSDKHttpLogin()
extern "C"  Dictionary_2_t827649927 * PluginXiaoMai_ReqSDKHttpLogin_m2683243929 (PluginXiaoMai_t2024051491 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginXiaoMai::IsLoginSuccess(System.Boolean)
extern "C"  bool PluginXiaoMai_IsLoginSuccess_m2647110083 (PluginXiaoMai_t2024051491 * __this, bool ___isOpenUseLogin0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiaoMai::OpenUserLogin()
extern "C"  void PluginXiaoMai_OpenUserLogin_m3049431034 (PluginXiaoMai_t2024051491 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiaoMai::UserPay(CEvent.ZEvent)
extern "C"  void PluginXiaoMai_UserPay_m3458590264 (PluginXiaoMai_t2024051491 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.WWWForm PluginXiaoMai::BuildOrderParam2WWWForm(Mihua.SDK.PayInfo)
extern "C"  WWWForm_t461342257 * PluginXiaoMai_BuildOrderParam2WWWForm_m2736948027 (PluginXiaoMai_t2024051491 * __this, PayInfo_t1775308120 * ___info0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiaoMai::CreateRole(CEvent.ZEvent)
extern "C"  void PluginXiaoMai_CreateRole_m732260813 (PluginXiaoMai_t2024051491 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiaoMai::EnterGame(CEvent.ZEvent)
extern "C"  void PluginXiaoMai_EnterGame_m1617103307 (PluginXiaoMai_t2024051491 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiaoMai::RoleUpgrade(CEvent.ZEvent)
extern "C"  void PluginXiaoMai_RoleUpgrade_m3888547247 (PluginXiaoMai_t2024051491 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiaoMai::OnLoginSuccess(System.String)
extern "C"  void PluginXiaoMai_OnLoginSuccess_m4291194541 (PluginXiaoMai_t2024051491 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiaoMai::OnLogoutSuccess(System.String)
extern "C"  void PluginXiaoMai_OnLogoutSuccess_m4181554562 (PluginXiaoMai_t2024051491 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiaoMai::OnLogout(System.String)
extern "C"  void PluginXiaoMai_OnLogout_m1581004797 (PluginXiaoMai_t2024051491 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiaoMai::SwitchAccount(System.String)
extern "C"  void PluginXiaoMai_SwitchAccount_m807646595 (PluginXiaoMai_t2024051491 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiaoMai::initSdk()
extern "C"  void PluginXiaoMai_initSdk_m222328880 (PluginXiaoMai_t2024051491 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiaoMai::login()
extern "C"  void PluginXiaoMai_login_m1545088015 (PluginXiaoMai_t2024051491 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiaoMai::logout()
extern "C"  void PluginXiaoMai_logout_m658910918 (PluginXiaoMai_t2024051491 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiaoMai::updateRoleInfo(System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginXiaoMai_updateRoleInfo_m2761459153 (PluginXiaoMai_t2024051491 * __this, String_t* ___dataType0, String_t* ___serverID1, String_t* ___serverName2, String_t* ___roleID3, String_t* ___roleName4, String_t* ___roleLevel5, String_t* ___roleVip6, String_t* ___roleBalence7, String_t* ___partyName8, String_t* ___rolelevelCtime9, String_t* ___rolelevelMtime10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiaoMai::pay(System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginXiaoMai_pay_m2424434252 (PluginXiaoMai_t2024051491 * __this, String_t* ___gameOrderId0, String_t* ___serverID1, String_t* ___key_serverName2, String_t* ___productId3, String_t* ___productName4, String_t* ___key_productdesc5, String_t* ___key_ext6, String_t* ___key_productPrice7, String_t* ___key_roleID8, String_t* ___key_roleName9, String_t* ___key_currencyName10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiaoMai::<OnLogoutSuccess>m__464()
extern "C"  void PluginXiaoMai_U3COnLogoutSuccessU3Em__464_m3863402635 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiaoMai::<OnLogout>m__465()
extern "C"  void PluginXiaoMai_U3COnLogoutU3Em__465_m2210066811 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiaoMai::<SwitchAccount>m__466()
extern "C"  void PluginXiaoMai_U3CSwitchAccountU3Em__466_m2657863246 (PluginXiaoMai_t2024051491 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiaoMai::ilo_AddEventListener1(System.String,CEvent.EventFunc`1<CEvent.ZEvent>)
extern "C"  void PluginXiaoMai_ilo_AddEventListener1_m4215813708 (Il2CppObject * __this /* static, unused */, String_t* ___eventType0, EventFunc_1_t1114914310 * ___fun1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionMgr PluginXiaoMai::ilo_get_Instance2()
extern "C"  VersionMgr_t1322950208 * PluginXiaoMai_ilo_get_Instance2_m3122985540 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginXiaoMai::ilo_get_isSdkLogin3(VersionMgr)
extern "C"  bool PluginXiaoMai_ilo_get_isSdkLogin3_m3995434264 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiaoMai::ilo_login4(PluginXiaoMai)
extern "C"  void PluginXiaoMai_ilo_login4_m540911765 (Il2CppObject * __this /* static, unused */, PluginXiaoMai_t2024051491 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiaoMai::ilo_updateRoleInfo5(PluginXiaoMai,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginXiaoMai_ilo_updateRoleInfo5_m2119031656 (Il2CppObject * __this /* static, unused */, PluginXiaoMai_t2024051491 * ____this0, String_t* ___dataType1, String_t* ___serverID2, String_t* ___serverName3, String_t* ___roleID4, String_t* ___roleName5, String_t* ___roleLevel6, String_t* ___roleVip7, String_t* ___roleBalence8, String_t* ___partyName9, String_t* ___rolelevelCtime10, String_t* ___rolelevelMtime11, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mihua.SDK.LvUpInfo PluginXiaoMai::ilo_Parse6(System.Object[])
extern "C"  LvUpInfo_t411687177 * PluginXiaoMai_ilo_Parse6_m1836874464 (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t1108656482* ___crtRoleOjs0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiaoMai::ilo_logout7(PluginXiaoMai)
extern "C"  void PluginXiaoMai_ilo_logout7_m1071674351 (Il2CppObject * __this /* static, unused */, PluginXiaoMai_t2024051491 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiaoMai::ilo_Logout8(System.Action)
extern "C"  void PluginXiaoMai_ilo_Logout8_m3801988220 (Il2CppObject * __this /* static, unused */, Action_t3771233898 * ___call0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PluginsSdkMgr PluginXiaoMai::ilo_get_PluginsSdkMgr9()
extern "C"  PluginsSdkMgr_t3884624670 * PluginXiaoMai_ilo_get_PluginsSdkMgr9_m1973209406 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
