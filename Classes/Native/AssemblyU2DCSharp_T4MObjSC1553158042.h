﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t215400611;
// T4MLodObjSC[]
struct T4MLodObjSCU5BU5D_t2636096386;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// UnityEngine.Transform
struct Transform_t1659122786;
// T4MBillBObjSC[]
struct T4MBillBObjSCU5BU5D_t3601884420;
// System.Single[]
struct SingleU5BU5D_t2316563989;
// UnityEngine.Material
struct Material_t3870600107;
// UnityEngine.MeshFilter
struct MeshFilter_t3839065225;
// UnityEngine.Texture2D
struct Texture2D_t3884108195;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"
#include "UnityEngine_UnityEngine_Vector44282066567.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// T4MObjSC
struct  T4MObjSC_t1553158042  : public MonoBehaviour_t667441552
{
public:
	// System.String T4MObjSC::ConvertType
	String_t* ___ConvertType_2;
	// System.Boolean T4MObjSC::EnabledLODSystem
	bool ___EnabledLODSystem_3;
	// UnityEngine.Vector3[] T4MObjSC::ObjPosition
	Vector3U5BU5D_t215400611* ___ObjPosition_4;
	// T4MLodObjSC[] T4MObjSC::ObjLodScript
	T4MLodObjSCU5BU5D_t2636096386* ___ObjLodScript_5;
	// System.Int32[] T4MObjSC::ObjLodStatus
	Int32U5BU5D_t3230847821* ___ObjLodStatus_6;
	// System.Single T4MObjSC::MaxViewDistance
	float ___MaxViewDistance_7;
	// System.Single T4MObjSC::LOD2Start
	float ___LOD2Start_8;
	// System.Single T4MObjSC::LOD3Start
	float ___LOD3Start_9;
	// System.Single T4MObjSC::Interval
	float ___Interval_10;
	// UnityEngine.Transform T4MObjSC::PlayerCamera
	Transform_t1659122786 * ___PlayerCamera_11;
	// UnityEngine.Vector3 T4MObjSC::OldPlayerPos
	Vector3_t4282066566  ___OldPlayerPos_12;
	// System.Int32 T4MObjSC::Mode
	int32_t ___Mode_13;
	// System.Int32 T4MObjSC::Master
	int32_t ___Master_14;
	// System.Boolean T4MObjSC::enabledBillboard
	bool ___enabledBillboard_15;
	// UnityEngine.Vector3[] T4MObjSC::BillboardPosition
	Vector3U5BU5D_t215400611* ___BillboardPosition_16;
	// System.Single T4MObjSC::BillInterval
	float ___BillInterval_17;
	// System.Int32[] T4MObjSC::BillStatus
	Int32U5BU5D_t3230847821* ___BillStatus_18;
	// System.Single T4MObjSC::BillMaxViewDistance
	float ___BillMaxViewDistance_19;
	// T4MBillBObjSC[] T4MObjSC::BillScript
	T4MBillBObjSCU5BU5D_t3601884420* ___BillScript_20;
	// System.Boolean T4MObjSC::enabledLayerCul
	bool ___enabledLayerCul_21;
	// System.Single T4MObjSC::BackGroundView
	float ___BackGroundView_22;
	// System.Single T4MObjSC::FarView
	float ___FarView_23;
	// System.Single T4MObjSC::NormalView
	float ___NormalView_24;
	// System.Single T4MObjSC::CloseView
	float ___CloseView_25;
	// System.Single[] T4MObjSC::distances
	SingleU5BU5D_t2316563989* ___distances_26;
	// System.Int32 T4MObjSC::Axis
	int32_t ___Axis_27;
	// System.Boolean T4MObjSC::LODbasedOnScript
	bool ___LODbasedOnScript_28;
	// System.Boolean T4MObjSC::BilBbasedOnScript
	bool ___BilBbasedOnScript_29;
	// UnityEngine.Material T4MObjSC::T4MMaterial
	Material_t3870600107 * ___T4MMaterial_30;
	// UnityEngine.MeshFilter T4MObjSC::T4MMesh
	MeshFilter_t3839065225 * ___T4MMesh_31;
	// UnityEngine.Color T4MObjSC::TranslucencyColor
	Color_t4194546905  ___TranslucencyColor_32;
	// UnityEngine.Vector4 T4MObjSC::Wind
	Vector4_t4282066567  ___Wind_33;
	// System.Single T4MObjSC::WindFrequency
	float ___WindFrequency_34;
	// System.Single T4MObjSC::GrassWindFrequency
	float ___GrassWindFrequency_35;
	// System.Boolean T4MObjSC::ActiveWind
	bool ___ActiveWind_36;
	// System.Boolean T4MObjSC::LayerCullPreview
	bool ___LayerCullPreview_37;
	// System.Boolean T4MObjSC::LODPreview
	bool ___LODPreview_38;
	// System.Boolean T4MObjSC::BillboardPreview
	bool ___BillboardPreview_39;
	// UnityEngine.Texture2D T4MObjSC::T4MMaskTex2d
	Texture2D_t3884108195 * ___T4MMaskTex2d_40;
	// UnityEngine.Texture2D T4MObjSC::T4MMaskTexd
	Texture2D_t3884108195 * ___T4MMaskTexd_41;

public:
	inline static int32_t get_offset_of_ConvertType_2() { return static_cast<int32_t>(offsetof(T4MObjSC_t1553158042, ___ConvertType_2)); }
	inline String_t* get_ConvertType_2() const { return ___ConvertType_2; }
	inline String_t** get_address_of_ConvertType_2() { return &___ConvertType_2; }
	inline void set_ConvertType_2(String_t* value)
	{
		___ConvertType_2 = value;
		Il2CppCodeGenWriteBarrier(&___ConvertType_2, value);
	}

	inline static int32_t get_offset_of_EnabledLODSystem_3() { return static_cast<int32_t>(offsetof(T4MObjSC_t1553158042, ___EnabledLODSystem_3)); }
	inline bool get_EnabledLODSystem_3() const { return ___EnabledLODSystem_3; }
	inline bool* get_address_of_EnabledLODSystem_3() { return &___EnabledLODSystem_3; }
	inline void set_EnabledLODSystem_3(bool value)
	{
		___EnabledLODSystem_3 = value;
	}

	inline static int32_t get_offset_of_ObjPosition_4() { return static_cast<int32_t>(offsetof(T4MObjSC_t1553158042, ___ObjPosition_4)); }
	inline Vector3U5BU5D_t215400611* get_ObjPosition_4() const { return ___ObjPosition_4; }
	inline Vector3U5BU5D_t215400611** get_address_of_ObjPosition_4() { return &___ObjPosition_4; }
	inline void set_ObjPosition_4(Vector3U5BU5D_t215400611* value)
	{
		___ObjPosition_4 = value;
		Il2CppCodeGenWriteBarrier(&___ObjPosition_4, value);
	}

	inline static int32_t get_offset_of_ObjLodScript_5() { return static_cast<int32_t>(offsetof(T4MObjSC_t1553158042, ___ObjLodScript_5)); }
	inline T4MLodObjSCU5BU5D_t2636096386* get_ObjLodScript_5() const { return ___ObjLodScript_5; }
	inline T4MLodObjSCU5BU5D_t2636096386** get_address_of_ObjLodScript_5() { return &___ObjLodScript_5; }
	inline void set_ObjLodScript_5(T4MLodObjSCU5BU5D_t2636096386* value)
	{
		___ObjLodScript_5 = value;
		Il2CppCodeGenWriteBarrier(&___ObjLodScript_5, value);
	}

	inline static int32_t get_offset_of_ObjLodStatus_6() { return static_cast<int32_t>(offsetof(T4MObjSC_t1553158042, ___ObjLodStatus_6)); }
	inline Int32U5BU5D_t3230847821* get_ObjLodStatus_6() const { return ___ObjLodStatus_6; }
	inline Int32U5BU5D_t3230847821** get_address_of_ObjLodStatus_6() { return &___ObjLodStatus_6; }
	inline void set_ObjLodStatus_6(Int32U5BU5D_t3230847821* value)
	{
		___ObjLodStatus_6 = value;
		Il2CppCodeGenWriteBarrier(&___ObjLodStatus_6, value);
	}

	inline static int32_t get_offset_of_MaxViewDistance_7() { return static_cast<int32_t>(offsetof(T4MObjSC_t1553158042, ___MaxViewDistance_7)); }
	inline float get_MaxViewDistance_7() const { return ___MaxViewDistance_7; }
	inline float* get_address_of_MaxViewDistance_7() { return &___MaxViewDistance_7; }
	inline void set_MaxViewDistance_7(float value)
	{
		___MaxViewDistance_7 = value;
	}

	inline static int32_t get_offset_of_LOD2Start_8() { return static_cast<int32_t>(offsetof(T4MObjSC_t1553158042, ___LOD2Start_8)); }
	inline float get_LOD2Start_8() const { return ___LOD2Start_8; }
	inline float* get_address_of_LOD2Start_8() { return &___LOD2Start_8; }
	inline void set_LOD2Start_8(float value)
	{
		___LOD2Start_8 = value;
	}

	inline static int32_t get_offset_of_LOD3Start_9() { return static_cast<int32_t>(offsetof(T4MObjSC_t1553158042, ___LOD3Start_9)); }
	inline float get_LOD3Start_9() const { return ___LOD3Start_9; }
	inline float* get_address_of_LOD3Start_9() { return &___LOD3Start_9; }
	inline void set_LOD3Start_9(float value)
	{
		___LOD3Start_9 = value;
	}

	inline static int32_t get_offset_of_Interval_10() { return static_cast<int32_t>(offsetof(T4MObjSC_t1553158042, ___Interval_10)); }
	inline float get_Interval_10() const { return ___Interval_10; }
	inline float* get_address_of_Interval_10() { return &___Interval_10; }
	inline void set_Interval_10(float value)
	{
		___Interval_10 = value;
	}

	inline static int32_t get_offset_of_PlayerCamera_11() { return static_cast<int32_t>(offsetof(T4MObjSC_t1553158042, ___PlayerCamera_11)); }
	inline Transform_t1659122786 * get_PlayerCamera_11() const { return ___PlayerCamera_11; }
	inline Transform_t1659122786 ** get_address_of_PlayerCamera_11() { return &___PlayerCamera_11; }
	inline void set_PlayerCamera_11(Transform_t1659122786 * value)
	{
		___PlayerCamera_11 = value;
		Il2CppCodeGenWriteBarrier(&___PlayerCamera_11, value);
	}

	inline static int32_t get_offset_of_OldPlayerPos_12() { return static_cast<int32_t>(offsetof(T4MObjSC_t1553158042, ___OldPlayerPos_12)); }
	inline Vector3_t4282066566  get_OldPlayerPos_12() const { return ___OldPlayerPos_12; }
	inline Vector3_t4282066566 * get_address_of_OldPlayerPos_12() { return &___OldPlayerPos_12; }
	inline void set_OldPlayerPos_12(Vector3_t4282066566  value)
	{
		___OldPlayerPos_12 = value;
	}

	inline static int32_t get_offset_of_Mode_13() { return static_cast<int32_t>(offsetof(T4MObjSC_t1553158042, ___Mode_13)); }
	inline int32_t get_Mode_13() const { return ___Mode_13; }
	inline int32_t* get_address_of_Mode_13() { return &___Mode_13; }
	inline void set_Mode_13(int32_t value)
	{
		___Mode_13 = value;
	}

	inline static int32_t get_offset_of_Master_14() { return static_cast<int32_t>(offsetof(T4MObjSC_t1553158042, ___Master_14)); }
	inline int32_t get_Master_14() const { return ___Master_14; }
	inline int32_t* get_address_of_Master_14() { return &___Master_14; }
	inline void set_Master_14(int32_t value)
	{
		___Master_14 = value;
	}

	inline static int32_t get_offset_of_enabledBillboard_15() { return static_cast<int32_t>(offsetof(T4MObjSC_t1553158042, ___enabledBillboard_15)); }
	inline bool get_enabledBillboard_15() const { return ___enabledBillboard_15; }
	inline bool* get_address_of_enabledBillboard_15() { return &___enabledBillboard_15; }
	inline void set_enabledBillboard_15(bool value)
	{
		___enabledBillboard_15 = value;
	}

	inline static int32_t get_offset_of_BillboardPosition_16() { return static_cast<int32_t>(offsetof(T4MObjSC_t1553158042, ___BillboardPosition_16)); }
	inline Vector3U5BU5D_t215400611* get_BillboardPosition_16() const { return ___BillboardPosition_16; }
	inline Vector3U5BU5D_t215400611** get_address_of_BillboardPosition_16() { return &___BillboardPosition_16; }
	inline void set_BillboardPosition_16(Vector3U5BU5D_t215400611* value)
	{
		___BillboardPosition_16 = value;
		Il2CppCodeGenWriteBarrier(&___BillboardPosition_16, value);
	}

	inline static int32_t get_offset_of_BillInterval_17() { return static_cast<int32_t>(offsetof(T4MObjSC_t1553158042, ___BillInterval_17)); }
	inline float get_BillInterval_17() const { return ___BillInterval_17; }
	inline float* get_address_of_BillInterval_17() { return &___BillInterval_17; }
	inline void set_BillInterval_17(float value)
	{
		___BillInterval_17 = value;
	}

	inline static int32_t get_offset_of_BillStatus_18() { return static_cast<int32_t>(offsetof(T4MObjSC_t1553158042, ___BillStatus_18)); }
	inline Int32U5BU5D_t3230847821* get_BillStatus_18() const { return ___BillStatus_18; }
	inline Int32U5BU5D_t3230847821** get_address_of_BillStatus_18() { return &___BillStatus_18; }
	inline void set_BillStatus_18(Int32U5BU5D_t3230847821* value)
	{
		___BillStatus_18 = value;
		Il2CppCodeGenWriteBarrier(&___BillStatus_18, value);
	}

	inline static int32_t get_offset_of_BillMaxViewDistance_19() { return static_cast<int32_t>(offsetof(T4MObjSC_t1553158042, ___BillMaxViewDistance_19)); }
	inline float get_BillMaxViewDistance_19() const { return ___BillMaxViewDistance_19; }
	inline float* get_address_of_BillMaxViewDistance_19() { return &___BillMaxViewDistance_19; }
	inline void set_BillMaxViewDistance_19(float value)
	{
		___BillMaxViewDistance_19 = value;
	}

	inline static int32_t get_offset_of_BillScript_20() { return static_cast<int32_t>(offsetof(T4MObjSC_t1553158042, ___BillScript_20)); }
	inline T4MBillBObjSCU5BU5D_t3601884420* get_BillScript_20() const { return ___BillScript_20; }
	inline T4MBillBObjSCU5BU5D_t3601884420** get_address_of_BillScript_20() { return &___BillScript_20; }
	inline void set_BillScript_20(T4MBillBObjSCU5BU5D_t3601884420* value)
	{
		___BillScript_20 = value;
		Il2CppCodeGenWriteBarrier(&___BillScript_20, value);
	}

	inline static int32_t get_offset_of_enabledLayerCul_21() { return static_cast<int32_t>(offsetof(T4MObjSC_t1553158042, ___enabledLayerCul_21)); }
	inline bool get_enabledLayerCul_21() const { return ___enabledLayerCul_21; }
	inline bool* get_address_of_enabledLayerCul_21() { return &___enabledLayerCul_21; }
	inline void set_enabledLayerCul_21(bool value)
	{
		___enabledLayerCul_21 = value;
	}

	inline static int32_t get_offset_of_BackGroundView_22() { return static_cast<int32_t>(offsetof(T4MObjSC_t1553158042, ___BackGroundView_22)); }
	inline float get_BackGroundView_22() const { return ___BackGroundView_22; }
	inline float* get_address_of_BackGroundView_22() { return &___BackGroundView_22; }
	inline void set_BackGroundView_22(float value)
	{
		___BackGroundView_22 = value;
	}

	inline static int32_t get_offset_of_FarView_23() { return static_cast<int32_t>(offsetof(T4MObjSC_t1553158042, ___FarView_23)); }
	inline float get_FarView_23() const { return ___FarView_23; }
	inline float* get_address_of_FarView_23() { return &___FarView_23; }
	inline void set_FarView_23(float value)
	{
		___FarView_23 = value;
	}

	inline static int32_t get_offset_of_NormalView_24() { return static_cast<int32_t>(offsetof(T4MObjSC_t1553158042, ___NormalView_24)); }
	inline float get_NormalView_24() const { return ___NormalView_24; }
	inline float* get_address_of_NormalView_24() { return &___NormalView_24; }
	inline void set_NormalView_24(float value)
	{
		___NormalView_24 = value;
	}

	inline static int32_t get_offset_of_CloseView_25() { return static_cast<int32_t>(offsetof(T4MObjSC_t1553158042, ___CloseView_25)); }
	inline float get_CloseView_25() const { return ___CloseView_25; }
	inline float* get_address_of_CloseView_25() { return &___CloseView_25; }
	inline void set_CloseView_25(float value)
	{
		___CloseView_25 = value;
	}

	inline static int32_t get_offset_of_distances_26() { return static_cast<int32_t>(offsetof(T4MObjSC_t1553158042, ___distances_26)); }
	inline SingleU5BU5D_t2316563989* get_distances_26() const { return ___distances_26; }
	inline SingleU5BU5D_t2316563989** get_address_of_distances_26() { return &___distances_26; }
	inline void set_distances_26(SingleU5BU5D_t2316563989* value)
	{
		___distances_26 = value;
		Il2CppCodeGenWriteBarrier(&___distances_26, value);
	}

	inline static int32_t get_offset_of_Axis_27() { return static_cast<int32_t>(offsetof(T4MObjSC_t1553158042, ___Axis_27)); }
	inline int32_t get_Axis_27() const { return ___Axis_27; }
	inline int32_t* get_address_of_Axis_27() { return &___Axis_27; }
	inline void set_Axis_27(int32_t value)
	{
		___Axis_27 = value;
	}

	inline static int32_t get_offset_of_LODbasedOnScript_28() { return static_cast<int32_t>(offsetof(T4MObjSC_t1553158042, ___LODbasedOnScript_28)); }
	inline bool get_LODbasedOnScript_28() const { return ___LODbasedOnScript_28; }
	inline bool* get_address_of_LODbasedOnScript_28() { return &___LODbasedOnScript_28; }
	inline void set_LODbasedOnScript_28(bool value)
	{
		___LODbasedOnScript_28 = value;
	}

	inline static int32_t get_offset_of_BilBbasedOnScript_29() { return static_cast<int32_t>(offsetof(T4MObjSC_t1553158042, ___BilBbasedOnScript_29)); }
	inline bool get_BilBbasedOnScript_29() const { return ___BilBbasedOnScript_29; }
	inline bool* get_address_of_BilBbasedOnScript_29() { return &___BilBbasedOnScript_29; }
	inline void set_BilBbasedOnScript_29(bool value)
	{
		___BilBbasedOnScript_29 = value;
	}

	inline static int32_t get_offset_of_T4MMaterial_30() { return static_cast<int32_t>(offsetof(T4MObjSC_t1553158042, ___T4MMaterial_30)); }
	inline Material_t3870600107 * get_T4MMaterial_30() const { return ___T4MMaterial_30; }
	inline Material_t3870600107 ** get_address_of_T4MMaterial_30() { return &___T4MMaterial_30; }
	inline void set_T4MMaterial_30(Material_t3870600107 * value)
	{
		___T4MMaterial_30 = value;
		Il2CppCodeGenWriteBarrier(&___T4MMaterial_30, value);
	}

	inline static int32_t get_offset_of_T4MMesh_31() { return static_cast<int32_t>(offsetof(T4MObjSC_t1553158042, ___T4MMesh_31)); }
	inline MeshFilter_t3839065225 * get_T4MMesh_31() const { return ___T4MMesh_31; }
	inline MeshFilter_t3839065225 ** get_address_of_T4MMesh_31() { return &___T4MMesh_31; }
	inline void set_T4MMesh_31(MeshFilter_t3839065225 * value)
	{
		___T4MMesh_31 = value;
		Il2CppCodeGenWriteBarrier(&___T4MMesh_31, value);
	}

	inline static int32_t get_offset_of_TranslucencyColor_32() { return static_cast<int32_t>(offsetof(T4MObjSC_t1553158042, ___TranslucencyColor_32)); }
	inline Color_t4194546905  get_TranslucencyColor_32() const { return ___TranslucencyColor_32; }
	inline Color_t4194546905 * get_address_of_TranslucencyColor_32() { return &___TranslucencyColor_32; }
	inline void set_TranslucencyColor_32(Color_t4194546905  value)
	{
		___TranslucencyColor_32 = value;
	}

	inline static int32_t get_offset_of_Wind_33() { return static_cast<int32_t>(offsetof(T4MObjSC_t1553158042, ___Wind_33)); }
	inline Vector4_t4282066567  get_Wind_33() const { return ___Wind_33; }
	inline Vector4_t4282066567 * get_address_of_Wind_33() { return &___Wind_33; }
	inline void set_Wind_33(Vector4_t4282066567  value)
	{
		___Wind_33 = value;
	}

	inline static int32_t get_offset_of_WindFrequency_34() { return static_cast<int32_t>(offsetof(T4MObjSC_t1553158042, ___WindFrequency_34)); }
	inline float get_WindFrequency_34() const { return ___WindFrequency_34; }
	inline float* get_address_of_WindFrequency_34() { return &___WindFrequency_34; }
	inline void set_WindFrequency_34(float value)
	{
		___WindFrequency_34 = value;
	}

	inline static int32_t get_offset_of_GrassWindFrequency_35() { return static_cast<int32_t>(offsetof(T4MObjSC_t1553158042, ___GrassWindFrequency_35)); }
	inline float get_GrassWindFrequency_35() const { return ___GrassWindFrequency_35; }
	inline float* get_address_of_GrassWindFrequency_35() { return &___GrassWindFrequency_35; }
	inline void set_GrassWindFrequency_35(float value)
	{
		___GrassWindFrequency_35 = value;
	}

	inline static int32_t get_offset_of_ActiveWind_36() { return static_cast<int32_t>(offsetof(T4MObjSC_t1553158042, ___ActiveWind_36)); }
	inline bool get_ActiveWind_36() const { return ___ActiveWind_36; }
	inline bool* get_address_of_ActiveWind_36() { return &___ActiveWind_36; }
	inline void set_ActiveWind_36(bool value)
	{
		___ActiveWind_36 = value;
	}

	inline static int32_t get_offset_of_LayerCullPreview_37() { return static_cast<int32_t>(offsetof(T4MObjSC_t1553158042, ___LayerCullPreview_37)); }
	inline bool get_LayerCullPreview_37() const { return ___LayerCullPreview_37; }
	inline bool* get_address_of_LayerCullPreview_37() { return &___LayerCullPreview_37; }
	inline void set_LayerCullPreview_37(bool value)
	{
		___LayerCullPreview_37 = value;
	}

	inline static int32_t get_offset_of_LODPreview_38() { return static_cast<int32_t>(offsetof(T4MObjSC_t1553158042, ___LODPreview_38)); }
	inline bool get_LODPreview_38() const { return ___LODPreview_38; }
	inline bool* get_address_of_LODPreview_38() { return &___LODPreview_38; }
	inline void set_LODPreview_38(bool value)
	{
		___LODPreview_38 = value;
	}

	inline static int32_t get_offset_of_BillboardPreview_39() { return static_cast<int32_t>(offsetof(T4MObjSC_t1553158042, ___BillboardPreview_39)); }
	inline bool get_BillboardPreview_39() const { return ___BillboardPreview_39; }
	inline bool* get_address_of_BillboardPreview_39() { return &___BillboardPreview_39; }
	inline void set_BillboardPreview_39(bool value)
	{
		___BillboardPreview_39 = value;
	}

	inline static int32_t get_offset_of_T4MMaskTex2d_40() { return static_cast<int32_t>(offsetof(T4MObjSC_t1553158042, ___T4MMaskTex2d_40)); }
	inline Texture2D_t3884108195 * get_T4MMaskTex2d_40() const { return ___T4MMaskTex2d_40; }
	inline Texture2D_t3884108195 ** get_address_of_T4MMaskTex2d_40() { return &___T4MMaskTex2d_40; }
	inline void set_T4MMaskTex2d_40(Texture2D_t3884108195 * value)
	{
		___T4MMaskTex2d_40 = value;
		Il2CppCodeGenWriteBarrier(&___T4MMaskTex2d_40, value);
	}

	inline static int32_t get_offset_of_T4MMaskTexd_41() { return static_cast<int32_t>(offsetof(T4MObjSC_t1553158042, ___T4MMaskTexd_41)); }
	inline Texture2D_t3884108195 * get_T4MMaskTexd_41() const { return ___T4MMaskTexd_41; }
	inline Texture2D_t3884108195 ** get_address_of_T4MMaskTexd_41() { return &___T4MMaskTexd_41; }
	inline void set_T4MMaskTexd_41(Texture2D_t3884108195 * value)
	{
		___T4MMaskTexd_41 = value;
		Il2CppCodeGenWriteBarrier(&___T4MMaskTexd_41, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
