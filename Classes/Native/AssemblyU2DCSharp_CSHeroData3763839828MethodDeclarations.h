﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CSHeroData
struct CSHeroData_t3763839828;
// System.String
struct String_t;
// CSHeroUnit
struct CSHeroUnit_t3764358446;
// System.Collections.Generic.List`1<CSHeroUnit>
struct List_1_t837576702;
// System.Collections.Generic.List`1<CSPlusAtt>
struct List_1_t341533415;
// CSPlusAtt
struct CSPlusAtt_t3268315159;
// System.Object
struct Il2CppObject;
// Newtonsoft.Json.Linq.JToken
struct JToken_t3412245951;
// Newtonsoft.Json.Linq.JObject
struct JObject_t1798544199;
// Newtonsoft.Json.Linq.JArray
struct JArray_t3394795039;
// Newtonsoft.Json.Linq.JContainer
struct JContainer_t3364442311;
// ReplayMgr
struct ReplayMgr_t1549183121;
// GameMgr
struct GameMgr_t1469029542;
// CSGameDataMgr
struct CSGameDataMgr_t2623305516;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_CSHeroUnit3764358446.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JObject1798544199.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JArray3394795039.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JContainer3364442311.h"
#include "AssemblyU2DCSharp_ReplayMgr1549183121.h"
#include "AssemblyU2DCSharp_CSHeroData3763839828.h"
#include "AssemblyU2DCSharp_GameMgr1469029542.h"

// System.Void CSHeroData::.ctor()
extern "C"  void CSHeroData__ctor_m410284999 (CSHeroData_t3763839828 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroData::LoadData(System.String)
extern "C"  void CSHeroData_LoadData_m3340153813 (CSHeroData_t3763839828 * __this, String_t* ___jsonStr0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroData::UnLoadData()
extern "C"  void CSHeroData_UnLoadData_m3137067206 (CSHeroData_t3763839828 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CSHeroUnit CSHeroData::GetHeroUnit(System.UInt32)
extern "C"  CSHeroUnit_t3764358446 * CSHeroData_GetHeroUnit_m254331760 (CSHeroData_t3763839828 * __this, uint32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CSHeroUnit> CSHeroData::GetHeroInfos()
extern "C"  List_1_t837576702 * CSHeroData_GetHeroInfos_m3555979859 (CSHeroData_t3763839828 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CSHeroUnit> CSHeroData::GetFightingHeroUntis(System.Int32)
extern "C"  List_1_t837576702 * CSHeroData_GetFightingHeroUntis_m3251742674 (CSHeroData_t3763839828 * __this, int32_t ___fuctionType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CSHeroUnit> CSHeroData::GetFightingHeroUntis2()
extern "C"  List_1_t837576702 * CSHeroData_GetFightingHeroUntis2_m849000883 (CSHeroData_t3763839828 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CSPlusAtt> CSHeroData::GetLeaderPlusAtt()
extern "C"  List_1_t341533415 * CSHeroData_GetLeaderPlusAtt_m2897143645 (CSHeroData_t3763839828 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CSPlusAtt> CSHeroData::GetReinPlusAtts()
extern "C"  List_1_t341533415 * CSHeroData_GetReinPlusAtts_m2789979431 (CSHeroData_t3763839828 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CSPlusAtt CSHeroData::GetLearerAttUnit(System.String)
extern "C"  CSPlusAtt_t3268315159 * CSHeroData_GetLearerAttUnit_m2140503921 (CSHeroData_t3763839828 * __this, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CSPlusAtt CSHeroData::GetReinAttUnit(System.String)
extern "C"  CSPlusAtt_t3268315159 * CSHeroData_GetReinAttUnit_m1067913522 (CSHeroData_t3763839828 * __this, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CSHeroData::<GetFightingHeroUntis>m__3D6(CSHeroUnit,CSHeroUnit)
extern "C"  int32_t CSHeroData_U3CGetFightingHeroUntisU3Em__3D6_m2803048100 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ___hu10, CSHeroUnit_t3764358446 * ___hu21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object CSHeroData::ilo_DeserializeObject1(System.String)
extern "C"  Il2CppObject * CSHeroData_ilo_DeserializeObject1_m4269078506 (Il2CppObject * __this /* static, unused */, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken CSHeroData::ilo_get_Item2(Newtonsoft.Json.Linq.JObject,System.String)
extern "C"  JToken_t3412245951 * CSHeroData_ilo_get_Item2_m4030202665 (Il2CppObject * __this /* static, unused */, JObject_t1798544199 * ____this0, String_t* ___propertyName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken CSHeroData::ilo_get_Item3(Newtonsoft.Json.Linq.JArray,System.Int32)
extern "C"  JToken_t3412245951 * CSHeroData_ilo_get_Item3_m4106257675 (Il2CppObject * __this /* static, unused */, JArray_t3394795039 * ____this0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CSHeroData::ilo_get_Count4(Newtonsoft.Json.Linq.JContainer)
extern "C"  int32_t CSHeroData_ilo_get_Count4_m30310198 (Il2CppObject * __this /* static, unused */, JContainer_t3364442311 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CSHeroData::ilo_get_IsPlayREC5(ReplayMgr)
extern "C"  bool CSHeroData_ilo_get_IsPlayREC5_m1151840113 (Il2CppObject * __this /* static, unused */, ReplayMgr_t1549183121 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CSHeroUnit CSHeroData::ilo_GetHeroUnit6(CSHeroData,System.UInt32)
extern "C"  CSHeroUnit_t3764358446 * CSHeroData_ilo_GetHeroUnit6_m3213294625 (Il2CppObject * __this /* static, unused */, CSHeroData_t3763839828 * ____this0, uint32_t ___id1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CSHeroData::ilo_get_IsLeaderPlusAtt7(GameMgr)
extern "C"  bool CSHeroData_ilo_get_IsLeaderPlusAtt7_m2073778460 (Il2CppObject * __this /* static, unused */, GameMgr_t1469029542 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CSGameDataMgr CSHeroData::ilo_get_CSGameDataMgr8()
extern "C"  CSGameDataMgr_t2623305516 * CSHeroData_ilo_get_CSGameDataMgr8_m653309228 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CSHeroUnit> CSHeroData::ilo_GetFightingHeroUntis9(CSHeroData,System.Int32)
extern "C"  List_1_t837576702 * CSHeroData_ilo_GetFightingHeroUntis9_m2968227620 (Il2CppObject * __this /* static, unused */, CSHeroData_t3763839828 * ____this0, int32_t ___fuctionType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 CSHeroData::ilo_get_mag_def10(CSHeroUnit)
extern "C"  uint32_t CSHeroData_ilo_get_mag_def10_m2063762136 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 CSHeroData::ilo_get_location11(CSHeroUnit)
extern "C"  uint32_t CSHeroData_ilo_get_location11_m1279107579 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
