﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PluginsSdkMgrGenerated
struct PluginsSdkMgrGenerated_t728608785;
// JSVCall
struct JSVCall_t3708497963;
// System.Action
struct Action_t3771233898;
// CSRepresentedObject
struct CSRepresentedObject_t3994124630;
// System.String
struct String_t;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;
// PluginsSdkMgr
struct PluginsSdkMgr_t3884624670;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// System.Delegate
struct Delegate_t3310234105;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_CSRepresentedObject3994124630.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_PluginsSdkMgr3884624670.h"
#include "AssemblyU2DCSharp_Gender2129321697.h"
#include "AssemblyU2DCSharp_PushType1840642452.h"
#include "mscorlib_System_Delegate3310234105.h"

// System.Void PluginsSdkMgrGenerated::.ctor()
extern "C"  void PluginsSdkMgrGenerated__ctor_m3033860010 (PluginsSdkMgrGenerated_t728608785 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginsSdkMgrGenerated::PluginsSdkMgr_PluginsSdkMgr1(JSVCall,System.Int32)
extern "C"  bool PluginsSdkMgrGenerated_PluginsSdkMgr_PluginsSdkMgr1_m2073694610 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgrGenerated::PluginsSdkMgr_LOGIN_SERVER_CONNECTION(JSVCall)
extern "C"  void PluginsSdkMgrGenerated_PluginsSdkMgr_LOGIN_SERVER_CONNECTION_m410232668 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgrGenerated::PluginsSdkMgr_ROLEENTERGAME(JSVCall)
extern "C"  void PluginsSdkMgrGenerated_PluginsSdkMgr_ROLEENTERGAME_m611605708 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgrGenerated::PluginsSdkMgr_CREATEROLE(JSVCall)
extern "C"  void PluginsSdkMgrGenerated_PluginsSdkMgr_CREATEROLE_m4040485082 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgrGenerated::PluginsSdkMgr_ROLEUPGRADE(JSVCall)
extern "C"  void PluginsSdkMgrGenerated_PluginsSdkMgr_ROLEUPGRADE_m3049354682 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgrGenerated::PluginsSdkMgr_USERPAY(JSVCall)
extern "C"  void PluginsSdkMgrGenerated_PluginsSdkMgr_USERPAY_m3481071747 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgrGenerated::PluginsSdkMgr_SDKLOGINSUCCESS(JSVCall)
extern "C"  void PluginsSdkMgrGenerated_PluginsSdkMgr_SDKLOGINSUCCESS_m2435908524 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgrGenerated::PluginsSdkMgr_SDKLOGINERROR(JSVCall)
extern "C"  void PluginsSdkMgrGenerated_PluginsSdkMgr_SDKLOGINERROR_m2761720199 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgrGenerated::PluginsSdkMgr_EV_PLAY_RMB(JSVCall)
extern "C"  void PluginsSdkMgrGenerated_PluginsSdkMgr_EV_PLAY_RMB_m1318059350 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgrGenerated::PluginsSdkMgr_BuildAudioMsg(JSVCall)
extern "C"  void PluginsSdkMgrGenerated_PluginsSdkMgr_BuildAudioMsg_m1324242215 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgrGenerated::PluginsSdkMgr_XunFei_QuitChat(JSVCall)
extern "C"  void PluginsSdkMgrGenerated_PluginsSdkMgr_XunFei_QuitChat_m3182733907 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgrGenerated::PluginsSdkMgr_XunFei_CreateGroup(JSVCall)
extern "C"  void PluginsSdkMgrGenerated_PluginsSdkMgr_XunFei_CreateGroup_m1776162383 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgrGenerated::PluginsSdkMgr_XunFei_CreateWorldGroupInfo(JSVCall)
extern "C"  void PluginsSdkMgrGenerated_PluginsSdkMgr_XunFei_CreateWorldGroupInfo_m2306996259 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgrGenerated::PluginsSdkMgr_XunFei_JoinGroupInfo(JSVCall)
extern "C"  void PluginsSdkMgrGenerated_PluginsSdkMgr_XunFei_JoinGroupInfo_m2136451183 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgrGenerated::PluginsSdkMgr_XunFei_JoinChat(JSVCall)
extern "C"  void PluginsSdkMgrGenerated_PluginsSdkMgr_XunFei_JoinChat_m4101090136 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgrGenerated::PluginsSdkMgr_XunFei_CheckGroup(JSVCall)
extern "C"  void PluginsSdkMgrGenerated_PluginsSdkMgr_XunFei_CheckGroup_m3339428675 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgrGenerated::PluginsSdkMgr_XunFei_OnReceiveMessage(JSVCall)
extern "C"  void PluginsSdkMgrGenerated_PluginsSdkMgr_XunFei_OnReceiveMessage_m3196012727 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgrGenerated::PluginsSdkMgr_PAY_FAIL(JSVCall)
extern "C"  void PluginsSdkMgrGenerated_PluginsSdkMgr_PAY_FAIL_m283937975 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgrGenerated::PluginsSdkMgr_PAY_SUCCESS(JSVCall)
extern "C"  void PluginsSdkMgrGenerated_PluginsSdkMgr_PAY_SUCCESS_m2231862804 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgrGenerated::PluginsSdkMgr_OPEN_USER_LOGIN(JSVCall)
extern "C"  void PluginsSdkMgrGenerated_PluginsSdkMgr_OPEN_USER_LOGIN_m3618377494 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgrGenerated::PluginsSdkMgr_HELP_APPKEY(JSVCall)
extern "C"  void PluginsSdkMgrGenerated_PluginsSdkMgr_HELP_APPKEY_m2940422116 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgrGenerated::PluginsSdkMgr_sjoyPlugin(JSVCall)
extern "C"  void PluginsSdkMgrGenerated_PluginsSdkMgr_sjoyPlugin_m3587454328 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgrGenerated::PluginsSdkMgr_pushPlugin(JSVCall)
extern "C"  void PluginsSdkMgrGenerated_PluginsSdkMgr_pushPlugin_m655459263 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgrGenerated::PluginsSdkMgr_reyunPlugin(JSVCall)
extern "C"  void PluginsSdkMgrGenerated_PluginsSdkMgr_reyunPlugin_m361052718 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgrGenerated::PluginsSdkMgr_buglyPlugin(JSVCall)
extern "C"  void PluginsSdkMgrGenerated_PluginsSdkMgr_buglyPlugin_m2132264108 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgrGenerated::PluginsSdkMgr_tlogPlugin(JSVCall)
extern "C"  void PluginsSdkMgrGenerated_PluginsSdkMgr_tlogPlugin_m1348518345 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgrGenerated::PluginsSdkMgr_xunfeiPlugin(JSVCall)
extern "C"  void PluginsSdkMgrGenerated_PluginsSdkMgr_xunfeiPlugin_m2044837536 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgrGenerated::PluginsSdkMgr_hongyouPlugin(JSVCall)
extern "C"  void PluginsSdkMgrGenerated_PluginsSdkMgr_hongyouPlugin_m4163509838 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgrGenerated::PluginsSdkMgr_yijiePlugin(JSVCall)
extern "C"  void PluginsSdkMgrGenerated_PluginsSdkMgr_yijiePlugin_m1727539479 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgrGenerated::PluginsSdkMgr_taiqiPlugin(JSVCall)
extern "C"  void PluginsSdkMgrGenerated_PluginsSdkMgr_taiqiPlugin_m3128979609 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgrGenerated::PluginsSdkMgr_anquPlugin(JSVCall)
extern "C"  void PluginsSdkMgrGenerated_PluginsSdkMgr_anquPlugin_m2368545512 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgrGenerated::PluginsSdkMgr_xiaomiPlugin(JSVCall)
extern "C"  void PluginsSdkMgrGenerated_PluginsSdkMgr_xiaomiPlugin_m2601819774 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgrGenerated::PluginsSdkMgr_xuegaoPlugin(JSVCall)
extern "C"  void PluginsSdkMgrGenerated_PluginsSdkMgr_xuegaoPlugin_m171568172 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgrGenerated::PluginsSdkMgr_yiwanPlugin(JSVCall)
extern "C"  void PluginsSdkMgrGenerated_PluginsSdkMgr_yiwanPlugin_m3409084793 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgrGenerated::PluginsSdkMgr_wanmiPlugin(JSVCall)
extern "C"  void PluginsSdkMgrGenerated_PluginsSdkMgr_wanmiPlugin_m93840973 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgrGenerated::PluginsSdkMgr_mihuaPlugin(JSVCall)
extern "C"  void PluginsSdkMgrGenerated_PluginsSdkMgr_mihuaPlugin_m1139624181 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgrGenerated::PluginsSdkMgr_huaweiPlugin(JSVCall)
extern "C"  void PluginsSdkMgrGenerated_PluginsSdkMgr_huaweiPlugin_m1281061842 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgrGenerated::PluginsSdkMgr_vivoPlugin(JSVCall)
extern "C"  void PluginsSdkMgrGenerated_PluginsSdkMgr_vivoPlugin_m3744718829 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgrGenerated::PluginsSdkMgr_oppoPlugin(JSVCall)
extern "C"  void PluginsSdkMgrGenerated_PluginsSdkMgr_oppoPlugin_m3146591865 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgrGenerated::PluginsSdkMgr_yybPlugin(JSVCall)
extern "C"  void PluginsSdkMgrGenerated_PluginsSdkMgr_yybPlugin_m2312407755 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgrGenerated::PluginsSdkMgr_flymePlugin(JSVCall)
extern "C"  void PluginsSdkMgrGenerated_PluginsSdkMgr_flymePlugin_m1713904930 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgrGenerated::PluginsSdkMgr_jinliPlugin(JSVCall)
extern "C"  void PluginsSdkMgrGenerated_PluginsSdkMgr_jinliPlugin_m1594438529 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgrGenerated::PluginsSdkMgr_ucPlugin(JSVCall)
extern "C"  void PluginsSdkMgrGenerated_PluginsSdkMgr_ucPlugin_m4213815115 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgrGenerated::PluginsSdkMgr_zq360Plugin(JSVCall)
extern "C"  void PluginsSdkMgrGenerated_PluginsSdkMgr_zq360Plugin_m3146469239 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgrGenerated::PluginsSdkMgr_quickPlugin(JSVCall)
extern "C"  void PluginsSdkMgrGenerated_PluginsSdkMgr_quickPlugin_m1858349120 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgrGenerated::PluginsSdkMgr_zxhyPlugin(JSVCall)
extern "C"  void PluginsSdkMgrGenerated_PluginsSdkMgr_zxhyPlugin_m13774186 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgrGenerated::PluginsSdkMgr_IsOpenSjoy(JSVCall)
extern "C"  void PluginsSdkMgrGenerated_PluginsSdkMgr_IsOpenSjoy_m2257714263 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgrGenerated::PluginsSdkMgr_isHideOldSever(JSVCall)
extern "C"  void PluginsSdkMgrGenerated_PluginsSdkMgr_isHideOldSever_m3387810294 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgrGenerated::PluginsSdkMgr_IsLoginClick(JSVCall)
extern "C"  void PluginsSdkMgrGenerated_PluginsSdkMgr_IsLoginClick_m1109223459 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgrGenerated::PluginsSdkMgr_androidJavaObject(JSVCall)
extern "C"  void PluginsSdkMgrGenerated_PluginsSdkMgr_androidJavaObject_m1483955824 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgrGenerated::PluginsSdkMgr_channelId(JSVCall)
extern "C"  void PluginsSdkMgrGenerated_PluginsSdkMgr_channelId_m3400963714 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgrGenerated::PluginsSdkMgr_DeviceID(JSVCall)
extern "C"  void PluginsSdkMgrGenerated_PluginsSdkMgr_DeviceID_m2038806171 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgrGenerated::PluginsSdkMgr_Instance(JSVCall)
extern "C"  void PluginsSdkMgrGenerated_PluginsSdkMgr_Instance_m2799158391 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginsSdkMgrGenerated::PluginsSdkMgr_Game_getDeviceId(JSVCall,System.Int32)
extern "C"  bool PluginsSdkMgrGenerated_PluginsSdkMgr_Game_getDeviceId_m1255964889 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginsSdkMgrGenerated::PluginsSdkMgr_Game_Login__String__Gender__String__String__Int32__String(JSVCall,System.Int32)
extern "C"  bool PluginsSdkMgrGenerated_PluginsSdkMgr_Game_Login__String__Gender__String__String__Int32__String_m4293859314 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginsSdkMgrGenerated::PluginsSdkMgr_Game_SetEvent__String__DictionaryT2_String_String(JSVCall,System.Int32)
extern "C"  bool PluginsSdkMgrGenerated_PluginsSdkMgr_Game_SetEvent__String__DictionaryT2_String_String_m2956055149 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginsSdkMgrGenerated::PluginsSdkMgr_GetPushState__String__PushType(JSVCall,System.Int32)
extern "C"  bool PluginsSdkMgrGenerated_PluginsSdkMgr_GetPushState__String__PushType_m3879216517 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginsSdkMgrGenerated::PluginsSdkMgr_HideLogo(JSVCall,System.Int32)
extern "C"  bool PluginsSdkMgrGenerated_PluginsSdkMgr_HideLogo_m3660418540 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginsSdkMgrGenerated::PluginsSdkMgr_Init(JSVCall,System.Int32)
extern "C"  bool PluginsSdkMgrGenerated_PluginsSdkMgr_Init_m633294767 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginsSdkMgrGenerated::PluginsSdkMgr_isAutoRelogin(JSVCall,System.Int32)
extern "C"  bool PluginsSdkMgrGenerated_PluginsSdkMgr_isAutoRelogin_m3817532544 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginsSdkMgrGenerated::PluginsSdkMgr_IsLoginSuccess__Boolean(JSVCall,System.Int32)
extern "C"  bool PluginsSdkMgrGenerated_PluginsSdkMgr_IsLoginSuccess__Boolean_m3911449543 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginsSdkMgrGenerated::PluginsSdkMgr_LoadLogo(JSVCall,System.Int32)
extern "C"  bool PluginsSdkMgrGenerated_PluginsSdkMgr_LoadLogo_m3371921552 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginsSdkMgrGenerated::PluginsSdkMgr_OnDestory(JSVCall,System.Int32)
extern "C"  bool PluginsSdkMgrGenerated_PluginsSdkMgr_OnDestory_m2468426936 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginsSdkMgrGenerated::PluginsSdkMgr_OpenUserLogin(JSVCall,System.Int32)
extern "C"  bool PluginsSdkMgrGenerated_PluginsSdkMgr_OpenUserLogin_m1327006583 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginsSdkMgrGenerated::PluginsSdkMgr_ReqHttpLogin__String__String(JSVCall,System.Int32)
extern "C"  bool PluginsSdkMgrGenerated_PluginsSdkMgr_ReqHttpLogin__String__String_m2697248964 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginsSdkMgrGenerated::PluginsSdkMgr_ReqHttpRegister__String__String(JSVCall,System.Int32)
extern "C"  bool PluginsSdkMgrGenerated_PluginsSdkMgr_ReqHttpRegister__String__String_m1035042990 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginsSdkMgrGenerated::PluginsSdkMgr_ReqSDKHttpLogin(JSVCall,System.Int32)
extern "C"  bool PluginsSdkMgrGenerated_PluginsSdkMgr_ReqSDKHttpLogin_m2519552360 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginsSdkMgrGenerated::PluginsSdkMgr_SendNotification__String__PushType__Int32__Single__String__String__Boolean__String(JSVCall,System.Int32)
extern "C"  bool PluginsSdkMgrGenerated_PluginsSdkMgr_SendNotification__String__PushType__Int32__Single__String__String__Boolean__String_m3275574968 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginsSdkMgrGenerated::PluginsSdkMgr_SettingPushState__String__PushType__Boolean(JSVCall,System.Int32)
extern "C"  bool PluginsSdkMgrGenerated_PluginsSdkMgr_SettingPushState__String__PushType__Boolean_m2562090687 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Action PluginsSdkMgrGenerated::PluginsSdkMgr_ShowDialog_GetDelegate_member16_arg5(CSRepresentedObject)
extern "C"  Action_t3771233898 * PluginsSdkMgrGenerated_PluginsSdkMgr_ShowDialog_GetDelegate_member16_arg5_m1909796546 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Action PluginsSdkMgrGenerated::PluginsSdkMgr_ShowDialog_GetDelegate_member16_arg6(CSRepresentedObject)
extern "C"  Action_t3771233898 * PluginsSdkMgrGenerated_PluginsSdkMgr_ShowDialog_GetDelegate_member16_arg6_m4166707553 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginsSdkMgrGenerated::PluginsSdkMgr_ShowDialog__String__String__Boolean__String__String__Action__Action(JSVCall,System.Int32)
extern "C"  bool PluginsSdkMgrGenerated_PluginsSdkMgr_ShowDialog__String__String__Boolean__String__String__Action__Action_m344804946 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginsSdkMgrGenerated::PluginsSdkMgr_StartActivityHelp__String__String__String__UInt32(JSVCall,System.Int32)
extern "C"  bool PluginsSdkMgrGenerated_PluginsSdkMgr_StartActivityHelp__String__String__String__UInt32_m1393374593 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginsSdkMgrGenerated::PluginsSdkMgr__showWarningBox1__String__String__String(JSVCall,System.Int32)
extern "C"  bool PluginsSdkMgrGenerated_PluginsSdkMgr__showWarningBox1__String__String__String_m706148856 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginsSdkMgrGenerated::PluginsSdkMgr__showWarningBox2__String__String__String__String(JSVCall,System.Int32)
extern "C"  bool PluginsSdkMgrGenerated_PluginsSdkMgr__showWarningBox2__String__String__String__String_m2923206026 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgrGenerated::__Register()
extern "C"  void PluginsSdkMgrGenerated___Register_m2180034397 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Action PluginsSdkMgrGenerated::<PluginsSdkMgr_ShowDialog__String__String__Boolean__String__String__Action__Action>m__90()
extern "C"  Action_t3771233898 * PluginsSdkMgrGenerated_U3CPluginsSdkMgr_ShowDialog__String__String__Boolean__String__String__Action__ActionU3Em__90_m2622011152 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Action PluginsSdkMgrGenerated::<PluginsSdkMgr_ShowDialog__String__String__Boolean__String__String__Action__Action>m__91()
extern "C"  Action_t3771233898 * PluginsSdkMgrGenerated_U3CPluginsSdkMgr_ShowDialog__String__String__Boolean__String__String__Action__ActionU3Em__91_m2622012113 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PluginsSdkMgrGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t PluginsSdkMgrGenerated_ilo_getObject1_m4151083816 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginsSdkMgrGenerated::ilo_attachFinalizerObject2(System.Int32)
extern "C"  bool PluginsSdkMgrGenerated_ilo_attachFinalizerObject2_m2572918966 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgrGenerated::ilo_setStringS3(System.Int32,System.String)
extern "C"  void PluginsSdkMgrGenerated_ilo_setStringS3_m52958837 (Il2CppObject * __this /* static, unused */, int32_t ___e0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PluginsSdkMgrGenerated::ilo_setObject4(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t PluginsSdkMgrGenerated_ilo_setObject4_m3788632419 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PluginsSdkMgrGenerated::ilo_getObject5(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * PluginsSdkMgrGenerated_ilo_getObject5_m2133526345 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgrGenerated::ilo_setBooleanS6(System.Int32,System.Boolean)
extern "C"  void PluginsSdkMgrGenerated_ilo_setBooleanS6_m1110095030 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginsSdkMgrGenerated::ilo_getBooleanS7(System.Int32)
extern "C"  bool PluginsSdkMgrGenerated_ilo_getBooleanS7_m1166760040 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PluginsSdkMgrGenerated::ilo_getStringS8(System.Int32)
extern "C"  String_t* PluginsSdkMgrGenerated_ilo_getStringS8_m617587439 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PluginsSdkMgrGenerated::ilo_getInt329(System.Int32)
extern "C"  int32_t PluginsSdkMgrGenerated_ilo_getInt329_m4005549945 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgrGenerated::ilo_Game_Login10(PluginsSdkMgr,System.String,Gender,System.String,System.String,System.Int32,System.String)
extern "C"  void PluginsSdkMgrGenerated_ilo_Game_Login10_m1397888094 (Il2CppObject * __this /* static, unused */, PluginsSdkMgr_t3884624670 * ____this0, String_t* ___account1, int32_t ___genders2, String_t* ___age3, String_t* ___serverId4, int32_t ___level5, String_t* ___rolename6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgrGenerated::ilo_Game_SetEvent11(PluginsSdkMgr,System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C"  void PluginsSdkMgrGenerated_ilo_Game_SetEvent11_m2224931489 (Il2CppObject * __this /* static, unused */, PluginsSdkMgr_t3884624670 * ____this0, String_t* ___eventName1, Dictionary_2_t827649927 * ___dict2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgrGenerated::ilo_Init12(PluginsSdkMgr)
extern "C"  void PluginsSdkMgrGenerated_ilo_Init12_m516597590 (Il2CppObject * __this /* static, unused */, PluginsSdkMgr_t3884624670 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginsSdkMgrGenerated::ilo_isAutoRelogin13(PluginsSdkMgr)
extern "C"  bool PluginsSdkMgrGenerated_ilo_isAutoRelogin13_m3936431540 (Il2CppObject * __this /* static, unused */, PluginsSdkMgr_t3884624670 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgrGenerated::ilo_ReqHttpLogin14(PluginsSdkMgr,System.String,System.String)
extern "C"  void PluginsSdkMgrGenerated_ilo_ReqHttpLogin14_m3060697017 (Il2CppObject * __this /* static, unused */, PluginsSdkMgr_t3884624670 * ____this0, String_t* ___username1, String_t* ___password2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgrGenerated::ilo_SendNotification15(PluginsSdkMgr,System.String,PushType,System.Int32,System.Single,System.String,System.String,System.Boolean,System.String)
extern "C"  void PluginsSdkMgrGenerated_ilo_SendNotification15_m1578137963 (Il2CppObject * __this /* static, unused */, PluginsSdkMgr_t3884624670 * ____this0, String_t* ___account1, int32_t ___pushType2, int32_t ___id3, float ___delay4, String_t* ___title5, String_t* ___message6, bool ___isLoopTimer7, String_t* ___integralTimes8, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PluginsSdkMgrGenerated::ilo_getEnum16(System.Int32)
extern "C"  int32_t PluginsSdkMgrGenerated_ilo_getEnum16_m1814204340 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgrGenerated::ilo_addJSFunCSDelegateRel17(System.Int32,System.Delegate)
extern "C"  void PluginsSdkMgrGenerated_ilo_addJSFunCSDelegateRel17_m486106499 (Il2CppObject * __this /* static, unused */, int32_t ___funID0, Delegate_t3310234105 * ___del1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgrGenerated::ilo_StartActivityHelp18(PluginsSdkMgr,System.String,System.String,System.String,System.UInt32)
extern "C"  void PluginsSdkMgrGenerated_ilo_StartActivityHelp18_m2505831278 (Il2CppObject * __this /* static, unused */, PluginsSdkMgr_t3884624670 * ____this0, String_t* ___username1, String_t* ___serverId2, String_t* ___userId3, uint32_t ___pay4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Action PluginsSdkMgrGenerated::ilo_PluginsSdkMgr_ShowDialog_GetDelegate_member16_arg619(CSRepresentedObject)
extern "C"  Action_t3771233898 * PluginsSdkMgrGenerated_ilo_PluginsSdkMgr_ShowDialog_GetDelegate_member16_arg619_m2289152358 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
