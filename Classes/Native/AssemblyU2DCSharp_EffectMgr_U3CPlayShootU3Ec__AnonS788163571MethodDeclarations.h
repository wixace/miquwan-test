﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EffectMgr/<PlayShoot>c__AnonStorey157
struct U3CPlayShootU3Ec__AnonStorey157_t788163571;
// Mihua.Asset.ABLoadOperation.AssetOperation
struct AssetOperation_t778728221;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Mihua_Asset_ABLoadOperation_Asset778728221.h"

// System.Void EffectMgr/<PlayShoot>c__AnonStorey157::.ctor()
extern "C"  void U3CPlayShootU3Ec__AnonStorey157__ctor_m3569123032 (U3CPlayShootU3Ec__AnonStorey157_t788163571 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EffectMgr/<PlayShoot>c__AnonStorey157::<>m__3DC(Mihua.Asset.ABLoadOperation.AssetOperation)
extern "C"  void U3CPlayShootU3Ec__AnonStorey157_U3CU3Em__3DC_m3205569402 (U3CPlayShootU3Ec__AnonStorey157_t788163571 * __this, AssetOperation_t778728221 * ___ao0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
