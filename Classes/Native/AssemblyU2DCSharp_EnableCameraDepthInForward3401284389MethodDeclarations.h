﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EnableCameraDepthInForward
struct EnableCameraDepthInForward_t3401284389;

#include "codegen/il2cpp-codegen.h"

// System.Void EnableCameraDepthInForward::.ctor()
extern "C"  void EnableCameraDepthInForward__ctor_m2536810262 (EnableCameraDepthInForward_t3401284389 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EnableCameraDepthInForward::Start()
extern "C"  void EnableCameraDepthInForward_Start_m1483948054 (EnableCameraDepthInForward_t3401284389 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EnableCameraDepthInForward::Set()
extern "C"  void EnableCameraDepthInForward_Set_m2226809046 (EnableCameraDepthInForward_t3401284389 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
