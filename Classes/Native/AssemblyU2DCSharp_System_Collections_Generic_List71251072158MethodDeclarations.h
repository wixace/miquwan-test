﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System_Collections_Generic_List77Generated/<ListA1_ConvertAll_GetDelegate_member8_arg0>c__AnonStorey7B`2<System.Object,System.Object>
struct U3CListA1_ConvertAll_GetDelegate_member8_arg0U3Ec__AnonStorey7B_2_t1251072158;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System_Collections_Generic_List77Generated/<ListA1_ConvertAll_GetDelegate_member8_arg0>c__AnonStorey7B`2<System.Object,System.Object>::.ctor()
extern "C"  void U3CListA1_ConvertAll_GetDelegate_member8_arg0U3Ec__AnonStorey7B_2__ctor_m946481058_gshared (U3CListA1_ConvertAll_GetDelegate_member8_arg0U3Ec__AnonStorey7B_2_t1251072158 * __this, const MethodInfo* method);
#define U3CListA1_ConvertAll_GetDelegate_member8_arg0U3Ec__AnonStorey7B_2__ctor_m946481058(__this, method) ((  void (*) (U3CListA1_ConvertAll_GetDelegate_member8_arg0U3Ec__AnonStorey7B_2_t1251072158 *, const MethodInfo*))U3CListA1_ConvertAll_GetDelegate_member8_arg0U3Ec__AnonStorey7B_2__ctor_m946481058_gshared)(__this, method)
// TOutput System_Collections_Generic_List77Generated/<ListA1_ConvertAll_GetDelegate_member8_arg0>c__AnonStorey7B`2<System.Object,System.Object>::<>m__9D(T)
extern "C"  Il2CppObject * U3CListA1_ConvertAll_GetDelegate_member8_arg0U3Ec__AnonStorey7B_2_U3CU3Em__9D_m2343419978_gshared (U3CListA1_ConvertAll_GetDelegate_member8_arg0U3Ec__AnonStorey7B_2_t1251072158 * __this, Il2CppObject * ___input0, const MethodInfo* method);
#define U3CListA1_ConvertAll_GetDelegate_member8_arg0U3Ec__AnonStorey7B_2_U3CU3Em__9D_m2343419978(__this, ___input0, method) ((  Il2CppObject * (*) (U3CListA1_ConvertAll_GetDelegate_member8_arg0U3Ec__AnonStorey7B_2_t1251072158 *, Il2CppObject *, const MethodInfo*))U3CListA1_ConvertAll_GetDelegate_member8_arg0U3Ec__AnonStorey7B_2_U3CU3Em__9D_m2343419978_gshared)(__this, ___input0, method)
