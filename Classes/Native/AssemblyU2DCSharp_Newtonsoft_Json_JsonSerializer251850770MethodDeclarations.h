﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.JsonSerializer
struct JsonSerializer_t251850770;
// System.EventHandler`1<Newtonsoft.Json.Serialization.ErrorEventArgs>
struct EventHandler_1_t937589677;
// Newtonsoft.Json.Serialization.IReferenceResolver
struct IReferenceResolver_t425424564;
// System.Runtime.Serialization.SerializationBinder
struct SerializationBinder_t2137423328;
// Newtonsoft.Json.JsonConverterCollection
struct JsonConverterCollection_t530165700;
// Newtonsoft.Json.Serialization.IContractResolver
struct IContractResolver_t507353639;
// Newtonsoft.Json.JsonSerializerSettings
struct JsonSerializerSettings_t2589405525;
// System.IO.TextReader
struct TextReader_t2148718976;
// System.Object
struct Il2CppObject;
// Newtonsoft.Json.JsonReader
struct JsonReader_t816925123;
// System.Type
struct Type_t;
// System.IO.TextWriter
struct TextWriter_t2304124208;
// Newtonsoft.Json.JsonWriter
struct JsonWriter_t972330355;
// Newtonsoft.Json.JsonConverter
struct JsonConverter_t2159686854;
// System.Collections.Generic.IList`1<Newtonsoft.Json.JsonConverter>
struct IList_1_t559366761;
// Newtonsoft.Json.Serialization.ErrorEventArgs
struct ErrorEventArgs_t792639131;
// Newtonsoft.Json.Serialization.JsonSerializerInternalWriter
struct JsonSerializerInternalWriter_t3814549686;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2137423328.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_TypeNameHandling2359325474.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_F3005881063.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_PreserveReferenc4230591217.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_ReferenceLoopHan2761661122.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_MissingMemberHan2077487315.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_NullValueHandlin2754652381.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_DefaultValueHand1569448045.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_ObjectCreationHand56081595.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_ConstructorHandl2475221485.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon2761351129.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonSerializerSe2589405525.h"
#include "mscorlib_System_IO_TextReader2148718976.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonReader816925123.h"
#include "mscorlib_System_Type2863145774.h"
#include "mscorlib_System_IO_TextWriter2304124208.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonWriter972330355.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_Err792639131.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonSerializer251850770.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_Js3814549686.h"
#include "mscorlib_System_String7231557.h"

// System.Void Newtonsoft.Json.JsonSerializer::.ctor()
extern "C"  void JsonSerializer__ctor_m1979999244 (JsonSerializer_t251850770 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonSerializer::add_Error(System.EventHandler`1<Newtonsoft.Json.Serialization.ErrorEventArgs>)
extern "C"  void JsonSerializer_add_Error_m3286323774 (JsonSerializer_t251850770 * __this, EventHandler_1_t937589677 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonSerializer::remove_Error(System.EventHandler`1<Newtonsoft.Json.Serialization.ErrorEventArgs>)
extern "C"  void JsonSerializer_remove_Error_m3825542125 (JsonSerializer_t251850770 * __this, EventHandler_1_t937589677 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Serialization.IReferenceResolver Newtonsoft.Json.JsonSerializer::get_ReferenceResolver()
extern "C"  Il2CppObject * JsonSerializer_get_ReferenceResolver_m1342858000 (JsonSerializer_t251850770 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonSerializer::set_ReferenceResolver(Newtonsoft.Json.Serialization.IReferenceResolver)
extern "C"  void JsonSerializer_set_ReferenceResolver_m1494109667 (JsonSerializer_t251850770 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Serialization.SerializationBinder Newtonsoft.Json.JsonSerializer::get_Binder()
extern "C"  SerializationBinder_t2137423328 * JsonSerializer_get_Binder_m291591969 (JsonSerializer_t251850770 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonSerializer::set_Binder(System.Runtime.Serialization.SerializationBinder)
extern "C"  void JsonSerializer_set_Binder_m224184536 (JsonSerializer_t251850770 * __this, SerializationBinder_t2137423328 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.TypeNameHandling Newtonsoft.Json.JsonSerializer::get_TypeNameHandling()
extern "C"  int32_t JsonSerializer_get_TypeNameHandling_m1750029897 (JsonSerializer_t251850770 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonSerializer::set_TypeNameHandling(Newtonsoft.Json.TypeNameHandling)
extern "C"  void JsonSerializer_set_TypeNameHandling_m500880000 (JsonSerializer_t251850770 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Serialization.Formatters.FormatterAssemblyStyle Newtonsoft.Json.JsonSerializer::get_TypeNameAssemblyFormat()
extern "C"  int32_t JsonSerializer_get_TypeNameAssemblyFormat_m366580461 (JsonSerializer_t251850770 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonSerializer::set_TypeNameAssemblyFormat(System.Runtime.Serialization.Formatters.FormatterAssemblyStyle)
extern "C"  void JsonSerializer_set_TypeNameAssemblyFormat_m1929259748 (JsonSerializer_t251850770 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.PreserveReferencesHandling Newtonsoft.Json.JsonSerializer::get_PreserveReferencesHandling()
extern "C"  int32_t JsonSerializer_get_PreserveReferencesHandling_m1144526569 (JsonSerializer_t251850770 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonSerializer::set_PreserveReferencesHandling(Newtonsoft.Json.PreserveReferencesHandling)
extern "C"  void JsonSerializer_set_PreserveReferencesHandling_m2807126944 (JsonSerializer_t251850770 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.ReferenceLoopHandling Newtonsoft.Json.JsonSerializer::get_ReferenceLoopHandling()
extern "C"  int32_t JsonSerializer_get_ReferenceLoopHandling_m304536129 (JsonSerializer_t251850770 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonSerializer::set_ReferenceLoopHandling(Newtonsoft.Json.ReferenceLoopHandling)
extern "C"  void JsonSerializer_set_ReferenceLoopHandling_m817218482 (JsonSerializer_t251850770 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.MissingMemberHandling Newtonsoft.Json.JsonSerializer::get_MissingMemberHandling()
extern "C"  int32_t JsonSerializer_get_MissingMemberHandling_m1435823779 (JsonSerializer_t251850770 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonSerializer::set_MissingMemberHandling(Newtonsoft.Json.MissingMemberHandling)
extern "C"  void JsonSerializer_set_MissingMemberHandling_m305666192 (JsonSerializer_t251850770 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.NullValueHandling Newtonsoft.Json.JsonSerializer::get_NullValueHandling()
extern "C"  int32_t JsonSerializer_get_NullValueHandling_m2095362487 (JsonSerializer_t251850770 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonSerializer::set_NullValueHandling(Newtonsoft.Json.NullValueHandling)
extern "C"  void JsonSerializer_set_NullValueHandling_m3488917628 (JsonSerializer_t251850770 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.DefaultValueHandling Newtonsoft.Json.JsonSerializer::get_DefaultValueHandling()
extern "C"  int32_t JsonSerializer_get_DefaultValueHandling_m3037498793 (JsonSerializer_t251850770 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonSerializer::set_DefaultValueHandling(Newtonsoft.Json.DefaultValueHandling)
extern "C"  void JsonSerializer_set_DefaultValueHandling_m1470150944 (JsonSerializer_t251850770 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.ObjectCreationHandling Newtonsoft.Json.JsonSerializer::get_ObjectCreationHandling()
extern "C"  int32_t JsonSerializer_get_ObjectCreationHandling_m19089449 (JsonSerializer_t251850770 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonSerializer::set_ObjectCreationHandling(Newtonsoft.Json.ObjectCreationHandling)
extern "C"  void JsonSerializer_set_ObjectCreationHandling_m331601120 (JsonSerializer_t251850770 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.ConstructorHandling Newtonsoft.Json.JsonSerializer::get_ConstructorHandling()
extern "C"  int32_t JsonSerializer_get_ConstructorHandling_m3488964503 (JsonSerializer_t251850770 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonSerializer::set_ConstructorHandling(Newtonsoft.Json.ConstructorHandling)
extern "C"  void JsonSerializer_set_ConstructorHandling_m2902206300 (JsonSerializer_t251850770 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.JsonConverterCollection Newtonsoft.Json.JsonSerializer::get_Converters()
extern "C"  JsonConverterCollection_t530165700 * JsonSerializer_get_Converters_m945106790 (JsonSerializer_t251850770 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Serialization.IContractResolver Newtonsoft.Json.JsonSerializer::get_ContractResolver()
extern "C"  Il2CppObject * JsonSerializer_get_ContractResolver_m1596101022 (JsonSerializer_t251850770 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonSerializer::set_ContractResolver(Newtonsoft.Json.Serialization.IContractResolver)
extern "C"  void JsonSerializer_set_ContractResolver_m1131817143 (JsonSerializer_t251850770 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Serialization.StreamingContext Newtonsoft.Json.JsonSerializer::get_Context()
extern "C"  StreamingContext_t2761351129  JsonSerializer_get_Context_m3933367197 (JsonSerializer_t251850770 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonSerializer::set_Context(System.Runtime.Serialization.StreamingContext)
extern "C"  void JsonSerializer_set_Context_m805798806 (JsonSerializer_t251850770 * __this, StreamingContext_t2761351129  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.JsonSerializer Newtonsoft.Json.JsonSerializer::Create(Newtonsoft.Json.JsonSerializerSettings)
extern "C"  JsonSerializer_t251850770 * JsonSerializer_Create_m1132421680 (Il2CppObject * __this /* static, unused */, JsonSerializerSettings_t2589405525 * ___settings0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonSerializer::Populate(System.IO.TextReader,System.Object)
extern "C"  void JsonSerializer_Populate_m435505527 (JsonSerializer_t251850770 * __this, TextReader_t2148718976 * ___reader0, Il2CppObject * ___target1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonSerializer::Populate(Newtonsoft.Json.JsonReader,System.Object)
extern "C"  void JsonSerializer_Populate_m2611706256 (JsonSerializer_t251850770 * __this, JsonReader_t816925123 * ___reader0, Il2CppObject * ___target1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonSerializer::PopulateInternal(Newtonsoft.Json.JsonReader,System.Object)
extern "C"  void JsonSerializer_PopulateInternal_m1215794989 (JsonSerializer_t251850770 * __this, JsonReader_t816925123 * ___reader0, Il2CppObject * ___target1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.JsonSerializer::Deserialize(Newtonsoft.Json.JsonReader)
extern "C"  Il2CppObject * JsonSerializer_Deserialize_m1491331252 (JsonSerializer_t251850770 * __this, JsonReader_t816925123 * ___reader0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.JsonSerializer::Deserialize(System.IO.TextReader,System.Type)
extern "C"  Il2CppObject * JsonSerializer_Deserialize_m1334748430 (JsonSerializer_t251850770 * __this, TextReader_t2148718976 * ___reader0, Type_t * ___objectType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.JsonSerializer::Deserialize(Newtonsoft.Json.JsonReader,System.Type)
extern "C"  Il2CppObject * JsonSerializer_Deserialize_m4191685095 (JsonSerializer_t251850770 * __this, JsonReader_t816925123 * ___reader0, Type_t * ___objectType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.JsonSerializer::DeserializeInternal(Newtonsoft.Json.JsonReader,System.Type)
extern "C"  Il2CppObject * JsonSerializer_DeserializeInternal_m2352828612 (JsonSerializer_t251850770 * __this, JsonReader_t816925123 * ___reader0, Type_t * ___objectType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonSerializer::Serialize(System.IO.TextWriter,System.Object)
extern "C"  void JsonSerializer_Serialize_m763129519 (JsonSerializer_t251850770 * __this, TextWriter_t2304124208 * ___textWriter0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonSerializer::Serialize(Newtonsoft.Json.JsonWriter,System.Object)
extern "C"  void JsonSerializer_Serialize_m659280072 (JsonSerializer_t251850770 * __this, JsonWriter_t972330355 * ___jsonWriter0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonSerializer::SerializeInternal(Newtonsoft.Json.JsonWriter,System.Object)
extern "C"  void JsonSerializer_SerializeInternal_m2888072805 (JsonSerializer_t251850770 * __this, JsonWriter_t972330355 * ___jsonWriter0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.JsonConverter Newtonsoft.Json.JsonSerializer::GetMatchingConverter(System.Type)
extern "C"  JsonConverter_t2159686854 * JsonSerializer_GetMatchingConverter_m127829860 (JsonSerializer_t251850770 * __this, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.JsonConverter Newtonsoft.Json.JsonSerializer::GetMatchingConverter(System.Collections.Generic.IList`1<Newtonsoft.Json.JsonConverter>,System.Type)
extern "C"  JsonConverter_t2159686854 * JsonSerializer_GetMatchingConverter_m3608745602 (Il2CppObject * __this /* static, unused */, Il2CppObject* ___converters0, Type_t * ___objectType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonSerializer::OnError(Newtonsoft.Json.Serialization.ErrorEventArgs)
extern "C"  void JsonSerializer_OnError_m2358242801 (JsonSerializer_t251850770 * __this, ErrorEventArgs_t792639131 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IList`1<Newtonsoft.Json.JsonConverter> Newtonsoft.Json.JsonSerializer::ilo_get_Converters1(Newtonsoft.Json.JsonSerializerSettings)
extern "C"  Il2CppObject* JsonSerializer_ilo_get_Converters1_m1396633237 (Il2CppObject * __this /* static, unused */, JsonSerializerSettings_t2589405525 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.JsonConverterCollection Newtonsoft.Json.JsonSerializer::ilo_get_Converters2(Newtonsoft.Json.JsonSerializer)
extern "C"  JsonConverterCollection_t530165700 * JsonSerializer_ilo_get_Converters2_m2388398732 (Il2CppObject * __this /* static, unused */, JsonSerializer_t251850770 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.TypeNameHandling Newtonsoft.Json.JsonSerializer::ilo_get_TypeNameHandling3(Newtonsoft.Json.JsonSerializerSettings)
extern "C"  int32_t JsonSerializer_ilo_get_TypeNameHandling3_m548553383 (Il2CppObject * __this /* static, unused */, JsonSerializerSettings_t2589405525 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonSerializer::ilo_set_TypeNameHandling4(Newtonsoft.Json.JsonSerializer,Newtonsoft.Json.TypeNameHandling)
extern "C"  void JsonSerializer_ilo_set_TypeNameHandling4_m3918349714 (Il2CppObject * __this /* static, unused */, JsonSerializer_t251850770 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Serialization.Formatters.FormatterAssemblyStyle Newtonsoft.Json.JsonSerializer::ilo_get_TypeNameAssemblyFormat5(Newtonsoft.Json.JsonSerializerSettings)
extern "C"  int32_t JsonSerializer_ilo_get_TypeNameAssemblyFormat5_m405052357 (Il2CppObject * __this /* static, unused */, JsonSerializerSettings_t2589405525 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonSerializer::ilo_set_TypeNameAssemblyFormat6(Newtonsoft.Json.JsonSerializer,System.Runtime.Serialization.Formatters.FormatterAssemblyStyle)
extern "C"  void JsonSerializer_ilo_set_TypeNameAssemblyFormat6_m2307141620 (Il2CppObject * __this /* static, unused */, JsonSerializer_t251850770 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.ReferenceLoopHandling Newtonsoft.Json.JsonSerializer::ilo_get_ReferenceLoopHandling7(Newtonsoft.Json.JsonSerializerSettings)
extern "C"  int32_t JsonSerializer_ilo_get_ReferenceLoopHandling7_m2674297465 (Il2CppObject * __this /* static, unused */, JsonSerializerSettings_t2589405525 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonSerializer::ilo_set_ObjectCreationHandling8(Newtonsoft.Json.JsonSerializer,Newtonsoft.Json.ObjectCreationHandling)
extern "C"  void JsonSerializer_ilo_set_ObjectCreationHandling8_m982724526 (Il2CppObject * __this /* static, unused */, JsonSerializer_t251850770 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonSerializer::ilo_set_NullValueHandling9(Newtonsoft.Json.JsonSerializer,Newtonsoft.Json.NullValueHandling)
extern "C"  void JsonSerializer_ilo_set_NullValueHandling9_m2322164789 (Il2CppObject * __this /* static, unused */, JsonSerializer_t251850770 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.ConstructorHandling Newtonsoft.Json.JsonSerializer::ilo_get_ConstructorHandling10(Newtonsoft.Json.JsonSerializerSettings)
extern "C"  int32_t JsonSerializer_ilo_get_ConstructorHandling10_m1664862833 (Il2CppObject * __this /* static, unused */, JsonSerializerSettings_t2589405525 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Serialization.IContractResolver Newtonsoft.Json.JsonSerializer::ilo_get_ContractResolver11(Newtonsoft.Json.JsonSerializerSettings)
extern "C"  Il2CppObject * JsonSerializer_ilo_get_ContractResolver11_m2802097471 (Il2CppObject * __this /* static, unused */, JsonSerializerSettings_t2589405525 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonSerializer::ilo_set_ContractResolver12(Newtonsoft.Json.JsonSerializer,Newtonsoft.Json.Serialization.IContractResolver)
extern "C"  void JsonSerializer_ilo_set_ContractResolver12_m83447280 (Il2CppObject * __this /* static, unused */, JsonSerializer_t251850770 * ____this0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Serialization.IReferenceResolver Newtonsoft.Json.JsonSerializer::ilo_get_ReferenceResolver13(Newtonsoft.Json.JsonSerializerSettings)
extern "C"  Il2CppObject * JsonSerializer_ilo_get_ReferenceResolver13_m504270253 (Il2CppObject * __this /* static, unused */, JsonSerializerSettings_t2589405525 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonSerializer::ilo_set_ReferenceResolver14(Newtonsoft.Json.JsonSerializer,Newtonsoft.Json.Serialization.IReferenceResolver)
extern "C"  void JsonSerializer_ilo_set_ReferenceResolver14_m1504082892 (Il2CppObject * __this /* static, unused */, JsonSerializer_t251850770 * ____this0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Serialization.SerializationBinder Newtonsoft.Json.JsonSerializer::ilo_get_Binder15(Newtonsoft.Json.JsonSerializerSettings)
extern "C"  SerializationBinder_t2137423328 * JsonSerializer_ilo_get_Binder15_m301892358 (Il2CppObject * __this /* static, unused */, JsonSerializerSettings_t2589405525 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonSerializer::ilo_set_Binder16(Newtonsoft.Json.JsonSerializer,System.Runtime.Serialization.SerializationBinder)
extern "C"  void JsonSerializer_ilo_set_Binder16_m2125324247 (Il2CppObject * __this /* static, unused */, JsonSerializer_t251850770 * ____this0, SerializationBinder_t2137423328 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonSerializer::ilo_Populate17(Newtonsoft.Json.JsonSerializer,Newtonsoft.Json.JsonReader,System.Object)
extern "C"  void JsonSerializer_ilo_Populate17_m678411976 (Il2CppObject * __this /* static, unused */, JsonSerializer_t251850770 * ____this0, JsonReader_t816925123 * ___reader1, Il2CppObject * ___target2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonSerializer::ilo_SerializeInternal18(Newtonsoft.Json.JsonSerializer,Newtonsoft.Json.JsonWriter,System.Object)
extern "C"  void JsonSerializer_ilo_SerializeInternal18_m3639491096 (Il2CppObject * __this /* static, unused */, JsonSerializer_t251850770 * ____this0, JsonWriter_t972330355 * ___jsonWriter1, Il2CppObject * ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonSerializer::ilo_Serialize19(Newtonsoft.Json.Serialization.JsonSerializerInternalWriter,Newtonsoft.Json.JsonWriter,System.Object)
extern "C"  void JsonSerializer_ilo_Serialize19_m4288159682 (Il2CppObject * __this /* static, unused */, JsonSerializerInternalWriter_t3814549686 * ____this0, JsonWriter_t972330355 * ___jsonWriter1, Il2CppObject * ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.JsonConverter Newtonsoft.Json.JsonSerializer::ilo_GetMatchingConverter20(System.Collections.Generic.IList`1<Newtonsoft.Json.JsonConverter>,System.Type)
extern "C"  JsonConverter_t2159686854 * JsonSerializer_ilo_GetMatchingConverter20_m2620281521 (Il2CppObject * __this /* static, unused */, Il2CppObject* ___converters0, Type_t * ___objectType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonSerializer::ilo_ArgumentNotNull21(System.Object,System.String)
extern "C"  void JsonSerializer_ilo_ArgumentNotNull21_m15747933 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, String_t* ___parameterName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
