//Generated on : 2020/6/19 16:01:49
void RegisterAllStrippedInternalCalls()
{
	//Start Registrations for type : UnityEngine.Advertisements.UnityAdsInternal

		//System.Boolean UnityEngine.Advertisements.UnityAdsInternal::CanShowAds(System.String)
		void Register_UnityEngine_Advertisements_UnityAdsInternal_CanShowAds();
		Register_UnityEngine_Advertisements_UnityAdsInternal_CanShowAds();

		//System.Boolean UnityEngine.Advertisements.UnityAdsInternal::Show(System.String,System.String,System.String)
		void Register_UnityEngine_Advertisements_UnityAdsInternal_Show();
		Register_UnityEngine_Advertisements_UnityAdsInternal_Show();

		//System.Void UnityEngine.Advertisements.UnityAdsInternal::Init(System.String,System.Boolean,System.Boolean,System.String)
		void Register_UnityEngine_Advertisements_UnityAdsInternal_Init();
		Register_UnityEngine_Advertisements_UnityAdsInternal_Init();

		//System.Void UnityEngine.Advertisements.UnityAdsInternal::RegisterNative()
		void Register_UnityEngine_Advertisements_UnityAdsInternal_RegisterNative();
		Register_UnityEngine_Advertisements_UnityAdsInternal_RegisterNative();

		//System.Void UnityEngine.Advertisements.UnityAdsInternal::SetCampaignDataURL(System.String)
		void Register_UnityEngine_Advertisements_UnityAdsInternal_SetCampaignDataURL();
		Register_UnityEngine_Advertisements_UnityAdsInternal_SetCampaignDataURL();

		//System.Void UnityEngine.Advertisements.UnityAdsInternal::SetLogLevel(System.Int32)
		void Register_UnityEngine_Advertisements_UnityAdsInternal_SetLogLevel();
		Register_UnityEngine_Advertisements_UnityAdsInternal_SetLogLevel();

	//End Registrations for type : UnityEngine.Advertisements.UnityAdsInternal

	//Start Registrations for type : UnityEngine.AnchoredJoint2D

		//System.Boolean UnityEngine.AnchoredJoint2D::get_autoConfigureConnectedAnchor()
		void Register_UnityEngine_AnchoredJoint2D_get_autoConfigureConnectedAnchor();
		Register_UnityEngine_AnchoredJoint2D_get_autoConfigureConnectedAnchor();

		//System.Void UnityEngine.AnchoredJoint2D::INTERNAL_get_anchor(UnityEngine.Vector2&)
		void Register_UnityEngine_AnchoredJoint2D_INTERNAL_get_anchor();
		Register_UnityEngine_AnchoredJoint2D_INTERNAL_get_anchor();

		//System.Void UnityEngine.AnchoredJoint2D::INTERNAL_get_connectedAnchor(UnityEngine.Vector2&)
		void Register_UnityEngine_AnchoredJoint2D_INTERNAL_get_connectedAnchor();
		Register_UnityEngine_AnchoredJoint2D_INTERNAL_get_connectedAnchor();

		//System.Void UnityEngine.AnchoredJoint2D::INTERNAL_set_anchor(UnityEngine.Vector2&)
		void Register_UnityEngine_AnchoredJoint2D_INTERNAL_set_anchor();
		Register_UnityEngine_AnchoredJoint2D_INTERNAL_set_anchor();

		//System.Void UnityEngine.AnchoredJoint2D::INTERNAL_set_connectedAnchor(UnityEngine.Vector2&)
		void Register_UnityEngine_AnchoredJoint2D_INTERNAL_set_connectedAnchor();
		Register_UnityEngine_AnchoredJoint2D_INTERNAL_set_connectedAnchor();

		//System.Void UnityEngine.AnchoredJoint2D::set_autoConfigureConnectedAnchor(System.Boolean)
		void Register_UnityEngine_AnchoredJoint2D_set_autoConfigureConnectedAnchor();
		Register_UnityEngine_AnchoredJoint2D_set_autoConfigureConnectedAnchor();

	//End Registrations for type : UnityEngine.AnchoredJoint2D

	//Start Registrations for type : UnityEngine.AndroidJNI

		//System.Boolean UnityEngine.AndroidJNI::CallBooleanMethod(System.IntPtr,System.IntPtr,UnityEngine.jvalue[])
		void Register_UnityEngine_AndroidJNI_CallBooleanMethod();
		Register_UnityEngine_AndroidJNI_CallBooleanMethod();

		//System.Boolean UnityEngine.AndroidJNI::CallStaticBooleanMethod(System.IntPtr,System.IntPtr,UnityEngine.jvalue[])
		void Register_UnityEngine_AndroidJNI_CallStaticBooleanMethod();
		Register_UnityEngine_AndroidJNI_CallStaticBooleanMethod();

		//System.Boolean UnityEngine.AndroidJNI::GetStaticBooleanField(System.IntPtr,System.IntPtr)
		void Register_UnityEngine_AndroidJNI_GetStaticBooleanField();
		Register_UnityEngine_AndroidJNI_GetStaticBooleanField();

		//System.Boolean[] UnityEngine.AndroidJNI::FromBooleanArray(System.IntPtr)
		void Register_UnityEngine_AndroidJNI_FromBooleanArray();
		Register_UnityEngine_AndroidJNI_FromBooleanArray();

		//System.Byte UnityEngine.AndroidJNI::CallByteMethod(System.IntPtr,System.IntPtr,UnityEngine.jvalue[])
		void Register_UnityEngine_AndroidJNI_CallByteMethod();
		Register_UnityEngine_AndroidJNI_CallByteMethod();

		//System.Byte UnityEngine.AndroidJNI::CallStaticByteMethod(System.IntPtr,System.IntPtr,UnityEngine.jvalue[])
		void Register_UnityEngine_AndroidJNI_CallStaticByteMethod();
		Register_UnityEngine_AndroidJNI_CallStaticByteMethod();

		//System.Byte UnityEngine.AndroidJNI::GetStaticByteField(System.IntPtr,System.IntPtr)
		void Register_UnityEngine_AndroidJNI_GetStaticByteField();
		Register_UnityEngine_AndroidJNI_GetStaticByteField();

		//System.Byte[] UnityEngine.AndroidJNI::FromByteArray(System.IntPtr)
		void Register_UnityEngine_AndroidJNI_FromByteArray();
		Register_UnityEngine_AndroidJNI_FromByteArray();

		//System.Char UnityEngine.AndroidJNI::CallCharMethod(System.IntPtr,System.IntPtr,UnityEngine.jvalue[])
		void Register_UnityEngine_AndroidJNI_CallCharMethod();
		Register_UnityEngine_AndroidJNI_CallCharMethod();

		//System.Char UnityEngine.AndroidJNI::CallStaticCharMethod(System.IntPtr,System.IntPtr,UnityEngine.jvalue[])
		void Register_UnityEngine_AndroidJNI_CallStaticCharMethod();
		Register_UnityEngine_AndroidJNI_CallStaticCharMethod();

		//System.Char UnityEngine.AndroidJNI::GetStaticCharField(System.IntPtr,System.IntPtr)
		void Register_UnityEngine_AndroidJNI_GetStaticCharField();
		Register_UnityEngine_AndroidJNI_GetStaticCharField();

		//System.Char[] UnityEngine.AndroidJNI::FromCharArray(System.IntPtr)
		void Register_UnityEngine_AndroidJNI_FromCharArray();
		Register_UnityEngine_AndroidJNI_FromCharArray();

		//System.Double UnityEngine.AndroidJNI::CallDoubleMethod(System.IntPtr,System.IntPtr,UnityEngine.jvalue[])
		void Register_UnityEngine_AndroidJNI_CallDoubleMethod();
		Register_UnityEngine_AndroidJNI_CallDoubleMethod();

		//System.Double UnityEngine.AndroidJNI::CallStaticDoubleMethod(System.IntPtr,System.IntPtr,UnityEngine.jvalue[])
		void Register_UnityEngine_AndroidJNI_CallStaticDoubleMethod();
		Register_UnityEngine_AndroidJNI_CallStaticDoubleMethod();

		//System.Double UnityEngine.AndroidJNI::GetStaticDoubleField(System.IntPtr,System.IntPtr)
		void Register_UnityEngine_AndroidJNI_GetStaticDoubleField();
		Register_UnityEngine_AndroidJNI_GetStaticDoubleField();

		//System.Double[] UnityEngine.AndroidJNI::FromDoubleArray(System.IntPtr)
		void Register_UnityEngine_AndroidJNI_FromDoubleArray();
		Register_UnityEngine_AndroidJNI_FromDoubleArray();

		//System.Int16 UnityEngine.AndroidJNI::CallShortMethod(System.IntPtr,System.IntPtr,UnityEngine.jvalue[])
		void Register_UnityEngine_AndroidJNI_CallShortMethod();
		Register_UnityEngine_AndroidJNI_CallShortMethod();

		//System.Int16 UnityEngine.AndroidJNI::CallStaticShortMethod(System.IntPtr,System.IntPtr,UnityEngine.jvalue[])
		void Register_UnityEngine_AndroidJNI_CallStaticShortMethod();
		Register_UnityEngine_AndroidJNI_CallStaticShortMethod();

		//System.Int16 UnityEngine.AndroidJNI::GetStaticShortField(System.IntPtr,System.IntPtr)
		void Register_UnityEngine_AndroidJNI_GetStaticShortField();
		Register_UnityEngine_AndroidJNI_GetStaticShortField();

		//System.Int16[] UnityEngine.AndroidJNI::FromShortArray(System.IntPtr)
		void Register_UnityEngine_AndroidJNI_FromShortArray();
		Register_UnityEngine_AndroidJNI_FromShortArray();

		//System.Int32 UnityEngine.AndroidJNI::CallIntMethod(System.IntPtr,System.IntPtr,UnityEngine.jvalue[])
		void Register_UnityEngine_AndroidJNI_CallIntMethod();
		Register_UnityEngine_AndroidJNI_CallIntMethod();

		//System.Int32 UnityEngine.AndroidJNI::CallStaticIntMethod(System.IntPtr,System.IntPtr,UnityEngine.jvalue[])
		void Register_UnityEngine_AndroidJNI_CallStaticIntMethod();
		Register_UnityEngine_AndroidJNI_CallStaticIntMethod();

		//System.Int32 UnityEngine.AndroidJNI::GetArrayLength(System.IntPtr)
		void Register_UnityEngine_AndroidJNI_GetArrayLength();
		Register_UnityEngine_AndroidJNI_GetArrayLength();

		//System.Int32 UnityEngine.AndroidJNI::GetStaticIntField(System.IntPtr,System.IntPtr)
		void Register_UnityEngine_AndroidJNI_GetStaticIntField();
		Register_UnityEngine_AndroidJNI_GetStaticIntField();

		//System.Int32[] UnityEngine.AndroidJNI::FromIntArray(System.IntPtr)
		void Register_UnityEngine_AndroidJNI_FromIntArray();
		Register_UnityEngine_AndroidJNI_FromIntArray();

		//System.Int64 UnityEngine.AndroidJNI::CallLongMethod(System.IntPtr,System.IntPtr,UnityEngine.jvalue[])
		void Register_UnityEngine_AndroidJNI_CallLongMethod();
		Register_UnityEngine_AndroidJNI_CallLongMethod();

		//System.Int64 UnityEngine.AndroidJNI::CallStaticLongMethod(System.IntPtr,System.IntPtr,UnityEngine.jvalue[])
		void Register_UnityEngine_AndroidJNI_CallStaticLongMethod();
		Register_UnityEngine_AndroidJNI_CallStaticLongMethod();

		//System.Int64 UnityEngine.AndroidJNI::GetStaticLongField(System.IntPtr,System.IntPtr)
		void Register_UnityEngine_AndroidJNI_GetStaticLongField();
		Register_UnityEngine_AndroidJNI_GetStaticLongField();

		//System.Int64[] UnityEngine.AndroidJNI::FromLongArray(System.IntPtr)
		void Register_UnityEngine_AndroidJNI_FromLongArray();
		Register_UnityEngine_AndroidJNI_FromLongArray();

		//System.IntPtr UnityEngine.AndroidJNI::CallObjectMethod(System.IntPtr,System.IntPtr,UnityEngine.jvalue[])
		void Register_UnityEngine_AndroidJNI_CallObjectMethod();
		Register_UnityEngine_AndroidJNI_CallObjectMethod();

		//System.IntPtr UnityEngine.AndroidJNI::CallStaticObjectMethod(System.IntPtr,System.IntPtr,UnityEngine.jvalue[])
		void Register_UnityEngine_AndroidJNI_CallStaticObjectMethod();
		Register_UnityEngine_AndroidJNI_CallStaticObjectMethod();

		//System.IntPtr UnityEngine.AndroidJNI::ExceptionOccurred()
		void Register_UnityEngine_AndroidJNI_ExceptionOccurred();
		Register_UnityEngine_AndroidJNI_ExceptionOccurred();

		//System.IntPtr UnityEngine.AndroidJNI::FindClass(System.String)
		void Register_UnityEngine_AndroidJNI_FindClass();
		Register_UnityEngine_AndroidJNI_FindClass();

		//System.IntPtr UnityEngine.AndroidJNI::FromReflectedField(System.IntPtr)
		void Register_UnityEngine_AndroidJNI_FromReflectedField();
		Register_UnityEngine_AndroidJNI_FromReflectedField();

		//System.IntPtr UnityEngine.AndroidJNI::FromReflectedMethod(System.IntPtr)
		void Register_UnityEngine_AndroidJNI_FromReflectedMethod();
		Register_UnityEngine_AndroidJNI_FromReflectedMethod();

		//System.IntPtr UnityEngine.AndroidJNI::GetFieldID(System.IntPtr,System.String,System.String)
		void Register_UnityEngine_AndroidJNI_GetFieldID();
		Register_UnityEngine_AndroidJNI_GetFieldID();

		//System.IntPtr UnityEngine.AndroidJNI::GetMethodID(System.IntPtr,System.String,System.String)
		void Register_UnityEngine_AndroidJNI_GetMethodID();
		Register_UnityEngine_AndroidJNI_GetMethodID();

		//System.IntPtr UnityEngine.AndroidJNI::GetObjectArrayElement(System.IntPtr,System.Int32)
		void Register_UnityEngine_AndroidJNI_GetObjectArrayElement();
		Register_UnityEngine_AndroidJNI_GetObjectArrayElement();

		//System.IntPtr UnityEngine.AndroidJNI::GetObjectClass(System.IntPtr)
		void Register_UnityEngine_AndroidJNI_GetObjectClass();
		Register_UnityEngine_AndroidJNI_GetObjectClass();

		//System.IntPtr UnityEngine.AndroidJNI::GetStaticFieldID(System.IntPtr,System.String,System.String)
		void Register_UnityEngine_AndroidJNI_GetStaticFieldID();
		Register_UnityEngine_AndroidJNI_GetStaticFieldID();

		//System.IntPtr UnityEngine.AndroidJNI::GetStaticMethodID(System.IntPtr,System.String,System.String)
		void Register_UnityEngine_AndroidJNI_GetStaticMethodID();
		Register_UnityEngine_AndroidJNI_GetStaticMethodID();

		//System.IntPtr UnityEngine.AndroidJNI::GetStaticObjectField(System.IntPtr,System.IntPtr)
		void Register_UnityEngine_AndroidJNI_GetStaticObjectField();
		Register_UnityEngine_AndroidJNI_GetStaticObjectField();

		//System.IntPtr UnityEngine.AndroidJNI::NewGlobalRef(System.IntPtr)
		void Register_UnityEngine_AndroidJNI_NewGlobalRef();
		Register_UnityEngine_AndroidJNI_NewGlobalRef();

		//System.IntPtr UnityEngine.AndroidJNI::NewObject(System.IntPtr,System.IntPtr,UnityEngine.jvalue[])
		void Register_UnityEngine_AndroidJNI_NewObject();
		Register_UnityEngine_AndroidJNI_NewObject();

		//System.IntPtr UnityEngine.AndroidJNI::NewObjectArray(System.Int32,System.IntPtr,System.IntPtr)
		void Register_UnityEngine_AndroidJNI_NewObjectArray();
		Register_UnityEngine_AndroidJNI_NewObjectArray();

		//System.IntPtr UnityEngine.AndroidJNI::NewStringUTF(System.String)
		void Register_UnityEngine_AndroidJNI_NewStringUTF();
		Register_UnityEngine_AndroidJNI_NewStringUTF();

		//System.IntPtr UnityEngine.AndroidJNI::ToBooleanArray(System.Boolean[])
		void Register_UnityEngine_AndroidJNI_ToBooleanArray();
		Register_UnityEngine_AndroidJNI_ToBooleanArray();

		//System.IntPtr UnityEngine.AndroidJNI::ToByteArray(System.Byte[])
		void Register_UnityEngine_AndroidJNI_ToByteArray();
		Register_UnityEngine_AndroidJNI_ToByteArray();

		//System.IntPtr UnityEngine.AndroidJNI::ToCharArray(System.Char[])
		void Register_UnityEngine_AndroidJNI_ToCharArray();
		Register_UnityEngine_AndroidJNI_ToCharArray();

		//System.IntPtr UnityEngine.AndroidJNI::ToDoubleArray(System.Double[])
		void Register_UnityEngine_AndroidJNI_ToDoubleArray();
		Register_UnityEngine_AndroidJNI_ToDoubleArray();

		//System.IntPtr UnityEngine.AndroidJNI::ToFloatArray(System.Single[])
		void Register_UnityEngine_AndroidJNI_ToFloatArray();
		Register_UnityEngine_AndroidJNI_ToFloatArray();

		//System.IntPtr UnityEngine.AndroidJNI::ToIntArray(System.Int32[])
		void Register_UnityEngine_AndroidJNI_ToIntArray();
		Register_UnityEngine_AndroidJNI_ToIntArray();

		//System.IntPtr UnityEngine.AndroidJNI::ToLongArray(System.Int64[])
		void Register_UnityEngine_AndroidJNI_ToLongArray();
		Register_UnityEngine_AndroidJNI_ToLongArray();

		//System.IntPtr UnityEngine.AndroidJNI::ToObjectArray(System.IntPtr[],System.IntPtr)
		void Register_UnityEngine_AndroidJNI_ToObjectArray();
		Register_UnityEngine_AndroidJNI_ToObjectArray();

		//System.IntPtr UnityEngine.AndroidJNI::ToShortArray(System.Int16[])
		void Register_UnityEngine_AndroidJNI_ToShortArray();
		Register_UnityEngine_AndroidJNI_ToShortArray();

		//System.Single UnityEngine.AndroidJNI::CallFloatMethod(System.IntPtr,System.IntPtr,UnityEngine.jvalue[])
		void Register_UnityEngine_AndroidJNI_CallFloatMethod();
		Register_UnityEngine_AndroidJNI_CallFloatMethod();

		//System.Single UnityEngine.AndroidJNI::CallStaticFloatMethod(System.IntPtr,System.IntPtr,UnityEngine.jvalue[])
		void Register_UnityEngine_AndroidJNI_CallStaticFloatMethod();
		Register_UnityEngine_AndroidJNI_CallStaticFloatMethod();

		//System.Single UnityEngine.AndroidJNI::GetStaticFloatField(System.IntPtr,System.IntPtr)
		void Register_UnityEngine_AndroidJNI_GetStaticFloatField();
		Register_UnityEngine_AndroidJNI_GetStaticFloatField();

		//System.Single[] UnityEngine.AndroidJNI::FromFloatArray(System.IntPtr)
		void Register_UnityEngine_AndroidJNI_FromFloatArray();
		Register_UnityEngine_AndroidJNI_FromFloatArray();

		//System.String UnityEngine.AndroidJNI::CallStaticStringMethod(System.IntPtr,System.IntPtr,UnityEngine.jvalue[])
		void Register_UnityEngine_AndroidJNI_CallStaticStringMethod();
		Register_UnityEngine_AndroidJNI_CallStaticStringMethod();

		//System.String UnityEngine.AndroidJNI::CallStringMethod(System.IntPtr,System.IntPtr,UnityEngine.jvalue[])
		void Register_UnityEngine_AndroidJNI_CallStringMethod();
		Register_UnityEngine_AndroidJNI_CallStringMethod();

		//System.String UnityEngine.AndroidJNI::GetStaticStringField(System.IntPtr,System.IntPtr)
		void Register_UnityEngine_AndroidJNI_GetStaticStringField();
		Register_UnityEngine_AndroidJNI_GetStaticStringField();

		//System.String UnityEngine.AndroidJNI::GetStringUTFChars(System.IntPtr)
		void Register_UnityEngine_AndroidJNI_GetStringUTFChars();
		Register_UnityEngine_AndroidJNI_GetStringUTFChars();

		//System.Void UnityEngine.AndroidJNI::CallStaticVoidMethod(System.IntPtr,System.IntPtr,UnityEngine.jvalue[])
		void Register_UnityEngine_AndroidJNI_CallStaticVoidMethod();
		Register_UnityEngine_AndroidJNI_CallStaticVoidMethod();

		//System.Void UnityEngine.AndroidJNI::CallVoidMethod(System.IntPtr,System.IntPtr,UnityEngine.jvalue[])
		void Register_UnityEngine_AndroidJNI_CallVoidMethod();
		Register_UnityEngine_AndroidJNI_CallVoidMethod();

		//System.Void UnityEngine.AndroidJNI::DeleteGlobalRef(System.IntPtr)
		void Register_UnityEngine_AndroidJNI_DeleteGlobalRef();
		Register_UnityEngine_AndroidJNI_DeleteGlobalRef();

		//System.Void UnityEngine.AndroidJNI::DeleteLocalRef(System.IntPtr)
		void Register_UnityEngine_AndroidJNI_DeleteLocalRef();
		Register_UnityEngine_AndroidJNI_DeleteLocalRef();

		//System.Void UnityEngine.AndroidJNI::ExceptionClear()
		void Register_UnityEngine_AndroidJNI_ExceptionClear();
		Register_UnityEngine_AndroidJNI_ExceptionClear();

		//System.Void UnityEngine.AndroidJNI::SetObjectArrayElement(System.IntPtr,System.Int32,System.IntPtr)
		void Register_UnityEngine_AndroidJNI_SetObjectArrayElement();
		Register_UnityEngine_AndroidJNI_SetObjectArrayElement();

	//End Registrations for type : UnityEngine.AndroidJNI

	//Start Registrations for type : UnityEngine.AndroidJNIHelper

		//System.IntPtr UnityEngine.AndroidJNIHelper::CreateJavaProxy(UnityEngine.AndroidJavaProxy)
		void Register_UnityEngine_AndroidJNIHelper_CreateJavaProxy();
		Register_UnityEngine_AndroidJNIHelper_CreateJavaProxy();

	//End Registrations for type : UnityEngine.AndroidJNIHelper

	//Start Registrations for type : UnityEngine.Animation

		//System.Boolean UnityEngine.Animation::IsPlaying(System.String)
		void Register_UnityEngine_Animation_IsPlaying();
		Register_UnityEngine_Animation_IsPlaying();

		//System.Boolean UnityEngine.Animation::Play(System.String,UnityEngine.PlayMode)
		void Register_UnityEngine_Animation_Play();
		Register_UnityEngine_Animation_Play();

		//System.Boolean UnityEngine.Animation::PlayDefaultAnimation(UnityEngine.PlayMode)
		void Register_UnityEngine_Animation_PlayDefaultAnimation();
		Register_UnityEngine_Animation_PlayDefaultAnimation();

		//System.Boolean UnityEngine.Animation::get_animatePhysics()
		void Register_UnityEngine_Animation_get_animatePhysics();
		Register_UnityEngine_Animation_get_animatePhysics();

		//System.Boolean UnityEngine.Animation::get_isPlaying()
		void Register_UnityEngine_Animation_get_isPlaying();
		Register_UnityEngine_Animation_get_isPlaying();

		//System.Boolean UnityEngine.Animation::get_playAutomatically()
		void Register_UnityEngine_Animation_get_playAutomatically();
		Register_UnityEngine_Animation_get_playAutomatically();

		//System.Int32 UnityEngine.Animation::GetClipCount()
		void Register_UnityEngine_Animation_GetClipCount();
		Register_UnityEngine_Animation_GetClipCount();

		//System.Int32 UnityEngine.Animation::GetStateCount()
		void Register_UnityEngine_Animation_GetStateCount();
		Register_UnityEngine_Animation_GetStateCount();

		//System.Void UnityEngine.Animation::AddClip(UnityEngine.AnimationClip,System.String,System.Int32,System.Int32,System.Boolean)
		void Register_UnityEngine_Animation_AddClip();
		Register_UnityEngine_Animation_AddClip();

		//System.Void UnityEngine.Animation::Blend(System.String,System.Single,System.Single)
		void Register_UnityEngine_Animation_Blend();
		Register_UnityEngine_Animation_Blend();

		//System.Void UnityEngine.Animation::CrossFade(System.String,System.Single,UnityEngine.PlayMode)
		void Register_UnityEngine_Animation_CrossFade();
		Register_UnityEngine_Animation_CrossFade();

		//System.Void UnityEngine.Animation::INTERNAL_CALL_Rewind(UnityEngine.Animation)
		void Register_UnityEngine_Animation_INTERNAL_CALL_Rewind();
		Register_UnityEngine_Animation_INTERNAL_CALL_Rewind();

		//System.Void UnityEngine.Animation::INTERNAL_CALL_Sample(UnityEngine.Animation)
		void Register_UnityEngine_Animation_INTERNAL_CALL_Sample();
		Register_UnityEngine_Animation_INTERNAL_CALL_Sample();

		//System.Void UnityEngine.Animation::INTERNAL_CALL_Stop(UnityEngine.Animation)
		void Register_UnityEngine_Animation_INTERNAL_CALL_Stop();
		Register_UnityEngine_Animation_INTERNAL_CALL_Stop();

		//System.Void UnityEngine.Animation::INTERNAL_CALL_SyncLayer(UnityEngine.Animation,System.Int32)
		void Register_UnityEngine_Animation_INTERNAL_CALL_SyncLayer();
		Register_UnityEngine_Animation_INTERNAL_CALL_SyncLayer();

		//System.Void UnityEngine.Animation::INTERNAL_get_localBounds(UnityEngine.Bounds&)
		void Register_UnityEngine_Animation_INTERNAL_get_localBounds();
		Register_UnityEngine_Animation_INTERNAL_get_localBounds();

		//System.Void UnityEngine.Animation::INTERNAL_set_localBounds(UnityEngine.Bounds&)
		void Register_UnityEngine_Animation_INTERNAL_set_localBounds();
		Register_UnityEngine_Animation_INTERNAL_set_localBounds();

		//System.Void UnityEngine.Animation::Internal_RewindByName(System.String)
		void Register_UnityEngine_Animation_Internal_RewindByName();
		Register_UnityEngine_Animation_Internal_RewindByName();

		//System.Void UnityEngine.Animation::Internal_StopByName(System.String)
		void Register_UnityEngine_Animation_Internal_StopByName();
		Register_UnityEngine_Animation_Internal_StopByName();

		//System.Void UnityEngine.Animation::RemoveClip(UnityEngine.AnimationClip)
		void Register_UnityEngine_Animation_RemoveClip();
		Register_UnityEngine_Animation_RemoveClip();

		//System.Void UnityEngine.Animation::RemoveClip2(System.String)
		void Register_UnityEngine_Animation_RemoveClip2();
		Register_UnityEngine_Animation_RemoveClip2();

		//System.Void UnityEngine.Animation::set_animatePhysics(System.Boolean)
		void Register_UnityEngine_Animation_set_animatePhysics();
		Register_UnityEngine_Animation_set_animatePhysics();

		//System.Void UnityEngine.Animation::set_clip(UnityEngine.AnimationClip)
		void Register_UnityEngine_Animation_set_clip();
		Register_UnityEngine_Animation_set_clip();

		//System.Void UnityEngine.Animation::set_cullingType(UnityEngine.AnimationCullingType)
		void Register_UnityEngine_Animation_set_cullingType();
		Register_UnityEngine_Animation_set_cullingType();

		//System.Void UnityEngine.Animation::set_playAutomatically(System.Boolean)
		void Register_UnityEngine_Animation_set_playAutomatically();
		Register_UnityEngine_Animation_set_playAutomatically();

		//System.Void UnityEngine.Animation::set_wrapMode(UnityEngine.WrapMode)
		void Register_UnityEngine_Animation_set_wrapMode();
		Register_UnityEngine_Animation_set_wrapMode();

		//UnityEngine.AnimationClip UnityEngine.Animation::get_clip()
		void Register_UnityEngine_Animation_get_clip();
		Register_UnityEngine_Animation_get_clip();

		//UnityEngine.AnimationCullingType UnityEngine.Animation::get_cullingType()
		void Register_UnityEngine_Animation_get_cullingType();
		Register_UnityEngine_Animation_get_cullingType();

		//UnityEngine.AnimationState UnityEngine.Animation::CrossFadeQueued(System.String,System.Single,UnityEngine.QueueMode,UnityEngine.PlayMode)
		void Register_UnityEngine_Animation_CrossFadeQueued();
		Register_UnityEngine_Animation_CrossFadeQueued();

		//UnityEngine.AnimationState UnityEngine.Animation::GetState(System.String)
		void Register_UnityEngine_Animation_GetState();
		Register_UnityEngine_Animation_GetState();

		//UnityEngine.AnimationState UnityEngine.Animation::GetStateAtIndex(System.Int32)
		void Register_UnityEngine_Animation_GetStateAtIndex();
		Register_UnityEngine_Animation_GetStateAtIndex();

		//UnityEngine.AnimationState UnityEngine.Animation::PlayQueued(System.String,UnityEngine.QueueMode,UnityEngine.PlayMode)
		void Register_UnityEngine_Animation_PlayQueued();
		Register_UnityEngine_Animation_PlayQueued();

		//UnityEngine.WrapMode UnityEngine.Animation::get_wrapMode()
		void Register_UnityEngine_Animation_get_wrapMode();
		Register_UnityEngine_Animation_get_wrapMode();

	//End Registrations for type : UnityEngine.Animation

	//Start Registrations for type : UnityEngine.AnimationClip

		//System.Array UnityEngine.AnimationClip::GetEventsInternal()
		void Register_UnityEngine_AnimationClip_GetEventsInternal();
		Register_UnityEngine_AnimationClip_GetEventsInternal();

		//System.Boolean UnityEngine.AnimationClip::get_humanMotion()
		void Register_UnityEngine_AnimationClip_get_humanMotion();
		Register_UnityEngine_AnimationClip_get_humanMotion();

		//System.Boolean UnityEngine.AnimationClip::get_legacy()
		void Register_UnityEngine_AnimationClip_get_legacy();
		Register_UnityEngine_AnimationClip_get_legacy();

		//System.Single UnityEngine.AnimationClip::get_frameRate()
		void Register_UnityEngine_AnimationClip_get_frameRate();
		Register_UnityEngine_AnimationClip_get_frameRate();

		//System.Single UnityEngine.AnimationClip::get_length()
		void Register_UnityEngine_AnimationClip_get_length();
		Register_UnityEngine_AnimationClip_get_length();

		//System.Void UnityEngine.AnimationClip::AddEventInternal(System.Object)
		void Register_UnityEngine_AnimationClip_AddEventInternal();
		Register_UnityEngine_AnimationClip_AddEventInternal();

		//System.Void UnityEngine.AnimationClip::INTERNAL_CALL_ClearCurves(UnityEngine.AnimationClip)
		void Register_UnityEngine_AnimationClip_INTERNAL_CALL_ClearCurves();
		Register_UnityEngine_AnimationClip_INTERNAL_CALL_ClearCurves();

		//System.Void UnityEngine.AnimationClip::INTERNAL_CALL_EnsureQuaternionContinuity(UnityEngine.AnimationClip)
		void Register_UnityEngine_AnimationClip_INTERNAL_CALL_EnsureQuaternionContinuity();
		Register_UnityEngine_AnimationClip_INTERNAL_CALL_EnsureQuaternionContinuity();

		//System.Void UnityEngine.AnimationClip::INTERNAL_get_localBounds(UnityEngine.Bounds&)
		void Register_UnityEngine_AnimationClip_INTERNAL_get_localBounds();
		Register_UnityEngine_AnimationClip_INTERNAL_get_localBounds();

		//System.Void UnityEngine.AnimationClip::INTERNAL_set_localBounds(UnityEngine.Bounds&)
		void Register_UnityEngine_AnimationClip_INTERNAL_set_localBounds();
		Register_UnityEngine_AnimationClip_INTERNAL_set_localBounds();

		//System.Void UnityEngine.AnimationClip::Internal_CreateAnimationClip(UnityEngine.AnimationClip)
		void Register_UnityEngine_AnimationClip_Internal_CreateAnimationClip();
		Register_UnityEngine_AnimationClip_Internal_CreateAnimationClip();

		//System.Void UnityEngine.AnimationClip::SampleAnimation(UnityEngine.GameObject,System.Single)
		void Register_UnityEngine_AnimationClip_SampleAnimation();
		Register_UnityEngine_AnimationClip_SampleAnimation();

		//System.Void UnityEngine.AnimationClip::SetCurve(System.String,System.Type,System.String,UnityEngine.AnimationCurve)
		void Register_UnityEngine_AnimationClip_SetCurve();
		Register_UnityEngine_AnimationClip_SetCurve();

		//System.Void UnityEngine.AnimationClip::SetEventsInternal(System.Array)
		void Register_UnityEngine_AnimationClip_SetEventsInternal();
		Register_UnityEngine_AnimationClip_SetEventsInternal();

		//System.Void UnityEngine.AnimationClip::set_frameRate(System.Single)
		void Register_UnityEngine_AnimationClip_set_frameRate();
		Register_UnityEngine_AnimationClip_set_frameRate();

		//System.Void UnityEngine.AnimationClip::set_legacy(System.Boolean)
		void Register_UnityEngine_AnimationClip_set_legacy();
		Register_UnityEngine_AnimationClip_set_legacy();

		//System.Void UnityEngine.AnimationClip::set_wrapMode(UnityEngine.WrapMode)
		void Register_UnityEngine_AnimationClip_set_wrapMode();
		Register_UnityEngine_AnimationClip_set_wrapMode();

		//UnityEngine.WrapMode UnityEngine.AnimationClip::get_wrapMode()
		void Register_UnityEngine_AnimationClip_get_wrapMode();
		Register_UnityEngine_AnimationClip_get_wrapMode();

	//End Registrations for type : UnityEngine.AnimationClip

	//Start Registrations for type : UnityEngine.AnimationCurve

		//System.Int32 UnityEngine.AnimationCurve::AddKey(System.Single,System.Single)
		void Register_UnityEngine_AnimationCurve_AddKey();
		Register_UnityEngine_AnimationCurve_AddKey();

		//System.Int32 UnityEngine.AnimationCurve::INTERNAL_CALL_AddKey_Internal(UnityEngine.AnimationCurve,UnityEngine.Keyframe&)
		void Register_UnityEngine_AnimationCurve_INTERNAL_CALL_AddKey_Internal();
		Register_UnityEngine_AnimationCurve_INTERNAL_CALL_AddKey_Internal();

		//System.Int32 UnityEngine.AnimationCurve::INTERNAL_CALL_MoveKey(UnityEngine.AnimationCurve,System.Int32,UnityEngine.Keyframe&)
		void Register_UnityEngine_AnimationCurve_INTERNAL_CALL_MoveKey();
		Register_UnityEngine_AnimationCurve_INTERNAL_CALL_MoveKey();

		//System.Int32 UnityEngine.AnimationCurve::get_length()
		void Register_UnityEngine_AnimationCurve_get_length();
		Register_UnityEngine_AnimationCurve_get_length();

		//System.Single UnityEngine.AnimationCurve::Evaluate(System.Single)
		void Register_UnityEngine_AnimationCurve_Evaluate();
		Register_UnityEngine_AnimationCurve_Evaluate();

		//System.Void UnityEngine.AnimationCurve::Cleanup()
		void Register_UnityEngine_AnimationCurve_Cleanup();
		Register_UnityEngine_AnimationCurve_Cleanup();

		//System.Void UnityEngine.AnimationCurve::INTERNAL_CALL_GetKey_Internal(UnityEngine.AnimationCurve,System.Int32,UnityEngine.Keyframe&)
		void Register_UnityEngine_AnimationCurve_INTERNAL_CALL_GetKey_Internal();
		Register_UnityEngine_AnimationCurve_INTERNAL_CALL_GetKey_Internal();

		//System.Void UnityEngine.AnimationCurve::Init(UnityEngine.Keyframe[])
		void Register_UnityEngine_AnimationCurve_Init();
		Register_UnityEngine_AnimationCurve_Init();

		//System.Void UnityEngine.AnimationCurve::RemoveKey(System.Int32)
		void Register_UnityEngine_AnimationCurve_RemoveKey();
		Register_UnityEngine_AnimationCurve_RemoveKey();

		//System.Void UnityEngine.AnimationCurve::SetKeys(UnityEngine.Keyframe[])
		void Register_UnityEngine_AnimationCurve_SetKeys();
		Register_UnityEngine_AnimationCurve_SetKeys();

		//System.Void UnityEngine.AnimationCurve::SmoothTangents(System.Int32,System.Single)
		void Register_UnityEngine_AnimationCurve_SmoothTangents();
		Register_UnityEngine_AnimationCurve_SmoothTangents();

		//System.Void UnityEngine.AnimationCurve::set_postWrapMode(UnityEngine.WrapMode)
		void Register_UnityEngine_AnimationCurve_set_postWrapMode();
		Register_UnityEngine_AnimationCurve_set_postWrapMode();

		//System.Void UnityEngine.AnimationCurve::set_preWrapMode(UnityEngine.WrapMode)
		void Register_UnityEngine_AnimationCurve_set_preWrapMode();
		Register_UnityEngine_AnimationCurve_set_preWrapMode();

		//UnityEngine.Keyframe[] UnityEngine.AnimationCurve::GetKeys()
		void Register_UnityEngine_AnimationCurve_GetKeys();
		Register_UnityEngine_AnimationCurve_GetKeys();

		//UnityEngine.WrapMode UnityEngine.AnimationCurve::get_postWrapMode()
		void Register_UnityEngine_AnimationCurve_get_postWrapMode();
		Register_UnityEngine_AnimationCurve_get_postWrapMode();

		//UnityEngine.WrapMode UnityEngine.AnimationCurve::get_preWrapMode()
		void Register_UnityEngine_AnimationCurve_get_preWrapMode();
		Register_UnityEngine_AnimationCurve_get_preWrapMode();

	//End Registrations for type : UnityEngine.AnimationCurve

	//Start Registrations for type : UnityEngine.AnimationState

		//System.Boolean UnityEngine.AnimationState::get_enabled()
		void Register_UnityEngine_AnimationState_get_enabled();
		Register_UnityEngine_AnimationState_get_enabled();

		//System.Int32 UnityEngine.AnimationState::get_layer()
		void Register_UnityEngine_AnimationState_get_layer();
		Register_UnityEngine_AnimationState_get_layer();

		//System.Single UnityEngine.AnimationState::get_length()
		void Register_UnityEngine_AnimationState_get_length();
		Register_UnityEngine_AnimationState_get_length();

		//System.Single UnityEngine.AnimationState::get_normalizedSpeed()
		void Register_UnityEngine_AnimationState_get_normalizedSpeed();
		Register_UnityEngine_AnimationState_get_normalizedSpeed();

		//System.Single UnityEngine.AnimationState::get_normalizedTime()
		void Register_UnityEngine_AnimationState_get_normalizedTime();
		Register_UnityEngine_AnimationState_get_normalizedTime();

		//System.Single UnityEngine.AnimationState::get_speed()
		void Register_UnityEngine_AnimationState_get_speed();
		Register_UnityEngine_AnimationState_get_speed();

		//System.Single UnityEngine.AnimationState::get_time()
		void Register_UnityEngine_AnimationState_get_time();
		Register_UnityEngine_AnimationState_get_time();

		//System.Single UnityEngine.AnimationState::get_weight()
		void Register_UnityEngine_AnimationState_get_weight();
		Register_UnityEngine_AnimationState_get_weight();

		//System.String UnityEngine.AnimationState::get_name()
		void Register_UnityEngine_AnimationState_get_name();
		Register_UnityEngine_AnimationState_get_name();

		//System.Void UnityEngine.AnimationState::AddMixingTransform(UnityEngine.Transform,System.Boolean)
		void Register_UnityEngine_AnimationState_AddMixingTransform();
		Register_UnityEngine_AnimationState_AddMixingTransform();

		//System.Void UnityEngine.AnimationState::RemoveMixingTransform(UnityEngine.Transform)
		void Register_UnityEngine_AnimationState_RemoveMixingTransform();
		Register_UnityEngine_AnimationState_RemoveMixingTransform();

		//System.Void UnityEngine.AnimationState::set_blendMode(UnityEngine.AnimationBlendMode)
		void Register_UnityEngine_AnimationState_set_blendMode();
		Register_UnityEngine_AnimationState_set_blendMode();

		//System.Void UnityEngine.AnimationState::set_enabled(System.Boolean)
		void Register_UnityEngine_AnimationState_set_enabled();
		Register_UnityEngine_AnimationState_set_enabled();

		//System.Void UnityEngine.AnimationState::set_layer(System.Int32)
		void Register_UnityEngine_AnimationState_set_layer();
		Register_UnityEngine_AnimationState_set_layer();

		//System.Void UnityEngine.AnimationState::set_name(System.String)
		void Register_UnityEngine_AnimationState_set_name();
		Register_UnityEngine_AnimationState_set_name();

		//System.Void UnityEngine.AnimationState::set_normalizedSpeed(System.Single)
		void Register_UnityEngine_AnimationState_set_normalizedSpeed();
		Register_UnityEngine_AnimationState_set_normalizedSpeed();

		//System.Void UnityEngine.AnimationState::set_normalizedTime(System.Single)
		void Register_UnityEngine_AnimationState_set_normalizedTime();
		Register_UnityEngine_AnimationState_set_normalizedTime();

		//System.Void UnityEngine.AnimationState::set_speed(System.Single)
		void Register_UnityEngine_AnimationState_set_speed();
		Register_UnityEngine_AnimationState_set_speed();

		//System.Void UnityEngine.AnimationState::set_time(System.Single)
		void Register_UnityEngine_AnimationState_set_time();
		Register_UnityEngine_AnimationState_set_time();

		//System.Void UnityEngine.AnimationState::set_weight(System.Single)
		void Register_UnityEngine_AnimationState_set_weight();
		Register_UnityEngine_AnimationState_set_weight();

		//System.Void UnityEngine.AnimationState::set_wrapMode(UnityEngine.WrapMode)
		void Register_UnityEngine_AnimationState_set_wrapMode();
		Register_UnityEngine_AnimationState_set_wrapMode();

		//UnityEngine.AnimationBlendMode UnityEngine.AnimationState::get_blendMode()
		void Register_UnityEngine_AnimationState_get_blendMode();
		Register_UnityEngine_AnimationState_get_blendMode();

		//UnityEngine.AnimationClip UnityEngine.AnimationState::get_clip()
		void Register_UnityEngine_AnimationState_get_clip();
		Register_UnityEngine_AnimationState_get_clip();

		//UnityEngine.WrapMode UnityEngine.AnimationState::get_wrapMode()
		void Register_UnityEngine_AnimationState_get_wrapMode();
		Register_UnityEngine_AnimationState_get_wrapMode();

	//End Registrations for type : UnityEngine.AnimationState

	//Start Registrations for type : UnityEngine.Animator

		//System.Boolean UnityEngine.Animator::CheckIfInIKPassInternal()
		void Register_UnityEngine_Animator_CheckIfInIKPassInternal();
		Register_UnityEngine_Animator_CheckIfInIKPassInternal();

		//System.Boolean UnityEngine.Animator::GetBoolID(System.Int32)
		void Register_UnityEngine_Animator_GetBoolID();
		Register_UnityEngine_Animator_GetBoolID();

		//System.Boolean UnityEngine.Animator::GetBoolString(System.String)
		void Register_UnityEngine_Animator_GetBoolString();
		Register_UnityEngine_Animator_GetBoolString();

		//System.Boolean UnityEngine.Animator::HasState(System.Int32,System.Int32)
		void Register_UnityEngine_Animator_HasState();
		Register_UnityEngine_Animator_HasState();

		//System.Boolean UnityEngine.Animator::IsInTransition(System.Int32)
		void Register_UnityEngine_Animator_IsInTransition();
		Register_UnityEngine_Animator_IsInTransition();

		//System.Boolean UnityEngine.Animator::IsParameterControlledByCurveID(System.Int32)
		void Register_UnityEngine_Animator_IsParameterControlledByCurveID();
		Register_UnityEngine_Animator_IsParameterControlledByCurveID();

		//System.Boolean UnityEngine.Animator::IsParameterControlledByCurveString(System.String)
		void Register_UnityEngine_Animator_IsParameterControlledByCurveString();
		Register_UnityEngine_Animator_IsParameterControlledByCurveString();

		//System.Boolean UnityEngine.Animator::get_applyRootMotion()
		void Register_UnityEngine_Animator_get_applyRootMotion();
		Register_UnityEngine_Animator_get_applyRootMotion();

		//System.Boolean UnityEngine.Animator::get_fireEvents()
		void Register_UnityEngine_Animator_get_fireEvents();
		Register_UnityEngine_Animator_get_fireEvents();

		//System.Boolean UnityEngine.Animator::get_hasRootMotion()
		void Register_UnityEngine_Animator_get_hasRootMotion();
		Register_UnityEngine_Animator_get_hasRootMotion();

		//System.Boolean UnityEngine.Animator::get_hasTransformHierarchy()
		void Register_UnityEngine_Animator_get_hasTransformHierarchy();
		Register_UnityEngine_Animator_get_hasTransformHierarchy();

		//System.Boolean UnityEngine.Animator::get_isHuman()
		void Register_UnityEngine_Animator_get_isHuman();
		Register_UnityEngine_Animator_get_isHuman();

		//System.Boolean UnityEngine.Animator::get_isInitialized()
		void Register_UnityEngine_Animator_get_isInitialized();
		Register_UnityEngine_Animator_get_isInitialized();

		//System.Boolean UnityEngine.Animator::get_isMatchingTarget()
		void Register_UnityEngine_Animator_get_isMatchingTarget();
		Register_UnityEngine_Animator_get_isMatchingTarget();

		//System.Boolean UnityEngine.Animator::get_isOptimizable()
		void Register_UnityEngine_Animator_get_isOptimizable();
		Register_UnityEngine_Animator_get_isOptimizable();

		//System.Boolean UnityEngine.Animator::get_layersAffectMassCenter()
		void Register_UnityEngine_Animator_get_layersAffectMassCenter();
		Register_UnityEngine_Animator_get_layersAffectMassCenter();

		//System.Boolean UnityEngine.Animator::get_linearVelocityBlending()
		void Register_UnityEngine_Animator_get_linearVelocityBlending();
		Register_UnityEngine_Animator_get_linearVelocityBlending();

		//System.Boolean UnityEngine.Animator::get_logWarnings()
		void Register_UnityEngine_Animator_get_logWarnings();
		Register_UnityEngine_Animator_get_logWarnings();

		//System.Boolean UnityEngine.Animator::get_stabilizeFeet()
		void Register_UnityEngine_Animator_get_stabilizeFeet();
		Register_UnityEngine_Animator_get_stabilizeFeet();

		//System.Int32 UnityEngine.Animator::GetIntegerID(System.Int32)
		void Register_UnityEngine_Animator_GetIntegerID();
		Register_UnityEngine_Animator_GetIntegerID();

		//System.Int32 UnityEngine.Animator::GetIntegerString(System.String)
		void Register_UnityEngine_Animator_GetIntegerString();
		Register_UnityEngine_Animator_GetIntegerString();

		//System.Int32 UnityEngine.Animator::GetLayerIndex(System.String)
		void Register_UnityEngine_Animator_GetLayerIndex();
		Register_UnityEngine_Animator_GetLayerIndex();

		//System.Int32 UnityEngine.Animator::StringToHash(System.String)
		void Register_UnityEngine_Animator_StringToHash();
		Register_UnityEngine_Animator_StringToHash();

		//System.Int32 UnityEngine.Animator::get_layerCount()
		void Register_UnityEngine_Animator_get_layerCount();
		Register_UnityEngine_Animator_get_layerCount();

		//System.Int32 UnityEngine.Animator::get_parameterCount()
		void Register_UnityEngine_Animator_get_parameterCount();
		Register_UnityEngine_Animator_get_parameterCount();

		//System.Single UnityEngine.Animator::GetFloatID(System.Int32)
		void Register_UnityEngine_Animator_GetFloatID();
		Register_UnityEngine_Animator_GetFloatID();

		//System.Single UnityEngine.Animator::GetFloatString(System.String)
		void Register_UnityEngine_Animator_GetFloatString();
		Register_UnityEngine_Animator_GetFloatString();

		//System.Single UnityEngine.Animator::GetHintWeightPositionInternal(UnityEngine.AvatarIKHint)
		void Register_UnityEngine_Animator_GetHintWeightPositionInternal();
		Register_UnityEngine_Animator_GetHintWeightPositionInternal();

		//System.Single UnityEngine.Animator::GetIKPositionWeightInternal(UnityEngine.AvatarIKGoal)
		void Register_UnityEngine_Animator_GetIKPositionWeightInternal();
		Register_UnityEngine_Animator_GetIKPositionWeightInternal();

		//System.Single UnityEngine.Animator::GetIKRotationWeightInternal(UnityEngine.AvatarIKGoal)
		void Register_UnityEngine_Animator_GetIKRotationWeightInternal();
		Register_UnityEngine_Animator_GetIKRotationWeightInternal();

		//System.Single UnityEngine.Animator::GetLayerWeight(System.Int32)
		void Register_UnityEngine_Animator_GetLayerWeight();
		Register_UnityEngine_Animator_GetLayerWeight();

		//System.Single UnityEngine.Animator::get_feetPivotActive()
		void Register_UnityEngine_Animator_get_feetPivotActive();
		Register_UnityEngine_Animator_get_feetPivotActive();

		//System.Single UnityEngine.Animator::get_gravityWeight()
		void Register_UnityEngine_Animator_get_gravityWeight();
		Register_UnityEngine_Animator_get_gravityWeight();

		//System.Single UnityEngine.Animator::get_humanScale()
		void Register_UnityEngine_Animator_get_humanScale();
		Register_UnityEngine_Animator_get_humanScale();

		//System.Single UnityEngine.Animator::get_leftFeetBottomHeight()
		void Register_UnityEngine_Animator_get_leftFeetBottomHeight();
		Register_UnityEngine_Animator_get_leftFeetBottomHeight();

		//System.Single UnityEngine.Animator::get_pivotWeight()
		void Register_UnityEngine_Animator_get_pivotWeight();
		Register_UnityEngine_Animator_get_pivotWeight();

		//System.Single UnityEngine.Animator::get_playbackTime()
		void Register_UnityEngine_Animator_get_playbackTime();
		Register_UnityEngine_Animator_get_playbackTime();

		//System.Single UnityEngine.Animator::get_recorderStartTime()
		void Register_UnityEngine_Animator_get_recorderStartTime();
		Register_UnityEngine_Animator_get_recorderStartTime();

		//System.Single UnityEngine.Animator::get_recorderStopTime()
		void Register_UnityEngine_Animator_get_recorderStopTime();
		Register_UnityEngine_Animator_get_recorderStopTime();

		//System.Single UnityEngine.Animator::get_rightFeetBottomHeight()
		void Register_UnityEngine_Animator_get_rightFeetBottomHeight();
		Register_UnityEngine_Animator_get_rightFeetBottomHeight();

		//System.Single UnityEngine.Animator::get_speed()
		void Register_UnityEngine_Animator_get_speed();
		Register_UnityEngine_Animator_get_speed();

		//System.String UnityEngine.Animator::GetLayerName(System.Int32)
		void Register_UnityEngine_Animator_GetLayerName();
		Register_UnityEngine_Animator_GetLayerName();

		//System.Void UnityEngine.Animator::ApplyBuiltinRootMotion()
		void Register_UnityEngine_Animator_ApplyBuiltinRootMotion();
		Register_UnityEngine_Animator_ApplyBuiltinRootMotion();

		//System.Void UnityEngine.Animator::CrossFade(System.Int32,System.Single,System.Int32,System.Single)
		void Register_UnityEngine_Animator_CrossFade();
		Register_UnityEngine_Animator_CrossFade();

		//System.Void UnityEngine.Animator::CrossFadeInFixedTime(System.Int32,System.Single,System.Int32,System.Single)
		void Register_UnityEngine_Animator_CrossFadeInFixedTime();
		Register_UnityEngine_Animator_CrossFadeInFixedTime();

		//System.Void UnityEngine.Animator::INTERNAL_CALL_GetIKHintPositionInternal(UnityEngine.Animator,UnityEngine.AvatarIKHint,UnityEngine.Vector3&)
		void Register_UnityEngine_Animator_INTERNAL_CALL_GetIKHintPositionInternal();
		Register_UnityEngine_Animator_INTERNAL_CALL_GetIKHintPositionInternal();

		//System.Void UnityEngine.Animator::INTERNAL_CALL_GetIKPositionInternal(UnityEngine.Animator,UnityEngine.AvatarIKGoal,UnityEngine.Vector3&)
		void Register_UnityEngine_Animator_INTERNAL_CALL_GetIKPositionInternal();
		Register_UnityEngine_Animator_INTERNAL_CALL_GetIKPositionInternal();

		//System.Void UnityEngine.Animator::INTERNAL_CALL_GetIKRotationInternal(UnityEngine.Animator,UnityEngine.AvatarIKGoal,UnityEngine.Quaternion&)
		void Register_UnityEngine_Animator_INTERNAL_CALL_GetIKRotationInternal();
		Register_UnityEngine_Animator_INTERNAL_CALL_GetIKRotationInternal();

		//System.Void UnityEngine.Animator::INTERNAL_CALL_MatchTarget(UnityEngine.Animator,UnityEngine.Vector3&,UnityEngine.Quaternion&,UnityEngine.AvatarTarget,UnityEngine.MatchTargetWeightMask&,System.Single,System.Single)
		void Register_UnityEngine_Animator_INTERNAL_CALL_MatchTarget();
		Register_UnityEngine_Animator_INTERNAL_CALL_MatchTarget();

		//System.Void UnityEngine.Animator::INTERNAL_CALL_SetBoneLocalRotationInternal(UnityEngine.Animator,UnityEngine.HumanBodyBones,UnityEngine.Quaternion&)
		void Register_UnityEngine_Animator_INTERNAL_CALL_SetBoneLocalRotationInternal();
		Register_UnityEngine_Animator_INTERNAL_CALL_SetBoneLocalRotationInternal();

		//System.Void UnityEngine.Animator::INTERNAL_CALL_SetIKHintPositionInternal(UnityEngine.Animator,UnityEngine.AvatarIKHint,UnityEngine.Vector3&)
		void Register_UnityEngine_Animator_INTERNAL_CALL_SetIKHintPositionInternal();
		Register_UnityEngine_Animator_INTERNAL_CALL_SetIKHintPositionInternal();

		//System.Void UnityEngine.Animator::INTERNAL_CALL_SetIKPositionInternal(UnityEngine.Animator,UnityEngine.AvatarIKGoal,UnityEngine.Vector3&)
		void Register_UnityEngine_Animator_INTERNAL_CALL_SetIKPositionInternal();
		Register_UnityEngine_Animator_INTERNAL_CALL_SetIKPositionInternal();

		//System.Void UnityEngine.Animator::INTERNAL_CALL_SetIKRotationInternal(UnityEngine.Animator,UnityEngine.AvatarIKGoal,UnityEngine.Quaternion&)
		void Register_UnityEngine_Animator_INTERNAL_CALL_SetIKRotationInternal();
		Register_UnityEngine_Animator_INTERNAL_CALL_SetIKRotationInternal();

		//System.Void UnityEngine.Animator::INTERNAL_CALL_SetLookAtPositionInternal(UnityEngine.Animator,UnityEngine.Vector3&)
		void Register_UnityEngine_Animator_INTERNAL_CALL_SetLookAtPositionInternal();
		Register_UnityEngine_Animator_INTERNAL_CALL_SetLookAtPositionInternal();

		//System.Void UnityEngine.Animator::INTERNAL_get_angularVelocity(UnityEngine.Vector3&)
		void Register_UnityEngine_Animator_INTERNAL_get_angularVelocity();
		Register_UnityEngine_Animator_INTERNAL_get_angularVelocity();

		//System.Void UnityEngine.Animator::INTERNAL_get_bodyPosition(UnityEngine.Vector3&)
		void Register_UnityEngine_Animator_INTERNAL_get_bodyPosition();
		Register_UnityEngine_Animator_INTERNAL_get_bodyPosition();

		//System.Void UnityEngine.Animator::INTERNAL_get_bodyRotation(UnityEngine.Quaternion&)
		void Register_UnityEngine_Animator_INTERNAL_get_bodyRotation();
		Register_UnityEngine_Animator_INTERNAL_get_bodyRotation();

		//System.Void UnityEngine.Animator::INTERNAL_get_deltaPosition(UnityEngine.Vector3&)
		void Register_UnityEngine_Animator_INTERNAL_get_deltaPosition();
		Register_UnityEngine_Animator_INTERNAL_get_deltaPosition();

		//System.Void UnityEngine.Animator::INTERNAL_get_deltaRotation(UnityEngine.Quaternion&)
		void Register_UnityEngine_Animator_INTERNAL_get_deltaRotation();
		Register_UnityEngine_Animator_INTERNAL_get_deltaRotation();

		//System.Void UnityEngine.Animator::INTERNAL_get_pivotPosition(UnityEngine.Vector3&)
		void Register_UnityEngine_Animator_INTERNAL_get_pivotPosition();
		Register_UnityEngine_Animator_INTERNAL_get_pivotPosition();

		//System.Void UnityEngine.Animator::INTERNAL_get_rootPosition(UnityEngine.Vector3&)
		void Register_UnityEngine_Animator_INTERNAL_get_rootPosition();
		Register_UnityEngine_Animator_INTERNAL_get_rootPosition();

		//System.Void UnityEngine.Animator::INTERNAL_get_rootRotation(UnityEngine.Quaternion&)
		void Register_UnityEngine_Animator_INTERNAL_get_rootRotation();
		Register_UnityEngine_Animator_INTERNAL_get_rootRotation();

		//System.Void UnityEngine.Animator::INTERNAL_get_targetPosition(UnityEngine.Vector3&)
		void Register_UnityEngine_Animator_INTERNAL_get_targetPosition();
		Register_UnityEngine_Animator_INTERNAL_get_targetPosition();

		//System.Void UnityEngine.Animator::INTERNAL_get_targetRotation(UnityEngine.Quaternion&)
		void Register_UnityEngine_Animator_INTERNAL_get_targetRotation();
		Register_UnityEngine_Animator_INTERNAL_get_targetRotation();

		//System.Void UnityEngine.Animator::INTERNAL_get_velocity(UnityEngine.Vector3&)
		void Register_UnityEngine_Animator_INTERNAL_get_velocity();
		Register_UnityEngine_Animator_INTERNAL_get_velocity();

		//System.Void UnityEngine.Animator::INTERNAL_set_bodyPosition(UnityEngine.Vector3&)
		void Register_UnityEngine_Animator_INTERNAL_set_bodyPosition();
		Register_UnityEngine_Animator_INTERNAL_set_bodyPosition();

		//System.Void UnityEngine.Animator::INTERNAL_set_bodyRotation(UnityEngine.Quaternion&)
		void Register_UnityEngine_Animator_INTERNAL_set_bodyRotation();
		Register_UnityEngine_Animator_INTERNAL_set_bodyRotation();

		//System.Void UnityEngine.Animator::INTERNAL_set_rootPosition(UnityEngine.Vector3&)
		void Register_UnityEngine_Animator_INTERNAL_set_rootPosition();
		Register_UnityEngine_Animator_INTERNAL_set_rootPosition();

		//System.Void UnityEngine.Animator::INTERNAL_set_rootRotation(UnityEngine.Quaternion&)
		void Register_UnityEngine_Animator_INTERNAL_set_rootRotation();
		Register_UnityEngine_Animator_INTERNAL_set_rootRotation();

		//System.Void UnityEngine.Animator::InterruptMatchTarget(System.Boolean)
		void Register_UnityEngine_Animator_InterruptMatchTarget();
		Register_UnityEngine_Animator_InterruptMatchTarget();

		//System.Void UnityEngine.Animator::Play(System.Int32,System.Int32,System.Single)
		void Register_UnityEngine_Animator_Play();
		Register_UnityEngine_Animator_Play();

		//System.Void UnityEngine.Animator::PlayInFixedTime(System.Int32,System.Int32,System.Single)
		void Register_UnityEngine_Animator_PlayInFixedTime();
		Register_UnityEngine_Animator_PlayInFixedTime();

		//System.Void UnityEngine.Animator::Rebind()
		void Register_UnityEngine_Animator_Rebind();
		Register_UnityEngine_Animator_Rebind();

		//System.Void UnityEngine.Animator::ResetTriggerID(System.Int32)
		void Register_UnityEngine_Animator_ResetTriggerID();
		Register_UnityEngine_Animator_ResetTriggerID();

		//System.Void UnityEngine.Animator::ResetTriggerString(System.String)
		void Register_UnityEngine_Animator_ResetTriggerString();
		Register_UnityEngine_Animator_ResetTriggerString();

		//System.Void UnityEngine.Animator::SetBoolID(System.Int32,System.Boolean)
		void Register_UnityEngine_Animator_SetBoolID();
		Register_UnityEngine_Animator_SetBoolID();

		//System.Void UnityEngine.Animator::SetBoolString(System.String,System.Boolean)
		void Register_UnityEngine_Animator_SetBoolString();
		Register_UnityEngine_Animator_SetBoolString();

		//System.Void UnityEngine.Animator::SetFloatID(System.Int32,System.Single)
		void Register_UnityEngine_Animator_SetFloatID();
		Register_UnityEngine_Animator_SetFloatID();

		//System.Void UnityEngine.Animator::SetFloatIDDamp(System.Int32,System.Single,System.Single,System.Single)
		void Register_UnityEngine_Animator_SetFloatIDDamp();
		Register_UnityEngine_Animator_SetFloatIDDamp();

		//System.Void UnityEngine.Animator::SetFloatString(System.String,System.Single)
		void Register_UnityEngine_Animator_SetFloatString();
		Register_UnityEngine_Animator_SetFloatString();

		//System.Void UnityEngine.Animator::SetFloatStringDamp(System.String,System.Single,System.Single,System.Single)
		void Register_UnityEngine_Animator_SetFloatStringDamp();
		Register_UnityEngine_Animator_SetFloatStringDamp();

		//System.Void UnityEngine.Animator::SetIKHintPositionWeightInternal(UnityEngine.AvatarIKHint,System.Single)
		void Register_UnityEngine_Animator_SetIKHintPositionWeightInternal();
		Register_UnityEngine_Animator_SetIKHintPositionWeightInternal();

		//System.Void UnityEngine.Animator::SetIKPositionWeightInternal(UnityEngine.AvatarIKGoal,System.Single)
		void Register_UnityEngine_Animator_SetIKPositionWeightInternal();
		Register_UnityEngine_Animator_SetIKPositionWeightInternal();

		//System.Void UnityEngine.Animator::SetIKRotationWeightInternal(UnityEngine.AvatarIKGoal,System.Single)
		void Register_UnityEngine_Animator_SetIKRotationWeightInternal();
		Register_UnityEngine_Animator_SetIKRotationWeightInternal();

		//System.Void UnityEngine.Animator::SetIntegerID(System.Int32,System.Int32)
		void Register_UnityEngine_Animator_SetIntegerID();
		Register_UnityEngine_Animator_SetIntegerID();

		//System.Void UnityEngine.Animator::SetIntegerString(System.String,System.Int32)
		void Register_UnityEngine_Animator_SetIntegerString();
		Register_UnityEngine_Animator_SetIntegerString();

		//System.Void UnityEngine.Animator::SetLayerWeight(System.Int32,System.Single)
		void Register_UnityEngine_Animator_SetLayerWeight();
		Register_UnityEngine_Animator_SetLayerWeight();

		//System.Void UnityEngine.Animator::SetLookAtWeightInternal(System.Single,System.Single,System.Single,System.Single,System.Single)
		void Register_UnityEngine_Animator_SetLookAtWeightInternal();
		Register_UnityEngine_Animator_SetLookAtWeightInternal();

		//System.Void UnityEngine.Animator::SetTarget(UnityEngine.AvatarTarget,System.Single)
		void Register_UnityEngine_Animator_SetTarget();
		Register_UnityEngine_Animator_SetTarget();

		//System.Void UnityEngine.Animator::SetTriggerID(System.Int32)
		void Register_UnityEngine_Animator_SetTriggerID();
		Register_UnityEngine_Animator_SetTriggerID();

		//System.Void UnityEngine.Animator::SetTriggerString(System.String)
		void Register_UnityEngine_Animator_SetTriggerString();
		Register_UnityEngine_Animator_SetTriggerString();

		//System.Void UnityEngine.Animator::StartPlayback()
		void Register_UnityEngine_Animator_StartPlayback();
		Register_UnityEngine_Animator_StartPlayback();

		//System.Void UnityEngine.Animator::StartRecording(System.Int32)
		void Register_UnityEngine_Animator_StartRecording();
		Register_UnityEngine_Animator_StartRecording();

		//System.Void UnityEngine.Animator::StopPlayback()
		void Register_UnityEngine_Animator_StopPlayback();
		Register_UnityEngine_Animator_StopPlayback();

		//System.Void UnityEngine.Animator::StopRecording()
		void Register_UnityEngine_Animator_StopRecording();
		Register_UnityEngine_Animator_StopRecording();

		//System.Void UnityEngine.Animator::Update(System.Single)
		void Register_UnityEngine_Animator_Update();
		Register_UnityEngine_Animator_Update();

		//System.Void UnityEngine.Animator::set_applyRootMotion(System.Boolean)
		void Register_UnityEngine_Animator_set_applyRootMotion();
		Register_UnityEngine_Animator_set_applyRootMotion();

		//System.Void UnityEngine.Animator::set_avatar(UnityEngine.Avatar)
		void Register_UnityEngine_Animator_set_avatar();
		Register_UnityEngine_Animator_set_avatar();

		//System.Void UnityEngine.Animator::set_cullingMode(UnityEngine.AnimatorCullingMode)
		void Register_UnityEngine_Animator_set_cullingMode();
		Register_UnityEngine_Animator_set_cullingMode();

		//System.Void UnityEngine.Animator::set_feetPivotActive(System.Single)
		void Register_UnityEngine_Animator_set_feetPivotActive();
		Register_UnityEngine_Animator_set_feetPivotActive();

		//System.Void UnityEngine.Animator::set_fireEvents(System.Boolean)
		void Register_UnityEngine_Animator_set_fireEvents();
		Register_UnityEngine_Animator_set_fireEvents();

		//System.Void UnityEngine.Animator::set_layersAffectMassCenter(System.Boolean)
		void Register_UnityEngine_Animator_set_layersAffectMassCenter();
		Register_UnityEngine_Animator_set_layersAffectMassCenter();

		//System.Void UnityEngine.Animator::set_linearVelocityBlending(System.Boolean)
		void Register_UnityEngine_Animator_set_linearVelocityBlending();
		Register_UnityEngine_Animator_set_linearVelocityBlending();

		//System.Void UnityEngine.Animator::set_logWarnings(System.Boolean)
		void Register_UnityEngine_Animator_set_logWarnings();
		Register_UnityEngine_Animator_set_logWarnings();

		//System.Void UnityEngine.Animator::set_playbackTime(System.Single)
		void Register_UnityEngine_Animator_set_playbackTime();
		Register_UnityEngine_Animator_set_playbackTime();

		//System.Void UnityEngine.Animator::set_recorderStartTime(System.Single)
		void Register_UnityEngine_Animator_set_recorderStartTime();
		Register_UnityEngine_Animator_set_recorderStartTime();

		//System.Void UnityEngine.Animator::set_recorderStopTime(System.Single)
		void Register_UnityEngine_Animator_set_recorderStopTime();
		Register_UnityEngine_Animator_set_recorderStopTime();

		//System.Void UnityEngine.Animator::set_runtimeAnimatorController(UnityEngine.RuntimeAnimatorController)
		void Register_UnityEngine_Animator_set_runtimeAnimatorController();
		Register_UnityEngine_Animator_set_runtimeAnimatorController();

		//System.Void UnityEngine.Animator::set_speed(System.Single)
		void Register_UnityEngine_Animator_set_speed();
		Register_UnityEngine_Animator_set_speed();

		//System.Void UnityEngine.Animator::set_stabilizeFeet(System.Boolean)
		void Register_UnityEngine_Animator_set_stabilizeFeet();
		Register_UnityEngine_Animator_set_stabilizeFeet();

		//System.Void UnityEngine.Animator::set_updateMode(UnityEngine.AnimatorUpdateMode)
		void Register_UnityEngine_Animator_set_updateMode();
		Register_UnityEngine_Animator_set_updateMode();

		//UnityEngine.AnimatorClipInfo[] UnityEngine.Animator::GetCurrentAnimatorClipInfo(System.Int32)
		void Register_UnityEngine_Animator_GetCurrentAnimatorClipInfo();
		Register_UnityEngine_Animator_GetCurrentAnimatorClipInfo();

		//UnityEngine.AnimatorClipInfo[] UnityEngine.Animator::GetNextAnimatorClipInfo(System.Int32)
		void Register_UnityEngine_Animator_GetNextAnimatorClipInfo();
		Register_UnityEngine_Animator_GetNextAnimatorClipInfo();

		//UnityEngine.AnimatorControllerParameter[] UnityEngine.Animator::get_parameters()
		void Register_UnityEngine_Animator_get_parameters();
		Register_UnityEngine_Animator_get_parameters();

		//UnityEngine.AnimatorCullingMode UnityEngine.Animator::get_cullingMode()
		void Register_UnityEngine_Animator_get_cullingMode();
		Register_UnityEngine_Animator_get_cullingMode();

		//UnityEngine.AnimatorRecorderMode UnityEngine.Animator::get_recorderMode()
		void Register_UnityEngine_Animator_get_recorderMode();
		Register_UnityEngine_Animator_get_recorderMode();

		//UnityEngine.AnimatorStateInfo UnityEngine.Animator::GetCurrentAnimatorStateInfo(System.Int32)
		void Register_UnityEngine_Animator_GetCurrentAnimatorStateInfo();
		Register_UnityEngine_Animator_GetCurrentAnimatorStateInfo();

		//UnityEngine.AnimatorStateInfo UnityEngine.Animator::GetNextAnimatorStateInfo(System.Int32)
		void Register_UnityEngine_Animator_GetNextAnimatorStateInfo();
		Register_UnityEngine_Animator_GetNextAnimatorStateInfo();

		//UnityEngine.AnimatorTransitionInfo UnityEngine.Animator::GetAnimatorTransitionInfo(System.Int32)
		void Register_UnityEngine_Animator_GetAnimatorTransitionInfo();
		Register_UnityEngine_Animator_GetAnimatorTransitionInfo();

		//UnityEngine.AnimatorUpdateMode UnityEngine.Animator::get_updateMode()
		void Register_UnityEngine_Animator_get_updateMode();
		Register_UnityEngine_Animator_get_updateMode();

		//UnityEngine.Avatar UnityEngine.Animator::get_avatar()
		void Register_UnityEngine_Animator_get_avatar();
		Register_UnityEngine_Animator_get_avatar();

		//UnityEngine.RuntimeAnimatorController UnityEngine.Animator::get_runtimeAnimatorController()
		void Register_UnityEngine_Animator_get_runtimeAnimatorController();
		Register_UnityEngine_Animator_get_runtimeAnimatorController();

		//UnityEngine.Transform UnityEngine.Animator::GetBoneTransform(UnityEngine.HumanBodyBones)
		void Register_UnityEngine_Animator_GetBoneTransform();
		Register_UnityEngine_Animator_GetBoneTransform();

	//End Registrations for type : UnityEngine.Animator

	//Start Registrations for type : UnityEngine.AnimatorClipInfo

		//UnityEngine.AnimationClip UnityEngine.AnimatorClipInfo::ClipInstanceToScriptingObject(System.Int32)
		void Register_UnityEngine_AnimatorClipInfo_ClipInstanceToScriptingObject();
		Register_UnityEngine_AnimatorClipInfo_ClipInstanceToScriptingObject();

	//End Registrations for type : UnityEngine.AnimatorClipInfo

	//Start Registrations for type : UnityEngine.AnimatorOverrideController

		//System.Void UnityEngine.AnimatorOverrideController::Internal_CreateAnimationSet(UnityEngine.AnimatorOverrideController)
		void Register_UnityEngine_AnimatorOverrideController_Internal_CreateAnimationSet();
		Register_UnityEngine_AnimatorOverrideController_Internal_CreateAnimationSet();

		//System.Void UnityEngine.AnimatorOverrideController::Internal_SetClip(UnityEngine.AnimationClip,UnityEngine.AnimationClip,System.Boolean)
		void Register_UnityEngine_AnimatorOverrideController_Internal_SetClip();
		Register_UnityEngine_AnimatorOverrideController_Internal_SetClip();

		//System.Void UnityEngine.AnimatorOverrideController::Internal_SetClipByName(System.String,UnityEngine.AnimationClip)
		void Register_UnityEngine_AnimatorOverrideController_Internal_SetClipByName();
		Register_UnityEngine_AnimatorOverrideController_Internal_SetClipByName();

		//System.Void UnityEngine.AnimatorOverrideController::Internal_SetDirty()
		void Register_UnityEngine_AnimatorOverrideController_Internal_SetDirty();
		Register_UnityEngine_AnimatorOverrideController_Internal_SetDirty();

		//System.Void UnityEngine.AnimatorOverrideController::set_runtimeAnimatorController(UnityEngine.RuntimeAnimatorController)
		void Register_UnityEngine_AnimatorOverrideController_set_runtimeAnimatorController();
		Register_UnityEngine_AnimatorOverrideController_set_runtimeAnimatorController();

		//UnityEngine.AnimationClip UnityEngine.AnimatorOverrideController::Internal_GetClip(UnityEngine.AnimationClip,System.Boolean)
		void Register_UnityEngine_AnimatorOverrideController_Internal_GetClip();
		Register_UnityEngine_AnimatorOverrideController_Internal_GetClip();

		//UnityEngine.AnimationClip UnityEngine.AnimatorOverrideController::Internal_GetClipByName(System.String,System.Boolean)
		void Register_UnityEngine_AnimatorOverrideController_Internal_GetClipByName();
		Register_UnityEngine_AnimatorOverrideController_Internal_GetClipByName();

		//UnityEngine.AnimationClip[] UnityEngine.AnimatorOverrideController::GetOriginalClips()
		void Register_UnityEngine_AnimatorOverrideController_GetOriginalClips();
		Register_UnityEngine_AnimatorOverrideController_GetOriginalClips();

		//UnityEngine.RuntimeAnimatorController UnityEngine.AnimatorOverrideController::get_runtimeAnimatorController()
		void Register_UnityEngine_AnimatorOverrideController_get_runtimeAnimatorController();
		Register_UnityEngine_AnimatorOverrideController_get_runtimeAnimatorController();

	//End Registrations for type : UnityEngine.AnimatorOverrideController

	//Start Registrations for type : UnityEngine.Application

		//System.Boolean UnityEngine.Application::CanStreamedLevelBeLoaded(System.Int32)
		void Register_UnityEngine_Application_CanStreamedLevelBeLoaded();
		Register_UnityEngine_Application_CanStreamedLevelBeLoaded();

		//System.Boolean UnityEngine.Application::CanStreamedLevelBeLoadedByName(System.String)
		void Register_UnityEngine_Application_CanStreamedLevelBeLoadedByName();
		Register_UnityEngine_Application_CanStreamedLevelBeLoadedByName();

		//System.Boolean UnityEngine.Application::HasProLicense()
		void Register_UnityEngine_Application_HasProLicense();
		Register_UnityEngine_Application_HasProLicense();

		//System.Boolean UnityEngine.Application::HasUserAuthorization(UnityEngine.UserAuthorization)
		void Register_UnityEngine_Application_HasUserAuthorization();
		Register_UnityEngine_Application_HasUserAuthorization();

		//System.Boolean UnityEngine.Application::RequestAdvertisingIdentifierAsync(UnityEngine.Application/AdvertisingIdentifierCallback)
		void Register_UnityEngine_Application_RequestAdvertisingIdentifierAsync();
		Register_UnityEngine_Application_RequestAdvertisingIdentifierAsync();

		//System.Boolean UnityEngine.Application::get_genuine()
		void Register_UnityEngine_Application_get_genuine();
		Register_UnityEngine_Application_get_genuine();

		//System.Boolean UnityEngine.Application::get_genuineCheckAvailable()
		void Register_UnityEngine_Application_get_genuineCheckAvailable();
		Register_UnityEngine_Application_get_genuineCheckAvailable();

		//System.Boolean UnityEngine.Application::get_isEditor()
		void Register_UnityEngine_Application_get_isEditor();
		Register_UnityEngine_Application_get_isEditor();

		//System.Boolean UnityEngine.Application::get_isPlaying()
		void Register_UnityEngine_Application_get_isPlaying();
		Register_UnityEngine_Application_get_isPlaying();

		//System.Boolean UnityEngine.Application::get_isShowingSplashScreen()
		void Register_UnityEngine_Application_get_isShowingSplashScreen();
		Register_UnityEngine_Application_get_isShowingSplashScreen();

		//System.Boolean UnityEngine.Application::get_isWebPlayer()
		void Register_UnityEngine_Application_get_isWebPlayer();
		Register_UnityEngine_Application_get_isWebPlayer();

		//System.Boolean UnityEngine.Application::get_runInBackground()
		void Register_UnityEngine_Application_get_runInBackground();
		Register_UnityEngine_Application_get_runInBackground();

		//System.Boolean UnityEngine.Application::get_webSecurityEnabled()
		void Register_UnityEngine_Application_get_webSecurityEnabled();
		Register_UnityEngine_Application_get_webSecurityEnabled();

		//System.Int32 UnityEngine.Application::get_streamedBytes()
		void Register_UnityEngine_Application_get_streamedBytes();
		Register_UnityEngine_Application_get_streamedBytes();

		//System.Int32 UnityEngine.Application::get_targetFrameRate()
		void Register_UnityEngine_Application_get_targetFrameRate();
		Register_UnityEngine_Application_get_targetFrameRate();

		//System.Single UnityEngine.Application::GetStreamProgressForLevel(System.Int32)
		void Register_UnityEngine_Application_GetStreamProgressForLevel();
		Register_UnityEngine_Application_GetStreamProgressForLevel();

		//System.Single UnityEngine.Application::GetStreamProgressForLevelByName(System.String)
		void Register_UnityEngine_Application_GetStreamProgressForLevelByName();
		Register_UnityEngine_Application_GetStreamProgressForLevelByName();

		//System.String UnityEngine.Application::get_absoluteURL()
		void Register_UnityEngine_Application_get_absoluteURL();
		Register_UnityEngine_Application_get_absoluteURL();

		//System.String UnityEngine.Application::get_bundleIdentifier()
		void Register_UnityEngine_Application_get_bundleIdentifier();
		Register_UnityEngine_Application_get_bundleIdentifier();

		//System.String UnityEngine.Application::get_cloudProjectId()
		void Register_UnityEngine_Application_get_cloudProjectId();
		Register_UnityEngine_Application_get_cloudProjectId();

		//System.String UnityEngine.Application::get_companyName()
		void Register_UnityEngine_Application_get_companyName();
		Register_UnityEngine_Application_get_companyName();

		//System.String UnityEngine.Application::get_dataPath()
		void Register_UnityEngine_Application_get_dataPath();
		Register_UnityEngine_Application_get_dataPath();

		//System.String UnityEngine.Application::get_persistentDataPath()
		void Register_UnityEngine_Application_get_persistentDataPath();
		Register_UnityEngine_Application_get_persistentDataPath();

		//System.String UnityEngine.Application::get_productName()
		void Register_UnityEngine_Application_get_productName();
		Register_UnityEngine_Application_get_productName();

		//System.String UnityEngine.Application::get_srcValue()
		void Register_UnityEngine_Application_get_srcValue();
		Register_UnityEngine_Application_get_srcValue();

		//System.String UnityEngine.Application::get_streamingAssetsPath()
		void Register_UnityEngine_Application_get_streamingAssetsPath();
		Register_UnityEngine_Application_get_streamingAssetsPath();

		//System.String UnityEngine.Application::get_temporaryCachePath()
		void Register_UnityEngine_Application_get_temporaryCachePath();
		Register_UnityEngine_Application_get_temporaryCachePath();

		//System.String UnityEngine.Application::get_unityVersion()
		void Register_UnityEngine_Application_get_unityVersion();
		Register_UnityEngine_Application_get_unityVersion();

		//System.String UnityEngine.Application::get_version()
		void Register_UnityEngine_Application_get_version();
		Register_UnityEngine_Application_get_version();

		//System.String UnityEngine.Application::get_webSecurityHostUrl()
		void Register_UnityEngine_Application_get_webSecurityHostUrl();
		Register_UnityEngine_Application_get_webSecurityHostUrl();

		//System.Void UnityEngine.Application::CancelQuit()
		void Register_UnityEngine_Application_CancelQuit();
		Register_UnityEngine_Application_CancelQuit();

		//System.Void UnityEngine.Application::CaptureScreenshot(System.String,System.Int32)
		void Register_UnityEngine_Application_CaptureScreenshot();
		Register_UnityEngine_Application_CaptureScreenshot();

		//System.Void UnityEngine.Application::Internal_ExternalCall(System.String)
		void Register_UnityEngine_Application_Internal_ExternalCall();
		Register_UnityEngine_Application_Internal_ExternalCall();

		//System.Void UnityEngine.Application::OpenURL(System.String)
		void Register_UnityEngine_Application_OpenURL();
		Register_UnityEngine_Application_OpenURL();

		//System.Void UnityEngine.Application::Quit()
		void Register_UnityEngine_Application_Quit();
		Register_UnityEngine_Application_Quit();

		//System.Void UnityEngine.Application::SetLogCallbackDefined(System.Boolean)
		void Register_UnityEngine_Application_SetLogCallbackDefined();
		Register_UnityEngine_Application_SetLogCallbackDefined();

		//System.Void UnityEngine.Application::set_backgroundLoadingPriority(UnityEngine.ThreadPriority)
		void Register_UnityEngine_Application_set_backgroundLoadingPriority();
		Register_UnityEngine_Application_set_backgroundLoadingPriority();

		//System.Void UnityEngine.Application::set_runInBackground(System.Boolean)
		void Register_UnityEngine_Application_set_runInBackground();
		Register_UnityEngine_Application_set_runInBackground();

		//System.Void UnityEngine.Application::set_stackTraceLogType(UnityEngine.StackTraceLogType)
		void Register_UnityEngine_Application_set_stackTraceLogType();
		Register_UnityEngine_Application_set_stackTraceLogType();

		//System.Void UnityEngine.Application::set_targetFrameRate(System.Int32)
		void Register_UnityEngine_Application_set_targetFrameRate();
		Register_UnityEngine_Application_set_targetFrameRate();

		//UnityEngine.ApplicationInstallMode UnityEngine.Application::get_installMode()
		void Register_UnityEngine_Application_get_installMode();
		Register_UnityEngine_Application_get_installMode();

		//UnityEngine.ApplicationSandboxType UnityEngine.Application::get_sandboxType()
		void Register_UnityEngine_Application_get_sandboxType();
		Register_UnityEngine_Application_get_sandboxType();

		//UnityEngine.AsyncOperation UnityEngine.Application::RequestUserAuthorization(UnityEngine.UserAuthorization)
		void Register_UnityEngine_Application_RequestUserAuthorization();
		Register_UnityEngine_Application_RequestUserAuthorization();

		//UnityEngine.NetworkReachability UnityEngine.Application::get_internetReachability()
		void Register_UnityEngine_Application_get_internetReachability();
		Register_UnityEngine_Application_get_internetReachability();

		//UnityEngine.RuntimePlatform UnityEngine.Application::get_platform()
		void Register_UnityEngine_Application_get_platform();
		Register_UnityEngine_Application_get_platform();

		//UnityEngine.StackTraceLogType UnityEngine.Application::get_stackTraceLogType()
		void Register_UnityEngine_Application_get_stackTraceLogType();
		Register_UnityEngine_Application_get_stackTraceLogType();

		//UnityEngine.SystemLanguage UnityEngine.Application::get_systemLanguage()
		void Register_UnityEngine_Application_get_systemLanguage();
		Register_UnityEngine_Application_get_systemLanguage();

		//UnityEngine.ThreadPriority UnityEngine.Application::get_backgroundLoadingPriority()
		void Register_UnityEngine_Application_get_backgroundLoadingPriority();
		Register_UnityEngine_Application_get_backgroundLoadingPriority();

	//End Registrations for type : UnityEngine.Application

	//Start Registrations for type : UnityEngine.AssetBundle

		//System.Boolean UnityEngine.AssetBundle::Contains(System.String)
		void Register_UnityEngine_AssetBundle_Contains();
		Register_UnityEngine_AssetBundle_Contains();

		//System.String[] UnityEngine.AssetBundle::GetAllAssetNames()
		void Register_UnityEngine_AssetBundle_GetAllAssetNames();
		Register_UnityEngine_AssetBundle_GetAllAssetNames();

		//System.String[] UnityEngine.AssetBundle::GetAllScenePaths()
		void Register_UnityEngine_AssetBundle_GetAllScenePaths();
		Register_UnityEngine_AssetBundle_GetAllScenePaths();

		//System.Void UnityEngine.AssetBundle::Unload(System.Boolean)
		void Register_UnityEngine_AssetBundle_Unload();
		Register_UnityEngine_AssetBundle_Unload();

		//UnityEngine.AssetBundle UnityEngine.AssetBundle::LoadFromFile(System.String,System.UInt32,System.UInt64)
		void Register_UnityEngine_AssetBundle_LoadFromFile();
		Register_UnityEngine_AssetBundle_LoadFromFile();

		//UnityEngine.AssetBundle UnityEngine.AssetBundle::LoadFromMemory(System.Byte[],System.UInt32)
		void Register_UnityEngine_AssetBundle_LoadFromMemory();
		Register_UnityEngine_AssetBundle_LoadFromMemory();

		//UnityEngine.AssetBundleCreateRequest UnityEngine.AssetBundle::LoadFromFileAsync(System.String,System.UInt32,System.UInt64)
		void Register_UnityEngine_AssetBundle_LoadFromFileAsync();
		Register_UnityEngine_AssetBundle_LoadFromFileAsync();

		//UnityEngine.AssetBundleCreateRequest UnityEngine.AssetBundle::LoadFromMemoryAsync(System.Byte[],System.UInt32)
		void Register_UnityEngine_AssetBundle_LoadFromMemoryAsync();
		Register_UnityEngine_AssetBundle_LoadFromMemoryAsync();

		//UnityEngine.AssetBundleRequest UnityEngine.AssetBundle::LoadAssetAsync_Internal(System.String,System.Type)
		void Register_UnityEngine_AssetBundle_LoadAssetAsync_Internal();
		Register_UnityEngine_AssetBundle_LoadAssetAsync_Internal();

		//UnityEngine.AssetBundleRequest UnityEngine.AssetBundle::LoadAssetWithSubAssetsAsync_Internal(System.String,System.Type)
		void Register_UnityEngine_AssetBundle_LoadAssetWithSubAssetsAsync_Internal();
		Register_UnityEngine_AssetBundle_LoadAssetWithSubAssetsAsync_Internal();

		//UnityEngine.Object UnityEngine.AssetBundle::LoadAsset_Internal(System.String,System.Type)
		void Register_UnityEngine_AssetBundle_LoadAsset_Internal();
		Register_UnityEngine_AssetBundle_LoadAsset_Internal();

		//UnityEngine.Object UnityEngine.AssetBundle::get_mainAsset()
		void Register_UnityEngine_AssetBundle_get_mainAsset();
		Register_UnityEngine_AssetBundle_get_mainAsset();

		//UnityEngine.Object[] UnityEngine.AssetBundle::LoadAssetWithSubAssets_Internal(System.String,System.Type)
		void Register_UnityEngine_AssetBundle_LoadAssetWithSubAssets_Internal();
		Register_UnityEngine_AssetBundle_LoadAssetWithSubAssets_Internal();

	//End Registrations for type : UnityEngine.AssetBundle

	//Start Registrations for type : UnityEngine.AssetBundleCreateRequest

		//System.Void UnityEngine.AssetBundleCreateRequest::DisableCompatibilityChecks()
		void Register_UnityEngine_AssetBundleCreateRequest_DisableCompatibilityChecks();
		Register_UnityEngine_AssetBundleCreateRequest_DisableCompatibilityChecks();

		//UnityEngine.AssetBundle UnityEngine.AssetBundleCreateRequest::get_assetBundle()
		void Register_UnityEngine_AssetBundleCreateRequest_get_assetBundle();
		Register_UnityEngine_AssetBundleCreateRequest_get_assetBundle();

	//End Registrations for type : UnityEngine.AssetBundleCreateRequest

	//Start Registrations for type : UnityEngine.AssetBundleRequest

		//UnityEngine.Object UnityEngine.AssetBundleRequest::get_asset()
		void Register_UnityEngine_AssetBundleRequest_get_asset();
		Register_UnityEngine_AssetBundleRequest_get_asset();

		//UnityEngine.Object[] UnityEngine.AssetBundleRequest::get_allAssets()
		void Register_UnityEngine_AssetBundleRequest_get_allAssets();
		Register_UnityEngine_AssetBundleRequest_get_allAssets();

	//End Registrations for type : UnityEngine.AssetBundleRequest

	//Start Registrations for type : UnityEngine.AsyncOperation

		//System.Boolean UnityEngine.AsyncOperation::get_allowSceneActivation()
		void Register_UnityEngine_AsyncOperation_get_allowSceneActivation();
		Register_UnityEngine_AsyncOperation_get_allowSceneActivation();

		//System.Boolean UnityEngine.AsyncOperation::get_isDone()
		void Register_UnityEngine_AsyncOperation_get_isDone();
		Register_UnityEngine_AsyncOperation_get_isDone();

		//System.Int32 UnityEngine.AsyncOperation::get_priority()
		void Register_UnityEngine_AsyncOperation_get_priority();
		Register_UnityEngine_AsyncOperation_get_priority();

		//System.Single UnityEngine.AsyncOperation::get_progress()
		void Register_UnityEngine_AsyncOperation_get_progress();
		Register_UnityEngine_AsyncOperation_get_progress();

		//System.Void UnityEngine.AsyncOperation::InternalDestroy()
		void Register_UnityEngine_AsyncOperation_InternalDestroy();
		Register_UnityEngine_AsyncOperation_InternalDestroy();

		//System.Void UnityEngine.AsyncOperation::set_allowSceneActivation(System.Boolean)
		void Register_UnityEngine_AsyncOperation_set_allowSceneActivation();
		Register_UnityEngine_AsyncOperation_set_allowSceneActivation();

		//System.Void UnityEngine.AsyncOperation::set_priority(System.Int32)
		void Register_UnityEngine_AsyncOperation_set_priority();
		Register_UnityEngine_AsyncOperation_set_priority();

	//End Registrations for type : UnityEngine.AsyncOperation

	//Start Registrations for type : UnityEngine.AudioChorusFilter

		//System.Single UnityEngine.AudioChorusFilter::get_delay()
		void Register_UnityEngine_AudioChorusFilter_get_delay();
		Register_UnityEngine_AudioChorusFilter_get_delay();

		//System.Single UnityEngine.AudioChorusFilter::get_depth()
		void Register_UnityEngine_AudioChorusFilter_get_depth();
		Register_UnityEngine_AudioChorusFilter_get_depth();

		//System.Single UnityEngine.AudioChorusFilter::get_dryMix()
		void Register_UnityEngine_AudioChorusFilter_get_dryMix();
		Register_UnityEngine_AudioChorusFilter_get_dryMix();

		//System.Single UnityEngine.AudioChorusFilter::get_rate()
		void Register_UnityEngine_AudioChorusFilter_get_rate();
		Register_UnityEngine_AudioChorusFilter_get_rate();

		//System.Single UnityEngine.AudioChorusFilter::get_wetMix1()
		void Register_UnityEngine_AudioChorusFilter_get_wetMix1();
		Register_UnityEngine_AudioChorusFilter_get_wetMix1();

		//System.Single UnityEngine.AudioChorusFilter::get_wetMix2()
		void Register_UnityEngine_AudioChorusFilter_get_wetMix2();
		Register_UnityEngine_AudioChorusFilter_get_wetMix2();

		//System.Single UnityEngine.AudioChorusFilter::get_wetMix3()
		void Register_UnityEngine_AudioChorusFilter_get_wetMix3();
		Register_UnityEngine_AudioChorusFilter_get_wetMix3();

		//System.Void UnityEngine.AudioChorusFilter::set_delay(System.Single)
		void Register_UnityEngine_AudioChorusFilter_set_delay();
		Register_UnityEngine_AudioChorusFilter_set_delay();

		//System.Void UnityEngine.AudioChorusFilter::set_depth(System.Single)
		void Register_UnityEngine_AudioChorusFilter_set_depth();
		Register_UnityEngine_AudioChorusFilter_set_depth();

		//System.Void UnityEngine.AudioChorusFilter::set_dryMix(System.Single)
		void Register_UnityEngine_AudioChorusFilter_set_dryMix();
		Register_UnityEngine_AudioChorusFilter_set_dryMix();

		//System.Void UnityEngine.AudioChorusFilter::set_rate(System.Single)
		void Register_UnityEngine_AudioChorusFilter_set_rate();
		Register_UnityEngine_AudioChorusFilter_set_rate();

		//System.Void UnityEngine.AudioChorusFilter::set_wetMix1(System.Single)
		void Register_UnityEngine_AudioChorusFilter_set_wetMix1();
		Register_UnityEngine_AudioChorusFilter_set_wetMix1();

		//System.Void UnityEngine.AudioChorusFilter::set_wetMix2(System.Single)
		void Register_UnityEngine_AudioChorusFilter_set_wetMix2();
		Register_UnityEngine_AudioChorusFilter_set_wetMix2();

		//System.Void UnityEngine.AudioChorusFilter::set_wetMix3(System.Single)
		void Register_UnityEngine_AudioChorusFilter_set_wetMix3();
		Register_UnityEngine_AudioChorusFilter_set_wetMix3();

	//End Registrations for type : UnityEngine.AudioChorusFilter

	//Start Registrations for type : UnityEngine.AudioClip

		//System.Boolean UnityEngine.AudioClip::GetData(System.Single[],System.Int32)
		void Register_UnityEngine_AudioClip_GetData();
		Register_UnityEngine_AudioClip_GetData();

		//System.Boolean UnityEngine.AudioClip::LoadAudioData()
		void Register_UnityEngine_AudioClip_LoadAudioData();
		Register_UnityEngine_AudioClip_LoadAudioData();

		//System.Boolean UnityEngine.AudioClip::SetData(System.Single[],System.Int32)
		void Register_UnityEngine_AudioClip_SetData();
		Register_UnityEngine_AudioClip_SetData();

		//System.Boolean UnityEngine.AudioClip::UnloadAudioData()
		void Register_UnityEngine_AudioClip_UnloadAudioData();
		Register_UnityEngine_AudioClip_UnloadAudioData();

		//System.Boolean UnityEngine.AudioClip::get_loadInBackground()
		void Register_UnityEngine_AudioClip_get_loadInBackground();
		Register_UnityEngine_AudioClip_get_loadInBackground();

		//System.Boolean UnityEngine.AudioClip::get_preloadAudioData()
		void Register_UnityEngine_AudioClip_get_preloadAudioData();
		Register_UnityEngine_AudioClip_get_preloadAudioData();

		//System.Int32 UnityEngine.AudioClip::get_channels()
		void Register_UnityEngine_AudioClip_get_channels();
		Register_UnityEngine_AudioClip_get_channels();

		//System.Int32 UnityEngine.AudioClip::get_frequency()
		void Register_UnityEngine_AudioClip_get_frequency();
		Register_UnityEngine_AudioClip_get_frequency();

		//System.Int32 UnityEngine.AudioClip::get_samples()
		void Register_UnityEngine_AudioClip_get_samples();
		Register_UnityEngine_AudioClip_get_samples();

		//System.Single UnityEngine.AudioClip::get_length()
		void Register_UnityEngine_AudioClip_get_length();
		Register_UnityEngine_AudioClip_get_length();

		//System.Void UnityEngine.AudioClip::Init_Internal(System.String,System.Int32,System.Int32,System.Int32,System.Boolean)
		void Register_UnityEngine_AudioClip_Init_Internal();
		Register_UnityEngine_AudioClip_Init_Internal();

		//UnityEngine.AudioClip UnityEngine.AudioClip::Construct_Internal()
		void Register_UnityEngine_AudioClip_Construct_Internal();
		Register_UnityEngine_AudioClip_Construct_Internal();

		//UnityEngine.AudioClipLoadType UnityEngine.AudioClip::get_loadType()
		void Register_UnityEngine_AudioClip_get_loadType();
		Register_UnityEngine_AudioClip_get_loadType();

		//UnityEngine.AudioDataLoadState UnityEngine.AudioClip::get_loadState()
		void Register_UnityEngine_AudioClip_get_loadState();
		Register_UnityEngine_AudioClip_get_loadState();

	//End Registrations for type : UnityEngine.AudioClip

	//Start Registrations for type : UnityEngine.AudioDistortionFilter

		//System.Single UnityEngine.AudioDistortionFilter::get_distortionLevel()
		void Register_UnityEngine_AudioDistortionFilter_get_distortionLevel();
		Register_UnityEngine_AudioDistortionFilter_get_distortionLevel();

		//System.Void UnityEngine.AudioDistortionFilter::set_distortionLevel(System.Single)
		void Register_UnityEngine_AudioDistortionFilter_set_distortionLevel();
		Register_UnityEngine_AudioDistortionFilter_set_distortionLevel();

	//End Registrations for type : UnityEngine.AudioDistortionFilter

	//Start Registrations for type : UnityEngine.AudioEchoFilter

		//System.Single UnityEngine.AudioEchoFilter::get_decayRatio()
		void Register_UnityEngine_AudioEchoFilter_get_decayRatio();
		Register_UnityEngine_AudioEchoFilter_get_decayRatio();

		//System.Single UnityEngine.AudioEchoFilter::get_delay()
		void Register_UnityEngine_AudioEchoFilter_get_delay();
		Register_UnityEngine_AudioEchoFilter_get_delay();

		//System.Single UnityEngine.AudioEchoFilter::get_dryMix()
		void Register_UnityEngine_AudioEchoFilter_get_dryMix();
		Register_UnityEngine_AudioEchoFilter_get_dryMix();

		//System.Single UnityEngine.AudioEchoFilter::get_wetMix()
		void Register_UnityEngine_AudioEchoFilter_get_wetMix();
		Register_UnityEngine_AudioEchoFilter_get_wetMix();

		//System.Void UnityEngine.AudioEchoFilter::set_decayRatio(System.Single)
		void Register_UnityEngine_AudioEchoFilter_set_decayRatio();
		Register_UnityEngine_AudioEchoFilter_set_decayRatio();

		//System.Void UnityEngine.AudioEchoFilter::set_delay(System.Single)
		void Register_UnityEngine_AudioEchoFilter_set_delay();
		Register_UnityEngine_AudioEchoFilter_set_delay();

		//System.Void UnityEngine.AudioEchoFilter::set_dryMix(System.Single)
		void Register_UnityEngine_AudioEchoFilter_set_dryMix();
		Register_UnityEngine_AudioEchoFilter_set_dryMix();

		//System.Void UnityEngine.AudioEchoFilter::set_wetMix(System.Single)
		void Register_UnityEngine_AudioEchoFilter_set_wetMix();
		Register_UnityEngine_AudioEchoFilter_set_wetMix();

	//End Registrations for type : UnityEngine.AudioEchoFilter

	//Start Registrations for type : UnityEngine.AudioHighPassFilter

		//System.Single UnityEngine.AudioHighPassFilter::get_cutoffFrequency()
		void Register_UnityEngine_AudioHighPassFilter_get_cutoffFrequency();
		Register_UnityEngine_AudioHighPassFilter_get_cutoffFrequency();

		//System.Single UnityEngine.AudioHighPassFilter::get_highpassResonanceQ()
		void Register_UnityEngine_AudioHighPassFilter_get_highpassResonanceQ();
		Register_UnityEngine_AudioHighPassFilter_get_highpassResonanceQ();

		//System.Void UnityEngine.AudioHighPassFilter::set_cutoffFrequency(System.Single)
		void Register_UnityEngine_AudioHighPassFilter_set_cutoffFrequency();
		Register_UnityEngine_AudioHighPassFilter_set_cutoffFrequency();

		//System.Void UnityEngine.AudioHighPassFilter::set_highpassResonanceQ(System.Single)
		void Register_UnityEngine_AudioHighPassFilter_set_highpassResonanceQ();
		Register_UnityEngine_AudioHighPassFilter_set_highpassResonanceQ();

	//End Registrations for type : UnityEngine.AudioHighPassFilter

	//Start Registrations for type : UnityEngine.AudioListener

		//System.Boolean UnityEngine.AudioListener::get_pause()
		void Register_UnityEngine_AudioListener_get_pause();
		Register_UnityEngine_AudioListener_get_pause();

		//System.Single UnityEngine.AudioListener::get_volume()
		void Register_UnityEngine_AudioListener_get_volume();
		Register_UnityEngine_AudioListener_get_volume();

		//System.Void UnityEngine.AudioListener::GetOutputDataHelper(System.Single[],System.Int32)
		void Register_UnityEngine_AudioListener_GetOutputDataHelper();
		Register_UnityEngine_AudioListener_GetOutputDataHelper();

		//System.Void UnityEngine.AudioListener::GetSpectrumDataHelper(System.Single[],System.Int32,UnityEngine.FFTWindow)
		void Register_UnityEngine_AudioListener_GetSpectrumDataHelper();
		Register_UnityEngine_AudioListener_GetSpectrumDataHelper();

		//System.Void UnityEngine.AudioListener::set_pause(System.Boolean)
		void Register_UnityEngine_AudioListener_set_pause();
		Register_UnityEngine_AudioListener_set_pause();

		//System.Void UnityEngine.AudioListener::set_velocityUpdateMode(UnityEngine.AudioVelocityUpdateMode)
		void Register_UnityEngine_AudioListener_set_velocityUpdateMode();
		Register_UnityEngine_AudioListener_set_velocityUpdateMode();

		//System.Void UnityEngine.AudioListener::set_volume(System.Single)
		void Register_UnityEngine_AudioListener_set_volume();
		Register_UnityEngine_AudioListener_set_volume();

		//UnityEngine.AudioVelocityUpdateMode UnityEngine.AudioListener::get_velocityUpdateMode()
		void Register_UnityEngine_AudioListener_get_velocityUpdateMode();
		Register_UnityEngine_AudioListener_get_velocityUpdateMode();

	//End Registrations for type : UnityEngine.AudioListener

	//Start Registrations for type : UnityEngine.AudioLowPassFilter

		//System.Single UnityEngine.AudioLowPassFilter::get_cutoffFrequency()
		void Register_UnityEngine_AudioLowPassFilter_get_cutoffFrequency();
		Register_UnityEngine_AudioLowPassFilter_get_cutoffFrequency();

		//System.Single UnityEngine.AudioLowPassFilter::get_lowpassResonanceQ()
		void Register_UnityEngine_AudioLowPassFilter_get_lowpassResonanceQ();
		Register_UnityEngine_AudioLowPassFilter_get_lowpassResonanceQ();

		//System.Void UnityEngine.AudioLowPassFilter::set_customCutoffCurve(UnityEngine.AnimationCurve)
		void Register_UnityEngine_AudioLowPassFilter_set_customCutoffCurve();
		Register_UnityEngine_AudioLowPassFilter_set_customCutoffCurve();

		//System.Void UnityEngine.AudioLowPassFilter::set_cutoffFrequency(System.Single)
		void Register_UnityEngine_AudioLowPassFilter_set_cutoffFrequency();
		Register_UnityEngine_AudioLowPassFilter_set_cutoffFrequency();

		//System.Void UnityEngine.AudioLowPassFilter::set_lowpassResonanceQ(System.Single)
		void Register_UnityEngine_AudioLowPassFilter_set_lowpassResonanceQ();
		Register_UnityEngine_AudioLowPassFilter_set_lowpassResonanceQ();

		//UnityEngine.AnimationCurve UnityEngine.AudioLowPassFilter::get_customCutoffCurve()
		void Register_UnityEngine_AudioLowPassFilter_get_customCutoffCurve();
		Register_UnityEngine_AudioLowPassFilter_get_customCutoffCurve();

	//End Registrations for type : UnityEngine.AudioLowPassFilter

	//Start Registrations for type : UnityEngine.AudioReverbFilter

		//System.Single UnityEngine.AudioReverbFilter::get_decayHFRatio()
		void Register_UnityEngine_AudioReverbFilter_get_decayHFRatio();
		Register_UnityEngine_AudioReverbFilter_get_decayHFRatio();

		//System.Single UnityEngine.AudioReverbFilter::get_decayTime()
		void Register_UnityEngine_AudioReverbFilter_get_decayTime();
		Register_UnityEngine_AudioReverbFilter_get_decayTime();

		//System.Single UnityEngine.AudioReverbFilter::get_density()
		void Register_UnityEngine_AudioReverbFilter_get_density();
		Register_UnityEngine_AudioReverbFilter_get_density();

		//System.Single UnityEngine.AudioReverbFilter::get_diffusion()
		void Register_UnityEngine_AudioReverbFilter_get_diffusion();
		Register_UnityEngine_AudioReverbFilter_get_diffusion();

		//System.Single UnityEngine.AudioReverbFilter::get_dryLevel()
		void Register_UnityEngine_AudioReverbFilter_get_dryLevel();
		Register_UnityEngine_AudioReverbFilter_get_dryLevel();

		//System.Single UnityEngine.AudioReverbFilter::get_hfReference()
		void Register_UnityEngine_AudioReverbFilter_get_hfReference();
		Register_UnityEngine_AudioReverbFilter_get_hfReference();

		//System.Single UnityEngine.AudioReverbFilter::get_lfReference()
		void Register_UnityEngine_AudioReverbFilter_get_lfReference();
		Register_UnityEngine_AudioReverbFilter_get_lfReference();

		//System.Single UnityEngine.AudioReverbFilter::get_reflectionsDelay()
		void Register_UnityEngine_AudioReverbFilter_get_reflectionsDelay();
		Register_UnityEngine_AudioReverbFilter_get_reflectionsDelay();

		//System.Single UnityEngine.AudioReverbFilter::get_reflectionsLevel()
		void Register_UnityEngine_AudioReverbFilter_get_reflectionsLevel();
		Register_UnityEngine_AudioReverbFilter_get_reflectionsLevel();

		//System.Single UnityEngine.AudioReverbFilter::get_reverbDelay()
		void Register_UnityEngine_AudioReverbFilter_get_reverbDelay();
		Register_UnityEngine_AudioReverbFilter_get_reverbDelay();

		//System.Single UnityEngine.AudioReverbFilter::get_reverbLevel()
		void Register_UnityEngine_AudioReverbFilter_get_reverbLevel();
		Register_UnityEngine_AudioReverbFilter_get_reverbLevel();

		//System.Single UnityEngine.AudioReverbFilter::get_room()
		void Register_UnityEngine_AudioReverbFilter_get_room();
		Register_UnityEngine_AudioReverbFilter_get_room();

		//System.Single UnityEngine.AudioReverbFilter::get_roomHF()
		void Register_UnityEngine_AudioReverbFilter_get_roomHF();
		Register_UnityEngine_AudioReverbFilter_get_roomHF();

		//System.Single UnityEngine.AudioReverbFilter::get_roomLF()
		void Register_UnityEngine_AudioReverbFilter_get_roomLF();
		Register_UnityEngine_AudioReverbFilter_get_roomLF();

		//System.Single UnityEngine.AudioReverbFilter::get_roomRolloff()
		void Register_UnityEngine_AudioReverbFilter_get_roomRolloff();
		Register_UnityEngine_AudioReverbFilter_get_roomRolloff();

		//System.Void UnityEngine.AudioReverbFilter::set_decayHFRatio(System.Single)
		void Register_UnityEngine_AudioReverbFilter_set_decayHFRatio();
		Register_UnityEngine_AudioReverbFilter_set_decayHFRatio();

		//System.Void UnityEngine.AudioReverbFilter::set_decayTime(System.Single)
		void Register_UnityEngine_AudioReverbFilter_set_decayTime();
		Register_UnityEngine_AudioReverbFilter_set_decayTime();

		//System.Void UnityEngine.AudioReverbFilter::set_density(System.Single)
		void Register_UnityEngine_AudioReverbFilter_set_density();
		Register_UnityEngine_AudioReverbFilter_set_density();

		//System.Void UnityEngine.AudioReverbFilter::set_diffusion(System.Single)
		void Register_UnityEngine_AudioReverbFilter_set_diffusion();
		Register_UnityEngine_AudioReverbFilter_set_diffusion();

		//System.Void UnityEngine.AudioReverbFilter::set_dryLevel(System.Single)
		void Register_UnityEngine_AudioReverbFilter_set_dryLevel();
		Register_UnityEngine_AudioReverbFilter_set_dryLevel();

		//System.Void UnityEngine.AudioReverbFilter::set_hfReference(System.Single)
		void Register_UnityEngine_AudioReverbFilter_set_hfReference();
		Register_UnityEngine_AudioReverbFilter_set_hfReference();

		//System.Void UnityEngine.AudioReverbFilter::set_lfReference(System.Single)
		void Register_UnityEngine_AudioReverbFilter_set_lfReference();
		Register_UnityEngine_AudioReverbFilter_set_lfReference();

		//System.Void UnityEngine.AudioReverbFilter::set_reflectionsDelay(System.Single)
		void Register_UnityEngine_AudioReverbFilter_set_reflectionsDelay();
		Register_UnityEngine_AudioReverbFilter_set_reflectionsDelay();

		//System.Void UnityEngine.AudioReverbFilter::set_reflectionsLevel(System.Single)
		void Register_UnityEngine_AudioReverbFilter_set_reflectionsLevel();
		Register_UnityEngine_AudioReverbFilter_set_reflectionsLevel();

		//System.Void UnityEngine.AudioReverbFilter::set_reverbDelay(System.Single)
		void Register_UnityEngine_AudioReverbFilter_set_reverbDelay();
		Register_UnityEngine_AudioReverbFilter_set_reverbDelay();

		//System.Void UnityEngine.AudioReverbFilter::set_reverbLevel(System.Single)
		void Register_UnityEngine_AudioReverbFilter_set_reverbLevel();
		Register_UnityEngine_AudioReverbFilter_set_reverbLevel();

		//System.Void UnityEngine.AudioReverbFilter::set_reverbPreset(UnityEngine.AudioReverbPreset)
		void Register_UnityEngine_AudioReverbFilter_set_reverbPreset();
		Register_UnityEngine_AudioReverbFilter_set_reverbPreset();

		//System.Void UnityEngine.AudioReverbFilter::set_room(System.Single)
		void Register_UnityEngine_AudioReverbFilter_set_room();
		Register_UnityEngine_AudioReverbFilter_set_room();

		//System.Void UnityEngine.AudioReverbFilter::set_roomHF(System.Single)
		void Register_UnityEngine_AudioReverbFilter_set_roomHF();
		Register_UnityEngine_AudioReverbFilter_set_roomHF();

		//System.Void UnityEngine.AudioReverbFilter::set_roomLF(System.Single)
		void Register_UnityEngine_AudioReverbFilter_set_roomLF();
		Register_UnityEngine_AudioReverbFilter_set_roomLF();

		//System.Void UnityEngine.AudioReverbFilter::set_roomRolloff(System.Single)
		void Register_UnityEngine_AudioReverbFilter_set_roomRolloff();
		Register_UnityEngine_AudioReverbFilter_set_roomRolloff();

		//UnityEngine.AudioReverbPreset UnityEngine.AudioReverbFilter::get_reverbPreset()
		void Register_UnityEngine_AudioReverbFilter_get_reverbPreset();
		Register_UnityEngine_AudioReverbFilter_get_reverbPreset();

	//End Registrations for type : UnityEngine.AudioReverbFilter

	//Start Registrations for type : UnityEngine.AudioReverbZone

		//System.Int32 UnityEngine.AudioReverbZone::get_reflections()
		void Register_UnityEngine_AudioReverbZone_get_reflections();
		Register_UnityEngine_AudioReverbZone_get_reflections();

		//System.Int32 UnityEngine.AudioReverbZone::get_reverb()
		void Register_UnityEngine_AudioReverbZone_get_reverb();
		Register_UnityEngine_AudioReverbZone_get_reverb();

		//System.Int32 UnityEngine.AudioReverbZone::get_room()
		void Register_UnityEngine_AudioReverbZone_get_room();
		Register_UnityEngine_AudioReverbZone_get_room();

		//System.Int32 UnityEngine.AudioReverbZone::get_roomHF()
		void Register_UnityEngine_AudioReverbZone_get_roomHF();
		Register_UnityEngine_AudioReverbZone_get_roomHF();

		//System.Int32 UnityEngine.AudioReverbZone::get_roomLF()
		void Register_UnityEngine_AudioReverbZone_get_roomLF();
		Register_UnityEngine_AudioReverbZone_get_roomLF();

		//System.Single UnityEngine.AudioReverbZone::get_HFReference()
		void Register_UnityEngine_AudioReverbZone_get_HFReference();
		Register_UnityEngine_AudioReverbZone_get_HFReference();

		//System.Single UnityEngine.AudioReverbZone::get_LFReference()
		void Register_UnityEngine_AudioReverbZone_get_LFReference();
		Register_UnityEngine_AudioReverbZone_get_LFReference();

		//System.Single UnityEngine.AudioReverbZone::get_decayHFRatio()
		void Register_UnityEngine_AudioReverbZone_get_decayHFRatio();
		Register_UnityEngine_AudioReverbZone_get_decayHFRatio();

		//System.Single UnityEngine.AudioReverbZone::get_decayTime()
		void Register_UnityEngine_AudioReverbZone_get_decayTime();
		Register_UnityEngine_AudioReverbZone_get_decayTime();

		//System.Single UnityEngine.AudioReverbZone::get_density()
		void Register_UnityEngine_AudioReverbZone_get_density();
		Register_UnityEngine_AudioReverbZone_get_density();

		//System.Single UnityEngine.AudioReverbZone::get_diffusion()
		void Register_UnityEngine_AudioReverbZone_get_diffusion();
		Register_UnityEngine_AudioReverbZone_get_diffusion();

		//System.Single UnityEngine.AudioReverbZone::get_maxDistance()
		void Register_UnityEngine_AudioReverbZone_get_maxDistance();
		Register_UnityEngine_AudioReverbZone_get_maxDistance();

		//System.Single UnityEngine.AudioReverbZone::get_minDistance()
		void Register_UnityEngine_AudioReverbZone_get_minDistance();
		Register_UnityEngine_AudioReverbZone_get_minDistance();

		//System.Single UnityEngine.AudioReverbZone::get_reflectionsDelay()
		void Register_UnityEngine_AudioReverbZone_get_reflectionsDelay();
		Register_UnityEngine_AudioReverbZone_get_reflectionsDelay();

		//System.Single UnityEngine.AudioReverbZone::get_reverbDelay()
		void Register_UnityEngine_AudioReverbZone_get_reverbDelay();
		Register_UnityEngine_AudioReverbZone_get_reverbDelay();

		//System.Single UnityEngine.AudioReverbZone::get_roomRolloffFactor()
		void Register_UnityEngine_AudioReverbZone_get_roomRolloffFactor();
		Register_UnityEngine_AudioReverbZone_get_roomRolloffFactor();

		//System.Void UnityEngine.AudioReverbZone::set_HFReference(System.Single)
		void Register_UnityEngine_AudioReverbZone_set_HFReference();
		Register_UnityEngine_AudioReverbZone_set_HFReference();

		//System.Void UnityEngine.AudioReverbZone::set_LFReference(System.Single)
		void Register_UnityEngine_AudioReverbZone_set_LFReference();
		Register_UnityEngine_AudioReverbZone_set_LFReference();

		//System.Void UnityEngine.AudioReverbZone::set_decayHFRatio(System.Single)
		void Register_UnityEngine_AudioReverbZone_set_decayHFRatio();
		Register_UnityEngine_AudioReverbZone_set_decayHFRatio();

		//System.Void UnityEngine.AudioReverbZone::set_decayTime(System.Single)
		void Register_UnityEngine_AudioReverbZone_set_decayTime();
		Register_UnityEngine_AudioReverbZone_set_decayTime();

		//System.Void UnityEngine.AudioReverbZone::set_density(System.Single)
		void Register_UnityEngine_AudioReverbZone_set_density();
		Register_UnityEngine_AudioReverbZone_set_density();

		//System.Void UnityEngine.AudioReverbZone::set_diffusion(System.Single)
		void Register_UnityEngine_AudioReverbZone_set_diffusion();
		Register_UnityEngine_AudioReverbZone_set_diffusion();

		//System.Void UnityEngine.AudioReverbZone::set_maxDistance(System.Single)
		void Register_UnityEngine_AudioReverbZone_set_maxDistance();
		Register_UnityEngine_AudioReverbZone_set_maxDistance();

		//System.Void UnityEngine.AudioReverbZone::set_minDistance(System.Single)
		void Register_UnityEngine_AudioReverbZone_set_minDistance();
		Register_UnityEngine_AudioReverbZone_set_minDistance();

		//System.Void UnityEngine.AudioReverbZone::set_reflections(System.Int32)
		void Register_UnityEngine_AudioReverbZone_set_reflections();
		Register_UnityEngine_AudioReverbZone_set_reflections();

		//System.Void UnityEngine.AudioReverbZone::set_reflectionsDelay(System.Single)
		void Register_UnityEngine_AudioReverbZone_set_reflectionsDelay();
		Register_UnityEngine_AudioReverbZone_set_reflectionsDelay();

		//System.Void UnityEngine.AudioReverbZone::set_reverb(System.Int32)
		void Register_UnityEngine_AudioReverbZone_set_reverb();
		Register_UnityEngine_AudioReverbZone_set_reverb();

		//System.Void UnityEngine.AudioReverbZone::set_reverbDelay(System.Single)
		void Register_UnityEngine_AudioReverbZone_set_reverbDelay();
		Register_UnityEngine_AudioReverbZone_set_reverbDelay();

		//System.Void UnityEngine.AudioReverbZone::set_reverbPreset(UnityEngine.AudioReverbPreset)
		void Register_UnityEngine_AudioReverbZone_set_reverbPreset();
		Register_UnityEngine_AudioReverbZone_set_reverbPreset();

		//System.Void UnityEngine.AudioReverbZone::set_room(System.Int32)
		void Register_UnityEngine_AudioReverbZone_set_room();
		Register_UnityEngine_AudioReverbZone_set_room();

		//System.Void UnityEngine.AudioReverbZone::set_roomHF(System.Int32)
		void Register_UnityEngine_AudioReverbZone_set_roomHF();
		Register_UnityEngine_AudioReverbZone_set_roomHF();

		//System.Void UnityEngine.AudioReverbZone::set_roomLF(System.Int32)
		void Register_UnityEngine_AudioReverbZone_set_roomLF();
		Register_UnityEngine_AudioReverbZone_set_roomLF();

		//System.Void UnityEngine.AudioReverbZone::set_roomRolloffFactor(System.Single)
		void Register_UnityEngine_AudioReverbZone_set_roomRolloffFactor();
		Register_UnityEngine_AudioReverbZone_set_roomRolloffFactor();

		//UnityEngine.AudioReverbPreset UnityEngine.AudioReverbZone::get_reverbPreset()
		void Register_UnityEngine_AudioReverbZone_get_reverbPreset();
		Register_UnityEngine_AudioReverbZone_get_reverbPreset();

	//End Registrations for type : UnityEngine.AudioReverbZone

	//Start Registrations for type : UnityEngine.AudioSettings

		//System.Boolean UnityEngine.AudioSettings::INTERNAL_CALL_Reset(UnityEngine.AudioConfiguration&)
		void Register_UnityEngine_AudioSettings_INTERNAL_CALL_Reset();
		Register_UnityEngine_AudioSettings_INTERNAL_CALL_Reset();

		//System.Double UnityEngine.AudioSettings::get_dspTime()
		void Register_UnityEngine_AudioSettings_get_dspTime();
		Register_UnityEngine_AudioSettings_get_dspTime();

		//System.Int32 UnityEngine.AudioSettings::get_outputSampleRate()
		void Register_UnityEngine_AudioSettings_get_outputSampleRate();
		Register_UnityEngine_AudioSettings_get_outputSampleRate();

		//System.Void UnityEngine.AudioSettings::GetDSPBufferSize(System.Int32&,System.Int32&)
		void Register_UnityEngine_AudioSettings_GetDSPBufferSize();
		Register_UnityEngine_AudioSettings_GetDSPBufferSize();

		//System.Void UnityEngine.AudioSettings::INTERNAL_CALL_GetConfiguration(UnityEngine.AudioConfiguration&)
		void Register_UnityEngine_AudioSettings_INTERNAL_CALL_GetConfiguration();
		Register_UnityEngine_AudioSettings_INTERNAL_CALL_GetConfiguration();

		//System.Void UnityEngine.AudioSettings::set_outputSampleRate(System.Int32)
		void Register_UnityEngine_AudioSettings_set_outputSampleRate();
		Register_UnityEngine_AudioSettings_set_outputSampleRate();

		//System.Void UnityEngine.AudioSettings::set_speakerMode(UnityEngine.AudioSpeakerMode)
		void Register_UnityEngine_AudioSettings_set_speakerMode();
		Register_UnityEngine_AudioSettings_set_speakerMode();

		//UnityEngine.AudioSpeakerMode UnityEngine.AudioSettings::get_driverCapabilities()
		void Register_UnityEngine_AudioSettings_get_driverCapabilities();
		Register_UnityEngine_AudioSettings_get_driverCapabilities();

		//UnityEngine.AudioSpeakerMode UnityEngine.AudioSettings::get_speakerMode()
		void Register_UnityEngine_AudioSettings_get_speakerMode();
		Register_UnityEngine_AudioSettings_get_speakerMode();

	//End Registrations for type : UnityEngine.AudioSettings

	//Start Registrations for type : UnityEngine.AudioSource

		//System.Boolean UnityEngine.AudioSource::GetSpatializerFloat(System.Int32,System.Single&)
		void Register_UnityEngine_AudioSource_GetSpatializerFloat();
		Register_UnityEngine_AudioSource_GetSpatializerFloat();

		//System.Boolean UnityEngine.AudioSource::SetSpatializerFloat(System.Int32,System.Single)
		void Register_UnityEngine_AudioSource_SetSpatializerFloat();
		Register_UnityEngine_AudioSource_SetSpatializerFloat();

		//System.Boolean UnityEngine.AudioSource::get_bypassEffects()
		void Register_UnityEngine_AudioSource_get_bypassEffects();
		Register_UnityEngine_AudioSource_get_bypassEffects();

		//System.Boolean UnityEngine.AudioSource::get_bypassListenerEffects()
		void Register_UnityEngine_AudioSource_get_bypassListenerEffects();
		Register_UnityEngine_AudioSource_get_bypassListenerEffects();

		//System.Boolean UnityEngine.AudioSource::get_bypassReverbZones()
		void Register_UnityEngine_AudioSource_get_bypassReverbZones();
		Register_UnityEngine_AudioSource_get_bypassReverbZones();

		//System.Boolean UnityEngine.AudioSource::get_ignoreListenerPause()
		void Register_UnityEngine_AudioSource_get_ignoreListenerPause();
		Register_UnityEngine_AudioSource_get_ignoreListenerPause();

		//System.Boolean UnityEngine.AudioSource::get_ignoreListenerVolume()
		void Register_UnityEngine_AudioSource_get_ignoreListenerVolume();
		Register_UnityEngine_AudioSource_get_ignoreListenerVolume();

		//System.Boolean UnityEngine.AudioSource::get_isPlaying()
		void Register_UnityEngine_AudioSource_get_isPlaying();
		Register_UnityEngine_AudioSource_get_isPlaying();

		//System.Boolean UnityEngine.AudioSource::get_loop()
		void Register_UnityEngine_AudioSource_get_loop();
		Register_UnityEngine_AudioSource_get_loop();

		//System.Boolean UnityEngine.AudioSource::get_mute()
		void Register_UnityEngine_AudioSource_get_mute();
		Register_UnityEngine_AudioSource_get_mute();

		//System.Boolean UnityEngine.AudioSource::get_playOnAwake()
		void Register_UnityEngine_AudioSource_get_playOnAwake();
		Register_UnityEngine_AudioSource_get_playOnAwake();

		//System.Boolean UnityEngine.AudioSource::get_spatialize()
		void Register_UnityEngine_AudioSource_get_spatialize();
		Register_UnityEngine_AudioSource_get_spatialize();

		//System.Int32 UnityEngine.AudioSource::get_priority()
		void Register_UnityEngine_AudioSource_get_priority();
		Register_UnityEngine_AudioSource_get_priority();

		//System.Int32 UnityEngine.AudioSource::get_timeSamples()
		void Register_UnityEngine_AudioSource_get_timeSamples();
		Register_UnityEngine_AudioSource_get_timeSamples();

		//System.Single UnityEngine.AudioSource::get_dopplerLevel()
		void Register_UnityEngine_AudioSource_get_dopplerLevel();
		Register_UnityEngine_AudioSource_get_dopplerLevel();

		//System.Single UnityEngine.AudioSource::get_maxDistance()
		void Register_UnityEngine_AudioSource_get_maxDistance();
		Register_UnityEngine_AudioSource_get_maxDistance();

		//System.Single UnityEngine.AudioSource::get_minDistance()
		void Register_UnityEngine_AudioSource_get_minDistance();
		Register_UnityEngine_AudioSource_get_minDistance();

		//System.Single UnityEngine.AudioSource::get_panStereo()
		void Register_UnityEngine_AudioSource_get_panStereo();
		Register_UnityEngine_AudioSource_get_panStereo();

		//System.Single UnityEngine.AudioSource::get_pitch()
		void Register_UnityEngine_AudioSource_get_pitch();
		Register_UnityEngine_AudioSource_get_pitch();

		//System.Single UnityEngine.AudioSource::get_reverbZoneMix()
		void Register_UnityEngine_AudioSource_get_reverbZoneMix();
		Register_UnityEngine_AudioSource_get_reverbZoneMix();

		//System.Single UnityEngine.AudioSource::get_spatialBlend()
		void Register_UnityEngine_AudioSource_get_spatialBlend();
		Register_UnityEngine_AudioSource_get_spatialBlend();

		//System.Single UnityEngine.AudioSource::get_spread()
		void Register_UnityEngine_AudioSource_get_spread();
		Register_UnityEngine_AudioSource_get_spread();

		//System.Single UnityEngine.AudioSource::get_time()
		void Register_UnityEngine_AudioSource_get_time();
		Register_UnityEngine_AudioSource_get_time();

		//System.Single UnityEngine.AudioSource::get_volume()
		void Register_UnityEngine_AudioSource_get_volume();
		Register_UnityEngine_AudioSource_get_volume();

		//System.Void UnityEngine.AudioSource::GetOutputDataHelper(System.Single[],System.Int32)
		void Register_UnityEngine_AudioSource_GetOutputDataHelper();
		Register_UnityEngine_AudioSource_GetOutputDataHelper();

		//System.Void UnityEngine.AudioSource::GetSpectrumDataHelper(System.Single[],System.Int32,UnityEngine.FFTWindow)
		void Register_UnityEngine_AudioSource_GetSpectrumDataHelper();
		Register_UnityEngine_AudioSource_GetSpectrumDataHelper();

		//System.Void UnityEngine.AudioSource::INTERNAL_CALL_Pause(UnityEngine.AudioSource)
		void Register_UnityEngine_AudioSource_INTERNAL_CALL_Pause();
		Register_UnityEngine_AudioSource_INTERNAL_CALL_Pause();

		//System.Void UnityEngine.AudioSource::INTERNAL_CALL_UnPause(UnityEngine.AudioSource)
		void Register_UnityEngine_AudioSource_INTERNAL_CALL_UnPause();
		Register_UnityEngine_AudioSource_INTERNAL_CALL_UnPause();

		//System.Void UnityEngine.AudioSource::Play(System.UInt64)
		void Register_UnityEngine_AudioSource_Play();
		Register_UnityEngine_AudioSource_Play();

		//System.Void UnityEngine.AudioSource::PlayDelayed(System.Single)
		void Register_UnityEngine_AudioSource_PlayDelayed();
		Register_UnityEngine_AudioSource_PlayDelayed();

		//System.Void UnityEngine.AudioSource::PlayOneShot(UnityEngine.AudioClip,System.Single)
		void Register_UnityEngine_AudioSource_PlayOneShot();
		Register_UnityEngine_AudioSource_PlayOneShot();

		//System.Void UnityEngine.AudioSource::PlayScheduled(System.Double)
		void Register_UnityEngine_AudioSource_PlayScheduled();
		Register_UnityEngine_AudioSource_PlayScheduled();

		//System.Void UnityEngine.AudioSource::SetCustomCurve(UnityEngine.AudioSourceCurveType,UnityEngine.AnimationCurve)
		void Register_UnityEngine_AudioSource_SetCustomCurve();
		Register_UnityEngine_AudioSource_SetCustomCurve();

		//System.Void UnityEngine.AudioSource::SetScheduledEndTime(System.Double)
		void Register_UnityEngine_AudioSource_SetScheduledEndTime();
		Register_UnityEngine_AudioSource_SetScheduledEndTime();

		//System.Void UnityEngine.AudioSource::SetScheduledStartTime(System.Double)
		void Register_UnityEngine_AudioSource_SetScheduledStartTime();
		Register_UnityEngine_AudioSource_SetScheduledStartTime();

		//System.Void UnityEngine.AudioSource::Stop()
		void Register_UnityEngine_AudioSource_Stop();
		Register_UnityEngine_AudioSource_Stop();

		//System.Void UnityEngine.AudioSource::set_bypassEffects(System.Boolean)
		void Register_UnityEngine_AudioSource_set_bypassEffects();
		Register_UnityEngine_AudioSource_set_bypassEffects();

		//System.Void UnityEngine.AudioSource::set_bypassListenerEffects(System.Boolean)
		void Register_UnityEngine_AudioSource_set_bypassListenerEffects();
		Register_UnityEngine_AudioSource_set_bypassListenerEffects();

		//System.Void UnityEngine.AudioSource::set_bypassReverbZones(System.Boolean)
		void Register_UnityEngine_AudioSource_set_bypassReverbZones();
		Register_UnityEngine_AudioSource_set_bypassReverbZones();

		//System.Void UnityEngine.AudioSource::set_clip(UnityEngine.AudioClip)
		void Register_UnityEngine_AudioSource_set_clip();
		Register_UnityEngine_AudioSource_set_clip();

		//System.Void UnityEngine.AudioSource::set_dopplerLevel(System.Single)
		void Register_UnityEngine_AudioSource_set_dopplerLevel();
		Register_UnityEngine_AudioSource_set_dopplerLevel();

		//System.Void UnityEngine.AudioSource::set_ignoreListenerPause(System.Boolean)
		void Register_UnityEngine_AudioSource_set_ignoreListenerPause();
		Register_UnityEngine_AudioSource_set_ignoreListenerPause();

		//System.Void UnityEngine.AudioSource::set_ignoreListenerVolume(System.Boolean)
		void Register_UnityEngine_AudioSource_set_ignoreListenerVolume();
		Register_UnityEngine_AudioSource_set_ignoreListenerVolume();

		//System.Void UnityEngine.AudioSource::set_loop(System.Boolean)
		void Register_UnityEngine_AudioSource_set_loop();
		Register_UnityEngine_AudioSource_set_loop();

		//System.Void UnityEngine.AudioSource::set_maxDistance(System.Single)
		void Register_UnityEngine_AudioSource_set_maxDistance();
		Register_UnityEngine_AudioSource_set_maxDistance();

		//System.Void UnityEngine.AudioSource::set_minDistance(System.Single)
		void Register_UnityEngine_AudioSource_set_minDistance();
		Register_UnityEngine_AudioSource_set_minDistance();

		//System.Void UnityEngine.AudioSource::set_mute(System.Boolean)
		void Register_UnityEngine_AudioSource_set_mute();
		Register_UnityEngine_AudioSource_set_mute();

		//System.Void UnityEngine.AudioSource::set_outputAudioMixerGroup(UnityEngine.Audio.AudioMixerGroup)
		void Register_UnityEngine_AudioSource_set_outputAudioMixerGroup();
		Register_UnityEngine_AudioSource_set_outputAudioMixerGroup();

		//System.Void UnityEngine.AudioSource::set_panStereo(System.Single)
		void Register_UnityEngine_AudioSource_set_panStereo();
		Register_UnityEngine_AudioSource_set_panStereo();

		//System.Void UnityEngine.AudioSource::set_pitch(System.Single)
		void Register_UnityEngine_AudioSource_set_pitch();
		Register_UnityEngine_AudioSource_set_pitch();

		//System.Void UnityEngine.AudioSource::set_playOnAwake(System.Boolean)
		void Register_UnityEngine_AudioSource_set_playOnAwake();
		Register_UnityEngine_AudioSource_set_playOnAwake();

		//System.Void UnityEngine.AudioSource::set_priority(System.Int32)
		void Register_UnityEngine_AudioSource_set_priority();
		Register_UnityEngine_AudioSource_set_priority();

		//System.Void UnityEngine.AudioSource::set_reverbZoneMix(System.Single)
		void Register_UnityEngine_AudioSource_set_reverbZoneMix();
		Register_UnityEngine_AudioSource_set_reverbZoneMix();

		//System.Void UnityEngine.AudioSource::set_rolloffMode(UnityEngine.AudioRolloffMode)
		void Register_UnityEngine_AudioSource_set_rolloffMode();
		Register_UnityEngine_AudioSource_set_rolloffMode();

		//System.Void UnityEngine.AudioSource::set_spatialBlend(System.Single)
		void Register_UnityEngine_AudioSource_set_spatialBlend();
		Register_UnityEngine_AudioSource_set_spatialBlend();

		//System.Void UnityEngine.AudioSource::set_spatialize(System.Boolean)
		void Register_UnityEngine_AudioSource_set_spatialize();
		Register_UnityEngine_AudioSource_set_spatialize();

		//System.Void UnityEngine.AudioSource::set_spread(System.Single)
		void Register_UnityEngine_AudioSource_set_spread();
		Register_UnityEngine_AudioSource_set_spread();

		//System.Void UnityEngine.AudioSource::set_time(System.Single)
		void Register_UnityEngine_AudioSource_set_time();
		Register_UnityEngine_AudioSource_set_time();

		//System.Void UnityEngine.AudioSource::set_timeSamples(System.Int32)
		void Register_UnityEngine_AudioSource_set_timeSamples();
		Register_UnityEngine_AudioSource_set_timeSamples();

		//System.Void UnityEngine.AudioSource::set_velocityUpdateMode(UnityEngine.AudioVelocityUpdateMode)
		void Register_UnityEngine_AudioSource_set_velocityUpdateMode();
		Register_UnityEngine_AudioSource_set_velocityUpdateMode();

		//System.Void UnityEngine.AudioSource::set_volume(System.Single)
		void Register_UnityEngine_AudioSource_set_volume();
		Register_UnityEngine_AudioSource_set_volume();

		//UnityEngine.AnimationCurve UnityEngine.AudioSource::GetCustomCurve(UnityEngine.AudioSourceCurveType)
		void Register_UnityEngine_AudioSource_GetCustomCurve();
		Register_UnityEngine_AudioSource_GetCustomCurve();

		//UnityEngine.Audio.AudioMixerGroup UnityEngine.AudioSource::get_outputAudioMixerGroup()
		void Register_UnityEngine_AudioSource_get_outputAudioMixerGroup();
		Register_UnityEngine_AudioSource_get_outputAudioMixerGroup();

		//UnityEngine.AudioClip UnityEngine.AudioSource::get_clip()
		void Register_UnityEngine_AudioSource_get_clip();
		Register_UnityEngine_AudioSource_get_clip();

		//UnityEngine.AudioRolloffMode UnityEngine.AudioSource::get_rolloffMode()
		void Register_UnityEngine_AudioSource_get_rolloffMode();
		Register_UnityEngine_AudioSource_get_rolloffMode();

		//UnityEngine.AudioVelocityUpdateMode UnityEngine.AudioSource::get_velocityUpdateMode()
		void Register_UnityEngine_AudioSource_get_velocityUpdateMode();
		Register_UnityEngine_AudioSource_get_velocityUpdateMode();

	//End Registrations for type : UnityEngine.AudioSource

	//Start Registrations for type : UnityEngine.Avatar

		//System.Boolean UnityEngine.Avatar::get_isHuman()
		void Register_UnityEngine_Avatar_get_isHuman();
		Register_UnityEngine_Avatar_get_isHuman();

		//System.Boolean UnityEngine.Avatar::get_isValid()
		void Register_UnityEngine_Avatar_get_isValid();
		Register_UnityEngine_Avatar_get_isValid();

	//End Registrations for type : UnityEngine.Avatar

	//Start Registrations for type : UnityEngine.AvatarBuilder

		//UnityEngine.Avatar UnityEngine.AvatarBuilder::BuildGenericAvatar(UnityEngine.GameObject,System.String)
		void Register_UnityEngine_AvatarBuilder_BuildGenericAvatar();
		Register_UnityEngine_AvatarBuilder_BuildGenericAvatar();

		//UnityEngine.Avatar UnityEngine.AvatarBuilder::INTERNAL_CALL_BuildHumanAvatarMono(UnityEngine.GameObject,UnityEngine.HumanDescription&)
		void Register_UnityEngine_AvatarBuilder_INTERNAL_CALL_BuildHumanAvatarMono();
		Register_UnityEngine_AvatarBuilder_INTERNAL_CALL_BuildHumanAvatarMono();

	//End Registrations for type : UnityEngine.AvatarBuilder

	//Start Registrations for type : UnityEngine.Behaviour

		//System.Boolean UnityEngine.Behaviour::get_enabled()
		void Register_UnityEngine_Behaviour_get_enabled();
		Register_UnityEngine_Behaviour_get_enabled();

		//System.Boolean UnityEngine.Behaviour::get_isActiveAndEnabled()
		void Register_UnityEngine_Behaviour_get_isActiveAndEnabled();
		Register_UnityEngine_Behaviour_get_isActiveAndEnabled();

		//System.Void UnityEngine.Behaviour::set_enabled(System.Boolean)
		void Register_UnityEngine_Behaviour_set_enabled();
		Register_UnityEngine_Behaviour_set_enabled();

	//End Registrations for type : UnityEngine.Behaviour

	//Start Registrations for type : UnityEngine.BitStream

		//System.Boolean UnityEngine.BitStream::get_isReading()
		void Register_UnityEngine_BitStream_get_isReading();
		Register_UnityEngine_BitStream_get_isReading();

		//System.Boolean UnityEngine.BitStream::get_isWriting()
		void Register_UnityEngine_BitStream_get_isWriting();
		Register_UnityEngine_BitStream_get_isWriting();

		//System.Void UnityEngine.BitStream::INTERNAL_CALL_Serializen(UnityEngine.BitStream,UnityEngine.NetworkViewID&)
		void Register_UnityEngine_BitStream_INTERNAL_CALL_Serializen();
		Register_UnityEngine_BitStream_INTERNAL_CALL_Serializen();

		//System.Void UnityEngine.BitStream::INTERNAL_CALL_Serializeq(UnityEngine.BitStream,UnityEngine.Quaternion&,System.Single)
		void Register_UnityEngine_BitStream_INTERNAL_CALL_Serializeq();
		Register_UnityEngine_BitStream_INTERNAL_CALL_Serializeq();

		//System.Void UnityEngine.BitStream::INTERNAL_CALL_Serializev(UnityEngine.BitStream,UnityEngine.Vector3&,System.Single)
		void Register_UnityEngine_BitStream_INTERNAL_CALL_Serializev();
		Register_UnityEngine_BitStream_INTERNAL_CALL_Serializev();

		//System.Void UnityEngine.BitStream::Serialize(System.String&)
		void Register_UnityEngine_BitStream_Serialize();
		Register_UnityEngine_BitStream_Serialize();

		//System.Void UnityEngine.BitStream::Serializeb(System.Int32&)
		void Register_UnityEngine_BitStream_Serializeb();
		Register_UnityEngine_BitStream_Serializeb();

		//System.Void UnityEngine.BitStream::Serializec(System.Char&)
		void Register_UnityEngine_BitStream_Serializec();
		Register_UnityEngine_BitStream_Serializec();

		//System.Void UnityEngine.BitStream::Serializef(System.Single&,System.Single)
		void Register_UnityEngine_BitStream_Serializef();
		Register_UnityEngine_BitStream_Serializef();

		//System.Void UnityEngine.BitStream::Serializei(System.Int32&)
		void Register_UnityEngine_BitStream_Serializei();
		Register_UnityEngine_BitStream_Serializei();

		//System.Void UnityEngine.BitStream::Serializes(System.Int16&)
		void Register_UnityEngine_BitStream_Serializes();
		Register_UnityEngine_BitStream_Serializes();

	//End Registrations for type : UnityEngine.BitStream

	//Start Registrations for type : UnityEngine.Bounds

		//System.Boolean UnityEngine.Bounds::INTERNAL_CALL_Internal_Contains(UnityEngine.Bounds&,UnityEngine.Vector3&)
		void Register_UnityEngine_Bounds_INTERNAL_CALL_Internal_Contains();
		Register_UnityEngine_Bounds_INTERNAL_CALL_Internal_Contains();

		//System.Boolean UnityEngine.Bounds::INTERNAL_CALL_Internal_IntersectRay(UnityEngine.Ray&,UnityEngine.Bounds&,System.Single&)
		void Register_UnityEngine_Bounds_INTERNAL_CALL_Internal_IntersectRay();
		Register_UnityEngine_Bounds_INTERNAL_CALL_Internal_IntersectRay();

		//System.Single UnityEngine.Bounds::INTERNAL_CALL_Internal_SqrDistance(UnityEngine.Bounds&,UnityEngine.Vector3&)
		void Register_UnityEngine_Bounds_INTERNAL_CALL_Internal_SqrDistance();
		Register_UnityEngine_Bounds_INTERNAL_CALL_Internal_SqrDistance();

		//System.Void UnityEngine.Bounds::INTERNAL_CALL_Internal_GetClosestPoint(UnityEngine.Bounds&,UnityEngine.Vector3&,UnityEngine.Vector3&)
		void Register_UnityEngine_Bounds_INTERNAL_CALL_Internal_GetClosestPoint();
		Register_UnityEngine_Bounds_INTERNAL_CALL_Internal_GetClosestPoint();

	//End Registrations for type : UnityEngine.Bounds

	//Start Registrations for type : UnityEngine.BoxCollider

		//System.Void UnityEngine.BoxCollider::INTERNAL_get_center(UnityEngine.Vector3&)
		void Register_UnityEngine_BoxCollider_INTERNAL_get_center();
		Register_UnityEngine_BoxCollider_INTERNAL_get_center();

		//System.Void UnityEngine.BoxCollider::INTERNAL_get_size(UnityEngine.Vector3&)
		void Register_UnityEngine_BoxCollider_INTERNAL_get_size();
		Register_UnityEngine_BoxCollider_INTERNAL_get_size();

		//System.Void UnityEngine.BoxCollider::INTERNAL_set_center(UnityEngine.Vector3&)
		void Register_UnityEngine_BoxCollider_INTERNAL_set_center();
		Register_UnityEngine_BoxCollider_INTERNAL_set_center();

		//System.Void UnityEngine.BoxCollider::INTERNAL_set_size(UnityEngine.Vector3&)
		void Register_UnityEngine_BoxCollider_INTERNAL_set_size();
		Register_UnityEngine_BoxCollider_INTERNAL_set_size();

	//End Registrations for type : UnityEngine.BoxCollider

	//Start Registrations for type : UnityEngine.BoxCollider2D

		//System.Void UnityEngine.BoxCollider2D::INTERNAL_get_size(UnityEngine.Vector2&)
		void Register_UnityEngine_BoxCollider2D_INTERNAL_get_size();
		Register_UnityEngine_BoxCollider2D_INTERNAL_get_size();

		//System.Void UnityEngine.BoxCollider2D::INTERNAL_set_size(UnityEngine.Vector2&)
		void Register_UnityEngine_BoxCollider2D_INTERNAL_set_size();
		Register_UnityEngine_BoxCollider2D_INTERNAL_set_size();

	//End Registrations for type : UnityEngine.BoxCollider2D

	//Start Registrations for type : UnityEngine.Caching

		//System.Boolean UnityEngine.Caching::Authorize(System.String,System.String,System.Int64,System.Int32,System.String)
		void Register_UnityEngine_Caching_Authorize();
		Register_UnityEngine_Caching_Authorize();

		//System.Boolean UnityEngine.Caching::CleanCache()
		void Register_UnityEngine_Caching_CleanCache();
		Register_UnityEngine_Caching_CleanCache();

		//System.Boolean UnityEngine.Caching::INTERNAL_CALL_IsVersionCached(System.String,UnityEngine.Hash128&)
		void Register_UnityEngine_Caching_INTERNAL_CALL_IsVersionCached();
		Register_UnityEngine_Caching_INTERNAL_CALL_IsVersionCached();

		//System.Boolean UnityEngine.Caching::INTERNAL_CALL_MarkAsUsed(System.String,UnityEngine.Hash128&)
		void Register_UnityEngine_Caching_INTERNAL_CALL_MarkAsUsed();
		Register_UnityEngine_Caching_INTERNAL_CALL_MarkAsUsed();

		//System.Boolean UnityEngine.Caching::get_compressionEnabled()
		void Register_UnityEngine_Caching_get_compressionEnabled();
		Register_UnityEngine_Caching_get_compressionEnabled();

		//System.Boolean UnityEngine.Caching::get_enabled()
		void Register_UnityEngine_Caching_get_enabled();
		Register_UnityEngine_Caching_get_enabled();

		//System.Boolean UnityEngine.Caching::get_ready()
		void Register_UnityEngine_Caching_get_ready();
		Register_UnityEngine_Caching_get_ready();

		//System.Int32 UnityEngine.Caching::get_expirationDelay()
		void Register_UnityEngine_Caching_get_expirationDelay();
		Register_UnityEngine_Caching_get_expirationDelay();

		//System.Int64 UnityEngine.Caching::get_maximumAvailableDiskSpace()
		void Register_UnityEngine_Caching_get_maximumAvailableDiskSpace();
		Register_UnityEngine_Caching_get_maximumAvailableDiskSpace();

		//System.Int64 UnityEngine.Caching::get_spaceFree()
		void Register_UnityEngine_Caching_get_spaceFree();
		Register_UnityEngine_Caching_get_spaceFree();

		//System.Int64 UnityEngine.Caching::get_spaceOccupied()
		void Register_UnityEngine_Caching_get_spaceOccupied();
		Register_UnityEngine_Caching_get_spaceOccupied();

		//System.Void UnityEngine.Caching::set_compressionEnabled(System.Boolean)
		void Register_UnityEngine_Caching_set_compressionEnabled();
		Register_UnityEngine_Caching_set_compressionEnabled();

		//System.Void UnityEngine.Caching::set_enabled(System.Boolean)
		void Register_UnityEngine_Caching_set_enabled();
		Register_UnityEngine_Caching_set_enabled();

		//System.Void UnityEngine.Caching::set_expirationDelay(System.Int32)
		void Register_UnityEngine_Caching_set_expirationDelay();
		Register_UnityEngine_Caching_set_expirationDelay();

		//System.Void UnityEngine.Caching::set_maximumAvailableDiskSpace(System.Int64)
		void Register_UnityEngine_Caching_set_maximumAvailableDiskSpace();
		Register_UnityEngine_Caching_set_maximumAvailableDiskSpace();

	//End Registrations for type : UnityEngine.Caching

	//Start Registrations for type : UnityEngine.Camera

		//System.Boolean UnityEngine.Camera::Internal_RenderToCubemapRT(UnityEngine.RenderTexture,System.Int32)
		void Register_UnityEngine_Camera_Internal_RenderToCubemapRT();
		Register_UnityEngine_Camera_Internal_RenderToCubemapRT();

		//System.Boolean UnityEngine.Camera::Internal_RenderToCubemapTexture(UnityEngine.Cubemap,System.Int32)
		void Register_UnityEngine_Camera_Internal_RenderToCubemapTexture();
		Register_UnityEngine_Camera_Internal_RenderToCubemapTexture();

		//System.Boolean UnityEngine.Camera::get_clearStencilAfterLightingPass()
		void Register_UnityEngine_Camera_get_clearStencilAfterLightingPass();
		Register_UnityEngine_Camera_get_clearStencilAfterLightingPass();

		//System.Boolean UnityEngine.Camera::get_hdr()
		void Register_UnityEngine_Camera_get_hdr();
		Register_UnityEngine_Camera_get_hdr();

		//System.Boolean UnityEngine.Camera::get_layerCullSpherical()
		void Register_UnityEngine_Camera_get_layerCullSpherical();
		Register_UnityEngine_Camera_get_layerCullSpherical();

		//System.Boolean UnityEngine.Camera::get_orthographic()
		void Register_UnityEngine_Camera_get_orthographic();
		Register_UnityEngine_Camera_get_orthographic();

		//System.Boolean UnityEngine.Camera::get_stereoEnabled()
		void Register_UnityEngine_Camera_get_stereoEnabled();
		Register_UnityEngine_Camera_get_stereoEnabled();

		//System.Boolean UnityEngine.Camera::get_stereoMirrorMode()
		void Register_UnityEngine_Camera_get_stereoMirrorMode();
		Register_UnityEngine_Camera_get_stereoMirrorMode();

		//System.Boolean UnityEngine.Camera::get_useOcclusionCulling()
		void Register_UnityEngine_Camera_get_useOcclusionCulling();
		Register_UnityEngine_Camera_get_useOcclusionCulling();

		//System.Int32 UnityEngine.Camera::GetAllCameras(UnityEngine.Camera[])
		void Register_UnityEngine_Camera_GetAllCameras();
		Register_UnityEngine_Camera_GetAllCameras();

		//System.Int32 UnityEngine.Camera::get_allCamerasCount()
		void Register_UnityEngine_Camera_get_allCamerasCount();
		Register_UnityEngine_Camera_get_allCamerasCount();

		//System.Int32 UnityEngine.Camera::get_commandBufferCount()
		void Register_UnityEngine_Camera_get_commandBufferCount();
		Register_UnityEngine_Camera_get_commandBufferCount();

		//System.Int32 UnityEngine.Camera::get_cullingMask()
		void Register_UnityEngine_Camera_get_cullingMask();
		Register_UnityEngine_Camera_get_cullingMask();

		//System.Int32 UnityEngine.Camera::get_eventMask()
		void Register_UnityEngine_Camera_get_eventMask();
		Register_UnityEngine_Camera_get_eventMask();

		//System.Int32 UnityEngine.Camera::get_pixelHeight()
		void Register_UnityEngine_Camera_get_pixelHeight();
		Register_UnityEngine_Camera_get_pixelHeight();

		//System.Int32 UnityEngine.Camera::get_pixelWidth()
		void Register_UnityEngine_Camera_get_pixelWidth();
		Register_UnityEngine_Camera_get_pixelWidth();

		//System.Int32 UnityEngine.Camera::get_targetDisplay()
		void Register_UnityEngine_Camera_get_targetDisplay();
		Register_UnityEngine_Camera_get_targetDisplay();

		//System.Single UnityEngine.Camera::get_aspect()
		void Register_UnityEngine_Camera_get_aspect();
		Register_UnityEngine_Camera_get_aspect();

		//System.Single UnityEngine.Camera::get_depth()
		void Register_UnityEngine_Camera_get_depth();
		Register_UnityEngine_Camera_get_depth();

		//System.Single UnityEngine.Camera::get_farClipPlane()
		void Register_UnityEngine_Camera_get_farClipPlane();
		Register_UnityEngine_Camera_get_farClipPlane();

		//System.Single UnityEngine.Camera::get_fieldOfView()
		void Register_UnityEngine_Camera_get_fieldOfView();
		Register_UnityEngine_Camera_get_fieldOfView();

		//System.Single UnityEngine.Camera::get_nearClipPlane()
		void Register_UnityEngine_Camera_get_nearClipPlane();
		Register_UnityEngine_Camera_get_nearClipPlane();

		//System.Single UnityEngine.Camera::get_orthographicSize()
		void Register_UnityEngine_Camera_get_orthographicSize();
		Register_UnityEngine_Camera_get_orthographicSize();

		//System.Single UnityEngine.Camera::get_stereoConvergence()
		void Register_UnityEngine_Camera_get_stereoConvergence();
		Register_UnityEngine_Camera_get_stereoConvergence();

		//System.Single UnityEngine.Camera::get_stereoSeparation()
		void Register_UnityEngine_Camera_get_stereoSeparation();
		Register_UnityEngine_Camera_get_stereoSeparation();

		//System.Single[] UnityEngine.Camera::get_layerCullDistances()
		void Register_UnityEngine_Camera_get_layerCullDistances();
		Register_UnityEngine_Camera_get_layerCullDistances();

		//System.Void UnityEngine.Camera::AddCommandBuffer(UnityEngine.Rendering.CameraEvent,UnityEngine.Rendering.CommandBuffer)
		void Register_UnityEngine_Camera_AddCommandBuffer();
		Register_UnityEngine_Camera_AddCommandBuffer();

		//System.Void UnityEngine.Camera::CopyFrom(UnityEngine.Camera)
		void Register_UnityEngine_Camera_CopyFrom();
		Register_UnityEngine_Camera_CopyFrom();

		//System.Void UnityEngine.Camera::INTERNAL_CALL_CalculateObliqueMatrix(UnityEngine.Camera,UnityEngine.Vector4&,UnityEngine.Matrix4x4&)
		void Register_UnityEngine_Camera_INTERNAL_CALL_CalculateObliqueMatrix();
		Register_UnityEngine_Camera_INTERNAL_CALL_CalculateObliqueMatrix();

		//System.Void UnityEngine.Camera::INTERNAL_CALL_ResetAspect(UnityEngine.Camera)
		void Register_UnityEngine_Camera_INTERNAL_CALL_ResetAspect();
		Register_UnityEngine_Camera_INTERNAL_CALL_ResetAspect();

		//System.Void UnityEngine.Camera::INTERNAL_CALL_ResetFieldOfView(UnityEngine.Camera)
		void Register_UnityEngine_Camera_INTERNAL_CALL_ResetFieldOfView();
		Register_UnityEngine_Camera_INTERNAL_CALL_ResetFieldOfView();

		//System.Void UnityEngine.Camera::INTERNAL_CALL_ResetProjectionMatrix(UnityEngine.Camera)
		void Register_UnityEngine_Camera_INTERNAL_CALL_ResetProjectionMatrix();
		Register_UnityEngine_Camera_INTERNAL_CALL_ResetProjectionMatrix();

		//System.Void UnityEngine.Camera::INTERNAL_CALL_ResetReplacementShader(UnityEngine.Camera)
		void Register_UnityEngine_Camera_INTERNAL_CALL_ResetReplacementShader();
		Register_UnityEngine_Camera_INTERNAL_CALL_ResetReplacementShader();

		//System.Void UnityEngine.Camera::INTERNAL_CALL_ResetWorldToCameraMatrix(UnityEngine.Camera)
		void Register_UnityEngine_Camera_INTERNAL_CALL_ResetWorldToCameraMatrix();
		Register_UnityEngine_Camera_INTERNAL_CALL_ResetWorldToCameraMatrix();

		//System.Void UnityEngine.Camera::INTERNAL_CALL_ScreenPointToRay(UnityEngine.Camera,UnityEngine.Vector3&,UnityEngine.Ray&)
		void Register_UnityEngine_Camera_INTERNAL_CALL_ScreenPointToRay();
		Register_UnityEngine_Camera_INTERNAL_CALL_ScreenPointToRay();

		//System.Void UnityEngine.Camera::INTERNAL_CALL_ScreenToViewportPoint(UnityEngine.Camera,UnityEngine.Vector3&,UnityEngine.Vector3&)
		void Register_UnityEngine_Camera_INTERNAL_CALL_ScreenToViewportPoint();
		Register_UnityEngine_Camera_INTERNAL_CALL_ScreenToViewportPoint();

		//System.Void UnityEngine.Camera::INTERNAL_CALL_ScreenToWorldPoint(UnityEngine.Camera,UnityEngine.Vector3&,UnityEngine.Vector3&)
		void Register_UnityEngine_Camera_INTERNAL_CALL_ScreenToWorldPoint();
		Register_UnityEngine_Camera_INTERNAL_CALL_ScreenToWorldPoint();

		//System.Void UnityEngine.Camera::INTERNAL_CALL_SetStereoProjectionMatrices(UnityEngine.Camera,UnityEngine.Matrix4x4&,UnityEngine.Matrix4x4&)
		void Register_UnityEngine_Camera_INTERNAL_CALL_SetStereoProjectionMatrices();
		Register_UnityEngine_Camera_INTERNAL_CALL_SetStereoProjectionMatrices();

		//System.Void UnityEngine.Camera::INTERNAL_CALL_SetStereoViewMatrices(UnityEngine.Camera,UnityEngine.Matrix4x4&,UnityEngine.Matrix4x4&)
		void Register_UnityEngine_Camera_INTERNAL_CALL_SetStereoViewMatrices();
		Register_UnityEngine_Camera_INTERNAL_CALL_SetStereoViewMatrices();

		//System.Void UnityEngine.Camera::INTERNAL_CALL_ViewportPointToRay(UnityEngine.Camera,UnityEngine.Vector3&,UnityEngine.Ray&)
		void Register_UnityEngine_Camera_INTERNAL_CALL_ViewportPointToRay();
		Register_UnityEngine_Camera_INTERNAL_CALL_ViewportPointToRay();

		//System.Void UnityEngine.Camera::INTERNAL_CALL_ViewportToScreenPoint(UnityEngine.Camera,UnityEngine.Vector3&,UnityEngine.Vector3&)
		void Register_UnityEngine_Camera_INTERNAL_CALL_ViewportToScreenPoint();
		Register_UnityEngine_Camera_INTERNAL_CALL_ViewportToScreenPoint();

		//System.Void UnityEngine.Camera::INTERNAL_CALL_ViewportToWorldPoint(UnityEngine.Camera,UnityEngine.Vector3&,UnityEngine.Vector3&)
		void Register_UnityEngine_Camera_INTERNAL_CALL_ViewportToWorldPoint();
		Register_UnityEngine_Camera_INTERNAL_CALL_ViewportToWorldPoint();

		//System.Void UnityEngine.Camera::INTERNAL_CALL_WorldToScreenPoint(UnityEngine.Camera,UnityEngine.Vector3&,UnityEngine.Vector3&)
		void Register_UnityEngine_Camera_INTERNAL_CALL_WorldToScreenPoint();
		Register_UnityEngine_Camera_INTERNAL_CALL_WorldToScreenPoint();

		//System.Void UnityEngine.Camera::INTERNAL_CALL_WorldToViewportPoint(UnityEngine.Camera,UnityEngine.Vector3&,UnityEngine.Vector3&)
		void Register_UnityEngine_Camera_INTERNAL_CALL_WorldToViewportPoint();
		Register_UnityEngine_Camera_INTERNAL_CALL_WorldToViewportPoint();

		//System.Void UnityEngine.Camera::INTERNAL_get_backgroundColor(UnityEngine.Color&)
		void Register_UnityEngine_Camera_INTERNAL_get_backgroundColor();
		Register_UnityEngine_Camera_INTERNAL_get_backgroundColor();

		//System.Void UnityEngine.Camera::INTERNAL_get_cameraToWorldMatrix(UnityEngine.Matrix4x4&)
		void Register_UnityEngine_Camera_INTERNAL_get_cameraToWorldMatrix();
		Register_UnityEngine_Camera_INTERNAL_get_cameraToWorldMatrix();

		//System.Void UnityEngine.Camera::INTERNAL_get_pixelRect(UnityEngine.Rect&)
		void Register_UnityEngine_Camera_INTERNAL_get_pixelRect();
		Register_UnityEngine_Camera_INTERNAL_get_pixelRect();

		//System.Void UnityEngine.Camera::INTERNAL_get_projectionMatrix(UnityEngine.Matrix4x4&)
		void Register_UnityEngine_Camera_INTERNAL_get_projectionMatrix();
		Register_UnityEngine_Camera_INTERNAL_get_projectionMatrix();

		//System.Void UnityEngine.Camera::INTERNAL_get_rect(UnityEngine.Rect&)
		void Register_UnityEngine_Camera_INTERNAL_get_rect();
		Register_UnityEngine_Camera_INTERNAL_get_rect();

		//System.Void UnityEngine.Camera::INTERNAL_get_velocity(UnityEngine.Vector3&)
		void Register_UnityEngine_Camera_INTERNAL_get_velocity();
		Register_UnityEngine_Camera_INTERNAL_get_velocity();

		//System.Void UnityEngine.Camera::INTERNAL_get_worldToCameraMatrix(UnityEngine.Matrix4x4&)
		void Register_UnityEngine_Camera_INTERNAL_get_worldToCameraMatrix();
		Register_UnityEngine_Camera_INTERNAL_get_worldToCameraMatrix();

		//System.Void UnityEngine.Camera::INTERNAL_set_backgroundColor(UnityEngine.Color&)
		void Register_UnityEngine_Camera_INTERNAL_set_backgroundColor();
		Register_UnityEngine_Camera_INTERNAL_set_backgroundColor();

		//System.Void UnityEngine.Camera::INTERNAL_set_pixelRect(UnityEngine.Rect&)
		void Register_UnityEngine_Camera_INTERNAL_set_pixelRect();
		Register_UnityEngine_Camera_INTERNAL_set_pixelRect();

		//System.Void UnityEngine.Camera::INTERNAL_set_projectionMatrix(UnityEngine.Matrix4x4&)
		void Register_UnityEngine_Camera_INTERNAL_set_projectionMatrix();
		Register_UnityEngine_Camera_INTERNAL_set_projectionMatrix();

		//System.Void UnityEngine.Camera::INTERNAL_set_rect(UnityEngine.Rect&)
		void Register_UnityEngine_Camera_INTERNAL_set_rect();
		Register_UnityEngine_Camera_INTERNAL_set_rect();

		//System.Void UnityEngine.Camera::INTERNAL_set_worldToCameraMatrix(UnityEngine.Matrix4x4&)
		void Register_UnityEngine_Camera_INTERNAL_set_worldToCameraMatrix();
		Register_UnityEngine_Camera_INTERNAL_set_worldToCameraMatrix();

		//System.Void UnityEngine.Camera::RemoveAllCommandBuffers()
		void Register_UnityEngine_Camera_RemoveAllCommandBuffers();
		Register_UnityEngine_Camera_RemoveAllCommandBuffers();

		//System.Void UnityEngine.Camera::RemoveCommandBuffer(UnityEngine.Rendering.CameraEvent,UnityEngine.Rendering.CommandBuffer)
		void Register_UnityEngine_Camera_RemoveCommandBuffer();
		Register_UnityEngine_Camera_RemoveCommandBuffer();

		//System.Void UnityEngine.Camera::RemoveCommandBuffers(UnityEngine.Rendering.CameraEvent)
		void Register_UnityEngine_Camera_RemoveCommandBuffers();
		Register_UnityEngine_Camera_RemoveCommandBuffers();

		//System.Void UnityEngine.Camera::Render()
		void Register_UnityEngine_Camera_Render();
		Register_UnityEngine_Camera_Render();

		//System.Void UnityEngine.Camera::RenderDontRestore()
		void Register_UnityEngine_Camera_RenderDontRestore();
		Register_UnityEngine_Camera_RenderDontRestore();

		//System.Void UnityEngine.Camera::RenderWithShader(UnityEngine.Shader,System.String)
		void Register_UnityEngine_Camera_RenderWithShader();
		Register_UnityEngine_Camera_RenderWithShader();

		//System.Void UnityEngine.Camera::ResetStereoProjectionMatrices()
		void Register_UnityEngine_Camera_ResetStereoProjectionMatrices();
		Register_UnityEngine_Camera_ResetStereoProjectionMatrices();

		//System.Void UnityEngine.Camera::ResetStereoViewMatrices()
		void Register_UnityEngine_Camera_ResetStereoViewMatrices();
		Register_UnityEngine_Camera_ResetStereoViewMatrices();

		//System.Void UnityEngine.Camera::SetReplacementShader(UnityEngine.Shader,System.String)
		void Register_UnityEngine_Camera_SetReplacementShader();
		Register_UnityEngine_Camera_SetReplacementShader();

		//System.Void UnityEngine.Camera::SetTargetBuffersImpl(UnityEngine.RenderBuffer&,UnityEngine.RenderBuffer&)
		void Register_UnityEngine_Camera_SetTargetBuffersImpl();
		Register_UnityEngine_Camera_SetTargetBuffersImpl();

		//System.Void UnityEngine.Camera::SetTargetBuffersMRTImpl(UnityEngine.RenderBuffer[],UnityEngine.RenderBuffer&)
		void Register_UnityEngine_Camera_SetTargetBuffersMRTImpl();
		Register_UnityEngine_Camera_SetTargetBuffersMRTImpl();

		//System.Void UnityEngine.Camera::SetupCurrent(UnityEngine.Camera)
		void Register_UnityEngine_Camera_SetupCurrent();
		Register_UnityEngine_Camera_SetupCurrent();

		//System.Void UnityEngine.Camera::set_aspect(System.Single)
		void Register_UnityEngine_Camera_set_aspect();
		Register_UnityEngine_Camera_set_aspect();

		//System.Void UnityEngine.Camera::set_cameraType(UnityEngine.CameraType)
		void Register_UnityEngine_Camera_set_cameraType();
		Register_UnityEngine_Camera_set_cameraType();

		//System.Void UnityEngine.Camera::set_clearFlags(UnityEngine.CameraClearFlags)
		void Register_UnityEngine_Camera_set_clearFlags();
		Register_UnityEngine_Camera_set_clearFlags();

		//System.Void UnityEngine.Camera::set_clearStencilAfterLightingPass(System.Boolean)
		void Register_UnityEngine_Camera_set_clearStencilAfterLightingPass();
		Register_UnityEngine_Camera_set_clearStencilAfterLightingPass();

		//System.Void UnityEngine.Camera::set_cullingMask(System.Int32)
		void Register_UnityEngine_Camera_set_cullingMask();
		Register_UnityEngine_Camera_set_cullingMask();

		//System.Void UnityEngine.Camera::set_depth(System.Single)
		void Register_UnityEngine_Camera_set_depth();
		Register_UnityEngine_Camera_set_depth();

		//System.Void UnityEngine.Camera::set_depthTextureMode(UnityEngine.DepthTextureMode)
		void Register_UnityEngine_Camera_set_depthTextureMode();
		Register_UnityEngine_Camera_set_depthTextureMode();

		//System.Void UnityEngine.Camera::set_eventMask(System.Int32)
		void Register_UnityEngine_Camera_set_eventMask();
		Register_UnityEngine_Camera_set_eventMask();

		//System.Void UnityEngine.Camera::set_farClipPlane(System.Single)
		void Register_UnityEngine_Camera_set_farClipPlane();
		Register_UnityEngine_Camera_set_farClipPlane();

		//System.Void UnityEngine.Camera::set_fieldOfView(System.Single)
		void Register_UnityEngine_Camera_set_fieldOfView();
		Register_UnityEngine_Camera_set_fieldOfView();

		//System.Void UnityEngine.Camera::set_hdr(System.Boolean)
		void Register_UnityEngine_Camera_set_hdr();
		Register_UnityEngine_Camera_set_hdr();

		//System.Void UnityEngine.Camera::set_layerCullDistances(System.Single[])
		void Register_UnityEngine_Camera_set_layerCullDistances();
		Register_UnityEngine_Camera_set_layerCullDistances();

		//System.Void UnityEngine.Camera::set_layerCullSpherical(System.Boolean)
		void Register_UnityEngine_Camera_set_layerCullSpherical();
		Register_UnityEngine_Camera_set_layerCullSpherical();

		//System.Void UnityEngine.Camera::set_nearClipPlane(System.Single)
		void Register_UnityEngine_Camera_set_nearClipPlane();
		Register_UnityEngine_Camera_set_nearClipPlane();

		//System.Void UnityEngine.Camera::set_opaqueSortMode(UnityEngine.Rendering.OpaqueSortMode)
		void Register_UnityEngine_Camera_set_opaqueSortMode();
		Register_UnityEngine_Camera_set_opaqueSortMode();

		//System.Void UnityEngine.Camera::set_orthographic(System.Boolean)
		void Register_UnityEngine_Camera_set_orthographic();
		Register_UnityEngine_Camera_set_orthographic();

		//System.Void UnityEngine.Camera::set_orthographicSize(System.Single)
		void Register_UnityEngine_Camera_set_orthographicSize();
		Register_UnityEngine_Camera_set_orthographicSize();

		//System.Void UnityEngine.Camera::set_renderingPath(UnityEngine.RenderingPath)
		void Register_UnityEngine_Camera_set_renderingPath();
		Register_UnityEngine_Camera_set_renderingPath();

		//System.Void UnityEngine.Camera::set_stereoConvergence(System.Single)
		void Register_UnityEngine_Camera_set_stereoConvergence();
		Register_UnityEngine_Camera_set_stereoConvergence();

		//System.Void UnityEngine.Camera::set_stereoMirrorMode(System.Boolean)
		void Register_UnityEngine_Camera_set_stereoMirrorMode();
		Register_UnityEngine_Camera_set_stereoMirrorMode();

		//System.Void UnityEngine.Camera::set_stereoSeparation(System.Single)
		void Register_UnityEngine_Camera_set_stereoSeparation();
		Register_UnityEngine_Camera_set_stereoSeparation();

		//System.Void UnityEngine.Camera::set_targetDisplay(System.Int32)
		void Register_UnityEngine_Camera_set_targetDisplay();
		Register_UnityEngine_Camera_set_targetDisplay();

		//System.Void UnityEngine.Camera::set_targetTexture(UnityEngine.RenderTexture)
		void Register_UnityEngine_Camera_set_targetTexture();
		Register_UnityEngine_Camera_set_targetTexture();

		//System.Void UnityEngine.Camera::set_transparencySortMode(UnityEngine.TransparencySortMode)
		void Register_UnityEngine_Camera_set_transparencySortMode();
		Register_UnityEngine_Camera_set_transparencySortMode();

		//System.Void UnityEngine.Camera::set_useOcclusionCulling(System.Boolean)
		void Register_UnityEngine_Camera_set_useOcclusionCulling();
		Register_UnityEngine_Camera_set_useOcclusionCulling();

		//UnityEngine.Camera UnityEngine.Camera::get_current()
		void Register_UnityEngine_Camera_get_current();
		Register_UnityEngine_Camera_get_current();

		//UnityEngine.Camera UnityEngine.Camera::get_main()
		void Register_UnityEngine_Camera_get_main();
		Register_UnityEngine_Camera_get_main();

		//UnityEngine.CameraClearFlags UnityEngine.Camera::get_clearFlags()
		void Register_UnityEngine_Camera_get_clearFlags();
		Register_UnityEngine_Camera_get_clearFlags();

		//UnityEngine.CameraType UnityEngine.Camera::get_cameraType()
		void Register_UnityEngine_Camera_get_cameraType();
		Register_UnityEngine_Camera_get_cameraType();

		//UnityEngine.Camera[] UnityEngine.Camera::get_allCameras()
		void Register_UnityEngine_Camera_get_allCameras();
		Register_UnityEngine_Camera_get_allCameras();

		//UnityEngine.DepthTextureMode UnityEngine.Camera::get_depthTextureMode()
		void Register_UnityEngine_Camera_get_depthTextureMode();
		Register_UnityEngine_Camera_get_depthTextureMode();

		//UnityEngine.GameObject UnityEngine.Camera::INTERNAL_CALL_RaycastTry(UnityEngine.Camera,UnityEngine.Ray&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
		void Register_UnityEngine_Camera_INTERNAL_CALL_RaycastTry();
		Register_UnityEngine_Camera_INTERNAL_CALL_RaycastTry();

		//UnityEngine.GameObject UnityEngine.Camera::INTERNAL_CALL_RaycastTry2D(UnityEngine.Camera,UnityEngine.Ray&,System.Single,System.Int32)
		void Register_UnityEngine_Camera_INTERNAL_CALL_RaycastTry2D();
		Register_UnityEngine_Camera_INTERNAL_CALL_RaycastTry2D();

		//UnityEngine.RenderTexture UnityEngine.Camera::get_targetTexture()
		void Register_UnityEngine_Camera_get_targetTexture();
		Register_UnityEngine_Camera_get_targetTexture();

		//UnityEngine.Rendering.CommandBuffer[] UnityEngine.Camera::GetCommandBuffers(UnityEngine.Rendering.CameraEvent)
		void Register_UnityEngine_Camera_GetCommandBuffers();
		Register_UnityEngine_Camera_GetCommandBuffers();

		//UnityEngine.Rendering.OpaqueSortMode UnityEngine.Camera::get_opaqueSortMode()
		void Register_UnityEngine_Camera_get_opaqueSortMode();
		Register_UnityEngine_Camera_get_opaqueSortMode();

		//UnityEngine.RenderingPath UnityEngine.Camera::get_actualRenderingPath()
		void Register_UnityEngine_Camera_get_actualRenderingPath();
		Register_UnityEngine_Camera_get_actualRenderingPath();

		//UnityEngine.RenderingPath UnityEngine.Camera::get_renderingPath()
		void Register_UnityEngine_Camera_get_renderingPath();
		Register_UnityEngine_Camera_get_renderingPath();

		//UnityEngine.TransparencySortMode UnityEngine.Camera::get_transparencySortMode()
		void Register_UnityEngine_Camera_get_transparencySortMode();
		Register_UnityEngine_Camera_get_transparencySortMode();

	//End Registrations for type : UnityEngine.Camera

	//Start Registrations for type : UnityEngine.Canvas

		//System.Boolean UnityEngine.Canvas::get_isRootCanvas()
		void Register_UnityEngine_Canvas_get_isRootCanvas();
		Register_UnityEngine_Canvas_get_isRootCanvas();

		//System.Boolean UnityEngine.Canvas::get_overridePixelPerfect()
		void Register_UnityEngine_Canvas_get_overridePixelPerfect();
		Register_UnityEngine_Canvas_get_overridePixelPerfect();

		//System.Boolean UnityEngine.Canvas::get_overrideSorting()
		void Register_UnityEngine_Canvas_get_overrideSorting();
		Register_UnityEngine_Canvas_get_overrideSorting();

		//System.Boolean UnityEngine.Canvas::get_pixelPerfect()
		void Register_UnityEngine_Canvas_get_pixelPerfect();
		Register_UnityEngine_Canvas_get_pixelPerfect();

		//System.Int32 UnityEngine.Canvas::get_cachedSortingLayerValue()
		void Register_UnityEngine_Canvas_get_cachedSortingLayerValue();
		Register_UnityEngine_Canvas_get_cachedSortingLayerValue();

		//System.Int32 UnityEngine.Canvas::get_renderOrder()
		void Register_UnityEngine_Canvas_get_renderOrder();
		Register_UnityEngine_Canvas_get_renderOrder();

		//System.Int32 UnityEngine.Canvas::get_sortingGridNormalizedSize()
		void Register_UnityEngine_Canvas_get_sortingGridNormalizedSize();
		Register_UnityEngine_Canvas_get_sortingGridNormalizedSize();

		//System.Int32 UnityEngine.Canvas::get_sortingLayerID()
		void Register_UnityEngine_Canvas_get_sortingLayerID();
		Register_UnityEngine_Canvas_get_sortingLayerID();

		//System.Int32 UnityEngine.Canvas::get_sortingOrder()
		void Register_UnityEngine_Canvas_get_sortingOrder();
		Register_UnityEngine_Canvas_get_sortingOrder();

		//System.Int32 UnityEngine.Canvas::get_targetDisplay()
		void Register_UnityEngine_Canvas_get_targetDisplay();
		Register_UnityEngine_Canvas_get_targetDisplay();

		//System.Single UnityEngine.Canvas::get_planeDistance()
		void Register_UnityEngine_Canvas_get_planeDistance();
		Register_UnityEngine_Canvas_get_planeDistance();

		//System.Single UnityEngine.Canvas::get_referencePixelsPerUnit()
		void Register_UnityEngine_Canvas_get_referencePixelsPerUnit();
		Register_UnityEngine_Canvas_get_referencePixelsPerUnit();

		//System.Single UnityEngine.Canvas::get_scaleFactor()
		void Register_UnityEngine_Canvas_get_scaleFactor();
		Register_UnityEngine_Canvas_get_scaleFactor();

		//System.String UnityEngine.Canvas::get_sortingLayerName()
		void Register_UnityEngine_Canvas_get_sortingLayerName();
		Register_UnityEngine_Canvas_get_sortingLayerName();

		//System.Void UnityEngine.Canvas::INTERNAL_get_pixelRect(UnityEngine.Rect&)
		void Register_UnityEngine_Canvas_INTERNAL_get_pixelRect();
		Register_UnityEngine_Canvas_INTERNAL_get_pixelRect();

		//System.Void UnityEngine.Canvas::set_overridePixelPerfect(System.Boolean)
		void Register_UnityEngine_Canvas_set_overridePixelPerfect();
		Register_UnityEngine_Canvas_set_overridePixelPerfect();

		//System.Void UnityEngine.Canvas::set_overrideSorting(System.Boolean)
		void Register_UnityEngine_Canvas_set_overrideSorting();
		Register_UnityEngine_Canvas_set_overrideSorting();

		//System.Void UnityEngine.Canvas::set_pixelPerfect(System.Boolean)
		void Register_UnityEngine_Canvas_set_pixelPerfect();
		Register_UnityEngine_Canvas_set_pixelPerfect();

		//System.Void UnityEngine.Canvas::set_planeDistance(System.Single)
		void Register_UnityEngine_Canvas_set_planeDistance();
		Register_UnityEngine_Canvas_set_planeDistance();

		//System.Void UnityEngine.Canvas::set_referencePixelsPerUnit(System.Single)
		void Register_UnityEngine_Canvas_set_referencePixelsPerUnit();
		Register_UnityEngine_Canvas_set_referencePixelsPerUnit();

		//System.Void UnityEngine.Canvas::set_renderMode(UnityEngine.RenderMode)
		void Register_UnityEngine_Canvas_set_renderMode();
		Register_UnityEngine_Canvas_set_renderMode();

		//System.Void UnityEngine.Canvas::set_scaleFactor(System.Single)
		void Register_UnityEngine_Canvas_set_scaleFactor();
		Register_UnityEngine_Canvas_set_scaleFactor();

		//System.Void UnityEngine.Canvas::set_sortingGridNormalizedSize(System.Int32)
		void Register_UnityEngine_Canvas_set_sortingGridNormalizedSize();
		Register_UnityEngine_Canvas_set_sortingGridNormalizedSize();

		//System.Void UnityEngine.Canvas::set_sortingLayerID(System.Int32)
		void Register_UnityEngine_Canvas_set_sortingLayerID();
		Register_UnityEngine_Canvas_set_sortingLayerID();

		//System.Void UnityEngine.Canvas::set_sortingLayerName(System.String)
		void Register_UnityEngine_Canvas_set_sortingLayerName();
		Register_UnityEngine_Canvas_set_sortingLayerName();

		//System.Void UnityEngine.Canvas::set_sortingOrder(System.Int32)
		void Register_UnityEngine_Canvas_set_sortingOrder();
		Register_UnityEngine_Canvas_set_sortingOrder();

		//System.Void UnityEngine.Canvas::set_targetDisplay(System.Int32)
		void Register_UnityEngine_Canvas_set_targetDisplay();
		Register_UnityEngine_Canvas_set_targetDisplay();

		//System.Void UnityEngine.Canvas::set_worldCamera(UnityEngine.Camera)
		void Register_UnityEngine_Canvas_set_worldCamera();
		Register_UnityEngine_Canvas_set_worldCamera();

		//UnityEngine.Camera UnityEngine.Canvas::get_worldCamera()
		void Register_UnityEngine_Canvas_get_worldCamera();
		Register_UnityEngine_Canvas_get_worldCamera();

		//UnityEngine.Canvas UnityEngine.Canvas::get_rootCanvas()
		void Register_UnityEngine_Canvas_get_rootCanvas();
		Register_UnityEngine_Canvas_get_rootCanvas();

		//UnityEngine.Material UnityEngine.Canvas::GetDefaultCanvasMaterial()
		void Register_UnityEngine_Canvas_GetDefaultCanvasMaterial();
		Register_UnityEngine_Canvas_GetDefaultCanvasMaterial();

		//UnityEngine.RenderMode UnityEngine.Canvas::get_renderMode()
		void Register_UnityEngine_Canvas_get_renderMode();
		Register_UnityEngine_Canvas_get_renderMode();

	//End Registrations for type : UnityEngine.Canvas

	//Start Registrations for type : UnityEngine.CanvasGroup

		//System.Boolean UnityEngine.CanvasGroup::get_blocksRaycasts()
		void Register_UnityEngine_CanvasGroup_get_blocksRaycasts();
		Register_UnityEngine_CanvasGroup_get_blocksRaycasts();

		//System.Boolean UnityEngine.CanvasGroup::get_ignoreParentGroups()
		void Register_UnityEngine_CanvasGroup_get_ignoreParentGroups();
		Register_UnityEngine_CanvasGroup_get_ignoreParentGroups();

		//System.Boolean UnityEngine.CanvasGroup::get_interactable()
		void Register_UnityEngine_CanvasGroup_get_interactable();
		Register_UnityEngine_CanvasGroup_get_interactable();

		//System.Single UnityEngine.CanvasGroup::get_alpha()
		void Register_UnityEngine_CanvasGroup_get_alpha();
		Register_UnityEngine_CanvasGroup_get_alpha();

		//System.Void UnityEngine.CanvasGroup::set_alpha(System.Single)
		void Register_UnityEngine_CanvasGroup_set_alpha();
		Register_UnityEngine_CanvasGroup_set_alpha();

		//System.Void UnityEngine.CanvasGroup::set_blocksRaycasts(System.Boolean)
		void Register_UnityEngine_CanvasGroup_set_blocksRaycasts();
		Register_UnityEngine_CanvasGroup_set_blocksRaycasts();

		//System.Void UnityEngine.CanvasGroup::set_ignoreParentGroups(System.Boolean)
		void Register_UnityEngine_CanvasGroup_set_ignoreParentGroups();
		Register_UnityEngine_CanvasGroup_set_ignoreParentGroups();

		//System.Void UnityEngine.CanvasGroup::set_interactable(System.Boolean)
		void Register_UnityEngine_CanvasGroup_set_interactable();
		Register_UnityEngine_CanvasGroup_set_interactable();

	//End Registrations for type : UnityEngine.CanvasGroup

	//Start Registrations for type : UnityEngine.CanvasRenderer

		//System.Boolean UnityEngine.CanvasRenderer::get_cull()
		void Register_UnityEngine_CanvasRenderer_get_cull();
		Register_UnityEngine_CanvasRenderer_get_cull();

		//System.Boolean UnityEngine.CanvasRenderer::get_hasMoved()
		void Register_UnityEngine_CanvasRenderer_get_hasMoved();
		Register_UnityEngine_CanvasRenderer_get_hasMoved();

		//System.Int32 UnityEngine.CanvasRenderer::get_absoluteDepth()
		void Register_UnityEngine_CanvasRenderer_get_absoluteDepth();
		Register_UnityEngine_CanvasRenderer_get_absoluteDepth();

		//System.Int32 UnityEngine.CanvasRenderer::get_materialCount()
		void Register_UnityEngine_CanvasRenderer_get_materialCount();
		Register_UnityEngine_CanvasRenderer_get_materialCount();

		//System.Void UnityEngine.CanvasRenderer::Clear()
		void Register_UnityEngine_CanvasRenderer_Clear();
		Register_UnityEngine_CanvasRenderer_Clear();

		//System.Void UnityEngine.CanvasRenderer::CreateUIVertexStreamInternal(System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object)
		void Register_UnityEngine_CanvasRenderer_CreateUIVertexStreamInternal();
		Register_UnityEngine_CanvasRenderer_CreateUIVertexStreamInternal();

		//System.Void UnityEngine.CanvasRenderer::DisableRectClipping()
		void Register_UnityEngine_CanvasRenderer_DisableRectClipping();
		Register_UnityEngine_CanvasRenderer_DisableRectClipping();

		//System.Void UnityEngine.CanvasRenderer::INTERNAL_CALL_EnableRectClipping(UnityEngine.CanvasRenderer,UnityEngine.Rect&)
		void Register_UnityEngine_CanvasRenderer_INTERNAL_CALL_EnableRectClipping();
		Register_UnityEngine_CanvasRenderer_INTERNAL_CALL_EnableRectClipping();

		//System.Void UnityEngine.CanvasRenderer::INTERNAL_CALL_GetColor(UnityEngine.CanvasRenderer,UnityEngine.Color&)
		void Register_UnityEngine_CanvasRenderer_INTERNAL_CALL_GetColor();
		Register_UnityEngine_CanvasRenderer_INTERNAL_CALL_GetColor();

		//System.Void UnityEngine.CanvasRenderer::INTERNAL_CALL_SetColor(UnityEngine.CanvasRenderer,UnityEngine.Color&)
		void Register_UnityEngine_CanvasRenderer_INTERNAL_CALL_SetColor();
		Register_UnityEngine_CanvasRenderer_INTERNAL_CALL_SetColor();

		//System.Void UnityEngine.CanvasRenderer::SetMaterial(UnityEngine.Material,System.Int32)
		void Register_UnityEngine_CanvasRenderer_SetMaterial();
		Register_UnityEngine_CanvasRenderer_SetMaterial();

		//System.Void UnityEngine.CanvasRenderer::SetMesh(UnityEngine.Mesh)
		void Register_UnityEngine_CanvasRenderer_SetMesh();
		Register_UnityEngine_CanvasRenderer_SetMesh();

		//System.Void UnityEngine.CanvasRenderer::SetPopMaterial(UnityEngine.Material,System.Int32)
		void Register_UnityEngine_CanvasRenderer_SetPopMaterial();
		Register_UnityEngine_CanvasRenderer_SetPopMaterial();

		//System.Void UnityEngine.CanvasRenderer::SetTexture(UnityEngine.Texture)
		void Register_UnityEngine_CanvasRenderer_SetTexture();
		Register_UnityEngine_CanvasRenderer_SetTexture();

		//System.Void UnityEngine.CanvasRenderer::SplitIndiciesStreamsInternal(System.Object,System.Object)
		void Register_UnityEngine_CanvasRenderer_SplitIndiciesStreamsInternal();
		Register_UnityEngine_CanvasRenderer_SplitIndiciesStreamsInternal();

		//System.Void UnityEngine.CanvasRenderer::SplitUIVertexStreamsInternal(System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object)
		void Register_UnityEngine_CanvasRenderer_SplitUIVertexStreamsInternal();
		Register_UnityEngine_CanvasRenderer_SplitUIVertexStreamsInternal();

		//System.Void UnityEngine.CanvasRenderer::set_cull(System.Boolean)
		void Register_UnityEngine_CanvasRenderer_set_cull();
		Register_UnityEngine_CanvasRenderer_set_cull();

		//System.Void UnityEngine.CanvasRenderer::set_hasPopInstruction(System.Boolean)
		void Register_UnityEngine_CanvasRenderer_set_hasPopInstruction();
		Register_UnityEngine_CanvasRenderer_set_hasPopInstruction();

		//System.Void UnityEngine.CanvasRenderer::set_materialCount(System.Int32)
		void Register_UnityEngine_CanvasRenderer_set_materialCount();
		Register_UnityEngine_CanvasRenderer_set_materialCount();

		//System.Void UnityEngine.CanvasRenderer::set_popMaterialCount(System.Int32)
		void Register_UnityEngine_CanvasRenderer_set_popMaterialCount();
		Register_UnityEngine_CanvasRenderer_set_popMaterialCount();

	//End Registrations for type : UnityEngine.CanvasRenderer

	//Start Registrations for type : UnityEngine.CapsuleCollider

		//System.Int32 UnityEngine.CapsuleCollider::get_direction()
		void Register_UnityEngine_CapsuleCollider_get_direction();
		Register_UnityEngine_CapsuleCollider_get_direction();

		//System.Single UnityEngine.CapsuleCollider::get_height()
		void Register_UnityEngine_CapsuleCollider_get_height();
		Register_UnityEngine_CapsuleCollider_get_height();

		//System.Single UnityEngine.CapsuleCollider::get_radius()
		void Register_UnityEngine_CapsuleCollider_get_radius();
		Register_UnityEngine_CapsuleCollider_get_radius();

		//System.Void UnityEngine.CapsuleCollider::INTERNAL_get_center(UnityEngine.Vector3&)
		void Register_UnityEngine_CapsuleCollider_INTERNAL_get_center();
		Register_UnityEngine_CapsuleCollider_INTERNAL_get_center();

		//System.Void UnityEngine.CapsuleCollider::INTERNAL_set_center(UnityEngine.Vector3&)
		void Register_UnityEngine_CapsuleCollider_INTERNAL_set_center();
		Register_UnityEngine_CapsuleCollider_INTERNAL_set_center();

		//System.Void UnityEngine.CapsuleCollider::set_direction(System.Int32)
		void Register_UnityEngine_CapsuleCollider_set_direction();
		Register_UnityEngine_CapsuleCollider_set_direction();

		//System.Void UnityEngine.CapsuleCollider::set_height(System.Single)
		void Register_UnityEngine_CapsuleCollider_set_height();
		Register_UnityEngine_CapsuleCollider_set_height();

		//System.Void UnityEngine.CapsuleCollider::set_radius(System.Single)
		void Register_UnityEngine_CapsuleCollider_set_radius();
		Register_UnityEngine_CapsuleCollider_set_radius();

	//End Registrations for type : UnityEngine.CapsuleCollider

	//Start Registrations for type : UnityEngine.CharacterController

		//System.Boolean UnityEngine.CharacterController::INTERNAL_CALL_SimpleMove(UnityEngine.CharacterController,UnityEngine.Vector3&)
		void Register_UnityEngine_CharacterController_INTERNAL_CALL_SimpleMove();
		Register_UnityEngine_CharacterController_INTERNAL_CALL_SimpleMove();

		//System.Boolean UnityEngine.CharacterController::get_detectCollisions()
		void Register_UnityEngine_CharacterController_get_detectCollisions();
		Register_UnityEngine_CharacterController_get_detectCollisions();

		//System.Boolean UnityEngine.CharacterController::get_isGrounded()
		void Register_UnityEngine_CharacterController_get_isGrounded();
		Register_UnityEngine_CharacterController_get_isGrounded();

		//System.Single UnityEngine.CharacterController::get_height()
		void Register_UnityEngine_CharacterController_get_height();
		Register_UnityEngine_CharacterController_get_height();

		//System.Single UnityEngine.CharacterController::get_radius()
		void Register_UnityEngine_CharacterController_get_radius();
		Register_UnityEngine_CharacterController_get_radius();

		//System.Single UnityEngine.CharacterController::get_skinWidth()
		void Register_UnityEngine_CharacterController_get_skinWidth();
		Register_UnityEngine_CharacterController_get_skinWidth();

		//System.Single UnityEngine.CharacterController::get_slopeLimit()
		void Register_UnityEngine_CharacterController_get_slopeLimit();
		Register_UnityEngine_CharacterController_get_slopeLimit();

		//System.Single UnityEngine.CharacterController::get_stepOffset()
		void Register_UnityEngine_CharacterController_get_stepOffset();
		Register_UnityEngine_CharacterController_get_stepOffset();

		//System.Void UnityEngine.CharacterController::INTERNAL_get_center(UnityEngine.Vector3&)
		void Register_UnityEngine_CharacterController_INTERNAL_get_center();
		Register_UnityEngine_CharacterController_INTERNAL_get_center();

		//System.Void UnityEngine.CharacterController::INTERNAL_get_velocity(UnityEngine.Vector3&)
		void Register_UnityEngine_CharacterController_INTERNAL_get_velocity();
		Register_UnityEngine_CharacterController_INTERNAL_get_velocity();

		//System.Void UnityEngine.CharacterController::INTERNAL_set_center(UnityEngine.Vector3&)
		void Register_UnityEngine_CharacterController_INTERNAL_set_center();
		Register_UnityEngine_CharacterController_INTERNAL_set_center();

		//System.Void UnityEngine.CharacterController::set_detectCollisions(System.Boolean)
		void Register_UnityEngine_CharacterController_set_detectCollisions();
		Register_UnityEngine_CharacterController_set_detectCollisions();

		//System.Void UnityEngine.CharacterController::set_height(System.Single)
		void Register_UnityEngine_CharacterController_set_height();
		Register_UnityEngine_CharacterController_set_height();

		//System.Void UnityEngine.CharacterController::set_radius(System.Single)
		void Register_UnityEngine_CharacterController_set_radius();
		Register_UnityEngine_CharacterController_set_radius();

		//System.Void UnityEngine.CharacterController::set_skinWidth(System.Single)
		void Register_UnityEngine_CharacterController_set_skinWidth();
		Register_UnityEngine_CharacterController_set_skinWidth();

		//System.Void UnityEngine.CharacterController::set_slopeLimit(System.Single)
		void Register_UnityEngine_CharacterController_set_slopeLimit();
		Register_UnityEngine_CharacterController_set_slopeLimit();

		//System.Void UnityEngine.CharacterController::set_stepOffset(System.Single)
		void Register_UnityEngine_CharacterController_set_stepOffset();
		Register_UnityEngine_CharacterController_set_stepOffset();

		//UnityEngine.CollisionFlags UnityEngine.CharacterController::INTERNAL_CALL_Move(UnityEngine.CharacterController,UnityEngine.Vector3&)
		void Register_UnityEngine_CharacterController_INTERNAL_CALL_Move();
		Register_UnityEngine_CharacterController_INTERNAL_CALL_Move();

		//UnityEngine.CollisionFlags UnityEngine.CharacterController::get_collisionFlags()
		void Register_UnityEngine_CharacterController_get_collisionFlags();
		Register_UnityEngine_CharacterController_get_collisionFlags();

	//End Registrations for type : UnityEngine.CharacterController

	//Start Registrations for type : UnityEngine.CharacterJoint

		//System.Boolean UnityEngine.CharacterJoint::get_enableProjection()
		void Register_UnityEngine_CharacterJoint_get_enableProjection();
		Register_UnityEngine_CharacterJoint_get_enableProjection();

		//System.Single UnityEngine.CharacterJoint::get_projectionAngle()
		void Register_UnityEngine_CharacterJoint_get_projectionAngle();
		Register_UnityEngine_CharacterJoint_get_projectionAngle();

		//System.Single UnityEngine.CharacterJoint::get_projectionDistance()
		void Register_UnityEngine_CharacterJoint_get_projectionDistance();
		Register_UnityEngine_CharacterJoint_get_projectionDistance();

		//System.Void UnityEngine.CharacterJoint::INTERNAL_get_highTwistLimit(UnityEngine.SoftJointLimit&)
		void Register_UnityEngine_CharacterJoint_INTERNAL_get_highTwistLimit();
		Register_UnityEngine_CharacterJoint_INTERNAL_get_highTwistLimit();

		//System.Void UnityEngine.CharacterJoint::INTERNAL_get_lowTwistLimit(UnityEngine.SoftJointLimit&)
		void Register_UnityEngine_CharacterJoint_INTERNAL_get_lowTwistLimit();
		Register_UnityEngine_CharacterJoint_INTERNAL_get_lowTwistLimit();

		//System.Void UnityEngine.CharacterJoint::INTERNAL_get_swing1Limit(UnityEngine.SoftJointLimit&)
		void Register_UnityEngine_CharacterJoint_INTERNAL_get_swing1Limit();
		Register_UnityEngine_CharacterJoint_INTERNAL_get_swing1Limit();

		//System.Void UnityEngine.CharacterJoint::INTERNAL_get_swing2Limit(UnityEngine.SoftJointLimit&)
		void Register_UnityEngine_CharacterJoint_INTERNAL_get_swing2Limit();
		Register_UnityEngine_CharacterJoint_INTERNAL_get_swing2Limit();

		//System.Void UnityEngine.CharacterJoint::INTERNAL_get_swingAxis(UnityEngine.Vector3&)
		void Register_UnityEngine_CharacterJoint_INTERNAL_get_swingAxis();
		Register_UnityEngine_CharacterJoint_INTERNAL_get_swingAxis();

		//System.Void UnityEngine.CharacterJoint::INTERNAL_get_swingLimitSpring(UnityEngine.SoftJointLimitSpring&)
		void Register_UnityEngine_CharacterJoint_INTERNAL_get_swingLimitSpring();
		Register_UnityEngine_CharacterJoint_INTERNAL_get_swingLimitSpring();

		//System.Void UnityEngine.CharacterJoint::INTERNAL_get_twistLimitSpring(UnityEngine.SoftJointLimitSpring&)
		void Register_UnityEngine_CharacterJoint_INTERNAL_get_twistLimitSpring();
		Register_UnityEngine_CharacterJoint_INTERNAL_get_twistLimitSpring();

		//System.Void UnityEngine.CharacterJoint::INTERNAL_set_highTwistLimit(UnityEngine.SoftJointLimit&)
		void Register_UnityEngine_CharacterJoint_INTERNAL_set_highTwistLimit();
		Register_UnityEngine_CharacterJoint_INTERNAL_set_highTwistLimit();

		//System.Void UnityEngine.CharacterJoint::INTERNAL_set_lowTwistLimit(UnityEngine.SoftJointLimit&)
		void Register_UnityEngine_CharacterJoint_INTERNAL_set_lowTwistLimit();
		Register_UnityEngine_CharacterJoint_INTERNAL_set_lowTwistLimit();

		//System.Void UnityEngine.CharacterJoint::INTERNAL_set_swing1Limit(UnityEngine.SoftJointLimit&)
		void Register_UnityEngine_CharacterJoint_INTERNAL_set_swing1Limit();
		Register_UnityEngine_CharacterJoint_INTERNAL_set_swing1Limit();

		//System.Void UnityEngine.CharacterJoint::INTERNAL_set_swing2Limit(UnityEngine.SoftJointLimit&)
		void Register_UnityEngine_CharacterJoint_INTERNAL_set_swing2Limit();
		Register_UnityEngine_CharacterJoint_INTERNAL_set_swing2Limit();

		//System.Void UnityEngine.CharacterJoint::INTERNAL_set_swingAxis(UnityEngine.Vector3&)
		void Register_UnityEngine_CharacterJoint_INTERNAL_set_swingAxis();
		Register_UnityEngine_CharacterJoint_INTERNAL_set_swingAxis();

		//System.Void UnityEngine.CharacterJoint::INTERNAL_set_swingLimitSpring(UnityEngine.SoftJointLimitSpring&)
		void Register_UnityEngine_CharacterJoint_INTERNAL_set_swingLimitSpring();
		Register_UnityEngine_CharacterJoint_INTERNAL_set_swingLimitSpring();

		//System.Void UnityEngine.CharacterJoint::INTERNAL_set_twistLimitSpring(UnityEngine.SoftJointLimitSpring&)
		void Register_UnityEngine_CharacterJoint_INTERNAL_set_twistLimitSpring();
		Register_UnityEngine_CharacterJoint_INTERNAL_set_twistLimitSpring();

		//System.Void UnityEngine.CharacterJoint::set_enableProjection(System.Boolean)
		void Register_UnityEngine_CharacterJoint_set_enableProjection();
		Register_UnityEngine_CharacterJoint_set_enableProjection();

		//System.Void UnityEngine.CharacterJoint::set_projectionAngle(System.Single)
		void Register_UnityEngine_CharacterJoint_set_projectionAngle();
		Register_UnityEngine_CharacterJoint_set_projectionAngle();

		//System.Void UnityEngine.CharacterJoint::set_projectionDistance(System.Single)
		void Register_UnityEngine_CharacterJoint_set_projectionDistance();
		Register_UnityEngine_CharacterJoint_set_projectionDistance();

	//End Registrations for type : UnityEngine.CharacterJoint

	//Start Registrations for type : UnityEngine.CircleCollider2D

		//System.Single UnityEngine.CircleCollider2D::get_radius()
		void Register_UnityEngine_CircleCollider2D_get_radius();
		Register_UnityEngine_CircleCollider2D_get_radius();

		//System.Void UnityEngine.CircleCollider2D::set_radius(System.Single)
		void Register_UnityEngine_CircleCollider2D_set_radius();
		Register_UnityEngine_CircleCollider2D_set_radius();

	//End Registrations for type : UnityEngine.CircleCollider2D

	//Start Registrations for type : UnityEngine.Cloth

		//System.Boolean UnityEngine.Cloth::get_enabled()
		void Register_UnityEngine_Cloth_get_enabled();
		Register_UnityEngine_Cloth_get_enabled();

		//System.Boolean UnityEngine.Cloth::get_solverFrequency()
		void Register_UnityEngine_Cloth_get_solverFrequency();
		Register_UnityEngine_Cloth_get_solverFrequency();

		//System.Boolean UnityEngine.Cloth::get_useGravity()
		void Register_UnityEngine_Cloth_get_useGravity();
		Register_UnityEngine_Cloth_get_useGravity();

		//System.Single UnityEngine.Cloth::get_bendingStiffness()
		void Register_UnityEngine_Cloth_get_bendingStiffness();
		Register_UnityEngine_Cloth_get_bendingStiffness();

		//System.Single UnityEngine.Cloth::get_collisionMassScale()
		void Register_UnityEngine_Cloth_get_collisionMassScale();
		Register_UnityEngine_Cloth_get_collisionMassScale();

		//System.Single UnityEngine.Cloth::get_damping()
		void Register_UnityEngine_Cloth_get_damping();
		Register_UnityEngine_Cloth_get_damping();

		//System.Single UnityEngine.Cloth::get_friction()
		void Register_UnityEngine_Cloth_get_friction();
		Register_UnityEngine_Cloth_get_friction();

		//System.Single UnityEngine.Cloth::get_sleepThreshold()
		void Register_UnityEngine_Cloth_get_sleepThreshold();
		Register_UnityEngine_Cloth_get_sleepThreshold();

		//System.Single UnityEngine.Cloth::get_stretchingStiffness()
		void Register_UnityEngine_Cloth_get_stretchingStiffness();
		Register_UnityEngine_Cloth_get_stretchingStiffness();

		//System.Single UnityEngine.Cloth::get_useContinuousCollision()
		void Register_UnityEngine_Cloth_get_useContinuousCollision();
		Register_UnityEngine_Cloth_get_useContinuousCollision();

		//System.Single UnityEngine.Cloth::get_useVirtualParticles()
		void Register_UnityEngine_Cloth_get_useVirtualParticles();
		Register_UnityEngine_Cloth_get_useVirtualParticles();

		//System.Single UnityEngine.Cloth::get_worldAccelerationScale()
		void Register_UnityEngine_Cloth_get_worldAccelerationScale();
		Register_UnityEngine_Cloth_get_worldAccelerationScale();

		//System.Single UnityEngine.Cloth::get_worldVelocityScale()
		void Register_UnityEngine_Cloth_get_worldVelocityScale();
		Register_UnityEngine_Cloth_get_worldVelocityScale();

		//System.Void UnityEngine.Cloth::INTERNAL_CALL_ClearTransformMotion(UnityEngine.Cloth)
		void Register_UnityEngine_Cloth_INTERNAL_CALL_ClearTransformMotion();
		Register_UnityEngine_Cloth_INTERNAL_CALL_ClearTransformMotion();

		//System.Void UnityEngine.Cloth::INTERNAL_get_externalAcceleration(UnityEngine.Vector3&)
		void Register_UnityEngine_Cloth_INTERNAL_get_externalAcceleration();
		Register_UnityEngine_Cloth_INTERNAL_get_externalAcceleration();

		//System.Void UnityEngine.Cloth::INTERNAL_get_randomAcceleration(UnityEngine.Vector3&)
		void Register_UnityEngine_Cloth_INTERNAL_get_randomAcceleration();
		Register_UnityEngine_Cloth_INTERNAL_get_randomAcceleration();

		//System.Void UnityEngine.Cloth::INTERNAL_set_externalAcceleration(UnityEngine.Vector3&)
		void Register_UnityEngine_Cloth_INTERNAL_set_externalAcceleration();
		Register_UnityEngine_Cloth_INTERNAL_set_externalAcceleration();

		//System.Void UnityEngine.Cloth::INTERNAL_set_randomAcceleration(UnityEngine.Vector3&)
		void Register_UnityEngine_Cloth_INTERNAL_set_randomAcceleration();
		Register_UnityEngine_Cloth_INTERNAL_set_randomAcceleration();

		//System.Void UnityEngine.Cloth::SetEnabledFading(System.Boolean,System.Single)
		void Register_UnityEngine_Cloth_SetEnabledFading();
		Register_UnityEngine_Cloth_SetEnabledFading();

		//System.Void UnityEngine.Cloth::set_bendingStiffness(System.Single)
		void Register_UnityEngine_Cloth_set_bendingStiffness();
		Register_UnityEngine_Cloth_set_bendingStiffness();

		//System.Void UnityEngine.Cloth::set_capsuleColliders(UnityEngine.CapsuleCollider[])
		void Register_UnityEngine_Cloth_set_capsuleColliders();
		Register_UnityEngine_Cloth_set_capsuleColliders();

		//System.Void UnityEngine.Cloth::set_coefficients(UnityEngine.ClothSkinningCoefficient[])
		void Register_UnityEngine_Cloth_set_coefficients();
		Register_UnityEngine_Cloth_set_coefficients();

		//System.Void UnityEngine.Cloth::set_collisionMassScale(System.Single)
		void Register_UnityEngine_Cloth_set_collisionMassScale();
		Register_UnityEngine_Cloth_set_collisionMassScale();

		//System.Void UnityEngine.Cloth::set_damping(System.Single)
		void Register_UnityEngine_Cloth_set_damping();
		Register_UnityEngine_Cloth_set_damping();

		//System.Void UnityEngine.Cloth::set_enabled(System.Boolean)
		void Register_UnityEngine_Cloth_set_enabled();
		Register_UnityEngine_Cloth_set_enabled();

		//System.Void UnityEngine.Cloth::set_friction(System.Single)
		void Register_UnityEngine_Cloth_set_friction();
		Register_UnityEngine_Cloth_set_friction();

		//System.Void UnityEngine.Cloth::set_sleepThreshold(System.Single)
		void Register_UnityEngine_Cloth_set_sleepThreshold();
		Register_UnityEngine_Cloth_set_sleepThreshold();

		//System.Void UnityEngine.Cloth::set_solverFrequency(System.Boolean)
		void Register_UnityEngine_Cloth_set_solverFrequency();
		Register_UnityEngine_Cloth_set_solverFrequency();

		//System.Void UnityEngine.Cloth::set_sphereColliders(UnityEngine.ClothSphereColliderPair[])
		void Register_UnityEngine_Cloth_set_sphereColliders();
		Register_UnityEngine_Cloth_set_sphereColliders();

		//System.Void UnityEngine.Cloth::set_stretchingStiffness(System.Single)
		void Register_UnityEngine_Cloth_set_stretchingStiffness();
		Register_UnityEngine_Cloth_set_stretchingStiffness();

		//System.Void UnityEngine.Cloth::set_useContinuousCollision(System.Single)
		void Register_UnityEngine_Cloth_set_useContinuousCollision();
		Register_UnityEngine_Cloth_set_useContinuousCollision();

		//System.Void UnityEngine.Cloth::set_useGravity(System.Boolean)
		void Register_UnityEngine_Cloth_set_useGravity();
		Register_UnityEngine_Cloth_set_useGravity();

		//System.Void UnityEngine.Cloth::set_useVirtualParticles(System.Single)
		void Register_UnityEngine_Cloth_set_useVirtualParticles();
		Register_UnityEngine_Cloth_set_useVirtualParticles();

		//System.Void UnityEngine.Cloth::set_worldAccelerationScale(System.Single)
		void Register_UnityEngine_Cloth_set_worldAccelerationScale();
		Register_UnityEngine_Cloth_set_worldAccelerationScale();

		//System.Void UnityEngine.Cloth::set_worldVelocityScale(System.Single)
		void Register_UnityEngine_Cloth_set_worldVelocityScale();
		Register_UnityEngine_Cloth_set_worldVelocityScale();

		//UnityEngine.CapsuleCollider[] UnityEngine.Cloth::get_capsuleColliders()
		void Register_UnityEngine_Cloth_get_capsuleColliders();
		Register_UnityEngine_Cloth_get_capsuleColliders();

		//UnityEngine.ClothSkinningCoefficient[] UnityEngine.Cloth::get_coefficients()
		void Register_UnityEngine_Cloth_get_coefficients();
		Register_UnityEngine_Cloth_get_coefficients();

		//UnityEngine.ClothSphereColliderPair[] UnityEngine.Cloth::get_sphereColliders()
		void Register_UnityEngine_Cloth_get_sphereColliders();
		Register_UnityEngine_Cloth_get_sphereColliders();

		//UnityEngine.Vector3[] UnityEngine.Cloth::get_normals()
		void Register_UnityEngine_Cloth_get_normals();
		Register_UnityEngine_Cloth_get_normals();

		//UnityEngine.Vector3[] UnityEngine.Cloth::get_vertices()
		void Register_UnityEngine_Cloth_get_vertices();
		Register_UnityEngine_Cloth_get_vertices();

	//End Registrations for type : UnityEngine.Cloth

	//Start Registrations for type : UnityEngine.Collider

		//System.Boolean UnityEngine.Collider::INTERNAL_CALL_Internal_Raycast(UnityEngine.Collider,UnityEngine.Ray&,UnityEngine.RaycastHit&,System.Single)
		void Register_UnityEngine_Collider_INTERNAL_CALL_Internal_Raycast();
		Register_UnityEngine_Collider_INTERNAL_CALL_Internal_Raycast();

		//System.Boolean UnityEngine.Collider::get_enabled()
		void Register_UnityEngine_Collider_get_enabled();
		Register_UnityEngine_Collider_get_enabled();

		//System.Boolean UnityEngine.Collider::get_isTrigger()
		void Register_UnityEngine_Collider_get_isTrigger();
		Register_UnityEngine_Collider_get_isTrigger();

		//System.Single UnityEngine.Collider::get_contactOffset()
		void Register_UnityEngine_Collider_get_contactOffset();
		Register_UnityEngine_Collider_get_contactOffset();

		//System.Void UnityEngine.Collider::INTERNAL_CALL_ClosestPointOnBounds(UnityEngine.Collider,UnityEngine.Vector3&,UnityEngine.Vector3&)
		void Register_UnityEngine_Collider_INTERNAL_CALL_ClosestPointOnBounds();
		Register_UnityEngine_Collider_INTERNAL_CALL_ClosestPointOnBounds();

		//System.Void UnityEngine.Collider::INTERNAL_get_bounds(UnityEngine.Bounds&)
		void Register_UnityEngine_Collider_INTERNAL_get_bounds();
		Register_UnityEngine_Collider_INTERNAL_get_bounds();

		//System.Void UnityEngine.Collider::set_contactOffset(System.Single)
		void Register_UnityEngine_Collider_set_contactOffset();
		Register_UnityEngine_Collider_set_contactOffset();

		//System.Void UnityEngine.Collider::set_enabled(System.Boolean)
		void Register_UnityEngine_Collider_set_enabled();
		Register_UnityEngine_Collider_set_enabled();

		//System.Void UnityEngine.Collider::set_isTrigger(System.Boolean)
		void Register_UnityEngine_Collider_set_isTrigger();
		Register_UnityEngine_Collider_set_isTrigger();

		//System.Void UnityEngine.Collider::set_material(UnityEngine.PhysicMaterial)
		void Register_UnityEngine_Collider_set_material();
		Register_UnityEngine_Collider_set_material();

		//System.Void UnityEngine.Collider::set_sharedMaterial(UnityEngine.PhysicMaterial)
		void Register_UnityEngine_Collider_set_sharedMaterial();
		Register_UnityEngine_Collider_set_sharedMaterial();

		//UnityEngine.PhysicMaterial UnityEngine.Collider::get_material()
		void Register_UnityEngine_Collider_get_material();
		Register_UnityEngine_Collider_get_material();

		//UnityEngine.PhysicMaterial UnityEngine.Collider::get_sharedMaterial()
		void Register_UnityEngine_Collider_get_sharedMaterial();
		Register_UnityEngine_Collider_get_sharedMaterial();

		//UnityEngine.Rigidbody UnityEngine.Collider::get_attachedRigidbody()
		void Register_UnityEngine_Collider_get_attachedRigidbody();
		Register_UnityEngine_Collider_get_attachedRigidbody();

	//End Registrations for type : UnityEngine.Collider

	//Start Registrations for type : UnityEngine.Collider2D

		//System.Boolean UnityEngine.Collider2D::INTERNAL_CALL_OverlapPoint(UnityEngine.Collider2D,UnityEngine.Vector2&)
		void Register_UnityEngine_Collider2D_INTERNAL_CALL_OverlapPoint();
		Register_UnityEngine_Collider2D_INTERNAL_CALL_OverlapPoint();

		//System.Boolean UnityEngine.Collider2D::IsTouching(UnityEngine.Collider2D)
		void Register_UnityEngine_Collider2D_IsTouching();
		Register_UnityEngine_Collider2D_IsTouching();

		//System.Boolean UnityEngine.Collider2D::IsTouchingLayers(System.Int32)
		void Register_UnityEngine_Collider2D_IsTouchingLayers();
		Register_UnityEngine_Collider2D_IsTouchingLayers();

		//System.Boolean UnityEngine.Collider2D::get_isTrigger()
		void Register_UnityEngine_Collider2D_get_isTrigger();
		Register_UnityEngine_Collider2D_get_isTrigger();

		//System.Boolean UnityEngine.Collider2D::get_usedByEffector()
		void Register_UnityEngine_Collider2D_get_usedByEffector();
		Register_UnityEngine_Collider2D_get_usedByEffector();

		//System.Int32 UnityEngine.Collider2D::get_shapeCount()
		void Register_UnityEngine_Collider2D_get_shapeCount();
		Register_UnityEngine_Collider2D_get_shapeCount();

		//System.Single UnityEngine.Collider2D::get_density()
		void Register_UnityEngine_Collider2D_get_density();
		Register_UnityEngine_Collider2D_get_density();

		//System.Void UnityEngine.Collider2D::INTERNAL_get_bounds(UnityEngine.Bounds&)
		void Register_UnityEngine_Collider2D_INTERNAL_get_bounds();
		Register_UnityEngine_Collider2D_INTERNAL_get_bounds();

		//System.Void UnityEngine.Collider2D::INTERNAL_get_offset(UnityEngine.Vector2&)
		void Register_UnityEngine_Collider2D_INTERNAL_get_offset();
		Register_UnityEngine_Collider2D_INTERNAL_get_offset();

		//System.Void UnityEngine.Collider2D::INTERNAL_set_offset(UnityEngine.Vector2&)
		void Register_UnityEngine_Collider2D_INTERNAL_set_offset();
		Register_UnityEngine_Collider2D_INTERNAL_set_offset();

		//System.Void UnityEngine.Collider2D::set_density(System.Single)
		void Register_UnityEngine_Collider2D_set_density();
		Register_UnityEngine_Collider2D_set_density();

		//System.Void UnityEngine.Collider2D::set_isTrigger(System.Boolean)
		void Register_UnityEngine_Collider2D_set_isTrigger();
		Register_UnityEngine_Collider2D_set_isTrigger();

		//System.Void UnityEngine.Collider2D::set_sharedMaterial(UnityEngine.PhysicsMaterial2D)
		void Register_UnityEngine_Collider2D_set_sharedMaterial();
		Register_UnityEngine_Collider2D_set_sharedMaterial();

		//System.Void UnityEngine.Collider2D::set_usedByEffector(System.Boolean)
		void Register_UnityEngine_Collider2D_set_usedByEffector();
		Register_UnityEngine_Collider2D_set_usedByEffector();

		//UnityEngine.PhysicsMaterial2D UnityEngine.Collider2D::get_sharedMaterial()
		void Register_UnityEngine_Collider2D_get_sharedMaterial();
		Register_UnityEngine_Collider2D_get_sharedMaterial();

		//UnityEngine.Rigidbody2D UnityEngine.Collider2D::get_attachedRigidbody()
		void Register_UnityEngine_Collider2D_get_attachedRigidbody();
		Register_UnityEngine_Collider2D_get_attachedRigidbody();

	//End Registrations for type : UnityEngine.Collider2D

	//Start Registrations for type : UnityEngine.CombineInstance

		//UnityEngine.Mesh UnityEngine.CombineInstance::InternalGetMesh(System.Int32)
		void Register_UnityEngine_CombineInstance_InternalGetMesh();
		Register_UnityEngine_CombineInstance_InternalGetMesh();

	//End Registrations for type : UnityEngine.CombineInstance

	//Start Registrations for type : UnityEngine.Compass

		//System.Boolean UnityEngine.Compass::get_enabled()
		void Register_UnityEngine_Compass_get_enabled();
		Register_UnityEngine_Compass_get_enabled();

		//System.Double UnityEngine.Compass::get_timestamp()
		void Register_UnityEngine_Compass_get_timestamp();
		Register_UnityEngine_Compass_get_timestamp();

		//System.Single UnityEngine.Compass::get_headingAccuracy()
		void Register_UnityEngine_Compass_get_headingAccuracy();
		Register_UnityEngine_Compass_get_headingAccuracy();

		//System.Single UnityEngine.Compass::get_magneticHeading()
		void Register_UnityEngine_Compass_get_magneticHeading();
		Register_UnityEngine_Compass_get_magneticHeading();

		//System.Single UnityEngine.Compass::get_trueHeading()
		void Register_UnityEngine_Compass_get_trueHeading();
		Register_UnityEngine_Compass_get_trueHeading();

		//System.Void UnityEngine.Compass::INTERNAL_get_rawVector(UnityEngine.Vector3&)
		void Register_UnityEngine_Compass_INTERNAL_get_rawVector();
		Register_UnityEngine_Compass_INTERNAL_get_rawVector();

		//System.Void UnityEngine.Compass::set_enabled(System.Boolean)
		void Register_UnityEngine_Compass_set_enabled();
		Register_UnityEngine_Compass_set_enabled();

	//End Registrations for type : UnityEngine.Compass

	//Start Registrations for type : UnityEngine.Component

		//System.Boolean UnityEngine.Component::CompareTag(System.String)
		void Register_UnityEngine_Component_CompareTag();
		Register_UnityEngine_Component_CompareTag();

		//System.Void UnityEngine.Component::BroadcastMessage(System.String,System.Object,UnityEngine.SendMessageOptions)
		void Register_UnityEngine_Component_BroadcastMessage();
		Register_UnityEngine_Component_BroadcastMessage();

		//System.Void UnityEngine.Component::GetComponentFastPath(System.Type,System.IntPtr)
		void Register_UnityEngine_Component_GetComponentFastPath();
		Register_UnityEngine_Component_GetComponentFastPath();

		//System.Void UnityEngine.Component::GetComponentsForListInternal(System.Type,System.Object)
		void Register_UnityEngine_Component_GetComponentsForListInternal();
		Register_UnityEngine_Component_GetComponentsForListInternal();

		//System.Void UnityEngine.Component::SendMessage(System.String,System.Object,UnityEngine.SendMessageOptions)
		void Register_UnityEngine_Component_SendMessage();
		Register_UnityEngine_Component_SendMessage();

		//System.Void UnityEngine.Component::SendMessageUpwards(System.String,System.Object,UnityEngine.SendMessageOptions)
		void Register_UnityEngine_Component_SendMessageUpwards();
		Register_UnityEngine_Component_SendMessageUpwards();

		//UnityEngine.Component UnityEngine.Component::GetComponent(System.String)
		void Register_UnityEngine_Component_GetComponent();
		Register_UnityEngine_Component_GetComponent();

		//UnityEngine.GameObject UnityEngine.Component::get_gameObject()
		void Register_UnityEngine_Component_get_gameObject();
		Register_UnityEngine_Component_get_gameObject();

		//UnityEngine.Transform UnityEngine.Component::get_transform()
		void Register_UnityEngine_Component_get_transform();
		Register_UnityEngine_Component_get_transform();

	//End Registrations for type : UnityEngine.Component

	//Start Registrations for type : UnityEngine.ComputeBuffer

		//System.Int32 UnityEngine.ComputeBuffer::get_count()
		void Register_UnityEngine_ComputeBuffer_get_count();
		Register_UnityEngine_ComputeBuffer_get_count();

		//System.Int32 UnityEngine.ComputeBuffer::get_stride()
		void Register_UnityEngine_ComputeBuffer_get_stride();
		Register_UnityEngine_ComputeBuffer_get_stride();

		//System.Void UnityEngine.ComputeBuffer::CopyCount(UnityEngine.ComputeBuffer,UnityEngine.ComputeBuffer,System.Int32)
		void Register_UnityEngine_ComputeBuffer_CopyCount();
		Register_UnityEngine_ComputeBuffer_CopyCount();

		//System.Void UnityEngine.ComputeBuffer::DestroyBuffer(UnityEngine.ComputeBuffer)
		void Register_UnityEngine_ComputeBuffer_DestroyBuffer();
		Register_UnityEngine_ComputeBuffer_DestroyBuffer();

		//System.Void UnityEngine.ComputeBuffer::InitBuffer(UnityEngine.ComputeBuffer,System.Int32,System.Int32,UnityEngine.ComputeBufferType)
		void Register_UnityEngine_ComputeBuffer_InitBuffer();
		Register_UnityEngine_ComputeBuffer_InitBuffer();

		//System.Void UnityEngine.ComputeBuffer::InternalGetData(System.Array,System.Int32)
		void Register_UnityEngine_ComputeBuffer_InternalGetData();
		Register_UnityEngine_ComputeBuffer_InternalGetData();

		//System.Void UnityEngine.ComputeBuffer::InternalSetData(System.Array,System.Int32)
		void Register_UnityEngine_ComputeBuffer_InternalSetData();
		Register_UnityEngine_ComputeBuffer_InternalSetData();

	//End Registrations for type : UnityEngine.ComputeBuffer

	//Start Registrations for type : UnityEngine.ComputeShader

		//System.Int32 UnityEngine.ComputeShader::FindKernel(System.String)
		void Register_UnityEngine_ComputeShader_FindKernel();
		Register_UnityEngine_ComputeShader_FindKernel();

		//System.Void UnityEngine.ComputeShader::Dispatch(System.Int32,System.Int32,System.Int32,System.Int32)
		void Register_UnityEngine_ComputeShader_Dispatch();
		Register_UnityEngine_ComputeShader_Dispatch();

		//System.Void UnityEngine.ComputeShader::INTERNAL_CALL_SetVector(UnityEngine.ComputeShader,System.String,UnityEngine.Vector4&)
		void Register_UnityEngine_ComputeShader_INTERNAL_CALL_SetVector();
		Register_UnityEngine_ComputeShader_INTERNAL_CALL_SetVector();

		//System.Void UnityEngine.ComputeShader::Internal_SetFloats(System.String,System.Single[])
		void Register_UnityEngine_ComputeShader_Internal_SetFloats();
		Register_UnityEngine_ComputeShader_Internal_SetFloats();

		//System.Void UnityEngine.ComputeShader::Internal_SetInts(System.String,System.Int32[])
		void Register_UnityEngine_ComputeShader_Internal_SetInts();
		Register_UnityEngine_ComputeShader_Internal_SetInts();

		//System.Void UnityEngine.ComputeShader::SetBuffer(System.Int32,System.String,UnityEngine.ComputeBuffer)
		void Register_UnityEngine_ComputeShader_SetBuffer();
		Register_UnityEngine_ComputeShader_SetBuffer();

		//System.Void UnityEngine.ComputeShader::SetFloat(System.String,System.Single)
		void Register_UnityEngine_ComputeShader_SetFloat();
		Register_UnityEngine_ComputeShader_SetFloat();

		//System.Void UnityEngine.ComputeShader::SetInt(System.String,System.Int32)
		void Register_UnityEngine_ComputeShader_SetInt();
		Register_UnityEngine_ComputeShader_SetInt();

		//System.Void UnityEngine.ComputeShader::SetTexture(System.Int32,System.String,UnityEngine.Texture)
		void Register_UnityEngine_ComputeShader_SetTexture();
		Register_UnityEngine_ComputeShader_SetTexture();

	//End Registrations for type : UnityEngine.ComputeShader

	//Start Registrations for type : UnityEngine.ConfigurableJoint

		//System.Boolean UnityEngine.ConfigurableJoint::get_configuredInWorldSpace()
		void Register_UnityEngine_ConfigurableJoint_get_configuredInWorldSpace();
		Register_UnityEngine_ConfigurableJoint_get_configuredInWorldSpace();

		//System.Boolean UnityEngine.ConfigurableJoint::get_swapBodies()
		void Register_UnityEngine_ConfigurableJoint_get_swapBodies();
		Register_UnityEngine_ConfigurableJoint_get_swapBodies();

		//System.Single UnityEngine.ConfigurableJoint::get_projectionAngle()
		void Register_UnityEngine_ConfigurableJoint_get_projectionAngle();
		Register_UnityEngine_ConfigurableJoint_get_projectionAngle();

		//System.Single UnityEngine.ConfigurableJoint::get_projectionDistance()
		void Register_UnityEngine_ConfigurableJoint_get_projectionDistance();
		Register_UnityEngine_ConfigurableJoint_get_projectionDistance();

		//System.Void UnityEngine.ConfigurableJoint::INTERNAL_get_angularXDrive(UnityEngine.JointDrive&)
		void Register_UnityEngine_ConfigurableJoint_INTERNAL_get_angularXDrive();
		Register_UnityEngine_ConfigurableJoint_INTERNAL_get_angularXDrive();

		//System.Void UnityEngine.ConfigurableJoint::INTERNAL_get_angularXLimitSpring(UnityEngine.SoftJointLimitSpring&)
		void Register_UnityEngine_ConfigurableJoint_INTERNAL_get_angularXLimitSpring();
		Register_UnityEngine_ConfigurableJoint_INTERNAL_get_angularXLimitSpring();

		//System.Void UnityEngine.ConfigurableJoint::INTERNAL_get_angularYLimit(UnityEngine.SoftJointLimit&)
		void Register_UnityEngine_ConfigurableJoint_INTERNAL_get_angularYLimit();
		Register_UnityEngine_ConfigurableJoint_INTERNAL_get_angularYLimit();

		//System.Void UnityEngine.ConfigurableJoint::INTERNAL_get_angularYZDrive(UnityEngine.JointDrive&)
		void Register_UnityEngine_ConfigurableJoint_INTERNAL_get_angularYZDrive();
		Register_UnityEngine_ConfigurableJoint_INTERNAL_get_angularYZDrive();

		//System.Void UnityEngine.ConfigurableJoint::INTERNAL_get_angularYZLimitSpring(UnityEngine.SoftJointLimitSpring&)
		void Register_UnityEngine_ConfigurableJoint_INTERNAL_get_angularYZLimitSpring();
		Register_UnityEngine_ConfigurableJoint_INTERNAL_get_angularYZLimitSpring();

		//System.Void UnityEngine.ConfigurableJoint::INTERNAL_get_angularZLimit(UnityEngine.SoftJointLimit&)
		void Register_UnityEngine_ConfigurableJoint_INTERNAL_get_angularZLimit();
		Register_UnityEngine_ConfigurableJoint_INTERNAL_get_angularZLimit();

		//System.Void UnityEngine.ConfigurableJoint::INTERNAL_get_highAngularXLimit(UnityEngine.SoftJointLimit&)
		void Register_UnityEngine_ConfigurableJoint_INTERNAL_get_highAngularXLimit();
		Register_UnityEngine_ConfigurableJoint_INTERNAL_get_highAngularXLimit();

		//System.Void UnityEngine.ConfigurableJoint::INTERNAL_get_linearLimit(UnityEngine.SoftJointLimit&)
		void Register_UnityEngine_ConfigurableJoint_INTERNAL_get_linearLimit();
		Register_UnityEngine_ConfigurableJoint_INTERNAL_get_linearLimit();

		//System.Void UnityEngine.ConfigurableJoint::INTERNAL_get_linearLimitSpring(UnityEngine.SoftJointLimitSpring&)
		void Register_UnityEngine_ConfigurableJoint_INTERNAL_get_linearLimitSpring();
		Register_UnityEngine_ConfigurableJoint_INTERNAL_get_linearLimitSpring();

		//System.Void UnityEngine.ConfigurableJoint::INTERNAL_get_lowAngularXLimit(UnityEngine.SoftJointLimit&)
		void Register_UnityEngine_ConfigurableJoint_INTERNAL_get_lowAngularXLimit();
		Register_UnityEngine_ConfigurableJoint_INTERNAL_get_lowAngularXLimit();

		//System.Void UnityEngine.ConfigurableJoint::INTERNAL_get_secondaryAxis(UnityEngine.Vector3&)
		void Register_UnityEngine_ConfigurableJoint_INTERNAL_get_secondaryAxis();
		Register_UnityEngine_ConfigurableJoint_INTERNAL_get_secondaryAxis();

		//System.Void UnityEngine.ConfigurableJoint::INTERNAL_get_slerpDrive(UnityEngine.JointDrive&)
		void Register_UnityEngine_ConfigurableJoint_INTERNAL_get_slerpDrive();
		Register_UnityEngine_ConfigurableJoint_INTERNAL_get_slerpDrive();

		//System.Void UnityEngine.ConfigurableJoint::INTERNAL_get_targetAngularVelocity(UnityEngine.Vector3&)
		void Register_UnityEngine_ConfigurableJoint_INTERNAL_get_targetAngularVelocity();
		Register_UnityEngine_ConfigurableJoint_INTERNAL_get_targetAngularVelocity();

		//System.Void UnityEngine.ConfigurableJoint::INTERNAL_get_targetPosition(UnityEngine.Vector3&)
		void Register_UnityEngine_ConfigurableJoint_INTERNAL_get_targetPosition();
		Register_UnityEngine_ConfigurableJoint_INTERNAL_get_targetPosition();

		//System.Void UnityEngine.ConfigurableJoint::INTERNAL_get_targetRotation(UnityEngine.Quaternion&)
		void Register_UnityEngine_ConfigurableJoint_INTERNAL_get_targetRotation();
		Register_UnityEngine_ConfigurableJoint_INTERNAL_get_targetRotation();

		//System.Void UnityEngine.ConfigurableJoint::INTERNAL_get_targetVelocity(UnityEngine.Vector3&)
		void Register_UnityEngine_ConfigurableJoint_INTERNAL_get_targetVelocity();
		Register_UnityEngine_ConfigurableJoint_INTERNAL_get_targetVelocity();

		//System.Void UnityEngine.ConfigurableJoint::INTERNAL_get_xDrive(UnityEngine.JointDrive&)
		void Register_UnityEngine_ConfigurableJoint_INTERNAL_get_xDrive();
		Register_UnityEngine_ConfigurableJoint_INTERNAL_get_xDrive();

		//System.Void UnityEngine.ConfigurableJoint::INTERNAL_get_yDrive(UnityEngine.JointDrive&)
		void Register_UnityEngine_ConfigurableJoint_INTERNAL_get_yDrive();
		Register_UnityEngine_ConfigurableJoint_INTERNAL_get_yDrive();

		//System.Void UnityEngine.ConfigurableJoint::INTERNAL_get_zDrive(UnityEngine.JointDrive&)
		void Register_UnityEngine_ConfigurableJoint_INTERNAL_get_zDrive();
		Register_UnityEngine_ConfigurableJoint_INTERNAL_get_zDrive();

		//System.Void UnityEngine.ConfigurableJoint::INTERNAL_set_angularXDrive(UnityEngine.JointDrive&)
		void Register_UnityEngine_ConfigurableJoint_INTERNAL_set_angularXDrive();
		Register_UnityEngine_ConfigurableJoint_INTERNAL_set_angularXDrive();

		//System.Void UnityEngine.ConfigurableJoint::INTERNAL_set_angularXLimitSpring(UnityEngine.SoftJointLimitSpring&)
		void Register_UnityEngine_ConfigurableJoint_INTERNAL_set_angularXLimitSpring();
		Register_UnityEngine_ConfigurableJoint_INTERNAL_set_angularXLimitSpring();

		//System.Void UnityEngine.ConfigurableJoint::INTERNAL_set_angularYLimit(UnityEngine.SoftJointLimit&)
		void Register_UnityEngine_ConfigurableJoint_INTERNAL_set_angularYLimit();
		Register_UnityEngine_ConfigurableJoint_INTERNAL_set_angularYLimit();

		//System.Void UnityEngine.ConfigurableJoint::INTERNAL_set_angularYZDrive(UnityEngine.JointDrive&)
		void Register_UnityEngine_ConfigurableJoint_INTERNAL_set_angularYZDrive();
		Register_UnityEngine_ConfigurableJoint_INTERNAL_set_angularYZDrive();

		//System.Void UnityEngine.ConfigurableJoint::INTERNAL_set_angularYZLimitSpring(UnityEngine.SoftJointLimitSpring&)
		void Register_UnityEngine_ConfigurableJoint_INTERNAL_set_angularYZLimitSpring();
		Register_UnityEngine_ConfigurableJoint_INTERNAL_set_angularYZLimitSpring();

		//System.Void UnityEngine.ConfigurableJoint::INTERNAL_set_angularZLimit(UnityEngine.SoftJointLimit&)
		void Register_UnityEngine_ConfigurableJoint_INTERNAL_set_angularZLimit();
		Register_UnityEngine_ConfigurableJoint_INTERNAL_set_angularZLimit();

		//System.Void UnityEngine.ConfigurableJoint::INTERNAL_set_highAngularXLimit(UnityEngine.SoftJointLimit&)
		void Register_UnityEngine_ConfigurableJoint_INTERNAL_set_highAngularXLimit();
		Register_UnityEngine_ConfigurableJoint_INTERNAL_set_highAngularXLimit();

		//System.Void UnityEngine.ConfigurableJoint::INTERNAL_set_linearLimit(UnityEngine.SoftJointLimit&)
		void Register_UnityEngine_ConfigurableJoint_INTERNAL_set_linearLimit();
		Register_UnityEngine_ConfigurableJoint_INTERNAL_set_linearLimit();

		//System.Void UnityEngine.ConfigurableJoint::INTERNAL_set_linearLimitSpring(UnityEngine.SoftJointLimitSpring&)
		void Register_UnityEngine_ConfigurableJoint_INTERNAL_set_linearLimitSpring();
		Register_UnityEngine_ConfigurableJoint_INTERNAL_set_linearLimitSpring();

		//System.Void UnityEngine.ConfigurableJoint::INTERNAL_set_lowAngularXLimit(UnityEngine.SoftJointLimit&)
		void Register_UnityEngine_ConfigurableJoint_INTERNAL_set_lowAngularXLimit();
		Register_UnityEngine_ConfigurableJoint_INTERNAL_set_lowAngularXLimit();

		//System.Void UnityEngine.ConfigurableJoint::INTERNAL_set_secondaryAxis(UnityEngine.Vector3&)
		void Register_UnityEngine_ConfigurableJoint_INTERNAL_set_secondaryAxis();
		Register_UnityEngine_ConfigurableJoint_INTERNAL_set_secondaryAxis();

		//System.Void UnityEngine.ConfigurableJoint::INTERNAL_set_slerpDrive(UnityEngine.JointDrive&)
		void Register_UnityEngine_ConfigurableJoint_INTERNAL_set_slerpDrive();
		Register_UnityEngine_ConfigurableJoint_INTERNAL_set_slerpDrive();

		//System.Void UnityEngine.ConfigurableJoint::INTERNAL_set_targetAngularVelocity(UnityEngine.Vector3&)
		void Register_UnityEngine_ConfigurableJoint_INTERNAL_set_targetAngularVelocity();
		Register_UnityEngine_ConfigurableJoint_INTERNAL_set_targetAngularVelocity();

		//System.Void UnityEngine.ConfigurableJoint::INTERNAL_set_targetPosition(UnityEngine.Vector3&)
		void Register_UnityEngine_ConfigurableJoint_INTERNAL_set_targetPosition();
		Register_UnityEngine_ConfigurableJoint_INTERNAL_set_targetPosition();

		//System.Void UnityEngine.ConfigurableJoint::INTERNAL_set_targetRotation(UnityEngine.Quaternion&)
		void Register_UnityEngine_ConfigurableJoint_INTERNAL_set_targetRotation();
		Register_UnityEngine_ConfigurableJoint_INTERNAL_set_targetRotation();

		//System.Void UnityEngine.ConfigurableJoint::INTERNAL_set_targetVelocity(UnityEngine.Vector3&)
		void Register_UnityEngine_ConfigurableJoint_INTERNAL_set_targetVelocity();
		Register_UnityEngine_ConfigurableJoint_INTERNAL_set_targetVelocity();

		//System.Void UnityEngine.ConfigurableJoint::INTERNAL_set_xDrive(UnityEngine.JointDrive&)
		void Register_UnityEngine_ConfigurableJoint_INTERNAL_set_xDrive();
		Register_UnityEngine_ConfigurableJoint_INTERNAL_set_xDrive();

		//System.Void UnityEngine.ConfigurableJoint::INTERNAL_set_yDrive(UnityEngine.JointDrive&)
		void Register_UnityEngine_ConfigurableJoint_INTERNAL_set_yDrive();
		Register_UnityEngine_ConfigurableJoint_INTERNAL_set_yDrive();

		//System.Void UnityEngine.ConfigurableJoint::INTERNAL_set_zDrive(UnityEngine.JointDrive&)
		void Register_UnityEngine_ConfigurableJoint_INTERNAL_set_zDrive();
		Register_UnityEngine_ConfigurableJoint_INTERNAL_set_zDrive();

		//System.Void UnityEngine.ConfigurableJoint::set_angularXMotion(UnityEngine.ConfigurableJointMotion)
		void Register_UnityEngine_ConfigurableJoint_set_angularXMotion();
		Register_UnityEngine_ConfigurableJoint_set_angularXMotion();

		//System.Void UnityEngine.ConfigurableJoint::set_angularYMotion(UnityEngine.ConfigurableJointMotion)
		void Register_UnityEngine_ConfigurableJoint_set_angularYMotion();
		Register_UnityEngine_ConfigurableJoint_set_angularYMotion();

		//System.Void UnityEngine.ConfigurableJoint::set_angularZMotion(UnityEngine.ConfigurableJointMotion)
		void Register_UnityEngine_ConfigurableJoint_set_angularZMotion();
		Register_UnityEngine_ConfigurableJoint_set_angularZMotion();

		//System.Void UnityEngine.ConfigurableJoint::set_configuredInWorldSpace(System.Boolean)
		void Register_UnityEngine_ConfigurableJoint_set_configuredInWorldSpace();
		Register_UnityEngine_ConfigurableJoint_set_configuredInWorldSpace();

		//System.Void UnityEngine.ConfigurableJoint::set_projectionAngle(System.Single)
		void Register_UnityEngine_ConfigurableJoint_set_projectionAngle();
		Register_UnityEngine_ConfigurableJoint_set_projectionAngle();

		//System.Void UnityEngine.ConfigurableJoint::set_projectionDistance(System.Single)
		void Register_UnityEngine_ConfigurableJoint_set_projectionDistance();
		Register_UnityEngine_ConfigurableJoint_set_projectionDistance();

		//System.Void UnityEngine.ConfigurableJoint::set_projectionMode(UnityEngine.JointProjectionMode)
		void Register_UnityEngine_ConfigurableJoint_set_projectionMode();
		Register_UnityEngine_ConfigurableJoint_set_projectionMode();

		//System.Void UnityEngine.ConfigurableJoint::set_rotationDriveMode(UnityEngine.RotationDriveMode)
		void Register_UnityEngine_ConfigurableJoint_set_rotationDriveMode();
		Register_UnityEngine_ConfigurableJoint_set_rotationDriveMode();

		//System.Void UnityEngine.ConfigurableJoint::set_swapBodies(System.Boolean)
		void Register_UnityEngine_ConfigurableJoint_set_swapBodies();
		Register_UnityEngine_ConfigurableJoint_set_swapBodies();

		//System.Void UnityEngine.ConfigurableJoint::set_xMotion(UnityEngine.ConfigurableJointMotion)
		void Register_UnityEngine_ConfigurableJoint_set_xMotion();
		Register_UnityEngine_ConfigurableJoint_set_xMotion();

		//System.Void UnityEngine.ConfigurableJoint::set_yMotion(UnityEngine.ConfigurableJointMotion)
		void Register_UnityEngine_ConfigurableJoint_set_yMotion();
		Register_UnityEngine_ConfigurableJoint_set_yMotion();

		//System.Void UnityEngine.ConfigurableJoint::set_zMotion(UnityEngine.ConfigurableJointMotion)
		void Register_UnityEngine_ConfigurableJoint_set_zMotion();
		Register_UnityEngine_ConfigurableJoint_set_zMotion();

		//UnityEngine.ConfigurableJointMotion UnityEngine.ConfigurableJoint::get_angularXMotion()
		void Register_UnityEngine_ConfigurableJoint_get_angularXMotion();
		Register_UnityEngine_ConfigurableJoint_get_angularXMotion();

		//UnityEngine.ConfigurableJointMotion UnityEngine.ConfigurableJoint::get_angularYMotion()
		void Register_UnityEngine_ConfigurableJoint_get_angularYMotion();
		Register_UnityEngine_ConfigurableJoint_get_angularYMotion();

		//UnityEngine.ConfigurableJointMotion UnityEngine.ConfigurableJoint::get_angularZMotion()
		void Register_UnityEngine_ConfigurableJoint_get_angularZMotion();
		Register_UnityEngine_ConfigurableJoint_get_angularZMotion();

		//UnityEngine.ConfigurableJointMotion UnityEngine.ConfigurableJoint::get_xMotion()
		void Register_UnityEngine_ConfigurableJoint_get_xMotion();
		Register_UnityEngine_ConfigurableJoint_get_xMotion();

		//UnityEngine.ConfigurableJointMotion UnityEngine.ConfigurableJoint::get_yMotion()
		void Register_UnityEngine_ConfigurableJoint_get_yMotion();
		Register_UnityEngine_ConfigurableJoint_get_yMotion();

		//UnityEngine.ConfigurableJointMotion UnityEngine.ConfigurableJoint::get_zMotion()
		void Register_UnityEngine_ConfigurableJoint_get_zMotion();
		Register_UnityEngine_ConfigurableJoint_get_zMotion();

		//UnityEngine.JointProjectionMode UnityEngine.ConfigurableJoint::get_projectionMode()
		void Register_UnityEngine_ConfigurableJoint_get_projectionMode();
		Register_UnityEngine_ConfigurableJoint_get_projectionMode();

		//UnityEngine.RotationDriveMode UnityEngine.ConfigurableJoint::get_rotationDriveMode()
		void Register_UnityEngine_ConfigurableJoint_get_rotationDriveMode();
		Register_UnityEngine_ConfigurableJoint_get_rotationDriveMode();

	//End Registrations for type : UnityEngine.ConfigurableJoint

	//Start Registrations for type : UnityEngine.ConstantForce

		//System.Void UnityEngine.ConstantForce::INTERNAL_get_force(UnityEngine.Vector3&)
		void Register_UnityEngine_ConstantForce_INTERNAL_get_force();
		Register_UnityEngine_ConstantForce_INTERNAL_get_force();

		//System.Void UnityEngine.ConstantForce::INTERNAL_get_relativeForce(UnityEngine.Vector3&)
		void Register_UnityEngine_ConstantForce_INTERNAL_get_relativeForce();
		Register_UnityEngine_ConstantForce_INTERNAL_get_relativeForce();

		//System.Void UnityEngine.ConstantForce::INTERNAL_get_relativeTorque(UnityEngine.Vector3&)
		void Register_UnityEngine_ConstantForce_INTERNAL_get_relativeTorque();
		Register_UnityEngine_ConstantForce_INTERNAL_get_relativeTorque();

		//System.Void UnityEngine.ConstantForce::INTERNAL_get_torque(UnityEngine.Vector3&)
		void Register_UnityEngine_ConstantForce_INTERNAL_get_torque();
		Register_UnityEngine_ConstantForce_INTERNAL_get_torque();

		//System.Void UnityEngine.ConstantForce::INTERNAL_set_force(UnityEngine.Vector3&)
		void Register_UnityEngine_ConstantForce_INTERNAL_set_force();
		Register_UnityEngine_ConstantForce_INTERNAL_set_force();

		//System.Void UnityEngine.ConstantForce::INTERNAL_set_relativeForce(UnityEngine.Vector3&)
		void Register_UnityEngine_ConstantForce_INTERNAL_set_relativeForce();
		Register_UnityEngine_ConstantForce_INTERNAL_set_relativeForce();

		//System.Void UnityEngine.ConstantForce::INTERNAL_set_relativeTorque(UnityEngine.Vector3&)
		void Register_UnityEngine_ConstantForce_INTERNAL_set_relativeTorque();
		Register_UnityEngine_ConstantForce_INTERNAL_set_relativeTorque();

		//System.Void UnityEngine.ConstantForce::INTERNAL_set_torque(UnityEngine.Vector3&)
		void Register_UnityEngine_ConstantForce_INTERNAL_set_torque();
		Register_UnityEngine_ConstantForce_INTERNAL_set_torque();

	//End Registrations for type : UnityEngine.ConstantForce

	//Start Registrations for type : UnityEngine.ContactPoint

		//UnityEngine.Collider UnityEngine.ContactPoint::ColliderFromInstanceId(System.Int32)
		void Register_UnityEngine_ContactPoint_ColliderFromInstanceId();
		Register_UnityEngine_ContactPoint_ColliderFromInstanceId();

	//End Registrations for type : UnityEngine.ContactPoint

	//Start Registrations for type : UnityEngine.Coroutine

		//System.Void UnityEngine.Coroutine::ReleaseCoroutine()
		void Register_UnityEngine_Coroutine_ReleaseCoroutine();
		Register_UnityEngine_Coroutine_ReleaseCoroutine();

	//End Registrations for type : UnityEngine.Coroutine

	//Start Registrations for type : UnityEngine.CrashReport

		//System.Boolean UnityEngine.CrashReport::RemoveReport(System.String)
		void Register_UnityEngine_CrashReport_RemoveReport();
		Register_UnityEngine_CrashReport_RemoveReport();

		//System.String[] UnityEngine.CrashReport::GetReports()
		void Register_UnityEngine_CrashReport_GetReports();
		Register_UnityEngine_CrashReport_GetReports();

		//System.Void UnityEngine.CrashReport::GetReportData(System.String,System.Double&,System.String&)
		void Register_UnityEngine_CrashReport_GetReportData();
		Register_UnityEngine_CrashReport_GetReportData();

	//End Registrations for type : UnityEngine.CrashReport

	//Start Registrations for type : UnityEngine.Cubemap

		//System.Int32 UnityEngine.Cubemap::get_mipmapCount()
		void Register_UnityEngine_Cubemap_get_mipmapCount();
		Register_UnityEngine_Cubemap_get_mipmapCount();

		//System.Void UnityEngine.Cubemap::Apply(System.Boolean,System.Boolean)
		void Register_UnityEngine_Cubemap_Apply();
		Register_UnityEngine_Cubemap_Apply();

		//System.Void UnityEngine.Cubemap::INTERNAL_CALL_GetPixel(UnityEngine.Cubemap,UnityEngine.CubemapFace,System.Int32,System.Int32,UnityEngine.Color&)
		void Register_UnityEngine_Cubemap_INTERNAL_CALL_GetPixel();
		Register_UnityEngine_Cubemap_INTERNAL_CALL_GetPixel();

		//System.Void UnityEngine.Cubemap::INTERNAL_CALL_SetPixel(UnityEngine.Cubemap,UnityEngine.CubemapFace,System.Int32,System.Int32,UnityEngine.Color&)
		void Register_UnityEngine_Cubemap_INTERNAL_CALL_SetPixel();
		Register_UnityEngine_Cubemap_INTERNAL_CALL_SetPixel();

		//System.Void UnityEngine.Cubemap::Internal_Create(UnityEngine.Cubemap,System.Int32,UnityEngine.TextureFormat,System.Boolean)
		void Register_UnityEngine_Cubemap_Internal_Create();
		Register_UnityEngine_Cubemap_Internal_Create();

		//System.Void UnityEngine.Cubemap::SetPixels(UnityEngine.Color[],UnityEngine.CubemapFace,System.Int32)
		void Register_UnityEngine_Cubemap_SetPixels();
		Register_UnityEngine_Cubemap_SetPixels();

		//System.Void UnityEngine.Cubemap::SmoothEdges(System.Int32)
		void Register_UnityEngine_Cubemap_SmoothEdges();
		Register_UnityEngine_Cubemap_SmoothEdges();

		//UnityEngine.Color[] UnityEngine.Cubemap::GetPixels(UnityEngine.CubemapFace,System.Int32)
		void Register_UnityEngine_Cubemap_GetPixels();
		Register_UnityEngine_Cubemap_GetPixels();

		//UnityEngine.TextureFormat UnityEngine.Cubemap::get_format()
		void Register_UnityEngine_Cubemap_get_format();
		Register_UnityEngine_Cubemap_get_format();

	//End Registrations for type : UnityEngine.Cubemap

	//Start Registrations for type : UnityEngine.CullingGroup

		//System.Void UnityEngine.CullingGroup::Dispose()
		void Register_UnityEngine_CullingGroup_Dispose();
		Register_UnityEngine_CullingGroup_Dispose();

		//System.Void UnityEngine.CullingGroup::FinalizerFailure()
		void Register_UnityEngine_CullingGroup_FinalizerFailure();
		Register_UnityEngine_CullingGroup_FinalizerFailure();

	//End Registrations for type : UnityEngine.CullingGroup

	//Start Registrations for type : UnityEngine.Cursor

		//System.Boolean UnityEngine.Cursor::get_visible()
		void Register_UnityEngine_Cursor_get_visible();
		Register_UnityEngine_Cursor_get_visible();

		//System.Void UnityEngine.Cursor::INTERNAL_CALL_SetCursor(UnityEngine.Texture2D,UnityEngine.Vector2&,UnityEngine.CursorMode)
		void Register_UnityEngine_Cursor_INTERNAL_CALL_SetCursor();
		Register_UnityEngine_Cursor_INTERNAL_CALL_SetCursor();

		//System.Void UnityEngine.Cursor::set_lockState(UnityEngine.CursorLockMode)
		void Register_UnityEngine_Cursor_set_lockState();
		Register_UnityEngine_Cursor_set_lockState();

		//System.Void UnityEngine.Cursor::set_visible(System.Boolean)
		void Register_UnityEngine_Cursor_set_visible();
		Register_UnityEngine_Cursor_set_visible();

		//UnityEngine.CursorLockMode UnityEngine.Cursor::get_lockState()
		void Register_UnityEngine_Cursor_get_lockState();
		Register_UnityEngine_Cursor_get_lockState();

	//End Registrations for type : UnityEngine.Cursor

	//Start Registrations for type : UnityEngine.Debug

		//System.Boolean UnityEngine.Debug::get_developerConsoleVisible()
		void Register_UnityEngine_Debug_get_developerConsoleVisible();
		Register_UnityEngine_Debug_get_developerConsoleVisible();

		//System.Boolean UnityEngine.Debug::get_isDebugBuild()
		void Register_UnityEngine_Debug_get_isDebugBuild();
		Register_UnityEngine_Debug_get_isDebugBuild();

		//System.Void UnityEngine.Debug::Break()
		void Register_UnityEngine_Debug_Break();
		Register_UnityEngine_Debug_Break();

		//System.Void UnityEngine.Debug::ClearDeveloperConsole()
		void Register_UnityEngine_Debug_ClearDeveloperConsole();
		Register_UnityEngine_Debug_ClearDeveloperConsole();

		//System.Void UnityEngine.Debug::DebugBreak()
		void Register_UnityEngine_Debug_DebugBreak();
		Register_UnityEngine_Debug_DebugBreak();

		//System.Void UnityEngine.Debug::INTERNAL_CALL_DrawLine(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Color&,System.Single,System.Boolean)
		void Register_UnityEngine_Debug_INTERNAL_CALL_DrawLine();
		Register_UnityEngine_Debug_INTERNAL_CALL_DrawLine();

		//System.Void UnityEngine.Debug::set_developerConsoleVisible(System.Boolean)
		void Register_UnityEngine_Debug_set_developerConsoleVisible();
		Register_UnityEngine_Debug_set_developerConsoleVisible();

	//End Registrations for type : UnityEngine.Debug

	//Start Registrations for type : UnityEngine.DebugLogHandler

		//System.Void UnityEngine.DebugLogHandler::Internal_Log(UnityEngine.LogType,System.String,UnityEngine.Object)
		void Register_UnityEngine_DebugLogHandler_Internal_Log();
		Register_UnityEngine_DebugLogHandler_Internal_Log();

		//System.Void UnityEngine.DebugLogHandler::Internal_LogException(System.Exception,UnityEngine.Object)
		void Register_UnityEngine_DebugLogHandler_Internal_LogException();
		Register_UnityEngine_DebugLogHandler_Internal_LogException();

	//End Registrations for type : UnityEngine.DebugLogHandler

	//Start Registrations for type : UnityEngine.Display

		//System.Int32 UnityEngine.Display::RelativeMouseAtImpl(System.Int32,System.Int32,System.Int32&,System.Int32&)
		void Register_UnityEngine_Display_RelativeMouseAtImpl();
		Register_UnityEngine_Display_RelativeMouseAtImpl();

		//System.Void UnityEngine.Display::ActivateDisplayImpl(System.IntPtr,System.Int32,System.Int32,System.Int32)
		void Register_UnityEngine_Display_ActivateDisplayImpl();
		Register_UnityEngine_Display_ActivateDisplayImpl();

		//System.Void UnityEngine.Display::GetRenderingBuffersImpl(System.IntPtr,UnityEngine.RenderBuffer&,UnityEngine.RenderBuffer&)
		void Register_UnityEngine_Display_GetRenderingBuffersImpl();
		Register_UnityEngine_Display_GetRenderingBuffersImpl();

		//System.Void UnityEngine.Display::GetRenderingExtImpl(System.IntPtr,System.Int32&,System.Int32&)
		void Register_UnityEngine_Display_GetRenderingExtImpl();
		Register_UnityEngine_Display_GetRenderingExtImpl();

		//System.Void UnityEngine.Display::GetSystemExtImpl(System.IntPtr,System.Int32&,System.Int32&)
		void Register_UnityEngine_Display_GetSystemExtImpl();
		Register_UnityEngine_Display_GetSystemExtImpl();

		//System.Void UnityEngine.Display::SetParamsImpl(System.IntPtr,System.Int32,System.Int32,System.Int32,System.Int32)
		void Register_UnityEngine_Display_SetParamsImpl();
		Register_UnityEngine_Display_SetParamsImpl();

		//System.Void UnityEngine.Display::SetRenderingResolutionImpl(System.IntPtr,System.Int32,System.Int32)
		void Register_UnityEngine_Display_SetRenderingResolutionImpl();
		Register_UnityEngine_Display_SetRenderingResolutionImpl();

	//End Registrations for type : UnityEngine.Display

	//Start Registrations for type : UnityEngine.DistanceJoint2D

		//System.Boolean UnityEngine.DistanceJoint2D::get_autoConfigureDistance()
		void Register_UnityEngine_DistanceJoint2D_get_autoConfigureDistance();
		Register_UnityEngine_DistanceJoint2D_get_autoConfigureDistance();

		//System.Boolean UnityEngine.DistanceJoint2D::get_maxDistanceOnly()
		void Register_UnityEngine_DistanceJoint2D_get_maxDistanceOnly();
		Register_UnityEngine_DistanceJoint2D_get_maxDistanceOnly();

		//System.Single UnityEngine.DistanceJoint2D::get_distance()
		void Register_UnityEngine_DistanceJoint2D_get_distance();
		Register_UnityEngine_DistanceJoint2D_get_distance();

		//System.Void UnityEngine.DistanceJoint2D::set_autoConfigureDistance(System.Boolean)
		void Register_UnityEngine_DistanceJoint2D_set_autoConfigureDistance();
		Register_UnityEngine_DistanceJoint2D_set_autoConfigureDistance();

		//System.Void UnityEngine.DistanceJoint2D::set_distance(System.Single)
		void Register_UnityEngine_DistanceJoint2D_set_distance();
		Register_UnityEngine_DistanceJoint2D_set_distance();

		//System.Void UnityEngine.DistanceJoint2D::set_maxDistanceOnly(System.Boolean)
		void Register_UnityEngine_DistanceJoint2D_set_maxDistanceOnly();
		Register_UnityEngine_DistanceJoint2D_set_maxDistanceOnly();

	//End Registrations for type : UnityEngine.DistanceJoint2D

	//Start Registrations for type : UnityEngine.EdgeCollider2D

		//System.Int32 UnityEngine.EdgeCollider2D::get_edgeCount()
		void Register_UnityEngine_EdgeCollider2D_get_edgeCount();
		Register_UnityEngine_EdgeCollider2D_get_edgeCount();

		//System.Int32 UnityEngine.EdgeCollider2D::get_pointCount()
		void Register_UnityEngine_EdgeCollider2D_get_pointCount();
		Register_UnityEngine_EdgeCollider2D_get_pointCount();

		//System.Void UnityEngine.EdgeCollider2D::Reset()
		void Register_UnityEngine_EdgeCollider2D_Reset();
		Register_UnityEngine_EdgeCollider2D_Reset();

		//System.Void UnityEngine.EdgeCollider2D::set_points(UnityEngine.Vector2[])
		void Register_UnityEngine_EdgeCollider2D_set_points();
		Register_UnityEngine_EdgeCollider2D_set_points();

		//UnityEngine.Vector2[] UnityEngine.EdgeCollider2D::get_points()
		void Register_UnityEngine_EdgeCollider2D_get_points();
		Register_UnityEngine_EdgeCollider2D_get_points();

	//End Registrations for type : UnityEngine.EdgeCollider2D

	//Start Registrations for type : UnityEngine.Event

		//System.Boolean UnityEngine.Event::PopEvent(UnityEngine.Event)
		void Register_UnityEngine_Event_PopEvent();
		Register_UnityEngine_Event_PopEvent();

		//System.Char UnityEngine.Event::get_character()
		void Register_UnityEngine_Event_get_character();
		Register_UnityEngine_Event_get_character();

		//System.Int32 UnityEngine.Event::GetEventCount()
		void Register_UnityEngine_Event_GetEventCount();
		Register_UnityEngine_Event_GetEventCount();

		//System.Int32 UnityEngine.Event::get_button()
		void Register_UnityEngine_Event_get_button();
		Register_UnityEngine_Event_get_button();

		//System.Int32 UnityEngine.Event::get_clickCount()
		void Register_UnityEngine_Event_get_clickCount();
		Register_UnityEngine_Event_get_clickCount();

		//System.Int32 UnityEngine.Event::get_displayIndex()
		void Register_UnityEngine_Event_get_displayIndex();
		Register_UnityEngine_Event_get_displayIndex();

		//System.Single UnityEngine.Event::get_pressure()
		void Register_UnityEngine_Event_get_pressure();
		Register_UnityEngine_Event_get_pressure();

		//System.String UnityEngine.Event::get_commandName()
		void Register_UnityEngine_Event_get_commandName();
		Register_UnityEngine_Event_get_commandName();

		//System.Void UnityEngine.Event::Cleanup()
		void Register_UnityEngine_Event_Cleanup();
		Register_UnityEngine_Event_Cleanup();

		//System.Void UnityEngine.Event::INTERNAL_CALL_Internal_SetMouseDelta(UnityEngine.Event,UnityEngine.Vector2&)
		void Register_UnityEngine_Event_INTERNAL_CALL_Internal_SetMouseDelta();
		Register_UnityEngine_Event_INTERNAL_CALL_Internal_SetMouseDelta();

		//System.Void UnityEngine.Event::INTERNAL_CALL_Internal_SetMousePosition(UnityEngine.Event,UnityEngine.Vector2&)
		void Register_UnityEngine_Event_INTERNAL_CALL_Internal_SetMousePosition();
		Register_UnityEngine_Event_INTERNAL_CALL_Internal_SetMousePosition();

		//System.Void UnityEngine.Event::Init(System.Int32)
		void Register_UnityEngine_Event_Init();
		Register_UnityEngine_Event_Init();

		//System.Void UnityEngine.Event::InitCopy(UnityEngine.Event)
		void Register_UnityEngine_Event_InitCopy();
		Register_UnityEngine_Event_InitCopy();

		//System.Void UnityEngine.Event::Internal_GetMouseDelta(UnityEngine.Vector2&)
		void Register_UnityEngine_Event_Internal_GetMouseDelta();
		Register_UnityEngine_Event_Internal_GetMouseDelta();

		//System.Void UnityEngine.Event::Internal_GetMousePosition(UnityEngine.Vector2&)
		void Register_UnityEngine_Event_Internal_GetMousePosition();
		Register_UnityEngine_Event_Internal_GetMousePosition();

		//System.Void UnityEngine.Event::Internal_SetNativeEvent(System.IntPtr)
		void Register_UnityEngine_Event_Internal_SetNativeEvent();
		Register_UnityEngine_Event_Internal_SetNativeEvent();

		//System.Void UnityEngine.Event::Use()
		void Register_UnityEngine_Event_Use();
		Register_UnityEngine_Event_Use();

		//System.Void UnityEngine.Event::set_button(System.Int32)
		void Register_UnityEngine_Event_set_button();
		Register_UnityEngine_Event_set_button();

		//System.Void UnityEngine.Event::set_character(System.Char)
		void Register_UnityEngine_Event_set_character();
		Register_UnityEngine_Event_set_character();

		//System.Void UnityEngine.Event::set_clickCount(System.Int32)
		void Register_UnityEngine_Event_set_clickCount();
		Register_UnityEngine_Event_set_clickCount();

		//System.Void UnityEngine.Event::set_commandName(System.String)
		void Register_UnityEngine_Event_set_commandName();
		Register_UnityEngine_Event_set_commandName();

		//System.Void UnityEngine.Event::set_displayIndex(System.Int32)
		void Register_UnityEngine_Event_set_displayIndex();
		Register_UnityEngine_Event_set_displayIndex();

		//System.Void UnityEngine.Event::set_keyCode(UnityEngine.KeyCode)
		void Register_UnityEngine_Event_set_keyCode();
		Register_UnityEngine_Event_set_keyCode();

		//System.Void UnityEngine.Event::set_modifiers(UnityEngine.EventModifiers)
		void Register_UnityEngine_Event_set_modifiers();
		Register_UnityEngine_Event_set_modifiers();

		//System.Void UnityEngine.Event::set_pressure(System.Single)
		void Register_UnityEngine_Event_set_pressure();
		Register_UnityEngine_Event_set_pressure();

		//System.Void UnityEngine.Event::set_type(UnityEngine.EventType)
		void Register_UnityEngine_Event_set_type();
		Register_UnityEngine_Event_set_type();

		//UnityEngine.EventModifiers UnityEngine.Event::get_modifiers()
		void Register_UnityEngine_Event_get_modifiers();
		Register_UnityEngine_Event_get_modifiers();

		//UnityEngine.EventType UnityEngine.Event::GetTypeForControl(System.Int32)
		void Register_UnityEngine_Event_GetTypeForControl();
		Register_UnityEngine_Event_GetTypeForControl();

		//UnityEngine.EventType UnityEngine.Event::get_rawType()
		void Register_UnityEngine_Event_get_rawType();
		Register_UnityEngine_Event_get_rawType();

		//UnityEngine.EventType UnityEngine.Event::get_type()
		void Register_UnityEngine_Event_get_type();
		Register_UnityEngine_Event_get_type();

		//UnityEngine.KeyCode UnityEngine.Event::get_keyCode()
		void Register_UnityEngine_Event_get_keyCode();
		Register_UnityEngine_Event_get_keyCode();

	//End Registrations for type : UnityEngine.Event

	//Start Registrations for type : UnityEngine.Experimental.Director.AnimatorControllerPlayable

		//System.Boolean UnityEngine.Experimental.Director.AnimatorControllerPlayable::GetBoolID(System.Int32)
		void Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_GetBoolID();
		Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_GetBoolID();

		//System.Boolean UnityEngine.Experimental.Director.AnimatorControllerPlayable::GetBoolString(System.String)
		void Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_GetBoolString();
		Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_GetBoolString();

		//System.Boolean UnityEngine.Experimental.Director.AnimatorControllerPlayable::HasState(System.Int32,System.Int32)
		void Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_HasState();
		Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_HasState();

		//System.Boolean UnityEngine.Experimental.Director.AnimatorControllerPlayable::IsInTransition(System.Int32)
		void Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_IsInTransition();
		Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_IsInTransition();

		//System.Boolean UnityEngine.Experimental.Director.AnimatorControllerPlayable::IsParameterControlledByCurveID(System.Int32)
		void Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_IsParameterControlledByCurveID();
		Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_IsParameterControlledByCurveID();

		//System.Boolean UnityEngine.Experimental.Director.AnimatorControllerPlayable::IsParameterControlledByCurveString(System.String)
		void Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_IsParameterControlledByCurveString();
		Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_IsParameterControlledByCurveString();

		//System.Int32 UnityEngine.Experimental.Director.AnimatorControllerPlayable::GetIntegerID(System.Int32)
		void Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_GetIntegerID();
		Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_GetIntegerID();

		//System.Int32 UnityEngine.Experimental.Director.AnimatorControllerPlayable::GetIntegerString(System.String)
		void Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_GetIntegerString();
		Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_GetIntegerString();

		//System.Int32 UnityEngine.Experimental.Director.AnimatorControllerPlayable::GetLayerIndex(System.String)
		void Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_GetLayerIndex();
		Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_GetLayerIndex();

		//System.Int32 UnityEngine.Experimental.Director.AnimatorControllerPlayable::StringToHash(System.String)
		void Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_StringToHash();
		Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_StringToHash();

		//System.Int32 UnityEngine.Experimental.Director.AnimatorControllerPlayable::get_layerCount()
		void Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_get_layerCount();
		Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_get_layerCount();

		//System.Int32 UnityEngine.Experimental.Director.AnimatorControllerPlayable::get_parameterCount()
		void Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_get_parameterCount();
		Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_get_parameterCount();

		//System.Single UnityEngine.Experimental.Director.AnimatorControllerPlayable::GetFloatID(System.Int32)
		void Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_GetFloatID();
		Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_GetFloatID();

		//System.Single UnityEngine.Experimental.Director.AnimatorControllerPlayable::GetFloatString(System.String)
		void Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_GetFloatString();
		Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_GetFloatString();

		//System.Single UnityEngine.Experimental.Director.AnimatorControllerPlayable::GetLayerWeight(System.Int32)
		void Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_GetLayerWeight();
		Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_GetLayerWeight();

		//System.String UnityEngine.Experimental.Director.AnimatorControllerPlayable::GetLayerName(System.Int32)
		void Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_GetLayerName();
		Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_GetLayerName();

		//System.Void UnityEngine.Experimental.Director.AnimatorControllerPlayable::CrossFade(System.Int32,System.Single,System.Int32,System.Single)
		void Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_CrossFade();
		Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_CrossFade();

		//System.Void UnityEngine.Experimental.Director.AnimatorControllerPlayable::CrossFadeInFixedTime(System.Int32,System.Single,System.Int32,System.Single)
		void Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_CrossFadeInFixedTime();
		Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_CrossFadeInFixedTime();

		//System.Void UnityEngine.Experimental.Director.AnimatorControllerPlayable::Play(System.Int32,System.Int32,System.Single)
		void Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_Play();
		Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_Play();

		//System.Void UnityEngine.Experimental.Director.AnimatorControllerPlayable::PlayInFixedTime(System.Int32,System.Int32,System.Single)
		void Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_PlayInFixedTime();
		Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_PlayInFixedTime();

		//System.Void UnityEngine.Experimental.Director.AnimatorControllerPlayable::ResetTriggerID(System.Int32)
		void Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_ResetTriggerID();
		Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_ResetTriggerID();

		//System.Void UnityEngine.Experimental.Director.AnimatorControllerPlayable::ResetTriggerString(System.String)
		void Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_ResetTriggerString();
		Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_ResetTriggerString();

		//System.Void UnityEngine.Experimental.Director.AnimatorControllerPlayable::SetBoolID(System.Int32,System.Boolean)
		void Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_SetBoolID();
		Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_SetBoolID();

		//System.Void UnityEngine.Experimental.Director.AnimatorControllerPlayable::SetBoolString(System.String,System.Boolean)
		void Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_SetBoolString();
		Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_SetBoolString();

		//System.Void UnityEngine.Experimental.Director.AnimatorControllerPlayable::SetFloatID(System.Int32,System.Single)
		void Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_SetFloatID();
		Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_SetFloatID();

		//System.Void UnityEngine.Experimental.Director.AnimatorControllerPlayable::SetFloatString(System.String,System.Single)
		void Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_SetFloatString();
		Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_SetFloatString();

		//System.Void UnityEngine.Experimental.Director.AnimatorControllerPlayable::SetIntegerID(System.Int32,System.Int32)
		void Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_SetIntegerID();
		Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_SetIntegerID();

		//System.Void UnityEngine.Experimental.Director.AnimatorControllerPlayable::SetIntegerString(System.String,System.Int32)
		void Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_SetIntegerString();
		Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_SetIntegerString();

		//System.Void UnityEngine.Experimental.Director.AnimatorControllerPlayable::SetLayerWeight(System.Int32,System.Single)
		void Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_SetLayerWeight();
		Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_SetLayerWeight();

		//System.Void UnityEngine.Experimental.Director.AnimatorControllerPlayable::SetTriggerID(System.Int32)
		void Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_SetTriggerID();
		Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_SetTriggerID();

		//System.Void UnityEngine.Experimental.Director.AnimatorControllerPlayable::SetTriggerString(System.String)
		void Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_SetTriggerString();
		Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_SetTriggerString();

		//UnityEngine.AnimatorClipInfo[] UnityEngine.Experimental.Director.AnimatorControllerPlayable::GetCurrentAnimatorClipInfo(System.Int32)
		void Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_GetCurrentAnimatorClipInfo();
		Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_GetCurrentAnimatorClipInfo();

		//UnityEngine.AnimatorClipInfo[] UnityEngine.Experimental.Director.AnimatorControllerPlayable::GetNextAnimatorClipInfo(System.Int32)
		void Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_GetNextAnimatorClipInfo();
		Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_GetNextAnimatorClipInfo();

		//UnityEngine.AnimatorControllerParameter[] UnityEngine.Experimental.Director.AnimatorControllerPlayable::get_parameters()
		void Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_get_parameters();
		Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_get_parameters();

		//UnityEngine.AnimatorStateInfo UnityEngine.Experimental.Director.AnimatorControllerPlayable::GetCurrentAnimatorStateInfo(System.Int32)
		void Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_GetCurrentAnimatorStateInfo();
		Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_GetCurrentAnimatorStateInfo();

		//UnityEngine.AnimatorStateInfo UnityEngine.Experimental.Director.AnimatorControllerPlayable::GetNextAnimatorStateInfo(System.Int32)
		void Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_GetNextAnimatorStateInfo();
		Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_GetNextAnimatorStateInfo();

		//UnityEngine.AnimatorTransitionInfo UnityEngine.Experimental.Director.AnimatorControllerPlayable::GetAnimatorTransitionInfo(System.Int32)
		void Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_GetAnimatorTransitionInfo();
		Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_GetAnimatorTransitionInfo();

	//End Registrations for type : UnityEngine.Experimental.Director.AnimatorControllerPlayable

	//Start Registrations for type : UnityEngine.Experimental.Director.DirectorPlayer

		//System.Double UnityEngine.Experimental.Director.DirectorPlayer::GetTime()
		void Register_UnityEngine_Experimental_Director_DirectorPlayer_GetTime();
		Register_UnityEngine_Experimental_Director_DirectorPlayer_GetTime();

		//System.Void UnityEngine.Experimental.Director.DirectorPlayer::PlayInternal(UnityEngine.Experimental.Director.Playable,System.Object)
		void Register_UnityEngine_Experimental_Director_DirectorPlayer_PlayInternal();
		Register_UnityEngine_Experimental_Director_DirectorPlayer_PlayInternal();

		//System.Void UnityEngine.Experimental.Director.DirectorPlayer::SetTime(System.Double)
		void Register_UnityEngine_Experimental_Director_DirectorPlayer_SetTime();
		Register_UnityEngine_Experimental_Director_DirectorPlayer_SetTime();

		//System.Void UnityEngine.Experimental.Director.DirectorPlayer::SetTimeUpdateMode(UnityEngine.Experimental.Director.DirectorUpdateMode)
		void Register_UnityEngine_Experimental_Director_DirectorPlayer_SetTimeUpdateMode();
		Register_UnityEngine_Experimental_Director_DirectorPlayer_SetTimeUpdateMode();

		//System.Void UnityEngine.Experimental.Director.DirectorPlayer::Stop()
		void Register_UnityEngine_Experimental_Director_DirectorPlayer_Stop();
		Register_UnityEngine_Experimental_Director_DirectorPlayer_Stop();

		//UnityEngine.Experimental.Director.DirectorUpdateMode UnityEngine.Experimental.Director.DirectorPlayer::GetTimeUpdateMode()
		void Register_UnityEngine_Experimental_Director_DirectorPlayer_GetTimeUpdateMode();
		Register_UnityEngine_Experimental_Director_DirectorPlayer_GetTimeUpdateMode();

	//End Registrations for type : UnityEngine.Experimental.Director.DirectorPlayer

	//Start Registrations for type : UnityEngine.Experimental.Director.Playable

		//System.Boolean UnityEngine.Experimental.Director.Playable::ConnectInternal(UnityEngine.Experimental.Director.Playable,UnityEngine.Experimental.Director.Playable,System.Int32,System.Int32)
		void Register_UnityEngine_Experimental_Director_Playable_ConnectInternal();
		Register_UnityEngine_Experimental_Director_Playable_ConnectInternal();

		//System.Boolean UnityEngine.Experimental.Director.Playable::SetInputWeightInternal(System.Int32,System.Single)
		void Register_UnityEngine_Experimental_Director_Playable_SetInputWeightInternal();
		Register_UnityEngine_Experimental_Director_Playable_SetInputWeightInternal();

		//System.Double UnityEngine.Experimental.Director.Playable::get_time()
		void Register_UnityEngine_Experimental_Director_Playable_get_time();
		Register_UnityEngine_Experimental_Director_Playable_get_time();

		//System.Int32 UnityEngine.Experimental.Director.Playable::GenerateUniqueId()
		void Register_UnityEngine_Experimental_Director_Playable_GenerateUniqueId();
		Register_UnityEngine_Experimental_Director_Playable_GenerateUniqueId();

		//System.Int32 UnityEngine.Experimental.Director.Playable::GetUniqueIDInternal()
		void Register_UnityEngine_Experimental_Director_Playable_GetUniqueIDInternal();
		Register_UnityEngine_Experimental_Director_Playable_GetUniqueIDInternal();

		//System.Int32 UnityEngine.Experimental.Director.Playable::get_inputCount()
		void Register_UnityEngine_Experimental_Director_Playable_get_inputCount();
		Register_UnityEngine_Experimental_Director_Playable_get_inputCount();

		//System.Int32 UnityEngine.Experimental.Director.Playable::get_outputCount()
		void Register_UnityEngine_Experimental_Director_Playable_get_outputCount();
		Register_UnityEngine_Experimental_Director_Playable_get_outputCount();

		//System.Single UnityEngine.Experimental.Director.Playable::GetInputWeightInternal(System.Int32)
		void Register_UnityEngine_Experimental_Director_Playable_GetInputWeightInternal();
		Register_UnityEngine_Experimental_Director_Playable_GetInputWeightInternal();

		//System.Void UnityEngine.Experimental.Director.Playable::ClearInputs()
		void Register_UnityEngine_Experimental_Director_Playable_ClearInputs();
		Register_UnityEngine_Experimental_Director_Playable_ClearInputs();

		//System.Void UnityEngine.Experimental.Director.Playable::DisconnectInternal(UnityEngine.Experimental.Director.Playable,System.Int32)
		void Register_UnityEngine_Experimental_Director_Playable_DisconnectInternal();
		Register_UnityEngine_Experimental_Director_Playable_DisconnectInternal();

		//System.Void UnityEngine.Experimental.Director.Playable::GetInputsInternal(System.Object)
		void Register_UnityEngine_Experimental_Director_Playable_GetInputsInternal();
		Register_UnityEngine_Experimental_Director_Playable_GetInputsInternal();

		//System.Void UnityEngine.Experimental.Director.Playable::GetOutputsInternal(System.Object)
		void Register_UnityEngine_Experimental_Director_Playable_GetOutputsInternal();
		Register_UnityEngine_Experimental_Director_Playable_GetOutputsInternal();

		//System.Void UnityEngine.Experimental.Director.Playable::InstantiateEnginePlayable()
		void Register_UnityEngine_Experimental_Director_Playable_InstantiateEnginePlayable();
		Register_UnityEngine_Experimental_Director_Playable_InstantiateEnginePlayable();

		//System.Void UnityEngine.Experimental.Director.Playable::ReleaseEnginePlayable()
		void Register_UnityEngine_Experimental_Director_Playable_ReleaseEnginePlayable();
		Register_UnityEngine_Experimental_Director_Playable_ReleaseEnginePlayable();

		//System.Void UnityEngine.Experimental.Director.Playable::set_state(UnityEngine.Experimental.Director.PlayState)
		void Register_UnityEngine_Experimental_Director_Playable_set_state();
		Register_UnityEngine_Experimental_Director_Playable_set_state();

		//System.Void UnityEngine.Experimental.Director.Playable::set_time(System.Double)
		void Register_UnityEngine_Experimental_Director_Playable_set_time();
		Register_UnityEngine_Experimental_Director_Playable_set_time();

		//UnityEngine.Experimental.Director.PlayState UnityEngine.Experimental.Director.Playable::get_state()
		void Register_UnityEngine_Experimental_Director_Playable_get_state();
		Register_UnityEngine_Experimental_Director_Playable_get_state();

		//UnityEngine.Experimental.Director.Playable UnityEngine.Experimental.Director.Playable::GetInput(System.Int32)
		void Register_UnityEngine_Experimental_Director_Playable_GetInput();
		Register_UnityEngine_Experimental_Director_Playable_GetInput();

		//UnityEngine.Experimental.Director.Playable UnityEngine.Experimental.Director.Playable::GetOutput(System.Int32)
		void Register_UnityEngine_Experimental_Director_Playable_GetOutput();
		Register_UnityEngine_Experimental_Director_Playable_GetOutput();

		//UnityEngine.Experimental.Director.Playable[] UnityEngine.Experimental.Director.Playable::GetInputs()
		void Register_UnityEngine_Experimental_Director_Playable_GetInputs();
		Register_UnityEngine_Experimental_Director_Playable_GetInputs();

		//UnityEngine.Experimental.Director.Playable[] UnityEngine.Experimental.Director.Playable::GetOutputs()
		void Register_UnityEngine_Experimental_Director_Playable_GetOutputs();
		Register_UnityEngine_Experimental_Director_Playable_GetOutputs();

	//End Registrations for type : UnityEngine.Experimental.Director.Playable

	//Start Registrations for type : UnityEngine.Experimental.Networking.DownloadHandler

		//System.Void UnityEngine.Experimental.Networking.DownloadHandler::InternalCreateString()
		void Register_UnityEngine_Experimental_Networking_DownloadHandler_InternalCreateString();
		Register_UnityEngine_Experimental_Networking_DownloadHandler_InternalCreateString();

		//System.Void UnityEngine.Experimental.Networking.DownloadHandler::InternalDestroy()
		void Register_UnityEngine_Experimental_Networking_DownloadHandler_InternalDestroy();
		Register_UnityEngine_Experimental_Networking_DownloadHandler_InternalDestroy();

	//End Registrations for type : UnityEngine.Experimental.Networking.DownloadHandler

	//Start Registrations for type : UnityEngine.Experimental.Networking.DownloadHandlerBuffer

		//System.Byte[] UnityEngine.Experimental.Networking.DownloadHandlerBuffer::InternalGetData()
		void Register_UnityEngine_Experimental_Networking_DownloadHandlerBuffer_InternalGetData();
		Register_UnityEngine_Experimental_Networking_DownloadHandlerBuffer_InternalGetData();

		//System.String UnityEngine.Experimental.Networking.DownloadHandlerBuffer::InternalGetText()
		void Register_UnityEngine_Experimental_Networking_DownloadHandlerBuffer_InternalGetText();
		Register_UnityEngine_Experimental_Networking_DownloadHandlerBuffer_InternalGetText();

	//End Registrations for type : UnityEngine.Experimental.Networking.DownloadHandlerBuffer

	//Start Registrations for type : UnityEngine.Experimental.Networking.UnityWebRequest

		//System.Boolean UnityEngine.Experimental.Networking.UnityWebRequest::get_isError()
		void Register_UnityEngine_Experimental_Networking_UnityWebRequest_get_isError();
		Register_UnityEngine_Experimental_Networking_UnityWebRequest_get_isError();

		//System.Void UnityEngine.Experimental.Networking.UnityWebRequest::InternalCreate()
		void Register_UnityEngine_Experimental_Networking_UnityWebRequest_InternalCreate();
		Register_UnityEngine_Experimental_Networking_UnityWebRequest_InternalCreate();

		//System.Void UnityEngine.Experimental.Networking.UnityWebRequest::InternalDestroy()
		void Register_UnityEngine_Experimental_Networking_UnityWebRequest_InternalDestroy();
		Register_UnityEngine_Experimental_Networking_UnityWebRequest_InternalDestroy();

		//System.Void UnityEngine.Experimental.Networking.UnityWebRequest::InternalSetCustomMethod(System.String)
		void Register_UnityEngine_Experimental_Networking_UnityWebRequest_InternalSetCustomMethod();
		Register_UnityEngine_Experimental_Networking_UnityWebRequest_InternalSetCustomMethod();

		//System.Void UnityEngine.Experimental.Networking.UnityWebRequest::InternalSetMethod(UnityEngine.Experimental.Networking.UnityWebRequest/UnityWebRequestMethod)
		void Register_UnityEngine_Experimental_Networking_UnityWebRequest_InternalSetMethod();
		Register_UnityEngine_Experimental_Networking_UnityWebRequest_InternalSetMethod();

		//System.Void UnityEngine.Experimental.Networking.UnityWebRequest::InternalSetRequestHeader(System.String,System.String)
		void Register_UnityEngine_Experimental_Networking_UnityWebRequest_InternalSetRequestHeader();
		Register_UnityEngine_Experimental_Networking_UnityWebRequest_InternalSetRequestHeader();

		//System.Void UnityEngine.Experimental.Networking.UnityWebRequest::InternalSetUrl(System.String)
		void Register_UnityEngine_Experimental_Networking_UnityWebRequest_InternalSetUrl();
		Register_UnityEngine_Experimental_Networking_UnityWebRequest_InternalSetUrl();

		//System.Void UnityEngine.Experimental.Networking.UnityWebRequest::set_downloadHandler(UnityEngine.Experimental.Networking.DownloadHandler)
		void Register_UnityEngine_Experimental_Networking_UnityWebRequest_set_downloadHandler();
		Register_UnityEngine_Experimental_Networking_UnityWebRequest_set_downloadHandler();

		//System.Void UnityEngine.Experimental.Networking.UnityWebRequest::set_uploadHandler(UnityEngine.Experimental.Networking.UploadHandler)
		void Register_UnityEngine_Experimental_Networking_UnityWebRequest_set_uploadHandler();
		Register_UnityEngine_Experimental_Networking_UnityWebRequest_set_uploadHandler();

		//UnityEngine.AsyncOperation UnityEngine.Experimental.Networking.UnityWebRequest::InternalBegin()
		void Register_UnityEngine_Experimental_Networking_UnityWebRequest_InternalBegin();
		Register_UnityEngine_Experimental_Networking_UnityWebRequest_InternalBegin();

		//UnityEngine.Experimental.Networking.DownloadHandler UnityEngine.Experimental.Networking.UnityWebRequest::get_downloadHandler()
		void Register_UnityEngine_Experimental_Networking_UnityWebRequest_get_downloadHandler();
		Register_UnityEngine_Experimental_Networking_UnityWebRequest_get_downloadHandler();

		//UnityEngine.Experimental.Networking.UploadHandler UnityEngine.Experimental.Networking.UnityWebRequest::get_uploadHandler()
		void Register_UnityEngine_Experimental_Networking_UnityWebRequest_get_uploadHandler();
		Register_UnityEngine_Experimental_Networking_UnityWebRequest_get_uploadHandler();

	//End Registrations for type : UnityEngine.Experimental.Networking.UnityWebRequest

	//Start Registrations for type : UnityEngine.Experimental.Networking.UploadHandler

		//System.Void UnityEngine.Experimental.Networking.UploadHandler::InternalDestroy()
		void Register_UnityEngine_Experimental_Networking_UploadHandler_InternalDestroy();
		Register_UnityEngine_Experimental_Networking_UploadHandler_InternalDestroy();

	//End Registrations for type : UnityEngine.Experimental.Networking.UploadHandler

	//Start Registrations for type : UnityEngine.Font

		//System.Boolean UnityEngine.Font::GetCharacterInfo(System.Char,UnityEngine.CharacterInfo&,System.Int32,UnityEngine.FontStyle)
		void Register_UnityEngine_Font_GetCharacterInfo();
		Register_UnityEngine_Font_GetCharacterInfo();

		//System.Boolean UnityEngine.Font::HasCharacter(System.Char)
		void Register_UnityEngine_Font_HasCharacter();
		Register_UnityEngine_Font_HasCharacter();

		//System.Boolean UnityEngine.Font::get_dynamic()
		void Register_UnityEngine_Font_get_dynamic();
		Register_UnityEngine_Font_get_dynamic();

		//System.Int32 UnityEngine.Font::get_ascent()
		void Register_UnityEngine_Font_get_ascent();
		Register_UnityEngine_Font_get_ascent();

		//System.Int32 UnityEngine.Font::get_fontSize()
		void Register_UnityEngine_Font_get_fontSize();
		Register_UnityEngine_Font_get_fontSize();

		//System.Int32 UnityEngine.Font::get_lineHeight()
		void Register_UnityEngine_Font_get_lineHeight();
		Register_UnityEngine_Font_get_lineHeight();

		//System.String[] UnityEngine.Font::GetOSInstalledFontNames()
		void Register_UnityEngine_Font_GetOSInstalledFontNames();
		Register_UnityEngine_Font_GetOSInstalledFontNames();

		//System.String[] UnityEngine.Font::get_fontNames()
		void Register_UnityEngine_Font_get_fontNames();
		Register_UnityEngine_Font_get_fontNames();

		//System.Void UnityEngine.Font::Internal_CreateDynamicFont(UnityEngine.Font,System.String[],System.Int32)
		void Register_UnityEngine_Font_Internal_CreateDynamicFont();
		Register_UnityEngine_Font_Internal_CreateDynamicFont();

		//System.Void UnityEngine.Font::Internal_CreateFont(UnityEngine.Font,System.String)
		void Register_UnityEngine_Font_Internal_CreateFont();
		Register_UnityEngine_Font_Internal_CreateFont();

		//System.Void UnityEngine.Font::RequestCharactersInTexture(System.String,System.Int32,UnityEngine.FontStyle)
		void Register_UnityEngine_Font_RequestCharactersInTexture();
		Register_UnityEngine_Font_RequestCharactersInTexture();

		//System.Void UnityEngine.Font::set_characterInfo(UnityEngine.CharacterInfo[])
		void Register_UnityEngine_Font_set_characterInfo();
		Register_UnityEngine_Font_set_characterInfo();

		//System.Void UnityEngine.Font::set_fontNames(System.String[])
		void Register_UnityEngine_Font_set_fontNames();
		Register_UnityEngine_Font_set_fontNames();

		//System.Void UnityEngine.Font::set_material(UnityEngine.Material)
		void Register_UnityEngine_Font_set_material();
		Register_UnityEngine_Font_set_material();

		//UnityEngine.CharacterInfo[] UnityEngine.Font::get_characterInfo()
		void Register_UnityEngine_Font_get_characterInfo();
		Register_UnityEngine_Font_get_characterInfo();

		//UnityEngine.Material UnityEngine.Font::get_material()
		void Register_UnityEngine_Font_get_material();
		Register_UnityEngine_Font_get_material();

	//End Registrations for type : UnityEngine.Font

	//Start Registrations for type : UnityEngine.GameObject

		//System.Array UnityEngine.GameObject::GetComponentsInternal(System.Type,System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.Object)
		void Register_UnityEngine_GameObject_GetComponentsInternal();
		Register_UnityEngine_GameObject_GetComponentsInternal();

		//System.Boolean UnityEngine.GameObject::CompareTag(System.String)
		void Register_UnityEngine_GameObject_CompareTag();
		Register_UnityEngine_GameObject_CompareTag();

		//System.Boolean UnityEngine.GameObject::get_activeInHierarchy()
		void Register_UnityEngine_GameObject_get_activeInHierarchy();
		Register_UnityEngine_GameObject_get_activeInHierarchy();

		//System.Boolean UnityEngine.GameObject::get_activeSelf()
		void Register_UnityEngine_GameObject_get_activeSelf();
		Register_UnityEngine_GameObject_get_activeSelf();

		//System.Boolean UnityEngine.GameObject::get_isStatic()
		void Register_UnityEngine_GameObject_get_isStatic();
		Register_UnityEngine_GameObject_get_isStatic();

		//System.Boolean UnityEngine.GameObject::get_isStaticBatchable()
		void Register_UnityEngine_GameObject_get_isStaticBatchable();
		Register_UnityEngine_GameObject_get_isStaticBatchable();

		//System.Int32 UnityEngine.GameObject::get_layer()
		void Register_UnityEngine_GameObject_get_layer();
		Register_UnityEngine_GameObject_get_layer();

		//System.String UnityEngine.GameObject::get_tag()
		void Register_UnityEngine_GameObject_get_tag();
		Register_UnityEngine_GameObject_get_tag();

		//System.Void UnityEngine.GameObject::BroadcastMessage(System.String,System.Object,UnityEngine.SendMessageOptions)
		void Register_UnityEngine_GameObject_BroadcastMessage();
		Register_UnityEngine_GameObject_BroadcastMessage();

		//System.Void UnityEngine.GameObject::GetComponentFastPath(System.Type,System.IntPtr)
		void Register_UnityEngine_GameObject_GetComponentFastPath();
		Register_UnityEngine_GameObject_GetComponentFastPath();

		//System.Void UnityEngine.GameObject::INTERNAL_get_scene(UnityEngine.SceneManagement.Scene&)
		void Register_UnityEngine_GameObject_INTERNAL_get_scene();
		Register_UnityEngine_GameObject_INTERNAL_get_scene();

		//System.Void UnityEngine.GameObject::Internal_CreateGameObject(UnityEngine.GameObject,System.String)
		void Register_UnityEngine_GameObject_Internal_CreateGameObject();
		Register_UnityEngine_GameObject_Internal_CreateGameObject();

		//System.Void UnityEngine.GameObject::SendMessage(System.String,System.Object,UnityEngine.SendMessageOptions)
		void Register_UnityEngine_GameObject_SendMessage();
		Register_UnityEngine_GameObject_SendMessage();

		//System.Void UnityEngine.GameObject::SendMessageUpwards(System.String,System.Object,UnityEngine.SendMessageOptions)
		void Register_UnityEngine_GameObject_SendMessageUpwards();
		Register_UnityEngine_GameObject_SendMessageUpwards();

		//System.Void UnityEngine.GameObject::SetActive(System.Boolean)
		void Register_UnityEngine_GameObject_SetActive();
		Register_UnityEngine_GameObject_SetActive();

		//System.Void UnityEngine.GameObject::set_isStatic(System.Boolean)
		void Register_UnityEngine_GameObject_set_isStatic();
		Register_UnityEngine_GameObject_set_isStatic();

		//System.Void UnityEngine.GameObject::set_layer(System.Int32)
		void Register_UnityEngine_GameObject_set_layer();
		Register_UnityEngine_GameObject_set_layer();

		//System.Void UnityEngine.GameObject::set_tag(System.String)
		void Register_UnityEngine_GameObject_set_tag();
		Register_UnityEngine_GameObject_set_tag();

		//UnityEngine.Component UnityEngine.GameObject::GetComponent(System.Type)
		void Register_UnityEngine_GameObject_GetComponent();
		Register_UnityEngine_GameObject_GetComponent();

		//UnityEngine.Component UnityEngine.GameObject::GetComponentByName(System.String)
		void Register_UnityEngine_GameObject_GetComponentByName();
		Register_UnityEngine_GameObject_GetComponentByName();

		//UnityEngine.Component UnityEngine.GameObject::GetComponentInChildren(System.Type,System.Boolean)
		void Register_UnityEngine_GameObject_GetComponentInChildren();
		Register_UnityEngine_GameObject_GetComponentInChildren();

		//UnityEngine.Component UnityEngine.GameObject::GetComponentInParent(System.Type)
		void Register_UnityEngine_GameObject_GetComponentInParent();
		Register_UnityEngine_GameObject_GetComponentInParent();

		//UnityEngine.Component UnityEngine.GameObject::Internal_AddComponentWithType(System.Type)
		void Register_UnityEngine_GameObject_Internal_AddComponentWithType();
		Register_UnityEngine_GameObject_Internal_AddComponentWithType();

		//UnityEngine.GameObject UnityEngine.GameObject::CreatePrimitive(UnityEngine.PrimitiveType)
		void Register_UnityEngine_GameObject_CreatePrimitive();
		Register_UnityEngine_GameObject_CreatePrimitive();

		//UnityEngine.GameObject UnityEngine.GameObject::Find(System.String)
		void Register_UnityEngine_GameObject_Find();
		Register_UnityEngine_GameObject_Find();

		//UnityEngine.GameObject UnityEngine.GameObject::FindGameObjectWithTag(System.String)
		void Register_UnityEngine_GameObject_FindGameObjectWithTag();
		Register_UnityEngine_GameObject_FindGameObjectWithTag();

		//UnityEngine.GameObject[] UnityEngine.GameObject::FindGameObjectsWithTag(System.String)
		void Register_UnityEngine_GameObject_FindGameObjectsWithTag();
		Register_UnityEngine_GameObject_FindGameObjectsWithTag();

		//UnityEngine.Transform UnityEngine.GameObject::get_transform()
		void Register_UnityEngine_GameObject_get_transform();
		Register_UnityEngine_GameObject_get_transform();

	//End Registrations for type : UnityEngine.GameObject

	//Start Registrations for type : UnityEngine.GeometryUtility

		//System.Boolean UnityEngine.GeometryUtility::INTERNAL_CALL_TestPlanesAABB(UnityEngine.Plane[],UnityEngine.Bounds&)
		void Register_UnityEngine_GeometryUtility_INTERNAL_CALL_TestPlanesAABB();
		Register_UnityEngine_GeometryUtility_INTERNAL_CALL_TestPlanesAABB();

		//System.Void UnityEngine.GeometryUtility::INTERNAL_CALL_Internal_ExtractPlanes(UnityEngine.Plane[],UnityEngine.Matrix4x4&)
		void Register_UnityEngine_GeometryUtility_INTERNAL_CALL_Internal_ExtractPlanes();
		Register_UnityEngine_GeometryUtility_INTERNAL_CALL_Internal_ExtractPlanes();

	//End Registrations for type : UnityEngine.GeometryUtility

	//Start Registrations for type : UnityEngine.Gizmos

		//System.Void UnityEngine.Gizmos::INTERNAL_CALL_DrawCube(UnityEngine.Vector3&,UnityEngine.Vector3&)
		void Register_UnityEngine_Gizmos_INTERNAL_CALL_DrawCube();
		Register_UnityEngine_Gizmos_INTERNAL_CALL_DrawCube();

		//System.Void UnityEngine.Gizmos::INTERNAL_CALL_DrawFrustum(UnityEngine.Vector3&,System.Single,System.Single,System.Single,System.Single)
		void Register_UnityEngine_Gizmos_INTERNAL_CALL_DrawFrustum();
		Register_UnityEngine_Gizmos_INTERNAL_CALL_DrawFrustum();

		//System.Void UnityEngine.Gizmos::INTERNAL_CALL_DrawGUITexture(UnityEngine.Rect&,UnityEngine.Texture,System.Int32,System.Int32,System.Int32,System.Int32,UnityEngine.Material)
		void Register_UnityEngine_Gizmos_INTERNAL_CALL_DrawGUITexture();
		Register_UnityEngine_Gizmos_INTERNAL_CALL_DrawGUITexture();

		//System.Void UnityEngine.Gizmos::INTERNAL_CALL_DrawIcon(UnityEngine.Vector3&,System.String,System.Boolean)
		void Register_UnityEngine_Gizmos_INTERNAL_CALL_DrawIcon();
		Register_UnityEngine_Gizmos_INTERNAL_CALL_DrawIcon();

		//System.Void UnityEngine.Gizmos::INTERNAL_CALL_DrawLine(UnityEngine.Vector3&,UnityEngine.Vector3&)
		void Register_UnityEngine_Gizmos_INTERNAL_CALL_DrawLine();
		Register_UnityEngine_Gizmos_INTERNAL_CALL_DrawLine();

		//System.Void UnityEngine.Gizmos::INTERNAL_CALL_DrawMesh(UnityEngine.Mesh,System.Int32,UnityEngine.Vector3&,UnityEngine.Quaternion&,UnityEngine.Vector3&)
		void Register_UnityEngine_Gizmos_INTERNAL_CALL_DrawMesh();
		Register_UnityEngine_Gizmos_INTERNAL_CALL_DrawMesh();

		//System.Void UnityEngine.Gizmos::INTERNAL_CALL_DrawSphere(UnityEngine.Vector3&,System.Single)
		void Register_UnityEngine_Gizmos_INTERNAL_CALL_DrawSphere();
		Register_UnityEngine_Gizmos_INTERNAL_CALL_DrawSphere();

		//System.Void UnityEngine.Gizmos::INTERNAL_CALL_DrawWireCube(UnityEngine.Vector3&,UnityEngine.Vector3&)
		void Register_UnityEngine_Gizmos_INTERNAL_CALL_DrawWireCube();
		Register_UnityEngine_Gizmos_INTERNAL_CALL_DrawWireCube();

		//System.Void UnityEngine.Gizmos::INTERNAL_CALL_DrawWireMesh(UnityEngine.Mesh,System.Int32,UnityEngine.Vector3&,UnityEngine.Quaternion&,UnityEngine.Vector3&)
		void Register_UnityEngine_Gizmos_INTERNAL_CALL_DrawWireMesh();
		Register_UnityEngine_Gizmos_INTERNAL_CALL_DrawWireMesh();

		//System.Void UnityEngine.Gizmos::INTERNAL_CALL_DrawWireSphere(UnityEngine.Vector3&,System.Single)
		void Register_UnityEngine_Gizmos_INTERNAL_CALL_DrawWireSphere();
		Register_UnityEngine_Gizmos_INTERNAL_CALL_DrawWireSphere();

		//System.Void UnityEngine.Gizmos::INTERNAL_get_color(UnityEngine.Color&)
		void Register_UnityEngine_Gizmos_INTERNAL_get_color();
		Register_UnityEngine_Gizmos_INTERNAL_get_color();

		//System.Void UnityEngine.Gizmos::INTERNAL_get_matrix(UnityEngine.Matrix4x4&)
		void Register_UnityEngine_Gizmos_INTERNAL_get_matrix();
		Register_UnityEngine_Gizmos_INTERNAL_get_matrix();

		//System.Void UnityEngine.Gizmos::INTERNAL_set_color(UnityEngine.Color&)
		void Register_UnityEngine_Gizmos_INTERNAL_set_color();
		Register_UnityEngine_Gizmos_INTERNAL_set_color();

		//System.Void UnityEngine.Gizmos::INTERNAL_set_matrix(UnityEngine.Matrix4x4&)
		void Register_UnityEngine_Gizmos_INTERNAL_set_matrix();
		Register_UnityEngine_Gizmos_INTERNAL_set_matrix();

	//End Registrations for type : UnityEngine.Gizmos

	//Start Registrations for type : UnityEngine.GL

		//System.Boolean UnityEngine.GL::get_invertCulling()
		void Register_UnityEngine_GL_get_invertCulling();
		Register_UnityEngine_GL_get_invertCulling();

		//System.Boolean UnityEngine.GL::get_sRGBWrite()
		void Register_UnityEngine_GL_get_sRGBWrite();
		Register_UnityEngine_GL_get_sRGBWrite();

		//System.Boolean UnityEngine.GL::get_wireframe()
		void Register_UnityEngine_GL_get_wireframe();
		Register_UnityEngine_GL_get_wireframe();

		//System.Void UnityEngine.GL::Begin(System.Int32)
		void Register_UnityEngine_GL_Begin();
		Register_UnityEngine_GL_Begin();

		//System.Void UnityEngine.GL::ClearWithSkybox(System.Boolean,UnityEngine.Camera)
		void Register_UnityEngine_GL_ClearWithSkybox();
		Register_UnityEngine_GL_ClearWithSkybox();

		//System.Void UnityEngine.GL::End()
		void Register_UnityEngine_GL_End();
		Register_UnityEngine_GL_End();

		//System.Void UnityEngine.GL::INTERNAL_CALL_Color(UnityEngine.Color&)
		void Register_UnityEngine_GL_INTERNAL_CALL_Color();
		Register_UnityEngine_GL_INTERNAL_CALL_Color();

		//System.Void UnityEngine.GL::INTERNAL_CALL_GetGPUProjectionMatrix(UnityEngine.Matrix4x4&,System.Boolean,UnityEngine.Matrix4x4&)
		void Register_UnityEngine_GL_INTERNAL_CALL_GetGPUProjectionMatrix();
		Register_UnityEngine_GL_INTERNAL_CALL_GetGPUProjectionMatrix();

		//System.Void UnityEngine.GL::INTERNAL_CALL_Internal_Clear(System.Boolean,System.Boolean,UnityEngine.Color&,System.Single)
		void Register_UnityEngine_GL_INTERNAL_CALL_Internal_Clear();
		Register_UnityEngine_GL_INTERNAL_CALL_Internal_Clear();

		//System.Void UnityEngine.GL::INTERNAL_CALL_LoadProjectionMatrix(UnityEngine.Matrix4x4&)
		void Register_UnityEngine_GL_INTERNAL_CALL_LoadProjectionMatrix();
		Register_UnityEngine_GL_INTERNAL_CALL_LoadProjectionMatrix();

		//System.Void UnityEngine.GL::INTERNAL_CALL_MultMatrix(UnityEngine.Matrix4x4&)
		void Register_UnityEngine_GL_INTERNAL_CALL_MultMatrix();
		Register_UnityEngine_GL_INTERNAL_CALL_MultMatrix();

		//System.Void UnityEngine.GL::INTERNAL_CALL_MultiTexCoord(System.Int32,UnityEngine.Vector3&)
		void Register_UnityEngine_GL_INTERNAL_CALL_MultiTexCoord();
		Register_UnityEngine_GL_INTERNAL_CALL_MultiTexCoord();

		//System.Void UnityEngine.GL::INTERNAL_CALL_TexCoord(UnityEngine.Vector3&)
		void Register_UnityEngine_GL_INTERNAL_CALL_TexCoord();
		Register_UnityEngine_GL_INTERNAL_CALL_TexCoord();

		//System.Void UnityEngine.GL::INTERNAL_CALL_Vertex(UnityEngine.Vector3&)
		void Register_UnityEngine_GL_INTERNAL_CALL_Vertex();
		Register_UnityEngine_GL_INTERNAL_CALL_Vertex();

		//System.Void UnityEngine.GL::INTERNAL_CALL_Viewport(UnityEngine.Rect&)
		void Register_UnityEngine_GL_INTERNAL_CALL_Viewport();
		Register_UnityEngine_GL_INTERNAL_CALL_Viewport();

		//System.Void UnityEngine.GL::INTERNAL_get_modelview(UnityEngine.Matrix4x4&)
		void Register_UnityEngine_GL_INTERNAL_get_modelview();
		Register_UnityEngine_GL_INTERNAL_get_modelview();

		//System.Void UnityEngine.GL::INTERNAL_set_modelview(UnityEngine.Matrix4x4&)
		void Register_UnityEngine_GL_INTERNAL_set_modelview();
		Register_UnityEngine_GL_INTERNAL_set_modelview();

		//System.Void UnityEngine.GL::InvalidateState()
		void Register_UnityEngine_GL_InvalidateState();
		Register_UnityEngine_GL_InvalidateState();

		//System.Void UnityEngine.GL::IssuePluginEventInternal(System.IntPtr,System.Int32)
		void Register_UnityEngine_GL_IssuePluginEventInternal();
		Register_UnityEngine_GL_IssuePluginEventInternal();

		//System.Void UnityEngine.GL::LoadIdentity()
		void Register_UnityEngine_GL_LoadIdentity();
		Register_UnityEngine_GL_LoadIdentity();

		//System.Void UnityEngine.GL::LoadOrtho()
		void Register_UnityEngine_GL_LoadOrtho();
		Register_UnityEngine_GL_LoadOrtho();

		//System.Void UnityEngine.GL::LoadPixelMatrix()
		void Register_UnityEngine_GL_LoadPixelMatrix();
		Register_UnityEngine_GL_LoadPixelMatrix();

		//System.Void UnityEngine.GL::LoadPixelMatrixArgs(System.Single,System.Single,System.Single,System.Single)
		void Register_UnityEngine_GL_LoadPixelMatrixArgs();
		Register_UnityEngine_GL_LoadPixelMatrixArgs();

		//System.Void UnityEngine.GL::MultiTexCoord2(System.Int32,System.Single,System.Single)
		void Register_UnityEngine_GL_MultiTexCoord2();
		Register_UnityEngine_GL_MultiTexCoord2();

		//System.Void UnityEngine.GL::MultiTexCoord3(System.Int32,System.Single,System.Single,System.Single)
		void Register_UnityEngine_GL_MultiTexCoord3();
		Register_UnityEngine_GL_MultiTexCoord3();

		//System.Void UnityEngine.GL::PopMatrix()
		void Register_UnityEngine_GL_PopMatrix();
		Register_UnityEngine_GL_PopMatrix();

		//System.Void UnityEngine.GL::PushMatrix()
		void Register_UnityEngine_GL_PushMatrix();
		Register_UnityEngine_GL_PushMatrix();

		//System.Void UnityEngine.GL::RenderTargetBarrier()
		void Register_UnityEngine_GL_RenderTargetBarrier();
		Register_UnityEngine_GL_RenderTargetBarrier();

		//System.Void UnityEngine.GL::TexCoord2(System.Single,System.Single)
		void Register_UnityEngine_GL_TexCoord2();
		Register_UnityEngine_GL_TexCoord2();

		//System.Void UnityEngine.GL::TexCoord3(System.Single,System.Single,System.Single)
		void Register_UnityEngine_GL_TexCoord3();
		Register_UnityEngine_GL_TexCoord3();

		//System.Void UnityEngine.GL::Vertex3(System.Single,System.Single,System.Single)
		void Register_UnityEngine_GL_Vertex3();
		Register_UnityEngine_GL_Vertex3();

		//System.Void UnityEngine.GL::set_invertCulling(System.Boolean)
		void Register_UnityEngine_GL_set_invertCulling();
		Register_UnityEngine_GL_set_invertCulling();

		//System.Void UnityEngine.GL::set_sRGBWrite(System.Boolean)
		void Register_UnityEngine_GL_set_sRGBWrite();
		Register_UnityEngine_GL_set_sRGBWrite();

		//System.Void UnityEngine.GL::set_wireframe(System.Boolean)
		void Register_UnityEngine_GL_set_wireframe();
		Register_UnityEngine_GL_set_wireframe();

	//End Registrations for type : UnityEngine.GL

	//Start Registrations for type : UnityEngine.Gradient

		//System.Void UnityEngine.Gradient::Cleanup()
		void Register_UnityEngine_Gradient_Cleanup();
		Register_UnityEngine_Gradient_Cleanup();

		//System.Void UnityEngine.Gradient::INTERNAL_CALL_Evaluate(UnityEngine.Gradient,System.Single,UnityEngine.Color&)
		void Register_UnityEngine_Gradient_INTERNAL_CALL_Evaluate();
		Register_UnityEngine_Gradient_INTERNAL_CALL_Evaluate();

		//System.Void UnityEngine.Gradient::Init()
		void Register_UnityEngine_Gradient_Init();
		Register_UnityEngine_Gradient_Init();

		//System.Void UnityEngine.Gradient::SetKeys(UnityEngine.GradientColorKey[],UnityEngine.GradientAlphaKey[])
		void Register_UnityEngine_Gradient_SetKeys();
		Register_UnityEngine_Gradient_SetKeys();

		//System.Void UnityEngine.Gradient::set_alphaKeys(UnityEngine.GradientAlphaKey[])
		void Register_UnityEngine_Gradient_set_alphaKeys();
		Register_UnityEngine_Gradient_set_alphaKeys();

		//System.Void UnityEngine.Gradient::set_colorKeys(UnityEngine.GradientColorKey[])
		void Register_UnityEngine_Gradient_set_colorKeys();
		Register_UnityEngine_Gradient_set_colorKeys();

		//UnityEngine.GradientAlphaKey[] UnityEngine.Gradient::get_alphaKeys()
		void Register_UnityEngine_Gradient_get_alphaKeys();
		Register_UnityEngine_Gradient_get_alphaKeys();

		//UnityEngine.GradientColorKey[] UnityEngine.Gradient::get_colorKeys()
		void Register_UnityEngine_Gradient_get_colorKeys();
		Register_UnityEngine_Gradient_get_colorKeys();

	//End Registrations for type : UnityEngine.Gradient

	//Start Registrations for type : UnityEngine.Graphics

		//System.Void UnityEngine.Graphics::Blit(UnityEngine.Texture,UnityEngine.RenderTexture)
		void Register_UnityEngine_Graphics_Blit();
		Register_UnityEngine_Graphics_Blit();

		//System.Void UnityEngine.Graphics::ClearRandomWriteTargets()
		void Register_UnityEngine_Graphics_ClearRandomWriteTargets();
		Register_UnityEngine_Graphics_ClearRandomWriteTargets();

		//System.Void UnityEngine.Graphics::DrawProcedural(UnityEngine.MeshTopology,System.Int32,System.Int32)
		void Register_UnityEngine_Graphics_DrawProcedural();
		Register_UnityEngine_Graphics_DrawProcedural();

		//System.Void UnityEngine.Graphics::DrawProceduralIndirect(UnityEngine.MeshTopology,UnityEngine.ComputeBuffer,System.Int32)
		void Register_UnityEngine_Graphics_DrawProceduralIndirect();
		Register_UnityEngine_Graphics_DrawProceduralIndirect();

		//System.Void UnityEngine.Graphics::DrawTexture(UnityEngine.InternalDrawTextureArguments&)
		void Register_UnityEngine_Graphics_DrawTexture();
		Register_UnityEngine_Graphics_DrawTexture();

		//System.Void UnityEngine.Graphics::ExecuteCommandBuffer(UnityEngine.Rendering.CommandBuffer)
		void Register_UnityEngine_Graphics_ExecuteCommandBuffer();
		Register_UnityEngine_Graphics_ExecuteCommandBuffer();

		//System.Void UnityEngine.Graphics::GetActiveColorBuffer(UnityEngine.RenderBuffer&)
		void Register_UnityEngine_Graphics_GetActiveColorBuffer();
		Register_UnityEngine_Graphics_GetActiveColorBuffer();

		//System.Void UnityEngine.Graphics::GetActiveDepthBuffer(UnityEngine.RenderBuffer&)
		void Register_UnityEngine_Graphics_GetActiveDepthBuffer();
		Register_UnityEngine_Graphics_GetActiveDepthBuffer();

		//System.Void UnityEngine.Graphics::INTERNAL_CALL_Internal_DrawMeshNow1(UnityEngine.Mesh,UnityEngine.Vector3&,UnityEngine.Quaternion&,System.Int32)
		void Register_UnityEngine_Graphics_INTERNAL_CALL_Internal_DrawMeshNow1();
		Register_UnityEngine_Graphics_INTERNAL_CALL_Internal_DrawMeshNow1();

		//System.Void UnityEngine.Graphics::INTERNAL_CALL_Internal_DrawMeshNow2(UnityEngine.Mesh,UnityEngine.Matrix4x4&,System.Int32)
		void Register_UnityEngine_Graphics_INTERNAL_CALL_Internal_DrawMeshNow2();
		Register_UnityEngine_Graphics_INTERNAL_CALL_Internal_DrawMeshNow2();

		//System.Void UnityEngine.Graphics::Internal_BlitMaterial(UnityEngine.Texture,UnityEngine.RenderTexture,UnityEngine.Material,System.Int32,System.Boolean)
		void Register_UnityEngine_Graphics_Internal_BlitMaterial();
		Register_UnityEngine_Graphics_Internal_BlitMaterial();

		//System.Void UnityEngine.Graphics::Internal_BlitMultiTap(UnityEngine.Texture,UnityEngine.RenderTexture,UnityEngine.Material,UnityEngine.Vector2[])
		void Register_UnityEngine_Graphics_Internal_BlitMultiTap();
		Register_UnityEngine_Graphics_Internal_BlitMultiTap();

		//System.Void UnityEngine.Graphics::Internal_DrawMeshMatrix(UnityEngine.Internal_DrawMeshMatrixArguments&,UnityEngine.MaterialPropertyBlock,UnityEngine.Material,UnityEngine.Mesh,UnityEngine.Camera)
		void Register_UnityEngine_Graphics_Internal_DrawMeshMatrix();
		Register_UnityEngine_Graphics_Internal_DrawMeshMatrix();

		//System.Void UnityEngine.Graphics::Internal_DrawMeshTR(UnityEngine.Internal_DrawMeshTRArguments&,UnityEngine.MaterialPropertyBlock,UnityEngine.Material,UnityEngine.Mesh,UnityEngine.Camera)
		void Register_UnityEngine_Graphics_Internal_DrawMeshTR();
		Register_UnityEngine_Graphics_Internal_DrawMeshTR();

		//System.Void UnityEngine.Graphics::Internal_SetMRTFullSetup(UnityEngine.RenderBuffer[],UnityEngine.RenderBuffer&,System.Int32,UnityEngine.CubemapFace,UnityEngine.Rendering.RenderBufferLoadAction[],UnityEngine.Rendering.RenderBufferStoreAction[],UnityEngine.Rendering.RenderBufferLoadAction,UnityEngine.Rendering.RenderBufferStoreAction)
		void Register_UnityEngine_Graphics_Internal_SetMRTFullSetup();
		Register_UnityEngine_Graphics_Internal_SetMRTFullSetup();

		//System.Void UnityEngine.Graphics::Internal_SetMRTSimple(UnityEngine.RenderBuffer[],UnityEngine.RenderBuffer&,System.Int32,UnityEngine.CubemapFace)
		void Register_UnityEngine_Graphics_Internal_SetMRTSimple();
		Register_UnityEngine_Graphics_Internal_SetMRTSimple();

		//System.Void UnityEngine.Graphics::Internal_SetNullRT()
		void Register_UnityEngine_Graphics_Internal_SetNullRT();
		Register_UnityEngine_Graphics_Internal_SetNullRT();

		//System.Void UnityEngine.Graphics::Internal_SetRTSimple(UnityEngine.RenderBuffer&,UnityEngine.RenderBuffer&,System.Int32,UnityEngine.CubemapFace)
		void Register_UnityEngine_Graphics_Internal_SetRTSimple();
		Register_UnityEngine_Graphics_Internal_SetRTSimple();

		//System.Void UnityEngine.Graphics::Internal_SetRandomWriteTargetBuffer(System.Int32,UnityEngine.ComputeBuffer)
		void Register_UnityEngine_Graphics_Internal_SetRandomWriteTargetBuffer();
		Register_UnityEngine_Graphics_Internal_SetRandomWriteTargetBuffer();

		//System.Void UnityEngine.Graphics::Internal_SetRandomWriteTargetRT(System.Int32,UnityEngine.RenderTexture)
		void Register_UnityEngine_Graphics_Internal_SetRandomWriteTargetRT();
		Register_UnityEngine_Graphics_Internal_SetRandomWriteTargetRT();

	//End Registrations for type : UnityEngine.Graphics

	//Start Registrations for type : UnityEngine.GUI

		//System.Boolean UnityEngine.GUI::INTERNAL_CALL_DoButton(UnityEngine.Rect&,UnityEngine.GUIContent,System.IntPtr)
		void Register_UnityEngine_GUI_INTERNAL_CALL_DoButton();
		Register_UnityEngine_GUI_INTERNAL_CALL_DoButton();

		//System.Boolean UnityEngine.GUI::INTERNAL_CALL_DoToggle(UnityEngine.Rect&,System.Int32,System.Boolean,UnityEngine.GUIContent,System.IntPtr)
		void Register_UnityEngine_GUI_INTERNAL_CALL_DoToggle();
		Register_UnityEngine_GUI_INTERNAL_CALL_DoToggle();

		//System.Boolean UnityEngine.GUI::get_changed()
		void Register_UnityEngine_GUI_get_changed();
		Register_UnityEngine_GUI_get_changed();

		//System.Boolean UnityEngine.GUI::get_enabled()
		void Register_UnityEngine_GUI_get_enabled();
		Register_UnityEngine_GUI_get_enabled();

		//System.Boolean UnityEngine.GUI::get_usePageScrollbars()
		void Register_UnityEngine_GUI_get_usePageScrollbars();
		Register_UnityEngine_GUI_get_usePageScrollbars();

		//System.Int32 UnityEngine.GUI::get_depth()
		void Register_UnityEngine_GUI_get_depth();
		Register_UnityEngine_GUI_get_depth();

		//System.String UnityEngine.GUI::GetNameOfFocusedControl()
		void Register_UnityEngine_GUI_GetNameOfFocusedControl();
		Register_UnityEngine_GUI_GetNameOfFocusedControl();

		//System.String UnityEngine.GUI::Internal_GetTooltip()
		void Register_UnityEngine_GUI_Internal_GetTooltip();
		Register_UnityEngine_GUI_Internal_GetTooltip();

		//System.Void UnityEngine.GUI::BringWindowToBack(System.Int32)
		void Register_UnityEngine_GUI_BringWindowToBack();
		Register_UnityEngine_GUI_BringWindowToBack();

		//System.Void UnityEngine.GUI::BringWindowToFront(System.Int32)
		void Register_UnityEngine_GUI_BringWindowToFront();
		Register_UnityEngine_GUI_BringWindowToFront();

		//System.Void UnityEngine.GUI::FocusControl(System.String)
		void Register_UnityEngine_GUI_FocusControl();
		Register_UnityEngine_GUI_FocusControl();

		//System.Void UnityEngine.GUI::FocusWindow(System.Int32)
		void Register_UnityEngine_GUI_FocusWindow();
		Register_UnityEngine_GUI_FocusWindow();

		//System.Void UnityEngine.GUI::INTERNAL_CALL_DoLabel(UnityEngine.Rect&,UnityEngine.GUIContent,System.IntPtr)
		void Register_UnityEngine_GUI_INTERNAL_CALL_DoLabel();
		Register_UnityEngine_GUI_INTERNAL_CALL_DoLabel();

		//System.Void UnityEngine.GUI::INTERNAL_CALL_DoModalWindow(System.Int32,UnityEngine.Rect&,UnityEngine.GUI/WindowFunction,UnityEngine.GUIContent,UnityEngine.GUIStyle,UnityEngine.GUISkin,UnityEngine.Rect&)
		void Register_UnityEngine_GUI_INTERNAL_CALL_DoModalWindow();
		Register_UnityEngine_GUI_INTERNAL_CALL_DoModalWindow();

		//System.Void UnityEngine.GUI::INTERNAL_CALL_DoWindow(System.Int32,UnityEngine.Rect&,UnityEngine.GUI/WindowFunction,UnityEngine.GUIContent,UnityEngine.GUIStyle,UnityEngine.GUISkin,System.Boolean,UnityEngine.Rect&)
		void Register_UnityEngine_GUI_INTERNAL_CALL_DoWindow();
		Register_UnityEngine_GUI_INTERNAL_CALL_DoWindow();

		//System.Void UnityEngine.GUI::INTERNAL_CALL_DragWindow(UnityEngine.Rect&)
		void Register_UnityEngine_GUI_INTERNAL_CALL_DragWindow();
		Register_UnityEngine_GUI_INTERNAL_CALL_DragWindow();

		//System.Void UnityEngine.GUI::INTERNAL_get_backgroundColor(UnityEngine.Color&)
		void Register_UnityEngine_GUI_INTERNAL_get_backgroundColor();
		Register_UnityEngine_GUI_INTERNAL_get_backgroundColor();

		//System.Void UnityEngine.GUI::INTERNAL_get_color(UnityEngine.Color&)
		void Register_UnityEngine_GUI_INTERNAL_get_color();
		Register_UnityEngine_GUI_INTERNAL_get_color();

		//System.Void UnityEngine.GUI::INTERNAL_get_contentColor(UnityEngine.Color&)
		void Register_UnityEngine_GUI_INTERNAL_get_contentColor();
		Register_UnityEngine_GUI_INTERNAL_get_contentColor();

		//System.Void UnityEngine.GUI::INTERNAL_set_backgroundColor(UnityEngine.Color&)
		void Register_UnityEngine_GUI_INTERNAL_set_backgroundColor();
		Register_UnityEngine_GUI_INTERNAL_set_backgroundColor();

		//System.Void UnityEngine.GUI::INTERNAL_set_color(UnityEngine.Color&)
		void Register_UnityEngine_GUI_INTERNAL_set_color();
		Register_UnityEngine_GUI_INTERNAL_set_color();

		//System.Void UnityEngine.GUI::INTERNAL_set_contentColor(UnityEngine.Color&)
		void Register_UnityEngine_GUI_INTERNAL_set_contentColor();
		Register_UnityEngine_GUI_INTERNAL_set_contentColor();

		//System.Void UnityEngine.GUI::InternalRepaintEditorWindow()
		void Register_UnityEngine_GUI_InternalRepaintEditorWindow();
		Register_UnityEngine_GUI_InternalRepaintEditorWindow();

		//System.Void UnityEngine.GUI::Internal_SetTooltip(System.String)
		void Register_UnityEngine_GUI_Internal_SetTooltip();
		Register_UnityEngine_GUI_Internal_SetTooltip();

		//System.Void UnityEngine.GUI::SetNextControlName(System.String)
		void Register_UnityEngine_GUI_SetNextControlName();
		Register_UnityEngine_GUI_SetNextControlName();

		//System.Void UnityEngine.GUI::UnfocusWindow()
		void Register_UnityEngine_GUI_UnfocusWindow();
		Register_UnityEngine_GUI_UnfocusWindow();

		//System.Void UnityEngine.GUI::set_changed(System.Boolean)
		void Register_UnityEngine_GUI_set_changed();
		Register_UnityEngine_GUI_set_changed();

		//System.Void UnityEngine.GUI::set_depth(System.Int32)
		void Register_UnityEngine_GUI_set_depth();
		Register_UnityEngine_GUI_set_depth();

		//System.Void UnityEngine.GUI::set_enabled(System.Boolean)
		void Register_UnityEngine_GUI_set_enabled();
		Register_UnityEngine_GUI_set_enabled();

		//UnityEngine.Material UnityEngine.GUI::get_blendMaterial()
		void Register_UnityEngine_GUI_get_blendMaterial();
		Register_UnityEngine_GUI_get_blendMaterial();

		//UnityEngine.Material UnityEngine.GUI::get_blitMaterial()
		void Register_UnityEngine_GUI_get_blitMaterial();
		Register_UnityEngine_GUI_get_blitMaterial();

	//End Registrations for type : UnityEngine.GUI

	//Start Registrations for type : UnityEngine.GUIClip

		//System.Void UnityEngine.GUIClip::INTERNAL_CALL_Clip_Vector2(UnityEngine.Vector2&)
		void Register_UnityEngine_GUIClip_INTERNAL_CALL_Clip_Vector2();
		Register_UnityEngine_GUIClip_INTERNAL_CALL_Clip_Vector2();

		//System.Void UnityEngine.GUIClip::INTERNAL_CALL_GetMatrix(UnityEngine.Matrix4x4&)
		void Register_UnityEngine_GUIClip_INTERNAL_CALL_GetMatrix();
		Register_UnityEngine_GUIClip_INTERNAL_CALL_GetMatrix();

		//System.Void UnityEngine.GUIClip::INTERNAL_CALL_Push(UnityEngine.Rect&,UnityEngine.Vector2&,UnityEngine.Vector2&,System.Boolean)
		void Register_UnityEngine_GUIClip_INTERNAL_CALL_Push();
		Register_UnityEngine_GUIClip_INTERNAL_CALL_Push();

		//System.Void UnityEngine.GUIClip::INTERNAL_CALL_SetMatrix(UnityEngine.Matrix4x4&)
		void Register_UnityEngine_GUIClip_INTERNAL_CALL_SetMatrix();
		Register_UnityEngine_GUIClip_INTERNAL_CALL_SetMatrix();

		//System.Void UnityEngine.GUIClip::INTERNAL_CALL_Unclip_Vector2(UnityEngine.Vector2&)
		void Register_UnityEngine_GUIClip_INTERNAL_CALL_Unclip_Vector2();
		Register_UnityEngine_GUIClip_INTERNAL_CALL_Unclip_Vector2();

		//System.Void UnityEngine.GUIClip::Pop()
		void Register_UnityEngine_GUIClip_Pop();
		Register_UnityEngine_GUIClip_Pop();

	//End Registrations for type : UnityEngine.GUIClip

	//Start Registrations for type : UnityEngine.GUIElement

		//System.Boolean UnityEngine.GUIElement::INTERNAL_CALL_HitTest(UnityEngine.GUIElement,UnityEngine.Vector3&,UnityEngine.Camera)
		void Register_UnityEngine_GUIElement_INTERNAL_CALL_HitTest();
		Register_UnityEngine_GUIElement_INTERNAL_CALL_HitTest();

		//System.Void UnityEngine.GUIElement::INTERNAL_CALL_GetScreenRect(UnityEngine.GUIElement,UnityEngine.Camera,UnityEngine.Rect&)
		void Register_UnityEngine_GUIElement_INTERNAL_CALL_GetScreenRect();
		Register_UnityEngine_GUIElement_INTERNAL_CALL_GetScreenRect();

	//End Registrations for type : UnityEngine.GUIElement

	//Start Registrations for type : UnityEngine.GUILayer

		//UnityEngine.GUIElement UnityEngine.GUILayer::INTERNAL_CALL_HitTest(UnityEngine.GUILayer,UnityEngine.Vector3&)
		void Register_UnityEngine_GUILayer_INTERNAL_CALL_HitTest();
		Register_UnityEngine_GUILayer_INTERNAL_CALL_HitTest();

	//End Registrations for type : UnityEngine.GUILayer

	//Start Registrations for type : UnityEngine.GUILayoutUtility

		//System.Void UnityEngine.GUILayoutUtility::INTERNAL_CALL_Internal_GetWindowRect(System.Int32,UnityEngine.Rect&)
		void Register_UnityEngine_GUILayoutUtility_INTERNAL_CALL_Internal_GetWindowRect();
		Register_UnityEngine_GUILayoutUtility_INTERNAL_CALL_Internal_GetWindowRect();

		//System.Void UnityEngine.GUILayoutUtility::INTERNAL_CALL_Internal_MoveWindow(System.Int32,UnityEngine.Rect&)
		void Register_UnityEngine_GUILayoutUtility_INTERNAL_CALL_Internal_MoveWindow();
		Register_UnityEngine_GUILayoutUtility_INTERNAL_CALL_Internal_MoveWindow();

	//End Registrations for type : UnityEngine.GUILayoutUtility

	//Start Registrations for type : UnityEngine.GUISettings

		//System.Single UnityEngine.GUISettings::Internal_GetCursorFlashSpeed()
		void Register_UnityEngine_GUISettings_Internal_GetCursorFlashSpeed();
		Register_UnityEngine_GUISettings_Internal_GetCursorFlashSpeed();

	//End Registrations for type : UnityEngine.GUISettings

	//Start Registrations for type : UnityEngine.GUIStyle

		//System.Boolean UnityEngine.GUIStyle::get_richText()
		void Register_UnityEngine_GUIStyle_get_richText();
		Register_UnityEngine_GUIStyle_get_richText();

		//System.Boolean UnityEngine.GUIStyle::get_stretchHeight()
		void Register_UnityEngine_GUIStyle_get_stretchHeight();
		Register_UnityEngine_GUIStyle_get_stretchHeight();

		//System.Boolean UnityEngine.GUIStyle::get_stretchWidth()
		void Register_UnityEngine_GUIStyle_get_stretchWidth();
		Register_UnityEngine_GUIStyle_get_stretchWidth();

		//System.Boolean UnityEngine.GUIStyle::get_wordWrap()
		void Register_UnityEngine_GUIStyle_get_wordWrap();
		Register_UnityEngine_GUIStyle_get_wordWrap();

		//System.Int32 UnityEngine.GUIStyle::INTERNAL_CALL_Internal_GetCursorStringIndex(System.IntPtr,UnityEngine.Rect&,UnityEngine.GUIContent,UnityEngine.Vector2&)
		void Register_UnityEngine_GUIStyle_INTERNAL_CALL_Internal_GetCursorStringIndex();
		Register_UnityEngine_GUIStyle_INTERNAL_CALL_Internal_GetCursorStringIndex();

		//System.Int32 UnityEngine.GUIStyle::Internal_GetNumCharactersThatFitWithinWidth(System.IntPtr,System.String,System.Single)
		void Register_UnityEngine_GUIStyle_Internal_GetNumCharactersThatFitWithinWidth();
		Register_UnityEngine_GUIStyle_Internal_GetNumCharactersThatFitWithinWidth();

		//System.Int32 UnityEngine.GUIStyle::get_fontSize()
		void Register_UnityEngine_GUIStyle_get_fontSize();
		Register_UnityEngine_GUIStyle_get_fontSize();

		//System.IntPtr UnityEngine.GUIStyle::GetRectOffsetPtr(System.Int32)
		void Register_UnityEngine_GUIStyle_GetRectOffsetPtr();
		Register_UnityEngine_GUIStyle_GetRectOffsetPtr();

		//System.IntPtr UnityEngine.GUIStyle::GetStyleStatePtr(System.Int32)
		void Register_UnityEngine_GUIStyle_GetStyleStatePtr();
		Register_UnityEngine_GUIStyle_GetStyleStatePtr();

		//System.Single UnityEngine.GUIStyle::Internal_CalcHeight(System.IntPtr,UnityEngine.GUIContent,System.Single)
		void Register_UnityEngine_GUIStyle_Internal_CalcHeight();
		Register_UnityEngine_GUIStyle_Internal_CalcHeight();

		//System.Single UnityEngine.GUIStyle::Internal_GetCursorFlashOffset()
		void Register_UnityEngine_GUIStyle_Internal_GetCursorFlashOffset();
		Register_UnityEngine_GUIStyle_Internal_GetCursorFlashOffset();

		//System.Single UnityEngine.GUIStyle::Internal_GetLineHeight(System.IntPtr)
		void Register_UnityEngine_GUIStyle_Internal_GetLineHeight();
		Register_UnityEngine_GUIStyle_Internal_GetLineHeight();

		//System.Single UnityEngine.GUIStyle::get_fixedHeight()
		void Register_UnityEngine_GUIStyle_get_fixedHeight();
		Register_UnityEngine_GUIStyle_get_fixedHeight();

		//System.Single UnityEngine.GUIStyle::get_fixedWidth()
		void Register_UnityEngine_GUIStyle_get_fixedWidth();
		Register_UnityEngine_GUIStyle_get_fixedWidth();

		//System.String UnityEngine.GUIStyle::get_name()
		void Register_UnityEngine_GUIStyle_get_name();
		Register_UnityEngine_GUIStyle_get_name();

		//System.Void UnityEngine.GUIStyle::AssignRectOffset(System.Int32,System.IntPtr)
		void Register_UnityEngine_GUIStyle_AssignRectOffset();
		Register_UnityEngine_GUIStyle_AssignRectOffset();

		//System.Void UnityEngine.GUIStyle::AssignStyleState(System.Int32,System.IntPtr)
		void Register_UnityEngine_GUIStyle_AssignStyleState();
		Register_UnityEngine_GUIStyle_AssignStyleState();

		//System.Void UnityEngine.GUIStyle::Cleanup()
		void Register_UnityEngine_GUIStyle_Cleanup();
		Register_UnityEngine_GUIStyle_Cleanup();

		//System.Void UnityEngine.GUIStyle::INTERNAL_CALL_Internal_CalcSizeWithConstraints(System.IntPtr,UnityEngine.GUIContent,UnityEngine.Vector2&,UnityEngine.Vector2&)
		void Register_UnityEngine_GUIStyle_INTERNAL_CALL_Internal_CalcSizeWithConstraints();
		Register_UnityEngine_GUIStyle_INTERNAL_CALL_Internal_CalcSizeWithConstraints();

		//System.Void UnityEngine.GUIStyle::INTERNAL_CALL_Internal_Draw2(System.IntPtr,UnityEngine.Rect&,UnityEngine.GUIContent,System.Int32,System.Boolean)
		void Register_UnityEngine_GUIStyle_INTERNAL_CALL_Internal_Draw2();
		Register_UnityEngine_GUIStyle_INTERNAL_CALL_Internal_Draw2();

		//System.Void UnityEngine.GUIStyle::INTERNAL_CALL_Internal_DrawCursor(System.IntPtr,UnityEngine.Rect&,UnityEngine.GUIContent,System.Int32,UnityEngine.Color&)
		void Register_UnityEngine_GUIStyle_INTERNAL_CALL_Internal_DrawCursor();
		Register_UnityEngine_GUIStyle_INTERNAL_CALL_Internal_DrawCursor();

		//System.Void UnityEngine.GUIStyle::INTERNAL_CALL_Internal_DrawPrefixLabel(System.IntPtr,UnityEngine.Rect&,UnityEngine.GUIContent,System.Int32,System.Boolean)
		void Register_UnityEngine_GUIStyle_INTERNAL_CALL_Internal_DrawPrefixLabel();
		Register_UnityEngine_GUIStyle_INTERNAL_CALL_Internal_DrawPrefixLabel();

		//System.Void UnityEngine.GUIStyle::INTERNAL_CALL_Internal_GetCursorPixelPosition(System.IntPtr,UnityEngine.Rect&,UnityEngine.GUIContent,System.Int32,UnityEngine.Vector2&)
		void Register_UnityEngine_GUIStyle_INTERNAL_CALL_Internal_GetCursorPixelPosition();
		Register_UnityEngine_GUIStyle_INTERNAL_CALL_Internal_GetCursorPixelPosition();

		//System.Void UnityEngine.GUIStyle::INTERNAL_get_Internal_clipOffset(UnityEngine.Vector2&)
		void Register_UnityEngine_GUIStyle_INTERNAL_get_Internal_clipOffset();
		Register_UnityEngine_GUIStyle_INTERNAL_get_Internal_clipOffset();

		//System.Void UnityEngine.GUIStyle::INTERNAL_get_contentOffset(UnityEngine.Vector2&)
		void Register_UnityEngine_GUIStyle_INTERNAL_get_contentOffset();
		Register_UnityEngine_GUIStyle_INTERNAL_get_contentOffset();

		//System.Void UnityEngine.GUIStyle::INTERNAL_set_Internal_clipOffset(UnityEngine.Vector2&)
		void Register_UnityEngine_GUIStyle_INTERNAL_set_Internal_clipOffset();
		Register_UnityEngine_GUIStyle_INTERNAL_set_Internal_clipOffset();

		//System.Void UnityEngine.GUIStyle::INTERNAL_set_contentOffset(UnityEngine.Vector2&)
		void Register_UnityEngine_GUIStyle_INTERNAL_set_contentOffset();
		Register_UnityEngine_GUIStyle_INTERNAL_set_contentOffset();

		//System.Void UnityEngine.GUIStyle::Init()
		void Register_UnityEngine_GUIStyle_Init();
		Register_UnityEngine_GUIStyle_Init();

		//System.Void UnityEngine.GUIStyle::InitCopy(UnityEngine.GUIStyle)
		void Register_UnityEngine_GUIStyle_InitCopy();
		Register_UnityEngine_GUIStyle_InitCopy();

		//System.Void UnityEngine.GUIStyle::Internal_CalcMinMaxWidth(System.IntPtr,UnityEngine.GUIContent,System.Single&,System.Single&)
		void Register_UnityEngine_GUIStyle_Internal_CalcMinMaxWidth();
		Register_UnityEngine_GUIStyle_Internal_CalcMinMaxWidth();

		//System.Void UnityEngine.GUIStyle::Internal_CalcSize(System.IntPtr,UnityEngine.GUIContent,UnityEngine.Vector2&)
		void Register_UnityEngine_GUIStyle_Internal_CalcSize();
		Register_UnityEngine_GUIStyle_Internal_CalcSize();

		//System.Void UnityEngine.GUIStyle::Internal_Draw(UnityEngine.GUIContent,UnityEngine.Internal_DrawArguments&)
		void Register_UnityEngine_GUIStyle_Internal_Draw();
		Register_UnityEngine_GUIStyle_Internal_Draw();

		//System.Void UnityEngine.GUIStyle::Internal_DrawWithTextSelection(UnityEngine.GUIContent,UnityEngine.Internal_DrawWithTextSelectionArguments&)
		void Register_UnityEngine_GUIStyle_Internal_DrawWithTextSelection();
		Register_UnityEngine_GUIStyle_Internal_DrawWithTextSelection();

		//System.Void UnityEngine.GUIStyle::SetDefaultFont(UnityEngine.Font)
		void Register_UnityEngine_GUIStyle_SetDefaultFont();
		Register_UnityEngine_GUIStyle_SetDefaultFont();

		//System.Void UnityEngine.GUIStyle::SetFontInternal(UnityEngine.Font)
		void Register_UnityEngine_GUIStyle_SetFontInternal();
		Register_UnityEngine_GUIStyle_SetFontInternal();

		//System.Void UnityEngine.GUIStyle::set_alignment(UnityEngine.TextAnchor)
		void Register_UnityEngine_GUIStyle_set_alignment();
		Register_UnityEngine_GUIStyle_set_alignment();

		//System.Void UnityEngine.GUIStyle::set_clipping(UnityEngine.TextClipping)
		void Register_UnityEngine_GUIStyle_set_clipping();
		Register_UnityEngine_GUIStyle_set_clipping();

		//System.Void UnityEngine.GUIStyle::set_fixedHeight(System.Single)
		void Register_UnityEngine_GUIStyle_set_fixedHeight();
		Register_UnityEngine_GUIStyle_set_fixedHeight();

		//System.Void UnityEngine.GUIStyle::set_fixedWidth(System.Single)
		void Register_UnityEngine_GUIStyle_set_fixedWidth();
		Register_UnityEngine_GUIStyle_set_fixedWidth();

		//System.Void UnityEngine.GUIStyle::set_fontSize(System.Int32)
		void Register_UnityEngine_GUIStyle_set_fontSize();
		Register_UnityEngine_GUIStyle_set_fontSize();

		//System.Void UnityEngine.GUIStyle::set_fontStyle(UnityEngine.FontStyle)
		void Register_UnityEngine_GUIStyle_set_fontStyle();
		Register_UnityEngine_GUIStyle_set_fontStyle();

		//System.Void UnityEngine.GUIStyle::set_imagePosition(UnityEngine.ImagePosition)
		void Register_UnityEngine_GUIStyle_set_imagePosition();
		Register_UnityEngine_GUIStyle_set_imagePosition();

		//System.Void UnityEngine.GUIStyle::set_name(System.String)
		void Register_UnityEngine_GUIStyle_set_name();
		Register_UnityEngine_GUIStyle_set_name();

		//System.Void UnityEngine.GUIStyle::set_richText(System.Boolean)
		void Register_UnityEngine_GUIStyle_set_richText();
		Register_UnityEngine_GUIStyle_set_richText();

		//System.Void UnityEngine.GUIStyle::set_stretchHeight(System.Boolean)
		void Register_UnityEngine_GUIStyle_set_stretchHeight();
		Register_UnityEngine_GUIStyle_set_stretchHeight();

		//System.Void UnityEngine.GUIStyle::set_stretchWidth(System.Boolean)
		void Register_UnityEngine_GUIStyle_set_stretchWidth();
		Register_UnityEngine_GUIStyle_set_stretchWidth();

		//System.Void UnityEngine.GUIStyle::set_wordWrap(System.Boolean)
		void Register_UnityEngine_GUIStyle_set_wordWrap();
		Register_UnityEngine_GUIStyle_set_wordWrap();

		//UnityEngine.Font UnityEngine.GUIStyle::GetFontInternal()
		void Register_UnityEngine_GUIStyle_GetFontInternal();
		Register_UnityEngine_GUIStyle_GetFontInternal();

		//UnityEngine.Font UnityEngine.GUIStyle::GetFontInternalDuringLoadingThread()
		void Register_UnityEngine_GUIStyle_GetFontInternalDuringLoadingThread();
		Register_UnityEngine_GUIStyle_GetFontInternalDuringLoadingThread();

		//UnityEngine.FontStyle UnityEngine.GUIStyle::get_fontStyle()
		void Register_UnityEngine_GUIStyle_get_fontStyle();
		Register_UnityEngine_GUIStyle_get_fontStyle();

		//UnityEngine.ImagePosition UnityEngine.GUIStyle::get_imagePosition()
		void Register_UnityEngine_GUIStyle_get_imagePosition();
		Register_UnityEngine_GUIStyle_get_imagePosition();

		//UnityEngine.TextAnchor UnityEngine.GUIStyle::get_alignment()
		void Register_UnityEngine_GUIStyle_get_alignment();
		Register_UnityEngine_GUIStyle_get_alignment();

		//UnityEngine.TextClipping UnityEngine.GUIStyle::get_clipping()
		void Register_UnityEngine_GUIStyle_get_clipping();
		Register_UnityEngine_GUIStyle_get_clipping();

	//End Registrations for type : UnityEngine.GUIStyle

	//Start Registrations for type : UnityEngine.GUIStyleState

		//System.Void UnityEngine.GUIStyleState::Cleanup()
		void Register_UnityEngine_GUIStyleState_Cleanup();
		Register_UnityEngine_GUIStyleState_Cleanup();

		//System.Void UnityEngine.GUIStyleState::INTERNAL_get_textColor(UnityEngine.Color&)
		void Register_UnityEngine_GUIStyleState_INTERNAL_get_textColor();
		Register_UnityEngine_GUIStyleState_INTERNAL_get_textColor();

		//System.Void UnityEngine.GUIStyleState::INTERNAL_set_textColor(UnityEngine.Color&)
		void Register_UnityEngine_GUIStyleState_INTERNAL_set_textColor();
		Register_UnityEngine_GUIStyleState_INTERNAL_set_textColor();

		//System.Void UnityEngine.GUIStyleState::Init()
		void Register_UnityEngine_GUIStyleState_Init();
		Register_UnityEngine_GUIStyleState_Init();

		//System.Void UnityEngine.GUIStyleState::SetBackgroundInternal(UnityEngine.Texture2D)
		void Register_UnityEngine_GUIStyleState_SetBackgroundInternal();
		Register_UnityEngine_GUIStyleState_SetBackgroundInternal();

		//UnityEngine.Texture2D UnityEngine.GUIStyleState::GetBackgroundInternal()
		void Register_UnityEngine_GUIStyleState_GetBackgroundInternal();
		Register_UnityEngine_GUIStyleState_GetBackgroundInternal();

		//UnityEngine.Texture2D UnityEngine.GUIStyleState::GetBackgroundInternalFromDeserialization()
		void Register_UnityEngine_GUIStyleState_GetBackgroundInternalFromDeserialization();
		Register_UnityEngine_GUIStyleState_GetBackgroundInternalFromDeserialization();

	//End Registrations for type : UnityEngine.GUIStyleState

	//Start Registrations for type : UnityEngine.GUIText

		//System.Boolean UnityEngine.GUIText::get_richText()
		void Register_UnityEngine_GUIText_get_richText();
		Register_UnityEngine_GUIText_get_richText();

		//System.Int32 UnityEngine.GUIText::get_fontSize()
		void Register_UnityEngine_GUIText_get_fontSize();
		Register_UnityEngine_GUIText_get_fontSize();

		//System.Single UnityEngine.GUIText::get_lineSpacing()
		void Register_UnityEngine_GUIText_get_lineSpacing();
		Register_UnityEngine_GUIText_get_lineSpacing();

		//System.Single UnityEngine.GUIText::get_tabSize()
		void Register_UnityEngine_GUIText_get_tabSize();
		Register_UnityEngine_GUIText_get_tabSize();

		//System.String UnityEngine.GUIText::get_text()
		void Register_UnityEngine_GUIText_get_text();
		Register_UnityEngine_GUIText_get_text();

		//System.Void UnityEngine.GUIText::INTERNAL_CALL_Internal_SetPixelOffset(UnityEngine.GUIText,UnityEngine.Vector2&)
		void Register_UnityEngine_GUIText_INTERNAL_CALL_Internal_SetPixelOffset();
		Register_UnityEngine_GUIText_INTERNAL_CALL_Internal_SetPixelOffset();

		//System.Void UnityEngine.GUIText::INTERNAL_get_color(UnityEngine.Color&)
		void Register_UnityEngine_GUIText_INTERNAL_get_color();
		Register_UnityEngine_GUIText_INTERNAL_get_color();

		//System.Void UnityEngine.GUIText::INTERNAL_set_color(UnityEngine.Color&)
		void Register_UnityEngine_GUIText_INTERNAL_set_color();
		Register_UnityEngine_GUIText_INTERNAL_set_color();

		//System.Void UnityEngine.GUIText::Internal_GetPixelOffset(UnityEngine.Vector2&)
		void Register_UnityEngine_GUIText_Internal_GetPixelOffset();
		Register_UnityEngine_GUIText_Internal_GetPixelOffset();

		//System.Void UnityEngine.GUIText::set_alignment(UnityEngine.TextAlignment)
		void Register_UnityEngine_GUIText_set_alignment();
		Register_UnityEngine_GUIText_set_alignment();

		//System.Void UnityEngine.GUIText::set_anchor(UnityEngine.TextAnchor)
		void Register_UnityEngine_GUIText_set_anchor();
		Register_UnityEngine_GUIText_set_anchor();

		//System.Void UnityEngine.GUIText::set_font(UnityEngine.Font)
		void Register_UnityEngine_GUIText_set_font();
		Register_UnityEngine_GUIText_set_font();

		//System.Void UnityEngine.GUIText::set_fontSize(System.Int32)
		void Register_UnityEngine_GUIText_set_fontSize();
		Register_UnityEngine_GUIText_set_fontSize();

		//System.Void UnityEngine.GUIText::set_fontStyle(UnityEngine.FontStyle)
		void Register_UnityEngine_GUIText_set_fontStyle();
		Register_UnityEngine_GUIText_set_fontStyle();

		//System.Void UnityEngine.GUIText::set_lineSpacing(System.Single)
		void Register_UnityEngine_GUIText_set_lineSpacing();
		Register_UnityEngine_GUIText_set_lineSpacing();

		//System.Void UnityEngine.GUIText::set_material(UnityEngine.Material)
		void Register_UnityEngine_GUIText_set_material();
		Register_UnityEngine_GUIText_set_material();

		//System.Void UnityEngine.GUIText::set_richText(System.Boolean)
		void Register_UnityEngine_GUIText_set_richText();
		Register_UnityEngine_GUIText_set_richText();

		//System.Void UnityEngine.GUIText::set_tabSize(System.Single)
		void Register_UnityEngine_GUIText_set_tabSize();
		Register_UnityEngine_GUIText_set_tabSize();

		//System.Void UnityEngine.GUIText::set_text(System.String)
		void Register_UnityEngine_GUIText_set_text();
		Register_UnityEngine_GUIText_set_text();

		//UnityEngine.Font UnityEngine.GUIText::get_font()
		void Register_UnityEngine_GUIText_get_font();
		Register_UnityEngine_GUIText_get_font();

		//UnityEngine.FontStyle UnityEngine.GUIText::get_fontStyle()
		void Register_UnityEngine_GUIText_get_fontStyle();
		Register_UnityEngine_GUIText_get_fontStyle();

		//UnityEngine.Material UnityEngine.GUIText::get_material()
		void Register_UnityEngine_GUIText_get_material();
		Register_UnityEngine_GUIText_get_material();

		//UnityEngine.TextAlignment UnityEngine.GUIText::get_alignment()
		void Register_UnityEngine_GUIText_get_alignment();
		Register_UnityEngine_GUIText_get_alignment();

		//UnityEngine.TextAnchor UnityEngine.GUIText::get_anchor()
		void Register_UnityEngine_GUIText_get_anchor();
		Register_UnityEngine_GUIText_get_anchor();

	//End Registrations for type : UnityEngine.GUIText

	//Start Registrations for type : UnityEngine.GUITexture

		//System.Void UnityEngine.GUITexture::INTERNAL_get_color(UnityEngine.Color&)
		void Register_UnityEngine_GUITexture_INTERNAL_get_color();
		Register_UnityEngine_GUITexture_INTERNAL_get_color();

		//System.Void UnityEngine.GUITexture::INTERNAL_get_pixelInset(UnityEngine.Rect&)
		void Register_UnityEngine_GUITexture_INTERNAL_get_pixelInset();
		Register_UnityEngine_GUITexture_INTERNAL_get_pixelInset();

		//System.Void UnityEngine.GUITexture::INTERNAL_set_color(UnityEngine.Color&)
		void Register_UnityEngine_GUITexture_INTERNAL_set_color();
		Register_UnityEngine_GUITexture_INTERNAL_set_color();

		//System.Void UnityEngine.GUITexture::INTERNAL_set_pixelInset(UnityEngine.Rect&)
		void Register_UnityEngine_GUITexture_INTERNAL_set_pixelInset();
		Register_UnityEngine_GUITexture_INTERNAL_set_pixelInset();

		//System.Void UnityEngine.GUITexture::set_border(UnityEngine.RectOffset)
		void Register_UnityEngine_GUITexture_set_border();
		Register_UnityEngine_GUITexture_set_border();

		//System.Void UnityEngine.GUITexture::set_texture(UnityEngine.Texture)
		void Register_UnityEngine_GUITexture_set_texture();
		Register_UnityEngine_GUITexture_set_texture();

		//UnityEngine.RectOffset UnityEngine.GUITexture::get_border()
		void Register_UnityEngine_GUITexture_get_border();
		Register_UnityEngine_GUITexture_get_border();

		//UnityEngine.Texture UnityEngine.GUITexture::get_texture()
		void Register_UnityEngine_GUITexture_get_texture();
		Register_UnityEngine_GUITexture_get_texture();

	//End Registrations for type : UnityEngine.GUITexture

	//Start Registrations for type : UnityEngine.GUIUtility

		//System.Boolean UnityEngine.GUIUtility::get_hasModalWindow()
		void Register_UnityEngine_GUIUtility_get_hasModalWindow();
		Register_UnityEngine_GUIUtility_get_hasModalWindow();

		//System.Boolean UnityEngine.GUIUtility::get_mouseUsed()
		void Register_UnityEngine_GUIUtility_get_mouseUsed();
		Register_UnityEngine_GUIUtility_get_mouseUsed();

		//System.Int32 UnityEngine.GUIUtility::GetControlID(System.Int32,UnityEngine.FocusType)
		void Register_UnityEngine_GUIUtility_GetControlID();
		Register_UnityEngine_GUIUtility_GetControlID();

		//System.Int32 UnityEngine.GUIUtility::INTERNAL_CALL_Internal_GetNextControlID2(System.Int32,UnityEngine.FocusType,UnityEngine.Rect&)
		void Register_UnityEngine_GUIUtility_INTERNAL_CALL_Internal_GetNextControlID2();
		Register_UnityEngine_GUIUtility_INTERNAL_CALL_Internal_GetNextControlID2();

		//System.Int32 UnityEngine.GUIUtility::Internal_GetGUIDepth()
		void Register_UnityEngine_GUIUtility_Internal_GetGUIDepth();
		Register_UnityEngine_GUIUtility_Internal_GetGUIDepth();

		//System.Int32 UnityEngine.GUIUtility::Internal_GetHotControl()
		void Register_UnityEngine_GUIUtility_Internal_GetHotControl();
		Register_UnityEngine_GUIUtility_Internal_GetHotControl();

		//System.Int32 UnityEngine.GUIUtility::get_keyboardControl()
		void Register_UnityEngine_GUIUtility_get_keyboardControl();
		Register_UnityEngine_GUIUtility_get_keyboardControl();

		//System.Single UnityEngine.GUIUtility::Internal_GetPixelsPerPoint()
		void Register_UnityEngine_GUIUtility_Internal_GetPixelsPerPoint();
		Register_UnityEngine_GUIUtility_Internal_GetPixelsPerPoint();

		//System.String UnityEngine.GUIUtility::get_systemCopyBuffer()
		void Register_UnityEngine_GUIUtility_get_systemCopyBuffer();
		Register_UnityEngine_GUIUtility_get_systemCopyBuffer();

		//System.Void UnityEngine.GUIUtility::Internal_ExitGUI()
		void Register_UnityEngine_GUIUtility_Internal_ExitGUI();
		Register_UnityEngine_GUIUtility_Internal_ExitGUI();

		//System.Void UnityEngine.GUIUtility::Internal_SetHotControl(System.Int32)
		void Register_UnityEngine_GUIUtility_Internal_SetHotControl();
		Register_UnityEngine_GUIUtility_Internal_SetHotControl();

		//System.Void UnityEngine.GUIUtility::set_keyboardControl(System.Int32)
		void Register_UnityEngine_GUIUtility_set_keyboardControl();
		Register_UnityEngine_GUIUtility_set_keyboardControl();

		//System.Void UnityEngine.GUIUtility::set_mouseUsed(System.Boolean)
		void Register_UnityEngine_GUIUtility_set_mouseUsed();
		Register_UnityEngine_GUIUtility_set_mouseUsed();

		//System.Void UnityEngine.GUIUtility::set_systemCopyBuffer(System.String)
		void Register_UnityEngine_GUIUtility_set_systemCopyBuffer();
		Register_UnityEngine_GUIUtility_set_systemCopyBuffer();

		//System.Void UnityEngine.GUIUtility::set_textFieldInput(System.Boolean)
		void Register_UnityEngine_GUIUtility_set_textFieldInput();
		Register_UnityEngine_GUIUtility_set_textFieldInput();

		//UnityEngine.GUISkin UnityEngine.GUIUtility::Internal_GetDefaultSkin(System.Int32)
		void Register_UnityEngine_GUIUtility_Internal_GetDefaultSkin();
		Register_UnityEngine_GUIUtility_Internal_GetDefaultSkin();

	//End Registrations for type : UnityEngine.GUIUtility

	//Start Registrations for type : UnityEngine.Gyroscope

		//System.Boolean UnityEngine.Gyroscope::getEnabled_Internal(System.Int32)
		void Register_UnityEngine_Gyroscope_getEnabled_Internal();
		Register_UnityEngine_Gyroscope_getEnabled_Internal();

		//System.Single UnityEngine.Gyroscope::getUpdateInterval_Internal(System.Int32)
		void Register_UnityEngine_Gyroscope_getUpdateInterval_Internal();
		Register_UnityEngine_Gyroscope_getUpdateInterval_Internal();

		//System.Void UnityEngine.Gyroscope::INTERNAL_CALL_attitude_Internal(System.Int32,UnityEngine.Quaternion&)
		void Register_UnityEngine_Gyroscope_INTERNAL_CALL_attitude_Internal();
		Register_UnityEngine_Gyroscope_INTERNAL_CALL_attitude_Internal();

		//System.Void UnityEngine.Gyroscope::INTERNAL_CALL_gravity_Internal(System.Int32,UnityEngine.Vector3&)
		void Register_UnityEngine_Gyroscope_INTERNAL_CALL_gravity_Internal();
		Register_UnityEngine_Gyroscope_INTERNAL_CALL_gravity_Internal();

		//System.Void UnityEngine.Gyroscope::INTERNAL_CALL_rotationRateUnbiased_Internal(System.Int32,UnityEngine.Vector3&)
		void Register_UnityEngine_Gyroscope_INTERNAL_CALL_rotationRateUnbiased_Internal();
		Register_UnityEngine_Gyroscope_INTERNAL_CALL_rotationRateUnbiased_Internal();

		//System.Void UnityEngine.Gyroscope::INTERNAL_CALL_rotationRate_Internal(System.Int32,UnityEngine.Vector3&)
		void Register_UnityEngine_Gyroscope_INTERNAL_CALL_rotationRate_Internal();
		Register_UnityEngine_Gyroscope_INTERNAL_CALL_rotationRate_Internal();

		//System.Void UnityEngine.Gyroscope::INTERNAL_CALL_userAcceleration_Internal(System.Int32,UnityEngine.Vector3&)
		void Register_UnityEngine_Gyroscope_INTERNAL_CALL_userAcceleration_Internal();
		Register_UnityEngine_Gyroscope_INTERNAL_CALL_userAcceleration_Internal();

		//System.Void UnityEngine.Gyroscope::setEnabled_Internal(System.Int32,System.Boolean)
		void Register_UnityEngine_Gyroscope_setEnabled_Internal();
		Register_UnityEngine_Gyroscope_setEnabled_Internal();

		//System.Void UnityEngine.Gyroscope::setUpdateInterval_Internal(System.Int32,System.Single)
		void Register_UnityEngine_Gyroscope_setUpdateInterval_Internal();
		Register_UnityEngine_Gyroscope_setUpdateInterval_Internal();

	//End Registrations for type : UnityEngine.Gyroscope

	//Start Registrations for type : UnityEngine.Handheld

		//System.Void UnityEngine.Handheld::Vibrate()
		void Register_UnityEngine_Handheld_Vibrate();
		Register_UnityEngine_Handheld_Vibrate();

	//End Registrations for type : UnityEngine.Handheld

	//Start Registrations for type : UnityEngine.Hash128

		//System.String UnityEngine.Hash128::Internal_Hash128ToString(System.UInt32,System.UInt32,System.UInt32,System.UInt32)
		void Register_UnityEngine_Hash128_Internal_Hash128ToString();
		Register_UnityEngine_Hash128_Internal_Hash128ToString();

	//End Registrations for type : UnityEngine.Hash128

	//Start Registrations for type : UnityEngine.HingeJoint

		//System.Boolean UnityEngine.HingeJoint::get_useLimits()
		void Register_UnityEngine_HingeJoint_get_useLimits();
		Register_UnityEngine_HingeJoint_get_useLimits();

		//System.Boolean UnityEngine.HingeJoint::get_useMotor()
		void Register_UnityEngine_HingeJoint_get_useMotor();
		Register_UnityEngine_HingeJoint_get_useMotor();

		//System.Boolean UnityEngine.HingeJoint::get_useSpring()
		void Register_UnityEngine_HingeJoint_get_useSpring();
		Register_UnityEngine_HingeJoint_get_useSpring();

		//System.Single UnityEngine.HingeJoint::get_angle()
		void Register_UnityEngine_HingeJoint_get_angle();
		Register_UnityEngine_HingeJoint_get_angle();

		//System.Single UnityEngine.HingeJoint::get_velocity()
		void Register_UnityEngine_HingeJoint_get_velocity();
		Register_UnityEngine_HingeJoint_get_velocity();

		//System.Void UnityEngine.HingeJoint::INTERNAL_get_limits(UnityEngine.JointLimits&)
		void Register_UnityEngine_HingeJoint_INTERNAL_get_limits();
		Register_UnityEngine_HingeJoint_INTERNAL_get_limits();

		//System.Void UnityEngine.HingeJoint::INTERNAL_get_motor(UnityEngine.JointMotor&)
		void Register_UnityEngine_HingeJoint_INTERNAL_get_motor();
		Register_UnityEngine_HingeJoint_INTERNAL_get_motor();

		//System.Void UnityEngine.HingeJoint::INTERNAL_get_spring(UnityEngine.JointSpring&)
		void Register_UnityEngine_HingeJoint_INTERNAL_get_spring();
		Register_UnityEngine_HingeJoint_INTERNAL_get_spring();

		//System.Void UnityEngine.HingeJoint::INTERNAL_set_limits(UnityEngine.JointLimits&)
		void Register_UnityEngine_HingeJoint_INTERNAL_set_limits();
		Register_UnityEngine_HingeJoint_INTERNAL_set_limits();

		//System.Void UnityEngine.HingeJoint::INTERNAL_set_motor(UnityEngine.JointMotor&)
		void Register_UnityEngine_HingeJoint_INTERNAL_set_motor();
		Register_UnityEngine_HingeJoint_INTERNAL_set_motor();

		//System.Void UnityEngine.HingeJoint::INTERNAL_set_spring(UnityEngine.JointSpring&)
		void Register_UnityEngine_HingeJoint_INTERNAL_set_spring();
		Register_UnityEngine_HingeJoint_INTERNAL_set_spring();

		//System.Void UnityEngine.HingeJoint::set_useLimits(System.Boolean)
		void Register_UnityEngine_HingeJoint_set_useLimits();
		Register_UnityEngine_HingeJoint_set_useLimits();

		//System.Void UnityEngine.HingeJoint::set_useMotor(System.Boolean)
		void Register_UnityEngine_HingeJoint_set_useMotor();
		Register_UnityEngine_HingeJoint_set_useMotor();

		//System.Void UnityEngine.HingeJoint::set_useSpring(System.Boolean)
		void Register_UnityEngine_HingeJoint_set_useSpring();
		Register_UnityEngine_HingeJoint_set_useSpring();

	//End Registrations for type : UnityEngine.HingeJoint

	//Start Registrations for type : UnityEngine.HingeJoint2D

		//System.Boolean UnityEngine.HingeJoint2D::get_useLimits()
		void Register_UnityEngine_HingeJoint2D_get_useLimits();
		Register_UnityEngine_HingeJoint2D_get_useLimits();

		//System.Boolean UnityEngine.HingeJoint2D::get_useMotor()
		void Register_UnityEngine_HingeJoint2D_get_useMotor();
		Register_UnityEngine_HingeJoint2D_get_useMotor();

		//System.Single UnityEngine.HingeJoint2D::INTERNAL_CALL_GetMotorTorque(UnityEngine.HingeJoint2D,System.Single)
		void Register_UnityEngine_HingeJoint2D_INTERNAL_CALL_GetMotorTorque();
		Register_UnityEngine_HingeJoint2D_INTERNAL_CALL_GetMotorTorque();

		//System.Single UnityEngine.HingeJoint2D::get_jointAngle()
		void Register_UnityEngine_HingeJoint2D_get_jointAngle();
		Register_UnityEngine_HingeJoint2D_get_jointAngle();

		//System.Single UnityEngine.HingeJoint2D::get_jointSpeed()
		void Register_UnityEngine_HingeJoint2D_get_jointSpeed();
		Register_UnityEngine_HingeJoint2D_get_jointSpeed();

		//System.Single UnityEngine.HingeJoint2D::get_referenceAngle()
		void Register_UnityEngine_HingeJoint2D_get_referenceAngle();
		Register_UnityEngine_HingeJoint2D_get_referenceAngle();

		//System.Void UnityEngine.HingeJoint2D::INTERNAL_get_limits(UnityEngine.JointAngleLimits2D&)
		void Register_UnityEngine_HingeJoint2D_INTERNAL_get_limits();
		Register_UnityEngine_HingeJoint2D_INTERNAL_get_limits();

		//System.Void UnityEngine.HingeJoint2D::INTERNAL_get_motor(UnityEngine.JointMotor2D&)
		void Register_UnityEngine_HingeJoint2D_INTERNAL_get_motor();
		Register_UnityEngine_HingeJoint2D_INTERNAL_get_motor();

		//System.Void UnityEngine.HingeJoint2D::INTERNAL_set_limits(UnityEngine.JointAngleLimits2D&)
		void Register_UnityEngine_HingeJoint2D_INTERNAL_set_limits();
		Register_UnityEngine_HingeJoint2D_INTERNAL_set_limits();

		//System.Void UnityEngine.HingeJoint2D::INTERNAL_set_motor(UnityEngine.JointMotor2D&)
		void Register_UnityEngine_HingeJoint2D_INTERNAL_set_motor();
		Register_UnityEngine_HingeJoint2D_INTERNAL_set_motor();

		//System.Void UnityEngine.HingeJoint2D::set_useLimits(System.Boolean)
		void Register_UnityEngine_HingeJoint2D_set_useLimits();
		Register_UnityEngine_HingeJoint2D_set_useLimits();

		//System.Void UnityEngine.HingeJoint2D::set_useMotor(System.Boolean)
		void Register_UnityEngine_HingeJoint2D_set_useMotor();
		Register_UnityEngine_HingeJoint2D_set_useMotor();

		//UnityEngine.JointLimitState2D UnityEngine.HingeJoint2D::get_limitState()
		void Register_UnityEngine_HingeJoint2D_get_limitState();
		Register_UnityEngine_HingeJoint2D_get_limitState();

	//End Registrations for type : UnityEngine.HingeJoint2D

	//Start Registrations for type : UnityEngine.HumanTrait

		//System.Boolean UnityEngine.HumanTrait::RequiredBone(System.Int32)
		void Register_UnityEngine_HumanTrait_RequiredBone();
		Register_UnityEngine_HumanTrait_RequiredBone();

		//System.Int32 UnityEngine.HumanTrait::BoneFromMuscle(System.Int32)
		void Register_UnityEngine_HumanTrait_BoneFromMuscle();
		Register_UnityEngine_HumanTrait_BoneFromMuscle();

		//System.Int32 UnityEngine.HumanTrait::GetParentBone(System.Int32)
		void Register_UnityEngine_HumanTrait_GetParentBone();
		Register_UnityEngine_HumanTrait_GetParentBone();

		//System.Int32 UnityEngine.HumanTrait::MuscleFromBone(System.Int32,System.Int32)
		void Register_UnityEngine_HumanTrait_MuscleFromBone();
		Register_UnityEngine_HumanTrait_MuscleFromBone();

		//System.Int32 UnityEngine.HumanTrait::get_BoneCount()
		void Register_UnityEngine_HumanTrait_get_BoneCount();
		Register_UnityEngine_HumanTrait_get_BoneCount();

		//System.Int32 UnityEngine.HumanTrait::get_MuscleCount()
		void Register_UnityEngine_HumanTrait_get_MuscleCount();
		Register_UnityEngine_HumanTrait_get_MuscleCount();

		//System.Int32 UnityEngine.HumanTrait::get_RequiredBoneCount()
		void Register_UnityEngine_HumanTrait_get_RequiredBoneCount();
		Register_UnityEngine_HumanTrait_get_RequiredBoneCount();

		//System.Single UnityEngine.HumanTrait::GetMuscleDefaultMax(System.Int32)
		void Register_UnityEngine_HumanTrait_GetMuscleDefaultMax();
		Register_UnityEngine_HumanTrait_GetMuscleDefaultMax();

		//System.Single UnityEngine.HumanTrait::GetMuscleDefaultMin(System.Int32)
		void Register_UnityEngine_HumanTrait_GetMuscleDefaultMin();
		Register_UnityEngine_HumanTrait_GetMuscleDefaultMin();

		//System.String[] UnityEngine.HumanTrait::get_BoneName()
		void Register_UnityEngine_HumanTrait_get_BoneName();
		Register_UnityEngine_HumanTrait_get_BoneName();

		//System.String[] UnityEngine.HumanTrait::get_MuscleName()
		void Register_UnityEngine_HumanTrait_get_MuscleName();
		Register_UnityEngine_HumanTrait_get_MuscleName();

	//End Registrations for type : UnityEngine.HumanTrait

	//Start Registrations for type : UnityEngine.Input

		//System.Boolean UnityEngine.Input::GetButton(System.String)
		void Register_UnityEngine_Input_GetButton();
		Register_UnityEngine_Input_GetButton();

		//System.Boolean UnityEngine.Input::GetButtonDown(System.String)
		void Register_UnityEngine_Input_GetButtonDown();
		Register_UnityEngine_Input_GetButtonDown();

		//System.Boolean UnityEngine.Input::GetButtonUp(System.String)
		void Register_UnityEngine_Input_GetButtonUp();
		Register_UnityEngine_Input_GetButtonUp();

		//System.Boolean UnityEngine.Input::GetKeyDownInt(System.Int32)
		void Register_UnityEngine_Input_GetKeyDownInt();
		Register_UnityEngine_Input_GetKeyDownInt();

		//System.Boolean UnityEngine.Input::GetKeyDownString(System.String)
		void Register_UnityEngine_Input_GetKeyDownString();
		Register_UnityEngine_Input_GetKeyDownString();

		//System.Boolean UnityEngine.Input::GetKeyInt(System.Int32)
		void Register_UnityEngine_Input_GetKeyInt();
		Register_UnityEngine_Input_GetKeyInt();

		//System.Boolean UnityEngine.Input::GetKeyString(System.String)
		void Register_UnityEngine_Input_GetKeyString();
		Register_UnityEngine_Input_GetKeyString();

		//System.Boolean UnityEngine.Input::GetKeyUpInt(System.Int32)
		void Register_UnityEngine_Input_GetKeyUpInt();
		Register_UnityEngine_Input_GetKeyUpInt();

		//System.Boolean UnityEngine.Input::GetKeyUpString(System.String)
		void Register_UnityEngine_Input_GetKeyUpString();
		Register_UnityEngine_Input_GetKeyUpString();

		//System.Boolean UnityEngine.Input::GetMouseButton(System.Int32)
		void Register_UnityEngine_Input_GetMouseButton();
		Register_UnityEngine_Input_GetMouseButton();

		//System.Boolean UnityEngine.Input::GetMouseButtonDown(System.Int32)
		void Register_UnityEngine_Input_GetMouseButtonDown();
		Register_UnityEngine_Input_GetMouseButtonDown();

		//System.Boolean UnityEngine.Input::GetMouseButtonUp(System.Int32)
		void Register_UnityEngine_Input_GetMouseButtonUp();
		Register_UnityEngine_Input_GetMouseButtonUp();

		//System.Boolean UnityEngine.Input::get_anyKey()
		void Register_UnityEngine_Input_get_anyKey();
		Register_UnityEngine_Input_get_anyKey();

		//System.Boolean UnityEngine.Input::get_anyKeyDown()
		void Register_UnityEngine_Input_get_anyKeyDown();
		Register_UnityEngine_Input_get_anyKeyDown();

		//System.Boolean UnityEngine.Input::get_backButtonLeavesApp()
		void Register_UnityEngine_Input_get_backButtonLeavesApp();
		Register_UnityEngine_Input_get_backButtonLeavesApp();

		//System.Boolean UnityEngine.Input::get_compensateSensors()
		void Register_UnityEngine_Input_get_compensateSensors();
		Register_UnityEngine_Input_get_compensateSensors();

		//System.Boolean UnityEngine.Input::get_imeIsSelected()
		void Register_UnityEngine_Input_get_imeIsSelected();
		Register_UnityEngine_Input_get_imeIsSelected();

		//System.Boolean UnityEngine.Input::get_mousePresent()
		void Register_UnityEngine_Input_get_mousePresent();
		Register_UnityEngine_Input_get_mousePresent();

		//System.Boolean UnityEngine.Input::get_multiTouchEnabled()
		void Register_UnityEngine_Input_get_multiTouchEnabled();
		Register_UnityEngine_Input_get_multiTouchEnabled();

		//System.Boolean UnityEngine.Input::get_simulateMouseWithTouches()
		void Register_UnityEngine_Input_get_simulateMouseWithTouches();
		Register_UnityEngine_Input_get_simulateMouseWithTouches();

		//System.Boolean UnityEngine.Input::get_stylusTouchSupported()
		void Register_UnityEngine_Input_get_stylusTouchSupported();
		Register_UnityEngine_Input_get_stylusTouchSupported();

		//System.Boolean UnityEngine.Input::get_touchPressureSupported()
		void Register_UnityEngine_Input_get_touchPressureSupported();
		Register_UnityEngine_Input_get_touchPressureSupported();

		//System.Int32 UnityEngine.Input::get_accelerationEventCount()
		void Register_UnityEngine_Input_get_accelerationEventCount();
		Register_UnityEngine_Input_get_accelerationEventCount();

		//System.Int32 UnityEngine.Input::get_touchCount()
		void Register_UnityEngine_Input_get_touchCount();
		Register_UnityEngine_Input_get_touchCount();

		//System.Int32 UnityEngine.Input::mainGyroIndex_Internal()
		void Register_UnityEngine_Input_mainGyroIndex_Internal();
		Register_UnityEngine_Input_mainGyroIndex_Internal();

		//System.Single UnityEngine.Input::GetAxis(System.String)
		void Register_UnityEngine_Input_GetAxis();
		Register_UnityEngine_Input_GetAxis();

		//System.Single UnityEngine.Input::GetAxisRaw(System.String)
		void Register_UnityEngine_Input_GetAxisRaw();
		Register_UnityEngine_Input_GetAxisRaw();

		//System.String UnityEngine.Input::get_compositionString()
		void Register_UnityEngine_Input_get_compositionString();
		Register_UnityEngine_Input_get_compositionString();

		//System.String UnityEngine.Input::get_inputString()
		void Register_UnityEngine_Input_get_inputString();
		Register_UnityEngine_Input_get_inputString();

		//System.String[] UnityEngine.Input::GetJoystickNames()
		void Register_UnityEngine_Input_GetJoystickNames();
		Register_UnityEngine_Input_GetJoystickNames();

		//System.Void UnityEngine.Input::INTERNAL_CALL_GetAccelerationEvent(System.Int32,UnityEngine.AccelerationEvent&)
		void Register_UnityEngine_Input_INTERNAL_CALL_GetAccelerationEvent();
		Register_UnityEngine_Input_INTERNAL_CALL_GetAccelerationEvent();

		//System.Void UnityEngine.Input::INTERNAL_CALL_GetTouch(System.Int32,UnityEngine.Touch&)
		void Register_UnityEngine_Input_INTERNAL_CALL_GetTouch();
		Register_UnityEngine_Input_INTERNAL_CALL_GetTouch();

		//System.Void UnityEngine.Input::INTERNAL_get_acceleration(UnityEngine.Vector3&)
		void Register_UnityEngine_Input_INTERNAL_get_acceleration();
		Register_UnityEngine_Input_INTERNAL_get_acceleration();

		//System.Void UnityEngine.Input::INTERNAL_get_compositionCursorPos(UnityEngine.Vector2&)
		void Register_UnityEngine_Input_INTERNAL_get_compositionCursorPos();
		Register_UnityEngine_Input_INTERNAL_get_compositionCursorPos();

		//System.Void UnityEngine.Input::INTERNAL_get_mousePosition(UnityEngine.Vector3&)
		void Register_UnityEngine_Input_INTERNAL_get_mousePosition();
		Register_UnityEngine_Input_INTERNAL_get_mousePosition();

		//System.Void UnityEngine.Input::INTERNAL_get_mouseScrollDelta(UnityEngine.Vector2&)
		void Register_UnityEngine_Input_INTERNAL_get_mouseScrollDelta();
		Register_UnityEngine_Input_INTERNAL_get_mouseScrollDelta();

		//System.Void UnityEngine.Input::INTERNAL_set_compositionCursorPos(UnityEngine.Vector2&)
		void Register_UnityEngine_Input_INTERNAL_set_compositionCursorPos();
		Register_UnityEngine_Input_INTERNAL_set_compositionCursorPos();

		//System.Void UnityEngine.Input::ResetInputAxes()
		void Register_UnityEngine_Input_ResetInputAxes();
		Register_UnityEngine_Input_ResetInputAxes();

		//System.Void UnityEngine.Input::set_backButtonLeavesApp(System.Boolean)
		void Register_UnityEngine_Input_set_backButtonLeavesApp();
		Register_UnityEngine_Input_set_backButtonLeavesApp();

		//System.Void UnityEngine.Input::set_compensateSensors(System.Boolean)
		void Register_UnityEngine_Input_set_compensateSensors();
		Register_UnityEngine_Input_set_compensateSensors();

		//System.Void UnityEngine.Input::set_imeCompositionMode(UnityEngine.IMECompositionMode)
		void Register_UnityEngine_Input_set_imeCompositionMode();
		Register_UnityEngine_Input_set_imeCompositionMode();

		//System.Void UnityEngine.Input::set_multiTouchEnabled(System.Boolean)
		void Register_UnityEngine_Input_set_multiTouchEnabled();
		Register_UnityEngine_Input_set_multiTouchEnabled();

		//System.Void UnityEngine.Input::set_simulateMouseWithTouches(System.Boolean)
		void Register_UnityEngine_Input_set_simulateMouseWithTouches();
		Register_UnityEngine_Input_set_simulateMouseWithTouches();

		//UnityEngine.DeviceOrientation UnityEngine.Input::get_deviceOrientation()
		void Register_UnityEngine_Input_get_deviceOrientation();
		Register_UnityEngine_Input_get_deviceOrientation();

		//UnityEngine.IMECompositionMode UnityEngine.Input::get_imeCompositionMode()
		void Register_UnityEngine_Input_get_imeCompositionMode();
		Register_UnityEngine_Input_get_imeCompositionMode();

	//End Registrations for type : UnityEngine.Input

	//Start Registrations for type : UnityEngine.iOS.ADBannerView

		//System.Void UnityEngine.iOS.ADBannerView::Native_DestroyBanner(System.IntPtr)
		void Register_UnityEngine_iOS_ADBannerView_Native_DestroyBanner();
		Register_UnityEngine_iOS_ADBannerView_Native_DestroyBanner();

	//End Registrations for type : UnityEngine.iOS.ADBannerView

	//Start Registrations for type : UnityEngine.iOS.ADInterstitialAd

		//System.Void UnityEngine.iOS.ADInterstitialAd::Native_DestroyInterstitial(System.IntPtr)
		void Register_UnityEngine_iOS_ADInterstitialAd_Native_DestroyInterstitial();
		Register_UnityEngine_iOS_ADInterstitialAd_Native_DestroyInterstitial();

	//End Registrations for type : UnityEngine.iOS.ADInterstitialAd

	//Start Registrations for type : UnityEngine.iOS.Device

		//System.String UnityEngine.iOS.Device::get_vendorIdentifier()
		void Register_UnityEngine_iOS_Device_get_vendorIdentifier();
		Register_UnityEngine_iOS_Device_get_vendorIdentifier();

		//UnityEngine.iOS.DeviceGeneration UnityEngine.iOS.Device::get_generation()
		void Register_UnityEngine_iOS_Device_get_generation();
		Register_UnityEngine_iOS_Device_get_generation();

	//End Registrations for type : UnityEngine.iOS.Device

	//Start Registrations for type : UnityEngine.iOS.LocalNotification

		//System.Boolean UnityEngine.iOS.LocalNotification::get_hasAction()
		void Register_UnityEngine_iOS_LocalNotification_get_hasAction();
		Register_UnityEngine_iOS_LocalNotification_get_hasAction();

		//System.Collections.IDictionary UnityEngine.iOS.LocalNotification::get_userInfo()
		void Register_UnityEngine_iOS_LocalNotification_get_userInfo();
		Register_UnityEngine_iOS_LocalNotification_get_userInfo();

		//System.Double UnityEngine.iOS.LocalNotification::GetFireDate()
		void Register_UnityEngine_iOS_LocalNotification_GetFireDate();
		Register_UnityEngine_iOS_LocalNotification_GetFireDate();

		//System.Int32 UnityEngine.iOS.LocalNotification::get_applicationIconBadgeNumber()
		void Register_UnityEngine_iOS_LocalNotification_get_applicationIconBadgeNumber();
		Register_UnityEngine_iOS_LocalNotification_get_applicationIconBadgeNumber();

		//System.String UnityEngine.iOS.LocalNotification::get_alertAction()
		void Register_UnityEngine_iOS_LocalNotification_get_alertAction();
		Register_UnityEngine_iOS_LocalNotification_get_alertAction();

		//System.String UnityEngine.iOS.LocalNotification::get_alertBody()
		void Register_UnityEngine_iOS_LocalNotification_get_alertBody();
		Register_UnityEngine_iOS_LocalNotification_get_alertBody();

		//System.String UnityEngine.iOS.LocalNotification::get_alertLaunchImage()
		void Register_UnityEngine_iOS_LocalNotification_get_alertLaunchImage();
		Register_UnityEngine_iOS_LocalNotification_get_alertLaunchImage();

		//System.String UnityEngine.iOS.LocalNotification::get_defaultSoundName()
		void Register_UnityEngine_iOS_LocalNotification_get_defaultSoundName();
		Register_UnityEngine_iOS_LocalNotification_get_defaultSoundName();

		//System.String UnityEngine.iOS.LocalNotification::get_soundName()
		void Register_UnityEngine_iOS_LocalNotification_get_soundName();
		Register_UnityEngine_iOS_LocalNotification_get_soundName();

		//System.String UnityEngine.iOS.LocalNotification::get_timeZone()
		void Register_UnityEngine_iOS_LocalNotification_get_timeZone();
		Register_UnityEngine_iOS_LocalNotification_get_timeZone();

		//System.Void UnityEngine.iOS.LocalNotification::Destroy()
		void Register_UnityEngine_iOS_LocalNotification_Destroy();
		Register_UnityEngine_iOS_LocalNotification_Destroy();

		//System.Void UnityEngine.iOS.LocalNotification::InitWrapper()
		void Register_UnityEngine_iOS_LocalNotification_InitWrapper();
		Register_UnityEngine_iOS_LocalNotification_InitWrapper();

		//System.Void UnityEngine.iOS.LocalNotification::SetFireDate(System.Double)
		void Register_UnityEngine_iOS_LocalNotification_SetFireDate();
		Register_UnityEngine_iOS_LocalNotification_SetFireDate();

		//System.Void UnityEngine.iOS.LocalNotification::set_alertAction(System.String)
		void Register_UnityEngine_iOS_LocalNotification_set_alertAction();
		Register_UnityEngine_iOS_LocalNotification_set_alertAction();

		//System.Void UnityEngine.iOS.LocalNotification::set_alertBody(System.String)
		void Register_UnityEngine_iOS_LocalNotification_set_alertBody();
		Register_UnityEngine_iOS_LocalNotification_set_alertBody();

		//System.Void UnityEngine.iOS.LocalNotification::set_alertLaunchImage(System.String)
		void Register_UnityEngine_iOS_LocalNotification_set_alertLaunchImage();
		Register_UnityEngine_iOS_LocalNotification_set_alertLaunchImage();

		//System.Void UnityEngine.iOS.LocalNotification::set_applicationIconBadgeNumber(System.Int32)
		void Register_UnityEngine_iOS_LocalNotification_set_applicationIconBadgeNumber();
		Register_UnityEngine_iOS_LocalNotification_set_applicationIconBadgeNumber();

		//System.Void UnityEngine.iOS.LocalNotification::set_hasAction(System.Boolean)
		void Register_UnityEngine_iOS_LocalNotification_set_hasAction();
		Register_UnityEngine_iOS_LocalNotification_set_hasAction();

		//System.Void UnityEngine.iOS.LocalNotification::set_repeatCalendar(UnityEngine.iOS.CalendarIdentifier)
		void Register_UnityEngine_iOS_LocalNotification_set_repeatCalendar();
		Register_UnityEngine_iOS_LocalNotification_set_repeatCalendar();

		//System.Void UnityEngine.iOS.LocalNotification::set_repeatInterval(UnityEngine.iOS.CalendarUnit)
		void Register_UnityEngine_iOS_LocalNotification_set_repeatInterval();
		Register_UnityEngine_iOS_LocalNotification_set_repeatInterval();

		//System.Void UnityEngine.iOS.LocalNotification::set_soundName(System.String)
		void Register_UnityEngine_iOS_LocalNotification_set_soundName();
		Register_UnityEngine_iOS_LocalNotification_set_soundName();

		//System.Void UnityEngine.iOS.LocalNotification::set_timeZone(System.String)
		void Register_UnityEngine_iOS_LocalNotification_set_timeZone();
		Register_UnityEngine_iOS_LocalNotification_set_timeZone();

		//System.Void UnityEngine.iOS.LocalNotification::set_userInfo(System.Collections.IDictionary)
		void Register_UnityEngine_iOS_LocalNotification_set_userInfo();
		Register_UnityEngine_iOS_LocalNotification_set_userInfo();

		//UnityEngine.iOS.CalendarIdentifier UnityEngine.iOS.LocalNotification::get_repeatCalendar()
		void Register_UnityEngine_iOS_LocalNotification_get_repeatCalendar();
		Register_UnityEngine_iOS_LocalNotification_get_repeatCalendar();

		//UnityEngine.iOS.CalendarUnit UnityEngine.iOS.LocalNotification::get_repeatInterval()
		void Register_UnityEngine_iOS_LocalNotification_get_repeatInterval();
		Register_UnityEngine_iOS_LocalNotification_get_repeatInterval();

	//End Registrations for type : UnityEngine.iOS.LocalNotification

	//Start Registrations for type : UnityEngine.iOS.NotificationServices

		//System.Void UnityEngine.iOS.NotificationServices::CancelAllLocalNotifications()
		void Register_UnityEngine_iOS_NotificationServices_CancelAllLocalNotifications();
		Register_UnityEngine_iOS_NotificationServices_CancelAllLocalNotifications();

		//System.Void UnityEngine.iOS.NotificationServices::ClearLocalNotifications()
		void Register_UnityEngine_iOS_NotificationServices_ClearLocalNotifications();
		Register_UnityEngine_iOS_NotificationServices_ClearLocalNotifications();

		//System.Void UnityEngine.iOS.NotificationServices::PresentLocalNotificationNow(UnityEngine.iOS.LocalNotification)
		void Register_UnityEngine_iOS_NotificationServices_PresentLocalNotificationNow();
		Register_UnityEngine_iOS_NotificationServices_PresentLocalNotificationNow();

		//System.Void UnityEngine.iOS.NotificationServices::RegisterForNotifications(UnityEngine.iOS.NotificationType,System.Boolean)
		void Register_UnityEngine_iOS_NotificationServices_RegisterForNotifications();
		Register_UnityEngine_iOS_NotificationServices_RegisterForNotifications();

		//System.Void UnityEngine.iOS.NotificationServices::ScheduleLocalNotification(UnityEngine.iOS.LocalNotification)
		void Register_UnityEngine_iOS_NotificationServices_ScheduleLocalNotification();
		Register_UnityEngine_iOS_NotificationServices_ScheduleLocalNotification();

	//End Registrations for type : UnityEngine.iOS.NotificationServices

	//Start Registrations for type : UnityEngine.iOS.RemoteNotification

		//System.Boolean UnityEngine.iOS.RemoteNotification::get_hasAction()
		void Register_UnityEngine_iOS_RemoteNotification_get_hasAction();
		Register_UnityEngine_iOS_RemoteNotification_get_hasAction();

		//System.Collections.IDictionary UnityEngine.iOS.RemoteNotification::get_userInfo()
		void Register_UnityEngine_iOS_RemoteNotification_get_userInfo();
		Register_UnityEngine_iOS_RemoteNotification_get_userInfo();

		//System.Int32 UnityEngine.iOS.RemoteNotification::get_applicationIconBadgeNumber()
		void Register_UnityEngine_iOS_RemoteNotification_get_applicationIconBadgeNumber();
		Register_UnityEngine_iOS_RemoteNotification_get_applicationIconBadgeNumber();

		//System.String UnityEngine.iOS.RemoteNotification::get_alertBody()
		void Register_UnityEngine_iOS_RemoteNotification_get_alertBody();
		Register_UnityEngine_iOS_RemoteNotification_get_alertBody();

		//System.String UnityEngine.iOS.RemoteNotification::get_soundName()
		void Register_UnityEngine_iOS_RemoteNotification_get_soundName();
		Register_UnityEngine_iOS_RemoteNotification_get_soundName();

		//System.Void UnityEngine.iOS.RemoteNotification::Destroy()
		void Register_UnityEngine_iOS_RemoteNotification_Destroy();
		Register_UnityEngine_iOS_RemoteNotification_Destroy();

	//End Registrations for type : UnityEngine.iOS.RemoteNotification

	//Start Registrations for type : UnityEngine.Joint

		//System.Boolean UnityEngine.Joint::get_autoConfigureConnectedAnchor()
		void Register_UnityEngine_Joint_get_autoConfigureConnectedAnchor();
		Register_UnityEngine_Joint_get_autoConfigureConnectedAnchor();

		//System.Boolean UnityEngine.Joint::get_enableCollision()
		void Register_UnityEngine_Joint_get_enableCollision();
		Register_UnityEngine_Joint_get_enableCollision();

		//System.Boolean UnityEngine.Joint::get_enablePreprocessing()
		void Register_UnityEngine_Joint_get_enablePreprocessing();
		Register_UnityEngine_Joint_get_enablePreprocessing();

		//System.Single UnityEngine.Joint::get_breakForce()
		void Register_UnityEngine_Joint_get_breakForce();
		Register_UnityEngine_Joint_get_breakForce();

		//System.Single UnityEngine.Joint::get_breakTorque()
		void Register_UnityEngine_Joint_get_breakTorque();
		Register_UnityEngine_Joint_get_breakTorque();

		//System.Void UnityEngine.Joint::INTERNAL_get_anchor(UnityEngine.Vector3&)
		void Register_UnityEngine_Joint_INTERNAL_get_anchor();
		Register_UnityEngine_Joint_INTERNAL_get_anchor();

		//System.Void UnityEngine.Joint::INTERNAL_get_axis(UnityEngine.Vector3&)
		void Register_UnityEngine_Joint_INTERNAL_get_axis();
		Register_UnityEngine_Joint_INTERNAL_get_axis();

		//System.Void UnityEngine.Joint::INTERNAL_get_connectedAnchor(UnityEngine.Vector3&)
		void Register_UnityEngine_Joint_INTERNAL_get_connectedAnchor();
		Register_UnityEngine_Joint_INTERNAL_get_connectedAnchor();

		//System.Void UnityEngine.Joint::INTERNAL_set_anchor(UnityEngine.Vector3&)
		void Register_UnityEngine_Joint_INTERNAL_set_anchor();
		Register_UnityEngine_Joint_INTERNAL_set_anchor();

		//System.Void UnityEngine.Joint::INTERNAL_set_axis(UnityEngine.Vector3&)
		void Register_UnityEngine_Joint_INTERNAL_set_axis();
		Register_UnityEngine_Joint_INTERNAL_set_axis();

		//System.Void UnityEngine.Joint::INTERNAL_set_connectedAnchor(UnityEngine.Vector3&)
		void Register_UnityEngine_Joint_INTERNAL_set_connectedAnchor();
		Register_UnityEngine_Joint_INTERNAL_set_connectedAnchor();

		//System.Void UnityEngine.Joint::set_autoConfigureConnectedAnchor(System.Boolean)
		void Register_UnityEngine_Joint_set_autoConfigureConnectedAnchor();
		Register_UnityEngine_Joint_set_autoConfigureConnectedAnchor();

		//System.Void UnityEngine.Joint::set_breakForce(System.Single)
		void Register_UnityEngine_Joint_set_breakForce();
		Register_UnityEngine_Joint_set_breakForce();

		//System.Void UnityEngine.Joint::set_breakTorque(System.Single)
		void Register_UnityEngine_Joint_set_breakTorque();
		Register_UnityEngine_Joint_set_breakTorque();

		//System.Void UnityEngine.Joint::set_connectedBody(UnityEngine.Rigidbody)
		void Register_UnityEngine_Joint_set_connectedBody();
		Register_UnityEngine_Joint_set_connectedBody();

		//System.Void UnityEngine.Joint::set_enableCollision(System.Boolean)
		void Register_UnityEngine_Joint_set_enableCollision();
		Register_UnityEngine_Joint_set_enableCollision();

		//System.Void UnityEngine.Joint::set_enablePreprocessing(System.Boolean)
		void Register_UnityEngine_Joint_set_enablePreprocessing();
		Register_UnityEngine_Joint_set_enablePreprocessing();

		//UnityEngine.Rigidbody UnityEngine.Joint::get_connectedBody()
		void Register_UnityEngine_Joint_get_connectedBody();
		Register_UnityEngine_Joint_get_connectedBody();

	//End Registrations for type : UnityEngine.Joint

	//Start Registrations for type : UnityEngine.Joint2D

		//System.Boolean UnityEngine.Joint2D::get_enableCollision()
		void Register_UnityEngine_Joint2D_get_enableCollision();
		Register_UnityEngine_Joint2D_get_enableCollision();

		//System.Single UnityEngine.Joint2D::INTERNAL_CALL_GetReactionTorque(UnityEngine.Joint2D,System.Single)
		void Register_UnityEngine_Joint2D_INTERNAL_CALL_GetReactionTorque();
		Register_UnityEngine_Joint2D_INTERNAL_CALL_GetReactionTorque();

		//System.Single UnityEngine.Joint2D::get_breakForce()
		void Register_UnityEngine_Joint2D_get_breakForce();
		Register_UnityEngine_Joint2D_get_breakForce();

		//System.Single UnityEngine.Joint2D::get_breakTorque()
		void Register_UnityEngine_Joint2D_get_breakTorque();
		Register_UnityEngine_Joint2D_get_breakTorque();

		//System.Void UnityEngine.Joint2D::Joint2D_CUSTOM_INTERNAL_GetReactionForce(UnityEngine.Joint2D,System.Single,UnityEngine.Vector2&)
		void Register_UnityEngine_Joint2D_Joint2D_CUSTOM_INTERNAL_GetReactionForce();
		Register_UnityEngine_Joint2D_Joint2D_CUSTOM_INTERNAL_GetReactionForce();

		//System.Void UnityEngine.Joint2D::set_breakForce(System.Single)
		void Register_UnityEngine_Joint2D_set_breakForce();
		Register_UnityEngine_Joint2D_set_breakForce();

		//System.Void UnityEngine.Joint2D::set_breakTorque(System.Single)
		void Register_UnityEngine_Joint2D_set_breakTorque();
		Register_UnityEngine_Joint2D_set_breakTorque();

		//System.Void UnityEngine.Joint2D::set_connectedBody(UnityEngine.Rigidbody2D)
		void Register_UnityEngine_Joint2D_set_connectedBody();
		Register_UnityEngine_Joint2D_set_connectedBody();

		//System.Void UnityEngine.Joint2D::set_enableCollision(System.Boolean)
		void Register_UnityEngine_Joint2D_set_enableCollision();
		Register_UnityEngine_Joint2D_set_enableCollision();

		//UnityEngine.Rigidbody2D UnityEngine.Joint2D::get_connectedBody()
		void Register_UnityEngine_Joint2D_get_connectedBody();
		Register_UnityEngine_Joint2D_get_connectedBody();

	//End Registrations for type : UnityEngine.Joint2D

	//Start Registrations for type : UnityEngine.JsonUtility

		//System.Object UnityEngine.JsonUtility::FromJson(System.String,System.Type)
		void Register_UnityEngine_JsonUtility_FromJson();
		Register_UnityEngine_JsonUtility_FromJson();

	//End Registrations for type : UnityEngine.JsonUtility

	//Start Registrations for type : UnityEngine.LayerMask

		//System.Int32 UnityEngine.LayerMask::NameToLayer(System.String)
		void Register_UnityEngine_LayerMask_NameToLayer();
		Register_UnityEngine_LayerMask_NameToLayer();

		//System.String UnityEngine.LayerMask::LayerToName(System.Int32)
		void Register_UnityEngine_LayerMask_LayerToName();
		Register_UnityEngine_LayerMask_LayerToName();

	//End Registrations for type : UnityEngine.LayerMask

	//Start Registrations for type : UnityEngine.LensFlare

		//System.Single UnityEngine.LensFlare::get_brightness()
		void Register_UnityEngine_LensFlare_get_brightness();
		Register_UnityEngine_LensFlare_get_brightness();

		//System.Single UnityEngine.LensFlare::get_fadeSpeed()
		void Register_UnityEngine_LensFlare_get_fadeSpeed();
		Register_UnityEngine_LensFlare_get_fadeSpeed();

		//System.Void UnityEngine.LensFlare::INTERNAL_get_color(UnityEngine.Color&)
		void Register_UnityEngine_LensFlare_INTERNAL_get_color();
		Register_UnityEngine_LensFlare_INTERNAL_get_color();

		//System.Void UnityEngine.LensFlare::INTERNAL_set_color(UnityEngine.Color&)
		void Register_UnityEngine_LensFlare_INTERNAL_set_color();
		Register_UnityEngine_LensFlare_INTERNAL_set_color();

		//System.Void UnityEngine.LensFlare::set_brightness(System.Single)
		void Register_UnityEngine_LensFlare_set_brightness();
		Register_UnityEngine_LensFlare_set_brightness();

		//System.Void UnityEngine.LensFlare::set_fadeSpeed(System.Single)
		void Register_UnityEngine_LensFlare_set_fadeSpeed();
		Register_UnityEngine_LensFlare_set_fadeSpeed();

		//System.Void UnityEngine.LensFlare::set_flare(UnityEngine.Flare)
		void Register_UnityEngine_LensFlare_set_flare();
		Register_UnityEngine_LensFlare_set_flare();

		//UnityEngine.Flare UnityEngine.LensFlare::get_flare()
		void Register_UnityEngine_LensFlare_get_flare();
		Register_UnityEngine_LensFlare_get_flare();

	//End Registrations for type : UnityEngine.LensFlare

	//Start Registrations for type : UnityEngine.Light

		//System.Boolean UnityEngine.Light::get_alreadyLightmapped()
		void Register_UnityEngine_Light_get_alreadyLightmapped();
		Register_UnityEngine_Light_get_alreadyLightmapped();

		//System.Int32 UnityEngine.Light::get_commandBufferCount()
		void Register_UnityEngine_Light_get_commandBufferCount();
		Register_UnityEngine_Light_get_commandBufferCount();

		//System.Int32 UnityEngine.Light::get_cullingMask()
		void Register_UnityEngine_Light_get_cullingMask();
		Register_UnityEngine_Light_get_cullingMask();

		//System.Single UnityEngine.Light::get_bounceIntensity()
		void Register_UnityEngine_Light_get_bounceIntensity();
		Register_UnityEngine_Light_get_bounceIntensity();

		//System.Single UnityEngine.Light::get_cookieSize()
		void Register_UnityEngine_Light_get_cookieSize();
		Register_UnityEngine_Light_get_cookieSize();

		//System.Single UnityEngine.Light::get_intensity()
		void Register_UnityEngine_Light_get_intensity();
		Register_UnityEngine_Light_get_intensity();

		//System.Single UnityEngine.Light::get_range()
		void Register_UnityEngine_Light_get_range();
		Register_UnityEngine_Light_get_range();

		//System.Single UnityEngine.Light::get_shadowBias()
		void Register_UnityEngine_Light_get_shadowBias();
		Register_UnityEngine_Light_get_shadowBias();

		//System.Single UnityEngine.Light::get_shadowNearPlane()
		void Register_UnityEngine_Light_get_shadowNearPlane();
		Register_UnityEngine_Light_get_shadowNearPlane();

		//System.Single UnityEngine.Light::get_shadowNormalBias()
		void Register_UnityEngine_Light_get_shadowNormalBias();
		Register_UnityEngine_Light_get_shadowNormalBias();

		//System.Single UnityEngine.Light::get_shadowStrength()
		void Register_UnityEngine_Light_get_shadowStrength();
		Register_UnityEngine_Light_get_shadowStrength();

		//System.Single UnityEngine.Light::get_spotAngle()
		void Register_UnityEngine_Light_get_spotAngle();
		Register_UnityEngine_Light_get_spotAngle();

		//System.Void UnityEngine.Light::AddCommandBuffer(UnityEngine.Rendering.LightEvent,UnityEngine.Rendering.CommandBuffer)
		void Register_UnityEngine_Light_AddCommandBuffer();
		Register_UnityEngine_Light_AddCommandBuffer();

		//System.Void UnityEngine.Light::INTERNAL_get_color(UnityEngine.Color&)
		void Register_UnityEngine_Light_INTERNAL_get_color();
		Register_UnityEngine_Light_INTERNAL_get_color();

		//System.Void UnityEngine.Light::INTERNAL_set_color(UnityEngine.Color&)
		void Register_UnityEngine_Light_INTERNAL_set_color();
		Register_UnityEngine_Light_INTERNAL_set_color();

		//System.Void UnityEngine.Light::RemoveAllCommandBuffers()
		void Register_UnityEngine_Light_RemoveAllCommandBuffers();
		Register_UnityEngine_Light_RemoveAllCommandBuffers();

		//System.Void UnityEngine.Light::RemoveCommandBuffer(UnityEngine.Rendering.LightEvent,UnityEngine.Rendering.CommandBuffer)
		void Register_UnityEngine_Light_RemoveCommandBuffer();
		Register_UnityEngine_Light_RemoveCommandBuffer();

		//System.Void UnityEngine.Light::RemoveCommandBuffers(UnityEngine.Rendering.LightEvent)
		void Register_UnityEngine_Light_RemoveCommandBuffers();
		Register_UnityEngine_Light_RemoveCommandBuffers();

		//System.Void UnityEngine.Light::set_alreadyLightmapped(System.Boolean)
		void Register_UnityEngine_Light_set_alreadyLightmapped();
		Register_UnityEngine_Light_set_alreadyLightmapped();

		//System.Void UnityEngine.Light::set_bounceIntensity(System.Single)
		void Register_UnityEngine_Light_set_bounceIntensity();
		Register_UnityEngine_Light_set_bounceIntensity();

		//System.Void UnityEngine.Light::set_cookie(UnityEngine.Texture)
		void Register_UnityEngine_Light_set_cookie();
		Register_UnityEngine_Light_set_cookie();

		//System.Void UnityEngine.Light::set_cookieSize(System.Single)
		void Register_UnityEngine_Light_set_cookieSize();
		Register_UnityEngine_Light_set_cookieSize();

		//System.Void UnityEngine.Light::set_cullingMask(System.Int32)
		void Register_UnityEngine_Light_set_cullingMask();
		Register_UnityEngine_Light_set_cullingMask();

		//System.Void UnityEngine.Light::set_flare(UnityEngine.Flare)
		void Register_UnityEngine_Light_set_flare();
		Register_UnityEngine_Light_set_flare();

		//System.Void UnityEngine.Light::set_intensity(System.Single)
		void Register_UnityEngine_Light_set_intensity();
		Register_UnityEngine_Light_set_intensity();

		//System.Void UnityEngine.Light::set_range(System.Single)
		void Register_UnityEngine_Light_set_range();
		Register_UnityEngine_Light_set_range();

		//System.Void UnityEngine.Light::set_renderMode(UnityEngine.LightRenderMode)
		void Register_UnityEngine_Light_set_renderMode();
		Register_UnityEngine_Light_set_renderMode();

		//System.Void UnityEngine.Light::set_shadowBias(System.Single)
		void Register_UnityEngine_Light_set_shadowBias();
		Register_UnityEngine_Light_set_shadowBias();

		//System.Void UnityEngine.Light::set_shadowNearPlane(System.Single)
		void Register_UnityEngine_Light_set_shadowNearPlane();
		Register_UnityEngine_Light_set_shadowNearPlane();

		//System.Void UnityEngine.Light::set_shadowNormalBias(System.Single)
		void Register_UnityEngine_Light_set_shadowNormalBias();
		Register_UnityEngine_Light_set_shadowNormalBias();

		//System.Void UnityEngine.Light::set_shadowStrength(System.Single)
		void Register_UnityEngine_Light_set_shadowStrength();
		Register_UnityEngine_Light_set_shadowStrength();

		//System.Void UnityEngine.Light::set_shadows(UnityEngine.LightShadows)
		void Register_UnityEngine_Light_set_shadows();
		Register_UnityEngine_Light_set_shadows();

		//System.Void UnityEngine.Light::set_spotAngle(System.Single)
		void Register_UnityEngine_Light_set_spotAngle();
		Register_UnityEngine_Light_set_spotAngle();

		//System.Void UnityEngine.Light::set_type(UnityEngine.LightType)
		void Register_UnityEngine_Light_set_type();
		Register_UnityEngine_Light_set_type();

		//UnityEngine.Flare UnityEngine.Light::get_flare()
		void Register_UnityEngine_Light_get_flare();
		Register_UnityEngine_Light_get_flare();

		//UnityEngine.LightRenderMode UnityEngine.Light::get_renderMode()
		void Register_UnityEngine_Light_get_renderMode();
		Register_UnityEngine_Light_get_renderMode();

		//UnityEngine.LightShadows UnityEngine.Light::get_shadows()
		void Register_UnityEngine_Light_get_shadows();
		Register_UnityEngine_Light_get_shadows();

		//UnityEngine.LightType UnityEngine.Light::get_type()
		void Register_UnityEngine_Light_get_type();
		Register_UnityEngine_Light_get_type();

		//UnityEngine.Light[] UnityEngine.Light::GetLights(UnityEngine.LightType,System.Int32)
		void Register_UnityEngine_Light_GetLights();
		Register_UnityEngine_Light_GetLights();

		//UnityEngine.Rendering.CommandBuffer[] UnityEngine.Light::GetCommandBuffers(UnityEngine.Rendering.LightEvent)
		void Register_UnityEngine_Light_GetCommandBuffers();
		Register_UnityEngine_Light_GetCommandBuffers();

		//UnityEngine.Texture UnityEngine.Light::get_cookie()
		void Register_UnityEngine_Light_get_cookie();
		Register_UnityEngine_Light_get_cookie();

	//End Registrations for type : UnityEngine.Light

	//Start Registrations for type : UnityEngine.LightmapSettings

		//System.Void UnityEngine.LightmapSettings::set_lightProbes(UnityEngine.LightProbes)
		void Register_UnityEngine_LightmapSettings_set_lightProbes();
		Register_UnityEngine_LightmapSettings_set_lightProbes();

		//System.Void UnityEngine.LightmapSettings::set_lightmaps(UnityEngine.LightmapData[])
		void Register_UnityEngine_LightmapSettings_set_lightmaps();
		Register_UnityEngine_LightmapSettings_set_lightmaps();

		//System.Void UnityEngine.LightmapSettings::set_lightmapsMode(UnityEngine.LightmapsMode)
		void Register_UnityEngine_LightmapSettings_set_lightmapsMode();
		Register_UnityEngine_LightmapSettings_set_lightmapsMode();

		//UnityEngine.LightProbes UnityEngine.LightmapSettings::get_lightProbes()
		void Register_UnityEngine_LightmapSettings_get_lightProbes();
		Register_UnityEngine_LightmapSettings_get_lightProbes();

		//UnityEngine.LightmapData[] UnityEngine.LightmapSettings::get_lightmaps()
		void Register_UnityEngine_LightmapSettings_get_lightmaps();
		Register_UnityEngine_LightmapSettings_get_lightmaps();

		//UnityEngine.LightmapsMode UnityEngine.LightmapSettings::get_lightmapsMode()
		void Register_UnityEngine_LightmapSettings_get_lightmapsMode();
		Register_UnityEngine_LightmapSettings_get_lightmapsMode();

	//End Registrations for type : UnityEngine.LightmapSettings

	//Start Registrations for type : UnityEngine.LightProbeGroup

		//System.Void UnityEngine.LightProbeGroup::set_probePositions(UnityEngine.Vector3[])
		void Register_UnityEngine_LightProbeGroup_set_probePositions();
		Register_UnityEngine_LightProbeGroup_set_probePositions();

		//UnityEngine.Vector3[] UnityEngine.LightProbeGroup::get_probePositions()
		void Register_UnityEngine_LightProbeGroup_get_probePositions();
		Register_UnityEngine_LightProbeGroup_get_probePositions();

	//End Registrations for type : UnityEngine.LightProbeGroup

	//Start Registrations for type : UnityEngine.LightProbes

		//System.Int32 UnityEngine.LightProbes::get_cellCount()
		void Register_UnityEngine_LightProbes_get_cellCount();
		Register_UnityEngine_LightProbes_get_cellCount();

		//System.Int32 UnityEngine.LightProbes::get_count()
		void Register_UnityEngine_LightProbes_get_count();
		Register_UnityEngine_LightProbes_get_count();

		//System.Void UnityEngine.LightProbes::INTERNAL_CALL_GetInterpolatedProbe(UnityEngine.Vector3&,UnityEngine.Renderer,UnityEngine.Rendering.SphericalHarmonicsL2&)
		void Register_UnityEngine_LightProbes_INTERNAL_CALL_GetInterpolatedProbe();
		Register_UnityEngine_LightProbes_INTERNAL_CALL_GetInterpolatedProbe();

		//System.Void UnityEngine.LightProbes::set_bakedProbes(UnityEngine.Rendering.SphericalHarmonicsL2[])
		void Register_UnityEngine_LightProbes_set_bakedProbes();
		Register_UnityEngine_LightProbes_set_bakedProbes();

		//UnityEngine.Rendering.SphericalHarmonicsL2[] UnityEngine.LightProbes::get_bakedProbes()
		void Register_UnityEngine_LightProbes_get_bakedProbes();
		Register_UnityEngine_LightProbes_get_bakedProbes();

		//UnityEngine.Vector3[] UnityEngine.LightProbes::get_positions()
		void Register_UnityEngine_LightProbes_get_positions();
		Register_UnityEngine_LightProbes_get_positions();

	//End Registrations for type : UnityEngine.LightProbes

	//Start Registrations for type : UnityEngine.LineRenderer

		//System.Boolean UnityEngine.LineRenderer::get_useWorldSpace()
		void Register_UnityEngine_LineRenderer_get_useWorldSpace();
		Register_UnityEngine_LineRenderer_get_useWorldSpace();

		//System.Void UnityEngine.LineRenderer::INTERNAL_CALL_SetColors(UnityEngine.LineRenderer,UnityEngine.Color&,UnityEngine.Color&)
		void Register_UnityEngine_LineRenderer_INTERNAL_CALL_SetColors();
		Register_UnityEngine_LineRenderer_INTERNAL_CALL_SetColors();

		//System.Void UnityEngine.LineRenderer::INTERNAL_CALL_SetPosition(UnityEngine.LineRenderer,System.Int32,UnityEngine.Vector3&)
		void Register_UnityEngine_LineRenderer_INTERNAL_CALL_SetPosition();
		Register_UnityEngine_LineRenderer_INTERNAL_CALL_SetPosition();

		//System.Void UnityEngine.LineRenderer::INTERNAL_CALL_SetVertexCount(UnityEngine.LineRenderer,System.Int32)
		void Register_UnityEngine_LineRenderer_INTERNAL_CALL_SetVertexCount();
		Register_UnityEngine_LineRenderer_INTERNAL_CALL_SetVertexCount();

		//System.Void UnityEngine.LineRenderer::INTERNAL_CALL_SetWidth(UnityEngine.LineRenderer,System.Single,System.Single)
		void Register_UnityEngine_LineRenderer_INTERNAL_CALL_SetWidth();
		Register_UnityEngine_LineRenderer_INTERNAL_CALL_SetWidth();

		//System.Void UnityEngine.LineRenderer::SetPositions(UnityEngine.Vector3[])
		void Register_UnityEngine_LineRenderer_SetPositions();
		Register_UnityEngine_LineRenderer_SetPositions();

		//System.Void UnityEngine.LineRenderer::set_useWorldSpace(System.Boolean)
		void Register_UnityEngine_LineRenderer_set_useWorldSpace();
		Register_UnityEngine_LineRenderer_set_useWorldSpace();

	//End Registrations for type : UnityEngine.LineRenderer

	//Start Registrations for type : UnityEngine.LocationService

		//System.Boolean UnityEngine.LocationService::get_isEnabledByUser()
		void Register_UnityEngine_LocationService_get_isEnabledByUser();
		Register_UnityEngine_LocationService_get_isEnabledByUser();

		//System.Void UnityEngine.LocationService::Start(System.Single,System.Single)
		void Register_UnityEngine_LocationService_Start();
		Register_UnityEngine_LocationService_Start();

		//System.Void UnityEngine.LocationService::Stop()
		void Register_UnityEngine_LocationService_Stop();
		Register_UnityEngine_LocationService_Stop();

		//UnityEngine.LocationInfo UnityEngine.LocationService::get_lastData()
		void Register_UnityEngine_LocationService_get_lastData();
		Register_UnityEngine_LocationService_get_lastData();

		//UnityEngine.LocationServiceStatus UnityEngine.LocationService::get_status()
		void Register_UnityEngine_LocationService_get_status();
		Register_UnityEngine_LocationService_get_status();

	//End Registrations for type : UnityEngine.LocationService

	//Start Registrations for type : UnityEngine.LODGroup

		//System.Boolean UnityEngine.LODGroup::get_animateCrossFading()
		void Register_UnityEngine_LODGroup_get_animateCrossFading();
		Register_UnityEngine_LODGroup_get_animateCrossFading();

		//System.Boolean UnityEngine.LODGroup::get_enabled()
		void Register_UnityEngine_LODGroup_get_enabled();
		Register_UnityEngine_LODGroup_get_enabled();

		//System.Int32 UnityEngine.LODGroup::get_lodCount()
		void Register_UnityEngine_LODGroup_get_lodCount();
		Register_UnityEngine_LODGroup_get_lodCount();

		//System.Single UnityEngine.LODGroup::get_crossFadeAnimationDuration()
		void Register_UnityEngine_LODGroup_get_crossFadeAnimationDuration();
		Register_UnityEngine_LODGroup_get_crossFadeAnimationDuration();

		//System.Single UnityEngine.LODGroup::get_size()
		void Register_UnityEngine_LODGroup_get_size();
		Register_UnityEngine_LODGroup_get_size();

		//System.Void UnityEngine.LODGroup::ForceLOD(System.Int32)
		void Register_UnityEngine_LODGroup_ForceLOD();
		Register_UnityEngine_LODGroup_ForceLOD();

		//System.Void UnityEngine.LODGroup::INTERNAL_get_localReferencePoint(UnityEngine.Vector3&)
		void Register_UnityEngine_LODGroup_INTERNAL_get_localReferencePoint();
		Register_UnityEngine_LODGroup_INTERNAL_get_localReferencePoint();

		//System.Void UnityEngine.LODGroup::INTERNAL_set_localReferencePoint(UnityEngine.Vector3&)
		void Register_UnityEngine_LODGroup_INTERNAL_set_localReferencePoint();
		Register_UnityEngine_LODGroup_INTERNAL_set_localReferencePoint();

		//System.Void UnityEngine.LODGroup::RecalculateBounds()
		void Register_UnityEngine_LODGroup_RecalculateBounds();
		Register_UnityEngine_LODGroup_RecalculateBounds();

		//System.Void UnityEngine.LODGroup::SetLODs(UnityEngine.LOD[])
		void Register_UnityEngine_LODGroup_SetLODs();
		Register_UnityEngine_LODGroup_SetLODs();

		//System.Void UnityEngine.LODGroup::set_animateCrossFading(System.Boolean)
		void Register_UnityEngine_LODGroup_set_animateCrossFading();
		Register_UnityEngine_LODGroup_set_animateCrossFading();

		//System.Void UnityEngine.LODGroup::set_crossFadeAnimationDuration(System.Single)
		void Register_UnityEngine_LODGroup_set_crossFadeAnimationDuration();
		Register_UnityEngine_LODGroup_set_crossFadeAnimationDuration();

		//System.Void UnityEngine.LODGroup::set_enabled(System.Boolean)
		void Register_UnityEngine_LODGroup_set_enabled();
		Register_UnityEngine_LODGroup_set_enabled();

		//System.Void UnityEngine.LODGroup::set_fadeMode(UnityEngine.LODFadeMode)
		void Register_UnityEngine_LODGroup_set_fadeMode();
		Register_UnityEngine_LODGroup_set_fadeMode();

		//System.Void UnityEngine.LODGroup::set_size(System.Single)
		void Register_UnityEngine_LODGroup_set_size();
		Register_UnityEngine_LODGroup_set_size();

		//UnityEngine.LODFadeMode UnityEngine.LODGroup::get_fadeMode()
		void Register_UnityEngine_LODGroup_get_fadeMode();
		Register_UnityEngine_LODGroup_get_fadeMode();

		//UnityEngine.LOD[] UnityEngine.LODGroup::GetLODs()
		void Register_UnityEngine_LODGroup_GetLODs();
		Register_UnityEngine_LODGroup_GetLODs();

	//End Registrations for type : UnityEngine.LODGroup

	//Start Registrations for type : UnityEngine.Material

		//System.Boolean UnityEngine.Material::HasProperty(System.Int32)
		void Register_UnityEngine_Material_HasProperty();
		Register_UnityEngine_Material_HasProperty();

		//System.Boolean UnityEngine.Material::IsKeywordEnabled(System.String)
		void Register_UnityEngine_Material_IsKeywordEnabled();
		Register_UnityEngine_Material_IsKeywordEnabled();

		//System.Boolean UnityEngine.Material::SetPass(System.Int32)
		void Register_UnityEngine_Material_SetPass();
		Register_UnityEngine_Material_SetPass();

		//System.Int32 UnityEngine.Material::get_passCount()
		void Register_UnityEngine_Material_get_passCount();
		Register_UnityEngine_Material_get_passCount();

		//System.Int32 UnityEngine.Material::get_renderQueue()
		void Register_UnityEngine_Material_get_renderQueue();
		Register_UnityEngine_Material_get_renderQueue();

		//System.Single UnityEngine.Material::GetFloat(System.Int32)
		void Register_UnityEngine_Material_GetFloat();
		Register_UnityEngine_Material_GetFloat();

		//System.String UnityEngine.Material::GetTag(System.String,System.Boolean,System.String)
		void Register_UnityEngine_Material_GetTag();
		Register_UnityEngine_Material_GetTag();

		//System.String[] UnityEngine.Material::get_shaderKeywords()
		void Register_UnityEngine_Material_get_shaderKeywords();
		Register_UnityEngine_Material_get_shaderKeywords();

		//System.Void UnityEngine.Material::CopyPropertiesFromMaterial(UnityEngine.Material)
		void Register_UnityEngine_Material_CopyPropertiesFromMaterial();
		Register_UnityEngine_Material_CopyPropertiesFromMaterial();

		//System.Void UnityEngine.Material::DisableKeyword(System.String)
		void Register_UnityEngine_Material_DisableKeyword();
		Register_UnityEngine_Material_DisableKeyword();

		//System.Void UnityEngine.Material::EnableKeyword(System.String)
		void Register_UnityEngine_Material_EnableKeyword();
		Register_UnityEngine_Material_EnableKeyword();

		//System.Void UnityEngine.Material::INTERNAL_CALL_GetColor(UnityEngine.Material,System.Int32,UnityEngine.Color&)
		void Register_UnityEngine_Material_INTERNAL_CALL_GetColor();
		Register_UnityEngine_Material_INTERNAL_CALL_GetColor();

		//System.Void UnityEngine.Material::INTERNAL_CALL_GetMatrix(UnityEngine.Material,System.Int32,UnityEngine.Matrix4x4&)
		void Register_UnityEngine_Material_INTERNAL_CALL_GetMatrix();
		Register_UnityEngine_Material_INTERNAL_CALL_GetMatrix();

		//System.Void UnityEngine.Material::INTERNAL_CALL_SetColor(UnityEngine.Material,System.Int32,UnityEngine.Color&)
		void Register_UnityEngine_Material_INTERNAL_CALL_SetColor();
		Register_UnityEngine_Material_INTERNAL_CALL_SetColor();

		//System.Void UnityEngine.Material::INTERNAL_CALL_SetMatrix(UnityEngine.Material,System.Int32,UnityEngine.Matrix4x4&)
		void Register_UnityEngine_Material_INTERNAL_CALL_SetMatrix();
		Register_UnityEngine_Material_INTERNAL_CALL_SetMatrix();

		//System.Void UnityEngine.Material::INTERNAL_CALL_SetTextureOffset(UnityEngine.Material,System.String,UnityEngine.Vector2&)
		void Register_UnityEngine_Material_INTERNAL_CALL_SetTextureOffset();
		Register_UnityEngine_Material_INTERNAL_CALL_SetTextureOffset();

		//System.Void UnityEngine.Material::INTERNAL_CALL_SetTextureScale(UnityEngine.Material,System.String,UnityEngine.Vector2&)
		void Register_UnityEngine_Material_INTERNAL_CALL_SetTextureScale();
		Register_UnityEngine_Material_INTERNAL_CALL_SetTextureScale();

		//System.Void UnityEngine.Material::Internal_CreateWithMaterial(UnityEngine.Material,UnityEngine.Material)
		void Register_UnityEngine_Material_Internal_CreateWithMaterial();
		Register_UnityEngine_Material_Internal_CreateWithMaterial();

		//System.Void UnityEngine.Material::Internal_CreateWithShader(UnityEngine.Material,UnityEngine.Shader)
		void Register_UnityEngine_Material_Internal_CreateWithShader();
		Register_UnityEngine_Material_Internal_CreateWithShader();

		//System.Void UnityEngine.Material::Internal_GetTextureScaleAndOffset(UnityEngine.Material,System.String,UnityEngine.Vector4&)
		void Register_UnityEngine_Material_Internal_GetTextureScaleAndOffset();
		Register_UnityEngine_Material_Internal_GetTextureScaleAndOffset();

		//System.Void UnityEngine.Material::Lerp(UnityEngine.Material,UnityEngine.Material,System.Single)
		void Register_UnityEngine_Material_Lerp();
		Register_UnityEngine_Material_Lerp();

		//System.Void UnityEngine.Material::SetBuffer(System.String,UnityEngine.ComputeBuffer)
		void Register_UnityEngine_Material_SetBuffer();
		Register_UnityEngine_Material_SetBuffer();

		//System.Void UnityEngine.Material::SetFloat(System.Int32,System.Single)
		void Register_UnityEngine_Material_SetFloat();
		Register_UnityEngine_Material_SetFloat();

		//System.Void UnityEngine.Material::SetOverrideTag(System.String,System.String)
		void Register_UnityEngine_Material_SetOverrideTag();
		Register_UnityEngine_Material_SetOverrideTag();

		//System.Void UnityEngine.Material::SetTexture(System.Int32,UnityEngine.Texture)
		void Register_UnityEngine_Material_SetTexture();
		Register_UnityEngine_Material_SetTexture();

		//System.Void UnityEngine.Material::set_globalIlluminationFlags(UnityEngine.MaterialGlobalIlluminationFlags)
		void Register_UnityEngine_Material_set_globalIlluminationFlags();
		Register_UnityEngine_Material_set_globalIlluminationFlags();

		//System.Void UnityEngine.Material::set_renderQueue(System.Int32)
		void Register_UnityEngine_Material_set_renderQueue();
		Register_UnityEngine_Material_set_renderQueue();

		//System.Void UnityEngine.Material::set_shader(UnityEngine.Shader)
		void Register_UnityEngine_Material_set_shader();
		Register_UnityEngine_Material_set_shader();

		//System.Void UnityEngine.Material::set_shaderKeywords(System.String[])
		void Register_UnityEngine_Material_set_shaderKeywords();
		Register_UnityEngine_Material_set_shaderKeywords();

		//UnityEngine.MaterialGlobalIlluminationFlags UnityEngine.Material::get_globalIlluminationFlags()
		void Register_UnityEngine_Material_get_globalIlluminationFlags();
		Register_UnityEngine_Material_get_globalIlluminationFlags();

		//UnityEngine.Shader UnityEngine.Material::get_shader()
		void Register_UnityEngine_Material_get_shader();
		Register_UnityEngine_Material_get_shader();

		//UnityEngine.Texture UnityEngine.Material::GetTexture(System.Int32)
		void Register_UnityEngine_Material_GetTexture();
		Register_UnityEngine_Material_GetTexture();

	//End Registrations for type : UnityEngine.Material

	//Start Registrations for type : UnityEngine.MaterialPropertyBlock

		//System.Boolean UnityEngine.MaterialPropertyBlock::get_isEmpty()
		void Register_UnityEngine_MaterialPropertyBlock_get_isEmpty();
		Register_UnityEngine_MaterialPropertyBlock_get_isEmpty();

		//System.Single UnityEngine.MaterialPropertyBlock::GetFloat(System.Int32)
		void Register_UnityEngine_MaterialPropertyBlock_GetFloat();
		Register_UnityEngine_MaterialPropertyBlock_GetFloat();

		//System.Void UnityEngine.MaterialPropertyBlock::Clear()
		void Register_UnityEngine_MaterialPropertyBlock_Clear();
		Register_UnityEngine_MaterialPropertyBlock_Clear();

		//System.Void UnityEngine.MaterialPropertyBlock::DestroyBlock()
		void Register_UnityEngine_MaterialPropertyBlock_DestroyBlock();
		Register_UnityEngine_MaterialPropertyBlock_DestroyBlock();

		//System.Void UnityEngine.MaterialPropertyBlock::INTERNAL_CALL_GetMatrix(UnityEngine.MaterialPropertyBlock,System.Int32,UnityEngine.Matrix4x4&)
		void Register_UnityEngine_MaterialPropertyBlock_INTERNAL_CALL_GetMatrix();
		Register_UnityEngine_MaterialPropertyBlock_INTERNAL_CALL_GetMatrix();

		//System.Void UnityEngine.MaterialPropertyBlock::INTERNAL_CALL_GetVector(UnityEngine.MaterialPropertyBlock,System.Int32,UnityEngine.Vector4&)
		void Register_UnityEngine_MaterialPropertyBlock_INTERNAL_CALL_GetVector();
		Register_UnityEngine_MaterialPropertyBlock_INTERNAL_CALL_GetVector();

		//System.Void UnityEngine.MaterialPropertyBlock::INTERNAL_CALL_SetColor(UnityEngine.MaterialPropertyBlock,System.Int32,UnityEngine.Color&)
		void Register_UnityEngine_MaterialPropertyBlock_INTERNAL_CALL_SetColor();
		Register_UnityEngine_MaterialPropertyBlock_INTERNAL_CALL_SetColor();

		//System.Void UnityEngine.MaterialPropertyBlock::INTERNAL_CALL_SetMatrix(UnityEngine.MaterialPropertyBlock,System.Int32,UnityEngine.Matrix4x4&)
		void Register_UnityEngine_MaterialPropertyBlock_INTERNAL_CALL_SetMatrix();
		Register_UnityEngine_MaterialPropertyBlock_INTERNAL_CALL_SetMatrix();

		//System.Void UnityEngine.MaterialPropertyBlock::INTERNAL_CALL_SetVector(UnityEngine.MaterialPropertyBlock,System.Int32,UnityEngine.Vector4&)
		void Register_UnityEngine_MaterialPropertyBlock_INTERNAL_CALL_SetVector();
		Register_UnityEngine_MaterialPropertyBlock_INTERNAL_CALL_SetVector();

		//System.Void UnityEngine.MaterialPropertyBlock::InitBlock()
		void Register_UnityEngine_MaterialPropertyBlock_InitBlock();
		Register_UnityEngine_MaterialPropertyBlock_InitBlock();

		//System.Void UnityEngine.MaterialPropertyBlock::SetFloat(System.Int32,System.Single)
		void Register_UnityEngine_MaterialPropertyBlock_SetFloat();
		Register_UnityEngine_MaterialPropertyBlock_SetFloat();

		//System.Void UnityEngine.MaterialPropertyBlock::SetTexture(System.Int32,UnityEngine.Texture)
		void Register_UnityEngine_MaterialPropertyBlock_SetTexture();
		Register_UnityEngine_MaterialPropertyBlock_SetTexture();

		//UnityEngine.Texture UnityEngine.MaterialPropertyBlock::GetTexture(System.Int32)
		void Register_UnityEngine_MaterialPropertyBlock_GetTexture();
		Register_UnityEngine_MaterialPropertyBlock_GetTexture();

	//End Registrations for type : UnityEngine.MaterialPropertyBlock

	//Start Registrations for type : UnityEngine.Mathf

		//System.Boolean UnityEngine.Mathf::IsPowerOfTwo(System.Int32)
		void Register_UnityEngine_Mathf_IsPowerOfTwo();
		Register_UnityEngine_Mathf_IsPowerOfTwo();

		//System.Int32 UnityEngine.Mathf::ClosestPowerOfTwo(System.Int32)
		void Register_UnityEngine_Mathf_ClosestPowerOfTwo();
		Register_UnityEngine_Mathf_ClosestPowerOfTwo();

		//System.Int32 UnityEngine.Mathf::NextPowerOfTwo(System.Int32)
		void Register_UnityEngine_Mathf_NextPowerOfTwo();
		Register_UnityEngine_Mathf_NextPowerOfTwo();

		//System.Single UnityEngine.Mathf::GammaToLinearSpace(System.Single)
		void Register_UnityEngine_Mathf_GammaToLinearSpace();
		Register_UnityEngine_Mathf_GammaToLinearSpace();

		//System.Single UnityEngine.Mathf::HalfToFloat(System.UInt16)
		void Register_UnityEngine_Mathf_HalfToFloat();
		Register_UnityEngine_Mathf_HalfToFloat();

		//System.Single UnityEngine.Mathf::LinearToGammaSpace(System.Single)
		void Register_UnityEngine_Mathf_LinearToGammaSpace();
		Register_UnityEngine_Mathf_LinearToGammaSpace();

		//System.Single UnityEngine.Mathf::PerlinNoise(System.Single,System.Single)
		void Register_UnityEngine_Mathf_PerlinNoise();
		Register_UnityEngine_Mathf_PerlinNoise();

		//System.UInt16 UnityEngine.Mathf::FloatToHalf(System.Single)
		void Register_UnityEngine_Mathf_FloatToHalf();
		Register_UnityEngine_Mathf_FloatToHalf();

	//End Registrations for type : UnityEngine.Mathf

	//Start Registrations for type : UnityEngine.Matrix4x4

		//System.Boolean UnityEngine.Matrix4x4::get_isIdentity()
		void Register_UnityEngine_Matrix4x4_get_isIdentity();
		Register_UnityEngine_Matrix4x4_get_isIdentity();

		//System.Single UnityEngine.Matrix4x4::INTERNAL_CALL_Determinant(UnityEngine.Matrix4x4&)
		void Register_UnityEngine_Matrix4x4_INTERNAL_CALL_Determinant();
		Register_UnityEngine_Matrix4x4_INTERNAL_CALL_Determinant();

		//System.Void UnityEngine.Matrix4x4::INTERNAL_CALL_Inverse(UnityEngine.Matrix4x4&,UnityEngine.Matrix4x4&)
		void Register_UnityEngine_Matrix4x4_INTERNAL_CALL_Inverse();
		Register_UnityEngine_Matrix4x4_INTERNAL_CALL_Inverse();

		//System.Void UnityEngine.Matrix4x4::INTERNAL_CALL_Ortho(System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,UnityEngine.Matrix4x4&)
		void Register_UnityEngine_Matrix4x4_INTERNAL_CALL_Ortho();
		Register_UnityEngine_Matrix4x4_INTERNAL_CALL_Ortho();

		//System.Void UnityEngine.Matrix4x4::INTERNAL_CALL_Perspective(System.Single,System.Single,System.Single,System.Single,UnityEngine.Matrix4x4&)
		void Register_UnityEngine_Matrix4x4_INTERNAL_CALL_Perspective();
		Register_UnityEngine_Matrix4x4_INTERNAL_CALL_Perspective();

		//System.Void UnityEngine.Matrix4x4::INTERNAL_CALL_TRS(UnityEngine.Vector3&,UnityEngine.Quaternion&,UnityEngine.Vector3&,UnityEngine.Matrix4x4&)
		void Register_UnityEngine_Matrix4x4_INTERNAL_CALL_TRS();
		Register_UnityEngine_Matrix4x4_INTERNAL_CALL_TRS();

		//System.Void UnityEngine.Matrix4x4::INTERNAL_CALL_Transpose(UnityEngine.Matrix4x4&,UnityEngine.Matrix4x4&)
		void Register_UnityEngine_Matrix4x4_INTERNAL_CALL_Transpose();
		Register_UnityEngine_Matrix4x4_INTERNAL_CALL_Transpose();

	//End Registrations for type : UnityEngine.Matrix4x4

	//Start Registrations for type : UnityEngine.Mesh

		//System.Array UnityEngine.Mesh::ExtractListData(System.Object)
		void Register_UnityEngine_Mesh_ExtractListData();
		Register_UnityEngine_Mesh_ExtractListData();

		//System.Boolean UnityEngine.Mesh::CheckCanAccessUVChannel(System.Int32)
		void Register_UnityEngine_Mesh_CheckCanAccessUVChannel();
		Register_UnityEngine_Mesh_CheckCanAccessUVChannel();

		//System.Boolean UnityEngine.Mesh::get_canAccess()
		void Register_UnityEngine_Mesh_get_canAccess();
		Register_UnityEngine_Mesh_get_canAccess();

		//System.Boolean UnityEngine.Mesh::get_isReadable()
		void Register_UnityEngine_Mesh_get_isReadable();
		Register_UnityEngine_Mesh_get_isReadable();

		//System.Int32 UnityEngine.Mesh::GetBlendShapeFrameCount(System.Int32)
		void Register_UnityEngine_Mesh_GetBlendShapeFrameCount();
		Register_UnityEngine_Mesh_GetBlendShapeFrameCount();

		//System.Int32 UnityEngine.Mesh::GetBlendShapeIndex(System.String)
		void Register_UnityEngine_Mesh_GetBlendShapeIndex();
		Register_UnityEngine_Mesh_GetBlendShapeIndex();

		//System.Int32 UnityEngine.Mesh::get_blendShapeCount()
		void Register_UnityEngine_Mesh_get_blendShapeCount();
		Register_UnityEngine_Mesh_get_blendShapeCount();

		//System.Int32 UnityEngine.Mesh::get_subMeshCount()
		void Register_UnityEngine_Mesh_get_subMeshCount();
		Register_UnityEngine_Mesh_get_subMeshCount();

		//System.Int32 UnityEngine.Mesh::get_vertexCount()
		void Register_UnityEngine_Mesh_get_vertexCount();
		Register_UnityEngine_Mesh_get_vertexCount();

		//System.Int32[] UnityEngine.Mesh::GetIndices(System.Int32)
		void Register_UnityEngine_Mesh_GetIndices();
		Register_UnityEngine_Mesh_GetIndices();

		//System.Int32[] UnityEngine.Mesh::GetTriangles(System.Int32)
		void Register_UnityEngine_Mesh_GetTriangles();
		Register_UnityEngine_Mesh_GetTriangles();

		//System.Int32[] UnityEngine.Mesh::get_triangles()
		void Register_UnityEngine_Mesh_get_triangles();
		Register_UnityEngine_Mesh_get_triangles();

		//System.Single UnityEngine.Mesh::GetBlendShapeFrameWeight(System.Int32,System.Int32)
		void Register_UnityEngine_Mesh_GetBlendShapeFrameWeight();
		Register_UnityEngine_Mesh_GetBlendShapeFrameWeight();

		//System.String UnityEngine.Mesh::GetBlendShapeName(System.Int32)
		void Register_UnityEngine_Mesh_GetBlendShapeName();
		Register_UnityEngine_Mesh_GetBlendShapeName();

		//System.Void UnityEngine.Mesh::AddBlendShapeFrame(System.String,System.Single,UnityEngine.Vector3[],UnityEngine.Vector3[],UnityEngine.Vector3[])
		void Register_UnityEngine_Mesh_AddBlendShapeFrame();
		Register_UnityEngine_Mesh_AddBlendShapeFrame();

		//System.Void UnityEngine.Mesh::Clear(System.Boolean)
		void Register_UnityEngine_Mesh_Clear();
		Register_UnityEngine_Mesh_Clear();

		//System.Void UnityEngine.Mesh::ClearBlendShapes()
		void Register_UnityEngine_Mesh_ClearBlendShapes();
		Register_UnityEngine_Mesh_ClearBlendShapes();

		//System.Void UnityEngine.Mesh::CombineMeshes(UnityEngine.CombineInstance[],System.Boolean,System.Boolean)
		void Register_UnityEngine_Mesh_CombineMeshes();
		Register_UnityEngine_Mesh_CombineMeshes();

		//System.Void UnityEngine.Mesh::GetBlendShapeFrameVertices(System.Int32,System.Int32,UnityEngine.Vector3[],UnityEngine.Vector3[],UnityEngine.Vector3[])
		void Register_UnityEngine_Mesh_GetBlendShapeFrameVertices();
		Register_UnityEngine_Mesh_GetBlendShapeFrameVertices();

		//System.Void UnityEngine.Mesh::GetUVsInternal(System.Array,System.Int32,System.Int32)
		void Register_UnityEngine_Mesh_GetUVsInternal();
		Register_UnityEngine_Mesh_GetUVsInternal();

		//System.Void UnityEngine.Mesh::INTERNAL_get_bounds(UnityEngine.Bounds&)
		void Register_UnityEngine_Mesh_INTERNAL_get_bounds();
		Register_UnityEngine_Mesh_INTERNAL_get_bounds();

		//System.Void UnityEngine.Mesh::INTERNAL_set_bounds(UnityEngine.Bounds&)
		void Register_UnityEngine_Mesh_INTERNAL_set_bounds();
		Register_UnityEngine_Mesh_INTERNAL_set_bounds();

		//System.Void UnityEngine.Mesh::Internal_Create(UnityEngine.Mesh)
		void Register_UnityEngine_Mesh_Internal_Create();
		Register_UnityEngine_Mesh_Internal_Create();

		//System.Void UnityEngine.Mesh::MarkDynamic()
		void Register_UnityEngine_Mesh_MarkDynamic();
		Register_UnityEngine_Mesh_MarkDynamic();

		//System.Void UnityEngine.Mesh::Optimize()
		void Register_UnityEngine_Mesh_Optimize();
		Register_UnityEngine_Mesh_Optimize();

		//System.Void UnityEngine.Mesh::RecalculateBounds()
		void Register_UnityEngine_Mesh_RecalculateBounds();
		Register_UnityEngine_Mesh_RecalculateBounds();

		//System.Void UnityEngine.Mesh::RecalculateNormals()
		void Register_UnityEngine_Mesh_RecalculateNormals();
		Register_UnityEngine_Mesh_RecalculateNormals();

		//System.Void UnityEngine.Mesh::ResizeList(System.Object,System.Int32)
		void Register_UnityEngine_Mesh_ResizeList();
		Register_UnityEngine_Mesh_ResizeList();

		//System.Void UnityEngine.Mesh::SetColors32Internal(System.Object)
		void Register_UnityEngine_Mesh_SetColors32Internal();
		Register_UnityEngine_Mesh_SetColors32Internal();

		//System.Void UnityEngine.Mesh::SetColorsInternal(System.Object)
		void Register_UnityEngine_Mesh_SetColorsInternal();
		Register_UnityEngine_Mesh_SetColorsInternal();

		//System.Void UnityEngine.Mesh::SetIndices(System.Int32[],UnityEngine.MeshTopology,System.Int32)
		void Register_UnityEngine_Mesh_SetIndices();
		Register_UnityEngine_Mesh_SetIndices();

		//System.Void UnityEngine.Mesh::SetNormalsInternal(System.Object)
		void Register_UnityEngine_Mesh_SetNormalsInternal();
		Register_UnityEngine_Mesh_SetNormalsInternal();

		//System.Void UnityEngine.Mesh::SetTangentsInternal(System.Object)
		void Register_UnityEngine_Mesh_SetTangentsInternal();
		Register_UnityEngine_Mesh_SetTangentsInternal();

		//System.Void UnityEngine.Mesh::SetTriangles(System.Int32[],System.Int32)
		void Register_UnityEngine_Mesh_SetTriangles();
		Register_UnityEngine_Mesh_SetTriangles();

		//System.Void UnityEngine.Mesh::SetTrianglesInternal(System.Object,System.Int32)
		void Register_UnityEngine_Mesh_SetTrianglesInternal();
		Register_UnityEngine_Mesh_SetTrianglesInternal();

		//System.Void UnityEngine.Mesh::SetUVsInternal(System.Array,System.Int32,System.Int32,System.Int32)
		void Register_UnityEngine_Mesh_SetUVsInternal();
		Register_UnityEngine_Mesh_SetUVsInternal();

		//System.Void UnityEngine.Mesh::SetVerticesInternal(System.Object)
		void Register_UnityEngine_Mesh_SetVerticesInternal();
		Register_UnityEngine_Mesh_SetVerticesInternal();

		//System.Void UnityEngine.Mesh::UploadMeshData(System.Boolean)
		void Register_UnityEngine_Mesh_UploadMeshData();
		Register_UnityEngine_Mesh_UploadMeshData();

		//System.Void UnityEngine.Mesh::set_bindposes(UnityEngine.Matrix4x4[])
		void Register_UnityEngine_Mesh_set_bindposes();
		Register_UnityEngine_Mesh_set_bindposes();

		//System.Void UnityEngine.Mesh::set_boneWeights(UnityEngine.BoneWeight[])
		void Register_UnityEngine_Mesh_set_boneWeights();
		Register_UnityEngine_Mesh_set_boneWeights();

		//System.Void UnityEngine.Mesh::set_colors(UnityEngine.Color[])
		void Register_UnityEngine_Mesh_set_colors();
		Register_UnityEngine_Mesh_set_colors();

		//System.Void UnityEngine.Mesh::set_colors32(UnityEngine.Color32[])
		void Register_UnityEngine_Mesh_set_colors32();
		Register_UnityEngine_Mesh_set_colors32();

		//System.Void UnityEngine.Mesh::set_normals(UnityEngine.Vector3[])
		void Register_UnityEngine_Mesh_set_normals();
		Register_UnityEngine_Mesh_set_normals();

		//System.Void UnityEngine.Mesh::set_subMeshCount(System.Int32)
		void Register_UnityEngine_Mesh_set_subMeshCount();
		Register_UnityEngine_Mesh_set_subMeshCount();

		//System.Void UnityEngine.Mesh::set_tangents(UnityEngine.Vector4[])
		void Register_UnityEngine_Mesh_set_tangents();
		Register_UnityEngine_Mesh_set_tangents();

		//System.Void UnityEngine.Mesh::set_triangles(System.Int32[])
		void Register_UnityEngine_Mesh_set_triangles();
		Register_UnityEngine_Mesh_set_triangles();

		//System.Void UnityEngine.Mesh::set_uv(UnityEngine.Vector2[])
		void Register_UnityEngine_Mesh_set_uv();
		Register_UnityEngine_Mesh_set_uv();

		//System.Void UnityEngine.Mesh::set_uv2(UnityEngine.Vector2[])
		void Register_UnityEngine_Mesh_set_uv2();
		Register_UnityEngine_Mesh_set_uv2();

		//System.Void UnityEngine.Mesh::set_uv3(UnityEngine.Vector2[])
		void Register_UnityEngine_Mesh_set_uv3();
		Register_UnityEngine_Mesh_set_uv3();

		//System.Void UnityEngine.Mesh::set_uv4(UnityEngine.Vector2[])
		void Register_UnityEngine_Mesh_set_uv4();
		Register_UnityEngine_Mesh_set_uv4();

		//System.Void UnityEngine.Mesh::set_vertices(UnityEngine.Vector3[])
		void Register_UnityEngine_Mesh_set_vertices();
		Register_UnityEngine_Mesh_set_vertices();

		//UnityEngine.BoneWeight[] UnityEngine.Mesh::get_boneWeights()
		void Register_UnityEngine_Mesh_get_boneWeights();
		Register_UnityEngine_Mesh_get_boneWeights();

		//UnityEngine.Color32[] UnityEngine.Mesh::get_colors32()
		void Register_UnityEngine_Mesh_get_colors32();
		Register_UnityEngine_Mesh_get_colors32();

		//UnityEngine.Color[] UnityEngine.Mesh::get_colors()
		void Register_UnityEngine_Mesh_get_colors();
		Register_UnityEngine_Mesh_get_colors();

		//UnityEngine.Matrix4x4[] UnityEngine.Mesh::get_bindposes()
		void Register_UnityEngine_Mesh_get_bindposes();
		Register_UnityEngine_Mesh_get_bindposes();

		//UnityEngine.MeshTopology UnityEngine.Mesh::GetTopology(System.Int32)
		void Register_UnityEngine_Mesh_GetTopology();
		Register_UnityEngine_Mesh_GetTopology();

		//UnityEngine.Vector2[] UnityEngine.Mesh::get_uv()
		void Register_UnityEngine_Mesh_get_uv();
		Register_UnityEngine_Mesh_get_uv();

		//UnityEngine.Vector2[] UnityEngine.Mesh::get_uv2()
		void Register_UnityEngine_Mesh_get_uv2();
		Register_UnityEngine_Mesh_get_uv2();

		//UnityEngine.Vector2[] UnityEngine.Mesh::get_uv3()
		void Register_UnityEngine_Mesh_get_uv3();
		Register_UnityEngine_Mesh_get_uv3();

		//UnityEngine.Vector2[] UnityEngine.Mesh::get_uv4()
		void Register_UnityEngine_Mesh_get_uv4();
		Register_UnityEngine_Mesh_get_uv4();

		//UnityEngine.Vector3[] UnityEngine.Mesh::get_normals()
		void Register_UnityEngine_Mesh_get_normals();
		Register_UnityEngine_Mesh_get_normals();

		//UnityEngine.Vector3[] UnityEngine.Mesh::get_vertices()
		void Register_UnityEngine_Mesh_get_vertices();
		Register_UnityEngine_Mesh_get_vertices();

		//UnityEngine.Vector4[] UnityEngine.Mesh::get_tangents()
		void Register_UnityEngine_Mesh_get_tangents();
		Register_UnityEngine_Mesh_get_tangents();

	//End Registrations for type : UnityEngine.Mesh

	//Start Registrations for type : UnityEngine.MeshCollider

		//System.Boolean UnityEngine.MeshCollider::get_convex()
		void Register_UnityEngine_MeshCollider_get_convex();
		Register_UnityEngine_MeshCollider_get_convex();

		//System.Void UnityEngine.MeshCollider::set_convex(System.Boolean)
		void Register_UnityEngine_MeshCollider_set_convex();
		Register_UnityEngine_MeshCollider_set_convex();

		//System.Void UnityEngine.MeshCollider::set_sharedMesh(UnityEngine.Mesh)
		void Register_UnityEngine_MeshCollider_set_sharedMesh();
		Register_UnityEngine_MeshCollider_set_sharedMesh();

		//UnityEngine.Mesh UnityEngine.MeshCollider::get_sharedMesh()
		void Register_UnityEngine_MeshCollider_get_sharedMesh();
		Register_UnityEngine_MeshCollider_get_sharedMesh();

	//End Registrations for type : UnityEngine.MeshCollider

	//Start Registrations for type : UnityEngine.MeshFilter

		//System.Void UnityEngine.MeshFilter::set_mesh(UnityEngine.Mesh)
		void Register_UnityEngine_MeshFilter_set_mesh();
		Register_UnityEngine_MeshFilter_set_mesh();

		//System.Void UnityEngine.MeshFilter::set_sharedMesh(UnityEngine.Mesh)
		void Register_UnityEngine_MeshFilter_set_sharedMesh();
		Register_UnityEngine_MeshFilter_set_sharedMesh();

		//UnityEngine.Mesh UnityEngine.MeshFilter::get_mesh()
		void Register_UnityEngine_MeshFilter_get_mesh();
		Register_UnityEngine_MeshFilter_get_mesh();

		//UnityEngine.Mesh UnityEngine.MeshFilter::get_sharedMesh()
		void Register_UnityEngine_MeshFilter_get_sharedMesh();
		Register_UnityEngine_MeshFilter_get_sharedMesh();

	//End Registrations for type : UnityEngine.MeshFilter

	//Start Registrations for type : UnityEngine.MeshRenderer

		//System.Void UnityEngine.MeshRenderer::set_additionalVertexStreams(UnityEngine.Mesh)
		void Register_UnityEngine_MeshRenderer_set_additionalVertexStreams();
		Register_UnityEngine_MeshRenderer_set_additionalVertexStreams();

		//UnityEngine.Mesh UnityEngine.MeshRenderer::get_additionalVertexStreams()
		void Register_UnityEngine_MeshRenderer_get_additionalVertexStreams();
		Register_UnityEngine_MeshRenderer_get_additionalVertexStreams();

	//End Registrations for type : UnityEngine.MeshRenderer

	//Start Registrations for type : UnityEngine.Microphone

		//System.Boolean UnityEngine.Microphone::IsRecording(System.String)
		void Register_UnityEngine_Microphone_IsRecording();
		Register_UnityEngine_Microphone_IsRecording();

		//System.Int32 UnityEngine.Microphone::GetPosition(System.String)
		void Register_UnityEngine_Microphone_GetPosition();
		Register_UnityEngine_Microphone_GetPosition();

		//System.String[] UnityEngine.Microphone::get_devices()
		void Register_UnityEngine_Microphone_get_devices();
		Register_UnityEngine_Microphone_get_devices();

		//System.Void UnityEngine.Microphone::End(System.String)
		void Register_UnityEngine_Microphone_End();
		Register_UnityEngine_Microphone_End();

		//System.Void UnityEngine.Microphone::GetDeviceCaps(System.String,System.Int32&,System.Int32&)
		void Register_UnityEngine_Microphone_GetDeviceCaps();
		Register_UnityEngine_Microphone_GetDeviceCaps();

		//UnityEngine.AudioClip UnityEngine.Microphone::Start(System.String,System.Boolean,System.Int32,System.Int32)
		void Register_UnityEngine_Microphone_Start();
		Register_UnityEngine_Microphone_Start();

	//End Registrations for type : UnityEngine.Microphone

	//Start Registrations for type : UnityEngine.MonoBehaviour

		//System.Boolean UnityEngine.MonoBehaviour::Internal_IsInvokingAll()
		void Register_UnityEngine_MonoBehaviour_Internal_IsInvokingAll();
		Register_UnityEngine_MonoBehaviour_Internal_IsInvokingAll();

		//System.Boolean UnityEngine.MonoBehaviour::IsInvoking(System.String)
		void Register_UnityEngine_MonoBehaviour_IsInvoking();
		Register_UnityEngine_MonoBehaviour_IsInvoking();

		//System.Boolean UnityEngine.MonoBehaviour::get_useGUILayout()
		void Register_UnityEngine_MonoBehaviour_get_useGUILayout();
		Register_UnityEngine_MonoBehaviour_get_useGUILayout();

		//System.Void UnityEngine.MonoBehaviour::CancelInvoke(System.String)
		void Register_UnityEngine_MonoBehaviour_CancelInvoke();
		Register_UnityEngine_MonoBehaviour_CancelInvoke();

		//System.Void UnityEngine.MonoBehaviour::Internal_CancelInvokeAll()
		void Register_UnityEngine_MonoBehaviour_Internal_CancelInvokeAll();
		Register_UnityEngine_MonoBehaviour_Internal_CancelInvokeAll();

		//System.Void UnityEngine.MonoBehaviour::Invoke(System.String,System.Single)
		void Register_UnityEngine_MonoBehaviour_Invoke();
		Register_UnityEngine_MonoBehaviour_Invoke();

		//System.Void UnityEngine.MonoBehaviour::InvokeRepeating(System.String,System.Single,System.Single)
		void Register_UnityEngine_MonoBehaviour_InvokeRepeating();
		Register_UnityEngine_MonoBehaviour_InvokeRepeating();

		//System.Void UnityEngine.MonoBehaviour::StopAllCoroutines()
		void Register_UnityEngine_MonoBehaviour_StopAllCoroutines();
		Register_UnityEngine_MonoBehaviour_StopAllCoroutines();

		//System.Void UnityEngine.MonoBehaviour::StopCoroutine(System.String)
		void Register_UnityEngine_MonoBehaviour_StopCoroutine();
		Register_UnityEngine_MonoBehaviour_StopCoroutine();

		//System.Void UnityEngine.MonoBehaviour::StopCoroutineViaEnumerator_Auto(System.Collections.IEnumerator)
		void Register_UnityEngine_MonoBehaviour_StopCoroutineViaEnumerator_Auto();
		Register_UnityEngine_MonoBehaviour_StopCoroutineViaEnumerator_Auto();

		//System.Void UnityEngine.MonoBehaviour::StopCoroutine_Auto(UnityEngine.Coroutine)
		void Register_UnityEngine_MonoBehaviour_StopCoroutine_Auto();
		Register_UnityEngine_MonoBehaviour_StopCoroutine_Auto();

		//System.Void UnityEngine.MonoBehaviour::set_useGUILayout(System.Boolean)
		void Register_UnityEngine_MonoBehaviour_set_useGUILayout();
		Register_UnityEngine_MonoBehaviour_set_useGUILayout();

		//UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.String,System.Object)
		void Register_UnityEngine_MonoBehaviour_StartCoroutine();
		Register_UnityEngine_MonoBehaviour_StartCoroutine();

		//UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine_Auto(System.Collections.IEnumerator)
		void Register_UnityEngine_MonoBehaviour_StartCoroutine_Auto();
		Register_UnityEngine_MonoBehaviour_StartCoroutine_Auto();

	//End Registrations for type : UnityEngine.MonoBehaviour

	//Start Registrations for type : UnityEngine.NavMesh

		//System.Boolean UnityEngine.NavMesh::INTERNAL_CALL_CalculatePathInternal(UnityEngine.Vector3&,UnityEngine.Vector3&,System.Int32,UnityEngine.NavMeshPath)
		void Register_UnityEngine_NavMesh_INTERNAL_CALL_CalculatePathInternal();
		Register_UnityEngine_NavMesh_INTERNAL_CALL_CalculatePathInternal();

		//System.Boolean UnityEngine.NavMesh::INTERNAL_CALL_FindClosestEdge(UnityEngine.Vector3&,UnityEngine.NavMeshHit&,System.Int32)
		void Register_UnityEngine_NavMesh_INTERNAL_CALL_FindClosestEdge();
		Register_UnityEngine_NavMesh_INTERNAL_CALL_FindClosestEdge();

		//System.Boolean UnityEngine.NavMesh::INTERNAL_CALL_Raycast(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.NavMeshHit&,System.Int32)
		void Register_UnityEngine_NavMesh_INTERNAL_CALL_Raycast();
		Register_UnityEngine_NavMesh_INTERNAL_CALL_Raycast();

		//System.Boolean UnityEngine.NavMesh::INTERNAL_CALL_SamplePosition(UnityEngine.Vector3&,UnityEngine.NavMeshHit&,System.Single,System.Int32)
		void Register_UnityEngine_NavMesh_INTERNAL_CALL_SamplePosition();
		Register_UnityEngine_NavMesh_INTERNAL_CALL_SamplePosition();

		//System.Int32 UnityEngine.NavMesh::GetAreaFromName(System.String)
		void Register_UnityEngine_NavMesh_GetAreaFromName();
		Register_UnityEngine_NavMesh_GetAreaFromName();

		//System.Int32 UnityEngine.NavMesh::GetPathfindingIterationsPerFrame()
		void Register_UnityEngine_NavMesh_GetPathfindingIterationsPerFrame();
		Register_UnityEngine_NavMesh_GetPathfindingIterationsPerFrame();

		//System.Object UnityEngine.NavMesh::TriangulateInternal()
		void Register_UnityEngine_NavMesh_TriangulateInternal();
		Register_UnityEngine_NavMesh_TriangulateInternal();

		//System.Single UnityEngine.NavMesh::GetAreaCost(System.Int32)
		void Register_UnityEngine_NavMesh_GetAreaCost();
		Register_UnityEngine_NavMesh_GetAreaCost();

		//System.Single UnityEngine.NavMesh::GetAvoidancePredictionTime()
		void Register_UnityEngine_NavMesh_GetAvoidancePredictionTime();
		Register_UnityEngine_NavMesh_GetAvoidancePredictionTime();

		//System.Void UnityEngine.NavMesh::SetAreaCost(System.Int32,System.Single)
		void Register_UnityEngine_NavMesh_SetAreaCost();
		Register_UnityEngine_NavMesh_SetAreaCost();

		//System.Void UnityEngine.NavMesh::SetAvoidancePredictionTime(System.Single)
		void Register_UnityEngine_NavMesh_SetAvoidancePredictionTime();
		Register_UnityEngine_NavMesh_SetAvoidancePredictionTime();

		//System.Void UnityEngine.NavMesh::SetPathfindingIterationsPerFrame(System.Int32)
		void Register_UnityEngine_NavMesh_SetPathfindingIterationsPerFrame();
		Register_UnityEngine_NavMesh_SetPathfindingIterationsPerFrame();

	//End Registrations for type : UnityEngine.NavMesh

	//Start Registrations for type : UnityEngine.NavMeshAgent

		//System.Boolean UnityEngine.NavMeshAgent::FindClosestEdge(UnityEngine.NavMeshHit&)
		void Register_UnityEngine_NavMeshAgent_FindClosestEdge();
		Register_UnityEngine_NavMeshAgent_FindClosestEdge();

		//System.Boolean UnityEngine.NavMeshAgent::INTERNAL_CALL_CalculatePathInternal(UnityEngine.NavMeshAgent,UnityEngine.Vector3&,UnityEngine.NavMeshPath)
		void Register_UnityEngine_NavMeshAgent_INTERNAL_CALL_CalculatePathInternal();
		Register_UnityEngine_NavMeshAgent_INTERNAL_CALL_CalculatePathInternal();

		//System.Boolean UnityEngine.NavMeshAgent::INTERNAL_CALL_Raycast(UnityEngine.NavMeshAgent,UnityEngine.Vector3&,UnityEngine.NavMeshHit&)
		void Register_UnityEngine_NavMeshAgent_INTERNAL_CALL_Raycast();
		Register_UnityEngine_NavMeshAgent_INTERNAL_CALL_Raycast();

		//System.Boolean UnityEngine.NavMeshAgent::INTERNAL_CALL_SetDestination(UnityEngine.NavMeshAgent,UnityEngine.Vector3&)
		void Register_UnityEngine_NavMeshAgent_INTERNAL_CALL_SetDestination();
		Register_UnityEngine_NavMeshAgent_INTERNAL_CALL_SetDestination();

		//System.Boolean UnityEngine.NavMeshAgent::INTERNAL_CALL_Warp(UnityEngine.NavMeshAgent,UnityEngine.Vector3&)
		void Register_UnityEngine_NavMeshAgent_INTERNAL_CALL_Warp();
		Register_UnityEngine_NavMeshAgent_INTERNAL_CALL_Warp();

		//System.Boolean UnityEngine.NavMeshAgent::SamplePathPosition(System.Int32,System.Single,UnityEngine.NavMeshHit&)
		void Register_UnityEngine_NavMeshAgent_SamplePathPosition();
		Register_UnityEngine_NavMeshAgent_SamplePathPosition();

		//System.Boolean UnityEngine.NavMeshAgent::SetPath(UnityEngine.NavMeshPath)
		void Register_UnityEngine_NavMeshAgent_SetPath();
		Register_UnityEngine_NavMeshAgent_SetPath();

		//System.Boolean UnityEngine.NavMeshAgent::get_autoBraking()
		void Register_UnityEngine_NavMeshAgent_get_autoBraking();
		Register_UnityEngine_NavMeshAgent_get_autoBraking();

		//System.Boolean UnityEngine.NavMeshAgent::get_autoRepath()
		void Register_UnityEngine_NavMeshAgent_get_autoRepath();
		Register_UnityEngine_NavMeshAgent_get_autoRepath();

		//System.Boolean UnityEngine.NavMeshAgent::get_autoTraverseOffMeshLink()
		void Register_UnityEngine_NavMeshAgent_get_autoTraverseOffMeshLink();
		Register_UnityEngine_NavMeshAgent_get_autoTraverseOffMeshLink();

		//System.Boolean UnityEngine.NavMeshAgent::get_hasPath()
		void Register_UnityEngine_NavMeshAgent_get_hasPath();
		Register_UnityEngine_NavMeshAgent_get_hasPath();

		//System.Boolean UnityEngine.NavMeshAgent::get_isOnNavMesh()
		void Register_UnityEngine_NavMeshAgent_get_isOnNavMesh();
		Register_UnityEngine_NavMeshAgent_get_isOnNavMesh();

		//System.Boolean UnityEngine.NavMeshAgent::get_isOnOffMeshLink()
		void Register_UnityEngine_NavMeshAgent_get_isOnOffMeshLink();
		Register_UnityEngine_NavMeshAgent_get_isOnOffMeshLink();

		//System.Boolean UnityEngine.NavMeshAgent::get_isPathStale()
		void Register_UnityEngine_NavMeshAgent_get_isPathStale();
		Register_UnityEngine_NavMeshAgent_get_isPathStale();

		//System.Boolean UnityEngine.NavMeshAgent::get_pathPending()
		void Register_UnityEngine_NavMeshAgent_get_pathPending();
		Register_UnityEngine_NavMeshAgent_get_pathPending();

		//System.Boolean UnityEngine.NavMeshAgent::get_updatePosition()
		void Register_UnityEngine_NavMeshAgent_get_updatePosition();
		Register_UnityEngine_NavMeshAgent_get_updatePosition();

		//System.Boolean UnityEngine.NavMeshAgent::get_updateRotation()
		void Register_UnityEngine_NavMeshAgent_get_updateRotation();
		Register_UnityEngine_NavMeshAgent_get_updateRotation();

		//System.Int32 UnityEngine.NavMeshAgent::get_areaMask()
		void Register_UnityEngine_NavMeshAgent_get_areaMask();
		Register_UnityEngine_NavMeshAgent_get_areaMask();

		//System.Int32 UnityEngine.NavMeshAgent::get_avoidancePriority()
		void Register_UnityEngine_NavMeshAgent_get_avoidancePriority();
		Register_UnityEngine_NavMeshAgent_get_avoidancePriority();

		//System.Single UnityEngine.NavMeshAgent::GetAreaCost(System.Int32)
		void Register_UnityEngine_NavMeshAgent_GetAreaCost();
		Register_UnityEngine_NavMeshAgent_GetAreaCost();

		//System.Single UnityEngine.NavMeshAgent::get_acceleration()
		void Register_UnityEngine_NavMeshAgent_get_acceleration();
		Register_UnityEngine_NavMeshAgent_get_acceleration();

		//System.Single UnityEngine.NavMeshAgent::get_angularSpeed()
		void Register_UnityEngine_NavMeshAgent_get_angularSpeed();
		Register_UnityEngine_NavMeshAgent_get_angularSpeed();

		//System.Single UnityEngine.NavMeshAgent::get_baseOffset()
		void Register_UnityEngine_NavMeshAgent_get_baseOffset();
		Register_UnityEngine_NavMeshAgent_get_baseOffset();

		//System.Single UnityEngine.NavMeshAgent::get_height()
		void Register_UnityEngine_NavMeshAgent_get_height();
		Register_UnityEngine_NavMeshAgent_get_height();

		//System.Single UnityEngine.NavMeshAgent::get_radius()
		void Register_UnityEngine_NavMeshAgent_get_radius();
		Register_UnityEngine_NavMeshAgent_get_radius();

		//System.Single UnityEngine.NavMeshAgent::get_remainingDistance()
		void Register_UnityEngine_NavMeshAgent_get_remainingDistance();
		Register_UnityEngine_NavMeshAgent_get_remainingDistance();

		//System.Single UnityEngine.NavMeshAgent::get_speed()
		void Register_UnityEngine_NavMeshAgent_get_speed();
		Register_UnityEngine_NavMeshAgent_get_speed();

		//System.Single UnityEngine.NavMeshAgent::get_stoppingDistance()
		void Register_UnityEngine_NavMeshAgent_get_stoppingDistance();
		Register_UnityEngine_NavMeshAgent_get_stoppingDistance();

		//System.Void UnityEngine.NavMeshAgent::ActivateCurrentOffMeshLink(System.Boolean)
		void Register_UnityEngine_NavMeshAgent_ActivateCurrentOffMeshLink();
		Register_UnityEngine_NavMeshAgent_ActivateCurrentOffMeshLink();

		//System.Void UnityEngine.NavMeshAgent::CompleteOffMeshLink()
		void Register_UnityEngine_NavMeshAgent_CompleteOffMeshLink();
		Register_UnityEngine_NavMeshAgent_CompleteOffMeshLink();

		//System.Void UnityEngine.NavMeshAgent::CopyPathTo(UnityEngine.NavMeshPath)
		void Register_UnityEngine_NavMeshAgent_CopyPathTo();
		Register_UnityEngine_NavMeshAgent_CopyPathTo();

		//System.Void UnityEngine.NavMeshAgent::INTERNAL_CALL_Move(UnityEngine.NavMeshAgent,UnityEngine.Vector3&)
		void Register_UnityEngine_NavMeshAgent_INTERNAL_CALL_Move();
		Register_UnityEngine_NavMeshAgent_INTERNAL_CALL_Move();

		//System.Void UnityEngine.NavMeshAgent::INTERNAL_get_desiredVelocity(UnityEngine.Vector3&)
		void Register_UnityEngine_NavMeshAgent_INTERNAL_get_desiredVelocity();
		Register_UnityEngine_NavMeshAgent_INTERNAL_get_desiredVelocity();

		//System.Void UnityEngine.NavMeshAgent::INTERNAL_get_destination(UnityEngine.Vector3&)
		void Register_UnityEngine_NavMeshAgent_INTERNAL_get_destination();
		Register_UnityEngine_NavMeshAgent_INTERNAL_get_destination();

		//System.Void UnityEngine.NavMeshAgent::INTERNAL_get_nextPosition(UnityEngine.Vector3&)
		void Register_UnityEngine_NavMeshAgent_INTERNAL_get_nextPosition();
		Register_UnityEngine_NavMeshAgent_INTERNAL_get_nextPosition();

		//System.Void UnityEngine.NavMeshAgent::INTERNAL_get_pathEndPosition(UnityEngine.Vector3&)
		void Register_UnityEngine_NavMeshAgent_INTERNAL_get_pathEndPosition();
		Register_UnityEngine_NavMeshAgent_INTERNAL_get_pathEndPosition();

		//System.Void UnityEngine.NavMeshAgent::INTERNAL_get_steeringTarget(UnityEngine.Vector3&)
		void Register_UnityEngine_NavMeshAgent_INTERNAL_get_steeringTarget();
		Register_UnityEngine_NavMeshAgent_INTERNAL_get_steeringTarget();

		//System.Void UnityEngine.NavMeshAgent::INTERNAL_get_velocity(UnityEngine.Vector3&)
		void Register_UnityEngine_NavMeshAgent_INTERNAL_get_velocity();
		Register_UnityEngine_NavMeshAgent_INTERNAL_get_velocity();

		//System.Void UnityEngine.NavMeshAgent::INTERNAL_set_destination(UnityEngine.Vector3&)
		void Register_UnityEngine_NavMeshAgent_INTERNAL_set_destination();
		Register_UnityEngine_NavMeshAgent_INTERNAL_set_destination();

		//System.Void UnityEngine.NavMeshAgent::INTERNAL_set_nextPosition(UnityEngine.Vector3&)
		void Register_UnityEngine_NavMeshAgent_INTERNAL_set_nextPosition();
		Register_UnityEngine_NavMeshAgent_INTERNAL_set_nextPosition();

		//System.Void UnityEngine.NavMeshAgent::INTERNAL_set_velocity(UnityEngine.Vector3&)
		void Register_UnityEngine_NavMeshAgent_INTERNAL_set_velocity();
		Register_UnityEngine_NavMeshAgent_INTERNAL_set_velocity();

		//System.Void UnityEngine.NavMeshAgent::ResetPath()
		void Register_UnityEngine_NavMeshAgent_ResetPath();
		Register_UnityEngine_NavMeshAgent_ResetPath();

		//System.Void UnityEngine.NavMeshAgent::Resume()
		void Register_UnityEngine_NavMeshAgent_Resume();
		Register_UnityEngine_NavMeshAgent_Resume();

		//System.Void UnityEngine.NavMeshAgent::SetAreaCost(System.Int32,System.Single)
		void Register_UnityEngine_NavMeshAgent_SetAreaCost();
		Register_UnityEngine_NavMeshAgent_SetAreaCost();

		//System.Void UnityEngine.NavMeshAgent::StopInternal()
		void Register_UnityEngine_NavMeshAgent_StopInternal();
		Register_UnityEngine_NavMeshAgent_StopInternal();

		//System.Void UnityEngine.NavMeshAgent::set_acceleration(System.Single)
		void Register_UnityEngine_NavMeshAgent_set_acceleration();
		Register_UnityEngine_NavMeshAgent_set_acceleration();

		//System.Void UnityEngine.NavMeshAgent::set_angularSpeed(System.Single)
		void Register_UnityEngine_NavMeshAgent_set_angularSpeed();
		Register_UnityEngine_NavMeshAgent_set_angularSpeed();

		//System.Void UnityEngine.NavMeshAgent::set_areaMask(System.Int32)
		void Register_UnityEngine_NavMeshAgent_set_areaMask();
		Register_UnityEngine_NavMeshAgent_set_areaMask();

		//System.Void UnityEngine.NavMeshAgent::set_autoBraking(System.Boolean)
		void Register_UnityEngine_NavMeshAgent_set_autoBraking();
		Register_UnityEngine_NavMeshAgent_set_autoBraking();

		//System.Void UnityEngine.NavMeshAgent::set_autoRepath(System.Boolean)
		void Register_UnityEngine_NavMeshAgent_set_autoRepath();
		Register_UnityEngine_NavMeshAgent_set_autoRepath();

		//System.Void UnityEngine.NavMeshAgent::set_autoTraverseOffMeshLink(System.Boolean)
		void Register_UnityEngine_NavMeshAgent_set_autoTraverseOffMeshLink();
		Register_UnityEngine_NavMeshAgent_set_autoTraverseOffMeshLink();

		//System.Void UnityEngine.NavMeshAgent::set_avoidancePriority(System.Int32)
		void Register_UnityEngine_NavMeshAgent_set_avoidancePriority();
		Register_UnityEngine_NavMeshAgent_set_avoidancePriority();

		//System.Void UnityEngine.NavMeshAgent::set_baseOffset(System.Single)
		void Register_UnityEngine_NavMeshAgent_set_baseOffset();
		Register_UnityEngine_NavMeshAgent_set_baseOffset();

		//System.Void UnityEngine.NavMeshAgent::set_height(System.Single)
		void Register_UnityEngine_NavMeshAgent_set_height();
		Register_UnityEngine_NavMeshAgent_set_height();

		//System.Void UnityEngine.NavMeshAgent::set_obstacleAvoidanceType(UnityEngine.ObstacleAvoidanceType)
		void Register_UnityEngine_NavMeshAgent_set_obstacleAvoidanceType();
		Register_UnityEngine_NavMeshAgent_set_obstacleAvoidanceType();

		//System.Void UnityEngine.NavMeshAgent::set_radius(System.Single)
		void Register_UnityEngine_NavMeshAgent_set_radius();
		Register_UnityEngine_NavMeshAgent_set_radius();

		//System.Void UnityEngine.NavMeshAgent::set_speed(System.Single)
		void Register_UnityEngine_NavMeshAgent_set_speed();
		Register_UnityEngine_NavMeshAgent_set_speed();

		//System.Void UnityEngine.NavMeshAgent::set_stoppingDistance(System.Single)
		void Register_UnityEngine_NavMeshAgent_set_stoppingDistance();
		Register_UnityEngine_NavMeshAgent_set_stoppingDistance();

		//System.Void UnityEngine.NavMeshAgent::set_updatePosition(System.Boolean)
		void Register_UnityEngine_NavMeshAgent_set_updatePosition();
		Register_UnityEngine_NavMeshAgent_set_updatePosition();

		//System.Void UnityEngine.NavMeshAgent::set_updateRotation(System.Boolean)
		void Register_UnityEngine_NavMeshAgent_set_updateRotation();
		Register_UnityEngine_NavMeshAgent_set_updateRotation();

		//UnityEngine.NavMeshPathStatus UnityEngine.NavMeshAgent::get_pathStatus()
		void Register_UnityEngine_NavMeshAgent_get_pathStatus();
		Register_UnityEngine_NavMeshAgent_get_pathStatus();

		//UnityEngine.ObstacleAvoidanceType UnityEngine.NavMeshAgent::get_obstacleAvoidanceType()
		void Register_UnityEngine_NavMeshAgent_get_obstacleAvoidanceType();
		Register_UnityEngine_NavMeshAgent_get_obstacleAvoidanceType();

		//UnityEngine.OffMeshLinkData UnityEngine.NavMeshAgent::GetCurrentOffMeshLinkDataInternal()
		void Register_UnityEngine_NavMeshAgent_GetCurrentOffMeshLinkDataInternal();
		Register_UnityEngine_NavMeshAgent_GetCurrentOffMeshLinkDataInternal();

		//UnityEngine.OffMeshLinkData UnityEngine.NavMeshAgent::GetNextOffMeshLinkDataInternal()
		void Register_UnityEngine_NavMeshAgent_GetNextOffMeshLinkDataInternal();
		Register_UnityEngine_NavMeshAgent_GetNextOffMeshLinkDataInternal();

	//End Registrations for type : UnityEngine.NavMeshAgent

	//Start Registrations for type : UnityEngine.NavMeshObstacle

		//System.Boolean UnityEngine.NavMeshObstacle::get_carveOnlyStationary()
		void Register_UnityEngine_NavMeshObstacle_get_carveOnlyStationary();
		Register_UnityEngine_NavMeshObstacle_get_carveOnlyStationary();

		//System.Boolean UnityEngine.NavMeshObstacle::get_carving()
		void Register_UnityEngine_NavMeshObstacle_get_carving();
		Register_UnityEngine_NavMeshObstacle_get_carving();

		//System.Single UnityEngine.NavMeshObstacle::get_carvingMoveThreshold()
		void Register_UnityEngine_NavMeshObstacle_get_carvingMoveThreshold();
		Register_UnityEngine_NavMeshObstacle_get_carvingMoveThreshold();

		//System.Single UnityEngine.NavMeshObstacle::get_carvingTimeToStationary()
		void Register_UnityEngine_NavMeshObstacle_get_carvingTimeToStationary();
		Register_UnityEngine_NavMeshObstacle_get_carvingTimeToStationary();

		//System.Single UnityEngine.NavMeshObstacle::get_height()
		void Register_UnityEngine_NavMeshObstacle_get_height();
		Register_UnityEngine_NavMeshObstacle_get_height();

		//System.Single UnityEngine.NavMeshObstacle::get_radius()
		void Register_UnityEngine_NavMeshObstacle_get_radius();
		Register_UnityEngine_NavMeshObstacle_get_radius();

		//System.Void UnityEngine.NavMeshObstacle::INTERNAL_get_center(UnityEngine.Vector3&)
		void Register_UnityEngine_NavMeshObstacle_INTERNAL_get_center();
		Register_UnityEngine_NavMeshObstacle_INTERNAL_get_center();

		//System.Void UnityEngine.NavMeshObstacle::INTERNAL_get_size(UnityEngine.Vector3&)
		void Register_UnityEngine_NavMeshObstacle_INTERNAL_get_size();
		Register_UnityEngine_NavMeshObstacle_INTERNAL_get_size();

		//System.Void UnityEngine.NavMeshObstacle::INTERNAL_get_velocity(UnityEngine.Vector3&)
		void Register_UnityEngine_NavMeshObstacle_INTERNAL_get_velocity();
		Register_UnityEngine_NavMeshObstacle_INTERNAL_get_velocity();

		//System.Void UnityEngine.NavMeshObstacle::INTERNAL_set_center(UnityEngine.Vector3&)
		void Register_UnityEngine_NavMeshObstacle_INTERNAL_set_center();
		Register_UnityEngine_NavMeshObstacle_INTERNAL_set_center();

		//System.Void UnityEngine.NavMeshObstacle::INTERNAL_set_size(UnityEngine.Vector3&)
		void Register_UnityEngine_NavMeshObstacle_INTERNAL_set_size();
		Register_UnityEngine_NavMeshObstacle_INTERNAL_set_size();

		//System.Void UnityEngine.NavMeshObstacle::INTERNAL_set_velocity(UnityEngine.Vector3&)
		void Register_UnityEngine_NavMeshObstacle_INTERNAL_set_velocity();
		Register_UnityEngine_NavMeshObstacle_INTERNAL_set_velocity();

		//System.Void UnityEngine.NavMeshObstacle::set_carveOnlyStationary(System.Boolean)
		void Register_UnityEngine_NavMeshObstacle_set_carveOnlyStationary();
		Register_UnityEngine_NavMeshObstacle_set_carveOnlyStationary();

		//System.Void UnityEngine.NavMeshObstacle::set_carving(System.Boolean)
		void Register_UnityEngine_NavMeshObstacle_set_carving();
		Register_UnityEngine_NavMeshObstacle_set_carving();

		//System.Void UnityEngine.NavMeshObstacle::set_carvingMoveThreshold(System.Single)
		void Register_UnityEngine_NavMeshObstacle_set_carvingMoveThreshold();
		Register_UnityEngine_NavMeshObstacle_set_carvingMoveThreshold();

		//System.Void UnityEngine.NavMeshObstacle::set_carvingTimeToStationary(System.Single)
		void Register_UnityEngine_NavMeshObstacle_set_carvingTimeToStationary();
		Register_UnityEngine_NavMeshObstacle_set_carvingTimeToStationary();

		//System.Void UnityEngine.NavMeshObstacle::set_height(System.Single)
		void Register_UnityEngine_NavMeshObstacle_set_height();
		Register_UnityEngine_NavMeshObstacle_set_height();

		//System.Void UnityEngine.NavMeshObstacle::set_radius(System.Single)
		void Register_UnityEngine_NavMeshObstacle_set_radius();
		Register_UnityEngine_NavMeshObstacle_set_radius();

		//System.Void UnityEngine.NavMeshObstacle::set_shape(UnityEngine.NavMeshObstacleShape)
		void Register_UnityEngine_NavMeshObstacle_set_shape();
		Register_UnityEngine_NavMeshObstacle_set_shape();

		//UnityEngine.NavMeshObstacleShape UnityEngine.NavMeshObstacle::get_shape()
		void Register_UnityEngine_NavMeshObstacle_get_shape();
		Register_UnityEngine_NavMeshObstacle_get_shape();

	//End Registrations for type : UnityEngine.NavMeshObstacle

	//Start Registrations for type : UnityEngine.NavMeshPath

		//System.Int32 UnityEngine.NavMeshPath::GetCornersNonAlloc(UnityEngine.Vector3[])
		void Register_UnityEngine_NavMeshPath_GetCornersNonAlloc();
		Register_UnityEngine_NavMeshPath_GetCornersNonAlloc();

		//System.Void UnityEngine.NavMeshPath::.ctor()
		void Register_UnityEngine_NavMeshPath__ctor();
		Register_UnityEngine_NavMeshPath__ctor();

		//System.Void UnityEngine.NavMeshPath::ClearCornersInternal()
		void Register_UnityEngine_NavMeshPath_ClearCornersInternal();
		Register_UnityEngine_NavMeshPath_ClearCornersInternal();

		//System.Void UnityEngine.NavMeshPath::DestroyNavMeshPath()
		void Register_UnityEngine_NavMeshPath_DestroyNavMeshPath();
		Register_UnityEngine_NavMeshPath_DestroyNavMeshPath();

		//UnityEngine.NavMeshPathStatus UnityEngine.NavMeshPath::get_status()
		void Register_UnityEngine_NavMeshPath_get_status();
		Register_UnityEngine_NavMeshPath_get_status();

		//UnityEngine.Vector3[] UnityEngine.NavMeshPath::CalculateCornersInternal()
		void Register_UnityEngine_NavMeshPath_CalculateCornersInternal();
		Register_UnityEngine_NavMeshPath_CalculateCornersInternal();

	//End Registrations for type : UnityEngine.NavMeshPath

	//Start Registrations for type : UnityEngine.NetworkMessageInfo

		//UnityEngine.NetworkView UnityEngine.NetworkMessageInfo::NullNetworkView()
		void Register_UnityEngine_NetworkMessageInfo_NullNetworkView();
		Register_UnityEngine_NetworkMessageInfo_NullNetworkView();

	//End Registrations for type : UnityEngine.NetworkMessageInfo

	//Start Registrations for type : UnityEngine.NetworkPlayer

		//System.Int32 UnityEngine.NetworkPlayer::Internal_GetExternalPort()
		void Register_UnityEngine_NetworkPlayer_Internal_GetExternalPort();
		Register_UnityEngine_NetworkPlayer_Internal_GetExternalPort();

		//System.Int32 UnityEngine.NetworkPlayer::Internal_GetLocalPort()
		void Register_UnityEngine_NetworkPlayer_Internal_GetLocalPort();
		Register_UnityEngine_NetworkPlayer_Internal_GetLocalPort();

		//System.Int32 UnityEngine.NetworkPlayer::Internal_GetPlayerIndex()
		void Register_UnityEngine_NetworkPlayer_Internal_GetPlayerIndex();
		Register_UnityEngine_NetworkPlayer_Internal_GetPlayerIndex();

		//System.Int32 UnityEngine.NetworkPlayer::Internal_GetPort(System.Int32)
		void Register_UnityEngine_NetworkPlayer_Internal_GetPort();
		Register_UnityEngine_NetworkPlayer_Internal_GetPort();

		//System.String UnityEngine.NetworkPlayer::Internal_GetExternalIP()
		void Register_UnityEngine_NetworkPlayer_Internal_GetExternalIP();
		Register_UnityEngine_NetworkPlayer_Internal_GetExternalIP();

		//System.String UnityEngine.NetworkPlayer::Internal_GetGUID(System.Int32)
		void Register_UnityEngine_NetworkPlayer_Internal_GetGUID();
		Register_UnityEngine_NetworkPlayer_Internal_GetGUID();

		//System.String UnityEngine.NetworkPlayer::Internal_GetIPAddress(System.Int32)
		void Register_UnityEngine_NetworkPlayer_Internal_GetIPAddress();
		Register_UnityEngine_NetworkPlayer_Internal_GetIPAddress();

		//System.String UnityEngine.NetworkPlayer::Internal_GetLocalGUID()
		void Register_UnityEngine_NetworkPlayer_Internal_GetLocalGUID();
		Register_UnityEngine_NetworkPlayer_Internal_GetLocalGUID();

		//System.String UnityEngine.NetworkPlayer::Internal_GetLocalIP()
		void Register_UnityEngine_NetworkPlayer_Internal_GetLocalIP();
		Register_UnityEngine_NetworkPlayer_Internal_GetLocalIP();

	//End Registrations for type : UnityEngine.NetworkPlayer

	//Start Registrations for type : UnityEngine.NetworkView

		//UnityEngine.NetworkView UnityEngine.NetworkView::INTERNAL_CALL_Find(UnityEngine.NetworkViewID&)
		void Register_UnityEngine_NetworkView_INTERNAL_CALL_Find();
		Register_UnityEngine_NetworkView_INTERNAL_CALL_Find();

	//End Registrations for type : UnityEngine.NetworkView

	//Start Registrations for type : UnityEngine.NetworkViewID

		//System.Boolean UnityEngine.NetworkViewID::INTERNAL_CALL_Internal_Compare(UnityEngine.NetworkViewID&,UnityEngine.NetworkViewID&)
		void Register_UnityEngine_NetworkViewID_INTERNAL_CALL_Internal_Compare();
		Register_UnityEngine_NetworkViewID_INTERNAL_CALL_Internal_Compare();

		//System.Boolean UnityEngine.NetworkViewID::INTERNAL_CALL_Internal_IsMine(UnityEngine.NetworkViewID&)
		void Register_UnityEngine_NetworkViewID_INTERNAL_CALL_Internal_IsMine();
		Register_UnityEngine_NetworkViewID_INTERNAL_CALL_Internal_IsMine();

		//System.String UnityEngine.NetworkViewID::INTERNAL_CALL_Internal_GetString(UnityEngine.NetworkViewID&)
		void Register_UnityEngine_NetworkViewID_INTERNAL_CALL_Internal_GetString();
		Register_UnityEngine_NetworkViewID_INTERNAL_CALL_Internal_GetString();

		//System.Void UnityEngine.NetworkViewID::INTERNAL_CALL_Internal_GetOwner(UnityEngine.NetworkViewID&,UnityEngine.NetworkPlayer&)
		void Register_UnityEngine_NetworkViewID_INTERNAL_CALL_Internal_GetOwner();
		Register_UnityEngine_NetworkViewID_INTERNAL_CALL_Internal_GetOwner();

		//System.Void UnityEngine.NetworkViewID::INTERNAL_get_unassigned(UnityEngine.NetworkViewID&)
		void Register_UnityEngine_NetworkViewID_INTERNAL_get_unassigned();
		Register_UnityEngine_NetworkViewID_INTERNAL_get_unassigned();

	//End Registrations for type : UnityEngine.NetworkViewID

	//Start Registrations for type : UnityEngine.Object

		//System.Boolean UnityEngine.Object::DoesObjectWithInstanceIDExist(System.Int32)
		void Register_UnityEngine_Object_DoesObjectWithInstanceIDExist();
		Register_UnityEngine_Object_DoesObjectWithInstanceIDExist();

		//System.String UnityEngine.Object::ToString()
		void Register_UnityEngine_Object_ToString();
		Register_UnityEngine_Object_ToString();

		//System.String UnityEngine.Object::get_name()
		void Register_UnityEngine_Object_get_name();
		Register_UnityEngine_Object_get_name();

		//System.Void UnityEngine.Object::Destroy(UnityEngine.Object,System.Single)
		void Register_UnityEngine_Object_Destroy();
		Register_UnityEngine_Object_Destroy();

		//System.Void UnityEngine.Object::DestroyImmediate(UnityEngine.Object,System.Boolean)
		void Register_UnityEngine_Object_DestroyImmediate();
		Register_UnityEngine_Object_DestroyImmediate();

		//System.Void UnityEngine.Object::DestroyObject(UnityEngine.Object,System.Single)
		void Register_UnityEngine_Object_DestroyObject();
		Register_UnityEngine_Object_DestroyObject();

		//System.Void UnityEngine.Object::DontDestroyOnLoad(UnityEngine.Object)
		void Register_UnityEngine_Object_DontDestroyOnLoad();
		Register_UnityEngine_Object_DontDestroyOnLoad();

		//System.Void UnityEngine.Object::set_hideFlags(UnityEngine.HideFlags)
		void Register_UnityEngine_Object_set_hideFlags();
		Register_UnityEngine_Object_set_hideFlags();

		//System.Void UnityEngine.Object::set_name(System.String)
		void Register_UnityEngine_Object_set_name();
		Register_UnityEngine_Object_set_name();

		//UnityEngine.HideFlags UnityEngine.Object::get_hideFlags()
		void Register_UnityEngine_Object_get_hideFlags();
		Register_UnityEngine_Object_get_hideFlags();

		//UnityEngine.Object UnityEngine.Object::INTERNAL_CALL_Internal_InstantiateSingle(UnityEngine.Object,UnityEngine.Vector3&,UnityEngine.Quaternion&)
		void Register_UnityEngine_Object_INTERNAL_CALL_Internal_InstantiateSingle();
		Register_UnityEngine_Object_INTERNAL_CALL_Internal_InstantiateSingle();

		//UnityEngine.Object UnityEngine.Object::Internal_CloneSingle(UnityEngine.Object)
		void Register_UnityEngine_Object_Internal_CloneSingle();
		Register_UnityEngine_Object_Internal_CloneSingle();

		//UnityEngine.Object[] UnityEngine.Object::FindObjectsOfType(System.Type)
		void Register_UnityEngine_Object_FindObjectsOfType();
		Register_UnityEngine_Object_FindObjectsOfType();

		//UnityEngine.Object[] UnityEngine.Object::FindObjectsOfTypeIncludingAssets(System.Type)
		void Register_UnityEngine_Object_FindObjectsOfTypeIncludingAssets();
		Register_UnityEngine_Object_FindObjectsOfTypeIncludingAssets();

		//UnityEngine.Object[] UnityEngine.Object::FindSceneObjectsOfType(System.Type)
		void Register_UnityEngine_Object_FindSceneObjectsOfType();
		Register_UnityEngine_Object_FindSceneObjectsOfType();

	//End Registrations for type : UnityEngine.Object

	//Start Registrations for type : UnityEngine.OcclusionArea

		//System.Void UnityEngine.OcclusionArea::INTERNAL_get_center(UnityEngine.Vector3&)
		void Register_UnityEngine_OcclusionArea_INTERNAL_get_center();
		Register_UnityEngine_OcclusionArea_INTERNAL_get_center();

		//System.Void UnityEngine.OcclusionArea::INTERNAL_get_size(UnityEngine.Vector3&)
		void Register_UnityEngine_OcclusionArea_INTERNAL_get_size();
		Register_UnityEngine_OcclusionArea_INTERNAL_get_size();

		//System.Void UnityEngine.OcclusionArea::INTERNAL_set_center(UnityEngine.Vector3&)
		void Register_UnityEngine_OcclusionArea_INTERNAL_set_center();
		Register_UnityEngine_OcclusionArea_INTERNAL_set_center();

		//System.Void UnityEngine.OcclusionArea::INTERNAL_set_size(UnityEngine.Vector3&)
		void Register_UnityEngine_OcclusionArea_INTERNAL_set_size();
		Register_UnityEngine_OcclusionArea_INTERNAL_set_size();

	//End Registrations for type : UnityEngine.OcclusionArea

	//Start Registrations for type : UnityEngine.OcclusionPortal

		//System.Boolean UnityEngine.OcclusionPortal::get_open()
		void Register_UnityEngine_OcclusionPortal_get_open();
		Register_UnityEngine_OcclusionPortal_get_open();

		//System.Void UnityEngine.OcclusionPortal::set_open(System.Boolean)
		void Register_UnityEngine_OcclusionPortal_set_open();
		Register_UnityEngine_OcclusionPortal_set_open();

	//End Registrations for type : UnityEngine.OcclusionPortal

	//Start Registrations for type : UnityEngine.OffMeshLink

		//System.Boolean UnityEngine.OffMeshLink::get_activated()
		void Register_UnityEngine_OffMeshLink_get_activated();
		Register_UnityEngine_OffMeshLink_get_activated();

		//System.Boolean UnityEngine.OffMeshLink::get_autoUpdatePositions()
		void Register_UnityEngine_OffMeshLink_get_autoUpdatePositions();
		Register_UnityEngine_OffMeshLink_get_autoUpdatePositions();

		//System.Boolean UnityEngine.OffMeshLink::get_biDirectional()
		void Register_UnityEngine_OffMeshLink_get_biDirectional();
		Register_UnityEngine_OffMeshLink_get_biDirectional();

		//System.Boolean UnityEngine.OffMeshLink::get_occupied()
		void Register_UnityEngine_OffMeshLink_get_occupied();
		Register_UnityEngine_OffMeshLink_get_occupied();

		//System.Int32 UnityEngine.OffMeshLink::get_area()
		void Register_UnityEngine_OffMeshLink_get_area();
		Register_UnityEngine_OffMeshLink_get_area();

		//System.Single UnityEngine.OffMeshLink::get_costOverride()
		void Register_UnityEngine_OffMeshLink_get_costOverride();
		Register_UnityEngine_OffMeshLink_get_costOverride();

		//System.Void UnityEngine.OffMeshLink::UpdatePositions()
		void Register_UnityEngine_OffMeshLink_UpdatePositions();
		Register_UnityEngine_OffMeshLink_UpdatePositions();

		//System.Void UnityEngine.OffMeshLink::set_activated(System.Boolean)
		void Register_UnityEngine_OffMeshLink_set_activated();
		Register_UnityEngine_OffMeshLink_set_activated();

		//System.Void UnityEngine.OffMeshLink::set_area(System.Int32)
		void Register_UnityEngine_OffMeshLink_set_area();
		Register_UnityEngine_OffMeshLink_set_area();

		//System.Void UnityEngine.OffMeshLink::set_autoUpdatePositions(System.Boolean)
		void Register_UnityEngine_OffMeshLink_set_autoUpdatePositions();
		Register_UnityEngine_OffMeshLink_set_autoUpdatePositions();

		//System.Void UnityEngine.OffMeshLink::set_biDirectional(System.Boolean)
		void Register_UnityEngine_OffMeshLink_set_biDirectional();
		Register_UnityEngine_OffMeshLink_set_biDirectional();

		//System.Void UnityEngine.OffMeshLink::set_costOverride(System.Single)
		void Register_UnityEngine_OffMeshLink_set_costOverride();
		Register_UnityEngine_OffMeshLink_set_costOverride();

		//System.Void UnityEngine.OffMeshLink::set_endTransform(UnityEngine.Transform)
		void Register_UnityEngine_OffMeshLink_set_endTransform();
		Register_UnityEngine_OffMeshLink_set_endTransform();

		//System.Void UnityEngine.OffMeshLink::set_startTransform(UnityEngine.Transform)
		void Register_UnityEngine_OffMeshLink_set_startTransform();
		Register_UnityEngine_OffMeshLink_set_startTransform();

		//UnityEngine.Transform UnityEngine.OffMeshLink::get_endTransform()
		void Register_UnityEngine_OffMeshLink_get_endTransform();
		Register_UnityEngine_OffMeshLink_get_endTransform();

		//UnityEngine.Transform UnityEngine.OffMeshLink::get_startTransform()
		void Register_UnityEngine_OffMeshLink_get_startTransform();
		Register_UnityEngine_OffMeshLink_get_startTransform();

	//End Registrations for type : UnityEngine.OffMeshLink

	//Start Registrations for type : UnityEngine.OffMeshLinkData

		//UnityEngine.OffMeshLink UnityEngine.OffMeshLinkData::GetOffMeshLinkInternal(System.Int32)
		void Register_UnityEngine_OffMeshLinkData_GetOffMeshLinkInternal();
		Register_UnityEngine_OffMeshLinkData_GetOffMeshLinkInternal();

	//End Registrations for type : UnityEngine.OffMeshLinkData

	//Start Registrations for type : UnityEngine.ParticleAnimator

		//System.Boolean UnityEngine.ParticleAnimator::get_autodestruct()
		void Register_UnityEngine_ParticleAnimator_get_autodestruct();
		Register_UnityEngine_ParticleAnimator_get_autodestruct();

		//System.Boolean UnityEngine.ParticleAnimator::get_doesAnimateColor()
		void Register_UnityEngine_ParticleAnimator_get_doesAnimateColor();
		Register_UnityEngine_ParticleAnimator_get_doesAnimateColor();

		//System.Single UnityEngine.ParticleAnimator::get_damping()
		void Register_UnityEngine_ParticleAnimator_get_damping();
		Register_UnityEngine_ParticleAnimator_get_damping();

		//System.Single UnityEngine.ParticleAnimator::get_sizeGrow()
		void Register_UnityEngine_ParticleAnimator_get_sizeGrow();
		Register_UnityEngine_ParticleAnimator_get_sizeGrow();

		//System.Void UnityEngine.ParticleAnimator::INTERNAL_get_force(UnityEngine.Vector3&)
		void Register_UnityEngine_ParticleAnimator_INTERNAL_get_force();
		Register_UnityEngine_ParticleAnimator_INTERNAL_get_force();

		//System.Void UnityEngine.ParticleAnimator::INTERNAL_get_localRotationAxis(UnityEngine.Vector3&)
		void Register_UnityEngine_ParticleAnimator_INTERNAL_get_localRotationAxis();
		Register_UnityEngine_ParticleAnimator_INTERNAL_get_localRotationAxis();

		//System.Void UnityEngine.ParticleAnimator::INTERNAL_get_rndForce(UnityEngine.Vector3&)
		void Register_UnityEngine_ParticleAnimator_INTERNAL_get_rndForce();
		Register_UnityEngine_ParticleAnimator_INTERNAL_get_rndForce();

		//System.Void UnityEngine.ParticleAnimator::INTERNAL_get_worldRotationAxis(UnityEngine.Vector3&)
		void Register_UnityEngine_ParticleAnimator_INTERNAL_get_worldRotationAxis();
		Register_UnityEngine_ParticleAnimator_INTERNAL_get_worldRotationAxis();

		//System.Void UnityEngine.ParticleAnimator::INTERNAL_set_force(UnityEngine.Vector3&)
		void Register_UnityEngine_ParticleAnimator_INTERNAL_set_force();
		Register_UnityEngine_ParticleAnimator_INTERNAL_set_force();

		//System.Void UnityEngine.ParticleAnimator::INTERNAL_set_localRotationAxis(UnityEngine.Vector3&)
		void Register_UnityEngine_ParticleAnimator_INTERNAL_set_localRotationAxis();
		Register_UnityEngine_ParticleAnimator_INTERNAL_set_localRotationAxis();

		//System.Void UnityEngine.ParticleAnimator::INTERNAL_set_rndForce(UnityEngine.Vector3&)
		void Register_UnityEngine_ParticleAnimator_INTERNAL_set_rndForce();
		Register_UnityEngine_ParticleAnimator_INTERNAL_set_rndForce();

		//System.Void UnityEngine.ParticleAnimator::INTERNAL_set_worldRotationAxis(UnityEngine.Vector3&)
		void Register_UnityEngine_ParticleAnimator_INTERNAL_set_worldRotationAxis();
		Register_UnityEngine_ParticleAnimator_INTERNAL_set_worldRotationAxis();

		//System.Void UnityEngine.ParticleAnimator::set_autodestruct(System.Boolean)
		void Register_UnityEngine_ParticleAnimator_set_autodestruct();
		Register_UnityEngine_ParticleAnimator_set_autodestruct();

		//System.Void UnityEngine.ParticleAnimator::set_colorAnimation(UnityEngine.Color[])
		void Register_UnityEngine_ParticleAnimator_set_colorAnimation();
		Register_UnityEngine_ParticleAnimator_set_colorAnimation();

		//System.Void UnityEngine.ParticleAnimator::set_damping(System.Single)
		void Register_UnityEngine_ParticleAnimator_set_damping();
		Register_UnityEngine_ParticleAnimator_set_damping();

		//System.Void UnityEngine.ParticleAnimator::set_doesAnimateColor(System.Boolean)
		void Register_UnityEngine_ParticleAnimator_set_doesAnimateColor();
		Register_UnityEngine_ParticleAnimator_set_doesAnimateColor();

		//System.Void UnityEngine.ParticleAnimator::set_sizeGrow(System.Single)
		void Register_UnityEngine_ParticleAnimator_set_sizeGrow();
		Register_UnityEngine_ParticleAnimator_set_sizeGrow();

		//UnityEngine.Color[] UnityEngine.ParticleAnimator::get_colorAnimation()
		void Register_UnityEngine_ParticleAnimator_get_colorAnimation();
		Register_UnityEngine_ParticleAnimator_get_colorAnimation();

	//End Registrations for type : UnityEngine.ParticleAnimator

	//Start Registrations for type : UnityEngine.ParticleEmitter

		//System.Boolean UnityEngine.ParticleEmitter::get_emit()
		void Register_UnityEngine_ParticleEmitter_get_emit();
		Register_UnityEngine_ParticleEmitter_get_emit();

		//System.Boolean UnityEngine.ParticleEmitter::get_enabled()
		void Register_UnityEngine_ParticleEmitter_get_enabled();
		Register_UnityEngine_ParticleEmitter_get_enabled();

		//System.Boolean UnityEngine.ParticleEmitter::get_rndRotation()
		void Register_UnityEngine_ParticleEmitter_get_rndRotation();
		Register_UnityEngine_ParticleEmitter_get_rndRotation();

		//System.Boolean UnityEngine.ParticleEmitter::get_useWorldSpace()
		void Register_UnityEngine_ParticleEmitter_get_useWorldSpace();
		Register_UnityEngine_ParticleEmitter_get_useWorldSpace();

		//System.Int32 UnityEngine.ParticleEmitter::get_particleCount()
		void Register_UnityEngine_ParticleEmitter_get_particleCount();
		Register_UnityEngine_ParticleEmitter_get_particleCount();

		//System.Single UnityEngine.ParticleEmitter::get_angularVelocity()
		void Register_UnityEngine_ParticleEmitter_get_angularVelocity();
		Register_UnityEngine_ParticleEmitter_get_angularVelocity();

		//System.Single UnityEngine.ParticleEmitter::get_emitterVelocityScale()
		void Register_UnityEngine_ParticleEmitter_get_emitterVelocityScale();
		Register_UnityEngine_ParticleEmitter_get_emitterVelocityScale();

		//System.Single UnityEngine.ParticleEmitter::get_maxEmission()
		void Register_UnityEngine_ParticleEmitter_get_maxEmission();
		Register_UnityEngine_ParticleEmitter_get_maxEmission();

		//System.Single UnityEngine.ParticleEmitter::get_maxEnergy()
		void Register_UnityEngine_ParticleEmitter_get_maxEnergy();
		Register_UnityEngine_ParticleEmitter_get_maxEnergy();

		//System.Single UnityEngine.ParticleEmitter::get_maxSize()
		void Register_UnityEngine_ParticleEmitter_get_maxSize();
		Register_UnityEngine_ParticleEmitter_get_maxSize();

		//System.Single UnityEngine.ParticleEmitter::get_minEmission()
		void Register_UnityEngine_ParticleEmitter_get_minEmission();
		Register_UnityEngine_ParticleEmitter_get_minEmission();

		//System.Single UnityEngine.ParticleEmitter::get_minEnergy()
		void Register_UnityEngine_ParticleEmitter_get_minEnergy();
		Register_UnityEngine_ParticleEmitter_get_minEnergy();

		//System.Single UnityEngine.ParticleEmitter::get_minSize()
		void Register_UnityEngine_ParticleEmitter_get_minSize();
		Register_UnityEngine_ParticleEmitter_get_minSize();

		//System.Single UnityEngine.ParticleEmitter::get_rndAngularVelocity()
		void Register_UnityEngine_ParticleEmitter_get_rndAngularVelocity();
		Register_UnityEngine_ParticleEmitter_get_rndAngularVelocity();

		//System.Void UnityEngine.ParticleEmitter::Emit2(System.Int32)
		void Register_UnityEngine_ParticleEmitter_Emit2();
		Register_UnityEngine_ParticleEmitter_Emit2();

		//System.Void UnityEngine.ParticleEmitter::Emit3(UnityEngine.InternalEmitParticleArguments&)
		void Register_UnityEngine_ParticleEmitter_Emit3();
		Register_UnityEngine_ParticleEmitter_Emit3();

		//System.Void UnityEngine.ParticleEmitter::INTERNAL_CALL_ClearParticles(UnityEngine.ParticleEmitter)
		void Register_UnityEngine_ParticleEmitter_INTERNAL_CALL_ClearParticles();
		Register_UnityEngine_ParticleEmitter_INTERNAL_CALL_ClearParticles();

		//System.Void UnityEngine.ParticleEmitter::INTERNAL_get_localVelocity(UnityEngine.Vector3&)
		void Register_UnityEngine_ParticleEmitter_INTERNAL_get_localVelocity();
		Register_UnityEngine_ParticleEmitter_INTERNAL_get_localVelocity();

		//System.Void UnityEngine.ParticleEmitter::INTERNAL_get_rndVelocity(UnityEngine.Vector3&)
		void Register_UnityEngine_ParticleEmitter_INTERNAL_get_rndVelocity();
		Register_UnityEngine_ParticleEmitter_INTERNAL_get_rndVelocity();

		//System.Void UnityEngine.ParticleEmitter::INTERNAL_get_worldVelocity(UnityEngine.Vector3&)
		void Register_UnityEngine_ParticleEmitter_INTERNAL_get_worldVelocity();
		Register_UnityEngine_ParticleEmitter_INTERNAL_get_worldVelocity();

		//System.Void UnityEngine.ParticleEmitter::INTERNAL_set_localVelocity(UnityEngine.Vector3&)
		void Register_UnityEngine_ParticleEmitter_INTERNAL_set_localVelocity();
		Register_UnityEngine_ParticleEmitter_INTERNAL_set_localVelocity();

		//System.Void UnityEngine.ParticleEmitter::INTERNAL_set_rndVelocity(UnityEngine.Vector3&)
		void Register_UnityEngine_ParticleEmitter_INTERNAL_set_rndVelocity();
		Register_UnityEngine_ParticleEmitter_INTERNAL_set_rndVelocity();

		//System.Void UnityEngine.ParticleEmitter::INTERNAL_set_worldVelocity(UnityEngine.Vector3&)
		void Register_UnityEngine_ParticleEmitter_INTERNAL_set_worldVelocity();
		Register_UnityEngine_ParticleEmitter_INTERNAL_set_worldVelocity();

		//System.Void UnityEngine.ParticleEmitter::Simulate(System.Single)
		void Register_UnityEngine_ParticleEmitter_Simulate();
		Register_UnityEngine_ParticleEmitter_Simulate();

		//System.Void UnityEngine.ParticleEmitter::set_angularVelocity(System.Single)
		void Register_UnityEngine_ParticleEmitter_set_angularVelocity();
		Register_UnityEngine_ParticleEmitter_set_angularVelocity();

		//System.Void UnityEngine.ParticleEmitter::set_emit(System.Boolean)
		void Register_UnityEngine_ParticleEmitter_set_emit();
		Register_UnityEngine_ParticleEmitter_set_emit();

		//System.Void UnityEngine.ParticleEmitter::set_emitterVelocityScale(System.Single)
		void Register_UnityEngine_ParticleEmitter_set_emitterVelocityScale();
		Register_UnityEngine_ParticleEmitter_set_emitterVelocityScale();

		//System.Void UnityEngine.ParticleEmitter::set_enabled(System.Boolean)
		void Register_UnityEngine_ParticleEmitter_set_enabled();
		Register_UnityEngine_ParticleEmitter_set_enabled();

		//System.Void UnityEngine.ParticleEmitter::set_maxEmission(System.Single)
		void Register_UnityEngine_ParticleEmitter_set_maxEmission();
		Register_UnityEngine_ParticleEmitter_set_maxEmission();

		//System.Void UnityEngine.ParticleEmitter::set_maxEnergy(System.Single)
		void Register_UnityEngine_ParticleEmitter_set_maxEnergy();
		Register_UnityEngine_ParticleEmitter_set_maxEnergy();

		//System.Void UnityEngine.ParticleEmitter::set_maxSize(System.Single)
		void Register_UnityEngine_ParticleEmitter_set_maxSize();
		Register_UnityEngine_ParticleEmitter_set_maxSize();

		//System.Void UnityEngine.ParticleEmitter::set_minEmission(System.Single)
		void Register_UnityEngine_ParticleEmitter_set_minEmission();
		Register_UnityEngine_ParticleEmitter_set_minEmission();

		//System.Void UnityEngine.ParticleEmitter::set_minEnergy(System.Single)
		void Register_UnityEngine_ParticleEmitter_set_minEnergy();
		Register_UnityEngine_ParticleEmitter_set_minEnergy();

		//System.Void UnityEngine.ParticleEmitter::set_minSize(System.Single)
		void Register_UnityEngine_ParticleEmitter_set_minSize();
		Register_UnityEngine_ParticleEmitter_set_minSize();

		//System.Void UnityEngine.ParticleEmitter::set_particles(UnityEngine.Particle[])
		void Register_UnityEngine_ParticleEmitter_set_particles();
		Register_UnityEngine_ParticleEmitter_set_particles();

		//System.Void UnityEngine.ParticleEmitter::set_rndAngularVelocity(System.Single)
		void Register_UnityEngine_ParticleEmitter_set_rndAngularVelocity();
		Register_UnityEngine_ParticleEmitter_set_rndAngularVelocity();

		//System.Void UnityEngine.ParticleEmitter::set_rndRotation(System.Boolean)
		void Register_UnityEngine_ParticleEmitter_set_rndRotation();
		Register_UnityEngine_ParticleEmitter_set_rndRotation();

		//System.Void UnityEngine.ParticleEmitter::set_useWorldSpace(System.Boolean)
		void Register_UnityEngine_ParticleEmitter_set_useWorldSpace();
		Register_UnityEngine_ParticleEmitter_set_useWorldSpace();

		//UnityEngine.Particle[] UnityEngine.ParticleEmitter::get_particles()
		void Register_UnityEngine_ParticleEmitter_get_particles();
		Register_UnityEngine_ParticleEmitter_get_particles();

	//End Registrations for type : UnityEngine.ParticleEmitter

	//Start Registrations for type : UnityEngine.ParticleRenderer

		//System.Int32 UnityEngine.ParticleRenderer::get_uvAnimationXTile()
		void Register_UnityEngine_ParticleRenderer_get_uvAnimationXTile();
		Register_UnityEngine_ParticleRenderer_get_uvAnimationXTile();

		//System.Int32 UnityEngine.ParticleRenderer::get_uvAnimationYTile()
		void Register_UnityEngine_ParticleRenderer_get_uvAnimationYTile();
		Register_UnityEngine_ParticleRenderer_get_uvAnimationYTile();

		//System.Single UnityEngine.ParticleRenderer::get_cameraVelocityScale()
		void Register_UnityEngine_ParticleRenderer_get_cameraVelocityScale();
		Register_UnityEngine_ParticleRenderer_get_cameraVelocityScale();

		//System.Single UnityEngine.ParticleRenderer::get_lengthScale()
		void Register_UnityEngine_ParticleRenderer_get_lengthScale();
		Register_UnityEngine_ParticleRenderer_get_lengthScale();

		//System.Single UnityEngine.ParticleRenderer::get_maxParticleSize()
		void Register_UnityEngine_ParticleRenderer_get_maxParticleSize();
		Register_UnityEngine_ParticleRenderer_get_maxParticleSize();

		//System.Single UnityEngine.ParticleRenderer::get_uvAnimationCycles()
		void Register_UnityEngine_ParticleRenderer_get_uvAnimationCycles();
		Register_UnityEngine_ParticleRenderer_get_uvAnimationCycles();

		//System.Single UnityEngine.ParticleRenderer::get_velocityScale()
		void Register_UnityEngine_ParticleRenderer_get_velocityScale();
		Register_UnityEngine_ParticleRenderer_get_velocityScale();

		//System.Void UnityEngine.ParticleRenderer::set_cameraVelocityScale(System.Single)
		void Register_UnityEngine_ParticleRenderer_set_cameraVelocityScale();
		Register_UnityEngine_ParticleRenderer_set_cameraVelocityScale();

		//System.Void UnityEngine.ParticleRenderer::set_lengthScale(System.Single)
		void Register_UnityEngine_ParticleRenderer_set_lengthScale();
		Register_UnityEngine_ParticleRenderer_set_lengthScale();

		//System.Void UnityEngine.ParticleRenderer::set_maxParticleSize(System.Single)
		void Register_UnityEngine_ParticleRenderer_set_maxParticleSize();
		Register_UnityEngine_ParticleRenderer_set_maxParticleSize();

		//System.Void UnityEngine.ParticleRenderer::set_particleRenderMode(UnityEngine.ParticleRenderMode)
		void Register_UnityEngine_ParticleRenderer_set_particleRenderMode();
		Register_UnityEngine_ParticleRenderer_set_particleRenderMode();

		//System.Void UnityEngine.ParticleRenderer::set_uvAnimationCycles(System.Single)
		void Register_UnityEngine_ParticleRenderer_set_uvAnimationCycles();
		Register_UnityEngine_ParticleRenderer_set_uvAnimationCycles();

		//System.Void UnityEngine.ParticleRenderer::set_uvAnimationXTile(System.Int32)
		void Register_UnityEngine_ParticleRenderer_set_uvAnimationXTile();
		Register_UnityEngine_ParticleRenderer_set_uvAnimationXTile();

		//System.Void UnityEngine.ParticleRenderer::set_uvAnimationYTile(System.Int32)
		void Register_UnityEngine_ParticleRenderer_set_uvAnimationYTile();
		Register_UnityEngine_ParticleRenderer_set_uvAnimationYTile();

		//System.Void UnityEngine.ParticleRenderer::set_uvTiles(UnityEngine.Rect[])
		void Register_UnityEngine_ParticleRenderer_set_uvTiles();
		Register_UnityEngine_ParticleRenderer_set_uvTiles();

		//System.Void UnityEngine.ParticleRenderer::set_velocityScale(System.Single)
		void Register_UnityEngine_ParticleRenderer_set_velocityScale();
		Register_UnityEngine_ParticleRenderer_set_velocityScale();

		//UnityEngine.ParticleRenderMode UnityEngine.ParticleRenderer::get_particleRenderMode()
		void Register_UnityEngine_ParticleRenderer_get_particleRenderMode();
		Register_UnityEngine_ParticleRenderer_get_particleRenderMode();

		//UnityEngine.Rect[] UnityEngine.ParticleRenderer::get_uvTiles()
		void Register_UnityEngine_ParticleRenderer_get_uvTiles();
		Register_UnityEngine_ParticleRenderer_get_uvTiles();

	//End Registrations for type : UnityEngine.ParticleRenderer

	//Start Registrations for type : UnityEngine.ParticleSystem

		//System.Boolean UnityEngine.ParticleSystem::Internal_Clear(UnityEngine.ParticleSystem)
		void Register_UnityEngine_ParticleSystem_Internal_Clear();
		Register_UnityEngine_ParticleSystem_Internal_Clear();

		//System.Boolean UnityEngine.ParticleSystem::Internal_IsAlive(UnityEngine.ParticleSystem)
		void Register_UnityEngine_ParticleSystem_Internal_IsAlive();
		Register_UnityEngine_ParticleSystem_Internal_IsAlive();

		//System.Boolean UnityEngine.ParticleSystem::Internal_Pause(UnityEngine.ParticleSystem)
		void Register_UnityEngine_ParticleSystem_Internal_Pause();
		Register_UnityEngine_ParticleSystem_Internal_Pause();

		//System.Boolean UnityEngine.ParticleSystem::Internal_Play(UnityEngine.ParticleSystem)
		void Register_UnityEngine_ParticleSystem_Internal_Play();
		Register_UnityEngine_ParticleSystem_Internal_Play();

		//System.Boolean UnityEngine.ParticleSystem::Internal_Simulate(UnityEngine.ParticleSystem,System.Single,System.Boolean)
		void Register_UnityEngine_ParticleSystem_Internal_Simulate();
		Register_UnityEngine_ParticleSystem_Internal_Simulate();

		//System.Boolean UnityEngine.ParticleSystem::Internal_Stop(UnityEngine.ParticleSystem)
		void Register_UnityEngine_ParticleSystem_Internal_Stop();
		Register_UnityEngine_ParticleSystem_Internal_Stop();

		//System.Boolean UnityEngine.ParticleSystem::get_isPaused()
		void Register_UnityEngine_ParticleSystem_get_isPaused();
		Register_UnityEngine_ParticleSystem_get_isPaused();

		//System.Boolean UnityEngine.ParticleSystem::get_isPlaying()
		void Register_UnityEngine_ParticleSystem_get_isPlaying();
		Register_UnityEngine_ParticleSystem_get_isPlaying();

		//System.Boolean UnityEngine.ParticleSystem::get_isStopped()
		void Register_UnityEngine_ParticleSystem_get_isStopped();
		Register_UnityEngine_ParticleSystem_get_isStopped();

		//System.Boolean UnityEngine.ParticleSystem::get_loop()
		void Register_UnityEngine_ParticleSystem_get_loop();
		Register_UnityEngine_ParticleSystem_get_loop();

		//System.Boolean UnityEngine.ParticleSystem::get_playOnAwake()
		void Register_UnityEngine_ParticleSystem_get_playOnAwake();
		Register_UnityEngine_ParticleSystem_get_playOnAwake();

		//System.Int32 UnityEngine.ParticleSystem::GetParticles(UnityEngine.ParticleSystem/Particle[])
		void Register_UnityEngine_ParticleSystem_GetParticles();
		Register_UnityEngine_ParticleSystem_GetParticles();

		//System.Int32 UnityEngine.ParticleSystem::get_maxParticles()
		void Register_UnityEngine_ParticleSystem_get_maxParticles();
		Register_UnityEngine_ParticleSystem_get_maxParticles();

		//System.Int32 UnityEngine.ParticleSystem::get_particleCount()
		void Register_UnityEngine_ParticleSystem_get_particleCount();
		Register_UnityEngine_ParticleSystem_get_particleCount();

		//System.Single UnityEngine.ParticleSystem::get_duration()
		void Register_UnityEngine_ParticleSystem_get_duration();
		Register_UnityEngine_ParticleSystem_get_duration();

		//System.Single UnityEngine.ParticleSystem::get_gravityModifier()
		void Register_UnityEngine_ParticleSystem_get_gravityModifier();
		Register_UnityEngine_ParticleSystem_get_gravityModifier();

		//System.Single UnityEngine.ParticleSystem::get_playbackSpeed()
		void Register_UnityEngine_ParticleSystem_get_playbackSpeed();
		Register_UnityEngine_ParticleSystem_get_playbackSpeed();

		//System.Single UnityEngine.ParticleSystem::get_startDelay()
		void Register_UnityEngine_ParticleSystem_get_startDelay();
		Register_UnityEngine_ParticleSystem_get_startDelay();

		//System.Single UnityEngine.ParticleSystem::get_startLifetime()
		void Register_UnityEngine_ParticleSystem_get_startLifetime();
		Register_UnityEngine_ParticleSystem_get_startLifetime();

		//System.Single UnityEngine.ParticleSystem::get_startRotation()
		void Register_UnityEngine_ParticleSystem_get_startRotation();
		Register_UnityEngine_ParticleSystem_get_startRotation();

		//System.Single UnityEngine.ParticleSystem::get_startSize()
		void Register_UnityEngine_ParticleSystem_get_startSize();
		Register_UnityEngine_ParticleSystem_get_startSize();

		//System.Single UnityEngine.ParticleSystem::get_startSpeed()
		void Register_UnityEngine_ParticleSystem_get_startSpeed();
		Register_UnityEngine_ParticleSystem_get_startSpeed();

		//System.Single UnityEngine.ParticleSystem::get_time()
		void Register_UnityEngine_ParticleSystem_get_time();
		Register_UnityEngine_ParticleSystem_get_time();

		//System.UInt32 UnityEngine.ParticleSystem::get_randomSeed()
		void Register_UnityEngine_ParticleSystem_get_randomSeed();
		Register_UnityEngine_ParticleSystem_get_randomSeed();

		//System.Void UnityEngine.ParticleSystem::INTERNAL_CALL_Emit(UnityEngine.ParticleSystem,System.Int32)
		void Register_UnityEngine_ParticleSystem_INTERNAL_CALL_Emit();
		Register_UnityEngine_ParticleSystem_INTERNAL_CALL_Emit();

		//System.Void UnityEngine.ParticleSystem::INTERNAL_get_startColor(UnityEngine.Color&)
		void Register_UnityEngine_ParticleSystem_INTERNAL_get_startColor();
		Register_UnityEngine_ParticleSystem_INTERNAL_get_startColor();

		//System.Void UnityEngine.ParticleSystem::INTERNAL_get_startRotation3D(UnityEngine.Vector3&)
		void Register_UnityEngine_ParticleSystem_INTERNAL_get_startRotation3D();
		Register_UnityEngine_ParticleSystem_INTERNAL_get_startRotation3D();

		//System.Void UnityEngine.ParticleSystem::INTERNAL_set_startColor(UnityEngine.Color&)
		void Register_UnityEngine_ParticleSystem_INTERNAL_set_startColor();
		Register_UnityEngine_ParticleSystem_INTERNAL_set_startColor();

		//System.Void UnityEngine.ParticleSystem::INTERNAL_set_startRotation3D(UnityEngine.Vector3&)
		void Register_UnityEngine_ParticleSystem_INTERNAL_set_startRotation3D();
		Register_UnityEngine_ParticleSystem_INTERNAL_set_startRotation3D();

		//System.Void UnityEngine.ParticleSystem::Internal_Emit(UnityEngine.ParticleSystem/EmitParams&,System.Int32)
		void Register_UnityEngine_ParticleSystem_Internal_Emit();
		Register_UnityEngine_ParticleSystem_Internal_Emit();

		//System.Void UnityEngine.ParticleSystem::SetParticles(UnityEngine.ParticleSystem/Particle[],System.Int32)
		void Register_UnityEngine_ParticleSystem_SetParticles();
		Register_UnityEngine_ParticleSystem_SetParticles();

		//System.Void UnityEngine.ParticleSystem::set_gravityModifier(System.Single)
		void Register_UnityEngine_ParticleSystem_set_gravityModifier();
		Register_UnityEngine_ParticleSystem_set_gravityModifier();

		//System.Void UnityEngine.ParticleSystem::set_loop(System.Boolean)
		void Register_UnityEngine_ParticleSystem_set_loop();
		Register_UnityEngine_ParticleSystem_set_loop();

		//System.Void UnityEngine.ParticleSystem::set_maxParticles(System.Int32)
		void Register_UnityEngine_ParticleSystem_set_maxParticles();
		Register_UnityEngine_ParticleSystem_set_maxParticles();

		//System.Void UnityEngine.ParticleSystem::set_playOnAwake(System.Boolean)
		void Register_UnityEngine_ParticleSystem_set_playOnAwake();
		Register_UnityEngine_ParticleSystem_set_playOnAwake();

		//System.Void UnityEngine.ParticleSystem::set_playbackSpeed(System.Single)
		void Register_UnityEngine_ParticleSystem_set_playbackSpeed();
		Register_UnityEngine_ParticleSystem_set_playbackSpeed();

		//System.Void UnityEngine.ParticleSystem::set_randomSeed(System.UInt32)
		void Register_UnityEngine_ParticleSystem_set_randomSeed();
		Register_UnityEngine_ParticleSystem_set_randomSeed();

		//System.Void UnityEngine.ParticleSystem::set_scalingMode(UnityEngine.ParticleSystemScalingMode)
		void Register_UnityEngine_ParticleSystem_set_scalingMode();
		Register_UnityEngine_ParticleSystem_set_scalingMode();

		//System.Void UnityEngine.ParticleSystem::set_simulationSpace(UnityEngine.ParticleSystemSimulationSpace)
		void Register_UnityEngine_ParticleSystem_set_simulationSpace();
		Register_UnityEngine_ParticleSystem_set_simulationSpace();

		//System.Void UnityEngine.ParticleSystem::set_startDelay(System.Single)
		void Register_UnityEngine_ParticleSystem_set_startDelay();
		Register_UnityEngine_ParticleSystem_set_startDelay();

		//System.Void UnityEngine.ParticleSystem::set_startLifetime(System.Single)
		void Register_UnityEngine_ParticleSystem_set_startLifetime();
		Register_UnityEngine_ParticleSystem_set_startLifetime();

		//System.Void UnityEngine.ParticleSystem::set_startRotation(System.Single)
		void Register_UnityEngine_ParticleSystem_set_startRotation();
		Register_UnityEngine_ParticleSystem_set_startRotation();

		//System.Void UnityEngine.ParticleSystem::set_startSize(System.Single)
		void Register_UnityEngine_ParticleSystem_set_startSize();
		Register_UnityEngine_ParticleSystem_set_startSize();

		//System.Void UnityEngine.ParticleSystem::set_startSpeed(System.Single)
		void Register_UnityEngine_ParticleSystem_set_startSpeed();
		Register_UnityEngine_ParticleSystem_set_startSpeed();

		//System.Void UnityEngine.ParticleSystem::set_time(System.Single)
		void Register_UnityEngine_ParticleSystem_set_time();
		Register_UnityEngine_ParticleSystem_set_time();

		//UnityEngine.ParticleSystemScalingMode UnityEngine.ParticleSystem::get_scalingMode()
		void Register_UnityEngine_ParticleSystem_get_scalingMode();
		Register_UnityEngine_ParticleSystem_get_scalingMode();

		//UnityEngine.ParticleSystemSimulationSpace UnityEngine.ParticleSystem::get_simulationSpace()
		void Register_UnityEngine_ParticleSystem_get_simulationSpace();
		Register_UnityEngine_ParticleSystem_get_simulationSpace();

	//End Registrations for type : UnityEngine.ParticleSystem

	//Start Registrations for type : UnityEngine.ParticleSystemRenderer

		//System.Single UnityEngine.ParticleSystemRenderer::get_cameraVelocityScale()
		void Register_UnityEngine_ParticleSystemRenderer_get_cameraVelocityScale();
		Register_UnityEngine_ParticleSystemRenderer_get_cameraVelocityScale();

		//System.Single UnityEngine.ParticleSystemRenderer::get_lengthScale()
		void Register_UnityEngine_ParticleSystemRenderer_get_lengthScale();
		Register_UnityEngine_ParticleSystemRenderer_get_lengthScale();

		//System.Single UnityEngine.ParticleSystemRenderer::get_maxParticleSize()
		void Register_UnityEngine_ParticleSystemRenderer_get_maxParticleSize();
		Register_UnityEngine_ParticleSystemRenderer_get_maxParticleSize();

		//System.Single UnityEngine.ParticleSystemRenderer::get_minParticleSize()
		void Register_UnityEngine_ParticleSystemRenderer_get_minParticleSize();
		Register_UnityEngine_ParticleSystemRenderer_get_minParticleSize();

		//System.Single UnityEngine.ParticleSystemRenderer::get_normalDirection()
		void Register_UnityEngine_ParticleSystemRenderer_get_normalDirection();
		Register_UnityEngine_ParticleSystemRenderer_get_normalDirection();

		//System.Single UnityEngine.ParticleSystemRenderer::get_sortingFudge()
		void Register_UnityEngine_ParticleSystemRenderer_get_sortingFudge();
		Register_UnityEngine_ParticleSystemRenderer_get_sortingFudge();

		//System.Single UnityEngine.ParticleSystemRenderer::get_velocityScale()
		void Register_UnityEngine_ParticleSystemRenderer_get_velocityScale();
		Register_UnityEngine_ParticleSystemRenderer_get_velocityScale();

		//System.Void UnityEngine.ParticleSystemRenderer::INTERNAL_get_pivot(UnityEngine.Vector3&)
		void Register_UnityEngine_ParticleSystemRenderer_INTERNAL_get_pivot();
		Register_UnityEngine_ParticleSystemRenderer_INTERNAL_get_pivot();

		//System.Void UnityEngine.ParticleSystemRenderer::INTERNAL_set_pivot(UnityEngine.Vector3&)
		void Register_UnityEngine_ParticleSystemRenderer_INTERNAL_set_pivot();
		Register_UnityEngine_ParticleSystemRenderer_INTERNAL_set_pivot();

		//System.Void UnityEngine.ParticleSystemRenderer::set_alignment(UnityEngine.ParticleSystemRenderSpace)
		void Register_UnityEngine_ParticleSystemRenderer_set_alignment();
		Register_UnityEngine_ParticleSystemRenderer_set_alignment();

		//System.Void UnityEngine.ParticleSystemRenderer::set_cameraVelocityScale(System.Single)
		void Register_UnityEngine_ParticleSystemRenderer_set_cameraVelocityScale();
		Register_UnityEngine_ParticleSystemRenderer_set_cameraVelocityScale();

		//System.Void UnityEngine.ParticleSystemRenderer::set_lengthScale(System.Single)
		void Register_UnityEngine_ParticleSystemRenderer_set_lengthScale();
		Register_UnityEngine_ParticleSystemRenderer_set_lengthScale();

		//System.Void UnityEngine.ParticleSystemRenderer::set_maxParticleSize(System.Single)
		void Register_UnityEngine_ParticleSystemRenderer_set_maxParticleSize();
		Register_UnityEngine_ParticleSystemRenderer_set_maxParticleSize();

		//System.Void UnityEngine.ParticleSystemRenderer::set_mesh(UnityEngine.Mesh)
		void Register_UnityEngine_ParticleSystemRenderer_set_mesh();
		Register_UnityEngine_ParticleSystemRenderer_set_mesh();

		//System.Void UnityEngine.ParticleSystemRenderer::set_minParticleSize(System.Single)
		void Register_UnityEngine_ParticleSystemRenderer_set_minParticleSize();
		Register_UnityEngine_ParticleSystemRenderer_set_minParticleSize();

		//System.Void UnityEngine.ParticleSystemRenderer::set_normalDirection(System.Single)
		void Register_UnityEngine_ParticleSystemRenderer_set_normalDirection();
		Register_UnityEngine_ParticleSystemRenderer_set_normalDirection();

		//System.Void UnityEngine.ParticleSystemRenderer::set_renderMode(UnityEngine.ParticleSystemRenderMode)
		void Register_UnityEngine_ParticleSystemRenderer_set_renderMode();
		Register_UnityEngine_ParticleSystemRenderer_set_renderMode();

		//System.Void UnityEngine.ParticleSystemRenderer::set_sortMode(UnityEngine.ParticleSystemSortMode)
		void Register_UnityEngine_ParticleSystemRenderer_set_sortMode();
		Register_UnityEngine_ParticleSystemRenderer_set_sortMode();

		//System.Void UnityEngine.ParticleSystemRenderer::set_sortingFudge(System.Single)
		void Register_UnityEngine_ParticleSystemRenderer_set_sortingFudge();
		Register_UnityEngine_ParticleSystemRenderer_set_sortingFudge();

		//System.Void UnityEngine.ParticleSystemRenderer::set_velocityScale(System.Single)
		void Register_UnityEngine_ParticleSystemRenderer_set_velocityScale();
		Register_UnityEngine_ParticleSystemRenderer_set_velocityScale();

		//UnityEngine.Mesh UnityEngine.ParticleSystemRenderer::get_mesh()
		void Register_UnityEngine_ParticleSystemRenderer_get_mesh();
		Register_UnityEngine_ParticleSystemRenderer_get_mesh();

		//UnityEngine.ParticleSystemRenderMode UnityEngine.ParticleSystemRenderer::get_renderMode()
		void Register_UnityEngine_ParticleSystemRenderer_get_renderMode();
		Register_UnityEngine_ParticleSystemRenderer_get_renderMode();

		//UnityEngine.ParticleSystemRenderSpace UnityEngine.ParticleSystemRenderer::get_alignment()
		void Register_UnityEngine_ParticleSystemRenderer_get_alignment();
		Register_UnityEngine_ParticleSystemRenderer_get_alignment();

		//UnityEngine.ParticleSystemSortMode UnityEngine.ParticleSystemRenderer::get_sortMode()
		void Register_UnityEngine_ParticleSystemRenderer_get_sortMode();
		Register_UnityEngine_ParticleSystemRenderer_get_sortMode();

	//End Registrations for type : UnityEngine.ParticleSystemRenderer

	//Start Registrations for type : UnityEngine.PhysicMaterial

		//System.Single UnityEngine.PhysicMaterial::get_bounciness()
		void Register_UnityEngine_PhysicMaterial_get_bounciness();
		Register_UnityEngine_PhysicMaterial_get_bounciness();

		//System.Single UnityEngine.PhysicMaterial::get_dynamicFriction()
		void Register_UnityEngine_PhysicMaterial_get_dynamicFriction();
		Register_UnityEngine_PhysicMaterial_get_dynamicFriction();

		//System.Single UnityEngine.PhysicMaterial::get_staticFriction()
		void Register_UnityEngine_PhysicMaterial_get_staticFriction();
		Register_UnityEngine_PhysicMaterial_get_staticFriction();

		//System.Void UnityEngine.PhysicMaterial::Internal_CreateDynamicsMaterial(UnityEngine.PhysicMaterial,System.String)
		void Register_UnityEngine_PhysicMaterial_Internal_CreateDynamicsMaterial();
		Register_UnityEngine_PhysicMaterial_Internal_CreateDynamicsMaterial();

		//System.Void UnityEngine.PhysicMaterial::set_bounceCombine(UnityEngine.PhysicMaterialCombine)
		void Register_UnityEngine_PhysicMaterial_set_bounceCombine();
		Register_UnityEngine_PhysicMaterial_set_bounceCombine();

		//System.Void UnityEngine.PhysicMaterial::set_bounciness(System.Single)
		void Register_UnityEngine_PhysicMaterial_set_bounciness();
		Register_UnityEngine_PhysicMaterial_set_bounciness();

		//System.Void UnityEngine.PhysicMaterial::set_dynamicFriction(System.Single)
		void Register_UnityEngine_PhysicMaterial_set_dynamicFriction();
		Register_UnityEngine_PhysicMaterial_set_dynamicFriction();

		//System.Void UnityEngine.PhysicMaterial::set_frictionCombine(UnityEngine.PhysicMaterialCombine)
		void Register_UnityEngine_PhysicMaterial_set_frictionCombine();
		Register_UnityEngine_PhysicMaterial_set_frictionCombine();

		//System.Void UnityEngine.PhysicMaterial::set_staticFriction(System.Single)
		void Register_UnityEngine_PhysicMaterial_set_staticFriction();
		Register_UnityEngine_PhysicMaterial_set_staticFriction();

		//UnityEngine.PhysicMaterialCombine UnityEngine.PhysicMaterial::get_bounceCombine()
		void Register_UnityEngine_PhysicMaterial_get_bounceCombine();
		Register_UnityEngine_PhysicMaterial_get_bounceCombine();

		//UnityEngine.PhysicMaterialCombine UnityEngine.PhysicMaterial::get_frictionCombine()
		void Register_UnityEngine_PhysicMaterial_get_frictionCombine();
		Register_UnityEngine_PhysicMaterial_get_frictionCombine();

	//End Registrations for type : UnityEngine.PhysicMaterial

	//Start Registrations for type : UnityEngine.Physics

		//System.Boolean UnityEngine.Physics::GetIgnoreLayerCollision(System.Int32,System.Int32)
		void Register_UnityEngine_Physics_GetIgnoreLayerCollision();
		Register_UnityEngine_Physics_GetIgnoreLayerCollision();

		//System.Boolean UnityEngine.Physics::INTERNAL_CALL_CheckBox(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Quaternion&,System.Int32,UnityEngine.QueryTriggerInteraction)
		void Register_UnityEngine_Physics_INTERNAL_CALL_CheckBox();
		Register_UnityEngine_Physics_INTERNAL_CALL_CheckBox();

		//System.Boolean UnityEngine.Physics::INTERNAL_CALL_CheckCapsule(UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
		void Register_UnityEngine_Physics_INTERNAL_CALL_CheckCapsule();
		Register_UnityEngine_Physics_INTERNAL_CALL_CheckCapsule();

		//System.Boolean UnityEngine.Physics::INTERNAL_CALL_CheckSphere(UnityEngine.Vector3&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
		void Register_UnityEngine_Physics_INTERNAL_CALL_CheckSphere();
		Register_UnityEngine_Physics_INTERNAL_CALL_CheckSphere();

		//System.Boolean UnityEngine.Physics::INTERNAL_CALL_Internal_BoxCast(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Quaternion&,UnityEngine.Vector3&,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
		void Register_UnityEngine_Physics_INTERNAL_CALL_Internal_BoxCast();
		Register_UnityEngine_Physics_INTERNAL_CALL_Internal_BoxCast();

		//System.Boolean UnityEngine.Physics::INTERNAL_CALL_Internal_CapsuleCast(UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,UnityEngine.Vector3&,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
		void Register_UnityEngine_Physics_INTERNAL_CALL_Internal_CapsuleCast();
		Register_UnityEngine_Physics_INTERNAL_CALL_Internal_CapsuleCast();

		//System.Boolean UnityEngine.Physics::INTERNAL_CALL_Internal_Raycast(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
		void Register_UnityEngine_Physics_INTERNAL_CALL_Internal_Raycast();
		Register_UnityEngine_Physics_INTERNAL_CALL_Internal_Raycast();

		//System.Boolean UnityEngine.Physics::INTERNAL_CALL_Internal_RaycastTest(UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
		void Register_UnityEngine_Physics_INTERNAL_CALL_Internal_RaycastTest();
		Register_UnityEngine_Physics_INTERNAL_CALL_Internal_RaycastTest();

		//System.Boolean UnityEngine.Physics::get_queriesHitTriggers()
		void Register_UnityEngine_Physics_get_queriesHitTriggers();
		Register_UnityEngine_Physics_get_queriesHitTriggers();

		//System.Int32 UnityEngine.Physics::INTERNAL_CALL_BoxCastNonAlloc(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.RaycastHit[],UnityEngine.Quaternion&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
		void Register_UnityEngine_Physics_INTERNAL_CALL_BoxCastNonAlloc();
		Register_UnityEngine_Physics_INTERNAL_CALL_BoxCastNonAlloc();

		//System.Int32 UnityEngine.Physics::INTERNAL_CALL_CapsuleCastNonAlloc(UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,UnityEngine.Vector3&,UnityEngine.RaycastHit[],System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
		void Register_UnityEngine_Physics_INTERNAL_CALL_CapsuleCastNonAlloc();
		Register_UnityEngine_Physics_INTERNAL_CALL_CapsuleCastNonAlloc();

		//System.Int32 UnityEngine.Physics::INTERNAL_CALL_OverlapBoxNonAlloc(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Collider[],UnityEngine.Quaternion&,System.Int32,UnityEngine.QueryTriggerInteraction)
		void Register_UnityEngine_Physics_INTERNAL_CALL_OverlapBoxNonAlloc();
		Register_UnityEngine_Physics_INTERNAL_CALL_OverlapBoxNonAlloc();

		//System.Int32 UnityEngine.Physics::INTERNAL_CALL_OverlapSphereNonAlloc(UnityEngine.Vector3&,System.Single,UnityEngine.Collider[],System.Int32,UnityEngine.QueryTriggerInteraction)
		void Register_UnityEngine_Physics_INTERNAL_CALL_OverlapSphereNonAlloc();
		Register_UnityEngine_Physics_INTERNAL_CALL_OverlapSphereNonAlloc();

		//System.Int32 UnityEngine.Physics::INTERNAL_CALL_RaycastNonAlloc(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.RaycastHit[],System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
		void Register_UnityEngine_Physics_INTERNAL_CALL_RaycastNonAlloc();
		Register_UnityEngine_Physics_INTERNAL_CALL_RaycastNonAlloc();

		//System.Int32 UnityEngine.Physics::get_solverIterationCount()
		void Register_UnityEngine_Physics_get_solverIterationCount();
		Register_UnityEngine_Physics_get_solverIterationCount();

		//System.Single UnityEngine.Physics::get_bounceThreshold()
		void Register_UnityEngine_Physics_get_bounceThreshold();
		Register_UnityEngine_Physics_get_bounceThreshold();

		//System.Single UnityEngine.Physics::get_defaultContactOffset()
		void Register_UnityEngine_Physics_get_defaultContactOffset();
		Register_UnityEngine_Physics_get_defaultContactOffset();

		//System.Single UnityEngine.Physics::get_sleepThreshold()
		void Register_UnityEngine_Physics_get_sleepThreshold();
		Register_UnityEngine_Physics_get_sleepThreshold();

		//System.Void UnityEngine.Physics::INTERNAL_get_gravity(UnityEngine.Vector3&)
		void Register_UnityEngine_Physics_INTERNAL_get_gravity();
		Register_UnityEngine_Physics_INTERNAL_get_gravity();

		//System.Void UnityEngine.Physics::INTERNAL_set_gravity(UnityEngine.Vector3&)
		void Register_UnityEngine_Physics_INTERNAL_set_gravity();
		Register_UnityEngine_Physics_INTERNAL_set_gravity();

		//System.Void UnityEngine.Physics::IgnoreCollision(UnityEngine.Collider,UnityEngine.Collider,System.Boolean)
		void Register_UnityEngine_Physics_IgnoreCollision();
		Register_UnityEngine_Physics_IgnoreCollision();

		//System.Void UnityEngine.Physics::IgnoreLayerCollision(System.Int32,System.Int32,System.Boolean)
		void Register_UnityEngine_Physics_IgnoreLayerCollision();
		Register_UnityEngine_Physics_IgnoreLayerCollision();

		//System.Void UnityEngine.Physics::set_bounceThreshold(System.Single)
		void Register_UnityEngine_Physics_set_bounceThreshold();
		Register_UnityEngine_Physics_set_bounceThreshold();

		//System.Void UnityEngine.Physics::set_defaultContactOffset(System.Single)
		void Register_UnityEngine_Physics_set_defaultContactOffset();
		Register_UnityEngine_Physics_set_defaultContactOffset();

		//System.Void UnityEngine.Physics::set_queriesHitTriggers(System.Boolean)
		void Register_UnityEngine_Physics_set_queriesHitTriggers();
		Register_UnityEngine_Physics_set_queriesHitTriggers();

		//System.Void UnityEngine.Physics::set_sleepThreshold(System.Single)
		void Register_UnityEngine_Physics_set_sleepThreshold();
		Register_UnityEngine_Physics_set_sleepThreshold();

		//System.Void UnityEngine.Physics::set_solverIterationCount(System.Int32)
		void Register_UnityEngine_Physics_set_solverIterationCount();
		Register_UnityEngine_Physics_set_solverIterationCount();

		//UnityEngine.Collider[] UnityEngine.Physics::INTERNAL_CALL_OverlapBox(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Quaternion&,System.Int32,UnityEngine.QueryTriggerInteraction)
		void Register_UnityEngine_Physics_INTERNAL_CALL_OverlapBox();
		Register_UnityEngine_Physics_INTERNAL_CALL_OverlapBox();

		//UnityEngine.Collider[] UnityEngine.Physics::INTERNAL_CALL_OverlapSphere(UnityEngine.Vector3&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
		void Register_UnityEngine_Physics_INTERNAL_CALL_OverlapSphere();
		Register_UnityEngine_Physics_INTERNAL_CALL_OverlapSphere();

		//UnityEngine.RaycastHit[] UnityEngine.Physics::INTERNAL_CALL_BoxCastAll(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Quaternion&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
		void Register_UnityEngine_Physics_INTERNAL_CALL_BoxCastAll();
		Register_UnityEngine_Physics_INTERNAL_CALL_BoxCastAll();

		//UnityEngine.RaycastHit[] UnityEngine.Physics::INTERNAL_CALL_CapsuleCastAll(UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,UnityEngine.Vector3&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
		void Register_UnityEngine_Physics_INTERNAL_CALL_CapsuleCastAll();
		Register_UnityEngine_Physics_INTERNAL_CALL_CapsuleCastAll();

		//UnityEngine.RaycastHit[] UnityEngine.Physics::INTERNAL_CALL_RaycastAll(UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
		void Register_UnityEngine_Physics_INTERNAL_CALL_RaycastAll();
		Register_UnityEngine_Physics_INTERNAL_CALL_RaycastAll();

	//End Registrations for type : UnityEngine.Physics

	//Start Registrations for type : UnityEngine.Physics2D

		//System.Boolean UnityEngine.Physics2D::GetIgnoreCollision(UnityEngine.Collider2D,UnityEngine.Collider2D)
		void Register_UnityEngine_Physics2D_GetIgnoreCollision();
		Register_UnityEngine_Physics2D_GetIgnoreCollision();

		//System.Boolean UnityEngine.Physics2D::GetIgnoreLayerCollision(System.Int32,System.Int32)
		void Register_UnityEngine_Physics2D_GetIgnoreLayerCollision();
		Register_UnityEngine_Physics2D_GetIgnoreLayerCollision();

		//System.Boolean UnityEngine.Physics2D::IsTouching(UnityEngine.Collider2D,UnityEngine.Collider2D)
		void Register_UnityEngine_Physics2D_IsTouching();
		Register_UnityEngine_Physics2D_IsTouching();

		//System.Boolean UnityEngine.Physics2D::IsTouchingLayers(UnityEngine.Collider2D,System.Int32)
		void Register_UnityEngine_Physics2D_IsTouchingLayers();
		Register_UnityEngine_Physics2D_IsTouchingLayers();

		//System.Boolean UnityEngine.Physics2D::get_changeStopsCallbacks()
		void Register_UnityEngine_Physics2D_get_changeStopsCallbacks();
		Register_UnityEngine_Physics2D_get_changeStopsCallbacks();

		//System.Boolean UnityEngine.Physics2D::get_queriesHitTriggers()
		void Register_UnityEngine_Physics2D_get_queriesHitTriggers();
		Register_UnityEngine_Physics2D_get_queriesHitTriggers();

		//System.Boolean UnityEngine.Physics2D::get_queriesStartInColliders()
		void Register_UnityEngine_Physics2D_get_queriesStartInColliders();
		Register_UnityEngine_Physics2D_get_queriesStartInColliders();

		//System.Int32 UnityEngine.Physics2D::INTERNAL_CALL_BoxCastNonAlloc(UnityEngine.Vector2&,UnityEngine.Vector2&,System.Single,UnityEngine.Vector2&,UnityEngine.RaycastHit2D[],System.Single,System.Int32,System.Single,System.Single)
		void Register_UnityEngine_Physics2D_INTERNAL_CALL_BoxCastNonAlloc();
		Register_UnityEngine_Physics2D_INTERNAL_CALL_BoxCastNonAlloc();

		//System.Int32 UnityEngine.Physics2D::INTERNAL_CALL_CircleCastNonAlloc(UnityEngine.Vector2&,System.Single,UnityEngine.Vector2&,UnityEngine.RaycastHit2D[],System.Single,System.Int32,System.Single,System.Single)
		void Register_UnityEngine_Physics2D_INTERNAL_CALL_CircleCastNonAlloc();
		Register_UnityEngine_Physics2D_INTERNAL_CALL_CircleCastNonAlloc();

		//System.Int32 UnityEngine.Physics2D::INTERNAL_CALL_GetRayIntersectionNonAlloc(UnityEngine.Ray&,UnityEngine.RaycastHit2D[],System.Single,System.Int32)
		void Register_UnityEngine_Physics2D_INTERNAL_CALL_GetRayIntersectionNonAlloc();
		Register_UnityEngine_Physics2D_INTERNAL_CALL_GetRayIntersectionNonAlloc();

		//System.Int32 UnityEngine.Physics2D::INTERNAL_CALL_LinecastNonAlloc(UnityEngine.Vector2&,UnityEngine.Vector2&,UnityEngine.RaycastHit2D[],System.Int32,System.Single,System.Single)
		void Register_UnityEngine_Physics2D_INTERNAL_CALL_LinecastNonAlloc();
		Register_UnityEngine_Physics2D_INTERNAL_CALL_LinecastNonAlloc();

		//System.Int32 UnityEngine.Physics2D::INTERNAL_CALL_OverlapAreaNonAlloc(UnityEngine.Vector2&,UnityEngine.Vector2&,UnityEngine.Collider2D[],System.Int32,System.Single,System.Single)
		void Register_UnityEngine_Physics2D_INTERNAL_CALL_OverlapAreaNonAlloc();
		Register_UnityEngine_Physics2D_INTERNAL_CALL_OverlapAreaNonAlloc();

		//System.Int32 UnityEngine.Physics2D::INTERNAL_CALL_OverlapCircleNonAlloc(UnityEngine.Vector2&,System.Single,UnityEngine.Collider2D[],System.Int32,System.Single,System.Single)
		void Register_UnityEngine_Physics2D_INTERNAL_CALL_OverlapCircleNonAlloc();
		Register_UnityEngine_Physics2D_INTERNAL_CALL_OverlapCircleNonAlloc();

		//System.Int32 UnityEngine.Physics2D::INTERNAL_CALL_OverlapPointNonAlloc(UnityEngine.Vector2&,UnityEngine.Collider2D[],System.Int32,System.Single,System.Single)
		void Register_UnityEngine_Physics2D_INTERNAL_CALL_OverlapPointNonAlloc();
		Register_UnityEngine_Physics2D_INTERNAL_CALL_OverlapPointNonAlloc();

		//System.Int32 UnityEngine.Physics2D::INTERNAL_CALL_RaycastNonAlloc(UnityEngine.Vector2&,UnityEngine.Vector2&,UnityEngine.RaycastHit2D[],System.Single,System.Int32,System.Single,System.Single)
		void Register_UnityEngine_Physics2D_INTERNAL_CALL_RaycastNonAlloc();
		Register_UnityEngine_Physics2D_INTERNAL_CALL_RaycastNonAlloc();

		//System.Int32 UnityEngine.Physics2D::get_positionIterations()
		void Register_UnityEngine_Physics2D_get_positionIterations();
		Register_UnityEngine_Physics2D_get_positionIterations();

		//System.Int32 UnityEngine.Physics2D::get_velocityIterations()
		void Register_UnityEngine_Physics2D_get_velocityIterations();
		Register_UnityEngine_Physics2D_get_velocityIterations();

		//System.Single UnityEngine.Physics2D::get_angularSleepTolerance()
		void Register_UnityEngine_Physics2D_get_angularSleepTolerance();
		Register_UnityEngine_Physics2D_get_angularSleepTolerance();

		//System.Single UnityEngine.Physics2D::get_baumgarteScale()
		void Register_UnityEngine_Physics2D_get_baumgarteScale();
		Register_UnityEngine_Physics2D_get_baumgarteScale();

		//System.Single UnityEngine.Physics2D::get_baumgarteTOIScale()
		void Register_UnityEngine_Physics2D_get_baumgarteTOIScale();
		Register_UnityEngine_Physics2D_get_baumgarteTOIScale();

		//System.Single UnityEngine.Physics2D::get_linearSleepTolerance()
		void Register_UnityEngine_Physics2D_get_linearSleepTolerance();
		Register_UnityEngine_Physics2D_get_linearSleepTolerance();

		//System.Single UnityEngine.Physics2D::get_maxAngularCorrection()
		void Register_UnityEngine_Physics2D_get_maxAngularCorrection();
		Register_UnityEngine_Physics2D_get_maxAngularCorrection();

		//System.Single UnityEngine.Physics2D::get_maxLinearCorrection()
		void Register_UnityEngine_Physics2D_get_maxLinearCorrection();
		Register_UnityEngine_Physics2D_get_maxLinearCorrection();

		//System.Single UnityEngine.Physics2D::get_maxRotationSpeed()
		void Register_UnityEngine_Physics2D_get_maxRotationSpeed();
		Register_UnityEngine_Physics2D_get_maxRotationSpeed();

		//System.Single UnityEngine.Physics2D::get_maxTranslationSpeed()
		void Register_UnityEngine_Physics2D_get_maxTranslationSpeed();
		Register_UnityEngine_Physics2D_get_maxTranslationSpeed();

		//System.Single UnityEngine.Physics2D::get_minPenetrationForPenalty()
		void Register_UnityEngine_Physics2D_get_minPenetrationForPenalty();
		Register_UnityEngine_Physics2D_get_minPenetrationForPenalty();

		//System.Single UnityEngine.Physics2D::get_timeToSleep()
		void Register_UnityEngine_Physics2D_get_timeToSleep();
		Register_UnityEngine_Physics2D_get_timeToSleep();

		//System.Single UnityEngine.Physics2D::get_velocityThreshold()
		void Register_UnityEngine_Physics2D_get_velocityThreshold();
		Register_UnityEngine_Physics2D_get_velocityThreshold();

		//System.Void UnityEngine.Physics2D::INTERNAL_CALL_Internal_BoxCast(UnityEngine.Vector2&,UnityEngine.Vector2&,System.Single,UnityEngine.Vector2&,System.Single,System.Int32,System.Single,System.Single,UnityEngine.RaycastHit2D&)
		void Register_UnityEngine_Physics2D_INTERNAL_CALL_Internal_BoxCast();
		Register_UnityEngine_Physics2D_INTERNAL_CALL_Internal_BoxCast();

		//System.Void UnityEngine.Physics2D::INTERNAL_CALL_Internal_CircleCast(UnityEngine.Vector2&,System.Single,UnityEngine.Vector2&,System.Single,System.Int32,System.Single,System.Single,UnityEngine.RaycastHit2D&)
		void Register_UnityEngine_Physics2D_INTERNAL_CALL_Internal_CircleCast();
		Register_UnityEngine_Physics2D_INTERNAL_CALL_Internal_CircleCast();

		//System.Void UnityEngine.Physics2D::INTERNAL_CALL_Internal_GetRayIntersection(UnityEngine.Ray&,System.Single,System.Int32,UnityEngine.RaycastHit2D&)
		void Register_UnityEngine_Physics2D_INTERNAL_CALL_Internal_GetRayIntersection();
		Register_UnityEngine_Physics2D_INTERNAL_CALL_Internal_GetRayIntersection();

		//System.Void UnityEngine.Physics2D::INTERNAL_CALL_Internal_Linecast(UnityEngine.Vector2&,UnityEngine.Vector2&,System.Int32,System.Single,System.Single,UnityEngine.RaycastHit2D&)
		void Register_UnityEngine_Physics2D_INTERNAL_CALL_Internal_Linecast();
		Register_UnityEngine_Physics2D_INTERNAL_CALL_Internal_Linecast();

		//System.Void UnityEngine.Physics2D::INTERNAL_CALL_Internal_Raycast(UnityEngine.Vector2&,UnityEngine.Vector2&,System.Single,System.Int32,System.Single,System.Single,UnityEngine.RaycastHit2D&)
		void Register_UnityEngine_Physics2D_INTERNAL_CALL_Internal_Raycast();
		Register_UnityEngine_Physics2D_INTERNAL_CALL_Internal_Raycast();

		//System.Void UnityEngine.Physics2D::INTERNAL_get_gravity(UnityEngine.Vector2&)
		void Register_UnityEngine_Physics2D_INTERNAL_get_gravity();
		Register_UnityEngine_Physics2D_INTERNAL_get_gravity();

		//System.Void UnityEngine.Physics2D::INTERNAL_set_gravity(UnityEngine.Vector2&)
		void Register_UnityEngine_Physics2D_INTERNAL_set_gravity();
		Register_UnityEngine_Physics2D_INTERNAL_set_gravity();

		//System.Void UnityEngine.Physics2D::IgnoreCollision(UnityEngine.Collider2D,UnityEngine.Collider2D,System.Boolean)
		void Register_UnityEngine_Physics2D_IgnoreCollision();
		Register_UnityEngine_Physics2D_IgnoreCollision();

		//System.Void UnityEngine.Physics2D::IgnoreLayerCollision(System.Int32,System.Int32,System.Boolean)
		void Register_UnityEngine_Physics2D_IgnoreLayerCollision();
		Register_UnityEngine_Physics2D_IgnoreLayerCollision();

		//System.Void UnityEngine.Physics2D::set_angularSleepTolerance(System.Single)
		void Register_UnityEngine_Physics2D_set_angularSleepTolerance();
		Register_UnityEngine_Physics2D_set_angularSleepTolerance();

		//System.Void UnityEngine.Physics2D::set_baumgarteScale(System.Single)
		void Register_UnityEngine_Physics2D_set_baumgarteScale();
		Register_UnityEngine_Physics2D_set_baumgarteScale();

		//System.Void UnityEngine.Physics2D::set_baumgarteTOIScale(System.Single)
		void Register_UnityEngine_Physics2D_set_baumgarteTOIScale();
		Register_UnityEngine_Physics2D_set_baumgarteTOIScale();

		//System.Void UnityEngine.Physics2D::set_changeStopsCallbacks(System.Boolean)
		void Register_UnityEngine_Physics2D_set_changeStopsCallbacks();
		Register_UnityEngine_Physics2D_set_changeStopsCallbacks();

		//System.Void UnityEngine.Physics2D::set_linearSleepTolerance(System.Single)
		void Register_UnityEngine_Physics2D_set_linearSleepTolerance();
		Register_UnityEngine_Physics2D_set_linearSleepTolerance();

		//System.Void UnityEngine.Physics2D::set_maxAngularCorrection(System.Single)
		void Register_UnityEngine_Physics2D_set_maxAngularCorrection();
		Register_UnityEngine_Physics2D_set_maxAngularCorrection();

		//System.Void UnityEngine.Physics2D::set_maxLinearCorrection(System.Single)
		void Register_UnityEngine_Physics2D_set_maxLinearCorrection();
		Register_UnityEngine_Physics2D_set_maxLinearCorrection();

		//System.Void UnityEngine.Physics2D::set_maxRotationSpeed(System.Single)
		void Register_UnityEngine_Physics2D_set_maxRotationSpeed();
		Register_UnityEngine_Physics2D_set_maxRotationSpeed();

		//System.Void UnityEngine.Physics2D::set_maxTranslationSpeed(System.Single)
		void Register_UnityEngine_Physics2D_set_maxTranslationSpeed();
		Register_UnityEngine_Physics2D_set_maxTranslationSpeed();

		//System.Void UnityEngine.Physics2D::set_minPenetrationForPenalty(System.Single)
		void Register_UnityEngine_Physics2D_set_minPenetrationForPenalty();
		Register_UnityEngine_Physics2D_set_minPenetrationForPenalty();

		//System.Void UnityEngine.Physics2D::set_positionIterations(System.Int32)
		void Register_UnityEngine_Physics2D_set_positionIterations();
		Register_UnityEngine_Physics2D_set_positionIterations();

		//System.Void UnityEngine.Physics2D::set_queriesHitTriggers(System.Boolean)
		void Register_UnityEngine_Physics2D_set_queriesHitTriggers();
		Register_UnityEngine_Physics2D_set_queriesHitTriggers();

		//System.Void UnityEngine.Physics2D::set_queriesStartInColliders(System.Boolean)
		void Register_UnityEngine_Physics2D_set_queriesStartInColliders();
		Register_UnityEngine_Physics2D_set_queriesStartInColliders();

		//System.Void UnityEngine.Physics2D::set_timeToSleep(System.Single)
		void Register_UnityEngine_Physics2D_set_timeToSleep();
		Register_UnityEngine_Physics2D_set_timeToSleep();

		//System.Void UnityEngine.Physics2D::set_velocityIterations(System.Int32)
		void Register_UnityEngine_Physics2D_set_velocityIterations();
		Register_UnityEngine_Physics2D_set_velocityIterations();

		//System.Void UnityEngine.Physics2D::set_velocityThreshold(System.Single)
		void Register_UnityEngine_Physics2D_set_velocityThreshold();
		Register_UnityEngine_Physics2D_set_velocityThreshold();

		//UnityEngine.Collider2D UnityEngine.Physics2D::INTERNAL_CALL_OverlapArea(UnityEngine.Vector2&,UnityEngine.Vector2&,System.Int32,System.Single,System.Single)
		void Register_UnityEngine_Physics2D_INTERNAL_CALL_OverlapArea();
		Register_UnityEngine_Physics2D_INTERNAL_CALL_OverlapArea();

		//UnityEngine.Collider2D UnityEngine.Physics2D::INTERNAL_CALL_OverlapCircle(UnityEngine.Vector2&,System.Single,System.Int32,System.Single,System.Single)
		void Register_UnityEngine_Physics2D_INTERNAL_CALL_OverlapCircle();
		Register_UnityEngine_Physics2D_INTERNAL_CALL_OverlapCircle();

		//UnityEngine.Collider2D UnityEngine.Physics2D::INTERNAL_CALL_OverlapPoint(UnityEngine.Vector2&,System.Int32,System.Single,System.Single)
		void Register_UnityEngine_Physics2D_INTERNAL_CALL_OverlapPoint();
		Register_UnityEngine_Physics2D_INTERNAL_CALL_OverlapPoint();

		//UnityEngine.Collider2D[] UnityEngine.Physics2D::INTERNAL_CALL_OverlapAreaAll(UnityEngine.Vector2&,UnityEngine.Vector2&,System.Int32,System.Single,System.Single)
		void Register_UnityEngine_Physics2D_INTERNAL_CALL_OverlapAreaAll();
		Register_UnityEngine_Physics2D_INTERNAL_CALL_OverlapAreaAll();

		//UnityEngine.Collider2D[] UnityEngine.Physics2D::INTERNAL_CALL_OverlapCircleAll(UnityEngine.Vector2&,System.Single,System.Int32,System.Single,System.Single)
		void Register_UnityEngine_Physics2D_INTERNAL_CALL_OverlapCircleAll();
		Register_UnityEngine_Physics2D_INTERNAL_CALL_OverlapCircleAll();

		//UnityEngine.Collider2D[] UnityEngine.Physics2D::INTERNAL_CALL_OverlapPointAll(UnityEngine.Vector2&,System.Int32,System.Single,System.Single)
		void Register_UnityEngine_Physics2D_INTERNAL_CALL_OverlapPointAll();
		Register_UnityEngine_Physics2D_INTERNAL_CALL_OverlapPointAll();

		//UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::INTERNAL_CALL_BoxCastAll(UnityEngine.Vector2&,UnityEngine.Vector2&,System.Single,UnityEngine.Vector2&,System.Single,System.Int32,System.Single,System.Single)
		void Register_UnityEngine_Physics2D_INTERNAL_CALL_BoxCastAll();
		Register_UnityEngine_Physics2D_INTERNAL_CALL_BoxCastAll();

		//UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::INTERNAL_CALL_CircleCastAll(UnityEngine.Vector2&,System.Single,UnityEngine.Vector2&,System.Single,System.Int32,System.Single,System.Single)
		void Register_UnityEngine_Physics2D_INTERNAL_CALL_CircleCastAll();
		Register_UnityEngine_Physics2D_INTERNAL_CALL_CircleCastAll();

		//UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::INTERNAL_CALL_GetRayIntersectionAll(UnityEngine.Ray&,System.Single,System.Int32)
		void Register_UnityEngine_Physics2D_INTERNAL_CALL_GetRayIntersectionAll();
		Register_UnityEngine_Physics2D_INTERNAL_CALL_GetRayIntersectionAll();

		//UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::INTERNAL_CALL_LinecastAll(UnityEngine.Vector2&,UnityEngine.Vector2&,System.Int32,System.Single,System.Single)
		void Register_UnityEngine_Physics2D_INTERNAL_CALL_LinecastAll();
		Register_UnityEngine_Physics2D_INTERNAL_CALL_LinecastAll();

		//UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::INTERNAL_CALL_RaycastAll(UnityEngine.Vector2&,UnityEngine.Vector2&,System.Single,System.Int32,System.Single,System.Single)
		void Register_UnityEngine_Physics2D_INTERNAL_CALL_RaycastAll();
		Register_UnityEngine_Physics2D_INTERNAL_CALL_RaycastAll();

	//End Registrations for type : UnityEngine.Physics2D

	//Start Registrations for type : UnityEngine.PhysicsMaterial2D

		//System.Single UnityEngine.PhysicsMaterial2D::get_bounciness()
		void Register_UnityEngine_PhysicsMaterial2D_get_bounciness();
		Register_UnityEngine_PhysicsMaterial2D_get_bounciness();

		//System.Single UnityEngine.PhysicsMaterial2D::get_friction()
		void Register_UnityEngine_PhysicsMaterial2D_get_friction();
		Register_UnityEngine_PhysicsMaterial2D_get_friction();

		//System.Void UnityEngine.PhysicsMaterial2D::Internal_Create(UnityEngine.PhysicsMaterial2D,System.String)
		void Register_UnityEngine_PhysicsMaterial2D_Internal_Create();
		Register_UnityEngine_PhysicsMaterial2D_Internal_Create();

		//System.Void UnityEngine.PhysicsMaterial2D::set_bounciness(System.Single)
		void Register_UnityEngine_PhysicsMaterial2D_set_bounciness();
		Register_UnityEngine_PhysicsMaterial2D_set_bounciness();

		//System.Void UnityEngine.PhysicsMaterial2D::set_friction(System.Single)
		void Register_UnityEngine_PhysicsMaterial2D_set_friction();
		Register_UnityEngine_PhysicsMaterial2D_set_friction();

	//End Registrations for type : UnityEngine.PhysicsMaterial2D

	//Start Registrations for type : UnityEngine.PlayerPrefs

		//System.Boolean UnityEngine.PlayerPrefs::HasKey(System.String)
		void Register_UnityEngine_PlayerPrefs_HasKey();
		Register_UnityEngine_PlayerPrefs_HasKey();

		//System.Boolean UnityEngine.PlayerPrefs::TrySetFloat(System.String,System.Single)
		void Register_UnityEngine_PlayerPrefs_TrySetFloat();
		Register_UnityEngine_PlayerPrefs_TrySetFloat();

		//System.Boolean UnityEngine.PlayerPrefs::TrySetInt(System.String,System.Int32)
		void Register_UnityEngine_PlayerPrefs_TrySetInt();
		Register_UnityEngine_PlayerPrefs_TrySetInt();

		//System.Boolean UnityEngine.PlayerPrefs::TrySetSetString(System.String,System.String)
		void Register_UnityEngine_PlayerPrefs_TrySetSetString();
		Register_UnityEngine_PlayerPrefs_TrySetSetString();

		//System.Int32 UnityEngine.PlayerPrefs::GetInt(System.String,System.Int32)
		void Register_UnityEngine_PlayerPrefs_GetInt();
		Register_UnityEngine_PlayerPrefs_GetInt();

		//System.Single UnityEngine.PlayerPrefs::GetFloat(System.String,System.Single)
		void Register_UnityEngine_PlayerPrefs_GetFloat();
		Register_UnityEngine_PlayerPrefs_GetFloat();

		//System.String UnityEngine.PlayerPrefs::GetString(System.String,System.String)
		void Register_UnityEngine_PlayerPrefs_GetString();
		Register_UnityEngine_PlayerPrefs_GetString();

		//System.Void UnityEngine.PlayerPrefs::DeleteAll()
		void Register_UnityEngine_PlayerPrefs_DeleteAll();
		Register_UnityEngine_PlayerPrefs_DeleteAll();

		//System.Void UnityEngine.PlayerPrefs::DeleteKey(System.String)
		void Register_UnityEngine_PlayerPrefs_DeleteKey();
		Register_UnityEngine_PlayerPrefs_DeleteKey();

		//System.Void UnityEngine.PlayerPrefs::Save()
		void Register_UnityEngine_PlayerPrefs_Save();
		Register_UnityEngine_PlayerPrefs_Save();

	//End Registrations for type : UnityEngine.PlayerPrefs

	//Start Registrations for type : UnityEngine.PolygonCollider2D

		//System.Int32 UnityEngine.PolygonCollider2D::GetTotalPointCount()
		void Register_UnityEngine_PolygonCollider2D_GetTotalPointCount();
		Register_UnityEngine_PolygonCollider2D_GetTotalPointCount();

		//System.Int32 UnityEngine.PolygonCollider2D::get_pathCount()
		void Register_UnityEngine_PolygonCollider2D_get_pathCount();
		Register_UnityEngine_PolygonCollider2D_get_pathCount();

		//System.Void UnityEngine.PolygonCollider2D::INTERNAL_CALL_CreatePrimitive(UnityEngine.PolygonCollider2D,System.Int32,UnityEngine.Vector2&,UnityEngine.Vector2&)
		void Register_UnityEngine_PolygonCollider2D_INTERNAL_CALL_CreatePrimitive();
		Register_UnityEngine_PolygonCollider2D_INTERNAL_CALL_CreatePrimitive();

		//System.Void UnityEngine.PolygonCollider2D::SetPath(System.Int32,UnityEngine.Vector2[])
		void Register_UnityEngine_PolygonCollider2D_SetPath();
		Register_UnityEngine_PolygonCollider2D_SetPath();

		//System.Void UnityEngine.PolygonCollider2D::set_pathCount(System.Int32)
		void Register_UnityEngine_PolygonCollider2D_set_pathCount();
		Register_UnityEngine_PolygonCollider2D_set_pathCount();

		//System.Void UnityEngine.PolygonCollider2D::set_points(UnityEngine.Vector2[])
		void Register_UnityEngine_PolygonCollider2D_set_points();
		Register_UnityEngine_PolygonCollider2D_set_points();

		//UnityEngine.Vector2[] UnityEngine.PolygonCollider2D::GetPath(System.Int32)
		void Register_UnityEngine_PolygonCollider2D_GetPath();
		Register_UnityEngine_PolygonCollider2D_GetPath();

		//UnityEngine.Vector2[] UnityEngine.PolygonCollider2D::get_points()
		void Register_UnityEngine_PolygonCollider2D_get_points();
		Register_UnityEngine_PolygonCollider2D_get_points();

	//End Registrations for type : UnityEngine.PolygonCollider2D

	//Start Registrations for type : UnityEngine.ProceduralMaterial

		//System.Boolean UnityEngine.ProceduralMaterial::GetProceduralBoolean(System.String)
		void Register_UnityEngine_ProceduralMaterial_GetProceduralBoolean();
		Register_UnityEngine_ProceduralMaterial_GetProceduralBoolean();

		//System.Boolean UnityEngine.ProceduralMaterial::HasProceduralProperty(System.String)
		void Register_UnityEngine_ProceduralMaterial_HasProceduralProperty();
		Register_UnityEngine_ProceduralMaterial_HasProceduralProperty();

		//System.Boolean UnityEngine.ProceduralMaterial::IsProceduralPropertyCached(System.String)
		void Register_UnityEngine_ProceduralMaterial_IsProceduralPropertyCached();
		Register_UnityEngine_ProceduralMaterial_IsProceduralPropertyCached();

		//System.Boolean UnityEngine.ProceduralMaterial::IsProceduralPropertyVisible(System.String)
		void Register_UnityEngine_ProceduralMaterial_IsProceduralPropertyVisible();
		Register_UnityEngine_ProceduralMaterial_IsProceduralPropertyVisible();

		//System.Boolean UnityEngine.ProceduralMaterial::get_isCachedDataAvailable()
		void Register_UnityEngine_ProceduralMaterial_get_isCachedDataAvailable();
		Register_UnityEngine_ProceduralMaterial_get_isCachedDataAvailable();

		//System.Boolean UnityEngine.ProceduralMaterial::get_isFrozen()
		void Register_UnityEngine_ProceduralMaterial_get_isFrozen();
		Register_UnityEngine_ProceduralMaterial_get_isFrozen();

		//System.Boolean UnityEngine.ProceduralMaterial::get_isLoadTimeGenerated()
		void Register_UnityEngine_ProceduralMaterial_get_isLoadTimeGenerated();
		Register_UnityEngine_ProceduralMaterial_get_isLoadTimeGenerated();

		//System.Boolean UnityEngine.ProceduralMaterial::get_isProcessing()
		void Register_UnityEngine_ProceduralMaterial_get_isProcessing();
		Register_UnityEngine_ProceduralMaterial_get_isProcessing();

		//System.Boolean UnityEngine.ProceduralMaterial::get_isReadable()
		void Register_UnityEngine_ProceduralMaterial_get_isReadable();
		Register_UnityEngine_ProceduralMaterial_get_isReadable();

		//System.Boolean UnityEngine.ProceduralMaterial::get_isSupported()
		void Register_UnityEngine_ProceduralMaterial_get_isSupported();
		Register_UnityEngine_ProceduralMaterial_get_isSupported();

		//System.Int32 UnityEngine.ProceduralMaterial::GetProceduralEnum(System.String)
		void Register_UnityEngine_ProceduralMaterial_GetProceduralEnum();
		Register_UnityEngine_ProceduralMaterial_GetProceduralEnum();

		//System.Int32 UnityEngine.ProceduralMaterial::get_animationUpdateRate()
		void Register_UnityEngine_ProceduralMaterial_get_animationUpdateRate();
		Register_UnityEngine_ProceduralMaterial_get_animationUpdateRate();

		//System.Single UnityEngine.ProceduralMaterial::GetProceduralFloat(System.String)
		void Register_UnityEngine_ProceduralMaterial_GetProceduralFloat();
		Register_UnityEngine_ProceduralMaterial_GetProceduralFloat();

		//System.String UnityEngine.ProceduralMaterial::get_preset()
		void Register_UnityEngine_ProceduralMaterial_get_preset();
		Register_UnityEngine_ProceduralMaterial_get_preset();

		//System.Void UnityEngine.ProceduralMaterial::CacheProceduralProperty(System.String,System.Boolean)
		void Register_UnityEngine_ProceduralMaterial_CacheProceduralProperty();
		Register_UnityEngine_ProceduralMaterial_CacheProceduralProperty();

		//System.Void UnityEngine.ProceduralMaterial::ClearCache()
		void Register_UnityEngine_ProceduralMaterial_ClearCache();
		Register_UnityEngine_ProceduralMaterial_ClearCache();

		//System.Void UnityEngine.ProceduralMaterial::FreezeAndReleaseSourceData()
		void Register_UnityEngine_ProceduralMaterial_FreezeAndReleaseSourceData();
		Register_UnityEngine_ProceduralMaterial_FreezeAndReleaseSourceData();

		//System.Void UnityEngine.ProceduralMaterial::INTERNAL_CALL_GetProceduralColor(UnityEngine.ProceduralMaterial,System.String,UnityEngine.Color&)
		void Register_UnityEngine_ProceduralMaterial_INTERNAL_CALL_GetProceduralColor();
		Register_UnityEngine_ProceduralMaterial_INTERNAL_CALL_GetProceduralColor();

		//System.Void UnityEngine.ProceduralMaterial::INTERNAL_CALL_GetProceduralVector(UnityEngine.ProceduralMaterial,System.String,UnityEngine.Vector4&)
		void Register_UnityEngine_ProceduralMaterial_INTERNAL_CALL_GetProceduralVector();
		Register_UnityEngine_ProceduralMaterial_INTERNAL_CALL_GetProceduralVector();

		//System.Void UnityEngine.ProceduralMaterial::INTERNAL_CALL_SetProceduralColor(UnityEngine.ProceduralMaterial,System.String,UnityEngine.Color&)
		void Register_UnityEngine_ProceduralMaterial_INTERNAL_CALL_SetProceduralColor();
		Register_UnityEngine_ProceduralMaterial_INTERNAL_CALL_SetProceduralColor();

		//System.Void UnityEngine.ProceduralMaterial::INTERNAL_CALL_SetProceduralVector(UnityEngine.ProceduralMaterial,System.String,UnityEngine.Vector4&)
		void Register_UnityEngine_ProceduralMaterial_INTERNAL_CALL_SetProceduralVector();
		Register_UnityEngine_ProceduralMaterial_INTERNAL_CALL_SetProceduralVector();

		//System.Void UnityEngine.ProceduralMaterial::RebuildTextures()
		void Register_UnityEngine_ProceduralMaterial_RebuildTextures();
		Register_UnityEngine_ProceduralMaterial_RebuildTextures();

		//System.Void UnityEngine.ProceduralMaterial::RebuildTexturesImmediately()
		void Register_UnityEngine_ProceduralMaterial_RebuildTexturesImmediately();
		Register_UnityEngine_ProceduralMaterial_RebuildTexturesImmediately();

		//System.Void UnityEngine.ProceduralMaterial::SetProceduralBoolean(System.String,System.Boolean)
		void Register_UnityEngine_ProceduralMaterial_SetProceduralBoolean();
		Register_UnityEngine_ProceduralMaterial_SetProceduralBoolean();

		//System.Void UnityEngine.ProceduralMaterial::SetProceduralEnum(System.String,System.Int32)
		void Register_UnityEngine_ProceduralMaterial_SetProceduralEnum();
		Register_UnityEngine_ProceduralMaterial_SetProceduralEnum();

		//System.Void UnityEngine.ProceduralMaterial::SetProceduralFloat(System.String,System.Single)
		void Register_UnityEngine_ProceduralMaterial_SetProceduralFloat();
		Register_UnityEngine_ProceduralMaterial_SetProceduralFloat();

		//System.Void UnityEngine.ProceduralMaterial::SetProceduralTexture(System.String,UnityEngine.Texture2D)
		void Register_UnityEngine_ProceduralMaterial_SetProceduralTexture();
		Register_UnityEngine_ProceduralMaterial_SetProceduralTexture();

		//System.Void UnityEngine.ProceduralMaterial::StopRebuilds()
		void Register_UnityEngine_ProceduralMaterial_StopRebuilds();
		Register_UnityEngine_ProceduralMaterial_StopRebuilds();

		//System.Void UnityEngine.ProceduralMaterial::set_animationUpdateRate(System.Int32)
		void Register_UnityEngine_ProceduralMaterial_set_animationUpdateRate();
		Register_UnityEngine_ProceduralMaterial_set_animationUpdateRate();

		//System.Void UnityEngine.ProceduralMaterial::set_cacheSize(UnityEngine.ProceduralCacheSize)
		void Register_UnityEngine_ProceduralMaterial_set_cacheSize();
		Register_UnityEngine_ProceduralMaterial_set_cacheSize();

		//System.Void UnityEngine.ProceduralMaterial::set_isLoadTimeGenerated(System.Boolean)
		void Register_UnityEngine_ProceduralMaterial_set_isLoadTimeGenerated();
		Register_UnityEngine_ProceduralMaterial_set_isLoadTimeGenerated();

		//System.Void UnityEngine.ProceduralMaterial::set_isReadable(System.Boolean)
		void Register_UnityEngine_ProceduralMaterial_set_isReadable();
		Register_UnityEngine_ProceduralMaterial_set_isReadable();

		//System.Void UnityEngine.ProceduralMaterial::set_preset(System.String)
		void Register_UnityEngine_ProceduralMaterial_set_preset();
		Register_UnityEngine_ProceduralMaterial_set_preset();

		//System.Void UnityEngine.ProceduralMaterial::set_substanceProcessorUsage(UnityEngine.ProceduralProcessorUsage)
		void Register_UnityEngine_ProceduralMaterial_set_substanceProcessorUsage();
		Register_UnityEngine_ProceduralMaterial_set_substanceProcessorUsage();

		//UnityEngine.ProceduralCacheSize UnityEngine.ProceduralMaterial::get_cacheSize()
		void Register_UnityEngine_ProceduralMaterial_get_cacheSize();
		Register_UnityEngine_ProceduralMaterial_get_cacheSize();

		//UnityEngine.ProceduralLoadingBehavior UnityEngine.ProceduralMaterial::get_loadingBehavior()
		void Register_UnityEngine_ProceduralMaterial_get_loadingBehavior();
		Register_UnityEngine_ProceduralMaterial_get_loadingBehavior();

		//UnityEngine.ProceduralProcessorUsage UnityEngine.ProceduralMaterial::get_substanceProcessorUsage()
		void Register_UnityEngine_ProceduralMaterial_get_substanceProcessorUsage();
		Register_UnityEngine_ProceduralMaterial_get_substanceProcessorUsage();

		//UnityEngine.ProceduralPropertyDescription[] UnityEngine.ProceduralMaterial::GetProceduralPropertyDescriptions()
		void Register_UnityEngine_ProceduralMaterial_GetProceduralPropertyDescriptions();
		Register_UnityEngine_ProceduralMaterial_GetProceduralPropertyDescriptions();

		//UnityEngine.ProceduralTexture UnityEngine.ProceduralMaterial::GetGeneratedTexture(System.String)
		void Register_UnityEngine_ProceduralMaterial_GetGeneratedTexture();
		Register_UnityEngine_ProceduralMaterial_GetGeneratedTexture();

		//UnityEngine.Texture2D UnityEngine.ProceduralMaterial::GetProceduralTexture(System.String)
		void Register_UnityEngine_ProceduralMaterial_GetProceduralTexture();
		Register_UnityEngine_ProceduralMaterial_GetProceduralTexture();

		//UnityEngine.Texture[] UnityEngine.ProceduralMaterial::GetGeneratedTextures()
		void Register_UnityEngine_ProceduralMaterial_GetGeneratedTextures();
		Register_UnityEngine_ProceduralMaterial_GetGeneratedTextures();

	//End Registrations for type : UnityEngine.ProceduralMaterial

	//Start Registrations for type : UnityEngine.ProceduralTexture

		//System.Boolean UnityEngine.ProceduralTexture::get_hasAlpha()
		void Register_UnityEngine_ProceduralTexture_get_hasAlpha();
		Register_UnityEngine_ProceduralTexture_get_hasAlpha();

		//UnityEngine.Color32[] UnityEngine.ProceduralTexture::GetPixels32(System.Int32,System.Int32,System.Int32,System.Int32)
		void Register_UnityEngine_ProceduralTexture_GetPixels32();
		Register_UnityEngine_ProceduralTexture_GetPixels32();

		//UnityEngine.ProceduralOutputType UnityEngine.ProceduralTexture::GetProceduralOutputType()
		void Register_UnityEngine_ProceduralTexture_GetProceduralOutputType();
		Register_UnityEngine_ProceduralTexture_GetProceduralOutputType();

		//UnityEngine.TextureFormat UnityEngine.ProceduralTexture::get_format()
		void Register_UnityEngine_ProceduralTexture_get_format();
		Register_UnityEngine_ProceduralTexture_get_format();

	//End Registrations for type : UnityEngine.ProceduralTexture

	//Start Registrations for type : UnityEngine.Profiler

		//System.Boolean UnityEngine.Profiler::get_enableBinaryLog()
		void Register_UnityEngine_Profiler_get_enableBinaryLog();
		Register_UnityEngine_Profiler_get_enableBinaryLog();

		//System.Boolean UnityEngine.Profiler::get_enabled()
		void Register_UnityEngine_Profiler_get_enabled();
		Register_UnityEngine_Profiler_get_enabled();

		//System.Boolean UnityEngine.Profiler::get_supported()
		void Register_UnityEngine_Profiler_get_supported();
		Register_UnityEngine_Profiler_get_supported();

		//System.Int32 UnityEngine.Profiler::GetRuntimeMemorySize(UnityEngine.Object)
		void Register_UnityEngine_Profiler_GetRuntimeMemorySize();
		Register_UnityEngine_Profiler_GetRuntimeMemorySize();

		//System.Int32 UnityEngine.Profiler::get_maxNumberOfSamplesPerFrame()
		void Register_UnityEngine_Profiler_get_maxNumberOfSamplesPerFrame();
		Register_UnityEngine_Profiler_get_maxNumberOfSamplesPerFrame();

		//System.String UnityEngine.Profiler::get_logFile()
		void Register_UnityEngine_Profiler_get_logFile();
		Register_UnityEngine_Profiler_get_logFile();

		//System.UInt32 UnityEngine.Profiler::GetMonoHeapSize()
		void Register_UnityEngine_Profiler_GetMonoHeapSize();
		Register_UnityEngine_Profiler_GetMonoHeapSize();

		//System.UInt32 UnityEngine.Profiler::GetMonoUsedSize()
		void Register_UnityEngine_Profiler_GetMonoUsedSize();
		Register_UnityEngine_Profiler_GetMonoUsedSize();

		//System.UInt32 UnityEngine.Profiler::GetTotalAllocatedMemory()
		void Register_UnityEngine_Profiler_GetTotalAllocatedMemory();
		Register_UnityEngine_Profiler_GetTotalAllocatedMemory();

		//System.UInt32 UnityEngine.Profiler::GetTotalReservedMemory()
		void Register_UnityEngine_Profiler_GetTotalReservedMemory();
		Register_UnityEngine_Profiler_GetTotalReservedMemory();

		//System.UInt32 UnityEngine.Profiler::GetTotalUnusedReservedMemory()
		void Register_UnityEngine_Profiler_GetTotalUnusedReservedMemory();
		Register_UnityEngine_Profiler_GetTotalUnusedReservedMemory();

		//System.UInt32 UnityEngine.Profiler::get_usedHeapSize()
		void Register_UnityEngine_Profiler_get_usedHeapSize();
		Register_UnityEngine_Profiler_get_usedHeapSize();

		//System.Void UnityEngine.Profiler::set_enableBinaryLog(System.Boolean)
		void Register_UnityEngine_Profiler_set_enableBinaryLog();
		Register_UnityEngine_Profiler_set_enableBinaryLog();

		//System.Void UnityEngine.Profiler::set_enabled(System.Boolean)
		void Register_UnityEngine_Profiler_set_enabled();
		Register_UnityEngine_Profiler_set_enabled();

		//System.Void UnityEngine.Profiler::set_logFile(System.String)
		void Register_UnityEngine_Profiler_set_logFile();
		Register_UnityEngine_Profiler_set_logFile();

		//System.Void UnityEngine.Profiler::set_maxNumberOfSamplesPerFrame(System.Int32)
		void Register_UnityEngine_Profiler_set_maxNumberOfSamplesPerFrame();
		Register_UnityEngine_Profiler_set_maxNumberOfSamplesPerFrame();

	//End Registrations for type : UnityEngine.Profiler

	//Start Registrations for type : UnityEngine.Projector

		//System.Boolean UnityEngine.Projector::get_orthographic()
		void Register_UnityEngine_Projector_get_orthographic();
		Register_UnityEngine_Projector_get_orthographic();

		//System.Int32 UnityEngine.Projector::get_ignoreLayers()
		void Register_UnityEngine_Projector_get_ignoreLayers();
		Register_UnityEngine_Projector_get_ignoreLayers();

		//System.Single UnityEngine.Projector::get_aspectRatio()
		void Register_UnityEngine_Projector_get_aspectRatio();
		Register_UnityEngine_Projector_get_aspectRatio();

		//System.Single UnityEngine.Projector::get_farClipPlane()
		void Register_UnityEngine_Projector_get_farClipPlane();
		Register_UnityEngine_Projector_get_farClipPlane();

		//System.Single UnityEngine.Projector::get_fieldOfView()
		void Register_UnityEngine_Projector_get_fieldOfView();
		Register_UnityEngine_Projector_get_fieldOfView();

		//System.Single UnityEngine.Projector::get_nearClipPlane()
		void Register_UnityEngine_Projector_get_nearClipPlane();
		Register_UnityEngine_Projector_get_nearClipPlane();

		//System.Single UnityEngine.Projector::get_orthographicSize()
		void Register_UnityEngine_Projector_get_orthographicSize();
		Register_UnityEngine_Projector_get_orthographicSize();

		//System.Void UnityEngine.Projector::set_aspectRatio(System.Single)
		void Register_UnityEngine_Projector_set_aspectRatio();
		Register_UnityEngine_Projector_set_aspectRatio();

		//System.Void UnityEngine.Projector::set_farClipPlane(System.Single)
		void Register_UnityEngine_Projector_set_farClipPlane();
		Register_UnityEngine_Projector_set_farClipPlane();

		//System.Void UnityEngine.Projector::set_fieldOfView(System.Single)
		void Register_UnityEngine_Projector_set_fieldOfView();
		Register_UnityEngine_Projector_set_fieldOfView();

		//System.Void UnityEngine.Projector::set_ignoreLayers(System.Int32)
		void Register_UnityEngine_Projector_set_ignoreLayers();
		Register_UnityEngine_Projector_set_ignoreLayers();

		//System.Void UnityEngine.Projector::set_material(UnityEngine.Material)
		void Register_UnityEngine_Projector_set_material();
		Register_UnityEngine_Projector_set_material();

		//System.Void UnityEngine.Projector::set_nearClipPlane(System.Single)
		void Register_UnityEngine_Projector_set_nearClipPlane();
		Register_UnityEngine_Projector_set_nearClipPlane();

		//System.Void UnityEngine.Projector::set_orthographic(System.Boolean)
		void Register_UnityEngine_Projector_set_orthographic();
		Register_UnityEngine_Projector_set_orthographic();

		//System.Void UnityEngine.Projector::set_orthographicSize(System.Single)
		void Register_UnityEngine_Projector_set_orthographicSize();
		Register_UnityEngine_Projector_set_orthographicSize();

		//UnityEngine.Material UnityEngine.Projector::get_material()
		void Register_UnityEngine_Projector_get_material();
		Register_UnityEngine_Projector_get_material();

	//End Registrations for type : UnityEngine.Projector

	//Start Registrations for type : UnityEngine.QualitySettings

		//System.Boolean UnityEngine.QualitySettings::get_billboardsFaceCameraPosition()
		void Register_UnityEngine_QualitySettings_get_billboardsFaceCameraPosition();
		Register_UnityEngine_QualitySettings_get_billboardsFaceCameraPosition();

		//System.Boolean UnityEngine.QualitySettings::get_realtimeReflectionProbes()
		void Register_UnityEngine_QualitySettings_get_realtimeReflectionProbes();
		Register_UnityEngine_QualitySettings_get_realtimeReflectionProbes();

		//System.Boolean UnityEngine.QualitySettings::get_softVegetation()
		void Register_UnityEngine_QualitySettings_get_softVegetation();
		Register_UnityEngine_QualitySettings_get_softVegetation();

		//System.Int32 UnityEngine.QualitySettings::GetQualityLevel()
		void Register_UnityEngine_QualitySettings_GetQualityLevel();
		Register_UnityEngine_QualitySettings_GetQualityLevel();

		//System.Int32 UnityEngine.QualitySettings::get_antiAliasing()
		void Register_UnityEngine_QualitySettings_get_antiAliasing();
		Register_UnityEngine_QualitySettings_get_antiAliasing();

		//System.Int32 UnityEngine.QualitySettings::get_asyncUploadBufferSize()
		void Register_UnityEngine_QualitySettings_get_asyncUploadBufferSize();
		Register_UnityEngine_QualitySettings_get_asyncUploadBufferSize();

		//System.Int32 UnityEngine.QualitySettings::get_asyncUploadTimeSlice()
		void Register_UnityEngine_QualitySettings_get_asyncUploadTimeSlice();
		Register_UnityEngine_QualitySettings_get_asyncUploadTimeSlice();

		//System.Int32 UnityEngine.QualitySettings::get_masterTextureLimit()
		void Register_UnityEngine_QualitySettings_get_masterTextureLimit();
		Register_UnityEngine_QualitySettings_get_masterTextureLimit();

		//System.Int32 UnityEngine.QualitySettings::get_maxQueuedFrames()
		void Register_UnityEngine_QualitySettings_get_maxQueuedFrames();
		Register_UnityEngine_QualitySettings_get_maxQueuedFrames();

		//System.Int32 UnityEngine.QualitySettings::get_maximumLODLevel()
		void Register_UnityEngine_QualitySettings_get_maximumLODLevel();
		Register_UnityEngine_QualitySettings_get_maximumLODLevel();

		//System.Int32 UnityEngine.QualitySettings::get_particleRaycastBudget()
		void Register_UnityEngine_QualitySettings_get_particleRaycastBudget();
		Register_UnityEngine_QualitySettings_get_particleRaycastBudget();

		//System.Int32 UnityEngine.QualitySettings::get_pixelLightCount()
		void Register_UnityEngine_QualitySettings_get_pixelLightCount();
		Register_UnityEngine_QualitySettings_get_pixelLightCount();

		//System.Int32 UnityEngine.QualitySettings::get_shadowCascades()
		void Register_UnityEngine_QualitySettings_get_shadowCascades();
		Register_UnityEngine_QualitySettings_get_shadowCascades();

		//System.Int32 UnityEngine.QualitySettings::get_vSyncCount()
		void Register_UnityEngine_QualitySettings_get_vSyncCount();
		Register_UnityEngine_QualitySettings_get_vSyncCount();

		//System.Single UnityEngine.QualitySettings::get_lodBias()
		void Register_UnityEngine_QualitySettings_get_lodBias();
		Register_UnityEngine_QualitySettings_get_lodBias();

		//System.Single UnityEngine.QualitySettings::get_shadowCascade2Split()
		void Register_UnityEngine_QualitySettings_get_shadowCascade2Split();
		Register_UnityEngine_QualitySettings_get_shadowCascade2Split();

		//System.Single UnityEngine.QualitySettings::get_shadowDistance()
		void Register_UnityEngine_QualitySettings_get_shadowDistance();
		Register_UnityEngine_QualitySettings_get_shadowDistance();

		//System.Single UnityEngine.QualitySettings::get_shadowNearPlaneOffset()
		void Register_UnityEngine_QualitySettings_get_shadowNearPlaneOffset();
		Register_UnityEngine_QualitySettings_get_shadowNearPlaneOffset();

		//System.String[] UnityEngine.QualitySettings::get_names()
		void Register_UnityEngine_QualitySettings_get_names();
		Register_UnityEngine_QualitySettings_get_names();

		//System.Void UnityEngine.QualitySettings::DecreaseLevel(System.Boolean)
		void Register_UnityEngine_QualitySettings_DecreaseLevel();
		Register_UnityEngine_QualitySettings_DecreaseLevel();

		//System.Void UnityEngine.QualitySettings::INTERNAL_get_shadowCascade4Split(UnityEngine.Vector3&)
		void Register_UnityEngine_QualitySettings_INTERNAL_get_shadowCascade4Split();
		Register_UnityEngine_QualitySettings_INTERNAL_get_shadowCascade4Split();

		//System.Void UnityEngine.QualitySettings::INTERNAL_set_shadowCascade4Split(UnityEngine.Vector3&)
		void Register_UnityEngine_QualitySettings_INTERNAL_set_shadowCascade4Split();
		Register_UnityEngine_QualitySettings_INTERNAL_set_shadowCascade4Split();

		//System.Void UnityEngine.QualitySettings::IncreaseLevel(System.Boolean)
		void Register_UnityEngine_QualitySettings_IncreaseLevel();
		Register_UnityEngine_QualitySettings_IncreaseLevel();

		//System.Void UnityEngine.QualitySettings::SetQualityLevel(System.Int32,System.Boolean)
		void Register_UnityEngine_QualitySettings_SetQualityLevel();
		Register_UnityEngine_QualitySettings_SetQualityLevel();

		//System.Void UnityEngine.QualitySettings::set_anisotropicFiltering(UnityEngine.AnisotropicFiltering)
		void Register_UnityEngine_QualitySettings_set_anisotropicFiltering();
		Register_UnityEngine_QualitySettings_set_anisotropicFiltering();

		//System.Void UnityEngine.QualitySettings::set_antiAliasing(System.Int32)
		void Register_UnityEngine_QualitySettings_set_antiAliasing();
		Register_UnityEngine_QualitySettings_set_antiAliasing();

		//System.Void UnityEngine.QualitySettings::set_asyncUploadBufferSize(System.Int32)
		void Register_UnityEngine_QualitySettings_set_asyncUploadBufferSize();
		Register_UnityEngine_QualitySettings_set_asyncUploadBufferSize();

		//System.Void UnityEngine.QualitySettings::set_asyncUploadTimeSlice(System.Int32)
		void Register_UnityEngine_QualitySettings_set_asyncUploadTimeSlice();
		Register_UnityEngine_QualitySettings_set_asyncUploadTimeSlice();

		//System.Void UnityEngine.QualitySettings::set_billboardsFaceCameraPosition(System.Boolean)
		void Register_UnityEngine_QualitySettings_set_billboardsFaceCameraPosition();
		Register_UnityEngine_QualitySettings_set_billboardsFaceCameraPosition();

		//System.Void UnityEngine.QualitySettings::set_blendWeights(UnityEngine.BlendWeights)
		void Register_UnityEngine_QualitySettings_set_blendWeights();
		Register_UnityEngine_QualitySettings_set_blendWeights();

		//System.Void UnityEngine.QualitySettings::set_lodBias(System.Single)
		void Register_UnityEngine_QualitySettings_set_lodBias();
		Register_UnityEngine_QualitySettings_set_lodBias();

		//System.Void UnityEngine.QualitySettings::set_masterTextureLimit(System.Int32)
		void Register_UnityEngine_QualitySettings_set_masterTextureLimit();
		Register_UnityEngine_QualitySettings_set_masterTextureLimit();

		//System.Void UnityEngine.QualitySettings::set_maxQueuedFrames(System.Int32)
		void Register_UnityEngine_QualitySettings_set_maxQueuedFrames();
		Register_UnityEngine_QualitySettings_set_maxQueuedFrames();

		//System.Void UnityEngine.QualitySettings::set_maximumLODLevel(System.Int32)
		void Register_UnityEngine_QualitySettings_set_maximumLODLevel();
		Register_UnityEngine_QualitySettings_set_maximumLODLevel();

		//System.Void UnityEngine.QualitySettings::set_particleRaycastBudget(System.Int32)
		void Register_UnityEngine_QualitySettings_set_particleRaycastBudget();
		Register_UnityEngine_QualitySettings_set_particleRaycastBudget();

		//System.Void UnityEngine.QualitySettings::set_pixelLightCount(System.Int32)
		void Register_UnityEngine_QualitySettings_set_pixelLightCount();
		Register_UnityEngine_QualitySettings_set_pixelLightCount();

		//System.Void UnityEngine.QualitySettings::set_realtimeReflectionProbes(System.Boolean)
		void Register_UnityEngine_QualitySettings_set_realtimeReflectionProbes();
		Register_UnityEngine_QualitySettings_set_realtimeReflectionProbes();

		//System.Void UnityEngine.QualitySettings::set_shadowCascade2Split(System.Single)
		void Register_UnityEngine_QualitySettings_set_shadowCascade2Split();
		Register_UnityEngine_QualitySettings_set_shadowCascade2Split();

		//System.Void UnityEngine.QualitySettings::set_shadowCascades(System.Int32)
		void Register_UnityEngine_QualitySettings_set_shadowCascades();
		Register_UnityEngine_QualitySettings_set_shadowCascades();

		//System.Void UnityEngine.QualitySettings::set_shadowDistance(System.Single)
		void Register_UnityEngine_QualitySettings_set_shadowDistance();
		Register_UnityEngine_QualitySettings_set_shadowDistance();

		//System.Void UnityEngine.QualitySettings::set_shadowNearPlaneOffset(System.Single)
		void Register_UnityEngine_QualitySettings_set_shadowNearPlaneOffset();
		Register_UnityEngine_QualitySettings_set_shadowNearPlaneOffset();

		//System.Void UnityEngine.QualitySettings::set_shadowProjection(UnityEngine.ShadowProjection)
		void Register_UnityEngine_QualitySettings_set_shadowProjection();
		Register_UnityEngine_QualitySettings_set_shadowProjection();

		//System.Void UnityEngine.QualitySettings::set_softVegetation(System.Boolean)
		void Register_UnityEngine_QualitySettings_set_softVegetation();
		Register_UnityEngine_QualitySettings_set_softVegetation();

		//System.Void UnityEngine.QualitySettings::set_vSyncCount(System.Int32)
		void Register_UnityEngine_QualitySettings_set_vSyncCount();
		Register_UnityEngine_QualitySettings_set_vSyncCount();

		//UnityEngine.AnisotropicFiltering UnityEngine.QualitySettings::get_anisotropicFiltering()
		void Register_UnityEngine_QualitySettings_get_anisotropicFiltering();
		Register_UnityEngine_QualitySettings_get_anisotropicFiltering();

		//UnityEngine.BlendWeights UnityEngine.QualitySettings::get_blendWeights()
		void Register_UnityEngine_QualitySettings_get_blendWeights();
		Register_UnityEngine_QualitySettings_get_blendWeights();

		//UnityEngine.ColorSpace UnityEngine.QualitySettings::get_activeColorSpace()
		void Register_UnityEngine_QualitySettings_get_activeColorSpace();
		Register_UnityEngine_QualitySettings_get_activeColorSpace();

		//UnityEngine.ColorSpace UnityEngine.QualitySettings::get_desiredColorSpace()
		void Register_UnityEngine_QualitySettings_get_desiredColorSpace();
		Register_UnityEngine_QualitySettings_get_desiredColorSpace();

		//UnityEngine.ShadowProjection UnityEngine.QualitySettings::get_shadowProjection()
		void Register_UnityEngine_QualitySettings_get_shadowProjection();
		Register_UnityEngine_QualitySettings_get_shadowProjection();

	//End Registrations for type : UnityEngine.QualitySettings

	//Start Registrations for type : UnityEngine.Quaternion

		//System.Void UnityEngine.Quaternion::INTERNAL_CALL_AngleAxis(System.Single,UnityEngine.Vector3&,UnityEngine.Quaternion&)
		void Register_UnityEngine_Quaternion_INTERNAL_CALL_AngleAxis();
		Register_UnityEngine_Quaternion_INTERNAL_CALL_AngleAxis();

		//System.Void UnityEngine.Quaternion::INTERNAL_CALL_FromToRotation(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Quaternion&)
		void Register_UnityEngine_Quaternion_INTERNAL_CALL_FromToRotation();
		Register_UnityEngine_Quaternion_INTERNAL_CALL_FromToRotation();

		//System.Void UnityEngine.Quaternion::INTERNAL_CALL_Internal_FromEulerRad(UnityEngine.Vector3&,UnityEngine.Quaternion&)
		void Register_UnityEngine_Quaternion_INTERNAL_CALL_Internal_FromEulerRad();
		Register_UnityEngine_Quaternion_INTERNAL_CALL_Internal_FromEulerRad();

		//System.Void UnityEngine.Quaternion::INTERNAL_CALL_Internal_ToAxisAngleRad(UnityEngine.Quaternion&,UnityEngine.Vector3&,System.Single&)
		void Register_UnityEngine_Quaternion_INTERNAL_CALL_Internal_ToAxisAngleRad();
		Register_UnityEngine_Quaternion_INTERNAL_CALL_Internal_ToAxisAngleRad();

		//System.Void UnityEngine.Quaternion::INTERNAL_CALL_Internal_ToEulerRad(UnityEngine.Quaternion&,UnityEngine.Vector3&)
		void Register_UnityEngine_Quaternion_INTERNAL_CALL_Internal_ToEulerRad();
		Register_UnityEngine_Quaternion_INTERNAL_CALL_Internal_ToEulerRad();

		//System.Void UnityEngine.Quaternion::INTERNAL_CALL_Inverse(UnityEngine.Quaternion&,UnityEngine.Quaternion&)
		void Register_UnityEngine_Quaternion_INTERNAL_CALL_Inverse();
		Register_UnityEngine_Quaternion_INTERNAL_CALL_Inverse();

		//System.Void UnityEngine.Quaternion::INTERNAL_CALL_Lerp(UnityEngine.Quaternion&,UnityEngine.Quaternion&,System.Single,UnityEngine.Quaternion&)
		void Register_UnityEngine_Quaternion_INTERNAL_CALL_Lerp();
		Register_UnityEngine_Quaternion_INTERNAL_CALL_Lerp();

		//System.Void UnityEngine.Quaternion::INTERNAL_CALL_LerpUnclamped(UnityEngine.Quaternion&,UnityEngine.Quaternion&,System.Single,UnityEngine.Quaternion&)
		void Register_UnityEngine_Quaternion_INTERNAL_CALL_LerpUnclamped();
		Register_UnityEngine_Quaternion_INTERNAL_CALL_LerpUnclamped();

		//System.Void UnityEngine.Quaternion::INTERNAL_CALL_LookRotation(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Quaternion&)
		void Register_UnityEngine_Quaternion_INTERNAL_CALL_LookRotation();
		Register_UnityEngine_Quaternion_INTERNAL_CALL_LookRotation();

		//System.Void UnityEngine.Quaternion::INTERNAL_CALL_Slerp(UnityEngine.Quaternion&,UnityEngine.Quaternion&,System.Single,UnityEngine.Quaternion&)
		void Register_UnityEngine_Quaternion_INTERNAL_CALL_Slerp();
		Register_UnityEngine_Quaternion_INTERNAL_CALL_Slerp();

		//System.Void UnityEngine.Quaternion::INTERNAL_CALL_SlerpUnclamped(UnityEngine.Quaternion&,UnityEngine.Quaternion&,System.Single,UnityEngine.Quaternion&)
		void Register_UnityEngine_Quaternion_INTERNAL_CALL_SlerpUnclamped();
		Register_UnityEngine_Quaternion_INTERNAL_CALL_SlerpUnclamped();

	//End Registrations for type : UnityEngine.Quaternion

	//Start Registrations for type : UnityEngine.Random

		//System.Int32 UnityEngine.Random::RandomRangeInt(System.Int32,System.Int32)
		void Register_UnityEngine_Random_RandomRangeInt();
		Register_UnityEngine_Random_RandomRangeInt();

		//System.Int32 UnityEngine.Random::get_seed()
		void Register_UnityEngine_Random_get_seed();
		Register_UnityEngine_Random_get_seed();

		//System.Single UnityEngine.Random::Range(System.Single,System.Single)
		void Register_UnityEngine_Random_Range();
		Register_UnityEngine_Random_Range();

		//System.Single UnityEngine.Random::get_value()
		void Register_UnityEngine_Random_get_value();
		Register_UnityEngine_Random_get_value();

		//System.Void UnityEngine.Random::GetRandomUnitCircle(UnityEngine.Vector2&)
		void Register_UnityEngine_Random_GetRandomUnitCircle();
		Register_UnityEngine_Random_GetRandomUnitCircle();

		//System.Void UnityEngine.Random::INTERNAL_get_insideUnitSphere(UnityEngine.Vector3&)
		void Register_UnityEngine_Random_INTERNAL_get_insideUnitSphere();
		Register_UnityEngine_Random_INTERNAL_get_insideUnitSphere();

		//System.Void UnityEngine.Random::INTERNAL_get_onUnitSphere(UnityEngine.Vector3&)
		void Register_UnityEngine_Random_INTERNAL_get_onUnitSphere();
		Register_UnityEngine_Random_INTERNAL_get_onUnitSphere();

		//System.Void UnityEngine.Random::INTERNAL_get_rotation(UnityEngine.Quaternion&)
		void Register_UnityEngine_Random_INTERNAL_get_rotation();
		Register_UnityEngine_Random_INTERNAL_get_rotation();

		//System.Void UnityEngine.Random::INTERNAL_get_rotationUniform(UnityEngine.Quaternion&)
		void Register_UnityEngine_Random_INTERNAL_get_rotationUniform();
		Register_UnityEngine_Random_INTERNAL_get_rotationUniform();

		//System.Void UnityEngine.Random::set_seed(System.Int32)
		void Register_UnityEngine_Random_set_seed();
		Register_UnityEngine_Random_set_seed();

	//End Registrations for type : UnityEngine.Random

	//Start Registrations for type : UnityEngine.RaycastHit

		//System.Void UnityEngine.RaycastHit::INTERNAL_CALL_CalculateRaycastTexCoord(UnityEngine.Vector2&,UnityEngine.Collider,UnityEngine.Vector2&,UnityEngine.Vector3&,System.Int32,System.Int32)
		void Register_UnityEngine_RaycastHit_INTERNAL_CALL_CalculateRaycastTexCoord();
		Register_UnityEngine_RaycastHit_INTERNAL_CALL_CalculateRaycastTexCoord();

	//End Registrations for type : UnityEngine.RaycastHit

	//Start Registrations for type : UnityEngine.RectOffset

		//System.Int32 UnityEngine.RectOffset::get_bottom()
		void Register_UnityEngine_RectOffset_get_bottom();
		Register_UnityEngine_RectOffset_get_bottom();

		//System.Int32 UnityEngine.RectOffset::get_horizontal()
		void Register_UnityEngine_RectOffset_get_horizontal();
		Register_UnityEngine_RectOffset_get_horizontal();

		//System.Int32 UnityEngine.RectOffset::get_left()
		void Register_UnityEngine_RectOffset_get_left();
		Register_UnityEngine_RectOffset_get_left();

		//System.Int32 UnityEngine.RectOffset::get_right()
		void Register_UnityEngine_RectOffset_get_right();
		Register_UnityEngine_RectOffset_get_right();

		//System.Int32 UnityEngine.RectOffset::get_top()
		void Register_UnityEngine_RectOffset_get_top();
		Register_UnityEngine_RectOffset_get_top();

		//System.Int32 UnityEngine.RectOffset::get_vertical()
		void Register_UnityEngine_RectOffset_get_vertical();
		Register_UnityEngine_RectOffset_get_vertical();

		//System.Void UnityEngine.RectOffset::Cleanup()
		void Register_UnityEngine_RectOffset_Cleanup();
		Register_UnityEngine_RectOffset_Cleanup();

		//System.Void UnityEngine.RectOffset::INTERNAL_CALL_Add(UnityEngine.RectOffset,UnityEngine.Rect&,UnityEngine.Rect&)
		void Register_UnityEngine_RectOffset_INTERNAL_CALL_Add();
		Register_UnityEngine_RectOffset_INTERNAL_CALL_Add();

		//System.Void UnityEngine.RectOffset::INTERNAL_CALL_Remove(UnityEngine.RectOffset,UnityEngine.Rect&,UnityEngine.Rect&)
		void Register_UnityEngine_RectOffset_INTERNAL_CALL_Remove();
		Register_UnityEngine_RectOffset_INTERNAL_CALL_Remove();

		//System.Void UnityEngine.RectOffset::Init()
		void Register_UnityEngine_RectOffset_Init();
		Register_UnityEngine_RectOffset_Init();

		//System.Void UnityEngine.RectOffset::set_bottom(System.Int32)
		void Register_UnityEngine_RectOffset_set_bottom();
		Register_UnityEngine_RectOffset_set_bottom();

		//System.Void UnityEngine.RectOffset::set_left(System.Int32)
		void Register_UnityEngine_RectOffset_set_left();
		Register_UnityEngine_RectOffset_set_left();

		//System.Void UnityEngine.RectOffset::set_right(System.Int32)
		void Register_UnityEngine_RectOffset_set_right();
		Register_UnityEngine_RectOffset_set_right();

		//System.Void UnityEngine.RectOffset::set_top(System.Int32)
		void Register_UnityEngine_RectOffset_set_top();
		Register_UnityEngine_RectOffset_set_top();

	//End Registrations for type : UnityEngine.RectOffset

	//Start Registrations for type : UnityEngine.RectTransform

		//System.Void UnityEngine.RectTransform::INTERNAL_get_anchorMax(UnityEngine.Vector2&)
		void Register_UnityEngine_RectTransform_INTERNAL_get_anchorMax();
		Register_UnityEngine_RectTransform_INTERNAL_get_anchorMax();

		//System.Void UnityEngine.RectTransform::INTERNAL_get_anchorMin(UnityEngine.Vector2&)
		void Register_UnityEngine_RectTransform_INTERNAL_get_anchorMin();
		Register_UnityEngine_RectTransform_INTERNAL_get_anchorMin();

		//System.Void UnityEngine.RectTransform::INTERNAL_get_anchoredPosition(UnityEngine.Vector2&)
		void Register_UnityEngine_RectTransform_INTERNAL_get_anchoredPosition();
		Register_UnityEngine_RectTransform_INTERNAL_get_anchoredPosition();

		//System.Void UnityEngine.RectTransform::INTERNAL_get_pivot(UnityEngine.Vector2&)
		void Register_UnityEngine_RectTransform_INTERNAL_get_pivot();
		Register_UnityEngine_RectTransform_INTERNAL_get_pivot();

		//System.Void UnityEngine.RectTransform::INTERNAL_get_rect(UnityEngine.Rect&)
		void Register_UnityEngine_RectTransform_INTERNAL_get_rect();
		Register_UnityEngine_RectTransform_INTERNAL_get_rect();

		//System.Void UnityEngine.RectTransform::INTERNAL_get_sizeDelta(UnityEngine.Vector2&)
		void Register_UnityEngine_RectTransform_INTERNAL_get_sizeDelta();
		Register_UnityEngine_RectTransform_INTERNAL_get_sizeDelta();

		//System.Void UnityEngine.RectTransform::INTERNAL_set_anchorMax(UnityEngine.Vector2&)
		void Register_UnityEngine_RectTransform_INTERNAL_set_anchorMax();
		Register_UnityEngine_RectTransform_INTERNAL_set_anchorMax();

		//System.Void UnityEngine.RectTransform::INTERNAL_set_anchorMin(UnityEngine.Vector2&)
		void Register_UnityEngine_RectTransform_INTERNAL_set_anchorMin();
		Register_UnityEngine_RectTransform_INTERNAL_set_anchorMin();

		//System.Void UnityEngine.RectTransform::INTERNAL_set_anchoredPosition(UnityEngine.Vector2&)
		void Register_UnityEngine_RectTransform_INTERNAL_set_anchoredPosition();
		Register_UnityEngine_RectTransform_INTERNAL_set_anchoredPosition();

		//System.Void UnityEngine.RectTransform::INTERNAL_set_pivot(UnityEngine.Vector2&)
		void Register_UnityEngine_RectTransform_INTERNAL_set_pivot();
		Register_UnityEngine_RectTransform_INTERNAL_set_pivot();

		//System.Void UnityEngine.RectTransform::INTERNAL_set_sizeDelta(UnityEngine.Vector2&)
		void Register_UnityEngine_RectTransform_INTERNAL_set_sizeDelta();
		Register_UnityEngine_RectTransform_INTERNAL_set_sizeDelta();

	//End Registrations for type : UnityEngine.RectTransform

	//Start Registrations for type : UnityEngine.RectTransformUtility

		//System.Boolean UnityEngine.RectTransformUtility::INTERNAL_CALL_RectangleContainsScreenPoint(UnityEngine.RectTransform,UnityEngine.Vector2&,UnityEngine.Camera)
		void Register_UnityEngine_RectTransformUtility_INTERNAL_CALL_RectangleContainsScreenPoint();
		Register_UnityEngine_RectTransformUtility_INTERNAL_CALL_RectangleContainsScreenPoint();

		//System.Void UnityEngine.RectTransformUtility::INTERNAL_CALL_PixelAdjustPoint(UnityEngine.Vector2&,UnityEngine.Transform,UnityEngine.Canvas,UnityEngine.Vector2&)
		void Register_UnityEngine_RectTransformUtility_INTERNAL_CALL_PixelAdjustPoint();
		Register_UnityEngine_RectTransformUtility_INTERNAL_CALL_PixelAdjustPoint();

		//System.Void UnityEngine.RectTransformUtility::INTERNAL_CALL_PixelAdjustRect(UnityEngine.RectTransform,UnityEngine.Canvas,UnityEngine.Rect&)
		void Register_UnityEngine_RectTransformUtility_INTERNAL_CALL_PixelAdjustRect();
		Register_UnityEngine_RectTransformUtility_INTERNAL_CALL_PixelAdjustRect();

	//End Registrations for type : UnityEngine.RectTransformUtility

	//Start Registrations for type : UnityEngine.Renderer

		//System.Boolean UnityEngine.Renderer::get_enabled()
		void Register_UnityEngine_Renderer_get_enabled();
		Register_UnityEngine_Renderer_get_enabled();

		//System.Boolean UnityEngine.Renderer::get_isPartOfStaticBatch()
		void Register_UnityEngine_Renderer_get_isPartOfStaticBatch();
		Register_UnityEngine_Renderer_get_isPartOfStaticBatch();

		//System.Boolean UnityEngine.Renderer::get_isVisible()
		void Register_UnityEngine_Renderer_get_isVisible();
		Register_UnityEngine_Renderer_get_isVisible();

		//System.Boolean UnityEngine.Renderer::get_receiveShadows()
		void Register_UnityEngine_Renderer_get_receiveShadows();
		Register_UnityEngine_Renderer_get_receiveShadows();

		//System.Boolean UnityEngine.Renderer::get_useLightProbes()
		void Register_UnityEngine_Renderer_get_useLightProbes();
		Register_UnityEngine_Renderer_get_useLightProbes();

		//System.Int32 UnityEngine.Renderer::get_lightmapIndex()
		void Register_UnityEngine_Renderer_get_lightmapIndex();
		Register_UnityEngine_Renderer_get_lightmapIndex();

		//System.Int32 UnityEngine.Renderer::get_realtimeLightmapIndex()
		void Register_UnityEngine_Renderer_get_realtimeLightmapIndex();
		Register_UnityEngine_Renderer_get_realtimeLightmapIndex();

		//System.Int32 UnityEngine.Renderer::get_sortingLayerID()
		void Register_UnityEngine_Renderer_get_sortingLayerID();
		Register_UnityEngine_Renderer_get_sortingLayerID();

		//System.Int32 UnityEngine.Renderer::get_sortingOrder()
		void Register_UnityEngine_Renderer_get_sortingOrder();
		Register_UnityEngine_Renderer_get_sortingOrder();

		//System.Int32 UnityEngine.Renderer::get_staticBatchIndex()
		void Register_UnityEngine_Renderer_get_staticBatchIndex();
		Register_UnityEngine_Renderer_get_staticBatchIndex();

		//System.String UnityEngine.Renderer::get_sortingLayerName()
		void Register_UnityEngine_Renderer_get_sortingLayerName();
		Register_UnityEngine_Renderer_get_sortingLayerName();

		//System.Void UnityEngine.Renderer::GetClosestReflectionProbesInternal(System.Object)
		void Register_UnityEngine_Renderer_GetClosestReflectionProbesInternal();
		Register_UnityEngine_Renderer_GetClosestReflectionProbesInternal();

		//System.Void UnityEngine.Renderer::GetPropertyBlock(UnityEngine.MaterialPropertyBlock)
		void Register_UnityEngine_Renderer_GetPropertyBlock();
		Register_UnityEngine_Renderer_GetPropertyBlock();

		//System.Void UnityEngine.Renderer::INTERNAL_get_bounds(UnityEngine.Bounds&)
		void Register_UnityEngine_Renderer_INTERNAL_get_bounds();
		Register_UnityEngine_Renderer_INTERNAL_get_bounds();

		//System.Void UnityEngine.Renderer::INTERNAL_get_lightmapScaleOffset(UnityEngine.Vector4&)
		void Register_UnityEngine_Renderer_INTERNAL_get_lightmapScaleOffset();
		Register_UnityEngine_Renderer_INTERNAL_get_lightmapScaleOffset();

		//System.Void UnityEngine.Renderer::INTERNAL_get_localToWorldMatrix(UnityEngine.Matrix4x4&)
		void Register_UnityEngine_Renderer_INTERNAL_get_localToWorldMatrix();
		Register_UnityEngine_Renderer_INTERNAL_get_localToWorldMatrix();

		//System.Void UnityEngine.Renderer::INTERNAL_get_realtimeLightmapScaleOffset(UnityEngine.Vector4&)
		void Register_UnityEngine_Renderer_INTERNAL_get_realtimeLightmapScaleOffset();
		Register_UnityEngine_Renderer_INTERNAL_get_realtimeLightmapScaleOffset();

		//System.Void UnityEngine.Renderer::INTERNAL_get_worldToLocalMatrix(UnityEngine.Matrix4x4&)
		void Register_UnityEngine_Renderer_INTERNAL_get_worldToLocalMatrix();
		Register_UnityEngine_Renderer_INTERNAL_get_worldToLocalMatrix();

		//System.Void UnityEngine.Renderer::INTERNAL_set_lightmapScaleOffset(UnityEngine.Vector4&)
		void Register_UnityEngine_Renderer_INTERNAL_set_lightmapScaleOffset();
		Register_UnityEngine_Renderer_INTERNAL_set_lightmapScaleOffset();

		//System.Void UnityEngine.Renderer::INTERNAL_set_realtimeLightmapScaleOffset(UnityEngine.Vector4&)
		void Register_UnityEngine_Renderer_INTERNAL_set_realtimeLightmapScaleOffset();
		Register_UnityEngine_Renderer_INTERNAL_set_realtimeLightmapScaleOffset();

		//System.Void UnityEngine.Renderer::SetPropertyBlock(UnityEngine.MaterialPropertyBlock)
		void Register_UnityEngine_Renderer_SetPropertyBlock();
		Register_UnityEngine_Renderer_SetPropertyBlock();

		//System.Void UnityEngine.Renderer::SetSubsetIndex(System.Int32,System.Int32)
		void Register_UnityEngine_Renderer_SetSubsetIndex();
		Register_UnityEngine_Renderer_SetSubsetIndex();

		//System.Void UnityEngine.Renderer::set_enabled(System.Boolean)
		void Register_UnityEngine_Renderer_set_enabled();
		Register_UnityEngine_Renderer_set_enabled();

		//System.Void UnityEngine.Renderer::set_lightmapIndex(System.Int32)
		void Register_UnityEngine_Renderer_set_lightmapIndex();
		Register_UnityEngine_Renderer_set_lightmapIndex();

		//System.Void UnityEngine.Renderer::set_material(UnityEngine.Material)
		void Register_UnityEngine_Renderer_set_material();
		Register_UnityEngine_Renderer_set_material();

		//System.Void UnityEngine.Renderer::set_materials(UnityEngine.Material[])
		void Register_UnityEngine_Renderer_set_materials();
		Register_UnityEngine_Renderer_set_materials();

		//System.Void UnityEngine.Renderer::set_probeAnchor(UnityEngine.Transform)
		void Register_UnityEngine_Renderer_set_probeAnchor();
		Register_UnityEngine_Renderer_set_probeAnchor();

		//System.Void UnityEngine.Renderer::set_realtimeLightmapIndex(System.Int32)
		void Register_UnityEngine_Renderer_set_realtimeLightmapIndex();
		Register_UnityEngine_Renderer_set_realtimeLightmapIndex();

		//System.Void UnityEngine.Renderer::set_receiveShadows(System.Boolean)
		void Register_UnityEngine_Renderer_set_receiveShadows();
		Register_UnityEngine_Renderer_set_receiveShadows();

		//System.Void UnityEngine.Renderer::set_reflectionProbeUsage(UnityEngine.Rendering.ReflectionProbeUsage)
		void Register_UnityEngine_Renderer_set_reflectionProbeUsage();
		Register_UnityEngine_Renderer_set_reflectionProbeUsage();

		//System.Void UnityEngine.Renderer::set_shadowCastingMode(UnityEngine.Rendering.ShadowCastingMode)
		void Register_UnityEngine_Renderer_set_shadowCastingMode();
		Register_UnityEngine_Renderer_set_shadowCastingMode();

		//System.Void UnityEngine.Renderer::set_sharedMaterial(UnityEngine.Material)
		void Register_UnityEngine_Renderer_set_sharedMaterial();
		Register_UnityEngine_Renderer_set_sharedMaterial();

		//System.Void UnityEngine.Renderer::set_sharedMaterials(UnityEngine.Material[])
		void Register_UnityEngine_Renderer_set_sharedMaterials();
		Register_UnityEngine_Renderer_set_sharedMaterials();

		//System.Void UnityEngine.Renderer::set_sortingLayerID(System.Int32)
		void Register_UnityEngine_Renderer_set_sortingLayerID();
		Register_UnityEngine_Renderer_set_sortingLayerID();

		//System.Void UnityEngine.Renderer::set_sortingLayerName(System.String)
		void Register_UnityEngine_Renderer_set_sortingLayerName();
		Register_UnityEngine_Renderer_set_sortingLayerName();

		//System.Void UnityEngine.Renderer::set_sortingOrder(System.Int32)
		void Register_UnityEngine_Renderer_set_sortingOrder();
		Register_UnityEngine_Renderer_set_sortingOrder();

		//System.Void UnityEngine.Renderer::set_staticBatchRootTransform(UnityEngine.Transform)
		void Register_UnityEngine_Renderer_set_staticBatchRootTransform();
		Register_UnityEngine_Renderer_set_staticBatchRootTransform();

		//System.Void UnityEngine.Renderer::set_useLightProbes(System.Boolean)
		void Register_UnityEngine_Renderer_set_useLightProbes();
		Register_UnityEngine_Renderer_set_useLightProbes();

		//UnityEngine.Material UnityEngine.Renderer::get_material()
		void Register_UnityEngine_Renderer_get_material();
		Register_UnityEngine_Renderer_get_material();

		//UnityEngine.Material UnityEngine.Renderer::get_sharedMaterial()
		void Register_UnityEngine_Renderer_get_sharedMaterial();
		Register_UnityEngine_Renderer_get_sharedMaterial();

		//UnityEngine.Material[] UnityEngine.Renderer::get_materials()
		void Register_UnityEngine_Renderer_get_materials();
		Register_UnityEngine_Renderer_get_materials();

		//UnityEngine.Material[] UnityEngine.Renderer::get_sharedMaterials()
		void Register_UnityEngine_Renderer_get_sharedMaterials();
		Register_UnityEngine_Renderer_get_sharedMaterials();

		//UnityEngine.Rendering.ReflectionProbeUsage UnityEngine.Renderer::get_reflectionProbeUsage()
		void Register_UnityEngine_Renderer_get_reflectionProbeUsage();
		Register_UnityEngine_Renderer_get_reflectionProbeUsage();

		//UnityEngine.Rendering.ShadowCastingMode UnityEngine.Renderer::get_shadowCastingMode()
		void Register_UnityEngine_Renderer_get_shadowCastingMode();
		Register_UnityEngine_Renderer_get_shadowCastingMode();

		//UnityEngine.Transform UnityEngine.Renderer::get_probeAnchor()
		void Register_UnityEngine_Renderer_get_probeAnchor();
		Register_UnityEngine_Renderer_get_probeAnchor();

	//End Registrations for type : UnityEngine.Renderer

	//Start Registrations for type : UnityEngine.Rendering.CommandBuffer

		//System.Void UnityEngine.Rendering.CommandBuffer::ReleaseBuffer()
		void Register_UnityEngine_Rendering_CommandBuffer_ReleaseBuffer();
		Register_UnityEngine_Rendering_CommandBuffer_ReleaseBuffer();

	//End Registrations for type : UnityEngine.Rendering.CommandBuffer

	//Start Registrations for type : UnityEngine.RenderSettings

		//System.Boolean UnityEngine.RenderSettings::get_fog()
		void Register_UnityEngine_RenderSettings_get_fog();
		Register_UnityEngine_RenderSettings_get_fog();

		//System.Int32 UnityEngine.RenderSettings::get_defaultReflectionResolution()
		void Register_UnityEngine_RenderSettings_get_defaultReflectionResolution();
		Register_UnityEngine_RenderSettings_get_defaultReflectionResolution();

		//System.Int32 UnityEngine.RenderSettings::get_reflectionBounces()
		void Register_UnityEngine_RenderSettings_get_reflectionBounces();
		Register_UnityEngine_RenderSettings_get_reflectionBounces();

		//System.Single UnityEngine.RenderSettings::get_ambientIntensity()
		void Register_UnityEngine_RenderSettings_get_ambientIntensity();
		Register_UnityEngine_RenderSettings_get_ambientIntensity();

		//System.Single UnityEngine.RenderSettings::get_flareFadeSpeed()
		void Register_UnityEngine_RenderSettings_get_flareFadeSpeed();
		Register_UnityEngine_RenderSettings_get_flareFadeSpeed();

		//System.Single UnityEngine.RenderSettings::get_flareStrength()
		void Register_UnityEngine_RenderSettings_get_flareStrength();
		Register_UnityEngine_RenderSettings_get_flareStrength();

		//System.Single UnityEngine.RenderSettings::get_fogDensity()
		void Register_UnityEngine_RenderSettings_get_fogDensity();
		Register_UnityEngine_RenderSettings_get_fogDensity();

		//System.Single UnityEngine.RenderSettings::get_fogEndDistance()
		void Register_UnityEngine_RenderSettings_get_fogEndDistance();
		Register_UnityEngine_RenderSettings_get_fogEndDistance();

		//System.Single UnityEngine.RenderSettings::get_fogStartDistance()
		void Register_UnityEngine_RenderSettings_get_fogStartDistance();
		Register_UnityEngine_RenderSettings_get_fogStartDistance();

		//System.Single UnityEngine.RenderSettings::get_haloStrength()
		void Register_UnityEngine_RenderSettings_get_haloStrength();
		Register_UnityEngine_RenderSettings_get_haloStrength();

		//System.Single UnityEngine.RenderSettings::get_reflectionIntensity()
		void Register_UnityEngine_RenderSettings_get_reflectionIntensity();
		Register_UnityEngine_RenderSettings_get_reflectionIntensity();

		//System.Void UnityEngine.RenderSettings::INTERNAL_get_ambientEquatorColor(UnityEngine.Color&)
		void Register_UnityEngine_RenderSettings_INTERNAL_get_ambientEquatorColor();
		Register_UnityEngine_RenderSettings_INTERNAL_get_ambientEquatorColor();

		//System.Void UnityEngine.RenderSettings::INTERNAL_get_ambientGroundColor(UnityEngine.Color&)
		void Register_UnityEngine_RenderSettings_INTERNAL_get_ambientGroundColor();
		Register_UnityEngine_RenderSettings_INTERNAL_get_ambientGroundColor();

		//System.Void UnityEngine.RenderSettings::INTERNAL_get_ambientLight(UnityEngine.Color&)
		void Register_UnityEngine_RenderSettings_INTERNAL_get_ambientLight();
		Register_UnityEngine_RenderSettings_INTERNAL_get_ambientLight();

		//System.Void UnityEngine.RenderSettings::INTERNAL_get_ambientProbe(UnityEngine.Rendering.SphericalHarmonicsL2&)
		void Register_UnityEngine_RenderSettings_INTERNAL_get_ambientProbe();
		Register_UnityEngine_RenderSettings_INTERNAL_get_ambientProbe();

		//System.Void UnityEngine.RenderSettings::INTERNAL_get_ambientSkyColor(UnityEngine.Color&)
		void Register_UnityEngine_RenderSettings_INTERNAL_get_ambientSkyColor();
		Register_UnityEngine_RenderSettings_INTERNAL_get_ambientSkyColor();

		//System.Void UnityEngine.RenderSettings::INTERNAL_get_fogColor(UnityEngine.Color&)
		void Register_UnityEngine_RenderSettings_INTERNAL_get_fogColor();
		Register_UnityEngine_RenderSettings_INTERNAL_get_fogColor();

		//System.Void UnityEngine.RenderSettings::INTERNAL_set_ambientEquatorColor(UnityEngine.Color&)
		void Register_UnityEngine_RenderSettings_INTERNAL_set_ambientEquatorColor();
		Register_UnityEngine_RenderSettings_INTERNAL_set_ambientEquatorColor();

		//System.Void UnityEngine.RenderSettings::INTERNAL_set_ambientGroundColor(UnityEngine.Color&)
		void Register_UnityEngine_RenderSettings_INTERNAL_set_ambientGroundColor();
		Register_UnityEngine_RenderSettings_INTERNAL_set_ambientGroundColor();

		//System.Void UnityEngine.RenderSettings::INTERNAL_set_ambientLight(UnityEngine.Color&)
		void Register_UnityEngine_RenderSettings_INTERNAL_set_ambientLight();
		Register_UnityEngine_RenderSettings_INTERNAL_set_ambientLight();

		//System.Void UnityEngine.RenderSettings::INTERNAL_set_ambientProbe(UnityEngine.Rendering.SphericalHarmonicsL2&)
		void Register_UnityEngine_RenderSettings_INTERNAL_set_ambientProbe();
		Register_UnityEngine_RenderSettings_INTERNAL_set_ambientProbe();

		//System.Void UnityEngine.RenderSettings::INTERNAL_set_ambientSkyColor(UnityEngine.Color&)
		void Register_UnityEngine_RenderSettings_INTERNAL_set_ambientSkyColor();
		Register_UnityEngine_RenderSettings_INTERNAL_set_ambientSkyColor();

		//System.Void UnityEngine.RenderSettings::INTERNAL_set_fogColor(UnityEngine.Color&)
		void Register_UnityEngine_RenderSettings_INTERNAL_set_fogColor();
		Register_UnityEngine_RenderSettings_INTERNAL_set_fogColor();

		//System.Void UnityEngine.RenderSettings::set_ambientIntensity(System.Single)
		void Register_UnityEngine_RenderSettings_set_ambientIntensity();
		Register_UnityEngine_RenderSettings_set_ambientIntensity();

		//System.Void UnityEngine.RenderSettings::set_ambientMode(UnityEngine.Rendering.AmbientMode)
		void Register_UnityEngine_RenderSettings_set_ambientMode();
		Register_UnityEngine_RenderSettings_set_ambientMode();

		//System.Void UnityEngine.RenderSettings::set_customReflection(UnityEngine.Cubemap)
		void Register_UnityEngine_RenderSettings_set_customReflection();
		Register_UnityEngine_RenderSettings_set_customReflection();

		//System.Void UnityEngine.RenderSettings::set_defaultReflectionMode(UnityEngine.Rendering.DefaultReflectionMode)
		void Register_UnityEngine_RenderSettings_set_defaultReflectionMode();
		Register_UnityEngine_RenderSettings_set_defaultReflectionMode();

		//System.Void UnityEngine.RenderSettings::set_defaultReflectionResolution(System.Int32)
		void Register_UnityEngine_RenderSettings_set_defaultReflectionResolution();
		Register_UnityEngine_RenderSettings_set_defaultReflectionResolution();

		//System.Void UnityEngine.RenderSettings::set_flareFadeSpeed(System.Single)
		void Register_UnityEngine_RenderSettings_set_flareFadeSpeed();
		Register_UnityEngine_RenderSettings_set_flareFadeSpeed();

		//System.Void UnityEngine.RenderSettings::set_flareStrength(System.Single)
		void Register_UnityEngine_RenderSettings_set_flareStrength();
		Register_UnityEngine_RenderSettings_set_flareStrength();

		//System.Void UnityEngine.RenderSettings::set_fog(System.Boolean)
		void Register_UnityEngine_RenderSettings_set_fog();
		Register_UnityEngine_RenderSettings_set_fog();

		//System.Void UnityEngine.RenderSettings::set_fogDensity(System.Single)
		void Register_UnityEngine_RenderSettings_set_fogDensity();
		Register_UnityEngine_RenderSettings_set_fogDensity();

		//System.Void UnityEngine.RenderSettings::set_fogEndDistance(System.Single)
		void Register_UnityEngine_RenderSettings_set_fogEndDistance();
		Register_UnityEngine_RenderSettings_set_fogEndDistance();

		//System.Void UnityEngine.RenderSettings::set_fogMode(UnityEngine.FogMode)
		void Register_UnityEngine_RenderSettings_set_fogMode();
		Register_UnityEngine_RenderSettings_set_fogMode();

		//System.Void UnityEngine.RenderSettings::set_fogStartDistance(System.Single)
		void Register_UnityEngine_RenderSettings_set_fogStartDistance();
		Register_UnityEngine_RenderSettings_set_fogStartDistance();

		//System.Void UnityEngine.RenderSettings::set_haloStrength(System.Single)
		void Register_UnityEngine_RenderSettings_set_haloStrength();
		Register_UnityEngine_RenderSettings_set_haloStrength();

		//System.Void UnityEngine.RenderSettings::set_reflectionBounces(System.Int32)
		void Register_UnityEngine_RenderSettings_set_reflectionBounces();
		Register_UnityEngine_RenderSettings_set_reflectionBounces();

		//System.Void UnityEngine.RenderSettings::set_reflectionIntensity(System.Single)
		void Register_UnityEngine_RenderSettings_set_reflectionIntensity();
		Register_UnityEngine_RenderSettings_set_reflectionIntensity();

		//System.Void UnityEngine.RenderSettings::set_skybox(UnityEngine.Material)
		void Register_UnityEngine_RenderSettings_set_skybox();
		Register_UnityEngine_RenderSettings_set_skybox();

		//UnityEngine.Cubemap UnityEngine.RenderSettings::get_customReflection()
		void Register_UnityEngine_RenderSettings_get_customReflection();
		Register_UnityEngine_RenderSettings_get_customReflection();

		//UnityEngine.FogMode UnityEngine.RenderSettings::get_fogMode()
		void Register_UnityEngine_RenderSettings_get_fogMode();
		Register_UnityEngine_RenderSettings_get_fogMode();

		//UnityEngine.Material UnityEngine.RenderSettings::get_skybox()
		void Register_UnityEngine_RenderSettings_get_skybox();
		Register_UnityEngine_RenderSettings_get_skybox();

		//UnityEngine.Rendering.AmbientMode UnityEngine.RenderSettings::get_ambientMode()
		void Register_UnityEngine_RenderSettings_get_ambientMode();
		Register_UnityEngine_RenderSettings_get_ambientMode();

		//UnityEngine.Rendering.DefaultReflectionMode UnityEngine.RenderSettings::get_defaultReflectionMode()
		void Register_UnityEngine_RenderSettings_get_defaultReflectionMode();
		Register_UnityEngine_RenderSettings_get_defaultReflectionMode();

	//End Registrations for type : UnityEngine.RenderSettings

	//Start Registrations for type : UnityEngine.RenderTexture

		//System.Boolean UnityEngine.RenderTexture::INTERNAL_CALL_Create(UnityEngine.RenderTexture)
		void Register_UnityEngine_RenderTexture_INTERNAL_CALL_Create();
		Register_UnityEngine_RenderTexture_INTERNAL_CALL_Create();

		//System.Boolean UnityEngine.RenderTexture::INTERNAL_CALL_IsCreated(UnityEngine.RenderTexture)
		void Register_UnityEngine_RenderTexture_INTERNAL_CALL_IsCreated();
		Register_UnityEngine_RenderTexture_INTERNAL_CALL_IsCreated();

		//System.Boolean UnityEngine.RenderTexture::SupportsStencil(UnityEngine.RenderTexture)
		void Register_UnityEngine_RenderTexture_SupportsStencil();
		Register_UnityEngine_RenderTexture_SupportsStencil();

		//System.Boolean UnityEngine.RenderTexture::get_enableRandomWrite()
		void Register_UnityEngine_RenderTexture_get_enableRandomWrite();
		Register_UnityEngine_RenderTexture_get_enableRandomWrite();

		//System.Boolean UnityEngine.RenderTexture::get_generateMips()
		void Register_UnityEngine_RenderTexture_get_generateMips();
		Register_UnityEngine_RenderTexture_get_generateMips();

		//System.Boolean UnityEngine.RenderTexture::get_isCubemap()
		void Register_UnityEngine_RenderTexture_get_isCubemap();
		Register_UnityEngine_RenderTexture_get_isCubemap();

		//System.Boolean UnityEngine.RenderTexture::get_isPowerOfTwo()
		void Register_UnityEngine_RenderTexture_get_isPowerOfTwo();
		Register_UnityEngine_RenderTexture_get_isPowerOfTwo();

		//System.Boolean UnityEngine.RenderTexture::get_isVolume()
		void Register_UnityEngine_RenderTexture_get_isVolume();
		Register_UnityEngine_RenderTexture_get_isVolume();

		//System.Boolean UnityEngine.RenderTexture::get_sRGB()
		void Register_UnityEngine_RenderTexture_get_sRGB();
		Register_UnityEngine_RenderTexture_get_sRGB();

		//System.Boolean UnityEngine.RenderTexture::get_useMipMap()
		void Register_UnityEngine_RenderTexture_get_useMipMap();
		Register_UnityEngine_RenderTexture_get_useMipMap();

		//System.Int32 UnityEngine.RenderTexture::Internal_GetHeight(UnityEngine.RenderTexture)
		void Register_UnityEngine_RenderTexture_Internal_GetHeight();
		Register_UnityEngine_RenderTexture_Internal_GetHeight();

		//System.Int32 UnityEngine.RenderTexture::Internal_GetWidth(UnityEngine.RenderTexture)
		void Register_UnityEngine_RenderTexture_Internal_GetWidth();
		Register_UnityEngine_RenderTexture_Internal_GetWidth();

		//System.Int32 UnityEngine.RenderTexture::get_antiAliasing()
		void Register_UnityEngine_RenderTexture_get_antiAliasing();
		Register_UnityEngine_RenderTexture_get_antiAliasing();

		//System.Int32 UnityEngine.RenderTexture::get_depth()
		void Register_UnityEngine_RenderTexture_get_depth();
		Register_UnityEngine_RenderTexture_get_depth();

		//System.Int32 UnityEngine.RenderTexture::get_volumeDepth()
		void Register_UnityEngine_RenderTexture_get_volumeDepth();
		Register_UnityEngine_RenderTexture_get_volumeDepth();

		//System.Void UnityEngine.RenderTexture::DiscardContents(System.Boolean,System.Boolean)
		void Register_UnityEngine_RenderTexture_DiscardContents();
		Register_UnityEngine_RenderTexture_DiscardContents();

		//System.Void UnityEngine.RenderTexture::GetColorBuffer(UnityEngine.RenderBuffer&)
		void Register_UnityEngine_RenderTexture_GetColorBuffer();
		Register_UnityEngine_RenderTexture_GetColorBuffer();

		//System.Void UnityEngine.RenderTexture::GetDepthBuffer(UnityEngine.RenderBuffer&)
		void Register_UnityEngine_RenderTexture_GetDepthBuffer();
		Register_UnityEngine_RenderTexture_GetDepthBuffer();

		//System.Void UnityEngine.RenderTexture::INTERNAL_CALL_DiscardContents(UnityEngine.RenderTexture)
		void Register_UnityEngine_RenderTexture_INTERNAL_CALL_DiscardContents();
		Register_UnityEngine_RenderTexture_INTERNAL_CALL_DiscardContents();

		//System.Void UnityEngine.RenderTexture::INTERNAL_CALL_MarkRestoreExpected(UnityEngine.RenderTexture)
		void Register_UnityEngine_RenderTexture_INTERNAL_CALL_MarkRestoreExpected();
		Register_UnityEngine_RenderTexture_INTERNAL_CALL_MarkRestoreExpected();

		//System.Void UnityEngine.RenderTexture::INTERNAL_CALL_Release(UnityEngine.RenderTexture)
		void Register_UnityEngine_RenderTexture_INTERNAL_CALL_Release();
		Register_UnityEngine_RenderTexture_INTERNAL_CALL_Release();

		//System.Void UnityEngine.RenderTexture::Internal_CreateRenderTexture(UnityEngine.RenderTexture)
		void Register_UnityEngine_RenderTexture_Internal_CreateRenderTexture();
		Register_UnityEngine_RenderTexture_Internal_CreateRenderTexture();

		//System.Void UnityEngine.RenderTexture::Internal_GetTexelOffset(UnityEngine.RenderTexture,UnityEngine.Vector2&)
		void Register_UnityEngine_RenderTexture_Internal_GetTexelOffset();
		Register_UnityEngine_RenderTexture_Internal_GetTexelOffset();

		//System.Void UnityEngine.RenderTexture::Internal_SetHeight(UnityEngine.RenderTexture,System.Int32)
		void Register_UnityEngine_RenderTexture_Internal_SetHeight();
		Register_UnityEngine_RenderTexture_Internal_SetHeight();

		//System.Void UnityEngine.RenderTexture::Internal_SetSRGBReadWrite(UnityEngine.RenderTexture,System.Boolean)
		void Register_UnityEngine_RenderTexture_Internal_SetSRGBReadWrite();
		Register_UnityEngine_RenderTexture_Internal_SetSRGBReadWrite();

		//System.Void UnityEngine.RenderTexture::Internal_SetWidth(UnityEngine.RenderTexture,System.Int32)
		void Register_UnityEngine_RenderTexture_Internal_SetWidth();
		Register_UnityEngine_RenderTexture_Internal_SetWidth();

		//System.Void UnityEngine.RenderTexture::ReleaseTemporary(UnityEngine.RenderTexture)
		void Register_UnityEngine_RenderTexture_ReleaseTemporary();
		Register_UnityEngine_RenderTexture_ReleaseTemporary();

		//System.Void UnityEngine.RenderTexture::SetGlobalShaderProperty(System.String)
		void Register_UnityEngine_RenderTexture_SetGlobalShaderProperty();
		Register_UnityEngine_RenderTexture_SetGlobalShaderProperty();

		//System.Void UnityEngine.RenderTexture::set_active(UnityEngine.RenderTexture)
		void Register_UnityEngine_RenderTexture_set_active();
		Register_UnityEngine_RenderTexture_set_active();

		//System.Void UnityEngine.RenderTexture::set_antiAliasing(System.Int32)
		void Register_UnityEngine_RenderTexture_set_antiAliasing();
		Register_UnityEngine_RenderTexture_set_antiAliasing();

		//System.Void UnityEngine.RenderTexture::set_depth(System.Int32)
		void Register_UnityEngine_RenderTexture_set_depth();
		Register_UnityEngine_RenderTexture_set_depth();

		//System.Void UnityEngine.RenderTexture::set_enableRandomWrite(System.Boolean)
		void Register_UnityEngine_RenderTexture_set_enableRandomWrite();
		Register_UnityEngine_RenderTexture_set_enableRandomWrite();

		//System.Void UnityEngine.RenderTexture::set_format(UnityEngine.RenderTextureFormat)
		void Register_UnityEngine_RenderTexture_set_format();
		Register_UnityEngine_RenderTexture_set_format();

		//System.Void UnityEngine.RenderTexture::set_generateMips(System.Boolean)
		void Register_UnityEngine_RenderTexture_set_generateMips();
		Register_UnityEngine_RenderTexture_set_generateMips();

		//System.Void UnityEngine.RenderTexture::set_isCubemap(System.Boolean)
		void Register_UnityEngine_RenderTexture_set_isCubemap();
		Register_UnityEngine_RenderTexture_set_isCubemap();

		//System.Void UnityEngine.RenderTexture::set_isPowerOfTwo(System.Boolean)
		void Register_UnityEngine_RenderTexture_set_isPowerOfTwo();
		Register_UnityEngine_RenderTexture_set_isPowerOfTwo();

		//System.Void UnityEngine.RenderTexture::set_isVolume(System.Boolean)
		void Register_UnityEngine_RenderTexture_set_isVolume();
		Register_UnityEngine_RenderTexture_set_isVolume();

		//System.Void UnityEngine.RenderTexture::set_useMipMap(System.Boolean)
		void Register_UnityEngine_RenderTexture_set_useMipMap();
		Register_UnityEngine_RenderTexture_set_useMipMap();

		//System.Void UnityEngine.RenderTexture::set_volumeDepth(System.Int32)
		void Register_UnityEngine_RenderTexture_set_volumeDepth();
		Register_UnityEngine_RenderTexture_set_volumeDepth();

		//UnityEngine.RenderTexture UnityEngine.RenderTexture::GetTemporary(System.Int32,System.Int32,System.Int32,UnityEngine.RenderTextureFormat,UnityEngine.RenderTextureReadWrite,System.Int32)
		void Register_UnityEngine_RenderTexture_GetTemporary();
		Register_UnityEngine_RenderTexture_GetTemporary();

		//UnityEngine.RenderTexture UnityEngine.RenderTexture::get_active()
		void Register_UnityEngine_RenderTexture_get_active();
		Register_UnityEngine_RenderTexture_get_active();

		//UnityEngine.RenderTextureFormat UnityEngine.RenderTexture::get_format()
		void Register_UnityEngine_RenderTexture_get_format();
		Register_UnityEngine_RenderTexture_get_format();

	//End Registrations for type : UnityEngine.RenderTexture

	//Start Registrations for type : UnityEngine.Resources

		//System.Void UnityEngine.Resources::UnloadAsset(UnityEngine.Object)
		void Register_UnityEngine_Resources_UnloadAsset();
		Register_UnityEngine_Resources_UnloadAsset();

		//UnityEngine.AsyncOperation UnityEngine.Resources::UnloadUnusedAssets()
		void Register_UnityEngine_Resources_UnloadUnusedAssets();
		Register_UnityEngine_Resources_UnloadUnusedAssets();

		//UnityEngine.Object UnityEngine.Resources::GetBuiltinResource(System.Type,System.String)
		void Register_UnityEngine_Resources_GetBuiltinResource();
		Register_UnityEngine_Resources_GetBuiltinResource();

		//UnityEngine.Object UnityEngine.Resources::Load(System.String,System.Type)
		void Register_UnityEngine_Resources_Load();
		Register_UnityEngine_Resources_Load();

		//UnityEngine.Object[] UnityEngine.Resources::FindObjectsOfTypeAll(System.Type)
		void Register_UnityEngine_Resources_FindObjectsOfTypeAll();
		Register_UnityEngine_Resources_FindObjectsOfTypeAll();

		//UnityEngine.Object[] UnityEngine.Resources::LoadAll(System.String,System.Type)
		void Register_UnityEngine_Resources_LoadAll();
		Register_UnityEngine_Resources_LoadAll();

		//UnityEngine.ResourceRequest UnityEngine.Resources::LoadAsync(System.String,System.Type)
		void Register_UnityEngine_Resources_LoadAsync();
		Register_UnityEngine_Resources_LoadAsync();

	//End Registrations for type : UnityEngine.Resources

	//Start Registrations for type : UnityEngine.Rigidbody

		//System.Boolean UnityEngine.Rigidbody::INTERNAL_CALL_IsSleeping(UnityEngine.Rigidbody)
		void Register_UnityEngine_Rigidbody_INTERNAL_CALL_IsSleeping();
		Register_UnityEngine_Rigidbody_INTERNAL_CALL_IsSleeping();

		//System.Boolean UnityEngine.Rigidbody::INTERNAL_CALL_SweepTest(UnityEngine.Rigidbody,UnityEngine.Vector3&,UnityEngine.RaycastHit&,System.Single,UnityEngine.QueryTriggerInteraction)
		void Register_UnityEngine_Rigidbody_INTERNAL_CALL_SweepTest();
		Register_UnityEngine_Rigidbody_INTERNAL_CALL_SweepTest();

		//System.Boolean UnityEngine.Rigidbody::get_detectCollisions()
		void Register_UnityEngine_Rigidbody_get_detectCollisions();
		Register_UnityEngine_Rigidbody_get_detectCollisions();

		//System.Boolean UnityEngine.Rigidbody::get_freezeRotation()
		void Register_UnityEngine_Rigidbody_get_freezeRotation();
		Register_UnityEngine_Rigidbody_get_freezeRotation();

		//System.Boolean UnityEngine.Rigidbody::get_isKinematic()
		void Register_UnityEngine_Rigidbody_get_isKinematic();
		Register_UnityEngine_Rigidbody_get_isKinematic();

		//System.Boolean UnityEngine.Rigidbody::get_useConeFriction()
		void Register_UnityEngine_Rigidbody_get_useConeFriction();
		Register_UnityEngine_Rigidbody_get_useConeFriction();

		//System.Boolean UnityEngine.Rigidbody::get_useGravity()
		void Register_UnityEngine_Rigidbody_get_useGravity();
		Register_UnityEngine_Rigidbody_get_useGravity();

		//System.Int32 UnityEngine.Rigidbody::get_solverIterationCount()
		void Register_UnityEngine_Rigidbody_get_solverIterationCount();
		Register_UnityEngine_Rigidbody_get_solverIterationCount();

		//System.Single UnityEngine.Rigidbody::get_angularDrag()
		void Register_UnityEngine_Rigidbody_get_angularDrag();
		Register_UnityEngine_Rigidbody_get_angularDrag();

		//System.Single UnityEngine.Rigidbody::get_drag()
		void Register_UnityEngine_Rigidbody_get_drag();
		Register_UnityEngine_Rigidbody_get_drag();

		//System.Single UnityEngine.Rigidbody::get_mass()
		void Register_UnityEngine_Rigidbody_get_mass();
		Register_UnityEngine_Rigidbody_get_mass();

		//System.Single UnityEngine.Rigidbody::get_maxAngularVelocity()
		void Register_UnityEngine_Rigidbody_get_maxAngularVelocity();
		Register_UnityEngine_Rigidbody_get_maxAngularVelocity();

		//System.Single UnityEngine.Rigidbody::get_maxDepenetrationVelocity()
		void Register_UnityEngine_Rigidbody_get_maxDepenetrationVelocity();
		Register_UnityEngine_Rigidbody_get_maxDepenetrationVelocity();

		//System.Single UnityEngine.Rigidbody::get_sleepThreshold()
		void Register_UnityEngine_Rigidbody_get_sleepThreshold();
		Register_UnityEngine_Rigidbody_get_sleepThreshold();

		//System.Void UnityEngine.Rigidbody::INTERNAL_CALL_AddExplosionForce(UnityEngine.Rigidbody,System.Single,UnityEngine.Vector3&,System.Single,System.Single,UnityEngine.ForceMode)
		void Register_UnityEngine_Rigidbody_INTERNAL_CALL_AddExplosionForce();
		Register_UnityEngine_Rigidbody_INTERNAL_CALL_AddExplosionForce();

		//System.Void UnityEngine.Rigidbody::INTERNAL_CALL_AddForce(UnityEngine.Rigidbody,UnityEngine.Vector3&,UnityEngine.ForceMode)
		void Register_UnityEngine_Rigidbody_INTERNAL_CALL_AddForce();
		Register_UnityEngine_Rigidbody_INTERNAL_CALL_AddForce();

		//System.Void UnityEngine.Rigidbody::INTERNAL_CALL_AddForceAtPosition(UnityEngine.Rigidbody,UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.ForceMode)
		void Register_UnityEngine_Rigidbody_INTERNAL_CALL_AddForceAtPosition();
		Register_UnityEngine_Rigidbody_INTERNAL_CALL_AddForceAtPosition();

		//System.Void UnityEngine.Rigidbody::INTERNAL_CALL_AddRelativeForce(UnityEngine.Rigidbody,UnityEngine.Vector3&,UnityEngine.ForceMode)
		void Register_UnityEngine_Rigidbody_INTERNAL_CALL_AddRelativeForce();
		Register_UnityEngine_Rigidbody_INTERNAL_CALL_AddRelativeForce();

		//System.Void UnityEngine.Rigidbody::INTERNAL_CALL_AddRelativeTorque(UnityEngine.Rigidbody,UnityEngine.Vector3&,UnityEngine.ForceMode)
		void Register_UnityEngine_Rigidbody_INTERNAL_CALL_AddRelativeTorque();
		Register_UnityEngine_Rigidbody_INTERNAL_CALL_AddRelativeTorque();

		//System.Void UnityEngine.Rigidbody::INTERNAL_CALL_AddTorque(UnityEngine.Rigidbody,UnityEngine.Vector3&,UnityEngine.ForceMode)
		void Register_UnityEngine_Rigidbody_INTERNAL_CALL_AddTorque();
		Register_UnityEngine_Rigidbody_INTERNAL_CALL_AddTorque();

		//System.Void UnityEngine.Rigidbody::INTERNAL_CALL_ClosestPointOnBounds(UnityEngine.Rigidbody,UnityEngine.Vector3&,UnityEngine.Vector3&)
		void Register_UnityEngine_Rigidbody_INTERNAL_CALL_ClosestPointOnBounds();
		Register_UnityEngine_Rigidbody_INTERNAL_CALL_ClosestPointOnBounds();

		//System.Void UnityEngine.Rigidbody::INTERNAL_CALL_GetPointVelocity(UnityEngine.Rigidbody,UnityEngine.Vector3&,UnityEngine.Vector3&)
		void Register_UnityEngine_Rigidbody_INTERNAL_CALL_GetPointVelocity();
		Register_UnityEngine_Rigidbody_INTERNAL_CALL_GetPointVelocity();

		//System.Void UnityEngine.Rigidbody::INTERNAL_CALL_GetRelativePointVelocity(UnityEngine.Rigidbody,UnityEngine.Vector3&,UnityEngine.Vector3&)
		void Register_UnityEngine_Rigidbody_INTERNAL_CALL_GetRelativePointVelocity();
		Register_UnityEngine_Rigidbody_INTERNAL_CALL_GetRelativePointVelocity();

		//System.Void UnityEngine.Rigidbody::INTERNAL_CALL_MovePosition(UnityEngine.Rigidbody,UnityEngine.Vector3&)
		void Register_UnityEngine_Rigidbody_INTERNAL_CALL_MovePosition();
		Register_UnityEngine_Rigidbody_INTERNAL_CALL_MovePosition();

		//System.Void UnityEngine.Rigidbody::INTERNAL_CALL_MoveRotation(UnityEngine.Rigidbody,UnityEngine.Quaternion&)
		void Register_UnityEngine_Rigidbody_INTERNAL_CALL_MoveRotation();
		Register_UnityEngine_Rigidbody_INTERNAL_CALL_MoveRotation();

		//System.Void UnityEngine.Rigidbody::INTERNAL_CALL_ResetCenterOfMass(UnityEngine.Rigidbody)
		void Register_UnityEngine_Rigidbody_INTERNAL_CALL_ResetCenterOfMass();
		Register_UnityEngine_Rigidbody_INTERNAL_CALL_ResetCenterOfMass();

		//System.Void UnityEngine.Rigidbody::INTERNAL_CALL_ResetInertiaTensor(UnityEngine.Rigidbody)
		void Register_UnityEngine_Rigidbody_INTERNAL_CALL_ResetInertiaTensor();
		Register_UnityEngine_Rigidbody_INTERNAL_CALL_ResetInertiaTensor();

		//System.Void UnityEngine.Rigidbody::INTERNAL_CALL_SetDensity(UnityEngine.Rigidbody,System.Single)
		void Register_UnityEngine_Rigidbody_INTERNAL_CALL_SetDensity();
		Register_UnityEngine_Rigidbody_INTERNAL_CALL_SetDensity();

		//System.Void UnityEngine.Rigidbody::INTERNAL_CALL_Sleep(UnityEngine.Rigidbody)
		void Register_UnityEngine_Rigidbody_INTERNAL_CALL_Sleep();
		Register_UnityEngine_Rigidbody_INTERNAL_CALL_Sleep();

		//System.Void UnityEngine.Rigidbody::INTERNAL_CALL_WakeUp(UnityEngine.Rigidbody)
		void Register_UnityEngine_Rigidbody_INTERNAL_CALL_WakeUp();
		Register_UnityEngine_Rigidbody_INTERNAL_CALL_WakeUp();

		//System.Void UnityEngine.Rigidbody::INTERNAL_get_angularVelocity(UnityEngine.Vector3&)
		void Register_UnityEngine_Rigidbody_INTERNAL_get_angularVelocity();
		Register_UnityEngine_Rigidbody_INTERNAL_get_angularVelocity();

		//System.Void UnityEngine.Rigidbody::INTERNAL_get_centerOfMass(UnityEngine.Vector3&)
		void Register_UnityEngine_Rigidbody_INTERNAL_get_centerOfMass();
		Register_UnityEngine_Rigidbody_INTERNAL_get_centerOfMass();

		//System.Void UnityEngine.Rigidbody::INTERNAL_get_inertiaTensor(UnityEngine.Vector3&)
		void Register_UnityEngine_Rigidbody_INTERNAL_get_inertiaTensor();
		Register_UnityEngine_Rigidbody_INTERNAL_get_inertiaTensor();

		//System.Void UnityEngine.Rigidbody::INTERNAL_get_inertiaTensorRotation(UnityEngine.Quaternion&)
		void Register_UnityEngine_Rigidbody_INTERNAL_get_inertiaTensorRotation();
		Register_UnityEngine_Rigidbody_INTERNAL_get_inertiaTensorRotation();

		//System.Void UnityEngine.Rigidbody::INTERNAL_get_position(UnityEngine.Vector3&)
		void Register_UnityEngine_Rigidbody_INTERNAL_get_position();
		Register_UnityEngine_Rigidbody_INTERNAL_get_position();

		//System.Void UnityEngine.Rigidbody::INTERNAL_get_rotation(UnityEngine.Quaternion&)
		void Register_UnityEngine_Rigidbody_INTERNAL_get_rotation();
		Register_UnityEngine_Rigidbody_INTERNAL_get_rotation();

		//System.Void UnityEngine.Rigidbody::INTERNAL_get_velocity(UnityEngine.Vector3&)
		void Register_UnityEngine_Rigidbody_INTERNAL_get_velocity();
		Register_UnityEngine_Rigidbody_INTERNAL_get_velocity();

		//System.Void UnityEngine.Rigidbody::INTERNAL_get_worldCenterOfMass(UnityEngine.Vector3&)
		void Register_UnityEngine_Rigidbody_INTERNAL_get_worldCenterOfMass();
		Register_UnityEngine_Rigidbody_INTERNAL_get_worldCenterOfMass();

		//System.Void UnityEngine.Rigidbody::INTERNAL_set_angularVelocity(UnityEngine.Vector3&)
		void Register_UnityEngine_Rigidbody_INTERNAL_set_angularVelocity();
		Register_UnityEngine_Rigidbody_INTERNAL_set_angularVelocity();

		//System.Void UnityEngine.Rigidbody::INTERNAL_set_centerOfMass(UnityEngine.Vector3&)
		void Register_UnityEngine_Rigidbody_INTERNAL_set_centerOfMass();
		Register_UnityEngine_Rigidbody_INTERNAL_set_centerOfMass();

		//System.Void UnityEngine.Rigidbody::INTERNAL_set_inertiaTensor(UnityEngine.Vector3&)
		void Register_UnityEngine_Rigidbody_INTERNAL_set_inertiaTensor();
		Register_UnityEngine_Rigidbody_INTERNAL_set_inertiaTensor();

		//System.Void UnityEngine.Rigidbody::INTERNAL_set_inertiaTensorRotation(UnityEngine.Quaternion&)
		void Register_UnityEngine_Rigidbody_INTERNAL_set_inertiaTensorRotation();
		Register_UnityEngine_Rigidbody_INTERNAL_set_inertiaTensorRotation();

		//System.Void UnityEngine.Rigidbody::INTERNAL_set_position(UnityEngine.Vector3&)
		void Register_UnityEngine_Rigidbody_INTERNAL_set_position();
		Register_UnityEngine_Rigidbody_INTERNAL_set_position();

		//System.Void UnityEngine.Rigidbody::INTERNAL_set_rotation(UnityEngine.Quaternion&)
		void Register_UnityEngine_Rigidbody_INTERNAL_set_rotation();
		Register_UnityEngine_Rigidbody_INTERNAL_set_rotation();

		//System.Void UnityEngine.Rigidbody::INTERNAL_set_velocity(UnityEngine.Vector3&)
		void Register_UnityEngine_Rigidbody_INTERNAL_set_velocity();
		Register_UnityEngine_Rigidbody_INTERNAL_set_velocity();

		//System.Void UnityEngine.Rigidbody::set_angularDrag(System.Single)
		void Register_UnityEngine_Rigidbody_set_angularDrag();
		Register_UnityEngine_Rigidbody_set_angularDrag();

		//System.Void UnityEngine.Rigidbody::set_collisionDetectionMode(UnityEngine.CollisionDetectionMode)
		void Register_UnityEngine_Rigidbody_set_collisionDetectionMode();
		Register_UnityEngine_Rigidbody_set_collisionDetectionMode();

		//System.Void UnityEngine.Rigidbody::set_constraints(UnityEngine.RigidbodyConstraints)
		void Register_UnityEngine_Rigidbody_set_constraints();
		Register_UnityEngine_Rigidbody_set_constraints();

		//System.Void UnityEngine.Rigidbody::set_detectCollisions(System.Boolean)
		void Register_UnityEngine_Rigidbody_set_detectCollisions();
		Register_UnityEngine_Rigidbody_set_detectCollisions();

		//System.Void UnityEngine.Rigidbody::set_drag(System.Single)
		void Register_UnityEngine_Rigidbody_set_drag();
		Register_UnityEngine_Rigidbody_set_drag();

		//System.Void UnityEngine.Rigidbody::set_freezeRotation(System.Boolean)
		void Register_UnityEngine_Rigidbody_set_freezeRotation();
		Register_UnityEngine_Rigidbody_set_freezeRotation();

		//System.Void UnityEngine.Rigidbody::set_interpolation(UnityEngine.RigidbodyInterpolation)
		void Register_UnityEngine_Rigidbody_set_interpolation();
		Register_UnityEngine_Rigidbody_set_interpolation();

		//System.Void UnityEngine.Rigidbody::set_isKinematic(System.Boolean)
		void Register_UnityEngine_Rigidbody_set_isKinematic();
		Register_UnityEngine_Rigidbody_set_isKinematic();

		//System.Void UnityEngine.Rigidbody::set_mass(System.Single)
		void Register_UnityEngine_Rigidbody_set_mass();
		Register_UnityEngine_Rigidbody_set_mass();

		//System.Void UnityEngine.Rigidbody::set_maxAngularVelocity(System.Single)
		void Register_UnityEngine_Rigidbody_set_maxAngularVelocity();
		Register_UnityEngine_Rigidbody_set_maxAngularVelocity();

		//System.Void UnityEngine.Rigidbody::set_maxDepenetrationVelocity(System.Single)
		void Register_UnityEngine_Rigidbody_set_maxDepenetrationVelocity();
		Register_UnityEngine_Rigidbody_set_maxDepenetrationVelocity();

		//System.Void UnityEngine.Rigidbody::set_sleepThreshold(System.Single)
		void Register_UnityEngine_Rigidbody_set_sleepThreshold();
		Register_UnityEngine_Rigidbody_set_sleepThreshold();

		//System.Void UnityEngine.Rigidbody::set_solverIterationCount(System.Int32)
		void Register_UnityEngine_Rigidbody_set_solverIterationCount();
		Register_UnityEngine_Rigidbody_set_solverIterationCount();

		//System.Void UnityEngine.Rigidbody::set_useConeFriction(System.Boolean)
		void Register_UnityEngine_Rigidbody_set_useConeFriction();
		Register_UnityEngine_Rigidbody_set_useConeFriction();

		//System.Void UnityEngine.Rigidbody::set_useGravity(System.Boolean)
		void Register_UnityEngine_Rigidbody_set_useGravity();
		Register_UnityEngine_Rigidbody_set_useGravity();

		//UnityEngine.CollisionDetectionMode UnityEngine.Rigidbody::get_collisionDetectionMode()
		void Register_UnityEngine_Rigidbody_get_collisionDetectionMode();
		Register_UnityEngine_Rigidbody_get_collisionDetectionMode();

		//UnityEngine.RaycastHit[] UnityEngine.Rigidbody::INTERNAL_CALL_SweepTestAll(UnityEngine.Rigidbody,UnityEngine.Vector3&,System.Single,UnityEngine.QueryTriggerInteraction)
		void Register_UnityEngine_Rigidbody_INTERNAL_CALL_SweepTestAll();
		Register_UnityEngine_Rigidbody_INTERNAL_CALL_SweepTestAll();

		//UnityEngine.RigidbodyConstraints UnityEngine.Rigidbody::get_constraints()
		void Register_UnityEngine_Rigidbody_get_constraints();
		Register_UnityEngine_Rigidbody_get_constraints();

		//UnityEngine.RigidbodyInterpolation UnityEngine.Rigidbody::get_interpolation()
		void Register_UnityEngine_Rigidbody_get_interpolation();
		Register_UnityEngine_Rigidbody_get_interpolation();

	//End Registrations for type : UnityEngine.Rigidbody

	//Start Registrations for type : UnityEngine.Rigidbody2D

		//System.Boolean UnityEngine.Rigidbody2D::IsAwake()
		void Register_UnityEngine_Rigidbody2D_IsAwake();
		Register_UnityEngine_Rigidbody2D_IsAwake();

		//System.Boolean UnityEngine.Rigidbody2D::IsSleeping()
		void Register_UnityEngine_Rigidbody2D_IsSleeping();
		Register_UnityEngine_Rigidbody2D_IsSleeping();

		//System.Boolean UnityEngine.Rigidbody2D::IsTouching(UnityEngine.Collider2D)
		void Register_UnityEngine_Rigidbody2D_IsTouching();
		Register_UnityEngine_Rigidbody2D_IsTouching();

		//System.Boolean UnityEngine.Rigidbody2D::IsTouchingLayers(System.Int32)
		void Register_UnityEngine_Rigidbody2D_IsTouchingLayers();
		Register_UnityEngine_Rigidbody2D_IsTouchingLayers();

		//System.Boolean UnityEngine.Rigidbody2D::get_freezeRotation()
		void Register_UnityEngine_Rigidbody2D_get_freezeRotation();
		Register_UnityEngine_Rigidbody2D_get_freezeRotation();

		//System.Boolean UnityEngine.Rigidbody2D::get_isKinematic()
		void Register_UnityEngine_Rigidbody2D_get_isKinematic();
		Register_UnityEngine_Rigidbody2D_get_isKinematic();

		//System.Boolean UnityEngine.Rigidbody2D::get_simulated()
		void Register_UnityEngine_Rigidbody2D_get_simulated();
		Register_UnityEngine_Rigidbody2D_get_simulated();

		//System.Boolean UnityEngine.Rigidbody2D::get_useAutoMass()
		void Register_UnityEngine_Rigidbody2D_get_useAutoMass();
		Register_UnityEngine_Rigidbody2D_get_useAutoMass();

		//System.Single UnityEngine.Rigidbody2D::get_angularDrag()
		void Register_UnityEngine_Rigidbody2D_get_angularDrag();
		Register_UnityEngine_Rigidbody2D_get_angularDrag();

		//System.Single UnityEngine.Rigidbody2D::get_angularVelocity()
		void Register_UnityEngine_Rigidbody2D_get_angularVelocity();
		Register_UnityEngine_Rigidbody2D_get_angularVelocity();

		//System.Single UnityEngine.Rigidbody2D::get_drag()
		void Register_UnityEngine_Rigidbody2D_get_drag();
		Register_UnityEngine_Rigidbody2D_get_drag();

		//System.Single UnityEngine.Rigidbody2D::get_gravityScale()
		void Register_UnityEngine_Rigidbody2D_get_gravityScale();
		Register_UnityEngine_Rigidbody2D_get_gravityScale();

		//System.Single UnityEngine.Rigidbody2D::get_inertia()
		void Register_UnityEngine_Rigidbody2D_get_inertia();
		Register_UnityEngine_Rigidbody2D_get_inertia();

		//System.Single UnityEngine.Rigidbody2D::get_mass()
		void Register_UnityEngine_Rigidbody2D_get_mass();
		Register_UnityEngine_Rigidbody2D_get_mass();

		//System.Single UnityEngine.Rigidbody2D::get_rotation()
		void Register_UnityEngine_Rigidbody2D_get_rotation();
		Register_UnityEngine_Rigidbody2D_get_rotation();

		//System.Void UnityEngine.Rigidbody2D::AddTorque(System.Single,UnityEngine.ForceMode2D)
		void Register_UnityEngine_Rigidbody2D_AddTorque();
		Register_UnityEngine_Rigidbody2D_AddTorque();

		//System.Void UnityEngine.Rigidbody2D::INTERNAL_CALL_AddForce(UnityEngine.Rigidbody2D,UnityEngine.Vector2&,UnityEngine.ForceMode2D)
		void Register_UnityEngine_Rigidbody2D_INTERNAL_CALL_AddForce();
		Register_UnityEngine_Rigidbody2D_INTERNAL_CALL_AddForce();

		//System.Void UnityEngine.Rigidbody2D::INTERNAL_CALL_AddForceAtPosition(UnityEngine.Rigidbody2D,UnityEngine.Vector2&,UnityEngine.Vector2&,UnityEngine.ForceMode2D)
		void Register_UnityEngine_Rigidbody2D_INTERNAL_CALL_AddForceAtPosition();
		Register_UnityEngine_Rigidbody2D_INTERNAL_CALL_AddForceAtPosition();

		//System.Void UnityEngine.Rigidbody2D::INTERNAL_CALL_AddRelativeForce(UnityEngine.Rigidbody2D,UnityEngine.Vector2&,UnityEngine.ForceMode2D)
		void Register_UnityEngine_Rigidbody2D_INTERNAL_CALL_AddRelativeForce();
		Register_UnityEngine_Rigidbody2D_INTERNAL_CALL_AddRelativeForce();

		//System.Void UnityEngine.Rigidbody2D::INTERNAL_CALL_MovePosition(UnityEngine.Rigidbody2D,UnityEngine.Vector2&)
		void Register_UnityEngine_Rigidbody2D_INTERNAL_CALL_MovePosition();
		Register_UnityEngine_Rigidbody2D_INTERNAL_CALL_MovePosition();

		//System.Void UnityEngine.Rigidbody2D::INTERNAL_CALL_MoveRotation(UnityEngine.Rigidbody2D,System.Single)
		void Register_UnityEngine_Rigidbody2D_INTERNAL_CALL_MoveRotation();
		Register_UnityEngine_Rigidbody2D_INTERNAL_CALL_MoveRotation();

		//System.Void UnityEngine.Rigidbody2D::INTERNAL_CALL_Rigidbody2D_CUSTOM_INTERNAL_GetPoint(UnityEngine.Rigidbody2D,UnityEngine.Vector2&,UnityEngine.Vector2&)
		void Register_UnityEngine_Rigidbody2D_INTERNAL_CALL_Rigidbody2D_CUSTOM_INTERNAL_GetPoint();
		Register_UnityEngine_Rigidbody2D_INTERNAL_CALL_Rigidbody2D_CUSTOM_INTERNAL_GetPoint();

		//System.Void UnityEngine.Rigidbody2D::INTERNAL_CALL_Rigidbody2D_CUSTOM_INTERNAL_GetPointVelocity(UnityEngine.Rigidbody2D,UnityEngine.Vector2&,UnityEngine.Vector2&)
		void Register_UnityEngine_Rigidbody2D_INTERNAL_CALL_Rigidbody2D_CUSTOM_INTERNAL_GetPointVelocity();
		Register_UnityEngine_Rigidbody2D_INTERNAL_CALL_Rigidbody2D_CUSTOM_INTERNAL_GetPointVelocity();

		//System.Void UnityEngine.Rigidbody2D::INTERNAL_CALL_Rigidbody2D_CUSTOM_INTERNAL_GetRelativePoint(UnityEngine.Rigidbody2D,UnityEngine.Vector2&,UnityEngine.Vector2&)
		void Register_UnityEngine_Rigidbody2D_INTERNAL_CALL_Rigidbody2D_CUSTOM_INTERNAL_GetRelativePoint();
		Register_UnityEngine_Rigidbody2D_INTERNAL_CALL_Rigidbody2D_CUSTOM_INTERNAL_GetRelativePoint();

		//System.Void UnityEngine.Rigidbody2D::INTERNAL_CALL_Rigidbody2D_CUSTOM_INTERNAL_GetRelativePointVelocity(UnityEngine.Rigidbody2D,UnityEngine.Vector2&,UnityEngine.Vector2&)
		void Register_UnityEngine_Rigidbody2D_INTERNAL_CALL_Rigidbody2D_CUSTOM_INTERNAL_GetRelativePointVelocity();
		Register_UnityEngine_Rigidbody2D_INTERNAL_CALL_Rigidbody2D_CUSTOM_INTERNAL_GetRelativePointVelocity();

		//System.Void UnityEngine.Rigidbody2D::INTERNAL_CALL_Rigidbody2D_CUSTOM_INTERNAL_GetRelativeVector(UnityEngine.Rigidbody2D,UnityEngine.Vector2&,UnityEngine.Vector2&)
		void Register_UnityEngine_Rigidbody2D_INTERNAL_CALL_Rigidbody2D_CUSTOM_INTERNAL_GetRelativeVector();
		Register_UnityEngine_Rigidbody2D_INTERNAL_CALL_Rigidbody2D_CUSTOM_INTERNAL_GetRelativeVector();

		//System.Void UnityEngine.Rigidbody2D::INTERNAL_CALL_Rigidbody2D_CUSTOM_INTERNAL_GetVector(UnityEngine.Rigidbody2D,UnityEngine.Vector2&,UnityEngine.Vector2&)
		void Register_UnityEngine_Rigidbody2D_INTERNAL_CALL_Rigidbody2D_CUSTOM_INTERNAL_GetVector();
		Register_UnityEngine_Rigidbody2D_INTERNAL_CALL_Rigidbody2D_CUSTOM_INTERNAL_GetVector();

		//System.Void UnityEngine.Rigidbody2D::INTERNAL_get_centerOfMass(UnityEngine.Vector2&)
		void Register_UnityEngine_Rigidbody2D_INTERNAL_get_centerOfMass();
		Register_UnityEngine_Rigidbody2D_INTERNAL_get_centerOfMass();

		//System.Void UnityEngine.Rigidbody2D::INTERNAL_get_position(UnityEngine.Vector2&)
		void Register_UnityEngine_Rigidbody2D_INTERNAL_get_position();
		Register_UnityEngine_Rigidbody2D_INTERNAL_get_position();

		//System.Void UnityEngine.Rigidbody2D::INTERNAL_get_velocity(UnityEngine.Vector2&)
		void Register_UnityEngine_Rigidbody2D_INTERNAL_get_velocity();
		Register_UnityEngine_Rigidbody2D_INTERNAL_get_velocity();

		//System.Void UnityEngine.Rigidbody2D::INTERNAL_get_worldCenterOfMass(UnityEngine.Vector2&)
		void Register_UnityEngine_Rigidbody2D_INTERNAL_get_worldCenterOfMass();
		Register_UnityEngine_Rigidbody2D_INTERNAL_get_worldCenterOfMass();

		//System.Void UnityEngine.Rigidbody2D::INTERNAL_set_centerOfMass(UnityEngine.Vector2&)
		void Register_UnityEngine_Rigidbody2D_INTERNAL_set_centerOfMass();
		Register_UnityEngine_Rigidbody2D_INTERNAL_set_centerOfMass();

		//System.Void UnityEngine.Rigidbody2D::INTERNAL_set_position(UnityEngine.Vector2&)
		void Register_UnityEngine_Rigidbody2D_INTERNAL_set_position();
		Register_UnityEngine_Rigidbody2D_INTERNAL_set_position();

		//System.Void UnityEngine.Rigidbody2D::INTERNAL_set_velocity(UnityEngine.Vector2&)
		void Register_UnityEngine_Rigidbody2D_INTERNAL_set_velocity();
		Register_UnityEngine_Rigidbody2D_INTERNAL_set_velocity();

		//System.Void UnityEngine.Rigidbody2D::Sleep()
		void Register_UnityEngine_Rigidbody2D_Sleep();
		Register_UnityEngine_Rigidbody2D_Sleep();

		//System.Void UnityEngine.Rigidbody2D::WakeUp()
		void Register_UnityEngine_Rigidbody2D_WakeUp();
		Register_UnityEngine_Rigidbody2D_WakeUp();

		//System.Void UnityEngine.Rigidbody2D::set_angularDrag(System.Single)
		void Register_UnityEngine_Rigidbody2D_set_angularDrag();
		Register_UnityEngine_Rigidbody2D_set_angularDrag();

		//System.Void UnityEngine.Rigidbody2D::set_angularVelocity(System.Single)
		void Register_UnityEngine_Rigidbody2D_set_angularVelocity();
		Register_UnityEngine_Rigidbody2D_set_angularVelocity();

		//System.Void UnityEngine.Rigidbody2D::set_collisionDetectionMode(UnityEngine.CollisionDetectionMode2D)
		void Register_UnityEngine_Rigidbody2D_set_collisionDetectionMode();
		Register_UnityEngine_Rigidbody2D_set_collisionDetectionMode();

		//System.Void UnityEngine.Rigidbody2D::set_constraints(UnityEngine.RigidbodyConstraints2D)
		void Register_UnityEngine_Rigidbody2D_set_constraints();
		Register_UnityEngine_Rigidbody2D_set_constraints();

		//System.Void UnityEngine.Rigidbody2D::set_drag(System.Single)
		void Register_UnityEngine_Rigidbody2D_set_drag();
		Register_UnityEngine_Rigidbody2D_set_drag();

		//System.Void UnityEngine.Rigidbody2D::set_freezeRotation(System.Boolean)
		void Register_UnityEngine_Rigidbody2D_set_freezeRotation();
		Register_UnityEngine_Rigidbody2D_set_freezeRotation();

		//System.Void UnityEngine.Rigidbody2D::set_gravityScale(System.Single)
		void Register_UnityEngine_Rigidbody2D_set_gravityScale();
		Register_UnityEngine_Rigidbody2D_set_gravityScale();

		//System.Void UnityEngine.Rigidbody2D::set_inertia(System.Single)
		void Register_UnityEngine_Rigidbody2D_set_inertia();
		Register_UnityEngine_Rigidbody2D_set_inertia();

		//System.Void UnityEngine.Rigidbody2D::set_interpolation(UnityEngine.RigidbodyInterpolation2D)
		void Register_UnityEngine_Rigidbody2D_set_interpolation();
		Register_UnityEngine_Rigidbody2D_set_interpolation();

		//System.Void UnityEngine.Rigidbody2D::set_isKinematic(System.Boolean)
		void Register_UnityEngine_Rigidbody2D_set_isKinematic();
		Register_UnityEngine_Rigidbody2D_set_isKinematic();

		//System.Void UnityEngine.Rigidbody2D::set_mass(System.Single)
		void Register_UnityEngine_Rigidbody2D_set_mass();
		Register_UnityEngine_Rigidbody2D_set_mass();

		//System.Void UnityEngine.Rigidbody2D::set_rotation(System.Single)
		void Register_UnityEngine_Rigidbody2D_set_rotation();
		Register_UnityEngine_Rigidbody2D_set_rotation();

		//System.Void UnityEngine.Rigidbody2D::set_simulated(System.Boolean)
		void Register_UnityEngine_Rigidbody2D_set_simulated();
		Register_UnityEngine_Rigidbody2D_set_simulated();

		//System.Void UnityEngine.Rigidbody2D::set_sleepMode(UnityEngine.RigidbodySleepMode2D)
		void Register_UnityEngine_Rigidbody2D_set_sleepMode();
		Register_UnityEngine_Rigidbody2D_set_sleepMode();

		//System.Void UnityEngine.Rigidbody2D::set_useAutoMass(System.Boolean)
		void Register_UnityEngine_Rigidbody2D_set_useAutoMass();
		Register_UnityEngine_Rigidbody2D_set_useAutoMass();

		//UnityEngine.CollisionDetectionMode2D UnityEngine.Rigidbody2D::get_collisionDetectionMode()
		void Register_UnityEngine_Rigidbody2D_get_collisionDetectionMode();
		Register_UnityEngine_Rigidbody2D_get_collisionDetectionMode();

		//UnityEngine.RigidbodyConstraints2D UnityEngine.Rigidbody2D::get_constraints()
		void Register_UnityEngine_Rigidbody2D_get_constraints();
		Register_UnityEngine_Rigidbody2D_get_constraints();

		//UnityEngine.RigidbodyInterpolation2D UnityEngine.Rigidbody2D::get_interpolation()
		void Register_UnityEngine_Rigidbody2D_get_interpolation();
		Register_UnityEngine_Rigidbody2D_get_interpolation();

		//UnityEngine.RigidbodySleepMode2D UnityEngine.Rigidbody2D::get_sleepMode()
		void Register_UnityEngine_Rigidbody2D_get_sleepMode();
		Register_UnityEngine_Rigidbody2D_get_sleepMode();

	//End Registrations for type : UnityEngine.Rigidbody2D

	//Start Registrations for type : UnityEngine.RuntimeAnimatorController

		//UnityEngine.AnimationClip[] UnityEngine.RuntimeAnimatorController::get_animationClips()
		void Register_UnityEngine_RuntimeAnimatorController_get_animationClips();
		Register_UnityEngine_RuntimeAnimatorController_get_animationClips();

	//End Registrations for type : UnityEngine.RuntimeAnimatorController

	//Start Registrations for type : UnityEngine.SceneManagement.Scene

		//System.String UnityEngine.SceneManagement.Scene::GetNameInternal(System.Int32)
		void Register_UnityEngine_SceneManagement_Scene_GetNameInternal();
		Register_UnityEngine_SceneManagement_Scene_GetNameInternal();

	//End Registrations for type : UnityEngine.SceneManagement.Scene

	//Start Registrations for type : UnityEngine.SceneManagement.SceneManager

		//System.Void UnityEngine.SceneManagement.SceneManager::INTERNAL_CALL_GetActiveScene(UnityEngine.SceneManagement.Scene&)
		void Register_UnityEngine_SceneManagement_SceneManager_INTERNAL_CALL_GetActiveScene();
		Register_UnityEngine_SceneManagement_SceneManager_INTERNAL_CALL_GetActiveScene();

		//UnityEngine.AsyncOperation UnityEngine.SceneManagement.SceneManager::LoadSceneAsyncNameIndexInternal(System.String,System.Int32,System.Boolean,System.Boolean)
		void Register_UnityEngine_SceneManagement_SceneManager_LoadSceneAsyncNameIndexInternal();
		Register_UnityEngine_SceneManagement_SceneManager_LoadSceneAsyncNameIndexInternal();

	//End Registrations for type : UnityEngine.SceneManagement.SceneManager

	//Start Registrations for type : UnityEngine.Screen

		//System.Boolean UnityEngine.Screen::get_autorotateToLandscapeLeft()
		void Register_UnityEngine_Screen_get_autorotateToLandscapeLeft();
		Register_UnityEngine_Screen_get_autorotateToLandscapeLeft();

		//System.Boolean UnityEngine.Screen::get_autorotateToLandscapeRight()
		void Register_UnityEngine_Screen_get_autorotateToLandscapeRight();
		Register_UnityEngine_Screen_get_autorotateToLandscapeRight();

		//System.Boolean UnityEngine.Screen::get_autorotateToPortrait()
		void Register_UnityEngine_Screen_get_autorotateToPortrait();
		Register_UnityEngine_Screen_get_autorotateToPortrait();

		//System.Boolean UnityEngine.Screen::get_autorotateToPortraitUpsideDown()
		void Register_UnityEngine_Screen_get_autorotateToPortraitUpsideDown();
		Register_UnityEngine_Screen_get_autorotateToPortraitUpsideDown();

		//System.Boolean UnityEngine.Screen::get_fullScreen()
		void Register_UnityEngine_Screen_get_fullScreen();
		Register_UnityEngine_Screen_get_fullScreen();

		//System.Int32 UnityEngine.Screen::get_height()
		void Register_UnityEngine_Screen_get_height();
		Register_UnityEngine_Screen_get_height();

		//System.Int32 UnityEngine.Screen::get_sleepTimeout()
		void Register_UnityEngine_Screen_get_sleepTimeout();
		Register_UnityEngine_Screen_get_sleepTimeout();

		//System.Int32 UnityEngine.Screen::get_width()
		void Register_UnityEngine_Screen_get_width();
		Register_UnityEngine_Screen_get_width();

		//System.Single UnityEngine.Screen::get_dpi()
		void Register_UnityEngine_Screen_get_dpi();
		Register_UnityEngine_Screen_get_dpi();

		//System.Void UnityEngine.Screen::SetResolution(System.Int32,System.Int32,System.Boolean,System.Int32)
		void Register_UnityEngine_Screen_SetResolution();
		Register_UnityEngine_Screen_SetResolution();

		//System.Void UnityEngine.Screen::set_autorotateToLandscapeLeft(System.Boolean)
		void Register_UnityEngine_Screen_set_autorotateToLandscapeLeft();
		Register_UnityEngine_Screen_set_autorotateToLandscapeLeft();

		//System.Void UnityEngine.Screen::set_autorotateToLandscapeRight(System.Boolean)
		void Register_UnityEngine_Screen_set_autorotateToLandscapeRight();
		Register_UnityEngine_Screen_set_autorotateToLandscapeRight();

		//System.Void UnityEngine.Screen::set_autorotateToPortrait(System.Boolean)
		void Register_UnityEngine_Screen_set_autorotateToPortrait();
		Register_UnityEngine_Screen_set_autorotateToPortrait();

		//System.Void UnityEngine.Screen::set_autorotateToPortraitUpsideDown(System.Boolean)
		void Register_UnityEngine_Screen_set_autorotateToPortraitUpsideDown();
		Register_UnityEngine_Screen_set_autorotateToPortraitUpsideDown();

		//System.Void UnityEngine.Screen::set_fullScreen(System.Boolean)
		void Register_UnityEngine_Screen_set_fullScreen();
		Register_UnityEngine_Screen_set_fullScreen();

		//System.Void UnityEngine.Screen::set_orientation(UnityEngine.ScreenOrientation)
		void Register_UnityEngine_Screen_set_orientation();
		Register_UnityEngine_Screen_set_orientation();

		//System.Void UnityEngine.Screen::set_sleepTimeout(System.Int32)
		void Register_UnityEngine_Screen_set_sleepTimeout();
		Register_UnityEngine_Screen_set_sleepTimeout();

		//UnityEngine.Resolution UnityEngine.Screen::get_currentResolution()
		void Register_UnityEngine_Screen_get_currentResolution();
		Register_UnityEngine_Screen_get_currentResolution();

		//UnityEngine.Resolution[] UnityEngine.Screen::get_resolutions()
		void Register_UnityEngine_Screen_get_resolutions();
		Register_UnityEngine_Screen_get_resolutions();

		//UnityEngine.ScreenOrientation UnityEngine.Screen::get_orientation()
		void Register_UnityEngine_Screen_get_orientation();
		Register_UnityEngine_Screen_get_orientation();

	//End Registrations for type : UnityEngine.Screen

	//Start Registrations for type : UnityEngine.ScriptableObject

		//System.Void UnityEngine.ScriptableObject::Internal_CreateScriptableObject(UnityEngine.ScriptableObject)
		void Register_UnityEngine_ScriptableObject_Internal_CreateScriptableObject();
		Register_UnityEngine_ScriptableObject_Internal_CreateScriptableObject();

		//UnityEngine.ScriptableObject UnityEngine.ScriptableObject::CreateInstance(System.String)
		void Register_UnityEngine_ScriptableObject_CreateInstance();
		Register_UnityEngine_ScriptableObject_CreateInstance();

		//UnityEngine.ScriptableObject UnityEngine.ScriptableObject::CreateInstanceFromType(System.Type)
		void Register_UnityEngine_ScriptableObject_CreateInstanceFromType();
		Register_UnityEngine_ScriptableObject_CreateInstanceFromType();

	//End Registrations for type : UnityEngine.ScriptableObject

	//Start Registrations for type : UnityEngine.Shader

		//System.Boolean UnityEngine.Shader::IsKeywordEnabled(System.String)
		void Register_UnityEngine_Shader_IsKeywordEnabled();
		Register_UnityEngine_Shader_IsKeywordEnabled();

		//System.Boolean UnityEngine.Shader::get_isSupported()
		void Register_UnityEngine_Shader_get_isSupported();
		Register_UnityEngine_Shader_get_isSupported();

		//System.Int32 UnityEngine.Shader::PropertyToID(System.String)
		void Register_UnityEngine_Shader_PropertyToID();
		Register_UnityEngine_Shader_PropertyToID();

		//System.Int32 UnityEngine.Shader::get_globalMaximumLOD()
		void Register_UnityEngine_Shader_get_globalMaximumLOD();
		Register_UnityEngine_Shader_get_globalMaximumLOD();

		//System.Int32 UnityEngine.Shader::get_maximumLOD()
		void Register_UnityEngine_Shader_get_maximumLOD();
		Register_UnityEngine_Shader_get_maximumLOD();

		//System.Int32 UnityEngine.Shader::get_renderQueue()
		void Register_UnityEngine_Shader_get_renderQueue();
		Register_UnityEngine_Shader_get_renderQueue();

		//System.Void UnityEngine.Shader::DisableKeyword(System.String)
		void Register_UnityEngine_Shader_DisableKeyword();
		Register_UnityEngine_Shader_DisableKeyword();

		//System.Void UnityEngine.Shader::EnableKeyword(System.String)
		void Register_UnityEngine_Shader_EnableKeyword();
		Register_UnityEngine_Shader_EnableKeyword();

		//System.Void UnityEngine.Shader::INTERNAL_CALL_SetGlobalColor(System.Int32,UnityEngine.Color&)
		void Register_UnityEngine_Shader_INTERNAL_CALL_SetGlobalColor();
		Register_UnityEngine_Shader_INTERNAL_CALL_SetGlobalColor();

		//System.Void UnityEngine.Shader::INTERNAL_CALL_SetGlobalMatrix(System.Int32,UnityEngine.Matrix4x4&)
		void Register_UnityEngine_Shader_INTERNAL_CALL_SetGlobalMatrix();
		Register_UnityEngine_Shader_INTERNAL_CALL_SetGlobalMatrix();

		//System.Void UnityEngine.Shader::SetGlobalBuffer(System.String,UnityEngine.ComputeBuffer)
		void Register_UnityEngine_Shader_SetGlobalBuffer();
		Register_UnityEngine_Shader_SetGlobalBuffer();

		//System.Void UnityEngine.Shader::SetGlobalFloat(System.Int32,System.Single)
		void Register_UnityEngine_Shader_SetGlobalFloat();
		Register_UnityEngine_Shader_SetGlobalFloat();

		//System.Void UnityEngine.Shader::SetGlobalTexture(System.Int32,UnityEngine.Texture)
		void Register_UnityEngine_Shader_SetGlobalTexture();
		Register_UnityEngine_Shader_SetGlobalTexture();

		//System.Void UnityEngine.Shader::WarmupAllShaders()
		void Register_UnityEngine_Shader_WarmupAllShaders();
		Register_UnityEngine_Shader_WarmupAllShaders();

		//System.Void UnityEngine.Shader::set_globalMaximumLOD(System.Int32)
		void Register_UnityEngine_Shader_set_globalMaximumLOD();
		Register_UnityEngine_Shader_set_globalMaximumLOD();

		//System.Void UnityEngine.Shader::set_maximumLOD(System.Int32)
		void Register_UnityEngine_Shader_set_maximumLOD();
		Register_UnityEngine_Shader_set_maximumLOD();

		//UnityEngine.DisableBatchingType UnityEngine.Shader::get_disableBatching()
		void Register_UnityEngine_Shader_get_disableBatching();
		Register_UnityEngine_Shader_get_disableBatching();

		//UnityEngine.Shader UnityEngine.Shader::Find(System.String)
		void Register_UnityEngine_Shader_Find();
		Register_UnityEngine_Shader_Find();

	//End Registrations for type : UnityEngine.Shader

	//Start Registrations for type : UnityEngine.SkinnedMeshRenderer

		//System.Boolean UnityEngine.SkinnedMeshRenderer::get_updateWhenOffscreen()
		void Register_UnityEngine_SkinnedMeshRenderer_get_updateWhenOffscreen();
		Register_UnityEngine_SkinnedMeshRenderer_get_updateWhenOffscreen();

		//System.Single UnityEngine.SkinnedMeshRenderer::GetBlendShapeWeight(System.Int32)
		void Register_UnityEngine_SkinnedMeshRenderer_GetBlendShapeWeight();
		Register_UnityEngine_SkinnedMeshRenderer_GetBlendShapeWeight();

		//System.Void UnityEngine.SkinnedMeshRenderer::BakeMesh(UnityEngine.Mesh)
		void Register_UnityEngine_SkinnedMeshRenderer_BakeMesh();
		Register_UnityEngine_SkinnedMeshRenderer_BakeMesh();

		//System.Void UnityEngine.SkinnedMeshRenderer::INTERNAL_get_localBounds(UnityEngine.Bounds&)
		void Register_UnityEngine_SkinnedMeshRenderer_INTERNAL_get_localBounds();
		Register_UnityEngine_SkinnedMeshRenderer_INTERNAL_get_localBounds();

		//System.Void UnityEngine.SkinnedMeshRenderer::INTERNAL_set_localBounds(UnityEngine.Bounds&)
		void Register_UnityEngine_SkinnedMeshRenderer_INTERNAL_set_localBounds();
		Register_UnityEngine_SkinnedMeshRenderer_INTERNAL_set_localBounds();

		//System.Void UnityEngine.SkinnedMeshRenderer::SetBlendShapeWeight(System.Int32,System.Single)
		void Register_UnityEngine_SkinnedMeshRenderer_SetBlendShapeWeight();
		Register_UnityEngine_SkinnedMeshRenderer_SetBlendShapeWeight();

		//System.Void UnityEngine.SkinnedMeshRenderer::set_bones(UnityEngine.Transform[])
		void Register_UnityEngine_SkinnedMeshRenderer_set_bones();
		Register_UnityEngine_SkinnedMeshRenderer_set_bones();

		//System.Void UnityEngine.SkinnedMeshRenderer::set_quality(UnityEngine.SkinQuality)
		void Register_UnityEngine_SkinnedMeshRenderer_set_quality();
		Register_UnityEngine_SkinnedMeshRenderer_set_quality();

		//System.Void UnityEngine.SkinnedMeshRenderer::set_rootBone(UnityEngine.Transform)
		void Register_UnityEngine_SkinnedMeshRenderer_set_rootBone();
		Register_UnityEngine_SkinnedMeshRenderer_set_rootBone();

		//System.Void UnityEngine.SkinnedMeshRenderer::set_sharedMesh(UnityEngine.Mesh)
		void Register_UnityEngine_SkinnedMeshRenderer_set_sharedMesh();
		Register_UnityEngine_SkinnedMeshRenderer_set_sharedMesh();

		//System.Void UnityEngine.SkinnedMeshRenderer::set_updateWhenOffscreen(System.Boolean)
		void Register_UnityEngine_SkinnedMeshRenderer_set_updateWhenOffscreen();
		Register_UnityEngine_SkinnedMeshRenderer_set_updateWhenOffscreen();

		//UnityEngine.Mesh UnityEngine.SkinnedMeshRenderer::get_sharedMesh()
		void Register_UnityEngine_SkinnedMeshRenderer_get_sharedMesh();
		Register_UnityEngine_SkinnedMeshRenderer_get_sharedMesh();

		//UnityEngine.SkinQuality UnityEngine.SkinnedMeshRenderer::get_quality()
		void Register_UnityEngine_SkinnedMeshRenderer_get_quality();
		Register_UnityEngine_SkinnedMeshRenderer_get_quality();

		//UnityEngine.Transform UnityEngine.SkinnedMeshRenderer::get_rootBone()
		void Register_UnityEngine_SkinnedMeshRenderer_get_rootBone();
		Register_UnityEngine_SkinnedMeshRenderer_get_rootBone();

		//UnityEngine.Transform[] UnityEngine.SkinnedMeshRenderer::get_bones()
		void Register_UnityEngine_SkinnedMeshRenderer_get_bones();
		Register_UnityEngine_SkinnedMeshRenderer_get_bones();

	//End Registrations for type : UnityEngine.SkinnedMeshRenderer

	//Start Registrations for type : UnityEngine.Skybox

		//System.Void UnityEngine.Skybox::set_material(UnityEngine.Material)
		void Register_UnityEngine_Skybox_set_material();
		Register_UnityEngine_Skybox_set_material();

		//UnityEngine.Material UnityEngine.Skybox::get_material()
		void Register_UnityEngine_Skybox_get_material();
		Register_UnityEngine_Skybox_get_material();

	//End Registrations for type : UnityEngine.Skybox

	//Start Registrations for type : UnityEngine.SliderJoint2D

		//System.Boolean UnityEngine.SliderJoint2D::get_autoConfigureAngle()
		void Register_UnityEngine_SliderJoint2D_get_autoConfigureAngle();
		Register_UnityEngine_SliderJoint2D_get_autoConfigureAngle();

		//System.Boolean UnityEngine.SliderJoint2D::get_useLimits()
		void Register_UnityEngine_SliderJoint2D_get_useLimits();
		Register_UnityEngine_SliderJoint2D_get_useLimits();

		//System.Boolean UnityEngine.SliderJoint2D::get_useMotor()
		void Register_UnityEngine_SliderJoint2D_get_useMotor();
		Register_UnityEngine_SliderJoint2D_get_useMotor();

		//System.Single UnityEngine.SliderJoint2D::INTERNAL_CALL_GetMotorForce(UnityEngine.SliderJoint2D,System.Single)
		void Register_UnityEngine_SliderJoint2D_INTERNAL_CALL_GetMotorForce();
		Register_UnityEngine_SliderJoint2D_INTERNAL_CALL_GetMotorForce();

		//System.Single UnityEngine.SliderJoint2D::get_angle()
		void Register_UnityEngine_SliderJoint2D_get_angle();
		Register_UnityEngine_SliderJoint2D_get_angle();

		//System.Single UnityEngine.SliderJoint2D::get_jointSpeed()
		void Register_UnityEngine_SliderJoint2D_get_jointSpeed();
		Register_UnityEngine_SliderJoint2D_get_jointSpeed();

		//System.Single UnityEngine.SliderJoint2D::get_jointTranslation()
		void Register_UnityEngine_SliderJoint2D_get_jointTranslation();
		Register_UnityEngine_SliderJoint2D_get_jointTranslation();

		//System.Single UnityEngine.SliderJoint2D::get_referenceAngle()
		void Register_UnityEngine_SliderJoint2D_get_referenceAngle();
		Register_UnityEngine_SliderJoint2D_get_referenceAngle();

		//System.Void UnityEngine.SliderJoint2D::INTERNAL_get_limits(UnityEngine.JointTranslationLimits2D&)
		void Register_UnityEngine_SliderJoint2D_INTERNAL_get_limits();
		Register_UnityEngine_SliderJoint2D_INTERNAL_get_limits();

		//System.Void UnityEngine.SliderJoint2D::INTERNAL_get_motor(UnityEngine.JointMotor2D&)
		void Register_UnityEngine_SliderJoint2D_INTERNAL_get_motor();
		Register_UnityEngine_SliderJoint2D_INTERNAL_get_motor();

		//System.Void UnityEngine.SliderJoint2D::INTERNAL_set_limits(UnityEngine.JointTranslationLimits2D&)
		void Register_UnityEngine_SliderJoint2D_INTERNAL_set_limits();
		Register_UnityEngine_SliderJoint2D_INTERNAL_set_limits();

		//System.Void UnityEngine.SliderJoint2D::INTERNAL_set_motor(UnityEngine.JointMotor2D&)
		void Register_UnityEngine_SliderJoint2D_INTERNAL_set_motor();
		Register_UnityEngine_SliderJoint2D_INTERNAL_set_motor();

		//System.Void UnityEngine.SliderJoint2D::set_angle(System.Single)
		void Register_UnityEngine_SliderJoint2D_set_angle();
		Register_UnityEngine_SliderJoint2D_set_angle();

		//System.Void UnityEngine.SliderJoint2D::set_autoConfigureAngle(System.Boolean)
		void Register_UnityEngine_SliderJoint2D_set_autoConfigureAngle();
		Register_UnityEngine_SliderJoint2D_set_autoConfigureAngle();

		//System.Void UnityEngine.SliderJoint2D::set_useLimits(System.Boolean)
		void Register_UnityEngine_SliderJoint2D_set_useLimits();
		Register_UnityEngine_SliderJoint2D_set_useLimits();

		//System.Void UnityEngine.SliderJoint2D::set_useMotor(System.Boolean)
		void Register_UnityEngine_SliderJoint2D_set_useMotor();
		Register_UnityEngine_SliderJoint2D_set_useMotor();

		//UnityEngine.JointLimitState2D UnityEngine.SliderJoint2D::get_limitState()
		void Register_UnityEngine_SliderJoint2D_get_limitState();
		Register_UnityEngine_SliderJoint2D_get_limitState();

	//End Registrations for type : UnityEngine.SliderJoint2D

	//Start Registrations for type : UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform

		//System.Boolean UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_Authenticated()
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_Authenticated();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_Authenticated();

		//System.Boolean UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_Underage()
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_Underage();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_Underage();

		//System.String UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_UserID()
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_UserID();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_UserID();

		//System.String UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_UserName()
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_UserName();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_UserName();

		//System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_Authenticate()
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_Authenticate();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_Authenticate();

		//System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadAchievementDescriptions()
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_LoadAchievementDescriptions();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_LoadAchievementDescriptions();

		//System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadAchievements()
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_LoadAchievements();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_LoadAchievements();

		//System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadFriends()
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_LoadFriends();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_LoadFriends();

		//System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadScores(System.String)
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_LoadScores();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_LoadScores();

		//System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadUsers(System.String[])
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_LoadUsers();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_LoadUsers();

		//System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ReportProgress(System.String,System.Double)
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_ReportProgress();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_ReportProgress();

		//System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ReportScore(System.Int64,System.String)
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_ReportScore();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_ReportScore();

		//System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ResetAllAchievements()
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_ResetAllAchievements();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_ResetAllAchievements();

		//System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ShowAchievementsUI()
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_ShowAchievementsUI();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_ShowAchievementsUI();

		//System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ShowDefaultAchievementBanner(System.Boolean)
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_ShowDefaultAchievementBanner();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_ShowDefaultAchievementBanner();

		//System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ShowLeaderboardUI()
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_ShowLeaderboardUI();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_ShowLeaderboardUI();

		//System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ShowSpecificLeaderboardUI(System.String,System.Int32)
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_ShowSpecificLeaderboardUI();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_ShowSpecificLeaderboardUI();

		//UnityEngine.Texture2D UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_UserImage()
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_UserImage();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_UserImage();

	//End Registrations for type : UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform

	//Start Registrations for type : UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard

		//System.Boolean UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::Loading()
		void Register_UnityEngine_SocialPlatforms_GameCenter_GcLeaderboard_Loading();
		Register_UnityEngine_SocialPlatforms_GameCenter_GcLeaderboard_Loading();

		//System.Void UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::Dispose()
		void Register_UnityEngine_SocialPlatforms_GameCenter_GcLeaderboard_Dispose();
		Register_UnityEngine_SocialPlatforms_GameCenter_GcLeaderboard_Dispose();

		//System.Void UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::Internal_LoadScores(System.String,System.Int32,System.Int32,System.Int32,System.Int32)
		void Register_UnityEngine_SocialPlatforms_GameCenter_GcLeaderboard_Internal_LoadScores();
		Register_UnityEngine_SocialPlatforms_GameCenter_GcLeaderboard_Internal_LoadScores();

		//System.Void UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::Internal_LoadScoresWithUsers(System.String,System.Int32,System.String[])
		void Register_UnityEngine_SocialPlatforms_GameCenter_GcLeaderboard_Internal_LoadScoresWithUsers();
		Register_UnityEngine_SocialPlatforms_GameCenter_GcLeaderboard_Internal_LoadScoresWithUsers();

	//End Registrations for type : UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard

	//Start Registrations for type : UnityEngine.SortingLayer

		//System.Int32 UnityEngine.SortingLayer::GetLayerValueFromID(System.Int32)
		void Register_UnityEngine_SortingLayer_GetLayerValueFromID();
		Register_UnityEngine_SortingLayer_GetLayerValueFromID();

	//End Registrations for type : UnityEngine.SortingLayer

	//Start Registrations for type : UnityEngine.SparseTexture

		//System.Boolean UnityEngine.SparseTexture::get_isCreated()
		void Register_UnityEngine_SparseTexture_get_isCreated();
		Register_UnityEngine_SparseTexture_get_isCreated();

		//System.Int32 UnityEngine.SparseTexture::get_tileHeight()
		void Register_UnityEngine_SparseTexture_get_tileHeight();
		Register_UnityEngine_SparseTexture_get_tileHeight();

		//System.Int32 UnityEngine.SparseTexture::get_tileWidth()
		void Register_UnityEngine_SparseTexture_get_tileWidth();
		Register_UnityEngine_SparseTexture_get_tileWidth();

		//System.Void UnityEngine.SparseTexture::Internal_Create(UnityEngine.SparseTexture,System.Int32,System.Int32,UnityEngine.TextureFormat,System.Int32,System.Boolean)
		void Register_UnityEngine_SparseTexture_Internal_Create();
		Register_UnityEngine_SparseTexture_Internal_Create();

		//System.Void UnityEngine.SparseTexture::UnloadTile(System.Int32,System.Int32,System.Int32)
		void Register_UnityEngine_SparseTexture_UnloadTile();
		Register_UnityEngine_SparseTexture_UnloadTile();

		//System.Void UnityEngine.SparseTexture::UpdateTile(System.Int32,System.Int32,System.Int32,UnityEngine.Color32[])
		void Register_UnityEngine_SparseTexture_UpdateTile();
		Register_UnityEngine_SparseTexture_UpdateTile();

		//System.Void UnityEngine.SparseTexture::UpdateTileRaw(System.Int32,System.Int32,System.Int32,System.Byte[])
		void Register_UnityEngine_SparseTexture_UpdateTileRaw();
		Register_UnityEngine_SparseTexture_UpdateTileRaw();

	//End Registrations for type : UnityEngine.SparseTexture

	//Start Registrations for type : UnityEngine.SphereCollider

		//System.Single UnityEngine.SphereCollider::get_radius()
		void Register_UnityEngine_SphereCollider_get_radius();
		Register_UnityEngine_SphereCollider_get_radius();

		//System.Void UnityEngine.SphereCollider::INTERNAL_get_center(UnityEngine.Vector3&)
		void Register_UnityEngine_SphereCollider_INTERNAL_get_center();
		Register_UnityEngine_SphereCollider_INTERNAL_get_center();

		//System.Void UnityEngine.SphereCollider::INTERNAL_set_center(UnityEngine.Vector3&)
		void Register_UnityEngine_SphereCollider_INTERNAL_set_center();
		Register_UnityEngine_SphereCollider_INTERNAL_set_center();

		//System.Void UnityEngine.SphereCollider::set_radius(System.Single)
		void Register_UnityEngine_SphereCollider_set_radius();
		Register_UnityEngine_SphereCollider_set_radius();

	//End Registrations for type : UnityEngine.SphereCollider

	//Start Registrations for type : UnityEngine.SpringJoint

		//System.Single UnityEngine.SpringJoint::get_damper()
		void Register_UnityEngine_SpringJoint_get_damper();
		Register_UnityEngine_SpringJoint_get_damper();

		//System.Single UnityEngine.SpringJoint::get_maxDistance()
		void Register_UnityEngine_SpringJoint_get_maxDistance();
		Register_UnityEngine_SpringJoint_get_maxDistance();

		//System.Single UnityEngine.SpringJoint::get_minDistance()
		void Register_UnityEngine_SpringJoint_get_minDistance();
		Register_UnityEngine_SpringJoint_get_minDistance();

		//System.Single UnityEngine.SpringJoint::get_spring()
		void Register_UnityEngine_SpringJoint_get_spring();
		Register_UnityEngine_SpringJoint_get_spring();

		//System.Single UnityEngine.SpringJoint::get_tolerance()
		void Register_UnityEngine_SpringJoint_get_tolerance();
		Register_UnityEngine_SpringJoint_get_tolerance();

		//System.Void UnityEngine.SpringJoint::set_damper(System.Single)
		void Register_UnityEngine_SpringJoint_set_damper();
		Register_UnityEngine_SpringJoint_set_damper();

		//System.Void UnityEngine.SpringJoint::set_maxDistance(System.Single)
		void Register_UnityEngine_SpringJoint_set_maxDistance();
		Register_UnityEngine_SpringJoint_set_maxDistance();

		//System.Void UnityEngine.SpringJoint::set_minDistance(System.Single)
		void Register_UnityEngine_SpringJoint_set_minDistance();
		Register_UnityEngine_SpringJoint_set_minDistance();

		//System.Void UnityEngine.SpringJoint::set_spring(System.Single)
		void Register_UnityEngine_SpringJoint_set_spring();
		Register_UnityEngine_SpringJoint_set_spring();

		//System.Void UnityEngine.SpringJoint::set_tolerance(System.Single)
		void Register_UnityEngine_SpringJoint_set_tolerance();
		Register_UnityEngine_SpringJoint_set_tolerance();

	//End Registrations for type : UnityEngine.SpringJoint

	//Start Registrations for type : UnityEngine.SpringJoint2D

		//System.Boolean UnityEngine.SpringJoint2D::get_autoConfigureDistance()
		void Register_UnityEngine_SpringJoint2D_get_autoConfigureDistance();
		Register_UnityEngine_SpringJoint2D_get_autoConfigureDistance();

		//System.Single UnityEngine.SpringJoint2D::get_dampingRatio()
		void Register_UnityEngine_SpringJoint2D_get_dampingRatio();
		Register_UnityEngine_SpringJoint2D_get_dampingRatio();

		//System.Single UnityEngine.SpringJoint2D::get_distance()
		void Register_UnityEngine_SpringJoint2D_get_distance();
		Register_UnityEngine_SpringJoint2D_get_distance();

		//System.Single UnityEngine.SpringJoint2D::get_frequency()
		void Register_UnityEngine_SpringJoint2D_get_frequency();
		Register_UnityEngine_SpringJoint2D_get_frequency();

		//System.Void UnityEngine.SpringJoint2D::set_autoConfigureDistance(System.Boolean)
		void Register_UnityEngine_SpringJoint2D_set_autoConfigureDistance();
		Register_UnityEngine_SpringJoint2D_set_autoConfigureDistance();

		//System.Void UnityEngine.SpringJoint2D::set_dampingRatio(System.Single)
		void Register_UnityEngine_SpringJoint2D_set_dampingRatio();
		Register_UnityEngine_SpringJoint2D_set_dampingRatio();

		//System.Void UnityEngine.SpringJoint2D::set_distance(System.Single)
		void Register_UnityEngine_SpringJoint2D_set_distance();
		Register_UnityEngine_SpringJoint2D_set_distance();

		//System.Void UnityEngine.SpringJoint2D::set_frequency(System.Single)
		void Register_UnityEngine_SpringJoint2D_set_frequency();
		Register_UnityEngine_SpringJoint2D_set_frequency();

	//End Registrations for type : UnityEngine.SpringJoint2D

	//Start Registrations for type : UnityEngine.Sprite

		//System.Boolean UnityEngine.Sprite::get_packed()
		void Register_UnityEngine_Sprite_get_packed();
		Register_UnityEngine_Sprite_get_packed();

		//System.Single UnityEngine.Sprite::get_pixelsPerUnit()
		void Register_UnityEngine_Sprite_get_pixelsPerUnit();
		Register_UnityEngine_Sprite_get_pixelsPerUnit();

		//System.UInt16[] UnityEngine.Sprite::get_triangles()
		void Register_UnityEngine_Sprite_get_triangles();
		Register_UnityEngine_Sprite_get_triangles();

		//System.Void UnityEngine.Sprite::INTERNAL_get_border(UnityEngine.Vector4&)
		void Register_UnityEngine_Sprite_INTERNAL_get_border();
		Register_UnityEngine_Sprite_INTERNAL_get_border();

		//System.Void UnityEngine.Sprite::INTERNAL_get_bounds(UnityEngine.Bounds&)
		void Register_UnityEngine_Sprite_INTERNAL_get_bounds();
		Register_UnityEngine_Sprite_INTERNAL_get_bounds();

		//System.Void UnityEngine.Sprite::INTERNAL_get_rect(UnityEngine.Rect&)
		void Register_UnityEngine_Sprite_INTERNAL_get_rect();
		Register_UnityEngine_Sprite_INTERNAL_get_rect();

		//System.Void UnityEngine.Sprite::INTERNAL_get_textureRect(UnityEngine.Rect&)
		void Register_UnityEngine_Sprite_INTERNAL_get_textureRect();
		Register_UnityEngine_Sprite_INTERNAL_get_textureRect();

		//System.Void UnityEngine.Sprite::Internal_GetPivot(UnityEngine.Sprite,UnityEngine.Vector2&)
		void Register_UnityEngine_Sprite_Internal_GetPivot();
		Register_UnityEngine_Sprite_Internal_GetPivot();

		//System.Void UnityEngine.Sprite::Internal_GetTextureRectOffset(UnityEngine.Sprite,UnityEngine.Vector2&)
		void Register_UnityEngine_Sprite_Internal_GetTextureRectOffset();
		Register_UnityEngine_Sprite_Internal_GetTextureRectOffset();

		//System.Void UnityEngine.Sprite::OverrideGeometry(UnityEngine.Vector2[],System.UInt16[])
		void Register_UnityEngine_Sprite_OverrideGeometry();
		Register_UnityEngine_Sprite_OverrideGeometry();

		//UnityEngine.Sprite UnityEngine.Sprite::INTERNAL_CALL_Create(UnityEngine.Texture2D,UnityEngine.Rect&,UnityEngine.Vector2&,System.Single,System.UInt32,UnityEngine.SpriteMeshType,UnityEngine.Vector4&)
		void Register_UnityEngine_Sprite_INTERNAL_CALL_Create();
		Register_UnityEngine_Sprite_INTERNAL_CALL_Create();

		//UnityEngine.SpritePackingMode UnityEngine.Sprite::get_packingMode()
		void Register_UnityEngine_Sprite_get_packingMode();
		Register_UnityEngine_Sprite_get_packingMode();

		//UnityEngine.SpritePackingRotation UnityEngine.Sprite::get_packingRotation()
		void Register_UnityEngine_Sprite_get_packingRotation();
		Register_UnityEngine_Sprite_get_packingRotation();

		//UnityEngine.Texture2D UnityEngine.Sprite::get_associatedAlphaSplitTexture()
		void Register_UnityEngine_Sprite_get_associatedAlphaSplitTexture();
		Register_UnityEngine_Sprite_get_associatedAlphaSplitTexture();

		//UnityEngine.Texture2D UnityEngine.Sprite::get_texture()
		void Register_UnityEngine_Sprite_get_texture();
		Register_UnityEngine_Sprite_get_texture();

		//UnityEngine.Vector2[] UnityEngine.Sprite::get_uv()
		void Register_UnityEngine_Sprite_get_uv();
		Register_UnityEngine_Sprite_get_uv();

		//UnityEngine.Vector2[] UnityEngine.Sprite::get_vertices()
		void Register_UnityEngine_Sprite_get_vertices();
		Register_UnityEngine_Sprite_get_vertices();

	//End Registrations for type : UnityEngine.Sprite

	//Start Registrations for type : UnityEngine.SpriteRenderer

		//System.Boolean UnityEngine.SpriteRenderer::get_flipX()
		void Register_UnityEngine_SpriteRenderer_get_flipX();
		Register_UnityEngine_SpriteRenderer_get_flipX();

		//System.Boolean UnityEngine.SpriteRenderer::get_flipY()
		void Register_UnityEngine_SpriteRenderer_get_flipY();
		Register_UnityEngine_SpriteRenderer_get_flipY();

		//System.Void UnityEngine.SpriteRenderer::INTERNAL_get_color(UnityEngine.Color&)
		void Register_UnityEngine_SpriteRenderer_INTERNAL_get_color();
		Register_UnityEngine_SpriteRenderer_INTERNAL_get_color();

		//System.Void UnityEngine.SpriteRenderer::INTERNAL_set_color(UnityEngine.Color&)
		void Register_UnityEngine_SpriteRenderer_INTERNAL_set_color();
		Register_UnityEngine_SpriteRenderer_INTERNAL_set_color();

		//System.Void UnityEngine.SpriteRenderer::SetSprite_INTERNAL(UnityEngine.Sprite)
		void Register_UnityEngine_SpriteRenderer_SetSprite_INTERNAL();
		Register_UnityEngine_SpriteRenderer_SetSprite_INTERNAL();

		//System.Void UnityEngine.SpriteRenderer::set_flipX(System.Boolean)
		void Register_UnityEngine_SpriteRenderer_set_flipX();
		Register_UnityEngine_SpriteRenderer_set_flipX();

		//System.Void UnityEngine.SpriteRenderer::set_flipY(System.Boolean)
		void Register_UnityEngine_SpriteRenderer_set_flipY();
		Register_UnityEngine_SpriteRenderer_set_flipY();

		//UnityEngine.Sprite UnityEngine.SpriteRenderer::GetSprite_INTERNAL()
		void Register_UnityEngine_SpriteRenderer_GetSprite_INTERNAL();
		Register_UnityEngine_SpriteRenderer_GetSprite_INTERNAL();

	//End Registrations for type : UnityEngine.SpriteRenderer

	//Start Registrations for type : UnityEngine.Sprites.DataUtility

		//System.Void UnityEngine.Sprites.DataUtility::INTERNAL_CALL_GetInnerUV(UnityEngine.Sprite,UnityEngine.Vector4&)
		void Register_UnityEngine_Sprites_DataUtility_INTERNAL_CALL_GetInnerUV();
		Register_UnityEngine_Sprites_DataUtility_INTERNAL_CALL_GetInnerUV();

		//System.Void UnityEngine.Sprites.DataUtility::INTERNAL_CALL_GetOuterUV(UnityEngine.Sprite,UnityEngine.Vector4&)
		void Register_UnityEngine_Sprites_DataUtility_INTERNAL_CALL_GetOuterUV();
		Register_UnityEngine_Sprites_DataUtility_INTERNAL_CALL_GetOuterUV();

		//System.Void UnityEngine.Sprites.DataUtility::INTERNAL_CALL_GetPadding(UnityEngine.Sprite,UnityEngine.Vector4&)
		void Register_UnityEngine_Sprites_DataUtility_INTERNAL_CALL_GetPadding();
		Register_UnityEngine_Sprites_DataUtility_INTERNAL_CALL_GetPadding();

		//System.Void UnityEngine.Sprites.DataUtility::Internal_GetMinSize(UnityEngine.Sprite,UnityEngine.Vector2&)
		void Register_UnityEngine_Sprites_DataUtility_Internal_GetMinSize();
		Register_UnityEngine_Sprites_DataUtility_Internal_GetMinSize();

	//End Registrations for type : UnityEngine.Sprites.DataUtility

	//Start Registrations for type : UnityEngine.StaticBatchingUtility

		//System.Void UnityEngine.StaticBatchingUtility::InternalCombineIndices(UnityEngine.MeshSubsetCombineUtility/SubMeshInstance[],UnityEngine.Mesh)
		void Register_UnityEngine_StaticBatchingUtility_InternalCombineIndices();
		Register_UnityEngine_StaticBatchingUtility_InternalCombineIndices();

		//UnityEngine.Mesh UnityEngine.StaticBatchingUtility::InternalCombineVertices(UnityEngine.MeshSubsetCombineUtility/MeshInstance[],System.String)
		void Register_UnityEngine_StaticBatchingUtility_InternalCombineVertices();
		Register_UnityEngine_StaticBatchingUtility_InternalCombineVertices();

	//End Registrations for type : UnityEngine.StaticBatchingUtility

	//Start Registrations for type : UnityEngine.SystemInfo

		//System.Boolean UnityEngine.SystemInfo::SupportsRenderTextureFormat(UnityEngine.RenderTextureFormat)
		void Register_UnityEngine_SystemInfo_SupportsRenderTextureFormat();
		Register_UnityEngine_SystemInfo_SupportsRenderTextureFormat();

		//System.Boolean UnityEngine.SystemInfo::SupportsTextureFormat(UnityEngine.TextureFormat)
		void Register_UnityEngine_SystemInfo_SupportsTextureFormat();
		Register_UnityEngine_SystemInfo_SupportsTextureFormat();

		//System.Boolean UnityEngine.SystemInfo::get_graphicsMultiThreaded()
		void Register_UnityEngine_SystemInfo_get_graphicsMultiThreaded();
		Register_UnityEngine_SystemInfo_get_graphicsMultiThreaded();

		//System.Boolean UnityEngine.SystemInfo::get_supports3DTextures()
		void Register_UnityEngine_SystemInfo_get_supports3DTextures();
		Register_UnityEngine_SystemInfo_get_supports3DTextures();

		//System.Boolean UnityEngine.SystemInfo::get_supportsAccelerometer()
		void Register_UnityEngine_SystemInfo_get_supportsAccelerometer();
		Register_UnityEngine_SystemInfo_get_supportsAccelerometer();

		//System.Boolean UnityEngine.SystemInfo::get_supportsComputeShaders()
		void Register_UnityEngine_SystemInfo_get_supportsComputeShaders();
		Register_UnityEngine_SystemInfo_get_supportsComputeShaders();

		//System.Boolean UnityEngine.SystemInfo::get_supportsGyroscope()
		void Register_UnityEngine_SystemInfo_get_supportsGyroscope();
		Register_UnityEngine_SystemInfo_get_supportsGyroscope();

		//System.Boolean UnityEngine.SystemInfo::get_supportsImageEffects()
		void Register_UnityEngine_SystemInfo_get_supportsImageEffects();
		Register_UnityEngine_SystemInfo_get_supportsImageEffects();

		//System.Boolean UnityEngine.SystemInfo::get_supportsInstancing()
		void Register_UnityEngine_SystemInfo_get_supportsInstancing();
		Register_UnityEngine_SystemInfo_get_supportsInstancing();

		//System.Boolean UnityEngine.SystemInfo::get_supportsLocationService()
		void Register_UnityEngine_SystemInfo_get_supportsLocationService();
		Register_UnityEngine_SystemInfo_get_supportsLocationService();

		//System.Boolean UnityEngine.SystemInfo::get_supportsRawShadowDepthSampling()
		void Register_UnityEngine_SystemInfo_get_supportsRawShadowDepthSampling();
		Register_UnityEngine_SystemInfo_get_supportsRawShadowDepthSampling();

		//System.Boolean UnityEngine.SystemInfo::get_supportsRenderTextures()
		void Register_UnityEngine_SystemInfo_get_supportsRenderTextures();
		Register_UnityEngine_SystemInfo_get_supportsRenderTextures();

		//System.Boolean UnityEngine.SystemInfo::get_supportsRenderToCubemap()
		void Register_UnityEngine_SystemInfo_get_supportsRenderToCubemap();
		Register_UnityEngine_SystemInfo_get_supportsRenderToCubemap();

		//System.Boolean UnityEngine.SystemInfo::get_supportsShadows()
		void Register_UnityEngine_SystemInfo_get_supportsShadows();
		Register_UnityEngine_SystemInfo_get_supportsShadows();

		//System.Boolean UnityEngine.SystemInfo::get_supportsSparseTextures()
		void Register_UnityEngine_SystemInfo_get_supportsSparseTextures();
		Register_UnityEngine_SystemInfo_get_supportsSparseTextures();

		//System.Boolean UnityEngine.SystemInfo::get_supportsVibration()
		void Register_UnityEngine_SystemInfo_get_supportsVibration();
		Register_UnityEngine_SystemInfo_get_supportsVibration();

		//System.Int32 UnityEngine.SystemInfo::get_graphicsDeviceID()
		void Register_UnityEngine_SystemInfo_get_graphicsDeviceID();
		Register_UnityEngine_SystemInfo_get_graphicsDeviceID();

		//System.Int32 UnityEngine.SystemInfo::get_graphicsDeviceVendorID()
		void Register_UnityEngine_SystemInfo_get_graphicsDeviceVendorID();
		Register_UnityEngine_SystemInfo_get_graphicsDeviceVendorID();

		//System.Int32 UnityEngine.SystemInfo::get_graphicsMemorySize()
		void Register_UnityEngine_SystemInfo_get_graphicsMemorySize();
		Register_UnityEngine_SystemInfo_get_graphicsMemorySize();

		//System.Int32 UnityEngine.SystemInfo::get_graphicsShaderLevel()
		void Register_UnityEngine_SystemInfo_get_graphicsShaderLevel();
		Register_UnityEngine_SystemInfo_get_graphicsShaderLevel();

		//System.Int32 UnityEngine.SystemInfo::get_maxTextureSize()
		void Register_UnityEngine_SystemInfo_get_maxTextureSize();
		Register_UnityEngine_SystemInfo_get_maxTextureSize();

		//System.Int32 UnityEngine.SystemInfo::get_processorCount()
		void Register_UnityEngine_SystemInfo_get_processorCount();
		Register_UnityEngine_SystemInfo_get_processorCount();

		//System.Int32 UnityEngine.SystemInfo::get_processorFrequency()
		void Register_UnityEngine_SystemInfo_get_processorFrequency();
		Register_UnityEngine_SystemInfo_get_processorFrequency();

		//System.Int32 UnityEngine.SystemInfo::get_supportedRenderTargetCount()
		void Register_UnityEngine_SystemInfo_get_supportedRenderTargetCount();
		Register_UnityEngine_SystemInfo_get_supportedRenderTargetCount();

		//System.Int32 UnityEngine.SystemInfo::get_supportsStencil()
		void Register_UnityEngine_SystemInfo_get_supportsStencil();
		Register_UnityEngine_SystemInfo_get_supportsStencil();

		//System.Int32 UnityEngine.SystemInfo::get_systemMemorySize()
		void Register_UnityEngine_SystemInfo_get_systemMemorySize();
		Register_UnityEngine_SystemInfo_get_systemMemorySize();

		//System.String UnityEngine.SystemInfo::get_deviceModel()
		void Register_UnityEngine_SystemInfo_get_deviceModel();
		Register_UnityEngine_SystemInfo_get_deviceModel();

		//System.String UnityEngine.SystemInfo::get_deviceName()
		void Register_UnityEngine_SystemInfo_get_deviceName();
		Register_UnityEngine_SystemInfo_get_deviceName();

		//System.String UnityEngine.SystemInfo::get_deviceUniqueIdentifier()
		void Register_UnityEngine_SystemInfo_get_deviceUniqueIdentifier();
		Register_UnityEngine_SystemInfo_get_deviceUniqueIdentifier();

		//System.String UnityEngine.SystemInfo::get_graphicsDeviceName()
		void Register_UnityEngine_SystemInfo_get_graphicsDeviceName();
		Register_UnityEngine_SystemInfo_get_graphicsDeviceName();

		//System.String UnityEngine.SystemInfo::get_graphicsDeviceVendor()
		void Register_UnityEngine_SystemInfo_get_graphicsDeviceVendor();
		Register_UnityEngine_SystemInfo_get_graphicsDeviceVendor();

		//System.String UnityEngine.SystemInfo::get_graphicsDeviceVersion()
		void Register_UnityEngine_SystemInfo_get_graphicsDeviceVersion();
		Register_UnityEngine_SystemInfo_get_graphicsDeviceVersion();

		//System.String UnityEngine.SystemInfo::get_operatingSystem()
		void Register_UnityEngine_SystemInfo_get_operatingSystem();
		Register_UnityEngine_SystemInfo_get_operatingSystem();

		//System.String UnityEngine.SystemInfo::get_processorType()
		void Register_UnityEngine_SystemInfo_get_processorType();
		Register_UnityEngine_SystemInfo_get_processorType();

		//UnityEngine.DeviceType UnityEngine.SystemInfo::get_deviceType()
		void Register_UnityEngine_SystemInfo_get_deviceType();
		Register_UnityEngine_SystemInfo_get_deviceType();

		//UnityEngine.NPOTSupport UnityEngine.SystemInfo::get_npotSupport()
		void Register_UnityEngine_SystemInfo_get_npotSupport();
		Register_UnityEngine_SystemInfo_get_npotSupport();

		//UnityEngine.Rendering.GraphicsDeviceType UnityEngine.SystemInfo::get_graphicsDeviceType()
		void Register_UnityEngine_SystemInfo_get_graphicsDeviceType();
		Register_UnityEngine_SystemInfo_get_graphicsDeviceType();

	//End Registrations for type : UnityEngine.SystemInfo

	//Start Registrations for type : UnityEngine.Terrain

		//System.Void UnityEngine.Terrain::INTERNAL_CALL_GetPosition(UnityEngine.Terrain,UnityEngine.Vector3&)
		void Register_UnityEngine_Terrain_INTERNAL_CALL_GetPosition();
		Register_UnityEngine_Terrain_INTERNAL_CALL_GetPosition();

		//UnityEngine.TerrainData UnityEngine.Terrain::get_terrainData()
		void Register_UnityEngine_Terrain_get_terrainData();
		Register_UnityEngine_Terrain_get_terrainData();

	//End Registrations for type : UnityEngine.Terrain

	//Start Registrations for type : UnityEngine.TerrainCollider

		//System.Void UnityEngine.TerrainCollider::set_terrainData(UnityEngine.TerrainData)
		void Register_UnityEngine_TerrainCollider_set_terrainData();
		Register_UnityEngine_TerrainCollider_set_terrainData();

		//UnityEngine.TerrainData UnityEngine.TerrainCollider::get_terrainData()
		void Register_UnityEngine_TerrainCollider_get_terrainData();
		Register_UnityEngine_TerrainCollider_get_terrainData();

	//End Registrations for type : UnityEngine.TerrainCollider

	//Start Registrations for type : UnityEngine.TerrainData

		//System.Int32 UnityEngine.TerrainData::Internal_GetMaximumAlphamapResolution()
		void Register_UnityEngine_TerrainData_Internal_GetMaximumAlphamapResolution();
		Register_UnityEngine_TerrainData_Internal_GetMaximumAlphamapResolution();

		//System.Int32 UnityEngine.TerrainData::Internal_GetMaximumBaseMapResolution()
		void Register_UnityEngine_TerrainData_Internal_GetMaximumBaseMapResolution();
		Register_UnityEngine_TerrainData_Internal_GetMaximumBaseMapResolution();

		//System.Int32 UnityEngine.TerrainData::Internal_GetMaximumDetailPatchCount()
		void Register_UnityEngine_TerrainData_Internal_GetMaximumDetailPatchCount();
		Register_UnityEngine_TerrainData_Internal_GetMaximumDetailPatchCount();

		//System.Int32 UnityEngine.TerrainData::Internal_GetMaximumDetailResolutionPerPatch()
		void Register_UnityEngine_TerrainData_Internal_GetMaximumDetailResolutionPerPatch();
		Register_UnityEngine_TerrainData_Internal_GetMaximumDetailResolutionPerPatch();

		//System.Int32 UnityEngine.TerrainData::Internal_GetMaximumResolution()
		void Register_UnityEngine_TerrainData_Internal_GetMaximumResolution();
		Register_UnityEngine_TerrainData_Internal_GetMaximumResolution();

		//System.Int32 UnityEngine.TerrainData::Internal_GetMinimumAlphamapResolution()
		void Register_UnityEngine_TerrainData_Internal_GetMinimumAlphamapResolution();
		Register_UnityEngine_TerrainData_Internal_GetMinimumAlphamapResolution();

		//System.Int32 UnityEngine.TerrainData::Internal_GetMinimumBaseMapResolution()
		void Register_UnityEngine_TerrainData_Internal_GetMinimumBaseMapResolution();
		Register_UnityEngine_TerrainData_Internal_GetMinimumBaseMapResolution();

		//System.Int32 UnityEngine.TerrainData::Internal_GetMinimumDetailResolutionPerPatch()
		void Register_UnityEngine_TerrainData_Internal_GetMinimumDetailResolutionPerPatch();
		Register_UnityEngine_TerrainData_Internal_GetMinimumDetailResolutionPerPatch();

		//System.Int32 UnityEngine.TerrainData::get_heightmapHeight()
		void Register_UnityEngine_TerrainData_get_heightmapHeight();
		Register_UnityEngine_TerrainData_get_heightmapHeight();

		//System.Int32 UnityEngine.TerrainData::get_heightmapWidth()
		void Register_UnityEngine_TerrainData_get_heightmapWidth();
		Register_UnityEngine_TerrainData_get_heightmapWidth();

		//System.Single[,] UnityEngine.TerrainData::GetHeights(System.Int32,System.Int32,System.Int32,System.Int32)
		void Register_UnityEngine_TerrainData_GetHeights();
		Register_UnityEngine_TerrainData_GetHeights();

		//System.Void UnityEngine.TerrainData::INTERNAL_get_heightmapScale(UnityEngine.Vector3&)
		void Register_UnityEngine_TerrainData_INTERNAL_get_heightmapScale();
		Register_UnityEngine_TerrainData_INTERNAL_get_heightmapScale();

		//System.Void UnityEngine.TerrainData::INTERNAL_get_size(UnityEngine.Vector3&)
		void Register_UnityEngine_TerrainData_INTERNAL_get_size();
		Register_UnityEngine_TerrainData_INTERNAL_get_size();

		//UnityEngine.TreeInstance[] UnityEngine.TerrainData::get_treeInstances()
		void Register_UnityEngine_TerrainData_get_treeInstances();
		Register_UnityEngine_TerrainData_get_treeInstances();

		//UnityEngine.TreePrototype[] UnityEngine.TerrainData::get_treePrototypes()
		void Register_UnityEngine_TerrainData_get_treePrototypes();
		Register_UnityEngine_TerrainData_get_treePrototypes();

	//End Registrations for type : UnityEngine.TerrainData

	//Start Registrations for type : UnityEngine.TextAsset

		//System.Byte[] UnityEngine.TextAsset::get_bytes()
		void Register_UnityEngine_TextAsset_get_bytes();
		Register_UnityEngine_TextAsset_get_bytes();

		//System.String UnityEngine.TextAsset::get_text()
		void Register_UnityEngine_TextAsset_get_text();
		Register_UnityEngine_TextAsset_get_text();

	//End Registrations for type : UnityEngine.TextAsset

	//Start Registrations for type : UnityEngine.TextGenerator

		//System.Boolean UnityEngine.TextGenerator::INTERNAL_CALL_Populate_Internal_cpp(UnityEngine.TextGenerator,System.String,UnityEngine.Font,UnityEngine.Color&,System.Int32,System.Single,System.Single,UnityEngine.FontStyle,System.Boolean,System.Boolean,System.Int32,System.Int32,System.Int32,System.Int32,System.Boolean,UnityEngine.TextAnchor,System.Single,System.Single,System.Single,System.Single,System.Boolean,System.Boolean)
		void Register_UnityEngine_TextGenerator_INTERNAL_CALL_Populate_Internal_cpp();
		Register_UnityEngine_TextGenerator_INTERNAL_CALL_Populate_Internal_cpp();

		//System.Int32 UnityEngine.TextGenerator::get_characterCount()
		void Register_UnityEngine_TextGenerator_get_characterCount();
		Register_UnityEngine_TextGenerator_get_characterCount();

		//System.Int32 UnityEngine.TextGenerator::get_fontSizeUsedForBestFit()
		void Register_UnityEngine_TextGenerator_get_fontSizeUsedForBestFit();
		Register_UnityEngine_TextGenerator_get_fontSizeUsedForBestFit();

		//System.Int32 UnityEngine.TextGenerator::get_lineCount()
		void Register_UnityEngine_TextGenerator_get_lineCount();
		Register_UnityEngine_TextGenerator_get_lineCount();

		//System.Int32 UnityEngine.TextGenerator::get_vertexCount()
		void Register_UnityEngine_TextGenerator_get_vertexCount();
		Register_UnityEngine_TextGenerator_get_vertexCount();

		//System.Void UnityEngine.TextGenerator::Dispose_cpp()
		void Register_UnityEngine_TextGenerator_Dispose_cpp();
		Register_UnityEngine_TextGenerator_Dispose_cpp();

		//System.Void UnityEngine.TextGenerator::GetCharactersInternal(System.Object)
		void Register_UnityEngine_TextGenerator_GetCharactersInternal();
		Register_UnityEngine_TextGenerator_GetCharactersInternal();

		//System.Void UnityEngine.TextGenerator::GetLinesInternal(System.Object)
		void Register_UnityEngine_TextGenerator_GetLinesInternal();
		Register_UnityEngine_TextGenerator_GetLinesInternal();

		//System.Void UnityEngine.TextGenerator::GetVerticesInternal(System.Object)
		void Register_UnityEngine_TextGenerator_GetVerticesInternal();
		Register_UnityEngine_TextGenerator_GetVerticesInternal();

		//System.Void UnityEngine.TextGenerator::INTERNAL_get_rectExtents(UnityEngine.Rect&)
		void Register_UnityEngine_TextGenerator_INTERNAL_get_rectExtents();
		Register_UnityEngine_TextGenerator_INTERNAL_get_rectExtents();

		//System.Void UnityEngine.TextGenerator::Init()
		void Register_UnityEngine_TextGenerator_Init();
		Register_UnityEngine_TextGenerator_Init();

		//UnityEngine.UICharInfo[] UnityEngine.TextGenerator::GetCharactersArray()
		void Register_UnityEngine_TextGenerator_GetCharactersArray();
		Register_UnityEngine_TextGenerator_GetCharactersArray();

		//UnityEngine.UILineInfo[] UnityEngine.TextGenerator::GetLinesArray()
		void Register_UnityEngine_TextGenerator_GetLinesArray();
		Register_UnityEngine_TextGenerator_GetLinesArray();

		//UnityEngine.UIVertex[] UnityEngine.TextGenerator::GetVerticesArray()
		void Register_UnityEngine_TextGenerator_GetVerticesArray();
		Register_UnityEngine_TextGenerator_GetVerticesArray();

	//End Registrations for type : UnityEngine.TextGenerator

	//Start Registrations for type : UnityEngine.TextMesh

		//System.Boolean UnityEngine.TextMesh::get_richText()
		void Register_UnityEngine_TextMesh_get_richText();
		Register_UnityEngine_TextMesh_get_richText();

		//System.Int32 UnityEngine.TextMesh::get_fontSize()
		void Register_UnityEngine_TextMesh_get_fontSize();
		Register_UnityEngine_TextMesh_get_fontSize();

		//System.Single UnityEngine.TextMesh::get_characterSize()
		void Register_UnityEngine_TextMesh_get_characterSize();
		Register_UnityEngine_TextMesh_get_characterSize();

		//System.Single UnityEngine.TextMesh::get_lineSpacing()
		void Register_UnityEngine_TextMesh_get_lineSpacing();
		Register_UnityEngine_TextMesh_get_lineSpacing();

		//System.Single UnityEngine.TextMesh::get_offsetZ()
		void Register_UnityEngine_TextMesh_get_offsetZ();
		Register_UnityEngine_TextMesh_get_offsetZ();

		//System.Single UnityEngine.TextMesh::get_tabSize()
		void Register_UnityEngine_TextMesh_get_tabSize();
		Register_UnityEngine_TextMesh_get_tabSize();

		//System.String UnityEngine.TextMesh::get_text()
		void Register_UnityEngine_TextMesh_get_text();
		Register_UnityEngine_TextMesh_get_text();

		//System.Void UnityEngine.TextMesh::INTERNAL_get_color(UnityEngine.Color&)
		void Register_UnityEngine_TextMesh_INTERNAL_get_color();
		Register_UnityEngine_TextMesh_INTERNAL_get_color();

		//System.Void UnityEngine.TextMesh::INTERNAL_set_color(UnityEngine.Color&)
		void Register_UnityEngine_TextMesh_INTERNAL_set_color();
		Register_UnityEngine_TextMesh_INTERNAL_set_color();

		//System.Void UnityEngine.TextMesh::set_alignment(UnityEngine.TextAlignment)
		void Register_UnityEngine_TextMesh_set_alignment();
		Register_UnityEngine_TextMesh_set_alignment();

		//System.Void UnityEngine.TextMesh::set_anchor(UnityEngine.TextAnchor)
		void Register_UnityEngine_TextMesh_set_anchor();
		Register_UnityEngine_TextMesh_set_anchor();

		//System.Void UnityEngine.TextMesh::set_characterSize(System.Single)
		void Register_UnityEngine_TextMesh_set_characterSize();
		Register_UnityEngine_TextMesh_set_characterSize();

		//System.Void UnityEngine.TextMesh::set_font(UnityEngine.Font)
		void Register_UnityEngine_TextMesh_set_font();
		Register_UnityEngine_TextMesh_set_font();

		//System.Void UnityEngine.TextMesh::set_fontSize(System.Int32)
		void Register_UnityEngine_TextMesh_set_fontSize();
		Register_UnityEngine_TextMesh_set_fontSize();

		//System.Void UnityEngine.TextMesh::set_fontStyle(UnityEngine.FontStyle)
		void Register_UnityEngine_TextMesh_set_fontStyle();
		Register_UnityEngine_TextMesh_set_fontStyle();

		//System.Void UnityEngine.TextMesh::set_lineSpacing(System.Single)
		void Register_UnityEngine_TextMesh_set_lineSpacing();
		Register_UnityEngine_TextMesh_set_lineSpacing();

		//System.Void UnityEngine.TextMesh::set_offsetZ(System.Single)
		void Register_UnityEngine_TextMesh_set_offsetZ();
		Register_UnityEngine_TextMesh_set_offsetZ();

		//System.Void UnityEngine.TextMesh::set_richText(System.Boolean)
		void Register_UnityEngine_TextMesh_set_richText();
		Register_UnityEngine_TextMesh_set_richText();

		//System.Void UnityEngine.TextMesh::set_tabSize(System.Single)
		void Register_UnityEngine_TextMesh_set_tabSize();
		Register_UnityEngine_TextMesh_set_tabSize();

		//System.Void UnityEngine.TextMesh::set_text(System.String)
		void Register_UnityEngine_TextMesh_set_text();
		Register_UnityEngine_TextMesh_set_text();

		//UnityEngine.Font UnityEngine.TextMesh::get_font()
		void Register_UnityEngine_TextMesh_get_font();
		Register_UnityEngine_TextMesh_get_font();

		//UnityEngine.FontStyle UnityEngine.TextMesh::get_fontStyle()
		void Register_UnityEngine_TextMesh_get_fontStyle();
		Register_UnityEngine_TextMesh_get_fontStyle();

		//UnityEngine.TextAlignment UnityEngine.TextMesh::get_alignment()
		void Register_UnityEngine_TextMesh_get_alignment();
		Register_UnityEngine_TextMesh_get_alignment();

		//UnityEngine.TextAnchor UnityEngine.TextMesh::get_anchor()
		void Register_UnityEngine_TextMesh_get_anchor();
		Register_UnityEngine_TextMesh_get_anchor();

	//End Registrations for type : UnityEngine.TextMesh

	//Start Registrations for type : UnityEngine.Texture

		//System.Int32 UnityEngine.Texture::Internal_GetHeight(UnityEngine.Texture)
		void Register_UnityEngine_Texture_Internal_GetHeight();
		Register_UnityEngine_Texture_Internal_GetHeight();

		//System.Int32 UnityEngine.Texture::Internal_GetWidth(UnityEngine.Texture)
		void Register_UnityEngine_Texture_Internal_GetWidth();
		Register_UnityEngine_Texture_Internal_GetWidth();

		//System.Int32 UnityEngine.Texture::get_anisoLevel()
		void Register_UnityEngine_Texture_get_anisoLevel();
		Register_UnityEngine_Texture_get_anisoLevel();

		//System.Int32 UnityEngine.Texture::get_masterTextureLimit()
		void Register_UnityEngine_Texture_get_masterTextureLimit();
		Register_UnityEngine_Texture_get_masterTextureLimit();

		//System.IntPtr UnityEngine.Texture::GetNativeTexturePtr()
		void Register_UnityEngine_Texture_GetNativeTexturePtr();
		Register_UnityEngine_Texture_GetNativeTexturePtr();

		//System.Single UnityEngine.Texture::get_mipMapBias()
		void Register_UnityEngine_Texture_get_mipMapBias();
		Register_UnityEngine_Texture_get_mipMapBias();

		//System.Void UnityEngine.Texture::INTERNAL_get_texelSize(UnityEngine.Vector2&)
		void Register_UnityEngine_Texture_INTERNAL_get_texelSize();
		Register_UnityEngine_Texture_INTERNAL_get_texelSize();

		//System.Void UnityEngine.Texture::SetGlobalAnisotropicFilteringLimits(System.Int32,System.Int32)
		void Register_UnityEngine_Texture_SetGlobalAnisotropicFilteringLimits();
		Register_UnityEngine_Texture_SetGlobalAnisotropicFilteringLimits();

		//System.Void UnityEngine.Texture::set_anisoLevel(System.Int32)
		void Register_UnityEngine_Texture_set_anisoLevel();
		Register_UnityEngine_Texture_set_anisoLevel();

		//System.Void UnityEngine.Texture::set_anisotropicFiltering(UnityEngine.AnisotropicFiltering)
		void Register_UnityEngine_Texture_set_anisotropicFiltering();
		Register_UnityEngine_Texture_set_anisotropicFiltering();

		//System.Void UnityEngine.Texture::set_filterMode(UnityEngine.FilterMode)
		void Register_UnityEngine_Texture_set_filterMode();
		Register_UnityEngine_Texture_set_filterMode();

		//System.Void UnityEngine.Texture::set_masterTextureLimit(System.Int32)
		void Register_UnityEngine_Texture_set_masterTextureLimit();
		Register_UnityEngine_Texture_set_masterTextureLimit();

		//System.Void UnityEngine.Texture::set_mipMapBias(System.Single)
		void Register_UnityEngine_Texture_set_mipMapBias();
		Register_UnityEngine_Texture_set_mipMapBias();

		//System.Void UnityEngine.Texture::set_wrapMode(UnityEngine.TextureWrapMode)
		void Register_UnityEngine_Texture_set_wrapMode();
		Register_UnityEngine_Texture_set_wrapMode();

		//UnityEngine.AnisotropicFiltering UnityEngine.Texture::get_anisotropicFiltering()
		void Register_UnityEngine_Texture_get_anisotropicFiltering();
		Register_UnityEngine_Texture_get_anisotropicFiltering();

		//UnityEngine.FilterMode UnityEngine.Texture::get_filterMode()
		void Register_UnityEngine_Texture_get_filterMode();
		Register_UnityEngine_Texture_get_filterMode();

		//UnityEngine.TextureWrapMode UnityEngine.Texture::get_wrapMode()
		void Register_UnityEngine_Texture_get_wrapMode();
		Register_UnityEngine_Texture_get_wrapMode();

	//End Registrations for type : UnityEngine.Texture

	//Start Registrations for type : UnityEngine.Texture2D

		//System.Boolean UnityEngine.Texture2D::Internal_ResizeWH(System.Int32,System.Int32)
		void Register_UnityEngine_Texture2D_Internal_ResizeWH();
		Register_UnityEngine_Texture2D_Internal_ResizeWH();

		//System.Boolean UnityEngine.Texture2D::LoadImage(System.Byte[],System.Boolean)
		void Register_UnityEngine_Texture2D_LoadImage();
		Register_UnityEngine_Texture2D_LoadImage();

		//System.Boolean UnityEngine.Texture2D::Resize(System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean)
		void Register_UnityEngine_Texture2D_Resize();
		Register_UnityEngine_Texture2D_Resize();

		//System.Byte[] UnityEngine.Texture2D::EncodeToJPG(System.Int32)
		void Register_UnityEngine_Texture2D_EncodeToJPG();
		Register_UnityEngine_Texture2D_EncodeToJPG();

		//System.Byte[] UnityEngine.Texture2D::EncodeToPNG()
		void Register_UnityEngine_Texture2D_EncodeToPNG();
		Register_UnityEngine_Texture2D_EncodeToPNG();

		//System.Byte[] UnityEngine.Texture2D::GetRawTextureData()
		void Register_UnityEngine_Texture2D_GetRawTextureData();
		Register_UnityEngine_Texture2D_GetRawTextureData();

		//System.Int32 UnityEngine.Texture2D::get_mipmapCount()
		void Register_UnityEngine_Texture2D_get_mipmapCount();
		Register_UnityEngine_Texture2D_get_mipmapCount();

		//System.Void UnityEngine.Texture2D::Apply(System.Boolean,System.Boolean)
		void Register_UnityEngine_Texture2D_Apply();
		Register_UnityEngine_Texture2D_Apply();

		//System.Void UnityEngine.Texture2D::INTERNAL_CALL_Compress(UnityEngine.Texture2D,System.Boolean)
		void Register_UnityEngine_Texture2D_INTERNAL_CALL_Compress();
		Register_UnityEngine_Texture2D_INTERNAL_CALL_Compress();

		//System.Void UnityEngine.Texture2D::INTERNAL_CALL_GetPixel(UnityEngine.Texture2D,System.Int32,System.Int32,UnityEngine.Color&)
		void Register_UnityEngine_Texture2D_INTERNAL_CALL_GetPixel();
		Register_UnityEngine_Texture2D_INTERNAL_CALL_GetPixel();

		//System.Void UnityEngine.Texture2D::INTERNAL_CALL_GetPixelBilinear(UnityEngine.Texture2D,System.Single,System.Single,UnityEngine.Color&)
		void Register_UnityEngine_Texture2D_INTERNAL_CALL_GetPixelBilinear();
		Register_UnityEngine_Texture2D_INTERNAL_CALL_GetPixelBilinear();

		//System.Void UnityEngine.Texture2D::INTERNAL_CALL_ReadPixels(UnityEngine.Texture2D,UnityEngine.Rect&,System.Int32,System.Int32,System.Boolean)
		void Register_UnityEngine_Texture2D_INTERNAL_CALL_ReadPixels();
		Register_UnityEngine_Texture2D_INTERNAL_CALL_ReadPixels();

		//System.Void UnityEngine.Texture2D::INTERNAL_CALL_SetPixel(UnityEngine.Texture2D,System.Int32,System.Int32,UnityEngine.Color&)
		void Register_UnityEngine_Texture2D_INTERNAL_CALL_SetPixel();
		Register_UnityEngine_Texture2D_INTERNAL_CALL_SetPixel();

		//System.Void UnityEngine.Texture2D::Internal_Create(UnityEngine.Texture2D,System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean,System.Boolean,System.IntPtr)
		void Register_UnityEngine_Texture2D_Internal_Create();
		Register_UnityEngine_Texture2D_Internal_Create();

		//System.Void UnityEngine.Texture2D::LoadRawTextureData_ImplArray(System.Byte[])
		void Register_UnityEngine_Texture2D_LoadRawTextureData_ImplArray();
		Register_UnityEngine_Texture2D_LoadRawTextureData_ImplArray();

		//System.Void UnityEngine.Texture2D::LoadRawTextureData_ImplPointer(System.IntPtr,System.Int32)
		void Register_UnityEngine_Texture2D_LoadRawTextureData_ImplPointer();
		Register_UnityEngine_Texture2D_LoadRawTextureData_ImplPointer();

		//System.Void UnityEngine.Texture2D::SetAllPixels32(UnityEngine.Color32[],System.Int32)
		void Register_UnityEngine_Texture2D_SetAllPixels32();
		Register_UnityEngine_Texture2D_SetAllPixels32();

		//System.Void UnityEngine.Texture2D::SetBlockOfPixels32(System.Int32,System.Int32,System.Int32,System.Int32,UnityEngine.Color32[],System.Int32)
		void Register_UnityEngine_Texture2D_SetBlockOfPixels32();
		Register_UnityEngine_Texture2D_SetBlockOfPixels32();

		//System.Void UnityEngine.Texture2D::SetPixels(System.Int32,System.Int32,System.Int32,System.Int32,UnityEngine.Color[],System.Int32)
		void Register_UnityEngine_Texture2D_SetPixels();
		Register_UnityEngine_Texture2D_SetPixels();

		//System.Void UnityEngine.Texture2D::UpdateExternalTexture(System.IntPtr)
		void Register_UnityEngine_Texture2D_UpdateExternalTexture();
		Register_UnityEngine_Texture2D_UpdateExternalTexture();

		//UnityEngine.Color32[] UnityEngine.Texture2D::GetPixels32(System.Int32)
		void Register_UnityEngine_Texture2D_GetPixels32();
		Register_UnityEngine_Texture2D_GetPixels32();

		//UnityEngine.Color[] UnityEngine.Texture2D::GetPixels(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
		void Register_UnityEngine_Texture2D_GetPixels();
		Register_UnityEngine_Texture2D_GetPixels();

		//UnityEngine.Rect[] UnityEngine.Texture2D::PackTextures(UnityEngine.Texture2D[],System.Int32,System.Int32,System.Boolean)
		void Register_UnityEngine_Texture2D_PackTextures();
		Register_UnityEngine_Texture2D_PackTextures();

		//UnityEngine.Texture2D UnityEngine.Texture2D::get_blackTexture()
		void Register_UnityEngine_Texture2D_get_blackTexture();
		Register_UnityEngine_Texture2D_get_blackTexture();

		//UnityEngine.Texture2D UnityEngine.Texture2D::get_whiteTexture()
		void Register_UnityEngine_Texture2D_get_whiteTexture();
		Register_UnityEngine_Texture2D_get_whiteTexture();

		//UnityEngine.TextureFormat UnityEngine.Texture2D::get_format()
		void Register_UnityEngine_Texture2D_get_format();
		Register_UnityEngine_Texture2D_get_format();

	//End Registrations for type : UnityEngine.Texture2D

	//Start Registrations for type : UnityEngine.Texture3D

		//System.Int32 UnityEngine.Texture3D::get_depth()
		void Register_UnityEngine_Texture3D_get_depth();
		Register_UnityEngine_Texture3D_get_depth();

		//System.Void UnityEngine.Texture3D::Apply(System.Boolean)
		void Register_UnityEngine_Texture3D_Apply();
		Register_UnityEngine_Texture3D_Apply();

		//System.Void UnityEngine.Texture3D::Internal_Create(UnityEngine.Texture3D,System.Int32,System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean)
		void Register_UnityEngine_Texture3D_Internal_Create();
		Register_UnityEngine_Texture3D_Internal_Create();

		//System.Void UnityEngine.Texture3D::SetPixels(UnityEngine.Color[],System.Int32)
		void Register_UnityEngine_Texture3D_SetPixels();
		Register_UnityEngine_Texture3D_SetPixels();

		//System.Void UnityEngine.Texture3D::SetPixels32(UnityEngine.Color32[],System.Int32)
		void Register_UnityEngine_Texture3D_SetPixels32();
		Register_UnityEngine_Texture3D_SetPixels32();

		//UnityEngine.Color32[] UnityEngine.Texture3D::GetPixels32(System.Int32)
		void Register_UnityEngine_Texture3D_GetPixels32();
		Register_UnityEngine_Texture3D_GetPixels32();

		//UnityEngine.Color[] UnityEngine.Texture3D::GetPixels(System.Int32)
		void Register_UnityEngine_Texture3D_GetPixels();
		Register_UnityEngine_Texture3D_GetPixels();

		//UnityEngine.TextureFormat UnityEngine.Texture3D::get_format()
		void Register_UnityEngine_Texture3D_get_format();
		Register_UnityEngine_Texture3D_get_format();

	//End Registrations for type : UnityEngine.Texture3D

	//Start Registrations for type : UnityEngine.Time

		//System.Int32 UnityEngine.Time::get_captureFramerate()
		void Register_UnityEngine_Time_get_captureFramerate();
		Register_UnityEngine_Time_get_captureFramerate();

		//System.Int32 UnityEngine.Time::get_frameCount()
		void Register_UnityEngine_Time_get_frameCount();
		Register_UnityEngine_Time_get_frameCount();

		//System.Int32 UnityEngine.Time::get_renderedFrameCount()
		void Register_UnityEngine_Time_get_renderedFrameCount();
		Register_UnityEngine_Time_get_renderedFrameCount();

		//System.Single UnityEngine.Time::get_deltaTime()
		void Register_UnityEngine_Time_get_deltaTime();
		Register_UnityEngine_Time_get_deltaTime();

		//System.Single UnityEngine.Time::get_fixedDeltaTime()
		void Register_UnityEngine_Time_get_fixedDeltaTime();
		Register_UnityEngine_Time_get_fixedDeltaTime();

		//System.Single UnityEngine.Time::get_fixedTime()
		void Register_UnityEngine_Time_get_fixedTime();
		Register_UnityEngine_Time_get_fixedTime();

		//System.Single UnityEngine.Time::get_maximumDeltaTime()
		void Register_UnityEngine_Time_get_maximumDeltaTime();
		Register_UnityEngine_Time_get_maximumDeltaTime();

		//System.Single UnityEngine.Time::get_realtimeSinceStartup()
		void Register_UnityEngine_Time_get_realtimeSinceStartup();
		Register_UnityEngine_Time_get_realtimeSinceStartup();

		//System.Single UnityEngine.Time::get_smoothDeltaTime()
		void Register_UnityEngine_Time_get_smoothDeltaTime();
		Register_UnityEngine_Time_get_smoothDeltaTime();

		//System.Single UnityEngine.Time::get_time()
		void Register_UnityEngine_Time_get_time();
		Register_UnityEngine_Time_get_time();

		//System.Single UnityEngine.Time::get_timeScale()
		void Register_UnityEngine_Time_get_timeScale();
		Register_UnityEngine_Time_get_timeScale();

		//System.Single UnityEngine.Time::get_timeSinceLevelLoad()
		void Register_UnityEngine_Time_get_timeSinceLevelLoad();
		Register_UnityEngine_Time_get_timeSinceLevelLoad();

		//System.Single UnityEngine.Time::get_unscaledDeltaTime()
		void Register_UnityEngine_Time_get_unscaledDeltaTime();
		Register_UnityEngine_Time_get_unscaledDeltaTime();

		//System.Single UnityEngine.Time::get_unscaledTime()
		void Register_UnityEngine_Time_get_unscaledTime();
		Register_UnityEngine_Time_get_unscaledTime();

		//System.Void UnityEngine.Time::set_captureFramerate(System.Int32)
		void Register_UnityEngine_Time_set_captureFramerate();
		Register_UnityEngine_Time_set_captureFramerate();

		//System.Void UnityEngine.Time::set_fixedDeltaTime(System.Single)
		void Register_UnityEngine_Time_set_fixedDeltaTime();
		Register_UnityEngine_Time_set_fixedDeltaTime();

		//System.Void UnityEngine.Time::set_maximumDeltaTime(System.Single)
		void Register_UnityEngine_Time_set_maximumDeltaTime();
		Register_UnityEngine_Time_set_maximumDeltaTime();

		//System.Void UnityEngine.Time::set_timeScale(System.Single)
		void Register_UnityEngine_Time_set_timeScale();
		Register_UnityEngine_Time_set_timeScale();

	//End Registrations for type : UnityEngine.Time

	//Start Registrations for type : UnityEngine.TouchScreenKeyboard

		//System.Boolean UnityEngine.TouchScreenKeyboard::get_active()
		void Register_UnityEngine_TouchScreenKeyboard_get_active();
		Register_UnityEngine_TouchScreenKeyboard_get_active();

		//System.Boolean UnityEngine.TouchScreenKeyboard::get_done()
		void Register_UnityEngine_TouchScreenKeyboard_get_done();
		Register_UnityEngine_TouchScreenKeyboard_get_done();

		//System.Boolean UnityEngine.TouchScreenKeyboard::get_hideInput()
		void Register_UnityEngine_TouchScreenKeyboard_get_hideInput();
		Register_UnityEngine_TouchScreenKeyboard_get_hideInput();

		//System.Boolean UnityEngine.TouchScreenKeyboard::get_visible()
		void Register_UnityEngine_TouchScreenKeyboard_get_visible();
		Register_UnityEngine_TouchScreenKeyboard_get_visible();

		//System.Boolean UnityEngine.TouchScreenKeyboard::get_wasCanceled()
		void Register_UnityEngine_TouchScreenKeyboard_get_wasCanceled();
		Register_UnityEngine_TouchScreenKeyboard_get_wasCanceled();

		//System.Int32 UnityEngine.TouchScreenKeyboard::get_targetDisplay()
		void Register_UnityEngine_TouchScreenKeyboard_get_targetDisplay();
		Register_UnityEngine_TouchScreenKeyboard_get_targetDisplay();

		//System.String UnityEngine.TouchScreenKeyboard::get_text()
		void Register_UnityEngine_TouchScreenKeyboard_get_text();
		Register_UnityEngine_TouchScreenKeyboard_get_text();

		//System.Void UnityEngine.TouchScreenKeyboard::Destroy()
		void Register_UnityEngine_TouchScreenKeyboard_Destroy();
		Register_UnityEngine_TouchScreenKeyboard_Destroy();

		//System.Void UnityEngine.TouchScreenKeyboard::INTERNAL_get_area(UnityEngine.Rect&)
		void Register_UnityEngine_TouchScreenKeyboard_INTERNAL_get_area();
		Register_UnityEngine_TouchScreenKeyboard_INTERNAL_get_area();

		//System.Void UnityEngine.TouchScreenKeyboard::TouchScreenKeyboard_InternalConstructorHelper(UnityEngine.TouchScreenKeyboard_InternalConstructorHelperArguments&,System.String,System.String)
		void Register_UnityEngine_TouchScreenKeyboard_TouchScreenKeyboard_InternalConstructorHelper();
		Register_UnityEngine_TouchScreenKeyboard_TouchScreenKeyboard_InternalConstructorHelper();

		//System.Void UnityEngine.TouchScreenKeyboard::set_active(System.Boolean)
		void Register_UnityEngine_TouchScreenKeyboard_set_active();
		Register_UnityEngine_TouchScreenKeyboard_set_active();

		//System.Void UnityEngine.TouchScreenKeyboard::set_hideInput(System.Boolean)
		void Register_UnityEngine_TouchScreenKeyboard_set_hideInput();
		Register_UnityEngine_TouchScreenKeyboard_set_hideInput();

		//System.Void UnityEngine.TouchScreenKeyboard::set_targetDisplay(System.Int32)
		void Register_UnityEngine_TouchScreenKeyboard_set_targetDisplay();
		Register_UnityEngine_TouchScreenKeyboard_set_targetDisplay();

		//System.Void UnityEngine.TouchScreenKeyboard::set_text(System.String)
		void Register_UnityEngine_TouchScreenKeyboard_set_text();
		Register_UnityEngine_TouchScreenKeyboard_set_text();

	//End Registrations for type : UnityEngine.TouchScreenKeyboard

	//Start Registrations for type : UnityEngine.TrailRenderer

		//System.Boolean UnityEngine.TrailRenderer::get_autodestruct()
		void Register_UnityEngine_TrailRenderer_get_autodestruct();
		Register_UnityEngine_TrailRenderer_get_autodestruct();

		//System.Single UnityEngine.TrailRenderer::get_endWidth()
		void Register_UnityEngine_TrailRenderer_get_endWidth();
		Register_UnityEngine_TrailRenderer_get_endWidth();

		//System.Single UnityEngine.TrailRenderer::get_startWidth()
		void Register_UnityEngine_TrailRenderer_get_startWidth();
		Register_UnityEngine_TrailRenderer_get_startWidth();

		//System.Single UnityEngine.TrailRenderer::get_time()
		void Register_UnityEngine_TrailRenderer_get_time();
		Register_UnityEngine_TrailRenderer_get_time();

		//System.Void UnityEngine.TrailRenderer::Clear()
		void Register_UnityEngine_TrailRenderer_Clear();
		Register_UnityEngine_TrailRenderer_Clear();

		//System.Void UnityEngine.TrailRenderer::set_autodestruct(System.Boolean)
		void Register_UnityEngine_TrailRenderer_set_autodestruct();
		Register_UnityEngine_TrailRenderer_set_autodestruct();

		//System.Void UnityEngine.TrailRenderer::set_endWidth(System.Single)
		void Register_UnityEngine_TrailRenderer_set_endWidth();
		Register_UnityEngine_TrailRenderer_set_endWidth();

		//System.Void UnityEngine.TrailRenderer::set_startWidth(System.Single)
		void Register_UnityEngine_TrailRenderer_set_startWidth();
		Register_UnityEngine_TrailRenderer_set_startWidth();

		//System.Void UnityEngine.TrailRenderer::set_time(System.Single)
		void Register_UnityEngine_TrailRenderer_set_time();
		Register_UnityEngine_TrailRenderer_set_time();

	//End Registrations for type : UnityEngine.TrailRenderer

	//Start Registrations for type : UnityEngine.Transform

		//System.Boolean UnityEngine.Transform::IsChildOf(UnityEngine.Transform)
		void Register_UnityEngine_Transform_IsChildOf();
		Register_UnityEngine_Transform_IsChildOf();

		//System.Boolean UnityEngine.Transform::get_hasChanged()
		void Register_UnityEngine_Transform_get_hasChanged();
		Register_UnityEngine_Transform_get_hasChanged();

		//System.Int32 UnityEngine.Transform::GetSiblingIndex()
		void Register_UnityEngine_Transform_GetSiblingIndex();
		Register_UnityEngine_Transform_GetSiblingIndex();

		//System.Int32 UnityEngine.Transform::get_childCount()
		void Register_UnityEngine_Transform_get_childCount();
		Register_UnityEngine_Transform_get_childCount();

		//System.Void UnityEngine.Transform::DetachChildren()
		void Register_UnityEngine_Transform_DetachChildren();
		Register_UnityEngine_Transform_DetachChildren();

		//System.Void UnityEngine.Transform::INTERNAL_CALL_InverseTransformDirection(UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Vector3&)
		void Register_UnityEngine_Transform_INTERNAL_CALL_InverseTransformDirection();
		Register_UnityEngine_Transform_INTERNAL_CALL_InverseTransformDirection();

		//System.Void UnityEngine.Transform::INTERNAL_CALL_InverseTransformPoint(UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Vector3&)
		void Register_UnityEngine_Transform_INTERNAL_CALL_InverseTransformPoint();
		Register_UnityEngine_Transform_INTERNAL_CALL_InverseTransformPoint();

		//System.Void UnityEngine.Transform::INTERNAL_CALL_InverseTransformVector(UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Vector3&)
		void Register_UnityEngine_Transform_INTERNAL_CALL_InverseTransformVector();
		Register_UnityEngine_Transform_INTERNAL_CALL_InverseTransformVector();

		//System.Void UnityEngine.Transform::INTERNAL_CALL_LookAt(UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Vector3&)
		void Register_UnityEngine_Transform_INTERNAL_CALL_LookAt();
		Register_UnityEngine_Transform_INTERNAL_CALL_LookAt();

		//System.Void UnityEngine.Transform::INTERNAL_CALL_RotateAroundInternal(UnityEngine.Transform,UnityEngine.Vector3&,System.Single)
		void Register_UnityEngine_Transform_INTERNAL_CALL_RotateAroundInternal();
		Register_UnityEngine_Transform_INTERNAL_CALL_RotateAroundInternal();

		//System.Void UnityEngine.Transform::INTERNAL_CALL_TransformDirection(UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Vector3&)
		void Register_UnityEngine_Transform_INTERNAL_CALL_TransformDirection();
		Register_UnityEngine_Transform_INTERNAL_CALL_TransformDirection();

		//System.Void UnityEngine.Transform::INTERNAL_CALL_TransformPoint(UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Vector3&)
		void Register_UnityEngine_Transform_INTERNAL_CALL_TransformPoint();
		Register_UnityEngine_Transform_INTERNAL_CALL_TransformPoint();

		//System.Void UnityEngine.Transform::INTERNAL_CALL_TransformVector(UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Vector3&)
		void Register_UnityEngine_Transform_INTERNAL_CALL_TransformVector();
		Register_UnityEngine_Transform_INTERNAL_CALL_TransformVector();

		//System.Void UnityEngine.Transform::INTERNAL_get_localEulerAngles(UnityEngine.Vector3&)
		void Register_UnityEngine_Transform_INTERNAL_get_localEulerAngles();
		Register_UnityEngine_Transform_INTERNAL_get_localEulerAngles();

		//System.Void UnityEngine.Transform::INTERNAL_get_localPosition(UnityEngine.Vector3&)
		void Register_UnityEngine_Transform_INTERNAL_get_localPosition();
		Register_UnityEngine_Transform_INTERNAL_get_localPosition();

		//System.Void UnityEngine.Transform::INTERNAL_get_localRotation(UnityEngine.Quaternion&)
		void Register_UnityEngine_Transform_INTERNAL_get_localRotation();
		Register_UnityEngine_Transform_INTERNAL_get_localRotation();

		//System.Void UnityEngine.Transform::INTERNAL_get_localScale(UnityEngine.Vector3&)
		void Register_UnityEngine_Transform_INTERNAL_get_localScale();
		Register_UnityEngine_Transform_INTERNAL_get_localScale();

		//System.Void UnityEngine.Transform::INTERNAL_get_localToWorldMatrix(UnityEngine.Matrix4x4&)
		void Register_UnityEngine_Transform_INTERNAL_get_localToWorldMatrix();
		Register_UnityEngine_Transform_INTERNAL_get_localToWorldMatrix();

		//System.Void UnityEngine.Transform::INTERNAL_get_lossyScale(UnityEngine.Vector3&)
		void Register_UnityEngine_Transform_INTERNAL_get_lossyScale();
		Register_UnityEngine_Transform_INTERNAL_get_lossyScale();

		//System.Void UnityEngine.Transform::INTERNAL_get_position(UnityEngine.Vector3&)
		void Register_UnityEngine_Transform_INTERNAL_get_position();
		Register_UnityEngine_Transform_INTERNAL_get_position();

		//System.Void UnityEngine.Transform::INTERNAL_get_rotation(UnityEngine.Quaternion&)
		void Register_UnityEngine_Transform_INTERNAL_get_rotation();
		Register_UnityEngine_Transform_INTERNAL_get_rotation();

		//System.Void UnityEngine.Transform::INTERNAL_get_worldToLocalMatrix(UnityEngine.Matrix4x4&)
		void Register_UnityEngine_Transform_INTERNAL_get_worldToLocalMatrix();
		Register_UnityEngine_Transform_INTERNAL_get_worldToLocalMatrix();

		//System.Void UnityEngine.Transform::INTERNAL_set_localEulerAngles(UnityEngine.Vector3&)
		void Register_UnityEngine_Transform_INTERNAL_set_localEulerAngles();
		Register_UnityEngine_Transform_INTERNAL_set_localEulerAngles();

		//System.Void UnityEngine.Transform::INTERNAL_set_localPosition(UnityEngine.Vector3&)
		void Register_UnityEngine_Transform_INTERNAL_set_localPosition();
		Register_UnityEngine_Transform_INTERNAL_set_localPosition();

		//System.Void UnityEngine.Transform::INTERNAL_set_localRotation(UnityEngine.Quaternion&)
		void Register_UnityEngine_Transform_INTERNAL_set_localRotation();
		Register_UnityEngine_Transform_INTERNAL_set_localRotation();

		//System.Void UnityEngine.Transform::INTERNAL_set_localScale(UnityEngine.Vector3&)
		void Register_UnityEngine_Transform_INTERNAL_set_localScale();
		Register_UnityEngine_Transform_INTERNAL_set_localScale();

		//System.Void UnityEngine.Transform::INTERNAL_set_position(UnityEngine.Vector3&)
		void Register_UnityEngine_Transform_INTERNAL_set_position();
		Register_UnityEngine_Transform_INTERNAL_set_position();

		//System.Void UnityEngine.Transform::INTERNAL_set_rotation(UnityEngine.Quaternion&)
		void Register_UnityEngine_Transform_INTERNAL_set_rotation();
		Register_UnityEngine_Transform_INTERNAL_set_rotation();

		//System.Void UnityEngine.Transform::SetAsFirstSibling()
		void Register_UnityEngine_Transform_SetAsFirstSibling();
		Register_UnityEngine_Transform_SetAsFirstSibling();

		//System.Void UnityEngine.Transform::SetAsLastSibling()
		void Register_UnityEngine_Transform_SetAsLastSibling();
		Register_UnityEngine_Transform_SetAsLastSibling();

		//System.Void UnityEngine.Transform::SetParent(UnityEngine.Transform,System.Boolean)
		void Register_UnityEngine_Transform_SetParent();
		Register_UnityEngine_Transform_SetParent();

		//System.Void UnityEngine.Transform::SetSiblingIndex(System.Int32)
		void Register_UnityEngine_Transform_SetSiblingIndex();
		Register_UnityEngine_Transform_SetSiblingIndex();

		//System.Void UnityEngine.Transform::set_hasChanged(System.Boolean)
		void Register_UnityEngine_Transform_set_hasChanged();
		Register_UnityEngine_Transform_set_hasChanged();

		//System.Void UnityEngine.Transform::set_parentInternal(UnityEngine.Transform)
		void Register_UnityEngine_Transform_set_parentInternal();
		Register_UnityEngine_Transform_set_parentInternal();

		//UnityEngine.Transform UnityEngine.Transform::Find(System.String)
		void Register_UnityEngine_Transform_Find();
		Register_UnityEngine_Transform_Find();

		//UnityEngine.Transform UnityEngine.Transform::GetChild(System.Int32)
		void Register_UnityEngine_Transform_GetChild();
		Register_UnityEngine_Transform_GetChild();

		//UnityEngine.Transform UnityEngine.Transform::get_parentInternal()
		void Register_UnityEngine_Transform_get_parentInternal();
		Register_UnityEngine_Transform_get_parentInternal();

		//UnityEngine.Transform UnityEngine.Transform::get_root()
		void Register_UnityEngine_Transform_get_root();
		Register_UnityEngine_Transform_get_root();

	//End Registrations for type : UnityEngine.Transform

	//Start Registrations for type : UnityEngine.UnhandledExceptionHandler

		//System.Void UnityEngine.UnhandledExceptionHandler::NativeUnhandledExceptionHandler()
		void Register_UnityEngine_UnhandledExceptionHandler_NativeUnhandledExceptionHandler();
		Register_UnityEngine_UnhandledExceptionHandler_NativeUnhandledExceptionHandler();

	//End Registrations for type : UnityEngine.UnhandledExceptionHandler

	//Start Registrations for type : UnityEngine.Vector3

		//System.Void UnityEngine.Vector3::INTERNAL_CALL_Internal_OrthoNormalize2(UnityEngine.Vector3&,UnityEngine.Vector3&)
		void Register_UnityEngine_Vector3_INTERNAL_CALL_Internal_OrthoNormalize2();
		Register_UnityEngine_Vector3_INTERNAL_CALL_Internal_OrthoNormalize2();

		//System.Void UnityEngine.Vector3::INTERNAL_CALL_Internal_OrthoNormalize3(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Vector3&)
		void Register_UnityEngine_Vector3_INTERNAL_CALL_Internal_OrthoNormalize3();
		Register_UnityEngine_Vector3_INTERNAL_CALL_Internal_OrthoNormalize3();

		//System.Void UnityEngine.Vector3::INTERNAL_CALL_RotateTowards(UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,System.Single,UnityEngine.Vector3&)
		void Register_UnityEngine_Vector3_INTERNAL_CALL_RotateTowards();
		Register_UnityEngine_Vector3_INTERNAL_CALL_RotateTowards();

		//System.Void UnityEngine.Vector3::INTERNAL_CALL_Slerp(UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,UnityEngine.Vector3&)
		void Register_UnityEngine_Vector3_INTERNAL_CALL_Slerp();
		Register_UnityEngine_Vector3_INTERNAL_CALL_Slerp();

		//System.Void UnityEngine.Vector3::INTERNAL_CALL_SlerpUnclamped(UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,UnityEngine.Vector3&)
		void Register_UnityEngine_Vector3_INTERNAL_CALL_SlerpUnclamped();
		Register_UnityEngine_Vector3_INTERNAL_CALL_SlerpUnclamped();

	//End Registrations for type : UnityEngine.Vector3

	//Start Registrations for type : UnityEngine.WebCamTexture

		//System.Boolean UnityEngine.WebCamTexture::get_didUpdateThisFrame()
		void Register_UnityEngine_WebCamTexture_get_didUpdateThisFrame();
		Register_UnityEngine_WebCamTexture_get_didUpdateThisFrame();

		//System.Boolean UnityEngine.WebCamTexture::get_isPlaying()
		void Register_UnityEngine_WebCamTexture_get_isPlaying();
		Register_UnityEngine_WebCamTexture_get_isPlaying();

		//System.Boolean UnityEngine.WebCamTexture::get_videoVerticallyMirrored()
		void Register_UnityEngine_WebCamTexture_get_videoVerticallyMirrored();
		Register_UnityEngine_WebCamTexture_get_videoVerticallyMirrored();

		//System.Int32 UnityEngine.WebCamTexture::get_requestedHeight()
		void Register_UnityEngine_WebCamTexture_get_requestedHeight();
		Register_UnityEngine_WebCamTexture_get_requestedHeight();

		//System.Int32 UnityEngine.WebCamTexture::get_requestedWidth()
		void Register_UnityEngine_WebCamTexture_get_requestedWidth();
		Register_UnityEngine_WebCamTexture_get_requestedWidth();

		//System.Int32 UnityEngine.WebCamTexture::get_videoRotationAngle()
		void Register_UnityEngine_WebCamTexture_get_videoRotationAngle();
		Register_UnityEngine_WebCamTexture_get_videoRotationAngle();

		//System.Single UnityEngine.WebCamTexture::get_requestedFPS()
		void Register_UnityEngine_WebCamTexture_get_requestedFPS();
		Register_UnityEngine_WebCamTexture_get_requestedFPS();

		//System.String UnityEngine.WebCamTexture::get_deviceName()
		void Register_UnityEngine_WebCamTexture_get_deviceName();
		Register_UnityEngine_WebCamTexture_get_deviceName();

		//System.Void UnityEngine.WebCamTexture::INTERNAL_CALL_GetPixel(UnityEngine.WebCamTexture,System.Int32,System.Int32,UnityEngine.Color&)
		void Register_UnityEngine_WebCamTexture_INTERNAL_CALL_GetPixel();
		Register_UnityEngine_WebCamTexture_INTERNAL_CALL_GetPixel();

		//System.Void UnityEngine.WebCamTexture::INTERNAL_CALL_Pause(UnityEngine.WebCamTexture)
		void Register_UnityEngine_WebCamTexture_INTERNAL_CALL_Pause();
		Register_UnityEngine_WebCamTexture_INTERNAL_CALL_Pause();

		//System.Void UnityEngine.WebCamTexture::INTERNAL_CALL_Play(UnityEngine.WebCamTexture)
		void Register_UnityEngine_WebCamTexture_INTERNAL_CALL_Play();
		Register_UnityEngine_WebCamTexture_INTERNAL_CALL_Play();

		//System.Void UnityEngine.WebCamTexture::INTERNAL_CALL_Stop(UnityEngine.WebCamTexture)
		void Register_UnityEngine_WebCamTexture_INTERNAL_CALL_Stop();
		Register_UnityEngine_WebCamTexture_INTERNAL_CALL_Stop();

		//System.Void UnityEngine.WebCamTexture::Internal_CreateWebCamTexture(UnityEngine.WebCamTexture,System.String,System.Int32,System.Int32,System.Int32)
		void Register_UnityEngine_WebCamTexture_Internal_CreateWebCamTexture();
		Register_UnityEngine_WebCamTexture_Internal_CreateWebCamTexture();

		//System.Void UnityEngine.WebCamTexture::set_deviceName(System.String)
		void Register_UnityEngine_WebCamTexture_set_deviceName();
		Register_UnityEngine_WebCamTexture_set_deviceName();

		//System.Void UnityEngine.WebCamTexture::set_requestedFPS(System.Single)
		void Register_UnityEngine_WebCamTexture_set_requestedFPS();
		Register_UnityEngine_WebCamTexture_set_requestedFPS();

		//System.Void UnityEngine.WebCamTexture::set_requestedHeight(System.Int32)
		void Register_UnityEngine_WebCamTexture_set_requestedHeight();
		Register_UnityEngine_WebCamTexture_set_requestedHeight();

		//System.Void UnityEngine.WebCamTexture::set_requestedWidth(System.Int32)
		void Register_UnityEngine_WebCamTexture_set_requestedWidth();
		Register_UnityEngine_WebCamTexture_set_requestedWidth();

		//UnityEngine.Color32[] UnityEngine.WebCamTexture::GetPixels32(UnityEngine.Color32[])
		void Register_UnityEngine_WebCamTexture_GetPixels32();
		Register_UnityEngine_WebCamTexture_GetPixels32();

		//UnityEngine.Color[] UnityEngine.WebCamTexture::GetPixels(System.Int32,System.Int32,System.Int32,System.Int32)
		void Register_UnityEngine_WebCamTexture_GetPixels();
		Register_UnityEngine_WebCamTexture_GetPixels();

		//UnityEngine.WebCamDevice[] UnityEngine.WebCamTexture::get_devices()
		void Register_UnityEngine_WebCamTexture_get_devices();
		Register_UnityEngine_WebCamTexture_get_devices();

	//End Registrations for type : UnityEngine.WebCamTexture

	//Start Registrations for type : UnityEngine.WheelCollider

		//System.Boolean UnityEngine.WheelCollider::GetGroundHit(UnityEngine.WheelHit&)
		void Register_UnityEngine_WheelCollider_GetGroundHit();
		Register_UnityEngine_WheelCollider_GetGroundHit();

		//System.Boolean UnityEngine.WheelCollider::get_isGrounded()
		void Register_UnityEngine_WheelCollider_get_isGrounded();
		Register_UnityEngine_WheelCollider_get_isGrounded();

		//System.Single UnityEngine.WheelCollider::get_brakeTorque()
		void Register_UnityEngine_WheelCollider_get_brakeTorque();
		Register_UnityEngine_WheelCollider_get_brakeTorque();

		//System.Single UnityEngine.WheelCollider::get_forceAppPointDistance()
		void Register_UnityEngine_WheelCollider_get_forceAppPointDistance();
		Register_UnityEngine_WheelCollider_get_forceAppPointDistance();

		//System.Single UnityEngine.WheelCollider::get_mass()
		void Register_UnityEngine_WheelCollider_get_mass();
		Register_UnityEngine_WheelCollider_get_mass();

		//System.Single UnityEngine.WheelCollider::get_motorTorque()
		void Register_UnityEngine_WheelCollider_get_motorTorque();
		Register_UnityEngine_WheelCollider_get_motorTorque();

		//System.Single UnityEngine.WheelCollider::get_radius()
		void Register_UnityEngine_WheelCollider_get_radius();
		Register_UnityEngine_WheelCollider_get_radius();

		//System.Single UnityEngine.WheelCollider::get_rpm()
		void Register_UnityEngine_WheelCollider_get_rpm();
		Register_UnityEngine_WheelCollider_get_rpm();

		//System.Single UnityEngine.WheelCollider::get_sprungMass()
		void Register_UnityEngine_WheelCollider_get_sprungMass();
		Register_UnityEngine_WheelCollider_get_sprungMass();

		//System.Single UnityEngine.WheelCollider::get_steerAngle()
		void Register_UnityEngine_WheelCollider_get_steerAngle();
		Register_UnityEngine_WheelCollider_get_steerAngle();

		//System.Single UnityEngine.WheelCollider::get_suspensionDistance()
		void Register_UnityEngine_WheelCollider_get_suspensionDistance();
		Register_UnityEngine_WheelCollider_get_suspensionDistance();

		//System.Single UnityEngine.WheelCollider::get_wheelDampingRate()
		void Register_UnityEngine_WheelCollider_get_wheelDampingRate();
		Register_UnityEngine_WheelCollider_get_wheelDampingRate();

		//System.Void UnityEngine.WheelCollider::ConfigureVehicleSubsteps(System.Single,System.Int32,System.Int32)
		void Register_UnityEngine_WheelCollider_ConfigureVehicleSubsteps();
		Register_UnityEngine_WheelCollider_ConfigureVehicleSubsteps();

		//System.Void UnityEngine.WheelCollider::GetWorldPose(UnityEngine.Vector3&,UnityEngine.Quaternion&)
		void Register_UnityEngine_WheelCollider_GetWorldPose();
		Register_UnityEngine_WheelCollider_GetWorldPose();

		//System.Void UnityEngine.WheelCollider::INTERNAL_get_center(UnityEngine.Vector3&)
		void Register_UnityEngine_WheelCollider_INTERNAL_get_center();
		Register_UnityEngine_WheelCollider_INTERNAL_get_center();

		//System.Void UnityEngine.WheelCollider::INTERNAL_get_forwardFriction(UnityEngine.WheelFrictionCurve&)
		void Register_UnityEngine_WheelCollider_INTERNAL_get_forwardFriction();
		Register_UnityEngine_WheelCollider_INTERNAL_get_forwardFriction();

		//System.Void UnityEngine.WheelCollider::INTERNAL_get_sidewaysFriction(UnityEngine.WheelFrictionCurve&)
		void Register_UnityEngine_WheelCollider_INTERNAL_get_sidewaysFriction();
		Register_UnityEngine_WheelCollider_INTERNAL_get_sidewaysFriction();

		//System.Void UnityEngine.WheelCollider::INTERNAL_get_suspensionSpring(UnityEngine.JointSpring&)
		void Register_UnityEngine_WheelCollider_INTERNAL_get_suspensionSpring();
		Register_UnityEngine_WheelCollider_INTERNAL_get_suspensionSpring();

		//System.Void UnityEngine.WheelCollider::INTERNAL_set_center(UnityEngine.Vector3&)
		void Register_UnityEngine_WheelCollider_INTERNAL_set_center();
		Register_UnityEngine_WheelCollider_INTERNAL_set_center();

		//System.Void UnityEngine.WheelCollider::INTERNAL_set_forwardFriction(UnityEngine.WheelFrictionCurve&)
		void Register_UnityEngine_WheelCollider_INTERNAL_set_forwardFriction();
		Register_UnityEngine_WheelCollider_INTERNAL_set_forwardFriction();

		//System.Void UnityEngine.WheelCollider::INTERNAL_set_sidewaysFriction(UnityEngine.WheelFrictionCurve&)
		void Register_UnityEngine_WheelCollider_INTERNAL_set_sidewaysFriction();
		Register_UnityEngine_WheelCollider_INTERNAL_set_sidewaysFriction();

		//System.Void UnityEngine.WheelCollider::INTERNAL_set_suspensionSpring(UnityEngine.JointSpring&)
		void Register_UnityEngine_WheelCollider_INTERNAL_set_suspensionSpring();
		Register_UnityEngine_WheelCollider_INTERNAL_set_suspensionSpring();

		//System.Void UnityEngine.WheelCollider::set_brakeTorque(System.Single)
		void Register_UnityEngine_WheelCollider_set_brakeTorque();
		Register_UnityEngine_WheelCollider_set_brakeTorque();

		//System.Void UnityEngine.WheelCollider::set_forceAppPointDistance(System.Single)
		void Register_UnityEngine_WheelCollider_set_forceAppPointDistance();
		Register_UnityEngine_WheelCollider_set_forceAppPointDistance();

		//System.Void UnityEngine.WheelCollider::set_mass(System.Single)
		void Register_UnityEngine_WheelCollider_set_mass();
		Register_UnityEngine_WheelCollider_set_mass();

		//System.Void UnityEngine.WheelCollider::set_motorTorque(System.Single)
		void Register_UnityEngine_WheelCollider_set_motorTorque();
		Register_UnityEngine_WheelCollider_set_motorTorque();

		//System.Void UnityEngine.WheelCollider::set_radius(System.Single)
		void Register_UnityEngine_WheelCollider_set_radius();
		Register_UnityEngine_WheelCollider_set_radius();

		//System.Void UnityEngine.WheelCollider::set_steerAngle(System.Single)
		void Register_UnityEngine_WheelCollider_set_steerAngle();
		Register_UnityEngine_WheelCollider_set_steerAngle();

		//System.Void UnityEngine.WheelCollider::set_suspensionDistance(System.Single)
		void Register_UnityEngine_WheelCollider_set_suspensionDistance();
		Register_UnityEngine_WheelCollider_set_suspensionDistance();

		//System.Void UnityEngine.WheelCollider::set_wheelDampingRate(System.Single)
		void Register_UnityEngine_WheelCollider_set_wheelDampingRate();
		Register_UnityEngine_WheelCollider_set_wheelDampingRate();

	//End Registrations for type : UnityEngine.WheelCollider

	//Start Registrations for type : UnityEngine.WheelJoint2D

		//System.Boolean UnityEngine.WheelJoint2D::get_useMotor()
		void Register_UnityEngine_WheelJoint2D_get_useMotor();
		Register_UnityEngine_WheelJoint2D_get_useMotor();

		//System.Single UnityEngine.WheelJoint2D::INTERNAL_CALL_GetMotorTorque(UnityEngine.WheelJoint2D,System.Single)
		void Register_UnityEngine_WheelJoint2D_INTERNAL_CALL_GetMotorTorque();
		Register_UnityEngine_WheelJoint2D_INTERNAL_CALL_GetMotorTorque();

		//System.Single UnityEngine.WheelJoint2D::get_jointSpeed()
		void Register_UnityEngine_WheelJoint2D_get_jointSpeed();
		Register_UnityEngine_WheelJoint2D_get_jointSpeed();

		//System.Single UnityEngine.WheelJoint2D::get_jointTranslation()
		void Register_UnityEngine_WheelJoint2D_get_jointTranslation();
		Register_UnityEngine_WheelJoint2D_get_jointTranslation();

		//System.Void UnityEngine.WheelJoint2D::INTERNAL_get_motor(UnityEngine.JointMotor2D&)
		void Register_UnityEngine_WheelJoint2D_INTERNAL_get_motor();
		Register_UnityEngine_WheelJoint2D_INTERNAL_get_motor();

		//System.Void UnityEngine.WheelJoint2D::INTERNAL_get_suspension(UnityEngine.JointSuspension2D&)
		void Register_UnityEngine_WheelJoint2D_INTERNAL_get_suspension();
		Register_UnityEngine_WheelJoint2D_INTERNAL_get_suspension();

		//System.Void UnityEngine.WheelJoint2D::INTERNAL_set_motor(UnityEngine.JointMotor2D&)
		void Register_UnityEngine_WheelJoint2D_INTERNAL_set_motor();
		Register_UnityEngine_WheelJoint2D_INTERNAL_set_motor();

		//System.Void UnityEngine.WheelJoint2D::INTERNAL_set_suspension(UnityEngine.JointSuspension2D&)
		void Register_UnityEngine_WheelJoint2D_INTERNAL_set_suspension();
		Register_UnityEngine_WheelJoint2D_INTERNAL_set_suspension();

		//System.Void UnityEngine.WheelJoint2D::set_useMotor(System.Boolean)
		void Register_UnityEngine_WheelJoint2D_set_useMotor();
		Register_UnityEngine_WheelJoint2D_set_useMotor();

	//End Registrations for type : UnityEngine.WheelJoint2D

	//Start Registrations for type : UnityEngine.WWW

		//System.Boolean UnityEngine.WWW::get_isDone()
		void Register_UnityEngine_WWW_get_isDone();
		Register_UnityEngine_WWW_get_isDone();

		//System.Byte[] UnityEngine.WWW::get_bytes()
		void Register_UnityEngine_WWW_get_bytes();
		Register_UnityEngine_WWW_get_bytes();

		//System.Int32 UnityEngine.WWW::get_bytesDownloaded()
		void Register_UnityEngine_WWW_get_bytesDownloaded();
		Register_UnityEngine_WWW_get_bytesDownloaded();

		//System.Int32 UnityEngine.WWW::get_size()
		void Register_UnityEngine_WWW_get_size();
		Register_UnityEngine_WWW_get_size();

		//System.Single UnityEngine.WWW::get_progress()
		void Register_UnityEngine_WWW_get_progress();
		Register_UnityEngine_WWW_get_progress();

		//System.Single UnityEngine.WWW::get_uploadProgress()
		void Register_UnityEngine_WWW_get_uploadProgress();
		Register_UnityEngine_WWW_get_uploadProgress();

		//System.String UnityEngine.WWW::get_error()
		void Register_UnityEngine_WWW_get_error();
		Register_UnityEngine_WWW_get_error();

		//System.String UnityEngine.WWW::get_responseHeadersString()
		void Register_UnityEngine_WWW_get_responseHeadersString();
		Register_UnityEngine_WWW_get_responseHeadersString();

		//System.String UnityEngine.WWW::get_url()
		void Register_UnityEngine_WWW_get_url();
		Register_UnityEngine_WWW_get_url();

		//System.Void UnityEngine.WWW::DestroyWWW(System.Boolean)
		void Register_UnityEngine_WWW_DestroyWWW();
		Register_UnityEngine_WWW_DestroyWWW();

		//System.Void UnityEngine.WWW::INTERNAL_CALL_WWW(UnityEngine.WWW,System.String,UnityEngine.Hash128&,System.UInt32)
		void Register_UnityEngine_WWW_INTERNAL_CALL_WWW();
		Register_UnityEngine_WWW_INTERNAL_CALL_WWW();

		//System.Void UnityEngine.WWW::InitWWW(System.String,System.Byte[],System.String[])
		void Register_UnityEngine_WWW_InitWWW();
		Register_UnityEngine_WWW_InitWWW();

		//System.Void UnityEngine.WWW::LoadImageIntoTexture(UnityEngine.Texture2D)
		void Register_UnityEngine_WWW_LoadImageIntoTexture();
		Register_UnityEngine_WWW_LoadImageIntoTexture();

		//System.Void UnityEngine.WWW::set_threadPriority(UnityEngine.ThreadPriority)
		void Register_UnityEngine_WWW_set_threadPriority();
		Register_UnityEngine_WWW_set_threadPriority();

		//UnityEngine.AssetBundle UnityEngine.WWW::get_assetBundle()
		void Register_UnityEngine_WWW_get_assetBundle();
		Register_UnityEngine_WWW_get_assetBundle();

		//UnityEngine.AudioClip UnityEngine.WWW::GetAudioClipInternal(System.Boolean,System.Boolean,System.Boolean,UnityEngine.AudioType)
		void Register_UnityEngine_WWW_GetAudioClipInternal();
		Register_UnityEngine_WWW_GetAudioClipInternal();

		//UnityEngine.Texture2D UnityEngine.WWW::GetTexture(System.Boolean)
		void Register_UnityEngine_WWW_GetTexture();
		Register_UnityEngine_WWW_GetTexture();

		//UnityEngine.ThreadPriority UnityEngine.WWW::get_threadPriority()
		void Register_UnityEngine_WWW_get_threadPriority();
		Register_UnityEngine_WWW_get_threadPriority();

	//End Registrations for type : UnityEngine.WWW

}
