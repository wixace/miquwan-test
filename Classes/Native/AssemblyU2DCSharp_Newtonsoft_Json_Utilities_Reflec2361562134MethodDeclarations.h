﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Utilities.ReflectionUtils/<GetChildPrivateProperties>c__AnonStorey148
struct U3CGetChildPrivatePropertiesU3Ec__AnonStorey148_t2361562134;
// System.Reflection.PropertyInfo
struct PropertyInfo_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_PropertyInfo924268725.h"

// System.Void Newtonsoft.Json.Utilities.ReflectionUtils/<GetChildPrivateProperties>c__AnonStorey148::.ctor()
extern "C"  void U3CGetChildPrivatePropertiesU3Ec__AnonStorey148__ctor_m3198115349 (U3CGetChildPrivatePropertiesU3Ec__AnonStorey148_t2361562134 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Utilities.ReflectionUtils/<GetChildPrivateProperties>c__AnonStorey148::<>m__3B4(System.Reflection.PropertyInfo)
extern "C"  bool U3CGetChildPrivatePropertiesU3Ec__AnonStorey148_U3CU3Em__3B4_m2386847150 (U3CGetChildPrivatePropertiesU3Ec__AnonStorey148_t2361562134 * __this, PropertyInfo_t * ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
