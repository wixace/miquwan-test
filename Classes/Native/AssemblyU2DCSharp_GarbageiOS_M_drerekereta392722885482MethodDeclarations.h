﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_drerekereta39
struct M_drerekereta39_t2722885482;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_drerekereta392722885482.h"

// System.Void GarbageiOS.M_drerekereta39::.ctor()
extern "C"  void M_drerekereta39__ctor_m4131069241 (M_drerekereta39_t2722885482 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_drerekereta39::M_chuletallQeaferemall0(System.String[],System.Int32)
extern "C"  void M_drerekereta39_M_chuletallQeaferemall0_m3178809711 (M_drerekereta39_t2722885482 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_drerekereta39::M_faiqall1(System.String[],System.Int32)
extern "C"  void M_drerekereta39_M_faiqall1_m3168831303 (M_drerekereta39_t2722885482 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_drerekereta39::M_rearserelTraisa2(System.String[],System.Int32)
extern "C"  void M_drerekereta39_M_rearserelTraisa2_m3262265423 (M_drerekereta39_t2722885482 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_drerekereta39::M_pagayne3(System.String[],System.Int32)
extern "C"  void M_drerekereta39_M_pagayne3_m1530520834 (M_drerekereta39_t2722885482 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_drerekereta39::M_pinallmaDissa4(System.String[],System.Int32)
extern "C"  void M_drerekereta39_M_pinallmaDissa4_m716159532 (M_drerekereta39_t2722885482 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_drerekereta39::ilo_M_chuletallQeaferemall01(GarbageiOS.M_drerekereta39,System.String[],System.Int32)
extern "C"  void M_drerekereta39_ilo_M_chuletallQeaferemall01_m515127761 (Il2CppObject * __this /* static, unused */, M_drerekereta39_t2722885482 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_drerekereta39::ilo_M_faiqall12(GarbageiOS.M_drerekereta39,System.String[],System.Int32)
extern "C"  void M_drerekereta39_ilo_M_faiqall12_m3915218766 (Il2CppObject * __this /* static, unused */, M_drerekereta39_t2722885482 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
