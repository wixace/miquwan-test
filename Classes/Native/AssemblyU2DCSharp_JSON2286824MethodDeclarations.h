﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// JSON
struct JSON_t2286824;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// SharpKit.JavaScript.JsFunc`3<System.Object,System.Object,System.Object>
struct JsFunc_3_t2543207488;
// SharpKit.JavaScript.JsString
struct JsString_t4170003438;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_String7231557.h"
#include "SharpKit_JavaScript_SharpKit_JavaScript_JsString4170003438.h"

// System.Void JSON::.ctor()
extern "C"  void JSON__ctor_m3471551155 (JSON_t2286824 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String JSON::stringify(System.Object,SharpKit.JavaScript.JsFunc`3<System.Object,System.Object,System.Object>)
extern "C"  String_t* JSON_stringify_m2862499694 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___obj0, JsFunc_3_t2543207488 * ___replacer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String JSON::stringify(System.Object,SharpKit.JavaScript.JsFunc`3<System.Object,System.Object,System.Object>,System.Int32)
extern "C"  String_t* JSON_stringify_m2239551529 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___obj0, JsFunc_3_t2543207488 * ___replacer1, int32_t ___space2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String JSON::stringify(System.Object,SharpKit.JavaScript.JsFunc`3<System.Object,System.Object,System.Object>,System.String)
extern "C"  String_t* JSON_stringify_m1163319082 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___obj0, JsFunc_3_t2543207488 * ___replacer1, String_t* ___space2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String JSON::stringify(System.Object)
extern "C"  String_t* JSON_stringify_m1661467515 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object JSON::parse(SharpKit.JavaScript.JsString)
extern "C"  Il2CppObject * JSON_parse_m871681462 (Il2CppObject * __this /* static, unused */, JsString_t4170003438 * ___json0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
