﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ProtoBuf.Serializers.ParseableSerializer
struct ParseableSerializer_t3389210553;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Type
struct Type_t;
// ProtoBuf.Meta.TypeModel
struct TypeModel_t2730011105;
// System.Object
struct Il2CppObject;
// ProtoBuf.ProtoReader
struct ProtoReader_t3962509489;
// ProtoBuf.ProtoWriter
struct ProtoWriter_t4117914721;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_MethodInfo318736065.h"
#include "mscorlib_System_Type2863145774.h"
#include "AssemblyU2DCSharp_ProtoBuf_Meta_TypeModel2730011105.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_ProtoBuf_ProtoReader3962509489.h"
#include "AssemblyU2DCSharp_ProtoBuf_ProtoWriter4117914721.h"

// System.Void ProtoBuf.Serializers.ParseableSerializer::.ctor(System.Reflection.MethodInfo)
extern "C"  void ParseableSerializer__ctor_m2417967577 (ParseableSerializer_t3389210553 * __this, MethodInfo_t * ___parse0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Serializers.ParseableSerializer::ProtoBuf.Serializers.IProtoSerializer.get_RequiresOldValue()
extern "C"  bool ParseableSerializer_ProtoBuf_Serializers_IProtoSerializer_get_RequiresOldValue_m1508958810 (ParseableSerializer_t3389210553 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Serializers.ParseableSerializer::ProtoBuf.Serializers.IProtoSerializer.get_ReturnsValue()
extern "C"  bool ParseableSerializer_ProtoBuf_Serializers_IProtoSerializer_get_ReturnsValue_m2111836656 (ParseableSerializer_t3389210553 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.Serializers.ParseableSerializer ProtoBuf.Serializers.ParseableSerializer::TryCreate(System.Type,ProtoBuf.Meta.TypeModel)
extern "C"  ParseableSerializer_t3389210553 * ParseableSerializer_TryCreate_m29891753 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, TypeModel_t2730011105 * ___model1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo ProtoBuf.Serializers.ParseableSerializer::GetCustomToString(System.Type)
extern "C"  MethodInfo_t * ParseableSerializer_GetCustomToString_m1211383878 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type ProtoBuf.Serializers.ParseableSerializer::get_ExpectedType()
extern "C"  Type_t * ParseableSerializer_get_ExpectedType_m3288016013 (ParseableSerializer_t3389210553 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProtoBuf.Serializers.ParseableSerializer::Read(System.Object,ProtoBuf.ProtoReader)
extern "C"  Il2CppObject * ParseableSerializer_Read_m663769229 (ParseableSerializer_t3389210553 * __this, Il2CppObject * ___value0, ProtoReader_t3962509489 * ___source1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Serializers.ParseableSerializer::Write(System.Object,ProtoBuf.ProtoWriter)
extern "C"  void ParseableSerializer_Write_m2173254883 (ParseableSerializer_t3389210553 * __this, Il2CppObject * ___value0, ProtoWriter_t4117914721 * ___dest1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo ProtoBuf.Serializers.ParseableSerializer::ilo_GetCustomToString1(System.Type)
extern "C"  MethodInfo_t * ParseableSerializer_ilo_GetCustomToString1_m1407420550 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ProtoBuf.Serializers.ParseableSerializer::ilo_ReadString2(ProtoBuf.ProtoReader)
extern "C"  String_t* ParseableSerializer_ilo_ReadString2_m2654004535 (Il2CppObject * __this /* static, unused */, ProtoReader_t3962509489 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
