﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PluginNewMiquwan
struct PluginNewMiquwan_t2470753303;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// System.String
struct String_t;
// VersionMgr
struct VersionMgr_t1322950208;
// VersionInfo
struct VersionInfo_t2356638086;
// ProductsCfgMgr
struct ProductsCfgMgr_t2493714872;
// System.Object
struct Il2CppObject;
// PluginsSdkMgr
struct PluginsSdkMgr_t3884624670;
// System.Action
struct Action_t3771233898;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_VersionMgr1322950208.h"
#include "AssemblyU2DCSharp_ProductsCfgMgr2493714872.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_PluginsSdkMgr3884624670.h"
#include "System_Core_System_Action3771233898.h"
#include "AssemblyU2DCSharp_PluginNewMiquwan2470753303.h"

// System.Void PluginNewMiquwan::.ctor()
extern "C"  void PluginNewMiquwan__ctor_m1471421540 (PluginNewMiquwan_t2470753303 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginNewMiquwan::Init()
extern "C"  void PluginNewMiquwan_Init_m137549168 (PluginNewMiquwan_t2470753303 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginNewMiquwan::ReqSDKHttpLogin()
extern "C"  Dictionary_2_t827649927 * PluginNewMiquwan_ReqSDKHttpLogin_m2258710201 (PluginNewMiquwan_t2470753303 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginNewMiquwan::IsLoginSuccess(System.Boolean)
extern "C"  bool PluginNewMiquwan_IsLoginSuccess_m937375855 (PluginNewMiquwan_t2470753303 * __this, bool ___isOpenUseLogin0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginNewMiquwan::OpenUserLogin()
extern "C"  void PluginNewMiquwan_OpenUserLogin_m72129206 (PluginNewMiquwan_t2470753303 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginNewMiquwan::RolelvUpinfo(CEvent.ZEvent)
extern "C"  void PluginNewMiquwan_RolelvUpinfo_m1839524242 (PluginNewMiquwan_t2470753303 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginNewMiquwan::UserPay(CEvent.ZEvent)
extern "C"  void PluginNewMiquwan_UserPay_m4071333884 (PluginNewMiquwan_t2470753303 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginNewMiquwan::OnInitSuccess(System.String)
extern "C"  void PluginNewMiquwan_OnInitSuccess_m1639961036 (PluginNewMiquwan_t2470753303 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginNewMiquwan::OnInitFail(System.String)
extern "C"  void PluginNewMiquwan_OnInitFail_m2483540725 (PluginNewMiquwan_t2470753303 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginNewMiquwan::OnLoginSuccess(System.String)
extern "C"  void PluginNewMiquwan_OnLoginSuccess_m3983863913 (PluginNewMiquwan_t2470753303 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginNewMiquwan::OnLoginFail(System.String)
extern "C"  void PluginNewMiquwan_OnLoginFail_m2605154680 (PluginNewMiquwan_t2470753303 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginNewMiquwan::OnPaySuccess(System.String)
extern "C"  void PluginNewMiquwan_OnPaySuccess_m3665713384 (PluginNewMiquwan_t2470753303 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginNewMiquwan::OnPayFail(System.String)
extern "C"  void PluginNewMiquwan_OnPayFail_m291504473 (PluginNewMiquwan_t2470753303 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginNewMiquwan::OnExitGameSuccess(System.String)
extern "C"  void PluginNewMiquwan_OnExitGameSuccess_m1428467692 (PluginNewMiquwan_t2470753303 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginNewMiquwan::LogoutSuccess()
extern "C"  void PluginNewMiquwan_LogoutSuccess_m23421275 (PluginNewMiquwan_t2470753303 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginNewMiquwan::OnLogoutSuccess(System.String)
extern "C"  void PluginNewMiquwan_OnLogoutSuccess_m3244239686 (PluginNewMiquwan_t2470753303 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginNewMiquwan::OnLogoutFail(System.String)
extern "C"  void PluginNewMiquwan_OnLogoutFail_m3263265723 (PluginNewMiquwan_t2470753303 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginNewMiquwan::MQWIn(System.String)
extern "C"  void PluginNewMiquwan_MQWIn_m4067873256 (Il2CppObject * __this /* static, unused */, String_t* ___appId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginNewMiquwan::MQWR(System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginNewMiquwan_MQWR_m3998107723 (Il2CppObject * __this /* static, unused */, String_t* ___playerId0, String_t* ___playerName1, String_t* ___playerLevel2, String_t* ___vipLv3, String_t* ___serverId4, String_t* ___serverName5, String_t* ___type6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginNewMiquwan::MQWL()
extern "C"  void PluginNewMiquwan_MQWL_m224708985 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginNewMiquwan::logout()
extern "C"  void PluginNewMiquwan_logout_m737578890 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginNewMiquwan::MQWP(System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginNewMiquwan_MQWP_m3383595257 (Il2CppObject * __this /* static, unused */, String_t* ___productId0, String_t* ___productName1, String_t* ___roleId2, String_t* ___roleName3, String_t* ___roleLevel4, String_t* ___Price5, String_t* ___extendInfo6, String_t* ___serverId7, String_t* ___serverName8, String_t* ___gold9, String_t* ___vipLv10, String_t* ___orderId11, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginNewMiquwan::<OnLogoutSuccess>m__444()
extern "C"  void PluginNewMiquwan_U3COnLogoutSuccessU3Em__444_m202551177 (PluginNewMiquwan_t2470753303 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionMgr PluginNewMiquwan::ilo_get_Instance1()
extern "C"  VersionMgr_t1322950208 * PluginNewMiquwan_ilo_get_Instance1_m2206900773 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionInfo PluginNewMiquwan::ilo_get_currentVS2(VersionMgr)
extern "C"  VersionInfo_t2356638086 * PluginNewMiquwan_ilo_get_currentVS2_m3535208005 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginNewMiquwan::ilo_MQWIn3(System.String)
extern "C"  void PluginNewMiquwan_ilo_MQWIn3_m3004328148 (Il2CppObject * __this /* static, unused */, String_t* ___appId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginNewMiquwan::ilo_MQWR4(System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginNewMiquwan_ilo_MQWR4_m1006901766 (Il2CppObject * __this /* static, unused */, String_t* ___playerId0, String_t* ___playerName1, String_t* ___playerLevel2, String_t* ___vipLv3, String_t* ___serverId4, String_t* ___serverName5, String_t* ___type6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PluginNewMiquwan::ilo_GetProductID5(ProductsCfgMgr,System.String)
extern "C"  String_t* PluginNewMiquwan_ilo_GetProductID5_m1201412561 (Il2CppObject * __this /* static, unused */, ProductsCfgMgr_t2493714872 * ____this0, String_t* ___name1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginNewMiquwan::ilo_MQWP6(System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginNewMiquwan_ilo_MQWP6_m3624855918 (Il2CppObject * __this /* static, unused */, String_t* ___productId0, String_t* ___productName1, String_t* ___roleId2, String_t* ___roleName3, String_t* ___roleLevel4, String_t* ___Price5, String_t* ___extendInfo6, String_t* ___serverId7, String_t* ___serverName8, String_t* ___gold9, String_t* ___vipLv10, String_t* ___orderId11, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginNewMiquwan::ilo_Log7(System.Object,System.Boolean)
extern "C"  void PluginNewMiquwan_ilo_Log7_m3067899887 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PluginsSdkMgr PluginNewMiquwan::ilo_get_PluginsSdkMgr8()
extern "C"  PluginsSdkMgr_t3884624670 * PluginNewMiquwan_ilo_get_PluginsSdkMgr8_m3968047243 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginNewMiquwan::ilo_ReqSDKHttpLogin9(PluginsSdkMgr)
extern "C"  void PluginNewMiquwan_ilo_ReqSDKHttpLogin9_m2684657773 (Il2CppObject * __this /* static, unused */, PluginsSdkMgr_t3884624670 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginNewMiquwan::ilo_Logout10(System.Action)
extern "C"  void PluginNewMiquwan_ilo_Logout10_m3665243201 (Il2CppObject * __this /* static, unused */, Action_t3771233898 * ___call0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginNewMiquwan::ilo_OpenUserLogin11(PluginNewMiquwan)
extern "C"  void PluginNewMiquwan_ilo_OpenUserLogin11_m561721356 (Il2CppObject * __this /* static, unused */, PluginNewMiquwan_t2470753303 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
