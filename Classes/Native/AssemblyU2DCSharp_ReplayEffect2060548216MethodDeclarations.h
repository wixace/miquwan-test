﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ReplayEffect
struct ReplayEffect_t2060548216;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// System.String[]
struct StringU5BU5D_t4054002952;
// System.String
struct String_t;
// CombatEntity
struct CombatEntity_t684137495;
// ReplayBase
struct ReplayBase_t779703160;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_ReplayBase779703160.h"
#include "AssemblyU2DCSharp_ReplayEffect2060548216.h"
#include "AssemblyU2DCSharp_CombatEntity684137495.h"

// System.Void ReplayEffect::.ctor()
extern "C"  void ReplayEffect__ctor_m2542436643 (ReplayEffect_t2060548216 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReplayEffect::.ctor(System.Object[])
extern "C"  void ReplayEffect__ctor_m4058654767 (ReplayEffect_t2060548216 * __this, ObjectU5BU5D_t1108656482* ___datas0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] ReplayEffect::ParserJsonStr(System.String)
extern "C"  StringU5BU5D_t4054002952* ReplayEffect_ParserJsonStr_m1958036760 (ReplayEffect_t2060548216 * __this, String_t* ___json0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ReplayEffect::GetJsonStr()
extern "C"  String_t* ReplayEffect_GetJsonStr_m2481900375 (ReplayEffect_t2060548216 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CombatEntity ReplayEffect::get_SrcEntity()
extern "C"  CombatEntity_t684137495 * ReplayEffect_get_SrcEntity_m1631618233 (ReplayEffect_t2060548216 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CombatEntity ReplayEffect::get_HostEntity()
extern "C"  CombatEntity_t684137495 * ReplayEffect_get_HostEntity_m1650128251 (ReplayEffect_t2060548216 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReplayEffect::Execute()
extern "C"  void ReplayEffect_Execute_m2296611958 (ReplayEffect_t2060548216 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] ReplayEffect::ilo_ParserJsonStr1(ReplayBase,System.String)
extern "C"  StringU5BU5D_t4054002952* ReplayEffect_ilo_ParserJsonStr1_m2146167200 (Il2CppObject * __this /* static, unused */, ReplayBase_t779703160 * ____this0, String_t* ___json1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ReplayEffect::ilo_GetJsonStr2(ReplayBase)
extern "C"  String_t* ReplayEffect_ilo_GetJsonStr2_m924434834 (Il2CppObject * __this /* static, unused */, ReplayBase_t779703160 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CombatEntity ReplayEffect::ilo_get_SrcEntity3(ReplayEffect)
extern "C"  CombatEntity_t684137495 * ReplayEffect_ilo_get_SrcEntity3_m2057989751 (Il2CppObject * __this /* static, unused */, ReplayEffect_t2060548216 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ReplayEffect::ilo_get_isDeath4(CombatEntity)
extern "C"  bool ReplayEffect_ilo_get_isDeath4_m1083292452 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CombatEntity ReplayEffect::ilo_get_HostEntity5(ReplayEffect)
extern "C"  CombatEntity_t684137495 * ReplayEffect_ilo_get_HostEntity5_m1213919857 (Il2CppObject * __this /* static, unused */, ReplayEffect_t2060548216 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
