﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.RecastBBTree
struct RecastBBTree_t4266206694;
// System.Collections.Generic.List`1<Pathfinding.RecastMeshObj>
struct List_1_t3437381290;
// Pathfinding.RecastBBTreeBox
struct RecastBBTreeBox_t3100392477;
// Pathfinding.RecastMeshObj
struct RecastMeshObj_t2069195738;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Rect4241904616.h"
#include "AssemblyU2DCSharp_Pathfinding_RecastBBTreeBox3100392477.h"
#include "AssemblyU2DCSharp_Pathfinding_RecastMeshObj2069195738.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_Pathfinding_RecastBBTree4266206694.h"

// System.Void Pathfinding.RecastBBTree::.ctor()
extern "C"  void RecastBBTree__ctor_m1640899393 (RecastBBTree_t4266206694 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RecastBBTree::QueryInBounds(UnityEngine.Rect,System.Collections.Generic.List`1<Pathfinding.RecastMeshObj>)
extern "C"  void RecastBBTree_QueryInBounds_m1072065426 (RecastBBTree_t4266206694 * __this, Rect_t4241904616  ___bounds0, List_1_t3437381290 * ___buffer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RecastBBTree::QueryBoxInBounds(Pathfinding.RecastBBTreeBox,UnityEngine.Rect,System.Collections.Generic.List`1<Pathfinding.RecastMeshObj>)
extern "C"  void RecastBBTree_QueryBoxInBounds_m1349768398 (RecastBBTree_t4266206694 * __this, RecastBBTreeBox_t3100392477 * ___box0, Rect_t4241904616  ___bounds1, List_1_t3437381290 * ___boxes2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.RecastBBTree::Remove(Pathfinding.RecastMeshObj)
extern "C"  bool RecastBBTree_Remove_m2895364393 (RecastBBTree_t4266206694 * __this, RecastMeshObj_t2069195738 * ___mesh0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.RecastBBTreeBox Pathfinding.RecastBBTree::RemoveBox(Pathfinding.RecastBBTreeBox,Pathfinding.RecastMeshObj,UnityEngine.Rect,System.Boolean&)
extern "C"  RecastBBTreeBox_t3100392477 * RecastBBTree_RemoveBox_m2959599647 (RecastBBTree_t4266206694 * __this, RecastBBTreeBox_t3100392477 * ___c0, RecastMeshObj_t2069195738 * ___mesh1, Rect_t4241904616  ___bounds2, bool* ___found3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RecastBBTree::Insert(Pathfinding.RecastMeshObj)
extern "C"  void RecastBBTree_Insert_m66710792 (RecastBBTree_t4266206694 * __this, RecastMeshObj_t2069195738 * ___mesh0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RecastBBTree::OnDrawGizmos()
extern "C"  void RecastBBTree_OnDrawGizmos_m2882204351 (RecastBBTree_t4266206694 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RecastBBTree::OnDrawGizmos(Pathfinding.RecastBBTreeBox)
extern "C"  void RecastBBTree_OnDrawGizmos_m3870190370 (RecastBBTree_t4266206694 * __this, RecastBBTreeBox_t3100392477 * ___box0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RecastBBTree::TestIntersections(UnityEngine.Vector3,System.Single)
extern "C"  void RecastBBTree_TestIntersections_m2486030567 (RecastBBTree_t4266206694 * __this, Vector3_t4282066566  ___p0, float ___radius1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RecastBBTree::TestIntersections(Pathfinding.RecastBBTreeBox,UnityEngine.Vector3,System.Single)
extern "C"  void RecastBBTree_TestIntersections_m2871623330 (RecastBBTree_t4266206694 * __this, RecastBBTreeBox_t3100392477 * ___box0, Vector3_t4282066566  ___p1, float ___radius2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.RecastBBTree::RectIntersectsRect(UnityEngine.Rect,UnityEngine.Rect)
extern "C"  bool RecastBBTree_RectIntersectsRect_m2896106547 (RecastBBTree_t4266206694 * __this, Rect_t4241904616  ___r0, Rect_t4241904616  ___r21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.RecastBBTree::RectIntersectsCircle(UnityEngine.Rect,UnityEngine.Vector3,System.Single)
extern "C"  bool RecastBBTree_RectIntersectsCircle_m112957372 (RecastBBTree_t4266206694 * __this, Rect_t4241904616  ___r0, Vector3_t4282066566  ___p1, float ___radius2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.RecastBBTree::RectContains(UnityEngine.Rect,UnityEngine.Vector3)
extern "C"  bool RecastBBTree_RectContains_m1789635538 (RecastBBTree_t4266206694 * __this, Rect_t4241904616  ___r0, Vector3_t4282066566  ___p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.RecastBBTree::ZIntersectsCircle(System.Single,System.Single,System.Single,UnityEngine.Vector3,System.Single)
extern "C"  bool RecastBBTree_ZIntersectsCircle_m1827490596 (RecastBBTree_t4266206694 * __this, float ___z10, float ___z21, float ___xpos2, Vector3_t4282066566  ___circle3, float ___radius4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.RecastBBTree::XIntersectsCircle(System.Single,System.Single,System.Single,UnityEngine.Vector3,System.Single)
extern "C"  bool RecastBBTree_XIntersectsCircle_m3446465510 (RecastBBTree_t4266206694 * __this, float ___x10, float ___x21, float ___zpos2, Vector3_t4282066566  ___circle3, float ___radius4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Pathfinding.RecastBBTree::ExpansionRequired(UnityEngine.Rect,UnityEngine.Rect)
extern "C"  float RecastBBTree_ExpansionRequired_m2347330869 (RecastBBTree_t4266206694 * __this, Rect_t4241904616  ___r0, Rect_t4241904616  ___r21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect Pathfinding.RecastBBTree::ExpandToContain(UnityEngine.Rect,UnityEngine.Rect)
extern "C"  Rect_t4241904616  RecastBBTree_ExpandToContain_m721714154 (RecastBBTree_t4266206694 * __this, Rect_t4241904616  ___r0, Rect_t4241904616  ___r21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Pathfinding.RecastBBTree::RectArea(UnityEngine.Rect)
extern "C"  float RecastBBTree_RectArea_m203466181 (RecastBBTree_t4266206694 * __this, Rect_t4241904616  ___r0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RecastBBTree::ToString()
extern "C"  void RecastBBTree_ToString_m2445395023 (RecastBBTree_t4266206694 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.RecastBBTreeBox Pathfinding.RecastBBTree::ilo_RemoveBox1(Pathfinding.RecastBBTree,Pathfinding.RecastBBTreeBox,Pathfinding.RecastMeshObj,UnityEngine.Rect,System.Boolean&)
extern "C"  RecastBBTreeBox_t3100392477 * RecastBBTree_ilo_RemoveBox1_m2638138489 (Il2CppObject * __this /* static, unused */, RecastBBTree_t4266206694 * ____this0, RecastBBTreeBox_t3100392477 * ___c1, RecastMeshObj_t2069195738 * ___mesh2, Rect_t4241904616  ___bounds3, bool* ___found4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect Pathfinding.RecastBBTree::ilo_ExpandToContain2(Pathfinding.RecastBBTree,UnityEngine.Rect,UnityEngine.Rect)
extern "C"  Rect_t4241904616  RecastBBTree_ilo_ExpandToContain2_m1136125841 (Il2CppObject * __this /* static, unused */, RecastBBTree_t4266206694 * ____this0, Rect_t4241904616  ___r1, Rect_t4241904616  ___r22, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RecastBBTree::ilo_TestIntersections3(Pathfinding.RecastBBTree,Pathfinding.RecastBBTreeBox,UnityEngine.Vector3,System.Single)
extern "C"  void RecastBBTree_ilo_TestIntersections3_m2881346250 (Il2CppObject * __this /* static, unused */, RecastBBTree_t4266206694 * ____this0, RecastBBTreeBox_t3100392477 * ___box1, Vector3_t4282066566  ___p2, float ___radius3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.RecastBBTree::ilo_ZIntersectsCircle4(Pathfinding.RecastBBTree,System.Single,System.Single,System.Single,UnityEngine.Vector3,System.Single)
extern "C"  bool RecastBBTree_ilo_ZIntersectsCircle4_m1528385805 (Il2CppObject * __this /* static, unused */, RecastBBTree_t4266206694 * ____this0, float ___z11, float ___z22, float ___xpos3, Vector3_t4282066566  ___circle4, float ___radius5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Pathfinding.RecastBBTree::ilo_RectArea5(Pathfinding.RecastBBTree,UnityEngine.Rect)
extern "C"  float RecastBBTree_ilo_RectArea5_m4171875873 (Il2CppObject * __this /* static, unused */, RecastBBTree_t4266206694 * ____this0, Rect_t4241904616  ___r1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RecastBBTree::ilo_WriteChildren6(Pathfinding.RecastBBTreeBox,System.Int32)
extern "C"  void RecastBBTree_ilo_WriteChildren6_m3182130372 (Il2CppObject * __this /* static, unused */, RecastBBTreeBox_t3100392477 * ____this0, int32_t ___level1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
