﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_geforsea96
struct  M_geforsea96_t943115953  : public Il2CppObject
{
public:
	// System.UInt32 GarbageiOS.M_geforsea96::_cermuCekoda
	uint32_t ____cermuCekoda_0;
	// System.Single GarbageiOS.M_geforsea96::_jearvair
	float ____jearvair_1;
	// System.String GarbageiOS.M_geforsea96::_wisyeyu
	String_t* ____wisyeyu_2;
	// System.UInt32 GarbageiOS.M_geforsea96::_heredere
	uint32_t ____heredere_3;
	// System.Boolean GarbageiOS.M_geforsea96::_piye
	bool ____piye_4;
	// System.Boolean GarbageiOS.M_geforsea96::_miha
	bool ____miha_5;
	// System.Boolean GarbageiOS.M_geforsea96::_nelralYurwerecea
	bool ____nelralYurwerecea_6;
	// System.Int32 GarbageiOS.M_geforsea96::_vetai
	int32_t ____vetai_7;
	// System.Boolean GarbageiOS.M_geforsea96::_burwharcoJurfouja
	bool ____burwharcoJurfouja_8;
	// System.Single GarbageiOS.M_geforsea96::_bairneFalcurjai
	float ____bairneFalcurjai_9;
	// System.UInt32 GarbageiOS.M_geforsea96::_sobutoCiras
	uint32_t ____sobutoCiras_10;
	// System.Single GarbageiOS.M_geforsea96::_cemrur
	float ____cemrur_11;
	// System.UInt32 GarbageiOS.M_geforsea96::_demnohasTesteretar
	uint32_t ____demnohasTesteretar_12;
	// System.Boolean GarbageiOS.M_geforsea96::_costoono
	bool ____costoono_13;
	// System.Single GarbageiOS.M_geforsea96::_curhirereNurrai
	float ____curhirereNurrai_14;
	// System.Int32 GarbageiOS.M_geforsea96::_feduMawhearchur
	int32_t ____feduMawhearchur_15;
	// System.String GarbageiOS.M_geforsea96::_cissoumiMeter
	String_t* ____cissoumiMeter_16;
	// System.String GarbageiOS.M_geforsea96::_biskai
	String_t* ____biskai_17;
	// System.Int32 GarbageiOS.M_geforsea96::_mercasur
	int32_t ____mercasur_18;
	// System.Boolean GarbageiOS.M_geforsea96::_gerpair
	bool ____gerpair_19;

public:
	inline static int32_t get_offset_of__cermuCekoda_0() { return static_cast<int32_t>(offsetof(M_geforsea96_t943115953, ____cermuCekoda_0)); }
	inline uint32_t get__cermuCekoda_0() const { return ____cermuCekoda_0; }
	inline uint32_t* get_address_of__cermuCekoda_0() { return &____cermuCekoda_0; }
	inline void set__cermuCekoda_0(uint32_t value)
	{
		____cermuCekoda_0 = value;
	}

	inline static int32_t get_offset_of__jearvair_1() { return static_cast<int32_t>(offsetof(M_geforsea96_t943115953, ____jearvair_1)); }
	inline float get__jearvair_1() const { return ____jearvair_1; }
	inline float* get_address_of__jearvair_1() { return &____jearvair_1; }
	inline void set__jearvair_1(float value)
	{
		____jearvair_1 = value;
	}

	inline static int32_t get_offset_of__wisyeyu_2() { return static_cast<int32_t>(offsetof(M_geforsea96_t943115953, ____wisyeyu_2)); }
	inline String_t* get__wisyeyu_2() const { return ____wisyeyu_2; }
	inline String_t** get_address_of__wisyeyu_2() { return &____wisyeyu_2; }
	inline void set__wisyeyu_2(String_t* value)
	{
		____wisyeyu_2 = value;
		Il2CppCodeGenWriteBarrier(&____wisyeyu_2, value);
	}

	inline static int32_t get_offset_of__heredere_3() { return static_cast<int32_t>(offsetof(M_geforsea96_t943115953, ____heredere_3)); }
	inline uint32_t get__heredere_3() const { return ____heredere_3; }
	inline uint32_t* get_address_of__heredere_3() { return &____heredere_3; }
	inline void set__heredere_3(uint32_t value)
	{
		____heredere_3 = value;
	}

	inline static int32_t get_offset_of__piye_4() { return static_cast<int32_t>(offsetof(M_geforsea96_t943115953, ____piye_4)); }
	inline bool get__piye_4() const { return ____piye_4; }
	inline bool* get_address_of__piye_4() { return &____piye_4; }
	inline void set__piye_4(bool value)
	{
		____piye_4 = value;
	}

	inline static int32_t get_offset_of__miha_5() { return static_cast<int32_t>(offsetof(M_geforsea96_t943115953, ____miha_5)); }
	inline bool get__miha_5() const { return ____miha_5; }
	inline bool* get_address_of__miha_5() { return &____miha_5; }
	inline void set__miha_5(bool value)
	{
		____miha_5 = value;
	}

	inline static int32_t get_offset_of__nelralYurwerecea_6() { return static_cast<int32_t>(offsetof(M_geforsea96_t943115953, ____nelralYurwerecea_6)); }
	inline bool get__nelralYurwerecea_6() const { return ____nelralYurwerecea_6; }
	inline bool* get_address_of__nelralYurwerecea_6() { return &____nelralYurwerecea_6; }
	inline void set__nelralYurwerecea_6(bool value)
	{
		____nelralYurwerecea_6 = value;
	}

	inline static int32_t get_offset_of__vetai_7() { return static_cast<int32_t>(offsetof(M_geforsea96_t943115953, ____vetai_7)); }
	inline int32_t get__vetai_7() const { return ____vetai_7; }
	inline int32_t* get_address_of__vetai_7() { return &____vetai_7; }
	inline void set__vetai_7(int32_t value)
	{
		____vetai_7 = value;
	}

	inline static int32_t get_offset_of__burwharcoJurfouja_8() { return static_cast<int32_t>(offsetof(M_geforsea96_t943115953, ____burwharcoJurfouja_8)); }
	inline bool get__burwharcoJurfouja_8() const { return ____burwharcoJurfouja_8; }
	inline bool* get_address_of__burwharcoJurfouja_8() { return &____burwharcoJurfouja_8; }
	inline void set__burwharcoJurfouja_8(bool value)
	{
		____burwharcoJurfouja_8 = value;
	}

	inline static int32_t get_offset_of__bairneFalcurjai_9() { return static_cast<int32_t>(offsetof(M_geforsea96_t943115953, ____bairneFalcurjai_9)); }
	inline float get__bairneFalcurjai_9() const { return ____bairneFalcurjai_9; }
	inline float* get_address_of__bairneFalcurjai_9() { return &____bairneFalcurjai_9; }
	inline void set__bairneFalcurjai_9(float value)
	{
		____bairneFalcurjai_9 = value;
	}

	inline static int32_t get_offset_of__sobutoCiras_10() { return static_cast<int32_t>(offsetof(M_geforsea96_t943115953, ____sobutoCiras_10)); }
	inline uint32_t get__sobutoCiras_10() const { return ____sobutoCiras_10; }
	inline uint32_t* get_address_of__sobutoCiras_10() { return &____sobutoCiras_10; }
	inline void set__sobutoCiras_10(uint32_t value)
	{
		____sobutoCiras_10 = value;
	}

	inline static int32_t get_offset_of__cemrur_11() { return static_cast<int32_t>(offsetof(M_geforsea96_t943115953, ____cemrur_11)); }
	inline float get__cemrur_11() const { return ____cemrur_11; }
	inline float* get_address_of__cemrur_11() { return &____cemrur_11; }
	inline void set__cemrur_11(float value)
	{
		____cemrur_11 = value;
	}

	inline static int32_t get_offset_of__demnohasTesteretar_12() { return static_cast<int32_t>(offsetof(M_geforsea96_t943115953, ____demnohasTesteretar_12)); }
	inline uint32_t get__demnohasTesteretar_12() const { return ____demnohasTesteretar_12; }
	inline uint32_t* get_address_of__demnohasTesteretar_12() { return &____demnohasTesteretar_12; }
	inline void set__demnohasTesteretar_12(uint32_t value)
	{
		____demnohasTesteretar_12 = value;
	}

	inline static int32_t get_offset_of__costoono_13() { return static_cast<int32_t>(offsetof(M_geforsea96_t943115953, ____costoono_13)); }
	inline bool get__costoono_13() const { return ____costoono_13; }
	inline bool* get_address_of__costoono_13() { return &____costoono_13; }
	inline void set__costoono_13(bool value)
	{
		____costoono_13 = value;
	}

	inline static int32_t get_offset_of__curhirereNurrai_14() { return static_cast<int32_t>(offsetof(M_geforsea96_t943115953, ____curhirereNurrai_14)); }
	inline float get__curhirereNurrai_14() const { return ____curhirereNurrai_14; }
	inline float* get_address_of__curhirereNurrai_14() { return &____curhirereNurrai_14; }
	inline void set__curhirereNurrai_14(float value)
	{
		____curhirereNurrai_14 = value;
	}

	inline static int32_t get_offset_of__feduMawhearchur_15() { return static_cast<int32_t>(offsetof(M_geforsea96_t943115953, ____feduMawhearchur_15)); }
	inline int32_t get__feduMawhearchur_15() const { return ____feduMawhearchur_15; }
	inline int32_t* get_address_of__feduMawhearchur_15() { return &____feduMawhearchur_15; }
	inline void set__feduMawhearchur_15(int32_t value)
	{
		____feduMawhearchur_15 = value;
	}

	inline static int32_t get_offset_of__cissoumiMeter_16() { return static_cast<int32_t>(offsetof(M_geforsea96_t943115953, ____cissoumiMeter_16)); }
	inline String_t* get__cissoumiMeter_16() const { return ____cissoumiMeter_16; }
	inline String_t** get_address_of__cissoumiMeter_16() { return &____cissoumiMeter_16; }
	inline void set__cissoumiMeter_16(String_t* value)
	{
		____cissoumiMeter_16 = value;
		Il2CppCodeGenWriteBarrier(&____cissoumiMeter_16, value);
	}

	inline static int32_t get_offset_of__biskai_17() { return static_cast<int32_t>(offsetof(M_geforsea96_t943115953, ____biskai_17)); }
	inline String_t* get__biskai_17() const { return ____biskai_17; }
	inline String_t** get_address_of__biskai_17() { return &____biskai_17; }
	inline void set__biskai_17(String_t* value)
	{
		____biskai_17 = value;
		Il2CppCodeGenWriteBarrier(&____biskai_17, value);
	}

	inline static int32_t get_offset_of__mercasur_18() { return static_cast<int32_t>(offsetof(M_geforsea96_t943115953, ____mercasur_18)); }
	inline int32_t get__mercasur_18() const { return ____mercasur_18; }
	inline int32_t* get_address_of__mercasur_18() { return &____mercasur_18; }
	inline void set__mercasur_18(int32_t value)
	{
		____mercasur_18 = value;
	}

	inline static int32_t get_offset_of__gerpair_19() { return static_cast<int32_t>(offsetof(M_geforsea96_t943115953, ____gerpair_19)); }
	inline bool get__gerpair_19() const { return ____gerpair_19; }
	inline bool* get_address_of__gerpair_19() { return &____gerpair_19; }
	inline void set__gerpair_19(bool value)
	{
		____gerpair_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
