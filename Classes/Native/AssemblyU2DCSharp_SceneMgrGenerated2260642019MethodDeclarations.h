﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SceneMgrGenerated
struct SceneMgrGenerated_t2260642019;
// JSVCall
struct JSVCall_t3708497963;
// System.String
struct String_t;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;
// SceneMgr
struct SceneMgr_t3584105996;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_SceneMgr3584105996.h"

// System.Void SceneMgrGenerated::.ctor()
extern "C"  void SceneMgrGenerated__ctor_m2519815144 (SceneMgrGenerated_t2260642019 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SceneMgrGenerated::SceneMgr_SceneMgr1(JSVCall,System.Int32)
extern "C"  bool SceneMgrGenerated_SceneMgr_SceneMgr1_m570847754 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SceneMgrGenerated::SceneMgr_SCENE_LOAD_OK(JSVCall)
extern "C"  void SceneMgrGenerated_SceneMgr_SCENE_LOAD_OK_m832832892 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SceneMgrGenerated::SceneMgr_JUMP_STORY(JSVCall)
extern "C"  void SceneMgrGenerated_SceneMgr_JUMP_STORY_m2758391498 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SceneMgrGenerated::SceneMgr_SCENE_START_LOAD(JSVCall)
extern "C"  void SceneMgrGenerated_SceneMgr_SCENE_START_LOAD_m2165606936 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SceneMgrGenerated::SceneMgr_PROGRESS_UPDATE(JSVCall)
extern "C"  void SceneMgrGenerated_SceneMgr_PROGRESS_UPDATE_m209322883 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SceneMgrGenerated::SceneMgr_curSceneName(JSVCall)
extern "C"  void SceneMgrGenerated_SceneMgr_curSceneName_m3955818711 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SceneMgrGenerated::SceneMgr_checkPoint(JSVCall)
extern "C"  void SceneMgrGenerated_SceneMgr_checkPoint_m737142630 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SceneMgrGenerated::SceneMgr_isloading(JSVCall)
extern "C"  void SceneMgrGenerated_SceneMgr_isloading_m1823700396 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SceneMgrGenerated::SceneMgr_Init(JSVCall,System.Int32)
extern "C"  bool SceneMgrGenerated_SceneMgr_Init_m952738477 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SceneMgrGenerated::SceneMgr_LoadScene__Int32(JSVCall,System.Int32)
extern "C"  bool SceneMgrGenerated_SceneMgr_LoadScene__Int32_m2306380325 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SceneMgrGenerated::SceneMgr_Logout(JSVCall,System.Int32)
extern "C"  bool SceneMgrGenerated_SceneMgr_Logout_m1958570151 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SceneMgrGenerated::SceneMgr_RequestEnter__UInt32(JSVCall,System.Int32)
extern "C"  bool SceneMgrGenerated_SceneMgr_RequestEnter__UInt32_m3500300223 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SceneMgrGenerated::SceneMgr_ZUpdate(JSVCall,System.Int32)
extern "C"  bool SceneMgrGenerated_SceneMgr_ZUpdate_m2712531624 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SceneMgrGenerated::__Register()
extern "C"  void SceneMgrGenerated___Register_m875583967 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SceneMgrGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t SceneMgrGenerated_ilo_getObject1_m2923465422 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SceneMgrGenerated::ilo_setStringS2(System.Int32,System.String)
extern "C"  void SceneMgrGenerated_ilo_setStringS2_m3533944626 (Il2CppObject * __this /* static, unused */, int32_t ___e0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SceneMgrGenerated::ilo_setObject3(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t SceneMgrGenerated_ilo_setObject3_m1395899080 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SceneMgrGenerated::ilo_get_isloading4(SceneMgr)
extern "C"  bool SceneMgrGenerated_ilo_get_isloading4_m3003288026 (Il2CppObject * __this /* static, unused */, SceneMgr_t3584105996 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SceneMgrGenerated::ilo_setBooleanS5(System.Int32,System.Boolean)
extern "C"  void SceneMgrGenerated_ilo_setBooleanS5_m897398485 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SceneMgrGenerated::ilo_getInt326(System.Int32)
extern "C"  int32_t SceneMgrGenerated_ilo_getInt326_m3045524560 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SceneMgrGenerated::ilo_RequestEnter7(SceneMgr,System.UInt32)
extern "C"  bool SceneMgrGenerated_ilo_RequestEnter7_m891804893 (Il2CppObject * __this /* static, unused */, SceneMgr_t3584105996 * ____this0, uint32_t ___sceneID1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
