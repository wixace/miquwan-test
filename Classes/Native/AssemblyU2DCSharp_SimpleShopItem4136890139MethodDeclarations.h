﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SimpleShopItem
struct SimpleShopItem_t4136890139;
// UnityEngine.Sprite
struct Sprite_t3199167241;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Sprite3199167241.h"
#include "mscorlib_System_String7231557.h"

// System.Void SimpleShopItem::.ctor()
extern "C"  void SimpleShopItem__ctor_m2034084832 (SimpleShopItem_t4136890139 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SimpleShopItem::Init(UnityEngine.Sprite,System.String,System.String)
extern "C"  void SimpleShopItem_Init_m3568348656 (SimpleShopItem_t4136890139 * __this, Sprite_t3199167241 * ___spr0, String_t* ___title1, String_t* ___price2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
