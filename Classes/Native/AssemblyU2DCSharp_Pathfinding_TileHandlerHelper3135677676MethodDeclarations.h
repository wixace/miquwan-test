﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.TileHandlerHelper
struct TileHandlerHelper_t3135677676;
// Pathfinding.Util.TileHandler
struct TileHandler_t1505605502;
// Pathfinding.NavmeshCut
struct NavmeshCut_t300992648;
// System.Action`1<Pathfinding.NavmeshCut>
struct Action_1_t696808784;
// System.Collections.Generic.List`1<Pathfinding.NavmeshCut>
struct List_1_t1669178200;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_Util_TileHandler1505605502.h"
#include "AssemblyU2DCSharp_Pathfinding_NavmeshCut300992648.h"
#include "UnityEngine_UnityEngine_Bounds2711641849.h"

// System.Void Pathfinding.TileHandlerHelper::.ctor()
extern "C"  void TileHandlerHelper__ctor_m56765899 (TileHandlerHelper_t3135677676 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.TileHandlerHelper::UseSpecifiedHandler(Pathfinding.Util.TileHandler)
extern "C"  void TileHandlerHelper_UseSpecifiedHandler_m3279916464 (TileHandlerHelper_t3135677676 * __this, TileHandler_t1505605502 * ___handler0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.TileHandlerHelper::OnEnable()
extern "C"  void TileHandlerHelper_OnEnable_m967360987 (TileHandlerHelper_t3135677676 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.TileHandlerHelper::OnDisable()
extern "C"  void TileHandlerHelper_OnDisable_m364356786 (TileHandlerHelper_t3135677676 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.TileHandlerHelper::DiscardPending()
extern "C"  void TileHandlerHelper_DiscardPending_m1219233010 (TileHandlerHelper_t3135677676 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.TileHandlerHelper::Start()
extern "C"  void TileHandlerHelper_Start_m3298870987 (TileHandlerHelper_t3135677676 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.TileHandlerHelper::HandleOnDestroyCallback(Pathfinding.NavmeshCut)
extern "C"  void TileHandlerHelper_HandleOnDestroyCallback_m2754133605 (TileHandlerHelper_t3135677676 * __this, NavmeshCut_t300992648 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.TileHandlerHelper::Update()
extern "C"  void TileHandlerHelper_Update_m3486604962 (TileHandlerHelper_t3135677676 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.TileHandlerHelper::ForceUpdate()
extern "C"  void TileHandlerHelper_ForceUpdate_m258376573 (TileHandlerHelper_t3135677676 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.TileHandlerHelper::ilo_add_OnDestroyCallback1(System.Action`1<Pathfinding.NavmeshCut>)
extern "C"  void TileHandlerHelper_ilo_add_OnDestroyCallback1_m632889803 (Il2CppObject * __this /* static, unused */, Action_1_t696808784 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.TileHandlerHelper::ilo_NotifyUpdated2(Pathfinding.NavmeshCut)
extern "C"  void TileHandlerHelper_ilo_NotifyUpdated2_m3711914384 (Il2CppObject * __this /* static, unused */, NavmeshCut_t300992648 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.TileHandlerHelper::ilo_CreateTileTypesFromGraph3(Pathfinding.Util.TileHandler)
extern "C"  void TileHandlerHelper_ilo_CreateTileTypesFromGraph3_m3018856282 (Il2CppObject * __this /* static, unused */, TileHandler_t1505605502 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<Pathfinding.NavmeshCut> Pathfinding.TileHandlerHelper::ilo_GetAll4()
extern "C"  List_1_t1669178200 * TileHandlerHelper_ilo_GetAll4_m2413226702 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.TileHandlerHelper::ilo_RequiresUpdate5(Pathfinding.NavmeshCut)
extern "C"  bool TileHandlerHelper_ilo_RequiresUpdate5_m3524687948 (Il2CppObject * __this /* static, unused */, NavmeshCut_t300992648 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Bounds Pathfinding.TileHandlerHelper::ilo_get_LastBounds6(Pathfinding.NavmeshCut)
extern "C"  Bounds_t2711641849  TileHandlerHelper_ilo_get_LastBounds6_m2170088173 (Il2CppObject * __this /* static, unused */, NavmeshCut_t300992648 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
