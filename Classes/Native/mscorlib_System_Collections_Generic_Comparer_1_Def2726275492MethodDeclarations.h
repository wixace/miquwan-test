﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1/DefaultComparer<Pathfinding.Voxels.ExtraMesh>
struct DefaultComparer_t2726275492;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_Voxels_ExtraMesh4218029715.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<Pathfinding.Voxels.ExtraMesh>::.ctor()
extern "C"  void DefaultComparer__ctor_m648500140_gshared (DefaultComparer_t2726275492 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m648500140(__this, method) ((  void (*) (DefaultComparer_t2726275492 *, const MethodInfo*))DefaultComparer__ctor_m648500140_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<Pathfinding.Voxels.ExtraMesh>::Compare(T,T)
extern "C"  int32_t DefaultComparer_Compare_m78450251_gshared (DefaultComparer_t2726275492 * __this, ExtraMesh_t4218029715  ___x0, ExtraMesh_t4218029715  ___y1, const MethodInfo* method);
#define DefaultComparer_Compare_m78450251(__this, ___x0, ___y1, method) ((  int32_t (*) (DefaultComparer_t2726275492 *, ExtraMesh_t4218029715 , ExtraMesh_t4218029715 , const MethodInfo*))DefaultComparer_Compare_m78450251_gshared)(__this, ___x0, ___y1, method)
