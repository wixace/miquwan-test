﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.IO.TextWriter
struct TextWriter_t2304124208;
// System.String
struct String_t;
// System.IO.StringWriter
struct StringWriter_t4216882900;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IO_TextWriter2304124208.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Nullable_1_gen1237965023.h"

// System.Void Newtonsoft.Json.Utilities.JavaScriptUtils::WriteEscapedJavaScriptString(System.IO.TextWriter,System.String,System.Char,System.Boolean)
extern "C"  void JavaScriptUtils_WriteEscapedJavaScriptString_m3530770797 (Il2CppObject * __this /* static, unused */, TextWriter_t2304124208 * ___writer0, String_t* ___value1, Il2CppChar ___delimiter2, bool ___appendDelimiters3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Utilities.JavaScriptUtils::ToEscapedJavaScriptString(System.String)
extern "C"  String_t* JavaScriptUtils_ToEscapedJavaScriptString_m2746513563 (Il2CppObject * __this /* static, unused */, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Utilities.JavaScriptUtils::ToEscapedJavaScriptString(System.String,System.Char,System.Boolean)
extern "C"  String_t* JavaScriptUtils_ToEscapedJavaScriptString_m3737412235 (Il2CppObject * __this /* static, unused */, String_t* ___value0, Il2CppChar ___delimiter1, bool ___appendDelimiters2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Utilities.JavaScriptUtils::ilo_ToEscapedJavaScriptString1(System.String,System.Char,System.Boolean)
extern "C"  String_t* JavaScriptUtils_ilo_ToEscapedJavaScriptString1_m554009955 (Il2CppObject * __this /* static, unused */, String_t* ___value0, Il2CppChar ___delimiter1, bool ___appendDelimiters2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Int32> Newtonsoft.Json.Utilities.JavaScriptUtils::ilo_GetLength2(System.String)
extern "C"  Nullable_1_t1237965023  JavaScriptUtils_ilo_GetLength2_m2601670692 (Il2CppObject * __this /* static, unused */, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.StringWriter Newtonsoft.Json.Utilities.JavaScriptUtils::ilo_CreateStringWriter3(System.Int32)
extern "C"  StringWriter_t4216882900 * JavaScriptUtils_ilo_CreateStringWriter3_m4082094431 (Il2CppObject * __this /* static, unused */, int32_t ___capacity0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Utilities.JavaScriptUtils::ilo_WriteEscapedJavaScriptString4(System.IO.TextWriter,System.String,System.Char,System.Boolean)
extern "C"  void JavaScriptUtils_ilo_WriteEscapedJavaScriptString4_m1000732886 (Il2CppObject * __this /* static, unused */, TextWriter_t2304124208 * ___writer0, String_t* ___value1, Il2CppChar ___delimiter2, bool ___appendDelimiters3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
