﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ProtoBuf.Serializers.UInt16Serializer
struct UInt16Serializer_t3377533291;
// ProtoBuf.Meta.TypeModel
struct TypeModel_t2730011105;
// System.Type
struct Type_t;
// System.Object
struct Il2CppObject;
// ProtoBuf.ProtoReader
struct ProtoReader_t3962509489;
// ProtoBuf.ProtoWriter
struct ProtoWriter_t4117914721;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ProtoBuf_Meta_TypeModel2730011105.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_ProtoBuf_ProtoReader3962509489.h"
#include "AssemblyU2DCSharp_ProtoBuf_ProtoWriter4117914721.h"

// System.Void ProtoBuf.Serializers.UInt16Serializer::.ctor(ProtoBuf.Meta.TypeModel)
extern "C"  void UInt16Serializer__ctor_m682505311 (UInt16Serializer_t3377533291 * __this, TypeModel_t2730011105 * ___model0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Serializers.UInt16Serializer::.cctor()
extern "C"  void UInt16Serializer__cctor_m2319512867 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Serializers.UInt16Serializer::ProtoBuf.Serializers.IProtoSerializer.get_RequiresOldValue()
extern "C"  bool UInt16Serializer_ProtoBuf_Serializers_IProtoSerializer_get_RequiresOldValue_m1453665492 (UInt16Serializer_t3377533291 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Serializers.UInt16Serializer::ProtoBuf.Serializers.IProtoSerializer.get_ReturnsValue()
extern "C"  bool UInt16Serializer_ProtoBuf_Serializers_IProtoSerializer_get_ReturnsValue_m872458602 (UInt16Serializer_t3377533291 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type ProtoBuf.Serializers.UInt16Serializer::get_ExpectedType()
extern "C"  Type_t * UInt16Serializer_get_ExpectedType_m3433364091 (UInt16Serializer_t3377533291 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProtoBuf.Serializers.UInt16Serializer::Read(System.Object,ProtoBuf.ProtoReader)
extern "C"  Il2CppObject * UInt16Serializer_Read_m497978405 (UInt16Serializer_t3377533291 * __this, Il2CppObject * ___value0, ProtoReader_t3962509489 * ___source1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Serializers.UInt16Serializer::Write(System.Object,ProtoBuf.ProtoWriter)
extern "C"  void UInt16Serializer_Write_m1757619905 (UInt16Serializer_t3377533291 * __this, Il2CppObject * ___value0, ProtoWriter_t4117914721 * ___dest1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt16 ProtoBuf.Serializers.UInt16Serializer::ilo_ReadUInt161(ProtoBuf.ProtoReader)
extern "C"  uint16_t UInt16Serializer_ilo_ReadUInt161_m2234282078 (Il2CppObject * __this /* static, unused */, ProtoReader_t3962509489 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Serializers.UInt16Serializer::ilo_WriteUInt162(System.UInt16,ProtoBuf.ProtoWriter)
extern "C"  void UInt16Serializer_ilo_WriteUInt162_m3909833179 (Il2CppObject * __this /* static, unused */, uint16_t ___value0, ProtoWriter_t4117914721 * ___writer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
