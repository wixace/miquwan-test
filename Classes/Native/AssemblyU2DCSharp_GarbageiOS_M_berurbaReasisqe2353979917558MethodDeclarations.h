﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_berurbaReasisqe235
struct M_berurbaReasisqe235_t3979917558;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_berurbaReasisqe2353979917558.h"

// System.Void GarbageiOS.M_berurbaReasisqe235::.ctor()
extern "C"  void M_berurbaReasisqe235__ctor_m2232344573 (M_berurbaReasisqe235_t3979917558 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_berurbaReasisqe235::M_temtis0(System.String[],System.Int32)
extern "C"  void M_berurbaReasisqe235_M_temtis0_m1894296430 (M_berurbaReasisqe235_t3979917558 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_berurbaReasisqe235::M_dorstar1(System.String[],System.Int32)
extern "C"  void M_berurbaReasisqe235_M_dorstar1_m4194957608 (M_berurbaReasisqe235_t3979917558 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_berurbaReasisqe235::M_biyoudree2(System.String[],System.Int32)
extern "C"  void M_berurbaReasisqe235_M_biyoudree2_m460972476 (M_berurbaReasisqe235_t3979917558 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_berurbaReasisqe235::M_rearhem3(System.String[],System.Int32)
extern "C"  void M_berurbaReasisqe235_M_rearhem3_m62314615 (M_berurbaReasisqe235_t3979917558 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_berurbaReasisqe235::M_jeke4(System.String[],System.Int32)
extern "C"  void M_berurbaReasisqe235_M_jeke4_m2572732223 (M_berurbaReasisqe235_t3979917558 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_berurbaReasisqe235::M_disbemsu5(System.String[],System.Int32)
extern "C"  void M_berurbaReasisqe235_M_disbemsu5_m205723031 (M_berurbaReasisqe235_t3979917558 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_berurbaReasisqe235::ilo_M_temtis01(GarbageiOS.M_berurbaReasisqe235,System.String[],System.Int32)
extern "C"  void M_berurbaReasisqe235_ilo_M_temtis01_m1973805928 (Il2CppObject * __this /* static, unused */, M_berurbaReasisqe235_t3979917558 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_berurbaReasisqe235::ilo_M_rearhem32(GarbageiOS.M_berurbaReasisqe235,System.String[],System.Int32)
extern "C"  void M_berurbaReasisqe235_ilo_M_rearhem32_m2359158202 (Il2CppObject * __this /* static, unused */, M_berurbaReasisqe235_t3979917558 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
