﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Pathfinding.Poly2Tri.TriangulationPoint
struct TriangulationPoint_t3810082933;

#include "mscorlib_System_ValueType1744280289.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.Poly2Tri.FixedArray3`1<Pathfinding.Poly2Tri.TriangulationPoint>
struct  FixedArray3_1_t165589287 
{
public:
	// T Pathfinding.Poly2Tri.FixedArray3`1::_0
	TriangulationPoint_t3810082933 * ____0_0;
	// T Pathfinding.Poly2Tri.FixedArray3`1::_1
	TriangulationPoint_t3810082933 * ____1_1;
	// T Pathfinding.Poly2Tri.FixedArray3`1::_2
	TriangulationPoint_t3810082933 * ____2_2;

public:
	inline static int32_t get_offset_of__0_0() { return static_cast<int32_t>(offsetof(FixedArray3_1_t165589287, ____0_0)); }
	inline TriangulationPoint_t3810082933 * get__0_0() const { return ____0_0; }
	inline TriangulationPoint_t3810082933 ** get_address_of__0_0() { return &____0_0; }
	inline void set__0_0(TriangulationPoint_t3810082933 * value)
	{
		____0_0 = value;
		Il2CppCodeGenWriteBarrier(&____0_0, value);
	}

	inline static int32_t get_offset_of__1_1() { return static_cast<int32_t>(offsetof(FixedArray3_1_t165589287, ____1_1)); }
	inline TriangulationPoint_t3810082933 * get__1_1() const { return ____1_1; }
	inline TriangulationPoint_t3810082933 ** get_address_of__1_1() { return &____1_1; }
	inline void set__1_1(TriangulationPoint_t3810082933 * value)
	{
		____1_1 = value;
		Il2CppCodeGenWriteBarrier(&____1_1, value);
	}

	inline static int32_t get_offset_of__2_2() { return static_cast<int32_t>(offsetof(FixedArray3_1_t165589287, ____2_2)); }
	inline TriangulationPoint_t3810082933 * get__2_2() const { return ____2_2; }
	inline TriangulationPoint_t3810082933 ** get_address_of__2_2() { return &____2_2; }
	inline void set__2_2(TriangulationPoint_t3810082933 * value)
	{
		____2_2 = value;
		Il2CppCodeGenWriteBarrier(&____2_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
