﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mihua_MEventDelegateGenerated
struct Mihua_MEventDelegateGenerated_t3614693686;
// Mihua.MEventDelegate/RequestDelegate
struct RequestDelegate_t1822766829;
// CSRepresentedObject
struct CSRepresentedObject_t3994124630;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CSRepresentedObject3994124630.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"

// System.Void Mihua_MEventDelegateGenerated::.ctor()
extern "C"  void Mihua_MEventDelegateGenerated__ctor_m2018096373 (Mihua_MEventDelegateGenerated_t3614693686 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mihua.MEventDelegate/RequestDelegate Mihua_MEventDelegateGenerated::MEventDelegate__ctor_GetDelegate_member0_arg0(CSRepresentedObject)
extern "C"  RequestDelegate_t1822766829 * Mihua_MEventDelegateGenerated_MEventDelegate__ctor_GetDelegate_member0_arg0_m590182745 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mihua_MEventDelegateGenerated::MEventDelegate_MEventDelegate1(JSVCall,System.Int32)
extern "C"  bool Mihua_MEventDelegateGenerated_MEventDelegate_MEventDelegate1_m1671205021 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mihua.MEventDelegate/RequestDelegate Mihua_MEventDelegateGenerated::MEventDelegate_requestDelegate_GetDelegate_member0_arg0(CSRepresentedObject)
extern "C"  RequestDelegate_t1822766829 * Mihua_MEventDelegateGenerated_MEventDelegate_requestDelegate_GetDelegate_member0_arg0_m1615386778 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua_MEventDelegateGenerated::MEventDelegate_requestDelegate(JSVCall)
extern "C"  void Mihua_MEventDelegateGenerated_MEventDelegate_requestDelegate_m2486919345 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua_MEventDelegateGenerated::__Register()
extern "C"  void Mihua_MEventDelegateGenerated___Register_m2103154930 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mihua.MEventDelegate/RequestDelegate Mihua_MEventDelegateGenerated::<MEventDelegate_MEventDelegate1>m__80()
extern "C"  RequestDelegate_t1822766829 * Mihua_MEventDelegateGenerated_U3CMEventDelegate_MEventDelegate1U3Em__80_m1402347288 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mihua.MEventDelegate/RequestDelegate Mihua_MEventDelegateGenerated::<MEventDelegate_requestDelegate>m__82()
extern "C"  RequestDelegate_t1822766829 * Mihua_MEventDelegateGenerated_U3CMEventDelegate_requestDelegateU3Em__82_m2407876431 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mihua_MEventDelegateGenerated::ilo_attachFinalizerObject1(System.Int32)
extern "C"  bool Mihua_MEventDelegateGenerated_ilo_attachFinalizerObject1_m509529762 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mihua_MEventDelegateGenerated::ilo_isFunctionS2(System.Int32)
extern "C"  bool Mihua_MEventDelegateGenerated_ilo_isFunctionS2_m3932017856 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CSRepresentedObject Mihua_MEventDelegateGenerated::ilo_getFunctionS3(System.Int32)
extern "C"  CSRepresentedObject_t3994124630 * Mihua_MEventDelegateGenerated_ilo_getFunctionS3_m2511511228 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Mihua_MEventDelegateGenerated::ilo_getObject4(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * Mihua_MEventDelegateGenerated_ilo_getObject4_m2913554451 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
