﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType1744280289.h"
#include "AssemblyU2DCSharp_Pathfinding_LocalAvoidance_Inter1564900636.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.LocalAvoidance/IntersectionPair
struct  IntersectionPair_t2821319919 
{
public:
	// System.Single Pathfinding.LocalAvoidance/IntersectionPair::factor
	float ___factor_0;
	// Pathfinding.LocalAvoidance/IntersectionState Pathfinding.LocalAvoidance/IntersectionPair::state
	int32_t ___state_1;

public:
	inline static int32_t get_offset_of_factor_0() { return static_cast<int32_t>(offsetof(IntersectionPair_t2821319919, ___factor_0)); }
	inline float get_factor_0() const { return ___factor_0; }
	inline float* get_address_of_factor_0() { return &___factor_0; }
	inline void set_factor_0(float value)
	{
		___factor_0 = value;
	}

	inline static int32_t get_offset_of_state_1() { return static_cast<int32_t>(offsetof(IntersectionPair_t2821319919, ___state_1)); }
	inline int32_t get_state_1() const { return ___state_1; }
	inline int32_t* get_address_of_state_1() { return &___state_1; }
	inline void set_state_1(int32_t value)
	{
		___state_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for marshalling of: Pathfinding.LocalAvoidance/IntersectionPair
struct IntersectionPair_t2821319919_marshaled_pinvoke
{
	float ___factor_0;
	int32_t ___state_1;
};
// Native definition for marshalling of: Pathfinding.LocalAvoidance/IntersectionPair
struct IntersectionPair_t2821319919_marshaled_com
{
	float ___factor_0;
	int32_t ___state_1;
};
