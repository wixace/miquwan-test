﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PluginGuangdiantong
struct PluginGuangdiantong_t1625076095;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void PluginGuangdiantong::.ctor()
extern "C"  void PluginGuangdiantong__ctor_m1885839564 (PluginGuangdiantong_t1625076095 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginGuangdiantong::Init()
extern "C"  void PluginGuangdiantong_Init_m843654152 (PluginGuangdiantong_t1625076095 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginGuangdiantong::CreatRole(CEvent.ZEvent)
extern "C"  void PluginGuangdiantong_CreatRole_m3277462738 (PluginGuangdiantong_t1625076095 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginGuangdiantong::RoleUpgrade(CEvent.ZEvent)
extern "C"  void PluginGuangdiantong_RoleUpgrade_m3561410571 (PluginGuangdiantong_t1625076095 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginGuangdiantong::UserPay(CEvent.ZEvent)
extern "C"  void PluginGuangdiantong_UserPay_m3611410068 (PluginGuangdiantong_t1625076095 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginGuangdiantong::initGdt(System.String,System.String)
extern "C"  void PluginGuangdiantong_initGdt_m4284503821 (Il2CppObject * __this /* static, unused */, String_t* ___initId0, String_t* ___initKey1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginGuangdiantong::creatRoleGdt(System.String,System.String,System.String,System.String)
extern "C"  void PluginGuangdiantong_creatRoleGdt_m2992319558 (Il2CppObject * __this /* static, unused */, String_t* ___serverName0, String_t* ___roleName1, String_t* ___serverId2, String_t* ___roleId3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginGuangdiantong::updateRoleGdt(System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginGuangdiantong_updateRoleGdt_m3304378508 (Il2CppObject * __this /* static, unused */, String_t* ___serverName0, String_t* ___roleName1, String_t* ___serverId2, String_t* ___roleId3, String_t* ___vipLv4, String_t* ___roleLv5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginGuangdiantong::gdtP(System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginGuangdiantong_gdtP_m361887217 (Il2CppObject * __this /* static, unused */, String_t* ___roleId0, String_t* ___roleName1, String_t* ___serverId2, String_t* ___money3, String_t* ___orderNo4, String_t* ___productName5, String_t* ___productType6, String_t* ___productId7, String_t* ___payNum8, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginGuangdiantong::ilo_Log1(System.Object,System.Boolean)
extern "C"  void PluginGuangdiantong_ilo_Log1_m4275940609 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginGuangdiantong::ilo_creatRoleGdt2(System.String,System.String,System.String,System.String)
extern "C"  void PluginGuangdiantong_ilo_creatRoleGdt2_m590044805 (Il2CppObject * __this /* static, unused */, String_t* ___serverName0, String_t* ___roleName1, String_t* ___serverId2, String_t* ___roleId3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
