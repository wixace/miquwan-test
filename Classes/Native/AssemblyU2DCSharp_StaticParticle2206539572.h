﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<UnityEngine.ParticleSystem>
struct List_1_t1749658729;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StaticParticle
struct  StaticParticle_t2206539572  : public Il2CppObject
{
public:
	// System.Boolean StaticParticle::isCurRander
	bool ___isCurRander_0;
	// System.Boolean StaticParticle::isShowRander
	bool ___isShowRander_1;
	// System.Collections.Generic.List`1<UnityEngine.ParticleSystem> StaticParticle::particleList
	List_1_t1749658729 * ___particleList_2;

public:
	inline static int32_t get_offset_of_isCurRander_0() { return static_cast<int32_t>(offsetof(StaticParticle_t2206539572, ___isCurRander_0)); }
	inline bool get_isCurRander_0() const { return ___isCurRander_0; }
	inline bool* get_address_of_isCurRander_0() { return &___isCurRander_0; }
	inline void set_isCurRander_0(bool value)
	{
		___isCurRander_0 = value;
	}

	inline static int32_t get_offset_of_isShowRander_1() { return static_cast<int32_t>(offsetof(StaticParticle_t2206539572, ___isShowRander_1)); }
	inline bool get_isShowRander_1() const { return ___isShowRander_1; }
	inline bool* get_address_of_isShowRander_1() { return &___isShowRander_1; }
	inline void set_isShowRander_1(bool value)
	{
		___isShowRander_1 = value;
	}

	inline static int32_t get_offset_of_particleList_2() { return static_cast<int32_t>(offsetof(StaticParticle_t2206539572, ___particleList_2)); }
	inline List_1_t1749658729 * get_particleList_2() const { return ___particleList_2; }
	inline List_1_t1749658729 ** get_address_of_particleList_2() { return &___particleList_2; }
	inline void set_particleList_2(List_1_t1749658729 * value)
	{
		___particleList_2 = value;
		Il2CppCodeGenWriteBarrier(&___particleList_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
