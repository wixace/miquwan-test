﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Pathfinding.Poly2Tri.DTSweepConstraint
struct DTSweepConstraint_t3360279023;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.Poly2Tri.DTSweepEdgeEvent
struct  DTSweepEdgeEvent_t3441061589  : public Il2CppObject
{
public:
	// Pathfinding.Poly2Tri.DTSweepConstraint Pathfinding.Poly2Tri.DTSweepEdgeEvent::ConstrainedEdge
	DTSweepConstraint_t3360279023 * ___ConstrainedEdge_0;
	// System.Boolean Pathfinding.Poly2Tri.DTSweepEdgeEvent::Right
	bool ___Right_1;

public:
	inline static int32_t get_offset_of_ConstrainedEdge_0() { return static_cast<int32_t>(offsetof(DTSweepEdgeEvent_t3441061589, ___ConstrainedEdge_0)); }
	inline DTSweepConstraint_t3360279023 * get_ConstrainedEdge_0() const { return ___ConstrainedEdge_0; }
	inline DTSweepConstraint_t3360279023 ** get_address_of_ConstrainedEdge_0() { return &___ConstrainedEdge_0; }
	inline void set_ConstrainedEdge_0(DTSweepConstraint_t3360279023 * value)
	{
		___ConstrainedEdge_0 = value;
		Il2CppCodeGenWriteBarrier(&___ConstrainedEdge_0, value);
	}

	inline static int32_t get_offset_of_Right_1() { return static_cast<int32_t>(offsetof(DTSweepEdgeEvent_t3441061589, ___Right_1)); }
	inline bool get_Right_1() const { return ___Right_1; }
	inline bool* get_address_of_Right_1() { return &___Right_1; }
	inline void set_Right_1(bool value)
	{
		___Right_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
