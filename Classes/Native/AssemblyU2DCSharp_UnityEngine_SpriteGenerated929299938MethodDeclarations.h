﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_SpriteGenerated
struct UnityEngine_SpriteGenerated_t929299938;
// JSVCall
struct JSVCall_t3708497963;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t4024180168;
// System.UInt16[]
struct UInt16U5BU5D_t801649474;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"

// System.Void UnityEngine_SpriteGenerated::.ctor()
extern "C"  void UnityEngine_SpriteGenerated__ctor_m647441353 (UnityEngine_SpriteGenerated_t929299938 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_SpriteGenerated::Sprite_Sprite1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_SpriteGenerated_Sprite_Sprite1_m2338809769 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SpriteGenerated::Sprite_bounds(JSVCall)
extern "C"  void UnityEngine_SpriteGenerated_Sprite_bounds_m3645738065 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SpriteGenerated::Sprite_rect(JSVCall)
extern "C"  void UnityEngine_SpriteGenerated_Sprite_rect_m3810412034 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SpriteGenerated::Sprite_pixelsPerUnit(JSVCall)
extern "C"  void UnityEngine_SpriteGenerated_Sprite_pixelsPerUnit_m2964984146 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SpriteGenerated::Sprite_texture(JSVCall)
extern "C"  void UnityEngine_SpriteGenerated_Sprite_texture_m842172651 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SpriteGenerated::Sprite_associatedAlphaSplitTexture(JSVCall)
extern "C"  void UnityEngine_SpriteGenerated_Sprite_associatedAlphaSplitTexture_m874282765 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SpriteGenerated::Sprite_textureRect(JSVCall)
extern "C"  void UnityEngine_SpriteGenerated_Sprite_textureRect_m3957523687 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SpriteGenerated::Sprite_textureRectOffset(JSVCall)
extern "C"  void UnityEngine_SpriteGenerated_Sprite_textureRectOffset_m2147090644 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SpriteGenerated::Sprite_packed(JSVCall)
extern "C"  void UnityEngine_SpriteGenerated_Sprite_packed_m3514302958 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SpriteGenerated::Sprite_packingMode(JSVCall)
extern "C"  void UnityEngine_SpriteGenerated_Sprite_packingMode_m3543250106 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SpriteGenerated::Sprite_packingRotation(JSVCall)
extern "C"  void UnityEngine_SpriteGenerated_Sprite_packingRotation_m360165823 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SpriteGenerated::Sprite_pivot(JSVCall)
extern "C"  void UnityEngine_SpriteGenerated_Sprite_pivot_m3465328036 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SpriteGenerated::Sprite_border(JSVCall)
extern "C"  void UnityEngine_SpriteGenerated_Sprite_border_m1634682042 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SpriteGenerated::Sprite_vertices(JSVCall)
extern "C"  void UnityEngine_SpriteGenerated_Sprite_vertices_m2991673229 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SpriteGenerated::Sprite_triangles(JSVCall)
extern "C"  void UnityEngine_SpriteGenerated_Sprite_triangles_m2083960987 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SpriteGenerated::Sprite_uv(JSVCall)
extern "C"  void UnityEngine_SpriteGenerated_Sprite_uv_m4153511397 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_SpriteGenerated::Sprite_OverrideGeometry__Vector2_Array__UInt16_Array(JSVCall,System.Int32)
extern "C"  bool UnityEngine_SpriteGenerated_Sprite_OverrideGeometry__Vector2_Array__UInt16_Array_m2581293409 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_SpriteGenerated::Sprite_Create__Texture2D__Rect__Vector2__Single__UInt32__SpriteMeshType__Vector4(JSVCall,System.Int32)
extern "C"  bool UnityEngine_SpriteGenerated_Sprite_Create__Texture2D__Rect__Vector2__Single__UInt32__SpriteMeshType__Vector4_m1857930663 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_SpriteGenerated::Sprite_Create__Texture2D__Rect__Vector2__Single__UInt32__SpriteMeshType(JSVCall,System.Int32)
extern "C"  bool UnityEngine_SpriteGenerated_Sprite_Create__Texture2D__Rect__Vector2__Single__UInt32__SpriteMeshType_m4158674796 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_SpriteGenerated::Sprite_Create__Texture2D__Rect__Vector2__Single__UInt32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_SpriteGenerated_Sprite_Create__Texture2D__Rect__Vector2__Single__UInt32_m3993756096 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_SpriteGenerated::Sprite_Create__Texture2D__Rect__Vector2__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_SpriteGenerated_Sprite_Create__Texture2D__Rect__Vector2__Single_m240378215 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_SpriteGenerated::Sprite_Create__Texture2D__Rect__Vector2(JSVCall,System.Int32)
extern "C"  bool UnityEngine_SpriteGenerated_Sprite_Create__Texture2D__Rect__Vector2_m408199455 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SpriteGenerated::__Register()
extern "C"  void UnityEngine_SpriteGenerated___Register_m2093696670 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2[] UnityEngine_SpriteGenerated::<Sprite_OverrideGeometry__Vector2_Array__UInt16_Array>m__2D6()
extern "C"  Vector2U5BU5D_t4024180168* UnityEngine_SpriteGenerated_U3CSprite_OverrideGeometry__Vector2_Array__UInt16_ArrayU3Em__2D6_m4034331473 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt16[] UnityEngine_SpriteGenerated::<Sprite_OverrideGeometry__Vector2_Array__UInt16_Array>m__2D7()
extern "C"  UInt16U5BU5D_t801649474* UnityEngine_SpriteGenerated_U3CSprite_OverrideGeometry__Vector2_Array__UInt16_ArrayU3Em__2D7_m1129011608 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_SpriteGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UnityEngine_SpriteGenerated_ilo_getObject1_m3379888909 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SpriteGenerated::ilo_addJSCSRel2(System.Int32,System.Object)
extern "C"  void UnityEngine_SpriteGenerated_ilo_addJSCSRel2_m4248974982 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_SpriteGenerated::ilo_setObject3(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UnityEngine_SpriteGenerated_ilo_setObject3_m3063904007 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SpriteGenerated::ilo_setVector2S4(System.Int32,UnityEngine.Vector2)
extern "C"  void UnityEngine_SpriteGenerated_ilo_setVector2S4_m3443593857 (Il2CppObject * __this /* static, unused */, int32_t ___e0, Vector2_t4282066565  ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SpriteGenerated::ilo_setArrayS5(System.Int32,System.Int32,System.Boolean)
extern "C"  void UnityEngine_SpriteGenerated_ilo_setArrayS5_m1682386782 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___count1, bool ___bClear2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SpriteGenerated::ilo_setUInt166(System.Int32,System.UInt16)
extern "C"  void UnityEngine_SpriteGenerated_ilo_setUInt166_m2741223330 (Il2CppObject * __this /* static, unused */, int32_t ___e0, uint16_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 UnityEngine_SpriteGenerated::ilo_getUInt327(System.Int32)
extern "C"  uint32_t UnityEngine_SpriteGenerated_ilo_getUInt327_m2381662220 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine_SpriteGenerated::ilo_getSingle8(System.Int32)
extern "C"  float UnityEngine_SpriteGenerated_ilo_getSingle8_m1436034477 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_SpriteGenerated::ilo_getEnum9(System.Int32)
extern "C"  int32_t UnityEngine_SpriteGenerated_ilo_getEnum9_m1994231123 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine_SpriteGenerated::ilo_getVector2S10(System.Int32)
extern "C"  Vector2_t4282066565  UnityEngine_SpriteGenerated_ilo_getVector2S10_m1189752793 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_SpriteGenerated::ilo_getObject11(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_SpriteGenerated_ilo_getObject11_m2171074373 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_SpriteGenerated::ilo_getElement12(System.Int32,System.Int32)
extern "C"  int32_t UnityEngine_SpriteGenerated_ilo_getElement12_m3630210303 (Il2CppObject * __this /* static, unused */, int32_t ___id0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
