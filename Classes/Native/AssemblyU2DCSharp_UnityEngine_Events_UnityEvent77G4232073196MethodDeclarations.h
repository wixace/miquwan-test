﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_Events_UnityEvent77Generated
struct UnityEngine_Events_UnityEvent77Generated_t4232073196;
// JSVCall
struct JSVCall_t3708497963;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"

// System.Void UnityEngine_Events_UnityEvent77Generated::.ctor()
extern "C"  void UnityEngine_Events_UnityEvent77Generated__ctor_m666843183 (UnityEngine_Events_UnityEvent77Generated_t4232073196 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Events_UnityEvent77Generated::.cctor()
extern "C"  void UnityEngine_Events_UnityEvent77Generated__cctor_m3010173278 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Events_UnityEvent77Generated::UnityEventA1_AddListener__UnityActionT1_T0(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Events_UnityEvent77Generated_UnityEventA1_AddListener__UnityActionT1_T0_m3190839063 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Events_UnityEvent77Generated::UnityEventA1_Invoke__T0(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Events_UnityEvent77Generated_UnityEventA1_Invoke__T0_m1485193991 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Events_UnityEvent77Generated::UnityEventA1_RemoveListener__UnityActionT1_T0(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Events_UnityEvent77Generated_UnityEventA1_RemoveListener__UnityActionT1_T0_m3779618622 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Events_UnityEvent77Generated::__Register()
extern "C"  void UnityEngine_Events_UnityEvent77Generated___Register_m483985912 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
