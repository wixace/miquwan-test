﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_cejatal127
struct  M_cejatal127_t611124192  : public Il2CppObject
{
public:
	// System.Int32 GarbageiOS.M_cejatal127::_damawhor
	int32_t ____damawhor_0;
	// System.Single GarbageiOS.M_cejatal127::_xiswuPeapova
	float ____xiswuPeapova_1;
	// System.Int32 GarbageiOS.M_cejatal127::_jenearsis
	int32_t ____jenearsis_2;
	// System.Single GarbageiOS.M_cejatal127::_cagewa
	float ____cagewa_3;
	// System.UInt32 GarbageiOS.M_cejatal127::_supeduJoube
	uint32_t ____supeduJoube_4;
	// System.UInt32 GarbageiOS.M_cejatal127::_rarlas
	uint32_t ____rarlas_5;
	// System.Int32 GarbageiOS.M_cejatal127::_vukirlouStejafow
	int32_t ____vukirlouStejafow_6;
	// System.Single GarbageiOS.M_cejatal127::_memaiderLairkor
	float ____memaiderLairkor_7;
	// System.UInt32 GarbageiOS.M_cejatal127::_harnou
	uint32_t ____harnou_8;
	// System.Boolean GarbageiOS.M_cejatal127::_mempearStirleejall
	bool ____mempearStirleejall_9;
	// System.Single GarbageiOS.M_cejatal127::_doryeHalsayga
	float ____doryeHalsayga_10;
	// System.UInt32 GarbageiOS.M_cejatal127::_jasgu
	uint32_t ____jasgu_11;
	// System.UInt32 GarbageiOS.M_cejatal127::_jowdayci
	uint32_t ____jowdayci_12;
	// System.Boolean GarbageiOS.M_cejatal127::_moheseMuxaje
	bool ____moheseMuxaje_13;
	// System.Int32 GarbageiOS.M_cejatal127::_semjelrooPetro
	int32_t ____semjelrooPetro_14;
	// System.Int32 GarbageiOS.M_cejatal127::_ferecajal
	int32_t ____ferecajal_15;
	// System.Single GarbageiOS.M_cejatal127::_nasgooCosorsee
	float ____nasgooCosorsee_16;
	// System.Boolean GarbageiOS.M_cejatal127::_sembe
	bool ____sembe_17;
	// System.UInt32 GarbageiOS.M_cejatal127::_bearmeSaljatee
	uint32_t ____bearmeSaljatee_18;
	// System.Int32 GarbageiOS.M_cejatal127::_didosairSimegas
	int32_t ____didosairSimegas_19;
	// System.String GarbageiOS.M_cejatal127::_qelou
	String_t* ____qelou_20;
	// System.UInt32 GarbageiOS.M_cejatal127::_parmasnai
	uint32_t ____parmasnai_21;
	// System.String GarbageiOS.M_cejatal127::_souruyoo
	String_t* ____souruyoo_22;
	// System.Single GarbageiOS.M_cejatal127::_ceja
	float ____ceja_23;
	// System.String GarbageiOS.M_cejatal127::_dalhouQarbowlee
	String_t* ____dalhouQarbowlee_24;
	// System.Int32 GarbageiOS.M_cejatal127::_tremxucur
	int32_t ____tremxucur_25;
	// System.UInt32 GarbageiOS.M_cejatal127::_seestear
	uint32_t ____seestear_26;
	// System.UInt32 GarbageiOS.M_cejatal127::_whaterdaCousem
	uint32_t ____whaterdaCousem_27;
	// System.String GarbageiOS.M_cejatal127::_porduZeremeljo
	String_t* ____porduZeremeljo_28;
	// System.UInt32 GarbageiOS.M_cejatal127::_bejooMaha
	uint32_t ____bejooMaha_29;
	// System.UInt32 GarbageiOS.M_cejatal127::_murjeBakaw
	uint32_t ____murjeBakaw_30;
	// System.UInt32 GarbageiOS.M_cejatal127::_sese
	uint32_t ____sese_31;
	// System.Boolean GarbageiOS.M_cejatal127::_stijaydrir
	bool ____stijaydrir_32;
	// System.Boolean GarbageiOS.M_cejatal127::_mipouheCati
	bool ____mipouheCati_33;
	// System.Int32 GarbageiOS.M_cejatal127::_leresapas
	int32_t ____leresapas_34;
	// System.Boolean GarbageiOS.M_cejatal127::_kaidre
	bool ____kaidre_35;
	// System.Boolean GarbageiOS.M_cejatal127::_wearsor
	bool ____wearsor_36;
	// System.Int32 GarbageiOS.M_cejatal127::_kalcesea
	int32_t ____kalcesea_37;
	// System.UInt32 GarbageiOS.M_cejatal127::_tatee
	uint32_t ____tatee_38;
	// System.String GarbageiOS.M_cejatal127::_soukear
	String_t* ____soukear_39;
	// System.Boolean GarbageiOS.M_cejatal127::_giseZalha
	bool ____giseZalha_40;
	// System.UInt32 GarbageiOS.M_cejatal127::_selchalWacohem
	uint32_t ____selchalWacohem_41;
	// System.Int32 GarbageiOS.M_cejatal127::_luterecearBoukime
	int32_t ____luterecearBoukime_42;
	// System.Single GarbageiOS.M_cejatal127::_bupouMolair
	float ____bupouMolair_43;
	// System.String GarbageiOS.M_cejatal127::_delpearMorhel
	String_t* ____delpearMorhel_44;
	// System.Int32 GarbageiOS.M_cejatal127::_raytipallTereder
	int32_t ____raytipallTereder_45;
	// System.String GarbageiOS.M_cejatal127::_pajoumeSalwirtair
	String_t* ____pajoumeSalwirtair_46;
	// System.UInt32 GarbageiOS.M_cejatal127::_kearwerfooPajorsa
	uint32_t ____kearwerfooPajorsa_47;
	// System.UInt32 GarbageiOS.M_cejatal127::_foroharMahi
	uint32_t ____foroharMahi_48;
	// System.Boolean GarbageiOS.M_cejatal127::_tredreBedepor
	bool ____tredreBedepor_49;
	// System.Boolean GarbageiOS.M_cejatal127::_qairdereqiNarjiwere
	bool ____qairdereqiNarjiwere_50;
	// System.Int32 GarbageiOS.M_cejatal127::_tawheBojomall
	int32_t ____tawheBojomall_51;
	// System.UInt32 GarbageiOS.M_cejatal127::_kesayki
	uint32_t ____kesayki_52;
	// System.Single GarbageiOS.M_cejatal127::_nallsumem
	float ____nallsumem_53;
	// System.String GarbageiOS.M_cejatal127::_jircaybeCaymo
	String_t* ____jircaybeCaymo_54;
	// System.Boolean GarbageiOS.M_cejatal127::_koupar
	bool ____koupar_55;
	// System.Boolean GarbageiOS.M_cejatal127::_kermoCini
	bool ____kermoCini_56;
	// System.Int32 GarbageiOS.M_cejatal127::_japallyoGaba
	int32_t ____japallyoGaba_57;
	// System.Single GarbageiOS.M_cejatal127::_talfeder
	float ____talfeder_58;
	// System.String GarbageiOS.M_cejatal127::_wirxahoPastaball
	String_t* ____wirxahoPastaball_59;
	// System.Int32 GarbageiOS.M_cejatal127::_neacear
	int32_t ____neacear_60;
	// System.Boolean GarbageiOS.M_cejatal127::_sijis
	bool ____sijis_61;
	// System.Boolean GarbageiOS.M_cejatal127::_marxaixePohe
	bool ____marxaixePohe_62;
	// System.Single GarbageiOS.M_cejatal127::_fearje
	float ____fearje_63;
	// System.Int32 GarbageiOS.M_cejatal127::_furkispallDrebalne
	int32_t ____furkispallDrebalne_64;
	// System.String GarbageiOS.M_cejatal127::_sallrawcaw
	String_t* ____sallrawcaw_65;
	// System.Single GarbageiOS.M_cejatal127::_hartaiseFowusti
	float ____hartaiseFowusti_66;
	// System.Single GarbageiOS.M_cejatal127::_lernear
	float ____lernear_67;
	// System.UInt32 GarbageiOS.M_cejatal127::_tayvalRerefalkai
	uint32_t ____tayvalRerefalkai_68;
	// System.String GarbageiOS.M_cejatal127::_cheardenerSeyeresi
	String_t* ____cheardenerSeyeresi_69;
	// System.Int32 GarbageiOS.M_cejatal127::_titou
	int32_t ____titou_70;
	// System.Int32 GarbageiOS.M_cejatal127::_melbalMowa
	int32_t ____melbalMowa_71;
	// System.UInt32 GarbageiOS.M_cejatal127::_drulepe
	uint32_t ____drulepe_72;
	// System.Boolean GarbageiOS.M_cejatal127::_deedaiTouhouse
	bool ____deedaiTouhouse_73;
	// System.Single GarbageiOS.M_cejatal127::_neekojel
	float ____neekojel_74;
	// System.Boolean GarbageiOS.M_cejatal127::_selpamair
	bool ____selpamair_75;
	// System.Boolean GarbageiOS.M_cejatal127::_richirsou
	bool ____richirsou_76;
	// System.Single GarbageiOS.M_cejatal127::_dimureNorcem
	float ____dimureNorcem_77;
	// System.Int32 GarbageiOS.M_cejatal127::_gallyay
	int32_t ____gallyay_78;

public:
	inline static int32_t get_offset_of__damawhor_0() { return static_cast<int32_t>(offsetof(M_cejatal127_t611124192, ____damawhor_0)); }
	inline int32_t get__damawhor_0() const { return ____damawhor_0; }
	inline int32_t* get_address_of__damawhor_0() { return &____damawhor_0; }
	inline void set__damawhor_0(int32_t value)
	{
		____damawhor_0 = value;
	}

	inline static int32_t get_offset_of__xiswuPeapova_1() { return static_cast<int32_t>(offsetof(M_cejatal127_t611124192, ____xiswuPeapova_1)); }
	inline float get__xiswuPeapova_1() const { return ____xiswuPeapova_1; }
	inline float* get_address_of__xiswuPeapova_1() { return &____xiswuPeapova_1; }
	inline void set__xiswuPeapova_1(float value)
	{
		____xiswuPeapova_1 = value;
	}

	inline static int32_t get_offset_of__jenearsis_2() { return static_cast<int32_t>(offsetof(M_cejatal127_t611124192, ____jenearsis_2)); }
	inline int32_t get__jenearsis_2() const { return ____jenearsis_2; }
	inline int32_t* get_address_of__jenearsis_2() { return &____jenearsis_2; }
	inline void set__jenearsis_2(int32_t value)
	{
		____jenearsis_2 = value;
	}

	inline static int32_t get_offset_of__cagewa_3() { return static_cast<int32_t>(offsetof(M_cejatal127_t611124192, ____cagewa_3)); }
	inline float get__cagewa_3() const { return ____cagewa_3; }
	inline float* get_address_of__cagewa_3() { return &____cagewa_3; }
	inline void set__cagewa_3(float value)
	{
		____cagewa_3 = value;
	}

	inline static int32_t get_offset_of__supeduJoube_4() { return static_cast<int32_t>(offsetof(M_cejatal127_t611124192, ____supeduJoube_4)); }
	inline uint32_t get__supeduJoube_4() const { return ____supeduJoube_4; }
	inline uint32_t* get_address_of__supeduJoube_4() { return &____supeduJoube_4; }
	inline void set__supeduJoube_4(uint32_t value)
	{
		____supeduJoube_4 = value;
	}

	inline static int32_t get_offset_of__rarlas_5() { return static_cast<int32_t>(offsetof(M_cejatal127_t611124192, ____rarlas_5)); }
	inline uint32_t get__rarlas_5() const { return ____rarlas_5; }
	inline uint32_t* get_address_of__rarlas_5() { return &____rarlas_5; }
	inline void set__rarlas_5(uint32_t value)
	{
		____rarlas_5 = value;
	}

	inline static int32_t get_offset_of__vukirlouStejafow_6() { return static_cast<int32_t>(offsetof(M_cejatal127_t611124192, ____vukirlouStejafow_6)); }
	inline int32_t get__vukirlouStejafow_6() const { return ____vukirlouStejafow_6; }
	inline int32_t* get_address_of__vukirlouStejafow_6() { return &____vukirlouStejafow_6; }
	inline void set__vukirlouStejafow_6(int32_t value)
	{
		____vukirlouStejafow_6 = value;
	}

	inline static int32_t get_offset_of__memaiderLairkor_7() { return static_cast<int32_t>(offsetof(M_cejatal127_t611124192, ____memaiderLairkor_7)); }
	inline float get__memaiderLairkor_7() const { return ____memaiderLairkor_7; }
	inline float* get_address_of__memaiderLairkor_7() { return &____memaiderLairkor_7; }
	inline void set__memaiderLairkor_7(float value)
	{
		____memaiderLairkor_7 = value;
	}

	inline static int32_t get_offset_of__harnou_8() { return static_cast<int32_t>(offsetof(M_cejatal127_t611124192, ____harnou_8)); }
	inline uint32_t get__harnou_8() const { return ____harnou_8; }
	inline uint32_t* get_address_of__harnou_8() { return &____harnou_8; }
	inline void set__harnou_8(uint32_t value)
	{
		____harnou_8 = value;
	}

	inline static int32_t get_offset_of__mempearStirleejall_9() { return static_cast<int32_t>(offsetof(M_cejatal127_t611124192, ____mempearStirleejall_9)); }
	inline bool get__mempearStirleejall_9() const { return ____mempearStirleejall_9; }
	inline bool* get_address_of__mempearStirleejall_9() { return &____mempearStirleejall_9; }
	inline void set__mempearStirleejall_9(bool value)
	{
		____mempearStirleejall_9 = value;
	}

	inline static int32_t get_offset_of__doryeHalsayga_10() { return static_cast<int32_t>(offsetof(M_cejatal127_t611124192, ____doryeHalsayga_10)); }
	inline float get__doryeHalsayga_10() const { return ____doryeHalsayga_10; }
	inline float* get_address_of__doryeHalsayga_10() { return &____doryeHalsayga_10; }
	inline void set__doryeHalsayga_10(float value)
	{
		____doryeHalsayga_10 = value;
	}

	inline static int32_t get_offset_of__jasgu_11() { return static_cast<int32_t>(offsetof(M_cejatal127_t611124192, ____jasgu_11)); }
	inline uint32_t get__jasgu_11() const { return ____jasgu_11; }
	inline uint32_t* get_address_of__jasgu_11() { return &____jasgu_11; }
	inline void set__jasgu_11(uint32_t value)
	{
		____jasgu_11 = value;
	}

	inline static int32_t get_offset_of__jowdayci_12() { return static_cast<int32_t>(offsetof(M_cejatal127_t611124192, ____jowdayci_12)); }
	inline uint32_t get__jowdayci_12() const { return ____jowdayci_12; }
	inline uint32_t* get_address_of__jowdayci_12() { return &____jowdayci_12; }
	inline void set__jowdayci_12(uint32_t value)
	{
		____jowdayci_12 = value;
	}

	inline static int32_t get_offset_of__moheseMuxaje_13() { return static_cast<int32_t>(offsetof(M_cejatal127_t611124192, ____moheseMuxaje_13)); }
	inline bool get__moheseMuxaje_13() const { return ____moheseMuxaje_13; }
	inline bool* get_address_of__moheseMuxaje_13() { return &____moheseMuxaje_13; }
	inline void set__moheseMuxaje_13(bool value)
	{
		____moheseMuxaje_13 = value;
	}

	inline static int32_t get_offset_of__semjelrooPetro_14() { return static_cast<int32_t>(offsetof(M_cejatal127_t611124192, ____semjelrooPetro_14)); }
	inline int32_t get__semjelrooPetro_14() const { return ____semjelrooPetro_14; }
	inline int32_t* get_address_of__semjelrooPetro_14() { return &____semjelrooPetro_14; }
	inline void set__semjelrooPetro_14(int32_t value)
	{
		____semjelrooPetro_14 = value;
	}

	inline static int32_t get_offset_of__ferecajal_15() { return static_cast<int32_t>(offsetof(M_cejatal127_t611124192, ____ferecajal_15)); }
	inline int32_t get__ferecajal_15() const { return ____ferecajal_15; }
	inline int32_t* get_address_of__ferecajal_15() { return &____ferecajal_15; }
	inline void set__ferecajal_15(int32_t value)
	{
		____ferecajal_15 = value;
	}

	inline static int32_t get_offset_of__nasgooCosorsee_16() { return static_cast<int32_t>(offsetof(M_cejatal127_t611124192, ____nasgooCosorsee_16)); }
	inline float get__nasgooCosorsee_16() const { return ____nasgooCosorsee_16; }
	inline float* get_address_of__nasgooCosorsee_16() { return &____nasgooCosorsee_16; }
	inline void set__nasgooCosorsee_16(float value)
	{
		____nasgooCosorsee_16 = value;
	}

	inline static int32_t get_offset_of__sembe_17() { return static_cast<int32_t>(offsetof(M_cejatal127_t611124192, ____sembe_17)); }
	inline bool get__sembe_17() const { return ____sembe_17; }
	inline bool* get_address_of__sembe_17() { return &____sembe_17; }
	inline void set__sembe_17(bool value)
	{
		____sembe_17 = value;
	}

	inline static int32_t get_offset_of__bearmeSaljatee_18() { return static_cast<int32_t>(offsetof(M_cejatal127_t611124192, ____bearmeSaljatee_18)); }
	inline uint32_t get__bearmeSaljatee_18() const { return ____bearmeSaljatee_18; }
	inline uint32_t* get_address_of__bearmeSaljatee_18() { return &____bearmeSaljatee_18; }
	inline void set__bearmeSaljatee_18(uint32_t value)
	{
		____bearmeSaljatee_18 = value;
	}

	inline static int32_t get_offset_of__didosairSimegas_19() { return static_cast<int32_t>(offsetof(M_cejatal127_t611124192, ____didosairSimegas_19)); }
	inline int32_t get__didosairSimegas_19() const { return ____didosairSimegas_19; }
	inline int32_t* get_address_of__didosairSimegas_19() { return &____didosairSimegas_19; }
	inline void set__didosairSimegas_19(int32_t value)
	{
		____didosairSimegas_19 = value;
	}

	inline static int32_t get_offset_of__qelou_20() { return static_cast<int32_t>(offsetof(M_cejatal127_t611124192, ____qelou_20)); }
	inline String_t* get__qelou_20() const { return ____qelou_20; }
	inline String_t** get_address_of__qelou_20() { return &____qelou_20; }
	inline void set__qelou_20(String_t* value)
	{
		____qelou_20 = value;
		Il2CppCodeGenWriteBarrier(&____qelou_20, value);
	}

	inline static int32_t get_offset_of__parmasnai_21() { return static_cast<int32_t>(offsetof(M_cejatal127_t611124192, ____parmasnai_21)); }
	inline uint32_t get__parmasnai_21() const { return ____parmasnai_21; }
	inline uint32_t* get_address_of__parmasnai_21() { return &____parmasnai_21; }
	inline void set__parmasnai_21(uint32_t value)
	{
		____parmasnai_21 = value;
	}

	inline static int32_t get_offset_of__souruyoo_22() { return static_cast<int32_t>(offsetof(M_cejatal127_t611124192, ____souruyoo_22)); }
	inline String_t* get__souruyoo_22() const { return ____souruyoo_22; }
	inline String_t** get_address_of__souruyoo_22() { return &____souruyoo_22; }
	inline void set__souruyoo_22(String_t* value)
	{
		____souruyoo_22 = value;
		Il2CppCodeGenWriteBarrier(&____souruyoo_22, value);
	}

	inline static int32_t get_offset_of__ceja_23() { return static_cast<int32_t>(offsetof(M_cejatal127_t611124192, ____ceja_23)); }
	inline float get__ceja_23() const { return ____ceja_23; }
	inline float* get_address_of__ceja_23() { return &____ceja_23; }
	inline void set__ceja_23(float value)
	{
		____ceja_23 = value;
	}

	inline static int32_t get_offset_of__dalhouQarbowlee_24() { return static_cast<int32_t>(offsetof(M_cejatal127_t611124192, ____dalhouQarbowlee_24)); }
	inline String_t* get__dalhouQarbowlee_24() const { return ____dalhouQarbowlee_24; }
	inline String_t** get_address_of__dalhouQarbowlee_24() { return &____dalhouQarbowlee_24; }
	inline void set__dalhouQarbowlee_24(String_t* value)
	{
		____dalhouQarbowlee_24 = value;
		Il2CppCodeGenWriteBarrier(&____dalhouQarbowlee_24, value);
	}

	inline static int32_t get_offset_of__tremxucur_25() { return static_cast<int32_t>(offsetof(M_cejatal127_t611124192, ____tremxucur_25)); }
	inline int32_t get__tremxucur_25() const { return ____tremxucur_25; }
	inline int32_t* get_address_of__tremxucur_25() { return &____tremxucur_25; }
	inline void set__tremxucur_25(int32_t value)
	{
		____tremxucur_25 = value;
	}

	inline static int32_t get_offset_of__seestear_26() { return static_cast<int32_t>(offsetof(M_cejatal127_t611124192, ____seestear_26)); }
	inline uint32_t get__seestear_26() const { return ____seestear_26; }
	inline uint32_t* get_address_of__seestear_26() { return &____seestear_26; }
	inline void set__seestear_26(uint32_t value)
	{
		____seestear_26 = value;
	}

	inline static int32_t get_offset_of__whaterdaCousem_27() { return static_cast<int32_t>(offsetof(M_cejatal127_t611124192, ____whaterdaCousem_27)); }
	inline uint32_t get__whaterdaCousem_27() const { return ____whaterdaCousem_27; }
	inline uint32_t* get_address_of__whaterdaCousem_27() { return &____whaterdaCousem_27; }
	inline void set__whaterdaCousem_27(uint32_t value)
	{
		____whaterdaCousem_27 = value;
	}

	inline static int32_t get_offset_of__porduZeremeljo_28() { return static_cast<int32_t>(offsetof(M_cejatal127_t611124192, ____porduZeremeljo_28)); }
	inline String_t* get__porduZeremeljo_28() const { return ____porduZeremeljo_28; }
	inline String_t** get_address_of__porduZeremeljo_28() { return &____porduZeremeljo_28; }
	inline void set__porduZeremeljo_28(String_t* value)
	{
		____porduZeremeljo_28 = value;
		Il2CppCodeGenWriteBarrier(&____porduZeremeljo_28, value);
	}

	inline static int32_t get_offset_of__bejooMaha_29() { return static_cast<int32_t>(offsetof(M_cejatal127_t611124192, ____bejooMaha_29)); }
	inline uint32_t get__bejooMaha_29() const { return ____bejooMaha_29; }
	inline uint32_t* get_address_of__bejooMaha_29() { return &____bejooMaha_29; }
	inline void set__bejooMaha_29(uint32_t value)
	{
		____bejooMaha_29 = value;
	}

	inline static int32_t get_offset_of__murjeBakaw_30() { return static_cast<int32_t>(offsetof(M_cejatal127_t611124192, ____murjeBakaw_30)); }
	inline uint32_t get__murjeBakaw_30() const { return ____murjeBakaw_30; }
	inline uint32_t* get_address_of__murjeBakaw_30() { return &____murjeBakaw_30; }
	inline void set__murjeBakaw_30(uint32_t value)
	{
		____murjeBakaw_30 = value;
	}

	inline static int32_t get_offset_of__sese_31() { return static_cast<int32_t>(offsetof(M_cejatal127_t611124192, ____sese_31)); }
	inline uint32_t get__sese_31() const { return ____sese_31; }
	inline uint32_t* get_address_of__sese_31() { return &____sese_31; }
	inline void set__sese_31(uint32_t value)
	{
		____sese_31 = value;
	}

	inline static int32_t get_offset_of__stijaydrir_32() { return static_cast<int32_t>(offsetof(M_cejatal127_t611124192, ____stijaydrir_32)); }
	inline bool get__stijaydrir_32() const { return ____stijaydrir_32; }
	inline bool* get_address_of__stijaydrir_32() { return &____stijaydrir_32; }
	inline void set__stijaydrir_32(bool value)
	{
		____stijaydrir_32 = value;
	}

	inline static int32_t get_offset_of__mipouheCati_33() { return static_cast<int32_t>(offsetof(M_cejatal127_t611124192, ____mipouheCati_33)); }
	inline bool get__mipouheCati_33() const { return ____mipouheCati_33; }
	inline bool* get_address_of__mipouheCati_33() { return &____mipouheCati_33; }
	inline void set__mipouheCati_33(bool value)
	{
		____mipouheCati_33 = value;
	}

	inline static int32_t get_offset_of__leresapas_34() { return static_cast<int32_t>(offsetof(M_cejatal127_t611124192, ____leresapas_34)); }
	inline int32_t get__leresapas_34() const { return ____leresapas_34; }
	inline int32_t* get_address_of__leresapas_34() { return &____leresapas_34; }
	inline void set__leresapas_34(int32_t value)
	{
		____leresapas_34 = value;
	}

	inline static int32_t get_offset_of__kaidre_35() { return static_cast<int32_t>(offsetof(M_cejatal127_t611124192, ____kaidre_35)); }
	inline bool get__kaidre_35() const { return ____kaidre_35; }
	inline bool* get_address_of__kaidre_35() { return &____kaidre_35; }
	inline void set__kaidre_35(bool value)
	{
		____kaidre_35 = value;
	}

	inline static int32_t get_offset_of__wearsor_36() { return static_cast<int32_t>(offsetof(M_cejatal127_t611124192, ____wearsor_36)); }
	inline bool get__wearsor_36() const { return ____wearsor_36; }
	inline bool* get_address_of__wearsor_36() { return &____wearsor_36; }
	inline void set__wearsor_36(bool value)
	{
		____wearsor_36 = value;
	}

	inline static int32_t get_offset_of__kalcesea_37() { return static_cast<int32_t>(offsetof(M_cejatal127_t611124192, ____kalcesea_37)); }
	inline int32_t get__kalcesea_37() const { return ____kalcesea_37; }
	inline int32_t* get_address_of__kalcesea_37() { return &____kalcesea_37; }
	inline void set__kalcesea_37(int32_t value)
	{
		____kalcesea_37 = value;
	}

	inline static int32_t get_offset_of__tatee_38() { return static_cast<int32_t>(offsetof(M_cejatal127_t611124192, ____tatee_38)); }
	inline uint32_t get__tatee_38() const { return ____tatee_38; }
	inline uint32_t* get_address_of__tatee_38() { return &____tatee_38; }
	inline void set__tatee_38(uint32_t value)
	{
		____tatee_38 = value;
	}

	inline static int32_t get_offset_of__soukear_39() { return static_cast<int32_t>(offsetof(M_cejatal127_t611124192, ____soukear_39)); }
	inline String_t* get__soukear_39() const { return ____soukear_39; }
	inline String_t** get_address_of__soukear_39() { return &____soukear_39; }
	inline void set__soukear_39(String_t* value)
	{
		____soukear_39 = value;
		Il2CppCodeGenWriteBarrier(&____soukear_39, value);
	}

	inline static int32_t get_offset_of__giseZalha_40() { return static_cast<int32_t>(offsetof(M_cejatal127_t611124192, ____giseZalha_40)); }
	inline bool get__giseZalha_40() const { return ____giseZalha_40; }
	inline bool* get_address_of__giseZalha_40() { return &____giseZalha_40; }
	inline void set__giseZalha_40(bool value)
	{
		____giseZalha_40 = value;
	}

	inline static int32_t get_offset_of__selchalWacohem_41() { return static_cast<int32_t>(offsetof(M_cejatal127_t611124192, ____selchalWacohem_41)); }
	inline uint32_t get__selchalWacohem_41() const { return ____selchalWacohem_41; }
	inline uint32_t* get_address_of__selchalWacohem_41() { return &____selchalWacohem_41; }
	inline void set__selchalWacohem_41(uint32_t value)
	{
		____selchalWacohem_41 = value;
	}

	inline static int32_t get_offset_of__luterecearBoukime_42() { return static_cast<int32_t>(offsetof(M_cejatal127_t611124192, ____luterecearBoukime_42)); }
	inline int32_t get__luterecearBoukime_42() const { return ____luterecearBoukime_42; }
	inline int32_t* get_address_of__luterecearBoukime_42() { return &____luterecearBoukime_42; }
	inline void set__luterecearBoukime_42(int32_t value)
	{
		____luterecearBoukime_42 = value;
	}

	inline static int32_t get_offset_of__bupouMolair_43() { return static_cast<int32_t>(offsetof(M_cejatal127_t611124192, ____bupouMolair_43)); }
	inline float get__bupouMolair_43() const { return ____bupouMolair_43; }
	inline float* get_address_of__bupouMolair_43() { return &____bupouMolair_43; }
	inline void set__bupouMolair_43(float value)
	{
		____bupouMolair_43 = value;
	}

	inline static int32_t get_offset_of__delpearMorhel_44() { return static_cast<int32_t>(offsetof(M_cejatal127_t611124192, ____delpearMorhel_44)); }
	inline String_t* get__delpearMorhel_44() const { return ____delpearMorhel_44; }
	inline String_t** get_address_of__delpearMorhel_44() { return &____delpearMorhel_44; }
	inline void set__delpearMorhel_44(String_t* value)
	{
		____delpearMorhel_44 = value;
		Il2CppCodeGenWriteBarrier(&____delpearMorhel_44, value);
	}

	inline static int32_t get_offset_of__raytipallTereder_45() { return static_cast<int32_t>(offsetof(M_cejatal127_t611124192, ____raytipallTereder_45)); }
	inline int32_t get__raytipallTereder_45() const { return ____raytipallTereder_45; }
	inline int32_t* get_address_of__raytipallTereder_45() { return &____raytipallTereder_45; }
	inline void set__raytipallTereder_45(int32_t value)
	{
		____raytipallTereder_45 = value;
	}

	inline static int32_t get_offset_of__pajoumeSalwirtair_46() { return static_cast<int32_t>(offsetof(M_cejatal127_t611124192, ____pajoumeSalwirtair_46)); }
	inline String_t* get__pajoumeSalwirtair_46() const { return ____pajoumeSalwirtair_46; }
	inline String_t** get_address_of__pajoumeSalwirtair_46() { return &____pajoumeSalwirtair_46; }
	inline void set__pajoumeSalwirtair_46(String_t* value)
	{
		____pajoumeSalwirtair_46 = value;
		Il2CppCodeGenWriteBarrier(&____pajoumeSalwirtair_46, value);
	}

	inline static int32_t get_offset_of__kearwerfooPajorsa_47() { return static_cast<int32_t>(offsetof(M_cejatal127_t611124192, ____kearwerfooPajorsa_47)); }
	inline uint32_t get__kearwerfooPajorsa_47() const { return ____kearwerfooPajorsa_47; }
	inline uint32_t* get_address_of__kearwerfooPajorsa_47() { return &____kearwerfooPajorsa_47; }
	inline void set__kearwerfooPajorsa_47(uint32_t value)
	{
		____kearwerfooPajorsa_47 = value;
	}

	inline static int32_t get_offset_of__foroharMahi_48() { return static_cast<int32_t>(offsetof(M_cejatal127_t611124192, ____foroharMahi_48)); }
	inline uint32_t get__foroharMahi_48() const { return ____foroharMahi_48; }
	inline uint32_t* get_address_of__foroharMahi_48() { return &____foroharMahi_48; }
	inline void set__foroharMahi_48(uint32_t value)
	{
		____foroharMahi_48 = value;
	}

	inline static int32_t get_offset_of__tredreBedepor_49() { return static_cast<int32_t>(offsetof(M_cejatal127_t611124192, ____tredreBedepor_49)); }
	inline bool get__tredreBedepor_49() const { return ____tredreBedepor_49; }
	inline bool* get_address_of__tredreBedepor_49() { return &____tredreBedepor_49; }
	inline void set__tredreBedepor_49(bool value)
	{
		____tredreBedepor_49 = value;
	}

	inline static int32_t get_offset_of__qairdereqiNarjiwere_50() { return static_cast<int32_t>(offsetof(M_cejatal127_t611124192, ____qairdereqiNarjiwere_50)); }
	inline bool get__qairdereqiNarjiwere_50() const { return ____qairdereqiNarjiwere_50; }
	inline bool* get_address_of__qairdereqiNarjiwere_50() { return &____qairdereqiNarjiwere_50; }
	inline void set__qairdereqiNarjiwere_50(bool value)
	{
		____qairdereqiNarjiwere_50 = value;
	}

	inline static int32_t get_offset_of__tawheBojomall_51() { return static_cast<int32_t>(offsetof(M_cejatal127_t611124192, ____tawheBojomall_51)); }
	inline int32_t get__tawheBojomall_51() const { return ____tawheBojomall_51; }
	inline int32_t* get_address_of__tawheBojomall_51() { return &____tawheBojomall_51; }
	inline void set__tawheBojomall_51(int32_t value)
	{
		____tawheBojomall_51 = value;
	}

	inline static int32_t get_offset_of__kesayki_52() { return static_cast<int32_t>(offsetof(M_cejatal127_t611124192, ____kesayki_52)); }
	inline uint32_t get__kesayki_52() const { return ____kesayki_52; }
	inline uint32_t* get_address_of__kesayki_52() { return &____kesayki_52; }
	inline void set__kesayki_52(uint32_t value)
	{
		____kesayki_52 = value;
	}

	inline static int32_t get_offset_of__nallsumem_53() { return static_cast<int32_t>(offsetof(M_cejatal127_t611124192, ____nallsumem_53)); }
	inline float get__nallsumem_53() const { return ____nallsumem_53; }
	inline float* get_address_of__nallsumem_53() { return &____nallsumem_53; }
	inline void set__nallsumem_53(float value)
	{
		____nallsumem_53 = value;
	}

	inline static int32_t get_offset_of__jircaybeCaymo_54() { return static_cast<int32_t>(offsetof(M_cejatal127_t611124192, ____jircaybeCaymo_54)); }
	inline String_t* get__jircaybeCaymo_54() const { return ____jircaybeCaymo_54; }
	inline String_t** get_address_of__jircaybeCaymo_54() { return &____jircaybeCaymo_54; }
	inline void set__jircaybeCaymo_54(String_t* value)
	{
		____jircaybeCaymo_54 = value;
		Il2CppCodeGenWriteBarrier(&____jircaybeCaymo_54, value);
	}

	inline static int32_t get_offset_of__koupar_55() { return static_cast<int32_t>(offsetof(M_cejatal127_t611124192, ____koupar_55)); }
	inline bool get__koupar_55() const { return ____koupar_55; }
	inline bool* get_address_of__koupar_55() { return &____koupar_55; }
	inline void set__koupar_55(bool value)
	{
		____koupar_55 = value;
	}

	inline static int32_t get_offset_of__kermoCini_56() { return static_cast<int32_t>(offsetof(M_cejatal127_t611124192, ____kermoCini_56)); }
	inline bool get__kermoCini_56() const { return ____kermoCini_56; }
	inline bool* get_address_of__kermoCini_56() { return &____kermoCini_56; }
	inline void set__kermoCini_56(bool value)
	{
		____kermoCini_56 = value;
	}

	inline static int32_t get_offset_of__japallyoGaba_57() { return static_cast<int32_t>(offsetof(M_cejatal127_t611124192, ____japallyoGaba_57)); }
	inline int32_t get__japallyoGaba_57() const { return ____japallyoGaba_57; }
	inline int32_t* get_address_of__japallyoGaba_57() { return &____japallyoGaba_57; }
	inline void set__japallyoGaba_57(int32_t value)
	{
		____japallyoGaba_57 = value;
	}

	inline static int32_t get_offset_of__talfeder_58() { return static_cast<int32_t>(offsetof(M_cejatal127_t611124192, ____talfeder_58)); }
	inline float get__talfeder_58() const { return ____talfeder_58; }
	inline float* get_address_of__talfeder_58() { return &____talfeder_58; }
	inline void set__talfeder_58(float value)
	{
		____talfeder_58 = value;
	}

	inline static int32_t get_offset_of__wirxahoPastaball_59() { return static_cast<int32_t>(offsetof(M_cejatal127_t611124192, ____wirxahoPastaball_59)); }
	inline String_t* get__wirxahoPastaball_59() const { return ____wirxahoPastaball_59; }
	inline String_t** get_address_of__wirxahoPastaball_59() { return &____wirxahoPastaball_59; }
	inline void set__wirxahoPastaball_59(String_t* value)
	{
		____wirxahoPastaball_59 = value;
		Il2CppCodeGenWriteBarrier(&____wirxahoPastaball_59, value);
	}

	inline static int32_t get_offset_of__neacear_60() { return static_cast<int32_t>(offsetof(M_cejatal127_t611124192, ____neacear_60)); }
	inline int32_t get__neacear_60() const { return ____neacear_60; }
	inline int32_t* get_address_of__neacear_60() { return &____neacear_60; }
	inline void set__neacear_60(int32_t value)
	{
		____neacear_60 = value;
	}

	inline static int32_t get_offset_of__sijis_61() { return static_cast<int32_t>(offsetof(M_cejatal127_t611124192, ____sijis_61)); }
	inline bool get__sijis_61() const { return ____sijis_61; }
	inline bool* get_address_of__sijis_61() { return &____sijis_61; }
	inline void set__sijis_61(bool value)
	{
		____sijis_61 = value;
	}

	inline static int32_t get_offset_of__marxaixePohe_62() { return static_cast<int32_t>(offsetof(M_cejatal127_t611124192, ____marxaixePohe_62)); }
	inline bool get__marxaixePohe_62() const { return ____marxaixePohe_62; }
	inline bool* get_address_of__marxaixePohe_62() { return &____marxaixePohe_62; }
	inline void set__marxaixePohe_62(bool value)
	{
		____marxaixePohe_62 = value;
	}

	inline static int32_t get_offset_of__fearje_63() { return static_cast<int32_t>(offsetof(M_cejatal127_t611124192, ____fearje_63)); }
	inline float get__fearje_63() const { return ____fearje_63; }
	inline float* get_address_of__fearje_63() { return &____fearje_63; }
	inline void set__fearje_63(float value)
	{
		____fearje_63 = value;
	}

	inline static int32_t get_offset_of__furkispallDrebalne_64() { return static_cast<int32_t>(offsetof(M_cejatal127_t611124192, ____furkispallDrebalne_64)); }
	inline int32_t get__furkispallDrebalne_64() const { return ____furkispallDrebalne_64; }
	inline int32_t* get_address_of__furkispallDrebalne_64() { return &____furkispallDrebalne_64; }
	inline void set__furkispallDrebalne_64(int32_t value)
	{
		____furkispallDrebalne_64 = value;
	}

	inline static int32_t get_offset_of__sallrawcaw_65() { return static_cast<int32_t>(offsetof(M_cejatal127_t611124192, ____sallrawcaw_65)); }
	inline String_t* get__sallrawcaw_65() const { return ____sallrawcaw_65; }
	inline String_t** get_address_of__sallrawcaw_65() { return &____sallrawcaw_65; }
	inline void set__sallrawcaw_65(String_t* value)
	{
		____sallrawcaw_65 = value;
		Il2CppCodeGenWriteBarrier(&____sallrawcaw_65, value);
	}

	inline static int32_t get_offset_of__hartaiseFowusti_66() { return static_cast<int32_t>(offsetof(M_cejatal127_t611124192, ____hartaiseFowusti_66)); }
	inline float get__hartaiseFowusti_66() const { return ____hartaiseFowusti_66; }
	inline float* get_address_of__hartaiseFowusti_66() { return &____hartaiseFowusti_66; }
	inline void set__hartaiseFowusti_66(float value)
	{
		____hartaiseFowusti_66 = value;
	}

	inline static int32_t get_offset_of__lernear_67() { return static_cast<int32_t>(offsetof(M_cejatal127_t611124192, ____lernear_67)); }
	inline float get__lernear_67() const { return ____lernear_67; }
	inline float* get_address_of__lernear_67() { return &____lernear_67; }
	inline void set__lernear_67(float value)
	{
		____lernear_67 = value;
	}

	inline static int32_t get_offset_of__tayvalRerefalkai_68() { return static_cast<int32_t>(offsetof(M_cejatal127_t611124192, ____tayvalRerefalkai_68)); }
	inline uint32_t get__tayvalRerefalkai_68() const { return ____tayvalRerefalkai_68; }
	inline uint32_t* get_address_of__tayvalRerefalkai_68() { return &____tayvalRerefalkai_68; }
	inline void set__tayvalRerefalkai_68(uint32_t value)
	{
		____tayvalRerefalkai_68 = value;
	}

	inline static int32_t get_offset_of__cheardenerSeyeresi_69() { return static_cast<int32_t>(offsetof(M_cejatal127_t611124192, ____cheardenerSeyeresi_69)); }
	inline String_t* get__cheardenerSeyeresi_69() const { return ____cheardenerSeyeresi_69; }
	inline String_t** get_address_of__cheardenerSeyeresi_69() { return &____cheardenerSeyeresi_69; }
	inline void set__cheardenerSeyeresi_69(String_t* value)
	{
		____cheardenerSeyeresi_69 = value;
		Il2CppCodeGenWriteBarrier(&____cheardenerSeyeresi_69, value);
	}

	inline static int32_t get_offset_of__titou_70() { return static_cast<int32_t>(offsetof(M_cejatal127_t611124192, ____titou_70)); }
	inline int32_t get__titou_70() const { return ____titou_70; }
	inline int32_t* get_address_of__titou_70() { return &____titou_70; }
	inline void set__titou_70(int32_t value)
	{
		____titou_70 = value;
	}

	inline static int32_t get_offset_of__melbalMowa_71() { return static_cast<int32_t>(offsetof(M_cejatal127_t611124192, ____melbalMowa_71)); }
	inline int32_t get__melbalMowa_71() const { return ____melbalMowa_71; }
	inline int32_t* get_address_of__melbalMowa_71() { return &____melbalMowa_71; }
	inline void set__melbalMowa_71(int32_t value)
	{
		____melbalMowa_71 = value;
	}

	inline static int32_t get_offset_of__drulepe_72() { return static_cast<int32_t>(offsetof(M_cejatal127_t611124192, ____drulepe_72)); }
	inline uint32_t get__drulepe_72() const { return ____drulepe_72; }
	inline uint32_t* get_address_of__drulepe_72() { return &____drulepe_72; }
	inline void set__drulepe_72(uint32_t value)
	{
		____drulepe_72 = value;
	}

	inline static int32_t get_offset_of__deedaiTouhouse_73() { return static_cast<int32_t>(offsetof(M_cejatal127_t611124192, ____deedaiTouhouse_73)); }
	inline bool get__deedaiTouhouse_73() const { return ____deedaiTouhouse_73; }
	inline bool* get_address_of__deedaiTouhouse_73() { return &____deedaiTouhouse_73; }
	inline void set__deedaiTouhouse_73(bool value)
	{
		____deedaiTouhouse_73 = value;
	}

	inline static int32_t get_offset_of__neekojel_74() { return static_cast<int32_t>(offsetof(M_cejatal127_t611124192, ____neekojel_74)); }
	inline float get__neekojel_74() const { return ____neekojel_74; }
	inline float* get_address_of__neekojel_74() { return &____neekojel_74; }
	inline void set__neekojel_74(float value)
	{
		____neekojel_74 = value;
	}

	inline static int32_t get_offset_of__selpamair_75() { return static_cast<int32_t>(offsetof(M_cejatal127_t611124192, ____selpamair_75)); }
	inline bool get__selpamair_75() const { return ____selpamair_75; }
	inline bool* get_address_of__selpamair_75() { return &____selpamair_75; }
	inline void set__selpamair_75(bool value)
	{
		____selpamair_75 = value;
	}

	inline static int32_t get_offset_of__richirsou_76() { return static_cast<int32_t>(offsetof(M_cejatal127_t611124192, ____richirsou_76)); }
	inline bool get__richirsou_76() const { return ____richirsou_76; }
	inline bool* get_address_of__richirsou_76() { return &____richirsou_76; }
	inline void set__richirsou_76(bool value)
	{
		____richirsou_76 = value;
	}

	inline static int32_t get_offset_of__dimureNorcem_77() { return static_cast<int32_t>(offsetof(M_cejatal127_t611124192, ____dimureNorcem_77)); }
	inline float get__dimureNorcem_77() const { return ____dimureNorcem_77; }
	inline float* get_address_of__dimureNorcem_77() { return &____dimureNorcem_77; }
	inline void set__dimureNorcem_77(float value)
	{
		____dimureNorcem_77 = value;
	}

	inline static int32_t get_offset_of__gallyay_78() { return static_cast<int32_t>(offsetof(M_cejatal127_t611124192, ____gallyay_78)); }
	inline int32_t get__gallyay_78() const { return ____gallyay_78; }
	inline int32_t* get_address_of__gallyay_78() { return &____gallyay_78; }
	inline void set__gallyay_78(int32_t value)
	{
		____gallyay_78 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
