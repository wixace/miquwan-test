﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Mesh
struct Mesh_t4241756145;
// UnityEngine.CombineInstance
struct CombineInstance_t1399074090;
struct CombineInstance_t1399074090_marshaled_pinvoke;
struct CombineInstance_t1399074090_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_CombineInstance1399074090.h"
#include "UnityEngine_UnityEngine_Mesh4241756145.h"
#include "UnityEngine_UnityEngine_Matrix4x41651859333.h"

// UnityEngine.Mesh UnityEngine.CombineInstance::get_mesh()
extern "C"  Mesh_t4241756145 * CombineInstance_get_mesh_m3495034986 (CombineInstance_t1399074090 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.CombineInstance::set_mesh(UnityEngine.Mesh)
extern "C"  void CombineInstance_set_mesh_m991002533 (CombineInstance_t1399074090 * __this, Mesh_t4241756145 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.CombineInstance::get_subMeshIndex()
extern "C"  int32_t CombineInstance_get_subMeshIndex_m1187917421 (CombineInstance_t1399074090 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.CombineInstance::set_subMeshIndex(System.Int32)
extern "C"  void CombineInstance_set_subMeshIndex_m3542070322 (CombineInstance_t1399074090 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Matrix4x4 UnityEngine.CombineInstance::get_transform()
extern "C"  Matrix4x4_t1651859333  CombineInstance_get_transform_m1169333377 (CombineInstance_t1399074090 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.CombineInstance::set_transform(UnityEngine.Matrix4x4)
extern "C"  void CombineInstance_set_transform_m3266216072 (CombineInstance_t1399074090 * __this, Matrix4x4_t1651859333  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Mesh UnityEngine.CombineInstance::InternalGetMesh(System.Int32)
extern "C"  Mesh_t4241756145 * CombineInstance_InternalGetMesh_m545771653 (CombineInstance_t1399074090 * __this, int32_t ___instanceID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct CombineInstance_t1399074090;
struct CombineInstance_t1399074090_marshaled_pinvoke;

extern "C" void CombineInstance_t1399074090_marshal_pinvoke(const CombineInstance_t1399074090& unmarshaled, CombineInstance_t1399074090_marshaled_pinvoke& marshaled);
extern "C" void CombineInstance_t1399074090_marshal_pinvoke_back(const CombineInstance_t1399074090_marshaled_pinvoke& marshaled, CombineInstance_t1399074090& unmarshaled);
extern "C" void CombineInstance_t1399074090_marshal_pinvoke_cleanup(CombineInstance_t1399074090_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct CombineInstance_t1399074090;
struct CombineInstance_t1399074090_marshaled_com;

extern "C" void CombineInstance_t1399074090_marshal_com(const CombineInstance_t1399074090& unmarshaled, CombineInstance_t1399074090_marshaled_com& marshaled);
extern "C" void CombineInstance_t1399074090_marshal_com_back(const CombineInstance_t1399074090_marshaled_com& marshaled, CombineInstance_t1399074090& unmarshaled);
extern "C" void CombineInstance_t1399074090_marshal_com_cleanup(CombineInstance_t1399074090_marshaled_com& marshaled);
