﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._88e4b81d38d5cce2fb3fa43ca07c36e5
struct _88e4b81d38d5cce2fb3fa43ca07c36e5_t3595142612;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__88e4b81d38d5cce2fb3fa43c3595142612.h"

// System.Void Little._88e4b81d38d5cce2fb3fa43ca07c36e5::.ctor()
extern "C"  void _88e4b81d38d5cce2fb3fa43ca07c36e5__ctor_m1546710745 (_88e4b81d38d5cce2fb3fa43ca07c36e5_t3595142612 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._88e4b81d38d5cce2fb3fa43ca07c36e5::_88e4b81d38d5cce2fb3fa43ca07c36e5m2(System.Int32)
extern "C"  int32_t _88e4b81d38d5cce2fb3fa43ca07c36e5__88e4b81d38d5cce2fb3fa43ca07c36e5m2_m3363086617 (_88e4b81d38d5cce2fb3fa43ca07c36e5_t3595142612 * __this, int32_t ____88e4b81d38d5cce2fb3fa43ca07c36e5a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._88e4b81d38d5cce2fb3fa43ca07c36e5::_88e4b81d38d5cce2fb3fa43ca07c36e5m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _88e4b81d38d5cce2fb3fa43ca07c36e5__88e4b81d38d5cce2fb3fa43ca07c36e5m_m25720253 (_88e4b81d38d5cce2fb3fa43ca07c36e5_t3595142612 * __this, int32_t ____88e4b81d38d5cce2fb3fa43ca07c36e5a0, int32_t ____88e4b81d38d5cce2fb3fa43ca07c36e5741, int32_t ____88e4b81d38d5cce2fb3fa43ca07c36e5c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._88e4b81d38d5cce2fb3fa43ca07c36e5::ilo__88e4b81d38d5cce2fb3fa43ca07c36e5m21(Little._88e4b81d38d5cce2fb3fa43ca07c36e5,System.Int32)
extern "C"  int32_t _88e4b81d38d5cce2fb3fa43ca07c36e5_ilo__88e4b81d38d5cce2fb3fa43ca07c36e5m21_m3294519835 (Il2CppObject * __this /* static, unused */, _88e4b81d38d5cce2fb3fa43ca07c36e5_t3595142612 * ____this0, int32_t ____88e4b81d38d5cce2fb3fa43ca07c36e5a1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
