﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.FloodPath
struct FloodPath_t3766979749;
// OnPathDelegate
struct OnPathDelegate_t598607977;
// Pathfinding.GraphNode
struct GraphNode_t23612370;
// Pathfinding.PathHandler
struct PathHandler_t918952263;
// Pathfinding.Path
struct Path_t1974241691;
// Pathfinding.PathNode
struct PathNode_t417131581;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_OnPathDelegate598607977.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphNode23612370.h"
#include "AssemblyU2DCSharp_Pathfinding_PathHandler918952263.h"
#include "AssemblyU2DCSharp_Pathfinding_Path1974241691.h"
#include "AssemblyU2DCSharp_Pathfinding_PathNode417131581.h"
#include "AssemblyU2DCSharp_PathCompleteState1625108115.h"

// System.Void Pathfinding.FloodPath::.ctor(UnityEngine.Vector3,OnPathDelegate)
extern "C"  void FloodPath__ctor_m3796362038 (FloodPath_t3766979749 * __this, Vector3_t4282066566  ___start0, OnPathDelegate_t598607977 * ___callbackDelegate1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.FloodPath::.ctor()
extern "C"  void FloodPath__ctor_m1834176050 (FloodPath_t3766979749 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.FloodPath::get_FloodingPath()
extern "C"  bool FloodPath_get_FloodingPath_m1016553040 (FloodPath_t3766979749 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.FloodPath::HasPathTo(Pathfinding.GraphNode)
extern "C"  bool FloodPath_HasPathTo_m2400724462 (FloodPath_t3766979749 * __this, GraphNode_t23612370 * ___node0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.GraphNode Pathfinding.FloodPath::GetParent(Pathfinding.GraphNode)
extern "C"  GraphNode_t23612370 * FloodPath_GetParent_m2965150415 (FloodPath_t3766979749 * __this, GraphNode_t23612370 * ___node0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.FloodPath Pathfinding.FloodPath::Construct(UnityEngine.Vector3,OnPathDelegate)
extern "C"  FloodPath_t3766979749 * FloodPath_Construct_m2337802821 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___start0, OnPathDelegate_t598607977 * ___callback1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.FloodPath Pathfinding.FloodPath::Construct(Pathfinding.GraphNode,OnPathDelegate)
extern "C"  FloodPath_t3766979749 * FloodPath_Construct_m313022098 (Il2CppObject * __this /* static, unused */, GraphNode_t23612370 * ___start0, OnPathDelegate_t598607977 * ___callback1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.FloodPath::Setup(UnityEngine.Vector3,OnPathDelegate)
extern "C"  void FloodPath_Setup_m71958737 (FloodPath_t3766979749 * __this, Vector3_t4282066566  ___start0, OnPathDelegate_t598607977 * ___callback1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.FloodPath::Setup(Pathfinding.GraphNode,OnPathDelegate)
extern "C"  void FloodPath_Setup_m385276446 (FloodPath_t3766979749 * __this, GraphNode_t23612370 * ___start0, OnPathDelegate_t598607977 * ___callback1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.FloodPath::Reset()
extern "C"  void FloodPath_Reset_m3775576287 (FloodPath_t3766979749 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.FloodPath::Recycle()
extern "C"  void FloodPath_Recycle_m2620224867 (FloodPath_t3766979749 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.FloodPath::Prepare()
extern "C"  void FloodPath_Prepare_m342573783 (FloodPath_t3766979749 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.FloodPath::Initialize()
extern "C"  void FloodPath_Initialize_m2454460322 (FloodPath_t3766979749 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.FloodPath::CalculateStep(System.Int64)
extern "C"  void FloodPath_CalculateStep_m4110278900 (FloodPath_t3766979749 * __this, int64_t ___targetTick0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt16 Pathfinding.FloodPath::ilo_get_PathID1(Pathfinding.PathHandler)
extern "C"  uint16_t FloodPath_ilo_get_PathID1_m2520340733 (Il2CppObject * __this /* static, unused */, PathHandler_t918952263 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.FloodPath::ilo_Open2(Pathfinding.GraphNode,Pathfinding.Path,Pathfinding.PathNode,Pathfinding.PathHandler)
extern "C"  void FloodPath_ilo_Open2_m1408697974 (Il2CppObject * __this /* static, unused */, GraphNode_t23612370 * ____this0, Path_t1974241691 * ___path1, PathNode_t417131581 * ___pathNode2, PathHandler_t918952263 * ___handler3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.PathNode Pathfinding.FloodPath::ilo_PopNode3(Pathfinding.PathHandler)
extern "C"  PathNode_t417131581 * FloodPath_ilo_PopNode3_m2016499134 (Il2CppObject * __this /* static, unused */, PathHandler_t918952263 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PathCompleteState Pathfinding.FloodPath::ilo_get_CompleteState4(Pathfinding.Path)
extern "C"  int32_t FloodPath_ilo_get_CompleteState4_m1746345179 (Il2CppObject * __this /* static, unused */, Path_t1974241691 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
