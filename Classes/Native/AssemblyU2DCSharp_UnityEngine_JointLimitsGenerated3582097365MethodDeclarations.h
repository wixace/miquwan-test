﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_JointLimitsGenerated
struct UnityEngine_JointLimitsGenerated_t3582097365;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void UnityEngine_JointLimitsGenerated::.ctor()
extern "C"  void UnityEngine_JointLimitsGenerated__ctor_m3702816358 (UnityEngine_JointLimitsGenerated_t3582097365 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_JointLimitsGenerated::.cctor()
extern "C"  void UnityEngine_JointLimitsGenerated__cctor_m2636061191 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_JointLimitsGenerated::JointLimits_JointLimits1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_JointLimitsGenerated_JointLimits_JointLimits1_m2961218062 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_JointLimitsGenerated::JointLimits_min(JSVCall)
extern "C"  void UnityEngine_JointLimitsGenerated_JointLimits_min_m2750435118 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_JointLimitsGenerated::JointLimits_max(JSVCall)
extern "C"  void UnityEngine_JointLimitsGenerated_JointLimits_max_m2276009052 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_JointLimitsGenerated::JointLimits_bounciness(JSVCall)
extern "C"  void UnityEngine_JointLimitsGenerated_JointLimits_bounciness_m1401999625 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_JointLimitsGenerated::JointLimits_bounceMinVelocity(JSVCall)
extern "C"  void UnityEngine_JointLimitsGenerated_JointLimits_bounceMinVelocity_m223163897 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_JointLimitsGenerated::JointLimits_contactDistance(JSVCall)
extern "C"  void UnityEngine_JointLimitsGenerated_JointLimits_contactDistance_m484780459 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_JointLimitsGenerated::__Register()
extern "C"  void UnityEngine_JointLimitsGenerated___Register_m169479713 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_JointLimitsGenerated::ilo_addJSCSRel1(System.Int32,System.Object)
extern "C"  void UnityEngine_JointLimitsGenerated_ilo_addJSCSRel1_m1329289762 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_JointLimitsGenerated::ilo_setSingle2(System.Int32,System.Single)
extern "C"  void UnityEngine_JointLimitsGenerated_ilo_setSingle2_m531772623 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine_JointLimitsGenerated::ilo_getSingle3(System.Int32)
extern "C"  float UnityEngine_JointLimitsGenerated_ilo_getSingle3_m1225335555 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_JointLimitsGenerated::ilo_changeJSObj4(System.Int32,System.Object)
extern "C"  void UnityEngine_JointLimitsGenerated_ilo_changeJSObj4_m1070038086 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObjNew1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
