﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_bipuSaynere54
struct  M_bipuSaynere54_t1648617992  : public Il2CppObject
{
public:
	// System.Int32 GarbageiOS.M_bipuSaynere54::_yofaiXenar
	int32_t ____yofaiXenar_0;
	// System.UInt32 GarbageiOS.M_bipuSaynere54::_retarsairNahaw
	uint32_t ____retarsairNahaw_1;
	// System.String GarbageiOS.M_bipuSaynere54::_xuwhurdeKairfepu
	String_t* ____xuwhurdeKairfepu_2;
	// System.Int32 GarbageiOS.M_bipuSaynere54::_rarcereNasheji
	int32_t ____rarcereNasheji_3;
	// System.Single GarbageiOS.M_bipuSaynere54::_rousiVeweaker
	float ____rousiVeweaker_4;
	// System.Int32 GarbageiOS.M_bipuSaynere54::_ritaFurpir
	int32_t ____ritaFurpir_5;
	// System.Boolean GarbageiOS.M_bipuSaynere54::_gorse
	bool ____gorse_6;
	// System.Single GarbageiOS.M_bipuSaynere54::_qekascayRoowe
	float ____qekascayRoowe_7;
	// System.Single GarbageiOS.M_bipuSaynere54::_rircear
	float ____rircear_8;
	// System.UInt32 GarbageiOS.M_bipuSaynere54::_girsowKirmisbas
	uint32_t ____girsowKirmisbas_9;
	// System.Single GarbageiOS.M_bipuSaynere54::_haca
	float ____haca_10;
	// System.UInt32 GarbageiOS.M_bipuSaynere54::_jermairkoZebal
	uint32_t ____jermairkoZebal_11;
	// System.Boolean GarbageiOS.M_bipuSaynere54::_mesehairTainukoo
	bool ____mesehairTainukoo_12;
	// System.UInt32 GarbageiOS.M_bipuSaynere54::_likemtu
	uint32_t ____likemtu_13;
	// System.String GarbageiOS.M_bipuSaynere54::_herchea
	String_t* ____herchea_14;
	// System.Boolean GarbageiOS.M_bipuSaynere54::_jererouStowmupu
	bool ____jererouStowmupu_15;
	// System.Single GarbageiOS.M_bipuSaynere54::_hereferPimorra
	float ____hereferPimorra_16;
	// System.UInt32 GarbageiOS.M_bipuSaynere54::_misdall
	uint32_t ____misdall_17;
	// System.UInt32 GarbageiOS.M_bipuSaynere54::_warsur
	uint32_t ____warsur_18;
	// System.Boolean GarbageiOS.M_bipuSaynere54::_muryelbelDorro
	bool ____muryelbelDorro_19;
	// System.Single GarbageiOS.M_bipuSaynere54::_joutooSorcearkur
	float ____joutooSorcearkur_20;
	// System.Single GarbageiOS.M_bipuSaynere54::_xedecem
	float ____xedecem_21;
	// System.String GarbageiOS.M_bipuSaynere54::_xoukemda
	String_t* ____xoukemda_22;
	// System.Boolean GarbageiOS.M_bipuSaynere54::_selrayjoMiku
	bool ____selrayjoMiku_23;
	// System.UInt32 GarbageiOS.M_bipuSaynere54::_simemdall
	uint32_t ____simemdall_24;

public:
	inline static int32_t get_offset_of__yofaiXenar_0() { return static_cast<int32_t>(offsetof(M_bipuSaynere54_t1648617992, ____yofaiXenar_0)); }
	inline int32_t get__yofaiXenar_0() const { return ____yofaiXenar_0; }
	inline int32_t* get_address_of__yofaiXenar_0() { return &____yofaiXenar_0; }
	inline void set__yofaiXenar_0(int32_t value)
	{
		____yofaiXenar_0 = value;
	}

	inline static int32_t get_offset_of__retarsairNahaw_1() { return static_cast<int32_t>(offsetof(M_bipuSaynere54_t1648617992, ____retarsairNahaw_1)); }
	inline uint32_t get__retarsairNahaw_1() const { return ____retarsairNahaw_1; }
	inline uint32_t* get_address_of__retarsairNahaw_1() { return &____retarsairNahaw_1; }
	inline void set__retarsairNahaw_1(uint32_t value)
	{
		____retarsairNahaw_1 = value;
	}

	inline static int32_t get_offset_of__xuwhurdeKairfepu_2() { return static_cast<int32_t>(offsetof(M_bipuSaynere54_t1648617992, ____xuwhurdeKairfepu_2)); }
	inline String_t* get__xuwhurdeKairfepu_2() const { return ____xuwhurdeKairfepu_2; }
	inline String_t** get_address_of__xuwhurdeKairfepu_2() { return &____xuwhurdeKairfepu_2; }
	inline void set__xuwhurdeKairfepu_2(String_t* value)
	{
		____xuwhurdeKairfepu_2 = value;
		Il2CppCodeGenWriteBarrier(&____xuwhurdeKairfepu_2, value);
	}

	inline static int32_t get_offset_of__rarcereNasheji_3() { return static_cast<int32_t>(offsetof(M_bipuSaynere54_t1648617992, ____rarcereNasheji_3)); }
	inline int32_t get__rarcereNasheji_3() const { return ____rarcereNasheji_3; }
	inline int32_t* get_address_of__rarcereNasheji_3() { return &____rarcereNasheji_3; }
	inline void set__rarcereNasheji_3(int32_t value)
	{
		____rarcereNasheji_3 = value;
	}

	inline static int32_t get_offset_of__rousiVeweaker_4() { return static_cast<int32_t>(offsetof(M_bipuSaynere54_t1648617992, ____rousiVeweaker_4)); }
	inline float get__rousiVeweaker_4() const { return ____rousiVeweaker_4; }
	inline float* get_address_of__rousiVeweaker_4() { return &____rousiVeweaker_4; }
	inline void set__rousiVeweaker_4(float value)
	{
		____rousiVeweaker_4 = value;
	}

	inline static int32_t get_offset_of__ritaFurpir_5() { return static_cast<int32_t>(offsetof(M_bipuSaynere54_t1648617992, ____ritaFurpir_5)); }
	inline int32_t get__ritaFurpir_5() const { return ____ritaFurpir_5; }
	inline int32_t* get_address_of__ritaFurpir_5() { return &____ritaFurpir_5; }
	inline void set__ritaFurpir_5(int32_t value)
	{
		____ritaFurpir_5 = value;
	}

	inline static int32_t get_offset_of__gorse_6() { return static_cast<int32_t>(offsetof(M_bipuSaynere54_t1648617992, ____gorse_6)); }
	inline bool get__gorse_6() const { return ____gorse_6; }
	inline bool* get_address_of__gorse_6() { return &____gorse_6; }
	inline void set__gorse_6(bool value)
	{
		____gorse_6 = value;
	}

	inline static int32_t get_offset_of__qekascayRoowe_7() { return static_cast<int32_t>(offsetof(M_bipuSaynere54_t1648617992, ____qekascayRoowe_7)); }
	inline float get__qekascayRoowe_7() const { return ____qekascayRoowe_7; }
	inline float* get_address_of__qekascayRoowe_7() { return &____qekascayRoowe_7; }
	inline void set__qekascayRoowe_7(float value)
	{
		____qekascayRoowe_7 = value;
	}

	inline static int32_t get_offset_of__rircear_8() { return static_cast<int32_t>(offsetof(M_bipuSaynere54_t1648617992, ____rircear_8)); }
	inline float get__rircear_8() const { return ____rircear_8; }
	inline float* get_address_of__rircear_8() { return &____rircear_8; }
	inline void set__rircear_8(float value)
	{
		____rircear_8 = value;
	}

	inline static int32_t get_offset_of__girsowKirmisbas_9() { return static_cast<int32_t>(offsetof(M_bipuSaynere54_t1648617992, ____girsowKirmisbas_9)); }
	inline uint32_t get__girsowKirmisbas_9() const { return ____girsowKirmisbas_9; }
	inline uint32_t* get_address_of__girsowKirmisbas_9() { return &____girsowKirmisbas_9; }
	inline void set__girsowKirmisbas_9(uint32_t value)
	{
		____girsowKirmisbas_9 = value;
	}

	inline static int32_t get_offset_of__haca_10() { return static_cast<int32_t>(offsetof(M_bipuSaynere54_t1648617992, ____haca_10)); }
	inline float get__haca_10() const { return ____haca_10; }
	inline float* get_address_of__haca_10() { return &____haca_10; }
	inline void set__haca_10(float value)
	{
		____haca_10 = value;
	}

	inline static int32_t get_offset_of__jermairkoZebal_11() { return static_cast<int32_t>(offsetof(M_bipuSaynere54_t1648617992, ____jermairkoZebal_11)); }
	inline uint32_t get__jermairkoZebal_11() const { return ____jermairkoZebal_11; }
	inline uint32_t* get_address_of__jermairkoZebal_11() { return &____jermairkoZebal_11; }
	inline void set__jermairkoZebal_11(uint32_t value)
	{
		____jermairkoZebal_11 = value;
	}

	inline static int32_t get_offset_of__mesehairTainukoo_12() { return static_cast<int32_t>(offsetof(M_bipuSaynere54_t1648617992, ____mesehairTainukoo_12)); }
	inline bool get__mesehairTainukoo_12() const { return ____mesehairTainukoo_12; }
	inline bool* get_address_of__mesehairTainukoo_12() { return &____mesehairTainukoo_12; }
	inline void set__mesehairTainukoo_12(bool value)
	{
		____mesehairTainukoo_12 = value;
	}

	inline static int32_t get_offset_of__likemtu_13() { return static_cast<int32_t>(offsetof(M_bipuSaynere54_t1648617992, ____likemtu_13)); }
	inline uint32_t get__likemtu_13() const { return ____likemtu_13; }
	inline uint32_t* get_address_of__likemtu_13() { return &____likemtu_13; }
	inline void set__likemtu_13(uint32_t value)
	{
		____likemtu_13 = value;
	}

	inline static int32_t get_offset_of__herchea_14() { return static_cast<int32_t>(offsetof(M_bipuSaynere54_t1648617992, ____herchea_14)); }
	inline String_t* get__herchea_14() const { return ____herchea_14; }
	inline String_t** get_address_of__herchea_14() { return &____herchea_14; }
	inline void set__herchea_14(String_t* value)
	{
		____herchea_14 = value;
		Il2CppCodeGenWriteBarrier(&____herchea_14, value);
	}

	inline static int32_t get_offset_of__jererouStowmupu_15() { return static_cast<int32_t>(offsetof(M_bipuSaynere54_t1648617992, ____jererouStowmupu_15)); }
	inline bool get__jererouStowmupu_15() const { return ____jererouStowmupu_15; }
	inline bool* get_address_of__jererouStowmupu_15() { return &____jererouStowmupu_15; }
	inline void set__jererouStowmupu_15(bool value)
	{
		____jererouStowmupu_15 = value;
	}

	inline static int32_t get_offset_of__hereferPimorra_16() { return static_cast<int32_t>(offsetof(M_bipuSaynere54_t1648617992, ____hereferPimorra_16)); }
	inline float get__hereferPimorra_16() const { return ____hereferPimorra_16; }
	inline float* get_address_of__hereferPimorra_16() { return &____hereferPimorra_16; }
	inline void set__hereferPimorra_16(float value)
	{
		____hereferPimorra_16 = value;
	}

	inline static int32_t get_offset_of__misdall_17() { return static_cast<int32_t>(offsetof(M_bipuSaynere54_t1648617992, ____misdall_17)); }
	inline uint32_t get__misdall_17() const { return ____misdall_17; }
	inline uint32_t* get_address_of__misdall_17() { return &____misdall_17; }
	inline void set__misdall_17(uint32_t value)
	{
		____misdall_17 = value;
	}

	inline static int32_t get_offset_of__warsur_18() { return static_cast<int32_t>(offsetof(M_bipuSaynere54_t1648617992, ____warsur_18)); }
	inline uint32_t get__warsur_18() const { return ____warsur_18; }
	inline uint32_t* get_address_of__warsur_18() { return &____warsur_18; }
	inline void set__warsur_18(uint32_t value)
	{
		____warsur_18 = value;
	}

	inline static int32_t get_offset_of__muryelbelDorro_19() { return static_cast<int32_t>(offsetof(M_bipuSaynere54_t1648617992, ____muryelbelDorro_19)); }
	inline bool get__muryelbelDorro_19() const { return ____muryelbelDorro_19; }
	inline bool* get_address_of__muryelbelDorro_19() { return &____muryelbelDorro_19; }
	inline void set__muryelbelDorro_19(bool value)
	{
		____muryelbelDorro_19 = value;
	}

	inline static int32_t get_offset_of__joutooSorcearkur_20() { return static_cast<int32_t>(offsetof(M_bipuSaynere54_t1648617992, ____joutooSorcearkur_20)); }
	inline float get__joutooSorcearkur_20() const { return ____joutooSorcearkur_20; }
	inline float* get_address_of__joutooSorcearkur_20() { return &____joutooSorcearkur_20; }
	inline void set__joutooSorcearkur_20(float value)
	{
		____joutooSorcearkur_20 = value;
	}

	inline static int32_t get_offset_of__xedecem_21() { return static_cast<int32_t>(offsetof(M_bipuSaynere54_t1648617992, ____xedecem_21)); }
	inline float get__xedecem_21() const { return ____xedecem_21; }
	inline float* get_address_of__xedecem_21() { return &____xedecem_21; }
	inline void set__xedecem_21(float value)
	{
		____xedecem_21 = value;
	}

	inline static int32_t get_offset_of__xoukemda_22() { return static_cast<int32_t>(offsetof(M_bipuSaynere54_t1648617992, ____xoukemda_22)); }
	inline String_t* get__xoukemda_22() const { return ____xoukemda_22; }
	inline String_t** get_address_of__xoukemda_22() { return &____xoukemda_22; }
	inline void set__xoukemda_22(String_t* value)
	{
		____xoukemda_22 = value;
		Il2CppCodeGenWriteBarrier(&____xoukemda_22, value);
	}

	inline static int32_t get_offset_of__selrayjoMiku_23() { return static_cast<int32_t>(offsetof(M_bipuSaynere54_t1648617992, ____selrayjoMiku_23)); }
	inline bool get__selrayjoMiku_23() const { return ____selrayjoMiku_23; }
	inline bool* get_address_of__selrayjoMiku_23() { return &____selrayjoMiku_23; }
	inline void set__selrayjoMiku_23(bool value)
	{
		____selrayjoMiku_23 = value;
	}

	inline static int32_t get_offset_of__simemdall_24() { return static_cast<int32_t>(offsetof(M_bipuSaynere54_t1648617992, ____simemdall_24)); }
	inline uint32_t get__simemdall_24() const { return ____simemdall_24; }
	inline uint32_t* get_address_of__simemdall_24() { return &____simemdall_24; }
	inline void set__simemdall_24(uint32_t value)
	{
		____simemdall_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
