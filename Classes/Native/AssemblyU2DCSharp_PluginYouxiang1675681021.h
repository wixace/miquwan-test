﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PluginYouxiang
struct  PluginYouxiang_t1675681021  : public MonoBehaviour_t667441552
{
public:
	// System.String PluginYouxiang::ptid
	String_t* ___ptid_2;
	// System.String PluginYouxiang::gid
	String_t* ___gid_3;
	// System.String PluginYouxiang::token
	String_t* ___token_4;
	// System.String[] PluginYouxiang::sdkInfos
	StringU5BU5D_t4054002952* ___sdkInfos_5;
	// System.String PluginYouxiang::configId
	String_t* ___configId_6;
	// System.Boolean PluginYouxiang::isOut
	bool ___isOut_7;

public:
	inline static int32_t get_offset_of_ptid_2() { return static_cast<int32_t>(offsetof(PluginYouxiang_t1675681021, ___ptid_2)); }
	inline String_t* get_ptid_2() const { return ___ptid_2; }
	inline String_t** get_address_of_ptid_2() { return &___ptid_2; }
	inline void set_ptid_2(String_t* value)
	{
		___ptid_2 = value;
		Il2CppCodeGenWriteBarrier(&___ptid_2, value);
	}

	inline static int32_t get_offset_of_gid_3() { return static_cast<int32_t>(offsetof(PluginYouxiang_t1675681021, ___gid_3)); }
	inline String_t* get_gid_3() const { return ___gid_3; }
	inline String_t** get_address_of_gid_3() { return &___gid_3; }
	inline void set_gid_3(String_t* value)
	{
		___gid_3 = value;
		Il2CppCodeGenWriteBarrier(&___gid_3, value);
	}

	inline static int32_t get_offset_of_token_4() { return static_cast<int32_t>(offsetof(PluginYouxiang_t1675681021, ___token_4)); }
	inline String_t* get_token_4() const { return ___token_4; }
	inline String_t** get_address_of_token_4() { return &___token_4; }
	inline void set_token_4(String_t* value)
	{
		___token_4 = value;
		Il2CppCodeGenWriteBarrier(&___token_4, value);
	}

	inline static int32_t get_offset_of_sdkInfos_5() { return static_cast<int32_t>(offsetof(PluginYouxiang_t1675681021, ___sdkInfos_5)); }
	inline StringU5BU5D_t4054002952* get_sdkInfos_5() const { return ___sdkInfos_5; }
	inline StringU5BU5D_t4054002952** get_address_of_sdkInfos_5() { return &___sdkInfos_5; }
	inline void set_sdkInfos_5(StringU5BU5D_t4054002952* value)
	{
		___sdkInfos_5 = value;
		Il2CppCodeGenWriteBarrier(&___sdkInfos_5, value);
	}

	inline static int32_t get_offset_of_configId_6() { return static_cast<int32_t>(offsetof(PluginYouxiang_t1675681021, ___configId_6)); }
	inline String_t* get_configId_6() const { return ___configId_6; }
	inline String_t** get_address_of_configId_6() { return &___configId_6; }
	inline void set_configId_6(String_t* value)
	{
		___configId_6 = value;
		Il2CppCodeGenWriteBarrier(&___configId_6, value);
	}

	inline static int32_t get_offset_of_isOut_7() { return static_cast<int32_t>(offsetof(PluginYouxiang_t1675681021, ___isOut_7)); }
	inline bool get_isOut_7() const { return ___isOut_7; }
	inline bool* get_address_of_isOut_7() { return &___isOut_7; }
	inline void set_isOut_7(bool value)
	{
		___isOut_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
