﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_serejamir44
struct  M_serejamir44_t1742413338  : public Il2CppObject
{
public:
	// System.Int32 GarbageiOS.M_serejamir44::_houge
	int32_t ____houge_0;
	// System.UInt32 GarbageiOS.M_serejamir44::_yasjaraChallvizir
	uint32_t ____yasjaraChallvizir_1;
	// System.UInt32 GarbageiOS.M_serejamir44::_rahastor
	uint32_t ____rahastor_2;
	// System.String GarbageiOS.M_serejamir44::_mayaSowgeser
	String_t* ____mayaSowgeser_3;
	// System.Boolean GarbageiOS.M_serejamir44::_dalldallpallSaryee
	bool ____dalldallpallSaryee_4;
	// System.String GarbageiOS.M_serejamir44::_masi
	String_t* ____masi_5;
	// System.Single GarbageiOS.M_serejamir44::_dearcouBeageasou
	float ____dearcouBeageasou_6;
	// System.UInt32 GarbageiOS.M_serejamir44::_sairsawBakiste
	uint32_t ____sairsawBakiste_7;
	// System.Boolean GarbageiOS.M_serejamir44::_houyeCemlem
	bool ____houyeCemlem_8;
	// System.String GarbageiOS.M_serejamir44::_sebere
	String_t* ____sebere_9;
	// System.Boolean GarbageiOS.M_serejamir44::_bairde
	bool ____bairde_10;
	// System.Int32 GarbageiOS.M_serejamir44::_sadar
	int32_t ____sadar_11;
	// System.UInt32 GarbageiOS.M_serejamir44::_dimimee
	uint32_t ____dimimee_12;
	// System.Int32 GarbageiOS.M_serejamir44::_safatroo
	int32_t ____safatroo_13;
	// System.Int32 GarbageiOS.M_serejamir44::_cenurke
	int32_t ____cenurke_14;
	// System.Single GarbageiOS.M_serejamir44::_haralsee
	float ____haralsee_15;
	// System.Boolean GarbageiOS.M_serejamir44::_jouli
	bool ____jouli_16;
	// System.Single GarbageiOS.M_serejamir44::_dosou
	float ____dosou_17;
	// System.Single GarbageiOS.M_serejamir44::_celyorva
	float ____celyorva_18;
	// System.String GarbageiOS.M_serejamir44::_wanemmairTrasere
	String_t* ____wanemmairTrasere_19;
	// System.Int32 GarbageiOS.M_serejamir44::_rallnoumel
	int32_t ____rallnoumel_20;
	// System.Single GarbageiOS.M_serejamir44::_nisgusiCawe
	float ____nisgusiCawe_21;
	// System.Single GarbageiOS.M_serejamir44::_rorbayjee
	float ____rorbayjee_22;
	// System.UInt32 GarbageiOS.M_serejamir44::_whurcar
	uint32_t ____whurcar_23;
	// System.Int32 GarbageiOS.M_serejamir44::_ceasisRairsear
	int32_t ____ceasisRairsear_24;
	// System.Int32 GarbageiOS.M_serejamir44::_tairworPallhor
	int32_t ____tairworPallhor_25;
	// System.Boolean GarbageiOS.M_serejamir44::_tusea
	bool ____tusea_26;
	// System.Int32 GarbageiOS.M_serejamir44::_rallmermiDaijoosay
	int32_t ____rallmermiDaijoosay_27;
	// System.Boolean GarbageiOS.M_serejamir44::_mornarra
	bool ____mornarra_28;
	// System.Single GarbageiOS.M_serejamir44::_yearpaiChela
	float ____yearpaiChela_29;
	// System.Boolean GarbageiOS.M_serejamir44::_nalfar
	bool ____nalfar_30;
	// System.Boolean GarbageiOS.M_serejamir44::_semwalRenaygee
	bool ____semwalRenaygee_31;
	// System.Int32 GarbageiOS.M_serejamir44::_waizisda
	int32_t ____waizisda_32;
	// System.UInt32 GarbageiOS.M_serejamir44::_beemar
	uint32_t ____beemar_33;
	// System.UInt32 GarbageiOS.M_serejamir44::_vokirli
	uint32_t ____vokirli_34;
	// System.Boolean GarbageiOS.M_serejamir44::_qusiPayrisfel
	bool ____qusiPayrisfel_35;
	// System.Boolean GarbageiOS.M_serejamir44::_mowmoki
	bool ____mowmoki_36;
	// System.String GarbageiOS.M_serejamir44::_lairdray
	String_t* ____lairdray_37;
	// System.Int32 GarbageiOS.M_serejamir44::_carfearree
	int32_t ____carfearree_38;
	// System.Int32 GarbageiOS.M_serejamir44::_wermouCairtemsou
	int32_t ____wermouCairtemsou_39;
	// System.Boolean GarbageiOS.M_serejamir44::_mayris
	bool ____mayris_40;
	// System.String GarbageiOS.M_serejamir44::_woudasKerebis
	String_t* ____woudasKerebis_41;
	// System.UInt32 GarbageiOS.M_serejamir44::_zaldar
	uint32_t ____zaldar_42;
	// System.Int32 GarbageiOS.M_serejamir44::_chosarcorRowbacel
	int32_t ____chosarcorRowbacel_43;
	// System.Single GarbageiOS.M_serejamir44::_matu
	float ____matu_44;
	// System.UInt32 GarbageiOS.M_serejamir44::_bijall
	uint32_t ____bijall_45;
	// System.Boolean GarbageiOS.M_serejamir44::_misjaneWeevir
	bool ____misjaneWeevir_46;
	// System.Single GarbageiOS.M_serejamir44::_lowrar
	float ____lowrar_47;

public:
	inline static int32_t get_offset_of__houge_0() { return static_cast<int32_t>(offsetof(M_serejamir44_t1742413338, ____houge_0)); }
	inline int32_t get__houge_0() const { return ____houge_0; }
	inline int32_t* get_address_of__houge_0() { return &____houge_0; }
	inline void set__houge_0(int32_t value)
	{
		____houge_0 = value;
	}

	inline static int32_t get_offset_of__yasjaraChallvizir_1() { return static_cast<int32_t>(offsetof(M_serejamir44_t1742413338, ____yasjaraChallvizir_1)); }
	inline uint32_t get__yasjaraChallvizir_1() const { return ____yasjaraChallvizir_1; }
	inline uint32_t* get_address_of__yasjaraChallvizir_1() { return &____yasjaraChallvizir_1; }
	inline void set__yasjaraChallvizir_1(uint32_t value)
	{
		____yasjaraChallvizir_1 = value;
	}

	inline static int32_t get_offset_of__rahastor_2() { return static_cast<int32_t>(offsetof(M_serejamir44_t1742413338, ____rahastor_2)); }
	inline uint32_t get__rahastor_2() const { return ____rahastor_2; }
	inline uint32_t* get_address_of__rahastor_2() { return &____rahastor_2; }
	inline void set__rahastor_2(uint32_t value)
	{
		____rahastor_2 = value;
	}

	inline static int32_t get_offset_of__mayaSowgeser_3() { return static_cast<int32_t>(offsetof(M_serejamir44_t1742413338, ____mayaSowgeser_3)); }
	inline String_t* get__mayaSowgeser_3() const { return ____mayaSowgeser_3; }
	inline String_t** get_address_of__mayaSowgeser_3() { return &____mayaSowgeser_3; }
	inline void set__mayaSowgeser_3(String_t* value)
	{
		____mayaSowgeser_3 = value;
		Il2CppCodeGenWriteBarrier(&____mayaSowgeser_3, value);
	}

	inline static int32_t get_offset_of__dalldallpallSaryee_4() { return static_cast<int32_t>(offsetof(M_serejamir44_t1742413338, ____dalldallpallSaryee_4)); }
	inline bool get__dalldallpallSaryee_4() const { return ____dalldallpallSaryee_4; }
	inline bool* get_address_of__dalldallpallSaryee_4() { return &____dalldallpallSaryee_4; }
	inline void set__dalldallpallSaryee_4(bool value)
	{
		____dalldallpallSaryee_4 = value;
	}

	inline static int32_t get_offset_of__masi_5() { return static_cast<int32_t>(offsetof(M_serejamir44_t1742413338, ____masi_5)); }
	inline String_t* get__masi_5() const { return ____masi_5; }
	inline String_t** get_address_of__masi_5() { return &____masi_5; }
	inline void set__masi_5(String_t* value)
	{
		____masi_5 = value;
		Il2CppCodeGenWriteBarrier(&____masi_5, value);
	}

	inline static int32_t get_offset_of__dearcouBeageasou_6() { return static_cast<int32_t>(offsetof(M_serejamir44_t1742413338, ____dearcouBeageasou_6)); }
	inline float get__dearcouBeageasou_6() const { return ____dearcouBeageasou_6; }
	inline float* get_address_of__dearcouBeageasou_6() { return &____dearcouBeageasou_6; }
	inline void set__dearcouBeageasou_6(float value)
	{
		____dearcouBeageasou_6 = value;
	}

	inline static int32_t get_offset_of__sairsawBakiste_7() { return static_cast<int32_t>(offsetof(M_serejamir44_t1742413338, ____sairsawBakiste_7)); }
	inline uint32_t get__sairsawBakiste_7() const { return ____sairsawBakiste_7; }
	inline uint32_t* get_address_of__sairsawBakiste_7() { return &____sairsawBakiste_7; }
	inline void set__sairsawBakiste_7(uint32_t value)
	{
		____sairsawBakiste_7 = value;
	}

	inline static int32_t get_offset_of__houyeCemlem_8() { return static_cast<int32_t>(offsetof(M_serejamir44_t1742413338, ____houyeCemlem_8)); }
	inline bool get__houyeCemlem_8() const { return ____houyeCemlem_8; }
	inline bool* get_address_of__houyeCemlem_8() { return &____houyeCemlem_8; }
	inline void set__houyeCemlem_8(bool value)
	{
		____houyeCemlem_8 = value;
	}

	inline static int32_t get_offset_of__sebere_9() { return static_cast<int32_t>(offsetof(M_serejamir44_t1742413338, ____sebere_9)); }
	inline String_t* get__sebere_9() const { return ____sebere_9; }
	inline String_t** get_address_of__sebere_9() { return &____sebere_9; }
	inline void set__sebere_9(String_t* value)
	{
		____sebere_9 = value;
		Il2CppCodeGenWriteBarrier(&____sebere_9, value);
	}

	inline static int32_t get_offset_of__bairde_10() { return static_cast<int32_t>(offsetof(M_serejamir44_t1742413338, ____bairde_10)); }
	inline bool get__bairde_10() const { return ____bairde_10; }
	inline bool* get_address_of__bairde_10() { return &____bairde_10; }
	inline void set__bairde_10(bool value)
	{
		____bairde_10 = value;
	}

	inline static int32_t get_offset_of__sadar_11() { return static_cast<int32_t>(offsetof(M_serejamir44_t1742413338, ____sadar_11)); }
	inline int32_t get__sadar_11() const { return ____sadar_11; }
	inline int32_t* get_address_of__sadar_11() { return &____sadar_11; }
	inline void set__sadar_11(int32_t value)
	{
		____sadar_11 = value;
	}

	inline static int32_t get_offset_of__dimimee_12() { return static_cast<int32_t>(offsetof(M_serejamir44_t1742413338, ____dimimee_12)); }
	inline uint32_t get__dimimee_12() const { return ____dimimee_12; }
	inline uint32_t* get_address_of__dimimee_12() { return &____dimimee_12; }
	inline void set__dimimee_12(uint32_t value)
	{
		____dimimee_12 = value;
	}

	inline static int32_t get_offset_of__safatroo_13() { return static_cast<int32_t>(offsetof(M_serejamir44_t1742413338, ____safatroo_13)); }
	inline int32_t get__safatroo_13() const { return ____safatroo_13; }
	inline int32_t* get_address_of__safatroo_13() { return &____safatroo_13; }
	inline void set__safatroo_13(int32_t value)
	{
		____safatroo_13 = value;
	}

	inline static int32_t get_offset_of__cenurke_14() { return static_cast<int32_t>(offsetof(M_serejamir44_t1742413338, ____cenurke_14)); }
	inline int32_t get__cenurke_14() const { return ____cenurke_14; }
	inline int32_t* get_address_of__cenurke_14() { return &____cenurke_14; }
	inline void set__cenurke_14(int32_t value)
	{
		____cenurke_14 = value;
	}

	inline static int32_t get_offset_of__haralsee_15() { return static_cast<int32_t>(offsetof(M_serejamir44_t1742413338, ____haralsee_15)); }
	inline float get__haralsee_15() const { return ____haralsee_15; }
	inline float* get_address_of__haralsee_15() { return &____haralsee_15; }
	inline void set__haralsee_15(float value)
	{
		____haralsee_15 = value;
	}

	inline static int32_t get_offset_of__jouli_16() { return static_cast<int32_t>(offsetof(M_serejamir44_t1742413338, ____jouli_16)); }
	inline bool get__jouli_16() const { return ____jouli_16; }
	inline bool* get_address_of__jouli_16() { return &____jouli_16; }
	inline void set__jouli_16(bool value)
	{
		____jouli_16 = value;
	}

	inline static int32_t get_offset_of__dosou_17() { return static_cast<int32_t>(offsetof(M_serejamir44_t1742413338, ____dosou_17)); }
	inline float get__dosou_17() const { return ____dosou_17; }
	inline float* get_address_of__dosou_17() { return &____dosou_17; }
	inline void set__dosou_17(float value)
	{
		____dosou_17 = value;
	}

	inline static int32_t get_offset_of__celyorva_18() { return static_cast<int32_t>(offsetof(M_serejamir44_t1742413338, ____celyorva_18)); }
	inline float get__celyorva_18() const { return ____celyorva_18; }
	inline float* get_address_of__celyorva_18() { return &____celyorva_18; }
	inline void set__celyorva_18(float value)
	{
		____celyorva_18 = value;
	}

	inline static int32_t get_offset_of__wanemmairTrasere_19() { return static_cast<int32_t>(offsetof(M_serejamir44_t1742413338, ____wanemmairTrasere_19)); }
	inline String_t* get__wanemmairTrasere_19() const { return ____wanemmairTrasere_19; }
	inline String_t** get_address_of__wanemmairTrasere_19() { return &____wanemmairTrasere_19; }
	inline void set__wanemmairTrasere_19(String_t* value)
	{
		____wanemmairTrasere_19 = value;
		Il2CppCodeGenWriteBarrier(&____wanemmairTrasere_19, value);
	}

	inline static int32_t get_offset_of__rallnoumel_20() { return static_cast<int32_t>(offsetof(M_serejamir44_t1742413338, ____rallnoumel_20)); }
	inline int32_t get__rallnoumel_20() const { return ____rallnoumel_20; }
	inline int32_t* get_address_of__rallnoumel_20() { return &____rallnoumel_20; }
	inline void set__rallnoumel_20(int32_t value)
	{
		____rallnoumel_20 = value;
	}

	inline static int32_t get_offset_of__nisgusiCawe_21() { return static_cast<int32_t>(offsetof(M_serejamir44_t1742413338, ____nisgusiCawe_21)); }
	inline float get__nisgusiCawe_21() const { return ____nisgusiCawe_21; }
	inline float* get_address_of__nisgusiCawe_21() { return &____nisgusiCawe_21; }
	inline void set__nisgusiCawe_21(float value)
	{
		____nisgusiCawe_21 = value;
	}

	inline static int32_t get_offset_of__rorbayjee_22() { return static_cast<int32_t>(offsetof(M_serejamir44_t1742413338, ____rorbayjee_22)); }
	inline float get__rorbayjee_22() const { return ____rorbayjee_22; }
	inline float* get_address_of__rorbayjee_22() { return &____rorbayjee_22; }
	inline void set__rorbayjee_22(float value)
	{
		____rorbayjee_22 = value;
	}

	inline static int32_t get_offset_of__whurcar_23() { return static_cast<int32_t>(offsetof(M_serejamir44_t1742413338, ____whurcar_23)); }
	inline uint32_t get__whurcar_23() const { return ____whurcar_23; }
	inline uint32_t* get_address_of__whurcar_23() { return &____whurcar_23; }
	inline void set__whurcar_23(uint32_t value)
	{
		____whurcar_23 = value;
	}

	inline static int32_t get_offset_of__ceasisRairsear_24() { return static_cast<int32_t>(offsetof(M_serejamir44_t1742413338, ____ceasisRairsear_24)); }
	inline int32_t get__ceasisRairsear_24() const { return ____ceasisRairsear_24; }
	inline int32_t* get_address_of__ceasisRairsear_24() { return &____ceasisRairsear_24; }
	inline void set__ceasisRairsear_24(int32_t value)
	{
		____ceasisRairsear_24 = value;
	}

	inline static int32_t get_offset_of__tairworPallhor_25() { return static_cast<int32_t>(offsetof(M_serejamir44_t1742413338, ____tairworPallhor_25)); }
	inline int32_t get__tairworPallhor_25() const { return ____tairworPallhor_25; }
	inline int32_t* get_address_of__tairworPallhor_25() { return &____tairworPallhor_25; }
	inline void set__tairworPallhor_25(int32_t value)
	{
		____tairworPallhor_25 = value;
	}

	inline static int32_t get_offset_of__tusea_26() { return static_cast<int32_t>(offsetof(M_serejamir44_t1742413338, ____tusea_26)); }
	inline bool get__tusea_26() const { return ____tusea_26; }
	inline bool* get_address_of__tusea_26() { return &____tusea_26; }
	inline void set__tusea_26(bool value)
	{
		____tusea_26 = value;
	}

	inline static int32_t get_offset_of__rallmermiDaijoosay_27() { return static_cast<int32_t>(offsetof(M_serejamir44_t1742413338, ____rallmermiDaijoosay_27)); }
	inline int32_t get__rallmermiDaijoosay_27() const { return ____rallmermiDaijoosay_27; }
	inline int32_t* get_address_of__rallmermiDaijoosay_27() { return &____rallmermiDaijoosay_27; }
	inline void set__rallmermiDaijoosay_27(int32_t value)
	{
		____rallmermiDaijoosay_27 = value;
	}

	inline static int32_t get_offset_of__mornarra_28() { return static_cast<int32_t>(offsetof(M_serejamir44_t1742413338, ____mornarra_28)); }
	inline bool get__mornarra_28() const { return ____mornarra_28; }
	inline bool* get_address_of__mornarra_28() { return &____mornarra_28; }
	inline void set__mornarra_28(bool value)
	{
		____mornarra_28 = value;
	}

	inline static int32_t get_offset_of__yearpaiChela_29() { return static_cast<int32_t>(offsetof(M_serejamir44_t1742413338, ____yearpaiChela_29)); }
	inline float get__yearpaiChela_29() const { return ____yearpaiChela_29; }
	inline float* get_address_of__yearpaiChela_29() { return &____yearpaiChela_29; }
	inline void set__yearpaiChela_29(float value)
	{
		____yearpaiChela_29 = value;
	}

	inline static int32_t get_offset_of__nalfar_30() { return static_cast<int32_t>(offsetof(M_serejamir44_t1742413338, ____nalfar_30)); }
	inline bool get__nalfar_30() const { return ____nalfar_30; }
	inline bool* get_address_of__nalfar_30() { return &____nalfar_30; }
	inline void set__nalfar_30(bool value)
	{
		____nalfar_30 = value;
	}

	inline static int32_t get_offset_of__semwalRenaygee_31() { return static_cast<int32_t>(offsetof(M_serejamir44_t1742413338, ____semwalRenaygee_31)); }
	inline bool get__semwalRenaygee_31() const { return ____semwalRenaygee_31; }
	inline bool* get_address_of__semwalRenaygee_31() { return &____semwalRenaygee_31; }
	inline void set__semwalRenaygee_31(bool value)
	{
		____semwalRenaygee_31 = value;
	}

	inline static int32_t get_offset_of__waizisda_32() { return static_cast<int32_t>(offsetof(M_serejamir44_t1742413338, ____waizisda_32)); }
	inline int32_t get__waizisda_32() const { return ____waizisda_32; }
	inline int32_t* get_address_of__waizisda_32() { return &____waizisda_32; }
	inline void set__waizisda_32(int32_t value)
	{
		____waizisda_32 = value;
	}

	inline static int32_t get_offset_of__beemar_33() { return static_cast<int32_t>(offsetof(M_serejamir44_t1742413338, ____beemar_33)); }
	inline uint32_t get__beemar_33() const { return ____beemar_33; }
	inline uint32_t* get_address_of__beemar_33() { return &____beemar_33; }
	inline void set__beemar_33(uint32_t value)
	{
		____beemar_33 = value;
	}

	inline static int32_t get_offset_of__vokirli_34() { return static_cast<int32_t>(offsetof(M_serejamir44_t1742413338, ____vokirli_34)); }
	inline uint32_t get__vokirli_34() const { return ____vokirli_34; }
	inline uint32_t* get_address_of__vokirli_34() { return &____vokirli_34; }
	inline void set__vokirli_34(uint32_t value)
	{
		____vokirli_34 = value;
	}

	inline static int32_t get_offset_of__qusiPayrisfel_35() { return static_cast<int32_t>(offsetof(M_serejamir44_t1742413338, ____qusiPayrisfel_35)); }
	inline bool get__qusiPayrisfel_35() const { return ____qusiPayrisfel_35; }
	inline bool* get_address_of__qusiPayrisfel_35() { return &____qusiPayrisfel_35; }
	inline void set__qusiPayrisfel_35(bool value)
	{
		____qusiPayrisfel_35 = value;
	}

	inline static int32_t get_offset_of__mowmoki_36() { return static_cast<int32_t>(offsetof(M_serejamir44_t1742413338, ____mowmoki_36)); }
	inline bool get__mowmoki_36() const { return ____mowmoki_36; }
	inline bool* get_address_of__mowmoki_36() { return &____mowmoki_36; }
	inline void set__mowmoki_36(bool value)
	{
		____mowmoki_36 = value;
	}

	inline static int32_t get_offset_of__lairdray_37() { return static_cast<int32_t>(offsetof(M_serejamir44_t1742413338, ____lairdray_37)); }
	inline String_t* get__lairdray_37() const { return ____lairdray_37; }
	inline String_t** get_address_of__lairdray_37() { return &____lairdray_37; }
	inline void set__lairdray_37(String_t* value)
	{
		____lairdray_37 = value;
		Il2CppCodeGenWriteBarrier(&____lairdray_37, value);
	}

	inline static int32_t get_offset_of__carfearree_38() { return static_cast<int32_t>(offsetof(M_serejamir44_t1742413338, ____carfearree_38)); }
	inline int32_t get__carfearree_38() const { return ____carfearree_38; }
	inline int32_t* get_address_of__carfearree_38() { return &____carfearree_38; }
	inline void set__carfearree_38(int32_t value)
	{
		____carfearree_38 = value;
	}

	inline static int32_t get_offset_of__wermouCairtemsou_39() { return static_cast<int32_t>(offsetof(M_serejamir44_t1742413338, ____wermouCairtemsou_39)); }
	inline int32_t get__wermouCairtemsou_39() const { return ____wermouCairtemsou_39; }
	inline int32_t* get_address_of__wermouCairtemsou_39() { return &____wermouCairtemsou_39; }
	inline void set__wermouCairtemsou_39(int32_t value)
	{
		____wermouCairtemsou_39 = value;
	}

	inline static int32_t get_offset_of__mayris_40() { return static_cast<int32_t>(offsetof(M_serejamir44_t1742413338, ____mayris_40)); }
	inline bool get__mayris_40() const { return ____mayris_40; }
	inline bool* get_address_of__mayris_40() { return &____mayris_40; }
	inline void set__mayris_40(bool value)
	{
		____mayris_40 = value;
	}

	inline static int32_t get_offset_of__woudasKerebis_41() { return static_cast<int32_t>(offsetof(M_serejamir44_t1742413338, ____woudasKerebis_41)); }
	inline String_t* get__woudasKerebis_41() const { return ____woudasKerebis_41; }
	inline String_t** get_address_of__woudasKerebis_41() { return &____woudasKerebis_41; }
	inline void set__woudasKerebis_41(String_t* value)
	{
		____woudasKerebis_41 = value;
		Il2CppCodeGenWriteBarrier(&____woudasKerebis_41, value);
	}

	inline static int32_t get_offset_of__zaldar_42() { return static_cast<int32_t>(offsetof(M_serejamir44_t1742413338, ____zaldar_42)); }
	inline uint32_t get__zaldar_42() const { return ____zaldar_42; }
	inline uint32_t* get_address_of__zaldar_42() { return &____zaldar_42; }
	inline void set__zaldar_42(uint32_t value)
	{
		____zaldar_42 = value;
	}

	inline static int32_t get_offset_of__chosarcorRowbacel_43() { return static_cast<int32_t>(offsetof(M_serejamir44_t1742413338, ____chosarcorRowbacel_43)); }
	inline int32_t get__chosarcorRowbacel_43() const { return ____chosarcorRowbacel_43; }
	inline int32_t* get_address_of__chosarcorRowbacel_43() { return &____chosarcorRowbacel_43; }
	inline void set__chosarcorRowbacel_43(int32_t value)
	{
		____chosarcorRowbacel_43 = value;
	}

	inline static int32_t get_offset_of__matu_44() { return static_cast<int32_t>(offsetof(M_serejamir44_t1742413338, ____matu_44)); }
	inline float get__matu_44() const { return ____matu_44; }
	inline float* get_address_of__matu_44() { return &____matu_44; }
	inline void set__matu_44(float value)
	{
		____matu_44 = value;
	}

	inline static int32_t get_offset_of__bijall_45() { return static_cast<int32_t>(offsetof(M_serejamir44_t1742413338, ____bijall_45)); }
	inline uint32_t get__bijall_45() const { return ____bijall_45; }
	inline uint32_t* get_address_of__bijall_45() { return &____bijall_45; }
	inline void set__bijall_45(uint32_t value)
	{
		____bijall_45 = value;
	}

	inline static int32_t get_offset_of__misjaneWeevir_46() { return static_cast<int32_t>(offsetof(M_serejamir44_t1742413338, ____misjaneWeevir_46)); }
	inline bool get__misjaneWeevir_46() const { return ____misjaneWeevir_46; }
	inline bool* get_address_of__misjaneWeevir_46() { return &____misjaneWeevir_46; }
	inline void set__misjaneWeevir_46(bool value)
	{
		____misjaneWeevir_46 = value;
	}

	inline static int32_t get_offset_of__lowrar_47() { return static_cast<int32_t>(offsetof(M_serejamir44_t1742413338, ____lowrar_47)); }
	inline float get__lowrar_47() const { return ____lowrar_47; }
	inline float* get_address_of__lowrar_47() { return &____lowrar_47; }
	inline void set__lowrar_47(float value)
	{
		____lowrar_47 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
