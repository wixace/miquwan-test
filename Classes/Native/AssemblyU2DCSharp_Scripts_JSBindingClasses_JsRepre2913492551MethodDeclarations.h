﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Scripts.JSBindingClasses.JsRepresentClass
struct JsRepresentClass_t2913492551;
// System.String
struct String_t;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// System.Object
struct Il2CppObject;
// System.Func`2<System.Object,System.Object>
struct Func_2_t184564025;
// System.Func`3<System.Object,System.Object,System.Object>
struct Func_3_t3956694567;
// System.Func`4<System.Object,System.Object,System.Object,System.Object>
struct Func_4_t3774609659;
// System.Func`5<System.Object,System.Object,System.Object,System.Object,System.Object>
struct Func_5_t4203452149;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void Scripts.JSBindingClasses.JsRepresentClass::.ctor()
extern "C"  void JsRepresentClass__ctor_m3342453748 (JsRepresentClass_t2913492551 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Scripts.JSBindingClasses.JsRepresentClass::GetScriptName()
extern "C"  String_t* JsRepresentClass_GetScriptName_m1574049633 (JsRepresentClass_t2913492551 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Scripts.JSBindingClasses.JsRepresentClass::InitSuccess()
extern "C"  bool JsRepresentClass_InitSuccess_m1466637081 (JsRepresentClass_t2913492551 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Scripts.JSBindingClasses.JsRepresentClass::initJS(System.String)
extern "C"  void JsRepresentClass_initJS_m2677597849 (JsRepresentClass_t2913492551 * __this, String_t* ___jsClassName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Scripts.JSBindingClasses.JsRepresentClass::GetJSObjID()
extern "C"  int32_t JsRepresentClass_GetJSObjID_m460060021 (JsRepresentClass_t2913492551 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Scripts.JSBindingClasses.JsRepresentClass::callIfExistByfunID(System.Int32,System.Object[])
extern "C"  void JsRepresentClass_callIfExistByfunID_m1959776660 (JsRepresentClass_t2913492551 * __this, int32_t ___funID0, ObjectU5BU5D_t1108656482* ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Scripts.JSBindingClasses.JsRepresentClass::CallFunctionByFunName(System.String,System.Object[])
extern "C"  Il2CppObject * JsRepresentClass_CallFunctionByFunName_m2501875722 (JsRepresentClass_t2913492551 * __this, String_t* ___FunctionName0, ObjectU5BU5D_t1108656482* ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Func`2<System.Object,System.Object> Scripts.JSBindingClasses.JsRepresentClass::GeneratedJSFuntion1(System.String)
extern "C"  Func_2_t184564025 * JsRepresentClass_GeneratedJSFuntion1_m2192458802 (JsRepresentClass_t2913492551 * __this, String_t* ___funcitonName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Func`3<System.Object,System.Object,System.Object> Scripts.JSBindingClasses.JsRepresentClass::GeneratedJSFuntion2(System.String)
extern "C"  Func_3_t3956694567 * JsRepresentClass_GeneratedJSFuntion2_m2895065150 (JsRepresentClass_t2913492551 * __this, String_t* ___funcitonName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Func`4<System.Object,System.Object,System.Object,System.Object> Scripts.JSBindingClasses.JsRepresentClass::GeneratedJSFuntion3(System.String)
extern "C"  Func_4_t3774609659 * JsRepresentClass_GeneratedJSFuntion3_m3038832458 (JsRepresentClass_t2913492551 * __this, String_t* ___funcitonName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Func`5<System.Object,System.Object,System.Object,System.Object,System.Object> Scripts.JSBindingClasses.JsRepresentClass::GeneratedJSFuntion4(System.String)
extern "C"  Func_5_t4203452149 * JsRepresentClass_GeneratedJSFuntion4_m3021805910 (JsRepresentClass_t2913492551 * __this, String_t* ___funcitonName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Scripts.JSBindingClasses.JsRepresentClass::Finalize()
extern "C"  void JsRepresentClass_Finalize_m620322798 (JsRepresentClass_t2913492551 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Scripts.JSBindingClasses.JsRepresentClass::get_jsobjID()
extern "C"  int32_t JsRepresentClass_get_jsobjID_m3190295872 (JsRepresentClass_t2913492551 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Scripts.JSBindingClasses.JsRepresentClass::ilo_setTraceS1(System.Int32,System.Boolean)
extern "C"  void JsRepresentClass_ilo_setTraceS1_m725456552 (Il2CppObject * __this /* static, unused */, int32_t ___id0, bool ___trace1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
