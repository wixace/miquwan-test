﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.Poly2Tri.DTSweepDebugContext
struct DTSweepDebugContext_t3829537966;
// Pathfinding.Poly2Tri.DelaunayTriangle
struct DelaunayTriangle_t2835103587;
// Pathfinding.Poly2Tri.TriangulationPoint
struct TriangulationPoint_t3810082933;
// Pathfinding.Poly2Tri.AdvancingFrontNode
struct AdvancingFrontNode_t688967424;
// Pathfinding.Poly2Tri.DTSweepConstraint
struct DTSweepConstraint_t3360279023;

#include "codegen/il2cpp-codegen.h"
#include "Pathfinding_Poly2Tri_Pathfinding_Poly2Tri_Delaunay2835103587.h"
#include "Pathfinding_Poly2Tri_Pathfinding_Poly2Tri_Triangul3810082933.h"
#include "Pathfinding_Poly2Tri_Pathfinding_Poly2Tri_Advancing688967424.h"
#include "Pathfinding_Poly2Tri_Pathfinding_Poly2Tri_DTSweepC3360279023.h"

// System.Void Pathfinding.Poly2Tri.DTSweepDebugContext::set_PrimaryTriangle(Pathfinding.Poly2Tri.DelaunayTriangle)
extern "C"  void DTSweepDebugContext_set_PrimaryTriangle_m527373270 (DTSweepDebugContext_t3829537966 * __this, DelaunayTriangle_t2835103587 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Poly2Tri.DTSweepDebugContext::set_SecondaryTriangle(Pathfinding.Poly2Tri.DelaunayTriangle)
extern "C"  void DTSweepDebugContext_set_SecondaryTriangle_m1903864548 (DTSweepDebugContext_t3829537966 * __this, DelaunayTriangle_t2835103587 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Poly2Tri.DTSweepDebugContext::set_ActivePoint(Pathfinding.Poly2Tri.TriangulationPoint)
extern "C"  void DTSweepDebugContext_set_ActivePoint_m1406306500 (DTSweepDebugContext_t3829537966 * __this, TriangulationPoint_t3810082933 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Poly2Tri.DTSweepDebugContext::set_ActiveNode(Pathfinding.Poly2Tri.AdvancingFrontNode)
extern "C"  void DTSweepDebugContext_set_ActiveNode_m1532614081 (DTSweepDebugContext_t3829537966 * __this, AdvancingFrontNode_t688967424 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Poly2Tri.DTSweepDebugContext::set_ActiveConstraint(Pathfinding.Poly2Tri.DTSweepConstraint)
extern "C"  void DTSweepDebugContext_set_ActiveConstraint_m915019815 (DTSweepDebugContext_t3829537966 * __this, DTSweepConstraint_t3360279023 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Poly2Tri.DTSweepDebugContext::Clear()
extern "C"  void DTSweepDebugContext_Clear_m592559566 (DTSweepDebugContext_t3829537966 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
