﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIButton
struct UIButton_t179429094;
// System.String
struct String_t;
// UnityEngine.Sprite
struct Sprite_t3199167241;
// UISprite
struct UISprite_t661437049;
// UIButtonColor
struct UIButtonColor_t1546108957;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "UnityEngine_UnityEngine_Sprite3199167241.h"
#include "AssemblyU2DCSharp_UIButtonColor_State1650987007.h"
#include "AssemblyU2DCSharp_UIButton179429094.h"
#include "AssemblyU2DCSharp_UISprite661437049.h"
#include "AssemblyU2DCSharp_UIButtonColor1546108957.h"

// System.Void UIButton::.ctor()
extern "C"  void UIButton__ctor_m2667856501 (UIButton_t179429094 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIButton::get_isEnabled()
extern "C"  bool UIButton_get_isEnabled_m2604211981 (UIButton_t179429094 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButton::set_isEnabled(System.Boolean)
extern "C"  void UIButton_set_isEnabled_m1635057924 (UIButton_t179429094 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UIButton::get_normalSprite()
extern "C"  String_t* UIButton_get_normalSprite_m529196615 (UIButton_t179429094 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButton::set_normalSprite(System.String)
extern "C"  void UIButton_set_normalSprite_m4235132394 (UIButton_t179429094 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Sprite UIButton::get_normalSprite2D()
extern "C"  Sprite_t3199167241 * UIButton_get_normalSprite2D_m2171207479 (UIButton_t179429094 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButton::set_normalSprite2D(UnityEngine.Sprite)
extern "C"  void UIButton_set_normalSprite2D_m332991150 (UIButton_t179429094 * __this, Sprite_t3199167241 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButton::OnInit()
extern "C"  void UIButton_OnInit_m2717772926 (UIButton_t179429094 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButton::OnEnable()
extern "C"  void UIButton_OnEnable_m1814787313 (UIButton_t179429094 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButton::OnDragOver()
extern "C"  void UIButton_OnDragOver_m1140410134 (UIButton_t179429094 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButton::OnDragOut()
extern "C"  void UIButton_OnDragOut_m729506414 (UIButton_t179429094 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButton::OnClick()
extern "C"  void UIButton_OnClick_m1558826940 (UIButton_t179429094 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButton::SetState(UIButtonColor/State,System.Boolean)
extern "C"  void UIButton_SetState_m2025920392 (UIButton_t179429094 * __this, int32_t ___state0, bool ___immediate1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButton::SetSprite(System.String)
extern "C"  void UIButton_SetSprite_m1031804584 (UIButton_t179429094 * __this, String_t* ___sp0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButton::SetSprite(UnityEngine.Sprite)
extern "C"  void UIButton_SetSprite_m2963638558 (UIButton_t179429094 * __this, Sprite_t3199167241 * ___sp0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButton::ilo_SetState1(UIButton,UIButtonColor/State,System.Boolean)
extern "C"  void UIButton_ilo_SetState1_m1048823604 (Il2CppObject * __this /* static, unused */, UIButton_t179429094 * ____this0, int32_t ___state1, bool ___immediate2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UIButton::ilo_get_spriteName2(UISprite)
extern "C"  String_t* UIButton_ilo_get_spriteName2_m1404336413 (Il2CppObject * __this /* static, unused */, UISprite_t661437049 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButton::ilo_SetSprite3(UIButton,System.String)
extern "C"  void UIButton_ilo_SetSprite3_m2009505732 (Il2CppObject * __this /* static, unused */, UIButton_t179429094 * ____this0, String_t* ___sp1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButton::ilo_OnInit4(UIButton)
extern "C"  void UIButton_ilo_OnInit4_m1581724767 (Il2CppObject * __this /* static, unused */, UIButton_t179429094 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButton::ilo_OnInit5(UIButtonColor)
extern "C"  void UIButton_ilo_OnInit5_m3899950255 (Il2CppObject * __this /* static, unused */, UIButtonColor_t1546108957 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIButton::ilo_get_isEnabled6(UIButton)
extern "C"  bool UIButton_ilo_get_isEnabled6_m1829705528 (Il2CppObject * __this /* static, unused */, UIButton_t179429094 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UIButton::ilo_get_selectedObject7()
extern "C"  GameObject_t3674682005 * UIButton_ilo_get_selectedObject7_m2486600159 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButton::ilo_SetSprite8(UIButton,UnityEngine.Sprite)
extern "C"  void UIButton_ilo_SetSprite8_m3979586461 (Il2CppObject * __this /* static, unused */, UIButton_t179429094 * ____this0, Sprite_t3199167241 * ___sp1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButton::ilo_set_spriteName9(UISprite,System.String)
extern "C"  void UIButton_ilo_set_spriteName9_m1888601839 (Il2CppObject * __this /* static, unused */, UISprite_t661437049 * ____this0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
