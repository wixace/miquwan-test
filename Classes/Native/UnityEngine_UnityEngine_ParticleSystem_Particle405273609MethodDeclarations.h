﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.ParticleSystem/Particle
struct Particle_t405273609;
struct Particle_t405273609_marshaled_pinvoke;
struct Particle_t405273609_marshaled_com;

#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct Particle_t405273609;
struct Particle_t405273609_marshaled_pinvoke;

extern "C" void Particle_t405273609_marshal_pinvoke(const Particle_t405273609& unmarshaled, Particle_t405273609_marshaled_pinvoke& marshaled);
extern "C" void Particle_t405273609_marshal_pinvoke_back(const Particle_t405273609_marshaled_pinvoke& marshaled, Particle_t405273609& unmarshaled);
extern "C" void Particle_t405273609_marshal_pinvoke_cleanup(Particle_t405273609_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct Particle_t405273609;
struct Particle_t405273609_marshaled_com;

extern "C" void Particle_t405273609_marshal_com(const Particle_t405273609& unmarshaled, Particle_t405273609_marshaled_com& marshaled);
extern "C" void Particle_t405273609_marshal_com_back(const Particle_t405273609_marshaled_com& marshaled, Particle_t405273609& unmarshaled);
extern "C" void Particle_t405273609_marshal_com_cleanup(Particle_t405273609_marshaled_com& marshaled);
