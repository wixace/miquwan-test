﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_JointAngleLimits2DGenerated
struct UnityEngine_JointAngleLimits2DGenerated_t3230863988;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void UnityEngine_JointAngleLimits2DGenerated::.ctor()
extern "C"  void UnityEngine_JointAngleLimits2DGenerated__ctor_m966524791 (UnityEngine_JointAngleLimits2DGenerated_t3230863988 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_JointAngleLimits2DGenerated::.cctor()
extern "C"  void UnityEngine_JointAngleLimits2DGenerated__cctor_m3710368534 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_JointAngleLimits2DGenerated::JointAngleLimits2D_JointAngleLimits2D1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_JointAngleLimits2DGenerated_JointAngleLimits2D_JointAngleLimits2D1_m3001346683 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_JointAngleLimits2DGenerated::JointAngleLimits2D_min(JSVCall)
extern "C"  void UnityEngine_JointAngleLimits2DGenerated_JointAngleLimits2D_min_m660231636 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_JointAngleLimits2DGenerated::JointAngleLimits2D_max(JSVCall)
extern "C"  void UnityEngine_JointAngleLimits2DGenerated_JointAngleLimits2D_max_m185805570 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_JointAngleLimits2DGenerated::__Register()
extern "C"  void UnityEngine_JointAngleLimits2DGenerated___Register_m3820851120 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_JointAngleLimits2DGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UnityEngine_JointAngleLimits2DGenerated_ilo_getObject1_m3157781791 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_JointAngleLimits2DGenerated::ilo_setSingle2(System.Int32,System.Single)
extern "C"  void UnityEngine_JointAngleLimits2DGenerated_ilo_setSingle2_m3037057566 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine_JointAngleLimits2DGenerated::ilo_getSingle3(System.Int32)
extern "C"  float UnityEngine_JointAngleLimits2DGenerated_ilo_getSingle3_m1340705146 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_JointAngleLimits2DGenerated::ilo_changeJSObj4(System.Int32,System.Object)
extern "C"  void UnityEngine_JointAngleLimits2DGenerated_ilo_changeJSObj4_m3467182549 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObjNew1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
