﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SharpKit.JavaScript.JsPropertyAttribute
struct JsPropertyAttribute_t1787918292;
// System.String
struct String_t;
// System.Type
struct Type_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Type2863145774.h"

// System.Void SharpKit.JavaScript.JsPropertyAttribute::set_NativeField(System.Boolean)
extern "C"  void JsPropertyAttribute_set_NativeField_m348301126 (JsPropertyAttribute_t1787918292 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SharpKit.JavaScript.JsPropertyAttribute::set_Export(System.Boolean)
extern "C"  void JsPropertyAttribute_set_Export_m4029745185 (JsPropertyAttribute_t1787918292 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SharpKit.JavaScript.JsPropertyAttribute::set_Name(System.String)
extern "C"  void JsPropertyAttribute_set_Name_m4012174881 (JsPropertyAttribute_t1787918292 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SharpKit.JavaScript.JsPropertyAttribute::set_TargetProperty(System.String)
extern "C"  void JsPropertyAttribute_set_TargetProperty_m326317830 (JsPropertyAttribute_t1787918292 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SharpKit.JavaScript.JsPropertyAttribute::set_TargetType(System.Type)
extern "C"  void JsPropertyAttribute_set_TargetType_m612794424 (JsPropertyAttribute_t1787918292 * __this, Type_t * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SharpKit.JavaScript.JsPropertyAttribute::.ctor()
extern "C"  void JsPropertyAttribute__ctor_m1982769067 (JsPropertyAttribute_t1787918292 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
