﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.StartEndModifier
struct StartEndModifier_t2926562694;
// Pathfinding.Path
struct Path_t1974241691;
// Pathfinding.GraphNode
struct GraphNode_t23612370;
// Pathfinding.IRaycastableGraph
struct IRaycastableGraph_t2032416694;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_ModifierData4046421655.h"
#include "AssemblyU2DCSharp_Pathfinding_Path1974241691.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphNode23612370.h"
#include "AssemblyU2DCSharp_Pathfinding_Int31974045594.h"
#include "AssemblyU2DCSharp_Pathfinding_StartEndModifier2926562694.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphHitInfo3082627369.h"

// System.Void Pathfinding.StartEndModifier::.ctor()
extern "C"  void StartEndModifier__ctor_m2776187809 (StartEndModifier_t2926562694 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.ModifierData Pathfinding.StartEndModifier::get_input()
extern "C"  int32_t StartEndModifier_get_input_m595556526 (StartEndModifier_t2926562694 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.ModifierData Pathfinding.StartEndModifier::get_output()
extern "C"  int32_t StartEndModifier_get_output_m881903551 (StartEndModifier_t2926562694 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.StartEndModifier::Apply(Pathfinding.Path,Pathfinding.ModifierData)
extern "C"  void StartEndModifier_Apply_m2773353757 (StartEndModifier_t2926562694 * __this, Path_t1974241691 * ____p0, int32_t ___source1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Pathfinding.StartEndModifier::GetClampedPoint(UnityEngine.Vector3,UnityEngine.Vector3,Pathfinding.GraphNode)
extern "C"  Vector3_t4282066566  StartEndModifier_GetClampedPoint_m1812232353 (StartEndModifier_t2926562694 * __this, Vector3_t4282066566  ___from0, Vector3_t4282066566  ___to1, GraphNode_t23612370 * ___hint2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Pathfinding.StartEndModifier::ilo_op_Explicit1(Pathfinding.Int3)
extern "C"  Vector3_t4282066566  StartEndModifier_ilo_op_Explicit1_m779731969 (Il2CppObject * __this /* static, unused */, Int3_t1974045594  ___ob0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Pathfinding.StartEndModifier::ilo_GetClampedPoint2(Pathfinding.StartEndModifier,UnityEngine.Vector3,UnityEngine.Vector3,Pathfinding.GraphNode)
extern "C"  Vector3_t4282066566  StartEndModifier_ilo_GetClampedPoint2_m2648536552 (Il2CppObject * __this /* static, unused */, StartEndModifier_t2926562694 * ____this0, Vector3_t4282066566  ___from1, Vector3_t4282066566  ___to2, GraphNode_t23612370 * ___hint3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Pathfinding.StartEndModifier::ilo_NearestPointStrict3(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  StartEndModifier_ilo_NearestPointStrict3_m2466274741 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___lineStart0, Vector3_t4282066566  ___lineEnd1, Vector3_t4282066566  ___point2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.StartEndModifier::ilo_Linecast4(Pathfinding.IRaycastableGraph,UnityEngine.Vector3,UnityEngine.Vector3,Pathfinding.GraphNode,Pathfinding.GraphHitInfo&)
extern "C"  bool StartEndModifier_ilo_Linecast4_m2999507070 (Il2CppObject * __this /* static, unused */, Il2CppObject * ____this0, Vector3_t4282066566  ___start1, Vector3_t4282066566  ___end2, GraphNode_t23612370 * ___hint3, GraphHitInfo_t3082627369 * ___hit4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
