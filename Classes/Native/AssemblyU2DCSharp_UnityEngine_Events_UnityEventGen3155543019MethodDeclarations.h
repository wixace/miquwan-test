﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_Events_UnityEventGenerated/<UnityEvent_AddListener_GetDelegate_member0_arg0>c__AnonStoreyE5
struct U3CUnityEvent_AddListener_GetDelegate_member0_arg0U3Ec__AnonStoreyE5_t3155543019;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine_Events_UnityEventGenerated/<UnityEvent_AddListener_GetDelegate_member0_arg0>c__AnonStoreyE5::.ctor()
extern "C"  void U3CUnityEvent_AddListener_GetDelegate_member0_arg0U3Ec__AnonStoreyE5__ctor_m2416493536 (U3CUnityEvent_AddListener_GetDelegate_member0_arg0U3Ec__AnonStoreyE5_t3155543019 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Events_UnityEventGenerated/<UnityEvent_AddListener_GetDelegate_member0_arg0>c__AnonStoreyE5::<>m__1B1()
extern "C"  void U3CUnityEvent_AddListener_GetDelegate_member0_arg0U3Ec__AnonStoreyE5_U3CU3Em__1B1_m347422841 (U3CUnityEvent_AddListener_GetDelegate_member0_arg0U3Ec__AnonStoreyE5_t3155543019 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
