﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BetterList`1/<GetEnumerator>c__Iterator29<UnityEngine.Color>
struct U3CGetEnumeratorU3Ec__Iterator29_t1646543366;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"

// System.Void BetterList`1/<GetEnumerator>c__Iterator29<UnityEngine.Color>::.ctor()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator29__ctor_m1738519765_gshared (U3CGetEnumeratorU3Ec__Iterator29_t1646543366 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator29__ctor_m1738519765(__this, method) ((  void (*) (U3CGetEnumeratorU3Ec__Iterator29_t1646543366 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator29__ctor_m1738519765_gshared)(__this, method)
// T BetterList`1/<GetEnumerator>c__Iterator29<UnityEngine.Color>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  Color_t4194546905  U3CGetEnumeratorU3Ec__Iterator29_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m2751036222_gshared (U3CGetEnumeratorU3Ec__Iterator29_t1646543366 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator29_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m2751036222(__this, method) ((  Color_t4194546905  (*) (U3CGetEnumeratorU3Ec__Iterator29_t1646543366 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator29_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m2751036222_gshared)(__this, method)
// System.Object BetterList`1/<GetEnumerator>c__Iterator29<UnityEngine.Color>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator29_System_Collections_IEnumerator_get_Current_m1136369723_gshared (U3CGetEnumeratorU3Ec__Iterator29_t1646543366 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator29_System_Collections_IEnumerator_get_Current_m1136369723(__this, method) ((  Il2CppObject * (*) (U3CGetEnumeratorU3Ec__Iterator29_t1646543366 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator29_System_Collections_IEnumerator_get_Current_m1136369723_gshared)(__this, method)
// System.Boolean BetterList`1/<GetEnumerator>c__Iterator29<UnityEngine.Color>::MoveNext()
extern "C"  bool U3CGetEnumeratorU3Ec__Iterator29_MoveNext_m653766375_gshared (U3CGetEnumeratorU3Ec__Iterator29_t1646543366 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator29_MoveNext_m653766375(__this, method) ((  bool (*) (U3CGetEnumeratorU3Ec__Iterator29_t1646543366 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator29_MoveNext_m653766375_gshared)(__this, method)
// System.Void BetterList`1/<GetEnumerator>c__Iterator29<UnityEngine.Color>::Dispose()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator29_Dispose_m4168466386_gshared (U3CGetEnumeratorU3Ec__Iterator29_t1646543366 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator29_Dispose_m4168466386(__this, method) ((  void (*) (U3CGetEnumeratorU3Ec__Iterator29_t1646543366 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator29_Dispose_m4168466386_gshared)(__this, method)
// System.Void BetterList`1/<GetEnumerator>c__Iterator29<UnityEngine.Color>::Reset()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator29_Reset_m3679920002_gshared (U3CGetEnumeratorU3Ec__Iterator29_t1646543366 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator29_Reset_m3679920002(__this, method) ((  void (*) (U3CGetEnumeratorU3Ec__Iterator29_t1646543366 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator29_Reset_m3679920002_gshared)(__this, method)
