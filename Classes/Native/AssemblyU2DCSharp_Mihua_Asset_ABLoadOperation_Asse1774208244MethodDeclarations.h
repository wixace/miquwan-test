﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mihua.Asset.ABLoadOperation.AssetOperationPreload
struct AssetOperationPreload_t1774208244;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1375417109;
// Mihua.Asset.ABLoadOperation.AssetOperationPreload1
struct AssetOperationPreload1_t2389844357;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Mihua_Asset_ABLoadOperation_Asse2389844357.h"
#include "mscorlib_System_String7231557.h"

// System.Void Mihua.Asset.ABLoadOperation.AssetOperationPreload::.ctor(System.Collections.Generic.List`1<System.String>,System.Collections.Generic.List`1<System.String>)
extern "C"  void AssetOperationPreload__ctor_m1743763915 (AssetOperationPreload_t1774208244 * __this, List_1_t1375417109 * ____preloadList0, List_1_t1375417109 * ____objPreLoadList1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mihua.Asset.ABLoadOperation.AssetOperationPreload::IsDone()
extern "C"  bool AssetOperationPreload_IsDone_m1717866721 (AssetOperationPreload_t1774208244 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Mihua.Asset.ABLoadOperation.AssetOperationPreload::get_progress()
extern "C"  float AssetOperationPreload_get_progress_m1793006931 (AssetOperationPreload_t1774208244 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua.Asset.ABLoadOperation.AssetOperationPreload::Clear()
extern "C"  void AssetOperationPreload_Clear_m247297222 (AssetOperationPreload_t1774208244 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mihua.Asset.ABLoadOperation.AssetOperationPreload::ilo_get_unloading1()
extern "C"  bool AssetOperationPreload_ilo_get_unloading1_m1559335167 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua.Asset.ABLoadOperation.AssetOperationPreload::ilo_CacheAsset2(Mihua.Asset.ABLoadOperation.AssetOperationPreload1)
extern "C"  void AssetOperationPreload_ilo_CacheAsset2_m1532065609 (Il2CppObject * __this /* static, unused */, AssetOperationPreload1_t2389844357 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua.Asset.ABLoadOperation.AssetOperationPreload::ilo_Clear3(Mihua.Asset.ABLoadOperation.AssetOperationPreload1)
extern "C"  void AssetOperationPreload_ilo_Clear3_m4190657793 (Il2CppObject * __this /* static, unused */, AssetOperationPreload1_t2389844357 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua.Asset.ABLoadOperation.AssetOperationPreload::ilo_LoadAssetBundles4(System.String)
extern "C"  void AssetOperationPreload_ilo_LoadAssetBundles4_m2941115023 (Il2CppObject * __this /* static, unused */, String_t* ___assetBundleName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
