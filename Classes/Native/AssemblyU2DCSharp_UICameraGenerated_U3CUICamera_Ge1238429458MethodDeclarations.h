﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UICameraGenerated/<UICamera_GetKeyUp_GetDelegate_member2_arg0>c__AnonStorey9E
struct U3CUICamera_GetKeyUp_GetDelegate_member2_arg0U3Ec__AnonStorey9E_t1238429458;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_KeyCode3128317986.h"

// System.Void UICameraGenerated/<UICamera_GetKeyUp_GetDelegate_member2_arg0>c__AnonStorey9E::.ctor()
extern "C"  void U3CUICamera_GetKeyUp_GetDelegate_member2_arg0U3Ec__AnonStorey9E__ctor_m1869940377 (U3CUICamera_GetKeyUp_GetDelegate_member2_arg0U3Ec__AnonStorey9E_t1238429458 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UICameraGenerated/<UICamera_GetKeyUp_GetDelegate_member2_arg0>c__AnonStorey9E::<>m__100(UnityEngine.KeyCode)
extern "C"  bool U3CUICamera_GetKeyUp_GetDelegate_member2_arg0U3Ec__AnonStorey9E_U3CU3Em__100_m1958041120 (U3CUICamera_GetKeyUp_GetDelegate_member2_arg0U3Ec__AnonStorey9E_t1238429458 * __this, int32_t ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
