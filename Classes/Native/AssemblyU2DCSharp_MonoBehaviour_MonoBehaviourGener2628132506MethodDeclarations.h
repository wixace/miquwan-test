﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MonoBehaviour_MonoBehaviourGenerated
struct MonoBehaviour_MonoBehaviourGenerated_t2628132506;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Type
struct Type_t;
// MethodID
struct MethodID_t3916401116;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Type2863145774.h"
#include "AssemblyU2DCSharp_MethodID3916401116.h"

// System.Void MonoBehaviour_MonoBehaviourGenerated::.ctor()
extern "C"  void MonoBehaviour_MonoBehaviourGenerated__ctor_m2998589761 (MonoBehaviour_MonoBehaviourGenerated_t2628132506 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MonoBehaviour_MonoBehaviourGenerated::.cctor()
extern "C"  void MonoBehaviour_MonoBehaviourGenerated__cctor_m2279873164 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MonoBehaviour_MonoBehaviourGenerated::MonoBehaviour_MonoBehaviour_AddComponentT1__MonoBehaviour(JSVCall,System.Int32)
extern "C"  bool MonoBehaviour_MonoBehaviourGenerated_MonoBehaviour_MonoBehaviour_AddComponentT1__MonoBehaviour_m2186483186 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MonoBehaviour_MonoBehaviourGenerated::MonoBehaviour_MonoBehaviour_GetComponentT1__MonoBehaviour(JSVCall,System.Int32)
extern "C"  bool MonoBehaviour_MonoBehaviourGenerated_MonoBehaviour_MonoBehaviour_GetComponentT1__MonoBehaviour_m31002279 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MonoBehaviour_MonoBehaviourGenerated::__Register()
extern "C"  void MonoBehaviour_MonoBehaviourGenerated___Register_m296829990 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MonoBehaviour_MonoBehaviourGenerated::ilo_getObject1(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * MonoBehaviour_MonoBehaviourGenerated_ilo_getObject1_m459860238 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MonoBehaviour_MonoBehaviourGenerated::ilo_setWhatever2(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  void MonoBehaviour_MonoBehaviourGenerated_ilo_setWhatever2_m1520279257 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___obj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo MonoBehaviour_MonoBehaviourGenerated::ilo_makeGenericMethod3(System.Type,MethodID,System.Int32)
extern "C"  MethodInfo_t * MonoBehaviour_MonoBehaviourGenerated_ilo_makeGenericMethod3_m1824934973 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, MethodID_t3916401116 * ___methodID1, int32_t ___TCount2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
