﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<CurCameraShotCfg>
struct List_1_t1593047221;
// ProtoBuf.IExtension
struct IExtension_t1606339106;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraShotsCfg
struct  CameraShotsCfg_t1740446544  : public Il2CppObject
{
public:
	// System.Int32 CameraShotsCfg::_id
	int32_t ____id_0;
	// System.Single CameraShotsCfg::_waitTime
	float ____waitTime_1;
	// System.Collections.Generic.List`1<CurCameraShotCfg> CameraShotsCfg::_curCameraShot
	List_1_t1593047221 * ____curCameraShot_2;
	// ProtoBuf.IExtension CameraShotsCfg::extensionObject
	Il2CppObject * ___extensionObject_3;

public:
	inline static int32_t get_offset_of__id_0() { return static_cast<int32_t>(offsetof(CameraShotsCfg_t1740446544, ____id_0)); }
	inline int32_t get__id_0() const { return ____id_0; }
	inline int32_t* get_address_of__id_0() { return &____id_0; }
	inline void set__id_0(int32_t value)
	{
		____id_0 = value;
	}

	inline static int32_t get_offset_of__waitTime_1() { return static_cast<int32_t>(offsetof(CameraShotsCfg_t1740446544, ____waitTime_1)); }
	inline float get__waitTime_1() const { return ____waitTime_1; }
	inline float* get_address_of__waitTime_1() { return &____waitTime_1; }
	inline void set__waitTime_1(float value)
	{
		____waitTime_1 = value;
	}

	inline static int32_t get_offset_of__curCameraShot_2() { return static_cast<int32_t>(offsetof(CameraShotsCfg_t1740446544, ____curCameraShot_2)); }
	inline List_1_t1593047221 * get__curCameraShot_2() const { return ____curCameraShot_2; }
	inline List_1_t1593047221 ** get_address_of__curCameraShot_2() { return &____curCameraShot_2; }
	inline void set__curCameraShot_2(List_1_t1593047221 * value)
	{
		____curCameraShot_2 = value;
		Il2CppCodeGenWriteBarrier(&____curCameraShot_2, value);
	}

	inline static int32_t get_offset_of_extensionObject_3() { return static_cast<int32_t>(offsetof(CameraShotsCfg_t1740446544, ___extensionObject_3)); }
	inline Il2CppObject * get_extensionObject_3() const { return ___extensionObject_3; }
	inline Il2CppObject ** get_address_of_extensionObject_3() { return &___extensionObject_3; }
	inline void set_extensionObject_3(Il2CppObject * value)
	{
		___extensionObject_3 = value;
		Il2CppCodeGenWriteBarrier(&___extensionObject_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
