﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Pathfinding.Poly2Tri.TriangulationPoint
struct TriangulationPoint_t3810082933;

#include "mscorlib_System_NotImplementedException1912495542.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.Poly2Tri.PointOnEdgeException
struct  PointOnEdgeException_t989229367  : public NotImplementedException_t1912495542
{
public:
	// Pathfinding.Poly2Tri.TriangulationPoint Pathfinding.Poly2Tri.PointOnEdgeException::A
	TriangulationPoint_t3810082933 * ___A_11;
	// Pathfinding.Poly2Tri.TriangulationPoint Pathfinding.Poly2Tri.PointOnEdgeException::B
	TriangulationPoint_t3810082933 * ___B_12;
	// Pathfinding.Poly2Tri.TriangulationPoint Pathfinding.Poly2Tri.PointOnEdgeException::C
	TriangulationPoint_t3810082933 * ___C_13;

public:
	inline static int32_t get_offset_of_A_11() { return static_cast<int32_t>(offsetof(PointOnEdgeException_t989229367, ___A_11)); }
	inline TriangulationPoint_t3810082933 * get_A_11() const { return ___A_11; }
	inline TriangulationPoint_t3810082933 ** get_address_of_A_11() { return &___A_11; }
	inline void set_A_11(TriangulationPoint_t3810082933 * value)
	{
		___A_11 = value;
		Il2CppCodeGenWriteBarrier(&___A_11, value);
	}

	inline static int32_t get_offset_of_B_12() { return static_cast<int32_t>(offsetof(PointOnEdgeException_t989229367, ___B_12)); }
	inline TriangulationPoint_t3810082933 * get_B_12() const { return ___B_12; }
	inline TriangulationPoint_t3810082933 ** get_address_of_B_12() { return &___B_12; }
	inline void set_B_12(TriangulationPoint_t3810082933 * value)
	{
		___B_12 = value;
		Il2CppCodeGenWriteBarrier(&___B_12, value);
	}

	inline static int32_t get_offset_of_C_13() { return static_cast<int32_t>(offsetof(PointOnEdgeException_t989229367, ___C_13)); }
	inline TriangulationPoint_t3810082933 * get_C_13() const { return ___C_13; }
	inline TriangulationPoint_t3810082933 ** get_address_of_C_13() { return &___C_13; }
	inline void set_C_13(TriangulationPoint_t3810082933 * value)
	{
		___C_13 = value;
		Il2CppCodeGenWriteBarrier(&___C_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
