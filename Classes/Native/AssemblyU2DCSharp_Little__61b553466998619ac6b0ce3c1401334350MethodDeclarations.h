﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._61b553466998619ac6b0ce3c9cc8466d
struct _61b553466998619ac6b0ce3c9cc8466d_t1401334350;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._61b553466998619ac6b0ce3c9cc8466d::.ctor()
extern "C"  void _61b553466998619ac6b0ce3c9cc8466d__ctor_m847000479 (_61b553466998619ac6b0ce3c9cc8466d_t1401334350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._61b553466998619ac6b0ce3c9cc8466d::_61b553466998619ac6b0ce3c9cc8466dm2(System.Int32)
extern "C"  int32_t _61b553466998619ac6b0ce3c9cc8466d__61b553466998619ac6b0ce3c9cc8466dm2_m4201610969 (_61b553466998619ac6b0ce3c9cc8466d_t1401334350 * __this, int32_t ____61b553466998619ac6b0ce3c9cc8466da0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._61b553466998619ac6b0ce3c9cc8466d::_61b553466998619ac6b0ce3c9cc8466dm(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _61b553466998619ac6b0ce3c9cc8466d__61b553466998619ac6b0ce3c9cc8466dm_m479105021 (_61b553466998619ac6b0ce3c9cc8466d_t1401334350 * __this, int32_t ____61b553466998619ac6b0ce3c9cc8466da0, int32_t ____61b553466998619ac6b0ce3c9cc8466d71, int32_t ____61b553466998619ac6b0ce3c9cc8466dc2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
