﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "AssemblyU2DCSharp_CsCfgBase69924517.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// all_configCfg
struct  all_configCfg_t3865068708  : public CsCfgBase_t69924517
{
public:
	// System.Int32 all_configCfg::id
	int32_t ___id_0;
	// System.String all_configCfg::att_key
	String_t* ___att_key_1;
	// System.Int32 all_configCfg::att_type
	int32_t ___att_type_2;
	// System.Int32 all_configCfg::int_value
	int32_t ___int_value_3;
	// System.String all_configCfg::string_value
	String_t* ___string_value_4;
	// System.String all_configCfg::att_des
	String_t* ___att_des_5;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(all_configCfg_t3865068708, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_att_key_1() { return static_cast<int32_t>(offsetof(all_configCfg_t3865068708, ___att_key_1)); }
	inline String_t* get_att_key_1() const { return ___att_key_1; }
	inline String_t** get_address_of_att_key_1() { return &___att_key_1; }
	inline void set_att_key_1(String_t* value)
	{
		___att_key_1 = value;
		Il2CppCodeGenWriteBarrier(&___att_key_1, value);
	}

	inline static int32_t get_offset_of_att_type_2() { return static_cast<int32_t>(offsetof(all_configCfg_t3865068708, ___att_type_2)); }
	inline int32_t get_att_type_2() const { return ___att_type_2; }
	inline int32_t* get_address_of_att_type_2() { return &___att_type_2; }
	inline void set_att_type_2(int32_t value)
	{
		___att_type_2 = value;
	}

	inline static int32_t get_offset_of_int_value_3() { return static_cast<int32_t>(offsetof(all_configCfg_t3865068708, ___int_value_3)); }
	inline int32_t get_int_value_3() const { return ___int_value_3; }
	inline int32_t* get_address_of_int_value_3() { return &___int_value_3; }
	inline void set_int_value_3(int32_t value)
	{
		___int_value_3 = value;
	}

	inline static int32_t get_offset_of_string_value_4() { return static_cast<int32_t>(offsetof(all_configCfg_t3865068708, ___string_value_4)); }
	inline String_t* get_string_value_4() const { return ___string_value_4; }
	inline String_t** get_address_of_string_value_4() { return &___string_value_4; }
	inline void set_string_value_4(String_t* value)
	{
		___string_value_4 = value;
		Il2CppCodeGenWriteBarrier(&___string_value_4, value);
	}

	inline static int32_t get_offset_of_att_des_5() { return static_cast<int32_t>(offsetof(all_configCfg_t3865068708, ___att_des_5)); }
	inline String_t* get_att_des_5() const { return ___att_des_5; }
	inline String_t** get_address_of_att_des_5() { return &___att_des_5; }
	inline void set_att_des_5(String_t* value)
	{
		___att_des_5 = value;
		Il2CppCodeGenWriteBarrier(&___att_des_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
