﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_CrashReportGenerated
struct UnityEngine_CrashReportGenerated_t484504956;
// JSVCall
struct JSVCall_t3708497963;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void UnityEngine_CrashReportGenerated::.ctor()
extern "C"  void UnityEngine_CrashReportGenerated__ctor_m926641311 (UnityEngine_CrashReportGenerated_t484504956 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CrashReportGenerated::CrashReport_time(JSVCall)
extern "C"  void UnityEngine_CrashReportGenerated_CrashReport_time_m1833861521 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CrashReportGenerated::CrashReport_text(JSVCall)
extern "C"  void UnityEngine_CrashReportGenerated_CrashReport_text_m78199601 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CrashReportGenerated::CrashReport_reports(JSVCall)
extern "C"  void UnityEngine_CrashReportGenerated_CrashReport_reports_m1168050031 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CrashReportGenerated::CrashReport_lastReport(JSVCall)
extern "C"  void UnityEngine_CrashReportGenerated_CrashReport_lastReport_m571088788 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_CrashReportGenerated::CrashReport_Remove(JSVCall,System.Int32)
extern "C"  bool UnityEngine_CrashReportGenerated_CrashReport_Remove_m3782926641 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_CrashReportGenerated::CrashReport_RemoveAll(JSVCall,System.Int32)
extern "C"  bool UnityEngine_CrashReportGenerated_CrashReport_RemoveAll_m2139510706 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CrashReportGenerated::__Register()
extern "C"  void UnityEngine_CrashReportGenerated___Register_m3590069128 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_CrashReportGenerated::ilo_setObject1(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UnityEngine_CrashReportGenerated_ilo_setObject1_m4155166539 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CrashReportGenerated::ilo_setArrayS2(System.Int32,System.Int32,System.Boolean)
extern "C"  void UnityEngine_CrashReportGenerated_ilo_setArrayS2_m2109178117 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___count1, bool ___bClear2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
