﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIRect
struct UIRect_t2503437976;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// UnityEngine.Transform
struct Transform_t1659122786;
// UnityEngine.Camera
struct Camera_t2727095145;
// UIRoot
struct UIRoot_t2503447958;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t215400611;
// UIRect/AnchorPoint
struct AnchorPoint_t3581172420;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Transform1659122786.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_UIRect_AnchorPoint3581172420.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "AssemblyU2DCSharp_UIRect2503437976.h"
#include "UnityEngine_UnityEngine_Camera2727095145.h"

// System.Void UIRect::.ctor()
extern "C"  void UIRect__ctor_m1059512579 (UIRect_t2503437976 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIRect::.cctor()
extern "C"  void UIRect__cctor_m2298022666 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UIRect::get_cachedGameObject()
extern "C"  GameObject_t3674682005 * UIRect_get_cachedGameObject_m418520850 (UIRect_t2503437976 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UIRect::get_cachedTransform()
extern "C"  Transform_t1659122786 * UIRect_get_cachedTransform_m1757861860 (UIRect_t2503437976 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Camera UIRect::get_anchorCamera()
extern "C"  Camera_t2727095145 * UIRect_get_anchorCamera_m1098050341 (UIRect_t2503437976 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIRect::get_isFullyAnchored()
extern "C"  bool UIRect_get_isFullyAnchored_m227218776 (UIRect_t2503437976 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIRect::get_isAnchoredHorizontally()
extern "C"  bool UIRect_get_isAnchoredHorizontally_m2439216813 (UIRect_t2503437976 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIRect::get_isAnchoredVertically()
extern "C"  bool UIRect_get_isAnchoredVertically_m527266751 (UIRect_t2503437976 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIRect::get_canBeAnchored()
extern "C"  bool UIRect_get_canBeAnchored_m1850136011 (UIRect_t2503437976 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIRect UIRect::get_parent()
extern "C"  UIRect_t2503437976 * UIRect_get_parent_m2985047705 (UIRect_t2503437976 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIRoot UIRect::get_root()
extern "C"  UIRoot_t2503447958 * UIRect_get_root_m512643219 (UIRect_t2503437976 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIRect::get_isAnchored()
extern "C"  bool UIRect_get_isAnchored_m3752430236 (UIRect_t2503437976 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UIRect::get_cameraRayDistance()
extern "C"  float UIRect_get_cameraRayDistance_m2332969190 (UIRect_t2503437976 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIRect::Invalidate(System.Boolean)
extern "C"  void UIRect_Invalidate_m3602426579 (UIRect_t2503437976 * __this, bool ___includeChildren0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3[] UIRect::GetSides(UnityEngine.Transform)
extern "C"  Vector3U5BU5D_t215400611* UIRect_GetSides_m1517056542 (UIRect_t2503437976 * __this, Transform_t1659122786 * ___relativeTo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UIRect::GetLocalPos(UIRect/AnchorPoint,UnityEngine.Transform)
extern "C"  Vector3_t4282066566  UIRect_GetLocalPos_m763968249 (UIRect_t2503437976 * __this, AnchorPoint_t3581172420 * ___ac0, Transform_t1659122786 * ___trans1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIRect::OnEnable()
extern "C"  void UIRect_OnEnable_m2296161187 (UIRect_t2503437976 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIRect::OnInit()
extern "C"  void UIRect_OnInit_m103751600 (UIRect_t2503437976 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIRect::OnDisable()
extern "C"  void UIRect_OnDisable_m2902457322 (UIRect_t2503437976 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIRect::Start()
extern "C"  void UIRect_Start_m6650371 (UIRect_t2503437976 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIRect::Update()
extern "C"  void UIRect_Update_m212013674 (UIRect_t2503437976 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIRect::UpdateAnchors()
extern "C"  void UIRect_UpdateAnchors_m2241057846 (UIRect_t2503437976 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIRect::SetAnchor(UnityEngine.Transform)
extern "C"  void UIRect_SetAnchor_m1873301541 (UIRect_t2503437976 * __this, Transform_t1659122786 * ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIRect::SetAnchor(UnityEngine.GameObject)
extern "C"  void UIRect_SetAnchor_m2444135312 (UIRect_t2503437976 * __this, GameObject_t3674682005 * ___go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIRect::SetAnchor(UnityEngine.GameObject,System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  void UIRect_SetAnchor_m4102854224 (UIRect_t2503437976 * __this, GameObject_t3674682005 * ___go0, int32_t ___left1, int32_t ___bottom2, int32_t ___right3, int32_t ___top4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIRect::ResetAnchors()
extern "C"  void UIRect_ResetAnchors_m25739312 (UIRect_t2503437976 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIRect::ResetAndUpdateAnchors()
extern "C"  void UIRect_ResetAndUpdateAnchors_m2258824462 (UIRect_t2503437976 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIRect::FindCameraFor(UIRect/AnchorPoint)
extern "C"  void UIRect_FindCameraFor_m4212580360 (UIRect_t2503437976 * __this, AnchorPoint_t3581172420 * ___ap0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIRect::ParentHasChanged()
extern "C"  void UIRect_ParentHasChanged_m2258951653 (UIRect_t2503437976 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIRect::OnUpdate()
extern "C"  void UIRect_OnUpdate_m1976565097 (UIRect_t2503437976 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIRect::ilo_ResetAnchors1(UIRect)
extern "C"  void UIRect_ilo_ResetAnchors1_m4258248760 (Il2CppObject * __this /* static, unused */, UIRect_t2503437976 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UIRect::ilo_get_cachedTransform2(UIRect)
extern "C"  Transform_t1659122786 * UIRect_ilo_get_cachedTransform2_m1780511243 (Il2CppObject * __this /* static, unused */, UIRect_t2503437976 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3[] UIRect::ilo_GetSides3(UnityEngine.Camera,System.Single,UnityEngine.Transform)
extern "C"  Vector3U5BU5D_t215400611* UIRect_ilo_GetSides3_m4085252259 (Il2CppObject * __this /* static, unused */, Camera_t2727095145 * ___cam0, float ___depth1, Transform_t1659122786 * ___relativeTo2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIRect::ilo_OnStart4(UIRect)
extern "C"  void UIRect_ilo_OnStart4_m2403655757 (Il2CppObject * __this /* static, unused */, UIRect_t2503437976 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIRect::ilo_Update5(UIRect)
extern "C"  void UIRect_ilo_Update5_m1611633282 (Il2CppObject * __this /* static, unused */, UIRect_t2503437976 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIRect::ilo_OnAnchor6(UIRect)
extern "C"  void UIRect_ilo_OnAnchor6_m3517694456 (Il2CppObject * __this /* static, unused */, UIRect_t2503437976 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIRect::ilo_OnUpdate7(UIRect)
extern "C"  void UIRect_ilo_OnUpdate7_m1748259173 (Il2CppObject * __this /* static, unused */, UIRect_t2503437976 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIRect::ilo_FindCameraFor8(UIRect,UIRect/AnchorPoint)
extern "C"  void UIRect_ilo_FindCameraFor8_m281903929 (Il2CppObject * __this /* static, unused */, UIRect_t2503437976 * ____this0, AnchorPoint_t3581172420 * ___ap1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIRect::ilo_UpdateAnchors9(UIRect)
extern "C"  void UIRect_ilo_UpdateAnchors9_m4281909184 (Il2CppObject * __this /* static, unused */, UIRect_t2503437976 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Camera UIRect::ilo_FindCameraForLayer10(System.Int32)
extern "C"  Camera_t2727095145 * UIRect_ilo_FindCameraForLayer10_m3777807499 (Il2CppObject * __this /* static, unused */, int32_t ___layer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
