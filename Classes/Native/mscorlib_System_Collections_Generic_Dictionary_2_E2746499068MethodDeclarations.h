﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3363211663MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,JSMgr/JS_CS_Rel>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m3919994979(__this, ___dictionary0, method) ((  void (*) (Enumerator_t2746499068 *, Dictionary_2_t1429175676 *, const MethodInfo*))Enumerator__ctor_m3920831137_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,JSMgr/JS_CS_Rel>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3480746462(__this, method) ((  Il2CppObject * (*) (Enumerator_t2746499068 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3262087712_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,JSMgr/JS_CS_Rel>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2956482674(__this, method) ((  void (*) (Enumerator_t2746499068 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2959141748_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,JSMgr/JS_CS_Rel>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m491515195(__this, method) ((  DictionaryEntry_t1751606614  (*) (Enumerator_t2746499068 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2279524093_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,JSMgr/JS_CS_Rel>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3071268922(__this, method) ((  Il2CppObject * (*) (Enumerator_t2746499068 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1201448700_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,JSMgr/JS_CS_Rel>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1895338060(__this, method) ((  Il2CppObject * (*) (Enumerator_t2746499068 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m294434446_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,JSMgr/JS_CS_Rel>::MoveNext()
#define Enumerator_MoveNext_m291464414(__this, method) ((  bool (*) (Enumerator_t2746499068 *, const MethodInfo*))Enumerator_MoveNext_m217327200_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,JSMgr/JS_CS_Rel>::get_Current()
#define Enumerator_get_Current_m4093721106(__this, method) ((  KeyValuePair_2_t1327956382  (*) (Enumerator_t2746499068 *, const MethodInfo*))Enumerator_get_Current_m4240003024_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,JSMgr/JS_CS_Rel>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m1547512235(__this, method) ((  Il2CppObject * (*) (Enumerator_t2746499068 *, const MethodInfo*))Enumerator_get_CurrentKey_m3062159917_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,JSMgr/JS_CS_Rel>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m3040760591(__this, method) ((  JS_CS_Rel_t3554103776 * (*) (Enumerator_t2746499068 *, const MethodInfo*))Enumerator_get_CurrentValue_m592783249_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,JSMgr/JS_CS_Rel>::Reset()
#define Enumerator_Reset_m3588691509(__this, method) ((  void (*) (Enumerator_t2746499068 *, const MethodInfo*))Enumerator_Reset_m3001375603_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,JSMgr/JS_CS_Rel>::VerifyState()
#define Enumerator_VerifyState_m2071734910(__this, method) ((  void (*) (Enumerator_t2746499068 *, const MethodInfo*))Enumerator_VerifyState_m4290054460_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,JSMgr/JS_CS_Rel>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m817294950(__this, method) ((  void (*) (Enumerator_t2746499068 *, const MethodInfo*))Enumerator_VerifyCurrent_m2318603684_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,JSMgr/JS_CS_Rel>::Dispose()
#define Enumerator_Dispose_m2397230533(__this, method) ((  void (*) (Enumerator_t2746499068 *, const MethodInfo*))Enumerator_Dispose_m627360643_gshared)(__this, method)
