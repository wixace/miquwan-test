﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MScrollViewGenerated
struct MScrollViewGenerated_t1730636048;
// JSVCall
struct JSVCall_t3708497963;
// MScrollView/OnMoveCallBackFun
struct OnMoveCallBackFun_t3535987578;
// CSRepresentedObject
struct CSRepresentedObject_t3994124630;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// MScrollView
struct MScrollView_t1133512511;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_CSRepresentedObject3994124630.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "AssemblyU2DCSharp_MScrollView1133512511.h"
#include "AssemblyU2DCSharp_MScrollView_MoveWay3474941550.h"
#include "AssemblyU2DCSharp_MScrollView_OnMoveCallBackFun3535987578.h"

// System.Void MScrollViewGenerated::.ctor()
extern "C"  void MScrollViewGenerated__ctor_m803087243 (MScrollViewGenerated_t1730636048 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MScrollViewGenerated::MScrollView_MScrollView1(JSVCall,System.Int32)
extern "C"  bool MScrollViewGenerated_MScrollView_MScrollView1_m63801679 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MScrollViewGenerated::MScrollView_DragTarge(JSVCall)
extern "C"  void MScrollViewGenerated_MScrollView_DragTarge_m256094287 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MScrollViewGenerated::MScrollView_FingerDragDistance(JSVCall)
extern "C"  void MScrollViewGenerated_MScrollView_FingerDragDistance_m106451228 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MScrollViewGenerated::MScrollView_MoveDistanceUnit(JSVCall)
extern "C"  void MScrollViewGenerated_MScrollView_MoveDistanceUnit_m471617508 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MScrollViewGenerated::MScrollView_MoveSumTime(JSVCall)
extern "C"  void MScrollViewGenerated_MScrollView_MoveSumTime_m90680599 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MScrollViewGenerated::MScrollView_Index(JSVCall)
extern "C"  void MScrollViewGenerated_MScrollView_Index_m2360831308 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MScrollViewGenerated::MScrollView_Size(JSVCall)
extern "C"  void MScrollViewGenerated_MScrollView_Size_m3169859405 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MScrollViewGenerated::MScrollView_MoveLeft(JSVCall,System.Int32)
extern "C"  bool MScrollViewGenerated_MScrollView_MoveLeft_m1244817813 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MScrollViewGenerated::MScrollView_MoveRight(JSVCall,System.Int32)
extern "C"  bool MScrollViewGenerated_MScrollView_MoveRight_m2118751760 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MScrollViewGenerated::MScrollView_OnDrag__Vector2(JSVCall,System.Int32)
extern "C"  bool MScrollViewGenerated_MScrollView_OnDrag__Vector2_m3686136961 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MScrollViewGenerated::MScrollView_OnPress(JSVCall,System.Int32)
extern "C"  bool MScrollViewGenerated_MScrollView_OnPress_m2088219433 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MScrollView/OnMoveCallBackFun MScrollViewGenerated::MScrollView_Register_GetDelegate_member4_arg1(CSRepresentedObject)
extern "C"  OnMoveCallBackFun_t3535987578 * MScrollViewGenerated_MScrollView_Register_GetDelegate_member4_arg1_m4080446412 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MScrollViewGenerated::MScrollView_Register__MoveWay__OnMoveCallBackFun(JSVCall,System.Int32)
extern "C"  bool MScrollViewGenerated_MScrollView_Register__MoveWay__OnMoveCallBackFun_m3352809804 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MScrollViewGenerated::MScrollView_Reste(JSVCall,System.Int32)
extern "C"  bool MScrollViewGenerated_MScrollView_Reste_m1049306582 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MScrollViewGenerated::MScrollView_SetTargePosition__Int32__Boolean(JSVCall,System.Int32)
extern "C"  bool MScrollViewGenerated_MScrollView_SetTargePosition__Int32__Boolean_m2651732673 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MScrollViewGenerated::MScrollView_unRegister__MoveWay(JSVCall,System.Int32)
extern "C"  bool MScrollViewGenerated_MScrollView_unRegister__MoveWay_m4186163623 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MScrollViewGenerated::__Register()
extern "C"  void MScrollViewGenerated___Register_m391305244 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MScrollView/OnMoveCallBackFun MScrollViewGenerated::<MScrollView_Register__MoveWay__OnMoveCallBackFun>m__75()
extern "C"  OnMoveCallBackFun_t3535987578 * MScrollViewGenerated_U3CMScrollView_Register__MoveWay__OnMoveCallBackFunU3Em__75_m3919875528 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 MScrollViewGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t MScrollViewGenerated_ilo_getObject1_m767670631 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MScrollViewGenerated::ilo_getObject2(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * MScrollViewGenerated_ilo_getObject2_m582927749 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single MScrollViewGenerated::ilo_getSingle3(System.Int32)
extern "C"  float MScrollViewGenerated_ilo_getSingle3_m2279926014 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MScrollViewGenerated::ilo_setSingle4(System.Int32,System.Single)
extern "C"  void MScrollViewGenerated_ilo_setSingle4_m3540082316 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MScrollViewGenerated::ilo_setInt325(System.Int32,System.Int32)
extern "C"  void MScrollViewGenerated_ilo_setInt325_m1256549159 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 MScrollViewGenerated::ilo_getInt326(System.Int32)
extern "C"  int32_t MScrollViewGenerated_ilo_getInt326_m1313414807 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MScrollViewGenerated::ilo_OnPress7(MScrollView)
extern "C"  void MScrollViewGenerated_ilo_OnPress7_m2634639892 (Il2CppObject * __this /* static, unused */, MScrollView_t1133512511 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MScrollViewGenerated::ilo_Register8(MScrollView,MScrollView/MoveWay,MScrollView/OnMoveCallBackFun)
extern "C"  void MScrollViewGenerated_ilo_Register8_m3065464984 (Il2CppObject * __this /* static, unused */, MScrollView_t1133512511 * ____this0, int32_t ___key1, OnMoveCallBackFun_t3535987578 * ___call2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MScrollViewGenerated::ilo_Reste9(MScrollView)
extern "C"  void MScrollViewGenerated_ilo_Reste9_m2919330111 (Il2CppObject * __this /* static, unused */, MScrollView_t1133512511 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MScrollViewGenerated::ilo_SetTargePosition10(MScrollView,System.Int32,System.Boolean)
extern "C"  void MScrollViewGenerated_ilo_SetTargePosition10_m2034118532 (Il2CppObject * __this /* static, unused */, MScrollView_t1133512511 * ____this0, int32_t ___index1, bool ___isAim2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MScrollViewGenerated::ilo_getBooleanS11(System.Int32)
extern "C"  bool MScrollViewGenerated_ilo_getBooleanS11_m2111392532 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MScrollViewGenerated::ilo_isFunctionS12(System.Int32)
extern "C"  bool MScrollViewGenerated_ilo_isFunctionS12_m2181367845 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MScrollView/OnMoveCallBackFun MScrollViewGenerated::ilo_MScrollView_Register_GetDelegate_member4_arg113(CSRepresentedObject)
extern "C"  OnMoveCallBackFun_t3535987578 * MScrollViewGenerated_ilo_MScrollView_Register_GetDelegate_member4_arg113_m1577071325 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
