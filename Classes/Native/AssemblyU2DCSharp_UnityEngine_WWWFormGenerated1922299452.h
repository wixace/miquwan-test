﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// JSDataExchangeMgr/DGetV`1<System.Byte[]>
struct DGetV_1_t4138411810;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine_WWWFormGenerated
struct  UnityEngine_WWWFormGenerated_t1922299452  : public Il2CppObject
{
public:

public:
};

struct UnityEngine_WWWFormGenerated_t1922299452_StaticFields
{
public:
	// JSDataExchangeMgr/DGetV`1<System.Byte[]> UnityEngine_WWWFormGenerated::<>f__am$cache0
	DGetV_1_t4138411810 * ___U3CU3Ef__amU24cache0_0;
	// JSDataExchangeMgr/DGetV`1<System.Byte[]> UnityEngine_WWWFormGenerated::<>f__am$cache1
	DGetV_1_t4138411810 * ___U3CU3Ef__amU24cache1_1;
	// JSDataExchangeMgr/DGetV`1<System.Byte[]> UnityEngine_WWWFormGenerated::<>f__am$cache2
	DGetV_1_t4138411810 * ___U3CU3Ef__amU24cache2_2;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_0() { return static_cast<int32_t>(offsetof(UnityEngine_WWWFormGenerated_t1922299452_StaticFields, ___U3CU3Ef__amU24cache0_0)); }
	inline DGetV_1_t4138411810 * get_U3CU3Ef__amU24cache0_0() const { return ___U3CU3Ef__amU24cache0_0; }
	inline DGetV_1_t4138411810 ** get_address_of_U3CU3Ef__amU24cache0_0() { return &___U3CU3Ef__amU24cache0_0; }
	inline void set_U3CU3Ef__amU24cache0_0(DGetV_1_t4138411810 * value)
	{
		___U3CU3Ef__amU24cache0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_1() { return static_cast<int32_t>(offsetof(UnityEngine_WWWFormGenerated_t1922299452_StaticFields, ___U3CU3Ef__amU24cache1_1)); }
	inline DGetV_1_t4138411810 * get_U3CU3Ef__amU24cache1_1() const { return ___U3CU3Ef__amU24cache1_1; }
	inline DGetV_1_t4138411810 ** get_address_of_U3CU3Ef__amU24cache1_1() { return &___U3CU3Ef__amU24cache1_1; }
	inline void set_U3CU3Ef__amU24cache1_1(DGetV_1_t4138411810 * value)
	{
		___U3CU3Ef__amU24cache1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1_1, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_2() { return static_cast<int32_t>(offsetof(UnityEngine_WWWFormGenerated_t1922299452_StaticFields, ___U3CU3Ef__amU24cache2_2)); }
	inline DGetV_1_t4138411810 * get_U3CU3Ef__amU24cache2_2() const { return ___U3CU3Ef__amU24cache2_2; }
	inline DGetV_1_t4138411810 ** get_address_of_U3CU3Ef__amU24cache2_2() { return &___U3CU3Ef__amU24cache2_2; }
	inline void set_U3CU3Ef__amU24cache2_2(DGetV_1_t4138411810 * value)
	{
		___U3CU3Ef__amU24cache2_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache2_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
