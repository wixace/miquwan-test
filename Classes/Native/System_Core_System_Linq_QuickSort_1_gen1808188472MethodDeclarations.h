﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Linq.QuickSort`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct QuickSort_1_t1808188472;
// System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct IEnumerable_1_t950614638;
// System.Linq.SortContext`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct SortContext_1_t894152078;
// System.Int32[]
struct Int32U5BU5D_t3230847821;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Linq.QuickSort`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(System.Collections.Generic.IEnumerable`1<TElement>,System.Linq.SortContext`1<TElement>)
extern "C"  void QuickSort_1__ctor_m4111296475_gshared (QuickSort_1_t1808188472 * __this, Il2CppObject* ___source0, SortContext_1_t894152078 * ___context1, const MethodInfo* method);
#define QuickSort_1__ctor_m4111296475(__this, ___source0, ___context1, method) ((  void (*) (QuickSort_1_t1808188472 *, Il2CppObject*, SortContext_1_t894152078 *, const MethodInfo*))QuickSort_1__ctor_m4111296475_gshared)(__this, ___source0, ___context1, method)
// System.Int32[] System.Linq.QuickSort`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::CreateIndexes(System.Int32)
extern "C"  Int32U5BU5D_t3230847821* QuickSort_1_CreateIndexes_m1569871688_gshared (Il2CppObject * __this /* static, unused */, int32_t ___length0, const MethodInfo* method);
#define QuickSort_1_CreateIndexes_m1569871688(__this /* static, unused */, ___length0, method) ((  Int32U5BU5D_t3230847821* (*) (Il2CppObject * /* static, unused */, int32_t, const MethodInfo*))QuickSort_1_CreateIndexes_m1569871688_gshared)(__this /* static, unused */, ___length0, method)
// System.Void System.Linq.QuickSort`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::PerformSort()
extern "C"  void QuickSort_1_PerformSort_m4075992614_gshared (QuickSort_1_t1808188472 * __this, const MethodInfo* method);
#define QuickSort_1_PerformSort_m4075992614(__this, method) ((  void (*) (QuickSort_1_t1808188472 *, const MethodInfo*))QuickSort_1_PerformSort_m4075992614_gshared)(__this, method)
// System.Int32 System.Linq.QuickSort`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::CompareItems(System.Int32,System.Int32)
extern "C"  int32_t QuickSort_1_CompareItems_m501097278_gshared (QuickSort_1_t1808188472 * __this, int32_t ___first_index0, int32_t ___second_index1, const MethodInfo* method);
#define QuickSort_1_CompareItems_m501097278(__this, ___first_index0, ___second_index1, method) ((  int32_t (*) (QuickSort_1_t1808188472 *, int32_t, int32_t, const MethodInfo*))QuickSort_1_CompareItems_m501097278_gshared)(__this, ___first_index0, ___second_index1, method)
// System.Int32 System.Linq.QuickSort`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::MedianOfThree(System.Int32,System.Int32)
extern "C"  int32_t QuickSort_1_MedianOfThree_m1404151636_gshared (QuickSort_1_t1808188472 * __this, int32_t ___left0, int32_t ___right1, const MethodInfo* method);
#define QuickSort_1_MedianOfThree_m1404151636(__this, ___left0, ___right1, method) ((  int32_t (*) (QuickSort_1_t1808188472 *, int32_t, int32_t, const MethodInfo*))QuickSort_1_MedianOfThree_m1404151636_gshared)(__this, ___left0, ___right1, method)
// System.Void System.Linq.QuickSort`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Sort(System.Int32,System.Int32)
extern "C"  void QuickSort_1_Sort_m2367428525_gshared (QuickSort_1_t1808188472 * __this, int32_t ___left0, int32_t ___right1, const MethodInfo* method);
#define QuickSort_1_Sort_m2367428525(__this, ___left0, ___right1, method) ((  void (*) (QuickSort_1_t1808188472 *, int32_t, int32_t, const MethodInfo*))QuickSort_1_Sort_m2367428525_gshared)(__this, ___left0, ___right1, method)
// System.Void System.Linq.QuickSort`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::InsertionSort(System.Int32,System.Int32)
extern "C"  void QuickSort_1_InsertionSort_m2918960178_gshared (QuickSort_1_t1808188472 * __this, int32_t ___left0, int32_t ___right1, const MethodInfo* method);
#define QuickSort_1_InsertionSort_m2918960178(__this, ___left0, ___right1, method) ((  void (*) (QuickSort_1_t1808188472 *, int32_t, int32_t, const MethodInfo*))QuickSort_1_InsertionSort_m2918960178_gshared)(__this, ___left0, ___right1, method)
// System.Void System.Linq.QuickSort`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Swap(System.Int32,System.Int32)
extern "C"  void QuickSort_1_Swap_m284109464_gshared (QuickSort_1_t1808188472 * __this, int32_t ___left0, int32_t ___right1, const MethodInfo* method);
#define QuickSort_1_Swap_m284109464(__this, ___left0, ___right1, method) ((  void (*) (QuickSort_1_t1808188472 *, int32_t, int32_t, const MethodInfo*))QuickSort_1_Swap_m284109464_gshared)(__this, ___left0, ___right1, method)
// System.Collections.Generic.IEnumerable`1<TElement> System.Linq.QuickSort`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Sort(System.Collections.Generic.IEnumerable`1<TElement>,System.Linq.SortContext`1<TElement>)
extern "C"  Il2CppObject* QuickSort_1_Sort_m404915323_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___source0, SortContext_1_t894152078 * ___context1, const MethodInfo* method);
#define QuickSort_1_Sort_m404915323(__this /* static, unused */, ___source0, ___context1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, SortContext_1_t894152078 *, const MethodInfo*))QuickSort_1_Sort_m404915323_gshared)(__this /* static, unused */, ___source0, ___context1, method)
