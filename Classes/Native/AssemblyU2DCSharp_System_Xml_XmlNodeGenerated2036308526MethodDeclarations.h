﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System_Xml_XmlNodeGenerated
struct System_Xml_XmlNodeGenerated_t2036308526;
// JSVCall
struct JSVCall_t3708497963;
// System.String
struct String_t;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System_Xml_XmlNodeGenerated::.ctor()
extern "C"  void System_Xml_XmlNodeGenerated__ctor_m901293309 (System_Xml_XmlNodeGenerated_t2036308526 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Xml_XmlNodeGenerated::XmlNode_Attributes(JSVCall)
extern "C"  void System_Xml_XmlNodeGenerated_XmlNode_Attributes_m2361284483 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Xml_XmlNodeGenerated::XmlNode_BaseURI(JSVCall)
extern "C"  void System_Xml_XmlNodeGenerated_XmlNode_BaseURI_m774652439 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Xml_XmlNodeGenerated::XmlNode_ChildNodes(JSVCall)
extern "C"  void System_Xml_XmlNodeGenerated_XmlNode_ChildNodes_m3082515365 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Xml_XmlNodeGenerated::XmlNode_FirstChild(JSVCall)
extern "C"  void System_Xml_XmlNodeGenerated_XmlNode_FirstChild_m843555886 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Xml_XmlNodeGenerated::XmlNode_HasChildNodes(JSVCall)
extern "C"  void System_Xml_XmlNodeGenerated_XmlNode_HasChildNodes_m2675177283 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Xml_XmlNodeGenerated::XmlNode_InnerText(JSVCall)
extern "C"  void System_Xml_XmlNodeGenerated_XmlNode_InnerText_m3192686671 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Xml_XmlNodeGenerated::XmlNode_InnerXml(JSVCall)
extern "C"  void System_Xml_XmlNodeGenerated_XmlNode_InnerXml_m2916706105 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Xml_XmlNodeGenerated::XmlNode_IsReadOnly(JSVCall)
extern "C"  void System_Xml_XmlNodeGenerated_XmlNode_IsReadOnly_m902084302 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Xml_XmlNodeGenerated::XmlNode_Item_String(JSVCall)
extern "C"  void System_Xml_XmlNodeGenerated_XmlNode_Item_String_m3661250645 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Xml_XmlNodeGenerated::XmlNode_Item_String_String(JSVCall)
extern "C"  void System_Xml_XmlNodeGenerated_XmlNode_Item_String_String_m1512330375 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Xml_XmlNodeGenerated::XmlNode_LastChild(JSVCall)
extern "C"  void System_Xml_XmlNodeGenerated_XmlNode_LastChild_m2836535340 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Xml_XmlNodeGenerated::XmlNode_LocalName(JSVCall)
extern "C"  void System_Xml_XmlNodeGenerated_XmlNode_LocalName_m2362250684 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Xml_XmlNodeGenerated::XmlNode_Name(JSVCall)
extern "C"  void System_Xml_XmlNodeGenerated_XmlNode_Name_m2742667215 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Xml_XmlNodeGenerated::XmlNode_NamespaceURI(JSVCall)
extern "C"  void System_Xml_XmlNodeGenerated_XmlNode_NamespaceURI_m2880140041 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Xml_XmlNodeGenerated::XmlNode_NextSibling(JSVCall)
extern "C"  void System_Xml_XmlNodeGenerated_XmlNode_NextSibling_m3426416419 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Xml_XmlNodeGenerated::XmlNode_NodeType(JSVCall)
extern "C"  void System_Xml_XmlNodeGenerated_XmlNode_NodeType_m3143762718 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Xml_XmlNodeGenerated::XmlNode_OuterXml(JSVCall)
extern "C"  void System_Xml_XmlNodeGenerated_XmlNode_OuterXml_m3623234014 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Xml_XmlNodeGenerated::XmlNode_OwnerDocument(JSVCall)
extern "C"  void System_Xml_XmlNodeGenerated_XmlNode_OwnerDocument_m1082256996 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Xml_XmlNodeGenerated::XmlNode_ParentNode(JSVCall)
extern "C"  void System_Xml_XmlNodeGenerated_XmlNode_ParentNode_m582250158 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Xml_XmlNodeGenerated::XmlNode_Prefix(JSVCall)
extern "C"  void System_Xml_XmlNodeGenerated_XmlNode_Prefix_m432102056 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Xml_XmlNodeGenerated::XmlNode_PreviousSibling(JSVCall)
extern "C"  void System_Xml_XmlNodeGenerated_XmlNode_PreviousSibling_m2068658471 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Xml_XmlNodeGenerated::XmlNode_Value(JSVCall)
extern "C"  void System_Xml_XmlNodeGenerated_XmlNode_Value_m2127778113 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Xml_XmlNodeGenerated::XmlNode_SchemaInfo(JSVCall)
extern "C"  void System_Xml_XmlNodeGenerated_XmlNode_SchemaInfo_m3578418667 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XmlNodeGenerated::XmlNode_AppendChild__XmlNode(JSVCall,System.Int32)
extern "C"  bool System_Xml_XmlNodeGenerated_XmlNode_AppendChild__XmlNode_m1273940864 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XmlNodeGenerated::XmlNode_Clone(JSVCall,System.Int32)
extern "C"  bool System_Xml_XmlNodeGenerated_XmlNode_Clone_m2433105622 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XmlNodeGenerated::XmlNode_CloneNode__Boolean(JSVCall,System.Int32)
extern "C"  bool System_Xml_XmlNodeGenerated_XmlNode_CloneNode__Boolean_m540287794 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XmlNodeGenerated::XmlNode_CreateNavigator(JSVCall,System.Int32)
extern "C"  bool System_Xml_XmlNodeGenerated_XmlNode_CreateNavigator_m3260242356 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XmlNodeGenerated::XmlNode_GetEnumerator(JSVCall,System.Int32)
extern "C"  bool System_Xml_XmlNodeGenerated_XmlNode_GetEnumerator_m652834163 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XmlNodeGenerated::XmlNode_GetNamespaceOfPrefix__String(JSVCall,System.Int32)
extern "C"  bool System_Xml_XmlNodeGenerated_XmlNode_GetNamespaceOfPrefix__String_m1476150728 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XmlNodeGenerated::XmlNode_GetPrefixOfNamespace__String(JSVCall,System.Int32)
extern "C"  bool System_Xml_XmlNodeGenerated_XmlNode_GetPrefixOfNamespace__String_m2576337622 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XmlNodeGenerated::XmlNode_InsertAfter__XmlNode__XmlNode(JSVCall,System.Int32)
extern "C"  bool System_Xml_XmlNodeGenerated_XmlNode_InsertAfter__XmlNode__XmlNode_m1783473404 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XmlNodeGenerated::XmlNode_InsertBefore__XmlNode__XmlNode(JSVCall,System.Int32)
extern "C"  bool System_Xml_XmlNodeGenerated_XmlNode_InsertBefore__XmlNode__XmlNode_m2987239841 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XmlNodeGenerated::XmlNode_Normalize(JSVCall,System.Int32)
extern "C"  bool System_Xml_XmlNodeGenerated_XmlNode_Normalize_m3115518086 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XmlNodeGenerated::XmlNode_PrependChild__XmlNode(JSVCall,System.Int32)
extern "C"  bool System_Xml_XmlNodeGenerated_XmlNode_PrependChild__XmlNode_m679553860 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XmlNodeGenerated::XmlNode_RemoveAll(JSVCall,System.Int32)
extern "C"  bool System_Xml_XmlNodeGenerated_XmlNode_RemoveAll_m1478000630 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XmlNodeGenerated::XmlNode_RemoveChild__XmlNode(JSVCall,System.Int32)
extern "C"  bool System_Xml_XmlNodeGenerated_XmlNode_RemoveChild__XmlNode_m2981234666 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XmlNodeGenerated::XmlNode_ReplaceChild__XmlNode__XmlNode(JSVCall,System.Int32)
extern "C"  bool System_Xml_XmlNodeGenerated_XmlNode_ReplaceChild__XmlNode__XmlNode_m2991283409 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XmlNodeGenerated::XmlNode_SelectNodes__String__XmlNamespaceManager(JSVCall,System.Int32)
extern "C"  bool System_Xml_XmlNodeGenerated_XmlNode_SelectNodes__String__XmlNamespaceManager_m985839948 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XmlNodeGenerated::XmlNode_SelectNodes__String(JSVCall,System.Int32)
extern "C"  bool System_Xml_XmlNodeGenerated_XmlNode_SelectNodes__String_m731448543 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XmlNodeGenerated::XmlNode_SelectSingleNode__String__XmlNamespaceManager(JSVCall,System.Int32)
extern "C"  bool System_Xml_XmlNodeGenerated_XmlNode_SelectSingleNode__String__XmlNamespaceManager_m317079947 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XmlNodeGenerated::XmlNode_SelectSingleNode__String(JSVCall,System.Int32)
extern "C"  bool System_Xml_XmlNodeGenerated_XmlNode_SelectSingleNode__String_m346157888 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XmlNodeGenerated::XmlNode_Supports__String__String(JSVCall,System.Int32)
extern "C"  bool System_Xml_XmlNodeGenerated_XmlNode_Supports__String__String_m1965133071 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XmlNodeGenerated::XmlNode_WriteContentTo__XmlWriter(JSVCall,System.Int32)
extern "C"  bool System_Xml_XmlNodeGenerated_XmlNode_WriteContentTo__XmlWriter_m2996993038 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XmlNodeGenerated::XmlNode_WriteTo__XmlWriter(JSVCall,System.Int32)
extern "C"  bool System_Xml_XmlNodeGenerated_XmlNode_WriteTo__XmlWriter_m3186323033 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Xml_XmlNodeGenerated::__Register()
extern "C"  void System_Xml_XmlNodeGenerated___Register_m2372496874 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Xml_XmlNodeGenerated::ilo_setStringS1(System.Int32,System.String)
extern "C"  void System_Xml_XmlNodeGenerated_ilo_setStringS1_m134937926 (Il2CppObject * __this /* static, unused */, int32_t ___e0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System_Xml_XmlNodeGenerated::ilo_getStringS2(System.Int32)
extern "C"  String_t* System_Xml_XmlNodeGenerated_ilo_getStringS2_m1065817378 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System_Xml_XmlNodeGenerated::ilo_setObject3(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t System_Xml_XmlNodeGenerated_ilo_setObject3_m2794280531 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XmlNodeGenerated::ilo_getBooleanS4(System.Int32)
extern "C"  bool System_Xml_XmlNodeGenerated_ilo_getBooleanS4_m1354965002 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System_Xml_XmlNodeGenerated::ilo_getObject5(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * System_Xml_XmlNodeGenerated_ilo_getObject5_m2715877836 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
