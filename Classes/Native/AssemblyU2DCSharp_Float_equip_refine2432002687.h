﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t1659122786;
// UILabel
struct UILabel_t291504320;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "AssemblyU2DCSharp_FloatTextUnit2362298029.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Float_equip_refine
struct  Float_equip_refine_t2432002687  : public FloatTextUnit_t2362298029
{
public:
	// UnityEngine.Transform Float_equip_refine::_refine_panel
	Transform_t1659122786 * ____refine_panel_4;
	// UnityEngine.Transform Float_equip_refine::_refine_masetr_panel
	Transform_t1659122786 * ____refine_masetr_panel_5;
	// UILabel Float_equip_refine::_lab_tile
	UILabel_t291504320 * ____lab_tile_6;
	// UILabel Float_equip_refine::_lab_value
	UILabel_t291504320 * ____lab_value_7;
	// UILabel Float_equip_refine::_lab_master_tile
	UILabel_t291504320 * ____lab_master_tile_8;
	// UILabel Float_equip_refine::_lab_master_value
	UILabel_t291504320 * ____lab_master_value_9;
	// System.String[] Float_equip_refine::floatMasterArr
	StringU5BU5D_t4054002952* ___floatMasterArr_10;
	// System.Boolean Float_equip_refine::isMaster
	bool ___isMaster_11;
	// System.Int32 Float_equip_refine::masterlv
	int32_t ___masterlv_12;
	// System.Int32 Float_equip_refine::StrKey
	int32_t ___StrKey_13;
	// System.UInt32 Float_equip_refine::dealyID
	uint32_t ___dealyID_14;

public:
	inline static int32_t get_offset_of__refine_panel_4() { return static_cast<int32_t>(offsetof(Float_equip_refine_t2432002687, ____refine_panel_4)); }
	inline Transform_t1659122786 * get__refine_panel_4() const { return ____refine_panel_4; }
	inline Transform_t1659122786 ** get_address_of__refine_panel_4() { return &____refine_panel_4; }
	inline void set__refine_panel_4(Transform_t1659122786 * value)
	{
		____refine_panel_4 = value;
		Il2CppCodeGenWriteBarrier(&____refine_panel_4, value);
	}

	inline static int32_t get_offset_of__refine_masetr_panel_5() { return static_cast<int32_t>(offsetof(Float_equip_refine_t2432002687, ____refine_masetr_panel_5)); }
	inline Transform_t1659122786 * get__refine_masetr_panel_5() const { return ____refine_masetr_panel_5; }
	inline Transform_t1659122786 ** get_address_of__refine_masetr_panel_5() { return &____refine_masetr_panel_5; }
	inline void set__refine_masetr_panel_5(Transform_t1659122786 * value)
	{
		____refine_masetr_panel_5 = value;
		Il2CppCodeGenWriteBarrier(&____refine_masetr_panel_5, value);
	}

	inline static int32_t get_offset_of__lab_tile_6() { return static_cast<int32_t>(offsetof(Float_equip_refine_t2432002687, ____lab_tile_6)); }
	inline UILabel_t291504320 * get__lab_tile_6() const { return ____lab_tile_6; }
	inline UILabel_t291504320 ** get_address_of__lab_tile_6() { return &____lab_tile_6; }
	inline void set__lab_tile_6(UILabel_t291504320 * value)
	{
		____lab_tile_6 = value;
		Il2CppCodeGenWriteBarrier(&____lab_tile_6, value);
	}

	inline static int32_t get_offset_of__lab_value_7() { return static_cast<int32_t>(offsetof(Float_equip_refine_t2432002687, ____lab_value_7)); }
	inline UILabel_t291504320 * get__lab_value_7() const { return ____lab_value_7; }
	inline UILabel_t291504320 ** get_address_of__lab_value_7() { return &____lab_value_7; }
	inline void set__lab_value_7(UILabel_t291504320 * value)
	{
		____lab_value_7 = value;
		Il2CppCodeGenWriteBarrier(&____lab_value_7, value);
	}

	inline static int32_t get_offset_of__lab_master_tile_8() { return static_cast<int32_t>(offsetof(Float_equip_refine_t2432002687, ____lab_master_tile_8)); }
	inline UILabel_t291504320 * get__lab_master_tile_8() const { return ____lab_master_tile_8; }
	inline UILabel_t291504320 ** get_address_of__lab_master_tile_8() { return &____lab_master_tile_8; }
	inline void set__lab_master_tile_8(UILabel_t291504320 * value)
	{
		____lab_master_tile_8 = value;
		Il2CppCodeGenWriteBarrier(&____lab_master_tile_8, value);
	}

	inline static int32_t get_offset_of__lab_master_value_9() { return static_cast<int32_t>(offsetof(Float_equip_refine_t2432002687, ____lab_master_value_9)); }
	inline UILabel_t291504320 * get__lab_master_value_9() const { return ____lab_master_value_9; }
	inline UILabel_t291504320 ** get_address_of__lab_master_value_9() { return &____lab_master_value_9; }
	inline void set__lab_master_value_9(UILabel_t291504320 * value)
	{
		____lab_master_value_9 = value;
		Il2CppCodeGenWriteBarrier(&____lab_master_value_9, value);
	}

	inline static int32_t get_offset_of_floatMasterArr_10() { return static_cast<int32_t>(offsetof(Float_equip_refine_t2432002687, ___floatMasterArr_10)); }
	inline StringU5BU5D_t4054002952* get_floatMasterArr_10() const { return ___floatMasterArr_10; }
	inline StringU5BU5D_t4054002952** get_address_of_floatMasterArr_10() { return &___floatMasterArr_10; }
	inline void set_floatMasterArr_10(StringU5BU5D_t4054002952* value)
	{
		___floatMasterArr_10 = value;
		Il2CppCodeGenWriteBarrier(&___floatMasterArr_10, value);
	}

	inline static int32_t get_offset_of_isMaster_11() { return static_cast<int32_t>(offsetof(Float_equip_refine_t2432002687, ___isMaster_11)); }
	inline bool get_isMaster_11() const { return ___isMaster_11; }
	inline bool* get_address_of_isMaster_11() { return &___isMaster_11; }
	inline void set_isMaster_11(bool value)
	{
		___isMaster_11 = value;
	}

	inline static int32_t get_offset_of_masterlv_12() { return static_cast<int32_t>(offsetof(Float_equip_refine_t2432002687, ___masterlv_12)); }
	inline int32_t get_masterlv_12() const { return ___masterlv_12; }
	inline int32_t* get_address_of_masterlv_12() { return &___masterlv_12; }
	inline void set_masterlv_12(int32_t value)
	{
		___masterlv_12 = value;
	}

	inline static int32_t get_offset_of_StrKey_13() { return static_cast<int32_t>(offsetof(Float_equip_refine_t2432002687, ___StrKey_13)); }
	inline int32_t get_StrKey_13() const { return ___StrKey_13; }
	inline int32_t* get_address_of_StrKey_13() { return &___StrKey_13; }
	inline void set_StrKey_13(int32_t value)
	{
		___StrKey_13 = value;
	}

	inline static int32_t get_offset_of_dealyID_14() { return static_cast<int32_t>(offsetof(Float_equip_refine_t2432002687, ___dealyID_14)); }
	inline uint32_t get_dealyID_14() const { return ___dealyID_14; }
	inline uint32_t* get_address_of_dealyID_14() { return &___dealyID_14; }
	inline void set_dealyID_14(uint32_t value)
	{
		___dealyID_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
