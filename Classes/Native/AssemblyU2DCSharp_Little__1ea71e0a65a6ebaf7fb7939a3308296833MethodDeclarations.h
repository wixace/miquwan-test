﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._1ea71e0a65a6ebaf7fb7939afd4d412d
struct _1ea71e0a65a6ebaf7fb7939afd4d412d_t3308296833;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__1ea71e0a65a6ebaf7fb7939a3308296833.h"

// System.Void Little._1ea71e0a65a6ebaf7fb7939afd4d412d::.ctor()
extern "C"  void _1ea71e0a65a6ebaf7fb7939afd4d412d__ctor_m329370828 (_1ea71e0a65a6ebaf7fb7939afd4d412d_t3308296833 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._1ea71e0a65a6ebaf7fb7939afd4d412d::_1ea71e0a65a6ebaf7fb7939afd4d412dm2(System.Int32)
extern "C"  int32_t _1ea71e0a65a6ebaf7fb7939afd4d412d__1ea71e0a65a6ebaf7fb7939afd4d412dm2_m1453816313 (_1ea71e0a65a6ebaf7fb7939afd4d412d_t3308296833 * __this, int32_t ____1ea71e0a65a6ebaf7fb7939afd4d412da0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._1ea71e0a65a6ebaf7fb7939afd4d412d::_1ea71e0a65a6ebaf7fb7939afd4d412dm(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _1ea71e0a65a6ebaf7fb7939afd4d412d__1ea71e0a65a6ebaf7fb7939afd4d412dm_m1809930461 (_1ea71e0a65a6ebaf7fb7939afd4d412d_t3308296833 * __this, int32_t ____1ea71e0a65a6ebaf7fb7939afd4d412da0, int32_t ____1ea71e0a65a6ebaf7fb7939afd4d412d451, int32_t ____1ea71e0a65a6ebaf7fb7939afd4d412dc2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._1ea71e0a65a6ebaf7fb7939afd4d412d::ilo__1ea71e0a65a6ebaf7fb7939afd4d412dm21(Little._1ea71e0a65a6ebaf7fb7939afd4d412d,System.Int32)
extern "C"  int32_t _1ea71e0a65a6ebaf7fb7939afd4d412d_ilo__1ea71e0a65a6ebaf7fb7939afd4d412dm21_m1853657960 (Il2CppObject * __this /* static, unused */, _1ea71e0a65a6ebaf7fb7939afd4d412d_t3308296833 * ____this0, int32_t ____1ea71e0a65a6ebaf7fb7939afd4d412da1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
