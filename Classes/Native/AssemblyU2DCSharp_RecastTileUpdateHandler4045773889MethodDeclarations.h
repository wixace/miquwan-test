﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// RecastTileUpdateHandler
struct RecastTileUpdateHandler_t4045773889;
// Pathfinding.RecastGraph
struct RecastGraph_t2197443166;
// AstarPath
struct AstarPath_t4090270936;
// Pathfinding.GraphUpdateObject
struct GraphUpdateObject_t430843704;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_RecastGraph2197443166.h"
#include "UnityEngine_UnityEngine_Bounds2711641849.h"
#include "AssemblyU2DCSharp_RecastTileUpdateHandler4045773889.h"
#include "AssemblyU2DCSharp_AstarPath4090270936.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphUpdateObject430843704.h"

// System.Void RecastTileUpdateHandler::.ctor()
extern "C"  void RecastTileUpdateHandler__ctor_m102628426 (RecastTileUpdateHandler_t4045773889 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RecastTileUpdateHandler::SetGraph(Pathfinding.RecastGraph)
extern "C"  void RecastTileUpdateHandler_SetGraph_m1142211226 (RecastTileUpdateHandler_t4045773889 * __this, RecastGraph_t2197443166 * ___graph0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RecastTileUpdateHandler::ScheduleUpdate(UnityEngine.Bounds)
extern "C"  void RecastTileUpdateHandler_ScheduleUpdate_m3412361038 (RecastTileUpdateHandler_t4045773889 * __this, Bounds_t2711641849  ___bounds0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RecastTileUpdateHandler::OnEnable()
extern "C"  void RecastTileUpdateHandler_OnEnable_m1458302716 (RecastTileUpdateHandler_t4045773889 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RecastTileUpdateHandler::OnDisable()
extern "C"  void RecastTileUpdateHandler_OnDisable_m2698648497 (RecastTileUpdateHandler_t4045773889 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RecastTileUpdateHandler::Update()
extern "C"  void RecastTileUpdateHandler_Update_m613376003 (RecastTileUpdateHandler_t4045773889 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RecastTileUpdateHandler::UpdateDirtyTiles()
extern "C"  void RecastTileUpdateHandler_UpdateDirtyTiles_m3055481782 (RecastTileUpdateHandler_t4045773889 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RecastTileUpdateHandler::ilo_UpdateDirtyTiles1(RecastTileUpdateHandler)
extern "C"  void RecastTileUpdateHandler_ilo_UpdateDirtyTiles1_m971536071 (Il2CppObject * __this /* static, unused */, RecastTileUpdateHandler_t4045773889 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RecastTileUpdateHandler::ilo_SetGraph2(RecastTileUpdateHandler,Pathfinding.RecastGraph)
extern "C"  void RecastTileUpdateHandler_ilo_SetGraph2_m2992461264 (Il2CppObject * __this /* static, unused */, RecastTileUpdateHandler_t4045773889 * ____this0, RecastGraph_t2197443166 * ___graph1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Bounds RecastTileUpdateHandler::ilo_GetTileBounds3(Pathfinding.RecastGraph,System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  Bounds_t2711641849  RecastTileUpdateHandler_ilo_GetTileBounds3_m3003434058 (Il2CppObject * __this /* static, unused */, RecastGraph_t2197443166 * ____this0, int32_t ___x1, int32_t ___z2, int32_t ___width3, int32_t ___depth4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RecastTileUpdateHandler::ilo_UpdateGraphs4(AstarPath,Pathfinding.GraphUpdateObject)
extern "C"  void RecastTileUpdateHandler_ilo_UpdateGraphs4_m708971839 (Il2CppObject * __this /* static, unused */, AstarPath_t4090270936 * ____this0, GraphUpdateObject_t430843704 * ___ob1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
