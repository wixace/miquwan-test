﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ABDepends
struct ABDepends_t759746598;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1375417109;
// System.Collections.Generic.List`1<System.String[]>
struct List_1_t1127221208;

#include "UnityEngine_UnityEngine_ScriptableObject2970544072.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ABDepends
struct  ABDepends_t759746598  : public ScriptableObject_t2970544072
{
public:
	// System.Collections.Generic.List`1<System.String> ABDepends::nameList
	List_1_t1375417109 * ___nameList_3;
	// System.Collections.Generic.List`1<System.String[]> ABDepends::dependList
	List_1_t1127221208 * ___dependList_4;
	// System.Collections.Generic.List`1<System.String> ABDepends::dependStrList
	List_1_t1375417109 * ___dependStrList_5;
	// System.Boolean ABDepends::isInit
	bool ___isInit_6;

public:
	inline static int32_t get_offset_of_nameList_3() { return static_cast<int32_t>(offsetof(ABDepends_t759746598, ___nameList_3)); }
	inline List_1_t1375417109 * get_nameList_3() const { return ___nameList_3; }
	inline List_1_t1375417109 ** get_address_of_nameList_3() { return &___nameList_3; }
	inline void set_nameList_3(List_1_t1375417109 * value)
	{
		___nameList_3 = value;
		Il2CppCodeGenWriteBarrier(&___nameList_3, value);
	}

	inline static int32_t get_offset_of_dependList_4() { return static_cast<int32_t>(offsetof(ABDepends_t759746598, ___dependList_4)); }
	inline List_1_t1127221208 * get_dependList_4() const { return ___dependList_4; }
	inline List_1_t1127221208 ** get_address_of_dependList_4() { return &___dependList_4; }
	inline void set_dependList_4(List_1_t1127221208 * value)
	{
		___dependList_4 = value;
		Il2CppCodeGenWriteBarrier(&___dependList_4, value);
	}

	inline static int32_t get_offset_of_dependStrList_5() { return static_cast<int32_t>(offsetof(ABDepends_t759746598, ___dependStrList_5)); }
	inline List_1_t1375417109 * get_dependStrList_5() const { return ___dependStrList_5; }
	inline List_1_t1375417109 ** get_address_of_dependStrList_5() { return &___dependStrList_5; }
	inline void set_dependStrList_5(List_1_t1375417109 * value)
	{
		___dependStrList_5 = value;
		Il2CppCodeGenWriteBarrier(&___dependStrList_5, value);
	}

	inline static int32_t get_offset_of_isInit_6() { return static_cast<int32_t>(offsetof(ABDepends_t759746598, ___isInit_6)); }
	inline bool get_isInit_6() const { return ___isInit_6; }
	inline bool* get_address_of_isInit_6() { return &___isInit_6; }
	inline void set_isInit_6(bool value)
	{
		___isInit_6 = value;
	}
};

struct ABDepends_t759746598_StaticFields
{
public:
	// ABDepends ABDepends::abDepends
	ABDepends_t759746598 * ___abDepends_2;

public:
	inline static int32_t get_offset_of_abDepends_2() { return static_cast<int32_t>(offsetof(ABDepends_t759746598_StaticFields, ___abDepends_2)); }
	inline ABDepends_t759746598 * get_abDepends_2() const { return ___abDepends_2; }
	inline ABDepends_t759746598 ** get_address_of_abDepends_2() { return &___abDepends_2; }
	inline void set_abDepends_2(ABDepends_t759746598 * value)
	{
		___abDepends_2 = value;
		Il2CppCodeGenWriteBarrier(&___abDepends_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
