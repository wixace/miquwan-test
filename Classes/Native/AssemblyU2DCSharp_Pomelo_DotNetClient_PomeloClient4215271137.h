﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Pomelo.DotNetClient.EventManager
struct EventManager_t3099234959;
// System.Net.Sockets.Socket
struct Socket_t2157335841;
// Pomelo.DotNetClient.Protocol
struct Protocol_t400511732;
// Client/ResponseCallBack
struct ResponseCallBack_t59495818;
// Client/PushCallBack
struct PushCallBack_t4203546979;
// System.Threading.ManualResetEvent
struct ManualResetEvent_t924017833;
// Newtonsoft.Json.Linq.JObject
struct JObject_t1798544199;
// System.Action`1<Pomelo.DotNetClient.NetWorkState>
struct Action_1_t3724939911;

#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_Pomelo_DotNetClient_NetWorkState3329123775.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pomelo.DotNetClient.PomeloClient
struct  PomeloClient_t4215271137  : public Il2CppObject
{
public:
	// Pomelo.DotNetClient.NetWorkState Pomelo.DotNetClient.PomeloClient::netWorkState
	int32_t ___netWorkState_2;
	// Pomelo.DotNetClient.EventManager Pomelo.DotNetClient.PomeloClient::eventManager
	EventManager_t3099234959 * ___eventManager_3;
	// System.Net.Sockets.Socket Pomelo.DotNetClient.PomeloClient::socket
	Socket_t2157335841 * ___socket_4;
	// Pomelo.DotNetClient.Protocol Pomelo.DotNetClient.PomeloClient::protocol
	Protocol_t400511732 * ___protocol_5;
	// Client/ResponseCallBack Pomelo.DotNetClient.PomeloClient::responseFun
	ResponseCallBack_t59495818 * ___responseFun_6;
	// Client/PushCallBack Pomelo.DotNetClient.PomeloClient::pushFun
	PushCallBack_t4203546979 * ___pushFun_7;
	// System.Boolean Pomelo.DotNetClient.PomeloClient::disposed
	bool ___disposed_8;
	// System.Threading.ManualResetEvent Pomelo.DotNetClient.PomeloClient::timeoutEvent
	ManualResetEvent_t924017833 * ___timeoutEvent_10;
	// System.Int32 Pomelo.DotNetClient.PomeloClient::timeoutMSec
	int32_t ___timeoutMSec_11;
	// Newtonsoft.Json.Linq.JObject Pomelo.DotNetClient.PomeloClient::emptyMsg
	JObject_t1798544199 * ___emptyMsg_12;
	// System.Action`1<Pomelo.DotNetClient.NetWorkState> Pomelo.DotNetClient.PomeloClient::NetWorkStateChangedEvent
	Action_1_t3724939911 * ___NetWorkStateChangedEvent_13;

public:
	inline static int32_t get_offset_of_netWorkState_2() { return static_cast<int32_t>(offsetof(PomeloClient_t4215271137, ___netWorkState_2)); }
	inline int32_t get_netWorkState_2() const { return ___netWorkState_2; }
	inline int32_t* get_address_of_netWorkState_2() { return &___netWorkState_2; }
	inline void set_netWorkState_2(int32_t value)
	{
		___netWorkState_2 = value;
	}

	inline static int32_t get_offset_of_eventManager_3() { return static_cast<int32_t>(offsetof(PomeloClient_t4215271137, ___eventManager_3)); }
	inline EventManager_t3099234959 * get_eventManager_3() const { return ___eventManager_3; }
	inline EventManager_t3099234959 ** get_address_of_eventManager_3() { return &___eventManager_3; }
	inline void set_eventManager_3(EventManager_t3099234959 * value)
	{
		___eventManager_3 = value;
		Il2CppCodeGenWriteBarrier(&___eventManager_3, value);
	}

	inline static int32_t get_offset_of_socket_4() { return static_cast<int32_t>(offsetof(PomeloClient_t4215271137, ___socket_4)); }
	inline Socket_t2157335841 * get_socket_4() const { return ___socket_4; }
	inline Socket_t2157335841 ** get_address_of_socket_4() { return &___socket_4; }
	inline void set_socket_4(Socket_t2157335841 * value)
	{
		___socket_4 = value;
		Il2CppCodeGenWriteBarrier(&___socket_4, value);
	}

	inline static int32_t get_offset_of_protocol_5() { return static_cast<int32_t>(offsetof(PomeloClient_t4215271137, ___protocol_5)); }
	inline Protocol_t400511732 * get_protocol_5() const { return ___protocol_5; }
	inline Protocol_t400511732 ** get_address_of_protocol_5() { return &___protocol_5; }
	inline void set_protocol_5(Protocol_t400511732 * value)
	{
		___protocol_5 = value;
		Il2CppCodeGenWriteBarrier(&___protocol_5, value);
	}

	inline static int32_t get_offset_of_responseFun_6() { return static_cast<int32_t>(offsetof(PomeloClient_t4215271137, ___responseFun_6)); }
	inline ResponseCallBack_t59495818 * get_responseFun_6() const { return ___responseFun_6; }
	inline ResponseCallBack_t59495818 ** get_address_of_responseFun_6() { return &___responseFun_6; }
	inline void set_responseFun_6(ResponseCallBack_t59495818 * value)
	{
		___responseFun_6 = value;
		Il2CppCodeGenWriteBarrier(&___responseFun_6, value);
	}

	inline static int32_t get_offset_of_pushFun_7() { return static_cast<int32_t>(offsetof(PomeloClient_t4215271137, ___pushFun_7)); }
	inline PushCallBack_t4203546979 * get_pushFun_7() const { return ___pushFun_7; }
	inline PushCallBack_t4203546979 ** get_address_of_pushFun_7() { return &___pushFun_7; }
	inline void set_pushFun_7(PushCallBack_t4203546979 * value)
	{
		___pushFun_7 = value;
		Il2CppCodeGenWriteBarrier(&___pushFun_7, value);
	}

	inline static int32_t get_offset_of_disposed_8() { return static_cast<int32_t>(offsetof(PomeloClient_t4215271137, ___disposed_8)); }
	inline bool get_disposed_8() const { return ___disposed_8; }
	inline bool* get_address_of_disposed_8() { return &___disposed_8; }
	inline void set_disposed_8(bool value)
	{
		___disposed_8 = value;
	}

	inline static int32_t get_offset_of_timeoutEvent_10() { return static_cast<int32_t>(offsetof(PomeloClient_t4215271137, ___timeoutEvent_10)); }
	inline ManualResetEvent_t924017833 * get_timeoutEvent_10() const { return ___timeoutEvent_10; }
	inline ManualResetEvent_t924017833 ** get_address_of_timeoutEvent_10() { return &___timeoutEvent_10; }
	inline void set_timeoutEvent_10(ManualResetEvent_t924017833 * value)
	{
		___timeoutEvent_10 = value;
		Il2CppCodeGenWriteBarrier(&___timeoutEvent_10, value);
	}

	inline static int32_t get_offset_of_timeoutMSec_11() { return static_cast<int32_t>(offsetof(PomeloClient_t4215271137, ___timeoutMSec_11)); }
	inline int32_t get_timeoutMSec_11() const { return ___timeoutMSec_11; }
	inline int32_t* get_address_of_timeoutMSec_11() { return &___timeoutMSec_11; }
	inline void set_timeoutMSec_11(int32_t value)
	{
		___timeoutMSec_11 = value;
	}

	inline static int32_t get_offset_of_emptyMsg_12() { return static_cast<int32_t>(offsetof(PomeloClient_t4215271137, ___emptyMsg_12)); }
	inline JObject_t1798544199 * get_emptyMsg_12() const { return ___emptyMsg_12; }
	inline JObject_t1798544199 ** get_address_of_emptyMsg_12() { return &___emptyMsg_12; }
	inline void set_emptyMsg_12(JObject_t1798544199 * value)
	{
		___emptyMsg_12 = value;
		Il2CppCodeGenWriteBarrier(&___emptyMsg_12, value);
	}

	inline static int32_t get_offset_of_NetWorkStateChangedEvent_13() { return static_cast<int32_t>(offsetof(PomeloClient_t4215271137, ___NetWorkStateChangedEvent_13)); }
	inline Action_1_t3724939911 * get_NetWorkStateChangedEvent_13() const { return ___NetWorkStateChangedEvent_13; }
	inline Action_1_t3724939911 ** get_address_of_NetWorkStateChangedEvent_13() { return &___NetWorkStateChangedEvent_13; }
	inline void set_NetWorkStateChangedEvent_13(Action_1_t3724939911 * value)
	{
		___NetWorkStateChangedEvent_13 = value;
		Il2CppCodeGenWriteBarrier(&___NetWorkStateChangedEvent_13, value);
	}
};

struct PomeloClient_t4215271137_StaticFields
{
public:
	// System.UInt32 Pomelo.DotNetClient.PomeloClient::curSocketID
	uint32_t ___curSocketID_0;
	// System.UInt32 Pomelo.DotNetClient.PomeloClient::timeoutID
	uint32_t ___timeoutID_1;
	// System.UInt32 Pomelo.DotNetClient.PomeloClient::reqId
	uint32_t ___reqId_9;

public:
	inline static int32_t get_offset_of_curSocketID_0() { return static_cast<int32_t>(offsetof(PomeloClient_t4215271137_StaticFields, ___curSocketID_0)); }
	inline uint32_t get_curSocketID_0() const { return ___curSocketID_0; }
	inline uint32_t* get_address_of_curSocketID_0() { return &___curSocketID_0; }
	inline void set_curSocketID_0(uint32_t value)
	{
		___curSocketID_0 = value;
	}

	inline static int32_t get_offset_of_timeoutID_1() { return static_cast<int32_t>(offsetof(PomeloClient_t4215271137_StaticFields, ___timeoutID_1)); }
	inline uint32_t get_timeoutID_1() const { return ___timeoutID_1; }
	inline uint32_t* get_address_of_timeoutID_1() { return &___timeoutID_1; }
	inline void set_timeoutID_1(uint32_t value)
	{
		___timeoutID_1 = value;
	}

	inline static int32_t get_offset_of_reqId_9() { return static_cast<int32_t>(offsetof(PomeloClient_t4215271137_StaticFields, ___reqId_9)); }
	inline uint32_t get_reqId_9() const { return ___reqId_9; }
	inline uint32_t* get_address_of_reqId_9() { return &___reqId_9; }
	inline void set_reqId_9(uint32_t value)
	{
		___reqId_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
