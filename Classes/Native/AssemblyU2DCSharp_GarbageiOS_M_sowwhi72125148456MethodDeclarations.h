﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_sowwhi72
struct M_sowwhi72_t125148456;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_sowwhi72125148456.h"

// System.Void GarbageiOS.M_sowwhi72::.ctor()
extern "C"  void M_sowwhi72__ctor_m2599720459 (M_sowwhi72_t125148456 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_sowwhi72::M_jearkucowNalqato0(System.String[],System.Int32)
extern "C"  void M_sowwhi72_M_jearkucowNalqato0_m1198019759 (M_sowwhi72_t125148456 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_sowwhi72::M_corjeme1(System.String[],System.Int32)
extern "C"  void M_sowwhi72_M_corjeme1_m1238070874 (M_sowwhi72_t125148456 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_sowwhi72::M_sirsairJerjere2(System.String[],System.Int32)
extern "C"  void M_sowwhi72_M_sirsairJerjere2_m1650072462 (M_sowwhi72_t125148456 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_sowwhi72::ilo_M_jearkucowNalqato01(GarbageiOS.M_sowwhi72,System.String[],System.Int32)
extern "C"  void M_sowwhi72_ilo_M_jearkucowNalqato01_m3117429881 (Il2CppObject * __this /* static, unused */, M_sowwhi72_t125148456 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
