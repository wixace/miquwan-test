﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Utilities.WrapperDictionary
struct WrapperDictionary_t2273338311;
// System.String
struct String_t;
// System.Type
struct Type_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Type2863145774.h"

// System.Void Newtonsoft.Json.Utilities.WrapperDictionary::.ctor()
extern "C"  void WrapperDictionary__ctor_m3811871899 (WrapperDictionary_t2273338311 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Utilities.WrapperDictionary::GenerateKey(System.Type,System.Type)
extern "C"  String_t* WrapperDictionary_GenerateKey_m730010662 (Il2CppObject * __this /* static, unused */, Type_t * ___interfaceType0, Type_t * ___realObjectType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Newtonsoft.Json.Utilities.WrapperDictionary::GetType(System.Type,System.Type)
extern "C"  Type_t * WrapperDictionary_GetType_m237346505 (WrapperDictionary_t2273338311 * __this, Type_t * ___interfaceType0, Type_t * ___realObjectType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Utilities.WrapperDictionary::SetType(System.Type,System.Type,System.Type)
extern "C"  void WrapperDictionary_SetType_m2527301322 (WrapperDictionary_t2273338311 * __this, Type_t * ___interfaceType0, Type_t * ___realObjectType1, Type_t * ___wrapperType2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Utilities.WrapperDictionary::ilo_GenerateKey1(System.Type,System.Type)
extern "C"  String_t* WrapperDictionary_ilo_GenerateKey1_m2329153132 (Il2CppObject * __this /* static, unused */, Type_t * ___interfaceType0, Type_t * ___realObjectType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
