﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_Events_UnityEventBaseGenerated
struct UnityEngine_Events_UnityEventBaseGenerated_t2754673595;
// JSVCall
struct JSVCall_t3708497963;
// System.Type[]
struct TypeU5BU5D_t3339007067;
// System.String
struct String_t;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void UnityEngine_Events_UnityEventBaseGenerated::.ctor()
extern "C"  void UnityEngine_Events_UnityEventBaseGenerated__ctor_m2770616128 (UnityEngine_Events_UnityEventBaseGenerated_t2754673595 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Events_UnityEventBaseGenerated::UnityEventBase_GetPersistentEventCount(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Events_UnityEventBaseGenerated_UnityEventBase_GetPersistentEventCount_m223412225 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Events_UnityEventBaseGenerated::UnityEventBase_GetPersistentMethodName__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Events_UnityEventBaseGenerated_UnityEventBase_GetPersistentMethodName__Int32_m3901338392 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Events_UnityEventBaseGenerated::UnityEventBase_GetPersistentTarget__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Events_UnityEventBaseGenerated_UnityEventBase_GetPersistentTarget__Int32_m777755955 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Events_UnityEventBaseGenerated::UnityEventBase_RemoveAllListeners(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Events_UnityEventBaseGenerated_UnityEventBase_RemoveAllListeners_m1787212709 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Events_UnityEventBaseGenerated::UnityEventBase_SetPersistentListenerState__Int32__UnityEventCallState(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Events_UnityEventBaseGenerated_UnityEventBase_SetPersistentListenerState__Int32__UnityEventCallState_m2789959527 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Events_UnityEventBaseGenerated::UnityEventBase_ToString(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Events_UnityEventBaseGenerated_UnityEventBase_ToString_m3227026639 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Events_UnityEventBaseGenerated::UnityEventBase_GetValidMethodInfo__Object__String__Type_Array(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Events_UnityEventBaseGenerated_UnityEventBase_GetValidMethodInfo__Object__String__Type_Array_m3364197628 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Events_UnityEventBaseGenerated::__Register()
extern "C"  void UnityEngine_Events_UnityEventBaseGenerated___Register_m1166198151 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type[] UnityEngine_Events_UnityEventBaseGenerated::<UnityEventBase_GetValidMethodInfo__Object__String__Type_Array>m__1B0()
extern "C"  TypeU5BU5D_t3339007067* UnityEngine_Events_UnityEventBaseGenerated_U3CUnityEventBase_GetValidMethodInfo__Object__String__Type_ArrayU3Em__1B0_m1772837520 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_Events_UnityEventBaseGenerated::ilo_getInt321(System.Int32)
extern "C"  int32_t UnityEngine_Events_UnityEventBaseGenerated_ilo_getInt321_m643074311 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Events_UnityEventBaseGenerated::ilo_setStringS2(System.Int32,System.String)
extern "C"  void UnityEngine_Events_UnityEventBaseGenerated_ilo_setStringS2_m2708330122 (Il2CppObject * __this /* static, unused */, int32_t ___e0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_Events_UnityEventBaseGenerated::ilo_setObject3(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UnityEngine_Events_UnityEventBaseGenerated_ilo_setObject3_m600904268 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_Events_UnityEventBaseGenerated::ilo_getWhatever4(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_Events_UnityEventBaseGenerated_ilo_getWhatever4_m431252207 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
