﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// Pathfinding.AstarData
struct AstarData_t3283402719;
// Pathfinding.Serialization.JsonFx.JsonWriterSettings
struct JsonWriterSettings_t3394579648;
// Pathfinding.Serialization.JsonFx.JsonReaderSettings
struct JsonReaderSettings_t3095433488;
// Pathfinding.Ionic.Zip.ZipFile
struct ZipFile_t1348418467;
// System.IO.MemoryStream
struct MemoryStream_t418716369;
// Pathfinding.Serialization.GraphMeta
struct GraphMeta_t513097101;
// Pathfinding.Serialization.SerializeSettings
struct SerializeSettings_t2480699453;
// Pathfinding.NavGraph[]
struct NavGraphU5BU5D_t850130684;
// System.Text.UTF8Encoding
struct UTF8Encoding_t2817869802;
// System.Text.StringBuilder
struct StringBuilder_t243639308;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.Serialization.AstarSerializer
struct  AstarSerializer_t1384811327  : public Il2CppObject
{
public:
	// Pathfinding.AstarData Pathfinding.Serialization.AstarSerializer::data
	AstarData_t3283402719 * ___data_2;
	// Pathfinding.Serialization.JsonFx.JsonWriterSettings Pathfinding.Serialization.AstarSerializer::writerSettings
	JsonWriterSettings_t3394579648 * ___writerSettings_3;
	// Pathfinding.Serialization.JsonFx.JsonReaderSettings Pathfinding.Serialization.AstarSerializer::readerSettings
	JsonReaderSettings_t3095433488 * ___readerSettings_4;
	// Pathfinding.Ionic.Zip.ZipFile Pathfinding.Serialization.AstarSerializer::zip
	ZipFile_t1348418467 * ___zip_5;
	// System.IO.MemoryStream Pathfinding.Serialization.AstarSerializer::str
	MemoryStream_t418716369 * ___str_6;
	// Pathfinding.Serialization.GraphMeta Pathfinding.Serialization.AstarSerializer::meta
	GraphMeta_t513097101 * ___meta_7;
	// Pathfinding.Serialization.SerializeSettings Pathfinding.Serialization.AstarSerializer::settings
	SerializeSettings_t2480699453 * ___settings_8;
	// Pathfinding.NavGraph[] Pathfinding.Serialization.AstarSerializer::graphs
	NavGraphU5BU5D_t850130684* ___graphs_9;
	// System.UInt32 Pathfinding.Serialization.AstarSerializer::checksum
	uint32_t ___checksum_10;
	// System.Text.UTF8Encoding Pathfinding.Serialization.AstarSerializer::encoding
	UTF8Encoding_t2817869802 * ___encoding_11;

public:
	inline static int32_t get_offset_of_data_2() { return static_cast<int32_t>(offsetof(AstarSerializer_t1384811327, ___data_2)); }
	inline AstarData_t3283402719 * get_data_2() const { return ___data_2; }
	inline AstarData_t3283402719 ** get_address_of_data_2() { return &___data_2; }
	inline void set_data_2(AstarData_t3283402719 * value)
	{
		___data_2 = value;
		Il2CppCodeGenWriteBarrier(&___data_2, value);
	}

	inline static int32_t get_offset_of_writerSettings_3() { return static_cast<int32_t>(offsetof(AstarSerializer_t1384811327, ___writerSettings_3)); }
	inline JsonWriterSettings_t3394579648 * get_writerSettings_3() const { return ___writerSettings_3; }
	inline JsonWriterSettings_t3394579648 ** get_address_of_writerSettings_3() { return &___writerSettings_3; }
	inline void set_writerSettings_3(JsonWriterSettings_t3394579648 * value)
	{
		___writerSettings_3 = value;
		Il2CppCodeGenWriteBarrier(&___writerSettings_3, value);
	}

	inline static int32_t get_offset_of_readerSettings_4() { return static_cast<int32_t>(offsetof(AstarSerializer_t1384811327, ___readerSettings_4)); }
	inline JsonReaderSettings_t3095433488 * get_readerSettings_4() const { return ___readerSettings_4; }
	inline JsonReaderSettings_t3095433488 ** get_address_of_readerSettings_4() { return &___readerSettings_4; }
	inline void set_readerSettings_4(JsonReaderSettings_t3095433488 * value)
	{
		___readerSettings_4 = value;
		Il2CppCodeGenWriteBarrier(&___readerSettings_4, value);
	}

	inline static int32_t get_offset_of_zip_5() { return static_cast<int32_t>(offsetof(AstarSerializer_t1384811327, ___zip_5)); }
	inline ZipFile_t1348418467 * get_zip_5() const { return ___zip_5; }
	inline ZipFile_t1348418467 ** get_address_of_zip_5() { return &___zip_5; }
	inline void set_zip_5(ZipFile_t1348418467 * value)
	{
		___zip_5 = value;
		Il2CppCodeGenWriteBarrier(&___zip_5, value);
	}

	inline static int32_t get_offset_of_str_6() { return static_cast<int32_t>(offsetof(AstarSerializer_t1384811327, ___str_6)); }
	inline MemoryStream_t418716369 * get_str_6() const { return ___str_6; }
	inline MemoryStream_t418716369 ** get_address_of_str_6() { return &___str_6; }
	inline void set_str_6(MemoryStream_t418716369 * value)
	{
		___str_6 = value;
		Il2CppCodeGenWriteBarrier(&___str_6, value);
	}

	inline static int32_t get_offset_of_meta_7() { return static_cast<int32_t>(offsetof(AstarSerializer_t1384811327, ___meta_7)); }
	inline GraphMeta_t513097101 * get_meta_7() const { return ___meta_7; }
	inline GraphMeta_t513097101 ** get_address_of_meta_7() { return &___meta_7; }
	inline void set_meta_7(GraphMeta_t513097101 * value)
	{
		___meta_7 = value;
		Il2CppCodeGenWriteBarrier(&___meta_7, value);
	}

	inline static int32_t get_offset_of_settings_8() { return static_cast<int32_t>(offsetof(AstarSerializer_t1384811327, ___settings_8)); }
	inline SerializeSettings_t2480699453 * get_settings_8() const { return ___settings_8; }
	inline SerializeSettings_t2480699453 ** get_address_of_settings_8() { return &___settings_8; }
	inline void set_settings_8(SerializeSettings_t2480699453 * value)
	{
		___settings_8 = value;
		Il2CppCodeGenWriteBarrier(&___settings_8, value);
	}

	inline static int32_t get_offset_of_graphs_9() { return static_cast<int32_t>(offsetof(AstarSerializer_t1384811327, ___graphs_9)); }
	inline NavGraphU5BU5D_t850130684* get_graphs_9() const { return ___graphs_9; }
	inline NavGraphU5BU5D_t850130684** get_address_of_graphs_9() { return &___graphs_9; }
	inline void set_graphs_9(NavGraphU5BU5D_t850130684* value)
	{
		___graphs_9 = value;
		Il2CppCodeGenWriteBarrier(&___graphs_9, value);
	}

	inline static int32_t get_offset_of_checksum_10() { return static_cast<int32_t>(offsetof(AstarSerializer_t1384811327, ___checksum_10)); }
	inline uint32_t get_checksum_10() const { return ___checksum_10; }
	inline uint32_t* get_address_of_checksum_10() { return &___checksum_10; }
	inline void set_checksum_10(uint32_t value)
	{
		___checksum_10 = value;
	}

	inline static int32_t get_offset_of_encoding_11() { return static_cast<int32_t>(offsetof(AstarSerializer_t1384811327, ___encoding_11)); }
	inline UTF8Encoding_t2817869802 * get_encoding_11() const { return ___encoding_11; }
	inline UTF8Encoding_t2817869802 ** get_address_of_encoding_11() { return &___encoding_11; }
	inline void set_encoding_11(UTF8Encoding_t2817869802 * value)
	{
		___encoding_11 = value;
		Il2CppCodeGenWriteBarrier(&___encoding_11, value);
	}
};

struct AstarSerializer_t1384811327_StaticFields
{
public:
	// System.Text.StringBuilder Pathfinding.Serialization.AstarSerializer::_stringBuilder
	StringBuilder_t243639308 * ____stringBuilder_12;

public:
	inline static int32_t get_offset_of__stringBuilder_12() { return static_cast<int32_t>(offsetof(AstarSerializer_t1384811327_StaticFields, ____stringBuilder_12)); }
	inline StringBuilder_t243639308 * get__stringBuilder_12() const { return ____stringBuilder_12; }
	inline StringBuilder_t243639308 ** get_address_of__stringBuilder_12() { return &____stringBuilder_12; }
	inline void set__stringBuilder_12(StringBuilder_t243639308 * value)
	{
		____stringBuilder_12 = value;
		Il2CppCodeGenWriteBarrier(&____stringBuilder_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
