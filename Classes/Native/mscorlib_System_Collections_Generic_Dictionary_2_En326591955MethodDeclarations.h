﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3363211663MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,JSCache/TypeInfo>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m4255259008(__this, ___dictionary0, method) ((  void (*) (Enumerator_t326591955 *, Dictionary_2_t3304235859 *, const MethodInfo*))Enumerator__ctor_m3920831137_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Type,JSCache/TypeInfo>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2517345067(__this, method) ((  Il2CppObject * (*) (Enumerator_t326591955 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3262087712_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,JSCache/TypeInfo>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2850195253(__this, method) ((  void (*) (Enumerator_t326591955 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2959141748_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Type,JSCache/TypeInfo>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3395991404(__this, method) ((  DictionaryEntry_t1751606614  (*) (Enumerator_t326591955 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2279524093_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Type,JSCache/TypeInfo>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3921712071(__this, method) ((  Il2CppObject * (*) (Enumerator_t326591955 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1201448700_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Type,JSCache/TypeInfo>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3127418009(__this, method) ((  Il2CppObject * (*) (Enumerator_t326591955 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m294434446_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Type,JSCache/TypeInfo>::MoveNext()
#define Enumerator_MoveNext_m3239474661(__this, method) ((  bool (*) (Enumerator_t326591955 *, const MethodInfo*))Enumerator_MoveNext_m217327200_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Type,JSCache/TypeInfo>::get_Current()
#define Enumerator_get_Current_m1599499063(__this, method) ((  KeyValuePair_2_t3203016565  (*) (Enumerator_t326591955 *, const MethodInfo*))Enumerator_get_Current_m4240003024_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Type,JSCache/TypeInfo>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m161203502(__this, method) ((  Type_t * (*) (Enumerator_t326591955 *, const MethodInfo*))Enumerator_get_CurrentKey_m3062159917_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Type,JSCache/TypeInfo>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m405113966(__this, method) ((  TypeInfo_t3198813374 * (*) (Enumerator_t326591955 *, const MethodInfo*))Enumerator_get_CurrentValue_m592783249_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,JSCache/TypeInfo>::Reset()
#define Enumerator_Reset_m3370677202(__this, method) ((  void (*) (Enumerator_t326591955 *, const MethodInfo*))Enumerator_Reset_m3001375603_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,JSCache/TypeInfo>::VerifyState()
#define Enumerator_VerifyState_m2121670235(__this, method) ((  void (*) (Enumerator_t326591955 *, const MethodInfo*))Enumerator_VerifyState_m4290054460_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,JSCache/TypeInfo>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m1560502019(__this, method) ((  void (*) (Enumerator_t326591955 *, const MethodInfo*))Enumerator_VerifyCurrent_m2318603684_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,JSCache/TypeInfo>::Dispose()
#define Enumerator_Dispose_m3338879010(__this, method) ((  void (*) (Enumerator_t326591955 *, const MethodInfo*))Enumerator_Dispose_m627360643_gshared)(__this, method)
