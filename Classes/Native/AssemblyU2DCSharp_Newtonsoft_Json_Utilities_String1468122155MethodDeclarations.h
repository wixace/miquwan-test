﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Utilities.StringUtils/<ForgivingCaseSensitiveFind>c__AnonStorey14B`1<System.Object>
struct U3CForgivingCaseSensitiveFindU3Ec__AnonStorey14B_1_t1468122155;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void Newtonsoft.Json.Utilities.StringUtils/<ForgivingCaseSensitiveFind>c__AnonStorey14B`1<System.Object>::.ctor()
extern "C"  void U3CForgivingCaseSensitiveFindU3Ec__AnonStorey14B_1__ctor_m240006394_gshared (U3CForgivingCaseSensitiveFindU3Ec__AnonStorey14B_1_t1468122155 * __this, const MethodInfo* method);
#define U3CForgivingCaseSensitiveFindU3Ec__AnonStorey14B_1__ctor_m240006394(__this, method) ((  void (*) (U3CForgivingCaseSensitiveFindU3Ec__AnonStorey14B_1_t1468122155 *, const MethodInfo*))U3CForgivingCaseSensitiveFindU3Ec__AnonStorey14B_1__ctor_m240006394_gshared)(__this, method)
// System.Void Newtonsoft.Json.Utilities.StringUtils/<ForgivingCaseSensitiveFind>c__AnonStorey14B`1<System.Object>::<>m__3B7(TSource)
extern "C"  void U3CForgivingCaseSensitiveFindU3Ec__AnonStorey14B_1_U3CU3Em__3B7_m4011523132_gshared (U3CForgivingCaseSensitiveFindU3Ec__AnonStorey14B_1_t1468122155 * __this, Il2CppObject * ___itm0, const MethodInfo* method);
#define U3CForgivingCaseSensitiveFindU3Ec__AnonStorey14B_1_U3CU3Em__3B7_m4011523132(__this, ___itm0, method) ((  void (*) (U3CForgivingCaseSensitiveFindU3Ec__AnonStorey14B_1_t1468122155 *, Il2CppObject *, const MethodInfo*))U3CForgivingCaseSensitiveFindU3Ec__AnonStorey14B_1_U3CU3Em__3B7_m4011523132_gshared)(__this, ___itm0, method)
