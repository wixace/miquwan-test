﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// UnityEngine.WWWForm
struct WWWForm_t461342257;
// UnityEngine.WWW
struct WWW_t3134621005;
// System.Object
struct Il2CppObject;
// PluginSamsung
struct PluginSamsung_t1663707431;

#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2144973319.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_726430633.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PluginSamsung/<SendHttpLogin>c__Iterator34
struct  U3CSendHttpLoginU3Ec__Iterator34_t3547970329  : public Il2CppObject
{
public:
	// System.String PluginSamsung/<SendHttpLogin>c__Iterator34::<fieldText>__0
	String_t* ___U3CfieldTextU3E__0_0;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginSamsung/<SendHttpLogin>c__Iterator34::parameters
	Dictionary_2_t827649927 * ___parameters_1;
	// System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.String> PluginSamsung/<SendHttpLogin>c__Iterator34::<$s_318>__1
	Enumerator_t2144973319  ___U3CU24s_318U3E__1_2;
	// System.Collections.Generic.KeyValuePair`2<System.String,System.String> PluginSamsung/<SendHttpLogin>c__Iterator34::<keyvalue>__2
	KeyValuePair_2_t726430633  ___U3CkeyvalueU3E__2_3;
	// UnityEngine.WWWForm PluginSamsung/<SendHttpLogin>c__Iterator34::<form>__3
	WWWForm_t461342257 * ___U3CformU3E__3_4;
	// UnityEngine.WWW PluginSamsung/<SendHttpLogin>c__Iterator34::<www>__4
	WWW_t3134621005 * ___U3CwwwU3E__4_5;
	// System.Int32 PluginSamsung/<SendHttpLogin>c__Iterator34::$PC
	int32_t ___U24PC_6;
	// System.Object PluginSamsung/<SendHttpLogin>c__Iterator34::$current
	Il2CppObject * ___U24current_7;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginSamsung/<SendHttpLogin>c__Iterator34::<$>parameters
	Dictionary_2_t827649927 * ___U3CU24U3Eparameters_8;
	// PluginSamsung PluginSamsung/<SendHttpLogin>c__Iterator34::<>f__this
	PluginSamsung_t1663707431 * ___U3CU3Ef__this_9;

public:
	inline static int32_t get_offset_of_U3CfieldTextU3E__0_0() { return static_cast<int32_t>(offsetof(U3CSendHttpLoginU3Ec__Iterator34_t3547970329, ___U3CfieldTextU3E__0_0)); }
	inline String_t* get_U3CfieldTextU3E__0_0() const { return ___U3CfieldTextU3E__0_0; }
	inline String_t** get_address_of_U3CfieldTextU3E__0_0() { return &___U3CfieldTextU3E__0_0; }
	inline void set_U3CfieldTextU3E__0_0(String_t* value)
	{
		___U3CfieldTextU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CfieldTextU3E__0_0, value);
	}

	inline static int32_t get_offset_of_parameters_1() { return static_cast<int32_t>(offsetof(U3CSendHttpLoginU3Ec__Iterator34_t3547970329, ___parameters_1)); }
	inline Dictionary_2_t827649927 * get_parameters_1() const { return ___parameters_1; }
	inline Dictionary_2_t827649927 ** get_address_of_parameters_1() { return &___parameters_1; }
	inline void set_parameters_1(Dictionary_2_t827649927 * value)
	{
		___parameters_1 = value;
		Il2CppCodeGenWriteBarrier(&___parameters_1, value);
	}

	inline static int32_t get_offset_of_U3CU24s_318U3E__1_2() { return static_cast<int32_t>(offsetof(U3CSendHttpLoginU3Ec__Iterator34_t3547970329, ___U3CU24s_318U3E__1_2)); }
	inline Enumerator_t2144973319  get_U3CU24s_318U3E__1_2() const { return ___U3CU24s_318U3E__1_2; }
	inline Enumerator_t2144973319 * get_address_of_U3CU24s_318U3E__1_2() { return &___U3CU24s_318U3E__1_2; }
	inline void set_U3CU24s_318U3E__1_2(Enumerator_t2144973319  value)
	{
		___U3CU24s_318U3E__1_2 = value;
	}

	inline static int32_t get_offset_of_U3CkeyvalueU3E__2_3() { return static_cast<int32_t>(offsetof(U3CSendHttpLoginU3Ec__Iterator34_t3547970329, ___U3CkeyvalueU3E__2_3)); }
	inline KeyValuePair_2_t726430633  get_U3CkeyvalueU3E__2_3() const { return ___U3CkeyvalueU3E__2_3; }
	inline KeyValuePair_2_t726430633 * get_address_of_U3CkeyvalueU3E__2_3() { return &___U3CkeyvalueU3E__2_3; }
	inline void set_U3CkeyvalueU3E__2_3(KeyValuePair_2_t726430633  value)
	{
		___U3CkeyvalueU3E__2_3 = value;
	}

	inline static int32_t get_offset_of_U3CformU3E__3_4() { return static_cast<int32_t>(offsetof(U3CSendHttpLoginU3Ec__Iterator34_t3547970329, ___U3CformU3E__3_4)); }
	inline WWWForm_t461342257 * get_U3CformU3E__3_4() const { return ___U3CformU3E__3_4; }
	inline WWWForm_t461342257 ** get_address_of_U3CformU3E__3_4() { return &___U3CformU3E__3_4; }
	inline void set_U3CformU3E__3_4(WWWForm_t461342257 * value)
	{
		___U3CformU3E__3_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CformU3E__3_4, value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E__4_5() { return static_cast<int32_t>(offsetof(U3CSendHttpLoginU3Ec__Iterator34_t3547970329, ___U3CwwwU3E__4_5)); }
	inline WWW_t3134621005 * get_U3CwwwU3E__4_5() const { return ___U3CwwwU3E__4_5; }
	inline WWW_t3134621005 ** get_address_of_U3CwwwU3E__4_5() { return &___U3CwwwU3E__4_5; }
	inline void set_U3CwwwU3E__4_5(WWW_t3134621005 * value)
	{
		___U3CwwwU3E__4_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CwwwU3E__4_5, value);
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CSendHttpLoginU3Ec__Iterator34_t3547970329, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}

	inline static int32_t get_offset_of_U24current_7() { return static_cast<int32_t>(offsetof(U3CSendHttpLoginU3Ec__Iterator34_t3547970329, ___U24current_7)); }
	inline Il2CppObject * get_U24current_7() const { return ___U24current_7; }
	inline Il2CppObject ** get_address_of_U24current_7() { return &___U24current_7; }
	inline void set_U24current_7(Il2CppObject * value)
	{
		___U24current_7 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_7, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Eparameters_8() { return static_cast<int32_t>(offsetof(U3CSendHttpLoginU3Ec__Iterator34_t3547970329, ___U3CU24U3Eparameters_8)); }
	inline Dictionary_2_t827649927 * get_U3CU24U3Eparameters_8() const { return ___U3CU24U3Eparameters_8; }
	inline Dictionary_2_t827649927 ** get_address_of_U3CU24U3Eparameters_8() { return &___U3CU24U3Eparameters_8; }
	inline void set_U3CU24U3Eparameters_8(Dictionary_2_t827649927 * value)
	{
		___U3CU24U3Eparameters_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Eparameters_8, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_9() { return static_cast<int32_t>(offsetof(U3CSendHttpLoginU3Ec__Iterator34_t3547970329, ___U3CU3Ef__this_9)); }
	inline PluginSamsung_t1663707431 * get_U3CU3Ef__this_9() const { return ___U3CU3Ef__this_9; }
	inline PluginSamsung_t1663707431 ** get_address_of_U3CU3Ef__this_9() { return &___U3CU3Ef__this_9; }
	inline void set_U3CU3Ef__this_9(PluginSamsung_t1663707431 * value)
	{
		___U3CU3Ef__this_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
