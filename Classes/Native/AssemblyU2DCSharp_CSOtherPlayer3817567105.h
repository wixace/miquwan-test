﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// CSHeroUnit[]
struct CSHeroUnitU5BU5D_t1342235227;
// CSPVPRobotNPC[]
struct CSPVPRobotNPCU5BU5D_t1447782092;
// CSDevilSkillData
struct CSDevilSkillData_t3736118547;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CSOtherPlayer
struct  CSOtherPlayer_t3817567105  : public Il2CppObject
{
public:
	// System.UInt32 CSOtherPlayer::roleId
	uint32_t ___roleId_0;
	// System.String CSOtherPlayer::name
	String_t* ___name_1;
	// System.String CSOtherPlayer::icon
	String_t* ___icon_2;
	// System.UInt32 CSOtherPlayer::rank
	uint32_t ___rank_3;
	// System.UInt32 CSOtherPlayer::lv
	uint32_t ___lv_4;
	// System.UInt32 CSOtherPlayer::exp
	uint32_t ___exp_5;
	// System.UInt32 CSOtherPlayer::power
	uint32_t ___power_6;
	// System.UInt32 CSOtherPlayer::uPower
	uint32_t ___uPower_7;
	// System.UInt32 CSOtherPlayer::type
	uint32_t ___type_8;
	// CSHeroUnit[] CSOtherPlayer::heroInfoList
	CSHeroUnitU5BU5D_t1342235227* ___heroInfoList_9;
	// CSPVPRobotNPC[] CSOtherPlayer::robotNpcs
	CSPVPRobotNPCU5BU5D_t1447782092* ___robotNpcs_10;
	// CSDevilSkillData CSOtherPlayer::devilSkillData
	CSDevilSkillData_t3736118547 * ___devilSkillData_11;

public:
	inline static int32_t get_offset_of_roleId_0() { return static_cast<int32_t>(offsetof(CSOtherPlayer_t3817567105, ___roleId_0)); }
	inline uint32_t get_roleId_0() const { return ___roleId_0; }
	inline uint32_t* get_address_of_roleId_0() { return &___roleId_0; }
	inline void set_roleId_0(uint32_t value)
	{
		___roleId_0 = value;
	}

	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(CSOtherPlayer_t3817567105, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier(&___name_1, value);
	}

	inline static int32_t get_offset_of_icon_2() { return static_cast<int32_t>(offsetof(CSOtherPlayer_t3817567105, ___icon_2)); }
	inline String_t* get_icon_2() const { return ___icon_2; }
	inline String_t** get_address_of_icon_2() { return &___icon_2; }
	inline void set_icon_2(String_t* value)
	{
		___icon_2 = value;
		Il2CppCodeGenWriteBarrier(&___icon_2, value);
	}

	inline static int32_t get_offset_of_rank_3() { return static_cast<int32_t>(offsetof(CSOtherPlayer_t3817567105, ___rank_3)); }
	inline uint32_t get_rank_3() const { return ___rank_3; }
	inline uint32_t* get_address_of_rank_3() { return &___rank_3; }
	inline void set_rank_3(uint32_t value)
	{
		___rank_3 = value;
	}

	inline static int32_t get_offset_of_lv_4() { return static_cast<int32_t>(offsetof(CSOtherPlayer_t3817567105, ___lv_4)); }
	inline uint32_t get_lv_4() const { return ___lv_4; }
	inline uint32_t* get_address_of_lv_4() { return &___lv_4; }
	inline void set_lv_4(uint32_t value)
	{
		___lv_4 = value;
	}

	inline static int32_t get_offset_of_exp_5() { return static_cast<int32_t>(offsetof(CSOtherPlayer_t3817567105, ___exp_5)); }
	inline uint32_t get_exp_5() const { return ___exp_5; }
	inline uint32_t* get_address_of_exp_5() { return &___exp_5; }
	inline void set_exp_5(uint32_t value)
	{
		___exp_5 = value;
	}

	inline static int32_t get_offset_of_power_6() { return static_cast<int32_t>(offsetof(CSOtherPlayer_t3817567105, ___power_6)); }
	inline uint32_t get_power_6() const { return ___power_6; }
	inline uint32_t* get_address_of_power_6() { return &___power_6; }
	inline void set_power_6(uint32_t value)
	{
		___power_6 = value;
	}

	inline static int32_t get_offset_of_uPower_7() { return static_cast<int32_t>(offsetof(CSOtherPlayer_t3817567105, ___uPower_7)); }
	inline uint32_t get_uPower_7() const { return ___uPower_7; }
	inline uint32_t* get_address_of_uPower_7() { return &___uPower_7; }
	inline void set_uPower_7(uint32_t value)
	{
		___uPower_7 = value;
	}

	inline static int32_t get_offset_of_type_8() { return static_cast<int32_t>(offsetof(CSOtherPlayer_t3817567105, ___type_8)); }
	inline uint32_t get_type_8() const { return ___type_8; }
	inline uint32_t* get_address_of_type_8() { return &___type_8; }
	inline void set_type_8(uint32_t value)
	{
		___type_8 = value;
	}

	inline static int32_t get_offset_of_heroInfoList_9() { return static_cast<int32_t>(offsetof(CSOtherPlayer_t3817567105, ___heroInfoList_9)); }
	inline CSHeroUnitU5BU5D_t1342235227* get_heroInfoList_9() const { return ___heroInfoList_9; }
	inline CSHeroUnitU5BU5D_t1342235227** get_address_of_heroInfoList_9() { return &___heroInfoList_9; }
	inline void set_heroInfoList_9(CSHeroUnitU5BU5D_t1342235227* value)
	{
		___heroInfoList_9 = value;
		Il2CppCodeGenWriteBarrier(&___heroInfoList_9, value);
	}

	inline static int32_t get_offset_of_robotNpcs_10() { return static_cast<int32_t>(offsetof(CSOtherPlayer_t3817567105, ___robotNpcs_10)); }
	inline CSPVPRobotNPCU5BU5D_t1447782092* get_robotNpcs_10() const { return ___robotNpcs_10; }
	inline CSPVPRobotNPCU5BU5D_t1447782092** get_address_of_robotNpcs_10() { return &___robotNpcs_10; }
	inline void set_robotNpcs_10(CSPVPRobotNPCU5BU5D_t1447782092* value)
	{
		___robotNpcs_10 = value;
		Il2CppCodeGenWriteBarrier(&___robotNpcs_10, value);
	}

	inline static int32_t get_offset_of_devilSkillData_11() { return static_cast<int32_t>(offsetof(CSOtherPlayer_t3817567105, ___devilSkillData_11)); }
	inline CSDevilSkillData_t3736118547 * get_devilSkillData_11() const { return ___devilSkillData_11; }
	inline CSDevilSkillData_t3736118547 ** get_address_of_devilSkillData_11() { return &___devilSkillData_11; }
	inline void set_devilSkillData_11(CSDevilSkillData_t3736118547 * value)
	{
		___devilSkillData_11 = value;
		Il2CppCodeGenWriteBarrier(&___devilSkillData_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
