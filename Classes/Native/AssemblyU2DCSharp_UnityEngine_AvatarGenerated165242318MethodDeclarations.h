﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_AvatarGenerated
struct UnityEngine_AvatarGenerated_t165242318;
// JSVCall
struct JSVCall_t3708497963;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"

// System.Void UnityEngine_AvatarGenerated::.ctor()
extern "C"  void UnityEngine_AvatarGenerated__ctor_m133819741 (UnityEngine_AvatarGenerated_t165242318 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AvatarGenerated::Avatar_isValid(JSVCall)
extern "C"  void UnityEngine_AvatarGenerated_Avatar_isValid_m2539051124 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AvatarGenerated::Avatar_isHuman(JSVCall)
extern "C"  void UnityEngine_AvatarGenerated_Avatar_isHuman_m3403468387 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AvatarGenerated::__Register()
extern "C"  void UnityEngine_AvatarGenerated___Register_m2089181578 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AvatarGenerated::ilo_setBooleanS1(System.Int32,System.Boolean)
extern "C"  void UnityEngine_AvatarGenerated_ilo_setBooleanS1_m2143904270 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
