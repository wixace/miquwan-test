﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_noqall28
struct M_noqall28_t4104028199;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_noqall284104028199.h"

// System.Void GarbageiOS.M_noqall28::.ctor()
extern "C"  void M_noqall28__ctor_m634495468 (M_noqall28_t4104028199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_noqall28::M_soucar0(System.String[],System.Int32)
extern "C"  void M_noqall28_M_soucar0_m3529255332 (M_noqall28_t4104028199 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_noqall28::M_kedall1(System.String[],System.Int32)
extern "C"  void M_noqall28_M_kedall1_m2819453673 (M_noqall28_t4104028199 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_noqall28::M_dugar2(System.String[],System.Int32)
extern "C"  void M_noqall28_M_dugar2_m2469298508 (M_noqall28_t4104028199 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_noqall28::M_miswhu3(System.String[],System.Int32)
extern "C"  void M_noqall28_M_miswhu3_m3823956405 (M_noqall28_t4104028199 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_noqall28::M_kistee4(System.String[],System.Int32)
extern "C"  void M_noqall28_M_kistee4_m496735396 (M_noqall28_t4104028199 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_noqall28::M_jurwhi5(System.String[],System.Int32)
extern "C"  void M_noqall28_M_jurwhi5_m2090986995 (M_noqall28_t4104028199 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_noqall28::M_neejawRowci6(System.String[],System.Int32)
extern "C"  void M_noqall28_M_neejawRowci6_m1067782729 (M_noqall28_t4104028199 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_noqall28::M_liserHedeeqea7(System.String[],System.Int32)
extern "C"  void M_noqall28_M_liserHedeeqea7_m514487695 (M_noqall28_t4104028199 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_noqall28::M_nokayvalGese8(System.String[],System.Int32)
extern "C"  void M_noqall28_M_nokayvalGese8_m120977368 (M_noqall28_t4104028199 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_noqall28::ilo_M_liserHedeeqea71(GarbageiOS.M_noqall28,System.String[],System.Int32)
extern "C"  void M_noqall28_ilo_M_liserHedeeqea71_m2589061202 (Il2CppObject * __this /* static, unused */, M_noqall28_t4104028199 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
