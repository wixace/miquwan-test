﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PluginXiongmaowan
struct PluginXiongmaowan_t3347898093;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// System.String
struct String_t;
// CEvent.EventFunc`1<CEvent.ZEvent>
struct EventFunc_1_t1114914310;
// VersionMgr
struct VersionMgr_t1322950208;
// Mihua.SDK.PayInfo
struct PayInfo_t1775308120;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// ProductsCfgMgr
struct ProductsCfgMgr_t2493714872;
// System.Object
struct Il2CppObject;
// System.Action
struct Action_t3771233898;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_VersionMgr1322950208.h"
#include "AssemblyU2DCSharp_PluginXiongmaowan3347898093.h"
#include "AssemblyU2DCSharp_ProductsCfgMgr2493714872.h"
#include "mscorlib_System_Object4170816371.h"
#include "System_Core_System_Action3771233898.h"

// System.Void PluginXiongmaowan::.ctor()
extern "C"  void PluginXiongmaowan__ctor_m1709066526 (PluginXiongmaowan_t3347898093 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiongmaowan::Init()
extern "C"  void PluginXiongmaowan_Init_m3054709110 (PluginXiongmaowan_t3347898093 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginXiongmaowan::ReqSDKHttpLogin()
extern "C"  Dictionary_2_t827649927 * PluginXiongmaowan_ReqSDKHttpLogin_m947498639 (PluginXiongmaowan_t3347898093 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginXiongmaowan::IsLoginSuccess(System.Boolean)
extern "C"  bool PluginXiongmaowan_IsLoginSuccess_m3970941837 (PluginXiongmaowan_t3347898093 * __this, bool ___isOpenUseLogin0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiongmaowan::OpenUserLogin()
extern "C"  void PluginXiongmaowan_OpenUserLogin_m3309530480 (PluginXiongmaowan_t3347898093 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiongmaowan::RolelvUpinfo(CEvent.ZEvent)
extern "C"  void PluginXiongmaowan_RolelvUpinfo_m874739020 (PluginXiongmaowan_t3347898093 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiongmaowan::UserPay(CEvent.ZEvent)
extern "C"  void PluginXiongmaowan_UserPay_m2170497026 (PluginXiongmaowan_t3347898093 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiongmaowan::OnInitSuccess(System.String)
extern "C"  void PluginXiongmaowan_OnInitSuccess_m1796390226 (PluginXiongmaowan_t3347898093 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiongmaowan::OnInitFail(System.String)
extern "C"  void PluginXiongmaowan_OnInitFail_m3796501807 (PluginXiongmaowan_t3347898093 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiongmaowan::OnLoginSuccess(System.String)
extern "C"  void PluginXiongmaowan_OnLoginSuccess_m243234211 (PluginXiongmaowan_t3347898093 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiongmaowan::OnLoginFail(System.String)
extern "C"  void PluginXiongmaowan_OnLoginFail_m357275262 (PluginXiongmaowan_t3347898093 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiongmaowan::OnPaySuccess(System.String)
extern "C"  void PluginXiongmaowan_OnPaySuccess_m2700928162 (PluginXiongmaowan_t3347898093 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiongmaowan::OnPayFail(System.String)
extern "C"  void PluginXiongmaowan_OnPayFail_m3243352031 (PluginXiongmaowan_t3347898093 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiongmaowan::OnExitGameSuccess(System.String)
extern "C"  void PluginXiongmaowan_OnExitGameSuccess_m1550477426 (PluginXiongmaowan_t3347898093 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiongmaowan::LogoutSuccess()
extern "C"  void PluginXiongmaowan_LogoutSuccess_m3260822549 (PluginXiongmaowan_t3347898093 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiongmaowan::OnLogoutSuccess(System.String)
extern "C"  void PluginXiongmaowan_OnLogoutSuccess_m3248835916 (PluginXiongmaowan_t3347898093 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiongmaowan::OnLogoutFail(System.String)
extern "C"  void PluginXiongmaowan_OnLogoutFail_m2298480501 (PluginXiongmaowan_t3347898093 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiongmaowan::XMWSDKInit(System.String,System.String,System.String)
extern "C"  void PluginXiongmaowan_XMWSDKInit_m3138183852 (PluginXiongmaowan_t3347898093 * __this, String_t* ___hotKey0, String_t* ___appKey1, String_t* ___appId2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiongmaowan::XMWSDKLogin()
extern "C"  void PluginXiongmaowan_XMWSDKLogin_m1371979597 (PluginXiongmaowan_t3347898093 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiongmaowan::XMWSDKRole(System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginXiongmaowan_XMWSDKRole_m3268191350 (PluginXiongmaowan_t3347898093 * __this, String_t* ___serverId0, String_t* ___serverName1, String_t* ___roleId2, String_t* ___roleName3, String_t* ___roleLevel4, String_t* ___payLevel5, String_t* ___desc6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiongmaowan::XMWSDKPay(System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginXiongmaowan_XMWSDKPay_m4000267530 (PluginXiongmaowan_t3347898093 * __this, String_t* ___productId0, String_t* ___Order1, String_t* ___Price2, String_t* ___desc3, String_t* ___serverName4, String_t* ___serverId5, String_t* ___roleName6, String_t* ___roleID7, String_t* ___roleLevel8, String_t* ___goodsName9, String_t* ___goodsID10, String_t* ___notifyURL11, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiongmaowan::<OnLogoutSuccess>m__46D()
extern "C"  void PluginXiongmaowan_U3COnLogoutSuccessU3Em__46D_m1515587217 (PluginXiongmaowan_t3347898093 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiongmaowan::ilo_AddEventListener1(System.String,CEvent.EventFunc`1<CEvent.ZEvent>)
extern "C"  void PluginXiongmaowan_ilo_AddEventListener1_m2335112214 (Il2CppObject * __this /* static, unused */, String_t* ___eventType0, EventFunc_1_t1114914310 * ___fun1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionMgr PluginXiongmaowan::ilo_get_Instance2()
extern "C"  VersionMgr_t1322950208 * PluginXiongmaowan_ilo_get_Instance2_m3070154298 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginXiongmaowan::ilo_get_isSdkLogin3(VersionMgr)
extern "C"  bool PluginXiongmaowan_ilo_get_isSdkLogin3_m2084545678 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiongmaowan::ilo_XMWSDKLogin4(PluginXiongmaowan)
extern "C"  void PluginXiongmaowan_ilo_XMWSDKLogin4_m418973065 (Il2CppObject * __this /* static, unused */, PluginXiongmaowan_t3347898093 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiongmaowan::ilo_XMWSDKRole5(PluginXiongmaowan,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginXiongmaowan_ilo_XMWSDKRole5_m4213760313 (Il2CppObject * __this /* static, unused */, PluginXiongmaowan_t3347898093 * ____this0, String_t* ___serverId1, String_t* ___serverName2, String_t* ___roleId3, String_t* ___roleName4, String_t* ___roleLevel5, String_t* ___payLevel6, String_t* ___desc7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mihua.SDK.PayInfo PluginXiongmaowan::ilo_Parse6(System.Object[])
extern "C"  PayInfo_t1775308120 * PluginXiongmaowan_ilo_Parse6_m2626335101 (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t1108656482* ___payOjs0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PluginXiongmaowan::ilo_GetProductID7(ProductsCfgMgr,System.String)
extern "C"  String_t* PluginXiongmaowan_ilo_GetProductID7_m2333939763 (Il2CppObject * __this /* static, unused */, ProductsCfgMgr_t2493714872 * ____this0, String_t* ___name1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiongmaowan::ilo_XMWSDKPay8(PluginXiongmaowan,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginXiongmaowan_ilo_XMWSDKPay8_m2952264596 (Il2CppObject * __this /* static, unused */, PluginXiongmaowan_t3347898093 * ____this0, String_t* ___productId1, String_t* ___Order2, String_t* ___Price3, String_t* ___desc4, String_t* ___serverName5, String_t* ___serverId6, String_t* ___roleName7, String_t* ___roleID8, String_t* ___roleLevel9, String_t* ___goodsName10, String_t* ___goodsID11, String_t* ___notifyURL12, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiongmaowan::ilo_Log9(System.Object,System.Boolean)
extern "C"  void PluginXiongmaowan_ilo_Log9_m3410803831 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiongmaowan::ilo_DispatchEvent10(CEvent.ZEvent)
extern "C"  void PluginXiongmaowan_ilo_DispatchEvent10_m3225459635 (Il2CppObject * __this /* static, unused */, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiongmaowan::ilo_Logout11(System.Action)
extern "C"  void PluginXiongmaowan_ilo_Logout11_m2189923802 (Il2CppObject * __this /* static, unused */, Action_t3771233898 * ___call0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
