﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AnimationRunner
struct AnimationRunner_t1015409588;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// ReplayMgr
struct ReplayMgr_t1549183121;
// CombatEntity
struct CombatEntity_t684137495;
// System.Object[]
struct ObjectU5BU5D_t1108656482;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_AnimationRunner_AniState1127540784.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_AnimationRunner_AniType1006238651.h"
#include "UnityEngine_UnityEngine_WrapMode1491636113.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_AnimationRunner1015409588.h"
#include "AssemblyU2DCSharp_ReplayMgr1549183121.h"
#include "AssemblyU2DCSharp_ReplayExecuteType3106517448.h"
#include "AssemblyU2DCSharp_CombatEntity684137495.h"

// System.Void AnimationRunner::.ctor()
extern "C"  void AnimationRunner__ctor_m163431991 (AnimationRunner_t1015409588 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimationRunner::set_aniState(AnimationRunner/AniState)
extern "C"  void AnimationRunner_set_aniState_m3547120303 (AnimationRunner_t1015409588 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// AnimationRunner/AniState AnimationRunner::get_aniState()
extern "C"  int32_t AnimationRunner_get_aniState_m995524926 (AnimationRunner_t1015409588 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single AnimationRunner::get_PlayTime()
extern "C"  float AnimationRunner_get_PlayTime_m509557323 (AnimationRunner_t1015409588 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimationRunner::set_PlayTime(System.Single)
extern "C"  void AnimationRunner_set_PlayTime_m220252032 (AnimationRunner_t1015409588 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimationRunner::Awake()
extern "C"  void AnimationRunner_Awake_m401037210 (AnimationRunner_t1015409588 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimationRunner::Update()
extern "C"  void AnimationRunner_Update_m2498286518 (AnimationRunner_t1015409588 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimationRunner::SetRunSpeed(System.Single)
extern "C"  void AnimationRunner_SetRunSpeed_m3894110392 (AnimationRunner_t1015409588 * __this, float ___runSpeed0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AnimationRunner::HasExistAni(System.String)
extern "C"  bool AnimationRunner_HasExistAni_m1981964186 (AnimationRunner_t1015409588 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimationRunner::Play(System.String,System.Single,System.Boolean)
extern "C"  void AnimationRunner_Play_m1674961751 (AnimationRunner_t1015409588 * __this, String_t* ___aniName0, float ___playTime1, bool ___isSkill2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimationRunner::Play(System.Boolean,System.String,System.Single)
extern "C"  void AnimationRunner_Play_m4217163833 (AnimationRunner_t1015409588 * __this, bool ___isLoop0, String_t* ___aniName1, float ___playTime2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimationRunner::Play(AnimationRunner/AniType,System.Single,System.Boolean)
extern "C"  void AnimationRunner_Play_m1058383650 (AnimationRunner_t1015409588 * __this, int32_t ___aniType0, float ___playTime1, bool ___isskill2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AnimationRunner::IsPlay(AnimationRunner/AniType)
extern "C"  bool AnimationRunner_IsPlay_m1751746368 (AnimationRunner_t1015409588 * __this, int32_t ___aniType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimationRunner::SetActionTime(System.String,System.Single)
extern "C"  void AnimationRunner_SetActionTime_m3607676909 (AnimationRunner_t1015409588 * __this, String_t* ___aniName0, float ___playTime1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimationRunner::SetWrapModel(UnityEngine.WrapMode)
extern "C"  void AnimationRunner_SetWrapModel_m992123494 (AnimationRunner_t1015409588 * __this, int32_t ___wrap0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single AnimationRunner::GetActionTime(AnimationRunner/AniType)
extern "C"  float AnimationRunner_GetActionTime_m4074728669 (AnimationRunner_t1015409588 * __this, int32_t ___aniType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single AnimationRunner::GetActionTime()
extern "C"  float AnimationRunner_GetActionTime_m155079674 (AnimationRunner_t1015409588 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single AnimationRunner::GetActionTime(System.String)
extern "C"  float AnimationRunner_GetActionTime_m3168342344 (AnimationRunner_t1015409588 * __this, String_t* ___aniName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single AnimationRunner::GetActionNormalizedTime(AnimationRunner/AniType)
extern "C"  float AnimationRunner_GetActionNormalizedTime_m2024171558 (AnimationRunner_t1015409588 * __this, int32_t ___aniType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimationRunner::playOver()
extern "C"  void AnimationRunner_playOver_m4035643957 (AnimationRunner_t1015409588 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimationRunner::Clear()
extern "C"  void AnimationRunner_Clear_m1864532578 (AnimationRunner_t1015409588 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimationRunner::ilo_Log1(System.Object,System.Boolean)
extern "C"  void AnimationRunner_ilo_Log1_m2687343158 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimationRunner::ilo_playOver2(AnimationRunner)
extern "C"  void AnimationRunner_ilo_playOver2_m3851899090 (Il2CppObject * __this /* static, unused */, AnimationRunner_t1015409588 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single AnimationRunner::ilo_GetActionTime3(AnimationRunner,System.String)
extern "C"  float AnimationRunner_ilo_GetActionTime3_m2751427564 (Il2CppObject * __this /* static, unused */, AnimationRunner_t1015409588 * ____this0, String_t* ___aniName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimationRunner::ilo_set_PlayTime4(AnimationRunner,System.Single)
extern "C"  void AnimationRunner_ilo_set_PlayTime4_m2658972107 (Il2CppObject * __this /* static, unused */, AnimationRunner_t1015409588 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ReplayMgr AnimationRunner::ilo_get_Instance5()
extern "C"  ReplayMgr_t1549183121 * AnimationRunner_ilo_get_Instance5_m617596027 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimationRunner::ilo_AddData6(ReplayMgr,ReplayExecuteType,CombatEntity,System.Object[])
extern "C"  void AnimationRunner_ilo_AddData6_m383934657 (Il2CppObject * __this /* static, unused */, ReplayMgr_t1549183121 * ____this0, int32_t ___type1, CombatEntity_t684137495 * ___entity2, ObjectU5BU5D_t1108656482* ___datas3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AnimationRunner::ilo_HasExistAni7(AnimationRunner,System.String)
extern "C"  bool AnimationRunner_ilo_HasExistAni7_m1189242518 (Il2CppObject * __this /* static, unused */, AnimationRunner_t1015409588 * ____this0, String_t* ___name1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single AnimationRunner::ilo_get_PlayTime8(AnimationRunner)
extern "C"  float AnimationRunner_ilo_get_PlayTime8_m1209890338 (Il2CppObject * __this /* static, unused */, AnimationRunner_t1015409588 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimationRunner::ilo_Play9(AnimationRunner,AnimationRunner/AniType,System.Single,System.Boolean)
extern "C"  void AnimationRunner_ilo_Play9_m544441296 (Il2CppObject * __this /* static, unused */, AnimationRunner_t1015409588 * ____this0, int32_t ___aniType1, float ___playTime2, bool ___isskill3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
