﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// JSMgr/MethodCallBackInfo
struct MethodCallBackInfo_t2808125524;
// JSMgr/CSCallbackMethod
struct CSCallbackMethod_t4136956950;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSMgr_CSCallbackMethod4136956950.h"
#include "mscorlib_System_String7231557.h"

// System.Void JSMgr/MethodCallBackInfo::.ctor(JSMgr/CSCallbackMethod,System.String)
extern "C"  void MethodCallBackInfo__ctor_m2789893997 (MethodCallBackInfo_t2808125524 * __this, CSCallbackMethod_t4136956950 * ___f0, String_t* ___n1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
