﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_lorhissu239
struct M_lorhissu239_t38157747;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_lorhissu23938157747.h"

// System.Void GarbageiOS.M_lorhissu239::.ctor()
extern "C"  void M_lorhissu239__ctor_m2459105040 (M_lorhissu239_t38157747 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_lorhissu239::M_zoustetaw0(System.String[],System.Int32)
extern "C"  void M_lorhissu239_M_zoustetaw0_m3614302855 (M_lorhissu239_t38157747 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_lorhissu239::M_raballtaWousowho1(System.String[],System.Int32)
extern "C"  void M_lorhissu239_M_raballtaWousowho1_m2274052644 (M_lorhissu239_t38157747 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_lorhissu239::M_stocaCorjirnair2(System.String[],System.Int32)
extern "C"  void M_lorhissu239_M_stocaCorjirnair2_m4281745594 (M_lorhissu239_t38157747 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_lorhissu239::M_stimisnear3(System.String[],System.Int32)
extern "C"  void M_lorhissu239_M_stimisnear3_m2656819791 (M_lorhissu239_t38157747 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_lorhissu239::M_tacerbooDeata4(System.String[],System.Int32)
extern "C"  void M_lorhissu239_M_tacerbooDeata4_m3578681699 (M_lorhissu239_t38157747 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_lorhissu239::M_jaleata5(System.String[],System.Int32)
extern "C"  void M_lorhissu239_M_jaleata5_m42132820 (M_lorhissu239_t38157747 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_lorhissu239::M_nerexe6(System.String[],System.Int32)
extern "C"  void M_lorhissu239_M_nerexe6_m2302228946 (M_lorhissu239_t38157747 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_lorhissu239::ilo_M_zoustetaw01(GarbageiOS.M_lorhissu239,System.String[],System.Int32)
extern "C"  void M_lorhissu239_ilo_M_zoustetaw01_m2359804152 (Il2CppObject * __this /* static, unused */, M_lorhissu239_t38157747 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_lorhissu239::ilo_M_raballtaWousowho12(GarbageiOS.M_lorhissu239,System.String[],System.Int32)
extern "C"  void M_lorhissu239_ilo_M_raballtaWousowho12_m1871506286 (Il2CppObject * __this /* static, unused */, M_lorhissu239_t38157747 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_lorhissu239::ilo_M_stocaCorjirnair23(GarbageiOS.M_lorhissu239,System.String[],System.Int32)
extern "C"  void M_lorhissu239_ilo_M_stocaCorjirnair23_m3810339049 (Il2CppObject * __this /* static, unused */, M_lorhissu239_t38157747 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
