﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UI3DWidgetMgr
struct  UI3DWidgetMgr_t1313838767  : public Il2CppObject
{
public:

public:
};

struct UI3DWidgetMgr_t1313838767_StaticFields
{
public:
	// UnityEngine.GameObject UI3DWidgetMgr::widgetRoot
	GameObject_t3674682005 * ___widgetRoot_0;

public:
	inline static int32_t get_offset_of_widgetRoot_0() { return static_cast<int32_t>(offsetof(UI3DWidgetMgr_t1313838767_StaticFields, ___widgetRoot_0)); }
	inline GameObject_t3674682005 * get_widgetRoot_0() const { return ___widgetRoot_0; }
	inline GameObject_t3674682005 ** get_address_of_widgetRoot_0() { return &___widgetRoot_0; }
	inline void set_widgetRoot_0(GameObject_t3674682005 * value)
	{
		___widgetRoot_0 = value;
		Il2CppCodeGenWriteBarrier(&___widgetRoot_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
