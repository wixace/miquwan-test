﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.EuclideanEmbedding/<RecalculateCosts>c__AnonStorey120/<RecalculateCosts>c__AnonStorey11E
struct U3CRecalculateCostsU3Ec__AnonStorey11E_t853919929;
// Pathfinding.Path
struct Path_t1974241691;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_Path1974241691.h"

// System.Void Pathfinding.EuclideanEmbedding/<RecalculateCosts>c__AnonStorey120/<RecalculateCosts>c__AnonStorey11E::.ctor()
extern "C"  void U3CRecalculateCostsU3Ec__AnonStorey11E__ctor_m1363304962 (U3CRecalculateCostsU3Ec__AnonStorey11E_t853919929 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.EuclideanEmbedding/<RecalculateCosts>c__AnonStorey120/<RecalculateCosts>c__AnonStorey11E::<>m__353(Pathfinding.Path)
extern "C"  void U3CRecalculateCostsU3Ec__AnonStorey11E_U3CU3Em__353_m1962751065 (U3CRecalculateCostsU3Ec__AnonStorey11E_t853919929 * __this, Path_t1974241691 * ____p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
