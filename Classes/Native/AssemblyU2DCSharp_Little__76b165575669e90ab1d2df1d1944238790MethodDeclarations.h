﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._76b165575669e90ab1d2df1d191c9aef
struct _76b165575669e90ab1d2df1d191c9aef_t944238790;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__76b165575669e90ab1d2df1d1944238790.h"

// System.Void Little._76b165575669e90ab1d2df1d191c9aef::.ctor()
extern "C"  void _76b165575669e90ab1d2df1d191c9aef__ctor_m2922820647 (_76b165575669e90ab1d2df1d191c9aef_t944238790 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._76b165575669e90ab1d2df1d191c9aef::_76b165575669e90ab1d2df1d191c9aefm2(System.Int32)
extern "C"  int32_t _76b165575669e90ab1d2df1d191c9aef__76b165575669e90ab1d2df1d191c9aefm2_m887237593 (_76b165575669e90ab1d2df1d191c9aef_t944238790 * __this, int32_t ____76b165575669e90ab1d2df1d191c9aefa0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._76b165575669e90ab1d2df1d191c9aef::_76b165575669e90ab1d2df1d191c9aefm(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _76b165575669e90ab1d2df1d191c9aef__76b165575669e90ab1d2df1d191c9aefm_m59278077 (_76b165575669e90ab1d2df1d191c9aef_t944238790 * __this, int32_t ____76b165575669e90ab1d2df1d191c9aefa0, int32_t ____76b165575669e90ab1d2df1d191c9aef331, int32_t ____76b165575669e90ab1d2df1d191c9aefc2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._76b165575669e90ab1d2df1d191c9aef::ilo__76b165575669e90ab1d2df1d191c9aefm21(Little._76b165575669e90ab1d2df1d191c9aef,System.Int32)
extern "C"  int32_t _76b165575669e90ab1d2df1d191c9aef_ilo__76b165575669e90ab1d2df1d191c9aefm21_m509870413 (Il2CppObject * __this /* static, unused */, _76b165575669e90ab1d2df1d191c9aef_t944238790 * ____this0, int32_t ____76b165575669e90ab1d2df1d191c9aefa1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
