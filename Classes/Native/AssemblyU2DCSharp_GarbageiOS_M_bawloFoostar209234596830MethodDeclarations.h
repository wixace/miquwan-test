﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_bawloFoostar209
struct M_bawloFoostar209_t234596830;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_bawloFoostar209234596830.h"

// System.Void GarbageiOS.M_bawloFoostar209::.ctor()
extern "C"  void M_bawloFoostar209__ctor_m2028748613 (M_bawloFoostar209_t234596830 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_bawloFoostar209::M_tuwee0(System.String[],System.Int32)
extern "C"  void M_bawloFoostar209_M_tuwee0_m1171284450 (M_bawloFoostar209_t234596830 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_bawloFoostar209::M_conaskePujou1(System.String[],System.Int32)
extern "C"  void M_bawloFoostar209_M_conaskePujou1_m3570729948 (M_bawloFoostar209_t234596830 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_bawloFoostar209::M_rokai2(System.String[],System.Int32)
extern "C"  void M_bawloFoostar209_M_rokai2_m1777590564 (M_bawloFoostar209_t234596830 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_bawloFoostar209::ilo_M_tuwee01(GarbageiOS.M_bawloFoostar209,System.String[],System.Int32)
extern "C"  void M_bawloFoostar209_ilo_M_tuwee01_m1482354046 (Il2CppObject * __this /* static, unused */, M_bawloFoostar209_t234596830 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
