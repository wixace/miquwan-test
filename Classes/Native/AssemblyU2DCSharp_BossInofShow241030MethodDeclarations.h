﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BossInofShow
struct BossInofShow_t241030;
// System.String
struct String_t;
// UISprite
struct UISprite_t661437049;
// UITweener
struct UITweener_t105489188;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_UISprite661437049.h"
#include "AssemblyU2DCSharp_UITweener105489188.h"

// System.Void BossInofShow::.ctor()
extern "C"  void BossInofShow__ctor_m1527258069 (BossInofShow_t241030 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BossInofShow::Awake()
extern "C"  void BossInofShow_Awake_m1764863288 (BossInofShow_t241030 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BossInofShow::OnDestroy()
extern "C"  void BossInofShow_OnDestroy_m2936929870 (BossInofShow_t241030 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BossInofShow::SetName(System.String,System.Int32)
extern "C"  void BossInofShow_SetName_m2680195157 (BossInofShow_t241030 * __this, String_t* ___name0, int32_t ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BossInofShow::PlayForward()
extern "C"  void BossInofShow_PlayForward_m3948292132 (BossInofShow_t241030 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BossInofShow::PlayReverse()
extern "C"  void BossInofShow_PlayReverse_m2240894209 (BossInofShow_t241030 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BossInofShow::End()
extern "C"  void BossInofShow_End_m3209728910 (BossInofShow_t241030 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BossInofShow::Hide()
extern "C"  void BossInofShow_Hide_m798676881 (BossInofShow_t241030 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BossInofShow::Show()
extern "C"  void BossInofShow_Show_m1113019020 (BossInofShow_t241030 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BossInofShow::ilo_MakePixelPerfect1(UISprite)
extern "C"  void BossInofShow_ilo_MakePixelPerfect1_m2818960803 (Il2CppObject * __this /* static, unused */, UISprite_t661437049 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BossInofShow::ilo_set_spriteName2(UISprite,System.String)
extern "C"  void BossInofShow_ilo_set_spriteName2_m2868532552 (Il2CppObject * __this /* static, unused */, UISprite_t661437049 * ____this0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BossInofShow::ilo_PlayForward3(UITweener)
extern "C"  void BossInofShow_ilo_PlayForward3_m3435623530 (Il2CppObject * __this /* static, unused */, UITweener_t105489188 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
