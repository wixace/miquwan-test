﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_Texture2DGenerated
struct UnityEngine_Texture2DGenerated_t1750706890;
// JSVCall
struct JSVCall_t3708497963;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// UnityEngine.Texture2D[]
struct Texture2DU5BU5D_t2376705138;
// UnityEngine.Color[]
struct ColorU5BU5D_t2441545636;
// UnityEngine.Color32[]
struct Color32U5BU5D_t2960766953;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void UnityEngine_Texture2DGenerated::.ctor()
extern "C"  void UnityEngine_Texture2DGenerated__ctor_m3520296209 (UnityEngine_Texture2DGenerated_t1750706890 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Texture2DGenerated::Texture2D_Texture2D1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Texture2DGenerated_Texture2D_Texture2D1_m443774573 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Texture2DGenerated::Texture2D_Texture2D2(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Texture2DGenerated_Texture2D_Texture2D2_m1688539054 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Texture2DGenerated::Texture2D_Texture2D3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Texture2DGenerated_Texture2D_Texture2D3_m2933303535 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Texture2DGenerated::Texture2D_mipmapCount(JSVCall)
extern "C"  void UnityEngine_Texture2DGenerated_Texture2D_mipmapCount_m2231318883 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Texture2DGenerated::Texture2D_format(JSVCall)
extern "C"  void UnityEngine_Texture2DGenerated_Texture2D_format_m611368331 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Texture2DGenerated::Texture2D_whiteTexture(JSVCall)
extern "C"  void UnityEngine_Texture2DGenerated_Texture2D_whiteTexture_m1666384432 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Texture2DGenerated::Texture2D_blackTexture(JSVCall)
extern "C"  void UnityEngine_Texture2DGenerated_Texture2D_blackTexture_m1648777734 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Texture2DGenerated::Texture2D_Apply__Boolean__Boolean(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Texture2DGenerated_Texture2D_Apply__Boolean__Boolean_m4199482183 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Texture2DGenerated::Texture2D_Apply__Boolean(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Texture2DGenerated_Texture2D_Apply__Boolean_m2701104451 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Texture2DGenerated::Texture2D_Apply(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Texture2DGenerated_Texture2D_Apply_m353343431 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Texture2DGenerated::Texture2D_Compress__Boolean(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Texture2DGenerated_Texture2D_Compress__Boolean_m3163227487 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Texture2DGenerated::Texture2D_EncodeToJPG__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Texture2DGenerated_Texture2D_EncodeToJPG__Int32_m1593698151 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Texture2DGenerated::Texture2D_EncodeToJPG(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Texture2DGenerated_Texture2D_EncodeToJPG_m2389482441 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Texture2DGenerated::Texture2D_EncodeToPNG(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Texture2DGenerated_Texture2D_EncodeToPNG_m2945141777 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Texture2DGenerated::Texture2D_GetPixel__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Texture2DGenerated_Texture2D_GetPixel__Int32__Int32_m35472729 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Texture2DGenerated::Texture2D_GetPixelBilinear__Single__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Texture2DGenerated_Texture2D_GetPixelBilinear__Single__Single_m1894994165 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Texture2DGenerated::Texture2D_GetPixels__Int32__Int32__Int32__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Texture2DGenerated_Texture2D_GetPixels__Int32__Int32__Int32__Int32__Int32_m2668053908 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Texture2DGenerated::Texture2D_GetPixels__Int32__Int32__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Texture2DGenerated_Texture2D_GetPixels__Int32__Int32__Int32__Int32_m1056646716 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Texture2DGenerated::Texture2D_GetPixels__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Texture2DGenerated_Texture2D_GetPixels__Int32_m1388175252 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Texture2DGenerated::Texture2D_GetPixels(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Texture2DGenerated_Texture2D_GetPixels_m3827158076 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Texture2DGenerated::Texture2D_GetPixels32__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Texture2DGenerated_Texture2D_GetPixels32__Int32_m4050878549 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Texture2DGenerated::Texture2D_GetPixels32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Texture2DGenerated_Texture2D_GetPixels32_m257757851 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Texture2DGenerated::Texture2D_GetRawTextureData(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Texture2DGenerated_Texture2D_GetRawTextureData_m1133639084 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Texture2DGenerated::Texture2D_LoadImage__Byte_Array__Boolean(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Texture2DGenerated_Texture2D_LoadImage__Byte_Array__Boolean_m3291295418 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Texture2DGenerated::Texture2D_LoadImage__Byte_Array(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Texture2DGenerated_Texture2D_LoadImage__Byte_Array_m3326356592 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Texture2DGenerated::Texture2D_LoadRawTextureData__IntPtr__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Texture2DGenerated_Texture2D_LoadRawTextureData__IntPtr__Int32_m2629710853 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Texture2DGenerated::Texture2D_LoadRawTextureData__Byte_Array(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Texture2DGenerated_Texture2D_LoadRawTextureData__Byte_Array_m786387694 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Texture2DGenerated::Texture2D_PackTextures__Texture2D_Array__Int32__Int32__Boolean(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Texture2DGenerated_Texture2D_PackTextures__Texture2D_Array__Int32__Int32__Boolean_m2844135323 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Texture2DGenerated::Texture2D_PackTextures__Texture2D_Array__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Texture2DGenerated_Texture2D_PackTextures__Texture2D_Array__Int32__Int32_m2807929455 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Texture2DGenerated::Texture2D_PackTextures__Texture2D_Array__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Texture2DGenerated_Texture2D_PackTextures__Texture2D_Array__Int32_m195871169 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Texture2DGenerated::Texture2D_ReadPixels__Rect__Int32__Int32__Boolean(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Texture2DGenerated_Texture2D_ReadPixels__Rect__Int32__Int32__Boolean_m2817084058 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Texture2DGenerated::Texture2D_ReadPixels__Rect__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Texture2DGenerated_Texture2D_ReadPixels__Rect__Int32__Int32_m3449762960 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Texture2DGenerated::Texture2D_Resize__Int32__Int32__TextureFormat__Boolean(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Texture2DGenerated_Texture2D_Resize__Int32__Int32__TextureFormat__Boolean_m3932190771 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Texture2DGenerated::Texture2D_Resize__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Texture2DGenerated_Texture2D_Resize__Int32__Int32_m2031400253 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Texture2DGenerated::Texture2D_SetPixel__Int32__Int32__Color(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Texture2DGenerated_Texture2D_SetPixel__Int32__Int32__Color_m3394245624 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Texture2DGenerated::Texture2D_SetPixels__Int32__Int32__Int32__Int32__Color_Array__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Texture2DGenerated_Texture2D_SetPixels__Int32__Int32__Int32__Int32__Color_Array__Int32_m1003272345 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Texture2DGenerated::Texture2D_SetPixels__Int32__Int32__Int32__Int32__Color_Array(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Texture2DGenerated_Texture2D_SetPixels__Int32__Int32__Int32__Int32__Color_Array_m2259476183 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Texture2DGenerated::Texture2D_SetPixels__Color_Array__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Texture2DGenerated_Texture2D_SetPixels__Color_Array__Int32_m4290455705 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Texture2DGenerated::Texture2D_SetPixels__Color_Array(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Texture2DGenerated_Texture2D_SetPixels__Color_Array_m2702995159 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Texture2DGenerated::Texture2D_SetPixels32__Int32__Int32__Int32__Int32__Color32_Array__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Texture2DGenerated_Texture2D_SetPixels32__Int32__Int32__Int32__Int32__Color32_Array__Int32_m210898553 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Texture2DGenerated::Texture2D_SetPixels32__Int32__Int32__Int32__Int32__Color32_Array(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Texture2DGenerated_Texture2D_SetPixels32__Int32__Int32__Int32__Int32__Color32_Array_m15361271 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Texture2DGenerated::Texture2D_SetPixels32__Color32_Array__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Texture2DGenerated_Texture2D_SetPixels32__Color32_Array__Int32_m1841746681 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Texture2DGenerated::Texture2D_SetPixels32__Color32_Array(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Texture2DGenerated_Texture2D_SetPixels32__Color32_Array_m781299831 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Texture2DGenerated::Texture2D_UpdateExternalTexture__IntPtr(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Texture2DGenerated_Texture2D_UpdateExternalTexture__IntPtr_m2447457951 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Texture2DGenerated::Texture2D_CreateExternalTexture__Int32__Int32__TextureFormat__Boolean__Boolean__IntPtr(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Texture2DGenerated_Texture2D_CreateExternalTexture__Int32__Int32__TextureFormat__Boolean__Boolean__IntPtr_m1255733638 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Texture2DGenerated::__Register()
extern "C"  void UnityEngine_Texture2DGenerated___Register_m2209450582 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] UnityEngine_Texture2DGenerated::<Texture2D_LoadImage__Byte_Array__Boolean>m__2D9()
extern "C"  ByteU5BU5D_t4260760469* UnityEngine_Texture2DGenerated_U3CTexture2D_LoadImage__Byte_Array__BooleanU3Em__2D9_m220482166 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] UnityEngine_Texture2DGenerated::<Texture2D_LoadImage__Byte_Array>m__2DA()
extern "C"  ByteU5BU5D_t4260760469* UnityEngine_Texture2DGenerated_U3CTexture2D_LoadImage__Byte_ArrayU3Em__2DA_m358420540 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] UnityEngine_Texture2DGenerated::<Texture2D_LoadRawTextureData__Byte_Array>m__2DB()
extern "C"  ByteU5BU5D_t4260760469* UnityEngine_Texture2DGenerated_U3CTexture2D_LoadRawTextureData__Byte_ArrayU3Em__2DB_m3460793227 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture2D[] UnityEngine_Texture2DGenerated::<Texture2D_PackTextures__Texture2D_Array__Int32__Int32__Boolean>m__2DC()
extern "C"  Texture2DU5BU5D_t2376705138* UnityEngine_Texture2DGenerated_U3CTexture2D_PackTextures__Texture2D_Array__Int32__Int32__BooleanU3Em__2DC_m2685297648 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture2D[] UnityEngine_Texture2DGenerated::<Texture2D_PackTextures__Texture2D_Array__Int32__Int32>m__2DD()
extern "C"  Texture2DU5BU5D_t2376705138* UnityEngine_Texture2DGenerated_U3CTexture2D_PackTextures__Texture2D_Array__Int32__Int32U3Em__2DD_m2917398927 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture2D[] UnityEngine_Texture2DGenerated::<Texture2D_PackTextures__Texture2D_Array__Int32>m__2DE()
extern "C"  Texture2DU5BU5D_t2376705138* UnityEngine_Texture2DGenerated_U3CTexture2D_PackTextures__Texture2D_Array__Int32U3Em__2DE_m3707830668 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color[] UnityEngine_Texture2DGenerated::<Texture2D_SetPixels__Int32__Int32__Int32__Int32__Color_Array__Int32>m__2DF()
extern "C"  ColorU5BU5D_t2441545636* UnityEngine_Texture2DGenerated_U3CTexture2D_SetPixels__Int32__Int32__Int32__Int32__Color_Array__Int32U3Em__2DF_m938544285 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color[] UnityEngine_Texture2DGenerated::<Texture2D_SetPixels__Int32__Int32__Int32__Int32__Color_Array>m__2E0()
extern "C"  ColorU5BU5D_t2441545636* UnityEngine_Texture2DGenerated_U3CTexture2D_SetPixels__Int32__Int32__Int32__Int32__Color_ArrayU3Em__2E0_m268747018 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color[] UnityEngine_Texture2DGenerated::<Texture2D_SetPixels__Color_Array__Int32>m__2E1()
extern "C"  ColorU5BU5D_t2441545636* UnityEngine_Texture2DGenerated_U3CTexture2D_SetPixels__Color_Array__Int32U3Em__2E1_m3544743 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color[] UnityEngine_Texture2DGenerated::<Texture2D_SetPixels__Color_Array>m__2E2()
extern "C"  ColorU5BU5D_t2441545636* UnityEngine_Texture2DGenerated_U3CTexture2D_SetPixels__Color_ArrayU3Em__2E2_m1518334988 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color32[] UnityEngine_Texture2DGenerated::<Texture2D_SetPixels32__Int32__Int32__Int32__Int32__Color32_Array__Int32>m__2E3()
extern "C"  Color32U5BU5D_t2960766953* UnityEngine_Texture2DGenerated_U3CTexture2D_SetPixels32__Int32__Int32__Int32__Int32__Color32_Array__Int32U3Em__2E3_m714308840 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color32[] UnityEngine_Texture2DGenerated::<Texture2D_SetPixels32__Int32__Int32__Int32__Int32__Color32_Array>m__2E4()
extern "C"  Color32U5BU5D_t2960766953* UnityEngine_Texture2DGenerated_U3CTexture2D_SetPixels32__Int32__Int32__Int32__Int32__Color32_ArrayU3Em__2E4_m3401983407 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color32[] UnityEngine_Texture2DGenerated::<Texture2D_SetPixels32__Color32_Array__Int32>m__2E5()
extern "C"  Color32U5BU5D_t2960766953* UnityEngine_Texture2DGenerated_U3CTexture2D_SetPixels32__Color32_Array__Int32U3Em__2E5_m2179160298 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color32[] UnityEngine_Texture2DGenerated::<Texture2D_SetPixels32__Color32_Array>m__2E6()
extern "C"  Color32U5BU5D_t2960766953* UnityEngine_Texture2DGenerated_U3CTexture2D_SetPixels32__Color32_ArrayU3Em__2E6_m866412721 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_Texture2DGenerated::ilo_getInt321(System.Int32)
extern "C"  int32_t UnityEngine_Texture2DGenerated_ilo_getInt321_m634644312 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_Texture2DGenerated::ilo_getEnum2(System.Int32)
extern "C"  int32_t UnityEngine_Texture2DGenerated_ilo_getEnum2_m4120229280 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Texture2DGenerated::ilo_setInt323(System.Int32,System.Int32)
extern "C"  void UnityEngine_Texture2DGenerated_ilo_setInt323_m2778518435 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Texture2DGenerated::ilo_getBooleanS4(System.Int32)
extern "C"  bool UnityEngine_Texture2DGenerated_ilo_getBooleanS4_m570397790 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Texture2DGenerated::ilo_setByte5(System.Int32,System.Byte)
extern "C"  void UnityEngine_Texture2DGenerated_ilo_setByte5_m1280841863 (Il2CppObject * __this /* static, unused */, int32_t ___e0, uint8_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Texture2DGenerated::ilo_setArrayS6(System.Int32,System.Int32,System.Boolean)
extern "C"  void UnityEngine_Texture2DGenerated_ilo_setArrayS6_m1172450775 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___count1, bool ___bClear2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine_Texture2DGenerated::ilo_getSingle7(System.Int32)
extern "C"  float UnityEngine_Texture2DGenerated_ilo_getSingle7_m4210475324 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_Texture2DGenerated::ilo_setObject8(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UnityEngine_Texture2DGenerated_ilo_setObject8_m2628075872 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Texture2DGenerated::ilo_moveSaveID2Arr9(System.Int32)
extern "C"  void UnityEngine_Texture2DGenerated_ilo_moveSaveID2Arr9_m1261179374 (Il2CppObject * __this /* static, unused */, int32_t ___arrIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_Texture2DGenerated::ilo_getObject10(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_Texture2DGenerated_ilo_getObject10_m1956903426 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 UnityEngine_Texture2DGenerated::ilo_getIntPtr11(System.Int32)
extern "C"  int64_t UnityEngine_Texture2DGenerated_ilo_getIntPtr11_m2130443571 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte UnityEngine_Texture2DGenerated::ilo_getByte12(System.Int32)
extern "C"  uint8_t UnityEngine_Texture2DGenerated_ilo_getByte12_m3322327776 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_Texture2DGenerated::ilo_getElement13(System.Int32,System.Int32)
extern "C"  int32_t UnityEngine_Texture2DGenerated_ilo_getElement13_m906051274 (Il2CppObject * __this /* static, unused */, int32_t ___id0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_Texture2DGenerated::ilo_getArrayLength14(System.Int32)
extern "C"  int32_t UnityEngine_Texture2DGenerated_ilo_getArrayLength14_m1877958801 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_Texture2DGenerated::ilo_getObject15(System.Int32)
extern "C"  int32_t UnityEngine_Texture2DGenerated_ilo_getObject15_m2053754072 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
