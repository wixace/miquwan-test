﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BehaviourUtil/<DelayCallExec>c__Iterator3E
struct U3CDelayCallExecU3Ec__Iterator3E_t1083257579;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void BehaviourUtil/<DelayCallExec>c__Iterator3E::.ctor()
extern "C"  void U3CDelayCallExecU3Ec__Iterator3E__ctor_m2312576528 (U3CDelayCallExecU3Ec__Iterator3E_t1083257579 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object BehaviourUtil/<DelayCallExec>c__Iterator3E::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDelayCallExecU3Ec__Iterator3E_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m461993868 (U3CDelayCallExecU3Ec__Iterator3E_t1083257579 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object BehaviourUtil/<DelayCallExec>c__Iterator3E::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDelayCallExecU3Ec__Iterator3E_System_Collections_IEnumerator_get_Current_m3956544288 (U3CDelayCallExecU3Ec__Iterator3E_t1083257579 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BehaviourUtil/<DelayCallExec>c__Iterator3E::MoveNext()
extern "C"  bool U3CDelayCallExecU3Ec__Iterator3E_MoveNext_m1625521356 (U3CDelayCallExecU3Ec__Iterator3E_t1083257579 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BehaviourUtil/<DelayCallExec>c__Iterator3E::Dispose()
extern "C"  void U3CDelayCallExecU3Ec__Iterator3E_Dispose_m1786234445 (U3CDelayCallExecU3Ec__Iterator3E_t1083257579 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BehaviourUtil/<DelayCallExec>c__Iterator3E::Reset()
extern "C"  void U3CDelayCallExecU3Ec__Iterator3E_Reset_m4253976765 (U3CDelayCallExecU3Ec__Iterator3E_t1083257579 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
