﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Float_hero_attribute
struct Float_hero_attribute_t4099817722;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// FloatTextUnit
struct FloatTextUnit_t2362298029;
// EventDelegate/Callback
struct Callback_t1094463061;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "AssemblyU2DCSharp_FLOAT_TEXT_ID2199345770.h"
#include "AssemblyU2DCSharp_FloatTextUnit2362298029.h"
#include "AssemblyU2DCSharp_EventDelegate_Callback1094463061.h"
#include "AssemblyU2DCSharp_UITweener_Method1078127180.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void Float_hero_attribute::.ctor()
extern "C"  void Float_hero_attribute__ctor_m411734753 (Float_hero_attribute_t4099817722 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_hero_attribute::OnAwake(UnityEngine.GameObject)
extern "C"  void Float_hero_attribute_OnAwake_m3942261661 (Float_hero_attribute_t4099817722 * __this, GameObject_t3674682005 * ___viewGO0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_hero_attribute::OnDestroy()
extern "C"  void Float_hero_attribute_OnDestroy_m1764101978 (Float_hero_attribute_t4099817722 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FLOAT_TEXT_ID Float_hero_attribute::FloatID()
extern "C"  int32_t Float_hero_attribute_FloatID_m3469605485 (Float_hero_attribute_t4099817722 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_hero_attribute::Init()
extern "C"  void Float_hero_attribute_Init_m4121238355 (Float_hero_attribute_t4099817722 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_hero_attribute::SetFile(System.Object[])
extern "C"  void Float_hero_attribute_SetFile_m2157972501 (Float_hero_attribute_t4099817722 * __this, ObjectU5BU5D_t1108656482* ___args0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_hero_attribute::ilo_OnAwake1(FloatTextUnit,UnityEngine.GameObject)
extern "C"  void Float_hero_attribute_ilo_OnAwake1_m3061280442 (Il2CppObject * __this /* static, unused */, FloatTextUnit_t2362298029 * ____this0, GameObject_t3674682005 * ___viewGO1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_hero_attribute::ilo_MoveScale2(FloatTextUnit,System.Single,System.Single,System.Single,System.Single,EventDelegate/Callback,UnityEngine.GameObject,UITweener/Method)
extern "C"  void Float_hero_attribute_ilo_MoveScale2_m1805103427 (Il2CppObject * __this /* static, unused */, FloatTextUnit_t2362298029 * ____this0, float ___from1, float ___to2, float ___duration3, float ___delay4, Callback_t1094463061 * ___call5, GameObject_t3674682005 * ___go6, int32_t ___method7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_hero_attribute::ilo_MovePosition3(FloatTextUnit,UnityEngine.Vector3,System.Single,System.Single,System.Single,EventDelegate/Callback,UnityEngine.GameObject,UITweener/Method)
extern "C"  void Float_hero_attribute_ilo_MovePosition3_m2866819659 (Il2CppObject * __this /* static, unused */, FloatTextUnit_t2362298029 * ____this0, Vector3_t4282066566  ___from1, float ___toUp2, float ___duration3, float ___delay4, Callback_t1094463061 * ___call5, GameObject_t3674682005 * ___go6, int32_t ___method7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
