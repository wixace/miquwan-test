﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// AnQuIOSSDKMsg
struct AnQuIOSSDKMsg_t2914021123;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AnQuIOSSDKMsg
struct  AnQuIOSSDKMsg_t2914021123  : public Il2CppObject
{
public:

public:
};

struct AnQuIOSSDKMsg_t2914021123_StaticFields
{
public:
	// AnQuIOSSDKMsg AnQuIOSSDKMsg::_inst
	AnQuIOSSDKMsg_t2914021123 * ____inst_0;

public:
	inline static int32_t get_offset_of__inst_0() { return static_cast<int32_t>(offsetof(AnQuIOSSDKMsg_t2914021123_StaticFields, ____inst_0)); }
	inline AnQuIOSSDKMsg_t2914021123 * get__inst_0() const { return ____inst_0; }
	inline AnQuIOSSDKMsg_t2914021123 ** get_address_of__inst_0() { return &____inst_0; }
	inline void set__inst_0(AnQuIOSSDKMsg_t2914021123 * value)
	{
		____inst_0 = value;
		Il2CppCodeGenWriteBarrier(&____inst_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
