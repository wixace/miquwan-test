﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.LinkedLevelCell
struct LinkedLevelCell_t935711695;

#include "codegen/il2cpp-codegen.h"

// System.Void Pathfinding.LinkedLevelCell::.ctor()
extern "C"  void LinkedLevelCell__ctor_m198387784 (LinkedLevelCell_t935711695 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
