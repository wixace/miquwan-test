﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIEventListenerGenerated/<UIEventListener_onDragOver_GetDelegate_member10_arg0>c__AnonStoreyBE
struct U3CUIEventListener_onDragOver_GetDelegate_member10_arg0U3Ec__AnonStoreyBE_t2868507767;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"

// System.Void UIEventListenerGenerated/<UIEventListener_onDragOver_GetDelegate_member10_arg0>c__AnonStoreyBE::.ctor()
extern "C"  void U3CUIEventListener_onDragOver_GetDelegate_member10_arg0U3Ec__AnonStoreyBE__ctor_m1231683076 (U3CUIEventListener_onDragOver_GetDelegate_member10_arg0U3Ec__AnonStoreyBE_t2868507767 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIEventListenerGenerated/<UIEventListener_onDragOver_GetDelegate_member10_arg0>c__AnonStoreyBE::<>m__140(UnityEngine.GameObject)
extern "C"  void U3CUIEventListener_onDragOver_GetDelegate_member10_arg0U3Ec__AnonStoreyBE_U3CU3Em__140_m1406008538 (U3CUIEventListener_onDragOver_GetDelegate_member10_arg0U3Ec__AnonStoreyBE_t2868507767 * __this, GameObject_t3674682005 * ___go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
