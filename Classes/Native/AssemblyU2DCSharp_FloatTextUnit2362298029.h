﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<UITweener>
struct List_1_t1473674740;
// UnityEngine.Transform
struct Transform_t1659122786;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FloatTextUnit
struct  FloatTextUnit_t2362298029  : public Il2CppObject
{
public:
	// System.Collections.Generic.List`1<UITweener> FloatTextUnit::tweenList
	List_1_t1473674740 * ___tweenList_0;
	// UnityEngine.Transform FloatTextUnit::mTransform
	Transform_t1659122786 * ___mTransform_1;
	// System.Boolean FloatTextUnit::<IsLife>k__BackingField
	bool ___U3CIsLifeU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_tweenList_0() { return static_cast<int32_t>(offsetof(FloatTextUnit_t2362298029, ___tweenList_0)); }
	inline List_1_t1473674740 * get_tweenList_0() const { return ___tweenList_0; }
	inline List_1_t1473674740 ** get_address_of_tweenList_0() { return &___tweenList_0; }
	inline void set_tweenList_0(List_1_t1473674740 * value)
	{
		___tweenList_0 = value;
		Il2CppCodeGenWriteBarrier(&___tweenList_0, value);
	}

	inline static int32_t get_offset_of_mTransform_1() { return static_cast<int32_t>(offsetof(FloatTextUnit_t2362298029, ___mTransform_1)); }
	inline Transform_t1659122786 * get_mTransform_1() const { return ___mTransform_1; }
	inline Transform_t1659122786 ** get_address_of_mTransform_1() { return &___mTransform_1; }
	inline void set_mTransform_1(Transform_t1659122786 * value)
	{
		___mTransform_1 = value;
		Il2CppCodeGenWriteBarrier(&___mTransform_1, value);
	}

	inline static int32_t get_offset_of_U3CIsLifeU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(FloatTextUnit_t2362298029, ___U3CIsLifeU3Ek__BackingField_2)); }
	inline bool get_U3CIsLifeU3Ek__BackingField_2() const { return ___U3CIsLifeU3Ek__BackingField_2; }
	inline bool* get_address_of_U3CIsLifeU3Ek__BackingField_2() { return &___U3CIsLifeU3Ek__BackingField_2; }
	inline void set_U3CIsLifeU3Ek__BackingField_2(bool value)
	{
		___U3CIsLifeU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
