﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CEvent_EventBaseGenerated
struct CEvent_EventBaseGenerated_t3443762828;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"

// System.Void CEvent_EventBaseGenerated::.ctor()
extern "C"  void CEvent_EventBaseGenerated__ctor_m3256171615 (CEvent_EventBaseGenerated_t3443762828 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CEvent_EventBaseGenerated::EventBase_EventBase1(JSVCall,System.Int32)
extern "C"  bool CEvent_EventBaseGenerated_EventBase_EventBase1_m3092683451 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CEvent_EventBaseGenerated::EventBase_name(JSVCall)
extern "C"  void CEvent_EventBaseGenerated_EventBase_name_m2779125539 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CEvent_EventBaseGenerated::EventBase_target(JSVCall)
extern "C"  void CEvent_EventBaseGenerated_EventBase_target_m2842350813 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CEvent_EventBaseGenerated::__Register()
extern "C"  void CEvent_EventBaseGenerated___Register_m2911773640 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CEvent_EventBaseGenerated::ilo_addJSCSRel1(System.Int32,System.Object)
extern "C"  void CEvent_EventBaseGenerated_ilo_addJSCSRel1_m167661467 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object CEvent_EventBaseGenerated::ilo_getWhatever2(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * CEvent_EventBaseGenerated_ilo_getWhatever2_m2334880164 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
