﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIWidgetGenerated/<UIWidget_onChange_GetDelegate_member0_arg0>c__AnonStoreyD4
struct U3CUIWidget_onChange_GetDelegate_member0_arg0U3Ec__AnonStoreyD4_t2391986797;

#include "codegen/il2cpp-codegen.h"

// System.Void UIWidgetGenerated/<UIWidget_onChange_GetDelegate_member0_arg0>c__AnonStoreyD4::.ctor()
extern "C"  void U3CUIWidget_onChange_GetDelegate_member0_arg0U3Ec__AnonStoreyD4__ctor_m3913136542 (U3CUIWidget_onChange_GetDelegate_member0_arg0U3Ec__AnonStoreyD4_t2391986797 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidgetGenerated/<UIWidget_onChange_GetDelegate_member0_arg0>c__AnonStoreyD4::<>m__16D()
extern "C"  void U3CUIWidget_onChange_GetDelegate_member0_arg0U3Ec__AnonStoreyD4_U3CU3Em__16D_m783375578 (U3CUIWidget_onChange_GetDelegate_member0_arg0U3Ec__AnonStoreyD4_t2391986797 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
