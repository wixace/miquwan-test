﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._14a06909483835f99da5d6fe02487f24
struct _14a06909483835f99da5d6fe02487f24_t1230981052;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._14a06909483835f99da5d6fe02487f24::.ctor()
extern "C"  void _14a06909483835f99da5d6fe02487f24__ctor_m1368576497 (_14a06909483835f99da5d6fe02487f24_t1230981052 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._14a06909483835f99da5d6fe02487f24::_14a06909483835f99da5d6fe02487f24m2(System.Int32)
extern "C"  int32_t _14a06909483835f99da5d6fe02487f24__14a06909483835f99da5d6fe02487f24m2_m3411154969 (_14a06909483835f99da5d6fe02487f24_t1230981052 * __this, int32_t ____14a06909483835f99da5d6fe02487f24a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._14a06909483835f99da5d6fe02487f24::_14a06909483835f99da5d6fe02487f24m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _14a06909483835f99da5d6fe02487f24__14a06909483835f99da5d6fe02487f24m_m116317885 (_14a06909483835f99da5d6fe02487f24_t1230981052 * __this, int32_t ____14a06909483835f99da5d6fe02487f24a0, int32_t ____14a06909483835f99da5d6fe02487f24241, int32_t ____14a06909483835f99da5d6fe02487f24c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
