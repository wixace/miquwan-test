﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_tearjarorZehi155
struct  M_tearjarorZehi155_t1280640825  : public Il2CppObject
{
public:
	// System.Int32 GarbageiOS.M_tearjarorZehi155::_jemawe
	int32_t ____jemawe_0;
	// System.Single GarbageiOS.M_tearjarorZehi155::_sejowRidreastur
	float ____sejowRidreastur_1;
	// System.Single GarbageiOS.M_tearjarorZehi155::_talljiwas
	float ____talljiwas_2;
	// System.Int32 GarbageiOS.M_tearjarorZehi155::_wheasoseYisci
	int32_t ____wheasoseYisci_3;
	// System.Boolean GarbageiOS.M_tearjarorZehi155::_fejouSerstayxi
	bool ____fejouSerstayxi_4;
	// System.Boolean GarbageiOS.M_tearjarorZehi155::_dalku
	bool ____dalku_5;
	// System.Int32 GarbageiOS.M_tearjarorZehi155::_joodor
	int32_t ____joodor_6;
	// System.String GarbageiOS.M_tearjarorZehi155::_daskereRapea
	String_t* ____daskereRapea_7;
	// System.UInt32 GarbageiOS.M_tearjarorZehi155::_sousairTairdrebar
	uint32_t ____sousairTairdrebar_8;
	// System.UInt32 GarbageiOS.M_tearjarorZehi155::_treemaiJersapir
	uint32_t ____treemaiJersapir_9;
	// System.String GarbageiOS.M_tearjarorZehi155::_vawyecher
	String_t* ____vawyecher_10;
	// System.Boolean GarbageiOS.M_tearjarorZehi155::_rayxiwa
	bool ____rayxiwa_11;
	// System.Boolean GarbageiOS.M_tearjarorZehi155::_curleaRipe
	bool ____curleaRipe_12;
	// System.Int32 GarbageiOS.M_tearjarorZehi155::_tisboosouPewee
	int32_t ____tisboosouPewee_13;
	// System.Boolean GarbageiOS.M_tearjarorZehi155::_sudow
	bool ____sudow_14;
	// System.UInt32 GarbageiOS.M_tearjarorZehi155::_calldigi
	uint32_t ____calldigi_15;
	// System.Int32 GarbageiOS.M_tearjarorZehi155::_sawqairsterCegay
	int32_t ____sawqairsterCegay_16;
	// System.Boolean GarbageiOS.M_tearjarorZehi155::_veresoTelzoche
	bool ____veresoTelzoche_17;
	// System.UInt32 GarbageiOS.M_tearjarorZehi155::_wheartucooNemo
	uint32_t ____wheartucooNemo_18;
	// System.UInt32 GarbageiOS.M_tearjarorZehi155::_zerdailea
	uint32_t ____zerdailea_19;
	// System.Single GarbageiOS.M_tearjarorZehi155::_coliporQoja
	float ____coliporQoja_20;
	// System.Boolean GarbageiOS.M_tearjarorZehi155::_daywoo
	bool ____daywoo_21;
	// System.Boolean GarbageiOS.M_tearjarorZehi155::_sorstu
	bool ____sorstu_22;
	// System.Int32 GarbageiOS.M_tearjarorZehi155::_trikerejur
	int32_t ____trikerejur_23;
	// System.String GarbageiOS.M_tearjarorZehi155::_mirwace
	String_t* ____mirwace_24;
	// System.Single GarbageiOS.M_tearjarorZehi155::_rerehaiDroucou
	float ____rerehaiDroucou_25;
	// System.Boolean GarbageiOS.M_tearjarorZehi155::_bormi
	bool ____bormi_26;
	// System.Int32 GarbageiOS.M_tearjarorZehi155::_kaizalgiNemal
	int32_t ____kaizalgiNemal_27;
	// System.String GarbageiOS.M_tearjarorZehi155::_ceerur
	String_t* ____ceerur_28;
	// System.Int32 GarbageiOS.M_tearjarorZehi155::_piwow
	int32_t ____piwow_29;
	// System.Int32 GarbageiOS.M_tearjarorZehi155::_jaro
	int32_t ____jaro_30;
	// System.Int32 GarbageiOS.M_tearjarorZehi155::_rurnee
	int32_t ____rurnee_31;
	// System.Single GarbageiOS.M_tearjarorZehi155::_draymumorNowsairsou
	float ____draymumorNowsairsou_32;
	// System.UInt32 GarbageiOS.M_tearjarorZehi155::_cukai
	uint32_t ____cukai_33;
	// System.Int32 GarbageiOS.M_tearjarorZehi155::_paljas
	int32_t ____paljas_34;
	// System.String GarbageiOS.M_tearjarorZehi155::_karafar
	String_t* ____karafar_35;
	// System.String GarbageiOS.M_tearjarorZehi155::_bemmounowDarkeno
	String_t* ____bemmounowDarkeno_36;
	// System.String GarbageiOS.M_tearjarorZehi155::_mainurmarMeardoo
	String_t* ____mainurmarMeardoo_37;
	// System.Boolean GarbageiOS.M_tearjarorZehi155::_troudeeHoomu
	bool ____troudeeHoomu_38;
	// System.Single GarbageiOS.M_tearjarorZehi155::_gasfaiSacoboo
	float ____gasfaiSacoboo_39;
	// System.Int32 GarbageiOS.M_tearjarorZehi155::_roudraJouber
	int32_t ____roudraJouber_40;
	// System.Single GarbageiOS.M_tearjarorZehi155::_famowdas
	float ____famowdas_41;
	// System.Boolean GarbageiOS.M_tearjarorZehi155::_peamirsere
	bool ____peamirsere_42;
	// System.String GarbageiOS.M_tearjarorZehi155::_yallmo
	String_t* ____yallmo_43;
	// System.Int32 GarbageiOS.M_tearjarorZehi155::_trotukear
	int32_t ____trotukear_44;
	// System.UInt32 GarbageiOS.M_tearjarorZehi155::_hewusaiFeqija
	uint32_t ____hewusaiFeqija_45;
	// System.Single GarbageiOS.M_tearjarorZehi155::_goumasyirTrispar
	float ____goumasyirTrispar_46;
	// System.String GarbageiOS.M_tearjarorZehi155::_wurir
	String_t* ____wurir_47;

public:
	inline static int32_t get_offset_of__jemawe_0() { return static_cast<int32_t>(offsetof(M_tearjarorZehi155_t1280640825, ____jemawe_0)); }
	inline int32_t get__jemawe_0() const { return ____jemawe_0; }
	inline int32_t* get_address_of__jemawe_0() { return &____jemawe_0; }
	inline void set__jemawe_0(int32_t value)
	{
		____jemawe_0 = value;
	}

	inline static int32_t get_offset_of__sejowRidreastur_1() { return static_cast<int32_t>(offsetof(M_tearjarorZehi155_t1280640825, ____sejowRidreastur_1)); }
	inline float get__sejowRidreastur_1() const { return ____sejowRidreastur_1; }
	inline float* get_address_of__sejowRidreastur_1() { return &____sejowRidreastur_1; }
	inline void set__sejowRidreastur_1(float value)
	{
		____sejowRidreastur_1 = value;
	}

	inline static int32_t get_offset_of__talljiwas_2() { return static_cast<int32_t>(offsetof(M_tearjarorZehi155_t1280640825, ____talljiwas_2)); }
	inline float get__talljiwas_2() const { return ____talljiwas_2; }
	inline float* get_address_of__talljiwas_2() { return &____talljiwas_2; }
	inline void set__talljiwas_2(float value)
	{
		____talljiwas_2 = value;
	}

	inline static int32_t get_offset_of__wheasoseYisci_3() { return static_cast<int32_t>(offsetof(M_tearjarorZehi155_t1280640825, ____wheasoseYisci_3)); }
	inline int32_t get__wheasoseYisci_3() const { return ____wheasoseYisci_3; }
	inline int32_t* get_address_of__wheasoseYisci_3() { return &____wheasoseYisci_3; }
	inline void set__wheasoseYisci_3(int32_t value)
	{
		____wheasoseYisci_3 = value;
	}

	inline static int32_t get_offset_of__fejouSerstayxi_4() { return static_cast<int32_t>(offsetof(M_tearjarorZehi155_t1280640825, ____fejouSerstayxi_4)); }
	inline bool get__fejouSerstayxi_4() const { return ____fejouSerstayxi_4; }
	inline bool* get_address_of__fejouSerstayxi_4() { return &____fejouSerstayxi_4; }
	inline void set__fejouSerstayxi_4(bool value)
	{
		____fejouSerstayxi_4 = value;
	}

	inline static int32_t get_offset_of__dalku_5() { return static_cast<int32_t>(offsetof(M_tearjarorZehi155_t1280640825, ____dalku_5)); }
	inline bool get__dalku_5() const { return ____dalku_5; }
	inline bool* get_address_of__dalku_5() { return &____dalku_5; }
	inline void set__dalku_5(bool value)
	{
		____dalku_5 = value;
	}

	inline static int32_t get_offset_of__joodor_6() { return static_cast<int32_t>(offsetof(M_tearjarorZehi155_t1280640825, ____joodor_6)); }
	inline int32_t get__joodor_6() const { return ____joodor_6; }
	inline int32_t* get_address_of__joodor_6() { return &____joodor_6; }
	inline void set__joodor_6(int32_t value)
	{
		____joodor_6 = value;
	}

	inline static int32_t get_offset_of__daskereRapea_7() { return static_cast<int32_t>(offsetof(M_tearjarorZehi155_t1280640825, ____daskereRapea_7)); }
	inline String_t* get__daskereRapea_7() const { return ____daskereRapea_7; }
	inline String_t** get_address_of__daskereRapea_7() { return &____daskereRapea_7; }
	inline void set__daskereRapea_7(String_t* value)
	{
		____daskereRapea_7 = value;
		Il2CppCodeGenWriteBarrier(&____daskereRapea_7, value);
	}

	inline static int32_t get_offset_of__sousairTairdrebar_8() { return static_cast<int32_t>(offsetof(M_tearjarorZehi155_t1280640825, ____sousairTairdrebar_8)); }
	inline uint32_t get__sousairTairdrebar_8() const { return ____sousairTairdrebar_8; }
	inline uint32_t* get_address_of__sousairTairdrebar_8() { return &____sousairTairdrebar_8; }
	inline void set__sousairTairdrebar_8(uint32_t value)
	{
		____sousairTairdrebar_8 = value;
	}

	inline static int32_t get_offset_of__treemaiJersapir_9() { return static_cast<int32_t>(offsetof(M_tearjarorZehi155_t1280640825, ____treemaiJersapir_9)); }
	inline uint32_t get__treemaiJersapir_9() const { return ____treemaiJersapir_9; }
	inline uint32_t* get_address_of__treemaiJersapir_9() { return &____treemaiJersapir_9; }
	inline void set__treemaiJersapir_9(uint32_t value)
	{
		____treemaiJersapir_9 = value;
	}

	inline static int32_t get_offset_of__vawyecher_10() { return static_cast<int32_t>(offsetof(M_tearjarorZehi155_t1280640825, ____vawyecher_10)); }
	inline String_t* get__vawyecher_10() const { return ____vawyecher_10; }
	inline String_t** get_address_of__vawyecher_10() { return &____vawyecher_10; }
	inline void set__vawyecher_10(String_t* value)
	{
		____vawyecher_10 = value;
		Il2CppCodeGenWriteBarrier(&____vawyecher_10, value);
	}

	inline static int32_t get_offset_of__rayxiwa_11() { return static_cast<int32_t>(offsetof(M_tearjarorZehi155_t1280640825, ____rayxiwa_11)); }
	inline bool get__rayxiwa_11() const { return ____rayxiwa_11; }
	inline bool* get_address_of__rayxiwa_11() { return &____rayxiwa_11; }
	inline void set__rayxiwa_11(bool value)
	{
		____rayxiwa_11 = value;
	}

	inline static int32_t get_offset_of__curleaRipe_12() { return static_cast<int32_t>(offsetof(M_tearjarorZehi155_t1280640825, ____curleaRipe_12)); }
	inline bool get__curleaRipe_12() const { return ____curleaRipe_12; }
	inline bool* get_address_of__curleaRipe_12() { return &____curleaRipe_12; }
	inline void set__curleaRipe_12(bool value)
	{
		____curleaRipe_12 = value;
	}

	inline static int32_t get_offset_of__tisboosouPewee_13() { return static_cast<int32_t>(offsetof(M_tearjarorZehi155_t1280640825, ____tisboosouPewee_13)); }
	inline int32_t get__tisboosouPewee_13() const { return ____tisboosouPewee_13; }
	inline int32_t* get_address_of__tisboosouPewee_13() { return &____tisboosouPewee_13; }
	inline void set__tisboosouPewee_13(int32_t value)
	{
		____tisboosouPewee_13 = value;
	}

	inline static int32_t get_offset_of__sudow_14() { return static_cast<int32_t>(offsetof(M_tearjarorZehi155_t1280640825, ____sudow_14)); }
	inline bool get__sudow_14() const { return ____sudow_14; }
	inline bool* get_address_of__sudow_14() { return &____sudow_14; }
	inline void set__sudow_14(bool value)
	{
		____sudow_14 = value;
	}

	inline static int32_t get_offset_of__calldigi_15() { return static_cast<int32_t>(offsetof(M_tearjarorZehi155_t1280640825, ____calldigi_15)); }
	inline uint32_t get__calldigi_15() const { return ____calldigi_15; }
	inline uint32_t* get_address_of__calldigi_15() { return &____calldigi_15; }
	inline void set__calldigi_15(uint32_t value)
	{
		____calldigi_15 = value;
	}

	inline static int32_t get_offset_of__sawqairsterCegay_16() { return static_cast<int32_t>(offsetof(M_tearjarorZehi155_t1280640825, ____sawqairsterCegay_16)); }
	inline int32_t get__sawqairsterCegay_16() const { return ____sawqairsterCegay_16; }
	inline int32_t* get_address_of__sawqairsterCegay_16() { return &____sawqairsterCegay_16; }
	inline void set__sawqairsterCegay_16(int32_t value)
	{
		____sawqairsterCegay_16 = value;
	}

	inline static int32_t get_offset_of__veresoTelzoche_17() { return static_cast<int32_t>(offsetof(M_tearjarorZehi155_t1280640825, ____veresoTelzoche_17)); }
	inline bool get__veresoTelzoche_17() const { return ____veresoTelzoche_17; }
	inline bool* get_address_of__veresoTelzoche_17() { return &____veresoTelzoche_17; }
	inline void set__veresoTelzoche_17(bool value)
	{
		____veresoTelzoche_17 = value;
	}

	inline static int32_t get_offset_of__wheartucooNemo_18() { return static_cast<int32_t>(offsetof(M_tearjarorZehi155_t1280640825, ____wheartucooNemo_18)); }
	inline uint32_t get__wheartucooNemo_18() const { return ____wheartucooNemo_18; }
	inline uint32_t* get_address_of__wheartucooNemo_18() { return &____wheartucooNemo_18; }
	inline void set__wheartucooNemo_18(uint32_t value)
	{
		____wheartucooNemo_18 = value;
	}

	inline static int32_t get_offset_of__zerdailea_19() { return static_cast<int32_t>(offsetof(M_tearjarorZehi155_t1280640825, ____zerdailea_19)); }
	inline uint32_t get__zerdailea_19() const { return ____zerdailea_19; }
	inline uint32_t* get_address_of__zerdailea_19() { return &____zerdailea_19; }
	inline void set__zerdailea_19(uint32_t value)
	{
		____zerdailea_19 = value;
	}

	inline static int32_t get_offset_of__coliporQoja_20() { return static_cast<int32_t>(offsetof(M_tearjarorZehi155_t1280640825, ____coliporQoja_20)); }
	inline float get__coliporQoja_20() const { return ____coliporQoja_20; }
	inline float* get_address_of__coliporQoja_20() { return &____coliporQoja_20; }
	inline void set__coliporQoja_20(float value)
	{
		____coliporQoja_20 = value;
	}

	inline static int32_t get_offset_of__daywoo_21() { return static_cast<int32_t>(offsetof(M_tearjarorZehi155_t1280640825, ____daywoo_21)); }
	inline bool get__daywoo_21() const { return ____daywoo_21; }
	inline bool* get_address_of__daywoo_21() { return &____daywoo_21; }
	inline void set__daywoo_21(bool value)
	{
		____daywoo_21 = value;
	}

	inline static int32_t get_offset_of__sorstu_22() { return static_cast<int32_t>(offsetof(M_tearjarorZehi155_t1280640825, ____sorstu_22)); }
	inline bool get__sorstu_22() const { return ____sorstu_22; }
	inline bool* get_address_of__sorstu_22() { return &____sorstu_22; }
	inline void set__sorstu_22(bool value)
	{
		____sorstu_22 = value;
	}

	inline static int32_t get_offset_of__trikerejur_23() { return static_cast<int32_t>(offsetof(M_tearjarorZehi155_t1280640825, ____trikerejur_23)); }
	inline int32_t get__trikerejur_23() const { return ____trikerejur_23; }
	inline int32_t* get_address_of__trikerejur_23() { return &____trikerejur_23; }
	inline void set__trikerejur_23(int32_t value)
	{
		____trikerejur_23 = value;
	}

	inline static int32_t get_offset_of__mirwace_24() { return static_cast<int32_t>(offsetof(M_tearjarorZehi155_t1280640825, ____mirwace_24)); }
	inline String_t* get__mirwace_24() const { return ____mirwace_24; }
	inline String_t** get_address_of__mirwace_24() { return &____mirwace_24; }
	inline void set__mirwace_24(String_t* value)
	{
		____mirwace_24 = value;
		Il2CppCodeGenWriteBarrier(&____mirwace_24, value);
	}

	inline static int32_t get_offset_of__rerehaiDroucou_25() { return static_cast<int32_t>(offsetof(M_tearjarorZehi155_t1280640825, ____rerehaiDroucou_25)); }
	inline float get__rerehaiDroucou_25() const { return ____rerehaiDroucou_25; }
	inline float* get_address_of__rerehaiDroucou_25() { return &____rerehaiDroucou_25; }
	inline void set__rerehaiDroucou_25(float value)
	{
		____rerehaiDroucou_25 = value;
	}

	inline static int32_t get_offset_of__bormi_26() { return static_cast<int32_t>(offsetof(M_tearjarorZehi155_t1280640825, ____bormi_26)); }
	inline bool get__bormi_26() const { return ____bormi_26; }
	inline bool* get_address_of__bormi_26() { return &____bormi_26; }
	inline void set__bormi_26(bool value)
	{
		____bormi_26 = value;
	}

	inline static int32_t get_offset_of__kaizalgiNemal_27() { return static_cast<int32_t>(offsetof(M_tearjarorZehi155_t1280640825, ____kaizalgiNemal_27)); }
	inline int32_t get__kaizalgiNemal_27() const { return ____kaizalgiNemal_27; }
	inline int32_t* get_address_of__kaizalgiNemal_27() { return &____kaizalgiNemal_27; }
	inline void set__kaizalgiNemal_27(int32_t value)
	{
		____kaizalgiNemal_27 = value;
	}

	inline static int32_t get_offset_of__ceerur_28() { return static_cast<int32_t>(offsetof(M_tearjarorZehi155_t1280640825, ____ceerur_28)); }
	inline String_t* get__ceerur_28() const { return ____ceerur_28; }
	inline String_t** get_address_of__ceerur_28() { return &____ceerur_28; }
	inline void set__ceerur_28(String_t* value)
	{
		____ceerur_28 = value;
		Il2CppCodeGenWriteBarrier(&____ceerur_28, value);
	}

	inline static int32_t get_offset_of__piwow_29() { return static_cast<int32_t>(offsetof(M_tearjarorZehi155_t1280640825, ____piwow_29)); }
	inline int32_t get__piwow_29() const { return ____piwow_29; }
	inline int32_t* get_address_of__piwow_29() { return &____piwow_29; }
	inline void set__piwow_29(int32_t value)
	{
		____piwow_29 = value;
	}

	inline static int32_t get_offset_of__jaro_30() { return static_cast<int32_t>(offsetof(M_tearjarorZehi155_t1280640825, ____jaro_30)); }
	inline int32_t get__jaro_30() const { return ____jaro_30; }
	inline int32_t* get_address_of__jaro_30() { return &____jaro_30; }
	inline void set__jaro_30(int32_t value)
	{
		____jaro_30 = value;
	}

	inline static int32_t get_offset_of__rurnee_31() { return static_cast<int32_t>(offsetof(M_tearjarorZehi155_t1280640825, ____rurnee_31)); }
	inline int32_t get__rurnee_31() const { return ____rurnee_31; }
	inline int32_t* get_address_of__rurnee_31() { return &____rurnee_31; }
	inline void set__rurnee_31(int32_t value)
	{
		____rurnee_31 = value;
	}

	inline static int32_t get_offset_of__draymumorNowsairsou_32() { return static_cast<int32_t>(offsetof(M_tearjarorZehi155_t1280640825, ____draymumorNowsairsou_32)); }
	inline float get__draymumorNowsairsou_32() const { return ____draymumorNowsairsou_32; }
	inline float* get_address_of__draymumorNowsairsou_32() { return &____draymumorNowsairsou_32; }
	inline void set__draymumorNowsairsou_32(float value)
	{
		____draymumorNowsairsou_32 = value;
	}

	inline static int32_t get_offset_of__cukai_33() { return static_cast<int32_t>(offsetof(M_tearjarorZehi155_t1280640825, ____cukai_33)); }
	inline uint32_t get__cukai_33() const { return ____cukai_33; }
	inline uint32_t* get_address_of__cukai_33() { return &____cukai_33; }
	inline void set__cukai_33(uint32_t value)
	{
		____cukai_33 = value;
	}

	inline static int32_t get_offset_of__paljas_34() { return static_cast<int32_t>(offsetof(M_tearjarorZehi155_t1280640825, ____paljas_34)); }
	inline int32_t get__paljas_34() const { return ____paljas_34; }
	inline int32_t* get_address_of__paljas_34() { return &____paljas_34; }
	inline void set__paljas_34(int32_t value)
	{
		____paljas_34 = value;
	}

	inline static int32_t get_offset_of__karafar_35() { return static_cast<int32_t>(offsetof(M_tearjarorZehi155_t1280640825, ____karafar_35)); }
	inline String_t* get__karafar_35() const { return ____karafar_35; }
	inline String_t** get_address_of__karafar_35() { return &____karafar_35; }
	inline void set__karafar_35(String_t* value)
	{
		____karafar_35 = value;
		Il2CppCodeGenWriteBarrier(&____karafar_35, value);
	}

	inline static int32_t get_offset_of__bemmounowDarkeno_36() { return static_cast<int32_t>(offsetof(M_tearjarorZehi155_t1280640825, ____bemmounowDarkeno_36)); }
	inline String_t* get__bemmounowDarkeno_36() const { return ____bemmounowDarkeno_36; }
	inline String_t** get_address_of__bemmounowDarkeno_36() { return &____bemmounowDarkeno_36; }
	inline void set__bemmounowDarkeno_36(String_t* value)
	{
		____bemmounowDarkeno_36 = value;
		Il2CppCodeGenWriteBarrier(&____bemmounowDarkeno_36, value);
	}

	inline static int32_t get_offset_of__mainurmarMeardoo_37() { return static_cast<int32_t>(offsetof(M_tearjarorZehi155_t1280640825, ____mainurmarMeardoo_37)); }
	inline String_t* get__mainurmarMeardoo_37() const { return ____mainurmarMeardoo_37; }
	inline String_t** get_address_of__mainurmarMeardoo_37() { return &____mainurmarMeardoo_37; }
	inline void set__mainurmarMeardoo_37(String_t* value)
	{
		____mainurmarMeardoo_37 = value;
		Il2CppCodeGenWriteBarrier(&____mainurmarMeardoo_37, value);
	}

	inline static int32_t get_offset_of__troudeeHoomu_38() { return static_cast<int32_t>(offsetof(M_tearjarorZehi155_t1280640825, ____troudeeHoomu_38)); }
	inline bool get__troudeeHoomu_38() const { return ____troudeeHoomu_38; }
	inline bool* get_address_of__troudeeHoomu_38() { return &____troudeeHoomu_38; }
	inline void set__troudeeHoomu_38(bool value)
	{
		____troudeeHoomu_38 = value;
	}

	inline static int32_t get_offset_of__gasfaiSacoboo_39() { return static_cast<int32_t>(offsetof(M_tearjarorZehi155_t1280640825, ____gasfaiSacoboo_39)); }
	inline float get__gasfaiSacoboo_39() const { return ____gasfaiSacoboo_39; }
	inline float* get_address_of__gasfaiSacoboo_39() { return &____gasfaiSacoboo_39; }
	inline void set__gasfaiSacoboo_39(float value)
	{
		____gasfaiSacoboo_39 = value;
	}

	inline static int32_t get_offset_of__roudraJouber_40() { return static_cast<int32_t>(offsetof(M_tearjarorZehi155_t1280640825, ____roudraJouber_40)); }
	inline int32_t get__roudraJouber_40() const { return ____roudraJouber_40; }
	inline int32_t* get_address_of__roudraJouber_40() { return &____roudraJouber_40; }
	inline void set__roudraJouber_40(int32_t value)
	{
		____roudraJouber_40 = value;
	}

	inline static int32_t get_offset_of__famowdas_41() { return static_cast<int32_t>(offsetof(M_tearjarorZehi155_t1280640825, ____famowdas_41)); }
	inline float get__famowdas_41() const { return ____famowdas_41; }
	inline float* get_address_of__famowdas_41() { return &____famowdas_41; }
	inline void set__famowdas_41(float value)
	{
		____famowdas_41 = value;
	}

	inline static int32_t get_offset_of__peamirsere_42() { return static_cast<int32_t>(offsetof(M_tearjarorZehi155_t1280640825, ____peamirsere_42)); }
	inline bool get__peamirsere_42() const { return ____peamirsere_42; }
	inline bool* get_address_of__peamirsere_42() { return &____peamirsere_42; }
	inline void set__peamirsere_42(bool value)
	{
		____peamirsere_42 = value;
	}

	inline static int32_t get_offset_of__yallmo_43() { return static_cast<int32_t>(offsetof(M_tearjarorZehi155_t1280640825, ____yallmo_43)); }
	inline String_t* get__yallmo_43() const { return ____yallmo_43; }
	inline String_t** get_address_of__yallmo_43() { return &____yallmo_43; }
	inline void set__yallmo_43(String_t* value)
	{
		____yallmo_43 = value;
		Il2CppCodeGenWriteBarrier(&____yallmo_43, value);
	}

	inline static int32_t get_offset_of__trotukear_44() { return static_cast<int32_t>(offsetof(M_tearjarorZehi155_t1280640825, ____trotukear_44)); }
	inline int32_t get__trotukear_44() const { return ____trotukear_44; }
	inline int32_t* get_address_of__trotukear_44() { return &____trotukear_44; }
	inline void set__trotukear_44(int32_t value)
	{
		____trotukear_44 = value;
	}

	inline static int32_t get_offset_of__hewusaiFeqija_45() { return static_cast<int32_t>(offsetof(M_tearjarorZehi155_t1280640825, ____hewusaiFeqija_45)); }
	inline uint32_t get__hewusaiFeqija_45() const { return ____hewusaiFeqija_45; }
	inline uint32_t* get_address_of__hewusaiFeqija_45() { return &____hewusaiFeqija_45; }
	inline void set__hewusaiFeqija_45(uint32_t value)
	{
		____hewusaiFeqija_45 = value;
	}

	inline static int32_t get_offset_of__goumasyirTrispar_46() { return static_cast<int32_t>(offsetof(M_tearjarorZehi155_t1280640825, ____goumasyirTrispar_46)); }
	inline float get__goumasyirTrispar_46() const { return ____goumasyirTrispar_46; }
	inline float* get_address_of__goumasyirTrispar_46() { return &____goumasyirTrispar_46; }
	inline void set__goumasyirTrispar_46(float value)
	{
		____goumasyirTrispar_46 = value;
	}

	inline static int32_t get_offset_of__wurir_47() { return static_cast<int32_t>(offsetof(M_tearjarorZehi155_t1280640825, ____wurir_47)); }
	inline String_t* get__wurir_47() const { return ____wurir_47; }
	inline String_t** get_address_of__wurir_47() { return &____wurir_47; }
	inline void set__wurir_47(String_t* value)
	{
		____wurir_47 = value;
		Il2CppCodeGenWriteBarrier(&____wurir_47, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
