﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._a2a7a4df4084131ec373845ea2e38aa9
struct _a2a7a4df4084131ec373845ea2e38aa9_t1922737915;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__a2a7a4df4084131ec373845e1922737915.h"

// System.Void Little._a2a7a4df4084131ec373845ea2e38aa9::.ctor()
extern "C"  void _a2a7a4df4084131ec373845ea2e38aa9__ctor_m3116706194 (_a2a7a4df4084131ec373845ea2e38aa9_t1922737915 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._a2a7a4df4084131ec373845ea2e38aa9::_a2a7a4df4084131ec373845ea2e38aa9m2(System.Int32)
extern "C"  int32_t _a2a7a4df4084131ec373845ea2e38aa9__a2a7a4df4084131ec373845ea2e38aa9m2_m1983469497 (_a2a7a4df4084131ec373845ea2e38aa9_t1922737915 * __this, int32_t ____a2a7a4df4084131ec373845ea2e38aa9a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._a2a7a4df4084131ec373845ea2e38aa9::_a2a7a4df4084131ec373845ea2e38aa9m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _a2a7a4df4084131ec373845ea2e38aa9__a2a7a4df4084131ec373845ea2e38aa9m_m1093694237 (_a2a7a4df4084131ec373845ea2e38aa9_t1922737915 * __this, int32_t ____a2a7a4df4084131ec373845ea2e38aa9a0, int32_t ____a2a7a4df4084131ec373845ea2e38aa9911, int32_t ____a2a7a4df4084131ec373845ea2e38aa9c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._a2a7a4df4084131ec373845ea2e38aa9::ilo__a2a7a4df4084131ec373845ea2e38aa9m21(Little._a2a7a4df4084131ec373845ea2e38aa9,System.Int32)
extern "C"  int32_t _a2a7a4df4084131ec373845ea2e38aa9_ilo__a2a7a4df4084131ec373845ea2e38aa9m21_m3272252706 (Il2CppObject * __this /* static, unused */, _a2a7a4df4084131ec373845ea2e38aa9_t1922737915 * ____this0, int32_t ____a2a7a4df4084131ec373845ea2e38aa9a1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
