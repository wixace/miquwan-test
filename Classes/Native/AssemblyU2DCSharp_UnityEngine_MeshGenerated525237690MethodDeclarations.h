﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_MeshGenerated
struct UnityEngine_MeshGenerated_t525237690;
// JSVCall
struct JSVCall_t3708497963;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t215400611;
// UnityEngine.Vector4[]
struct Vector4U5BU5D_t701588350;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t4024180168;
// UnityEngine.Color[]
struct ColorU5BU5D_t2441545636;
// UnityEngine.Color32[]
struct Color32U5BU5D_t2960766953;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// UnityEngine.BoneWeight[]
struct BoneWeightU5BU5D_t1503835233;
// UnityEngine.Matrix4x4[]
struct Matrix4x4U5BU5D_t1421664456;
// UnityEngine.CombineInstance[]
struct CombineInstanceU5BU5D_t1850423023;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_String7231557.h"

// System.Void UnityEngine_MeshGenerated::.ctor()
extern "C"  void UnityEngine_MeshGenerated__ctor_m2927532273 (UnityEngine_MeshGenerated_t525237690 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MeshGenerated::Mesh_Mesh1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MeshGenerated_Mesh_Mesh1_m327370241 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_MeshGenerated::Mesh_isReadable(JSVCall)
extern "C"  void UnityEngine_MeshGenerated_Mesh_isReadable_m2852668140 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_MeshGenerated::Mesh_vertices(JSVCall)
extern "C"  void UnityEngine_MeshGenerated_Mesh_vertices_m116454669 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_MeshGenerated::Mesh_normals(JSVCall)
extern "C"  void UnityEngine_MeshGenerated_Mesh_normals_m1299075898 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_MeshGenerated::Mesh_tangents(JSVCall)
extern "C"  void UnityEngine_MeshGenerated_Mesh_tangents_m1403082200 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_MeshGenerated::Mesh_uv(JSVCall)
extern "C"  void UnityEngine_MeshGenerated_Mesh_uv_m2419807077 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_MeshGenerated::Mesh_uv2(JSVCall)
extern "C"  void UnityEngine_MeshGenerated_Mesh_uv2_m706337685 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_MeshGenerated::Mesh_uv3(JSVCall)
extern "C"  void UnityEngine_MeshGenerated_Mesh_uv3_m509824180 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_MeshGenerated::Mesh_uv4(JSVCall)
extern "C"  void UnityEngine_MeshGenerated_Mesh_uv4_m313310675 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_MeshGenerated::Mesh_bounds(JSVCall)
extern "C"  void UnityEngine_MeshGenerated_Mesh_bounds_m3566768593 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_MeshGenerated::Mesh_colors(JSVCall)
extern "C"  void UnityEngine_MeshGenerated_Mesh_colors_m4161547638 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_MeshGenerated::Mesh_colors32(JSVCall)
extern "C"  void UnityEngine_MeshGenerated_Mesh_colors32_m401843703 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_MeshGenerated::Mesh_triangles(JSVCall)
extern "C"  void UnityEngine_MeshGenerated_Mesh_triangles_m3146498843 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_MeshGenerated::Mesh_vertexCount(JSVCall)
extern "C"  void UnityEngine_MeshGenerated_Mesh_vertexCount_m3327092923 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_MeshGenerated::Mesh_subMeshCount(JSVCall)
extern "C"  void UnityEngine_MeshGenerated_Mesh_subMeshCount_m487988836 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_MeshGenerated::Mesh_boneWeights(JSVCall)
extern "C"  void UnityEngine_MeshGenerated_Mesh_boneWeights_m548732527 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_MeshGenerated::Mesh_bindposes(JSVCall)
extern "C"  void UnityEngine_MeshGenerated_Mesh_bindposes_m4134268897 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_MeshGenerated::Mesh_blendShapeCount(JSVCall)
extern "C"  void UnityEngine_MeshGenerated_Mesh_blendShapeCount_m320009191 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MeshGenerated::Mesh_AddBlendShapeFrame__String__Single__Vector3_Array__Vector3_Array__Vector3_Array(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MeshGenerated_Mesh_AddBlendShapeFrame__String__Single__Vector3_Array__Vector3_Array__Vector3_Array_m3142573650 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MeshGenerated::Mesh_Clear__Boolean(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MeshGenerated_Mesh_Clear__Boolean_m2290461888 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MeshGenerated::Mesh_Clear(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MeshGenerated_Mesh_Clear_m2603790250 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MeshGenerated::Mesh_ClearBlendShapes(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MeshGenerated_Mesh_ClearBlendShapes_m903453979 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MeshGenerated::Mesh_CombineMeshes__CombineInstance_Array__Boolean__Boolean(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MeshGenerated_Mesh_CombineMeshes__CombineInstance_Array__Boolean__Boolean_m3622303225 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MeshGenerated::Mesh_CombineMeshes__CombineInstance_Array__Boolean(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MeshGenerated_Mesh_CombineMeshes__CombineInstance_Array__Boolean_m393441873 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MeshGenerated::Mesh_CombineMeshes__CombineInstance_Array(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MeshGenerated_Mesh_CombineMeshes__CombineInstance_Array_m45232889 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MeshGenerated::Mesh_GetBlendShapeFrameCount__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MeshGenerated_Mesh_GetBlendShapeFrameCount__Int32_m406532491 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MeshGenerated::Mesh_GetBlendShapeFrameVertices__Int32__Int32__Vector3_Array__Vector3_Array__Vector3_Array(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MeshGenerated_Mesh_GetBlendShapeFrameVertices__Int32__Int32__Vector3_Array__Vector3_Array__Vector3_Array_m435145415 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MeshGenerated::Mesh_GetBlendShapeFrameWeight__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MeshGenerated_Mesh_GetBlendShapeFrameWeight__Int32__Int32_m3964933092 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MeshGenerated::Mesh_GetBlendShapeIndex__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MeshGenerated_Mesh_GetBlendShapeIndex__String_m4255849346 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MeshGenerated::Mesh_GetBlendShapeName__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MeshGenerated_Mesh_GetBlendShapeName__Int32_m3991499202 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MeshGenerated::Mesh_GetIndices__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MeshGenerated_Mesh_GetIndices__Int32_m2842040666 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MeshGenerated::Mesh_GetTopology__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MeshGenerated_Mesh_GetTopology__Int32_m507149006 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MeshGenerated::Mesh_GetTriangles__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MeshGenerated_Mesh_GetTriangles__Int32_m4003486614 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MeshGenerated::Mesh_GetUVs__Int32__ListT1_Vector4(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MeshGenerated_Mesh_GetUVs__Int32__ListT1_Vector4_m1196698236 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MeshGenerated::Mesh_GetUVs__Int32__ListT1_Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MeshGenerated_Mesh_GetUVs__Int32__ListT1_Vector3_m4246901051 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MeshGenerated::Mesh_GetUVs__Int32__ListT1_Vector2(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MeshGenerated_Mesh_GetUVs__Int32__ListT1_Vector2_m3002136570 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MeshGenerated::Mesh_MarkDynamic(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MeshGenerated_Mesh_MarkDynamic_m3750811471 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MeshGenerated::Mesh_Optimize(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MeshGenerated_Mesh_Optimize_m2809291938 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MeshGenerated::Mesh_RecalculateBounds(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MeshGenerated_Mesh_RecalculateBounds_m1865287749 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MeshGenerated::Mesh_RecalculateNormals(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MeshGenerated_Mesh_RecalculateNormals_m1820150302 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MeshGenerated::Mesh_SetColors__ListT1_Color(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MeshGenerated_Mesh_SetColors__ListT1_Color_m2527589838 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MeshGenerated::Mesh_SetColors__ListT1_Color32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MeshGenerated_Mesh_SetColors__ListT1_Color32_m1208164269 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MeshGenerated::Mesh_SetIndices__Int32_Array__MeshTopology__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MeshGenerated_Mesh_SetIndices__Int32_Array__MeshTopology__Int32_m1377874228 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MeshGenerated::Mesh_SetNormals__ListT1_Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MeshGenerated_Mesh_SetNormals__ListT1_Vector3_m2789083163 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MeshGenerated::Mesh_SetTangents__ListT1_Vector4(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MeshGenerated_Mesh_SetTangents__ListT1_Vector4_m2379534362 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MeshGenerated::Mesh_SetTriangles__ListT1_Int32__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MeshGenerated_Mesh_SetTriangles__ListT1_Int32__Int32_m3397325304 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MeshGenerated::Mesh_SetTriangles__Int32_Array__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MeshGenerated_Mesh_SetTriangles__Int32_Array__Int32_m610677556 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MeshGenerated::Mesh_SetUVs__Int32__ListT1_Vector2(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MeshGenerated_Mesh_SetUVs__Int32__ListT1_Vector2_m3715521798 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MeshGenerated::Mesh_SetUVs__Int32__ListT1_Vector4(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MeshGenerated_Mesh_SetUVs__Int32__ListT1_Vector4_m1910083464 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MeshGenerated::Mesh_SetUVs__Int32__ListT1_Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MeshGenerated_Mesh_SetUVs__Int32__ListT1_Vector3_m665318983 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MeshGenerated::Mesh_SetVertices__ListT1_Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MeshGenerated_Mesh_SetVertices__ListT1_Vector3_m3372974020 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MeshGenerated::Mesh_UploadMeshData__Boolean(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MeshGenerated_Mesh_UploadMeshData__Boolean_m2988809453 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_MeshGenerated::__Register()
extern "C"  void UnityEngine_MeshGenerated___Register_m297914998 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3[] UnityEngine_MeshGenerated::<Mesh_vertices>m__267()
extern "C"  Vector3U5BU5D_t215400611* UnityEngine_MeshGenerated_U3CMesh_verticesU3Em__267_m285087420 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3[] UnityEngine_MeshGenerated::<Mesh_normals>m__268()
extern "C"  Vector3U5BU5D_t215400611* UnityEngine_MeshGenerated_U3CMesh_normalsU3Em__268_m1081564234 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector4[] UnityEngine_MeshGenerated::<Mesh_tangents>m__269()
extern "C"  Vector4U5BU5D_t701588350* UnityEngine_MeshGenerated_U3CMesh_tangentsU3Em__269_m2009726376 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2[] UnityEngine_MeshGenerated::<Mesh_uv>m__26A()
extern "C"  Vector2U5BU5D_t4024180168* UnityEngine_MeshGenerated_U3CMesh_uvU3Em__26A_m3794148543 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2[] UnityEngine_MeshGenerated::<Mesh_uv2>m__26B()
extern "C"  Vector2U5BU5D_t4024180168* UnityEngine_MeshGenerated_U3CMesh_uv2U3Em__26B_m2977519662 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2[] UnityEngine_MeshGenerated::<Mesh_uv3>m__26C()
extern "C"  Vector2U5BU5D_t4024180168* UnityEngine_MeshGenerated_U3CMesh_uv3U3Em__26C_m2781007118 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2[] UnityEngine_MeshGenerated::<Mesh_uv4>m__26D()
extern "C"  Vector2U5BU5D_t4024180168* UnityEngine_MeshGenerated_U3CMesh_uv4U3Em__26D_m2584494574 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color[] UnityEngine_MeshGenerated::<Mesh_colors>m__26E()
extern "C"  ColorU5BU5D_t2441545636* UnityEngine_MeshGenerated_U3CMesh_colorsU3Em__26E_m1055209792 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color32[] UnityEngine_MeshGenerated::<Mesh_colors32>m__26F()
extern "C"  Color32U5BU5D_t2960766953* UnityEngine_MeshGenerated_U3CMesh_colors32U3Em__26F_m1292568963 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] UnityEngine_MeshGenerated::<Mesh_triangles>m__270()
extern "C"  Int32U5BU5D_t3230847821* UnityEngine_MeshGenerated_U3CMesh_trianglesU3Em__270_m837039726 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.BoneWeight[] UnityEngine_MeshGenerated::<Mesh_boneWeights>m__271()
extern "C"  BoneWeightU5BU5D_t1503835233* UnityEngine_MeshGenerated_U3CMesh_boneWeightsU3Em__271_m1602476423 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Matrix4x4[] UnityEngine_MeshGenerated::<Mesh_bindposes>m__272()
extern "C"  Matrix4x4U5BU5D_t1421664456* UnityEngine_MeshGenerated_U3CMesh_bindposesU3Em__272_m3060349257 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3[] UnityEngine_MeshGenerated::<Mesh_AddBlendShapeFrame__String__Single__Vector3_Array__Vector3_Array__Vector3_Array>m__273()
extern "C"  Vector3U5BU5D_t215400611* UnityEngine_MeshGenerated_U3CMesh_AddBlendShapeFrame__String__Single__Vector3_Array__Vector3_Array__Vector3_ArrayU3Em__273_m3141528219 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3[] UnityEngine_MeshGenerated::<Mesh_AddBlendShapeFrame__String__Single__Vector3_Array__Vector3_Array__Vector3_Array>m__274()
extern "C"  Vector3U5BU5D_t215400611* UnityEngine_MeshGenerated_U3CMesh_AddBlendShapeFrame__String__Single__Vector3_Array__Vector3_Array__Vector3_ArrayU3Em__274_m3141529180 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3[] UnityEngine_MeshGenerated::<Mesh_AddBlendShapeFrame__String__Single__Vector3_Array__Vector3_Array__Vector3_Array>m__275()
extern "C"  Vector3U5BU5D_t215400611* UnityEngine_MeshGenerated_U3CMesh_AddBlendShapeFrame__String__Single__Vector3_Array__Vector3_Array__Vector3_ArrayU3Em__275_m3141530141 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.CombineInstance[] UnityEngine_MeshGenerated::<Mesh_CombineMeshes__CombineInstance_Array__Boolean__Boolean>m__276()
extern "C"  CombineInstanceU5BU5D_t1850423023* UnityEngine_MeshGenerated_U3CMesh_CombineMeshes__CombineInstance_Array__Boolean__BooleanU3Em__276_m3174516475 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.CombineInstance[] UnityEngine_MeshGenerated::<Mesh_CombineMeshes__CombineInstance_Array__Boolean>m__277()
extern "C"  CombineInstanceU5BU5D_t1850423023* UnityEngine_MeshGenerated_U3CMesh_CombineMeshes__CombineInstance_Array__BooleanU3Em__277_m3233736420 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.CombineInstance[] UnityEngine_MeshGenerated::<Mesh_CombineMeshes__CombineInstance_Array>m__278()
extern "C"  CombineInstanceU5BU5D_t1850423023* UnityEngine_MeshGenerated_U3CMesh_CombineMeshes__CombineInstance_ArrayU3Em__278_m1383563645 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3[] UnityEngine_MeshGenerated::<Mesh_GetBlendShapeFrameVertices__Int32__Int32__Vector3_Array__Vector3_Array__Vector3_Array>m__279()
extern "C"  Vector3U5BU5D_t215400611* UnityEngine_MeshGenerated_U3CMesh_GetBlendShapeFrameVertices__Int32__Int32__Vector3_Array__Vector3_Array__Vector3_ArrayU3Em__279_m2082305420 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3[] UnityEngine_MeshGenerated::<Mesh_GetBlendShapeFrameVertices__Int32__Int32__Vector3_Array__Vector3_Array__Vector3_Array>m__27A()
extern "C"  Vector3U5BU5D_t215400611* UnityEngine_MeshGenerated_U3CMesh_GetBlendShapeFrameVertices__Int32__Int32__Vector3_Array__Vector3_Array__Vector3_ArrayU3Em__27A_m2082313108 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3[] UnityEngine_MeshGenerated::<Mesh_GetBlendShapeFrameVertices__Int32__Int32__Vector3_Array__Vector3_Array__Vector3_Array>m__27B()
extern "C"  Vector3U5BU5D_t215400611* UnityEngine_MeshGenerated_U3CMesh_GetBlendShapeFrameVertices__Int32__Int32__Vector3_Array__Vector3_Array__Vector3_ArrayU3Em__27B_m2082314069 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] UnityEngine_MeshGenerated::<Mesh_SetIndices__Int32_Array__MeshTopology__Int32>m__27C()
extern "C"  Int32U5BU5D_t3230847821* UnityEngine_MeshGenerated_U3CMesh_SetIndices__Int32_Array__MeshTopology__Int32U3Em__27C_m1829825125 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] UnityEngine_MeshGenerated::<Mesh_SetTriangles__Int32_Array__Int32>m__27D()
extern "C"  Int32U5BU5D_t3230847821* UnityEngine_MeshGenerated_U3CMesh_SetTriangles__Int32_Array__Int32U3Em__27D_m2873450534 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_MeshGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UnityEngine_MeshGenerated_ilo_getObject1_m3329860453 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MeshGenerated::ilo_attachFinalizerObject2(System.Int32)
extern "C"  bool UnityEngine_MeshGenerated_ilo_attachFinalizerObject2_m1266289255 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_MeshGenerated::ilo_setVector3S3(System.Int32,UnityEngine.Vector3)
extern "C"  void UnityEngine_MeshGenerated_ilo_setVector3S3_m3315011640 (Il2CppObject * __this /* static, unused */, int32_t ___e0, Vector3_t4282066566  ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_MeshGenerated::ilo_moveSaveID2Arr4(System.Int32)
extern "C"  void UnityEngine_MeshGenerated_ilo_moveSaveID2Arr4_m3540454025 (Il2CppObject * __this /* static, unused */, int32_t ___arrIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_MeshGenerated::ilo_setArrayS5(System.Int32,System.Int32,System.Boolean)
extern "C"  void UnityEngine_MeshGenerated_ilo_setArrayS5_m2709346102 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___count1, bool ___bClear2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_MeshGenerated::ilo_setVector2S6(System.Int32,UnityEngine.Vector2)
extern "C"  void UnityEngine_MeshGenerated_ilo_setVector2S6_m3866721243 (Il2CppObject * __this /* static, unused */, int32_t ___e0, Vector2_t4282066565  ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_MeshGenerated::ilo_setObject7(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UnityEngine_MeshGenerated_ilo_setObject7_m2436190307 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_MeshGenerated::ilo_getObject8(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_MeshGenerated_ilo_getObject8_m692487707 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_MeshGenerated::ilo_setInt329(System.Int32,System.Int32)
extern "C"  void UnityEngine_MeshGenerated_ilo_setInt329_m3268663293 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MeshGenerated::ilo_getBooleanS10(System.Int32)
extern "C"  bool UnityEngine_MeshGenerated_ilo_getBooleanS10_m3585249313 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_MeshGenerated::ilo_getInt3211(System.Int32)
extern "C"  int32_t UnityEngine_MeshGenerated_ilo_getInt3211_m2163410625 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_MeshGenerated::ilo_setStringS12(System.Int32,System.String)
extern "C"  void UnityEngine_MeshGenerated_ilo_setStringS12_m4004475540 (Il2CppObject * __this /* static, unused */, int32_t ___e0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_MeshGenerated::ilo_getArrayLength13(System.Int32)
extern "C"  int32_t UnityEngine_MeshGenerated_ilo_getArrayLength13_m3158587540 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_MeshGenerated::ilo_getElement14(System.Int32,System.Int32)
extern "C"  int32_t UnityEngine_MeshGenerated_ilo_getElement14_m1229869157 (Il2CppObject * __this /* static, unused */, int32_t ___id0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine_MeshGenerated::ilo_getVector3S15(System.Int32)
extern "C"  Vector3_t4282066566  UnityEngine_MeshGenerated_ilo_getVector3S15_m1047421060 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
