﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIEffectGenerated
struct UIEffectGenerated_t823190986;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// UIEffect
struct UIEffect_t251031877;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_UIEffect251031877.h"

// System.Void UIEffectGenerated::.ctor()
extern "C"  void UIEffectGenerated__ctor_m216509153 (UIEffectGenerated_t823190986 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIEffectGenerated::UIEffect_UIEffect1(JSVCall,System.Int32)
extern "C"  bool UIEffectGenerated_UIEffect_UIEffect1_m1256253969 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIEffectGenerated::UIEffect_Init(JSVCall,System.Int32)
extern "C"  bool UIEffectGenerated_UIEffect_Init_m628206413 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIEffectGenerated::__Register()
extern "C"  void UIEffectGenerated___Register_m3907417734 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIEffectGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UIEffectGenerated_ilo_getObject1_m1779333493 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIEffectGenerated::ilo_attachFinalizerObject2(System.Int32)
extern "C"  bool UIEffectGenerated_ilo_attachFinalizerObject2_m717652599 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIEffectGenerated::ilo_addJSCSRel3(System.Int32,System.Object)
extern "C"  void UIEffectGenerated_ilo_addJSCSRel3_m1049292319 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIEffectGenerated::ilo_Init4(UIEffect)
extern "C"  void UIEffectGenerated_ilo_Init4_m3864987627 (Il2CppObject * __this /* static, unused */, UIEffect_t251031877 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
