﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.WWWForm
struct WWWForm_t461342257;
// UnityEngine.WWW
struct WWW_t3134621005;
// Mihua.SDK.SdkwwwCallBack
struct SdkwwwCallBack_t3004559384;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mihua.SDK.Sdkwww/<RequestExe>c__Iterator38
struct  U3CRequestExeU3Ec__Iterator38_t3508389199  : public Il2CppObject
{
public:
	// System.String Mihua.SDK.Sdkwww/<RequestExe>c__Iterator38::url
	String_t* ___url_0;
	// UnityEngine.WWWForm Mihua.SDK.Sdkwww/<RequestExe>c__Iterator38::wwwForm
	WWWForm_t461342257 * ___wwwForm_1;
	// UnityEngine.WWW Mihua.SDK.Sdkwww/<RequestExe>c__Iterator38::<www>__0
	WWW_t3134621005 * ___U3CwwwU3E__0_2;
	// Mihua.SDK.SdkwwwCallBack Mihua.SDK.Sdkwww/<RequestExe>c__Iterator38::callBack
	SdkwwwCallBack_t3004559384 * ___callBack_3;
	// System.Int32 Mihua.SDK.Sdkwww/<RequestExe>c__Iterator38::$PC
	int32_t ___U24PC_4;
	// System.Object Mihua.SDK.Sdkwww/<RequestExe>c__Iterator38::$current
	Il2CppObject * ___U24current_5;
	// System.String Mihua.SDK.Sdkwww/<RequestExe>c__Iterator38::<$>url
	String_t* ___U3CU24U3Eurl_6;
	// UnityEngine.WWWForm Mihua.SDK.Sdkwww/<RequestExe>c__Iterator38::<$>wwwForm
	WWWForm_t461342257 * ___U3CU24U3EwwwForm_7;
	// Mihua.SDK.SdkwwwCallBack Mihua.SDK.Sdkwww/<RequestExe>c__Iterator38::<$>callBack
	SdkwwwCallBack_t3004559384 * ___U3CU24U3EcallBack_8;

public:
	inline static int32_t get_offset_of_url_0() { return static_cast<int32_t>(offsetof(U3CRequestExeU3Ec__Iterator38_t3508389199, ___url_0)); }
	inline String_t* get_url_0() const { return ___url_0; }
	inline String_t** get_address_of_url_0() { return &___url_0; }
	inline void set_url_0(String_t* value)
	{
		___url_0 = value;
		Il2CppCodeGenWriteBarrier(&___url_0, value);
	}

	inline static int32_t get_offset_of_wwwForm_1() { return static_cast<int32_t>(offsetof(U3CRequestExeU3Ec__Iterator38_t3508389199, ___wwwForm_1)); }
	inline WWWForm_t461342257 * get_wwwForm_1() const { return ___wwwForm_1; }
	inline WWWForm_t461342257 ** get_address_of_wwwForm_1() { return &___wwwForm_1; }
	inline void set_wwwForm_1(WWWForm_t461342257 * value)
	{
		___wwwForm_1 = value;
		Il2CppCodeGenWriteBarrier(&___wwwForm_1, value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E__0_2() { return static_cast<int32_t>(offsetof(U3CRequestExeU3Ec__Iterator38_t3508389199, ___U3CwwwU3E__0_2)); }
	inline WWW_t3134621005 * get_U3CwwwU3E__0_2() const { return ___U3CwwwU3E__0_2; }
	inline WWW_t3134621005 ** get_address_of_U3CwwwU3E__0_2() { return &___U3CwwwU3E__0_2; }
	inline void set_U3CwwwU3E__0_2(WWW_t3134621005 * value)
	{
		___U3CwwwU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CwwwU3E__0_2, value);
	}

	inline static int32_t get_offset_of_callBack_3() { return static_cast<int32_t>(offsetof(U3CRequestExeU3Ec__Iterator38_t3508389199, ___callBack_3)); }
	inline SdkwwwCallBack_t3004559384 * get_callBack_3() const { return ___callBack_3; }
	inline SdkwwwCallBack_t3004559384 ** get_address_of_callBack_3() { return &___callBack_3; }
	inline void set_callBack_3(SdkwwwCallBack_t3004559384 * value)
	{
		___callBack_3 = value;
		Il2CppCodeGenWriteBarrier(&___callBack_3, value);
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CRequestExeU3Ec__Iterator38_t3508389199, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CRequestExeU3Ec__Iterator38_t3508389199, ___U24current_5)); }
	inline Il2CppObject * get_U24current_5() const { return ___U24current_5; }
	inline Il2CppObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(Il2CppObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_5, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Eurl_6() { return static_cast<int32_t>(offsetof(U3CRequestExeU3Ec__Iterator38_t3508389199, ___U3CU24U3Eurl_6)); }
	inline String_t* get_U3CU24U3Eurl_6() const { return ___U3CU24U3Eurl_6; }
	inline String_t** get_address_of_U3CU24U3Eurl_6() { return &___U3CU24U3Eurl_6; }
	inline void set_U3CU24U3Eurl_6(String_t* value)
	{
		___U3CU24U3Eurl_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Eurl_6, value);
	}

	inline static int32_t get_offset_of_U3CU24U3EwwwForm_7() { return static_cast<int32_t>(offsetof(U3CRequestExeU3Ec__Iterator38_t3508389199, ___U3CU24U3EwwwForm_7)); }
	inline WWWForm_t461342257 * get_U3CU24U3EwwwForm_7() const { return ___U3CU24U3EwwwForm_7; }
	inline WWWForm_t461342257 ** get_address_of_U3CU24U3EwwwForm_7() { return &___U3CU24U3EwwwForm_7; }
	inline void set_U3CU24U3EwwwForm_7(WWWForm_t461342257 * value)
	{
		___U3CU24U3EwwwForm_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3EwwwForm_7, value);
	}

	inline static int32_t get_offset_of_U3CU24U3EcallBack_8() { return static_cast<int32_t>(offsetof(U3CRequestExeU3Ec__Iterator38_t3508389199, ___U3CU24U3EcallBack_8)); }
	inline SdkwwwCallBack_t3004559384 * get_U3CU24U3EcallBack_8() const { return ___U3CU24U3EcallBack_8; }
	inline SdkwwwCallBack_t3004559384 ** get_address_of_U3CU24U3EcallBack_8() { return &___U3CU24U3EcallBack_8; }
	inline void set_U3CU24U3EcallBack_8(SdkwwwCallBack_t3004559384 * value)
	{
		___U3CU24U3EcallBack_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3EcallBack_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
