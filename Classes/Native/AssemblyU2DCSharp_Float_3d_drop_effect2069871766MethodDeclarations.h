﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Float_3d_drop_effect
struct Float_3d_drop_effect_t2069871766;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// Mihua.Asset.ABLoadOperation.AssetOperation
struct AssetOperation_t778728221;
// FloatTextUnit
struct FloatTextUnit_t2362298029;
// UITweener
struct UITweener_t105489188;
// UnityEngine.Object
struct Object_t3071478659;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "AssemblyU2DCSharp_FLOAT_TEXT_ID2199345770.h"
#include "AssemblyU2DCSharp_Mihua_Asset_ABLoadOperation_Asset778728221.h"
#include "AssemblyU2DCSharp_FloatTextUnit2362298029.h"
#include "AssemblyU2DCSharp_UITweener105489188.h"

// System.Void Float_3d_drop_effect::.ctor()
extern "C"  void Float_3d_drop_effect__ctor_m147089093 (Float_3d_drop_effect_t2069871766 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_3d_drop_effect::OnAwake(UnityEngine.GameObject)
extern "C"  void Float_3d_drop_effect_OnAwake_m2326760321 (Float_3d_drop_effect_t2069871766 * __this, GameObject_t3674682005 * ___viewGO0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_3d_drop_effect::OnDestroy()
extern "C"  void Float_3d_drop_effect_OnDestroy_m1053511998 (Float_3d_drop_effect_t2069871766 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FLOAT_TEXT_ID Float_3d_drop_effect::FloatID()
extern "C"  int32_t Float_3d_drop_effect_FloatID_m2548196689 (Float_3d_drop_effect_t2069871766 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_3d_drop_effect::Play()
extern "C"  void Float_3d_drop_effect_Play_m847341587 (Float_3d_drop_effect_t2069871766 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_3d_drop_effect::Death()
extern "C"  void Float_3d_drop_effect_Death_m2532151639 (Float_3d_drop_effect_t2069871766 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_3d_drop_effect::SetFile(System.Object[])
extern "C"  void Float_3d_drop_effect_SetFile_m953416881 (Float_3d_drop_effect_t2069871766 * __this, ObjectU5BU5D_t1108656482* ___args0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_3d_drop_effect::<SetFile>m__3DE(Mihua.Asset.ABLoadOperation.AssetOperation)
extern "C"  void Float_3d_drop_effect_U3CSetFileU3Em__3DE_m694709453 (Float_3d_drop_effect_t2069871766 * __this, AssetOperation_t778728221 * ___ao0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_3d_drop_effect::ilo_set_IsLife1(FloatTextUnit,System.Boolean)
extern "C"  void Float_3d_drop_effect_ilo_set_IsLife1_m1551779990 (Il2CppObject * __this /* static, unused */, FloatTextUnit_t2362298029 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_3d_drop_effect::ilo_ResetToBeginning2(UITweener)
extern "C"  void Float_3d_drop_effect_ilo_ResetToBeginning2_m1052859039 (Il2CppObject * __this /* static, unused */, UITweener_t105489188 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object Float_3d_drop_effect::ilo_GetAsset3(Mihua.Asset.ABLoadOperation.AssetOperation)
extern "C"  Object_t3071478659 * Float_3d_drop_effect_ilo_GetAsset3_m1071997705 (Il2CppObject * __this /* static, unused */, AssetOperation_t778728221 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject Float_3d_drop_effect::ilo_Instantiate4(UnityEngine.GameObject)
extern "C"  GameObject_t3674682005 * Float_3d_drop_effect_ilo_Instantiate4_m661910601 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
