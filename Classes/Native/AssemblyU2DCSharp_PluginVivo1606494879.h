﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PluginVivo
struct  PluginVivo_t1606494879  : public MonoBehaviour_t667441552
{
public:
	// System.String PluginVivo::roleId
	String_t* ___roleId_2;
	// System.String PluginVivo::roleName
	String_t* ___roleName_3;
	// System.String PluginVivo::roleLevel
	String_t* ___roleLevel_4;
	// System.String PluginVivo::serverId
	String_t* ___serverId_5;
	// System.String PluginVivo::serverName
	String_t* ___serverName_6;
	// System.String PluginVivo::tokenId
	String_t* ___tokenId_7;
	// System.String PluginVivo::userId
	String_t* ___userId_8;
	// System.String PluginVivo::channelId
	String_t* ___channelId_9;
	// System.Single PluginVivo::lastSdkLoginOkTime
	float ___lastSdkLoginOkTime_10;

public:
	inline static int32_t get_offset_of_roleId_2() { return static_cast<int32_t>(offsetof(PluginVivo_t1606494879, ___roleId_2)); }
	inline String_t* get_roleId_2() const { return ___roleId_2; }
	inline String_t** get_address_of_roleId_2() { return &___roleId_2; }
	inline void set_roleId_2(String_t* value)
	{
		___roleId_2 = value;
		Il2CppCodeGenWriteBarrier(&___roleId_2, value);
	}

	inline static int32_t get_offset_of_roleName_3() { return static_cast<int32_t>(offsetof(PluginVivo_t1606494879, ___roleName_3)); }
	inline String_t* get_roleName_3() const { return ___roleName_3; }
	inline String_t** get_address_of_roleName_3() { return &___roleName_3; }
	inline void set_roleName_3(String_t* value)
	{
		___roleName_3 = value;
		Il2CppCodeGenWriteBarrier(&___roleName_3, value);
	}

	inline static int32_t get_offset_of_roleLevel_4() { return static_cast<int32_t>(offsetof(PluginVivo_t1606494879, ___roleLevel_4)); }
	inline String_t* get_roleLevel_4() const { return ___roleLevel_4; }
	inline String_t** get_address_of_roleLevel_4() { return &___roleLevel_4; }
	inline void set_roleLevel_4(String_t* value)
	{
		___roleLevel_4 = value;
		Il2CppCodeGenWriteBarrier(&___roleLevel_4, value);
	}

	inline static int32_t get_offset_of_serverId_5() { return static_cast<int32_t>(offsetof(PluginVivo_t1606494879, ___serverId_5)); }
	inline String_t* get_serverId_5() const { return ___serverId_5; }
	inline String_t** get_address_of_serverId_5() { return &___serverId_5; }
	inline void set_serverId_5(String_t* value)
	{
		___serverId_5 = value;
		Il2CppCodeGenWriteBarrier(&___serverId_5, value);
	}

	inline static int32_t get_offset_of_serverName_6() { return static_cast<int32_t>(offsetof(PluginVivo_t1606494879, ___serverName_6)); }
	inline String_t* get_serverName_6() const { return ___serverName_6; }
	inline String_t** get_address_of_serverName_6() { return &___serverName_6; }
	inline void set_serverName_6(String_t* value)
	{
		___serverName_6 = value;
		Il2CppCodeGenWriteBarrier(&___serverName_6, value);
	}

	inline static int32_t get_offset_of_tokenId_7() { return static_cast<int32_t>(offsetof(PluginVivo_t1606494879, ___tokenId_7)); }
	inline String_t* get_tokenId_7() const { return ___tokenId_7; }
	inline String_t** get_address_of_tokenId_7() { return &___tokenId_7; }
	inline void set_tokenId_7(String_t* value)
	{
		___tokenId_7 = value;
		Il2CppCodeGenWriteBarrier(&___tokenId_7, value);
	}

	inline static int32_t get_offset_of_userId_8() { return static_cast<int32_t>(offsetof(PluginVivo_t1606494879, ___userId_8)); }
	inline String_t* get_userId_8() const { return ___userId_8; }
	inline String_t** get_address_of_userId_8() { return &___userId_8; }
	inline void set_userId_8(String_t* value)
	{
		___userId_8 = value;
		Il2CppCodeGenWriteBarrier(&___userId_8, value);
	}

	inline static int32_t get_offset_of_channelId_9() { return static_cast<int32_t>(offsetof(PluginVivo_t1606494879, ___channelId_9)); }
	inline String_t* get_channelId_9() const { return ___channelId_9; }
	inline String_t** get_address_of_channelId_9() { return &___channelId_9; }
	inline void set_channelId_9(String_t* value)
	{
		___channelId_9 = value;
		Il2CppCodeGenWriteBarrier(&___channelId_9, value);
	}

	inline static int32_t get_offset_of_lastSdkLoginOkTime_10() { return static_cast<int32_t>(offsetof(PluginVivo_t1606494879, ___lastSdkLoginOkTime_10)); }
	inline float get_lastSdkLoginOkTime_10() const { return ___lastSdkLoginOkTime_10; }
	inline float* get_address_of_lastSdkLoginOkTime_10() { return &___lastSdkLoginOkTime_10; }
	inline void set_lastSdkLoginOkTime_10(float value)
	{
		___lastSdkLoginOkTime_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
