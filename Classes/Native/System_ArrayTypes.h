﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


// System.Collections.Generic.RBTree/Node
struct Node_t523796052;
// System.ComponentModel.PropertyDescriptor
struct PropertyDescriptor_t2073374448;
// System.ComponentModel.MemberDescriptor
struct MemberDescriptor_t2617136693;
// System.ComponentModel.EventDescriptor
struct EventDescriptor_t1405012495;
// System.ComponentModel.TypeDescriptionProvider
struct TypeDescriptionProvider_t3543085017;
// System.Collections.Generic.LinkedList`1<System.ComponentModel.TypeDescriptionProvider>
struct LinkedList_1_t1417720186;
// System.ComponentModel.WeakObjectWrapper
struct WeakObjectWrapper_t1518976226;
// System.Net.Mail.LinkedResource
struct LinkedResource_t2218228715;
// System.Net.Mail.AlternateView
struct AlternateView_t1036763893;
// System.Net.Mail.Attachment
struct Attachment_t2730920775;
// System.Net.Mail.MailAddress
struct MailAddress_t2991723827;
// System.Net.Mail.SmtpFailedRecipientException
struct SmtpFailedRecipientException_t2446799917;
// System.Net.IPAddress
struct IPAddress_t3525271463;
// System.Net.Cookie
struct Cookie_t2033273982;
// System.Text.RegularExpressions.Capture
struct Capture_t754001812;
// System.Text.RegularExpressions.Group
struct Group_t2151468941;
// System.Text.RegularExpressions.Regex
struct Regex_t2161232213;
// System.Collections.Generic.Stack`1<System.Object>
struct Stack_1_t2974409999;
// System.Collections.Generic.Stack`1<Pathfinding.GraphNode>
struct Stack_1_t3122173294;

#include "mscorlib_System_Array1146569071.h"
#include "System_System_Collections_Generic_RBTree_Node523796052.h"
#include "System_System_ComponentModel_PropertyDescriptor2073374448.h"
#include "System_System_ComponentModel_MemberDescriptor2617136693.h"
#include "System_System_ComponentModel_EventDescriptor1405012495.h"
#include "System_System_ComponentModel_TypeDescriptionProvid3543085017.h"
#include "System_System_Collections_Generic_LinkedList_1_gen1417720186.h"
#include "System_System_ComponentModel_WeakObjectWrapper1518976226.h"
#include "System_System_Net_Mail_LinkedResource2218228715.h"
#include "System_System_Net_Mail_AlternateView1036763893.h"
#include "System_System_Net_Mail_Attachment2730920775.h"
#include "System_System_Net_Mail_MailAddress2991723827.h"
#include "System_System_Net_Mail_SmtpFailedRecipientExceptio2446799917.h"
#include "System_System_Security_Cryptography_X509Certificate766901931.h"
#include "System_System_Net_IPAddress3525271463.h"
#include "System_System_Net_Cookie2033273982.h"
#include "System_System_Text_RegularExpressions_Capture754001812.h"
#include "System_System_Text_RegularExpressions_Group2151468941.h"
#include "System_System_Text_RegularExpressions_Mark3811539797.h"
#include "System_System_Uri_UriScheme1290668975.h"
#include "System_System_Text_RegularExpressions_Regex2161232213.h"
#include "System_System_Collections_Generic_Stack_1_gen2974409999.h"
#include "System_System_Collections_Generic_Stack_1_gen3122173294.h"

#pragma once
// System.Collections.Generic.RBTree/Node[]
struct NodeU5BU5D_t55392733  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Node_t523796052 * m_Items[1];

public:
	inline Node_t523796052 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Node_t523796052 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Node_t523796052 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.ComponentModel.PropertyDescriptor[]
struct PropertyDescriptorU5BU5D_t917702481  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) PropertyDescriptor_t2073374448 * m_Items[1];

public:
	inline PropertyDescriptor_t2073374448 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline PropertyDescriptor_t2073374448 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, PropertyDescriptor_t2073374448 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.ComponentModel.MemberDescriptor[]
struct MemberDescriptorU5BU5D_t1625508184  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) MemberDescriptor_t2617136693 * m_Items[1];

public:
	inline MemberDescriptor_t2617136693 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline MemberDescriptor_t2617136693 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, MemberDescriptor_t2617136693 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.ComponentModel.EventDescriptor[]
struct EventDescriptorU5BU5D_t1455053014  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) EventDescriptor_t1405012495 * m_Items[1];

public:
	inline EventDescriptor_t1405012495 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline EventDescriptor_t1405012495 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, EventDescriptor_t1405012495 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.ComponentModel.TypeDescriptionProvider[]
struct TypeDescriptionProviderU5BU5D_t4220469412  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) TypeDescriptionProvider_t3543085017 * m_Items[1];

public:
	inline TypeDescriptionProvider_t3543085017 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline TypeDescriptionProvider_t3543085017 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, TypeDescriptionProvider_t3543085017 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.LinkedList`1<System.ComponentModel.TypeDescriptionProvider>[]
struct LinkedList_1U5BU5D_t1670055775  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) LinkedList_1_t1417720186 * m_Items[1];

public:
	inline LinkedList_1_t1417720186 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline LinkedList_1_t1417720186 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, LinkedList_1_t1417720186 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.ComponentModel.WeakObjectWrapper[]
struct WeakObjectWrapperU5BU5D_t3390019927  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) WeakObjectWrapper_t1518976226 * m_Items[1];

public:
	inline WeakObjectWrapper_t1518976226 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline WeakObjectWrapper_t1518976226 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, WeakObjectWrapper_t1518976226 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Net.Mail.LinkedResource[]
struct LinkedResourceU5BU5D_t4016027658  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) LinkedResource_t2218228715 * m_Items[1];

public:
	inline LinkedResource_t2218228715 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline LinkedResource_t2218228715 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, LinkedResource_t2218228715 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Net.Mail.AlternateView[]
struct AlternateViewU5BU5D_t4260417944  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) AlternateView_t1036763893 * m_Items[1];

public:
	inline AlternateView_t1036763893 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline AlternateView_t1036763893 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, AlternateView_t1036763893 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Net.Mail.Attachment[]
struct AttachmentU5BU5D_t2874282942  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Attachment_t2730920775 * m_Items[1];

public:
	inline Attachment_t2730920775 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Attachment_t2730920775 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Attachment_t2730920775 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Net.Mail.MailAddress[]
struct MailAddressU5BU5D_t3312478370  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) MailAddress_t2991723827 * m_Items[1];

public:
	inline MailAddress_t2991723827 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline MailAddress_t2991723827 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, MailAddress_t2991723827 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Net.Mail.SmtpFailedRecipientException[]
struct SmtpFailedRecipientExceptionU5BU5D_t3524271232  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) SmtpFailedRecipientException_t2446799917 * m_Items[1];

public:
	inline SmtpFailedRecipientException_t2446799917 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline SmtpFailedRecipientException_t2446799917 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, SmtpFailedRecipientException_t2446799917 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Security.Cryptography.X509Certificates.X509ChainStatus[]
struct X509ChainStatusU5BU5D_t2899776074  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) X509ChainStatus_t766901931  m_Items[1];

public:
	inline X509ChainStatus_t766901931  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline X509ChainStatus_t766901931 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, X509ChainStatus_t766901931  value)
	{
		m_Items[index] = value;
	}
};
// System.Net.IPAddress[]
struct IPAddressU5BU5D_t1215594974  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) IPAddress_t3525271463 * m_Items[1];

public:
	inline IPAddress_t3525271463 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline IPAddress_t3525271463 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, IPAddress_t3525271463 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Net.Cookie[]
struct CookieU5BU5D_t1520446411  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Cookie_t2033273982 * m_Items[1];

public:
	inline Cookie_t2033273982 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Cookie_t2033273982 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Cookie_t2033273982 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Text.RegularExpressions.Capture[]
struct CaptureU5BU5D_t3823141789  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Capture_t754001812 * m_Items[1];

public:
	inline Capture_t754001812 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Capture_t754001812 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Capture_t754001812 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Text.RegularExpressions.Group[]
struct GroupU5BU5D_t1259259808  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Group_t2151468941 * m_Items[1];

public:
	inline Group_t2151468941 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Group_t2151468941 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Group_t2151468941 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Text.RegularExpressions.Mark[]
struct MarkU5BU5D_t581716920  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Mark_t3811539797  m_Items[1];

public:
	inline Mark_t3811539797  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Mark_t3811539797 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Mark_t3811539797  value)
	{
		m_Items[index] = value;
	}
};
// System.Uri/UriScheme[]
struct UriSchemeU5BU5D_t4293835958  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) UriScheme_t1290668975  m_Items[1];

public:
	inline UriScheme_t1290668975  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline UriScheme_t1290668975 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, UriScheme_t1290668975  value)
	{
		m_Items[index] = value;
	}
};
// System.Text.RegularExpressions.Regex[]
struct RegexU5BU5D_t3722511800  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Regex_t2161232213 * m_Items[1];

public:
	inline Regex_t2161232213 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Regex_t2161232213 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Regex_t2161232213 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.Stack`1<System.Object>[]
struct Stack_1U5BU5D_t3251663318  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Stack_1_t2974409999 * m_Items[1];

public:
	inline Stack_1_t2974409999 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Stack_1_t2974409999 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Stack_1_t2974409999 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.Stack`1<Pathfinding.GraphNode>[]
struct Stack_1U5BU5D_t3070456091  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Stack_1_t3122173294 * m_Items[1];

public:
	inline Stack_1_t3122173294 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Stack_1_t3122173294 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Stack_1_t3122173294 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
