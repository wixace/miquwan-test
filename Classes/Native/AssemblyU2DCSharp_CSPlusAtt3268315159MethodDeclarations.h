﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CSPlusAtt
struct CSPlusAtt_t3268315159;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void CSPlusAtt::.ctor(System.String,System.UInt32,System.UInt32)
extern "C"  void CSPlusAtt__ctor_m2662130870 (CSPlusAtt_t3268315159 * __this, String_t* ___key0, uint32_t ___value1, uint32_t ___per2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
