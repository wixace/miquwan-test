﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_cejatal127
struct M_cejatal127_t611124192;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_cejatal127611124192.h"

// System.Void GarbageiOS.M_cejatal127::.ctor()
extern "C"  void M_cejatal127__ctor_m1851827283 (M_cejatal127_t611124192 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_cejatal127::M_tagereli0(System.String[],System.Int32)
extern "C"  void M_cejatal127_M_tagereli0_m3952943787 (M_cejatal127_t611124192 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_cejatal127::M_jetirea1(System.String[],System.Int32)
extern "C"  void M_cejatal127_M_jetirea1_m4076871021 (M_cejatal127_t611124192 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_cejatal127::M_tepear2(System.String[],System.Int32)
extern "C"  void M_cejatal127_M_tepear2_m2603928017 (M_cejatal127_t611124192 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_cejatal127::M_bamaihay3(System.String[],System.Int32)
extern "C"  void M_cejatal127_M_bamaihay3_m3786908895 (M_cejatal127_t611124192 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_cejatal127::M_canas4(System.String[],System.Int32)
extern "C"  void M_cejatal127_M_canas4_m89844684 (M_cejatal127_t611124192 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_cejatal127::M_hasbe5(System.String[],System.Int32)
extern "C"  void M_cejatal127_M_hasbe5_m2622677074 (M_cejatal127_t611124192 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_cejatal127::M_jouserehorJawperzi6(System.String[],System.Int32)
extern "C"  void M_cejatal127_M_jouserehorJawperzi6_m1153114602 (M_cejatal127_t611124192 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_cejatal127::M_semmeta7(System.String[],System.Int32)
extern "C"  void M_cejatal127_M_semmeta7_m2399916913 (M_cejatal127_t611124192 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_cejatal127::ilo_M_bamaihay31(GarbageiOS.M_cejatal127,System.String[],System.Int32)
extern "C"  void M_cejatal127_ilo_M_bamaihay31_m1161588929 (Il2CppObject * __this /* static, unused */, M_cejatal127_t611124192 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
