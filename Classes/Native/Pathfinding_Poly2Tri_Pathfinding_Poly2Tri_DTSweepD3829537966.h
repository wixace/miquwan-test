﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Pathfinding.Poly2Tri.DelaunayTriangle
struct DelaunayTriangle_t2835103587;
// Pathfinding.Poly2Tri.TriangulationPoint
struct TriangulationPoint_t3810082933;
// Pathfinding.Poly2Tri.AdvancingFrontNode
struct AdvancingFrontNode_t688967424;
// Pathfinding.Poly2Tri.DTSweepConstraint
struct DTSweepConstraint_t3360279023;

#include "Pathfinding_Poly2Tri_Pathfinding_Poly2Tri_Triangul3598090913.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.Poly2Tri.DTSweepDebugContext
struct  DTSweepDebugContext_t3829537966  : public TriangulationDebugContext_t3598090913
{
public:
	// Pathfinding.Poly2Tri.DelaunayTriangle Pathfinding.Poly2Tri.DTSweepDebugContext::_primaryTriangle
	DelaunayTriangle_t2835103587 * ____primaryTriangle_1;
	// Pathfinding.Poly2Tri.DelaunayTriangle Pathfinding.Poly2Tri.DTSweepDebugContext::_secondaryTriangle
	DelaunayTriangle_t2835103587 * ____secondaryTriangle_2;
	// Pathfinding.Poly2Tri.TriangulationPoint Pathfinding.Poly2Tri.DTSweepDebugContext::_activePoint
	TriangulationPoint_t3810082933 * ____activePoint_3;
	// Pathfinding.Poly2Tri.AdvancingFrontNode Pathfinding.Poly2Tri.DTSweepDebugContext::_activeNode
	AdvancingFrontNode_t688967424 * ____activeNode_4;
	// Pathfinding.Poly2Tri.DTSweepConstraint Pathfinding.Poly2Tri.DTSweepDebugContext::_activeConstraint
	DTSweepConstraint_t3360279023 * ____activeConstraint_5;

public:
	inline static int32_t get_offset_of__primaryTriangle_1() { return static_cast<int32_t>(offsetof(DTSweepDebugContext_t3829537966, ____primaryTriangle_1)); }
	inline DelaunayTriangle_t2835103587 * get__primaryTriangle_1() const { return ____primaryTriangle_1; }
	inline DelaunayTriangle_t2835103587 ** get_address_of__primaryTriangle_1() { return &____primaryTriangle_1; }
	inline void set__primaryTriangle_1(DelaunayTriangle_t2835103587 * value)
	{
		____primaryTriangle_1 = value;
		Il2CppCodeGenWriteBarrier(&____primaryTriangle_1, value);
	}

	inline static int32_t get_offset_of__secondaryTriangle_2() { return static_cast<int32_t>(offsetof(DTSweepDebugContext_t3829537966, ____secondaryTriangle_2)); }
	inline DelaunayTriangle_t2835103587 * get__secondaryTriangle_2() const { return ____secondaryTriangle_2; }
	inline DelaunayTriangle_t2835103587 ** get_address_of__secondaryTriangle_2() { return &____secondaryTriangle_2; }
	inline void set__secondaryTriangle_2(DelaunayTriangle_t2835103587 * value)
	{
		____secondaryTriangle_2 = value;
		Il2CppCodeGenWriteBarrier(&____secondaryTriangle_2, value);
	}

	inline static int32_t get_offset_of__activePoint_3() { return static_cast<int32_t>(offsetof(DTSweepDebugContext_t3829537966, ____activePoint_3)); }
	inline TriangulationPoint_t3810082933 * get__activePoint_3() const { return ____activePoint_3; }
	inline TriangulationPoint_t3810082933 ** get_address_of__activePoint_3() { return &____activePoint_3; }
	inline void set__activePoint_3(TriangulationPoint_t3810082933 * value)
	{
		____activePoint_3 = value;
		Il2CppCodeGenWriteBarrier(&____activePoint_3, value);
	}

	inline static int32_t get_offset_of__activeNode_4() { return static_cast<int32_t>(offsetof(DTSweepDebugContext_t3829537966, ____activeNode_4)); }
	inline AdvancingFrontNode_t688967424 * get__activeNode_4() const { return ____activeNode_4; }
	inline AdvancingFrontNode_t688967424 ** get_address_of__activeNode_4() { return &____activeNode_4; }
	inline void set__activeNode_4(AdvancingFrontNode_t688967424 * value)
	{
		____activeNode_4 = value;
		Il2CppCodeGenWriteBarrier(&____activeNode_4, value);
	}

	inline static int32_t get_offset_of__activeConstraint_5() { return static_cast<int32_t>(offsetof(DTSweepDebugContext_t3829537966, ____activeConstraint_5)); }
	inline DTSweepConstraint_t3360279023 * get__activeConstraint_5() const { return ____activeConstraint_5; }
	inline DTSweepConstraint_t3360279023 ** get_address_of__activeConstraint_5() { return &____activeConstraint_5; }
	inline void set__activeConstraint_5(DTSweepConstraint_t3360279023 * value)
	{
		____activeConstraint_5 = value;
		Il2CppCodeGenWriteBarrier(&____activeConstraint_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
