﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Pathfinding.GraphNode
struct GraphNode_t23612370;
// Pathfinding.FloodPath
struct FloodPath_t3766979749;
// Pathfinding.EuclideanEmbedding/<RecalculateCosts>c__AnonStorey120
struct U3CRecalculateCostsU3Ec__AnonStorey120_t3302164579;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.EuclideanEmbedding/<RecalculateCosts>c__AnonStorey120/<RecalculateCosts>c__AnonStorey11E
struct  U3CRecalculateCostsU3Ec__AnonStorey11E_t853919929  : public Il2CppObject
{
public:
	// Pathfinding.GraphNode Pathfinding.EuclideanEmbedding/<RecalculateCosts>c__AnonStorey120/<RecalculateCosts>c__AnonStorey11E::pivot
	GraphNode_t23612370 * ___pivot_0;
	// System.Int32 Pathfinding.EuclideanEmbedding/<RecalculateCosts>c__AnonStorey120/<RecalculateCosts>c__AnonStorey11E::k
	int32_t ___k_1;
	// Pathfinding.FloodPath Pathfinding.EuclideanEmbedding/<RecalculateCosts>c__AnonStorey120/<RecalculateCosts>c__AnonStorey11E::fp
	FloodPath_t3766979749 * ___fp_2;
	// Pathfinding.EuclideanEmbedding/<RecalculateCosts>c__AnonStorey120 Pathfinding.EuclideanEmbedding/<RecalculateCosts>c__AnonStorey120/<RecalculateCosts>c__AnonStorey11E::<>f__ref$288
	U3CRecalculateCostsU3Ec__AnonStorey120_t3302164579 * ___U3CU3Ef__refU24288_3;

public:
	inline static int32_t get_offset_of_pivot_0() { return static_cast<int32_t>(offsetof(U3CRecalculateCostsU3Ec__AnonStorey11E_t853919929, ___pivot_0)); }
	inline GraphNode_t23612370 * get_pivot_0() const { return ___pivot_0; }
	inline GraphNode_t23612370 ** get_address_of_pivot_0() { return &___pivot_0; }
	inline void set_pivot_0(GraphNode_t23612370 * value)
	{
		___pivot_0 = value;
		Il2CppCodeGenWriteBarrier(&___pivot_0, value);
	}

	inline static int32_t get_offset_of_k_1() { return static_cast<int32_t>(offsetof(U3CRecalculateCostsU3Ec__AnonStorey11E_t853919929, ___k_1)); }
	inline int32_t get_k_1() const { return ___k_1; }
	inline int32_t* get_address_of_k_1() { return &___k_1; }
	inline void set_k_1(int32_t value)
	{
		___k_1 = value;
	}

	inline static int32_t get_offset_of_fp_2() { return static_cast<int32_t>(offsetof(U3CRecalculateCostsU3Ec__AnonStorey11E_t853919929, ___fp_2)); }
	inline FloodPath_t3766979749 * get_fp_2() const { return ___fp_2; }
	inline FloodPath_t3766979749 ** get_address_of_fp_2() { return &___fp_2; }
	inline void set_fp_2(FloodPath_t3766979749 * value)
	{
		___fp_2 = value;
		Il2CppCodeGenWriteBarrier(&___fp_2, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU24288_3() { return static_cast<int32_t>(offsetof(U3CRecalculateCostsU3Ec__AnonStorey11E_t853919929, ___U3CU3Ef__refU24288_3)); }
	inline U3CRecalculateCostsU3Ec__AnonStorey120_t3302164579 * get_U3CU3Ef__refU24288_3() const { return ___U3CU3Ef__refU24288_3; }
	inline U3CRecalculateCostsU3Ec__AnonStorey120_t3302164579 ** get_address_of_U3CU3Ef__refU24288_3() { return &___U3CU3Ef__refU24288_3; }
	inline void set_U3CU3Ef__refU24288_3(U3CRecalculateCostsU3Ec__AnonStorey120_t3302164579 * value)
	{
		___U3CU3Ef__refU24288_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU24288_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
