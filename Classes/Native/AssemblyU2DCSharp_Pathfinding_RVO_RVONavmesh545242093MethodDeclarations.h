﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.RVO.RVONavmesh
struct RVONavmesh_t545242093;
// Pathfinding.RVO.Simulator
struct Simulator_t2705969170;
// Pathfinding.NavGraph
struct NavGraph_t1254319713;
// Pathfinding.NavGraph[]
struct NavGraphU5BU5D_t850130684;
// AstarPath
struct AstarPath_t4090270936;
// Pathfinding.INavmesh
struct INavmesh_t889550173;
// Pathfinding.GraphNodeDelegateCancelable
struct GraphNodeDelegateCancelable_t3591372971;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_RVO_Simulator2705969170.h"
#include "AssemblyU2DCSharp_Pathfinding_NavGraph1254319713.h"
#include "AssemblyU2DCSharp_Pathfinding_RVO_RVONavmesh545242093.h"
#include "AssemblyU2DCSharp_AstarPath4090270936.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphNodeDelegateCan3591372971.h"

// System.Void Pathfinding.RVO.RVONavmesh::.ctor()
extern "C"  void RVONavmesh__ctor_m2683907465 (RVONavmesh_t545242093 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RVO.RVONavmesh::OnPostCacheLoad()
extern "C"  void RVONavmesh_OnPostCacheLoad_m3924200464 (RVONavmesh_t545242093 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RVO.RVONavmesh::OnLatePostScan()
extern "C"  void RVONavmesh_OnLatePostScan_m3342571805 (RVONavmesh_t545242093 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RVO.RVONavmesh::RemoveObstacles()
extern "C"  void RVONavmesh_RemoveObstacles_m3641962183 (RVONavmesh_t545242093 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RVO.RVONavmesh::AddGraphObstacles(Pathfinding.RVO.Simulator,Pathfinding.NavGraph)
extern "C"  void RVONavmesh_AddGraphObstacles_m3509437780 (RVONavmesh_t545242093 * __this, Simulator_t2705969170 * ___sim0, NavGraph_t1254319713 * ___graph1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RVO.RVONavmesh::ilo_RemoveObstacles1(Pathfinding.RVO.RVONavmesh)
extern "C"  void RVONavmesh_ilo_RemoveObstacles1_m969748941 (Il2CppObject * __this /* static, unused */, RVONavmesh_t545242093 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.NavGraph[] Pathfinding.RVO.RVONavmesh::ilo_get_graphs2(AstarPath)
extern "C"  NavGraphU5BU5D_t850130684* RVONavmesh_ilo_get_graphs2_m261566344 (Il2CppObject * __this /* static, unused */, AstarPath_t4090270936 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RVO.RVONavmesh::ilo_GetNodes3(Pathfinding.INavmesh,Pathfinding.GraphNodeDelegateCancelable)
extern "C"  void RVONavmesh_ilo_GetNodes3_m2203782760 (Il2CppObject * __this /* static, unused */, Il2CppObject * ____this0, GraphNodeDelegateCancelable_t3591372971 * ___del1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
