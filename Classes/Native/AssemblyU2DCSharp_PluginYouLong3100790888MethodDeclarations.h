﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PluginYouLong
struct PluginYouLong_t3100790888;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// System.String
struct String_t;
// CEvent.EventFunc`1<CEvent.ZEvent>
struct EventFunc_1_t1114914310;
// VersionMgr
struct VersionMgr_t1322950208;
// VersionInfo
struct VersionInfo_t2356638086;
// PluginsSdkMgr
struct PluginsSdkMgr_t3884624670;
// Mihua.SDK.CreatRoleInfo
struct CreatRoleInfo_t2858846543;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// Mihua.SDK.LvUpInfo
struct LvUpInfo_t411687177;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_VersionMgr1322950208.h"
#include "AssemblyU2DCSharp_PluginYouLong3100790888.h"
#include "AssemblyU2DCSharp_PluginsSdkMgr3884624670.h"

// System.Void PluginYouLong::.ctor()
extern "C"  void PluginYouLong__ctor_m727188995 (PluginYouLong_t3100790888 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYouLong::Init()
extern "C"  void PluginYouLong_Init_m2745940977 (PluginYouLong_t3100790888 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginYouLong::ReqSDKHttpLogin()
extern "C"  Dictionary_2_t827649927 * PluginYouLong_ReqSDKHttpLogin_m4169463860 (PluginYouLong_t3100790888 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginYouLong::IsLoginSuccess(System.Boolean)
extern "C"  bool PluginYouLong_IsLoginSuccess_m451353352 (PluginYouLong_t3100790888 * __this, bool ___isOpenUseLogin0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYouLong::OpenUserLogin()
extern "C"  void PluginYouLong_OpenUserLogin_m3833099605 (PluginYouLong_t3100790888 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYouLong::UserPay(CEvent.ZEvent)
extern "C"  void PluginYouLong_UserPay_m3778110589 (PluginYouLong_t3100790888 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYouLong::CreateRole(CEvent.ZEvent)
extern "C"  void PluginYouLong_CreateRole_m1914734952 (PluginYouLong_t3100790888 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYouLong::EnterGame(CEvent.ZEvent)
extern "C"  void PluginYouLong_EnterGame_m3733457616 (PluginYouLong_t3100790888 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYouLong::RoleUpgrade(CEvent.ZEvent)
extern "C"  void PluginYouLong_RoleUpgrade_m1890539892 (PluginYouLong_t3100790888 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYouLong::OnLoginSuccess(System.String)
extern "C"  void PluginYouLong_OnLoginSuccess_m1310869704 (PluginYouLong_t3100790888 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYouLong::OnLogoutSuccess(System.String)
extern "C"  void PluginYouLong_OnLogoutSuccess_m1985797831 (PluginYouLong_t3100790888 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYouLong::OnLogout(System.String)
extern "C"  void PluginYouLong_OnLogout_m2896200280 (PluginYouLong_t3100790888 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYouLong::initSdk(System.String,System.String,System.String)
extern "C"  void PluginYouLong_initSdk_m2513547791 (PluginYouLong_t3100790888 * __this, String_t* ___gameId0, String_t* ___gameKey1, String_t* ___channel2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYouLong::login()
extern "C"  void PluginYouLong_login_m249203818 (PluginYouLong_t3100790888 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYouLong::logout()
extern "C"  void PluginYouLong_logout_m3436173771 (PluginYouLong_t3100790888 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYouLong::updateRoleInfo(System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginYouLong_updateRoleInfo_m3024400132 (PluginYouLong_t3100790888 * __this, String_t* ___serverId0, String_t* ___serverName1, String_t* ___roleName2, String_t* ___roleId3, String_t* ___roleLv4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYouLong::pay(System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginYouLong_pay_m3498575761 (PluginYouLong_t3100790888 * __this, String_t* ___orderId0, String_t* ___serverId1, String_t* ___serverName2, String_t* ___roleId3, String_t* ___roleName4, String_t* ___roleLv5, String_t* ___productName6, String_t* ___productId7, String_t* ___gameName8, String_t* ___amount9, String_t* ___extra10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYouLong::<OnLogoutSuccess>m__476()
extern "C"  void PluginYouLong_U3COnLogoutSuccessU3Em__476_m750941191 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYouLong::<OnLogout>m__477()
extern "C"  void PluginYouLong_U3COnLogoutU3Em__477_m1038276129 (PluginYouLong_t3100790888 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYouLong::ilo_AddEventListener1(System.String,CEvent.EventFunc`1<CEvent.ZEvent>)
extern "C"  void PluginYouLong_ilo_AddEventListener1_m1177958033 (Il2CppObject * __this /* static, unused */, String_t* ___eventType0, EventFunc_1_t1114914310 * ___fun1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionMgr PluginYouLong::ilo_get_Instance2()
extern "C"  VersionMgr_t1322950208 * PluginYouLong_ilo_get_Instance2_m1156229663 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionInfo PluginYouLong::ilo_get_currentVS3(VersionMgr)
extern "C"  VersionInfo_t2356638086 * PluginYouLong_ilo_get_currentVS3_m3939801549 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYouLong::ilo_initSdk4(PluginYouLong,System.String,System.String,System.String)
extern "C"  void PluginYouLong_ilo_initSdk4_m1642587808 (Il2CppObject * __this /* static, unused */, PluginYouLong_t3100790888 * ____this0, String_t* ___gameId1, String_t* ___gameKey2, String_t* ___channel3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PluginsSdkMgr PluginYouLong::ilo_get_PluginsSdkMgr5()
extern "C"  PluginsSdkMgr_t3884624670 * PluginYouLong_ilo_get_PluginsSdkMgr5_m4089559871 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PluginYouLong::ilo_get_DeviceID6(PluginsSdkMgr)
extern "C"  String_t* PluginYouLong_ilo_get_DeviceID6_m261800391 (Il2CppObject * __this /* static, unused */, PluginsSdkMgr_t3884624670 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYouLong::ilo_login7(PluginYouLong)
extern "C"  void PluginYouLong_ilo_login7_m3505933608 (Il2CppObject * __this /* static, unused */, PluginYouLong_t3100790888 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mihua.SDK.CreatRoleInfo PluginYouLong::ilo_Parse8(System.Object[])
extern "C"  CreatRoleInfo_t2858846543 * PluginYouLong_ilo_Parse8_m93022473 (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t1108656482* ___crtRoleOjs0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mihua.SDK.LvUpInfo PluginYouLong::ilo_Parse9(System.Object[])
extern "C"  LvUpInfo_t411687177 * PluginYouLong_ilo_Parse9_m1331938840 (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t1108656482* ___crtRoleOjs0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYouLong::ilo_updateRoleInfo10(PluginYouLong,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginYouLong_ilo_updateRoleInfo10_m3524449718 (Il2CppObject * __this /* static, unused */, PluginYouLong_t3100790888 * ____this0, String_t* ___serverId1, String_t* ___serverName2, String_t* ___roleName3, String_t* ___roleId4, String_t* ___roleLv5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYouLong::ilo_ReqSDKHttpLogin11(PluginsSdkMgr)
extern "C"  void PluginYouLong_ilo_ReqSDKHttpLogin11_m2492959521 (Il2CppObject * __this /* static, unused */, PluginsSdkMgr_t3884624670 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYouLong::ilo_logout12(PluginYouLong)
extern "C"  void PluginYouLong_ilo_logout12_m3322618123 (Il2CppObject * __this /* static, unused */, PluginYouLong_t3100790888 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
