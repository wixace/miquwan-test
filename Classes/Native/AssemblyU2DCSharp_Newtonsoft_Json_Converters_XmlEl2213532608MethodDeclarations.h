﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Converters.XmlElementWrapper
struct XmlElementWrapper_t2213532608;
// System.Xml.XmlElement
struct XmlElement_t280387747;
// Newtonsoft.Json.Converters.IXmlNode
struct IXmlNode_t2349752174;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// Newtonsoft.Json.Converters.XmlNodeWrapper
struct XmlNodeWrapper_t2503940216;

#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_XmlElement280387747.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Converters_XmlNo2503940216.h"

// System.Void Newtonsoft.Json.Converters.XmlElementWrapper::.ctor(System.Xml.XmlElement)
extern "C"  void XmlElementWrapper__ctor_m3912127236 (XmlElementWrapper_t2213532608 * __this, XmlElement_t280387747 * ___element0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Converters.XmlElementWrapper::SetAttributeNode(Newtonsoft.Json.Converters.IXmlNode)
extern "C"  void XmlElementWrapper_SetAttributeNode_m595180559 (XmlElementWrapper_t2213532608 * __this, Il2CppObject * ___attribute0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Converters.XmlElementWrapper::GetPrefixOfNamespace(System.String)
extern "C"  String_t* XmlElementWrapper_GetPrefixOfNamespace_m1027796466 (XmlElementWrapper_t2213532608 * __this, String_t* ___namespaceURI0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Converters.XmlElementWrapper::ilo_get_WrappedNode1(Newtonsoft.Json.Converters.XmlNodeWrapper)
extern "C"  Il2CppObject * XmlElementWrapper_ilo_get_WrappedNode1_m3777683590 (Il2CppObject * __this /* static, unused */, XmlNodeWrapper_t2503940216 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
