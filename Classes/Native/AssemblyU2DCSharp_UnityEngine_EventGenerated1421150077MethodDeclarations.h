﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_EventGenerated
struct UnityEngine_EventGenerated_t1421150077;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void UnityEngine_EventGenerated::.ctor()
extern "C"  void UnityEngine_EventGenerated__ctor_m1484225982 (UnityEngine_EventGenerated_t1421150077 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_EventGenerated::Event_Event1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_EventGenerated_Event_Event1_m2841958214 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_EventGenerated::Event_Event2(JSVCall,System.Int32)
extern "C"  bool UnityEngine_EventGenerated_Event_Event2_m4086722695 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_EventGenerated::Event_Event3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_EventGenerated_Event_Event3_m1036519880 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventGenerated::Event_mousePosition(JSVCall)
extern "C"  void UnityEngine_EventGenerated_Event_mousePosition_m1838685346 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventGenerated::Event_delta(JSVCall)
extern "C"  void UnityEngine_EventGenerated_Event_delta_m4219081496 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventGenerated::Event_shift(JSVCall)
extern "C"  void UnityEngine_EventGenerated_Event_shift_m3906905486 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventGenerated::Event_control(JSVCall)
extern "C"  void UnityEngine_EventGenerated_Event_control_m490997907 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventGenerated::Event_alt(JSVCall)
extern "C"  void UnityEngine_EventGenerated_Event_alt_m815627975 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventGenerated::Event_command(JSVCall)
extern "C"  void UnityEngine_EventGenerated_Event_command_m4225030757 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventGenerated::Event_capsLock(JSVCall)
extern "C"  void UnityEngine_EventGenerated_Event_capsLock_m2061739216 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventGenerated::Event_numeric(JSVCall)
extern "C"  void UnityEngine_EventGenerated_Event_numeric_m4197497059 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventGenerated::Event_functionKey(JSVCall)
extern "C"  void UnityEngine_EventGenerated_Event_functionKey_m2786304201 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventGenerated::Event_current(JSVCall)
extern "C"  void UnityEngine_EventGenerated_Event_current_m2352680951 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventGenerated::Event_isKey(JSVCall)
extern "C"  void UnityEngine_EventGenerated_Event_isKey_m2676960923 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventGenerated::Event_isMouse(JSVCall)
extern "C"  void UnityEngine_EventGenerated_Event_isMouse_m3298585109 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventGenerated::Event_rawType(JSVCall)
extern "C"  void UnityEngine_EventGenerated_Event_rawType_m2818154510 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventGenerated::Event_type(JSVCall)
extern "C"  void UnityEngine_EventGenerated_Event_type_m80395266 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventGenerated::Event_button(JSVCall)
extern "C"  void UnityEngine_EventGenerated_Event_button_m1789482378 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventGenerated::Event_modifiers(JSVCall)
extern "C"  void UnityEngine_EventGenerated_Event_modifiers_m399400404 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventGenerated::Event_pressure(JSVCall)
extern "C"  void UnityEngine_EventGenerated_Event_pressure_m898877015 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventGenerated::Event_clickCount(JSVCall)
extern "C"  void UnityEngine_EventGenerated_Event_clickCount_m1031326965 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventGenerated::Event_character(JSVCall)
extern "C"  void UnityEngine_EventGenerated_Event_character_m2828185031 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventGenerated::Event_commandName(JSVCall)
extern "C"  void UnityEngine_EventGenerated_Event_commandName_m947565722 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventGenerated::Event_keyCode(JSVCall)
extern "C"  void UnityEngine_EventGenerated_Event_keyCode_m2311685604 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventGenerated::Event_displayIndex(JSVCall)
extern "C"  void UnityEngine_EventGenerated_Event_displayIndex_m3830776012 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_EventGenerated::Event_Equals__Object(JSVCall,System.Int32)
extern "C"  bool UnityEngine_EventGenerated_Event_Equals__Object_m1008801069 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_EventGenerated::Event_GetHashCode(JSVCall,System.Int32)
extern "C"  bool UnityEngine_EventGenerated_Event_GetHashCode_m910400004 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_EventGenerated::Event_GetTypeForControl__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_EventGenerated_Event_GetTypeForControl__Int32_m794846361 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_EventGenerated::Event_ToString(JSVCall,System.Int32)
extern "C"  bool UnityEngine_EventGenerated_Event_ToString_m1219431323 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_EventGenerated::Event_Use(JSVCall,System.Int32)
extern "C"  bool UnityEngine_EventGenerated_Event_Use_m1855306618 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_EventGenerated::Event_GetEventCount(JSVCall,System.Int32)
extern "C"  bool UnityEngine_EventGenerated_Event_GetEventCount_m1188073214 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_EventGenerated::Event_KeyboardEvent__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_EventGenerated_Event_KeyboardEvent__String_m4224776631 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_EventGenerated::Event_PopEvent__Event(JSVCall,System.Int32)
extern "C"  bool UnityEngine_EventGenerated_Event_PopEvent__Event_m1845148452 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventGenerated::__Register()
extern "C"  void UnityEngine_EventGenerated___Register_m1367926217 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_EventGenerated::ilo_attachFinalizerObject1(System.Int32)
extern "C"  bool UnityEngine_EventGenerated_ilo_attachFinalizerObject1_m387012833 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_EventGenerated::ilo_getObject2(System.Int32)
extern "C"  int32_t UnityEngine_EventGenerated_ilo_getObject2_m106927317 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_EventGenerated::ilo_getBooleanS3(System.Int32)
extern "C"  bool UnityEngine_EventGenerated_ilo_getBooleanS3_m425109712 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventGenerated::ilo_setBooleanS4(System.Int32,System.Boolean)
extern "C"  void UnityEngine_EventGenerated_ilo_setBooleanS4_m1090430092 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_EventGenerated::ilo_getEnum5(System.Int32)
extern "C"  int32_t UnityEngine_EventGenerated_ilo_getEnum5_m2841723286 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventGenerated::ilo_setInt326(System.Int32,System.Int32)
extern "C"  void UnityEngine_EventGenerated_ilo_setInt326_m2726587059 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_EventGenerated::ilo_getWhatever7(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_EventGenerated_ilo_getWhatever7_m1694844980 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_EventGenerated::ilo_getInt328(System.Int32)
extern "C"  int32_t UnityEngine_EventGenerated_ilo_getInt328_m3150194764 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventGenerated::ilo_setEnum9(System.Int32,System.Int32)
extern "C"  void UnityEngine_EventGenerated_ilo_setEnum9_m3394195031 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventGenerated::ilo_setStringS10(System.Int32,System.String)
extern "C"  void UnityEngine_EventGenerated_ilo_setStringS10_m3485249765 (Il2CppObject * __this /* static, unused */, int32_t ___e0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_EventGenerated::ilo_setObject11(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UnityEngine_EventGenerated_ilo_setObject11_m192792273 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
