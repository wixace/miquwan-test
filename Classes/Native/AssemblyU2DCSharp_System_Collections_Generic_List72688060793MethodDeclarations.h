﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System_Collections_Generic_List77Generated/<ListA1_FindAll__PredicateT1_T>c__AnonStorey82
struct U3CListA1_FindAll__PredicateT1_TU3Ec__AnonStorey82_t2688060793;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void System_Collections_Generic_List77Generated/<ListA1_FindAll__PredicateT1_T>c__AnonStorey82::.ctor()
extern "C"  void U3CListA1_FindAll__PredicateT1_TU3Ec__AnonStorey82__ctor_m1860538386 (U3CListA1_FindAll__PredicateT1_TU3Ec__AnonStorey82_t2688060793 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System_Collections_Generic_List77Generated/<ListA1_FindAll__PredicateT1_T>c__AnonStorey82::<>m__A7()
extern "C"  Il2CppObject * U3CListA1_FindAll__PredicateT1_TU3Ec__AnonStorey82_U3CU3Em__A7_m2370590502 (U3CListA1_FindAll__PredicateT1_TU3Ec__AnonStorey82_t2688060793 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
