﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_CameraGenerated/<Camera_onPostRender_GetDelegate_member2_arg0>c__AnonStoreyE0
struct U3CCamera_onPostRender_GetDelegate_member2_arg0U3Ec__AnonStoreyE0_t1474577720;
// UnityEngine.Camera
struct Camera_t2727095145;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Camera2727095145.h"

// System.Void UnityEngine_CameraGenerated/<Camera_onPostRender_GetDelegate_member2_arg0>c__AnonStoreyE0::.ctor()
extern "C"  void U3CCamera_onPostRender_GetDelegate_member2_arg0U3Ec__AnonStoreyE0__ctor_m3930267443 (U3CCamera_onPostRender_GetDelegate_member2_arg0U3Ec__AnonStoreyE0_t1474577720 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CameraGenerated/<Camera_onPostRender_GetDelegate_member2_arg0>c__AnonStoreyE0::<>m__194(UnityEngine.Camera)
extern "C"  void U3CCamera_onPostRender_GetDelegate_member2_arg0U3Ec__AnonStoreyE0_U3CU3Em__194_m2147827798 (U3CCamera_onPostRender_GetDelegate_member2_arg0U3Ec__AnonStoreyE0_t1474577720 * __this, Camera_t2727095145 * ___cam0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
