﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PositionFollower
struct PositionFollower_t58402951;

#include "codegen/il2cpp-codegen.h"

// System.Void PositionFollower::.ctor()
extern "C"  void PositionFollower__ctor_m3953239540 (PositionFollower_t58402951 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PositionFollower::Update()
extern "C"  void PositionFollower_Update_m4018203545 (PositionFollower_t58402951 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
