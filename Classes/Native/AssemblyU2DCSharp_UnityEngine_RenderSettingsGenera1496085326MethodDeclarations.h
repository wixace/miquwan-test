﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_RenderSettingsGenerated
struct UnityEngine_RenderSettingsGenerated_t1496085326;
// JSVCall
struct JSVCall_t3708497963;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void UnityEngine_RenderSettingsGenerated::.ctor()
extern "C"  void UnityEngine_RenderSettingsGenerated__ctor_m742434781 (UnityEngine_RenderSettingsGenerated_t1496085326 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RenderSettingsGenerated::RenderSettings_RenderSettings1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RenderSettingsGenerated_RenderSettings_RenderSettings1_m2911516821 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RenderSettingsGenerated::RenderSettings_fog(JSVCall)
extern "C"  void UnityEngine_RenderSettingsGenerated_RenderSettings_fog_m724116520 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RenderSettingsGenerated::RenderSettings_fogMode(JSVCall)
extern "C"  void UnityEngine_RenderSettingsGenerated_RenderSettings_fogMode_m3999801541 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RenderSettingsGenerated::RenderSettings_fogColor(JSVCall)
extern "C"  void UnityEngine_RenderSettingsGenerated_RenderSettings_fogColor_m3753199681 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RenderSettingsGenerated::RenderSettings_fogDensity(JSVCall)
extern "C"  void UnityEngine_RenderSettingsGenerated_RenderSettings_fogDensity_m3753480412 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RenderSettingsGenerated::RenderSettings_fogStartDistance(JSVCall)
extern "C"  void UnityEngine_RenderSettingsGenerated_RenderSettings_fogStartDistance_m1491590573 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RenderSettingsGenerated::RenderSettings_fogEndDistance(JSVCall)
extern "C"  void UnityEngine_RenderSettingsGenerated_RenderSettings_fogEndDistance_m3204288980 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RenderSettingsGenerated::RenderSettings_ambientMode(JSVCall)
extern "C"  void UnityEngine_RenderSettingsGenerated_RenderSettings_ambientMode_m2475212075 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RenderSettingsGenerated::RenderSettings_ambientSkyColor(JSVCall)
extern "C"  void UnityEngine_RenderSettingsGenerated_RenderSettings_ambientSkyColor_m2627503852 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RenderSettingsGenerated::RenderSettings_ambientEquatorColor(JSVCall)
extern "C"  void UnityEngine_RenderSettingsGenerated_RenderSettings_ambientEquatorColor_m2680330090 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RenderSettingsGenerated::RenderSettings_ambientGroundColor(JSVCall)
extern "C"  void UnityEngine_RenderSettingsGenerated_RenderSettings_ambientGroundColor_m3764876290 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RenderSettingsGenerated::RenderSettings_ambientLight(JSVCall)
extern "C"  void UnityEngine_RenderSettingsGenerated_RenderSettings_ambientLight_m3896241224 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RenderSettingsGenerated::RenderSettings_ambientIntensity(JSVCall)
extern "C"  void UnityEngine_RenderSettingsGenerated_RenderSettings_ambientIntensity_m3576294859 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RenderSettingsGenerated::RenderSettings_ambientProbe(JSVCall)
extern "C"  void UnityEngine_RenderSettingsGenerated_RenderSettings_ambientProbe_m1110550254 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RenderSettingsGenerated::RenderSettings_reflectionIntensity(JSVCall)
extern "C"  void UnityEngine_RenderSettingsGenerated_RenderSettings_reflectionIntensity_m1027899358 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RenderSettingsGenerated::RenderSettings_reflectionBounces(JSVCall)
extern "C"  void UnityEngine_RenderSettingsGenerated_RenderSettings_reflectionBounces_m738630374 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RenderSettingsGenerated::RenderSettings_haloStrength(JSVCall)
extern "C"  void UnityEngine_RenderSettingsGenerated_RenderSettings_haloStrength_m824794089 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RenderSettingsGenerated::RenderSettings_flareStrength(JSVCall)
extern "C"  void UnityEngine_RenderSettingsGenerated_RenderSettings_flareStrength_m203243991 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RenderSettingsGenerated::RenderSettings_flareFadeSpeed(JSVCall)
extern "C"  void UnityEngine_RenderSettingsGenerated_RenderSettings_flareFadeSpeed_m4042254729 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RenderSettingsGenerated::RenderSettings_skybox(JSVCall)
extern "C"  void UnityEngine_RenderSettingsGenerated_RenderSettings_skybox_m789095964 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RenderSettingsGenerated::RenderSettings_defaultReflectionMode(JSVCall)
extern "C"  void UnityEngine_RenderSettingsGenerated_RenderSettings_defaultReflectionMode_m557591287 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RenderSettingsGenerated::RenderSettings_defaultReflectionResolution(JSVCall)
extern "C"  void UnityEngine_RenderSettingsGenerated_RenderSettings_defaultReflectionResolution_m693382926 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RenderSettingsGenerated::RenderSettings_customReflection(JSVCall)
extern "C"  void UnityEngine_RenderSettingsGenerated_RenderSettings_customReflection_m2026558698 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RenderSettingsGenerated::__Register()
extern "C"  void UnityEngine_RenderSettingsGenerated___Register_m1406154506 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_RenderSettingsGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UnityEngine_RenderSettingsGenerated_ilo_getObject1_m2682767737 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine_RenderSettingsGenerated::ilo_getSingle2(System.Int32)
extern "C"  float UnityEngine_RenderSettingsGenerated_ilo_getSingle2_m1301302419 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_RenderSettingsGenerated::ilo_getEnum3(System.Int32)
extern "C"  int32_t UnityEngine_RenderSettingsGenerated_ilo_getEnum3_m3468853561 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_RenderSettingsGenerated::ilo_setObject4(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UnityEngine_RenderSettingsGenerated_ilo_setObject4_m2901914548 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RenderSettingsGenerated::ilo_setSingle5(System.Int32,System.Single)
extern "C"  void UnityEngine_RenderSettingsGenerated_ilo_setSingle5_m3925888379 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_RenderSettingsGenerated::ilo_getInt326(System.Int32)
extern "C"  int32_t UnityEngine_RenderSettingsGenerated_ilo_getInt326_m1375192133 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
