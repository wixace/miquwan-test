﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.AdvancedSmooth/ConstantTurn
struct ConstantTurn_t1658810934;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t215400611;
// System.Collections.Generic.List`1<Pathfinding.AdvancedSmooth/Turn>
struct List_1_t1833380930;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t1355284822;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_AdvancedSmooth_Turn465195378.h"

// System.Void Pathfinding.AdvancedSmooth/ConstantTurn::.ctor()
extern "C"  void ConstantTurn__ctor_m3446815221 (ConstantTurn_t1658810934 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.AdvancedSmooth/ConstantTurn::Prepare(System.Int32,UnityEngine.Vector3[])
extern "C"  void ConstantTurn_Prepare_m2019809756 (ConstantTurn_t1658810934 * __this, int32_t ___i0, Vector3U5BU5D_t215400611* ___vectorPath1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.AdvancedSmooth/ConstantTurn::TangentToTangent(System.Collections.Generic.List`1<Pathfinding.AdvancedSmooth/Turn>)
extern "C"  void ConstantTurn_TangentToTangent_m2598735914 (ConstantTurn_t1658810934 * __this, List_1_t1833380930 * ___turnList0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.AdvancedSmooth/ConstantTurn::GetPath(Pathfinding.AdvancedSmooth/Turn,System.Collections.Generic.List`1<UnityEngine.Vector3>)
extern "C"  void ConstantTurn_GetPath_m1862906993 (ConstantTurn_t1658810934 * __this, Turn_t465195378  ___turn0, List_1_t1355284822 * ___output1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
