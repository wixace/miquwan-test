﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Scripts.JSBindingClasses.JsRepresentClass/<GeneratedJSFuntion3>c__AnonStoreyFF
struct U3CGeneratedJSFuntion3U3Ec__AnonStoreyFF_t2781789981;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void Scripts.JSBindingClasses.JsRepresentClass/<GeneratedJSFuntion3>c__AnonStoreyFF::.ctor()
extern "C"  void U3CGeneratedJSFuntion3U3Ec__AnonStoreyFF__ctor_m764608030 (U3CGeneratedJSFuntion3U3Ec__AnonStoreyFF_t2781789981 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Scripts.JSBindingClasses.JsRepresentClass/<GeneratedJSFuntion3>c__AnonStoreyFF::<>m__323(System.Object,System.Object,System.Object)
extern "C"  Il2CppObject * U3CGeneratedJSFuntion3U3Ec__AnonStoreyFF_U3CU3Em__323_m2667127468 (U3CGeneratedJSFuntion3U3Ec__AnonStoreyFF_t2781789981 * __this, Il2CppObject * ___object10, Il2CppObject * ___object21, Il2CppObject * ___object32, const MethodInfo* method) IL2CPP_METHOD_ATTR;
