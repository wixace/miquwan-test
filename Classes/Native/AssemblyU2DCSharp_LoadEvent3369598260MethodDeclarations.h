﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LoadEvent
struct LoadEvent_t3369598260;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void LoadEvent::.ctor(System.String,System.Int32)
extern "C"  void LoadEvent__ctor_m105553164 (LoadEvent_t3369598260 * __this, String_t* ___type0, int32_t ___rate1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
