﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.CharacterController
struct CharacterController_t1618060635;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_CharacterController1618060635.h"
#include "UnityEngine_UnityEngine_CollisionFlags490137529.h"

// System.Void UnityEngine.CharacterController::.ctor()
extern "C"  void CharacterController__ctor_m3262463828 (CharacterController_t1618060635 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.CharacterController::SimpleMove(UnityEngine.Vector3)
extern "C"  bool CharacterController_SimpleMove_m3593592780 (CharacterController_t1618060635 * __this, Vector3_t4282066566  ___speed0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.CharacterController::INTERNAL_CALL_SimpleMove(UnityEngine.CharacterController,UnityEngine.Vector3&)
extern "C"  bool CharacterController_INTERNAL_CALL_SimpleMove_m34016609 (Il2CppObject * __this /* static, unused */, CharacterController_t1618060635 * ___self0, Vector3_t4282066566 * ___speed1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.CollisionFlags UnityEngine.CharacterController::Move(UnityEngine.Vector3)
extern "C"  int32_t CharacterController_Move_m3043020731 (CharacterController_t1618060635 * __this, Vector3_t4282066566  ___motion0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.CollisionFlags UnityEngine.CharacterController::INTERNAL_CALL_Move(UnityEngine.CharacterController,UnityEngine.Vector3&)
extern "C"  int32_t CharacterController_INTERNAL_CALL_Move_m985801042 (Il2CppObject * __this /* static, unused */, CharacterController_t1618060635 * ___self0, Vector3_t4282066566 * ___motion1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.CharacterController::get_isGrounded()
extern "C"  bool CharacterController_get_isGrounded_m1739295843 (CharacterController_t1618060635 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.CharacterController::get_velocity()
extern "C"  Vector3_t4282066566  CharacterController_get_velocity_m1414499516 (CharacterController_t1618060635 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.CharacterController::INTERNAL_get_velocity(UnityEngine.Vector3&)
extern "C"  void CharacterController_INTERNAL_get_velocity_m257673357 (CharacterController_t1618060635 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.CollisionFlags UnityEngine.CharacterController::get_collisionFlags()
extern "C"  int32_t CharacterController_get_collisionFlags_m3620825515 (CharacterController_t1618060635 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.CharacterController::get_radius()
extern "C"  float CharacterController_get_radius_m2930031455 (CharacterController_t1618060635 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.CharacterController::set_radius(System.Single)
extern "C"  void CharacterController_set_radius_m1330958508 (CharacterController_t1618060635 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.CharacterController::get_height()
extern "C"  float CharacterController_get_height_m2077757108 (CharacterController_t1618060635 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.CharacterController::set_height(System.Single)
extern "C"  void CharacterController_set_height_m3020181367 (CharacterController_t1618060635 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.CharacterController::get_center()
extern "C"  Vector3_t4282066566  CharacterController_get_center_m4000255412 (CharacterController_t1618060635 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.CharacterController::set_center(UnityEngine.Vector3)
extern "C"  void CharacterController_set_center_m3276939831 (CharacterController_t1618060635 * __this, Vector3_t4282066566  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.CharacterController::INTERNAL_get_center(UnityEngine.Vector3&)
extern "C"  void CharacterController_INTERNAL_get_center_m3546264837 (CharacterController_t1618060635 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.CharacterController::INTERNAL_set_center(UnityEngine.Vector3&)
extern "C"  void CharacterController_INTERNAL_set_center_m899855225 (CharacterController_t1618060635 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.CharacterController::get_slopeLimit()
extern "C"  float CharacterController_get_slopeLimit_m3981117981 (CharacterController_t1618060635 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.CharacterController::set_slopeLimit(System.Single)
extern "C"  void CharacterController_set_slopeLimit_m1749870254 (CharacterController_t1618060635 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.CharacterController::get_stepOffset()
extern "C"  float CharacterController_get_stepOffset_m1993248780 (CharacterController_t1618060635 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.CharacterController::set_stepOffset(System.Single)
extern "C"  void CharacterController_set_stepOffset_m3279469855 (CharacterController_t1618060635 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.CharacterController::get_skinWidth()
extern "C"  float CharacterController_get_skinWidth_m1072528190 (CharacterController_t1618060635 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.CharacterController::set_skinWidth(System.Single)
extern "C"  void CharacterController_set_skinWidth_m1708942125 (CharacterController_t1618060635 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.CharacterController::get_detectCollisions()
extern "C"  bool CharacterController_get_detectCollisions_m967296183 (CharacterController_t1618060635 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.CharacterController::set_detectCollisions(System.Boolean)
extern "C"  void CharacterController_set_detectCollisions_m3520854408 (CharacterController_t1618060635 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
