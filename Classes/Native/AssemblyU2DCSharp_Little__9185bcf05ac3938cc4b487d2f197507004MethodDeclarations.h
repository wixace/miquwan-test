﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._9185bcf05ac3938cc4b487d2f1b3a711
struct _9185bcf05ac3938cc4b487d2f1b3a711_t197507004;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__9185bcf05ac3938cc4b487d2f197507004.h"

// System.Void Little._9185bcf05ac3938cc4b487d2f1b3a711::.ctor()
extern "C"  void _9185bcf05ac3938cc4b487d2f1b3a711__ctor_m57303537 (_9185bcf05ac3938cc4b487d2f1b3a711_t197507004 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._9185bcf05ac3938cc4b487d2f1b3a711::_9185bcf05ac3938cc4b487d2f1b3a711m2(System.Int32)
extern "C"  int32_t _9185bcf05ac3938cc4b487d2f1b3a711__9185bcf05ac3938cc4b487d2f1b3a711m2_m2520520729 (_9185bcf05ac3938cc4b487d2f1b3a711_t197507004 * __this, int32_t ____9185bcf05ac3938cc4b487d2f1b3a711a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._9185bcf05ac3938cc4b487d2f1b3a711::_9185bcf05ac3938cc4b487d2f1b3a711m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _9185bcf05ac3938cc4b487d2f1b3a711__9185bcf05ac3938cc4b487d2f1b3a711m_m2001002173 (_9185bcf05ac3938cc4b487d2f1b3a711_t197507004 * __this, int32_t ____9185bcf05ac3938cc4b487d2f1b3a711a0, int32_t ____9185bcf05ac3938cc4b487d2f1b3a711131, int32_t ____9185bcf05ac3938cc4b487d2f1b3a711c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._9185bcf05ac3938cc4b487d2f1b3a711::ilo__9185bcf05ac3938cc4b487d2f1b3a711m21(Little._9185bcf05ac3938cc4b487d2f1b3a711,System.Int32)
extern "C"  int32_t _9185bcf05ac3938cc4b487d2f1b3a711_ilo__9185bcf05ac3938cc4b487d2f1b3a711m21_m216697091 (Il2CppObject * __this /* static, unused */, _9185bcf05ac3938cc4b487d2f1b3a711_t197507004 * ____this0, int32_t ____9185bcf05ac3938cc4b487d2f1b3a711a1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
