﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_sasal177
struct M_sasal177_t450230097;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_sasal177450230097.h"

// System.Void GarbageiOS.M_sasal177::.ctor()
extern "C"  void M_sasal177__ctor_m1159502338 (M_sasal177_t450230097 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_sasal177::M_yallmofal0(System.String[],System.Int32)
extern "C"  void M_sasal177_M_yallmofal0_m1341654132 (M_sasal177_t450230097 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_sasal177::M_pirtra1(System.String[],System.Int32)
extern "C"  void M_sasal177_M_pirtra1_m1668827724 (M_sasal177_t450230097 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_sasal177::M_jitou2(System.String[],System.Int32)
extern "C"  void M_sasal177_M_jitou2_m4176894178 (M_sasal177_t450230097 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_sasal177::M_zouveNoudar3(System.String[],System.Int32)
extern "C"  void M_sasal177_M_zouveNoudar3_m3656234062 (M_sasal177_t450230097 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_sasal177::M_beaselJotrow4(System.String[],System.Int32)
extern "C"  void M_sasal177_M_beaselJotrow4_m3152379186 (M_sasal177_t450230097 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_sasal177::M_whorow5(System.String[],System.Int32)
extern "C"  void M_sasal177_M_whorow5_m1481080094 (M_sasal177_t450230097 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_sasal177::M_seecorteCoulas6(System.String[],System.Int32)
extern "C"  void M_sasal177_M_seecorteCoulas6_m1289176962 (M_sasal177_t450230097 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_sasal177::ilo_M_yallmofal01(GarbageiOS.M_sasal177,System.String[],System.Int32)
extern "C"  void M_sasal177_ilo_M_yallmofal01_m3705988343 (Il2CppObject * __this /* static, unused */, M_sasal177_t450230097 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_sasal177::ilo_M_zouveNoudar32(GarbageiOS.M_sasal177,System.String[],System.Int32)
extern "C"  void M_sasal177_ilo_M_zouveNoudar32_m2411805214 (Il2CppObject * __this /* static, unused */, M_sasal177_t450230097 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
