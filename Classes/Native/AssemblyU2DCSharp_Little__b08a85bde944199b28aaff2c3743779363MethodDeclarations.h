﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._b08a85bde944199b28aaff2c748fdb5d
struct _b08a85bde944199b28aaff2c748fdb5d_t3743779363;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._b08a85bde944199b28aaff2c748fdb5d::.ctor()
extern "C"  void _b08a85bde944199b28aaff2c748fdb5d__ctor_m323179370 (_b08a85bde944199b28aaff2c748fdb5d_t3743779363 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._b08a85bde944199b28aaff2c748fdb5d::_b08a85bde944199b28aaff2c748fdb5dm2(System.Int32)
extern "C"  int32_t _b08a85bde944199b28aaff2c748fdb5d__b08a85bde944199b28aaff2c748fdb5dm2_m1327056569 (_b08a85bde944199b28aaff2c748fdb5d_t3743779363 * __this, int32_t ____b08a85bde944199b28aaff2c748fdb5da0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._b08a85bde944199b28aaff2c748fdb5d::_b08a85bde944199b28aaff2c748fdb5dm(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _b08a85bde944199b28aaff2c748fdb5d__b08a85bde944199b28aaff2c748fdb5dm_m3050005533 (_b08a85bde944199b28aaff2c748fdb5d_t3743779363 * __this, int32_t ____b08a85bde944199b28aaff2c748fdb5da0, int32_t ____b08a85bde944199b28aaff2c748fdb5d21, int32_t ____b08a85bde944199b28aaff2c748fdb5dc2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
