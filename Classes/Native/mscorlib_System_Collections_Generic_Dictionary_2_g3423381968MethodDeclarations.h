﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g4168079610MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Int32,JSCMapPathConfig>::.ctor()
#define Dictionary_2__ctor_m4221642398(__this, method) ((  void (*) (Dictionary_2_t3423381968 *, const MethodInfo*))Dictionary_2__ctor_m1050910858_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,JSCMapPathConfig>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m3663185388(__this, ___comparer0, method) ((  void (*) (Dictionary_2_t3423381968 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m3610002771_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,JSCMapPathConfig>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
#define Dictionary_2__ctor_m1905323203(__this, ___dictionary0, method) ((  void (*) (Dictionary_2_t3423381968 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m256662076_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,JSCMapPathConfig>::.ctor(System.Int32)
#define Dictionary_2__ctor_m572964294(__this, ___capacity0, method) ((  void (*) (Dictionary_2_t3423381968 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m3273912365_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,JSCMapPathConfig>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m1711802138(__this, ___dictionary0, ___comparer1, method) ((  void (*) (Dictionary_2_t3423381968 *, Il2CppObject*, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m1930793793_gshared)(__this, ___dictionary0, ___comparer1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,JSCMapPathConfig>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2__ctor_m3116701622(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t3423381968 *, SerializationInfo_t2185721892 *, StreamingContext_t2761351129 , const MethodInfo*))Dictionary_2__ctor_m1549788189_gshared)(__this, ___info0, ___context1, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<System.Int32,JSCMapPathConfig>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m3593521401(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t3423381968 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m1428264956_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<System.Int32,JSCMapPathConfig>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m628860281(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t3423381968 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m2955279704_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.Int32,JSCMapPathConfig>::System.Collections.IDictionary.get_Keys()
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m2580046203(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t3423381968 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m4198131226_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.Int32,JSCMapPathConfig>::System.Collections.IDictionary.get_Values()
#define Dictionary_2_System_Collections_IDictionary_get_Values_m3595826089(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t3423381968 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Values_m3797372040_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,JSCMapPathConfig>::System.Collections.IDictionary.get_IsFixedSize()
#define Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m2335388556(__this, method) ((  bool (*) (Dictionary_2_t3423381968 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m3627613121_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,JSCMapPathConfig>::System.Collections.IDictionary.get_IsReadOnly()
#define Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m1637722605(__this, method) ((  bool (*) (Dictionary_2_t3423381968 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m1263765272_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Int32,JSCMapPathConfig>::System.Collections.IDictionary.get_Item(System.Object)
#define Dictionary_2_System_Collections_IDictionary_get_Item_m669713587(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t3423381968 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m2843055522_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,JSCMapPathConfig>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_set_Item_m2383461720(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t3423381968 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m2020057553_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,JSCMapPathConfig>::System.Collections.IDictionary.Add(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_Add_m911903225(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t3423381968 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m2894265824_gshared)(__this, ___key0, ___value1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,JSCMapPathConfig>::System.Collections.IDictionary.Contains(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Contains_m4119673437(__this, ___key0, method) ((  bool (*) (Dictionary_2_t3423381968 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m2093434578_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,JSCMapPathConfig>::System.Collections.IDictionary.Remove(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Remove_m148432790(__this, ___key0, method) ((  void (*) (Dictionary_2_t3423381968 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m127000079_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,JSCMapPathConfig>::System.Collections.ICollection.get_IsSynchronized()
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m3192183319(__this, method) ((  bool (*) (Dictionary_2_t3423381968 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m4062325186_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Int32,JSCMapPathConfig>::System.Collections.ICollection.get_SyncRoot()
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m2260655491(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t3423381968 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m4065571764_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,JSCMapPathConfig>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m3101276059(__this, method) ((  bool (*) (Dictionary_2_t3423381968 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m840305542_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,JSCMapPathConfig>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1319325996(__this, ___keyValuePair0, method) ((  void (*) (Dictionary_2_t3423381968 *, KeyValuePair_2_t3322162674 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m2185230117_gshared)(__this, ___keyValuePair0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,JSCMapPathConfig>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m467334678(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t3423381968 *, KeyValuePair_2_t3322162674 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m3713378305_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,JSCMapPathConfig>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m456576528(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t3423381968 *, KeyValuePair_2U5BU5D_t1627025927*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m4220340169_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,JSCMapPathConfig>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m2763762363(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t3423381968 *, KeyValuePair_2_t3322162674 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m3330268006_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,JSCMapPathConfig>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Dictionary_2_System_Collections_ICollection_CopyTo_m3294940911(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t3423381968 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m323672040_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Int32,JSCMapPathConfig>::System.Collections.IEnumerable.GetEnumerator()
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m2957057322(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t3423381968 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m464503287_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Int32,JSCMapPathConfig>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m1806359527(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t3423381968 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m1289662318_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Int32,JSCMapPathConfig>::System.Collections.IDictionary.GetEnumerator()
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m3711170114(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t3423381968 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m3187662523_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Int32,JSCMapPathConfig>::get_Count()
#define Dictionary_2_get_Count_m1993690077(__this, method) ((  int32_t (*) (Dictionary_2_t3423381968 *, const MethodInfo*))Dictionary_2_get_Count_m655926012_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,JSCMapPathConfig>::get_Item(TKey)
#define Dictionary_2_get_Item_m3955808512(__this, ___key0, method) ((  JSCMapPathConfig_t3426118729 * (*) (Dictionary_2_t3423381968 *, int32_t, const MethodInfo*))Dictionary_2_get_Item_m2361773736_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,JSCMapPathConfig>::set_Item(TKey,TValue)
#define Dictionary_2_set_Item_m1768748277(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t3423381968 *, int32_t, JSCMapPathConfig_t3426118729 *, const MethodInfo*))Dictionary_2_set_Item_m1170572283_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,JSCMapPathConfig>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2_Init_m1722613613(__this, ___capacity0, ___hcp1, method) ((  void (*) (Dictionary_2_t3423381968 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2_Init_m3161627732_gshared)(__this, ___capacity0, ___hcp1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,JSCMapPathConfig>::InitArrays(System.Int32)
#define Dictionary_2_InitArrays_m1089278954(__this, ___size0, method) ((  void (*) (Dictionary_2_t3423381968 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m3089254883_gshared)(__this, ___size0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,JSCMapPathConfig>::CopyToCheck(System.Array,System.Int32)
#define Dictionary_2_CopyToCheck_m3575762534(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t3423381968 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m3741359263_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,JSCMapPathConfig>::make_pair(TKey,TValue)
#define Dictionary_2_make_pair_m265499314(__this /* static, unused */, ___key0, ___value1, method) ((  KeyValuePair_2_t3322162674  (*) (Il2CppObject * /* static, unused */, int32_t, JSCMapPathConfig_t3426118729 *, const MethodInfo*))Dictionary_2_make_pair_m2338171699_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TKey System.Collections.Generic.Dictionary`2<System.Int32,JSCMapPathConfig>::pick_key(TKey,TValue)
#define Dictionary_2_pick_key_m462920708(__this /* static, unused */, ___key0, ___value1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, int32_t, JSCMapPathConfig_t3426118729 *, const MethodInfo*))Dictionary_2_pick_key_m1394751787_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,JSCMapPathConfig>::pick_value(TKey,TValue)
#define Dictionary_2_pick_value_m579264836(__this /* static, unused */, ___key0, ___value1, method) ((  JSCMapPathConfig_t3426118729 * (*) (Il2CppObject * /* static, unused */, int32_t, JSCMapPathConfig_t3426118729 *, const MethodInfo*))Dictionary_2_pick_value_m1281047495_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,JSCMapPathConfig>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_CopyTo_m1599749801(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t3423381968 *, KeyValuePair_2U5BU5D_t1627025927*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m2503627344_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,JSCMapPathConfig>::Resize()
#define Dictionary_2_Resize_m1971951331(__this, method) ((  void (*) (Dictionary_2_t3423381968 *, const MethodInfo*))Dictionary_2_Resize_m1861476060_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,JSCMapPathConfig>::Add(TKey,TValue)
#define Dictionary_2_Add_m1750176968(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t3423381968 *, int32_t, JSCMapPathConfig_t3426118729 *, const MethodInfo*))Dictionary_2_Add_m2232043353_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,JSCMapPathConfig>::Clear()
#define Dictionary_2_Clear_m2039942176(__this, method) ((  void (*) (Dictionary_2_t3423381968 *, const MethodInfo*))Dictionary_2_Clear_m3560399111_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,JSCMapPathConfig>::ContainsKey(TKey)
#define Dictionary_2_ContainsKey_m2906184441(__this, ___key0, method) ((  bool (*) (Dictionary_2_t3423381968 *, int32_t, const MethodInfo*))Dictionary_2_ContainsKey_m2612169713_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,JSCMapPathConfig>::ContainsValue(TValue)
#define Dictionary_2_ContainsValue_m895877766(__this, ___value0, method) ((  bool (*) (Dictionary_2_t3423381968 *, JSCMapPathConfig_t3426118729 *, const MethodInfo*))Dictionary_2_ContainsValue_m454328177_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,JSCMapPathConfig>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2_GetObjectData_m866535699(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t3423381968 *, SerializationInfo_t2185721892 *, StreamingContext_t2761351129 , const MethodInfo*))Dictionary_2_GetObjectData_m3426598522_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,JSCMapPathConfig>::OnDeserialization(System.Object)
#define Dictionary_2_OnDeserialization_m431292209(__this, ___sender0, method) ((  void (*) (Dictionary_2_t3423381968 *, Il2CppObject *, const MethodInfo*))Dictionary_2_OnDeserialization_m3983879210_gshared)(__this, ___sender0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,JSCMapPathConfig>::Remove(TKey)
#define Dictionary_2_Remove_m114031530(__this, ___key0, method) ((  bool (*) (Dictionary_2_t3423381968 *, int32_t, const MethodInfo*))Dictionary_2_Remove_m183515743_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,JSCMapPathConfig>::TryGetValue(TKey,TValue&)
#define Dictionary_2_TryGetValue_m1458590687(__this, ___key0, ___value1, method) ((  bool (*) (Dictionary_2_t3423381968 *, int32_t, JSCMapPathConfig_t3426118729 **, const MethodInfo*))Dictionary_2_TryGetValue_m2256091851_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,JSCMapPathConfig>::get_Keys()
#define Dictionary_2_get_Keys_m3309452288(__this, method) ((  KeyCollection_t755174123 * (*) (Dictionary_2_t3423381968 *, const MethodInfo*))Dictionary_2_get_Keys_m4120714641_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,JSCMapPathConfig>::get_Values()
#define Dictionary_2_get_Values_m2176378880(__this, method) ((  ValueCollection_t2123987681 * (*) (Dictionary_2_t3423381968 *, const MethodInfo*))Dictionary_2_get_Values_m1815086189_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Int32,JSCMapPathConfig>::ToTKey(System.Object)
#define Dictionary_2_ToTKey_m4207746911(__this, ___key0, method) ((  int32_t (*) (Dictionary_2_t3423381968 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTKey_m844610694_gshared)(__this, ___key0, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,JSCMapPathConfig>::ToTValue(System.Object)
#define Dictionary_2_ToTValue_m3186546271(__this, ___value0, method) ((  JSCMapPathConfig_t3426118729 * (*) (Dictionary_2_t3423381968 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTValue_m3888328930_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,JSCMapPathConfig>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_ContainsKeyValuePair_m3434797453(__this, ___pair0, method) ((  bool (*) (Dictionary_2_t3423381968 *, KeyValuePair_2_t3322162674 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m139391042_gshared)(__this, ___pair0, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,JSCMapPathConfig>::GetEnumerator()
#define Dictionary_2_GetEnumerator_m3535391674(__this, method) ((  Enumerator_t445738064  (*) (Dictionary_2_t3423381968 *, const MethodInfo*))Dictionary_2_GetEnumerator_m3720989159_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Int32,JSCMapPathConfig>::<CopyTo>m__0(TKey,TValue)
#define Dictionary_2_U3CCopyToU3Em__0_m4221225097(__this /* static, unused */, ___key0, ___value1, method) ((  DictionaryEntry_t1751606614  (*) (Il2CppObject * /* static, unused */, int32_t, JSCMapPathConfig_t3426118729 *, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m2937104030_gshared)(__this /* static, unused */, ___key0, ___value1, method)
