﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._1f747e8755a8878c01957b6624b5993c
struct _1f747e8755a8878c01957b6624b5993c_t2896825798;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._1f747e8755a8878c01957b6624b5993c::.ctor()
extern "C"  void _1f747e8755a8878c01957b6624b5993c__ctor_m1508194087 (_1f747e8755a8878c01957b6624b5993c_t2896825798 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._1f747e8755a8878c01957b6624b5993c::_1f747e8755a8878c01957b6624b5993cm2(System.Int32)
extern "C"  int32_t _1f747e8755a8878c01957b6624b5993c__1f747e8755a8878c01957b6624b5993cm2_m2437221337 (_1f747e8755a8878c01957b6624b5993c_t2896825798 * __this, int32_t ____1f747e8755a8878c01957b6624b5993ca0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._1f747e8755a8878c01957b6624b5993c::_1f747e8755a8878c01957b6624b5993cm(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _1f747e8755a8878c01957b6624b5993c__1f747e8755a8878c01957b6624b5993cm_m572629757 (_1f747e8755a8878c01957b6624b5993c_t2896825798 * __this, int32_t ____1f747e8755a8878c01957b6624b5993ca0, int32_t ____1f747e8755a8878c01957b6624b5993c441, int32_t ____1f747e8755a8878c01957b6624b5993cc2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
