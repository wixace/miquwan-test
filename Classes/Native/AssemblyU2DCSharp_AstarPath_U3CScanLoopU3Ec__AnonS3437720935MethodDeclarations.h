﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AstarPath/<ScanLoop>c__AnonStorey105
struct U3CScanLoopU3Ec__AnonStorey105_t3437720935;
// Pathfinding.GraphNode
struct GraphNode_t23612370;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_Progress2476786339.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphNode23612370.h"

// System.Void AstarPath/<ScanLoop>c__AnonStorey105::.ctor()
extern "C"  void U3CScanLoopU3Ec__AnonStorey105__ctor_m2255083796 (U3CScanLoopU3Ec__AnonStorey105_t3437720935 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AstarPath/<ScanLoop>c__AnonStorey105::<>m__335(Pathfinding.Progress)
extern "C"  void U3CScanLoopU3Ec__AnonStorey105_U3CU3Em__335_m1828486723 (U3CScanLoopU3Ec__AnonStorey105_t3437720935 * __this, Progress_t2476786339  ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AstarPath/<ScanLoop>c__AnonStorey105::<>m__336(Pathfinding.GraphNode)
extern "C"  bool U3CScanLoopU3Ec__AnonStorey105_U3CU3Em__336_m1844948125 (U3CScanLoopU3Ec__AnonStorey105_t3437720935 * __this, GraphNode_t23612370 * ___node0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
