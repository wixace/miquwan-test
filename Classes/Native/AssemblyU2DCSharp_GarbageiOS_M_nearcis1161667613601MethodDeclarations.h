﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_nearcis116
struct M_nearcis116_t1667613601;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_nearcis1161667613601.h"

// System.Void GarbageiOS.M_nearcis116::.ctor()
extern "C"  void M_nearcis116__ctor_m2100104626 (M_nearcis116_t1667613601 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_nearcis116::M_sedeja0(System.String[],System.Int32)
extern "C"  void M_nearcis116_M_sedeja0_m4209701979 (M_nearcis116_t1667613601 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_nearcis116::M_cemboSirepo1(System.String[],System.Int32)
extern "C"  void M_nearcis116_M_cemboSirepo1_m4152731916 (M_nearcis116_t1667613601 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_nearcis116::M_howtrir2(System.String[],System.Int32)
extern "C"  void M_nearcis116_M_howtrir2_m2237760054 (M_nearcis116_t1667613601 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_nearcis116::M_bimeGeeqelkea3(System.String[],System.Int32)
extern "C"  void M_nearcis116_M_bimeGeeqelkea3_m3528419735 (M_nearcis116_t1667613601 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_nearcis116::M_rearpoukel4(System.String[],System.Int32)
extern "C"  void M_nearcis116_M_rearpoukel4_m1777781769 (M_nearcis116_t1667613601 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_nearcis116::M_machayjall5(System.String[],System.Int32)
extern "C"  void M_nearcis116_M_machayjall5_m772396738 (M_nearcis116_t1667613601 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_nearcis116::M_chawersuDrooyaw6(System.String[],System.Int32)
extern "C"  void M_nearcis116_M_chawersuDrooyaw6_m3417047290 (M_nearcis116_t1667613601 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_nearcis116::ilo_M_rearpoukel41(GarbageiOS.M_nearcis116,System.String[],System.Int32)
extern "C"  void M_nearcis116_ilo_M_rearpoukel41_m922045336 (Il2CppObject * __this /* static, unused */, M_nearcis116_t1667613601 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_nearcis116::ilo_M_machayjall52(GarbageiOS.M_nearcis116,System.String[],System.Int32)
extern "C"  void M_nearcis116_ilo_M_machayjall52_m451078208 (Il2CppObject * __this /* static, unused */, M_nearcis116_t1667613601 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
