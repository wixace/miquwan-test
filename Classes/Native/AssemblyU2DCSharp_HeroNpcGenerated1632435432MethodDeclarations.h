﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HeroNpcGenerated
struct HeroNpcGenerated_t1632435432;
// JSVCall
struct JSVCall_t3708497963;
// CSSkillData[]
struct CSSkillDataU5BU5D_t1071816810;
// HeroNpc
struct HeroNpc_t2475966567;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_HeroNpc2475966567.h"

// System.Void HeroNpcGenerated::.ctor()
extern "C"  void HeroNpcGenerated__ctor_m3933351603 (HeroNpcGenerated_t1632435432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroNpcGenerated::HeroNpc_HeroNpc1(JSVCall,System.Int32)
extern "C"  bool HeroNpcGenerated_HeroNpc_HeroNpc1_m877157079 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroNpcGenerated::HeroNpc_IsTrigger(JSVCall)
extern "C"  void HeroNpcGenerated_HeroNpc_IsTrigger_m2133183008 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroNpcGenerated::HeroNpc_Addhurt__Single(JSVCall,System.Int32)
extern "C"  bool HeroNpcGenerated_HeroNpc_Addhurt__Single_m2933217101 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroNpcGenerated::HeroNpc_CalcuFinalProp(JSVCall,System.Int32)
extern "C"  bool HeroNpcGenerated_HeroNpc_CalcuFinalProp_m66872614 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroNpcGenerated::HeroNpc_Create__CSHeroUnit__JSCLevelMonsterConfig(JSVCall,System.Int32)
extern "C"  bool HeroNpcGenerated_HeroNpc_Create__CSHeroUnit__JSCLevelMonsterConfig_m1143874301 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroNpcGenerated::HeroNpc_Create__herosCfg__JSCLevelMonsterConfig__CSSkillData_Array(JSVCall,System.Int32)
extern "C"  bool HeroNpcGenerated_HeroNpc_Create__herosCfg__JSCLevelMonsterConfig__CSSkillData_Array_m1539239111 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroNpcGenerated::HeroNpc_InitPos__Vector3__Quaternion(JSVCall,System.Int32)
extern "C"  bool HeroNpcGenerated_HeroNpc_InitPos__Vector3__Quaternion_m3007040023 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroNpcGenerated::HeroNpc_OnTriggerFun__Int32(JSVCall,System.Int32)
extern "C"  bool HeroNpcGenerated_HeroNpc_OnTriggerFun__Int32_m1712753853 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroNpcGenerated::HeroNpc_Start(JSVCall,System.Int32)
extern "C"  bool HeroNpcGenerated_HeroNpc_Start_m46589111 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroNpcGenerated::__Register()
extern "C"  void HeroNpcGenerated___Register_m3805251060 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CSSkillData[] HeroNpcGenerated::<HeroNpc_Create__herosCfg__JSCLevelMonsterConfig__CSSkillData_Array>m__68()
extern "C"  CSSkillDataU5BU5D_t1071816810* HeroNpcGenerated_U3CHeroNpc_Create__herosCfg__JSCLevelMonsterConfig__CSSkillData_ArrayU3Em__68_m2342926698 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroNpcGenerated::ilo_get_IsTrigger1(HeroNpc)
extern "C"  bool HeroNpcGenerated_ilo_get_IsTrigger1_m2104623623 (Il2CppObject * __this /* static, unused */, HeroNpc_t2475966567 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroNpcGenerated::ilo_Addhurt2(HeroNpc,System.Single)
extern "C"  void HeroNpcGenerated_ilo_Addhurt2_m4109036586 (Il2CppObject * __this /* static, unused */, HeroNpc_t2475966567 * ____this0, float ___delta1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroNpcGenerated::ilo_CalcuFinalProp3(HeroNpc)
extern "C"  void HeroNpcGenerated_ilo_CalcuFinalProp3_m3518877747 (Il2CppObject * __this /* static, unused */, HeroNpc_t2475966567 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroNpcGenerated::ilo_OnTriggerFun4(HeroNpc,System.Int32)
extern "C"  void HeroNpcGenerated_ilo_OnTriggerFun4_m379610776 (Il2CppObject * __this /* static, unused */, HeroNpc_t2475966567 * ____this0, int32_t ___effectId1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HeroNpcGenerated::ilo_getInt325(System.Int32)
extern "C"  int32_t HeroNpcGenerated_ilo_getInt325_m603341950 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroNpcGenerated::ilo_Start6(HeroNpc)
extern "C"  void HeroNpcGenerated_ilo_Start6_m2449477491 (Il2CppObject * __this /* static, unused */, HeroNpc_t2475966567 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HeroNpcGenerated::ilo_getObject7(System.Int32)
extern "C"  int32_t HeroNpcGenerated_ilo_getObject7_m175090629 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HeroNpcGenerated::ilo_getElement8(System.Int32,System.Int32)
extern "C"  int32_t HeroNpcGenerated_ilo_getElement8_m3666223076 (Il2CppObject * __this /* static, unused */, int32_t ___id0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
