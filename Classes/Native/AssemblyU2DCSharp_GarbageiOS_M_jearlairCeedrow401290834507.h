﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_jearlairCeedrow40
struct  M_jearlairCeedrow40_t1290834507  : public Il2CppObject
{
public:
	// System.UInt32 GarbageiOS.M_jearlairCeedrow40::_gumallti
	uint32_t ____gumallti_0;
	// System.Single GarbageiOS.M_jearlairCeedrow40::_cadawhurTuwee
	float ____cadawhurTuwee_1;
	// System.UInt32 GarbageiOS.M_jearlairCeedrow40::_soujer
	uint32_t ____soujer_2;
	// System.Boolean GarbageiOS.M_jearlairCeedrow40::_nearwemHearpea
	bool ____nearwemHearpea_3;
	// System.Boolean GarbageiOS.M_jearlairCeedrow40::_katemwaWherqeawhu
	bool ____katemwaWherqeawhu_4;
	// System.String GarbageiOS.M_jearlairCeedrow40::_raciLudow
	String_t* ____raciLudow_5;
	// System.String GarbageiOS.M_jearlairCeedrow40::_loheNardaypa
	String_t* ____loheNardaypa_6;
	// System.Boolean GarbageiOS.M_jearlairCeedrow40::_stalpa
	bool ____stalpa_7;
	// System.String GarbageiOS.M_jearlairCeedrow40::_lelrecelZevoo
	String_t* ____lelrecelZevoo_8;
	// System.Boolean GarbageiOS.M_jearlairCeedrow40::_teekurmoKayfinow
	bool ____teekurmoKayfinow_9;
	// System.Int32 GarbageiOS.M_jearlairCeedrow40::_honetallCouhufow
	int32_t ____honetallCouhufow_10;
	// System.UInt32 GarbageiOS.M_jearlairCeedrow40::_jadur
	uint32_t ____jadur_11;
	// System.String GarbageiOS.M_jearlairCeedrow40::_gesomuHallteerair
	String_t* ____gesomuHallteerair_12;
	// System.Boolean GarbageiOS.M_jearlairCeedrow40::_trisja
	bool ____trisja_13;
	// System.String GarbageiOS.M_jearlairCeedrow40::_cepiweeHairpouchur
	String_t* ____cepiweeHairpouchur_14;
	// System.UInt32 GarbageiOS.M_jearlairCeedrow40::_cowniPeehea
	uint32_t ____cowniPeehea_15;
	// System.String GarbageiOS.M_jearlairCeedrow40::_hisyiStabiwhe
	String_t* ____hisyiStabiwhe_16;
	// System.Single GarbageiOS.M_jearlairCeedrow40::_nurse
	float ____nurse_17;
	// System.Boolean GarbageiOS.M_jearlairCeedrow40::_ralqooBilarje
	bool ____ralqooBilarje_18;
	// System.UInt32 GarbageiOS.M_jearlairCeedrow40::_segeqooSurlaylo
	uint32_t ____segeqooSurlaylo_19;
	// System.String GarbageiOS.M_jearlairCeedrow40::_gerkapairLedrem
	String_t* ____gerkapairLedrem_20;

public:
	inline static int32_t get_offset_of__gumallti_0() { return static_cast<int32_t>(offsetof(M_jearlairCeedrow40_t1290834507, ____gumallti_0)); }
	inline uint32_t get__gumallti_0() const { return ____gumallti_0; }
	inline uint32_t* get_address_of__gumallti_0() { return &____gumallti_0; }
	inline void set__gumallti_0(uint32_t value)
	{
		____gumallti_0 = value;
	}

	inline static int32_t get_offset_of__cadawhurTuwee_1() { return static_cast<int32_t>(offsetof(M_jearlairCeedrow40_t1290834507, ____cadawhurTuwee_1)); }
	inline float get__cadawhurTuwee_1() const { return ____cadawhurTuwee_1; }
	inline float* get_address_of__cadawhurTuwee_1() { return &____cadawhurTuwee_1; }
	inline void set__cadawhurTuwee_1(float value)
	{
		____cadawhurTuwee_1 = value;
	}

	inline static int32_t get_offset_of__soujer_2() { return static_cast<int32_t>(offsetof(M_jearlairCeedrow40_t1290834507, ____soujer_2)); }
	inline uint32_t get__soujer_2() const { return ____soujer_2; }
	inline uint32_t* get_address_of__soujer_2() { return &____soujer_2; }
	inline void set__soujer_2(uint32_t value)
	{
		____soujer_2 = value;
	}

	inline static int32_t get_offset_of__nearwemHearpea_3() { return static_cast<int32_t>(offsetof(M_jearlairCeedrow40_t1290834507, ____nearwemHearpea_3)); }
	inline bool get__nearwemHearpea_3() const { return ____nearwemHearpea_3; }
	inline bool* get_address_of__nearwemHearpea_3() { return &____nearwemHearpea_3; }
	inline void set__nearwemHearpea_3(bool value)
	{
		____nearwemHearpea_3 = value;
	}

	inline static int32_t get_offset_of__katemwaWherqeawhu_4() { return static_cast<int32_t>(offsetof(M_jearlairCeedrow40_t1290834507, ____katemwaWherqeawhu_4)); }
	inline bool get__katemwaWherqeawhu_4() const { return ____katemwaWherqeawhu_4; }
	inline bool* get_address_of__katemwaWherqeawhu_4() { return &____katemwaWherqeawhu_4; }
	inline void set__katemwaWherqeawhu_4(bool value)
	{
		____katemwaWherqeawhu_4 = value;
	}

	inline static int32_t get_offset_of__raciLudow_5() { return static_cast<int32_t>(offsetof(M_jearlairCeedrow40_t1290834507, ____raciLudow_5)); }
	inline String_t* get__raciLudow_5() const { return ____raciLudow_5; }
	inline String_t** get_address_of__raciLudow_5() { return &____raciLudow_5; }
	inline void set__raciLudow_5(String_t* value)
	{
		____raciLudow_5 = value;
		Il2CppCodeGenWriteBarrier(&____raciLudow_5, value);
	}

	inline static int32_t get_offset_of__loheNardaypa_6() { return static_cast<int32_t>(offsetof(M_jearlairCeedrow40_t1290834507, ____loheNardaypa_6)); }
	inline String_t* get__loheNardaypa_6() const { return ____loheNardaypa_6; }
	inline String_t** get_address_of__loheNardaypa_6() { return &____loheNardaypa_6; }
	inline void set__loheNardaypa_6(String_t* value)
	{
		____loheNardaypa_6 = value;
		Il2CppCodeGenWriteBarrier(&____loheNardaypa_6, value);
	}

	inline static int32_t get_offset_of__stalpa_7() { return static_cast<int32_t>(offsetof(M_jearlairCeedrow40_t1290834507, ____stalpa_7)); }
	inline bool get__stalpa_7() const { return ____stalpa_7; }
	inline bool* get_address_of__stalpa_7() { return &____stalpa_7; }
	inline void set__stalpa_7(bool value)
	{
		____stalpa_7 = value;
	}

	inline static int32_t get_offset_of__lelrecelZevoo_8() { return static_cast<int32_t>(offsetof(M_jearlairCeedrow40_t1290834507, ____lelrecelZevoo_8)); }
	inline String_t* get__lelrecelZevoo_8() const { return ____lelrecelZevoo_8; }
	inline String_t** get_address_of__lelrecelZevoo_8() { return &____lelrecelZevoo_8; }
	inline void set__lelrecelZevoo_8(String_t* value)
	{
		____lelrecelZevoo_8 = value;
		Il2CppCodeGenWriteBarrier(&____lelrecelZevoo_8, value);
	}

	inline static int32_t get_offset_of__teekurmoKayfinow_9() { return static_cast<int32_t>(offsetof(M_jearlairCeedrow40_t1290834507, ____teekurmoKayfinow_9)); }
	inline bool get__teekurmoKayfinow_9() const { return ____teekurmoKayfinow_9; }
	inline bool* get_address_of__teekurmoKayfinow_9() { return &____teekurmoKayfinow_9; }
	inline void set__teekurmoKayfinow_9(bool value)
	{
		____teekurmoKayfinow_9 = value;
	}

	inline static int32_t get_offset_of__honetallCouhufow_10() { return static_cast<int32_t>(offsetof(M_jearlairCeedrow40_t1290834507, ____honetallCouhufow_10)); }
	inline int32_t get__honetallCouhufow_10() const { return ____honetallCouhufow_10; }
	inline int32_t* get_address_of__honetallCouhufow_10() { return &____honetallCouhufow_10; }
	inline void set__honetallCouhufow_10(int32_t value)
	{
		____honetallCouhufow_10 = value;
	}

	inline static int32_t get_offset_of__jadur_11() { return static_cast<int32_t>(offsetof(M_jearlairCeedrow40_t1290834507, ____jadur_11)); }
	inline uint32_t get__jadur_11() const { return ____jadur_11; }
	inline uint32_t* get_address_of__jadur_11() { return &____jadur_11; }
	inline void set__jadur_11(uint32_t value)
	{
		____jadur_11 = value;
	}

	inline static int32_t get_offset_of__gesomuHallteerair_12() { return static_cast<int32_t>(offsetof(M_jearlairCeedrow40_t1290834507, ____gesomuHallteerair_12)); }
	inline String_t* get__gesomuHallteerair_12() const { return ____gesomuHallteerair_12; }
	inline String_t** get_address_of__gesomuHallteerair_12() { return &____gesomuHallteerair_12; }
	inline void set__gesomuHallteerair_12(String_t* value)
	{
		____gesomuHallteerair_12 = value;
		Il2CppCodeGenWriteBarrier(&____gesomuHallteerair_12, value);
	}

	inline static int32_t get_offset_of__trisja_13() { return static_cast<int32_t>(offsetof(M_jearlairCeedrow40_t1290834507, ____trisja_13)); }
	inline bool get__trisja_13() const { return ____trisja_13; }
	inline bool* get_address_of__trisja_13() { return &____trisja_13; }
	inline void set__trisja_13(bool value)
	{
		____trisja_13 = value;
	}

	inline static int32_t get_offset_of__cepiweeHairpouchur_14() { return static_cast<int32_t>(offsetof(M_jearlairCeedrow40_t1290834507, ____cepiweeHairpouchur_14)); }
	inline String_t* get__cepiweeHairpouchur_14() const { return ____cepiweeHairpouchur_14; }
	inline String_t** get_address_of__cepiweeHairpouchur_14() { return &____cepiweeHairpouchur_14; }
	inline void set__cepiweeHairpouchur_14(String_t* value)
	{
		____cepiweeHairpouchur_14 = value;
		Il2CppCodeGenWriteBarrier(&____cepiweeHairpouchur_14, value);
	}

	inline static int32_t get_offset_of__cowniPeehea_15() { return static_cast<int32_t>(offsetof(M_jearlairCeedrow40_t1290834507, ____cowniPeehea_15)); }
	inline uint32_t get__cowniPeehea_15() const { return ____cowniPeehea_15; }
	inline uint32_t* get_address_of__cowniPeehea_15() { return &____cowniPeehea_15; }
	inline void set__cowniPeehea_15(uint32_t value)
	{
		____cowniPeehea_15 = value;
	}

	inline static int32_t get_offset_of__hisyiStabiwhe_16() { return static_cast<int32_t>(offsetof(M_jearlairCeedrow40_t1290834507, ____hisyiStabiwhe_16)); }
	inline String_t* get__hisyiStabiwhe_16() const { return ____hisyiStabiwhe_16; }
	inline String_t** get_address_of__hisyiStabiwhe_16() { return &____hisyiStabiwhe_16; }
	inline void set__hisyiStabiwhe_16(String_t* value)
	{
		____hisyiStabiwhe_16 = value;
		Il2CppCodeGenWriteBarrier(&____hisyiStabiwhe_16, value);
	}

	inline static int32_t get_offset_of__nurse_17() { return static_cast<int32_t>(offsetof(M_jearlairCeedrow40_t1290834507, ____nurse_17)); }
	inline float get__nurse_17() const { return ____nurse_17; }
	inline float* get_address_of__nurse_17() { return &____nurse_17; }
	inline void set__nurse_17(float value)
	{
		____nurse_17 = value;
	}

	inline static int32_t get_offset_of__ralqooBilarje_18() { return static_cast<int32_t>(offsetof(M_jearlairCeedrow40_t1290834507, ____ralqooBilarje_18)); }
	inline bool get__ralqooBilarje_18() const { return ____ralqooBilarje_18; }
	inline bool* get_address_of__ralqooBilarje_18() { return &____ralqooBilarje_18; }
	inline void set__ralqooBilarje_18(bool value)
	{
		____ralqooBilarje_18 = value;
	}

	inline static int32_t get_offset_of__segeqooSurlaylo_19() { return static_cast<int32_t>(offsetof(M_jearlairCeedrow40_t1290834507, ____segeqooSurlaylo_19)); }
	inline uint32_t get__segeqooSurlaylo_19() const { return ____segeqooSurlaylo_19; }
	inline uint32_t* get_address_of__segeqooSurlaylo_19() { return &____segeqooSurlaylo_19; }
	inline void set__segeqooSurlaylo_19(uint32_t value)
	{
		____segeqooSurlaylo_19 = value;
	}

	inline static int32_t get_offset_of__gerkapairLedrem_20() { return static_cast<int32_t>(offsetof(M_jearlairCeedrow40_t1290834507, ____gerkapairLedrem_20)); }
	inline String_t* get__gerkapairLedrem_20() const { return ____gerkapairLedrem_20; }
	inline String_t** get_address_of__gerkapairLedrem_20() { return &____gerkapairLedrem_20; }
	inline void set__gerkapairLedrem_20(String_t* value)
	{
		____gerkapairLedrem_20 = value;
		Il2CppCodeGenWriteBarrier(&____gerkapairLedrem_20, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
