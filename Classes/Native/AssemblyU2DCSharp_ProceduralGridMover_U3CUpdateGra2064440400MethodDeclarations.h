﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ProceduralGridMover/<UpdateGraphCoroutine>c__Iterator10
struct U3CUpdateGraphCoroutineU3Ec__Iterator10_t2064440400;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void ProceduralGridMover/<UpdateGraphCoroutine>c__Iterator10::.ctor()
extern "C"  void U3CUpdateGraphCoroutineU3Ec__Iterator10__ctor_m1935064347 (U3CUpdateGraphCoroutineU3Ec__Iterator10_t2064440400 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProceduralGridMover/<UpdateGraphCoroutine>c__Iterator10::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CUpdateGraphCoroutineU3Ec__Iterator10_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m559341591 (U3CUpdateGraphCoroutineU3Ec__Iterator10_t2064440400 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProceduralGridMover/<UpdateGraphCoroutine>c__Iterator10::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CUpdateGraphCoroutineU3Ec__Iterator10_System_Collections_IEnumerator_get_Current_m2430726571 (U3CUpdateGraphCoroutineU3Ec__Iterator10_t2064440400 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProceduralGridMover/<UpdateGraphCoroutine>c__Iterator10::MoveNext()
extern "C"  bool U3CUpdateGraphCoroutineU3Ec__Iterator10_MoveNext_m2020991737 (U3CUpdateGraphCoroutineU3Ec__Iterator10_t2064440400 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProceduralGridMover/<UpdateGraphCoroutine>c__Iterator10::Dispose()
extern "C"  void U3CUpdateGraphCoroutineU3Ec__Iterator10_Dispose_m4069248664 (U3CUpdateGraphCoroutineU3Ec__Iterator10_t2064440400 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProceduralGridMover/<UpdateGraphCoroutine>c__Iterator10::Reset()
extern "C"  void U3CUpdateGraphCoroutineU3Ec__Iterator10_Reset_m3876464584 (U3CUpdateGraphCoroutineU3Ec__Iterator10_t2064440400 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
