﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_coucawhe122
struct  M_coucawhe122_t3668090276  : public Il2CppObject
{
public:
	// System.String GarbageiOS.M_coucawhe122::_hawayPemhaw
	String_t* ____hawayPemhaw_0;
	// System.Int32 GarbageiOS.M_coucawhe122::_duleHeadrirlal
	int32_t ____duleHeadrirlal_1;
	// System.String GarbageiOS.M_coucawhe122::_yemmouNeajetri
	String_t* ____yemmouNeajetri_2;
	// System.UInt32 GarbageiOS.M_coucawhe122::_chirmurToduqi
	uint32_t ____chirmurToduqi_3;
	// System.UInt32 GarbageiOS.M_coucawhe122::_rempeaRifaw
	uint32_t ____rempeaRifaw_4;
	// System.Int32 GarbageiOS.M_coucawhe122::_narja
	int32_t ____narja_5;
	// System.Single GarbageiOS.M_coucawhe122::_dawlaiSesar
	float ____dawlaiSesar_6;
	// System.String GarbageiOS.M_coucawhe122::_selascall
	String_t* ____selascall_7;
	// System.Boolean GarbageiOS.M_coucawhe122::_cepasaSealetrel
	bool ____cepasaSealetrel_8;
	// System.String GarbageiOS.M_coucawhe122::_borkouJeatraw
	String_t* ____borkouJeatraw_9;
	// System.Int32 GarbageiOS.M_coucawhe122::_wamiSaler
	int32_t ____wamiSaler_10;
	// System.Single GarbageiOS.M_coucawhe122::_kelzemhirCorversee
	float ____kelzemhirCorversee_11;

public:
	inline static int32_t get_offset_of__hawayPemhaw_0() { return static_cast<int32_t>(offsetof(M_coucawhe122_t3668090276, ____hawayPemhaw_0)); }
	inline String_t* get__hawayPemhaw_0() const { return ____hawayPemhaw_0; }
	inline String_t** get_address_of__hawayPemhaw_0() { return &____hawayPemhaw_0; }
	inline void set__hawayPemhaw_0(String_t* value)
	{
		____hawayPemhaw_0 = value;
		Il2CppCodeGenWriteBarrier(&____hawayPemhaw_0, value);
	}

	inline static int32_t get_offset_of__duleHeadrirlal_1() { return static_cast<int32_t>(offsetof(M_coucawhe122_t3668090276, ____duleHeadrirlal_1)); }
	inline int32_t get__duleHeadrirlal_1() const { return ____duleHeadrirlal_1; }
	inline int32_t* get_address_of__duleHeadrirlal_1() { return &____duleHeadrirlal_1; }
	inline void set__duleHeadrirlal_1(int32_t value)
	{
		____duleHeadrirlal_1 = value;
	}

	inline static int32_t get_offset_of__yemmouNeajetri_2() { return static_cast<int32_t>(offsetof(M_coucawhe122_t3668090276, ____yemmouNeajetri_2)); }
	inline String_t* get__yemmouNeajetri_2() const { return ____yemmouNeajetri_2; }
	inline String_t** get_address_of__yemmouNeajetri_2() { return &____yemmouNeajetri_2; }
	inline void set__yemmouNeajetri_2(String_t* value)
	{
		____yemmouNeajetri_2 = value;
		Il2CppCodeGenWriteBarrier(&____yemmouNeajetri_2, value);
	}

	inline static int32_t get_offset_of__chirmurToduqi_3() { return static_cast<int32_t>(offsetof(M_coucawhe122_t3668090276, ____chirmurToduqi_3)); }
	inline uint32_t get__chirmurToduqi_3() const { return ____chirmurToduqi_3; }
	inline uint32_t* get_address_of__chirmurToduqi_3() { return &____chirmurToduqi_3; }
	inline void set__chirmurToduqi_3(uint32_t value)
	{
		____chirmurToduqi_3 = value;
	}

	inline static int32_t get_offset_of__rempeaRifaw_4() { return static_cast<int32_t>(offsetof(M_coucawhe122_t3668090276, ____rempeaRifaw_4)); }
	inline uint32_t get__rempeaRifaw_4() const { return ____rempeaRifaw_4; }
	inline uint32_t* get_address_of__rempeaRifaw_4() { return &____rempeaRifaw_4; }
	inline void set__rempeaRifaw_4(uint32_t value)
	{
		____rempeaRifaw_4 = value;
	}

	inline static int32_t get_offset_of__narja_5() { return static_cast<int32_t>(offsetof(M_coucawhe122_t3668090276, ____narja_5)); }
	inline int32_t get__narja_5() const { return ____narja_5; }
	inline int32_t* get_address_of__narja_5() { return &____narja_5; }
	inline void set__narja_5(int32_t value)
	{
		____narja_5 = value;
	}

	inline static int32_t get_offset_of__dawlaiSesar_6() { return static_cast<int32_t>(offsetof(M_coucawhe122_t3668090276, ____dawlaiSesar_6)); }
	inline float get__dawlaiSesar_6() const { return ____dawlaiSesar_6; }
	inline float* get_address_of__dawlaiSesar_6() { return &____dawlaiSesar_6; }
	inline void set__dawlaiSesar_6(float value)
	{
		____dawlaiSesar_6 = value;
	}

	inline static int32_t get_offset_of__selascall_7() { return static_cast<int32_t>(offsetof(M_coucawhe122_t3668090276, ____selascall_7)); }
	inline String_t* get__selascall_7() const { return ____selascall_7; }
	inline String_t** get_address_of__selascall_7() { return &____selascall_7; }
	inline void set__selascall_7(String_t* value)
	{
		____selascall_7 = value;
		Il2CppCodeGenWriteBarrier(&____selascall_7, value);
	}

	inline static int32_t get_offset_of__cepasaSealetrel_8() { return static_cast<int32_t>(offsetof(M_coucawhe122_t3668090276, ____cepasaSealetrel_8)); }
	inline bool get__cepasaSealetrel_8() const { return ____cepasaSealetrel_8; }
	inline bool* get_address_of__cepasaSealetrel_8() { return &____cepasaSealetrel_8; }
	inline void set__cepasaSealetrel_8(bool value)
	{
		____cepasaSealetrel_8 = value;
	}

	inline static int32_t get_offset_of__borkouJeatraw_9() { return static_cast<int32_t>(offsetof(M_coucawhe122_t3668090276, ____borkouJeatraw_9)); }
	inline String_t* get__borkouJeatraw_9() const { return ____borkouJeatraw_9; }
	inline String_t** get_address_of__borkouJeatraw_9() { return &____borkouJeatraw_9; }
	inline void set__borkouJeatraw_9(String_t* value)
	{
		____borkouJeatraw_9 = value;
		Il2CppCodeGenWriteBarrier(&____borkouJeatraw_9, value);
	}

	inline static int32_t get_offset_of__wamiSaler_10() { return static_cast<int32_t>(offsetof(M_coucawhe122_t3668090276, ____wamiSaler_10)); }
	inline int32_t get__wamiSaler_10() const { return ____wamiSaler_10; }
	inline int32_t* get_address_of__wamiSaler_10() { return &____wamiSaler_10; }
	inline void set__wamiSaler_10(int32_t value)
	{
		____wamiSaler_10 = value;
	}

	inline static int32_t get_offset_of__kelzemhirCorversee_11() { return static_cast<int32_t>(offsetof(M_coucawhe122_t3668090276, ____kelzemhirCorversee_11)); }
	inline float get__kelzemhirCorversee_11() const { return ____kelzemhirCorversee_11; }
	inline float* get_address_of__kelzemhirCorversee_11() { return &____kelzemhirCorversee_11; }
	inline void set__kelzemhirCorversee_11(float value)
	{
		____kelzemhirCorversee_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
