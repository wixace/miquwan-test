﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._40e018f3508a8f7c4130c7f43f3a2e6e
struct _40e018f3508a8f7c4130c7f43f3a2e6e_t3704539217;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._40e018f3508a8f7c4130c7f43f3a2e6e::.ctor()
extern "C"  void _40e018f3508a8f7c4130c7f43f3a2e6e__ctor_m307374332 (_40e018f3508a8f7c4130c7f43f3a2e6e_t3704539217 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._40e018f3508a8f7c4130c7f43f3a2e6e::_40e018f3508a8f7c4130c7f43f3a2e6em2(System.Int32)
extern "C"  int32_t _40e018f3508a8f7c4130c7f43f3a2e6e__40e018f3508a8f7c4130c7f43f3a2e6em2_m4212271097 (_40e018f3508a8f7c4130c7f43f3a2e6e_t3704539217 * __this, int32_t ____40e018f3508a8f7c4130c7f43f3a2e6ea0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._40e018f3508a8f7c4130c7f43f3a2e6e::_40e018f3508a8f7c4130c7f43f3a2e6em(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _40e018f3508a8f7c4130c7f43f3a2e6e__40e018f3508a8f7c4130c7f43f3a2e6em_m1765173981 (_40e018f3508a8f7c4130c7f43f3a2e6e_t3704539217 * __this, int32_t ____40e018f3508a8f7c4130c7f43f3a2e6ea0, int32_t ____40e018f3508a8f7c4130c7f43f3a2e6e41, int32_t ____40e018f3508a8f7c4130c7f43f3a2e6ec2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
