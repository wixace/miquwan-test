﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._38644fab01883fa718a304b3f9d46866
struct _38644fab01883fa718a304b3f9d46866_t1309415084;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._38644fab01883fa718a304b3f9d46866::.ctor()
extern "C"  void _38644fab01883fa718a304b3f9d46866__ctor_m3964129537 (_38644fab01883fa718a304b3f9d46866_t1309415084 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._38644fab01883fa718a304b3f9d46866::_38644fab01883fa718a304b3f9d46866m2(System.Int32)
extern "C"  int32_t _38644fab01883fa718a304b3f9d46866__38644fab01883fa718a304b3f9d46866m2_m3271569945 (_38644fab01883fa718a304b3f9d46866_t1309415084 * __this, int32_t ____38644fab01883fa718a304b3f9d46866a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._38644fab01883fa718a304b3f9d46866::_38644fab01883fa718a304b3f9d46866m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _38644fab01883fa718a304b3f9d46866__38644fab01883fa718a304b3f9d46866m_m2171733181 (_38644fab01883fa718a304b3f9d46866_t1309415084 * __this, int32_t ____38644fab01883fa718a304b3f9d46866a0, int32_t ____38644fab01883fa718a304b3f9d46866521, int32_t ____38644fab01883fa718a304b3f9d46866c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
