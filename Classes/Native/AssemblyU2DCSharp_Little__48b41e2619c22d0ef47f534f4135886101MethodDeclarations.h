﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._48b41e2619c22d0ef47f534fb01d5ce9
struct _48b41e2619c22d0ef47f534fb01d5ce9_t4135886101;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._48b41e2619c22d0ef47f534fb01d5ce9::.ctor()
extern "C"  void _48b41e2619c22d0ef47f534fb01d5ce9__ctor_m3995431096 (_48b41e2619c22d0ef47f534fb01d5ce9_t4135886101 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._48b41e2619c22d0ef47f534fb01d5ce9::_48b41e2619c22d0ef47f534fb01d5ce9m2(System.Int32)
extern "C"  int32_t _48b41e2619c22d0ef47f534fb01d5ce9__48b41e2619c22d0ef47f534fb01d5ce9m2_m1585828217 (_48b41e2619c22d0ef47f534fb01d5ce9_t4135886101 * __this, int32_t ____48b41e2619c22d0ef47f534fb01d5ce9a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._48b41e2619c22d0ef47f534fb01d5ce9::_48b41e2619c22d0ef47f534fb01d5ce9m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _48b41e2619c22d0ef47f534fb01d5ce9__48b41e2619c22d0ef47f534fb01d5ce9m_m564949341 (_48b41e2619c22d0ef47f534fb01d5ce9_t4135886101 * __this, int32_t ____48b41e2619c22d0ef47f534fb01d5ce9a0, int32_t ____48b41e2619c22d0ef47f534fb01d5ce9961, int32_t ____48b41e2619c22d0ef47f534fb01d5ce9c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
