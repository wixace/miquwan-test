﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Int32[]
struct Int32U5BU5D_t3230847821;
// Pathfinding.INavmeshHolder[]
struct INavmeshHolderU5BU5D_t3899225556;

#include "AssemblyU2DCSharp_Pathfinding_MeshNode3005053445.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.ConvexMeshNode
struct  ConvexMeshNode_t2987736716  : public MeshNode_t3005053445
{
public:
	// System.Int32[] Pathfinding.ConvexMeshNode::indices
	Int32U5BU5D_t3230847821* ___indices_17;

public:
	inline static int32_t get_offset_of_indices_17() { return static_cast<int32_t>(offsetof(ConvexMeshNode_t2987736716, ___indices_17)); }
	inline Int32U5BU5D_t3230847821* get_indices_17() const { return ___indices_17; }
	inline Int32U5BU5D_t3230847821** get_address_of_indices_17() { return &___indices_17; }
	inline void set_indices_17(Int32U5BU5D_t3230847821* value)
	{
		___indices_17 = value;
		Il2CppCodeGenWriteBarrier(&___indices_17, value);
	}
};

struct ConvexMeshNode_t2987736716_StaticFields
{
public:
	// Pathfinding.INavmeshHolder[] Pathfinding.ConvexMeshNode::navmeshHolders
	INavmeshHolderU5BU5D_t3899225556* ___navmeshHolders_18;

public:
	inline static int32_t get_offset_of_navmeshHolders_18() { return static_cast<int32_t>(offsetof(ConvexMeshNode_t2987736716_StaticFields, ___navmeshHolders_18)); }
	inline INavmeshHolderU5BU5D_t3899225556* get_navmeshHolders_18() const { return ___navmeshHolders_18; }
	inline INavmeshHolderU5BU5D_t3899225556** get_address_of_navmeshHolders_18() { return &___navmeshHolders_18; }
	inline void set_navmeshHolders_18(INavmeshHolderU5BU5D_t3899225556* value)
	{
		___navmeshHolders_18 = value;
		Il2CppCodeGenWriteBarrier(&___navmeshHolders_18, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
