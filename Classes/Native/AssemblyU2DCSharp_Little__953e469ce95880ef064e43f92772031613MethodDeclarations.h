﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._953e469ce95880ef064e43f99205f009
struct _953e469ce95880ef064e43f99205f009_t2772031613;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._953e469ce95880ef064e43f99205f009::.ctor()
extern "C"  void _953e469ce95880ef064e43f99205f009__ctor_m637045328 (_953e469ce95880ef064e43f99205f009_t2772031613 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._953e469ce95880ef064e43f99205f009::_953e469ce95880ef064e43f99205f009m2(System.Int32)
extern "C"  int32_t _953e469ce95880ef064e43f99205f009__953e469ce95880ef064e43f99205f009m2_m824498297 (_953e469ce95880ef064e43f99205f009_t2772031613 * __this, int32_t ____953e469ce95880ef064e43f99205f009a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._953e469ce95880ef064e43f99205f009::_953e469ce95880ef064e43f99205f009m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _953e469ce95880ef064e43f99205f009__953e469ce95880ef064e43f99205f009m_m557009501 (_953e469ce95880ef064e43f99205f009_t2772031613 * __this, int32_t ____953e469ce95880ef064e43f99205f009a0, int32_t ____953e469ce95880ef064e43f99205f009401, int32_t ____953e469ce95880ef064e43f99205f009c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
