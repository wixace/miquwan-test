﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Int3>
struct ReadOnlyCollection_1_t3531123130;
// System.Collections.Generic.IList`1<Pathfinding.Int3>
struct IList_1_t373725501;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// Pathfinding.Int3[]
struct Int3U5BU5D_t516284607;
// System.Collections.Generic.IEnumerator`1<Pathfinding.Int3>
struct IEnumerator_1_t3885910643;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_Int31974045594.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Int3>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C"  void ReadOnlyCollection_1__ctor_m175917026_gshared (ReadOnlyCollection_1_t3531123130 * __this, Il2CppObject* ___list0, const MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m175917026(__this, ___list0, method) ((  void (*) (ReadOnlyCollection_1_t3531123130 *, Il2CppObject*, const MethodInfo*))ReadOnlyCollection_1__ctor_m175917026_gshared)(__this, ___list0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Int3>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1465235660_gshared (ReadOnlyCollection_1_t3531123130 * __this, Int3_t1974045594  ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1465235660(__this, ___item0, method) ((  void (*) (ReadOnlyCollection_1_t3531123130 *, Int3_t1974045594 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1465235660_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Int3>::System.Collections.Generic.ICollection<T>.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m184356222_gshared (ReadOnlyCollection_1_t3531123130 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m184356222(__this, method) ((  void (*) (ReadOnlyCollection_1_t3531123130 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m184356222_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Int3>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m2571807923_gshared (ReadOnlyCollection_1_t3531123130 * __this, int32_t ___index0, Int3_t1974045594  ___item1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m2571807923(__this, ___index0, ___item1, method) ((  void (*) (ReadOnlyCollection_1_t3531123130 *, int32_t, Int3_t1974045594 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m2571807923_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Int3>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m3141134187_gshared (ReadOnlyCollection_1_t3531123130 * __this, Int3_t1974045594  ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m3141134187(__this, ___item0, method) ((  bool (*) (ReadOnlyCollection_1_t3531123130 *, Int3_t1974045594 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m3141134187_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Int3>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m445660793_gshared (ReadOnlyCollection_1_t3531123130 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m445660793(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t3531123130 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m445660793_gshared)(__this, ___index0, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Int3>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C"  Int3_t1974045594  ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m1302712671_gshared (ReadOnlyCollection_1_t3531123130 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m1302712671(__this, ___index0, method) ((  Int3_t1974045594  (*) (ReadOnlyCollection_1_t3531123130 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m1302712671_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Int3>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m3710815434_gshared (ReadOnlyCollection_1_t3531123130 * __this, int32_t ___index0, Int3_t1974045594  ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m3710815434(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t3531123130 *, int32_t, Int3_t1974045594 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m3710815434_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Int3>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3929651140_gshared (ReadOnlyCollection_1_t3531123130 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3929651140(__this, method) ((  bool (*) (ReadOnlyCollection_1_t3531123130 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3929651140_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Int3>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m2614973841_gshared (ReadOnlyCollection_1_t3531123130 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m2614973841(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t3531123130 *, Il2CppArray *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m2614973841_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Int3>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m2031371616_gshared (ReadOnlyCollection_1_t3531123130 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m2031371616(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t3531123130 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m2031371616_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Int3>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m606753629_gshared (ReadOnlyCollection_1_t3531123130 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m606753629(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t3531123130 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m606753629_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Int3>::System.Collections.IList.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Clear_m2864507039_gshared (ReadOnlyCollection_1_t3531123130 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m2864507039(__this, method) ((  void (*) (ReadOnlyCollection_1_t3531123130 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m2864507039_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Int3>::System.Collections.IList.Contains(System.Object)
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_Contains_m2941063043_gshared (ReadOnlyCollection_1_t3531123130 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m2941063043(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t3531123130 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m2941063043_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Int3>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m1160221493_gshared (ReadOnlyCollection_1_t3531123130 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m1160221493(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t3531123130 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m1160221493_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Int3>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Insert_m4226594856_gshared (ReadOnlyCollection_1_t3531123130 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m4226594856(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t3531123130 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m4226594856_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Int3>::System.Collections.IList.Remove(System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Remove_m651612160_gshared (ReadOnlyCollection_1_t3531123130 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m651612160(__this, ___value0, method) ((  void (*) (ReadOnlyCollection_1_t3531123130 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m651612160_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Int3>::System.Collections.IList.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m310496184_gshared (ReadOnlyCollection_1_t3531123130 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m310496184(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t3531123130 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m310496184_gshared)(__this, ___index0, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Int3>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m4166219001_gshared (ReadOnlyCollection_1_t3531123130 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m4166219001(__this, method) ((  bool (*) (ReadOnlyCollection_1_t3531123130 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m4166219001_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Int3>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m4136702827_gshared (ReadOnlyCollection_1_t3531123130 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m4136702827(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t3531123130 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m4136702827_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Int3>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m841702706_gshared (ReadOnlyCollection_1_t3531123130 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m841702706(__this, method) ((  bool (*) (ReadOnlyCollection_1_t3531123130 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m841702706_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Int3>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m2143728519_gshared (ReadOnlyCollection_1_t3531123130 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m2143728519(__this, method) ((  bool (*) (ReadOnlyCollection_1_t3531123130 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m2143728519_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Int3>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IList_get_Item_m3592279602_gshared (ReadOnlyCollection_1_t3531123130 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m3592279602(__this, ___index0, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t3531123130 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m3592279602_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Int3>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_set_Item_m3627514367_gshared (ReadOnlyCollection_1_t3531123130 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m3627514367(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t3531123130 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m3627514367_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Int3>::Contains(T)
extern "C"  bool ReadOnlyCollection_1_Contains_m3725425584_gshared (ReadOnlyCollection_1_t3531123130 * __this, Int3_t1974045594  ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m3725425584(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t3531123130 *, Int3_t1974045594 , const MethodInfo*))ReadOnlyCollection_1_Contains_m3725425584_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Int3>::CopyTo(T[],System.Int32)
extern "C"  void ReadOnlyCollection_1_CopyTo_m2317594620_gshared (ReadOnlyCollection_1_t3531123130 * __this, Int3U5BU5D_t516284607* ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m2317594620(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t3531123130 *, Int3U5BU5D_t516284607*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m2317594620_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Int3>::GetEnumerator()
extern "C"  Il2CppObject* ReadOnlyCollection_1_GetEnumerator_m798183303_gshared (ReadOnlyCollection_1_t3531123130 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m798183303(__this, method) ((  Il2CppObject* (*) (ReadOnlyCollection_1_t3531123130 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m798183303_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Int3>::IndexOf(T)
extern "C"  int32_t ReadOnlyCollection_1_IndexOf_m3402562504_gshared (ReadOnlyCollection_1_t3531123130 * __this, Int3_t1974045594  ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m3402562504(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t3531123130 *, Int3_t1974045594 , const MethodInfo*))ReadOnlyCollection_1_IndexOf_m3402562504_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Int3>::get_Count()
extern "C"  int32_t ReadOnlyCollection_1_get_Count_m2217530035_gshared (ReadOnlyCollection_1_t3531123130 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m2217530035(__this, method) ((  int32_t (*) (ReadOnlyCollection_1_t3531123130 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m2217530035_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Int3>::get_Item(System.Int32)
extern "C"  Int3_t1974045594  ReadOnlyCollection_1_get_Item_m622409375_gshared (ReadOnlyCollection_1_t3531123130 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m622409375(__this, ___index0, method) ((  Int3_t1974045594  (*) (ReadOnlyCollection_1_t3531123130 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m622409375_gshared)(__this, ___index0, method)
