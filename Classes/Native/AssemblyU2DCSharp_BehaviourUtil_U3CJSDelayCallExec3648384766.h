﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// BehaviourUtil/JSDelayFun1
struct JSDelayFun1_t1074711452;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BehaviourUtil/<JSDelayCallExec>c__Iterator3A
struct  U3CJSDelayCallExecU3Ec__Iterator3A_t3648384766  : public Il2CppObject
{
public:
	// System.UInt32 BehaviourUtil/<JSDelayCallExec>c__Iterator3A::<id>__0
	uint32_t ___U3CidU3E__0_0;
	// System.Single BehaviourUtil/<JSDelayCallExec>c__Iterator3A::delaytime
	float ___delaytime_1;
	// BehaviourUtil/JSDelayFun1 BehaviourUtil/<JSDelayCallExec>c__Iterator3A::act
	JSDelayFun1_t1074711452 * ___act_2;
	// System.Object BehaviourUtil/<JSDelayCallExec>c__Iterator3A::arg1
	Il2CppObject * ___arg1_3;
	// System.Int32 BehaviourUtil/<JSDelayCallExec>c__Iterator3A::$PC
	int32_t ___U24PC_4;
	// System.Object BehaviourUtil/<JSDelayCallExec>c__Iterator3A::$current
	Il2CppObject * ___U24current_5;
	// System.Single BehaviourUtil/<JSDelayCallExec>c__Iterator3A::<$>delaytime
	float ___U3CU24U3Edelaytime_6;
	// BehaviourUtil/JSDelayFun1 BehaviourUtil/<JSDelayCallExec>c__Iterator3A::<$>act
	JSDelayFun1_t1074711452 * ___U3CU24U3Eact_7;
	// System.Object BehaviourUtil/<JSDelayCallExec>c__Iterator3A::<$>arg1
	Il2CppObject * ___U3CU24U3Earg1_8;

public:
	inline static int32_t get_offset_of_U3CidU3E__0_0() { return static_cast<int32_t>(offsetof(U3CJSDelayCallExecU3Ec__Iterator3A_t3648384766, ___U3CidU3E__0_0)); }
	inline uint32_t get_U3CidU3E__0_0() const { return ___U3CidU3E__0_0; }
	inline uint32_t* get_address_of_U3CidU3E__0_0() { return &___U3CidU3E__0_0; }
	inline void set_U3CidU3E__0_0(uint32_t value)
	{
		___U3CidU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_delaytime_1() { return static_cast<int32_t>(offsetof(U3CJSDelayCallExecU3Ec__Iterator3A_t3648384766, ___delaytime_1)); }
	inline float get_delaytime_1() const { return ___delaytime_1; }
	inline float* get_address_of_delaytime_1() { return &___delaytime_1; }
	inline void set_delaytime_1(float value)
	{
		___delaytime_1 = value;
	}

	inline static int32_t get_offset_of_act_2() { return static_cast<int32_t>(offsetof(U3CJSDelayCallExecU3Ec__Iterator3A_t3648384766, ___act_2)); }
	inline JSDelayFun1_t1074711452 * get_act_2() const { return ___act_2; }
	inline JSDelayFun1_t1074711452 ** get_address_of_act_2() { return &___act_2; }
	inline void set_act_2(JSDelayFun1_t1074711452 * value)
	{
		___act_2 = value;
		Il2CppCodeGenWriteBarrier(&___act_2, value);
	}

	inline static int32_t get_offset_of_arg1_3() { return static_cast<int32_t>(offsetof(U3CJSDelayCallExecU3Ec__Iterator3A_t3648384766, ___arg1_3)); }
	inline Il2CppObject * get_arg1_3() const { return ___arg1_3; }
	inline Il2CppObject ** get_address_of_arg1_3() { return &___arg1_3; }
	inline void set_arg1_3(Il2CppObject * value)
	{
		___arg1_3 = value;
		Il2CppCodeGenWriteBarrier(&___arg1_3, value);
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CJSDelayCallExecU3Ec__Iterator3A_t3648384766, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CJSDelayCallExecU3Ec__Iterator3A_t3648384766, ___U24current_5)); }
	inline Il2CppObject * get_U24current_5() const { return ___U24current_5; }
	inline Il2CppObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(Il2CppObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_5, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Edelaytime_6() { return static_cast<int32_t>(offsetof(U3CJSDelayCallExecU3Ec__Iterator3A_t3648384766, ___U3CU24U3Edelaytime_6)); }
	inline float get_U3CU24U3Edelaytime_6() const { return ___U3CU24U3Edelaytime_6; }
	inline float* get_address_of_U3CU24U3Edelaytime_6() { return &___U3CU24U3Edelaytime_6; }
	inline void set_U3CU24U3Edelaytime_6(float value)
	{
		___U3CU24U3Edelaytime_6 = value;
	}

	inline static int32_t get_offset_of_U3CU24U3Eact_7() { return static_cast<int32_t>(offsetof(U3CJSDelayCallExecU3Ec__Iterator3A_t3648384766, ___U3CU24U3Eact_7)); }
	inline JSDelayFun1_t1074711452 * get_U3CU24U3Eact_7() const { return ___U3CU24U3Eact_7; }
	inline JSDelayFun1_t1074711452 ** get_address_of_U3CU24U3Eact_7() { return &___U3CU24U3Eact_7; }
	inline void set_U3CU24U3Eact_7(JSDelayFun1_t1074711452 * value)
	{
		___U3CU24U3Eact_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Eact_7, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Earg1_8() { return static_cast<int32_t>(offsetof(U3CJSDelayCallExecU3Ec__Iterator3A_t3648384766, ___U3CU24U3Earg1_8)); }
	inline Il2CppObject * get_U3CU24U3Earg1_8() const { return ___U3CU24U3Earg1_8; }
	inline Il2CppObject ** get_address_of_U3CU24U3Earg1_8() { return &___U3CU24U3Earg1_8; }
	inline void set_U3CU24U3Earg1_8(Il2CppObject * value)
	{
		___U3CU24U3Earg1_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Earg1_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
