﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PropertyBindingGenerated
struct PropertyBindingGenerated_t4226256031;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"

// System.Void PropertyBindingGenerated::.ctor()
extern "C"  void PropertyBindingGenerated__ctor_m3702834396 (PropertyBindingGenerated_t4226256031 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PropertyBindingGenerated::PropertyBinding_PropertyBinding1(JSVCall,System.Int32)
extern "C"  bool PropertyBindingGenerated_PropertyBinding_PropertyBinding1_m877745788 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PropertyBindingGenerated::PropertyBinding_source(JSVCall)
extern "C"  void PropertyBindingGenerated_PropertyBinding_source_m3436925557 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PropertyBindingGenerated::PropertyBinding_target(JSVCall)
extern "C"  void PropertyBindingGenerated_PropertyBinding_target_m646024639 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PropertyBindingGenerated::PropertyBinding_direction(JSVCall)
extern "C"  void PropertyBindingGenerated_PropertyBinding_direction_m646450685 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PropertyBindingGenerated::PropertyBinding_update(JSVCall)
extern "C"  void PropertyBindingGenerated_PropertyBinding_update_m3961546119 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PropertyBindingGenerated::PropertyBinding_editMode(JSVCall)
extern "C"  void PropertyBindingGenerated_PropertyBinding_editMode_m1619466627 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PropertyBindingGenerated::PropertyBinding_UpdateTarget(JSVCall,System.Int32)
extern "C"  bool PropertyBindingGenerated_PropertyBinding_UpdateTarget_m766650165 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PropertyBindingGenerated::__Register()
extern "C"  void PropertyBindingGenerated___Register_m1186029931 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PropertyBindingGenerated::ilo_addJSCSRel1(System.Int32,System.Object)
extern "C"  void PropertyBindingGenerated_ilo_addJSCSRel1_m1114381720 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PropertyBindingGenerated::ilo_getObject2(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * PropertyBindingGenerated_ilo_getObject2_m2635077780 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PropertyBindingGenerated::ilo_getEnum3(System.Int32)
extern "C"  int32_t PropertyBindingGenerated_ilo_getEnum3_m827486390 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PropertyBindingGenerated::ilo_setBooleanS4(System.Int32,System.Boolean)
extern "C"  void PropertyBindingGenerated_ilo_setBooleanS4_m2183898410 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PropertyBindingGenerated::ilo_getBooleanS5(System.Int32)
extern "C"  bool PropertyBindingGenerated_ilo_getBooleanS5_m2072310132 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
