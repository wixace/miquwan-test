﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_ReplayBase779703160.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ReplayGameSpeed
struct  ReplayGameSpeed_t283045678  : public ReplayBase_t779703160
{
public:
	// System.Single ReplayGameSpeed::gameSpeed
	float ___gameSpeed_8;

public:
	inline static int32_t get_offset_of_gameSpeed_8() { return static_cast<int32_t>(offsetof(ReplayGameSpeed_t283045678, ___gameSpeed_8)); }
	inline float get_gameSpeed_8() const { return ___gameSpeed_8; }
	inline float* get_address_of_gameSpeed_8() { return &___gameSpeed_8; }
	inline void set_gameSpeed_8(float value)
	{
		___gameSpeed_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
