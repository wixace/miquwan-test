﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AstarPath/<CalculatePaths>c__IteratorD
struct U3CCalculatePathsU3Ec__IteratorD_t1648267364;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void AstarPath/<CalculatePaths>c__IteratorD::.ctor()
extern "C"  void U3CCalculatePathsU3Ec__IteratorD__ctor_m233088183 (U3CCalculatePathsU3Ec__IteratorD_t1648267364 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object AstarPath/<CalculatePaths>c__IteratorD::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CCalculatePathsU3Ec__IteratorD_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3854003653 (U3CCalculatePathsU3Ec__IteratorD_t1648267364 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object AstarPath/<CalculatePaths>c__IteratorD::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCalculatePathsU3Ec__IteratorD_System_Collections_IEnumerator_get_Current_m3091176281 (U3CCalculatePathsU3Ec__IteratorD_t1648267364 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AstarPath/<CalculatePaths>c__IteratorD::MoveNext()
extern "C"  bool U3CCalculatePathsU3Ec__IteratorD_MoveNext_m5774021 (U3CCalculatePathsU3Ec__IteratorD_t1648267364 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AstarPath/<CalculatePaths>c__IteratorD::Dispose()
extern "C"  void U3CCalculatePathsU3Ec__IteratorD_Dispose_m557727540 (U3CCalculatePathsU3Ec__IteratorD_t1648267364 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AstarPath/<CalculatePaths>c__IteratorD::Reset()
extern "C"  void U3CCalculatePathsU3Ec__IteratorD_Reset_m2174488420 (U3CCalculatePathsU3Ec__IteratorD_t1648267364 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
