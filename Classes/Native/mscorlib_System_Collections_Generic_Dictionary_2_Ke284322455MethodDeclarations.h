﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/KeyCollection<UIModelDisplayType,System.Int32>
struct KeyCollection_t284322455;
// System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>
struct Dictionary_2_t2952530300;
// System.Collections.Generic.IEnumerator`1<UIModelDisplayType>
struct IEnumerator_1_t3803617728;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// UIModelDisplayType[]
struct UIModelDisplayTypeU5BU5D_t737309086;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_UIModelDisplayType1891752679.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K3567466354.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<UIModelDisplayType,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void KeyCollection__ctor_m3842608940_gshared (KeyCollection_t284322455 * __this, Dictionary_2_t2952530300 * ___dictionary0, const MethodInfo* method);
#define KeyCollection__ctor_m3842608940(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t284322455 *, Dictionary_2_t2952530300 *, const MethodInfo*))KeyCollection__ctor_m3842608940_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<UIModelDisplayType,System.Int32>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m2119302826_gshared (KeyCollection_t284322455 * __this, int32_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m2119302826(__this, ___item0, method) ((  void (*) (KeyCollection_t284322455 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m2119302826_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<UIModelDisplayType,System.Int32>::System.Collections.Generic.ICollection<TKey>.Clear()
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3984311969_gshared (KeyCollection_t284322455 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3984311969(__this, method) ((  void (*) (KeyCollection_t284322455 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3984311969_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<UIModelDisplayType,System.Int32>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1793948576_gshared (KeyCollection_t284322455 * __this, int32_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1793948576(__this, ___item0, method) ((  bool (*) (KeyCollection_t284322455 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1793948576_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<UIModelDisplayType,System.Int32>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3819197701_gshared (KeyCollection_t284322455 * __this, int32_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3819197701(__this, ___item0, method) ((  bool (*) (KeyCollection_t284322455 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3819197701_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<UIModelDisplayType,System.Int32>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1673800925_gshared (KeyCollection_t284322455 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1673800925(__this, method) ((  Il2CppObject* (*) (KeyCollection_t284322455 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1673800925_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<UIModelDisplayType,System.Int32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m2764816787_gshared (KeyCollection_t284322455 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_CopyTo_m2764816787(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t284322455 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m2764816787_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<UIModelDisplayType,System.Int32>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1825653966_gshared (KeyCollection_t284322455 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1825653966(__this, method) ((  Il2CppObject * (*) (KeyCollection_t284322455 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1825653966_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<UIModelDisplayType,System.Int32>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m288787201_gshared (KeyCollection_t284322455 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m288787201(__this, method) ((  bool (*) (KeyCollection_t284322455 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m288787201_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<UIModelDisplayType,System.Int32>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1597583603_gshared (KeyCollection_t284322455 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1597583603(__this, method) ((  bool (*) (KeyCollection_t284322455 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1597583603_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<UIModelDisplayType,System.Int32>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m1105465183_gshared (KeyCollection_t284322455 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m1105465183(__this, method) ((  Il2CppObject * (*) (KeyCollection_t284322455 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m1105465183_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<UIModelDisplayType,System.Int32>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m2526979553_gshared (KeyCollection_t284322455 * __this, UIModelDisplayTypeU5BU5D_t737309086* ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_CopyTo_m2526979553(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t284322455 *, UIModelDisplayTypeU5BU5D_t737309086*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m2526979553_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<UIModelDisplayType,System.Int32>::GetEnumerator()
extern "C"  Enumerator_t3567466354  KeyCollection_GetEnumerator_m3406649284_gshared (KeyCollection_t284322455 * __this, const MethodInfo* method);
#define KeyCollection_GetEnumerator_m3406649284(__this, method) ((  Enumerator_t3567466354  (*) (KeyCollection_t284322455 *, const MethodInfo*))KeyCollection_GetEnumerator_m3406649284_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<UIModelDisplayType,System.Int32>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m3378815673_gshared (KeyCollection_t284322455 * __this, const MethodInfo* method);
#define KeyCollection_get_Count_m3378815673(__this, method) ((  int32_t (*) (KeyCollection_t284322455 *, const MethodInfo*))KeyCollection_get_Count_m3378815673_gshared)(__this, method)
