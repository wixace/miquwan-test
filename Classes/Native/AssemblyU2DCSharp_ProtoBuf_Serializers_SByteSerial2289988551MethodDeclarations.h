﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ProtoBuf.Serializers.SByteSerializer
struct SByteSerializer_t2289988551;
// ProtoBuf.Meta.TypeModel
struct TypeModel_t2730011105;
// System.Type
struct Type_t;
// System.Object
struct Il2CppObject;
// ProtoBuf.ProtoReader
struct ProtoReader_t3962509489;
// ProtoBuf.ProtoWriter
struct ProtoWriter_t4117914721;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ProtoBuf_Meta_TypeModel2730011105.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_ProtoBuf_ProtoReader3962509489.h"
#include "AssemblyU2DCSharp_ProtoBuf_ProtoWriter4117914721.h"

// System.Void ProtoBuf.Serializers.SByteSerializer::.ctor(ProtoBuf.Meta.TypeModel)
extern "C"  void SByteSerializer__ctor_m3596341131 (SByteSerializer_t2289988551 * __this, TypeModel_t2730011105 * ___model0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Serializers.SByteSerializer::.cctor()
extern "C"  void SByteSerializer__cctor_m741554511 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Serializers.SByteSerializer::ProtoBuf.Serializers.IProtoSerializer.get_RequiresOldValue()
extern "C"  bool SByteSerializer_ProtoBuf_Serializers_IProtoSerializer_get_RequiresOldValue_m4270522344 (SByteSerializer_t2289988551 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Serializers.SByteSerializer::ProtoBuf.Serializers.IProtoSerializer.get_ReturnsValue()
extern "C"  bool SByteSerializer_ProtoBuf_Serializers_IProtoSerializer_get_ReturnsValue_m1918051966 (SByteSerializer_t2289988551 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type ProtoBuf.Serializers.SByteSerializer::get_ExpectedType()
extern "C"  Type_t * SByteSerializer_get_ExpectedType_m3977615003 (SByteSerializer_t2289988551 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProtoBuf.Serializers.SByteSerializer::Read(System.Object,ProtoBuf.ProtoReader)
extern "C"  Il2CppObject * SByteSerializer_Read_m1564363163 (SByteSerializer_t2289988551 * __this, Il2CppObject * ___value0, ProtoReader_t3962509489 * ___source1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Serializers.SByteSerializer::Write(System.Object,ProtoBuf.ProtoWriter)
extern "C"  void SByteSerializer_Write_m1050613781 (SByteSerializer_t2289988551 * __this, Il2CppObject * ___value0, ProtoWriter_t4117914721 * ___dest1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Serializers.SByteSerializer::ilo_WriteSByte1(System.SByte,ProtoBuf.ProtoWriter)
extern "C"  void SByteSerializer_ilo_WriteSByte1_m3955827788 (Il2CppObject * __this /* static, unused */, int8_t ___value0, ProtoWriter_t4117914721 * ___writer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
