﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TwistGesture
struct TwistGesture_t4198361154;
// FingerGestures/Finger
struct Finger_t182428197;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_FingerGestures_Finger182428197.h"

// System.Void TwistGesture::.ctor()
extern "C"  void TwistGesture__ctor_m1268659353 (TwistGesture_t4198361154 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TwistGesture::get_DeltaRotation()
extern "C"  float TwistGesture_get_DeltaRotation_m742639704 (TwistGesture_t4198361154 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TwistGesture::set_DeltaRotation(System.Single)
extern "C"  void TwistGesture_set_DeltaRotation_m1877470971 (TwistGesture_t4198361154 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TwistGesture::get_TotalRotation()
extern "C"  float TwistGesture_get_TotalRotation_m3831556324 (TwistGesture_t4198361154 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TwistGesture::set_TotalRotation(System.Single)
extern "C"  void TwistGesture_set_TotalRotation_m3446215151 (TwistGesture_t4198361154 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FingerGestures/Finger TwistGesture::get_Pivot()
extern "C"  Finger_t182428197 * TwistGesture_get_Pivot_m787295746 (TwistGesture_t4198361154 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TwistGesture::set_Pivot(FingerGestures/Finger)
extern "C"  void TwistGesture_set_Pivot_m1986426129 (TwistGesture_t4198361154 * __this, Finger_t182428197 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
