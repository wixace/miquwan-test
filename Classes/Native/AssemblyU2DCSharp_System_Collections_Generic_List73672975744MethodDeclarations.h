﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System_Collections_Generic_List77Generated/<ListA1_FindLastIndex_GetDelegate_member19_arg2>c__AnonStorey8B`1<System.Object>
struct U3CListA1_FindLastIndex_GetDelegate_member19_arg2U3Ec__AnonStorey8B_1_t3672975744;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System_Collections_Generic_List77Generated/<ListA1_FindLastIndex_GetDelegate_member19_arg2>c__AnonStorey8B`1<System.Object>::.ctor()
extern "C"  void U3CListA1_FindLastIndex_GetDelegate_member19_arg2U3Ec__AnonStorey8B_1__ctor_m864804951_gshared (U3CListA1_FindLastIndex_GetDelegate_member19_arg2U3Ec__AnonStorey8B_1_t3672975744 * __this, const MethodInfo* method);
#define U3CListA1_FindLastIndex_GetDelegate_member19_arg2U3Ec__AnonStorey8B_1__ctor_m864804951(__this, method) ((  void (*) (U3CListA1_FindLastIndex_GetDelegate_member19_arg2U3Ec__AnonStorey8B_1_t3672975744 *, const MethodInfo*))U3CListA1_FindLastIndex_GetDelegate_member19_arg2U3Ec__AnonStorey8B_1__ctor_m864804951_gshared)(__this, method)
// System.Boolean System_Collections_Generic_List77Generated/<ListA1_FindLastIndex_GetDelegate_member19_arg2>c__AnonStorey8B`1<System.Object>::<>m__B0(T)
extern "C"  bool U3CListA1_FindLastIndex_GetDelegate_member19_arg2U3Ec__AnonStorey8B_1_U3CU3Em__B0_m699080668_gshared (U3CListA1_FindLastIndex_GetDelegate_member19_arg2U3Ec__AnonStorey8B_1_t3672975744 * __this, Il2CppObject * ___obj0, const MethodInfo* method);
#define U3CListA1_FindLastIndex_GetDelegate_member19_arg2U3Ec__AnonStorey8B_1_U3CU3Em__B0_m699080668(__this, ___obj0, method) ((  bool (*) (U3CListA1_FindLastIndex_GetDelegate_member19_arg2U3Ec__AnonStorey8B_1_t3672975744 *, Il2CppObject *, const MethodInfo*))U3CListA1_FindLastIndex_GetDelegate_member19_arg2U3Ec__AnonStorey8B_1_U3CU3Em__B0_m699080668_gshared)(__this, ___obj0, method)
