﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_JointTranslationLimits2DGenerated
struct UnityEngine_JointTranslationLimits2DGenerated_t2188064470;
// JSVCall
struct JSVCall_t3708497963;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"

// System.Void UnityEngine_JointTranslationLimits2DGenerated::.ctor()
extern "C"  void UnityEngine_JointTranslationLimits2DGenerated__ctor_m3054689109 (UnityEngine_JointTranslationLimits2DGenerated_t2188064470 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_JointTranslationLimits2DGenerated::.cctor()
extern "C"  void UnityEngine_JointTranslationLimits2DGenerated__cctor_m4018952952 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_JointTranslationLimits2DGenerated::JointTranslationLimits2D_JointTranslationLimits2D1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_JointTranslationLimits2DGenerated_JointTranslationLimits2D_JointTranslationLimits2D1_m1417281693 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_JointTranslationLimits2DGenerated::JointTranslationLimits2D_min(JSVCall)
extern "C"  void UnityEngine_JointTranslationLimits2DGenerated_JointTranslationLimits2D_min_m372526996 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_JointTranslationLimits2DGenerated::JointTranslationLimits2D_max(JSVCall)
extern "C"  void UnityEngine_JointTranslationLimits2DGenerated_JointTranslationLimits2D_max_m4193068226 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_JointTranslationLimits2DGenerated::__Register()
extern "C"  void UnityEngine_JointTranslationLimits2DGenerated___Register_m4046155410 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_JointTranslationLimits2DGenerated::ilo_attachFinalizerObject1(System.Int32)
extern "C"  bool UnityEngine_JointTranslationLimits2DGenerated_ilo_attachFinalizerObject1_m2841435202 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_JointTranslationLimits2DGenerated::ilo_setSingle2(System.Int32,System.Single)
extern "C"  void UnityEngine_JointTranslationLimits2DGenerated_ilo_setSingle2_m2564553088 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
