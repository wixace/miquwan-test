﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "AssemblyU2DCSharp_Entity_Behavior_IBehavior770859129.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entity.Behavior.MoveBvr
struct  MoveBvr_t3321147243  : public IBehavior_t770859129
{
public:
	// UnityEngine.Vector3 Entity.Behavior.MoveBvr::targetPos
	Vector3_t4282066566  ___targetPos_3;
	// System.Single Entity.Behavior.MoveBvr::curTime
	float ___curTime_4;
	// System.Single Entity.Behavior.MoveBvr::distance
	float ___distance_5;
	// System.Single Entity.Behavior.MoveBvr::intervalTime
	float ___intervalTime_6;

public:
	inline static int32_t get_offset_of_targetPos_3() { return static_cast<int32_t>(offsetof(MoveBvr_t3321147243, ___targetPos_3)); }
	inline Vector3_t4282066566  get_targetPos_3() const { return ___targetPos_3; }
	inline Vector3_t4282066566 * get_address_of_targetPos_3() { return &___targetPos_3; }
	inline void set_targetPos_3(Vector3_t4282066566  value)
	{
		___targetPos_3 = value;
	}

	inline static int32_t get_offset_of_curTime_4() { return static_cast<int32_t>(offsetof(MoveBvr_t3321147243, ___curTime_4)); }
	inline float get_curTime_4() const { return ___curTime_4; }
	inline float* get_address_of_curTime_4() { return &___curTime_4; }
	inline void set_curTime_4(float value)
	{
		___curTime_4 = value;
	}

	inline static int32_t get_offset_of_distance_5() { return static_cast<int32_t>(offsetof(MoveBvr_t3321147243, ___distance_5)); }
	inline float get_distance_5() const { return ___distance_5; }
	inline float* get_address_of_distance_5() { return &___distance_5; }
	inline void set_distance_5(float value)
	{
		___distance_5 = value;
	}

	inline static int32_t get_offset_of_intervalTime_6() { return static_cast<int32_t>(offsetof(MoveBvr_t3321147243, ___intervalTime_6)); }
	inline float get_intervalTime_6() const { return ___intervalTime_6; }
	inline float* get_address_of_intervalTime_6() { return &___intervalTime_6; }
	inline void set_intervalTime_6(float value)
	{
		___intervalTime_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
