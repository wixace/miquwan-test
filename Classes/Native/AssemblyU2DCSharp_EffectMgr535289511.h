﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.Transform
struct Transform_t1659122786;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Collections.Generic.List`1<EffectCtrl>>
struct Dictionary_2_t779269139;
// System.Collections.Generic.List`1<EffectCtrl>
struct List_1_t782005900;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t747900261;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EffectMgr
struct  EffectMgr_t535289511  : public Il2CppObject
{
public:
	// UnityEngine.Transform EffectMgr::_effectParent
	Transform_t1659122786 * ____effectParent_2;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Collections.Generic.List`1<EffectCtrl>> EffectMgr::effectCacheDic
	Dictionary_2_t779269139 * ___effectCacheDic_3;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Collections.Generic.List`1<EffectCtrl>> EffectMgr::entityEffDic
	Dictionary_2_t779269139 * ___entityEffDic_4;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Collections.Generic.List`1<EffectCtrl>> EffectMgr::entityAllEffDic
	Dictionary_2_t779269139 * ___entityAllEffDic_5;
	// System.Collections.Generic.List`1<EffectCtrl> EffectMgr::allInBlackEffect
	List_1_t782005900 * ___allInBlackEffect_6;
	// System.Int32 EffectMgr::pauseSrcID
	int32_t ___pauseSrcID_7;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> EffectMgr::sceneShowEffectList
	List_1_t747900261 * ___sceneShowEffectList_8;

public:
	inline static int32_t get_offset_of__effectParent_2() { return static_cast<int32_t>(offsetof(EffectMgr_t535289511, ____effectParent_2)); }
	inline Transform_t1659122786 * get__effectParent_2() const { return ____effectParent_2; }
	inline Transform_t1659122786 ** get_address_of__effectParent_2() { return &____effectParent_2; }
	inline void set__effectParent_2(Transform_t1659122786 * value)
	{
		____effectParent_2 = value;
		Il2CppCodeGenWriteBarrier(&____effectParent_2, value);
	}

	inline static int32_t get_offset_of_effectCacheDic_3() { return static_cast<int32_t>(offsetof(EffectMgr_t535289511, ___effectCacheDic_3)); }
	inline Dictionary_2_t779269139 * get_effectCacheDic_3() const { return ___effectCacheDic_3; }
	inline Dictionary_2_t779269139 ** get_address_of_effectCacheDic_3() { return &___effectCacheDic_3; }
	inline void set_effectCacheDic_3(Dictionary_2_t779269139 * value)
	{
		___effectCacheDic_3 = value;
		Il2CppCodeGenWriteBarrier(&___effectCacheDic_3, value);
	}

	inline static int32_t get_offset_of_entityEffDic_4() { return static_cast<int32_t>(offsetof(EffectMgr_t535289511, ___entityEffDic_4)); }
	inline Dictionary_2_t779269139 * get_entityEffDic_4() const { return ___entityEffDic_4; }
	inline Dictionary_2_t779269139 ** get_address_of_entityEffDic_4() { return &___entityEffDic_4; }
	inline void set_entityEffDic_4(Dictionary_2_t779269139 * value)
	{
		___entityEffDic_4 = value;
		Il2CppCodeGenWriteBarrier(&___entityEffDic_4, value);
	}

	inline static int32_t get_offset_of_entityAllEffDic_5() { return static_cast<int32_t>(offsetof(EffectMgr_t535289511, ___entityAllEffDic_5)); }
	inline Dictionary_2_t779269139 * get_entityAllEffDic_5() const { return ___entityAllEffDic_5; }
	inline Dictionary_2_t779269139 ** get_address_of_entityAllEffDic_5() { return &___entityAllEffDic_5; }
	inline void set_entityAllEffDic_5(Dictionary_2_t779269139 * value)
	{
		___entityAllEffDic_5 = value;
		Il2CppCodeGenWriteBarrier(&___entityAllEffDic_5, value);
	}

	inline static int32_t get_offset_of_allInBlackEffect_6() { return static_cast<int32_t>(offsetof(EffectMgr_t535289511, ___allInBlackEffect_6)); }
	inline List_1_t782005900 * get_allInBlackEffect_6() const { return ___allInBlackEffect_6; }
	inline List_1_t782005900 ** get_address_of_allInBlackEffect_6() { return &___allInBlackEffect_6; }
	inline void set_allInBlackEffect_6(List_1_t782005900 * value)
	{
		___allInBlackEffect_6 = value;
		Il2CppCodeGenWriteBarrier(&___allInBlackEffect_6, value);
	}

	inline static int32_t get_offset_of_pauseSrcID_7() { return static_cast<int32_t>(offsetof(EffectMgr_t535289511, ___pauseSrcID_7)); }
	inline int32_t get_pauseSrcID_7() const { return ___pauseSrcID_7; }
	inline int32_t* get_address_of_pauseSrcID_7() { return &___pauseSrcID_7; }
	inline void set_pauseSrcID_7(int32_t value)
	{
		___pauseSrcID_7 = value;
	}

	inline static int32_t get_offset_of_sceneShowEffectList_8() { return static_cast<int32_t>(offsetof(EffectMgr_t535289511, ___sceneShowEffectList_8)); }
	inline List_1_t747900261 * get_sceneShowEffectList_8() const { return ___sceneShowEffectList_8; }
	inline List_1_t747900261 ** get_address_of_sceneShowEffectList_8() { return &___sceneShowEffectList_8; }
	inline void set_sceneShowEffectList_8(List_1_t747900261 * value)
	{
		___sceneShowEffectList_8 = value;
		Il2CppCodeGenWriteBarrier(&___sceneShowEffectList_8, value);
	}
};

struct EffectMgr_t535289511_StaticFields
{
public:
	// System.String EffectMgr::REMOVE_AL_EFF
	String_t* ___REMOVE_AL_EFF_1;

public:
	inline static int32_t get_offset_of_REMOVE_AL_EFF_1() { return static_cast<int32_t>(offsetof(EffectMgr_t535289511_StaticFields, ___REMOVE_AL_EFF_1)); }
	inline String_t* get_REMOVE_AL_EFF_1() const { return ___REMOVE_AL_EFF_1; }
	inline String_t** get_address_of_REMOVE_AL_EFF_1() { return &___REMOVE_AL_EFF_1; }
	inline void set_REMOVE_AL_EFF_1(String_t* value)
	{
		___REMOVE_AL_EFF_1 = value;
		Il2CppCodeGenWriteBarrier(&___REMOVE_AL_EFF_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
