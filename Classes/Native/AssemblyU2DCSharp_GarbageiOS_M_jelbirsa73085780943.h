﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_jelbirsa7
struct  M_jelbirsa7_t3085780943  : public Il2CppObject
{
public:
	// System.UInt32 GarbageiOS.M_jelbirsa7::_retriSader
	uint32_t ____retriSader_0;
	// System.Int32 GarbageiOS.M_jelbirsa7::_faseBesall
	int32_t ____faseBesall_1;
	// System.Single GarbageiOS.M_jelbirsa7::_menaiBezasou
	float ____menaiBezasou_2;
	// System.UInt32 GarbageiOS.M_jelbirsa7::_raldeamurBolea
	uint32_t ____raldeamurBolea_3;
	// System.UInt32 GarbageiOS.M_jelbirsa7::_firismooYubel
	uint32_t ____firismooYubel_4;
	// System.Int32 GarbageiOS.M_jelbirsa7::_sarnouKocou
	int32_t ____sarnouKocou_5;
	// System.Boolean GarbageiOS.M_jelbirsa7::_jolefis
	bool ____jolefis_6;
	// System.String GarbageiOS.M_jelbirsa7::_porayBairwoo
	String_t* ____porayBairwoo_7;

public:
	inline static int32_t get_offset_of__retriSader_0() { return static_cast<int32_t>(offsetof(M_jelbirsa7_t3085780943, ____retriSader_0)); }
	inline uint32_t get__retriSader_0() const { return ____retriSader_0; }
	inline uint32_t* get_address_of__retriSader_0() { return &____retriSader_0; }
	inline void set__retriSader_0(uint32_t value)
	{
		____retriSader_0 = value;
	}

	inline static int32_t get_offset_of__faseBesall_1() { return static_cast<int32_t>(offsetof(M_jelbirsa7_t3085780943, ____faseBesall_1)); }
	inline int32_t get__faseBesall_1() const { return ____faseBesall_1; }
	inline int32_t* get_address_of__faseBesall_1() { return &____faseBesall_1; }
	inline void set__faseBesall_1(int32_t value)
	{
		____faseBesall_1 = value;
	}

	inline static int32_t get_offset_of__menaiBezasou_2() { return static_cast<int32_t>(offsetof(M_jelbirsa7_t3085780943, ____menaiBezasou_2)); }
	inline float get__menaiBezasou_2() const { return ____menaiBezasou_2; }
	inline float* get_address_of__menaiBezasou_2() { return &____menaiBezasou_2; }
	inline void set__menaiBezasou_2(float value)
	{
		____menaiBezasou_2 = value;
	}

	inline static int32_t get_offset_of__raldeamurBolea_3() { return static_cast<int32_t>(offsetof(M_jelbirsa7_t3085780943, ____raldeamurBolea_3)); }
	inline uint32_t get__raldeamurBolea_3() const { return ____raldeamurBolea_3; }
	inline uint32_t* get_address_of__raldeamurBolea_3() { return &____raldeamurBolea_3; }
	inline void set__raldeamurBolea_3(uint32_t value)
	{
		____raldeamurBolea_3 = value;
	}

	inline static int32_t get_offset_of__firismooYubel_4() { return static_cast<int32_t>(offsetof(M_jelbirsa7_t3085780943, ____firismooYubel_4)); }
	inline uint32_t get__firismooYubel_4() const { return ____firismooYubel_4; }
	inline uint32_t* get_address_of__firismooYubel_4() { return &____firismooYubel_4; }
	inline void set__firismooYubel_4(uint32_t value)
	{
		____firismooYubel_4 = value;
	}

	inline static int32_t get_offset_of__sarnouKocou_5() { return static_cast<int32_t>(offsetof(M_jelbirsa7_t3085780943, ____sarnouKocou_5)); }
	inline int32_t get__sarnouKocou_5() const { return ____sarnouKocou_5; }
	inline int32_t* get_address_of__sarnouKocou_5() { return &____sarnouKocou_5; }
	inline void set__sarnouKocou_5(int32_t value)
	{
		____sarnouKocou_5 = value;
	}

	inline static int32_t get_offset_of__jolefis_6() { return static_cast<int32_t>(offsetof(M_jelbirsa7_t3085780943, ____jolefis_6)); }
	inline bool get__jolefis_6() const { return ____jolefis_6; }
	inline bool* get_address_of__jolefis_6() { return &____jolefis_6; }
	inline void set__jolefis_6(bool value)
	{
		____jolefis_6 = value;
	}

	inline static int32_t get_offset_of__porayBairwoo_7() { return static_cast<int32_t>(offsetof(M_jelbirsa7_t3085780943, ____porayBairwoo_7)); }
	inline String_t* get__porayBairwoo_7() const { return ____porayBairwoo_7; }
	inline String_t** get_address_of__porayBairwoo_7() { return &____porayBairwoo_7; }
	inline void set__porayBairwoo_7(String_t* value)
	{
		____porayBairwoo_7 = value;
		Il2CppCodeGenWriteBarrier(&____porayBairwoo_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
