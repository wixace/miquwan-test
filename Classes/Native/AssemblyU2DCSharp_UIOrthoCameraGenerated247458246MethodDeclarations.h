﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIOrthoCameraGenerated
struct UIOrthoCameraGenerated_t247458246;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void UIOrthoCameraGenerated::.ctor()
extern "C"  void UIOrthoCameraGenerated__ctor_m2145380757 (UIOrthoCameraGenerated_t247458246 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIOrthoCameraGenerated::UIOrthoCamera_UIOrthoCamera1(JSVCall,System.Int32)
extern "C"  bool UIOrthoCameraGenerated_UIOrthoCamera_UIOrthoCamera1_m1211968497 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIOrthoCameraGenerated::__Register()
extern "C"  void UIOrthoCameraGenerated___Register_m2425178194 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIOrthoCameraGenerated::ilo_addJSCSRel1(System.Int32,System.Object)
extern "C"  void UIOrthoCameraGenerated_ilo_addJSCSRel1_m3122469329 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
