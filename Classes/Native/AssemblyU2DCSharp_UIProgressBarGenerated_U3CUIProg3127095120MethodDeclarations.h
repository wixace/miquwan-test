﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIProgressBarGenerated/<UIProgressBar_onDragFinished_GetDelegate_member1_arg0>c__AnonStoreyCA
struct U3CUIProgressBar_onDragFinished_GetDelegate_member1_arg0U3Ec__AnonStoreyCA_t3127095120;

#include "codegen/il2cpp-codegen.h"

// System.Void UIProgressBarGenerated/<UIProgressBar_onDragFinished_GetDelegate_member1_arg0>c__AnonStoreyCA::.ctor()
extern "C"  void U3CUIProgressBar_onDragFinished_GetDelegate_member1_arg0U3Ec__AnonStoreyCA__ctor_m260365339 (U3CUIProgressBar_onDragFinished_GetDelegate_member1_arg0U3Ec__AnonStoreyCA_t3127095120 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIProgressBarGenerated/<UIProgressBar_onDragFinished_GetDelegate_member1_arg0>c__AnonStoreyCA::<>m__159()
extern "C"  void U3CUIProgressBar_onDragFinished_GetDelegate_member1_arg0U3Ec__AnonStoreyCA_U3CU3Em__159_m2662805395 (U3CUIProgressBar_onDragFinished_GetDelegate_member1_arg0U3Ec__AnonStoreyCA_t3127095120 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
