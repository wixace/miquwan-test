﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Dictionary`2<System.Int32,StaticParticle>
struct Dictionary_2_t2203802811;

#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_GameQuality2185543821.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameQutilyCtr
struct  GameQutilyCtr_t3963256169  : public Il2CppObject
{
public:
	// System.Int32 GameQutilyCtr::screenInitWidth
	int32_t ___screenInitWidth_0;
	// System.Int32 GameQutilyCtr::screenInitHeight
	int32_t ___screenInitHeight_1;
	// System.Collections.Generic.Dictionary`2<System.Int32,StaticParticle> GameQutilyCtr::staticParticleDic
	Dictionary_2_t2203802811 * ___staticParticleDic_2;
	// System.Int32 GameQutilyCtr::guiId
	int32_t ___guiId_3;
	// GameQuality GameQutilyCtr::<gameQuality>k__BackingField
	int32_t ___U3CgameQualityU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_screenInitWidth_0() { return static_cast<int32_t>(offsetof(GameQutilyCtr_t3963256169, ___screenInitWidth_0)); }
	inline int32_t get_screenInitWidth_0() const { return ___screenInitWidth_0; }
	inline int32_t* get_address_of_screenInitWidth_0() { return &___screenInitWidth_0; }
	inline void set_screenInitWidth_0(int32_t value)
	{
		___screenInitWidth_0 = value;
	}

	inline static int32_t get_offset_of_screenInitHeight_1() { return static_cast<int32_t>(offsetof(GameQutilyCtr_t3963256169, ___screenInitHeight_1)); }
	inline int32_t get_screenInitHeight_1() const { return ___screenInitHeight_1; }
	inline int32_t* get_address_of_screenInitHeight_1() { return &___screenInitHeight_1; }
	inline void set_screenInitHeight_1(int32_t value)
	{
		___screenInitHeight_1 = value;
	}

	inline static int32_t get_offset_of_staticParticleDic_2() { return static_cast<int32_t>(offsetof(GameQutilyCtr_t3963256169, ___staticParticleDic_2)); }
	inline Dictionary_2_t2203802811 * get_staticParticleDic_2() const { return ___staticParticleDic_2; }
	inline Dictionary_2_t2203802811 ** get_address_of_staticParticleDic_2() { return &___staticParticleDic_2; }
	inline void set_staticParticleDic_2(Dictionary_2_t2203802811 * value)
	{
		___staticParticleDic_2 = value;
		Il2CppCodeGenWriteBarrier(&___staticParticleDic_2, value);
	}

	inline static int32_t get_offset_of_guiId_3() { return static_cast<int32_t>(offsetof(GameQutilyCtr_t3963256169, ___guiId_3)); }
	inline int32_t get_guiId_3() const { return ___guiId_3; }
	inline int32_t* get_address_of_guiId_3() { return &___guiId_3; }
	inline void set_guiId_3(int32_t value)
	{
		___guiId_3 = value;
	}

	inline static int32_t get_offset_of_U3CgameQualityU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(GameQutilyCtr_t3963256169, ___U3CgameQualityU3Ek__BackingField_4)); }
	inline int32_t get_U3CgameQualityU3Ek__BackingField_4() const { return ___U3CgameQualityU3Ek__BackingField_4; }
	inline int32_t* get_address_of_U3CgameQualityU3Ek__BackingField_4() { return &___U3CgameQualityU3Ek__BackingField_4; }
	inline void set_U3CgameQualityU3Ek__BackingField_4(int32_t value)
	{
		___U3CgameQualityU3Ek__BackingField_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
