﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PluginPush/NotificationData
struct NotificationData_t3289515031;

#include "codegen/il2cpp-codegen.h"

// System.Void PluginPush/NotificationData::.ctor()
extern "C"  void NotificationData__ctor_m1233408308 (NotificationData_t3289515031 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
