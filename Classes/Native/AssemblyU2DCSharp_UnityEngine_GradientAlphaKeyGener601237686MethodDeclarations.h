﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_GradientAlphaKeyGenerated
struct UnityEngine_GradientAlphaKeyGenerated_t601237686;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void UnityEngine_GradientAlphaKeyGenerated::.ctor()
extern "C"  void UnityEngine_GradientAlphaKeyGenerated__ctor_m692415861 (UnityEngine_GradientAlphaKeyGenerated_t601237686 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GradientAlphaKeyGenerated::.cctor()
extern "C"  void UnityEngine_GradientAlphaKeyGenerated__cctor_m3802926296 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GradientAlphaKeyGenerated::GradientAlphaKey_GradientAlphaKey1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GradientAlphaKeyGenerated_GradientAlphaKey_GradientAlphaKey1_m1580564605 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GradientAlphaKeyGenerated::GradientAlphaKey_GradientAlphaKey2(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GradientAlphaKeyGenerated_GradientAlphaKey_GradientAlphaKey2_m2825329086 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GradientAlphaKeyGenerated::GradientAlphaKey_alpha(JSVCall)
extern "C"  void UnityEngine_GradientAlphaKeyGenerated_GradientAlphaKey_alpha_m3722977288 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GradientAlphaKeyGenerated::GradientAlphaKey_time(JSVCall)
extern "C"  void UnityEngine_GradientAlphaKeyGenerated_GradientAlphaKey_time_m3803378489 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GradientAlphaKeyGenerated::__Register()
extern "C"  void UnityEngine_GradientAlphaKeyGenerated___Register_m123678834 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_GradientAlphaKeyGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UnityEngine_GradientAlphaKeyGenerated_ilo_getObject1_m3707883105 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GradientAlphaKeyGenerated::ilo_addJSCSRel2(System.Int32,System.Object)
extern "C"  void UnityEngine_GradientAlphaKeyGenerated_ilo_addJSCSRel2_m2456162866 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GradientAlphaKeyGenerated::ilo_attachFinalizerObject3(System.Int32)
extern "C"  bool UnityEngine_GradientAlphaKeyGenerated_ilo_attachFinalizerObject3_m2038263460 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine_GradientAlphaKeyGenerated::ilo_getSingle4(System.Int32)
extern "C"  float UnityEngine_GradientAlphaKeyGenerated_ilo_getSingle4_m3607227773 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
