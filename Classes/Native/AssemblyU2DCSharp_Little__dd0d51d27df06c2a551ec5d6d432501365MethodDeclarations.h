﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._dd0d51d27df06c2a551ec5d6d2b2a7ba
struct _dd0d51d27df06c2a551ec5d6d2b2a7ba_t432501365;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__dd0d51d27df06c2a551ec5d6d432501365.h"

// System.Void Little._dd0d51d27df06c2a551ec5d6d2b2a7ba::.ctor()
extern "C"  void _dd0d51d27df06c2a551ec5d6d2b2a7ba__ctor_m197961560 (_dd0d51d27df06c2a551ec5d6d2b2a7ba_t432501365 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._dd0d51d27df06c2a551ec5d6d2b2a7ba::_dd0d51d27df06c2a551ec5d6d2b2a7bam2(System.Int32)
extern "C"  int32_t _dd0d51d27df06c2a551ec5d6d2b2a7ba__dd0d51d27df06c2a551ec5d6d2b2a7bam2_m2254099833 (_dd0d51d27df06c2a551ec5d6d2b2a7ba_t432501365 * __this, int32_t ____dd0d51d27df06c2a551ec5d6d2b2a7baa0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._dd0d51d27df06c2a551ec5d6d2b2a7ba::_dd0d51d27df06c2a551ec5d6d2b2a7bam(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _dd0d51d27df06c2a551ec5d6d2b2a7ba__dd0d51d27df06c2a551ec5d6d2b2a7bam_m1757670749 (_dd0d51d27df06c2a551ec5d6d2b2a7ba_t432501365 * __this, int32_t ____dd0d51d27df06c2a551ec5d6d2b2a7baa0, int32_t ____dd0d51d27df06c2a551ec5d6d2b2a7ba431, int32_t ____dd0d51d27df06c2a551ec5d6d2b2a7bac2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._dd0d51d27df06c2a551ec5d6d2b2a7ba::ilo__dd0d51d27df06c2a551ec5d6d2b2a7bam21(Little._dd0d51d27df06c2a551ec5d6d2b2a7ba,System.Int32)
extern "C"  int32_t _dd0d51d27df06c2a551ec5d6d2b2a7ba_ilo__dd0d51d27df06c2a551ec5d6d2b2a7bam21_m829441500 (Il2CppObject * __this /* static, unused */, _dd0d51d27df06c2a551ec5d6d2b2a7ba_t432501365 * ____this0, int32_t ____dd0d51d27df06c2a551ec5d6d2b2a7baa1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
