﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<HatredCtrl/stValue>
struct Collection_1_t2611403568;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// HatredCtrl/stValue[]
struct stValueU5BU5D_t1121401207;
// System.Collections.Generic.IEnumerator`1<HatredCtrl/stValue>
struct IEnumerator_1_t1042843163;
// System.Collections.Generic.IList`1<HatredCtrl/stValue>
struct IList_1_t1825625317;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_HatredCtrl_stValue3425945410.h"

// System.Void System.Collections.ObjectModel.Collection`1<HatredCtrl/stValue>::.ctor()
extern "C"  void Collection_1__ctor_m1858615369_gshared (Collection_1_t2611403568 * __this, const MethodInfo* method);
#define Collection_1__ctor_m1858615369(__this, method) ((  void (*) (Collection_1_t2611403568 *, const MethodInfo*))Collection_1__ctor_m1858615369_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<HatredCtrl/stValue>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1629754702_gshared (Collection_1_t2611403568 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1629754702(__this, method) ((  bool (*) (Collection_1_t2611403568 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1629754702_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<HatredCtrl/stValue>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Collection_1_System_Collections_ICollection_CopyTo_m976190747_gshared (Collection_1_t2611403568 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m976190747(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t2611403568 *, Il2CppArray *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m976190747_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<HatredCtrl/stValue>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Collection_1_System_Collections_IEnumerable_GetEnumerator_m1864549098_gshared (Collection_1_t2611403568 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m1864549098(__this, method) ((  Il2CppObject * (*) (Collection_1_t2611403568 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m1864549098_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<HatredCtrl/stValue>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_Add_m327044755_gshared (Collection_1_t2611403568 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m327044755(__this, ___value0, method) ((  int32_t (*) (Collection_1_t2611403568 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m327044755_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<HatredCtrl/stValue>::System.Collections.IList.Contains(System.Object)
extern "C"  bool Collection_1_System_Collections_IList_Contains_m1404760461_gshared (Collection_1_t2611403568 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m1404760461(__this, ___value0, method) ((  bool (*) (Collection_1_t2611403568 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m1404760461_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<HatredCtrl/stValue>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_IndexOf_m654246763_gshared (Collection_1_t2611403568 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m654246763(__this, ___value0, method) ((  int32_t (*) (Collection_1_t2611403568 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m654246763_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.Collection`1<HatredCtrl/stValue>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_Insert_m2706276958_gshared (Collection_1_t2611403568 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m2706276958(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t2611403568 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m2706276958_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<HatredCtrl/stValue>::System.Collections.IList.Remove(System.Object)
extern "C"  void Collection_1_System_Collections_IList_Remove_m1972020746_gshared (Collection_1_t2611403568 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m1972020746(__this, ___value0, method) ((  void (*) (Collection_1_t2611403568 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m1972020746_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<HatredCtrl/stValue>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m3347504815_gshared (Collection_1_t2611403568 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m3347504815(__this, method) ((  bool (*) (Collection_1_t2611403568 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m3347504815_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<HatredCtrl/stValue>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Collection_1_System_Collections_ICollection_get_SyncRoot_m4225118177_gshared (Collection_1_t2611403568 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m4225118177(__this, method) ((  Il2CppObject * (*) (Collection_1_t2611403568 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m4225118177_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<HatredCtrl/stValue>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool Collection_1_System_Collections_IList_get_IsFixedSize_m2936409788_gshared (Collection_1_t2611403568 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m2936409788(__this, method) ((  bool (*) (Collection_1_t2611403568 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m2936409788_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<HatredCtrl/stValue>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_IList_get_IsReadOnly_m4150962365_gshared (Collection_1_t2611403568 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m4150962365(__this, method) ((  bool (*) (Collection_1_t2611403568 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m4150962365_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<HatredCtrl/stValue>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * Collection_1_System_Collections_IList_get_Item_m2660085032_gshared (Collection_1_t2611403568 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m2660085032(__this, ___index0, method) ((  Il2CppObject * (*) (Collection_1_t2611403568 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m2660085032_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<HatredCtrl/stValue>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_set_Item_m2890895029_gshared (Collection_1_t2611403568 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m2890895029(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t2611403568 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m2890895029_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<HatredCtrl/stValue>::Add(T)
extern "C"  void Collection_1_Add_m3652328214_gshared (Collection_1_t2611403568 * __this, stValue_t3425945410  ___item0, const MethodInfo* method);
#define Collection_1_Add_m3652328214(__this, ___item0, method) ((  void (*) (Collection_1_t2611403568 *, stValue_t3425945410 , const MethodInfo*))Collection_1_Add_m3652328214_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<HatredCtrl/stValue>::Clear()
extern "C"  void Collection_1_Clear_m3559715956_gshared (Collection_1_t2611403568 * __this, const MethodInfo* method);
#define Collection_1_Clear_m3559715956(__this, method) ((  void (*) (Collection_1_t2611403568 *, const MethodInfo*))Collection_1_Clear_m3559715956_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<HatredCtrl/stValue>::ClearItems()
extern "C"  void Collection_1_ClearItems_m1663653134_gshared (Collection_1_t2611403568 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m1663653134(__this, method) ((  void (*) (Collection_1_t2611403568 *, const MethodInfo*))Collection_1_ClearItems_m1663653134_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<HatredCtrl/stValue>::Contains(T)
extern "C"  bool Collection_1_Contains_m4064629606_gshared (Collection_1_t2611403568 * __this, stValue_t3425945410  ___item0, const MethodInfo* method);
#define Collection_1_Contains_m4064629606(__this, ___item0, method) ((  bool (*) (Collection_1_t2611403568 *, stValue_t3425945410 , const MethodInfo*))Collection_1_Contains_m4064629606_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<HatredCtrl/stValue>::CopyTo(T[],System.Int32)
extern "C"  void Collection_1_CopyTo_m55290246_gshared (Collection_1_t2611403568 * __this, stValueU5BU5D_t1121401207* ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_CopyTo_m55290246(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t2611403568 *, stValueU5BU5D_t1121401207*, int32_t, const MethodInfo*))Collection_1_CopyTo_m55290246_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<HatredCtrl/stValue>::GetEnumerator()
extern "C"  Il2CppObject* Collection_1_GetEnumerator_m4270544829_gshared (Collection_1_t2611403568 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m4270544829(__this, method) ((  Il2CppObject* (*) (Collection_1_t2611403568 *, const MethodInfo*))Collection_1_GetEnumerator_m4270544829_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<HatredCtrl/stValue>::IndexOf(T)
extern "C"  int32_t Collection_1_IndexOf_m1658743250_gshared (Collection_1_t2611403568 * __this, stValue_t3425945410  ___item0, const MethodInfo* method);
#define Collection_1_IndexOf_m1658743250(__this, ___item0, method) ((  int32_t (*) (Collection_1_t2611403568 *, stValue_t3425945410 , const MethodInfo*))Collection_1_IndexOf_m1658743250_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<HatredCtrl/stValue>::Insert(System.Int32,T)
extern "C"  void Collection_1_Insert_m1286760829_gshared (Collection_1_t2611403568 * __this, int32_t ___index0, stValue_t3425945410  ___item1, const MethodInfo* method);
#define Collection_1_Insert_m1286760829(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t2611403568 *, int32_t, stValue_t3425945410 , const MethodInfo*))Collection_1_Insert_m1286760829_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.ObjectModel.Collection`1<HatredCtrl/stValue>::InsertItem(System.Int32,T)
extern "C"  void Collection_1_InsertItem_m3555627312_gshared (Collection_1_t2611403568 * __this, int32_t ___index0, stValue_t3425945410  ___item1, const MethodInfo* method);
#define Collection_1_InsertItem_m3555627312(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t2611403568 *, int32_t, stValue_t3425945410 , const MethodInfo*))Collection_1_InsertItem_m3555627312_gshared)(__this, ___index0, ___item1, method)
// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.Collection`1<HatredCtrl/stValue>::get_Items()
extern "C"  Il2CppObject* Collection_1_get_Items_m1894994996_gshared (Collection_1_t2611403568 * __this, const MethodInfo* method);
#define Collection_1_get_Items_m1894994996(__this, method) ((  Il2CppObject* (*) (Collection_1_t2611403568 *, const MethodInfo*))Collection_1_get_Items_m1894994996_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<HatredCtrl/stValue>::Remove(T)
extern "C"  bool Collection_1_Remove_m459519201_gshared (Collection_1_t2611403568 * __this, stValue_t3425945410  ___item0, const MethodInfo* method);
#define Collection_1_Remove_m459519201(__this, ___item0, method) ((  bool (*) (Collection_1_t2611403568 *, stValue_t3425945410 , const MethodInfo*))Collection_1_Remove_m459519201_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<HatredCtrl/stValue>::RemoveAt(System.Int32)
extern "C"  void Collection_1_RemoveAt_m3455580995_gshared (Collection_1_t2611403568 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveAt_m3455580995(__this, ___index0, method) ((  void (*) (Collection_1_t2611403568 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m3455580995_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<HatredCtrl/stValue>::RemoveItem(System.Int32)
extern "C"  void Collection_1_RemoveItem_m3703241379_gshared (Collection_1_t2611403568 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveItem_m3703241379(__this, ___index0, method) ((  void (*) (Collection_1_t2611403568 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m3703241379_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<HatredCtrl/stValue>::get_Count()
extern "C"  int32_t Collection_1_get_Count_m3993708009_gshared (Collection_1_t2611403568 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m3993708009(__this, method) ((  int32_t (*) (Collection_1_t2611403568 *, const MethodInfo*))Collection_1_get_Count_m3993708009_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<HatredCtrl/stValue>::get_Item(System.Int32)
extern "C"  stValue_t3425945410  Collection_1_get_Item_m843061097_gshared (Collection_1_t2611403568 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_get_Item_m843061097(__this, ___index0, method) ((  stValue_t3425945410  (*) (Collection_1_t2611403568 *, int32_t, const MethodInfo*))Collection_1_get_Item_m843061097_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<HatredCtrl/stValue>::set_Item(System.Int32,T)
extern "C"  void Collection_1_set_Item_m1436172052_gshared (Collection_1_t2611403568 * __this, int32_t ___index0, stValue_t3425945410  ___value1, const MethodInfo* method);
#define Collection_1_set_Item_m1436172052(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t2611403568 *, int32_t, stValue_t3425945410 , const MethodInfo*))Collection_1_set_Item_m1436172052_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<HatredCtrl/stValue>::SetItem(System.Int32,T)
extern "C"  void Collection_1_SetItem_m2551469797_gshared (Collection_1_t2611403568 * __this, int32_t ___index0, stValue_t3425945410  ___item1, const MethodInfo* method);
#define Collection_1_SetItem_m2551469797(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t2611403568 *, int32_t, stValue_t3425945410 , const MethodInfo*))Collection_1_SetItem_m2551469797_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<HatredCtrl/stValue>::IsValidItem(System.Object)
extern "C"  bool Collection_1_IsValidItem_m2036934150_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_IsValidItem_m2036934150(__this /* static, unused */, ___item0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_IsValidItem_m2036934150_gshared)(__this /* static, unused */, ___item0, method)
// T System.Collections.ObjectModel.Collection`1<HatredCtrl/stValue>::ConvertItem(System.Object)
extern "C"  stValue_t3425945410  Collection_1_ConvertItem_m2942167496_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_ConvertItem_m2942167496(__this /* static, unused */, ___item0, method) ((  stValue_t3425945410  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_ConvertItem_m2942167496_gshared)(__this /* static, unused */, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<HatredCtrl/stValue>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C"  void Collection_1_CheckWritable_m1846660678_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_CheckWritable_m1846660678(__this /* static, unused */, ___list0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_CheckWritable_m1846660678_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<HatredCtrl/stValue>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsSynchronized_m538708222_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsSynchronized_m538708222(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsSynchronized_m538708222_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<HatredCtrl/stValue>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsFixedSize_m1465978465_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsFixedSize_m1465978465(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsFixedSize_m1465978465_gshared)(__this /* static, unused */, ___list0, method)
