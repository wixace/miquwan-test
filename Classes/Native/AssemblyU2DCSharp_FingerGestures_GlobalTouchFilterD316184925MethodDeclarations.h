﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FingerGestures/GlobalTouchFilterDelegate
struct GlobalTouchFilterDelegate_t316184925;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void FingerGestures/GlobalTouchFilterDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void GlobalTouchFilterDelegate__ctor_m2788909956 (GlobalTouchFilterDelegate_t316184925 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FingerGestures/GlobalTouchFilterDelegate::Invoke(System.Int32,UnityEngine.Vector2)
extern "C"  bool GlobalTouchFilterDelegate_Invoke_m2639575 (GlobalTouchFilterDelegate_t316184925 * __this, int32_t ___fingerIndex0, Vector2_t4282066565  ___position1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult FingerGestures/GlobalTouchFilterDelegate::BeginInvoke(System.Int32,UnityEngine.Vector2,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * GlobalTouchFilterDelegate_BeginInvoke_m1345281204 (GlobalTouchFilterDelegate_t316184925 * __this, int32_t ___fingerIndex0, Vector2_t4282066565  ___position1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FingerGestures/GlobalTouchFilterDelegate::EndInvoke(System.IAsyncResult)
extern "C"  bool GlobalTouchFilterDelegate_EndInvoke_m2408026696 (GlobalTouchFilterDelegate_t316184925 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
