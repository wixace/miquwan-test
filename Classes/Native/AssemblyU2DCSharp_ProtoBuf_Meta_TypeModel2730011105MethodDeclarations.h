﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ProtoBuf.Meta.TypeModel
struct TypeModel_t2730011105;
// ProtoBuf.Meta.TypeFormatEventHandler
struct TypeFormatEventHandler_t3699612063;
// System.Type
struct Type_t;
// ProtoBuf.ProtoWriter
struct ProtoWriter_t4117914721;
// System.Object
struct Il2CppObject;
// System.IO.Stream
struct Stream_t1561764144;
// ProtoBuf.SerializationContext
struct SerializationContext_t3997850667;
// ProtoBuf.Serializer/TypeResolver
struct TypeResolver_t356109658;
// System.Collections.IEnumerable
struct IEnumerable_t3464557803;
// ProtoBuf.ProtoReader
struct ProtoReader_t3962509489;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// ProtoBuf.Meta.BasicList
struct BasicList_t528018366;
// ProtoBuf.Meta.RuntimeTypeModel
struct RuntimeTypeModel_t242172789;
// System.Exception
struct Exception_t3991598821;
// System.String
struct String_t;
// System.Reflection.Assembly
struct Assembly_t1418687608;
// System.Type[]
struct TypeU5BU5D_t3339007067;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// ProtoBuf.Meta.TypeFormatEventArgs
struct TypeFormatEventArgs_t2866968344;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ProtoBuf_Meta_TypeFormatEventHan3699612063.h"
#include "mscorlib_System_Type2863145774.h"
#include "AssemblyU2DCSharp_ProtoBuf_WireType2355646059.h"
#include "AssemblyU2DCSharp_ProtoBuf_ProtoTypeCode2227754741.h"
#include "AssemblyU2DCSharp_ProtoBuf_DataFormat274207885.h"
#include "AssemblyU2DCSharp_ProtoBuf_ProtoWriter4117914721.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IO_Stream1561764144.h"
#include "AssemblyU2DCSharp_ProtoBuf_SerializationContext3997850667.h"
#include "AssemblyU2DCSharp_ProtoBuf_PrefixStyle1974492709.h"
#include "AssemblyU2DCSharp_ProtoBuf_Serializer_TypeResolver356109658.h"
#include "AssemblyU2DCSharp_ProtoBuf_ProtoReader3962509489.h"
#include "AssemblyU2DCSharp_ProtoBuf_Meta_TypeModel2730011105.h"
#include "AssemblyU2DCSharp_ProtoBuf_Meta_BasicList528018366.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Reflection_Assembly1418687608.h"
#include "AssemblyU2DCSharp_ProtoBuf_SubItemToken3365128146.h"
#include "mscorlib_System_DateTime4283661327.h"
#include "mscorlib_System_Decimal1954350631.h"
#include "mscorlib_System_Guid2862754429.h"
#include "AssemblyU2DCSharp_ProtoBuf_Meta_TypeFormatEventArg2866968344.h"

// System.Void ProtoBuf.Meta.TypeModel::.ctor()
extern "C"  void TypeModel__ctor_m1406566754 (TypeModel_t2730011105 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.TypeModel::.cctor()
extern "C"  void TypeModel__cctor_m171800203 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.TypeModel::add_DynamicTypeFormatting(ProtoBuf.Meta.TypeFormatEventHandler)
extern "C"  void TypeModel_add_DynamicTypeFormatting_m3140710841 (TypeModel_t2730011105 * __this, TypeFormatEventHandler_t3699612063 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.TypeModel::remove_DynamicTypeFormatting(ProtoBuf.Meta.TypeFormatEventHandler)
extern "C"  void TypeModel_remove_DynamicTypeFormatting_m3007203710 (TypeModel_t2730011105 * __this, TypeFormatEventHandler_t3699612063 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type ProtoBuf.Meta.TypeModel::MapType(System.Type)
extern "C"  Type_t * TypeModel_MapType_m2509999337 (TypeModel_t2730011105 * __this, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type ProtoBuf.Meta.TypeModel::MapType(System.Type,System.Boolean)
extern "C"  Type_t * TypeModel_MapType_m1063139188 (TypeModel_t2730011105 * __this, Type_t * ___type0, bool ___demand1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.WireType ProtoBuf.Meta.TypeModel::GetWireType(ProtoBuf.ProtoTypeCode,ProtoBuf.DataFormat,System.Type&,System.Int32&)
extern "C"  int32_t TypeModel_GetWireType_m2210462316 (TypeModel_t2730011105 * __this, int32_t ___code0, int32_t ___format1, Type_t ** ___type2, int32_t* ___modelKey3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Meta.TypeModel::TrySerializeAuxiliaryType(ProtoBuf.ProtoWriter,System.Type,ProtoBuf.DataFormat,System.Int32,System.Object,System.Boolean)
extern "C"  bool TypeModel_TrySerializeAuxiliaryType_m1483278186 (TypeModel_t2730011105 * __this, ProtoWriter_t4117914721 * ___writer0, Type_t * ___type1, int32_t ___format2, int32_t ___tag3, Il2CppObject * ___value4, bool ___isInsideList5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.TypeModel::SerializeCore(ProtoBuf.ProtoWriter,System.Object)
extern "C"  void TypeModel_SerializeCore_m1500954229 (TypeModel_t2730011105 * __this, ProtoWriter_t4117914721 * ___writer0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.TypeModel::Serialize(System.IO.Stream,System.Object)
extern "C"  void TypeModel_Serialize_m475558661 (TypeModel_t2730011105 * __this, Stream_t1561764144 * ___dest0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.TypeModel::Serialize(System.IO.Stream,System.Object,ProtoBuf.SerializationContext)
extern "C"  void TypeModel_Serialize_m1285790511 (TypeModel_t2730011105 * __this, Stream_t1561764144 * ___dest0, Il2CppObject * ___value1, SerializationContext_t3997850667 * ___context2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.TypeModel::Serialize(ProtoBuf.ProtoWriter,System.Object)
extern "C"  void TypeModel_Serialize_m3238589878 (TypeModel_t2730011105 * __this, ProtoWriter_t4117914721 * ___dest0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProtoBuf.Meta.TypeModel::DeserializeWithLengthPrefix(System.IO.Stream,System.Object,System.Type,ProtoBuf.PrefixStyle,System.Int32)
extern "C"  Il2CppObject * TypeModel_DeserializeWithLengthPrefix_m3352354103 (TypeModel_t2730011105 * __this, Stream_t1561764144 * ___source0, Il2CppObject * ___value1, Type_t * ___type2, int32_t ___style3, int32_t ___fieldNumber4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProtoBuf.Meta.TypeModel::DeserializeWithLengthPrefix(System.IO.Stream,System.Object,System.Type,ProtoBuf.PrefixStyle,System.Int32,ProtoBuf.Serializer/TypeResolver)
extern "C"  Il2CppObject * TypeModel_DeserializeWithLengthPrefix_m1236340757 (TypeModel_t2730011105 * __this, Stream_t1561764144 * ___source0, Il2CppObject * ___value1, Type_t * ___type2, int32_t ___style3, int32_t ___expectedField4, TypeResolver_t356109658 * ___resolver5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProtoBuf.Meta.TypeModel::DeserializeWithLengthPrefix(System.IO.Stream,System.Object,System.Type,ProtoBuf.PrefixStyle,System.Int32,ProtoBuf.Serializer/TypeResolver,System.Int32&)
extern "C"  Il2CppObject * TypeModel_DeserializeWithLengthPrefix_m181749034 (TypeModel_t2730011105 * __this, Stream_t1561764144 * ___source0, Il2CppObject * ___value1, Type_t * ___type2, int32_t ___style3, int32_t ___expectedField4, TypeResolver_t356109658 * ___resolver5, int32_t* ___bytesRead6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProtoBuf.Meta.TypeModel::DeserializeWithLengthPrefix(System.IO.Stream,System.Object,System.Type,ProtoBuf.PrefixStyle,System.Int32,ProtoBuf.Serializer/TypeResolver,System.Int32&,System.Boolean&,ProtoBuf.SerializationContext)
extern "C"  Il2CppObject * TypeModel_DeserializeWithLengthPrefix_m1643452995 (TypeModel_t2730011105 * __this, Stream_t1561764144 * ___source0, Il2CppObject * ___value1, Type_t * ___type2, int32_t ___style3, int32_t ___expectedField4, TypeResolver_t356109658 * ___resolver5, int32_t* ___bytesRead6, bool* ___haveObject7, SerializationContext_t3997850667 * ___context8, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerable ProtoBuf.Meta.TypeModel::DeserializeItems(System.IO.Stream,System.Type,ProtoBuf.PrefixStyle,System.Int32,ProtoBuf.Serializer/TypeResolver)
extern "C"  Il2CppObject * TypeModel_DeserializeItems_m2105537206 (TypeModel_t2730011105 * __this, Stream_t1561764144 * ___source0, Type_t * ___type1, int32_t ___style2, int32_t ___expectedField3, TypeResolver_t356109658 * ___resolver4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerable ProtoBuf.Meta.TypeModel::DeserializeItems(System.IO.Stream,System.Type,ProtoBuf.PrefixStyle,System.Int32,ProtoBuf.Serializer/TypeResolver,ProtoBuf.SerializationContext)
extern "C"  Il2CppObject * TypeModel_DeserializeItems_m1607814944 (TypeModel_t2730011105 * __this, Stream_t1561764144 * ___source0, Type_t * ___type1, int32_t ___style2, int32_t ___expectedField3, TypeResolver_t356109658 * ___resolver4, SerializationContext_t3997850667 * ___context5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.TypeModel::SerializeWithLengthPrefix(System.IO.Stream,System.Object,System.Type,ProtoBuf.PrefixStyle,System.Int32)
extern "C"  void TypeModel_SerializeWithLengthPrefix_m227914019 (TypeModel_t2730011105 * __this, Stream_t1561764144 * ___dest0, Il2CppObject * ___value1, Type_t * ___type2, int32_t ___style3, int32_t ___fieldNumber4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.TypeModel::SerializeWithLengthPrefix(System.IO.Stream,System.Object,System.Type,ProtoBuf.PrefixStyle,System.Int32,ProtoBuf.SerializationContext)
extern "C"  void TypeModel_SerializeWithLengthPrefix_m149409997 (TypeModel_t2730011105 * __this, Stream_t1561764144 * ___dest0, Il2CppObject * ___value1, Type_t * ___type2, int32_t ___style3, int32_t ___fieldNumber4, SerializationContext_t3997850667 * ___context5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProtoBuf.Meta.TypeModel::Deserialize(System.IO.Stream,System.Object,System.Type)
extern "C"  Il2CppObject * TypeModel_Deserialize_m485997516 (TypeModel_t2730011105 * __this, Stream_t1561764144 * ___source0, Il2CppObject * ___value1, Type_t * ___type2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProtoBuf.Meta.TypeModel::Deserialize(System.IO.Stream,System.Object,System.Type,ProtoBuf.SerializationContext)
extern "C"  Il2CppObject * TypeModel_Deserialize_m1716030902 (TypeModel_t2730011105 * __this, Stream_t1561764144 * ___source0, Il2CppObject * ___value1, Type_t * ___type2, SerializationContext_t3997850667 * ___context3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Meta.TypeModel::PrepareDeserialize(System.Object,System.Type&)
extern "C"  bool TypeModel_PrepareDeserialize_m3221901323 (TypeModel_t2730011105 * __this, Il2CppObject * ___value0, Type_t ** ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProtoBuf.Meta.TypeModel::Deserialize(System.IO.Stream,System.Object,System.Type,System.Int32)
extern "C"  Il2CppObject * TypeModel_Deserialize_m3666865291 (TypeModel_t2730011105 * __this, Stream_t1561764144 * ___source0, Il2CppObject * ___value1, Type_t * ___type2, int32_t ___length3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProtoBuf.Meta.TypeModel::Deserialize(System.IO.Stream,System.Object,System.Type,System.Int32,ProtoBuf.SerializationContext)
extern "C"  Il2CppObject * TypeModel_Deserialize_m2300192309 (TypeModel_t2730011105 * __this, Stream_t1561764144 * ___source0, Il2CppObject * ___value1, Type_t * ___type2, int32_t ___length3, SerializationContext_t3997850667 * ___context4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProtoBuf.Meta.TypeModel::Deserialize(ProtoBuf.ProtoReader,System.Object,System.Type)
extern "C"  Il2CppObject * TypeModel_Deserialize_m2562141357 (TypeModel_t2730011105 * __this, ProtoReader_t3962509489 * ___source0, Il2CppObject * ___value1, Type_t * ___type2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProtoBuf.Meta.TypeModel::DeserializeCore(ProtoBuf.ProtoReader,System.Type,System.Object,System.Boolean)
extern "C"  Il2CppObject * TypeModel_DeserializeCore_m2436501841 (TypeModel_t2730011105 * __this, ProtoReader_t3962509489 * ___reader0, Type_t * ___type1, Il2CppObject * ___value2, bool ___noAutoCreate3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo ProtoBuf.Meta.TypeModel::ResolveListAdd(ProtoBuf.Meta.TypeModel,System.Type,System.Type,System.Boolean&)
extern "C"  MethodInfo_t * TypeModel_ResolveListAdd_m1742515471 (Il2CppObject * __this /* static, unused */, TypeModel_t2730011105 * ___model0, Type_t * ___listType1, Type_t * ___itemType2, bool* ___isList3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type ProtoBuf.Meta.TypeModel::GetListItemType(ProtoBuf.Meta.TypeModel,System.Type)
extern "C"  Type_t * TypeModel_GetListItemType_m754529025 (Il2CppObject * __this /* static, unused */, TypeModel_t2730011105 * ___model0, Type_t * ___listType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.TypeModel::TestEnumerableListPatterns(ProtoBuf.Meta.TypeModel,ProtoBuf.Meta.BasicList,System.Type)
extern "C"  void TypeModel_TestEnumerableListPatterns_m3990618629 (Il2CppObject * __this /* static, unused */, TypeModel_t2730011105 * ___model0, BasicList_t528018366 * ___candidates1, Type_t * ___iType2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Meta.TypeModel::CheckDictionaryAccessors(ProtoBuf.Meta.TypeModel,System.Type,System.Type)
extern "C"  bool TypeModel_CheckDictionaryAccessors_m1037401971 (Il2CppObject * __this /* static, unused */, TypeModel_t2730011105 * ___model0, Type_t * ___pair1, Type_t * ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Meta.TypeModel::TryDeserializeList(ProtoBuf.Meta.TypeModel,ProtoBuf.ProtoReader,ProtoBuf.DataFormat,System.Int32,System.Type,System.Type,System.Object&)
extern "C"  bool TypeModel_TryDeserializeList_m3959858190 (TypeModel_t2730011105 * __this, TypeModel_t2730011105 * ___model0, ProtoReader_t3962509489 * ___reader1, int32_t ___format2, int32_t ___tag3, Type_t * ___listType4, Type_t * ___itemType5, Il2CppObject ** ___value6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProtoBuf.Meta.TypeModel::CreateListInstance(System.Type,System.Type)
extern "C"  Il2CppObject * TypeModel_CreateListInstance_m2678517680 (Il2CppObject * __this /* static, unused */, Type_t * ___listType0, Type_t * ___itemType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Meta.TypeModel::TryDeserializeAuxiliaryType(ProtoBuf.ProtoReader,ProtoBuf.DataFormat,System.Int32,System.Type,System.Object&,System.Boolean,System.Boolean,System.Boolean,System.Boolean)
extern "C"  bool TypeModel_TryDeserializeAuxiliaryType_m1117447332 (TypeModel_t2730011105 * __this, ProtoReader_t3962509489 * ___reader0, int32_t ___format1, int32_t ___tag2, Type_t * ___type3, Il2CppObject ** ___value4, bool ___skipOtherFields5, bool ___asListItem6, bool ___autoCreate7, bool ___insideList8, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.Meta.RuntimeTypeModel ProtoBuf.Meta.TypeModel::Create()
extern "C"  RuntimeTypeModel_t242172789 * TypeModel_Create_m3914732670 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type ProtoBuf.Meta.TypeModel::ResolveProxies(System.Type)
extern "C"  Type_t * TypeModel_ResolveProxies_m143400433 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Meta.TypeModel::IsDefined(System.Type)
extern "C"  bool TypeModel_IsDefined_m3406640166 (TypeModel_t2730011105 * __this, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ProtoBuf.Meta.TypeModel::GetKey(System.Type&)
extern "C"  int32_t TypeModel_GetKey_m4142870512 (TypeModel_t2730011105 * __this, Type_t ** ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProtoBuf.Meta.TypeModel::DeepClone(System.Object)
extern "C"  Il2CppObject * TypeModel_DeepClone_m3494234766 (TypeModel_t2730011105 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.TypeModel::ThrowUnexpectedSubtype(System.Type,System.Type)
extern "C"  void TypeModel_ThrowUnexpectedSubtype_m1674084679 (Il2CppObject * __this /* static, unused */, Type_t * ___expected0, Type_t * ___actual1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.TypeModel::ThrowUnexpectedType(System.Type)
extern "C"  void TypeModel_ThrowUnexpectedType_m2663398184 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Exception ProtoBuf.Meta.TypeModel::CreateNestedListsNotSupported()
extern "C"  Exception_t3991598821 * TypeModel_CreateNestedListsNotSupported_m1390700586 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.TypeModel::ThrowCannotCreateInstance(System.Type)
extern "C"  void TypeModel_ThrowCannotCreateInstance_m2601495327 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ProtoBuf.Meta.TypeModel::SerializeType(ProtoBuf.Meta.TypeModel,System.Type)
extern "C"  String_t* TypeModel_SerializeType_m2199303583 (Il2CppObject * __this /* static, unused */, TypeModel_t2730011105 * ___model0, Type_t * ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type ProtoBuf.Meta.TypeModel::DeserializeType(ProtoBuf.Meta.TypeModel,System.String)
extern "C"  Type_t * TypeModel_DeserializeType_m2608693330 (Il2CppObject * __this /* static, unused */, TypeModel_t2730011105 * ___model0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Meta.TypeModel::CanSerializeContractType(System.Type)
extern "C"  bool TypeModel_CanSerializeContractType_m2525205967 (TypeModel_t2730011105 * __this, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Meta.TypeModel::CanSerialize(System.Type)
extern "C"  bool TypeModel_CanSerialize_m347549275 (TypeModel_t2730011105 * __this, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Meta.TypeModel::CanSerializeBasicType(System.Type)
extern "C"  bool TypeModel_CanSerializeBasicType_m1979526477 (TypeModel_t2730011105 * __this, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Meta.TypeModel::CanSerialize(System.Type,System.Boolean,System.Boolean,System.Boolean)
extern "C"  bool TypeModel_CanSerialize_m580565666 (TypeModel_t2730011105 * __this, Type_t * ___type0, bool ___allowBasic1, bool ___allowContract2, bool ___allowLists3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ProtoBuf.Meta.TypeModel::GetSchema(System.Type)
extern "C"  String_t* TypeModel_GetSchema_m460868799 (TypeModel_t2730011105 * __this, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type ProtoBuf.Meta.TypeModel::GetType(System.String,System.Reflection.Assembly)
extern "C"  Type_t * TypeModel_GetType_m2403828260 (TypeModel_t2730011105 * __this, String_t* ___fullName0, Assembly_t1418687608 * ___context1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type ProtoBuf.Meta.TypeModel::ResolveKnownType(System.String,ProtoBuf.Meta.TypeModel,System.Reflection.Assembly)
extern "C" IL2CPP_NO_INLINE Type_t * TypeModel_ResolveKnownType_m228590640 (Il2CppObject * __this /* static, unused */, String_t* ___name0, TypeModel_t2730011105 * ___model1, Assembly_t1418687608 * ___assembly2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ProtoBuf.Meta.TypeModel::ilo_GetKey1(ProtoBuf.Meta.TypeModel,System.Type&)
extern "C"  int32_t TypeModel_ilo_GetKey1_m3420612307 (Il2CppObject * __this /* static, unused */, TypeModel_t2730011105 * ____this0, Type_t ** ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.ProtoTypeCode ProtoBuf.Meta.TypeModel::ilo_GetTypeCode2(System.Type)
extern "C"  int32_t TypeModel_ilo_GetTypeCode2_m2447483648 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.WireType ProtoBuf.Meta.TypeModel::ilo_GetWireType3(ProtoBuf.Meta.TypeModel,ProtoBuf.ProtoTypeCode,ProtoBuf.DataFormat,System.Type&,System.Int32&)
extern "C"  int32_t TypeModel_ilo_GetWireType3_m2598882579 (Il2CppObject * __this /* static, unused */, TypeModel_t2730011105 * ____this0, int32_t ___code1, int32_t ___format2, Type_t ** ___type3, int32_t* ___modelKey4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.TypeModel::ilo_Serialize4(ProtoBuf.Meta.TypeModel,System.Int32,System.Object,ProtoBuf.ProtoWriter)
extern "C"  void TypeModel_ilo_Serialize4_m2283054889 (Il2CppObject * __this /* static, unused */, TypeModel_t2730011105 * ____this0, int32_t ___key1, Il2CppObject * ___value2, ProtoWriter_t4117914721 * ___dest3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.SubItemToken ProtoBuf.Meta.TypeModel::ilo_StartSubItem5(System.Object,ProtoBuf.ProtoWriter)
extern "C"  SubItemToken_t3365128146  TypeModel_ilo_StartSubItem5_m4225053397 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___instance0, ProtoWriter_t4117914721 * ___writer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.TypeModel::ilo_WriteUInt166(System.UInt16,ProtoBuf.ProtoWriter)
extern "C"  void TypeModel_ilo_WriteUInt166_m2163778887 (Il2CppObject * __this /* static, unused */, uint16_t ___value0, ProtoWriter_t4117914721 * ___writer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.TypeModel::ilo_WriteSByte7(System.SByte,ProtoBuf.ProtoWriter)
extern "C"  void TypeModel_ilo_WriteSByte7_m2844289218 (Il2CppObject * __this /* static, unused */, int8_t ___value0, ProtoWriter_t4117914721 * ___writer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.TypeModel::ilo_WriteByte8(System.Byte,ProtoBuf.ProtoWriter)
extern "C"  void TypeModel_ilo_WriteByte8_m1191984489 (Il2CppObject * __this /* static, unused */, uint8_t ___value0, ProtoWriter_t4117914721 * ___writer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.TypeModel::ilo_WriteDateTime9(System.DateTime,ProtoBuf.ProtoWriter)
extern "C"  void TypeModel_ilo_WriteDateTime9_m2609239178 (Il2CppObject * __this /* static, unused */, DateTime_t4283661327  ___value0, ProtoWriter_t4117914721 * ___dest1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.TypeModel::ilo_WriteDecimal10(System.Decimal,ProtoBuf.ProtoWriter)
extern "C"  void TypeModel_ilo_WriteDecimal10_m2074708538 (Il2CppObject * __this /* static, unused */, Decimal_t1954350631  ___value0, ProtoWriter_t4117914721 * ___writer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.TypeModel::ilo_WriteGuid11(System.Guid,ProtoBuf.ProtoWriter)
extern "C"  void TypeModel_ilo_WriteGuid11_m2627456601 (Il2CppObject * __this /* static, unused */, Guid_t2862754429  ___value0, ProtoWriter_t4117914721 * ___dest1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.TypeModel::ilo_WriteString12(System.String,ProtoBuf.ProtoWriter)
extern "C"  void TypeModel_ilo_WriteString12_m1066451818 (Il2CppObject * __this /* static, unused */, String_t* ___value0, ProtoWriter_t4117914721 * ___writer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Exception ProtoBuf.Meta.TypeModel::ilo_CreateNestedListsNotSupported13()
extern "C"  Exception_t3991598821 * TypeModel_ilo_CreateNestedListsNotSupported13_m126637497 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.TypeModel::ilo_ThrowUnexpectedType14(System.Type)
extern "C"  void TypeModel_ilo_ThrowUnexpectedType14_m3235093208 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.TypeModel::ilo_SetRootObject15(ProtoBuf.ProtoWriter,System.Object)
extern "C"  void TypeModel_ilo_SetRootObject15_m2706967978 (Il2CppObject * __this /* static, unused */, ProtoWriter_t4117914721 * ____this0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.TypeModel::ilo_SerializeCore16(ProtoBuf.Meta.TypeModel,ProtoBuf.ProtoWriter,System.Object)
extern "C"  void TypeModel_ilo_SerializeCore16_m2834368004 (Il2CppObject * __this /* static, unused */, TypeModel_t2730011105 * ____this0, ProtoWriter_t4117914721 * ___writer1, Il2CppObject * ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.TypeModel::ilo_Flush17(ProtoBuf.ProtoWriter)
extern "C"  void TypeModel_ilo_Flush17_m1354855839 (Il2CppObject * __this /* static, unused */, ProtoWriter_t4117914721 * ___writer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProtoBuf.Meta.TypeModel::ilo_DeserializeWithLengthPrefix18(ProtoBuf.Meta.TypeModel,System.IO.Stream,System.Object,System.Type,ProtoBuf.PrefixStyle,System.Int32,ProtoBuf.Serializer/TypeResolver,System.Int32&)
extern "C"  Il2CppObject * TypeModel_ilo_DeserializeWithLengthPrefix18_m2321348793 (Il2CppObject * __this /* static, unused */, TypeModel_t2730011105 * ____this0, Stream_t1561764144 * ___source1, Il2CppObject * ___value2, Type_t * ___type3, int32_t ___style4, int32_t ___expectedField5, TypeResolver_t356109658 * ___resolver6, int32_t* ___bytesRead7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type ProtoBuf.Meta.TypeModel::ilo_Invoke19(ProtoBuf.Serializer/TypeResolver,System.Int32)
extern "C"  Type_t * TypeModel_ilo_Invoke19_m588464950 (Il2CppObject * __this /* static, unused */, TypeResolver_t356109658 * ____this0, int32_t ___fieldNumber1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.ProtoReader ProtoBuf.Meta.TypeModel::ilo_Create20(System.IO.Stream,ProtoBuf.Meta.TypeModel,ProtoBuf.SerializationContext,System.Int32)
extern "C"  ProtoReader_t3962509489 * TypeModel_ilo_Create20_m851195961 (Il2CppObject * __this /* static, unused */, Stream_t1561764144 * ___source0, TypeModel_t2730011105 * ___model1, SerializationContext_t3997850667 * ___context2, int32_t ___len3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Meta.TypeModel::ilo_TryDeserializeAuxiliaryType21(ProtoBuf.Meta.TypeModel,ProtoBuf.ProtoReader,ProtoBuf.DataFormat,System.Int32,System.Type,System.Object&,System.Boolean,System.Boolean,System.Boolean,System.Boolean)
extern "C"  bool TypeModel_ilo_TryDeserializeAuxiliaryType21_m147533325 (Il2CppObject * __this /* static, unused */, TypeModel_t2730011105 * ____this0, ProtoReader_t3962509489 * ___reader1, int32_t ___format2, int32_t ___tag3, Type_t * ___type4, Il2CppObject ** ___value5, bool ___skipOtherFields6, bool ___asListItem7, bool ___autoCreate8, bool ___insideList9, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ProtoBuf.Meta.TypeModel::ilo_get_Position22(ProtoBuf.ProtoReader)
extern "C"  int32_t TypeModel_ilo_get_Position22_m2500041265 (Il2CppObject * __this /* static, unused */, ProtoReader_t3962509489 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.TypeModel::ilo_WriteObject23(System.Object,System.Int32,ProtoBuf.ProtoWriter,ProtoBuf.PrefixStyle,System.Int32)
extern "C"  void TypeModel_ilo_WriteObject23_m1617733836 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, int32_t ___key1, ProtoWriter_t4117914721 * ___writer2, int32_t ___style3, int32_t ___fieldNumber4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Meta.TypeModel::ilo_PrepareDeserialize24(ProtoBuf.Meta.TypeModel,System.Object,System.Type&)
extern "C"  bool TypeModel_ilo_PrepareDeserialize24_m1839241917 (Il2CppObject * __this /* static, unused */, TypeModel_t2730011105 * ____this0, Il2CppObject * ___value1, Type_t ** ___type2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.TypeModel::ilo_SetRootObject25(ProtoBuf.ProtoReader,System.Object)
extern "C"  void TypeModel_ilo_SetRootObject25_m1241893881 (Il2CppObject * __this /* static, unused */, ProtoReader_t3962509489 * ____this0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.TypeModel::ilo_CheckFullyConsumed26(ProtoBuf.ProtoReader)
extern "C"  void TypeModel_ilo_CheckFullyConsumed26_m309551675 (Il2CppObject * __this /* static, unused */, ProtoReader_t3962509489 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.TypeModel::ilo_Recycle27(ProtoBuf.ProtoReader)
extern "C"  void TypeModel_ilo_Recycle27_m2903435709 (Il2CppObject * __this /* static, unused */, ProtoReader_t3962509489 * ___reader0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type ProtoBuf.Meta.TypeModel::ilo_MapType28(ProtoBuf.Meta.TypeModel,System.Type)
extern "C"  Type_t * TypeModel_ilo_MapType28_m4101700921 (Il2CppObject * __this /* static, unused */, TypeModel_t2730011105 * ____this0, Type_t * ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProtoBuf.Meta.TypeModel::ilo_Deserialize29(ProtoBuf.Meta.TypeModel,System.IO.Stream,System.Object,System.Type,System.Int32,ProtoBuf.SerializationContext)
extern "C"  Il2CppObject * TypeModel_ilo_Deserialize29_m2344088932 (Il2CppObject * __this /* static, unused */, TypeModel_t2730011105 * ____this0, Stream_t1561764144 * ___source1, Il2CppObject * ___value2, Type_t * ___type3, int32_t ___length4, SerializationContext_t3997850667 * ___context5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProtoBuf.Meta.TypeModel::ilo_DeserializeCore30(ProtoBuf.Meta.TypeModel,ProtoBuf.ProtoReader,System.Type,System.Object,System.Boolean)
extern "C"  Il2CppObject * TypeModel_ilo_DeserializeCore30_m1856740330 (Il2CppObject * __this /* static, unused */, TypeModel_t2730011105 * ____this0, ProtoReader_t3962509489 * ___reader1, Type_t * ___type2, Il2CppObject * ___value3, bool ___noAutoCreate4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Meta.TypeModel::ilo_IsEnum31(System.Type)
extern "C"  bool TypeModel_ilo_IsEnum31_m1176283823 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProtoBuf.Meta.TypeModel::ilo_Deserialize32(ProtoBuf.Meta.TypeModel,System.Int32,System.Object,ProtoBuf.ProtoReader)
extern "C"  Il2CppObject * TypeModel_ilo_Deserialize32_m1748630562 (Il2CppObject * __this /* static, unused */, TypeModel_t2730011105 * ____this0, int32_t ___key1, Il2CppObject * ___value2, ProtoReader_t3962509489 * ___source3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type ProtoBuf.Meta.TypeModel::ilo_MapType33(ProtoBuf.Meta.TypeModel,System.Type)
extern "C"  Type_t * TypeModel_ilo_MapType33_m3604931935 (Il2CppObject * __this /* static, unused */, TypeModel_t2730011105 * ____this0, Type_t * ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo ProtoBuf.Meta.TypeModel::ilo_GetInstanceMethod34(System.Type,System.String,System.Type[])
extern "C"  MethodInfo_t * TypeModel_ilo_GetInstanceMethod34_m2835012930 (Il2CppObject * __this /* static, unused */, Type_t * ___declaringType0, String_t* ___name1, TypeU5BU5D_t3339007067* ___types2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ProtoBuf.Meta.TypeModel::ilo_Add35(ProtoBuf.Meta.BasicList,System.Object)
extern "C"  int32_t TypeModel_ilo_Add35_m2554366780 (Il2CppObject * __this /* static, unused */, BasicList_t528018366 * ____this0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Meta.TypeModel::ilo_Contains36(ProtoBuf.Meta.BasicList,System.Object)
extern "C"  bool TypeModel_ilo_Contains36_m2718091447 (Il2CppObject * __this /* static, unused */, BasicList_t528018366 * ____this0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ProtoBuf.Meta.TypeModel::ilo_get_Count37(ProtoBuf.Meta.BasicList)
extern "C"  int32_t TypeModel_ilo_get_Count37_m478899527 (Il2CppObject * __this /* static, unused */, BasicList_t528018366 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProtoBuf.Meta.TypeModel::ilo_get_Item38(ProtoBuf.Meta.BasicList,System.Int32)
extern "C"  Il2CppObject * TypeModel_ilo_get_Item38_m1711043372 (Il2CppObject * __this /* static, unused */, BasicList_t528018366 * ____this0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo ProtoBuf.Meta.TypeModel::ilo_ResolveListAdd39(ProtoBuf.Meta.TypeModel,System.Type,System.Type,System.Boolean&)
extern "C"  MethodInfo_t * TypeModel_ilo_ResolveListAdd39_m644090006 (Il2CppObject * __this /* static, unused */, TypeModel_t2730011105 * ___model0, Type_t * ___listType1, Type_t * ___itemType2, bool* ___isList3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Meta.TypeModel::ilo_TryDeserializeList40(ProtoBuf.Meta.TypeModel,ProtoBuf.Meta.TypeModel,ProtoBuf.ProtoReader,ProtoBuf.DataFormat,System.Int32,System.Type,System.Type,System.Object&)
extern "C"  bool TypeModel_ilo_TryDeserializeList40_m3269860986 (Il2CppObject * __this /* static, unused */, TypeModel_t2730011105 * ____this0, TypeModel_t2730011105 * ___model1, ProtoReader_t3962509489 * ___reader2, int32_t ___format3, int32_t ___tag4, Type_t * ___listType5, Type_t * ___itemType6, Il2CppObject ** ___value7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProtoBuf.Meta.TypeModel::ilo_CreateListInstance41(System.Type,System.Type)
extern "C"  Il2CppObject * TypeModel_ilo_CreateListInstance41_m1373600064 (Il2CppObject * __this /* static, unused */, Type_t * ___listType0, Type_t * ___itemType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.TypeModel::ilo_SkipField42(ProtoBuf.ProtoReader)
extern "C"  void TypeModel_ilo_SkipField42_m705599070 (Il2CppObject * __this /* static, unused */, ProtoReader_t3962509489 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.TypeModel::ilo_Hint43(ProtoBuf.ProtoReader,ProtoBuf.WireType)
extern "C"  void TypeModel_ilo_Hint43_m1829387133 (Il2CppObject * __this /* static, unused */, ProtoReader_t3962509489 * ____this0, int32_t ___wireType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.SubItemToken ProtoBuf.Meta.TypeModel::ilo_StartSubItem44(ProtoBuf.ProtoReader)
extern "C"  SubItemToken_t3365128146  TypeModel_ilo_StartSubItem44_m1430744244 (Il2CppObject * __this /* static, unused */, ProtoReader_t3962509489 * ___reader0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ProtoBuf.Meta.TypeModel::ilo_ReadInt3245(ProtoBuf.ProtoReader)
extern "C"  int32_t TypeModel_ilo_ReadInt3245_m1231747212 (Il2CppObject * __this /* static, unused */, ProtoReader_t3962509489 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt16 ProtoBuf.Meta.TypeModel::ilo_ReadUInt1646(ProtoBuf.ProtoReader)
extern "C"  uint16_t TypeModel_ilo_ReadUInt1646_m2941482415 (Il2CppObject * __this /* static, unused */, ProtoReader_t3962509489 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 ProtoBuf.Meta.TypeModel::ilo_ReadUInt3247(ProtoBuf.ProtoReader)
extern "C"  uint32_t TypeModel_ilo_ReadUInt3247_m2312614244 (Il2CppObject * __this /* static, unused */, ProtoReader_t3962509489 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Meta.TypeModel::ilo_ReadBoolean48(ProtoBuf.ProtoReader)
extern "C"  bool TypeModel_ilo_ReadBoolean48_m3021049743 (Il2CppObject * __this /* static, unused */, ProtoReader_t3962509489 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double ProtoBuf.Meta.TypeModel::ilo_ReadDouble49(ProtoBuf.ProtoReader)
extern "C"  double TypeModel_ilo_ReadDouble49_m2180814166 (Il2CppObject * __this /* static, unused */, ProtoReader_t3962509489 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ProtoBuf.Meta.TypeModel::ilo_ReadString50(ProtoBuf.ProtoReader)
extern "C"  String_t* TypeModel_ilo_ReadString50_m778094956 (Il2CppObject * __this /* static, unused */, ProtoReader_t3962509489 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Guid ProtoBuf.Meta.TypeModel::ilo_ReadGuid51(ProtoBuf.ProtoReader)
extern "C"  Guid_t2862754429  TypeModel_ilo_ReadGuid51_m523453533 (Il2CppObject * __this /* static, unused */, ProtoReader_t3962509489 * ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type ProtoBuf.Meta.TypeModel::ilo_GetUnderlyingType52(System.Type)
extern "C"  Type_t * TypeModel_ilo_GetUnderlyingType52_m784245704 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.TypeModel::ilo_Close53(ProtoBuf.ProtoWriter)
extern "C"  void TypeModel_ilo_Close53_m3080344331 (Il2CppObject * __this /* static, unused */, ProtoWriter_t4117914721 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.TypeModel::ilo_BlockCopy54(System.Byte[],System.Int32,System.Byte[],System.Int32,System.Int32)
extern "C"  void TypeModel_ilo_BlockCopy54_m4003825343 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___from0, int32_t ___fromIndex1, ByteU5BU5D_t4260760469* ___to2, int32_t ___toIndex3, int32_t ___count4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Meta.TypeModel::ilo_TrySerializeAuxiliaryType55(ProtoBuf.Meta.TypeModel,ProtoBuf.ProtoWriter,System.Type,ProtoBuf.DataFormat,System.Int32,System.Object,System.Boolean)
extern "C"  bool TypeModel_ilo_TrySerializeAuxiliaryType55_m1419819124 (Il2CppObject * __this /* static, unused */, TypeModel_t2730011105 * ____this0, ProtoWriter_t4117914721 * ___writer1, Type_t * ___type2, int32_t ___format3, int32_t ___tag4, Il2CppObject * ___value5, bool ___isInsideList6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.TypeModel::ilo_Invoke56(ProtoBuf.Meta.TypeFormatEventHandler,System.Object,ProtoBuf.Meta.TypeFormatEventArgs)
extern "C"  void TypeModel_ilo_Invoke56_m1133084097 (Il2CppObject * __this /* static, unused */, TypeFormatEventHandler_t3699612063 * ____this0, Il2CppObject * ___sender1, TypeFormatEventArgs_t2866968344 * ___args2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Meta.TypeModel::ilo_CanSerialize57(ProtoBuf.Meta.TypeModel,System.Type,System.Boolean,System.Boolean,System.Boolean)
extern "C"  bool TypeModel_ilo_CanSerialize57_m4257498740 (Il2CppObject * __this /* static, unused */, TypeModel_t2730011105 * ____this0, Type_t * ___type1, bool ___allowBasic2, bool ___allowContract3, bool ___allowLists4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type ProtoBuf.Meta.TypeModel::ilo_GetListItemType58(ProtoBuf.Meta.TypeModel,System.Type)
extern "C"  Type_t * TypeModel_ilo_GetListItemType58_m3648621361 (Il2CppObject * __this /* static, unused */, TypeModel_t2730011105 * ___model0, Type_t * ___listType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
