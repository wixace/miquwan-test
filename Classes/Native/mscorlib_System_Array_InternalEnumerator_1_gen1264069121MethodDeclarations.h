﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1264069121.h"
#include "mscorlib_System_Array1146569071.h"
#include "UnityEngine_UnityEngine_CharacterInfo2481726445.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.CharacterInfo>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2249466917_gshared (InternalEnumerator_1_t1264069121 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m2249466917(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t1264069121 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m2249466917_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.CharacterInfo>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3796077915_gshared (InternalEnumerator_1_t1264069121 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3796077915(__this, method) ((  void (*) (InternalEnumerator_1_t1264069121 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3796077915_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.CharacterInfo>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m912531793_gshared (InternalEnumerator_1_t1264069121 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m912531793(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t1264069121 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m912531793_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.CharacterInfo>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2172954684_gshared (InternalEnumerator_1_t1264069121 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m2172954684(__this, method) ((  void (*) (InternalEnumerator_1_t1264069121 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2172954684_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.CharacterInfo>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1726522891_gshared (InternalEnumerator_1_t1264069121 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m1726522891(__this, method) ((  bool (*) (InternalEnumerator_1_t1264069121 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1726522891_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.CharacterInfo>::get_Current()
extern "C"  CharacterInfo_t2481726445  InternalEnumerator_1_get_Current_m1503531342_gshared (InternalEnumerator_1_t1264069121 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1503531342(__this, method) ((  CharacterInfo_t2481726445  (*) (InternalEnumerator_1_t1264069121 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1503531342_gshared)(__this, method)
