﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.SpringJoint2D
struct SpringJoint2D_t2417999365;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.SpringJoint2D::.ctor()
extern "C"  void SpringJoint2D__ctor_m3620964650 (SpringJoint2D_t2417999365 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.SpringJoint2D::get_autoConfigureDistance()
extern "C"  bool SpringJoint2D_get_autoConfigureDistance_m420130161 (SpringJoint2D_t2417999365 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SpringJoint2D::set_autoConfigureDistance(System.Boolean)
extern "C"  void SpringJoint2D_set_autoConfigureDistance_m772352334 (SpringJoint2D_t2417999365 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.SpringJoint2D::get_distance()
extern "C"  float SpringJoint2D_get_distance_m3684756332 (SpringJoint2D_t2417999365 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SpringJoint2D::set_distance(System.Single)
extern "C"  void SpringJoint2D_set_distance_m2495030463 (SpringJoint2D_t2417999365 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.SpringJoint2D::get_dampingRatio()
extern "C"  float SpringJoint2D_get_dampingRatio_m3315649440 (SpringJoint2D_t2417999365 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SpringJoint2D::set_dampingRatio(System.Single)
extern "C"  void SpringJoint2D_set_dampingRatio_m1885911307 (SpringJoint2D_t2417999365 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.SpringJoint2D::get_frequency()
extern "C"  float SpringJoint2D_get_frequency_m414553703 (SpringJoint2D_t2417999365 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SpringJoint2D::set_frequency(System.Single)
extern "C"  void SpringJoint2D_set_frequency_m262343588 (SpringJoint2D_t2417999365 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
