﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman/Tree
struct Tree_t1054057453;
// ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman
struct DeflaterHuffman_t3769756376;
// System.Int16[]
struct Int16U5BU5D_t801762735;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// System.Int32[]
struct Int32U5BU5D_t3230847821;

#include "codegen/il2cpp-codegen.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Zi3769756376.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Zi1054057453.h"

// System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman/Tree::.ctor(ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman,System.Int32,System.Int32,System.Int32)
extern "C"  void Tree__ctor_m3029332514 (Tree_t1054057453 * __this, DeflaterHuffman_t3769756376 * ___dh0, int32_t ___elems1, int32_t ___minCodes2, int32_t ___maxLength3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman/Tree::Reset()
extern "C"  void Tree_Reset_m757761098 (Tree_t1054057453 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman/Tree::WriteSymbol(System.Int32)
extern "C"  void Tree_WriteSymbol_m4024054307 (Tree_t1054057453 * __this, int32_t ___code0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman/Tree::SetStaticCodes(System.Int16[],System.Byte[])
extern "C"  void Tree_SetStaticCodes_m469290505 (Tree_t1054057453 * __this, Int16U5BU5D_t801762735* ___staticCodes0, ByteU5BU5D_t4260760469* ___staticLengths1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman/Tree::BuildCodes()
extern "C"  void Tree_BuildCodes_m614041567 (Tree_t1054057453 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman/Tree::BuildTree()
extern "C"  void Tree_BuildTree_m3557342695 (Tree_t1054057453 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman/Tree::GetEncodedLength()
extern "C"  int32_t Tree_GetEncodedLength_m866994417 (Tree_t1054057453 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman/Tree::CalcBLFreq(ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman/Tree)
extern "C"  void Tree_CalcBLFreq_m2156942961 (Tree_t1054057453 * __this, Tree_t1054057453 * ___blTree0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman/Tree::WriteTree(ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman/Tree)
extern "C"  void Tree_WriteTree_m1704667083 (Tree_t1054057453 * __this, Tree_t1054057453 * ___blTree0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman/Tree::BuildLength(System.Int32[])
extern "C"  void Tree_BuildLength_m1615523614 (Tree_t1054057453 * __this, Int32U5BU5D_t3230847821* ___childs0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
