﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.Serialization.JsonFx.JsonOptInAttribute
struct JsonOptInAttribute_t3750829390;

#include "codegen/il2cpp-codegen.h"

// System.Void Pathfinding.Serialization.JsonFx.JsonOptInAttribute::.ctor()
extern "C"  void JsonOptInAttribute__ctor_m811349181 (JsonOptInAttribute_t3750829390 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
