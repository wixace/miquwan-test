﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_GUIUtilityGenerated
struct UnityEngine_GUIUtilityGenerated_t191851126;
// JSVCall
struct JSVCall_t3708497963;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void UnityEngine_GUIUtilityGenerated::.ctor()
extern "C"  void UnityEngine_GUIUtilityGenerated__ctor_m1509880245 (UnityEngine_GUIUtilityGenerated_t191851126 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIUtilityGenerated::GUIUtility_GUIUtility1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIUtilityGenerated_GUIUtility_GUIUtility1_m41775549 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUIUtilityGenerated::GUIUtility_hotControl(JSVCall)
extern "C"  void UnityEngine_GUIUtilityGenerated_GUIUtility_hotControl_m3127246742 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUIUtilityGenerated::GUIUtility_keyboardControl(JSVCall)
extern "C"  void UnityEngine_GUIUtilityGenerated_GUIUtility_keyboardControl_m232908400 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUIUtilityGenerated::GUIUtility_systemCopyBuffer(JSVCall)
extern "C"  void UnityEngine_GUIUtilityGenerated_GUIUtility_systemCopyBuffer_m4079778498 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUIUtilityGenerated::GUIUtility_hasModalWindow(JSVCall)
extern "C"  void UnityEngine_GUIUtilityGenerated_GUIUtility_hasModalWindow_m798053187 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIUtilityGenerated::GUIUtility_ExitGUI(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIUtilityGenerated_GUIUtility_ExitGUI_m3137048730 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIUtilityGenerated::GUIUtility_GetControlID__Int32__FocusType__Rect(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIUtilityGenerated_GUIUtility_GetControlID__Int32__FocusType__Rect_m2272406351 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIUtilityGenerated::GUIUtility_GetControlID__GUIContent__FocusType__Rect(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIUtilityGenerated_GUIUtility_GetControlID__GUIContent__FocusType__Rect_m4267980275 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIUtilityGenerated::GUIUtility_GetControlID__Int32__FocusType(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIUtilityGenerated_GUIUtility_GetControlID__Int32__FocusType_m3566112747 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIUtilityGenerated::GUIUtility_GetControlID__GUIContent__FocusType(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIUtilityGenerated_GUIUtility_GetControlID__GUIContent__FocusType_m3254150543 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIUtilityGenerated::GUIUtility_GetControlID__FocusType__Rect(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIUtilityGenerated_GUIUtility_GetControlID__FocusType__Rect_m1080153457 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIUtilityGenerated::GUIUtility_GetControlID__FocusType(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIUtilityGenerated_GUIUtility_GetControlID__FocusType_m1015610253 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIUtilityGenerated::GUIUtility_GetStateObject__Type__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIUtilityGenerated_GUIUtility_GetStateObject__Type__Int32_m2837915127 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIUtilityGenerated::GUIUtility_GUIToScreenPoint__Vector2(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIUtilityGenerated_GUIUtility_GUIToScreenPoint__Vector2_m1561198814 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIUtilityGenerated::GUIUtility_QueryStateObject__Type__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIUtilityGenerated_GUIUtility_QueryStateObject__Type__Int32_m1741989097 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIUtilityGenerated::GUIUtility_RotateAroundPivot__Single__Vector2(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIUtilityGenerated_GUIUtility_RotateAroundPivot__Single__Vector2_m3962853106 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIUtilityGenerated::GUIUtility_ScaleAroundPivot__Vector2__Vector2(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIUtilityGenerated_GUIUtility_ScaleAroundPivot__Vector2__Vector2_m1996067376 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIUtilityGenerated::GUIUtility_ScreenToGUIPoint__Vector2(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIUtilityGenerated_GUIUtility_ScreenToGUIPoint__Vector2_m3466414704 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIUtilityGenerated::GUIUtility_ScreenToGUIRect__Rect(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIUtilityGenerated_GUIUtility_ScreenToGUIRect__Rect_m2155414201 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUIUtilityGenerated::__Register()
extern "C"  void UnityEngine_GUIUtilityGenerated___Register_m254694450 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUIUtilityGenerated::ilo_setStringS1(System.Int32,System.String)
extern "C"  void UnityEngine_GUIUtilityGenerated_ilo_setStringS1_m4105915902 (Il2CppObject * __this /* static, unused */, int32_t ___e0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_GUIUtilityGenerated::ilo_getObject2(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_GUIUtilityGenerated_ilo_getObject2_m3466400913 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_GUIUtilityGenerated::ilo_getEnum3(System.Int32)
extern "C"  int32_t UnityEngine_GUIUtilityGenerated_ilo_getEnum3_m2463654241 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_GUIUtilityGenerated::ilo_getInt324(System.Int32)
extern "C"  int32_t UnityEngine_GUIUtilityGenerated_ilo_getInt324_m1558647963 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine_GUIUtilityGenerated::ilo_getVector2S5(System.Int32)
extern "C"  Vector2_t4282066565  UnityEngine_GUIUtilityGenerated_ilo_getVector2S5_m2253559347 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUIUtilityGenerated::ilo_setWhatever6(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  void UnityEngine_GUIUtilityGenerated_ilo_setWhatever6_m1361358057 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___obj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_GUIUtilityGenerated::ilo_setObject7(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UnityEngine_GUIUtilityGenerated_ilo_setObject7_m1172438175 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
