﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_halljasBistra153
struct  M_halljasBistra153_t4051408741  : public Il2CppObject
{
public:
	// System.String GarbageiOS.M_halljasBistra153::_hidrerayDaltall
	String_t* ____hidrerayDaltall_0;
	// System.Int32 GarbageiOS.M_halljasBistra153::_gemeFarleecor
	int32_t ____gemeFarleecor_1;
	// System.UInt32 GarbageiOS.M_halljasBistra153::_senayGedurco
	uint32_t ____senayGedurco_2;
	// System.String GarbageiOS.M_halljasBistra153::_merpas
	String_t* ____merpas_3;
	// System.Boolean GarbageiOS.M_halljasBistra153::_kanoo
	bool ____kanoo_4;
	// System.UInt32 GarbageiOS.M_halljasBistra153::_jaiwouMalfapaw
	uint32_t ____jaiwouMalfapaw_5;
	// System.UInt32 GarbageiOS.M_halljasBistra153::_bayboufalCiboudo
	uint32_t ____bayboufalCiboudo_6;
	// System.Boolean GarbageiOS.M_halljasBistra153::_saisurNairseakoo
	bool ____saisurNairseakoo_7;
	// System.Boolean GarbageiOS.M_halljasBistra153::_dewasawLeeseqe
	bool ____dewasawLeeseqe_8;
	// System.Int32 GarbageiOS.M_halljasBistra153::_pajoujor
	int32_t ____pajoujor_9;
	// System.Boolean GarbageiOS.M_halljasBistra153::_rairweTrallpomou
	bool ____rairweTrallpomou_10;
	// System.Int32 GarbageiOS.M_halljasBistra153::_sesurzay
	int32_t ____sesurzay_11;
	// System.Single GarbageiOS.M_halljasBistra153::_tisorhi
	float ____tisorhi_12;
	// System.Single GarbageiOS.M_halljasBistra153::_temsalhea
	float ____temsalhea_13;
	// System.Boolean GarbageiOS.M_halljasBistra153::_walo
	bool ____walo_14;
	// System.Boolean GarbageiOS.M_halljasBistra153::_gortonurMemmisar
	bool ____gortonurMemmisar_15;
	// System.Single GarbageiOS.M_halljasBistra153::_zirtrearRutir
	float ____zirtrearRutir_16;
	// System.UInt32 GarbageiOS.M_halljasBistra153::_rallderbalYairfisdai
	uint32_t ____rallderbalYairfisdai_17;
	// System.Int32 GarbageiOS.M_halljasBistra153::_besejereCeledall
	int32_t ____besejereCeledall_18;
	// System.Int32 GarbageiOS.M_halljasBistra153::_kirmeChaheaji
	int32_t ____kirmeChaheaji_19;
	// System.Single GarbageiOS.M_halljasBistra153::_sirnaFeremiso
	float ____sirnaFeremiso_20;
	// System.String GarbageiOS.M_halljasBistra153::_ceacirDibai
	String_t* ____ceacirDibai_21;
	// System.String GarbageiOS.M_halljasBistra153::_sispelKaswhu
	String_t* ____sispelKaswhu_22;
	// System.Boolean GarbageiOS.M_halljasBistra153::_mownea
	bool ____mownea_23;
	// System.UInt32 GarbageiOS.M_halljasBistra153::_nassurlemMordese
	uint32_t ____nassurlemMordese_24;
	// System.Single GarbageiOS.M_halljasBistra153::_falldahoCarloo
	float ____falldahoCarloo_25;
	// System.Single GarbageiOS.M_halljasBistra153::_tore
	float ____tore_26;
	// System.UInt32 GarbageiOS.M_halljasBistra153::_merrallfi
	uint32_t ____merrallfi_27;
	// System.Boolean GarbageiOS.M_halljasBistra153::_histrelcisXooyor
	bool ____histrelcisXooyor_28;
	// System.String GarbageiOS.M_halljasBistra153::_yeetoBinamis
	String_t* ____yeetoBinamis_29;
	// System.Boolean GarbageiOS.M_halljasBistra153::_keafiRakuda
	bool ____keafiRakuda_30;
	// System.Int32 GarbageiOS.M_halljasBistra153::_nowsouSurwhasstu
	int32_t ____nowsouSurwhasstu_31;
	// System.Single GarbageiOS.M_halljasBistra153::_kalde
	float ____kalde_32;
	// System.Boolean GarbageiOS.M_halljasBistra153::_naspalneQidall
	bool ____naspalneQidall_33;
	// System.Single GarbageiOS.M_halljasBistra153::_vorlarfow
	float ____vorlarfow_34;
	// System.Int32 GarbageiOS.M_halljasBistra153::_houtrouRoufafe
	int32_t ____houtrouRoufafe_35;
	// System.UInt32 GarbageiOS.M_halljasBistra153::_kelaci
	uint32_t ____kelaci_36;
	// System.Int32 GarbageiOS.M_halljasBistra153::_coureaDiwel
	int32_t ____coureaDiwel_37;
	// System.Boolean GarbageiOS.M_halljasBistra153::_ceeva
	bool ____ceeva_38;
	// System.Boolean GarbageiOS.M_halljasBistra153::_yaysearceePairkemda
	bool ____yaysearceePairkemda_39;
	// System.String GarbageiOS.M_halljasBistra153::_drearceardair
	String_t* ____drearceardair_40;
	// System.Boolean GarbageiOS.M_halljasBistra153::_pereluLarsairce
	bool ____pereluLarsairce_41;

public:
	inline static int32_t get_offset_of__hidrerayDaltall_0() { return static_cast<int32_t>(offsetof(M_halljasBistra153_t4051408741, ____hidrerayDaltall_0)); }
	inline String_t* get__hidrerayDaltall_0() const { return ____hidrerayDaltall_0; }
	inline String_t** get_address_of__hidrerayDaltall_0() { return &____hidrerayDaltall_0; }
	inline void set__hidrerayDaltall_0(String_t* value)
	{
		____hidrerayDaltall_0 = value;
		Il2CppCodeGenWriteBarrier(&____hidrerayDaltall_0, value);
	}

	inline static int32_t get_offset_of__gemeFarleecor_1() { return static_cast<int32_t>(offsetof(M_halljasBistra153_t4051408741, ____gemeFarleecor_1)); }
	inline int32_t get__gemeFarleecor_1() const { return ____gemeFarleecor_1; }
	inline int32_t* get_address_of__gemeFarleecor_1() { return &____gemeFarleecor_1; }
	inline void set__gemeFarleecor_1(int32_t value)
	{
		____gemeFarleecor_1 = value;
	}

	inline static int32_t get_offset_of__senayGedurco_2() { return static_cast<int32_t>(offsetof(M_halljasBistra153_t4051408741, ____senayGedurco_2)); }
	inline uint32_t get__senayGedurco_2() const { return ____senayGedurco_2; }
	inline uint32_t* get_address_of__senayGedurco_2() { return &____senayGedurco_2; }
	inline void set__senayGedurco_2(uint32_t value)
	{
		____senayGedurco_2 = value;
	}

	inline static int32_t get_offset_of__merpas_3() { return static_cast<int32_t>(offsetof(M_halljasBistra153_t4051408741, ____merpas_3)); }
	inline String_t* get__merpas_3() const { return ____merpas_3; }
	inline String_t** get_address_of__merpas_3() { return &____merpas_3; }
	inline void set__merpas_3(String_t* value)
	{
		____merpas_3 = value;
		Il2CppCodeGenWriteBarrier(&____merpas_3, value);
	}

	inline static int32_t get_offset_of__kanoo_4() { return static_cast<int32_t>(offsetof(M_halljasBistra153_t4051408741, ____kanoo_4)); }
	inline bool get__kanoo_4() const { return ____kanoo_4; }
	inline bool* get_address_of__kanoo_4() { return &____kanoo_4; }
	inline void set__kanoo_4(bool value)
	{
		____kanoo_4 = value;
	}

	inline static int32_t get_offset_of__jaiwouMalfapaw_5() { return static_cast<int32_t>(offsetof(M_halljasBistra153_t4051408741, ____jaiwouMalfapaw_5)); }
	inline uint32_t get__jaiwouMalfapaw_5() const { return ____jaiwouMalfapaw_5; }
	inline uint32_t* get_address_of__jaiwouMalfapaw_5() { return &____jaiwouMalfapaw_5; }
	inline void set__jaiwouMalfapaw_5(uint32_t value)
	{
		____jaiwouMalfapaw_5 = value;
	}

	inline static int32_t get_offset_of__bayboufalCiboudo_6() { return static_cast<int32_t>(offsetof(M_halljasBistra153_t4051408741, ____bayboufalCiboudo_6)); }
	inline uint32_t get__bayboufalCiboudo_6() const { return ____bayboufalCiboudo_6; }
	inline uint32_t* get_address_of__bayboufalCiboudo_6() { return &____bayboufalCiboudo_6; }
	inline void set__bayboufalCiboudo_6(uint32_t value)
	{
		____bayboufalCiboudo_6 = value;
	}

	inline static int32_t get_offset_of__saisurNairseakoo_7() { return static_cast<int32_t>(offsetof(M_halljasBistra153_t4051408741, ____saisurNairseakoo_7)); }
	inline bool get__saisurNairseakoo_7() const { return ____saisurNairseakoo_7; }
	inline bool* get_address_of__saisurNairseakoo_7() { return &____saisurNairseakoo_7; }
	inline void set__saisurNairseakoo_7(bool value)
	{
		____saisurNairseakoo_7 = value;
	}

	inline static int32_t get_offset_of__dewasawLeeseqe_8() { return static_cast<int32_t>(offsetof(M_halljasBistra153_t4051408741, ____dewasawLeeseqe_8)); }
	inline bool get__dewasawLeeseqe_8() const { return ____dewasawLeeseqe_8; }
	inline bool* get_address_of__dewasawLeeseqe_8() { return &____dewasawLeeseqe_8; }
	inline void set__dewasawLeeseqe_8(bool value)
	{
		____dewasawLeeseqe_8 = value;
	}

	inline static int32_t get_offset_of__pajoujor_9() { return static_cast<int32_t>(offsetof(M_halljasBistra153_t4051408741, ____pajoujor_9)); }
	inline int32_t get__pajoujor_9() const { return ____pajoujor_9; }
	inline int32_t* get_address_of__pajoujor_9() { return &____pajoujor_9; }
	inline void set__pajoujor_9(int32_t value)
	{
		____pajoujor_9 = value;
	}

	inline static int32_t get_offset_of__rairweTrallpomou_10() { return static_cast<int32_t>(offsetof(M_halljasBistra153_t4051408741, ____rairweTrallpomou_10)); }
	inline bool get__rairweTrallpomou_10() const { return ____rairweTrallpomou_10; }
	inline bool* get_address_of__rairweTrallpomou_10() { return &____rairweTrallpomou_10; }
	inline void set__rairweTrallpomou_10(bool value)
	{
		____rairweTrallpomou_10 = value;
	}

	inline static int32_t get_offset_of__sesurzay_11() { return static_cast<int32_t>(offsetof(M_halljasBistra153_t4051408741, ____sesurzay_11)); }
	inline int32_t get__sesurzay_11() const { return ____sesurzay_11; }
	inline int32_t* get_address_of__sesurzay_11() { return &____sesurzay_11; }
	inline void set__sesurzay_11(int32_t value)
	{
		____sesurzay_11 = value;
	}

	inline static int32_t get_offset_of__tisorhi_12() { return static_cast<int32_t>(offsetof(M_halljasBistra153_t4051408741, ____tisorhi_12)); }
	inline float get__tisorhi_12() const { return ____tisorhi_12; }
	inline float* get_address_of__tisorhi_12() { return &____tisorhi_12; }
	inline void set__tisorhi_12(float value)
	{
		____tisorhi_12 = value;
	}

	inline static int32_t get_offset_of__temsalhea_13() { return static_cast<int32_t>(offsetof(M_halljasBistra153_t4051408741, ____temsalhea_13)); }
	inline float get__temsalhea_13() const { return ____temsalhea_13; }
	inline float* get_address_of__temsalhea_13() { return &____temsalhea_13; }
	inline void set__temsalhea_13(float value)
	{
		____temsalhea_13 = value;
	}

	inline static int32_t get_offset_of__walo_14() { return static_cast<int32_t>(offsetof(M_halljasBistra153_t4051408741, ____walo_14)); }
	inline bool get__walo_14() const { return ____walo_14; }
	inline bool* get_address_of__walo_14() { return &____walo_14; }
	inline void set__walo_14(bool value)
	{
		____walo_14 = value;
	}

	inline static int32_t get_offset_of__gortonurMemmisar_15() { return static_cast<int32_t>(offsetof(M_halljasBistra153_t4051408741, ____gortonurMemmisar_15)); }
	inline bool get__gortonurMemmisar_15() const { return ____gortonurMemmisar_15; }
	inline bool* get_address_of__gortonurMemmisar_15() { return &____gortonurMemmisar_15; }
	inline void set__gortonurMemmisar_15(bool value)
	{
		____gortonurMemmisar_15 = value;
	}

	inline static int32_t get_offset_of__zirtrearRutir_16() { return static_cast<int32_t>(offsetof(M_halljasBistra153_t4051408741, ____zirtrearRutir_16)); }
	inline float get__zirtrearRutir_16() const { return ____zirtrearRutir_16; }
	inline float* get_address_of__zirtrearRutir_16() { return &____zirtrearRutir_16; }
	inline void set__zirtrearRutir_16(float value)
	{
		____zirtrearRutir_16 = value;
	}

	inline static int32_t get_offset_of__rallderbalYairfisdai_17() { return static_cast<int32_t>(offsetof(M_halljasBistra153_t4051408741, ____rallderbalYairfisdai_17)); }
	inline uint32_t get__rallderbalYairfisdai_17() const { return ____rallderbalYairfisdai_17; }
	inline uint32_t* get_address_of__rallderbalYairfisdai_17() { return &____rallderbalYairfisdai_17; }
	inline void set__rallderbalYairfisdai_17(uint32_t value)
	{
		____rallderbalYairfisdai_17 = value;
	}

	inline static int32_t get_offset_of__besejereCeledall_18() { return static_cast<int32_t>(offsetof(M_halljasBistra153_t4051408741, ____besejereCeledall_18)); }
	inline int32_t get__besejereCeledall_18() const { return ____besejereCeledall_18; }
	inline int32_t* get_address_of__besejereCeledall_18() { return &____besejereCeledall_18; }
	inline void set__besejereCeledall_18(int32_t value)
	{
		____besejereCeledall_18 = value;
	}

	inline static int32_t get_offset_of__kirmeChaheaji_19() { return static_cast<int32_t>(offsetof(M_halljasBistra153_t4051408741, ____kirmeChaheaji_19)); }
	inline int32_t get__kirmeChaheaji_19() const { return ____kirmeChaheaji_19; }
	inline int32_t* get_address_of__kirmeChaheaji_19() { return &____kirmeChaheaji_19; }
	inline void set__kirmeChaheaji_19(int32_t value)
	{
		____kirmeChaheaji_19 = value;
	}

	inline static int32_t get_offset_of__sirnaFeremiso_20() { return static_cast<int32_t>(offsetof(M_halljasBistra153_t4051408741, ____sirnaFeremiso_20)); }
	inline float get__sirnaFeremiso_20() const { return ____sirnaFeremiso_20; }
	inline float* get_address_of__sirnaFeremiso_20() { return &____sirnaFeremiso_20; }
	inline void set__sirnaFeremiso_20(float value)
	{
		____sirnaFeremiso_20 = value;
	}

	inline static int32_t get_offset_of__ceacirDibai_21() { return static_cast<int32_t>(offsetof(M_halljasBistra153_t4051408741, ____ceacirDibai_21)); }
	inline String_t* get__ceacirDibai_21() const { return ____ceacirDibai_21; }
	inline String_t** get_address_of__ceacirDibai_21() { return &____ceacirDibai_21; }
	inline void set__ceacirDibai_21(String_t* value)
	{
		____ceacirDibai_21 = value;
		Il2CppCodeGenWriteBarrier(&____ceacirDibai_21, value);
	}

	inline static int32_t get_offset_of__sispelKaswhu_22() { return static_cast<int32_t>(offsetof(M_halljasBistra153_t4051408741, ____sispelKaswhu_22)); }
	inline String_t* get__sispelKaswhu_22() const { return ____sispelKaswhu_22; }
	inline String_t** get_address_of__sispelKaswhu_22() { return &____sispelKaswhu_22; }
	inline void set__sispelKaswhu_22(String_t* value)
	{
		____sispelKaswhu_22 = value;
		Il2CppCodeGenWriteBarrier(&____sispelKaswhu_22, value);
	}

	inline static int32_t get_offset_of__mownea_23() { return static_cast<int32_t>(offsetof(M_halljasBistra153_t4051408741, ____mownea_23)); }
	inline bool get__mownea_23() const { return ____mownea_23; }
	inline bool* get_address_of__mownea_23() { return &____mownea_23; }
	inline void set__mownea_23(bool value)
	{
		____mownea_23 = value;
	}

	inline static int32_t get_offset_of__nassurlemMordese_24() { return static_cast<int32_t>(offsetof(M_halljasBistra153_t4051408741, ____nassurlemMordese_24)); }
	inline uint32_t get__nassurlemMordese_24() const { return ____nassurlemMordese_24; }
	inline uint32_t* get_address_of__nassurlemMordese_24() { return &____nassurlemMordese_24; }
	inline void set__nassurlemMordese_24(uint32_t value)
	{
		____nassurlemMordese_24 = value;
	}

	inline static int32_t get_offset_of__falldahoCarloo_25() { return static_cast<int32_t>(offsetof(M_halljasBistra153_t4051408741, ____falldahoCarloo_25)); }
	inline float get__falldahoCarloo_25() const { return ____falldahoCarloo_25; }
	inline float* get_address_of__falldahoCarloo_25() { return &____falldahoCarloo_25; }
	inline void set__falldahoCarloo_25(float value)
	{
		____falldahoCarloo_25 = value;
	}

	inline static int32_t get_offset_of__tore_26() { return static_cast<int32_t>(offsetof(M_halljasBistra153_t4051408741, ____tore_26)); }
	inline float get__tore_26() const { return ____tore_26; }
	inline float* get_address_of__tore_26() { return &____tore_26; }
	inline void set__tore_26(float value)
	{
		____tore_26 = value;
	}

	inline static int32_t get_offset_of__merrallfi_27() { return static_cast<int32_t>(offsetof(M_halljasBistra153_t4051408741, ____merrallfi_27)); }
	inline uint32_t get__merrallfi_27() const { return ____merrallfi_27; }
	inline uint32_t* get_address_of__merrallfi_27() { return &____merrallfi_27; }
	inline void set__merrallfi_27(uint32_t value)
	{
		____merrallfi_27 = value;
	}

	inline static int32_t get_offset_of__histrelcisXooyor_28() { return static_cast<int32_t>(offsetof(M_halljasBistra153_t4051408741, ____histrelcisXooyor_28)); }
	inline bool get__histrelcisXooyor_28() const { return ____histrelcisXooyor_28; }
	inline bool* get_address_of__histrelcisXooyor_28() { return &____histrelcisXooyor_28; }
	inline void set__histrelcisXooyor_28(bool value)
	{
		____histrelcisXooyor_28 = value;
	}

	inline static int32_t get_offset_of__yeetoBinamis_29() { return static_cast<int32_t>(offsetof(M_halljasBistra153_t4051408741, ____yeetoBinamis_29)); }
	inline String_t* get__yeetoBinamis_29() const { return ____yeetoBinamis_29; }
	inline String_t** get_address_of__yeetoBinamis_29() { return &____yeetoBinamis_29; }
	inline void set__yeetoBinamis_29(String_t* value)
	{
		____yeetoBinamis_29 = value;
		Il2CppCodeGenWriteBarrier(&____yeetoBinamis_29, value);
	}

	inline static int32_t get_offset_of__keafiRakuda_30() { return static_cast<int32_t>(offsetof(M_halljasBistra153_t4051408741, ____keafiRakuda_30)); }
	inline bool get__keafiRakuda_30() const { return ____keafiRakuda_30; }
	inline bool* get_address_of__keafiRakuda_30() { return &____keafiRakuda_30; }
	inline void set__keafiRakuda_30(bool value)
	{
		____keafiRakuda_30 = value;
	}

	inline static int32_t get_offset_of__nowsouSurwhasstu_31() { return static_cast<int32_t>(offsetof(M_halljasBistra153_t4051408741, ____nowsouSurwhasstu_31)); }
	inline int32_t get__nowsouSurwhasstu_31() const { return ____nowsouSurwhasstu_31; }
	inline int32_t* get_address_of__nowsouSurwhasstu_31() { return &____nowsouSurwhasstu_31; }
	inline void set__nowsouSurwhasstu_31(int32_t value)
	{
		____nowsouSurwhasstu_31 = value;
	}

	inline static int32_t get_offset_of__kalde_32() { return static_cast<int32_t>(offsetof(M_halljasBistra153_t4051408741, ____kalde_32)); }
	inline float get__kalde_32() const { return ____kalde_32; }
	inline float* get_address_of__kalde_32() { return &____kalde_32; }
	inline void set__kalde_32(float value)
	{
		____kalde_32 = value;
	}

	inline static int32_t get_offset_of__naspalneQidall_33() { return static_cast<int32_t>(offsetof(M_halljasBistra153_t4051408741, ____naspalneQidall_33)); }
	inline bool get__naspalneQidall_33() const { return ____naspalneQidall_33; }
	inline bool* get_address_of__naspalneQidall_33() { return &____naspalneQidall_33; }
	inline void set__naspalneQidall_33(bool value)
	{
		____naspalneQidall_33 = value;
	}

	inline static int32_t get_offset_of__vorlarfow_34() { return static_cast<int32_t>(offsetof(M_halljasBistra153_t4051408741, ____vorlarfow_34)); }
	inline float get__vorlarfow_34() const { return ____vorlarfow_34; }
	inline float* get_address_of__vorlarfow_34() { return &____vorlarfow_34; }
	inline void set__vorlarfow_34(float value)
	{
		____vorlarfow_34 = value;
	}

	inline static int32_t get_offset_of__houtrouRoufafe_35() { return static_cast<int32_t>(offsetof(M_halljasBistra153_t4051408741, ____houtrouRoufafe_35)); }
	inline int32_t get__houtrouRoufafe_35() const { return ____houtrouRoufafe_35; }
	inline int32_t* get_address_of__houtrouRoufafe_35() { return &____houtrouRoufafe_35; }
	inline void set__houtrouRoufafe_35(int32_t value)
	{
		____houtrouRoufafe_35 = value;
	}

	inline static int32_t get_offset_of__kelaci_36() { return static_cast<int32_t>(offsetof(M_halljasBistra153_t4051408741, ____kelaci_36)); }
	inline uint32_t get__kelaci_36() const { return ____kelaci_36; }
	inline uint32_t* get_address_of__kelaci_36() { return &____kelaci_36; }
	inline void set__kelaci_36(uint32_t value)
	{
		____kelaci_36 = value;
	}

	inline static int32_t get_offset_of__coureaDiwel_37() { return static_cast<int32_t>(offsetof(M_halljasBistra153_t4051408741, ____coureaDiwel_37)); }
	inline int32_t get__coureaDiwel_37() const { return ____coureaDiwel_37; }
	inline int32_t* get_address_of__coureaDiwel_37() { return &____coureaDiwel_37; }
	inline void set__coureaDiwel_37(int32_t value)
	{
		____coureaDiwel_37 = value;
	}

	inline static int32_t get_offset_of__ceeva_38() { return static_cast<int32_t>(offsetof(M_halljasBistra153_t4051408741, ____ceeva_38)); }
	inline bool get__ceeva_38() const { return ____ceeva_38; }
	inline bool* get_address_of__ceeva_38() { return &____ceeva_38; }
	inline void set__ceeva_38(bool value)
	{
		____ceeva_38 = value;
	}

	inline static int32_t get_offset_of__yaysearceePairkemda_39() { return static_cast<int32_t>(offsetof(M_halljasBistra153_t4051408741, ____yaysearceePairkemda_39)); }
	inline bool get__yaysearceePairkemda_39() const { return ____yaysearceePairkemda_39; }
	inline bool* get_address_of__yaysearceePairkemda_39() { return &____yaysearceePairkemda_39; }
	inline void set__yaysearceePairkemda_39(bool value)
	{
		____yaysearceePairkemda_39 = value;
	}

	inline static int32_t get_offset_of__drearceardair_40() { return static_cast<int32_t>(offsetof(M_halljasBistra153_t4051408741, ____drearceardair_40)); }
	inline String_t* get__drearceardair_40() const { return ____drearceardair_40; }
	inline String_t** get_address_of__drearceardair_40() { return &____drearceardair_40; }
	inline void set__drearceardair_40(String_t* value)
	{
		____drearceardair_40 = value;
		Il2CppCodeGenWriteBarrier(&____drearceardair_40, value);
	}

	inline static int32_t get_offset_of__pereluLarsairce_41() { return static_cast<int32_t>(offsetof(M_halljasBistra153_t4051408741, ____pereluLarsairce_41)); }
	inline bool get__pereluLarsairce_41() const { return ____pereluLarsairce_41; }
	inline bool* get_address_of__pereluLarsairce_41() { return &____pereluLarsairce_41; }
	inline void set__pereluLarsairce_41(bool value)
	{
		____pereluLarsairce_41 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
