﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ParticleScaler
struct ParticleScaler_t2808007342;

#include "codegen/il2cpp-codegen.h"

// System.Void ParticleScaler::.ctor()
extern "C"  void ParticleScaler__ctor_m631580077 (ParticleScaler_t2808007342 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ParticleScaler::Start()
extern "C"  void ParticleScaler_Start_m3873685165 (ParticleScaler_t2808007342 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ParticleScaler::Update()
extern "C"  void ParticleScaler_Update_m4125975296 (ParticleScaler_t2808007342 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ParticleScaler::ScaleShurikenSystems(System.Single)
extern "C"  void ParticleScaler_ScaleShurikenSystems_m1942731959 (ParticleScaler_t2808007342 * __this, float ___scaleFactor0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ParticleScaler::ScaleLegacySystems(System.Single)
extern "C"  void ParticleScaler_ScaleLegacySystems_m1489150083 (ParticleScaler_t2808007342 * __this, float ___scaleFactor0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ParticleScaler::ScaleTrailRenderers(System.Single)
extern "C"  void ParticleScaler_ScaleTrailRenderers_m1552785196 (ParticleScaler_t2808007342 * __this, float ___scaleFactor0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
