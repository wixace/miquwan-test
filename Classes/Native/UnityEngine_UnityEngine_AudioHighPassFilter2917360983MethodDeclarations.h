﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.AudioHighPassFilter
struct AudioHighPassFilter_t2917360983;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.AudioHighPassFilter::.ctor()
extern "C"  void AudioHighPassFilter__ctor_m1205189336 (AudioHighPassFilter_t2917360983 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.AudioHighPassFilter::get_cutoffFrequency()
extern "C"  float AudioHighPassFilter_get_cutoffFrequency_m4209560808 (AudioHighPassFilter_t2917360983 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioHighPassFilter::set_cutoffFrequency(System.Single)
extern "C"  void AudioHighPassFilter_set_cutoffFrequency_m3122042307 (AudioHighPassFilter_t2917360983 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.AudioHighPassFilter::get_highpassResonanceQ()
extern "C"  float AudioHighPassFilter_get_highpassResonanceQ_m3628151711 (AudioHighPassFilter_t2917360983 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioHighPassFilter::set_highpassResonanceQ(System.Single)
extern "C"  void AudioHighPassFilter_set_highpassResonanceQ_m3735666796 (AudioHighPassFilter_t2917360983 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
