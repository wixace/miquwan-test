﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ProtoBuf.Meta.MetaType/Comparer
struct Comparer_t255243159;
// System.Object
struct Il2CppObject;
// ProtoBuf.Meta.MetaType
struct MetaType_t448283965;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_ProtoBuf_Meta_MetaType448283965.h"

// System.Void ProtoBuf.Meta.MetaType/Comparer::.ctor()
extern "C"  void Comparer__ctor_m349265332 (Comparer_t255243159 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.MetaType/Comparer::.cctor()
extern "C"  void Comparer__cctor_m1755194489 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ProtoBuf.Meta.MetaType/Comparer::Compare(System.Object,System.Object)
extern "C"  int32_t Comparer_Compare_m1982221181 (Comparer_t255243159 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ProtoBuf.Meta.MetaType/Comparer::Compare(ProtoBuf.Meta.MetaType,ProtoBuf.Meta.MetaType)
extern "C"  int32_t Comparer_Compare_m3250205441 (Comparer_t255243159 * __this, MetaType_t448283965 * ___x0, MetaType_t448283965 * ___y1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
