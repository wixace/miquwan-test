﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_RenderTextureGenerated
struct UnityEngine_RenderTextureGenerated_t3251476434;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"

// System.Void UnityEngine_RenderTextureGenerated::.ctor()
extern "C"  void UnityEngine_RenderTextureGenerated__ctor_m3706578185 (UnityEngine_RenderTextureGenerated_t3251476434 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RenderTextureGenerated::RenderTexture_RenderTexture1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RenderTextureGenerated_RenderTexture_RenderTexture1_m1303087749 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RenderTextureGenerated::RenderTexture_RenderTexture2(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RenderTextureGenerated_RenderTexture_RenderTexture2_m2547852230 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RenderTextureGenerated::RenderTexture_RenderTexture3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RenderTextureGenerated_RenderTexture_RenderTexture3_m3792616711 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RenderTextureGenerated::RenderTexture_width(JSVCall)
extern "C"  void UnityEngine_RenderTextureGenerated_RenderTexture_width_m3890021876 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RenderTextureGenerated::RenderTexture_height(JSVCall)
extern "C"  void UnityEngine_RenderTextureGenerated_RenderTexture_height_m987037515 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RenderTextureGenerated::RenderTexture_depth(JSVCall)
extern "C"  void UnityEngine_RenderTextureGenerated_RenderTexture_depth_m3333338519 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RenderTextureGenerated::RenderTexture_isPowerOfTwo(JSVCall)
extern "C"  void UnityEngine_RenderTextureGenerated_RenderTexture_isPowerOfTwo_m2700252440 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RenderTextureGenerated::RenderTexture_sRGB(JSVCall)
extern "C"  void UnityEngine_RenderTextureGenerated_RenderTexture_sRGB_m244853112 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RenderTextureGenerated::RenderTexture_format(JSVCall)
extern "C"  void UnityEngine_RenderTextureGenerated_RenderTexture_format_m3628876411 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RenderTextureGenerated::RenderTexture_useMipMap(JSVCall)
extern "C"  void UnityEngine_RenderTextureGenerated_RenderTexture_useMipMap_m2395688203 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RenderTextureGenerated::RenderTexture_generateMips(JSVCall)
extern "C"  void UnityEngine_RenderTextureGenerated_RenderTexture_generateMips_m1644477022 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RenderTextureGenerated::RenderTexture_isCubemap(JSVCall)
extern "C"  void UnityEngine_RenderTextureGenerated_RenderTexture_isCubemap_m4133533469 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RenderTextureGenerated::RenderTexture_isVolume(JSVCall)
extern "C"  void UnityEngine_RenderTextureGenerated_RenderTexture_isVolume_m4266166574 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RenderTextureGenerated::RenderTexture_volumeDepth(JSVCall)
extern "C"  void UnityEngine_RenderTextureGenerated_RenderTexture_volumeDepth_m1871875153 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RenderTextureGenerated::RenderTexture_antiAliasing(JSVCall)
extern "C"  void UnityEngine_RenderTextureGenerated_RenderTexture_antiAliasing_m3329879294 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RenderTextureGenerated::RenderTexture_enableRandomWrite(JSVCall)
extern "C"  void UnityEngine_RenderTextureGenerated_RenderTexture_enableRandomWrite_m3062412225 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RenderTextureGenerated::RenderTexture_colorBuffer(JSVCall)
extern "C"  void UnityEngine_RenderTextureGenerated_RenderTexture_colorBuffer_m669800407 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RenderTextureGenerated::RenderTexture_depthBuffer(JSVCall)
extern "C"  void UnityEngine_RenderTextureGenerated_RenderTexture_depthBuffer_m3704393847 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RenderTextureGenerated::RenderTexture_active(JSVCall)
extern "C"  void UnityEngine_RenderTextureGenerated_RenderTexture_active_m626146380 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RenderTextureGenerated::RenderTexture_Create(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RenderTextureGenerated_RenderTexture_Create_m993163605 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RenderTextureGenerated::RenderTexture_DiscardContents__Boolean__Boolean(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RenderTextureGenerated_RenderTexture_DiscardContents__Boolean__Boolean_m2740897441 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RenderTextureGenerated::RenderTexture_DiscardContents(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RenderTextureGenerated_RenderTexture_DiscardContents_m2140102049 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RenderTextureGenerated::RenderTexture_GetTexelOffset(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RenderTextureGenerated_RenderTexture_GetTexelOffset_m2849554212 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RenderTextureGenerated::RenderTexture_IsCreated(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RenderTextureGenerated_RenderTexture_IsCreated_m3084975015 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RenderTextureGenerated::RenderTexture_MarkRestoreExpected(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RenderTextureGenerated_RenderTexture_MarkRestoreExpected_m3051093474 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RenderTextureGenerated::RenderTexture_Release(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RenderTextureGenerated_RenderTexture_Release_m52958864 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RenderTextureGenerated::RenderTexture_SetGlobalShaderProperty__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RenderTextureGenerated_RenderTexture_SetGlobalShaderProperty__String_m854935993 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RenderTextureGenerated::RenderTexture_GetTemporary__Int32__Int32__Int32__RenderTextureFormat__RenderTextureReadWrite__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RenderTextureGenerated_RenderTexture_GetTemporary__Int32__Int32__Int32__RenderTextureFormat__RenderTextureReadWrite__Int32_m3932347370 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RenderTextureGenerated::RenderTexture_GetTemporary__Int32__Int32__Int32__RenderTextureFormat__RenderTextureReadWrite(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RenderTextureGenerated_RenderTexture_GetTemporary__Int32__Int32__Int32__RenderTextureFormat__RenderTextureReadWrite_m3067765926 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RenderTextureGenerated::RenderTexture_GetTemporary__Int32__Int32__Int32__RenderTextureFormat(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RenderTextureGenerated_RenderTexture_GetTemporary__Int32__Int32__Int32__RenderTextureFormat_m1091376098 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RenderTextureGenerated::RenderTexture_GetTemporary__Int32__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RenderTextureGenerated_RenderTexture_GetTemporary__Int32__Int32__Int32_m3466778908 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RenderTextureGenerated::RenderTexture_GetTemporary__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RenderTextureGenerated_RenderTexture_GetTemporary__Int32__Int32_m3915578292 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RenderTextureGenerated::RenderTexture_ReleaseTemporary__RenderTexture(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RenderTextureGenerated_RenderTexture_ReleaseTemporary__RenderTexture_m2468901604 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RenderTextureGenerated::RenderTexture_SupportsStencil__RenderTexture(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RenderTextureGenerated_RenderTexture_SupportsStencil__RenderTexture_m2045239974 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RenderTextureGenerated::__Register()
extern "C"  void UnityEngine_RenderTextureGenerated___Register_m1777751390 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_RenderTextureGenerated::ilo_getInt321(System.Int32)
extern "C"  int32_t UnityEngine_RenderTextureGenerated_ilo_getInt321_m3332948560 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RenderTextureGenerated::ilo_addJSCSRel2(System.Int32,System.Object)
extern "C"  void UnityEngine_RenderTextureGenerated_ilo_addJSCSRel2_m4066725830 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_RenderTextureGenerated::ilo_getObject3(System.Int32)
extern "C"  int32_t UnityEngine_RenderTextureGenerated_ilo_getObject3_m3706107563 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RenderTextureGenerated::ilo_setInt324(System.Int32,System.Int32)
extern "C"  void UnityEngine_RenderTextureGenerated_ilo_setInt324_m3546499594 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RenderTextureGenerated::ilo_setBooleanS5(System.Int32,System.Boolean)
extern "C"  void UnityEngine_RenderTextureGenerated_ilo_setBooleanS5_m73721654 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RenderTextureGenerated::ilo_getBooleanS6(System.Int32)
extern "C"  bool UnityEngine_RenderTextureGenerated_ilo_getBooleanS6_m311199720 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_RenderTextureGenerated::ilo_getObject7(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_RenderTextureGenerated_ilo_getObject7_m4153449868 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RenderTextureGenerated::ilo_setVector2S8(System.Int32,UnityEngine.Vector2)
extern "C"  void UnityEngine_RenderTextureGenerated_ilo_setVector2S8_m3956150341 (Il2CppObject * __this /* static, unused */, int32_t ___e0, Vector2_t4282066565  ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine_RenderTextureGenerated::ilo_getStringS9(System.Int32)
extern "C"  String_t* UnityEngine_RenderTextureGenerated_ilo_getStringS9_m1201427919 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_RenderTextureGenerated::ilo_getEnum10(System.Int32)
extern "C"  int32_t UnityEngine_RenderTextureGenerated_ilo_getEnum10_m4156706573 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_RenderTextureGenerated::ilo_setObject11(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UnityEngine_RenderTextureGenerated_ilo_setObject11_m1491211740 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
