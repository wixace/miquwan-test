﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HitTarget
struct HitTarget_t3687896804;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void HitTarget::.ctor()
extern "C"  void HitTarget__ctor_m3779313415 (HitTarget_t3687896804 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HitTarget::GetJsonStr()
extern "C"  String_t* HitTarget_GetJsonStr_m2570954253 (HitTarget_t3687896804 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
