﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2953159047MethodDeclarations.h"

// System.Void System.Array/InternalEnumerator`1<T4MLodObjSC>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m3450998168(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t1070600679 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m2616641763_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<T4MLodObjSC>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m223760648(__this, method) ((  void (*) (InternalEnumerator_1_t1070600679 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2224260061_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<T4MLodObjSC>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4155572222(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t1070600679 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m390763987_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<T4MLodObjSC>::Dispose()
#define InternalEnumerator_1_Dispose_m27211119(__this, method) ((  void (*) (InternalEnumerator_1_t1070600679 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2760671866_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<T4MLodObjSC>::MoveNext()
#define InternalEnumerator_1_MoveNext_m1488308920(__this, method) ((  bool (*) (InternalEnumerator_1_t1070600679 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m3716548237_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<T4MLodObjSC>::get_Current()
#define InternalEnumerator_1_get_Current_m1404811969(__this, method) ((  T4MLodObjSC_t2288258003 * (*) (InternalEnumerator_1_t1070600679 *, const MethodInfo*))InternalEnumerator_1_get_Current_m2178852364_gshared)(__this, method)
