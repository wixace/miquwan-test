﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen926441464.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22144098788.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Pathfinding.Int2,System.Int32>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m650146672_gshared (InternalEnumerator_1_t926441464 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m650146672(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t926441464 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m650146672_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Pathfinding.Int2,System.Int32>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m396930096_gshared (InternalEnumerator_1_t926441464 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m396930096(__this, method) ((  void (*) (InternalEnumerator_1_t926441464 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m396930096_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Pathfinding.Int2,System.Int32>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3159549788_gshared (InternalEnumerator_1_t926441464 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3159549788(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t926441464 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3159549788_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Pathfinding.Int2,System.Int32>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1401063751_gshared (InternalEnumerator_1_t926441464 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m1401063751(__this, method) ((  void (*) (InternalEnumerator_1_t926441464 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1401063751_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Pathfinding.Int2,System.Int32>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m874917916_gshared (InternalEnumerator_1_t926441464 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m874917916(__this, method) ((  bool (*) (InternalEnumerator_1_t926441464 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m874917916_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Pathfinding.Int2,System.Int32>>::get_Current()
extern "C"  KeyValuePair_2_t2144098788  InternalEnumerator_1_get_Current_m2094746359_gshared (InternalEnumerator_1_t926441464 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m2094746359(__this, method) ((  KeyValuePair_2_t2144098788  (*) (InternalEnumerator_1_t926441464 *, const MethodInfo*))InternalEnumerator_1_get_Current_m2094746359_gshared)(__this, method)
