﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1457307743.h"
#include "mscorlib_System_Array1146569071.h"
#include "UnityEngine_UnityEngine_ClothSphereColliderPair2674965067.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.ClothSphereColliderPair>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2911954819_gshared (InternalEnumerator_1_t1457307743 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m2911954819(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t1457307743 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m2911954819_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.ClothSphereColliderPair>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2231279933_gshared (InternalEnumerator_1_t1457307743 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2231279933(__this, method) ((  void (*) (InternalEnumerator_1_t1457307743 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2231279933_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.ClothSphereColliderPair>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m970278259_gshared (InternalEnumerator_1_t1457307743 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m970278259(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t1457307743 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m970278259_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.ClothSphereColliderPair>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1608411418_gshared (InternalEnumerator_1_t1457307743 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m1608411418(__this, method) ((  void (*) (InternalEnumerator_1_t1457307743 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1608411418_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.ClothSphereColliderPair>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1119626861_gshared (InternalEnumerator_1_t1457307743 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m1119626861(__this, method) ((  bool (*) (InternalEnumerator_1_t1457307743 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1119626861_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.ClothSphereColliderPair>::get_Current()
extern "C"  ClothSphereColliderPair_t2674965067  InternalEnumerator_1_get_Current_m1541705196_gshared (InternalEnumerator_1_t1457307743 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1541705196(__this, method) ((  ClothSphereColliderPair_t2674965067  (*) (InternalEnumerator_1_t1457307743 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1541705196_gshared)(__this, method)
