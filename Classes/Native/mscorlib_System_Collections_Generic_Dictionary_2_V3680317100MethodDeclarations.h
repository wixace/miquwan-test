﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Object>
struct Dictionary_2_t1453516396;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V3680317100.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Pathfinding.Int3,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m691379919_gshared (Enumerator_t3680317100 * __this, Dictionary_2_t1453516396 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m691379919(__this, ___host0, method) ((  void (*) (Enumerator_t3680317100 *, Dictionary_2_t1453516396 *, const MethodInfo*))Enumerator__ctor_m691379919_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Pathfinding.Int3,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2045339260_gshared (Enumerator_t3680317100 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m2045339260(__this, method) ((  Il2CppObject * (*) (Enumerator_t3680317100 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2045339260_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Pathfinding.Int3,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3410325126_gshared (Enumerator_t3680317100 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m3410325126(__this, method) ((  void (*) (Enumerator_t3680317100 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3410325126_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Pathfinding.Int3,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m3351234353_gshared (Enumerator_t3680317100 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m3351234353(__this, method) ((  void (*) (Enumerator_t3680317100 *, const MethodInfo*))Enumerator_Dispose_m3351234353_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Pathfinding.Int3,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3318322230_gshared (Enumerator_t3680317100 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m3318322230(__this, method) ((  bool (*) (Enumerator_t3680317100 *, const MethodInfo*))Enumerator_MoveNext_m3318322230_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Pathfinding.Int3,System.Object>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m2476907572_gshared (Enumerator_t3680317100 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m2476907572(__this, method) ((  Il2CppObject * (*) (Enumerator_t3680317100 *, const MethodInfo*))Enumerator_get_Current_m2476907572_gshared)(__this, method)
