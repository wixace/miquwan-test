﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AIFollow
struct AIFollow_t4086657529;
// Pathfinding.Path
struct Path_t1974241691;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// Seeker
struct Seeker_t2472610117;
// OnPathDelegate
struct OnPathDelegate_t598607977;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_Path1974241691.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_AIFollow4086657529.h"
#include "AssemblyU2DCSharp_Seeker2472610117.h"
#include "AssemblyU2DCSharp_OnPathDelegate598607977.h"

// System.Void AIFollow::.ctor()
extern "C"  void AIFollow__ctor_m1391031490 (AIFollow_t4086657529 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AIFollow::Start()
extern "C"  void AIFollow_Start_m338169282 (AIFollow_t4086657529 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AIFollow::Reset()
extern "C"  void AIFollow_Reset_m3332431727 (AIFollow_t4086657529 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AIFollow::OnPathComplete(Pathfinding.Path)
extern "C"  void AIFollow_OnPathComplete_m1576501584 (AIFollow_t4086657529 * __this, Path_t1974241691 * ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator AIFollow::WaitToRepath()
extern "C"  Il2CppObject * AIFollow_WaitToRepath_m2344476498 (AIFollow_t4086657529 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AIFollow::Stop()
extern "C"  void AIFollow_Stop_m1950984132 (AIFollow_t4086657529 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AIFollow::Resume()
extern "C"  void AIFollow_Resume_m240794863 (AIFollow_t4086657529 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AIFollow::Repath()
extern "C"  void AIFollow_Repath_m136648410 (AIFollow_t4086657529 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AIFollow::PathToTarget(UnityEngine.Vector3)
extern "C"  void AIFollow_PathToTarget_m2202349254 (AIFollow_t4086657529 * __this, Vector3_t4282066566  ___targetPoint0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AIFollow::ReachedEndOfPath()
extern "C"  void AIFollow_ReachedEndOfPath_m326612903 (AIFollow_t4086657529 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AIFollow::Update()
extern "C"  void AIFollow_Update_m1899165323 (AIFollow_t4086657529 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AIFollow::OnDrawGizmos()
extern "C"  void AIFollow_OnDrawGizmos_m3992591902 (AIFollow_t4086657529 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AIFollow::ilo_Repath1(AIFollow)
extern "C"  void AIFollow_ilo_Repath1_m1398856205 (Il2CppObject * __this /* static, unused */, AIFollow_t4086657529 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AIFollow::ilo_get_error2(Pathfinding.Path)
extern "C"  bool AIFollow_ilo_get_error2_m4076916045 (Il2CppObject * __this /* static, unused */, Path_t1974241691 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single AIFollow::ilo_DistancePointSegmentStrict3(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  float AIFollow_ilo_DistancePointSegmentStrict3_m3991708748 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___a0, Vector3_t4282066566  ___b1, Vector3_t4282066566  ___p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Path AIFollow::ilo_StartPath4(Seeker,Pathfinding.Path,OnPathDelegate,System.Int32)
extern "C"  Path_t1974241691 * AIFollow_ilo_StartPath4_m691303974 (Il2CppObject * __this /* static, unused */, Seeker_t2472610117 * ____this0, Path_t1974241691 * ___p1, OnPathDelegate_t598607977 * ___callback2, int32_t ___graphMask3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AIFollow::ilo_ReachedEndOfPath5(AIFollow)
extern "C"  void AIFollow_ilo_ReachedEndOfPath5_m4066764452 (Il2CppObject * __this /* static, unused */, AIFollow_t4086657529 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
