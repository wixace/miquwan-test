﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Vector2[]
struct Vector2U5BU5D_t4024180168;

#include "AssemblyU2DCSharp_UIWidget769069560.h"
#include "AssemblyU2DCSharp_UIBasicSprite_Type741864970.h"
#include "AssemblyU2DCSharp_UIBasicSprite_FillDirection3514300524.h"
#include "AssemblyU2DCSharp_UIBasicSprite_Flip741435197.h"
#include "UnityEngine_UnityEngine_Rect4241904616.h"
#include "AssemblyU2DCSharp_UIBasicSprite_AdvancedType4228915532.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIBasicSprite
struct  UIBasicSprite_t2501337439  : public UIWidget_t769069560
{
public:
	// UIBasicSprite/Type UIBasicSprite::mType
	int32_t ___mType_53;
	// UIBasicSprite/FillDirection UIBasicSprite::mFillDirection
	int32_t ___mFillDirection_54;
	// System.Single UIBasicSprite::mFillAmount
	float ___mFillAmount_55;
	// System.Boolean UIBasicSprite::mInvert
	bool ___mInvert_56;
	// UIBasicSprite/Flip UIBasicSprite::mFlip
	int32_t ___mFlip_57;
	// UnityEngine.Rect UIBasicSprite::mInnerUV
	Rect_t4241904616  ___mInnerUV_58;
	// UnityEngine.Rect UIBasicSprite::mOuterUV
	Rect_t4241904616  ___mOuterUV_59;
	// UIBasicSprite/AdvancedType UIBasicSprite::centerType
	int32_t ___centerType_60;
	// UIBasicSprite/AdvancedType UIBasicSprite::leftType
	int32_t ___leftType_61;
	// UIBasicSprite/AdvancedType UIBasicSprite::rightType
	int32_t ___rightType_62;
	// UIBasicSprite/AdvancedType UIBasicSprite::bottomType
	int32_t ___bottomType_63;
	// UIBasicSprite/AdvancedType UIBasicSprite::topType
	int32_t ___topType_64;

public:
	inline static int32_t get_offset_of_mType_53() { return static_cast<int32_t>(offsetof(UIBasicSprite_t2501337439, ___mType_53)); }
	inline int32_t get_mType_53() const { return ___mType_53; }
	inline int32_t* get_address_of_mType_53() { return &___mType_53; }
	inline void set_mType_53(int32_t value)
	{
		___mType_53 = value;
	}

	inline static int32_t get_offset_of_mFillDirection_54() { return static_cast<int32_t>(offsetof(UIBasicSprite_t2501337439, ___mFillDirection_54)); }
	inline int32_t get_mFillDirection_54() const { return ___mFillDirection_54; }
	inline int32_t* get_address_of_mFillDirection_54() { return &___mFillDirection_54; }
	inline void set_mFillDirection_54(int32_t value)
	{
		___mFillDirection_54 = value;
	}

	inline static int32_t get_offset_of_mFillAmount_55() { return static_cast<int32_t>(offsetof(UIBasicSprite_t2501337439, ___mFillAmount_55)); }
	inline float get_mFillAmount_55() const { return ___mFillAmount_55; }
	inline float* get_address_of_mFillAmount_55() { return &___mFillAmount_55; }
	inline void set_mFillAmount_55(float value)
	{
		___mFillAmount_55 = value;
	}

	inline static int32_t get_offset_of_mInvert_56() { return static_cast<int32_t>(offsetof(UIBasicSprite_t2501337439, ___mInvert_56)); }
	inline bool get_mInvert_56() const { return ___mInvert_56; }
	inline bool* get_address_of_mInvert_56() { return &___mInvert_56; }
	inline void set_mInvert_56(bool value)
	{
		___mInvert_56 = value;
	}

	inline static int32_t get_offset_of_mFlip_57() { return static_cast<int32_t>(offsetof(UIBasicSprite_t2501337439, ___mFlip_57)); }
	inline int32_t get_mFlip_57() const { return ___mFlip_57; }
	inline int32_t* get_address_of_mFlip_57() { return &___mFlip_57; }
	inline void set_mFlip_57(int32_t value)
	{
		___mFlip_57 = value;
	}

	inline static int32_t get_offset_of_mInnerUV_58() { return static_cast<int32_t>(offsetof(UIBasicSprite_t2501337439, ___mInnerUV_58)); }
	inline Rect_t4241904616  get_mInnerUV_58() const { return ___mInnerUV_58; }
	inline Rect_t4241904616 * get_address_of_mInnerUV_58() { return &___mInnerUV_58; }
	inline void set_mInnerUV_58(Rect_t4241904616  value)
	{
		___mInnerUV_58 = value;
	}

	inline static int32_t get_offset_of_mOuterUV_59() { return static_cast<int32_t>(offsetof(UIBasicSprite_t2501337439, ___mOuterUV_59)); }
	inline Rect_t4241904616  get_mOuterUV_59() const { return ___mOuterUV_59; }
	inline Rect_t4241904616 * get_address_of_mOuterUV_59() { return &___mOuterUV_59; }
	inline void set_mOuterUV_59(Rect_t4241904616  value)
	{
		___mOuterUV_59 = value;
	}

	inline static int32_t get_offset_of_centerType_60() { return static_cast<int32_t>(offsetof(UIBasicSprite_t2501337439, ___centerType_60)); }
	inline int32_t get_centerType_60() const { return ___centerType_60; }
	inline int32_t* get_address_of_centerType_60() { return &___centerType_60; }
	inline void set_centerType_60(int32_t value)
	{
		___centerType_60 = value;
	}

	inline static int32_t get_offset_of_leftType_61() { return static_cast<int32_t>(offsetof(UIBasicSprite_t2501337439, ___leftType_61)); }
	inline int32_t get_leftType_61() const { return ___leftType_61; }
	inline int32_t* get_address_of_leftType_61() { return &___leftType_61; }
	inline void set_leftType_61(int32_t value)
	{
		___leftType_61 = value;
	}

	inline static int32_t get_offset_of_rightType_62() { return static_cast<int32_t>(offsetof(UIBasicSprite_t2501337439, ___rightType_62)); }
	inline int32_t get_rightType_62() const { return ___rightType_62; }
	inline int32_t* get_address_of_rightType_62() { return &___rightType_62; }
	inline void set_rightType_62(int32_t value)
	{
		___rightType_62 = value;
	}

	inline static int32_t get_offset_of_bottomType_63() { return static_cast<int32_t>(offsetof(UIBasicSprite_t2501337439, ___bottomType_63)); }
	inline int32_t get_bottomType_63() const { return ___bottomType_63; }
	inline int32_t* get_address_of_bottomType_63() { return &___bottomType_63; }
	inline void set_bottomType_63(int32_t value)
	{
		___bottomType_63 = value;
	}

	inline static int32_t get_offset_of_topType_64() { return static_cast<int32_t>(offsetof(UIBasicSprite_t2501337439, ___topType_64)); }
	inline int32_t get_topType_64() const { return ___topType_64; }
	inline int32_t* get_address_of_topType_64() { return &___topType_64; }
	inline void set_topType_64(int32_t value)
	{
		___topType_64 = value;
	}
};

struct UIBasicSprite_t2501337439_StaticFields
{
public:
	// UnityEngine.Vector2[] UIBasicSprite::mTempPos
	Vector2U5BU5D_t4024180168* ___mTempPos_65;
	// UnityEngine.Vector2[] UIBasicSprite::mTempUVs
	Vector2U5BU5D_t4024180168* ___mTempUVs_66;

public:
	inline static int32_t get_offset_of_mTempPos_65() { return static_cast<int32_t>(offsetof(UIBasicSprite_t2501337439_StaticFields, ___mTempPos_65)); }
	inline Vector2U5BU5D_t4024180168* get_mTempPos_65() const { return ___mTempPos_65; }
	inline Vector2U5BU5D_t4024180168** get_address_of_mTempPos_65() { return &___mTempPos_65; }
	inline void set_mTempPos_65(Vector2U5BU5D_t4024180168* value)
	{
		___mTempPos_65 = value;
		Il2CppCodeGenWriteBarrier(&___mTempPos_65, value);
	}

	inline static int32_t get_offset_of_mTempUVs_66() { return static_cast<int32_t>(offsetof(UIBasicSprite_t2501337439_StaticFields, ___mTempUVs_66)); }
	inline Vector2U5BU5D_t4024180168* get_mTempUVs_66() const { return ___mTempUVs_66; }
	inline Vector2U5BU5D_t4024180168** get_address_of_mTempUVs_66() { return &___mTempUVs_66; }
	inline void set_mTempUVs_66(Vector2U5BU5D_t4024180168* value)
	{
		___mTempUVs_66 = value;
		Il2CppCodeGenWriteBarrier(&___mTempUVs_66, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
