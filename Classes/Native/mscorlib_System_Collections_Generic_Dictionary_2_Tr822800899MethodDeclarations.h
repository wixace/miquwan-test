﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2583285505MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.String,Mihua.Assets.SubAssetMgr/ErrorInfo,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
#define Transform_1__ctor_m97511403(__this, ___object0, ___method1, method) ((  void (*) (Transform_1_t822800899 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m434775037_gshared)(__this, ___object0, ___method1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.String,Mihua.Assets.SubAssetMgr/ErrorInfo,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m829159473(__this, ___key0, ___value1, method) ((  DictionaryEntry_t1751606614  (*) (Transform_1_t822800899 *, String_t*, ErrorInfo_t2633981210 , const MethodInfo*))Transform_1_Invoke_m3507968607_gshared)(__this, ___key0, ___value1, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.String,Mihua.Assets.SubAssetMgr/ErrorInfo,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
#define Transform_1_BeginInvoke_m4181829392(__this, ___key0, ___value1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Transform_1_t822800899 *, String_t*, ErrorInfo_t2633981210 , AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))Transform_1_BeginInvoke_m3878339390_gshared)(__this, ___key0, ___value1, ___callback2, ___object3, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.String,Mihua.Assets.SubAssetMgr/ErrorInfo,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
#define Transform_1_EndInvoke_m3918811065(__this, ___result0, method) ((  DictionaryEntry_t1751606614  (*) (Transform_1_t822800899 *, Il2CppObject *, const MethodInfo*))Transform_1_EndInvoke_m3292751051_gshared)(__this, ___result0, method)
