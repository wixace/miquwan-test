﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EListGenerated
struct EListGenerated_t4095369804;
// JSVCall
struct JSVCall_t3708497963;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Type
struct Type_t;
// MethodID
struct MethodID_t3916401116;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Type2863145774.h"
#include "AssemblyU2DCSharp_MethodID3916401116.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"

// System.Void EListGenerated::.ctor()
extern "C"  void EListGenerated__ctor_m307707855 (EListGenerated_t4095369804 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EListGenerated::.cctor()
extern "C"  void EListGenerated__cctor_m466912702 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean EListGenerated::EList_AddT1__ListT1_T__T(JSVCall,System.Int32)
extern "C"  bool EListGenerated_EList_AddT1__ListT1_T__T_m2826157307 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean EListGenerated::EList_SortT1__ListT1_T__ComparisonT1_T(JSVCall,System.Int32)
extern "C"  bool EListGenerated_EList_SortT1__ListT1_T__ComparisonT1_T_m229382619 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean EListGenerated::EList_SwapT1__ListT1_T__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool EListGenerated_EList_SwapT1__ListT1_T__Int32__Int32_m1469480853 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EListGenerated::__Register()
extern "C"  void EListGenerated___Register_m339002968 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo EListGenerated::ilo_makeGenericMethod1(System.Type,MethodID,System.Int32)
extern "C"  MethodInfo_t * EListGenerated_ilo_makeGenericMethod1_m1135400717 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, MethodID_t3916401116 * ___methodID1, int32_t ___TCount2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object EListGenerated::ilo_getObject2(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * EListGenerated_ilo_getObject2_m2383753345 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 EListGenerated::ilo_getInt323(System.Int32)
extern "C"  int32_t EListGenerated_ilo_getInt323_m3649068952 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
