﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.Transform
struct Transform_t1659122786;
// System.Action
struct Action_t3771233898;
// UnityEngine.Animator
struct Animator_t2776330603;
// CSCheckPointUnit
struct CSCheckPointUnit_t3129216924;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraSmoothFollow
struct  CameraSmoothFollow_t2624612068  : public MonoBehaviour_t667441552
{
public:
	// System.Single CameraSmoothFollow::deltaTime
	float ___deltaTime_3;
	// UnityEngine.Transform CameraSmoothFollow::lastTarget
	Transform_t1659122786 * ___lastTarget_4;
	// System.Action CameraSmoothFollow::onFinished
	Action_t3771233898 * ___onFinished_5;
	// System.Boolean CameraSmoothFollow::isPlayMove
	bool ___isPlayMove_6;
	// System.Boolean CameraSmoothFollow::isChangeTarget
	bool ___isChangeTarget_7;
	// System.Single CameraSmoothFollow::lastChangeTime
	float ___lastChangeTime_8;
	// UnityEngine.Animator CameraSmoothFollow::mAnimator
	Animator_t2776330603 * ___mAnimator_9;
	// UnityEngine.Transform CameraSmoothFollow::_target
	Transform_t1659122786 * ____target_10;
	// UnityEngine.Vector3 CameraSmoothFollow::lastTargetPos
	Vector3_t4282066566  ___lastTargetPos_11;
	// System.Boolean CameraSmoothFollow::isNeedRXEvent
	bool ___isNeedRXEvent_12;
	// System.Single CameraSmoothFollow::rxError
	float ___rxError_13;
	// System.Boolean CameraSmoothFollow::isLockTarget
	bool ___isLockTarget_14;
	// System.Boolean CameraSmoothFollow::isDamping
	bool ___isDamping_15;
	// System.Single CameraSmoothFollow::distance
	float ___distance_16;
	// System.Single CameraSmoothFollow::lastDistance
	float ___lastDistance_17;
	// System.Single CameraSmoothFollow::rx
	float ___rx_18;
	// System.Single CameraSmoothFollow::ry
	float ___ry_19;
	// System.Single CameraSmoothFollow::rz
	float ___rz_20;
	// System.Single CameraSmoothFollow::posDamping
	float ___posDamping_21;
	// UnityEngine.Transform CameraSmoothFollow::mTransform
	Transform_t1659122786 * ___mTransform_22;
	// UnityEngine.Quaternion CameraSmoothFollow::currentRotation
	Quaternion_t1553702882  ___currentRotation_23;
	// UnityEngine.Quaternion CameraSmoothFollow::lastRotation
	Quaternion_t1553702882  ___lastRotation_24;
	// UnityEngine.Vector3 CameraSmoothFollow::lastPos
	Vector3_t4282066566  ___lastPos_25;
	// UnityEngine.Vector3 CameraSmoothFollow::targetPos
	Vector3_t4282066566  ___targetPos_26;
	// CSCheckPointUnit CameraSmoothFollow::chapter
	CSCheckPointUnit_t3129216924 * ___chapter_27;
	// System.Boolean CameraSmoothFollow::isStart
	bool ___isStart_28;
	// System.Single CameraSmoothFollow::distance_out
	float ___distance_out_29;
	// System.Single CameraSmoothFollow::distance_in
	float ___distance_in_30;
	// System.Single CameraSmoothFollow::rx_out
	float ___rx_out_31;
	// System.Single CameraSmoothFollow::rx_in
	float ___rx_in_32;
	// System.Single CameraSmoothFollow::alternateTime
	float ___alternateTime_33;
	// System.Single CameraSmoothFollow::alternateTimer
	float ___alternateTimer_34;
	// System.Boolean CameraSmoothFollow::ifMoveOnStatrBattle
	bool ___ifMoveOnStatrBattle_35;
	// System.Boolean CameraSmoothFollow::<IsOpeningMoveUpdate>k__BackingField
	bool ___U3CIsOpeningMoveUpdateU3Ek__BackingField_36;

public:
	inline static int32_t get_offset_of_deltaTime_3() { return static_cast<int32_t>(offsetof(CameraSmoothFollow_t2624612068, ___deltaTime_3)); }
	inline float get_deltaTime_3() const { return ___deltaTime_3; }
	inline float* get_address_of_deltaTime_3() { return &___deltaTime_3; }
	inline void set_deltaTime_3(float value)
	{
		___deltaTime_3 = value;
	}

	inline static int32_t get_offset_of_lastTarget_4() { return static_cast<int32_t>(offsetof(CameraSmoothFollow_t2624612068, ___lastTarget_4)); }
	inline Transform_t1659122786 * get_lastTarget_4() const { return ___lastTarget_4; }
	inline Transform_t1659122786 ** get_address_of_lastTarget_4() { return &___lastTarget_4; }
	inline void set_lastTarget_4(Transform_t1659122786 * value)
	{
		___lastTarget_4 = value;
		Il2CppCodeGenWriteBarrier(&___lastTarget_4, value);
	}

	inline static int32_t get_offset_of_onFinished_5() { return static_cast<int32_t>(offsetof(CameraSmoothFollow_t2624612068, ___onFinished_5)); }
	inline Action_t3771233898 * get_onFinished_5() const { return ___onFinished_5; }
	inline Action_t3771233898 ** get_address_of_onFinished_5() { return &___onFinished_5; }
	inline void set_onFinished_5(Action_t3771233898 * value)
	{
		___onFinished_5 = value;
		Il2CppCodeGenWriteBarrier(&___onFinished_5, value);
	}

	inline static int32_t get_offset_of_isPlayMove_6() { return static_cast<int32_t>(offsetof(CameraSmoothFollow_t2624612068, ___isPlayMove_6)); }
	inline bool get_isPlayMove_6() const { return ___isPlayMove_6; }
	inline bool* get_address_of_isPlayMove_6() { return &___isPlayMove_6; }
	inline void set_isPlayMove_6(bool value)
	{
		___isPlayMove_6 = value;
	}

	inline static int32_t get_offset_of_isChangeTarget_7() { return static_cast<int32_t>(offsetof(CameraSmoothFollow_t2624612068, ___isChangeTarget_7)); }
	inline bool get_isChangeTarget_7() const { return ___isChangeTarget_7; }
	inline bool* get_address_of_isChangeTarget_7() { return &___isChangeTarget_7; }
	inline void set_isChangeTarget_7(bool value)
	{
		___isChangeTarget_7 = value;
	}

	inline static int32_t get_offset_of_lastChangeTime_8() { return static_cast<int32_t>(offsetof(CameraSmoothFollow_t2624612068, ___lastChangeTime_8)); }
	inline float get_lastChangeTime_8() const { return ___lastChangeTime_8; }
	inline float* get_address_of_lastChangeTime_8() { return &___lastChangeTime_8; }
	inline void set_lastChangeTime_8(float value)
	{
		___lastChangeTime_8 = value;
	}

	inline static int32_t get_offset_of_mAnimator_9() { return static_cast<int32_t>(offsetof(CameraSmoothFollow_t2624612068, ___mAnimator_9)); }
	inline Animator_t2776330603 * get_mAnimator_9() const { return ___mAnimator_9; }
	inline Animator_t2776330603 ** get_address_of_mAnimator_9() { return &___mAnimator_9; }
	inline void set_mAnimator_9(Animator_t2776330603 * value)
	{
		___mAnimator_9 = value;
		Il2CppCodeGenWriteBarrier(&___mAnimator_9, value);
	}

	inline static int32_t get_offset_of__target_10() { return static_cast<int32_t>(offsetof(CameraSmoothFollow_t2624612068, ____target_10)); }
	inline Transform_t1659122786 * get__target_10() const { return ____target_10; }
	inline Transform_t1659122786 ** get_address_of__target_10() { return &____target_10; }
	inline void set__target_10(Transform_t1659122786 * value)
	{
		____target_10 = value;
		Il2CppCodeGenWriteBarrier(&____target_10, value);
	}

	inline static int32_t get_offset_of_lastTargetPos_11() { return static_cast<int32_t>(offsetof(CameraSmoothFollow_t2624612068, ___lastTargetPos_11)); }
	inline Vector3_t4282066566  get_lastTargetPos_11() const { return ___lastTargetPos_11; }
	inline Vector3_t4282066566 * get_address_of_lastTargetPos_11() { return &___lastTargetPos_11; }
	inline void set_lastTargetPos_11(Vector3_t4282066566  value)
	{
		___lastTargetPos_11 = value;
	}

	inline static int32_t get_offset_of_isNeedRXEvent_12() { return static_cast<int32_t>(offsetof(CameraSmoothFollow_t2624612068, ___isNeedRXEvent_12)); }
	inline bool get_isNeedRXEvent_12() const { return ___isNeedRXEvent_12; }
	inline bool* get_address_of_isNeedRXEvent_12() { return &___isNeedRXEvent_12; }
	inline void set_isNeedRXEvent_12(bool value)
	{
		___isNeedRXEvent_12 = value;
	}

	inline static int32_t get_offset_of_rxError_13() { return static_cast<int32_t>(offsetof(CameraSmoothFollow_t2624612068, ___rxError_13)); }
	inline float get_rxError_13() const { return ___rxError_13; }
	inline float* get_address_of_rxError_13() { return &___rxError_13; }
	inline void set_rxError_13(float value)
	{
		___rxError_13 = value;
	}

	inline static int32_t get_offset_of_isLockTarget_14() { return static_cast<int32_t>(offsetof(CameraSmoothFollow_t2624612068, ___isLockTarget_14)); }
	inline bool get_isLockTarget_14() const { return ___isLockTarget_14; }
	inline bool* get_address_of_isLockTarget_14() { return &___isLockTarget_14; }
	inline void set_isLockTarget_14(bool value)
	{
		___isLockTarget_14 = value;
	}

	inline static int32_t get_offset_of_isDamping_15() { return static_cast<int32_t>(offsetof(CameraSmoothFollow_t2624612068, ___isDamping_15)); }
	inline bool get_isDamping_15() const { return ___isDamping_15; }
	inline bool* get_address_of_isDamping_15() { return &___isDamping_15; }
	inline void set_isDamping_15(bool value)
	{
		___isDamping_15 = value;
	}

	inline static int32_t get_offset_of_distance_16() { return static_cast<int32_t>(offsetof(CameraSmoothFollow_t2624612068, ___distance_16)); }
	inline float get_distance_16() const { return ___distance_16; }
	inline float* get_address_of_distance_16() { return &___distance_16; }
	inline void set_distance_16(float value)
	{
		___distance_16 = value;
	}

	inline static int32_t get_offset_of_lastDistance_17() { return static_cast<int32_t>(offsetof(CameraSmoothFollow_t2624612068, ___lastDistance_17)); }
	inline float get_lastDistance_17() const { return ___lastDistance_17; }
	inline float* get_address_of_lastDistance_17() { return &___lastDistance_17; }
	inline void set_lastDistance_17(float value)
	{
		___lastDistance_17 = value;
	}

	inline static int32_t get_offset_of_rx_18() { return static_cast<int32_t>(offsetof(CameraSmoothFollow_t2624612068, ___rx_18)); }
	inline float get_rx_18() const { return ___rx_18; }
	inline float* get_address_of_rx_18() { return &___rx_18; }
	inline void set_rx_18(float value)
	{
		___rx_18 = value;
	}

	inline static int32_t get_offset_of_ry_19() { return static_cast<int32_t>(offsetof(CameraSmoothFollow_t2624612068, ___ry_19)); }
	inline float get_ry_19() const { return ___ry_19; }
	inline float* get_address_of_ry_19() { return &___ry_19; }
	inline void set_ry_19(float value)
	{
		___ry_19 = value;
	}

	inline static int32_t get_offset_of_rz_20() { return static_cast<int32_t>(offsetof(CameraSmoothFollow_t2624612068, ___rz_20)); }
	inline float get_rz_20() const { return ___rz_20; }
	inline float* get_address_of_rz_20() { return &___rz_20; }
	inline void set_rz_20(float value)
	{
		___rz_20 = value;
	}

	inline static int32_t get_offset_of_posDamping_21() { return static_cast<int32_t>(offsetof(CameraSmoothFollow_t2624612068, ___posDamping_21)); }
	inline float get_posDamping_21() const { return ___posDamping_21; }
	inline float* get_address_of_posDamping_21() { return &___posDamping_21; }
	inline void set_posDamping_21(float value)
	{
		___posDamping_21 = value;
	}

	inline static int32_t get_offset_of_mTransform_22() { return static_cast<int32_t>(offsetof(CameraSmoothFollow_t2624612068, ___mTransform_22)); }
	inline Transform_t1659122786 * get_mTransform_22() const { return ___mTransform_22; }
	inline Transform_t1659122786 ** get_address_of_mTransform_22() { return &___mTransform_22; }
	inline void set_mTransform_22(Transform_t1659122786 * value)
	{
		___mTransform_22 = value;
		Il2CppCodeGenWriteBarrier(&___mTransform_22, value);
	}

	inline static int32_t get_offset_of_currentRotation_23() { return static_cast<int32_t>(offsetof(CameraSmoothFollow_t2624612068, ___currentRotation_23)); }
	inline Quaternion_t1553702882  get_currentRotation_23() const { return ___currentRotation_23; }
	inline Quaternion_t1553702882 * get_address_of_currentRotation_23() { return &___currentRotation_23; }
	inline void set_currentRotation_23(Quaternion_t1553702882  value)
	{
		___currentRotation_23 = value;
	}

	inline static int32_t get_offset_of_lastRotation_24() { return static_cast<int32_t>(offsetof(CameraSmoothFollow_t2624612068, ___lastRotation_24)); }
	inline Quaternion_t1553702882  get_lastRotation_24() const { return ___lastRotation_24; }
	inline Quaternion_t1553702882 * get_address_of_lastRotation_24() { return &___lastRotation_24; }
	inline void set_lastRotation_24(Quaternion_t1553702882  value)
	{
		___lastRotation_24 = value;
	}

	inline static int32_t get_offset_of_lastPos_25() { return static_cast<int32_t>(offsetof(CameraSmoothFollow_t2624612068, ___lastPos_25)); }
	inline Vector3_t4282066566  get_lastPos_25() const { return ___lastPos_25; }
	inline Vector3_t4282066566 * get_address_of_lastPos_25() { return &___lastPos_25; }
	inline void set_lastPos_25(Vector3_t4282066566  value)
	{
		___lastPos_25 = value;
	}

	inline static int32_t get_offset_of_targetPos_26() { return static_cast<int32_t>(offsetof(CameraSmoothFollow_t2624612068, ___targetPos_26)); }
	inline Vector3_t4282066566  get_targetPos_26() const { return ___targetPos_26; }
	inline Vector3_t4282066566 * get_address_of_targetPos_26() { return &___targetPos_26; }
	inline void set_targetPos_26(Vector3_t4282066566  value)
	{
		___targetPos_26 = value;
	}

	inline static int32_t get_offset_of_chapter_27() { return static_cast<int32_t>(offsetof(CameraSmoothFollow_t2624612068, ___chapter_27)); }
	inline CSCheckPointUnit_t3129216924 * get_chapter_27() const { return ___chapter_27; }
	inline CSCheckPointUnit_t3129216924 ** get_address_of_chapter_27() { return &___chapter_27; }
	inline void set_chapter_27(CSCheckPointUnit_t3129216924 * value)
	{
		___chapter_27 = value;
		Il2CppCodeGenWriteBarrier(&___chapter_27, value);
	}

	inline static int32_t get_offset_of_isStart_28() { return static_cast<int32_t>(offsetof(CameraSmoothFollow_t2624612068, ___isStart_28)); }
	inline bool get_isStart_28() const { return ___isStart_28; }
	inline bool* get_address_of_isStart_28() { return &___isStart_28; }
	inline void set_isStart_28(bool value)
	{
		___isStart_28 = value;
	}

	inline static int32_t get_offset_of_distance_out_29() { return static_cast<int32_t>(offsetof(CameraSmoothFollow_t2624612068, ___distance_out_29)); }
	inline float get_distance_out_29() const { return ___distance_out_29; }
	inline float* get_address_of_distance_out_29() { return &___distance_out_29; }
	inline void set_distance_out_29(float value)
	{
		___distance_out_29 = value;
	}

	inline static int32_t get_offset_of_distance_in_30() { return static_cast<int32_t>(offsetof(CameraSmoothFollow_t2624612068, ___distance_in_30)); }
	inline float get_distance_in_30() const { return ___distance_in_30; }
	inline float* get_address_of_distance_in_30() { return &___distance_in_30; }
	inline void set_distance_in_30(float value)
	{
		___distance_in_30 = value;
	}

	inline static int32_t get_offset_of_rx_out_31() { return static_cast<int32_t>(offsetof(CameraSmoothFollow_t2624612068, ___rx_out_31)); }
	inline float get_rx_out_31() const { return ___rx_out_31; }
	inline float* get_address_of_rx_out_31() { return &___rx_out_31; }
	inline void set_rx_out_31(float value)
	{
		___rx_out_31 = value;
	}

	inline static int32_t get_offset_of_rx_in_32() { return static_cast<int32_t>(offsetof(CameraSmoothFollow_t2624612068, ___rx_in_32)); }
	inline float get_rx_in_32() const { return ___rx_in_32; }
	inline float* get_address_of_rx_in_32() { return &___rx_in_32; }
	inline void set_rx_in_32(float value)
	{
		___rx_in_32 = value;
	}

	inline static int32_t get_offset_of_alternateTime_33() { return static_cast<int32_t>(offsetof(CameraSmoothFollow_t2624612068, ___alternateTime_33)); }
	inline float get_alternateTime_33() const { return ___alternateTime_33; }
	inline float* get_address_of_alternateTime_33() { return &___alternateTime_33; }
	inline void set_alternateTime_33(float value)
	{
		___alternateTime_33 = value;
	}

	inline static int32_t get_offset_of_alternateTimer_34() { return static_cast<int32_t>(offsetof(CameraSmoothFollow_t2624612068, ___alternateTimer_34)); }
	inline float get_alternateTimer_34() const { return ___alternateTimer_34; }
	inline float* get_address_of_alternateTimer_34() { return &___alternateTimer_34; }
	inline void set_alternateTimer_34(float value)
	{
		___alternateTimer_34 = value;
	}

	inline static int32_t get_offset_of_ifMoveOnStatrBattle_35() { return static_cast<int32_t>(offsetof(CameraSmoothFollow_t2624612068, ___ifMoveOnStatrBattle_35)); }
	inline bool get_ifMoveOnStatrBattle_35() const { return ___ifMoveOnStatrBattle_35; }
	inline bool* get_address_of_ifMoveOnStatrBattle_35() { return &___ifMoveOnStatrBattle_35; }
	inline void set_ifMoveOnStatrBattle_35(bool value)
	{
		___ifMoveOnStatrBattle_35 = value;
	}

	inline static int32_t get_offset_of_U3CIsOpeningMoveUpdateU3Ek__BackingField_36() { return static_cast<int32_t>(offsetof(CameraSmoothFollow_t2624612068, ___U3CIsOpeningMoveUpdateU3Ek__BackingField_36)); }
	inline bool get_U3CIsOpeningMoveUpdateU3Ek__BackingField_36() const { return ___U3CIsOpeningMoveUpdateU3Ek__BackingField_36; }
	inline bool* get_address_of_U3CIsOpeningMoveUpdateU3Ek__BackingField_36() { return &___U3CIsOpeningMoveUpdateU3Ek__BackingField_36; }
	inline void set_U3CIsOpeningMoveUpdateU3Ek__BackingField_36(bool value)
	{
		___U3CIsOpeningMoveUpdateU3Ek__BackingField_36 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
