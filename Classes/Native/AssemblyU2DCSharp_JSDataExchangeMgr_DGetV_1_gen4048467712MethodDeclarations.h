﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// JSDataExchangeMgr/DGetV`1<System.Object>
struct DGetV_1_t4048467712;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void JSDataExchangeMgr/DGetV`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void DGetV_1__ctor_m3356914363_gshared (DGetV_1_t4048467712 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define DGetV_1__ctor_m3356914363(__this, ___object0, ___method1, method) ((  void (*) (DGetV_1_t4048467712 *, Il2CppObject *, IntPtr_t, const MethodInfo*))DGetV_1__ctor_m3356914363_gshared)(__this, ___object0, ___method1, method)
// T JSDataExchangeMgr/DGetV`1<System.Object>::Invoke()
extern "C"  Il2CppObject * DGetV_1_Invoke_m1509146772_gshared (DGetV_1_t4048467712 * __this, const MethodInfo* method);
#define DGetV_1_Invoke_m1509146772(__this, method) ((  Il2CppObject * (*) (DGetV_1_t4048467712 *, const MethodInfo*))DGetV_1_Invoke_m1509146772_gshared)(__this, method)
// System.IAsyncResult JSDataExchangeMgr/DGetV`1<System.Object>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DGetV_1_BeginInvoke_m2681184398_gshared (DGetV_1_t4048467712 * __this, AsyncCallback_t1369114871 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method);
#define DGetV_1_BeginInvoke_m2681184398(__this, ___callback0, ___object1, method) ((  Il2CppObject * (*) (DGetV_1_t4048467712 *, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))DGetV_1_BeginInvoke_m2681184398_gshared)(__this, ___callback0, ___object1, method)
// T JSDataExchangeMgr/DGetV`1<System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * DGetV_1_EndInvoke_m1776565962_gshared (DGetV_1_t4048467712 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define DGetV_1_EndInvoke_m1776565962(__this, ___result0, method) ((  Il2CppObject * (*) (DGetV_1_t4048467712 *, Il2CppObject *, const MethodInfo*))DGetV_1_EndInvoke_m1776565962_gshared)(__this, ___result0, method)
