﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t1974256870;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.Serialization.JsonFx.EcmaScriptIdentifier
struct  EcmaScriptIdentifier_t4023709608  : public Il2CppObject
{
public:
	// System.String Pathfinding.Serialization.JsonFx.EcmaScriptIdentifier::identifier
	String_t* ___identifier_0;

public:
	inline static int32_t get_offset_of_identifier_0() { return static_cast<int32_t>(offsetof(EcmaScriptIdentifier_t4023709608, ___identifier_0)); }
	inline String_t* get_identifier_0() const { return ___identifier_0; }
	inline String_t** get_address_of_identifier_0() { return &___identifier_0; }
	inline void set_identifier_0(String_t* value)
	{
		___identifier_0 = value;
		Il2CppCodeGenWriteBarrier(&___identifier_0, value);
	}
};

struct EcmaScriptIdentifier_t4023709608_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Pathfinding.Serialization.JsonFx.EcmaScriptIdentifier::<>f__switch$map0
	Dictionary_2_t1974256870 * ___U3CU3Ef__switchU24map0_1;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map0_1() { return static_cast<int32_t>(offsetof(EcmaScriptIdentifier_t4023709608_StaticFields, ___U3CU3Ef__switchU24map0_1)); }
	inline Dictionary_2_t1974256870 * get_U3CU3Ef__switchU24map0_1() const { return ___U3CU3Ef__switchU24map0_1; }
	inline Dictionary_2_t1974256870 ** get_address_of_U3CU3Ef__switchU24map0_1() { return &___U3CU3Ef__switchU24map0_1; }
	inline void set_U3CU3Ef__switchU24map0_1(Dictionary_2_t1974256870 * value)
	{
		___U3CU3Ef__switchU24map0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map0_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
