﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen4129802037.h"
#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DCSharp_SevenZip_Compression_RangeCoder_1052492065.h"

// System.Void System.Array/InternalEnumerator`1<SevenZip.Compression.RangeCoder.BitEncoder>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1427418948_gshared (InternalEnumerator_1_t4129802037 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m1427418948(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t4129802037 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m1427418948_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<SevenZip.Compression.RangeCoder.BitEncoder>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2671908828_gshared (InternalEnumerator_1_t4129802037 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2671908828(__this, method) ((  void (*) (InternalEnumerator_1_t4129802037 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2671908828_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<SevenZip.Compression.RangeCoder.BitEncoder>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m417673032_gshared (InternalEnumerator_1_t4129802037 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m417673032(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t4129802037 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m417673032_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<SevenZip.Compression.RangeCoder.BitEncoder>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3848519195_gshared (InternalEnumerator_1_t4129802037 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m3848519195(__this, method) ((  void (*) (InternalEnumerator_1_t4129802037 *, const MethodInfo*))InternalEnumerator_1_Dispose_m3848519195_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<SevenZip.Compression.RangeCoder.BitEncoder>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m4152710728_gshared (InternalEnumerator_1_t4129802037 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m4152710728(__this, method) ((  bool (*) (InternalEnumerator_1_t4129802037 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m4152710728_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<SevenZip.Compression.RangeCoder.BitEncoder>::get_Current()
extern "C"  BitEncoder_t1052492065  InternalEnumerator_1_get_Current_m2325687819_gshared (InternalEnumerator_1_t4129802037 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m2325687819(__this, method) ((  BitEncoder_t1052492065  (*) (InternalEnumerator_1_t4129802037 *, const MethodInfo*))InternalEnumerator_1_get_Current_m2325687819_gshared)(__this, method)
