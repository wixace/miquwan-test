﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UILabelGenerated
struct UILabelGenerated_t977375407;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// UILabel
struct UILabel_t291504320;
// UIFont
struct UIFont_t2503090435;
// UnityEngine.Object
struct Object_t3071478659;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "AssemblyU2DCSharp_UILabel291504320.h"
#include "AssemblyU2DCSharp_UIFont2503090435.h"
#include "UnityEngine_UnityEngine_Object3071478659.h"
#include "UnityEngine_UnityEngine_FontStyle3350479768.h"
#include "AssemblyU2DCSharp_NGUIText_SymbolStyle99318052.h"
#include "AssemblyU2DCSharp_UILabel_Effect3176279520.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "mscorlib_System_String7231557.h"
#include "UnityEngine_UnityEngine_KeyCode3128317986.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void UILabelGenerated::.ctor()
extern "C"  void UILabelGenerated__ctor_m4111067852 (UILabelGenerated_t977375407 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UILabelGenerated::UILabel_UILabel1(JSVCall,System.Int32)
extern "C"  bool UILabelGenerated_UILabel_UILabel1_m1501084588 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UILabelGenerated::UILabel_keepCrispWhenShrunk(JSVCall)
extern "C"  void UILabelGenerated_UILabel_keepCrispWhenShrunk_m2196849051 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UILabelGenerated::UILabel_finalFontSize(JSVCall)
extern "C"  void UILabelGenerated_UILabel_finalFontSize_m1811967222 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UILabelGenerated::UILabel_isAnchoredHorizontally(JSVCall)
extern "C"  void UILabelGenerated_UILabel_isAnchoredHorizontally_m1295163585 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UILabelGenerated::UILabel_isAnchoredVertically(JSVCall)
extern "C"  void UILabelGenerated_UILabel_isAnchoredVertically_m320119087 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UILabelGenerated::UILabel_material(JSVCall)
extern "C"  void UILabelGenerated_UILabel_material_m4147265865 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UILabelGenerated::UILabel_bitmapFont(JSVCall)
extern "C"  void UILabelGenerated_UILabel_bitmapFont_m2971004850 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UILabelGenerated::UILabel_trueTypeFont(JSVCall)
extern "C"  void UILabelGenerated_UILabel_trueTypeFont_m128719129 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UILabelGenerated::UILabel_ambigiousFont(JSVCall)
extern "C"  void UILabelGenerated_UILabel_ambigiousFont_m3122559125 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UILabelGenerated::UILabel_uintText(JSVCall)
extern "C"  void UILabelGenerated_UILabel_uintText_m2594222793 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UILabelGenerated::UILabel_intText(JSVCall)
extern "C"  void UILabelGenerated_UILabel_intText_m3672258816 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UILabelGenerated::UILabel_text(JSVCall)
extern "C"  void UILabelGenerated_UILabel_text_m811605251 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UILabelGenerated::UILabel_defaultFontSize(JSVCall)
extern "C"  void UILabelGenerated_UILabel_defaultFontSize_m2027598347 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UILabelGenerated::UILabel_fontSize(JSVCall)
extern "C"  void UILabelGenerated_UILabel_fontSize_m869742496 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UILabelGenerated::UILabel_fontStyle(JSVCall)
extern "C"  void UILabelGenerated_UILabel_fontStyle_m4006382938 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UILabelGenerated::UILabel_alignment(JSVCall)
extern "C"  void UILabelGenerated_UILabel_alignment_m1526529849 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UILabelGenerated::UILabel_applyGradient(JSVCall)
extern "C"  void UILabelGenerated_UILabel_applyGradient_m3254815678 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UILabelGenerated::UILabel_gradientTop(JSVCall)
extern "C"  void UILabelGenerated_UILabel_gradientTop_m361491927 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UILabelGenerated::UILabel_gradientBottom(JSVCall)
extern "C"  void UILabelGenerated_UILabel_gradientBottom_m2468707317 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UILabelGenerated::UILabel_spacingX(JSVCall)
extern "C"  void UILabelGenerated_UILabel_spacingX_m3476117947 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UILabelGenerated::UILabel_spacingY(JSVCall)
extern "C"  void UILabelGenerated_UILabel_spacingY_m3279604442 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UILabelGenerated::UILabel_useFloatSpacing(JSVCall)
extern "C"  void UILabelGenerated_UILabel_useFloatSpacing_m2942053262 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UILabelGenerated::UILabel_floatSpacingX(JSVCall)
extern "C"  void UILabelGenerated_UILabel_floatSpacingX_m961959659 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UILabelGenerated::UILabel_floatSpacingY(JSVCall)
extern "C"  void UILabelGenerated_UILabel_floatSpacingY_m765446154 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UILabelGenerated::UILabel_effectiveSpacingY(JSVCall)
extern "C"  void UILabelGenerated_UILabel_effectiveSpacingY_m2793035039 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UILabelGenerated::UILabel_effectiveSpacingX(JSVCall)
extern "C"  void UILabelGenerated_UILabel_effectiveSpacingX_m2989548544 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UILabelGenerated::UILabel_overflowEllipsis(JSVCall)
extern "C"  void UILabelGenerated_UILabel_overflowEllipsis_m2282351357 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UILabelGenerated::UILabel_supportEncoding(JSVCall)
extern "C"  void UILabelGenerated_UILabel_supportEncoding_m2399029562 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UILabelGenerated::UILabel_symbolStyle(JSVCall)
extern "C"  void UILabelGenerated_UILabel_symbolStyle_m4046146915 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UILabelGenerated::UILabel_overflowMethod(JSVCall)
extern "C"  void UILabelGenerated_UILabel_overflowMethod_m3745461997 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UILabelGenerated::UILabel_multiLine(JSVCall)
extern "C"  void UILabelGenerated_UILabel_multiLine_m1122407311 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UILabelGenerated::UILabel_localCorners(JSVCall)
extern "C"  void UILabelGenerated_UILabel_localCorners_m3558752285 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UILabelGenerated::UILabel_worldCorners(JSVCall)
extern "C"  void UILabelGenerated_UILabel_worldCorners_m1487031876 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UILabelGenerated::UILabel_drawingDimensions(JSVCall)
extern "C"  void UILabelGenerated_UILabel_drawingDimensions_m755500433 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UILabelGenerated::UILabel_maxLineCount(JSVCall)
extern "C"  void UILabelGenerated_UILabel_maxLineCount_m4134303481 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UILabelGenerated::UILabel_effectStyle(JSVCall)
extern "C"  void UILabelGenerated_UILabel_effectStyle_m3501304220 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UILabelGenerated::UILabel_effectColor(JSVCall)
extern "C"  void UILabelGenerated_UILabel_effectColor_m3433652266 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UILabelGenerated::UILabel_effectDistance(JSVCall)
extern "C"  void UILabelGenerated_UILabel_effectDistance_m3119787306 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UILabelGenerated::UILabel_processedText(JSVCall)
extern "C"  void UILabelGenerated_UILabel_processedText_m2774833025 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UILabelGenerated::UILabel_printedSize(JSVCall)
extern "C"  void UILabelGenerated_UILabel_printedSize_m761489583 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UILabelGenerated::UILabel_localSize(JSVCall)
extern "C"  void UILabelGenerated_UILabel_localSize_m651606320 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UILabelGenerated::UILabel_ApplyOffset__BetterListT1_Vector3__Int32(JSVCall,System.Int32)
extern "C"  bool UILabelGenerated_UILabel_ApplyOffset__BetterListT1_Vector3__Int32_m1968420076 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UILabelGenerated::UILabel_ApplyShadow__BetterListT1_Vector3__BetterListT1_Vector2__BetterListT1_Color32__Int32__Int32__Single__Single(JSVCall,System.Int32)
extern "C"  bool UILabelGenerated_UILabel_ApplyShadow__BetterListT1_Vector3__BetterListT1_Vector2__BetterListT1_Color32__Int32__Int32__Single__Single_m3665451434 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UILabelGenerated::UILabel_AssumeNaturalSize(JSVCall,System.Int32)
extern "C"  bool UILabelGenerated_UILabel_AssumeNaturalSize_m4016653637 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UILabelGenerated::UILabel_CalculateOffsetToFit__String(JSVCall,System.Int32)
extern "C"  bool UILabelGenerated_UILabel_CalculateOffsetToFit__String_m3974786601 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UILabelGenerated::UILabel_GetCharacterIndex__Int32__KeyCode(JSVCall,System.Int32)
extern "C"  bool UILabelGenerated_UILabel_GetCharacterIndex__Int32__KeyCode_m2586493636 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UILabelGenerated::UILabel_GetCharacterIndexAtPosition__Vector2__Boolean(JSVCall,System.Int32)
extern "C"  bool UILabelGenerated_UILabel_GetCharacterIndexAtPosition__Vector2__Boolean_m216424251 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UILabelGenerated::UILabel_GetCharacterIndexAtPosition__Vector3__Boolean(JSVCall,System.Int32)
extern "C"  bool UILabelGenerated_UILabel_GetCharacterIndexAtPosition__Vector3__Boolean_m2501287706 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UILabelGenerated::UILabel_GetIDKey(JSVCall,System.Int32)
extern "C"  bool UILabelGenerated_UILabel_GetIDKey_m1801227849 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UILabelGenerated::UILabel_GetSides__Transform(JSVCall,System.Int32)
extern "C"  bool UILabelGenerated_UILabel_GetSides__Transform_m830704557 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UILabelGenerated::UILabel_GetUrlAtCharacterIndex__Int32(JSVCall,System.Int32)
extern "C"  bool UILabelGenerated_UILabel_GetUrlAtCharacterIndex__Int32_m3237096160 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UILabelGenerated::UILabel_GetUrlAtPosition__Vector2(JSVCall,System.Int32)
extern "C"  bool UILabelGenerated_UILabel_GetUrlAtPosition__Vector2_m1767638561 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UILabelGenerated::UILabel_GetUrlAtPosition__Vector3(JSVCall,System.Int32)
extern "C"  bool UILabelGenerated_UILabel_GetUrlAtPosition__Vector3_m3012403042 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UILabelGenerated::UILabel_GetWordAtCharacterIndex__Int32(JSVCall,System.Int32)
extern "C"  bool UILabelGenerated_UILabel_GetWordAtCharacterIndex__Int32_m3365580621 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UILabelGenerated::UILabel_GetWordAtPosition__Vector3(JSVCall,System.Int32)
extern "C"  bool UILabelGenerated_UILabel_GetWordAtPosition__Vector3_m1859871311 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UILabelGenerated::UILabel_GetWordAtPosition__Vector2(JSVCall,System.Int32)
extern "C"  bool UILabelGenerated_UILabel_GetWordAtPosition__Vector2_m615106830 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UILabelGenerated::UILabel_MakePixelPerfect(JSVCall,System.Int32)
extern "C"  bool UILabelGenerated_UILabel_MakePixelPerfect_m1184949008 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UILabelGenerated::UILabel_MarkAsChanged(JSVCall,System.Int32)
extern "C"  bool UILabelGenerated_UILabel_MarkAsChanged_m2363064668 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UILabelGenerated::UILabel_OnFill__BetterListT1_Vector3__BetterListT1_Vector2__BetterListT1_Color32(JSVCall,System.Int32)
extern "C"  bool UILabelGenerated_UILabel_OnFill__BetterListT1_Vector3__BetterListT1_Vector2__BetterListT1_Color32_m1850206786 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UILabelGenerated::UILabel_PrintOverlay__Int32__Int32__UIGeometry__UIGeometry__Color__Color(JSVCall,System.Int32)
extern "C"  bool UILabelGenerated_UILabel_PrintOverlay__Int32__Int32__UIGeometry__UIGeometry__Color__Color_m295964714 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UILabelGenerated::UILabel_ProcessText(JSVCall,System.Int32)
extern "C"  bool UILabelGenerated_UILabel_ProcessText_m1998628355 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UILabelGenerated::UILabel_SetCurrentPercent(JSVCall,System.Int32)
extern "C"  bool UILabelGenerated_UILabel_SetCurrentPercent_m4174732629 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UILabelGenerated::UILabel_SetCurrentProgress(JSVCall,System.Int32)
extern "C"  bool UILabelGenerated_UILabel_SetCurrentProgress_m3902730655 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UILabelGenerated::UILabel_SetCurrentSelection(JSVCall,System.Int32)
extern "C"  bool UILabelGenerated_UILabel_SetCurrentSelection_m1532082076 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UILabelGenerated::UILabel_UpdateNGUIText(JSVCall,System.Int32)
extern "C"  bool UILabelGenerated_UILabel_UpdateNGUIText_m2400846846 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UILabelGenerated::UILabel_Wrap__String__String__Int32(JSVCall,System.Int32)
extern "C"  bool UILabelGenerated_UILabel_Wrap__String__String__Int32_m2071257833 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UILabelGenerated::UILabel_Wrap__String__String(JSVCall,System.Int32)
extern "C"  bool UILabelGenerated_UILabel_Wrap__String__String_m4131171975 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UILabelGenerated::__Register()
extern "C"  void UILabelGenerated___Register_m3629543803 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UILabelGenerated::ilo_addJSCSRel1(System.Int32,System.Object)
extern "C"  void UILabelGenerated_ilo_addJSCSRel1_m1126763400 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UILabelGenerated::ilo_setEnum2(System.Int32,System.Int32)
extern "C"  void UILabelGenerated_ilo_setEnum2_m3322073036 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UILabelGenerated::ilo_setInt323(System.Int32,System.Int32)
extern "C"  void UILabelGenerated_ilo_setInt323_m1811759432 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UILabelGenerated::ilo_setObject4(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UILabelGenerated_ilo_setObject4_m3994788353 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UILabelGenerated::ilo_set_bitmapFont5(UILabel,UIFont)
extern "C"  void UILabelGenerated_ilo_set_bitmapFont5_m4189248994 (Il2CppObject * __this /* static, unused */, UILabel_t291504320 * ____this0, UIFont_t2503090435 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UILabelGenerated::ilo_getObject6(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UILabelGenerated_ilo_getObject6_m904763816 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UILabelGenerated::ilo_set_ambigiousFont7(UILabel,UnityEngine.Object)
extern "C"  void UILabelGenerated_ilo_set_ambigiousFont7_m3795662070 (Il2CppObject * __this /* static, unused */, UILabel_t291504320 * ____this0, Object_t3071478659 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UILabelGenerated::ilo_get_intText8(UILabel)
extern "C"  int32_t UILabelGenerated_ilo_get_intText8_m3143620628 (Il2CppObject * __this /* static, unused */, UILabel_t291504320 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UILabelGenerated::ilo_getStringS9(System.Int32)
extern "C"  String_t* UILabelGenerated_ilo_getStringS9_m3727187090 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UILabelGenerated::ilo_get_defaultFontSize10(UILabel)
extern "C"  int32_t UILabelGenerated_ilo_get_defaultFontSize10_m3591434562 (Il2CppObject * __this /* static, unused */, UILabel_t291504320 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UILabelGenerated::ilo_getInt3211(System.Int32)
extern "C"  int32_t UILabelGenerated_ilo_getInt3211_m1269403426 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.FontStyle UILabelGenerated::ilo_get_fontStyle12(UILabel)
extern "C"  int32_t UILabelGenerated_ilo_get_fontStyle12_m1603351465 (Il2CppObject * __this /* static, unused */, UILabel_t291504320 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UILabelGenerated::ilo_getEnum13(System.Int32)
extern "C"  int32_t UILabelGenerated_ilo_getEnum13_m1669733523 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UILabelGenerated::ilo_setBooleanS14(System.Int32,System.Boolean)
extern "C"  void UILabelGenerated_ilo_setBooleanS14_m1741302565 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UILabelGenerated::ilo_getBooleanS15(System.Int32)
extern "C"  bool UILabelGenerated_ilo_getBooleanS15_m1035865689 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UILabelGenerated::ilo_get_spacingY16(UILabel)
extern "C"  int32_t UILabelGenerated_ilo_get_spacingY16_m3144333333 (Il2CppObject * __this /* static, unused */, UILabel_t291504320 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UILabelGenerated::ilo_get_floatSpacingX17(UILabel)
extern "C"  float UILabelGenerated_ilo_get_floatSpacingX17_m3454792697 (Il2CppObject * __this /* static, unused */, UILabel_t291504320 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UILabelGenerated::ilo_getSingle18(System.Int32)
extern "C"  float UILabelGenerated_ilo_getSingle18_m1839368993 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UILabelGenerated::ilo_get_effectiveSpacingX19(UILabel)
extern "C"  float UILabelGenerated_ilo_get_effectiveSpacingX19_m1223849356 (Il2CppObject * __this /* static, unused */, UILabel_t291504320 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UILabelGenerated::ilo_setSingle20(System.Int32,System.Single)
extern "C"  void UILabelGenerated_ilo_setSingle20_m1800425525 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UILabelGenerated::ilo_set_symbolStyle21(UILabel,NGUIText/SymbolStyle)
extern "C"  void UILabelGenerated_ilo_set_symbolStyle21_m840701954 (Il2CppObject * __this /* static, unused */, UILabel_t291504320 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UILabelGenerated::ilo_moveSaveID2Arr22(System.Int32)
extern "C"  void UILabelGenerated_ilo_moveSaveID2Arr22_m445059124 (Il2CppObject * __this /* static, unused */, int32_t ___arrIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UILabelGenerated::ilo_setArrayS23(System.Int32,System.Int32,System.Boolean)
extern "C"  void UILabelGenerated_ilo_setArrayS23_m1504474411 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___count1, bool ___bClear2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UILabelGenerated::ilo_get_maxLineCount24(UILabel)
extern "C"  int32_t UILabelGenerated_ilo_get_maxLineCount24_m3107735159 (Il2CppObject * __this /* static, unused */, UILabel_t291504320 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UILabelGenerated::ilo_set_maxLineCount25(UILabel,System.Int32)
extern "C"  void UILabelGenerated_ilo_set_maxLineCount25_m3423426023 (Il2CppObject * __this /* static, unused */, UILabel_t291504320 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UILabel/Effect UILabelGenerated::ilo_get_effectStyle26(UILabel)
extern "C"  int32_t UILabelGenerated_ilo_get_effectStyle26_m2136397469 (Il2CppObject * __this /* static, unused */, UILabel_t291504320 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UILabelGenerated::ilo_get_effectColor27(UILabel)
extern "C"  Color_t4194546905  UILabelGenerated_ilo_get_effectColor27_m988700660 (Il2CppObject * __this /* static, unused */, UILabel_t291504320 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UILabelGenerated::ilo_set_effectColor28(UILabel,UnityEngine.Color)
extern "C"  void UILabelGenerated_ilo_set_effectColor28_m2271021728 (Il2CppObject * __this /* static, unused */, UILabel_t291504320 * ____this0, Color_t4194546905  ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UILabelGenerated::ilo_set_effectDistance29(UILabel,UnityEngine.Vector2)
extern "C"  void UILabelGenerated_ilo_set_effectDistance29_m13356177 (Il2CppObject * __this /* static, unused */, UILabel_t291504320 * ____this0, Vector2_t4282066565  ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UILabelGenerated::ilo_setStringS30(System.Int32,System.String)
extern "C"  void UILabelGenerated_ilo_setStringS30_m2264419285 (Il2CppObject * __this /* static, unused */, int32_t ___e0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UILabelGenerated::ilo_get_printedSize31(UILabel)
extern "C"  Vector2_t4282066565  UILabelGenerated_ilo_get_printedSize31_m576526420 (Il2CppObject * __this /* static, unused */, UILabel_t291504320 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UILabelGenerated::ilo_AssumeNaturalSize32(UILabel)
extern "C"  void UILabelGenerated_ilo_AssumeNaturalSize32_m2377020606 (Il2CppObject * __this /* static, unused */, UILabel_t291504320 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UILabelGenerated::ilo_CalculateOffsetToFit33(UILabel,System.String)
extern "C"  int32_t UILabelGenerated_ilo_CalculateOffsetToFit33_m3598315096 (Il2CppObject * __this /* static, unused */, UILabel_t291504320 * ____this0, String_t* ___text1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UILabelGenerated::ilo_GetCharacterIndex34(UILabel,System.Int32,UnityEngine.KeyCode)
extern "C"  int32_t UILabelGenerated_ilo_GetCharacterIndex34_m1638046629 (Il2CppObject * __this /* static, unused */, UILabel_t291504320 * ____this0, int32_t ___currentIndex1, int32_t ___key2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UILabelGenerated::ilo_GetCharacterIndexAtPosition35(UILabel,UnityEngine.Vector2,System.Boolean)
extern "C"  int32_t UILabelGenerated_ilo_GetCharacterIndexAtPosition35_m1907467773 (Il2CppObject * __this /* static, unused */, UILabel_t291504320 * ____this0, Vector2_t4282066565  ___localPos1, bool ___precise2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UILabelGenerated::ilo_setVector3S36(System.Int32,UnityEngine.Vector3)
extern "C"  void UILabelGenerated_ilo_setVector3S36_m2085520643 (Il2CppObject * __this /* static, unused */, int32_t ___e0, Vector3_t4282066566  ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UILabelGenerated::ilo_getVector2S37(System.Int32)
extern "C"  Vector2_t4282066565  UILabelGenerated_ilo_getVector2S37_m947433099 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UILabelGenerated::ilo_getVector3S38(System.Int32)
extern "C"  Vector3_t4282066566  UILabelGenerated_ilo_getVector3S38_m2591103148 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UILabelGenerated::ilo_GetUrlAtPosition39(UILabel,UnityEngine.Vector3)
extern "C"  String_t* UILabelGenerated_ilo_GetUrlAtPosition39_m4205077596 (Il2CppObject * __this /* static, unused */, UILabel_t291504320 * ____this0, Vector3_t4282066566  ___worldPos1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UILabelGenerated::ilo_GetWordAtPosition40(UILabel,UnityEngine.Vector3)
extern "C"  String_t* UILabelGenerated_ilo_GetWordAtPosition40_m1429724121 (Il2CppObject * __this /* static, unused */, UILabel_t291504320 * ____this0, Vector3_t4282066566  ___worldPos1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UILabelGenerated::ilo_GetWordAtPosition41(UILabel,UnityEngine.Vector2)
extern "C"  String_t* UILabelGenerated_ilo_GetWordAtPosition41_m1441040217 (Il2CppObject * __this /* static, unused */, UILabel_t291504320 * ____this0, Vector2_t4282066565  ___localPos1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UILabelGenerated::ilo_ProcessText42(UILabel)
extern "C"  void UILabelGenerated_ilo_ProcessText42_m1533778305 (Il2CppObject * __this /* static, unused */, UILabel_t291504320 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UILabelGenerated::ilo_SetCurrentPercent43(UILabel)
extern "C"  void UILabelGenerated_ilo_SetCurrentPercent43_m1712253582 (Il2CppObject * __this /* static, unused */, UILabel_t291504320 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UILabelGenerated::ilo_Wrap44(UILabel,System.String,System.String&)
extern "C"  bool UILabelGenerated_ilo_Wrap44_m425603275 (Il2CppObject * __this /* static, unused */, UILabel_t291504320 * ____this0, String_t* ___text1, String_t** ___final2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
