﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_GUIElementGenerated
struct UnityEngine_GUIElementGenerated_t686825190;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"

// System.Void UnityEngine_GUIElementGenerated::.ctor()
extern "C"  void UnityEngine_GUIElementGenerated__ctor_m4029299525 (UnityEngine_GUIElementGenerated_t686825190 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIElementGenerated::GUIElement_GUIElement1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIElementGenerated_GUIElement_GUIElement1_m2358131757 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIElementGenerated::GUIElement_GetScreenRect__Camera(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIElementGenerated_GUIElement_GetScreenRect__Camera_m211891560 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIElementGenerated::GUIElement_GetScreenRect(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIElementGenerated_GUIElement_GetScreenRect_m3278573059 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIElementGenerated::GUIElement_HitTest__Vector3__Camera(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIElementGenerated_GUIElement_HitTest__Vector3__Camera_m2258377429 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIElementGenerated::GUIElement_HitTest__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIElementGenerated_GUIElement_HitTest__Vector3_m1996796016 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUIElementGenerated::__Register()
extern "C"  void UnityEngine_GUIElementGenerated___Register_m593659042 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIElementGenerated::ilo_attachFinalizerObject1(System.Int32)
extern "C"  bool UnityEngine_GUIElementGenerated_ilo_attachFinalizerObject1_m1538248530 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUIElementGenerated::ilo_addJSCSRel2(System.Int32,System.Object)
extern "C"  void UnityEngine_GUIElementGenerated_ilo_addJSCSRel2_m335820802 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_GUIElementGenerated::ilo_getObject3(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_GUIElementGenerated_ilo_getObject3_m4134593794 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
