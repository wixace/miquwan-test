﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PluginBuglyGenerated
struct PluginBuglyGenerated_t3084690881;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// PluginBugly
struct PluginBugly_t2538573678;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_PluginBugly2538573678.h"
#include "mscorlib_System_String7231557.h"

// System.Void PluginBuglyGenerated::.ctor()
extern "C"  void PluginBuglyGenerated__ctor_m4162035194 (PluginBuglyGenerated_t3084690881 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginBuglyGenerated::PluginBugly_PluginBugly1(JSVCall,System.Int32)
extern "C"  bool PluginBuglyGenerated_PluginBugly_PluginBugly1_m3955082338 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginBuglyGenerated::PluginBugly_BuglyInit(JSVCall,System.Int32)
extern "C"  bool PluginBuglyGenerated_PluginBugly_BuglyInit_m1080090516 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginBuglyGenerated::PluginBugly_Init(JSVCall,System.Int32)
extern "C"  bool PluginBuglyGenerated_PluginBugly_Init_m4081859919 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginBuglyGenerated::PluginBugly_OnDestory(JSVCall,System.Int32)
extern "C"  bool PluginBuglyGenerated_PluginBugly_OnDestory_m3668429592 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginBuglyGenerated::PluginBugly_RoleEnterGame__ZEvent(JSVCall,System.Int32)
extern "C"  bool PluginBuglyGenerated_PluginBugly_RoleEnterGame__ZEvent_m3642298871 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginBuglyGenerated::PluginBugly_SetUserTab__String__String(JSVCall,System.Int32)
extern "C"  bool PluginBuglyGenerated_PluginBugly_SetUserTab__String__String_m1430122089 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginBuglyGenerated::__Register()
extern "C"  void PluginBuglyGenerated___Register_m350456589 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginBuglyGenerated::ilo_addJSCSRel1(System.Int32,System.Object)
extern "C"  void PluginBuglyGenerated_ilo_addJSCSRel1_m381501366 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginBuglyGenerated::ilo_Init2(PluginBugly)
extern "C"  void PluginBuglyGenerated_ilo_Init2_m2983896381 (Il2CppObject * __this /* static, unused */, PluginBugly_t2538573678 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PluginBuglyGenerated::ilo_getStringS3(System.Int32)
extern "C"  String_t* PluginBuglyGenerated_ilo_getStringS3_m3008681274 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginBuglyGenerated::ilo_SetUserTab4(PluginBugly,System.String,System.String)
extern "C"  void PluginBuglyGenerated_ilo_SetUserTab4_m2214649451 (Il2CppObject * __this /* static, unused */, PluginBugly_t2538573678 * ____this0, String_t* ___userId1, String_t* ___userName2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
