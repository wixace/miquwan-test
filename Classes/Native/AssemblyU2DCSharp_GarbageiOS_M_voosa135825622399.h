﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_voosa135
struct  M_voosa135_t825622399  : public Il2CppObject
{
public:
	// System.String GarbageiOS.M_voosa135::_mufur
	String_t* ____mufur_0;
	// System.String GarbageiOS.M_voosa135::_cexemwe
	String_t* ____cexemwe_1;
	// System.String GarbageiOS.M_voosa135::_leferloo
	String_t* ____leferloo_2;
	// System.String GarbageiOS.M_voosa135::_jawsemlasNesi
	String_t* ____jawsemlasNesi_3;
	// System.Boolean GarbageiOS.M_voosa135::_nearlijel
	bool ____nearlijel_4;
	// System.String GarbageiOS.M_voosa135::_lidooballBagorral
	String_t* ____lidooballBagorral_5;
	// System.String GarbageiOS.M_voosa135::_misoosemNasyeete
	String_t* ____misoosemNasyeete_6;
	// System.Single GarbageiOS.M_voosa135::_chasla
	float ____chasla_7;
	// System.Int32 GarbageiOS.M_voosa135::_qemtroliBeta
	int32_t ____qemtroliBeta_8;
	// System.Single GarbageiOS.M_voosa135::_yelba
	float ____yelba_9;
	// System.Boolean GarbageiOS.M_voosa135::_dorcu
	bool ____dorcu_10;
	// System.String GarbageiOS.M_voosa135::_jecuFearyear
	String_t* ____jecuFearyear_11;
	// System.String GarbageiOS.M_voosa135::_houstinai
	String_t* ____houstinai_12;
	// System.UInt32 GarbageiOS.M_voosa135::_wayzigeLelbeacall
	uint32_t ____wayzigeLelbeacall_13;
	// System.UInt32 GarbageiOS.M_voosa135::_lorwhayrairHeemepar
	uint32_t ____lorwhayrairHeemepar_14;
	// System.Boolean GarbageiOS.M_voosa135::_zalljorwhou
	bool ____zalljorwhou_15;
	// System.UInt32 GarbageiOS.M_voosa135::_hemmor
	uint32_t ____hemmor_16;
	// System.Int32 GarbageiOS.M_voosa135::_wire
	int32_t ____wire_17;
	// System.Single GarbageiOS.M_voosa135::_dedaZefayyair
	float ____dedaZefayyair_18;
	// System.Single GarbageiOS.M_voosa135::_rasballWhijisfay
	float ____rasballWhijisfay_19;
	// System.String GarbageiOS.M_voosa135::_maimerekayPulawter
	String_t* ____maimerekayPulawter_20;
	// System.String GarbageiOS.M_voosa135::_kediser
	String_t* ____kediser_21;
	// System.Int32 GarbageiOS.M_voosa135::_furmawfas
	int32_t ____furmawfas_22;
	// System.String GarbageiOS.M_voosa135::_cermorcelDeredelmor
	String_t* ____cermorcelDeredelmor_23;
	// System.Boolean GarbageiOS.M_voosa135::_fiseSairrasi
	bool ____fiseSairrasi_24;
	// System.Single GarbageiOS.M_voosa135::_jiqi
	float ____jiqi_25;
	// System.Single GarbageiOS.M_voosa135::_cheevi
	float ____cheevi_26;
	// System.Boolean GarbageiOS.M_voosa135::_dralchoyeeSujearrem
	bool ____dralchoyeeSujearrem_27;
	// System.Single GarbageiOS.M_voosa135::_kerraylu
	float ____kerraylu_28;
	// System.Int32 GarbageiOS.M_voosa135::_haschutas
	int32_t ____haschutas_29;

public:
	inline static int32_t get_offset_of__mufur_0() { return static_cast<int32_t>(offsetof(M_voosa135_t825622399, ____mufur_0)); }
	inline String_t* get__mufur_0() const { return ____mufur_0; }
	inline String_t** get_address_of__mufur_0() { return &____mufur_0; }
	inline void set__mufur_0(String_t* value)
	{
		____mufur_0 = value;
		Il2CppCodeGenWriteBarrier(&____mufur_0, value);
	}

	inline static int32_t get_offset_of__cexemwe_1() { return static_cast<int32_t>(offsetof(M_voosa135_t825622399, ____cexemwe_1)); }
	inline String_t* get__cexemwe_1() const { return ____cexemwe_1; }
	inline String_t** get_address_of__cexemwe_1() { return &____cexemwe_1; }
	inline void set__cexemwe_1(String_t* value)
	{
		____cexemwe_1 = value;
		Il2CppCodeGenWriteBarrier(&____cexemwe_1, value);
	}

	inline static int32_t get_offset_of__leferloo_2() { return static_cast<int32_t>(offsetof(M_voosa135_t825622399, ____leferloo_2)); }
	inline String_t* get__leferloo_2() const { return ____leferloo_2; }
	inline String_t** get_address_of__leferloo_2() { return &____leferloo_2; }
	inline void set__leferloo_2(String_t* value)
	{
		____leferloo_2 = value;
		Il2CppCodeGenWriteBarrier(&____leferloo_2, value);
	}

	inline static int32_t get_offset_of__jawsemlasNesi_3() { return static_cast<int32_t>(offsetof(M_voosa135_t825622399, ____jawsemlasNesi_3)); }
	inline String_t* get__jawsemlasNesi_3() const { return ____jawsemlasNesi_3; }
	inline String_t** get_address_of__jawsemlasNesi_3() { return &____jawsemlasNesi_3; }
	inline void set__jawsemlasNesi_3(String_t* value)
	{
		____jawsemlasNesi_3 = value;
		Il2CppCodeGenWriteBarrier(&____jawsemlasNesi_3, value);
	}

	inline static int32_t get_offset_of__nearlijel_4() { return static_cast<int32_t>(offsetof(M_voosa135_t825622399, ____nearlijel_4)); }
	inline bool get__nearlijel_4() const { return ____nearlijel_4; }
	inline bool* get_address_of__nearlijel_4() { return &____nearlijel_4; }
	inline void set__nearlijel_4(bool value)
	{
		____nearlijel_4 = value;
	}

	inline static int32_t get_offset_of__lidooballBagorral_5() { return static_cast<int32_t>(offsetof(M_voosa135_t825622399, ____lidooballBagorral_5)); }
	inline String_t* get__lidooballBagorral_5() const { return ____lidooballBagorral_5; }
	inline String_t** get_address_of__lidooballBagorral_5() { return &____lidooballBagorral_5; }
	inline void set__lidooballBagorral_5(String_t* value)
	{
		____lidooballBagorral_5 = value;
		Il2CppCodeGenWriteBarrier(&____lidooballBagorral_5, value);
	}

	inline static int32_t get_offset_of__misoosemNasyeete_6() { return static_cast<int32_t>(offsetof(M_voosa135_t825622399, ____misoosemNasyeete_6)); }
	inline String_t* get__misoosemNasyeete_6() const { return ____misoosemNasyeete_6; }
	inline String_t** get_address_of__misoosemNasyeete_6() { return &____misoosemNasyeete_6; }
	inline void set__misoosemNasyeete_6(String_t* value)
	{
		____misoosemNasyeete_6 = value;
		Il2CppCodeGenWriteBarrier(&____misoosemNasyeete_6, value);
	}

	inline static int32_t get_offset_of__chasla_7() { return static_cast<int32_t>(offsetof(M_voosa135_t825622399, ____chasla_7)); }
	inline float get__chasla_7() const { return ____chasla_7; }
	inline float* get_address_of__chasla_7() { return &____chasla_7; }
	inline void set__chasla_7(float value)
	{
		____chasla_7 = value;
	}

	inline static int32_t get_offset_of__qemtroliBeta_8() { return static_cast<int32_t>(offsetof(M_voosa135_t825622399, ____qemtroliBeta_8)); }
	inline int32_t get__qemtroliBeta_8() const { return ____qemtroliBeta_8; }
	inline int32_t* get_address_of__qemtroliBeta_8() { return &____qemtroliBeta_8; }
	inline void set__qemtroliBeta_8(int32_t value)
	{
		____qemtroliBeta_8 = value;
	}

	inline static int32_t get_offset_of__yelba_9() { return static_cast<int32_t>(offsetof(M_voosa135_t825622399, ____yelba_9)); }
	inline float get__yelba_9() const { return ____yelba_9; }
	inline float* get_address_of__yelba_9() { return &____yelba_9; }
	inline void set__yelba_9(float value)
	{
		____yelba_9 = value;
	}

	inline static int32_t get_offset_of__dorcu_10() { return static_cast<int32_t>(offsetof(M_voosa135_t825622399, ____dorcu_10)); }
	inline bool get__dorcu_10() const { return ____dorcu_10; }
	inline bool* get_address_of__dorcu_10() { return &____dorcu_10; }
	inline void set__dorcu_10(bool value)
	{
		____dorcu_10 = value;
	}

	inline static int32_t get_offset_of__jecuFearyear_11() { return static_cast<int32_t>(offsetof(M_voosa135_t825622399, ____jecuFearyear_11)); }
	inline String_t* get__jecuFearyear_11() const { return ____jecuFearyear_11; }
	inline String_t** get_address_of__jecuFearyear_11() { return &____jecuFearyear_11; }
	inline void set__jecuFearyear_11(String_t* value)
	{
		____jecuFearyear_11 = value;
		Il2CppCodeGenWriteBarrier(&____jecuFearyear_11, value);
	}

	inline static int32_t get_offset_of__houstinai_12() { return static_cast<int32_t>(offsetof(M_voosa135_t825622399, ____houstinai_12)); }
	inline String_t* get__houstinai_12() const { return ____houstinai_12; }
	inline String_t** get_address_of__houstinai_12() { return &____houstinai_12; }
	inline void set__houstinai_12(String_t* value)
	{
		____houstinai_12 = value;
		Il2CppCodeGenWriteBarrier(&____houstinai_12, value);
	}

	inline static int32_t get_offset_of__wayzigeLelbeacall_13() { return static_cast<int32_t>(offsetof(M_voosa135_t825622399, ____wayzigeLelbeacall_13)); }
	inline uint32_t get__wayzigeLelbeacall_13() const { return ____wayzigeLelbeacall_13; }
	inline uint32_t* get_address_of__wayzigeLelbeacall_13() { return &____wayzigeLelbeacall_13; }
	inline void set__wayzigeLelbeacall_13(uint32_t value)
	{
		____wayzigeLelbeacall_13 = value;
	}

	inline static int32_t get_offset_of__lorwhayrairHeemepar_14() { return static_cast<int32_t>(offsetof(M_voosa135_t825622399, ____lorwhayrairHeemepar_14)); }
	inline uint32_t get__lorwhayrairHeemepar_14() const { return ____lorwhayrairHeemepar_14; }
	inline uint32_t* get_address_of__lorwhayrairHeemepar_14() { return &____lorwhayrairHeemepar_14; }
	inline void set__lorwhayrairHeemepar_14(uint32_t value)
	{
		____lorwhayrairHeemepar_14 = value;
	}

	inline static int32_t get_offset_of__zalljorwhou_15() { return static_cast<int32_t>(offsetof(M_voosa135_t825622399, ____zalljorwhou_15)); }
	inline bool get__zalljorwhou_15() const { return ____zalljorwhou_15; }
	inline bool* get_address_of__zalljorwhou_15() { return &____zalljorwhou_15; }
	inline void set__zalljorwhou_15(bool value)
	{
		____zalljorwhou_15 = value;
	}

	inline static int32_t get_offset_of__hemmor_16() { return static_cast<int32_t>(offsetof(M_voosa135_t825622399, ____hemmor_16)); }
	inline uint32_t get__hemmor_16() const { return ____hemmor_16; }
	inline uint32_t* get_address_of__hemmor_16() { return &____hemmor_16; }
	inline void set__hemmor_16(uint32_t value)
	{
		____hemmor_16 = value;
	}

	inline static int32_t get_offset_of__wire_17() { return static_cast<int32_t>(offsetof(M_voosa135_t825622399, ____wire_17)); }
	inline int32_t get__wire_17() const { return ____wire_17; }
	inline int32_t* get_address_of__wire_17() { return &____wire_17; }
	inline void set__wire_17(int32_t value)
	{
		____wire_17 = value;
	}

	inline static int32_t get_offset_of__dedaZefayyair_18() { return static_cast<int32_t>(offsetof(M_voosa135_t825622399, ____dedaZefayyair_18)); }
	inline float get__dedaZefayyair_18() const { return ____dedaZefayyair_18; }
	inline float* get_address_of__dedaZefayyair_18() { return &____dedaZefayyair_18; }
	inline void set__dedaZefayyair_18(float value)
	{
		____dedaZefayyair_18 = value;
	}

	inline static int32_t get_offset_of__rasballWhijisfay_19() { return static_cast<int32_t>(offsetof(M_voosa135_t825622399, ____rasballWhijisfay_19)); }
	inline float get__rasballWhijisfay_19() const { return ____rasballWhijisfay_19; }
	inline float* get_address_of__rasballWhijisfay_19() { return &____rasballWhijisfay_19; }
	inline void set__rasballWhijisfay_19(float value)
	{
		____rasballWhijisfay_19 = value;
	}

	inline static int32_t get_offset_of__maimerekayPulawter_20() { return static_cast<int32_t>(offsetof(M_voosa135_t825622399, ____maimerekayPulawter_20)); }
	inline String_t* get__maimerekayPulawter_20() const { return ____maimerekayPulawter_20; }
	inline String_t** get_address_of__maimerekayPulawter_20() { return &____maimerekayPulawter_20; }
	inline void set__maimerekayPulawter_20(String_t* value)
	{
		____maimerekayPulawter_20 = value;
		Il2CppCodeGenWriteBarrier(&____maimerekayPulawter_20, value);
	}

	inline static int32_t get_offset_of__kediser_21() { return static_cast<int32_t>(offsetof(M_voosa135_t825622399, ____kediser_21)); }
	inline String_t* get__kediser_21() const { return ____kediser_21; }
	inline String_t** get_address_of__kediser_21() { return &____kediser_21; }
	inline void set__kediser_21(String_t* value)
	{
		____kediser_21 = value;
		Il2CppCodeGenWriteBarrier(&____kediser_21, value);
	}

	inline static int32_t get_offset_of__furmawfas_22() { return static_cast<int32_t>(offsetof(M_voosa135_t825622399, ____furmawfas_22)); }
	inline int32_t get__furmawfas_22() const { return ____furmawfas_22; }
	inline int32_t* get_address_of__furmawfas_22() { return &____furmawfas_22; }
	inline void set__furmawfas_22(int32_t value)
	{
		____furmawfas_22 = value;
	}

	inline static int32_t get_offset_of__cermorcelDeredelmor_23() { return static_cast<int32_t>(offsetof(M_voosa135_t825622399, ____cermorcelDeredelmor_23)); }
	inline String_t* get__cermorcelDeredelmor_23() const { return ____cermorcelDeredelmor_23; }
	inline String_t** get_address_of__cermorcelDeredelmor_23() { return &____cermorcelDeredelmor_23; }
	inline void set__cermorcelDeredelmor_23(String_t* value)
	{
		____cermorcelDeredelmor_23 = value;
		Il2CppCodeGenWriteBarrier(&____cermorcelDeredelmor_23, value);
	}

	inline static int32_t get_offset_of__fiseSairrasi_24() { return static_cast<int32_t>(offsetof(M_voosa135_t825622399, ____fiseSairrasi_24)); }
	inline bool get__fiseSairrasi_24() const { return ____fiseSairrasi_24; }
	inline bool* get_address_of__fiseSairrasi_24() { return &____fiseSairrasi_24; }
	inline void set__fiseSairrasi_24(bool value)
	{
		____fiseSairrasi_24 = value;
	}

	inline static int32_t get_offset_of__jiqi_25() { return static_cast<int32_t>(offsetof(M_voosa135_t825622399, ____jiqi_25)); }
	inline float get__jiqi_25() const { return ____jiqi_25; }
	inline float* get_address_of__jiqi_25() { return &____jiqi_25; }
	inline void set__jiqi_25(float value)
	{
		____jiqi_25 = value;
	}

	inline static int32_t get_offset_of__cheevi_26() { return static_cast<int32_t>(offsetof(M_voosa135_t825622399, ____cheevi_26)); }
	inline float get__cheevi_26() const { return ____cheevi_26; }
	inline float* get_address_of__cheevi_26() { return &____cheevi_26; }
	inline void set__cheevi_26(float value)
	{
		____cheevi_26 = value;
	}

	inline static int32_t get_offset_of__dralchoyeeSujearrem_27() { return static_cast<int32_t>(offsetof(M_voosa135_t825622399, ____dralchoyeeSujearrem_27)); }
	inline bool get__dralchoyeeSujearrem_27() const { return ____dralchoyeeSujearrem_27; }
	inline bool* get_address_of__dralchoyeeSujearrem_27() { return &____dralchoyeeSujearrem_27; }
	inline void set__dralchoyeeSujearrem_27(bool value)
	{
		____dralchoyeeSujearrem_27 = value;
	}

	inline static int32_t get_offset_of__kerraylu_28() { return static_cast<int32_t>(offsetof(M_voosa135_t825622399, ____kerraylu_28)); }
	inline float get__kerraylu_28() const { return ____kerraylu_28; }
	inline float* get_address_of__kerraylu_28() { return &____kerraylu_28; }
	inline void set__kerraylu_28(float value)
	{
		____kerraylu_28 = value;
	}

	inline static int32_t get_offset_of__haschutas_29() { return static_cast<int32_t>(offsetof(M_voosa135_t825622399, ____haschutas_29)); }
	inline int32_t get__haschutas_29() const { return ____haschutas_29; }
	inline int32_t* get_address_of__haschutas_29() { return &____haschutas_29; }
	inline void set__haschutas_29(int32_t value)
	{
		____haschutas_29 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
