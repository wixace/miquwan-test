﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._a3f0b96b25ff1f517249ebc5d78cf977
struct _a3f0b96b25ff1f517249ebc5d78cf977_t3576449452;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__a3f0b96b25ff1f517249ebc53576449452.h"

// System.Void Little._a3f0b96b25ff1f517249ebc5d78cf977::.ctor()
extern "C"  void _a3f0b96b25ff1f517249ebc5d78cf977__ctor_m179371521 (_a3f0b96b25ff1f517249ebc5d78cf977_t3576449452 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._a3f0b96b25ff1f517249ebc5d78cf977::_a3f0b96b25ff1f517249ebc5d78cf977m2(System.Int32)
extern "C"  int32_t _a3f0b96b25ff1f517249ebc5d78cf977__a3f0b96b25ff1f517249ebc5d78cf977m2_m1828409881 (_a3f0b96b25ff1f517249ebc5d78cf977_t3576449452 * __this, int32_t ____a3f0b96b25ff1f517249ebc5d78cf977a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._a3f0b96b25ff1f517249ebc5d78cf977::_a3f0b96b25ff1f517249ebc5d78cf977m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _a3f0b96b25ff1f517249ebc5d78cf977__a3f0b96b25ff1f517249ebc5d78cf977m_m2721768637 (_a3f0b96b25ff1f517249ebc5d78cf977_t3576449452 * __this, int32_t ____a3f0b96b25ff1f517249ebc5d78cf977a0, int32_t ____a3f0b96b25ff1f517249ebc5d78cf97731, int32_t ____a3f0b96b25ff1f517249ebc5d78cf977c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._a3f0b96b25ff1f517249ebc5d78cf977::ilo__a3f0b96b25ff1f517249ebc5d78cf977m21(Little._a3f0b96b25ff1f517249ebc5d78cf977,System.Int32)
extern "C"  int32_t _a3f0b96b25ff1f517249ebc5d78cf977_ilo__a3f0b96b25ff1f517249ebc5d78cf977m21_m3822135539 (Il2CppObject * __this /* static, unused */, _a3f0b96b25ff1f517249ebc5d78cf977_t3576449452 * ____this0, int32_t ____a3f0b96b25ff1f517249ebc5d78cf977a1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
