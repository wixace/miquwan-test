﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIPanelGenerated/<UIPanel_onGeometryUpdated_GetDelegate_member1_arg0>c__AnonStoreyC8
struct U3CUIPanel_onGeometryUpdated_GetDelegate_member1_arg0U3Ec__AnonStoreyC8_t469740340;

#include "codegen/il2cpp-codegen.h"

// System.Void UIPanelGenerated/<UIPanel_onGeometryUpdated_GetDelegate_member1_arg0>c__AnonStoreyC8::.ctor()
extern "C"  void U3CUIPanel_onGeometryUpdated_GetDelegate_member1_arg0U3Ec__AnonStoreyC8__ctor_m2850507239 (U3CUIPanel_onGeometryUpdated_GetDelegate_member1_arg0U3Ec__AnonStoreyC8_t469740340 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPanelGenerated/<UIPanel_onGeometryUpdated_GetDelegate_member1_arg0>c__AnonStoreyC8::<>m__155()
extern "C"  void U3CUIPanel_onGeometryUpdated_GetDelegate_member1_arg0U3Ec__AnonStoreyC8_U3CU3Em__155_m2197704515 (U3CUIPanel_onGeometryUpdated_GetDelegate_member1_arg0U3Ec__AnonStoreyC8_t469740340 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
