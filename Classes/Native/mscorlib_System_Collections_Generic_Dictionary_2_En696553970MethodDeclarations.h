﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1190435706MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,herosCfg>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m3652258745(__this, ___dictionary0, method) ((  void (*) (Enumerator_t696553970 *, Dictionary_2_t3674197874 *, const MethodInfo*))Enumerator__ctor_m2377115088_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,herosCfg>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2396645448(__this, method) ((  Il2CppObject * (*) (Enumerator_t696553970 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1037642267_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,herosCfg>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1225294684(__this, method) ((  void (*) (Enumerator_t696553970 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2809374949_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,herosCfg>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3231309733(__this, method) ((  DictionaryEntry_t1751606614  (*) (Enumerator_t696553970 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2434214620_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,herosCfg>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2619068196(__this, method) ((  Il2CppObject * (*) (Enumerator_t696553970 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3735627447_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,herosCfg>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1122137270(__this, method) ((  Il2CppObject * (*) (Enumerator_t696553970 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m393753481_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,herosCfg>::MoveNext()
#define Enumerator_MoveNext_m2592166088(__this, method) ((  bool (*) (Enumerator_t696553970 *, const MethodInfo*))Enumerator_MoveNext_m1213995029_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,herosCfg>::get_Current()
#define Enumerator_get_Current_m404031464(__this, method) ((  KeyValuePair_2_t3572978580  (*) (Enumerator_t696553970 *, const MethodInfo*))Enumerator_get_Current_m1399860359_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,herosCfg>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m492052501(__this, method) ((  int32_t (*) (Enumerator_t696553970 *, const MethodInfo*))Enumerator_get_CurrentKey_m1767398110_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,herosCfg>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m2857699577(__this, method) ((  herosCfg_t3676934635 * (*) (Enumerator_t696553970 *, const MethodInfo*))Enumerator_get_CurrentValue_m3384846750_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,herosCfg>::Reset()
#define Enumerator_Reset_m860561547(__this, method) ((  void (*) (Enumerator_t696553970 *, const MethodInfo*))Enumerator_Reset_m1080084514_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,herosCfg>::VerifyState()
#define Enumerator_VerifyState_m25730132(__this, method) ((  void (*) (Enumerator_t696553970 *, const MethodInfo*))Enumerator_VerifyState_m2404513451_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,herosCfg>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m1701724860(__this, method) ((  void (*) (Enumerator_t696553970 *, const MethodInfo*))Enumerator_VerifyCurrent_m2789892947_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,herosCfg>::Dispose()
#define Enumerator_Dispose_m594387611(__this, method) ((  void (*) (Enumerator_t696553970 *, const MethodInfo*))Enumerator_Dispose_m1102561394_gshared)(__this, method)
