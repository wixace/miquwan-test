﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FlowControl.FlowBase
struct FlowBase_t3680091731;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_FlowControl_FlowBase3680091731.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void FlowControl.FlowBase::.ctor()
extern "C"  void FlowBase__ctor_m1576788029 (FlowBase_t3680091731 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlowControl.FlowBase::FlowControl.IFlowBase.Activate()
extern "C"  void FlowBase_FlowControl_IFlowBase_Activate_m1263026079 (FlowBase_t3680091731 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlowControl.FlowBase::FlowControl.IFlowUpdate.FlowUpdate(System.Single)
extern "C"  void FlowBase_FlowControl_IFlowUpdate_FlowUpdate_m3118114688 (FlowBase_t3680091731 * __this, float ___duringTime0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlowControl.FlowBase::FlowControl.IFlowBase.OnComplete()
extern "C"  void FlowBase_FlowControl_IFlowBase_OnComplete_m2287801060 (FlowBase_t3680091731 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlowControl.FlowBase::Activate()
extern "C"  void FlowBase_Activate_m4030468762 (FlowBase_t3680091731 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlowControl.FlowBase::Destroy()
extern "C"  void FlowBase_Destroy_m804003925 (FlowBase_t3680091731 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlowControl.FlowBase::FlowUpdate(System.Single)
extern "C"  void FlowBase_FlowUpdate_m765065133 (FlowBase_t3680091731 * __this, float ___duringTime0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlowControl.FlowBase::OnComplete()
extern "C"  void FlowBase_OnComplete_m3215463199 (FlowBase_t3680091731 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlowControl.FlowBase::Reset()
extern "C"  void FlowBase_Reset_m3518188266 (FlowBase_t3680091731 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 FlowControl.FlowBase::get_id()
extern "C"  int32_t FlowBase_get_id_m3013706489 (FlowBase_t3680091731 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlowControl.FlowBase::set_id(System.Int32)
extern "C"  void FlowBase_set_id_m3698779824 (FlowBase_t3680091731 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FlowControl.FlowBase::get_isActive()
extern "C"  bool FlowBase_get_isActive_m2584597844 (FlowBase_t3680091731 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlowControl.FlowBase::set_isActive(System.Boolean)
extern "C"  void FlowBase_set_isActive_m2784732811 (FlowBase_t3680091731 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FlowControl.FlowBase::get_isComplete()
extern "C"  bool FlowBase_get_isComplete_m3018606471 (FlowBase_t3680091731 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlowControl.FlowBase::set_isComplete(System.Boolean)
extern "C"  void FlowBase_set_isComplete_m186389374 (FlowBase_t3680091731 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FlowControl.FlowBase::get_isDestroy()
extern "C"  bool FlowBase_get_isDestroy_m300477774 (FlowBase_t3680091731 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlowControl.FlowBase::set_isDestroy(System.Boolean)
extern "C"  void FlowBase_set_isDestroy_m600185733 (FlowBase_t3680091731 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String FlowControl.FlowBase::get_name()
extern "C"  String_t* FlowBase_get_name_m3455051070 (FlowBase_t3680091731 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlowControl.FlowBase::set_name(System.String)
extern "C"  void FlowBase_set_name_m2075065043 (FlowBase_t3680091731 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlowControl.FlowBase::ilo_set_isActive1(FlowControl.FlowBase,System.Boolean)
extern "C"  void FlowBase_ilo_set_isActive1_m3183586543 (Il2CppObject * __this /* static, unused */, FlowBase_t3680091731 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlowControl.FlowBase::ilo_set_isComplete2(FlowControl.FlowBase,System.Boolean)
extern "C"  void FlowBase_ilo_set_isComplete2_m551804289 (Il2CppObject * __this /* static, unused */, FlowBase_t3680091731 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String FlowControl.FlowBase::ilo_get_name3(FlowControl.FlowBase)
extern "C"  String_t* FlowBase_ilo_get_name3_m3259709542 (Il2CppObject * __this /* static, unused */, FlowBase_t3680091731 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlowControl.FlowBase::ilo_Debug4(System.Object,System.Boolean,System.Int32)
extern "C"  void FlowBase_ilo_Debug4_m927560275 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, int32_t ___user2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlowControl.FlowBase::ilo_set_isDestroy5(FlowControl.FlowBase,System.Boolean)
extern "C"  void FlowBase_ilo_set_isDestroy5_m3926841951 (Il2CppObject * __this /* static, unused */, FlowBase_t3680091731 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
