﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_EventSystems_IEventSystemHandlerGenerated
struct UnityEngine_EventSystems_IEventSystemHandlerGenerated_t2217879154;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine_EventSystems_IEventSystemHandlerGenerated::.ctor()
extern "C"  void UnityEngine_EventSystems_IEventSystemHandlerGenerated__ctor_m2226541369 (UnityEngine_EventSystems_IEventSystemHandlerGenerated_t2217879154 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_IEventSystemHandlerGenerated::__Register()
extern "C"  void UnityEngine_EventSystems_IEventSystemHandlerGenerated___Register_m1714111790 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
