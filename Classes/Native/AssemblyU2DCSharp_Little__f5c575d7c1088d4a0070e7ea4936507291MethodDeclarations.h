﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._f5c575d7c1088d4a0070e7ea4380e1a8
struct _f5c575d7c1088d4a0070e7ea4380e1a8_t936507291;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._f5c575d7c1088d4a0070e7ea4380e1a8::.ctor()
extern "C"  void _f5c575d7c1088d4a0070e7ea4380e1a8__ctor_m2209254642 (_f5c575d7c1088d4a0070e7ea4380e1a8_t936507291 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._f5c575d7c1088d4a0070e7ea4380e1a8::_f5c575d7c1088d4a0070e7ea4380e1a8m2(System.Int32)
extern "C"  int32_t _f5c575d7c1088d4a0070e7ea4380e1a8__f5c575d7c1088d4a0070e7ea4380e1a8m2_m167941049 (_f5c575d7c1088d4a0070e7ea4380e1a8_t936507291 * __this, int32_t ____f5c575d7c1088d4a0070e7ea4380e1a8a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._f5c575d7c1088d4a0070e7ea4380e1a8::_f5c575d7c1088d4a0070e7ea4380e1a8m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _f5c575d7c1088d4a0070e7ea4380e1a8__f5c575d7c1088d4a0070e7ea4380e1a8m_m1090696989 (_f5c575d7c1088d4a0070e7ea4380e1a8_t936507291 * __this, int32_t ____f5c575d7c1088d4a0070e7ea4380e1a8a0, int32_t ____f5c575d7c1088d4a0070e7ea4380e1a8681, int32_t ____f5c575d7c1088d4a0070e7ea4380e1a8c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
