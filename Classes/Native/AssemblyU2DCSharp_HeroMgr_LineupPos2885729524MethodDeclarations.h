﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HeroMgr/LineupPos
struct LineupPos_t2885729524;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void HeroMgr/LineupPos::.ctor(UnityEngine.Vector3)
extern "C"  void LineupPos__ctor_m1897628546 (LineupPos_t2885729524 * __this, Vector3_t4282066566  ___pos0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroMgr/LineupPos::set_Pos(UnityEngine.Vector3)
extern "C"  void LineupPos_set_Pos_m3092231693 (LineupPos_t2885729524 * __this, Vector3_t4282066566  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 HeroMgr/LineupPos::get_Pos()
extern "C"  Vector3_t4282066566  LineupPos_get_Pos_m798786746 (LineupPos_t2885729524 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroMgr/LineupPos::Rigester()
extern "C"  void LineupPos_Rigester_m4266424912 (LineupPos_t2885729524 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroMgr/LineupPos::UnRigester()
extern "C"  void LineupPos_UnRigester_m62934729 (LineupPos_t2885729524 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroMgr/LineupPos::get_IsEmpty()
extern "C"  bool LineupPos_get_IsEmpty_m2240060323 (LineupPos_t2885729524 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
