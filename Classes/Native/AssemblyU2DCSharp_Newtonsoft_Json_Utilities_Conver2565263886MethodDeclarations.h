﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Func`2<System.Object,System.Object>
struct Func_2_t184564025;
// System.Type
struct Type_t;
// System.ComponentModel.TypeConverter
struct TypeConverter_t1753450284;
// System.Object
struct Il2CppObject;
// System.Globalization.CultureInfo
struct CultureInfo_t1065375142;
// Newtonsoft.Json.Utilities.ReflectionDelegateFactory
struct ReflectionDelegateFactory_t1590616920;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_Convert866134174.h"
#include "mscorlib_System_Type2863145774.h"
#include "System_System_ComponentModel_TypeConverter1753450284.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Globalization_CultureInfo1065375142.h"

// System.Void Newtonsoft.Json.Utilities.ConvertUtils::.cctor()
extern "C"  void ConvertUtils__cctor_m914182345 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Func`2<System.Object,System.Object> Newtonsoft.Json.Utilities.ConvertUtils::CreateCastConverter(Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey)
extern "C"  Func_2_t184564025 * ConvertUtils_CreateCastConverter_m3653731189 (Il2CppObject * __this /* static, unused */, TypeConvertKey_t866134174  ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Utilities.ConvertUtils::CanConvertType(System.Type,System.Type,System.Boolean)
extern "C"  bool ConvertUtils_CanConvertType_m732072898 (Il2CppObject * __this /* static, unused */, Type_t * ___initialType0, Type_t * ___targetType1, bool ___allowTypeNameToString2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Utilities.ConvertUtils::IsComponentConverter(System.ComponentModel.TypeConverter)
extern "C"  bool ConvertUtils_IsComponentConverter_m1333306734 (Il2CppObject * __this /* static, unused */, TypeConverter_t1753450284 * ___converter0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Utilities.ConvertUtils::Convert(System.Object,System.Globalization.CultureInfo,System.Type)
extern "C"  Il2CppObject * ConvertUtils_Convert_m3745593205 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___initialValue0, CultureInfo_t1065375142 * ___culture1, Type_t * ___targetType2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Utilities.ConvertUtils::TryConvert(System.Object,System.Globalization.CultureInfo,System.Type,System.Object&)
extern "C"  bool ConvertUtils_TryConvert_m3804558589 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___initialValue0, CultureInfo_t1065375142 * ___culture1, Type_t * ___targetType2, Il2CppObject ** ___convertedValue3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Utilities.ConvertUtils::ConvertOrCast(System.Object,System.Globalization.CultureInfo,System.Type)
extern "C"  Il2CppObject * ConvertUtils_ConvertOrCast_m760833303 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___initialValue0, CultureInfo_t1065375142 * ___culture1, Type_t * ___targetType2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Utilities.ConvertUtils::TryConvertOrCast(System.Object,System.Globalization.CultureInfo,System.Type,System.Object&)
extern "C"  bool ConvertUtils_TryConvertOrCast_m75399195 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___initialValue0, CultureInfo_t1065375142 * ___culture1, Type_t * ___targetType2, Il2CppObject ** ___convertedValue3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Utilities.ConvertUtils::EnsureTypeAssignable(System.Object,System.Type,System.Type)
extern "C"  Il2CppObject * ConvertUtils_EnsureTypeAssignable_m712946340 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, Type_t * ___initialType1, Type_t * ___targetType2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.ComponentModel.TypeConverter Newtonsoft.Json.Utilities.ConvertUtils::GetConverter(System.Type)
extern "C"  TypeConverter_t1753450284 * ConvertUtils_GetConverter_m1269630463 (Il2CppObject * __this /* static, unused */, Type_t * ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Utilities.ConvertUtils::IsInteger(System.Object)
extern "C"  bool ConvertUtils_IsInteger_m1657944146 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Newtonsoft.Json.Utilities.ConvertUtils::ilo_get_TargetType1(Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey&)
extern "C"  Type_t * ConvertUtils_ilo_get_TargetType1_m3629581194 (Il2CppObject * __this /* static, unused */, TypeConvertKey_t866134174 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Newtonsoft.Json.Utilities.ConvertUtils::ilo_get_InitialType2(Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey&)
extern "C"  Type_t * ConvertUtils_ilo_get_InitialType2_m4196983234 (Il2CppObject * __this /* static, unused */, TypeConvertKey_t866134174 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Utilities.ReflectionDelegateFactory Newtonsoft.Json.Utilities.ConvertUtils::ilo_get_ReflectionDelegateFactory3()
extern "C"  ReflectionDelegateFactory_t1590616920 * ConvertUtils_ilo_get_ReflectionDelegateFactory3_m2095010311 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Utilities.ConvertUtils::ilo_IsNullableType4(System.Type)
extern "C"  bool ConvertUtils_ilo_IsNullableType4_m2320207343 (Il2CppObject * __this /* static, unused */, Type_t * ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Utilities.ConvertUtils::ilo_IsComponentConverter5(System.ComponentModel.TypeConverter)
extern "C"  bool ConvertUtils_ilo_IsComponentConverter5_m1892667084 (Il2CppObject * __this /* static, unused */, TypeConverter_t1753450284 * ___converter0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.ComponentModel.TypeConverter Newtonsoft.Json.Utilities.ConvertUtils::ilo_GetConverter6(System.Type)
extern "C"  TypeConverter_t1753450284 * ConvertUtils_ilo_GetConverter6_m3316861710 (Il2CppObject * __this /* static, unused */, Type_t * ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
