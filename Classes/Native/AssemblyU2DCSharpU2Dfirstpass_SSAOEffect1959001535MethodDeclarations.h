﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SSAOEffect
struct SSAOEffect_t1959001535;
// UnityEngine.Material
struct Material_t3870600107;
// UnityEngine.Shader
struct Shader_t3191267369;
// UnityEngine.RenderTexture
struct RenderTexture_t1963041563;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Shader3191267369.h"
#include "UnityEngine_UnityEngine_Material3870600107.h"
#include "UnityEngine_UnityEngine_RenderTexture1963041563.h"

// System.Void SSAOEffect::.ctor()
extern "C"  void SSAOEffect__ctor_m99164016 (SSAOEffect_t1959001535 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material SSAOEffect::CreateMaterial(UnityEngine.Shader)
extern "C"  Material_t3870600107 * SSAOEffect_CreateMaterial_m1199633154 (Il2CppObject * __this /* static, unused */, Shader_t3191267369 * ___shader0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SSAOEffect::DestroyMaterial(UnityEngine.Material)
extern "C"  void SSAOEffect_DestroyMaterial_m4059222193 (Il2CppObject * __this /* static, unused */, Material_t3870600107 * ___mat0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SSAOEffect::OnDisable()
extern "C"  void SSAOEffect_OnDisable_m2993896407 (SSAOEffect_t1959001535 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SSAOEffect::Start()
extern "C"  void SSAOEffect_Start_m3341269104 (SSAOEffect_t1959001535 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SSAOEffect::OnEnable()
extern "C"  void SSAOEffect_OnEnable_m1329279510 (SSAOEffect_t1959001535 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SSAOEffect::CreateMaterials()
extern "C"  void SSAOEffect_CreateMaterials_m675868382 (SSAOEffect_t1959001535 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SSAOEffect::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void SSAOEffect_OnRenderImage_m1991603950 (SSAOEffect_t1959001535 * __this, RenderTexture_t1963041563 * ___source0, RenderTexture_t1963041563 * ___destination1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
