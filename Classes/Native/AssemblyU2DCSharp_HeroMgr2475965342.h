﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// HeroMgr
struct HeroMgr_t2475965342;
// System.Action
struct Action_t3771233898;
// System.Collections.Generic.List`1<HeroMgr/LineupPos>
struct List_1_t4253915076;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.Collections.Generic.List`1<Hero>
struct List_1_t1370431210;
// GameMgr
struct GameMgr_t1469029542;
// LevelConfigManager
struct LevelConfigManager_t657947911;
// JSCLevelHeroConfig
struct JSCLevelHeroConfig_t1953226502;
// Hero
struct Hero_t2245658;
// Guide
struct Guide_t69159644;
// CameraSmoothFollow
struct CameraSmoothFollow_t2624612068;
// System.Collections.Generic.List`1<CSHeroUnit>
struct List_1_t837576702;
// System.Comparison`1<Hero>
struct Comparison_1_t3013574141;
// System.Comparison`1<UnityEngine.GameObject>
struct Comparison_1_t2391043192;

#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HeroMgr
struct  HeroMgr_t2475965342  : public Il2CppObject
{
public:
	// System.Action HeroMgr::LeaveCallBack
	Action_t3771233898 * ___LeaveCallBack_7;
	// System.Int32 HeroMgr::killMonsterCount
	int32_t ___killMonsterCount_8;
	// System.Single HeroMgr::friendsNpcMaxHp
	float ___friendsNpcMaxHp_9;
	// System.Single HeroMgr::herosMaxHp
	float ___herosMaxHp_10;
	// System.Single HeroMgr::biaocheMaxHp
	float ___biaocheMaxHp_11;
	// UnityEngine.Quaternion HeroMgr::intRot
	Quaternion_t1553702882  ___intRot_12;
	// System.Collections.Generic.List`1<HeroMgr/LineupPos> HeroMgr::placeList
	List_1_t4253915076 * ___placeList_13;
	// System.Int32 HeroMgr::waitTargetReachedNum
	int32_t ___waitTargetReachedNum_14;
	// UnityEngine.GameObject HeroMgr::heroContainer
	GameObject_t3674682005 * ___heroContainer_15;
	// System.Collections.Generic.List`1<Hero> HeroMgr::herolist
	List_1_t1370431210 * ___herolist_16;
	// GameMgr HeroMgr::gameMgr
	GameMgr_t1469029542 * ___gameMgr_17;
	// LevelConfigManager HeroMgr::levelCfgMgr
	LevelConfigManager_t657947911 * ___levelCfgMgr_18;
	// JSCLevelHeroConfig HeroMgr::herosCfg
	JSCLevelHeroConfig_t1953226502 * ___herosCfg_19;
	// Hero HeroMgr::gadHero
	Hero_t2245658 * ___gadHero_20;
	// Guide HeroMgr::guide
	Guide_t69159644 * ___guide_21;
	// System.Int32 HeroMgr::deadHeroNum
	int32_t ___deadHeroNum_22;
	// System.Int32 HeroMgr::curRound
	int32_t ___curRound_23;
	// System.Int32 HeroMgr::power
	int32_t ___power_24;
	// System.Boolean HeroMgr::isPowerBuff
	bool ___isPowerBuff_25;
	// System.Boolean HeroMgr::isLeaveBattleOnWin
	bool ___isLeaveBattleOnWin_26;
	// CameraSmoothFollow HeroMgr::cameraSmoothFollow
	CameraSmoothFollow_t2624612068 * ___cameraSmoothFollow_27;
	// UnityEngine.GameObject HeroMgr::cameraTargetObj
	GameObject_t3674682005 * ___cameraTargetObj_28;
	// System.Collections.Generic.List`1<CSHeroUnit> HeroMgr::heroUnits
	List_1_t837576702 * ___heroUnits_29;
	// System.Int32 HeroMgr::heroIndex
	int32_t ___heroIndex_30;
	// UnityEngine.Vector3 HeroMgr::m_Vec
	Vector3_t4282066566  ___m_Vec_31;
	// System.Boolean HeroMgr::<isWaitTargetReached>k__BackingField
	bool ___U3CisWaitTargetReachedU3Ek__BackingField_32;
	// Hero HeroMgr::<captain>k__BackingField
	Hero_t2245658 * ___U3CcaptainU3Ek__BackingField_33;

public:
	inline static int32_t get_offset_of_LeaveCallBack_7() { return static_cast<int32_t>(offsetof(HeroMgr_t2475965342, ___LeaveCallBack_7)); }
	inline Action_t3771233898 * get_LeaveCallBack_7() const { return ___LeaveCallBack_7; }
	inline Action_t3771233898 ** get_address_of_LeaveCallBack_7() { return &___LeaveCallBack_7; }
	inline void set_LeaveCallBack_7(Action_t3771233898 * value)
	{
		___LeaveCallBack_7 = value;
		Il2CppCodeGenWriteBarrier(&___LeaveCallBack_7, value);
	}

	inline static int32_t get_offset_of_killMonsterCount_8() { return static_cast<int32_t>(offsetof(HeroMgr_t2475965342, ___killMonsterCount_8)); }
	inline int32_t get_killMonsterCount_8() const { return ___killMonsterCount_8; }
	inline int32_t* get_address_of_killMonsterCount_8() { return &___killMonsterCount_8; }
	inline void set_killMonsterCount_8(int32_t value)
	{
		___killMonsterCount_8 = value;
	}

	inline static int32_t get_offset_of_friendsNpcMaxHp_9() { return static_cast<int32_t>(offsetof(HeroMgr_t2475965342, ___friendsNpcMaxHp_9)); }
	inline float get_friendsNpcMaxHp_9() const { return ___friendsNpcMaxHp_9; }
	inline float* get_address_of_friendsNpcMaxHp_9() { return &___friendsNpcMaxHp_9; }
	inline void set_friendsNpcMaxHp_9(float value)
	{
		___friendsNpcMaxHp_9 = value;
	}

	inline static int32_t get_offset_of_herosMaxHp_10() { return static_cast<int32_t>(offsetof(HeroMgr_t2475965342, ___herosMaxHp_10)); }
	inline float get_herosMaxHp_10() const { return ___herosMaxHp_10; }
	inline float* get_address_of_herosMaxHp_10() { return &___herosMaxHp_10; }
	inline void set_herosMaxHp_10(float value)
	{
		___herosMaxHp_10 = value;
	}

	inline static int32_t get_offset_of_biaocheMaxHp_11() { return static_cast<int32_t>(offsetof(HeroMgr_t2475965342, ___biaocheMaxHp_11)); }
	inline float get_biaocheMaxHp_11() const { return ___biaocheMaxHp_11; }
	inline float* get_address_of_biaocheMaxHp_11() { return &___biaocheMaxHp_11; }
	inline void set_biaocheMaxHp_11(float value)
	{
		___biaocheMaxHp_11 = value;
	}

	inline static int32_t get_offset_of_intRot_12() { return static_cast<int32_t>(offsetof(HeroMgr_t2475965342, ___intRot_12)); }
	inline Quaternion_t1553702882  get_intRot_12() const { return ___intRot_12; }
	inline Quaternion_t1553702882 * get_address_of_intRot_12() { return &___intRot_12; }
	inline void set_intRot_12(Quaternion_t1553702882  value)
	{
		___intRot_12 = value;
	}

	inline static int32_t get_offset_of_placeList_13() { return static_cast<int32_t>(offsetof(HeroMgr_t2475965342, ___placeList_13)); }
	inline List_1_t4253915076 * get_placeList_13() const { return ___placeList_13; }
	inline List_1_t4253915076 ** get_address_of_placeList_13() { return &___placeList_13; }
	inline void set_placeList_13(List_1_t4253915076 * value)
	{
		___placeList_13 = value;
		Il2CppCodeGenWriteBarrier(&___placeList_13, value);
	}

	inline static int32_t get_offset_of_waitTargetReachedNum_14() { return static_cast<int32_t>(offsetof(HeroMgr_t2475965342, ___waitTargetReachedNum_14)); }
	inline int32_t get_waitTargetReachedNum_14() const { return ___waitTargetReachedNum_14; }
	inline int32_t* get_address_of_waitTargetReachedNum_14() { return &___waitTargetReachedNum_14; }
	inline void set_waitTargetReachedNum_14(int32_t value)
	{
		___waitTargetReachedNum_14 = value;
	}

	inline static int32_t get_offset_of_heroContainer_15() { return static_cast<int32_t>(offsetof(HeroMgr_t2475965342, ___heroContainer_15)); }
	inline GameObject_t3674682005 * get_heroContainer_15() const { return ___heroContainer_15; }
	inline GameObject_t3674682005 ** get_address_of_heroContainer_15() { return &___heroContainer_15; }
	inline void set_heroContainer_15(GameObject_t3674682005 * value)
	{
		___heroContainer_15 = value;
		Il2CppCodeGenWriteBarrier(&___heroContainer_15, value);
	}

	inline static int32_t get_offset_of_herolist_16() { return static_cast<int32_t>(offsetof(HeroMgr_t2475965342, ___herolist_16)); }
	inline List_1_t1370431210 * get_herolist_16() const { return ___herolist_16; }
	inline List_1_t1370431210 ** get_address_of_herolist_16() { return &___herolist_16; }
	inline void set_herolist_16(List_1_t1370431210 * value)
	{
		___herolist_16 = value;
		Il2CppCodeGenWriteBarrier(&___herolist_16, value);
	}

	inline static int32_t get_offset_of_gameMgr_17() { return static_cast<int32_t>(offsetof(HeroMgr_t2475965342, ___gameMgr_17)); }
	inline GameMgr_t1469029542 * get_gameMgr_17() const { return ___gameMgr_17; }
	inline GameMgr_t1469029542 ** get_address_of_gameMgr_17() { return &___gameMgr_17; }
	inline void set_gameMgr_17(GameMgr_t1469029542 * value)
	{
		___gameMgr_17 = value;
		Il2CppCodeGenWriteBarrier(&___gameMgr_17, value);
	}

	inline static int32_t get_offset_of_levelCfgMgr_18() { return static_cast<int32_t>(offsetof(HeroMgr_t2475965342, ___levelCfgMgr_18)); }
	inline LevelConfigManager_t657947911 * get_levelCfgMgr_18() const { return ___levelCfgMgr_18; }
	inline LevelConfigManager_t657947911 ** get_address_of_levelCfgMgr_18() { return &___levelCfgMgr_18; }
	inline void set_levelCfgMgr_18(LevelConfigManager_t657947911 * value)
	{
		___levelCfgMgr_18 = value;
		Il2CppCodeGenWriteBarrier(&___levelCfgMgr_18, value);
	}

	inline static int32_t get_offset_of_herosCfg_19() { return static_cast<int32_t>(offsetof(HeroMgr_t2475965342, ___herosCfg_19)); }
	inline JSCLevelHeroConfig_t1953226502 * get_herosCfg_19() const { return ___herosCfg_19; }
	inline JSCLevelHeroConfig_t1953226502 ** get_address_of_herosCfg_19() { return &___herosCfg_19; }
	inline void set_herosCfg_19(JSCLevelHeroConfig_t1953226502 * value)
	{
		___herosCfg_19 = value;
		Il2CppCodeGenWriteBarrier(&___herosCfg_19, value);
	}

	inline static int32_t get_offset_of_gadHero_20() { return static_cast<int32_t>(offsetof(HeroMgr_t2475965342, ___gadHero_20)); }
	inline Hero_t2245658 * get_gadHero_20() const { return ___gadHero_20; }
	inline Hero_t2245658 ** get_address_of_gadHero_20() { return &___gadHero_20; }
	inline void set_gadHero_20(Hero_t2245658 * value)
	{
		___gadHero_20 = value;
		Il2CppCodeGenWriteBarrier(&___gadHero_20, value);
	}

	inline static int32_t get_offset_of_guide_21() { return static_cast<int32_t>(offsetof(HeroMgr_t2475965342, ___guide_21)); }
	inline Guide_t69159644 * get_guide_21() const { return ___guide_21; }
	inline Guide_t69159644 ** get_address_of_guide_21() { return &___guide_21; }
	inline void set_guide_21(Guide_t69159644 * value)
	{
		___guide_21 = value;
		Il2CppCodeGenWriteBarrier(&___guide_21, value);
	}

	inline static int32_t get_offset_of_deadHeroNum_22() { return static_cast<int32_t>(offsetof(HeroMgr_t2475965342, ___deadHeroNum_22)); }
	inline int32_t get_deadHeroNum_22() const { return ___deadHeroNum_22; }
	inline int32_t* get_address_of_deadHeroNum_22() { return &___deadHeroNum_22; }
	inline void set_deadHeroNum_22(int32_t value)
	{
		___deadHeroNum_22 = value;
	}

	inline static int32_t get_offset_of_curRound_23() { return static_cast<int32_t>(offsetof(HeroMgr_t2475965342, ___curRound_23)); }
	inline int32_t get_curRound_23() const { return ___curRound_23; }
	inline int32_t* get_address_of_curRound_23() { return &___curRound_23; }
	inline void set_curRound_23(int32_t value)
	{
		___curRound_23 = value;
	}

	inline static int32_t get_offset_of_power_24() { return static_cast<int32_t>(offsetof(HeroMgr_t2475965342, ___power_24)); }
	inline int32_t get_power_24() const { return ___power_24; }
	inline int32_t* get_address_of_power_24() { return &___power_24; }
	inline void set_power_24(int32_t value)
	{
		___power_24 = value;
	}

	inline static int32_t get_offset_of_isPowerBuff_25() { return static_cast<int32_t>(offsetof(HeroMgr_t2475965342, ___isPowerBuff_25)); }
	inline bool get_isPowerBuff_25() const { return ___isPowerBuff_25; }
	inline bool* get_address_of_isPowerBuff_25() { return &___isPowerBuff_25; }
	inline void set_isPowerBuff_25(bool value)
	{
		___isPowerBuff_25 = value;
	}

	inline static int32_t get_offset_of_isLeaveBattleOnWin_26() { return static_cast<int32_t>(offsetof(HeroMgr_t2475965342, ___isLeaveBattleOnWin_26)); }
	inline bool get_isLeaveBattleOnWin_26() const { return ___isLeaveBattleOnWin_26; }
	inline bool* get_address_of_isLeaveBattleOnWin_26() { return &___isLeaveBattleOnWin_26; }
	inline void set_isLeaveBattleOnWin_26(bool value)
	{
		___isLeaveBattleOnWin_26 = value;
	}

	inline static int32_t get_offset_of_cameraSmoothFollow_27() { return static_cast<int32_t>(offsetof(HeroMgr_t2475965342, ___cameraSmoothFollow_27)); }
	inline CameraSmoothFollow_t2624612068 * get_cameraSmoothFollow_27() const { return ___cameraSmoothFollow_27; }
	inline CameraSmoothFollow_t2624612068 ** get_address_of_cameraSmoothFollow_27() { return &___cameraSmoothFollow_27; }
	inline void set_cameraSmoothFollow_27(CameraSmoothFollow_t2624612068 * value)
	{
		___cameraSmoothFollow_27 = value;
		Il2CppCodeGenWriteBarrier(&___cameraSmoothFollow_27, value);
	}

	inline static int32_t get_offset_of_cameraTargetObj_28() { return static_cast<int32_t>(offsetof(HeroMgr_t2475965342, ___cameraTargetObj_28)); }
	inline GameObject_t3674682005 * get_cameraTargetObj_28() const { return ___cameraTargetObj_28; }
	inline GameObject_t3674682005 ** get_address_of_cameraTargetObj_28() { return &___cameraTargetObj_28; }
	inline void set_cameraTargetObj_28(GameObject_t3674682005 * value)
	{
		___cameraTargetObj_28 = value;
		Il2CppCodeGenWriteBarrier(&___cameraTargetObj_28, value);
	}

	inline static int32_t get_offset_of_heroUnits_29() { return static_cast<int32_t>(offsetof(HeroMgr_t2475965342, ___heroUnits_29)); }
	inline List_1_t837576702 * get_heroUnits_29() const { return ___heroUnits_29; }
	inline List_1_t837576702 ** get_address_of_heroUnits_29() { return &___heroUnits_29; }
	inline void set_heroUnits_29(List_1_t837576702 * value)
	{
		___heroUnits_29 = value;
		Il2CppCodeGenWriteBarrier(&___heroUnits_29, value);
	}

	inline static int32_t get_offset_of_heroIndex_30() { return static_cast<int32_t>(offsetof(HeroMgr_t2475965342, ___heroIndex_30)); }
	inline int32_t get_heroIndex_30() const { return ___heroIndex_30; }
	inline int32_t* get_address_of_heroIndex_30() { return &___heroIndex_30; }
	inline void set_heroIndex_30(int32_t value)
	{
		___heroIndex_30 = value;
	}

	inline static int32_t get_offset_of_m_Vec_31() { return static_cast<int32_t>(offsetof(HeroMgr_t2475965342, ___m_Vec_31)); }
	inline Vector3_t4282066566  get_m_Vec_31() const { return ___m_Vec_31; }
	inline Vector3_t4282066566 * get_address_of_m_Vec_31() { return &___m_Vec_31; }
	inline void set_m_Vec_31(Vector3_t4282066566  value)
	{
		___m_Vec_31 = value;
	}

	inline static int32_t get_offset_of_U3CisWaitTargetReachedU3Ek__BackingField_32() { return static_cast<int32_t>(offsetof(HeroMgr_t2475965342, ___U3CisWaitTargetReachedU3Ek__BackingField_32)); }
	inline bool get_U3CisWaitTargetReachedU3Ek__BackingField_32() const { return ___U3CisWaitTargetReachedU3Ek__BackingField_32; }
	inline bool* get_address_of_U3CisWaitTargetReachedU3Ek__BackingField_32() { return &___U3CisWaitTargetReachedU3Ek__BackingField_32; }
	inline void set_U3CisWaitTargetReachedU3Ek__BackingField_32(bool value)
	{
		___U3CisWaitTargetReachedU3Ek__BackingField_32 = value;
	}

	inline static int32_t get_offset_of_U3CcaptainU3Ek__BackingField_33() { return static_cast<int32_t>(offsetof(HeroMgr_t2475965342, ___U3CcaptainU3Ek__BackingField_33)); }
	inline Hero_t2245658 * get_U3CcaptainU3Ek__BackingField_33() const { return ___U3CcaptainU3Ek__BackingField_33; }
	inline Hero_t2245658 ** get_address_of_U3CcaptainU3Ek__BackingField_33() { return &___U3CcaptainU3Ek__BackingField_33; }
	inline void set_U3CcaptainU3Ek__BackingField_33(Hero_t2245658 * value)
	{
		___U3CcaptainU3Ek__BackingField_33 = value;
		Il2CppCodeGenWriteBarrier(&___U3CcaptainU3Ek__BackingField_33, value);
	}
};

struct HeroMgr_t2475965342_StaticFields
{
public:
	// HeroMgr HeroMgr::_inst
	HeroMgr_t2475965342 * ____inst_6;
	// System.Comparison`1<Hero> HeroMgr::<>f__am$cache1C
	Comparison_1_t3013574141 * ___U3CU3Ef__amU24cache1C_34;
	// System.Comparison`1<UnityEngine.GameObject> HeroMgr::<>f__am$cache1D
	Comparison_1_t2391043192 * ___U3CU3Ef__amU24cache1D_35;

public:
	inline static int32_t get_offset_of__inst_6() { return static_cast<int32_t>(offsetof(HeroMgr_t2475965342_StaticFields, ____inst_6)); }
	inline HeroMgr_t2475965342 * get__inst_6() const { return ____inst_6; }
	inline HeroMgr_t2475965342 ** get_address_of__inst_6() { return &____inst_6; }
	inline void set__inst_6(HeroMgr_t2475965342 * value)
	{
		____inst_6 = value;
		Il2CppCodeGenWriteBarrier(&____inst_6, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1C_34() { return static_cast<int32_t>(offsetof(HeroMgr_t2475965342_StaticFields, ___U3CU3Ef__amU24cache1C_34)); }
	inline Comparison_1_t3013574141 * get_U3CU3Ef__amU24cache1C_34() const { return ___U3CU3Ef__amU24cache1C_34; }
	inline Comparison_1_t3013574141 ** get_address_of_U3CU3Ef__amU24cache1C_34() { return &___U3CU3Ef__amU24cache1C_34; }
	inline void set_U3CU3Ef__amU24cache1C_34(Comparison_1_t3013574141 * value)
	{
		___U3CU3Ef__amU24cache1C_34 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1C_34, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1D_35() { return static_cast<int32_t>(offsetof(HeroMgr_t2475965342_StaticFields, ___U3CU3Ef__amU24cache1D_35)); }
	inline Comparison_1_t2391043192 * get_U3CU3Ef__amU24cache1D_35() const { return ___U3CU3Ef__amU24cache1D_35; }
	inline Comparison_1_t2391043192 ** get_address_of_U3CU3Ef__amU24cache1D_35() { return &___U3CU3Ef__amU24cache1D_35; }
	inline void set_U3CU3Ef__amU24cache1D_35(Comparison_1_t2391043192 * value)
	{
		___U3CU3Ef__amU24cache1D_35 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1D_35, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
