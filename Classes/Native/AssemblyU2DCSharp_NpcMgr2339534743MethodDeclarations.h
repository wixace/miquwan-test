﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// NpcMgr
struct NpcMgr_t2339534743;
// Boss
struct Boss_t2076557;
// System.Collections.Generic.List`1<CSPVPRobotNPC>
struct List_1_t225956257;
// CSPVPRobotNPC[]
struct CSPVPRobotNPCU5BU5D_t1447782092;
// System.Collections.Generic.List`1<CSHeroUnit>
struct List_1_t837576702;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// Monster
struct Monster_t2901270458;
// CSPVPRobotNPC
struct CSPVPRobotNPC_t3152738001;
// JSCLevelMonsterConfig
struct JSCLevelMonsterConfig_t1924079698;
// HeroNpc
struct HeroNpc_t2475966567;
// CSHeroUnit
struct CSHeroUnit_t3764358446;
// CombatEntity
struct CombatEntity_t684137495;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// JSCWaveNpcConfig
struct JSCWaveNpcConfig_t4221504656;
// System.Collections.Generic.List`1<CombatEntity>
struct List_1_t2052323047;
// Biaoche
struct Biaoche_t1544748459;
// System.Collections.Generic.List`1<JSCLevelMonsterConfig>
struct List_1_t3292265250;
// System.String
struct String_t;
// CEvent.EventFunc`1<CEvent.ZEvent>
struct EventFunc_1_t1114914310;
// CSPlayerData
struct CSPlayerData_t1029357243;
// CSGameDataMgr
struct CSGameDataMgr_t2623305516;
// CSCheckPointUnit
struct CSCheckPointUnit_t3129216924;
// GameMgr
struct GameMgr_t1469029542;
// JSCLevelConfig
struct JSCLevelConfig_t1411099500;
// LevelConfigManager
struct LevelConfigManager_t657947911;
// System.Collections.Generic.List`1<JSCWaveNpcConfig>
struct List_1_t1294722912;
// CSDatacfgManager
struct CSDatacfgManager_t1565254243;
// monstersCfg
struct monstersCfg_t1542396363;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// EffectMgr
struct EffectMgr_t535289511;
// HeroMgr
struct HeroMgr_t2475965342;
// Entity.Behavior.IBehaviorCtrl
struct IBehaviorCtrl_t4225040900;
// taixuTipCfg
struct taixuTipCfg_t1268722370;
// Hero
struct Hero_t2245658;
// BuffCtrl
struct BuffCtrl_t2836564350;
// CameraShotMgr
struct CameraShotMgr_t1580128697;
// EnemyDropMgr
struct EnemyDropMgr_t2125592609;
// System.Action
struct Action_t3771233898;
// FightCtrl
struct FightCtrl_t648967803;
// System.Object[]
struct ObjectU5BU5D_t1108656482;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Boss2076557.h"
#include "AssemblyU2DCSharp_CSPVPRobotNPC3152738001.h"
#include "AssemblyU2DCSharp_JSCLevelMonsterConfig1924079698.h"
#include "AssemblyU2DCSharp_CSHeroUnit3764358446.h"
#include "AssemblyU2DCSharp_CombatEntity684137495.h"
#include "AssemblyU2DCSharp_NpcMgr_EditorNpcType2400575222.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "AssemblyU2DCSharp_HERO_ELEMENT2692579223.h"
#include "AssemblyU2DCSharp_HERO_COUNTRY1018279985.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_NpcMgr2339534743.h"
#include "AssemblyU2DCSharp_CSPlayerData1029357243.h"
#include "AssemblyU2DCSharp_GameMgr1469029542.h"
#include "AssemblyU2DCSharp_LevelConfigManager657947911.h"
#include "AssemblyU2DCSharp_JSCWaveNpcConfig4221504656.h"
#include "AssemblyU2DCSharp_JSCLevelConfig1411099500.h"
#include "AssemblyU2DCSharp_EntityEnum_UnitType643359572.h"
#include "AssemblyU2DCSharp_Monster2901270458.h"
#include "AssemblyU2DCSharp_monstersCfg1542396363.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "AssemblyU2DCSharp_EffectMgr535289511.h"
#include "AssemblyU2DCSharp_HeroMgr2475965342.h"
#include "AssemblyU2DCSharp_EntityEnum_UnitCamp642829979.h"
#include "AssemblyU2DCSharp_BuffCtrl2836564350.h"
#include "AssemblyU2DCSharp_EnemyDropMgr2125592609.h"
#include "System_Core_System_Action3771233898.h"
#include "AssemblyU2DCSharp_FightCtrl648967803.h"
#include "AssemblyU2DCSharp_Entity_Behavior_IBehaviorCtrl4225040900.h"
#include "AssemblyU2DCSharp_Entity_Behavior_EBehaviorID1238533136.h"

// System.Void NpcMgr::.ctor()
extern "C"  void NpcMgr__ctor_m3153888996 (NpcMgr_t2339534743 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// NpcMgr NpcMgr::get_instance()
extern "C"  NpcMgr_t2339534743 * NpcMgr_get_instance_m1039123300 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NpcMgr::set_boss(Boss)
extern "C"  void NpcMgr_set_boss_m3473822333 (NpcMgr_t2339534743 * __this, Boss_t2076557 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Boss NpcMgr::get_boss()
extern "C"  Boss_t2076557 * NpcMgr_get_boss_m1816973510 (NpcMgr_t2339534743 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NpcMgr::set_curRound(System.Int32)
extern "C"  void NpcMgr_set_curRound_m3675522876 (NpcMgr_t2339534743 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 NpcMgr::get_curRound()
extern "C"  int32_t NpcMgr_get_curRound_m3116903045 (NpcMgr_t2339534743 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NpcMgr::set_curOtherRound(System.Int32)
extern "C"  void NpcMgr_set_curOtherRound_m3262612692 (NpcMgr_t2339534743 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 NpcMgr::get_curOtherRound()
extern "C"  int32_t NpcMgr_get_curOtherRound_m3206818857 (NpcMgr_t2339534743 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NpcMgr::set_isKillBoss(System.Boolean)
extern "C"  void NpcMgr_set_isKillBoss_m1804670889 (NpcMgr_t2339534743 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NpcMgr::get_isKillBoss()
extern "C"  bool NpcMgr_get_isKillBoss_m3758616050 (NpcMgr_t2339534743 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NpcMgr::Init()
extern "C"  void NpcMgr_Init_m468916976 (NpcMgr_t2339534743 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NpcMgr::AddGlobalBuff(System.Int32)
extern "C"  void NpcMgr_AddGlobalBuff_m2205984554 (NpcMgr_t2339534743 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CSPVPRobotNPC> NpcMgr::GetRobotMonsters(CSPVPRobotNPC[])
extern "C"  List_1_t225956257 * NpcMgr_GetRobotMonsters_m1341333472 (NpcMgr_t2339534743 * __this, CSPVPRobotNPCU5BU5D_t1447782092* ___robotNpcs0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NpcMgr::SetJSCWaveNpcConfigPvp()
extern "C"  void NpcMgr_SetJSCWaveNpcConfigPvp_m1414202104 (NpcMgr_t2339534743 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NpcMgr::ClearMonsterUnits()
extern "C"  void NpcMgr_ClearMonsterUnits_m786909348 (NpcMgr_t2339534743 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NpcMgr::CreateHeroNPCList(System.Collections.Generic.List`1<CSHeroUnit>,System.Int32)
extern "C"  void NpcMgr_CreateHeroNPCList_m640745570 (NpcMgr_t2339534743 * __this, List_1_t837576702 * ___units0, int32_t ___count1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NpcMgr::CreateRobotNPCList(System.Collections.Generic.List`1<CSPVPRobotNPC>,System.Int32)
extern "C"  void NpcMgr_CreateRobotNPCList_m1724109489 (NpcMgr_t2339534743 * __this, List_1_t225956257 * ___robots0, int32_t ___count1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NpcMgr::CreatMonsterRobotList(System.Int32[])
extern "C"  void NpcMgr_CreatMonsterRobotList_m1036605128 (NpcMgr_t2339534743 * __this, Int32U5BU5D_t3230847821* ___monsterIds0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Monster NpcMgr::CreateRobotNPC(CSPVPRobotNPC,JSCLevelMonsterConfig)
extern "C"  Monster_t2901270458 * NpcMgr_CreateRobotNPC_m708562799 (NpcMgr_t2339534743 * __this, CSPVPRobotNPC_t3152738001 * ___robNpc0, JSCLevelMonsterConfig_t1924079698 * ___jscConfig1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HeroNpc NpcMgr::CreateHeroNPC(CSHeroUnit,JSCLevelMonsterConfig)
extern "C"  HeroNpc_t2475966567 * NpcMgr_CreateHeroNPC_m2561476941 (NpcMgr_t2339534743 * __this, CSHeroUnit_t3764358446 * ___unit0, JSCLevelMonsterConfig_t1924079698 * ___jscConfig1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NpcMgr::ChangeHeroToDead(CombatEntity)
extern "C"  void NpcMgr_ChangeHeroToDead_m635427282 (NpcMgr_t2339534743 * __this, CombatEntity_t684137495 * ___entity0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NpcMgr::ChangeHeroToDead(System.Int32)
extern "C"  void NpcMgr_ChangeHeroToDead_m2135765722 (NpcMgr_t2339534743 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NpcMgr::ClearFriendNpcEff()
extern "C"  void NpcMgr_ClearFriendNpcEff_m406816561 (NpcMgr_t2339534743 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CSHeroUnit NpcMgr::GetHeroUnitById(System.Collections.Generic.List`1<CSHeroUnit>,System.Int32)
extern "C"  CSHeroUnit_t3764358446 * NpcMgr_GetHeroUnitById_m2517905296 (NpcMgr_t2339534743 * __this, List_1_t837576702 * ___units0, int32_t ___id1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single NpcMgr::GetTriggerDistanceByRound(NpcMgr/EditorNpcType)
extern "C"  float NpcMgr_GetTriggerDistanceByRound_m3381634144 (NpcMgr_t2339534743 * __this, int32_t ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NpcMgr::LoadNpcList()
extern "C"  void NpcMgr_LoadNpcList_m338071643 (NpcMgr_t2339534743 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Monster NpcMgr::InitNpcEntity(JSCLevelMonsterConfig,NpcMgr/EditorNpcType,CombatEntity,System.Boolean,System.Boolean)
extern "C"  Monster_t2901270458 * NpcMgr_InitNpcEntity_m3971614370 (NpcMgr_t2339534743 * __this, JSCLevelMonsterConfig_t1924079698 * ___lconfig0, int32_t ___type1, CombatEntity_t684137495 * ___SummonEntity2, bool ____isSummon3, bool ___isFindPos4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Monster NpcMgr::CreatNpc(JSCLevelMonsterConfig,NpcMgr/EditorNpcType,System.Int32,CombatEntity,System.Boolean,System.Boolean)
extern "C"  Monster_t2901270458 * NpcMgr_CreatNpc_m3758277625 (NpcMgr_t2339534743 * __this, JSCLevelMonsterConfig_t1924079698 * ___lconfig0, int32_t ___type1, int32_t ___index2, CombatEntity_t684137495 * ___SummonEntity3, bool ____isSummon4, bool ___isFindPos5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CombatEntity NpcMgr::CreatGuideEnemyNpc(JSCLevelMonsterConfig,System.Boolean)
extern "C"  CombatEntity_t684137495 * NpcMgr_CreatGuideEnemyNpc_m1275482063 (NpcMgr_t2339534743 * __this, JSCLevelMonsterConfig_t1924079698 * ___lconfig0, bool ___isFight1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NpcMgr::CreatNextNpcList(CEvent.ZEvent)
extern "C"  void NpcMgr_CreatNextNpcList_m4262225496 (NpcMgr_t2339534743 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NpcMgr::CreatNextOtherList()
extern "C"  void NpcMgr_CreatNextOtherList_m1129186098 (NpcMgr_t2339534743 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NpcMgr::CreateAniShowNpcList(System.Int32,System.Int32,System.Int32)
extern "C"  void NpcMgr_CreateAniShowNpcList_m3252625235 (NpcMgr_t2339534743 * __this, int32_t ___type0, int32_t ___roundIndex1, int32_t ___npcId2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NpcMgr::SetNpcModelView(System.Boolean,System.Int32)
extern "C"  void NpcMgr_SetNpcModelView_m3146697231 (NpcMgr_t2339534743 * __this, bool ___isShow0, int32_t ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CombatEntity NpcMgr::GetAniShowNpc(System.Int32)
extern "C"  CombatEntity_t684137495 * NpcMgr_GetAniShowNpc_m929733419 (NpcMgr_t2339534743 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NpcMgr::CreatGuideNpcs(System.Int32,System.Boolean)
extern "C"  void NpcMgr_CreatGuideNpcs_m1843680455 (NpcMgr_t2339534743 * __this, int32_t ___round0, bool ___canFight1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NpcMgr::OnTriggerNpcs(CombatEntity)
extern "C"  void NpcMgr_OnTriggerNpcs_m2616034614 (NpcMgr_t2339534743 * __this, CombatEntity_t684137495 * ___main0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NpcMgr::OnTriggerOthers(CombatEntity)
extern "C"  void NpcMgr_OnTriggerOthers_m1415317159 (NpcMgr_t2339534743 * __this, CombatEntity_t684137495 * ___main0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NpcMgr::OnTriggerUnfinish(CombatEntity)
extern "C"  void NpcMgr_OnTriggerUnfinish_m780262768 (NpcMgr_t2339534743 * __this, CombatEntity_t684137495 * ___main0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NpcMgr::AddNpc(CombatEntity)
extern "C"  void NpcMgr_AddNpc_m787133673 (NpcMgr_t2339534743 * __this, CombatEntity_t684137495 * ___com0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// JSCWaveNpcConfig NpcMgr::GetWaveNpcCfg(System.Int32)
extern "C"  JSCWaveNpcConfig_t4221504656 * NpcMgr_GetWaveNpcCfg_m2491957144 (NpcMgr_t2339534743 * __this, int32_t ___round0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CombatEntity> NpcMgr::GetCurRoundMonster()
extern "C"  List_1_t2052323047 * NpcMgr_GetCurRoundMonster_m82565984 (NpcMgr_t2339534743 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CombatEntity> NpcMgr::GetCurRoundOther()
extern "C"  List_1_t2052323047 * NpcMgr_GetCurRoundOther_m508926550 (NpcMgr_t2339534743 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CombatEntity> NpcMgr::GetCurRoundGuide()
extern "C"  List_1_t2052323047 * NpcMgr_GetCurRoundGuide_m2028342082 (NpcMgr_t2339534743 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CombatEntity> NpcMgr::GetCurUnFinishNpc()
extern "C"  List_1_t2052323047 * NpcMgr_GetCurUnFinishNpc_m1982437667 (NpcMgr_t2339534743 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 NpcMgr::GetCurHostilNpcCount()
extern "C"  int32_t NpcMgr_GetCurHostilNpcCount_m3024752369 (NpcMgr_t2339534743 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CombatEntity> NpcMgr::GetAllMonsters()
extern "C"  List_1_t2052323047 * NpcMgr_GetAllMonsters_m817781038 (NpcMgr_t2339534743 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CombatEntity> NpcMgr::GetMonsters()
extern "C"  List_1_t2052323047 * NpcMgr_GetMonsters_m1317987687 (NpcMgr_t2339534743 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CombatEntity> NpcMgr::GetAllAniList(CombatEntity)
extern "C"  List_1_t2052323047 * NpcMgr_GetAllAniList_m304827344 (NpcMgr_t2339534743 * __this, CombatEntity_t684137495 * ___entity0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CombatEntity> NpcMgr::GetCameraShotFriends()
extern "C"  List_1_t2052323047 * NpcMgr_GetCameraShotFriends_m1525564458 (NpcMgr_t2339534743 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CombatEntity> NpcMgr::GetCameraShotMonsters()
extern "C"  List_1_t2052323047 * NpcMgr_GetCameraShotMonsters_m3931634246 (NpcMgr_t2339534743 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CombatEntity> NpcMgr::GetAllFriendNpc()
extern "C"  List_1_t2052323047 * NpcMgr_GetAllFriendNpc_m3706426256 (NpcMgr_t2339534743 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CombatEntity> NpcMgr::GetFriendsList()
extern "C"  List_1_t2052323047 * NpcMgr_GetFriendsList_m1217571367 (NpcMgr_t2339534743 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CombatEntity> NpcMgr::GetAllNeutral()
extern "C"  List_1_t2052323047 * NpcMgr_GetAllNeutral_m2962946036 (NpcMgr_t2339534743 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CombatEntity> NpcMgr::GetPathNpc()
extern "C"  List_1_t2052323047 * NpcMgr_GetPathNpc_m411740688 (NpcMgr_t2339534743 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CombatEntity NpcMgr::GetNpc(System.Int32)
extern "C"  CombatEntity_t684137495 * NpcMgr_GetNpc_m3293133794 (NpcMgr_t2339534743 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Biaoche NpcMgr::GetBiaoche()
extern "C"  Biaoche_t1544748459 * NpcMgr_GetBiaoche_m1217055677 (NpcMgr_t2339534743 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CombatEntity NpcMgr::GetHurtMaxNpc()
extern "C"  CombatEntity_t684137495 * NpcMgr_GetHurtMaxNpc_m2807361150 (NpcMgr_t2339534743 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CombatEntity NpcMgr::GetCureNpc()
extern "C"  CombatEntity_t684137495 * NpcMgr_GetCureNpc_m970432876 (NpcMgr_t2339534743 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CombatEntity NpcMgr::GetHostilNpcById(System.Int32)
extern "C"  CombatEntity_t684137495 * NpcMgr_GetHostilNpcById_m2522052873 (NpcMgr_t2339534743 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CombatEntity NpcMgr::GetNeutralNpcById(System.Int32)
extern "C"  CombatEntity_t684137495 * NpcMgr_GetNeutralNpcById_m2131726607 (NpcMgr_t2339534743 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CombatEntity NpcMgr::GetRemoteNpc()
extern "C"  CombatEntity_t684137495 * NpcMgr_GetRemoteNpc_m2266700843 (NpcMgr_t2339534743 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CombatEntity NpcMgr::GetHPMinNpc()
extern "C"  CombatEntity_t684137495 * NpcMgr_GetHPMinNpc_m1568137833 (NpcMgr_t2339534743 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CombatEntity NpcMgr::GetNearestNpc(CombatEntity)
extern "C"  CombatEntity_t684137495 * NpcMgr_GetNearestNpc_m2461623134 (NpcMgr_t2339534743 * __this, CombatEntity_t684137495 * ___entity0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CombatEntity NpcMgr::GetSexNpc(System.Int32)
extern "C"  CombatEntity_t684137495 * NpcMgr_GetSexNpc_m504758750 (NpcMgr_t2339534743 * __this, int32_t ___sexType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NpcMgr::LeaveBattle()
extern "C"  void NpcMgr_LeaveBattle_m1998386641 (NpcMgr_t2339534743 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NpcMgr::KillTaixuAllEnemy(CombatEntity)
extern "C"  void NpcMgr_KillTaixuAllEnemy_m1075542701 (NpcMgr_t2339534743 * __this, CombatEntity_t684137495 * ___cbe0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NpcMgr::RandomNpcPosition(System.Collections.Generic.List`1<JSCLevelMonsterConfig>)
extern "C"  void NpcMgr_RandomNpcPosition_m90903167 (NpcMgr_t2339534743 * __this, List_1_t3292265250 * ___configs0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NpcMgr::IsCurElementNpc(HERO_ELEMENT)
extern "C"  bool NpcMgr_IsCurElementNpc_m2945385138 (NpcMgr_t2339534743 * __this, int32_t ___element0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HERO_ELEMENT NpcMgr::GetElement(System.Int32)
extern "C"  int32_t NpcMgr_GetElement_m388063677 (NpcMgr_t2339534743 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 NpcMgr::get_TotalRound()
extern "C"  int32_t NpcMgr_get_TotalRound_m2682231713 (NpcMgr_t2339534743 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single NpcMgr::get_BossConsumeHpPer()
extern "C"  float NpcMgr_get_BossConsumeHpPer_m3763038747 (NpcMgr_t2339534743 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 NpcMgr::get_BossConsumeHp()
extern "C"  int32_t NpcMgr_get_BossConsumeHp_m3855209538 (NpcMgr_t2339534743 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NpcMgr::bossIsKilled()
extern "C"  bool NpcMgr_bossIsKilled_m3807815240 (NpcMgr_t2339534743 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NpcMgr::shiliBossIsKilled()
extern "C"  bool NpcMgr_shiliBossIsKilled_m2651509459 (NpcMgr_t2339534743 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 NpcMgr::get_ShiliBossConsumeHp()
extern "C"  int32_t NpcMgr_get_ShiliBossConsumeHp_m2689142141 (NpcMgr_t2339534743 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NpcMgr::IsKillAllMonster()
extern "C"  bool NpcMgr_IsKillAllMonster_m1803382165 (NpcMgr_t2339534743 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NpcMgr::Dead(CombatEntity)
extern "C"  void NpcMgr_Dead_m385532813 (NpcMgr_t2339534743 * __this, CombatEntity_t684137495 * ___unit0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 NpcMgr::GetHeroIntelligenceNum(System.Int32)
extern "C"  int32_t NpcMgr_GetHeroIntelligenceNum_m2213722102 (NpcMgr_t2339534743 * __this, int32_t ___intelligence0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CombatEntity> NpcMgr::GetHeroNpcList()
extern "C"  List_1_t2052323047 * NpcMgr_GetHeroNpcList_m2417527705 (NpcMgr_t2339534743 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NpcMgr::IsThreeElement(HERO_ELEMENT)
extern "C"  bool NpcMgr_IsThreeElement_m1152984165 (NpcMgr_t2339534743 * __this, int32_t ___element0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NpcMgr::IsThreeCountry(HERO_COUNTRY)
extern "C"  bool NpcMgr_IsThreeCountry_m1836851749 (NpcMgr_t2339534743 * __this, int32_t ___contyr0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NpcMgr::IsAllElement()
extern "C"  bool NpcMgr_IsAllElement_m1323943993 (NpcMgr_t2339534743 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NpcMgr::Clear()
extern "C"  void NpcMgr_Clear_m560022287 (NpcMgr_t2339534743 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NpcMgr::ClearAniShowList(System.Int32[])
extern "C"  void NpcMgr_ClearAniShowList_m412827193 (NpcMgr_t2339534743 * __this, Int32U5BU5D_t3230847821* ___special0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NpcMgr::CameraShotBattle(System.Int32,System.Int32[])
extern "C"  void NpcMgr_CameraShotBattle_m841364333 (NpcMgr_t2339534743 * __this, int32_t ___isTruth0, Int32U5BU5D_t3230847821* ___specialInTruth1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CombatEntity> NpcMgr::GetAllAniList()
extern "C"  List_1_t2052323047 * NpcMgr_GetAllAniList_m277398023 (NpcMgr_t2339534743 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NpcMgr::SetMonsterNotSelected(System.Boolean)
extern "C"  void NpcMgr_SetMonsterNotSelected_m2988744271 (NpcMgr_t2339534743 * __this, bool ___isShow0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NpcMgr::ilo_AddEventListener1(System.String,CEvent.EventFunc`1<CEvent.ZEvent>)
extern "C"  void NpcMgr_ilo_AddEventListener1_m3326483856 (Il2CppObject * __this /* static, unused */, String_t* ___eventType0, EventFunc_1_t1114914310 * ___fun1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NpcMgr::ilo_set_curOtherRound2(NpcMgr,System.Int32)
extern "C"  void NpcMgr_ilo_set_curOtherRound2_m1785045450 (Il2CppObject * __this /* static, unused */, NpcMgr_t2339534743 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NpcMgr::ilo_IsPVPRobat3(CSPlayerData)
extern "C"  bool NpcMgr_ilo_IsPVPRobat3_m402271707 (Il2CppObject * __this /* static, unused */, CSPlayerData_t1029357243 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CSGameDataMgr NpcMgr::ilo_get_CSGameDataMgr4()
extern "C"  CSGameDataMgr_t2623305516 * NpcMgr_ilo_get_CSGameDataMgr4_m213613611 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NpcMgr::ilo_CreateHeroNPCList5(NpcMgr,System.Collections.Generic.List`1<CSHeroUnit>,System.Int32)
extern "C"  void NpcMgr_ilo_CreateHeroNPCList5_m3103321077 (Il2CppObject * __this /* static, unused */, NpcMgr_t2339534743 * ____this0, List_1_t837576702 * ___units1, int32_t ___count2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CSPVPRobotNPC> NpcMgr::ilo_GetRobotMonsters6(NpcMgr,CSPVPRobotNPC[])
extern "C"  List_1_t225956257 * NpcMgr_ilo_GetRobotMonsters6_m2369415144 (Il2CppObject * __this /* static, unused */, NpcMgr_t2339534743 * ____this0, CSPVPRobotNPCU5BU5D_t1447782092* ___robotNpcs1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NpcMgr::ilo_CreatMonsterRobotList7(NpcMgr,System.Int32[])
extern "C"  void NpcMgr_ilo_CreatMonsterRobotList7_m3305669657 (Il2CppObject * __this /* static, unused */, NpcMgr_t2339534743 * ____this0, Int32U5BU5D_t3230847821* ___monsterIds1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CSHeroUnit> NpcMgr::ilo_GetMonsterUnits8(CSPlayerData)
extern "C"  List_1_t837576702 * NpcMgr_ilo_GetMonsterUnits8_m3477276582 (Il2CppObject * __this /* static, unused */, CSPlayerData_t1029357243 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NpcMgr::ilo_DispatchEvent9(CEvent.ZEvent)
extern "C"  void NpcMgr_ilo_DispatchEvent9_m790303855 (Il2CppObject * __this /* static, unused */, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CSCheckPointUnit NpcMgr::ilo_get_checkPoint10(GameMgr)
extern "C"  CSCheckPointUnit_t3129216924 * NpcMgr_ilo_get_checkPoint10_m320231720 (Il2CppObject * __this /* static, unused */, GameMgr_t1469029542 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// JSCLevelConfig NpcMgr::ilo_GetLevelConfigByID11(LevelConfigManager,System.Int32)
extern "C"  JSCLevelConfig_t1411099500 * NpcMgr_ilo_GetLevelConfigByID11_m1674229720 (Il2CppObject * __this /* static, unused */, LevelConfigManager_t657947911 * ____this0, int32_t ___level1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NpcMgr::ilo_set_delay12(JSCWaveNpcConfig,System.Single)
extern "C"  void NpcMgr_ilo_set_delay12_m2977822123 (Il2CppObject * __this /* static, unused */, JSCWaveNpcConfig_t4221504656 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<JSCLevelMonsterConfig> NpcMgr::ilo_get_wave13(JSCWaveNpcConfig)
extern "C"  List_1_t3292265250 * NpcMgr_ilo_get_wave13_m1931264026 (Il2CppObject * __this /* static, unused */, JSCWaveNpcConfig_t4221504656 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 NpcMgr::ilo_get_id14(JSCLevelMonsterConfig)
extern "C"  int32_t NpcMgr_ilo_get_id14_m2187773144 (Il2CppObject * __this /* static, unused */, JSCLevelMonsterConfig_t1924079698 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<JSCWaveNpcConfig> NpcMgr::ilo_get_monsters15(JSCLevelConfig)
extern "C"  List_1_t1294722912 * NpcMgr_ilo_get_monsters15_m397902896 (Il2CppObject * __this /* static, unused */, JSCLevelConfig_t1411099500 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NpcMgr::ilo_set_x16(JSCLevelMonsterConfig,System.Single)
extern "C"  void NpcMgr_ilo_set_x16_m655321430 (Il2CppObject * __this /* static, unused */, JSCLevelMonsterConfig_t1924079698 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single NpcMgr::ilo_get_y17(JSCLevelMonsterConfig)
extern "C"  float NpcMgr_ilo_get_y17_m4286495879 (Il2CppObject * __this /* static, unused */, JSCLevelMonsterConfig_t1924079698 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single NpcMgr::ilo_get_rot18(JSCLevelMonsterConfig)
extern "C"  float NpcMgr_ilo_get_rot18_m785478152 (Il2CppObject * __this /* static, unused */, JSCLevelMonsterConfig_t1924079698 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NpcMgr::ilo_set_rot19(JSCLevelMonsterConfig,System.Single)
extern "C"  void NpcMgr_ilo_set_rot19_m977914164 (Il2CppObject * __this /* static, unused */, JSCLevelMonsterConfig_t1924079698 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HeroNpc NpcMgr::ilo_CreateHeroNPC20(NpcMgr,CSHeroUnit,JSCLevelMonsterConfig)
extern "C"  HeroNpc_t2475966567 * NpcMgr_ilo_CreateHeroNPC20_m2879750845 (Il2CppObject * __this /* static, unused */, NpcMgr_t2339534743 * ____this0, CSHeroUnit_t3764358446 * ___unit1, JSCLevelMonsterConfig_t1924079698 * ___jscConfig2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CSDatacfgManager NpcMgr::ilo_get_CfgDataMgr21()
extern "C"  CSDatacfgManager_t1565254243 * NpcMgr_ilo_get_CfgDataMgr21_m2718514719 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NpcMgr::ilo_set_unitType22(CombatEntity,EntityEnum.UnitType)
extern "C"  void NpcMgr_ilo_set_unitType22_m308615163 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Monster NpcMgr::ilo_CreateRobotNPC23(NpcMgr,CSPVPRobotNPC,JSCLevelMonsterConfig)
extern "C"  Monster_t2901270458 * NpcMgr_ilo_CreateRobotNPC23_m228822846 (Il2CppObject * __this /* static, unused */, NpcMgr_t2339534743 * ____this0, CSPVPRobotNPC_t3152738001 * ___robNpc1, JSCLevelMonsterConfig_t1924079698 * ___jscConfig2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single NpcMgr::ilo_get_x24(JSCLevelMonsterConfig)
extern "C"  float NpcMgr_ilo_get_x24_m2379286988 (Il2CppObject * __this /* static, unused */, JSCLevelMonsterConfig_t1924079698 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Monster NpcMgr::ilo_CreatNpc25(NpcMgr,JSCLevelMonsterConfig,NpcMgr/EditorNpcType,System.Int32,CombatEntity,System.Boolean,System.Boolean)
extern "C"  Monster_t2901270458 * NpcMgr_ilo_CreatNpc25_m1055682718 (Il2CppObject * __this /* static, unused */, NpcMgr_t2339534743 * ____this0, JSCLevelMonsterConfig_t1924079698 * ___lconfig1, int32_t ___type2, int32_t ___index3, CombatEntity_t684137495 * ___SummonEntity4, bool ____isSummon5, bool ___isFindPos6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NpcMgr::ilo_OnTriggerFun26(Monster,System.Int32)
extern "C"  void NpcMgr_ilo_OnTriggerFun26_m730905948 (Il2CppObject * __this /* static, unused */, Monster_t2901270458 * ____this0, int32_t ___effectId1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NpcMgr::ilo_Create27(Monster,monstersCfg,JSCLevelMonsterConfig)
extern "C"  void NpcMgr_ilo_Create27_m3484207487 (Il2CppObject * __this /* static, unused */, Monster_t2901270458 * ____this0, monstersCfg_t1542396363 * ___mcfg1, JSCLevelMonsterConfig_t1924079698 * ___lmcfg2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single NpcMgr::ilo_get_maxHp28(CombatEntity)
extern "C"  float NpcMgr_ilo_get_maxHp28_m4174760245 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single NpcMgr::ilo_get_maxRage29(CombatEntity)
extern "C"  float NpcMgr_ilo_get_maxRage29_m2879493243 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NpcMgr::ilo_ResidualMp30(Monster,System.Single)
extern "C"  void NpcMgr_ilo_ResidualMp30_m1540224037 (Il2CppObject * __this /* static, unused */, Monster_t2901270458 * ____this0, float ___r_mp1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NpcMgr::ilo_set_viewLevel31(CombatEntity,System.Int32)
extern "C"  void NpcMgr_ilo_set_viewLevel31_m3718558559 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NpcMgr::ilo_AddNpc32(NpcMgr,CombatEntity)
extern "C"  void NpcMgr_ilo_AddNpc32_m1178618036 (Il2CppObject * __this /* static, unused */, NpcMgr_t2339534743 * ____this0, CombatEntity_t684137495 * ___com1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// EntityEnum.UnitType NpcMgr::ilo_get_unitType33(CombatEntity)
extern "C"  int32_t NpcMgr_ilo_get_unitType33_m3479126542 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject NpcMgr::ilo_Instantiate34(UnityEngine.GameObject)
extern "C"  GameObject_t3674682005 * NpcMgr_ilo_Instantiate34_m55073245 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NpcMgr::ilo_ChangeParent35(UnityEngine.GameObject,UnityEngine.GameObject,System.Boolean)
extern "C"  void NpcMgr_ilo_ChangeParent35_m4197296742 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ____this0, GameObject_t3674682005 * ___parent1, bool ___resetMat2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 NpcMgr::ilo_get_id36(CombatEntity)
extern "C"  int32_t NpcMgr_ilo_get_id36_m2713505617 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NpcMgr::ilo_ChangeHeroToDead37(NpcMgr,System.Int32)
extern "C"  void NpcMgr_ilo_ChangeHeroToDead37_m1729659854 (Il2CppObject * __this /* static, unused */, NpcMgr_t2339534743 * ____this0, int32_t ___id1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NpcMgr::ilo_set_y38(JSCLevelMonsterConfig,System.Single)
extern "C"  void NpcMgr_ilo_set_y38_m3537142261 (Il2CppObject * __this /* static, unused */, JSCLevelMonsterConfig_t1924079698 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 NpcMgr::ilo_get_id39(CSHeroUnit)
extern "C"  uint32_t NpcMgr_ilo_get_id39_m3120502378 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NpcMgr::ilo_SetViewEffect40(EffectMgr,System.Int32,System.Boolean)
extern "C"  void NpcMgr_ilo_SetViewEffect40_m1866462990 (Il2CppObject * __this /* static, unused */, EffectMgr_t535289511 * ____this0, int32_t ___srcID1, bool ___isShow2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 NpcMgr::ilo_get_curRound41(NpcMgr)
extern "C"  int32_t NpcMgr_ilo_get_curRound41_m75600062 (Il2CppObject * __this /* static, unused */, NpcMgr_t2339534743 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single NpcMgr::ilo_get_distance42(JSCWaveNpcConfig)
extern "C"  float NpcMgr_ilo_get_distance42_m1780932267 (Il2CppObject * __this /* static, unused */, JSCWaveNpcConfig_t4221504656 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<JSCWaveNpcConfig> NpcMgr::ilo_get_others43(JSCLevelConfig)
extern "C"  List_1_t1294722912 * NpcMgr_ilo_get_others43_m3265056533 (Il2CppObject * __this /* static, unused */, JSCLevelConfig_t1411099500 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 NpcMgr::ilo_get_curOtherRound44(NpcMgr)
extern "C"  int32_t NpcMgr_ilo_get_curOtherRound44_m2455092319 (Il2CppObject * __this /* static, unused */, NpcMgr_t2339534743 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 NpcMgr::ilo_get_level45(GameMgr)
extern "C"  int32_t NpcMgr_ilo_get_level45_m1218832015 (Il2CppObject * __this /* static, unused */, GameMgr_t1469029542 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NpcMgr::ilo_set_mapEditorType46(Monster,System.Int32)
extern "C"  void NpcMgr_ilo_set_mapEditorType46_m3507023510 (Il2CppObject * __this /* static, unused */, Monster_t2901270458 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NpcMgr::ilo_set_boss47(NpcMgr,Boss)
extern "C"  void NpcMgr_ilo_set_boss47_m3877929232 (Il2CppObject * __this /* static, unused */, NpcMgr_t2339534743 * ____this0, Boss_t2076557 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Boss NpcMgr::ilo_get_boss48(NpcMgr)
extern "C"  Boss_t2076557 * NpcMgr_ilo_get_boss48_m2620190598 (Il2CppObject * __this /* static, unused */, NpcMgr_t2339534743 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HeroMgr NpcMgr::ilo_get_instance49()
extern "C"  HeroMgr_t2475965342 * NpcMgr_ilo_get_instance49_m1413661963 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Entity.Behavior.IBehaviorCtrl NpcMgr::ilo_get_bvrCtrl50(CombatEntity)
extern "C"  IBehaviorCtrl_t4225040900 * NpcMgr_ilo_get_bvrCtrl50_m2777008853 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// taixuTipCfg NpcMgr::ilo_get_taixuTip51(GameMgr)
extern "C"  taixuTipCfg_t1268722370 * NpcMgr_ilo_get_taixuTip51_m118789569 (Il2CppObject * __this /* static, unused */, GameMgr_t1469029542 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NpcMgr::ilo_set_isRevive52(CombatEntity,System.Boolean)
extern "C"  void NpcMgr_ilo_set_isRevive52_m3857799464 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Hero NpcMgr::ilo_GetCaptain53(HeroMgr)
extern "C"  Hero_t2245658 * NpcMgr_ilo_GetCaptain53_m3888614994 (Il2CppObject * __this /* static, unused */, HeroMgr_t2475965342 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NpcMgr::ilo_set_curRound54(NpcMgr,System.Int32)
extern "C"  void NpcMgr_ilo_set_curRound54_m2147581649 (Il2CppObject * __this /* static, unused */, NpcMgr_t2339534743 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NpcMgr::ilo_OnTriggerFun55(Monster,CombatEntity,System.Int32)
extern "C"  void NpcMgr_ilo_OnTriggerFun55_m1948295975 (Il2CppObject * __this /* static, unused */, Monster_t2901270458 * ____this0, CombatEntity_t684137495 * ___main1, int32_t ___effectId2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// EntityEnum.UnitCamp NpcMgr::ilo_get_unitCamp56(CombatEntity)
extern "C"  int32_t NpcMgr_ilo_get_unitCamp56_m917629551 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NpcMgr::ilo_AddBuff57(BuffCtrl,System.Int32)
extern "C"  void NpcMgr_ilo_AddBuff57_m3985023568 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, int32_t ___id1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CameraShotMgr NpcMgr::ilo_get_CameraShotMgr58()
extern "C"  CameraShotMgr_t1580128697 * NpcMgr_ilo_get_CameraShotMgr58_m3966012680 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CombatEntity> NpcMgr::ilo_GetCameraShotMonsters59(NpcMgr)
extern "C"  List_1_t2052323047 * NpcMgr_ilo_GetCameraShotMonsters59_m1646045472 (Il2CppObject * __this /* static, unused */, NpcMgr_t2339534743 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CombatEntity> NpcMgr::ilo_GetCameraShotFriends60(NpcMgr)
extern "C"  List_1_t2052323047 * NpcMgr_ilo_GetCameraShotFriends60_m3006060448 (Il2CppObject * __this /* static, unused */, NpcMgr_t2339534743 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CombatEntity> NpcMgr::ilo_GetAllHostilEntity61(GameMgr)
extern "C"  List_1_t2052323047 * NpcMgr_ilo_GetAllHostilEntity61_m2131541979 (Il2CppObject * __this /* static, unused */, GameMgr_t1469029542 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single NpcMgr::ilo_get_totalDamage62(CombatEntity)
extern "C"  float NpcMgr_ilo_get_totalDamage62_m4275112114 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CombatEntity> NpcMgr::ilo_GetAllFriendsEntity63(GameMgr)
extern "C"  List_1_t2052323047 * NpcMgr_ilo_GetAllFriendsEntity63_m2127095677 (Il2CppObject * __this /* static, unused */, GameMgr_t1469029542 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 NpcMgr::ilo_get_atkType64(CombatEntity)
extern "C"  int32_t NpcMgr_ilo_get_atkType64_m1603496529 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HERO_ELEMENT NpcMgr::ilo_get_element65(CombatEntity)
extern "C"  int32_t NpcMgr_ilo_get_element65_m864879396 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] NpcMgr::ilo_GetArticle_Int66(System.String)
extern "C"  Int32U5BU5D_t3230847821* NpcMgr_ilo_GetArticle_Int66_m294735123 (Il2CppObject * __this /* static, unused */, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// LevelConfigManager NpcMgr::ilo_get_instance67()
extern "C"  LevelConfigManager_t657947911 * NpcMgr_ilo_get_instance67_m3190681576 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 NpcMgr::ilo_GetTotalRounds68(LevelConfigManager,System.Int32)
extern "C"  int32_t NpcMgr_ilo_GetTotalRounds68_m3477717288 (Il2CppObject * __this /* static, unused */, LevelConfigManager_t657947911 * ____this0, int32_t ___level1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single NpcMgr::ilo_get_hp69(CombatEntity)
extern "C"  float NpcMgr_ilo_get_hp69_m2711419100 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 NpcMgr::ilo_get_TotalRound70(NpcMgr)
extern "C"  int32_t NpcMgr_ilo_get_TotalRound70_m2569795958 (Il2CppObject * __this /* static, unused */, NpcMgr_t2339534743 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NpcMgr::ilo_set_isSkillFrozen71(CombatEntity,System.Boolean)
extern "C"  void NpcMgr_ilo_set_isSkillFrozen71_m129927073 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NpcMgr::ilo_IsKillAllMonster72(NpcMgr)
extern "C"  bool NpcMgr_ilo_IsKillAllMonster72_m2612695468 (Il2CppObject * __this /* static, unused */, NpcMgr_t2339534743 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NpcMgr::ilo_ClearDrop73(EnemyDropMgr)
extern "C"  void NpcMgr_ilo_ClearDrop73_m1738389286 (Il2CppObject * __this /* static, unused */, EnemyDropMgr_t2125592609 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// NpcMgr NpcMgr::ilo_get_instance74()
extern "C"  NpcMgr_t2339534743 * NpcMgr_ilo_get_instance74_m419948532 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NpcMgr::ilo_LeaveBattleReq75(HeroMgr,System.Action)
extern "C"  void NpcMgr_ilo_LeaveBattleReq75_m944166219 (Il2CppObject * __this /* static, unused */, HeroMgr_t2475965342 * ____this0, Action_t3771233898 * ___callBack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CombatEntity> NpcMgr::ilo_GetHeroNpcList76(NpcMgr)
extern "C"  List_1_t2052323047 * NpcMgr_ilo_GetHeroNpcList76_m1421794676 (Il2CppObject * __this /* static, unused */, NpcMgr_t2339534743 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NpcMgr::ilo_Clear77(CombatEntity)
extern "C"  void NpcMgr_ilo_Clear77_m3636652901 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NpcMgr::ilo_set_isDeath78(CombatEntity,System.Boolean)
extern "C"  void NpcMgr_ilo_set_isDeath78_m2107640119 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NpcMgr::ilo_set_isFrozen79(CombatEntity,System.Boolean)
extern "C"  void NpcMgr_ilo_set_isFrozen79_m3177257048 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NpcMgr::ilo_ClearmLastUseTime80(FightCtrl)
extern "C"  void NpcMgr_ilo_ClearmLastUseTime80_m173054898 (Il2CppObject * __this /* static, unused */, FightCtrl_t648967803 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NpcMgr::ilo_Clear81(EffectMgr)
extern "C"  void NpcMgr_ilo_Clear81_m970663190 (Il2CppObject * __this /* static, unused */, EffectMgr_t535289511 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// EffectMgr NpcMgr::ilo_get_EffectMgr82()
extern "C"  EffectMgr_t535289511 * NpcMgr_ilo_get_EffectMgr82_m3873438203 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NpcMgr::ilo_TranBehavior83(Entity.Behavior.IBehaviorCtrl,Entity.Behavior.EBehaviorID,System.Object[])
extern "C"  void NpcMgr_ilo_TranBehavior83_m59170313 (Il2CppObject * __this /* static, unused */, IBehaviorCtrl_t4225040900 * ____this0, uint8_t ___id1, ObjectU5BU5D_t1108656482* ___arg2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
