﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3363211663MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,CSPlusAtt>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m2685030764(__this, ___dictionary0, method) ((  void (*) (Enumerator_t1111089625 *, Dictionary_2_t4088733529 *, const MethodInfo*))Enumerator__ctor_m3920831137_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,CSPlusAtt>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2798617653(__this, method) ((  Il2CppObject * (*) (Enumerator_t1111089625 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3262087712_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,CSPlusAtt>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2411574217(__this, method) ((  void (*) (Enumerator_t1111089625 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2959141748_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,CSPlusAtt>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3090410258(__this, method) ((  DictionaryEntry_t1751606614  (*) (Enumerator_t1111089625 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2279524093_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,CSPlusAtt>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2270561617(__this, method) ((  Il2CppObject * (*) (Enumerator_t1111089625 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1201448700_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,CSPlusAtt>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1214763939(__this, method) ((  Il2CppObject * (*) (Enumerator_t1111089625 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m294434446_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,CSPlusAtt>::MoveNext()
#define Enumerator_MoveNext_m3799047989(__this, method) ((  bool (*) (Enumerator_t1111089625 *, const MethodInfo*))Enumerator_MoveNext_m217327200_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,CSPlusAtt>::get_Current()
#define Enumerator_get_Current_m3903195099(__this, method) ((  KeyValuePair_2_t3987514235  (*) (Enumerator_t1111089625 *, const MethodInfo*))Enumerator_get_Current_m4240003024_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,CSPlusAtt>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m3633213506(__this, method) ((  String_t* (*) (Enumerator_t1111089625 *, const MethodInfo*))Enumerator_get_CurrentKey_m3062159917_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,CSPlusAtt>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m3900400998(__this, method) ((  CSPlusAtt_t3268315159 * (*) (Enumerator_t1111089625 *, const MethodInfo*))Enumerator_get_CurrentValue_m592783249_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,CSPlusAtt>::Reset()
#define Enumerator_Reset_m488999358(__this, method) ((  void (*) (Enumerator_t1111089625 *, const MethodInfo*))Enumerator_Reset_m3001375603_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,CSPlusAtt>::VerifyState()
#define Enumerator_VerifyState_m2551059271(__this, method) ((  void (*) (Enumerator_t1111089625 *, const MethodInfo*))Enumerator_VerifyState_m4290054460_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,CSPlusAtt>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m1886505199(__this, method) ((  void (*) (Enumerator_t1111089625 *, const MethodInfo*))Enumerator_VerifyCurrent_m2318603684_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,CSPlusAtt>::Dispose()
#define Enumerator_Dispose_m5409550(__this, method) ((  void (*) (Enumerator_t1111089625 *, const MethodInfo*))Enumerator_Dispose_m627360643_gshared)(__this, method)
