﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ProceduralGridMover/<UpdateGraph>c__AnonStorey112
struct U3CUpdateGraphU3Ec__AnonStorey112_t1564295183;

#include "codegen/il2cpp-codegen.h"

// System.Void ProceduralGridMover/<UpdateGraph>c__AnonStorey112::.ctor()
extern "C"  void U3CUpdateGraphU3Ec__AnonStorey112__ctor_m4190006844 (U3CUpdateGraphU3Ec__AnonStorey112_t1564295183 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProceduralGridMover/<UpdateGraph>c__AnonStorey112::<>m__345(System.Boolean)
extern "C"  bool U3CUpdateGraphU3Ec__AnonStorey112_U3CU3Em__345_m3142393332 (U3CUpdateGraphU3Ec__AnonStorey112_t1564295183 * __this, bool ___force0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
