﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ShaderBossShow
struct ShaderBossShow_t500563759;
// UnityEngine.Material
struct Material_t3870600107;
// UnityEngine.RenderTexture
struct RenderTexture_t1963041563;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture1963041563.h"

// System.Void ShaderBossShow::.ctor()
extern "C"  void ShaderBossShow__ctor_m151679564 (ShaderBossShow_t500563759 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material ShaderBossShow::get_material()
extern "C"  Material_t3870600107 * ShaderBossShow_get_material_m3263761319 (ShaderBossShow_t500563759 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShaderBossShow::OnDestroy()
extern "C"  void ShaderBossShow_OnDestroy_m1317159237 (ShaderBossShow_t500563759 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShaderBossShow::Start()
extern "C"  void ShaderBossShow_Start_m3393784652 (ShaderBossShow_t500563759 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShaderBossShow::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void ShaderBossShow_OnRenderImage_m760982162 (ShaderBossShow_t500563759 * __this, RenderTexture_t1963041563 * ___sourceTexture0, RenderTexture_t1963041563 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShaderBossShow::Update()
extern "C"  void ShaderBossShow_Update_m2133961281 (ShaderBossShow_t500563759 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShaderBossShow::OnDisable()
extern "C"  void ShaderBossShow_OnDisable_m3434594483 (ShaderBossShow_t500563759 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
