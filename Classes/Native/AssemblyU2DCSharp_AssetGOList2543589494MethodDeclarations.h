﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AssetGOList
struct AssetGOList_t2543589494;

#include "codegen/il2cpp-codegen.h"

// System.Void AssetGOList::.ctor()
extern "C"  void AssetGOList__ctor_m126126005 (AssetGOList_t2543589494 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
