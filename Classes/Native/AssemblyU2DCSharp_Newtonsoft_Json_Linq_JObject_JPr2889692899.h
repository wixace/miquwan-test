﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Collections_ObjectModel_KeyedColle1927684661.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JObject/JPropertKeyedCollection
struct  JPropertKeyedCollection_t2889692899  : public KeyedCollection_2_t1927684661
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
