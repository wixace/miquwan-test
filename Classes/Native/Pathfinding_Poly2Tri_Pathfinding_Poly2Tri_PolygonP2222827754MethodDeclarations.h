﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.Poly2Tri.PolygonPoint
struct PolygonPoint_t2222827754;

#include "codegen/il2cpp-codegen.h"
#include "Pathfinding_Poly2Tri_Pathfinding_Poly2Tri_PolygonP2222827754.h"

// System.Void Pathfinding.Poly2Tri.PolygonPoint::.ctor(System.Double,System.Double)
extern "C"  void PolygonPoint__ctor_m3285103561 (PolygonPoint_t2222827754 * __this, double ___x0, double ___y1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Poly2Tri.PolygonPoint Pathfinding.Poly2Tri.PolygonPoint::get_Next()
extern "C"  PolygonPoint_t2222827754 * PolygonPoint_get_Next_m66309293 (PolygonPoint_t2222827754 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Poly2Tri.PolygonPoint::set_Next(Pathfinding.Poly2Tri.PolygonPoint)
extern "C"  void PolygonPoint_set_Next_m2943622558 (PolygonPoint_t2222827754 * __this, PolygonPoint_t2222827754 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Poly2Tri.PolygonPoint::set_Previous(Pathfinding.Poly2Tri.PolygonPoint)
extern "C"  void PolygonPoint_set_Previous_m2758999194 (PolygonPoint_t2222827754 * __this, PolygonPoint_t2222827754 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
