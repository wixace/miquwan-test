﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_bashisZujila9
struct  M_bashisZujila9_t733218764  : public Il2CppObject
{
public:
	// System.UInt32 GarbageiOS.M_bashisZujila9::_callpir
	uint32_t ____callpir_0;
	// System.Boolean GarbageiOS.M_bashisZujila9::_rairniJartrelai
	bool ____rairniJartrelai_1;
	// System.String GarbageiOS.M_bashisZujila9::_terhuparWocote
	String_t* ____terhuparWocote_2;
	// System.UInt32 GarbageiOS.M_bashisZujila9::_peacall
	uint32_t ____peacall_3;
	// System.Boolean GarbageiOS.M_bashisZujila9::_womawcouMoosa
	bool ____womawcouMoosa_4;
	// System.String GarbageiOS.M_bashisZujila9::_tairxalseeHereveje
	String_t* ____tairxalseeHereveje_5;
	// System.Single GarbageiOS.M_bashisZujila9::_kairtreacerSajoulir
	float ____kairtreacerSajoulir_6;
	// System.Single GarbageiOS.M_bashisZujila9::_palowkeaSemci
	float ____palowkeaSemci_7;
	// System.UInt32 GarbageiOS.M_bashisZujila9::_noujo
	uint32_t ____noujo_8;
	// System.UInt32 GarbageiOS.M_bashisZujila9::_worapi
	uint32_t ____worapi_9;
	// System.String GarbageiOS.M_bashisZujila9::_searmorChouli
	String_t* ____searmorChouli_10;
	// System.UInt32 GarbageiOS.M_bashisZujila9::_duteeso
	uint32_t ____duteeso_11;
	// System.Boolean GarbageiOS.M_bashisZujila9::_pacooMateretay
	bool ____pacooMateretay_12;
	// System.Boolean GarbageiOS.M_bashisZujila9::_lirsti
	bool ____lirsti_13;
	// System.Boolean GarbageiOS.M_bashisZujila9::_pasre
	bool ____pasre_14;
	// System.Int32 GarbageiOS.M_bashisZujila9::_gairsirXumo
	int32_t ____gairsirXumo_15;
	// System.Boolean GarbageiOS.M_bashisZujila9::_drassorFowyel
	bool ____drassorFowyel_16;
	// System.Boolean GarbageiOS.M_bashisZujila9::_casaljaTaherhor
	bool ____casaljaTaherhor_17;
	// System.Int32 GarbageiOS.M_bashisZujila9::_sicor
	int32_t ____sicor_18;
	// System.UInt32 GarbageiOS.M_bashisZujila9::_sujeebe
	uint32_t ____sujeebe_19;
	// System.String GarbageiOS.M_bashisZujila9::_gisairre
	String_t* ____gisairre_20;
	// System.Boolean GarbageiOS.M_bashisZujila9::_ruwemaNerege
	bool ____ruwemaNerege_21;
	// System.String GarbageiOS.M_bashisZujila9::_suyaiwou
	String_t* ____suyaiwou_22;
	// System.Boolean GarbageiOS.M_bashisZujila9::_soufouchall
	bool ____soufouchall_23;
	// System.UInt32 GarbageiOS.M_bashisZujila9::_yasceeharTalvou
	uint32_t ____yasceeharTalvou_24;
	// System.String GarbageiOS.M_bashisZujila9::_bagokoSouri
	String_t* ____bagokoSouri_25;
	// System.String GarbageiOS.M_bashisZujila9::_jersermelReyallci
	String_t* ____jersermelReyallci_26;
	// System.UInt32 GarbageiOS.M_bashisZujila9::_drodooxe
	uint32_t ____drodooxe_27;
	// System.Int32 GarbageiOS.M_bashisZujila9::_dowmalkeCarhirstaw
	int32_t ____dowmalkeCarhirstaw_28;
	// System.Int32 GarbageiOS.M_bashisZujila9::_moxirkiBerpouwe
	int32_t ____moxirkiBerpouwe_29;
	// System.Single GarbageiOS.M_bashisZujila9::_patis
	float ____patis_30;
	// System.Single GarbageiOS.M_bashisZujila9::_daliXawpai
	float ____daliXawpai_31;
	// System.UInt32 GarbageiOS.M_bashisZujila9::_ceebeaMelemtou
	uint32_t ____ceebeaMelemtou_32;
	// System.Single GarbageiOS.M_bashisZujila9::_wookir
	float ____wookir_33;
	// System.UInt32 GarbageiOS.M_bashisZujila9::_carhougere
	uint32_t ____carhougere_34;
	// System.Int32 GarbageiOS.M_bashisZujila9::_tairkiryair
	int32_t ____tairkiryair_35;
	// System.UInt32 GarbageiOS.M_bashisZujila9::_trisdrigar
	uint32_t ____trisdrigar_36;
	// System.Boolean GarbageiOS.M_bashisZujila9::_betemiBoutrallhi
	bool ____betemiBoutrallhi_37;
	// System.Single GarbageiOS.M_bashisZujila9::_cemoumearXaypea
	float ____cemoumearXaypea_38;
	// System.String GarbageiOS.M_bashisZujila9::_micorDrijemtrall
	String_t* ____micorDrijemtrall_39;
	// System.Int32 GarbageiOS.M_bashisZujila9::_bedalSaiwhu
	int32_t ____bedalSaiwhu_40;

public:
	inline static int32_t get_offset_of__callpir_0() { return static_cast<int32_t>(offsetof(M_bashisZujila9_t733218764, ____callpir_0)); }
	inline uint32_t get__callpir_0() const { return ____callpir_0; }
	inline uint32_t* get_address_of__callpir_0() { return &____callpir_0; }
	inline void set__callpir_0(uint32_t value)
	{
		____callpir_0 = value;
	}

	inline static int32_t get_offset_of__rairniJartrelai_1() { return static_cast<int32_t>(offsetof(M_bashisZujila9_t733218764, ____rairniJartrelai_1)); }
	inline bool get__rairniJartrelai_1() const { return ____rairniJartrelai_1; }
	inline bool* get_address_of__rairniJartrelai_1() { return &____rairniJartrelai_1; }
	inline void set__rairniJartrelai_1(bool value)
	{
		____rairniJartrelai_1 = value;
	}

	inline static int32_t get_offset_of__terhuparWocote_2() { return static_cast<int32_t>(offsetof(M_bashisZujila9_t733218764, ____terhuparWocote_2)); }
	inline String_t* get__terhuparWocote_2() const { return ____terhuparWocote_2; }
	inline String_t** get_address_of__terhuparWocote_2() { return &____terhuparWocote_2; }
	inline void set__terhuparWocote_2(String_t* value)
	{
		____terhuparWocote_2 = value;
		Il2CppCodeGenWriteBarrier(&____terhuparWocote_2, value);
	}

	inline static int32_t get_offset_of__peacall_3() { return static_cast<int32_t>(offsetof(M_bashisZujila9_t733218764, ____peacall_3)); }
	inline uint32_t get__peacall_3() const { return ____peacall_3; }
	inline uint32_t* get_address_of__peacall_3() { return &____peacall_3; }
	inline void set__peacall_3(uint32_t value)
	{
		____peacall_3 = value;
	}

	inline static int32_t get_offset_of__womawcouMoosa_4() { return static_cast<int32_t>(offsetof(M_bashisZujila9_t733218764, ____womawcouMoosa_4)); }
	inline bool get__womawcouMoosa_4() const { return ____womawcouMoosa_4; }
	inline bool* get_address_of__womawcouMoosa_4() { return &____womawcouMoosa_4; }
	inline void set__womawcouMoosa_4(bool value)
	{
		____womawcouMoosa_4 = value;
	}

	inline static int32_t get_offset_of__tairxalseeHereveje_5() { return static_cast<int32_t>(offsetof(M_bashisZujila9_t733218764, ____tairxalseeHereveje_5)); }
	inline String_t* get__tairxalseeHereveje_5() const { return ____tairxalseeHereveje_5; }
	inline String_t** get_address_of__tairxalseeHereveje_5() { return &____tairxalseeHereveje_5; }
	inline void set__tairxalseeHereveje_5(String_t* value)
	{
		____tairxalseeHereveje_5 = value;
		Il2CppCodeGenWriteBarrier(&____tairxalseeHereveje_5, value);
	}

	inline static int32_t get_offset_of__kairtreacerSajoulir_6() { return static_cast<int32_t>(offsetof(M_bashisZujila9_t733218764, ____kairtreacerSajoulir_6)); }
	inline float get__kairtreacerSajoulir_6() const { return ____kairtreacerSajoulir_6; }
	inline float* get_address_of__kairtreacerSajoulir_6() { return &____kairtreacerSajoulir_6; }
	inline void set__kairtreacerSajoulir_6(float value)
	{
		____kairtreacerSajoulir_6 = value;
	}

	inline static int32_t get_offset_of__palowkeaSemci_7() { return static_cast<int32_t>(offsetof(M_bashisZujila9_t733218764, ____palowkeaSemci_7)); }
	inline float get__palowkeaSemci_7() const { return ____palowkeaSemci_7; }
	inline float* get_address_of__palowkeaSemci_7() { return &____palowkeaSemci_7; }
	inline void set__palowkeaSemci_7(float value)
	{
		____palowkeaSemci_7 = value;
	}

	inline static int32_t get_offset_of__noujo_8() { return static_cast<int32_t>(offsetof(M_bashisZujila9_t733218764, ____noujo_8)); }
	inline uint32_t get__noujo_8() const { return ____noujo_8; }
	inline uint32_t* get_address_of__noujo_8() { return &____noujo_8; }
	inline void set__noujo_8(uint32_t value)
	{
		____noujo_8 = value;
	}

	inline static int32_t get_offset_of__worapi_9() { return static_cast<int32_t>(offsetof(M_bashisZujila9_t733218764, ____worapi_9)); }
	inline uint32_t get__worapi_9() const { return ____worapi_9; }
	inline uint32_t* get_address_of__worapi_9() { return &____worapi_9; }
	inline void set__worapi_9(uint32_t value)
	{
		____worapi_9 = value;
	}

	inline static int32_t get_offset_of__searmorChouli_10() { return static_cast<int32_t>(offsetof(M_bashisZujila9_t733218764, ____searmorChouli_10)); }
	inline String_t* get__searmorChouli_10() const { return ____searmorChouli_10; }
	inline String_t** get_address_of__searmorChouli_10() { return &____searmorChouli_10; }
	inline void set__searmorChouli_10(String_t* value)
	{
		____searmorChouli_10 = value;
		Il2CppCodeGenWriteBarrier(&____searmorChouli_10, value);
	}

	inline static int32_t get_offset_of__duteeso_11() { return static_cast<int32_t>(offsetof(M_bashisZujila9_t733218764, ____duteeso_11)); }
	inline uint32_t get__duteeso_11() const { return ____duteeso_11; }
	inline uint32_t* get_address_of__duteeso_11() { return &____duteeso_11; }
	inline void set__duteeso_11(uint32_t value)
	{
		____duteeso_11 = value;
	}

	inline static int32_t get_offset_of__pacooMateretay_12() { return static_cast<int32_t>(offsetof(M_bashisZujila9_t733218764, ____pacooMateretay_12)); }
	inline bool get__pacooMateretay_12() const { return ____pacooMateretay_12; }
	inline bool* get_address_of__pacooMateretay_12() { return &____pacooMateretay_12; }
	inline void set__pacooMateretay_12(bool value)
	{
		____pacooMateretay_12 = value;
	}

	inline static int32_t get_offset_of__lirsti_13() { return static_cast<int32_t>(offsetof(M_bashisZujila9_t733218764, ____lirsti_13)); }
	inline bool get__lirsti_13() const { return ____lirsti_13; }
	inline bool* get_address_of__lirsti_13() { return &____lirsti_13; }
	inline void set__lirsti_13(bool value)
	{
		____lirsti_13 = value;
	}

	inline static int32_t get_offset_of__pasre_14() { return static_cast<int32_t>(offsetof(M_bashisZujila9_t733218764, ____pasre_14)); }
	inline bool get__pasre_14() const { return ____pasre_14; }
	inline bool* get_address_of__pasre_14() { return &____pasre_14; }
	inline void set__pasre_14(bool value)
	{
		____pasre_14 = value;
	}

	inline static int32_t get_offset_of__gairsirXumo_15() { return static_cast<int32_t>(offsetof(M_bashisZujila9_t733218764, ____gairsirXumo_15)); }
	inline int32_t get__gairsirXumo_15() const { return ____gairsirXumo_15; }
	inline int32_t* get_address_of__gairsirXumo_15() { return &____gairsirXumo_15; }
	inline void set__gairsirXumo_15(int32_t value)
	{
		____gairsirXumo_15 = value;
	}

	inline static int32_t get_offset_of__drassorFowyel_16() { return static_cast<int32_t>(offsetof(M_bashisZujila9_t733218764, ____drassorFowyel_16)); }
	inline bool get__drassorFowyel_16() const { return ____drassorFowyel_16; }
	inline bool* get_address_of__drassorFowyel_16() { return &____drassorFowyel_16; }
	inline void set__drassorFowyel_16(bool value)
	{
		____drassorFowyel_16 = value;
	}

	inline static int32_t get_offset_of__casaljaTaherhor_17() { return static_cast<int32_t>(offsetof(M_bashisZujila9_t733218764, ____casaljaTaherhor_17)); }
	inline bool get__casaljaTaherhor_17() const { return ____casaljaTaherhor_17; }
	inline bool* get_address_of__casaljaTaherhor_17() { return &____casaljaTaherhor_17; }
	inline void set__casaljaTaherhor_17(bool value)
	{
		____casaljaTaherhor_17 = value;
	}

	inline static int32_t get_offset_of__sicor_18() { return static_cast<int32_t>(offsetof(M_bashisZujila9_t733218764, ____sicor_18)); }
	inline int32_t get__sicor_18() const { return ____sicor_18; }
	inline int32_t* get_address_of__sicor_18() { return &____sicor_18; }
	inline void set__sicor_18(int32_t value)
	{
		____sicor_18 = value;
	}

	inline static int32_t get_offset_of__sujeebe_19() { return static_cast<int32_t>(offsetof(M_bashisZujila9_t733218764, ____sujeebe_19)); }
	inline uint32_t get__sujeebe_19() const { return ____sujeebe_19; }
	inline uint32_t* get_address_of__sujeebe_19() { return &____sujeebe_19; }
	inline void set__sujeebe_19(uint32_t value)
	{
		____sujeebe_19 = value;
	}

	inline static int32_t get_offset_of__gisairre_20() { return static_cast<int32_t>(offsetof(M_bashisZujila9_t733218764, ____gisairre_20)); }
	inline String_t* get__gisairre_20() const { return ____gisairre_20; }
	inline String_t** get_address_of__gisairre_20() { return &____gisairre_20; }
	inline void set__gisairre_20(String_t* value)
	{
		____gisairre_20 = value;
		Il2CppCodeGenWriteBarrier(&____gisairre_20, value);
	}

	inline static int32_t get_offset_of__ruwemaNerege_21() { return static_cast<int32_t>(offsetof(M_bashisZujila9_t733218764, ____ruwemaNerege_21)); }
	inline bool get__ruwemaNerege_21() const { return ____ruwemaNerege_21; }
	inline bool* get_address_of__ruwemaNerege_21() { return &____ruwemaNerege_21; }
	inline void set__ruwemaNerege_21(bool value)
	{
		____ruwemaNerege_21 = value;
	}

	inline static int32_t get_offset_of__suyaiwou_22() { return static_cast<int32_t>(offsetof(M_bashisZujila9_t733218764, ____suyaiwou_22)); }
	inline String_t* get__suyaiwou_22() const { return ____suyaiwou_22; }
	inline String_t** get_address_of__suyaiwou_22() { return &____suyaiwou_22; }
	inline void set__suyaiwou_22(String_t* value)
	{
		____suyaiwou_22 = value;
		Il2CppCodeGenWriteBarrier(&____suyaiwou_22, value);
	}

	inline static int32_t get_offset_of__soufouchall_23() { return static_cast<int32_t>(offsetof(M_bashisZujila9_t733218764, ____soufouchall_23)); }
	inline bool get__soufouchall_23() const { return ____soufouchall_23; }
	inline bool* get_address_of__soufouchall_23() { return &____soufouchall_23; }
	inline void set__soufouchall_23(bool value)
	{
		____soufouchall_23 = value;
	}

	inline static int32_t get_offset_of__yasceeharTalvou_24() { return static_cast<int32_t>(offsetof(M_bashisZujila9_t733218764, ____yasceeharTalvou_24)); }
	inline uint32_t get__yasceeharTalvou_24() const { return ____yasceeharTalvou_24; }
	inline uint32_t* get_address_of__yasceeharTalvou_24() { return &____yasceeharTalvou_24; }
	inline void set__yasceeharTalvou_24(uint32_t value)
	{
		____yasceeharTalvou_24 = value;
	}

	inline static int32_t get_offset_of__bagokoSouri_25() { return static_cast<int32_t>(offsetof(M_bashisZujila9_t733218764, ____bagokoSouri_25)); }
	inline String_t* get__bagokoSouri_25() const { return ____bagokoSouri_25; }
	inline String_t** get_address_of__bagokoSouri_25() { return &____bagokoSouri_25; }
	inline void set__bagokoSouri_25(String_t* value)
	{
		____bagokoSouri_25 = value;
		Il2CppCodeGenWriteBarrier(&____bagokoSouri_25, value);
	}

	inline static int32_t get_offset_of__jersermelReyallci_26() { return static_cast<int32_t>(offsetof(M_bashisZujila9_t733218764, ____jersermelReyallci_26)); }
	inline String_t* get__jersermelReyallci_26() const { return ____jersermelReyallci_26; }
	inline String_t** get_address_of__jersermelReyallci_26() { return &____jersermelReyallci_26; }
	inline void set__jersermelReyallci_26(String_t* value)
	{
		____jersermelReyallci_26 = value;
		Il2CppCodeGenWriteBarrier(&____jersermelReyallci_26, value);
	}

	inline static int32_t get_offset_of__drodooxe_27() { return static_cast<int32_t>(offsetof(M_bashisZujila9_t733218764, ____drodooxe_27)); }
	inline uint32_t get__drodooxe_27() const { return ____drodooxe_27; }
	inline uint32_t* get_address_of__drodooxe_27() { return &____drodooxe_27; }
	inline void set__drodooxe_27(uint32_t value)
	{
		____drodooxe_27 = value;
	}

	inline static int32_t get_offset_of__dowmalkeCarhirstaw_28() { return static_cast<int32_t>(offsetof(M_bashisZujila9_t733218764, ____dowmalkeCarhirstaw_28)); }
	inline int32_t get__dowmalkeCarhirstaw_28() const { return ____dowmalkeCarhirstaw_28; }
	inline int32_t* get_address_of__dowmalkeCarhirstaw_28() { return &____dowmalkeCarhirstaw_28; }
	inline void set__dowmalkeCarhirstaw_28(int32_t value)
	{
		____dowmalkeCarhirstaw_28 = value;
	}

	inline static int32_t get_offset_of__moxirkiBerpouwe_29() { return static_cast<int32_t>(offsetof(M_bashisZujila9_t733218764, ____moxirkiBerpouwe_29)); }
	inline int32_t get__moxirkiBerpouwe_29() const { return ____moxirkiBerpouwe_29; }
	inline int32_t* get_address_of__moxirkiBerpouwe_29() { return &____moxirkiBerpouwe_29; }
	inline void set__moxirkiBerpouwe_29(int32_t value)
	{
		____moxirkiBerpouwe_29 = value;
	}

	inline static int32_t get_offset_of__patis_30() { return static_cast<int32_t>(offsetof(M_bashisZujila9_t733218764, ____patis_30)); }
	inline float get__patis_30() const { return ____patis_30; }
	inline float* get_address_of__patis_30() { return &____patis_30; }
	inline void set__patis_30(float value)
	{
		____patis_30 = value;
	}

	inline static int32_t get_offset_of__daliXawpai_31() { return static_cast<int32_t>(offsetof(M_bashisZujila9_t733218764, ____daliXawpai_31)); }
	inline float get__daliXawpai_31() const { return ____daliXawpai_31; }
	inline float* get_address_of__daliXawpai_31() { return &____daliXawpai_31; }
	inline void set__daliXawpai_31(float value)
	{
		____daliXawpai_31 = value;
	}

	inline static int32_t get_offset_of__ceebeaMelemtou_32() { return static_cast<int32_t>(offsetof(M_bashisZujila9_t733218764, ____ceebeaMelemtou_32)); }
	inline uint32_t get__ceebeaMelemtou_32() const { return ____ceebeaMelemtou_32; }
	inline uint32_t* get_address_of__ceebeaMelemtou_32() { return &____ceebeaMelemtou_32; }
	inline void set__ceebeaMelemtou_32(uint32_t value)
	{
		____ceebeaMelemtou_32 = value;
	}

	inline static int32_t get_offset_of__wookir_33() { return static_cast<int32_t>(offsetof(M_bashisZujila9_t733218764, ____wookir_33)); }
	inline float get__wookir_33() const { return ____wookir_33; }
	inline float* get_address_of__wookir_33() { return &____wookir_33; }
	inline void set__wookir_33(float value)
	{
		____wookir_33 = value;
	}

	inline static int32_t get_offset_of__carhougere_34() { return static_cast<int32_t>(offsetof(M_bashisZujila9_t733218764, ____carhougere_34)); }
	inline uint32_t get__carhougere_34() const { return ____carhougere_34; }
	inline uint32_t* get_address_of__carhougere_34() { return &____carhougere_34; }
	inline void set__carhougere_34(uint32_t value)
	{
		____carhougere_34 = value;
	}

	inline static int32_t get_offset_of__tairkiryair_35() { return static_cast<int32_t>(offsetof(M_bashisZujila9_t733218764, ____tairkiryair_35)); }
	inline int32_t get__tairkiryair_35() const { return ____tairkiryair_35; }
	inline int32_t* get_address_of__tairkiryair_35() { return &____tairkiryair_35; }
	inline void set__tairkiryair_35(int32_t value)
	{
		____tairkiryair_35 = value;
	}

	inline static int32_t get_offset_of__trisdrigar_36() { return static_cast<int32_t>(offsetof(M_bashisZujila9_t733218764, ____trisdrigar_36)); }
	inline uint32_t get__trisdrigar_36() const { return ____trisdrigar_36; }
	inline uint32_t* get_address_of__trisdrigar_36() { return &____trisdrigar_36; }
	inline void set__trisdrigar_36(uint32_t value)
	{
		____trisdrigar_36 = value;
	}

	inline static int32_t get_offset_of__betemiBoutrallhi_37() { return static_cast<int32_t>(offsetof(M_bashisZujila9_t733218764, ____betemiBoutrallhi_37)); }
	inline bool get__betemiBoutrallhi_37() const { return ____betemiBoutrallhi_37; }
	inline bool* get_address_of__betemiBoutrallhi_37() { return &____betemiBoutrallhi_37; }
	inline void set__betemiBoutrallhi_37(bool value)
	{
		____betemiBoutrallhi_37 = value;
	}

	inline static int32_t get_offset_of__cemoumearXaypea_38() { return static_cast<int32_t>(offsetof(M_bashisZujila9_t733218764, ____cemoumearXaypea_38)); }
	inline float get__cemoumearXaypea_38() const { return ____cemoumearXaypea_38; }
	inline float* get_address_of__cemoumearXaypea_38() { return &____cemoumearXaypea_38; }
	inline void set__cemoumearXaypea_38(float value)
	{
		____cemoumearXaypea_38 = value;
	}

	inline static int32_t get_offset_of__micorDrijemtrall_39() { return static_cast<int32_t>(offsetof(M_bashisZujila9_t733218764, ____micorDrijemtrall_39)); }
	inline String_t* get__micorDrijemtrall_39() const { return ____micorDrijemtrall_39; }
	inline String_t** get_address_of__micorDrijemtrall_39() { return &____micorDrijemtrall_39; }
	inline void set__micorDrijemtrall_39(String_t* value)
	{
		____micorDrijemtrall_39 = value;
		Il2CppCodeGenWriteBarrier(&____micorDrijemtrall_39, value);
	}

	inline static int32_t get_offset_of__bedalSaiwhu_40() { return static_cast<int32_t>(offsetof(M_bashisZujila9_t733218764, ____bedalSaiwhu_40)); }
	inline int32_t get__bedalSaiwhu_40() const { return ____bedalSaiwhu_40; }
	inline int32_t* get_address_of__bedalSaiwhu_40() { return &____bedalSaiwhu_40; }
	inline void set__bedalSaiwhu_40(int32_t value)
	{
		____bedalSaiwhu_40 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
