﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_HeadUpBloodMgr_LabelType4067300664.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NumObject
struct  NumObject_t3106938437  : public Il2CppObject
{
public:
	// System.Boolean NumObject::isHero
	bool ___isHero_0;
	// System.Int32 NumObject::value
	int32_t ___value_1;
	// UnityEngine.Vector3 NumObject::pos
	Vector3_t4282066566  ___pos_2;
	// System.Int32 NumObject::id
	int32_t ___id_3;
	// System.Single NumObject::time
	float ___time_4;
	// HeadUpBloodMgr/LabelType NumObject::type
	int32_t ___type_5;

public:
	inline static int32_t get_offset_of_isHero_0() { return static_cast<int32_t>(offsetof(NumObject_t3106938437, ___isHero_0)); }
	inline bool get_isHero_0() const { return ___isHero_0; }
	inline bool* get_address_of_isHero_0() { return &___isHero_0; }
	inline void set_isHero_0(bool value)
	{
		___isHero_0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(NumObject_t3106938437, ___value_1)); }
	inline int32_t get_value_1() const { return ___value_1; }
	inline int32_t* get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(int32_t value)
	{
		___value_1 = value;
	}

	inline static int32_t get_offset_of_pos_2() { return static_cast<int32_t>(offsetof(NumObject_t3106938437, ___pos_2)); }
	inline Vector3_t4282066566  get_pos_2() const { return ___pos_2; }
	inline Vector3_t4282066566 * get_address_of_pos_2() { return &___pos_2; }
	inline void set_pos_2(Vector3_t4282066566  value)
	{
		___pos_2 = value;
	}

	inline static int32_t get_offset_of_id_3() { return static_cast<int32_t>(offsetof(NumObject_t3106938437, ___id_3)); }
	inline int32_t get_id_3() const { return ___id_3; }
	inline int32_t* get_address_of_id_3() { return &___id_3; }
	inline void set_id_3(int32_t value)
	{
		___id_3 = value;
	}

	inline static int32_t get_offset_of_time_4() { return static_cast<int32_t>(offsetof(NumObject_t3106938437, ___time_4)); }
	inline float get_time_4() const { return ___time_4; }
	inline float* get_address_of_time_4() { return &___time_4; }
	inline void set_time_4(float value)
	{
		___time_4 = value;
	}

	inline static int32_t get_offset_of_type_5() { return static_cast<int32_t>(offsetof(NumObject_t3106938437, ___type_5)); }
	inline int32_t get_type_5() const { return ___type_5; }
	inline int32_t* get_address_of_type_5() { return &___type_5; }
	inline void set_type_5(int32_t value)
	{
		___type_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
