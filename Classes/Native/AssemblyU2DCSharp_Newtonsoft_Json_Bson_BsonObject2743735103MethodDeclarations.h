﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Bson.BsonObject
struct BsonObject_t2743735103;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.String
struct String_t;
// Newtonsoft.Json.Bson.BsonToken
struct BsonToken_t455725415;
// System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.Bson.BsonProperty>
struct IEnumerator_1_t1910719086;
// Newtonsoft.Json.Bson.BsonProperty
struct BsonProperty_t4293821333;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Bson_BsonToken455725415.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Bson_BsonType2455132538.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Bson_BsonObject2743735103.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Bson_BsonPropert4293821333.h"

// System.Void Newtonsoft.Json.Bson.BsonObject::.ctor()
extern "C"  void BsonObject__ctor_m420117619 (BsonObject_t2743735103 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Newtonsoft.Json.Bson.BsonObject::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * BsonObject_System_Collections_IEnumerable_GetEnumerator_m1055900558 (BsonObject_t2743735103 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Bson.BsonObject::Add(System.String,Newtonsoft.Json.Bson.BsonToken)
extern "C"  void BsonObject_Add_m409127766 (BsonObject_t2743735103 * __this, String_t* ___name0, BsonToken_t455725415 * ___token1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Bson.BsonType Newtonsoft.Json.Bson.BsonObject::get_Type()
extern "C"  int8_t BsonObject_get_Type_m2991329076 (BsonObject_t2743735103 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.Bson.BsonProperty> Newtonsoft.Json.Bson.BsonObject::GetEnumerator()
extern "C"  Il2CppObject* BsonObject_GetEnumerator_m790388065 (BsonObject_t2743735103 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.Bson.BsonProperty> Newtonsoft.Json.Bson.BsonObject::ilo_GetEnumerator1(Newtonsoft.Json.Bson.BsonObject)
extern "C"  Il2CppObject* BsonObject_ilo_GetEnumerator1_m743693845 (Il2CppObject * __this /* static, unused */, BsonObject_t2743735103 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Bson.BsonObject::ilo_set_Value2(Newtonsoft.Json.Bson.BsonProperty,Newtonsoft.Json.Bson.BsonToken)
extern "C"  void BsonObject_ilo_set_Value2_m3324637316 (Il2CppObject * __this /* static, unused */, BsonProperty_t4293821333 * ____this0, BsonToken_t455725415 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Bson.BsonObject::ilo_set_Parent3(Newtonsoft.Json.Bson.BsonToken,Newtonsoft.Json.Bson.BsonToken)
extern "C"  void BsonObject_ilo_set_Parent3_m237113628 (Il2CppObject * __this /* static, unused */, BsonToken_t455725415 * ____this0, BsonToken_t455725415 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
