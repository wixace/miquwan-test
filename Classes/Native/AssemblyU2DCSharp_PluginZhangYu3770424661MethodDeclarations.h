﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PluginZhangYu
struct PluginZhangYu_t3770424661;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// System.String
struct String_t;
// VersionMgr
struct VersionMgr_t1322950208;
// VersionInfo
struct VersionInfo_t2356638086;
// ProductsCfgMgr
struct ProductsCfgMgr_t2493714872;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_VersionMgr1322950208.h"
#include "AssemblyU2DCSharp_PluginZhangYu3770424661.h"
#include "AssemblyU2DCSharp_ProductsCfgMgr2493714872.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void PluginZhangYu::.ctor()
extern "C"  void PluginZhangYu__ctor_m2276553654 (PluginZhangYu_t3770424661 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZhangYu::Init()
extern "C"  void PluginZhangYu_Init_m2657373150 (PluginZhangYu_t3770424661 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginZhangYu::ReqSDKHttpLogin()
extern "C"  Dictionary_2_t827649927 * PluginZhangYu_ReqSDKHttpLogin_m4121109031 (PluginZhangYu_t3770424661 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginZhangYu::IsLoginSuccess(System.Boolean)
extern "C"  bool PluginZhangYu_IsLoginSuccess_m3378115573 (PluginZhangYu_t3770424661 * __this, bool ___isOpenUseLogin0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZhangYu::OpenUserLogin()
extern "C"  void PluginZhangYu_OpenUserLogin_m2800648200 (PluginZhangYu_t3770424661 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZhangYu::UserPay(CEvent.ZEvent)
extern "C"  void PluginZhangYu_UserPay_m4230904426 (PluginZhangYu_t3770424661 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZhangYu::OnInitSuccess(System.String)
extern "C"  void PluginZhangYu_OnInitSuccess_m2334433722 (PluginZhangYu_t3770424661 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZhangYu::OnInitFail(System.String)
extern "C"  void PluginZhangYu_OnInitFail_m1720760775 (PluginZhangYu_t3770424661 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZhangYu::OnLoginSuccess(System.String)
extern "C"  void PluginZhangYu_OnLoginSuccess_m4037680699 (PluginZhangYu_t3770424661 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZhangYu::OnLoginFail(System.String)
extern "C"  void PluginZhangYu_OnLoginFail_m433812710 (PluginZhangYu_t3770424661 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZhangYu::OnPaySuccess(System.String)
extern "C"  void PluginZhangYu_OnPaySuccess_m778621754 (PluginZhangYu_t3770424661 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZhangYu::OnPayFail(System.String)
extern "C"  void PluginZhangYu_OnPayFail_m3314939975 (PluginZhangYu_t3770424661 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZhangYu::OnExitGameSuccess(System.String)
extern "C"  void PluginZhangYu_OnExitGameSuccess_m2661538010 (PluginZhangYu_t3770424661 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZhangYu::LogoutSuccess()
extern "C"  void PluginZhangYu_LogoutSuccess_m2751940269 (PluginZhangYu_t3770424661 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZhangYu::OnLogoutSuccess(System.String)
extern "C"  void PluginZhangYu_OnLogoutSuccess_m617592756 (PluginZhangYu_t3770424661 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZhangYu::OnLogoutFail(System.String)
extern "C"  void PluginZhangYu_OnLogoutFail_m376174093 (PluginZhangYu_t3770424661 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZhangYu::InitZhangYu()
extern "C"  void PluginZhangYu_InitZhangYu_m2299864460 (PluginZhangYu_t3770424661 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZhangYu::LoginSdk()
extern "C"  void PluginZhangYu_LoginSdk_m3471518751 (PluginZhangYu_t3770424661 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZhangYu::Pay(System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginZhangYu_Pay_m2158064558 (PluginZhangYu_t3770424661 * __this, String_t* ___amount0, String_t* ___productId1, String_t* ___orderId2, String_t* ___serverId3, String_t* ___roleId4, String_t* ___roleLv5, String_t* ___extra6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZhangYu::logout()
extern "C"  void PluginZhangYu_logout_m4221837944 (PluginZhangYu_t3770424661 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZhangYu::<OnLogoutSuccess>m__481()
extern "C"  void PluginZhangYu_U3COnLogoutSuccessU3Em__481_m3734854804 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionMgr PluginZhangYu::ilo_get_Instance1()
extern "C"  VersionMgr_t1322950208 * PluginZhangYu_ilo_get_Instance1_m1931878289 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionInfo PluginZhangYu::ilo_get_currentVS2(VersionMgr)
extern "C"  VersionInfo_t2356638086 * PluginZhangYu_ilo_get_currentVS2_m2864999481 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZhangYu::ilo_InitZhangYu3(PluginZhangYu)
extern "C"  void PluginZhangYu_ilo_InitZhangYu3_m742003905 (Il2CppObject * __this /* static, unused */, PluginZhangYu_t3770424661 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginZhangYu::ilo_get_isSdkLogin4(VersionMgr)
extern "C"  bool PluginZhangYu_ilo_get_isSdkLogin4_m593397671 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZhangYu::ilo_OpenUserLogin5(PluginZhangYu)
extern "C"  void PluginZhangYu_ilo_OpenUserLogin5_m2617293499 (Il2CppObject * __this /* static, unused */, PluginZhangYu_t3770424661 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PluginZhangYu::ilo_GetProductID6(ProductsCfgMgr,System.String)
extern "C"  String_t* PluginZhangYu_ilo_GetProductID6_m1426533386 (Il2CppObject * __this /* static, unused */, ProductsCfgMgr_t2493714872 * ____this0, String_t* ___name1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZhangYu::ilo_Pay7(PluginZhangYu,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginZhangYu_ilo_Pay7_m3382817105 (Il2CppObject * __this /* static, unused */, PluginZhangYu_t3770424661 * ____this0, String_t* ___amount1, String_t* ___productId2, String_t* ___orderId3, String_t* ___serverId4, String_t* ___roleId5, String_t* ___roleLv6, String_t* ___extra7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZhangYu::ilo_Log8(System.Object,System.Boolean)
extern "C"  void PluginZhangYu_ilo_Log8_m3780212382 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZhangYu::ilo_DispatchEvent9(CEvent.ZEvent)
extern "C"  void PluginZhangYu_ilo_DispatchEvent9_m3250634433 (Il2CppObject * __this /* static, unused */, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZhangYu::ilo_logout10(PluginZhangYu)
extern "C"  void PluginZhangYu_ilo_logout10_m2106313779 (Il2CppObject * __this /* static, unused */, PluginZhangYu_t3770424661 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
