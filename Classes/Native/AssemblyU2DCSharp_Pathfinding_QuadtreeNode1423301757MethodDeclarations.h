﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.QuadtreeNode
struct QuadtreeNode_t1423301757;
// AstarPath
struct AstarPath_t4090270936;
// Pathfinding.GraphNodeDelegate
struct GraphNodeDelegate_t1466738551;
// Pathfinding.GraphNode
struct GraphNode_t23612370;
// Pathfinding.Path
struct Path_t1974241691;
// Pathfinding.PathNode
struct PathNode_t417131581;
// Pathfinding.PathHandler
struct PathHandler_t918952263;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_AstarPath4090270936.h"
#include "AssemblyU2DCSharp_Pathfinding_Int31974045594.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphNodeDelegate1466738551.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphNode23612370.h"
#include "AssemblyU2DCSharp_Pathfinding_Path1974241691.h"
#include "AssemblyU2DCSharp_Pathfinding_PathNode417131581.h"
#include "AssemblyU2DCSharp_Pathfinding_PathHandler918952263.h"

// System.Void Pathfinding.QuadtreeNode::.ctor(AstarPath)
extern "C"  void QuadtreeNode__ctor_m4187798800 (QuadtreeNode_t1423301757 * __this, AstarPath_t4090270936 * ___astar0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.QuadtreeNode::SetPosition(Pathfinding.Int3)
extern "C"  void QuadtreeNode_SetPosition_m4050174661 (QuadtreeNode_t1423301757 * __this, Int3_t1974045594  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.QuadtreeNode::GetConnections(Pathfinding.GraphNodeDelegate)
extern "C"  void QuadtreeNode_GetConnections_m249839342 (QuadtreeNode_t1423301757 * __this, GraphNodeDelegate_t1466738551 * ___del0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.QuadtreeNode::AddConnection(Pathfinding.GraphNode,System.UInt32)
extern "C"  void QuadtreeNode_AddConnection_m1701259129 (QuadtreeNode_t1423301757 * __this, GraphNode_t23612370 * ___node0, uint32_t ___cost1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.QuadtreeNode::RemoveConnection(Pathfinding.GraphNode)
extern "C"  void QuadtreeNode_RemoveConnection_m3114819184 (QuadtreeNode_t1423301757 * __this, GraphNode_t23612370 * ___node0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.QuadtreeNode::ClearConnections(System.Boolean)
extern "C"  void QuadtreeNode_ClearConnections_m191878329 (QuadtreeNode_t1423301757 * __this, bool ___alsoReverse0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.QuadtreeNode::Open(Pathfinding.Path,Pathfinding.PathNode,Pathfinding.PathHandler)
extern "C"  void QuadtreeNode_Open_m587460625 (QuadtreeNode_t1423301757 * __this, Path_t1974241691 * ___path0, PathNode_t417131581 * ___pathNode1, PathHandler_t918952263 * ___handler2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.QuadtreeNode::ilo_Invoke1(Pathfinding.GraphNodeDelegate,Pathfinding.GraphNode)
extern "C"  void QuadtreeNode_ilo_Invoke1_m2988341503 (Il2CppObject * __this /* static, unused */, GraphNodeDelegate_t1466738551 * ____this0, GraphNode_t23612370 * ___node1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.QuadtreeNode::ilo_RemoveConnection2(Pathfinding.GraphNode,Pathfinding.GraphNode)
extern "C"  void QuadtreeNode_ilo_RemoveConnection2_m2075285613 (Il2CppObject * __this /* static, unused */, GraphNode_t23612370 * ____this0, GraphNode_t23612370 * ___node1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.QuadtreeNode::ilo_CanTraverse3(Pathfinding.Path,Pathfinding.GraphNode)
extern "C"  bool QuadtreeNode_ilo_CanTraverse3_m2066727401 (Il2CppObject * __this /* static, unused */, Path_t1974241691 * ____this0, GraphNode_t23612370 * ___node1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.PathNode Pathfinding.QuadtreeNode::ilo_GetPathNode4(Pathfinding.PathHandler,Pathfinding.GraphNode)
extern "C"  PathNode_t417131581 * QuadtreeNode_ilo_GetPathNode4_m3494693517 (Il2CppObject * __this /* static, unused */, PathHandler_t918952263 * ____this0, GraphNode_t23612370 * ___node1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Pathfinding.QuadtreeNode::ilo_CalculateHScore5(Pathfinding.Path,Pathfinding.GraphNode)
extern "C"  uint32_t QuadtreeNode_ilo_CalculateHScore5_m4244992292 (Il2CppObject * __this /* static, unused */, Path_t1974241691 * ____this0, GraphNode_t23612370 * ___node1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Pathfinding.QuadtreeNode::ilo_GetTraversalCost6(Pathfinding.Path,Pathfinding.GraphNode)
extern "C"  uint32_t QuadtreeNode_ilo_GetTraversalCost6_m1686889778 (Il2CppObject * __this /* static, unused */, Path_t1974241691 * ____this0, GraphNode_t23612370 * ___node1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Pathfinding.QuadtreeNode::ilo_get_G7(Pathfinding.PathNode)
extern "C"  uint32_t QuadtreeNode_ilo_get_G7_m2236472560 (Il2CppObject * __this /* static, unused */, PathNode_t417131581 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.QuadtreeNode::ilo_ContainsConnection8(Pathfinding.GraphNode,Pathfinding.GraphNode)
extern "C"  bool QuadtreeNode_ilo_ContainsConnection8_m2930764662 (Il2CppObject * __this /* static, unused */, GraphNode_t23612370 * ____this0, GraphNode_t23612370 * ___node1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
