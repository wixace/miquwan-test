﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<MScrollView/MoveWay,System.Object>
struct Dictionary_2_t1442267592;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V3669068296.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<MScrollView/MoveWay,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m2320962993_gshared (Enumerator_t3669068296 * __this, Dictionary_2_t1442267592 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m2320962993(__this, ___host0, method) ((  void (*) (Enumerator_t3669068296 *, Dictionary_2_t1442267592 *, const MethodInfo*))Enumerator__ctor_m2320962993_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<MScrollView/MoveWay,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1446997712_gshared (Enumerator_t3669068296 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1446997712(__this, method) ((  Il2CppObject * (*) (Enumerator_t3669068296 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1446997712_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<MScrollView/MoveWay,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1043934820_gshared (Enumerator_t3669068296 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1043934820(__this, method) ((  void (*) (Enumerator_t3669068296 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1043934820_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<MScrollView/MoveWay,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m3466238611_gshared (Enumerator_t3669068296 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m3466238611(__this, method) ((  void (*) (Enumerator_t3669068296 *, const MethodInfo*))Enumerator_Dispose_m3466238611_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<MScrollView/MoveWay,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1332195024_gshared (Enumerator_t3669068296 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m1332195024(__this, method) ((  bool (*) (Enumerator_t3669068296 *, const MethodInfo*))Enumerator_MoveNext_m1332195024_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<MScrollView/MoveWay,System.Object>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m433719474_gshared (Enumerator_t3669068296 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m433719474(__this, method) ((  Il2CppObject * (*) (Enumerator_t3669068296 *, const MethodInfo*))Enumerator_get_Current_m433719474_gshared)(__this, method)
