﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._b545c3d8fff462e6ab058c3d4ad4d7c8
struct _b545c3d8fff462e6ab058c3d4ad4d7c8_t1229431059;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__b545c3d8fff462e6ab058c3d1229431059.h"

// System.Void Little._b545c3d8fff462e6ab058c3d4ad4d7c8::.ctor()
extern "C"  void _b545c3d8fff462e6ab058c3d4ad4d7c8__ctor_m1140066938 (_b545c3d8fff462e6ab058c3d4ad4d7c8_t1229431059 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._b545c3d8fff462e6ab058c3d4ad4d7c8::_b545c3d8fff462e6ab058c3d4ad4d7c8m2(System.Int32)
extern "C"  int32_t _b545c3d8fff462e6ab058c3d4ad4d7c8__b545c3d8fff462e6ab058c3d4ad4d7c8m2_m3699531961 (_b545c3d8fff462e6ab058c3d4ad4d7c8_t1229431059 * __this, int32_t ____b545c3d8fff462e6ab058c3d4ad4d7c8a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._b545c3d8fff462e6ab058c3d4ad4d7c8::_b545c3d8fff462e6ab058c3d4ad4d7c8m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _b545c3d8fff462e6ab058c3d4ad4d7c8__b545c3d8fff462e6ab058c3d4ad4d7c8m_m4076046877 (_b545c3d8fff462e6ab058c3d4ad4d7c8_t1229431059 * __this, int32_t ____b545c3d8fff462e6ab058c3d4ad4d7c8a0, int32_t ____b545c3d8fff462e6ab058c3d4ad4d7c8891, int32_t ____b545c3d8fff462e6ab058c3d4ad4d7c8c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._b545c3d8fff462e6ab058c3d4ad4d7c8::ilo__b545c3d8fff462e6ab058c3d4ad4d7c8m21(Little._b545c3d8fff462e6ab058c3d4ad4d7c8,System.Int32)
extern "C"  int32_t _b545c3d8fff462e6ab058c3d4ad4d7c8_ilo__b545c3d8fff462e6ab058c3d4ad4d7c8m21_m491856442 (Il2CppObject * __this /* static, unused */, _b545c3d8fff462e6ab058c3d4ad4d7c8_t1229431059 * ____this0, int32_t ____b545c3d8fff462e6ab058c3d4ad4d7c8a1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
