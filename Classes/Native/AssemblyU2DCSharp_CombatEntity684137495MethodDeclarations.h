﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CombatEntity
struct CombatEntity_t684137495;
// TargetMgr/AngleEntity
struct AngleEntity_t3194128142;
// OnPathDelegate
struct OnPathDelegate_t598607977;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.String
struct String_t;
// System.Collections.Generic.List`1<EnemyDropData>
struct List_1_t2836773265;
// System.Collections.Generic.List`1<AIObject>
struct List_1_t1405465687;
// AnimationRunner
struct AnimationRunner_t1015409588;
// FightCtrl
struct FightCtrl_t648967803;
// BuffCtrl
struct BuffCtrl_t2836564350;
// Entity.Behavior.IBehaviorCtrl
struct IBehaviorCtrl_t4225040900;
// UnityEngine.SkinnedMeshRenderer
struct SkinnedMeshRenderer_t3986041494;
// Blood
struct Blood_t64280026;
// PlotFightTalkMgr
struct PlotFightTalkMgr_t866599741;
// FS_ShadowSimple
struct FS_ShadowSimple_t4208748868;
// Skill
struct Skill_t79944241;
// UnityEngine.Collider
struct Collider_t2939674232;
// System.Action
struct Action_t3771233898;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// System.Object
struct Il2CppObject;
// skillCfg
struct skillCfg_t2142425171;
// UnitStateBase
struct UnitStateBase_t1040218558;
// Mihua.Asset.ABLoadOperation.AssetOperation
struct AssetOperation_t778728221;
// Pathfinding.Path
struct Path_t1974241691;
// AIPath
struct AIPath_t1930792045;
// GameMgr
struct GameMgr_t1469029542;
// GameQutilyCtr
struct GameQutilyCtr_t3963256169;
// GlobalGOMgr
struct GlobalGOMgr_t803081773;
// CSDatacfgManager
struct CSDatacfgManager_t1565254243;
// effectCfg
struct effectCfg_t2826279187;
// BloodMgr
struct BloodMgr_t3705885150;
// CSGameDataMgr
struct CSGameDataMgr_t2623305516;
// HeroMgr
struct HeroMgr_t2475965342;
// EffectMgr
struct EffectMgr_t535289511;
// CameraAnimationMgr
struct CameraAnimationMgr_t3311695833;
// CSCheckPointUnit
struct CSCheckPointUnit_t3129216924;
// ReplayMgr
struct ReplayMgr_t1549183121;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// LevelConfigManager
struct LevelConfigManager_t657947911;
// NpcMgr
struct NpcMgr_t2339534743;
// TeamSkillMgr
struct TeamSkillMgr_t2030650276;
// HeadUpBloodMgr
struct HeadUpBloodMgr_t2330866585;
// TargetMgr
struct TargetMgr_t1188374183;
// Hero
struct Hero_t2245658;
// SkillHurtShow
struct SkillHurtShow_t1728264445;
// System.Collections.Generic.List`1<CombatEntity>
struct List_1_t2052323047;
// CEvent.EventFunc`1<CEvent.ZEvent>
struct EventFunc_1_t1114914310;
// Mihua.Asset.OnLoadAsset
struct OnLoadAsset_t4181543125;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_TargetMgr_AngleEntity3194128142.h"
#include "AssemblyU2DCSharp_OnPathDelegate598607977.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_EntityEnum_UnitCamp642829979.h"
#include "AssemblyU2DCSharp_EntityEnum_ModelScaleTween3031370284.h"
#include "AssemblyU2DCSharp_EntityEnum_PetrifiedTween965315849.h"
#include "AssemblyU2DCSharp_HERO_TYPE820012127.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "AssemblyU2DCSharp_SEX_TYPE2342271891.h"
#include "AssemblyU2DCSharp_AnimationRunner1015409588.h"
#include "AssemblyU2DCSharp_EntityEnum_UnitTag299076696.h"
#include "AssemblyU2DCSharp_FightCtrl648967803.h"
#include "AssemblyU2DCSharp_BuffCtrl2836564350.h"
#include "AssemblyU2DCSharp_Entity_Behavior_IBehaviorCtrl4225040900.h"
#include "UnityEngine_UnityEngine_SkinnedMeshRenderer3986041494.h"
#include "AssemblyU2DCSharp_Blood64280026.h"
#include "AssemblyU2DCSharp_PlotFightTalkMgr866599741.h"
#include "AssemblyU2DCSharp_FS_ShadowSimple4208748868.h"
#include "AssemblyU2DCSharp_EntityEnum_UnitType643359572.h"
#include "AssemblyU2DCSharp_EntityEnum_DeadType1438946868.h"
#include "AssemblyU2DCSharp_CombatEntity684137495.h"
#include "System_Core_System_Action3771233898.h"
#include "AssemblyU2DCSharp_FightEnum_EDamageType2535357340.h"
#include "AssemblyU2DCSharp_HeadUpBloodMgr_LabelType4067300664.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_skillCfg2142425171.h"
#include "AssemblyU2DCSharp_HERO_ELEMENT2692579223.h"
#include "AssemblyU2DCSharp_UnitStateBase1040218558.h"
#include "AssemblyU2DCSharp_Mihua_Asset_ABLoadOperation_Asset778728221.h"
#include "AssemblyU2DCSharp_AIPath1930792045.h"
#include "AssemblyU2DCSharp_GameMgr1469029542.h"
#include "AssemblyU2DCSharp_CSDatacfgManager1565254243.h"
#include "AssemblyU2DCSharp_BloodMgr3705885150.h"
#include "AssemblyU2DCSharp_EffectMgr535289511.h"
#include "AssemblyU2DCSharp_CameraAnimationMgr3311695833.h"
#include "AssemblyU2DCSharp_ReplayMgr1549183121.h"
#include "AssemblyU2DCSharp_AnimationRunner_AniType1006238651.h"
#include "AssemblyU2DCSharp_ReplayExecuteType3106517448.h"
#include "AssemblyU2DCSharp_NpcMgr2339534743.h"
#include "AssemblyU2DCSharp_TeamSkillMgr2030650276.h"
#include "AssemblyU2DCSharp_HeadUpBloodMgr2330866585.h"
#include "AssemblyU2DCSharp_GameQutilyCtr3963256169.h"
#include "AssemblyU2DCSharp_TargetMgr1188374183.h"
#include "AssemblyU2DCSharp_HeroMgr2475965342.h"
#include "AssemblyU2DCSharp_Entity_Behavior_EBehaviorID1238533136.h"
#include "AssemblyU2DCSharp_SkillHurtShow1728264445.h"
#include "AssemblyU2DCSharp_Skill79944241.h"
#include "AssemblyU2DCSharp_BuffEnum_EBuffState2159802479.h"
#include "AssemblyU2DCSharp_Mihua_Asset_OnLoadAsset4181543125.h"

// System.Void CombatEntity::.ctor()
extern "C"  void CombatEntity__ctor_m1238990948 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::.cctor()
extern "C"  void CombatEntity__cctor_m3566884809 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_position(UnityEngine.Vector3)
extern "C"  void CombatEntity_set_position_m32616947 (CombatEntity_t684137495 * __this, Vector3_t4282066566  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 CombatEntity::get_position()
extern "C"  Vector3_t4282066566  CombatEntity_get_position_m1219180332 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_angleEntity(TargetMgr/AngleEntity)
extern "C"  void CombatEntity_set_angleEntity_m1849854441 (CombatEntity_t684137495 * __this, AngleEntity_t3194128142 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TargetMgr/AngleEntity CombatEntity::get_angleEntity()
extern "C"  AngleEntity_t3194128142 * CombatEntity_get_angleEntity_m1971727146 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntity::get_canMove()
extern "C"  bool CombatEntity_get_canMove_m51037062 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_canMove(System.Boolean)
extern "C"  void CombatEntity_set_canMove_m3968562109 (CombatEntity_t684137495 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::UpdateMove()
extern "C"  void CombatEntity_UpdateMove_m376668890 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::LookAt(UnityEngine.Vector3)
extern "C"  void CombatEntity_LookAt_m745547591 (CombatEntity_t684137495 * __this, Vector3_t4282066566  ___pos0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::Arrive(UnityEngine.Vector3)
extern "C"  void CombatEntity_Arrive_m3397188802 (CombatEntity_t684137495 * __this, Vector3_t4282066566  ___pos0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::RequestAstarPath(UnityEngine.Vector3,OnPathDelegate)
extern "C"  void CombatEntity_RequestAstarPath_m885986829 (CombatEntity_t684137495 * __this, Vector3_t4282066566  ___pos0, OnPathDelegate_t598607977 * ___onPathComplete1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::RemovePathCallback(OnPathDelegate)
extern "C"  void CombatEntity_RemovePathCallback_m2600993989 (CombatEntity_t684137495 * __this, OnPathDelegate_t598607977 * ___onPathComplete0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::UpdateYPos()
extern "C"  void CombatEntity_UpdateYPos_m691394468 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject CombatEntity::get_moveTargetRoot()
extern "C"  GameObject_t3674682005 * CombatEntity_get_moveTargetRoot_m2990423298 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_showname(System.String)
extern "C"  void CombatEntity_set_showname_m1216802013 (CombatEntity_t684137495 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String CombatEntity::get_showname()
extern "C"  String_t* CombatEntity_get_showname_m3943419508 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_unitCamp(EntityEnum.UnitCamp)
extern "C"  void CombatEntity_set_unitCamp_m1106201121 (CombatEntity_t684137495 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// EntityEnum.UnitCamp CombatEntity::get_unitCamp()
extern "C"  int32_t CombatEntity_get_unitCamp_m2845254098 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_modelScaleTween(EntityEnum.ModelScaleTween)
extern "C"  void CombatEntity_set_modelScaleTween_m1880523183 (CombatEntity_t684137495 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// EntityEnum.ModelScaleTween CombatEntity::get_modelScaleTween()
extern "C"  int32_t CombatEntity_get_modelScaleTween_m1197095494 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_petrifiedTween(EntityEnum.PetrifiedTween)
extern "C"  void CombatEntity_set_petrifiedTween_m2040881797 (CombatEntity_t684137495 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// EntityEnum.PetrifiedTween CombatEntity::get_petrifiedTween()
extern "C"  int32_t CombatEntity_get_petrifiedTween_m2721881554 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_playerId(System.UInt32)
extern "C"  void CombatEntity_set_playerId_m1550097281 (CombatEntity_t684137495 * __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 CombatEntity::get_playerId()
extern "C"  uint32_t CombatEntity_get_playerId_m2196404288 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_id(System.Int32)
extern "C"  void CombatEntity_set_id_m3867606505 (CombatEntity_t684137495 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntity::get_id()
extern "C"  int32_t CombatEntity_get_id_m1278563762 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_icon(System.String)
extern "C"  void CombatEntity_set_icon_m2524041548 (CombatEntity_t684137495 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String CombatEntity::get_icon()
extern "C"  String_t* CombatEntity_get_icon_m2513096997 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_largeIcon(System.String)
extern "C"  void CombatEntity_set_largeIcon_m341080585 (CombatEntity_t684137495 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String CombatEntity::get_largeIcon()
extern "C"  String_t* CombatEntity_get_largeIcon_m4015114058 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_bossshow(System.String)
extern "C"  void CombatEntity_set_bossshow_m2124858299 (CombatEntity_t684137495 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String CombatEntity::get_bossshow()
extern "C"  String_t* CombatEntity_get_bossshow_m2497849046 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_headicon(System.String)
extern "C"  void CombatEntity_set_headicon_m3082302828 (CombatEntity_t684137495 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String CombatEntity::get_headicon()
extern "C"  String_t* CombatEntity_get_headicon_m189057413 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_IntelligenceTip(System.Int32)
extern "C"  void CombatEntity_set_IntelligenceTip_m4101890898 (CombatEntity_t684137495 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntity::get_IntelligenceTip()
extern "C"  int32_t CombatEntity_get_IntelligenceTip_m2066716839 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_scale(System.Single)
extern "C"  void CombatEntity_set_scale_m1120476412 (CombatEntity_t684137495 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntity::get_scale()
extern "C"  float CombatEntity_get_scale_m2754679991 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_isSummonMonster(System.Boolean)
extern "C"  void CombatEntity_set_isSummonMonster_m3275420331 (CombatEntity_t684137495 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntity::get_isSummonMonster()
extern "C"  bool CombatEntity_get_isSummonMonster_m2330467060 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_halfwidth(System.Single)
extern "C"  void CombatEntity_set_halfwidth_m3938032563 (CombatEntity_t684137495 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntity::get_halfwidth()
extern "C"  float CombatEntity_get_halfwidth_m325969056 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_heromajor(HERO_TYPE)
extern "C"  void CombatEntity_set_heromajor_m2499910575 (CombatEntity_t684137495 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HERO_TYPE CombatEntity::get_heromajor()
extern "C"  int32_t CombatEntity_get_heromajor_m1835878948 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_atkEffDelayTime(System.Single)
extern "C"  void CombatEntity_set_atkEffDelayTime_m3758890723 (CombatEntity_t684137495 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntity::get_atkEffDelayTime()
extern "C"  float CombatEntity_get_atkEffDelayTime_m4041250160 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_skills(System.String)
extern "C"  void CombatEntity_set_skills_m187408739 (CombatEntity_t684137495 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String CombatEntity::get_skills()
extern "C"  String_t* CombatEntity_get_skills_m4196366318 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_awakenLv(System.Int32)
extern "C"  void CombatEntity_set_awakenLv_m3689315521 (CombatEntity_t684137495 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntity::get_awakenLv()
extern "C"  int32_t CombatEntity_get_awakenLv_m2684984586 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_lucencyWait(System.Single)
extern "C"  void CombatEntity_set_lucencyWait_m3116708312 (CombatEntity_t684137495 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntity::get_lucencyWait()
extern "C"  float CombatEntity_get_lucencyWait_m2459167835 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_islucency(System.Int32)
extern "C"  void CombatEntity_set_islucency_m4269397317 (CombatEntity_t684137495 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntity::get_islucency()
extern "C"  int32_t CombatEntity_get_islucency_m764801946 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_lucencytime(System.Single)
extern "C"  void CombatEntity_set_lucencytime_m901565856 (CombatEntity_t684137495 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntity::get_lucencytime()
extern "C"  float CombatEntity_get_lucencytime_m3296906131 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_elite(System.Boolean)
extern "C"  void CombatEntity_set_elite_m1979269007 (CombatEntity_t684137495 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntity::get_elite()
extern "C"  bool CombatEntity_get_elite_m3315562200 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_isAim(System.Boolean)
extern "C"  void CombatEntity_set_isAim_m3996443831 (CombatEntity_t684137495 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntity::get_isAim()
extern "C"  bool CombatEntity_get_isAim_m2733752832 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_dutytype(System.Int32)
extern "C"  void CombatEntity_set_dutytype_m2204365342 (CombatEntity_t684137495 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntity::get_dutytype()
extern "C"  int32_t CombatEntity_get_dutytype_m149845479 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_ifattacked(System.Int32)
extern "C"  void CombatEntity_set_ifattacked_m2458817042 (CombatEntity_t684137495 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntity::get_ifattacked()
extern "C"  int32_t CombatEntity_get_ifattacked_m3793577307 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_dropRwards(System.Collections.Generic.List`1<EnemyDropData>)
extern "C"  void CombatEntity_set_dropRwards_m2519983446 (CombatEntity_t684137495 * __this, List_1_t2836773265 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<EnemyDropData> CombatEntity::get_dropRwards()
extern "C"  List_1_t2836773265 * CombatEntity_get_dropRwards_m3731288863 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_storyId(System.String)
extern "C"  void CombatEntity_set_storyId_m1550174957 (CombatEntity_t684137495 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String CombatEntity::get_storyId()
extern "C"  String_t* CombatEntity_get_storyId_m956402726 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_elementEff(System.Int32)
extern "C"  void CombatEntity_set_elementEff_m2860993431 (CombatEntity_t684137495 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntity::get_elementEff()
extern "C"  int32_t CombatEntity_get_elementEff_m3093593696 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_atkedEff(System.Int32)
extern "C"  void CombatEntity_set_atkedEff_m2344822204 (CombatEntity_t684137495 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntity::get_atkedEff()
extern "C"  int32_t CombatEntity_get_atkedEff_m2964825733 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_deathEffect(System.Int32)
extern "C"  void CombatEntity_set_deathEffect_m1269568891 (CombatEntity_t684137495 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntity::get_deathEffect()
extern "C"  int32_t CombatEntity_get_deathEffect_m4154688336 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_taixudeatheffect(System.Int32)
extern "C"  void CombatEntity_set_taixudeatheffect_m1899123162 (CombatEntity_t684137495 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntity::get_taixudeatheffect()
extern "C"  int32_t CombatEntity_get_taixudeatheffect_m2869908899 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_reviveEffectId(System.Int32)
extern "C"  void CombatEntity_set_reviveEffectId_m485132111 (CombatEntity_t684137495 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntity::get_reviveEffectId()
extern "C"  int32_t CombatEntity_get_reviveEffectId_m3732435224 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_appeareffect(System.Int32)
extern "C"  void CombatEntity_set_appeareffect_m2751658484 (CombatEntity_t684137495 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntity::get_appeareffect()
extern "C"  int32_t CombatEntity_get_appeareffect_m1288024509 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_startPos(UnityEngine.Vector3)
extern "C"  void CombatEntity_set_startPos_m1063056426 (CombatEntity_t684137495 * __this, Vector3_t4282066566  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 CombatEntity::get_startPos()
extern "C"  Vector3_t4282066566  CombatEntity_get_startPos_m2545969749 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_startRotation(UnityEngine.Quaternion)
extern "C"  void CombatEntity_set_startRotation_m1848156752 (CombatEntity_t684137495 * __this, Quaternion_t1553702882  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion CombatEntity::get_startRotation()
extern "C"  Quaternion_t1553702882  CombatEntity_get_startRotation_m3754573105 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_atkDis(System.Single)
extern "C"  void CombatEntity_set_atkDis_m749215480 (CombatEntity_t684137495 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntity::get_atkDis()
extern "C"  float CombatEntity_get_atkDis_m642121451 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_viewDis(System.Single)
extern "C"  void CombatEntity_set_viewDis_m3841673661 (CombatEntity_t684137495 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntity::get_viewDis()
extern "C"  float CombatEntity_get_viewDis_m1828649046 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_originSpeed(System.Single)
extern "C"  void CombatEntity_set_originSpeed_m54698917 (CombatEntity_t684137495 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntity::get_originSpeed()
extern "C"  float CombatEntity_get_originSpeed_m2399179118 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_canAttack(System.Boolean)
extern "C"  void CombatEntity_set_canAttack_m2641943348 (CombatEntity_t684137495 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntity::get_canAttack()
extern "C"  bool CombatEntity_get_canAttack_m2446509885 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_isStun(System.Boolean)
extern "C"  void CombatEntity_set_isStun_m1705353208 (CombatEntity_t684137495 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntity::get_isStun()
extern "C"  bool CombatEntity_get_isStun_m3667748225 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_isDeath(System.Boolean)
extern "C"  void CombatEntity_set_isDeath_m4005067110 (CombatEntity_t684137495 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntity::get_isDeath()
extern "C"  bool CombatEntity_get_isDeath_m1155724527 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_isRevive(System.Boolean)
extern "C"  void CombatEntity_set_isRevive_m2694344979 (CombatEntity_t684137495 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntity::get_isRevive()
extern "C"  bool CombatEntity_get_isRevive_m688792028 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_isFend(System.Boolean)
extern "C"  void CombatEntity_set_isFend_m248653811 (CombatEntity_t684137495 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntity::get_isFend()
extern "C"  bool CombatEntity_get_isFend_m3281498300 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_isKnockingUp(System.Boolean)
extern "C"  void CombatEntity_set_isKnockingUp_m759116423 (CombatEntity_t684137495 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntity::get_isKnockingUp()
extern "C"  bool CombatEntity_get_isKnockingUp_m2243239504 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_isKnockingDown(System.Boolean)
extern "C"  void CombatEntity_set_isKnockingDown_m3009863054 (CombatEntity_t684137495 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntity::get_isKnockingDown()
extern "C"  bool CombatEntity_get_isKnockingDown_m3489350039 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_isMovement(System.Boolean)
extern "C"  void CombatEntity_set_isMovement_m2189581037 (CombatEntity_t684137495 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntity::get_isMovement()
extern "C"  bool CombatEntity_get_isMovement_m1081516342 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_isFlame(System.Boolean)
extern "C"  void CombatEntity_set_isFlame_m190609445 (CombatEntity_t684137495 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntity::get_isFlame()
extern "C"  bool CombatEntity_get_isFlame_m3130924526 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_isNotSelected(System.Boolean)
extern "C"  void CombatEntity_set_isNotSelected_m1535237632 (CombatEntity_t684137495 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntity::get_isNotSelected()
extern "C"  bool CombatEntity_get_isNotSelected_m2228748041 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_IsCheckTarget(System.Boolean)
extern "C"  void CombatEntity_set_IsCheckTarget_m3372971531 (CombatEntity_t684137495 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntity::get_IsCheckTarget()
extern "C"  bool CombatEntity_get_IsCheckTarget_m1625667156 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_IsAutoMove(System.Boolean)
extern "C"  void CombatEntity_set_IsAutoMove_m610603646 (CombatEntity_t684137495 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntity::get_IsAutoMove()
extern "C"  bool CombatEntity_get_IsAutoMove_m1536981639 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_IsMarshalMoving(System.Boolean)
extern "C"  void CombatEntity_set_IsMarshalMoving_m2608093790 (CombatEntity_t684137495 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntity::get_IsMarshalMoving()
extern "C"  bool CombatEntity_get_IsMarshalMoving_m1942005735 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_RvoRadius(System.Single)
extern "C"  void CombatEntity_set_RvoRadius_m3704775913 (CombatEntity_t684137495 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntity::get_RvoRadius()
extern "C"  float CombatEntity_get_RvoRadius_m1559780266 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_IsBirthEffect(System.Boolean)
extern "C"  void CombatEntity_set_IsBirthEffect_m2191891074 (CombatEntity_t684137495 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntity::get_IsBirthEffect()
extern "C"  bool CombatEntity_get_IsBirthEffect_m1668998923 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_starAttackTime(System.Single)
extern "C"  void CombatEntity_set_starAttackTime_m2448950407 (CombatEntity_t684137495 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntity::get_starAttackTime()
extern "C"  float CombatEntity_get_starAttackTime_m719742204 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_isAttack(System.Boolean)
extern "C"  void CombatEntity_set_isAttack_m2134777862 (CombatEntity_t684137495 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntity::get_isAttack()
extern "C"  bool CombatEntity_get_isAttack_m1488233999 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_canFight(System.Boolean)
extern "C"  void CombatEntity_set_canFight_m1285160468 (CombatEntity_t684137495 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntity::get_canFight()
extern "C"  bool CombatEntity_get_canFight_m3774092445 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_waistPoint(UnityEngine.GameObject)
extern "C"  void CombatEntity_set_waistPoint_m1739790821 (CombatEntity_t684137495 * __this, GameObject_t3674682005 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject CombatEntity::get_waistPoint()
extern "C"  GameObject_t3674682005 * CombatEntity_get_waistPoint_m211780526 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_InitPosition(UnityEngine.Vector3)
extern "C"  void CombatEntity_set_InitPosition_m3402126499 (CombatEntity_t684137495 * __this, Vector3_t4282066566  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 CombatEntity::get_InitPosition()
extern "C"  Vector3_t4282066566  CombatEntity_get_InitPosition_m32554876 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_UINamePos(System.Int32)
extern "C"  void CombatEntity_set_UINamePos_m2592698347 (CombatEntity_t684137495 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntity::get_UINamePos()
extern "C"  int32_t CombatEntity_get_UINamePos_m158062400 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_headBloodPos(System.Single)
extern "C"  void CombatEntity_set_headBloodPos_m1363119700 (CombatEntity_t684137495 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntity::get_headBloodPos()
extern "C"  float CombatEntity_get_headBloodPos_m3494471695 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_sexType(SEX_TYPE)
extern "C"  void CombatEntity_set_sexType_m662453458 (CombatEntity_t684137495 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SEX_TYPE CombatEntity::get_sexType()
extern "C"  int32_t CombatEntity_get_sexType_m2571799087 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_AIDelayList(System.Collections.Generic.List`1<AIObject>)
extern "C"  void CombatEntity_set_AIDelayList_m1827998395 (CombatEntity_t684137495 * __this, List_1_t1405465687 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<AIObject> CombatEntity::get_AIDelayList()
extern "C"  List_1_t1405465687 * CombatEntity_get_AIDelayList_m3823568792 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::AddDelayId(System.UInt32)
extern "C"  void CombatEntity_AddDelayId_m290657309 (CombatEntity_t684137495 * __this, uint32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_initBehavior(System.Int32)
extern "C"  void CombatEntity_set_initBehavior_m1321910544 (CombatEntity_t684137495 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntity::get_initBehavior()
extern "C"  int32_t CombatEntity_get_initBehavior_m814367449 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_characterAnim(AnimationRunner)
extern "C"  void CombatEntity_set_characterAnim_m1983510463 (CombatEntity_t684137495 * __this, AnimationRunner_t1015409588 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// AnimationRunner CombatEntity::get_characterAnim()
extern "C"  AnimationRunner_t1015409588 * CombatEntity_get_characterAnim_m1661418132 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_isSkillFrozen(System.Boolean)
extern "C"  void CombatEntity_set_isSkillFrozen_m3896672707 (CombatEntity_t684137495 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntity::get_isSkillFrozen()
extern "C"  bool CombatEntity_get_isSkillFrozen_m3941278732 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_isFrozen(System.Boolean)
extern "C"  void CombatEntity_set_isFrozen_m289707102 (CombatEntity_t684137495 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntity::get_isFrozen()
extern "C"  bool CombatEntity_get_isFrozen_m4012315751 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_totalDamage(System.Single)
extern "C"  void CombatEntity_set_totalDamage_m3418977875 (CombatEntity_t684137495 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntity::get_totalDamage()
extern "C"  float CombatEntity_get_totalDamage_m1572011008 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_atkUnitTag(EntityEnum.UnitTag)
extern "C"  void CombatEntity_set_atkUnitTag_m3742963311 (CombatEntity_t684137495 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// EntityEnum.UnitTag CombatEntity::get_atkUnitTag()
extern "C"  int32_t CombatEntity_get_atkUnitTag_m2289676088 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_unitTag(EntityEnum.UnitTag)
extern "C"  void CombatEntity_set_unitTag_m2616872239 (CombatEntity_t684137495 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// EntityEnum.UnitTag CombatEntity::get_unitTag()
extern "C"  int32_t CombatEntity_get_unitTag_m2041358750 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_fightCtrl(FightCtrl)
extern "C"  void CombatEntity_set_fightCtrl_m750882551 (CombatEntity_t684137495 * __this, FightCtrl_t648967803 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FightCtrl CombatEntity::get_fightCtrl()
extern "C"  FightCtrl_t648967803 * CombatEntity_get_fightCtrl_m523342044 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_buffCtrl(BuffCtrl)
extern "C"  void CombatEntity_set_buffCtrl_m3479844701 (CombatEntity_t684137495 * __this, BuffCtrl_t2836564350 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// BuffCtrl CombatEntity::get_buffCtrl()
extern "C"  BuffCtrl_t2836564350 * CombatEntity_get_buffCtrl_m1447829030 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_bvrCtrl(Entity.Behavior.IBehaviorCtrl)
extern "C"  void CombatEntity_set_bvrCtrl_m3164807503 (CombatEntity_t684137495 * __this, IBehaviorCtrl_t4225040900 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Entity.Behavior.IBehaviorCtrl CombatEntity::get_bvrCtrl()
extern "C"  IBehaviorCtrl_t4225040900 * CombatEntity_get_bvrCtrl_m396129668 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_skinMeshRender(UnityEngine.SkinnedMeshRenderer)
extern "C"  void CombatEntity_set_skinMeshRender_m2754445612 (CombatEntity_t684137495 * __this, SkinnedMeshRenderer_t3986041494 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.SkinnedMeshRenderer CombatEntity::get_skinMeshRender()
extern "C"  SkinnedMeshRenderer_t3986041494 * CombatEntity_get_skinMeshRender_m2173801331 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_blood(Blood)
extern "C"  void CombatEntity_set_blood_m3984117177 (CombatEntity_t684137495 * __this, Blood_t64280026 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Blood CombatEntity::get_blood()
extern "C"  Blood_t64280026 * CombatEntity_get_blood_m107603354 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_PlotFightMgr(PlotFightTalkMgr)
extern "C"  void CombatEntity_set_PlotFightMgr_m2271505257 (CombatEntity_t684137495 * __this, PlotFightTalkMgr_t866599741 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PlotFightTalkMgr CombatEntity::get_PlotFightMgr()
extern "C"  PlotFightTalkMgr_t866599741 * CombatEntity_get_PlotFightMgr_m2296120242 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_shadow(FS_ShadowSimple)
extern "C"  void CombatEntity_set_shadow_m2961589169 (CombatEntity_t684137495 * __this, FS_ShadowSimple_t4208748868 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FS_ShadowSimple CombatEntity::get_shadow()
extern "C"  FS_ShadowSimple_t4208748868 * CombatEntity_get_shadow_m495988088 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_unitType(EntityEnum.UnitType)
extern "C"  void CombatEntity_set_unitType_m3239712111 (CombatEntity_t684137495 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// EntityEnum.UnitType CombatEntity::get_unitType()
extern "C"  int32_t CombatEntity_get_unitType_m521494066 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_deadType(EntityEnum.DeadType)
extern "C"  void CombatEntity_set_deadType_m1633654703 (CombatEntity_t684137495 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// EntityEnum.DeadType CombatEntity::get_deadType()
extern "C"  int32_t CombatEntity_get_deadType_m3040842290 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntity::get_Radius()
extern "C"  float CombatEntity_get_Radius_m421193159 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_atkTarget(CombatEntity)
extern "C"  void CombatEntity_set_atkTarget_m517546903 (CombatEntity_t684137495 * __this, CombatEntity_t684137495 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CombatEntity CombatEntity::get_atkTarget()
extern "C"  CombatEntity_t684137495 * CombatEntity_get_atkTarget_m3349982716 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntity::VectorAngle(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  float CombatEntity_VectorAngle_m3200479846 (CombatEntity_t684137495 * __this, Vector3_t4282066566  ___from0, Vector3_t4282066566  ___to1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 CombatEntity::getAtkpos(CombatEntity)
extern "C"  Vector3_t4282066566  CombatEntity_getAtkpos_m2194108771 (CombatEntity_t684137495 * __this, CombatEntity_t684137495 * ___killer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntity::getposid(System.Int32)
extern "C"  int32_t CombatEntity_getposid_m768103992 (CombatEntity_t684137495 * __this, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Skill CombatEntity::GetActiveSkill()
extern "C"  Skill_t79944241 * CombatEntity_GetActiveSkill_m2834049623 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::Start()
extern "C"  void CombatEntity_Start_m186128740 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::Init()
extern "C"  void CombatEntity_Init_m1238430064 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::SetViewModel(System.Boolean)
extern "C"  void CombatEntity_SetViewModel_m3795823641 (CombatEntity_t684137495 * __this, bool ___isShow0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::OnTriggerFun(System.Int32)
extern "C"  void CombatEntity_OnTriggerFun_m4182786071 (CombatEntity_t684137495 * __this, int32_t ___effectId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::AddPetBuff()
extern "C"  void CombatEntity_AddPetBuff_m2585963281 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::AddBuffOnShiliFuben()
extern "C"  void CombatEntity_AddBuffOnShiliFuben_m2650963168 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::OnViewModel()
extern "C"  void CombatEntity_OnViewModel_m740763623 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::AddPowerBuff(System.Int32,System.Int32)
extern "C"  void CombatEntity_AddPowerBuff_m3215192079 (CombatEntity_t684137495 * __this, int32_t ___buff0, int32_t ___allConfigId1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::LoopAddBuff(System.Int32,System.Int32)
extern "C"  void CombatEntity_LoopAddBuff_m4203211092 (CombatEntity_t684137495 * __this, int32_t ___buffId0, int32_t ___delaytime1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::AddArenaBuff(System.Int32)
extern "C"  void CombatEntity_AddArenaBuff_m4023930698 (CombatEntity_t684137495 * __this, int32_t ___buffId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::Update()
extern "C"  void CombatEntity_Update_m1480875817 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider CombatEntity::get_boxCollider()
extern "C"  Collider_t2939674232 * CombatEntity_get_boxCollider_m205580198 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 CombatEntity::GetAreaPoint(UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  CombatEntity_GetAreaPoint_m2097171858 (CombatEntity_t684137495 * __this, Vector3_t4282066566  ___pos0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::UpdateTargetPos(UnityEngine.Vector3,System.Action)
extern "C"  void CombatEntity_UpdateTargetPos_m2625036372 (CombatEntity_t684137495 * __this, Vector3_t4282066566  ___targetPos0, Action_t3771233898 * ___OnTargetCall1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::OnTargetReached()
extern "C"  void CombatEntity_OnTargetReached_m1113289380 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::LookAtTarget(UnityEngine.Vector3)
extern "C"  void CombatEntity_LookAtTarget_m656403190 (CombatEntity_t684137495 * __this, Vector3_t4282066566  ___targetPos0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::StopMove(System.Boolean)
extern "C"  void CombatEntity_StopMove_m720115530 (CombatEntity_t684137495 * __this, bool ___isPlayAnim0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::OnCheckTarget()
extern "C"  void CombatEntity_OnCheckTarget_m3496790876 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::DiscreteBuffUpdate()
extern "C"  void CombatEntity_DiscreteBuffUpdate_m4262778805 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::lucency()
extern "C"  void CombatEntity_lucency_m3681304827 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::OnUseingSkill(System.Int32)
extern "C"  void CombatEntity_OnUseingSkill_m1036386634 (CombatEntity_t684137495 * __this, int32_t ___skillID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::OnLevelTime()
extern "C"  void CombatEntity_OnLevelTime_m1323364756 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::OnAttckTime()
extern "C"  void CombatEntity_OnAttckTime_m204042521 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::OnDelayTime()
extern "C"  void CombatEntity_OnDelayTime_m3980088403 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::UpdateRunEff()
extern "C"  void CombatEntity_UpdateRunEff_m2099043363 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::SearchAtkTarget()
extern "C"  void CombatEntity_SearchAtkTarget_m4074879363 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::CalcuFinalProp()
extern "C"  void CombatEntity_CalcuFinalProp_m4257978489 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::AfterAttack()
extern "C"  void CombatEntity_AfterAttack_m981535494 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::AddRage(System.Single)
extern "C"  void CombatEntity_AddRage_m2609579611 (CombatEntity_t684137495 * __this, float ___delta0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::BeAttacked(System.Int32,CombatEntity,FightEnum.EDamageType,System.Int32)
extern "C"  void CombatEntity_BeAttacked_m2355254728 (CombatEntity_t684137495 * __this, int32_t ___hp0, CombatEntity_t684137495 * ___src1, int32_t ___damageType2, int32_t ___skillID3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::BreakSuperArmor()
extern "C"  void CombatEntity_BreakSuperArmor_m3387163749 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::atkedEffRefresh()
extern "C"  void CombatEntity_atkedEffRefresh_m469247215 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::normolPalyAttacked(FightEnum.EDamageType)
extern "C"  void CombatEntity_normolPalyAttacked_m3677284227 (CombatEntity_t684137495 * __this, int32_t ___damageType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::BreakSkillPalyAttacked(FightEnum.EDamageType)
extern "C"  void CombatEntity_BreakSkillPalyAttacked_m1250503242 (CombatEntity_t684137495 * __this, int32_t ___damageType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::ShootEffect()
extern "C"  void CombatEntity_ShootEffect_m2102178642 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::FendMove()
extern "C"  void CombatEntity_FendMove_m353209222 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::AddHP(System.Single)
extern "C"  void CombatEntity_AddHP_m2548769504 (CombatEntity_t684137495 * __this, float ___delta0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::Addhurt(System.Single)
extern "C"  void CombatEntity_Addhurt_m1382916473 (CombatEntity_t684137495 * __this, float ___delta0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::ReduceHP(System.Single,CombatEntity,FightEnum.EDamageType,System.Boolean,System.Boolean,System.Int32,System.Int32)
extern "C"  void CombatEntity_ReduceHP_m3211510285 (CombatEntity_t684137495 * __this, float ___delta0, CombatEntity_t684137495 * ___src1, int32_t ___damageType2, bool ___isRepaly3, bool ___isActionSkill4, int32_t ___neardeathprotect5, int32_t ___buffId6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::UpdateHpBar()
extern "C"  void CombatEntity_UpdateHpBar_m2924807908 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::PlayHpTextEffect(System.Single,HeadUpBloodMgr/LabelType)
extern "C"  void CombatEntity_PlayHpTextEffect_m2549065085 (CombatEntity_t684137495 * __this, float ___delta0, int32_t ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::PlayHpTextEffect(System.String,HeadUpBloodMgr/LabelType,System.Int32)
extern "C"  void CombatEntity_PlayHpTextEffect_m4215795345 (CombatEntity_t684137495 * __this, String_t* ___delta0, int32_t ___type1, int32_t ___index2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::AIMonsterDead(System.Int32)
extern "C"  void CombatEntity_AIMonsterDead_m1364214409 (CombatEntity_t684137495 * __this, int32_t ___murderID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::AIBeAttacked()
extern "C"  void CombatEntity_AIBeAttacked_m1852567890 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::ATKDEBUFF()
extern "C"  void CombatEntity_ATKDEBUFF_m871399150 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::AGAINATTACK()
extern "C"  void CombatEntity_AGAINATTACK_m999423082 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::AIOtherDead(CEvent.ZEvent)
extern "C"  void CombatEntity_AIOtherDead_m982758669 (CombatEntity_t684137495 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::Answer(CEvent.ZEvent)
extern "C"  void CombatEntity_Answer_m4222513245 (CombatEntity_t684137495 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::ClosePlot(CEvent.ZEvent)
extern "C"  void CombatEntity_ClosePlot_m3111450272 (CombatEntity_t684137495 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::MoceCameraEnd(CEvent.ZEvent)
extern "C"  void CombatEntity_MoceCameraEnd_m3956267687 (CombatEntity_t684137495 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::OnDestroy()
extern "C"  void CombatEntity_OnDestroy_m1449952093 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::ChangeShader()
extern "C"  void CombatEntity_ChangeShader_m3689913301 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::RefreshShader()
extern "C"  void CombatEntity_RefreshShader_m1259655970 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::RefreshShaderProperty(System.String,System.Object)
extern "C"  void CombatEntity_RefreshShaderProperty_m605861145 (CombatEntity_t684137495 * __this, String_t* ___key0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::ReplayDead()
extern "C"  void CombatEntity_ReplayDead_m3359714123 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::Dead()
extern "C"  void CombatEntity_Dead_m1086718916 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::WinDead()
extern "C"  void CombatEntity_WinDead_m3763707810 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::Suicide()
extern "C"  void CombatEntity_Suicide_m2591215600 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::Clear()
extern "C"  void CombatEntity_Clear_m2940091535 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::FocusAttack(CombatEntity)
extern "C"  void CombatEntity_FocusAttack_m1864632139 (CombatEntity_t684137495 * __this, CombatEntity_t684137495 * ___focus0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::SetRunSpeed(System.Single)
extern "C"  void CombatEntity_SetRunSpeed_m2438208875 (CombatEntity_t684137495 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::SetModelScale()
extern "C"  void CombatEntity_SetModelScale_m681487717 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::UpdateScale()
extern "C"  void CombatEntity_UpdateScale_m3754179203 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::SetAttr(System.String,System.Single)
extern "C"  void CombatEntity_SetAttr_m1973021522 (CombatEntity_t684137495 * __this, String_t* ___key0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::OpenTaunt()
extern "C"  void CombatEntity_OpenTaunt_m2056832838 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::CloseTaunt()
extern "C"  void CombatEntity_CloseTaunt_m2907783158 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::SetNotSelected(System.Boolean)
extern "C"  void CombatEntity_SetNotSelected_m4221412323 (CombatEntity_t684137495 * __this, bool ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::SetHidePart(System.Int32)
extern "C"  void CombatEntity_SetHidePart_m3400073642 (CombatEntity_t684137495 * __this, int32_t ___hide0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::Revive()
extern "C"  void CombatEntity_Revive_m4192545973 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntity::TargetBuffCheck(System.Int32)
extern "C"  bool CombatEntity_TargetBuffCheck_m1130063427 (CombatEntity_t684137495 * __this, int32_t ___buffID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntity::GetSlotValue()
extern "C"  int32_t CombatEntity_GetSlotValue_m2660415115 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::OneSecTick()
extern "C"  void CombatEntity_OneSecTick_m581064552 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::OneSecAddHP()
extern "C"  void CombatEntity_OneSecAddHP_m1007204704 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::OneSecReduceHP()
extern "C"  void CombatEntity_OneSecReduceHP_m2024840569 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::OneSecAuraAddHP()
extern "C"  void CombatEntity_OneSecAuraAddHP_m4130317181 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::OneSecAuraReduceHP()
extern "C"  void CombatEntity_OneSecAuraReduceHP_m792109628 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::EnterPetrified()
extern "C"  void CombatEntity_EnterPetrified_m194989792 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::ExitPetrified()
extern "C"  void CombatEntity_ExitPetrified_m2344240060 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::EnterTankState()
extern "C"  void CombatEntity_EnterTankState_m3371850671 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::ExitTankState()
extern "C"  void CombatEntity_ExitTankState_m1226133643 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::TankState(CEvent.ZEvent)
extern "C"  void CombatEntity_TankState_m3862282770 (CombatEntity_t684137495 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::AllyAddBuff(System.Int32)
extern "C"  void CombatEntity_AllyAddBuff_m473855119 (CombatEntity_t684137495 * __this, int32_t ___buffID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::EnterCleave()
extern "C"  void CombatEntity_EnterCleave_m1092266734 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::ExitCleave()
extern "C"  void CombatEntity_ExitCleave_m3244508050 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::OnCleave(CEvent.ZEvent)
extern "C"  void CombatEntity_OnCleave_m1456121928 (CombatEntity_t684137495 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::UpdatePetrified()
extern "C"  void CombatEntity_UpdatePetrified_m1906835185 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::EnterGodDown()
extern "C"  void CombatEntity_EnterGodDown_m70500646 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::ExitGodDown()
extern "C"  void CombatEntity_ExitGodDown_m2365472002 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::UpdateGodDown()
extern "C"  void CombatEntity_UpdateGodDown_m1176191351 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::SetDistortion(System.Boolean,System.Single)
extern "C"  void CombatEntity_SetDistortion_m1849497597 (CombatEntity_t684137495 * __this, bool ___isShow0, float ___delayTime1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::SetDistortion(System.Boolean)
extern "C"  void CombatEntity_SetDistortion_m2029311768 (CombatEntity_t684137495 * __this, bool ___isShow0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::BeAttackedBuffAction(CombatEntity,skillCfg)
extern "C"  void CombatEntity_BeAttackedBuffAction_m3539652951 (CombatEntity_t684137495 * __this, CombatEntity_t684137495 * ___src0, skillCfg_t2142425171 * ___cfg1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::RuneAction(CombatEntity,skillCfg)
extern "C"  void CombatEntity_RuneAction_m867441210 (CombatEntity_t684137495 * __this, CombatEntity_t684137495 * ___src0, skillCfg_t2142425171 * ___cfg1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_viewLevel(System.Int32)
extern "C"  void CombatEntity_set_viewLevel_m920242101 (CombatEntity_t684137495 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntity::get_viewLevel()
extern "C"  int32_t CombatEntity_get_viewLevel_m872025610 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_viewGrade(System.Int32)
extern "C"  void CombatEntity_set_viewGrade_m963469000 (CombatEntity_t684137495 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntity::get_viewGrade()
extern "C"  int32_t CombatEntity_get_viewGrade_m1082223005 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_level(System.Int32)
extern "C"  void CombatEntity_set_level_m1051431066 (CombatEntity_t684137495 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntity::get_level()
extern "C"  int32_t CombatEntity_get_level_m393152879 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_atkType(System.Int32)
extern "C"  void CombatEntity_set_atkType_m3913272712 (CombatEntity_t684137495 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntity::get_atkType()
extern "C"  int32_t CombatEntity_get_atkType_m1292184029 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_damageType(System.Int32)
extern "C"  void CombatEntity_set_damageType_m4123577239 (CombatEntity_t684137495 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntity::get_damageType()
extern "C"  int32_t CombatEntity_get_damageType_m3118857824 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_atkPower(System.Single)
extern "C"  void CombatEntity_set_atkPower_m1905389217 (CombatEntity_t684137495 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntity::get_atkPower()
extern "C"  float CombatEntity_get_atkPower_m840891042 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_atkPowerP(System.Single)
extern "C"  void CombatEntity_set_atkPowerP_m3968220803 (CombatEntity_t684137495 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntity::get_atkPowerP()
extern "C"  float CombatEntity_get_atkPowerP_m297856976 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_atkPowerL(System.Single)
extern "C"  void CombatEntity_set_atkPowerL_m1715390215 (CombatEntity_t684137495 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntity::get_atkPowerL()
extern "C"  float CombatEntity_get_atkPowerL_m297853132 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_PD(System.Single)
extern "C"  void CombatEntity_set_PD_m3753927194 (CombatEntity_t684137495 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntity::get_PD()
extern "C"  float CombatEntity_get_PD_m1530247305 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_MD(System.Single)
extern "C"  void CombatEntity_set_MD_m3988965399 (CombatEntity_t684137495 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntity::get_MD()
extern "C"  float CombatEntity_get_MD_m1530157932 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_defenseP(System.Single)
extern "C"  void CombatEntity_set_defenseP_m1514746750 (CombatEntity_t684137495 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntity::get_defenseP()
extern "C"  float CombatEntity_get_defenseP_m2894598821 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_defenseL(System.Single)
extern "C"  void CombatEntity_set_defenseL_m3556883458 (CombatEntity_t684137495 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntity::get_defenseL()
extern "C"  float CombatEntity_get_defenseL_m2894594977 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_hp(System.Single)
extern "C"  void CombatEntity_set_hp_m884954246 (CombatEntity_t684137495 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntity::get_hp()
extern "C"  float CombatEntity_get_hp_m1531004573 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_maxHp(System.Single)
extern "C"  void CombatEntity_set_maxHp_m1009120378 (CombatEntity_t684137495 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntity::get_maxHp()
extern "C"  float CombatEntity_get_maxHp_m1687545977 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntity::get_PerHP()
extern "C"  int32_t CombatEntity_get_PerHP_m1308268656 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntity::get_HPSecDecPer()
extern "C"  float CombatEntity_get_HPSecDecPer_m2609240369 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_HPSecDecPer(System.Single)
extern "C"  void CombatEntity_set_HPSecDecPer_m3317256386 (CombatEntity_t684137495 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntity::get_HPSecDecNum()
extern "C"  float CombatEntity_get_HPSecDecNum_m2607865178 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_HPSecDecNum(System.Single)
extern "C"  void CombatEntity_set_HPSecDecNum_m3747223353 (CombatEntity_t684137495 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntity::get_HPRstNum()
extern "C"  float CombatEntity_get_HPRstNum_m2585638192 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_HPRstNum(System.Single)
extern "C"  void CombatEntity_set_HPRstNum_m3822537427 (CombatEntity_t684137495 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntity::get_HPRstPer()
extern "C"  float CombatEntity_get_HPRstPer_m2587013383 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_HPRstPer(System.Single)
extern "C"  void CombatEntity_set_HPRstPer_m3392570460 (CombatEntity_t684137495 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntity::get_starLevel()
extern "C"  int32_t CombatEntity_get_starLevel_m2616997309 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_starLevel(System.Int32)
extern "C"  void CombatEntity_set_starLevel_m1735850728 (CombatEntity_t684137495 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntity::get_hits()
extern "C"  float CombatEntity_get_hits_m2412451285 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_hits(System.Single)
extern "C"  void CombatEntity_set_hits_m1281280590 (CombatEntity_t684137495 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntity::get_dodge()
extern "C"  float CombatEntity_get_dodge_m2673198084 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_dodge(System.Single)
extern "C"  void CombatEntity_set_dodge_m246313679 (CombatEntity_t684137495 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntity::get_wreck()
extern "C"  float CombatEntity_get_wreck_m2442596415 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_wreck(System.Single)
extern "C"  void CombatEntity_set_wreck_m1946558068 (CombatEntity_t684137495 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntity::get_withstand()
extern "C"  float CombatEntity_get_withstand_m537201341 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_withstand(System.Single)
extern "C"  void CombatEntity_set_withstand_m3448769718 (CombatEntity_t684137495 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntity::get_crit()
extern "C"  float CombatEntity_get_crit_m2277290479 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_crit(System.Single)
extern "C"  void CombatEntity_set_crit_m2607884404 (CombatEntity_t684137495 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntity::get_tenacity()
extern "C"  float CombatEntity_get_tenacity_m1229246500 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_tenacity(System.Single)
extern "C"  void CombatEntity_set_tenacity_m435067999 (CombatEntity_t684137495 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntity::get_hitsP()
extern "C"  float CombatEntity_get_hitsP_m1771584253 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_hitsP(System.Single)
extern "C"  void CombatEntity_set_hitsP_m1800722550 (CombatEntity_t684137495 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntity::get_dodgeP()
extern "C"  float CombatEntity_get_dodgeP_m1264800430 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_dodgeP(System.Single)
extern "C"  void CombatEntity_set_dodgeP_m4076486677 (CombatEntity_t684137495 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntity::get_wreckP()
extern "C"  float CombatEntity_get_wreckP_m2706083283 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_wreckP(System.Single)
extern "C"  void CombatEntity_set_wreckP_m949487888 (CombatEntity_t684137495 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntity::get_withstandP()
extern "C"  float CombatEntity_get_withstandP_m3768378133 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_withstandP(System.Single)
extern "C"  void CombatEntity_set_withstandP_m273408782 (CombatEntity_t684137495 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntity::get_critP()
extern "C"  float CombatEntity_get_critP_m1876566563 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_critP(System.Single)
extern "C"  void CombatEntity_set_critP_m4270735120 (CombatEntity_t684137495 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntity::get_tenacityP()
extern "C"  float CombatEntity_get_tenacityP_m3746941582 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_tenacityP(System.Single)
extern "C"  void CombatEntity_set_tenacityP_m1337936005 (CombatEntity_t684137495 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntity::get_critHurt()
extern "C"  float CombatEntity_get_critHurt_m1344519006 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_critHurt(System.Single)
extern "C"  void CombatEntity_set_critHurt_m1881489253 (CombatEntity_t684137495 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntity::get_rHurt()
extern "C"  float CombatEntity_get_rHurt_m1112852814 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_rHurt(System.Single)
extern "C"  void CombatEntity_set_rHurt_m555472325 (CombatEntity_t684137495 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntity::get_cureP()
extern "C"  float CombatEntity_get_cureP_m1970318840 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_cureP(System.Single)
extern "C"  void CombatEntity_set_cureP_m2528793947 (CombatEntity_t684137495 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntity::get_hurtP()
extern "C"  float CombatEntity_get_hurtP_m2113316814 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_hurtP(System.Single)
extern "C"  void CombatEntity_set_hurtP_m3604449093 (CombatEntity_t684137495 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntity::get_hurtL()
extern "C"  float CombatEntity_get_hurtL_m2113312970 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_hurtL(System.Single)
extern "C"  void CombatEntity_set_hurtL_m1351618505 (CombatEntity_t684137495 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntity::get_finalHurt()
extern "C"  float CombatEntity_get_finalHurt_m801988946 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_finalHurt(System.Single)
extern "C"  void CombatEntity_set_finalHurt_m2593771585 (CombatEntity_t684137495 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_atkSpeed(System.Single)
extern "C"  void CombatEntity_set_atkSpeed_m3454389023 (CombatEntity_t684137495 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntity::get_atkSpeed()
extern "C"  float CombatEntity_get_atkSpeed_m3515394404 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_runSpeed(System.Single)
extern "C"  void CombatEntity_set_runSpeed_m1736545170 (CombatEntity_t684137495 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntity::get_runSpeed()
extern "C"  float CombatEntity_get_runSpeed_m3595641361 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_atkRange(System.Single)
extern "C"  void CombatEntity_set_atkRange_m1162790761 (CombatEntity_t684137495 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntity::get_atkRange()
extern "C"  float CombatEntity_get_atkRange_m2206825690 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_maxRage(System.Single)
extern "C"  void CombatEntity_set_maxRage_m2317063573 (CombatEntity_t684137495 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntity::get_maxRage()
extern "C"  float CombatEntity_get_maxRage_m2803387774 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_findEnemyRange(System.Single)
extern "C"  void CombatEntity_set_findEnemyRange_m3894042016 (CombatEntity_t684137495 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntity::get_findEnemyRange()
extern "C"  float CombatEntity_get_findEnemyRange_m1691105091 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_rage(System.Single)
extern "C"  void CombatEntity_set_rage_m3252495009 (CombatEntity_t684137495 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntity::get_rage()
extern "C"  float CombatEntity_get_rage_m2690953890 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_rageGrowth(System.Int32)
extern "C"  void CombatEntity_set_rageGrowth_m4101312642 (CombatEntity_t684137495 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntity::get_rageGrowth()
extern "C"  int32_t CombatEntity_get_rageGrowth_m2658798539 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HERO_ELEMENT CombatEntity::get_element()
extern "C"  int32_t CombatEntity_get_element_m3870399983 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_element(HERO_ELEMENT)
extern "C"  void CombatEntity_set_element_m2549126282 (CombatEntity_t684137495 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntity::get_stable()
extern "C"  int32_t CombatEntity_get_stable_m2736304338 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_stable(System.Int32)
extern "C"  void CombatEntity_set_stable_m4244981257 (CombatEntity_t684137495 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntity::get_notInvade()
extern "C"  int32_t CombatEntity_get_notInvade_m1854362799 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_notInvade(System.Int32)
extern "C"  void CombatEntity_set_notInvade_m2485206746 (CombatEntity_t684137495 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntity::get_unyielding()
extern "C"  int32_t CombatEntity_get_unyielding_m2664974501 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_unyielding(System.Int32)
extern "C"  void CombatEntity_set_unyielding_m686610012 (CombatEntity_t684137495 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntity::get_modelScale()
extern "C"  float CombatEntity_get_modelScale_m1770312438 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_modelScale(System.Single)
extern "C"  void CombatEntity_set_modelScale_m166627533 (CombatEntity_t684137495 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntity::get_beforModelScale()
extern "C"  float CombatEntity_get_beforModelScale_m2060241684 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_beforModelScale(System.Single)
extern "C"  void CombatEntity_set_beforModelScale_m1920152511 (CombatEntity_t684137495 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntity::get_modelScaleBetweenValue()
extern "C"  float CombatEntity_get_modelScaleBetweenValue_m1854759839 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_modelScaleBetweenValue(System.Single)
extern "C"  void CombatEntity_set_modelScaleBetweenValue_m596463300 (CombatEntity_t684137495 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntity::get_curModelScaleValue()
extern "C"  float CombatEntity_get_curModelScaleValue_m2360439685 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_curModelScaleValue(System.Single)
extern "C"  void CombatEntity_set_curModelScaleValue_m45461662 (CombatEntity_t684137495 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntity::get_vampire()
extern "C"  float CombatEntity_get_vampire_m3410151291 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_vampire(System.Single)
extern "C"  void CombatEntity_set_vampire_m112066232 (CombatEntity_t684137495 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntity::get_tauntDistance()
extern "C"  float CombatEntity_get_tauntDistance_m1364155056 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_tauntDistance(System.Single)
extern "C"  void CombatEntity_set_tauntDistance_m905374627 (CombatEntity_t684137495 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntity::get_angerSecDecNum()
extern "C"  int32_t CombatEntity_get_angerSecDecNum_m2114905701 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_angerSecDecNum(System.Int32)
extern "C"  void CombatEntity_set_angerSecDecNum_m2866966812 (CombatEntity_t684137495 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntity::get_antiHurt()
extern "C"  bool CombatEntity_get_antiHurt_m4078298318 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntity::get_disMove()
extern "C"  bool CombatEntity_get_disMove_m3738681796 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntity::get_disUseskill()
extern "C"  bool CombatEntity_get_disUseskill_m2917600221 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntity::get_disNormalattack()
extern "C"  bool CombatEntity_get_disNormalattack_m4035462754 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntity::get_dizzy()
extern "C"  bool CombatEntity_get_dizzy_m2358068889 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntity::get_antiControl()
extern "C"  bool CombatEntity_get_antiControl_m4241183456 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntity::get_antiNum()
extern "C"  bool CombatEntity_get_antiNum_m2353849289 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntity::get_isPauseUpdate()
extern "C"  bool CombatEntity_get_isPauseUpdate_m93538970 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_curAntiNum(System.Int32)
extern "C"  void CombatEntity_set_curAntiNum_m4289833842 (CombatEntity_t684137495 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntity::get_curAntiNum()
extern "C"  int32_t CombatEntity_get_curAntiNum_m2735967931 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_inUseAntiNum(System.Int32)
extern "C"  void CombatEntity_set_inUseAntiNum_m2944368080 (CombatEntity_t684137495 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntity::get_inUseAntiNum()
extern "C"  int32_t CombatEntity_get_inUseAntiNum_m3538165657 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntity::get_shield()
extern "C"  bool CombatEntity_get_shield_m3359087270 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_curShieldNum(System.Int32)
extern "C"  void CombatEntity_set_curShieldNum_m2857252683 (CombatEntity_t684137495 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntity::get_curShieldNum()
extern "C"  int32_t CombatEntity_get_curShieldNum_m12122516 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntity::get_antiMove()
extern "C"  bool CombatEntity_get_antiMove_m4216007696 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntity::get_antiBuff()
extern "C"  bool CombatEntity_get_antiBuff_m3906152466 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntity::get_antiBreak()
extern "C"  bool CombatEntity_get_antiBreak_m744746626 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntity::get_forceShift()
extern "C"  bool CombatEntity_get_forceShift_m2327799316 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_forceShift(System.Boolean)
extern "C"  void CombatEntity_set_forceShift_m409617227 (CombatEntity_t684137495 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntity::get_hidePart()
extern "C"  int32_t CombatEntity_get_hidePart_m135269868 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_hidePart(System.Int32)
extern "C"  void CombatEntity_set_hidePart_m1299229347 (CombatEntity_t684137495 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntity::get_isHide()
extern "C"  bool CombatEntity_get_isHide_m3342153737 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntity::get_countryRestraint()
extern "C"  bool CombatEntity_get_countryRestraint_m2685753875 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntity::get_restraintCountryType()
extern "C"  int32_t CombatEntity_get_restraintCountryType_m1646686491 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_restraintCountryType(System.Int32)
extern "C"  void CombatEntity_set_restraintCountryType_m66573906 (CombatEntity_t684137495 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntity::get_restraintCountryPer()
extern "C"  float CombatEntity_get_restraintCountryPer_m44875520 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_restraintCountryPer(System.Single)
extern "C"  void CombatEntity_set_restraintCountryPer_m1509780307 (CombatEntity_t684137495 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntity::get_stateRestraint()
extern "C"  bool CombatEntity_get_stateRestraint_m3234709816 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntity::get_restraintStateType()
extern "C"  int32_t CombatEntity_get_restraintStateType_m2989291606 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_restraintStateType(System.Int32)
extern "C"  void CombatEntity_set_restraintStateType_m2200911501 (CombatEntity_t684137495 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntity::get_restraintStatePer()
extern "C"  float CombatEntity_get_restraintStatePer_m3162234085 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_restraintStatePer(System.Single)
extern "C"  void CombatEntity_set_restraintStatePer_m3644117902 (CombatEntity_t684137495 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntity::get_NearDeathProtect()
extern "C"  bool CombatEntity_get_NearDeathProtect_m37314528 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntity::get_NDPAddBuff()
extern "C"  int32_t CombatEntity_get_NDPAddBuff_m1918629041 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_NDPAddBuff(System.Int32)
extern "C"  void CombatEntity_set_NDPAddBuff_m108187752 (CombatEntity_t684137495 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntity::get_NDPAddBuff02()
extern "C"  int32_t CombatEntity_get_NDPAddBuff02_m1261786675 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_NDPAddBuff02(System.Int32)
extern "C"  void CombatEntity_set_NDPAddBuff02_m1417780586 (CombatEntity_t684137495 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntity::get_BeAttackedLater()
extern "C"  bool CombatEntity_get_BeAttackedLater_m1407253959 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntity::get_BeAttackedLaterSkillID()
extern "C"  int32_t CombatEntity_get_BeAttackedLaterSkillID_m2063510625 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_BeAttackedLaterSkillID(System.Int32)
extern "C"  void CombatEntity_set_BeAttackedLaterSkillID_m2890192664 (CombatEntity_t684137495 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntity::get_BeAttackedLaterEffectID()
extern "C"  int32_t CombatEntity_get_BeAttackedLaterEffectID_m3969016761 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_BeAttackedLaterEffectID(System.Int32)
extern "C"  void CombatEntity_set_BeAttackedLaterEffectID_m2000562532 (CombatEntity_t684137495 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntity::get_DeathLater()
extern "C"  bool CombatEntity_get_DeathLater_m233034261 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntity::get_DeathLaterEffectID()
extern "C"  int32_t CombatEntity_get_DeathLaterEffectID_m2774200699 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_DeathLaterEffectID(System.Int32)
extern "C"  void CombatEntity_set_DeathLaterEffectID_m85960754 (CombatEntity_t684137495 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntity::get_DeathLaterHeroID()
extern "C"  int32_t CombatEntity_get_DeathLaterHeroID_m1308071652 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_DeathLaterHeroID(System.Int32)
extern "C"  void CombatEntity_set_DeathLaterHeroID_m3422927259 (CombatEntity_t684137495 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntity::get_BeattackedLaterSpeed()
extern "C"  int32_t CombatEntity_get_BeattackedLaterSpeed_m1137203100 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_BeattackedLaterSpeed(System.Int32)
extern "C"  void CombatEntity_set_BeattackedLaterSpeed_m1677132627 (CombatEntity_t684137495 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntity::get_DeathLaterSpeed()
extern "C"  int32_t CombatEntity_get_DeathLaterSpeed_m4288390458 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_DeathLaterSpeed(System.Int32)
extern "C"  void CombatEntity_set_DeathLaterSpeed_m1349823333 (CombatEntity_t684137495 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntity::get_AntiEndEffectID()
extern "C"  int32_t CombatEntity_get_AntiEndEffectID_m2581472432 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_AntiEndEffectID(System.Int32)
extern "C"  void CombatEntity_set_AntiEndEffectID_m2287315419 (CombatEntity_t684137495 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntity::get_AuraAddHP()
extern "C"  bool CombatEntity_get_AuraAddHP_m2304342827 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntity::get_AuraAddHPRadius()
extern "C"  int32_t CombatEntity_get_AuraAddHPRadius_m707946051 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_AuraAddHPRadius(System.Int32)
extern "C"  void CombatEntity_set_AuraAddHPRadius_m1955407598 (CombatEntity_t684137495 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntity::get_AuraAddHPNum()
extern "C"  int32_t CombatEntity_get_AuraAddHPNum_m249373655 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_AuraAddHPNum(System.Int32)
extern "C"  void CombatEntity_set_AuraAddHPNum_m2379093262 (CombatEntity_t684137495 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntity::get_AuraAddHPPer()
extern "C"  int32_t CombatEntity_get_AuraAddHPPer_m250748846 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_AuraAddHPPer(System.Int32)
extern "C"  void CombatEntity_set_AuraAddHPPer_m3612149349 (CombatEntity_t684137495 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntity::get_AuraReduceHP()
extern "C"  bool CombatEntity_get_AuraReduceHP_m3245900750 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntity::get_AuraReduceHPRadius()
extern "C"  int32_t CombatEntity_get_AuraReduceHPRadius_m1646873114 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_AuraReduceHPRadius(System.Int32)
extern "C"  void CombatEntity_set_AuraReduceHPRadius_m713755217 (CombatEntity_t684137495 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntity::get_AuraReduceHPNum()
extern "C"  int32_t CombatEntity_get_AuraReduceHPNum_m1647421280 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_AuraReduceHPNum(System.Int32)
extern "C"  void CombatEntity_set_AuraReduceHPNum_m4018840715 (CombatEntity_t684137495 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntity::get_AuraReduceHPPer()
extern "C"  int32_t CombatEntity_get_AuraReduceHPPer_m1648796471 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_AuraReduceHPPer(System.Int32)
extern "C"  void CombatEntity_set_AuraReduceHPPer_m956929506 (CombatEntity_t684137495 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntity::get_ActiveSkillDamageAddPer()
extern "C"  float CombatEntity_get_ActiveSkillDamageAddPer_m1933056227 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_ActiveSkillDamageAddPer(System.Single)
extern "C"  void CombatEntity_set_ActiveSkillDamageAddPer_m3853510224 (CombatEntity_t684137495 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntity::get_AddAngerRatio()
extern "C"  float CombatEntity_get_AddAngerRatio_m147060818 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_AddAngerRatio(System.Single)
extern "C"  void CombatEntity_set_AddAngerRatio_m771949889 (CombatEntity_t684137495 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntity::get_beHurtAnger()
extern "C"  int32_t CombatEntity_get_beHurtAnger_m3390196320 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_beHurtAnger(System.Int32)
extern "C"  void CombatEntity_set_beHurtAnger_m2874721419 (CombatEntity_t684137495 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntity::get_disUseActionSkill()
extern "C"  bool CombatEntity_get_disUseActionSkill_m947718247 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntity::get_isBerserker()
extern "C"  bool CombatEntity_get_isBerserker_m2847361026 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntity::get_superArmor()
extern "C"  int32_t CombatEntity_get_superArmor_m904876763 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_superArmor(System.Int32)
extern "C"  void CombatEntity_set_superArmor_m1130905490 (CombatEntity_t684137495 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntity::get_superArmorTime()
extern "C"  float CombatEntity_get_superArmorTime_m3473301702 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_superArmorTime(System.Single)
extern "C"  void CombatEntity_set_superArmorTime_m2246639869 (CombatEntity_t684137495 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::AssinPlusAtt()
extern "C"  void CombatEntity_AssinPlusAtt_m1874833793 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntity::get_isHpLineState()
extern "C"  bool CombatEntity_get_isHpLineState_m1930970000 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntity::get_hpLineNum()
extern "C"  int32_t CombatEntity_get_hpLineNum_m2564984885 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_hpLineNum(System.Int32)
extern "C"  void CombatEntity_set_hpLineNum_m420569440 (CombatEntity_t684137495 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntity::get_hpLineBuffID()
extern "C"  int32_t CombatEntity_get_hpLineBuffID_m2120532545 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_hpLineBuffID(System.Int32)
extern "C"  void CombatEntity_set_hpLineBuffID_m2333697144 (CombatEntity_t684137495 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntity::get_hpLineUpBuffID()
extern "C"  int32_t CombatEntity_get_hpLineUpBuffID_m1704504700 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_hpLineUpBuffID(System.Int32)
extern "C"  void CombatEntity_set_hpLineUpBuffID_m3569307059 (CombatEntity_t684137495 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntity::get_hpLineCleaveAddBuff()
extern "C"  int32_t CombatEntity_get_hpLineCleaveAddBuff_m3620487791 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_hpLineCleaveAddBuff(System.Int32)
extern "C"  void CombatEntity_set_hpLineCleaveAddBuff_m87991322 (CombatEntity_t684137495 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntity::get_hpLineUpFrameCount()
extern "C"  int32_t CombatEntity_get_hpLineUpFrameCount_m3021690096 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_hpLineUpFrameCount(System.Int32)
extern "C"  void CombatEntity_set_hpLineUpFrameCount_m2705687591 (CombatEntity_t684137495 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntity::get_controlBuffTimeBonus()
extern "C"  float CombatEntity_get_controlBuffTimeBonus_m3396348407 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_controlBuffTimeBonus(System.Single)
extern "C"  void CombatEntity_set_controlBuffTimeBonus_m2214284652 (CombatEntity_t684137495 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntity::get_controlBuffTimeBonusPer()
extern "C"  float CombatEntity_get_controlBuffTimeBonusPer_m4109640200 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_controlBuffTimeBonusPer(System.Single)
extern "C"  void CombatEntity_set_controlBuffTimeBonusPer_m2258149707 (CombatEntity_t684137495 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntity::get_runeNum()
extern "C"  int32_t CombatEntity_get_runeNum_m1187724119 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_runeNum(System.Int32)
extern "C"  void CombatEntity_set_runeNum_m3353704450 (CombatEntity_t684137495 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntity::get_runeNumAllSkill()
extern "C"  int32_t CombatEntity_get_runeNumAllSkill_m3555463847 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_runeNumAllSkill(System.Int32)
extern "C"  void CombatEntity_set_runeNumAllSkill_m2220055890 (CombatEntity_t684137495 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntity::get_runeState()
extern "C"  bool CombatEntity_get_runeState_m721053372 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntity::get_runeStateAllSkill()
extern "C"  bool CombatEntity_get_runeStateAllSkill_m381683980 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntity::get_runeDisappearTime()
extern "C"  int32_t CombatEntity_get_runeDisappearTime_m861380193 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_runeDisappearTime(System.Int32)
extern "C"  void CombatEntity_set_runeDisappearTime_m3179451276 (CombatEntity_t684137495 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntity::get_runeTakeEffectNum()
extern "C"  int32_t CombatEntity_get_runeTakeEffectNum_m787736895 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_runeTakeEffectNum(System.Int32)
extern "C"  void CombatEntity_set_runeTakeEffectNum_m3835658602 (CombatEntity_t684137495 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntity::get_runeTakeEffectBuffID()
extern "C"  int32_t CombatEntity_get_runeTakeEffectBuffID_m187520247 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_runeTakeEffectBuffID(System.Int32)
extern "C"  void CombatEntity_set_runeTakeEffectBuffID_m2069614638 (CombatEntity_t684137495 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntity::get_runeTakeEffectBuffID01()
extern "C"  int32_t CombatEntity_get_runeTakeEffectBuffID01_m4113545528 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_runeTakeEffectBuffID01(System.Int32)
extern "C"  void CombatEntity_set_runeTakeEffectBuffID01_m3645790575 (CombatEntity_t684137495 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntity::get_runeTakeEffectBuffID02()
extern "C"  int32_t CombatEntity_get_runeTakeEffectBuffID02_m4113546489 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_runeTakeEffectBuffID02(System.Int32)
extern "C"  void CombatEntity_set_runeTakeEffectBuffID02_m858375088 (CombatEntity_t684137495 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntity::get_godDownState()
extern "C"  bool CombatEntity_get_godDownState_m2783630256 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntity::get_godDownTweenTime()
extern "C"  float CombatEntity_get_godDownTweenTime_m3286424655 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_godDownTweenTime(System.Single)
extern "C"  void CombatEntity_set_godDownTweenTime_m3057325588 (CombatEntity_t684137495 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntity::get_antiDebuffDiscrete()
extern "C"  bool CombatEntity_get_antiDebuffDiscrete_m4216278828 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntity::get_antiDebuffDiscreteID()
extern "C"  int32_t CombatEntity_get_antiDebuffDiscreteID_m754511905 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_antiDebuffDiscreteID(System.Int32)
extern "C"  void CombatEntity_set_antiDebuffDiscreteID_m3138780248 (CombatEntity_t684137495 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntity::get_antiDebuffDiscreteAddBuffID()
extern "C"  int32_t CombatEntity_get_antiDebuffDiscreteAddBuffID_m4063674731 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_antiDebuffDiscreteAddBuffID(System.Int32)
extern "C"  void CombatEntity_set_antiDebuffDiscreteAddBuffID_m1218645782 (CombatEntity_t684137495 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntity::get_discreteBuffID()
extern "C"  int32_t CombatEntity_get_discreteBuffID_m2428723614 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_discreteBuffID(System.Int32)
extern "C"  void CombatEntity_set_discreteBuffID_m1591926485 (CombatEntity_t684137495 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntity::get_discreteBuffTime()
extern "C"  float CombatEntity_get_discreteBuffTime_m248405550 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_discreteBuffTime(System.Single)
extern "C"  void CombatEntity_set_discreteBuffTime_m1140577941 (CombatEntity_t684137495 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntity::get_discreteBuffTimer()
extern "C"  float CombatEntity_get_discreteBuffTimer_m3405675878 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_discreteBuffTimer(System.Single)
extern "C"  void CombatEntity_set_discreteBuffTimer_m1555614893 (CombatEntity_t684137495 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntity::get_isPetrified()
extern "C"  bool CombatEntity_get_isPetrified_m1306730931 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntity::get_petrifiedChangeTime()
extern "C"  float CombatEntity_get_petrifiedChangeTime_m2332550562 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_petrifiedChangeTime(System.Single)
extern "C"  void CombatEntity_set_petrifiedChangeTime_m1472830449 (CombatEntity_t684137495 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntity::get_isChangeSkill()
extern "C"  bool CombatEntity_get_isChangeSkill_m1818256444 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntity::get_changeOldSkillID()
extern "C"  int32_t CombatEntity_get_changeOldSkillID_m1043171948 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_changeOldSkillID(System.Int32)
extern "C"  void CombatEntity_set_changeOldSkillID_m696426787 (CombatEntity_t684137495 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntity::get_changeNewSkillID()
extern "C"  int32_t CombatEntity_get_changeNewSkillID_m1169057555 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_changeNewSkillID(System.Int32)
extern "C"  void CombatEntity_set_changeNewSkillID_m571652938 (CombatEntity_t684137495 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntity::get_changeSkillPro()
extern "C"  int32_t CombatEntity_get_changeSkillPro_m1529885123 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_changeSkillPro(System.Int32)
extern "C"  void CombatEntity_set_changeSkillPro_m3582435194 (CombatEntity_t684137495 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntity::get_isWhenCritAddBuff()
extern "C"  bool CombatEntity_get_isWhenCritAddBuff_m3509845179 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntity::get_whenCritAddBuffID()
extern "C"  int32_t CombatEntity_get_whenCritAddBuffID_m4154756102 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_whenCritAddBuffID(System.Int32)
extern "C"  void CombatEntity_set_whenCritAddBuffID_m2001154481 (CombatEntity_t684137495 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntity::get_isThumpState()
extern "C"  bool CombatEntity_get_isThumpState_m1469053076 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntity::get_thumpSkillID()
extern "C"  int32_t CombatEntity_get_thumpSkillID_m3729942143 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_thumpSkillID(System.Int32)
extern "C"  void CombatEntity_set_thumpSkillID_m594320310 (CombatEntity_t684137495 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntity::get_thumpAddValue()
extern "C"  float CombatEntity_get_thumpAddValue_m396909633 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_thumpAddValue(System.Single)
extern "C"  void CombatEntity_set_thumpAddValue_m346471858 (CombatEntity_t684137495 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntity::get_tankBuffID()
extern "C"  int32_t CombatEntity_get_tankBuffID_m1550293391 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_tankBuffID(System.Int32)
extern "C"  void CombatEntity_set_tankBuffID_m4257749062 (CombatEntity_t684137495 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_HTBuffID(System.Int32)
extern "C"  void CombatEntity_set_HTBuffID_m162473576 (CombatEntity_t684137495 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntity::get_HTBuffID()
extern "C"  int32_t CombatEntity_get_HTBuffID_m4252911921 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntity::get_cleaveSkillID()
extern "C"  int32_t CombatEntity_get_cleaveSkillID_m779526979 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_cleaveSkillID(System.Int32)
extern "C"  void CombatEntity_set_cleaveSkillID_m3391492718 (CombatEntity_t684137495 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntity::get_cleaveBuffID()
extern "C"  int32_t CombatEntity_get_cleaveBuffID_m377672025 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_cleaveBuffID(System.Int32)
extern "C"  void CombatEntity_set_cleaveBuffID_m4024536976 (CombatEntity_t684137495 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntity::get_isReduceRageHp()
extern "C"  bool CombatEntity_get_isReduceRageHp_m1805044930 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntity::get_reduceRageNum()
extern "C"  int32_t CombatEntity_get_reduceRageNum_m3376759006 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_reduceRageNum(System.Int32)
extern "C"  void CombatEntity_set_reduceRageNum_m1682317193 (CombatEntity_t684137495 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntity::get_isDeathHarvest()
extern "C"  bool CombatEntity_get_isDeathHarvest_m307405404 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntity::get_deathHarvestLine()
extern "C"  int32_t CombatEntity_get_deathHarvestLine_m4177122944 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_deathHarvestLine(System.Int32)
extern "C"  void CombatEntity_set_deathHarvestLine_m2591649591 (CombatEntity_t684137495 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntity::get_deathHarvestPer()
extern "C"  int32_t CombatEntity_get_deathHarvestPer_m1108154099 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_deathHarvestPer(System.Int32)
extern "C"  void CombatEntity_set_deathHarvestPer_m2750697374 (CombatEntity_t684137495 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntity::get_isResistanceCountry()
extern "C"  bool CombatEntity_get_isResistanceCountry_m3978758744 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntity::get_resistanceCountryType()
extern "C"  int32_t CombatEntity_get_resistanceCountryType_m3114748706 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_resistanceCountryType(System.Int32)
extern "C"  void CombatEntity_set_resistanceCountryType_m3655330253 (CombatEntity_t684137495 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntity::get_resistanceCountryPer()
extern "C"  float CombatEntity_get_resistanceCountryPer_m3852951893 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::set_resistanceCountryPer(System.Single)
extern "C"  void CombatEntity_set_resistanceCountryPer_m803569358 (CombatEntity_t684137495 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntity::get_isTimeBomb()
extern "C"  bool CombatEntity_get_isTimeBomb_m2233424790 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntity::get_isPlagueDust()
extern "C"  bool CombatEntity_get_isPlagueDust_m1808516299 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntity::get_isInfection()
extern "C"  bool CombatEntity_get_isInfection_m1250755982 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntity::get_isFierceWind()
extern "C"  bool CombatEntity_get_isFierceWind_m1647251585 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::AddState(UnitStateBase)
extern "C"  void CombatEntity_AddState_m1587384420 (CombatEntity_t684137495 * __this, UnitStateBase_t1040218558 * ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::DelState(UnitStateBase)
extern "C"  void CombatEntity_DelState_m679461582 (CombatEntity_t684137495 * __this, UnitStateBase_t1040218558 * ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::UpdateUnitState()
extern "C"  void CombatEntity_UpdateUnitState_m3979797990 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::RemoveAllStates()
extern "C"  void CombatEntity_RemoveAllStates_m711913313 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::ShaderBuffInit()
extern "C"  void CombatEntity_ShaderBuffInit_m1989898920 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::ShaderBuffDestory()
extern "C"  void CombatEntity_ShaderBuffDestory_m4234984734 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::ShaderBuffDead()
extern "C"  void CombatEntity_ShaderBuffDead_m1838187772 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::EnterGhost(System.Int32)
extern "C"  void CombatEntity_EnterGhost_m1962047688 (CombatEntity_t684137495 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::ExitGhost()
extern "C"  void CombatEntity_ExitGhost_m1748811987 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::GhostUpdate()
extern "C"  void CombatEntity_GhostUpdate_m2881796858 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::EnterSusanoo(System.Int32)
extern "C"  void CombatEntity_EnterSusanoo_m3904269015 (CombatEntity_t684137495 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::ExitSusanoo()
extern "C"  void CombatEntity_ExitSusanoo_m979552866 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::SusanooUpdate()
extern "C"  void CombatEntity_SusanooUpdate_m1364907465 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::EnterPartBloom(System.Int32)
extern "C"  void CombatEntity_EnterPartBloom_m153325257 (CombatEntity_t684137495 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::BloomUpdate()
extern "C"  void CombatEntity_BloomUpdate_m1123508846 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::ExitPartBloom()
extern "C"  void CombatEntity_ExitPartBloom_m1565606484 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::<OnTriggerFun>m__3C2()
extern "C"  void CombatEntity_U3COnTriggerFunU3Em__3C2_m347467473 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::<OnTriggerFun>m__3C3()
extern "C"  void CombatEntity_U3COnTriggerFunU3Em__3C3_m347468434 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::<RefreshShader>m__3C4()
extern "C"  void CombatEntity_U3CRefreshShaderU3Em__3C4_m681907315 (CombatEntity_t684137495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::<ShaderBuffInit>m__3C5(Mihua.Asset.ABLoadOperation.AssetOperation)
extern "C"  void CombatEntity_U3CShaderBuffInitU3Em__3C5_m2633830233 (CombatEntity_t684137495 * __this, AssetOperation_t778728221 * ___ao0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 CombatEntity::ilo_get_position1(CombatEntity)
extern "C"  Vector3_t4282066566  CombatEntity_ilo_get_position1_m182270045 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Path CombatEntity::ilo_SearchPath2(AIPath)
extern "C"  Path_t1974241691 * CombatEntity_ilo_SearchPath2_m323976185 (Il2CppObject * __this /* static, unused */, AIPath_t1930792045 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntity::ilo_get_scale3(CombatEntity)
extern "C"  float CombatEntity_ilo_get_scale3_m3433943674 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// AnimationRunner CombatEntity::ilo_get_characterAnim4(CombatEntity)
extern "C"  AnimationRunner_t1015409588 * CombatEntity_ilo_get_characterAnim4_m471941502 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::ilo_SetDelayPauseState5(FightCtrl,System.Boolean)
extern "C"  void CombatEntity_ilo_SetDelayPauseState5_m3468000910 (Il2CppObject * __this /* static, unused */, FightCtrl_t648967803 * ____this0, bool ___isPause1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntity::ilo_get_isFrozenIdle6(GameMgr)
extern "C"  bool CombatEntity_ilo_get_isFrozenIdle6_m2659346754 (Il2CppObject * __this /* static, unused */, GameMgr_t1469029542 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntity::ilo_get_isNotSelected7(CombatEntity)
extern "C"  bool CombatEntity_ilo_get_isNotSelected7_m466472812 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::ilo_SearchAtkTarget8(CombatEntity)
extern "C"  void CombatEntity_ilo_SearchAtkTarget8_m748544051 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntity::ilo_VectorAngle9(CombatEntity,UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  float CombatEntity_ilo_VectorAngle9_m3694061879 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, Vector3_t4282066566  ___from1, Vector3_t4282066566  ___to2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntity::ilo_getposid10(CombatEntity,System.Int32)
extern "C"  int32_t CombatEntity_ilo_getposid10_m3113538005 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntity::ilo_get_atkRange11(CombatEntity)
extern "C"  float CombatEntity_ilo_get_atkRange11_m3684107318 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Skill CombatEntity::ilo_get_activeSkill12(FightCtrl)
extern "C"  Skill_t79944241 * CombatEntity_ilo_get_activeSkill12_m766464167 (Il2CppObject * __this /* static, unused */, FightCtrl_t648967803 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::ilo_Log13(System.Object,System.Boolean)
extern "C"  void CombatEntity_ilo_Log13_m4224023614 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::ilo_ShaderBuffInit14(CombatEntity)
extern "C"  void CombatEntity_ilo_ShaderBuffInit14_m548633991 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GameQutilyCtr CombatEntity::ilo_get_GameQutilyCtr15()
extern "C"  GameQutilyCtr_t3963256169 * CombatEntity_ilo_get_GameQutilyCtr15_m2501995689 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String CombatEntity::ilo_get_skills16(CombatEntity)
extern "C"  String_t* CombatEntity_ilo_get_skills16_m2034633935 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GlobalGOMgr CombatEntity::ilo_get_GlobalGOMgr17()
extern "C"  GlobalGOMgr_t803081773 * CombatEntity_ilo_get_GlobalGOMgr17_m251140275 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CSDatacfgManager CombatEntity::ilo_get_CfgDataMgr18()
extern "C"  CSDatacfgManager_t1565254243 * CombatEntity_ilo_get_CfgDataMgr18_m1215506183 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// effectCfg CombatEntity::ilo_GeteffectCfg19(CSDatacfgManager,System.Int32)
extern "C"  effectCfg_t2826279187 * CombatEntity_ilo_GeteffectCfg19_m2235030754 (Il2CppObject * __this /* static, unused */, CSDatacfgManager_t1565254243 * ____this0, int32_t ___id1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject CombatEntity::ilo_Instantiate20(UnityEngine.GameObject)
extern "C"  GameObject_t3674682005 * CombatEntity_ilo_Instantiate20_m311300090 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::ilo_ChangeParent21(UnityEngine.GameObject,UnityEngine.GameObject,System.Boolean)
extern "C"  void CombatEntity_ilo_ChangeParent21_m4059974659 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ____this0, GameObject_t3674682005 * ___parent1, bool ___resetMat2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntity::ilo_get_atkedEff22(CombatEntity)
extern "C"  int32_t CombatEntity_ilo_get_atkedEff22_m427523521 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Blood CombatEntity::ilo_AddBlood23(BloodMgr,CombatEntity)
extern "C"  Blood_t64280026 * CombatEntity_ilo_AddBlood23_m2111086023 (Il2CppObject * __this /* static, unused */, BloodMgr_t3705885150 * ____this0, CombatEntity_t684137495 * ___unit1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::ilo_set_PlotFightMgr24(CombatEntity,PlotFightTalkMgr)
extern "C"  void CombatEntity_ilo_set_PlotFightMgr24_m3674924773 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, PlotFightTalkMgr_t866599741 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::ilo_set_characterAnim25(CombatEntity,AnimationRunner)
extern "C"  void CombatEntity_ilo_set_characterAnim25_m3621467280 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, AnimationRunner_t1015409588 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::ilo_set_canMove26(CombatEntity,System.Boolean)
extern "C"  void CombatEntity_ilo_set_canMove26_m332155901 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::ilo_set_isKnockingUp27(CombatEntity,System.Boolean)
extern "C"  void CombatEntity_ilo_set_isKnockingUp27_m1795685772 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::ilo_set_shadow28(CombatEntity,FS_ShadowSimple)
extern "C"  void CombatEntity_ilo_set_shadow28_m611748679 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, FS_ShadowSimple_t4208748868 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::ilo_set_runSpeed29(CombatEntity,System.Single)
extern "C"  void CombatEntity_ilo_set_runSpeed29_m1036287905 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 CombatEntity::ilo_GetFeetPosition30(AIPath)
extern "C"  Vector3_t4282066566  CombatEntity_ilo_GetFeetPosition30_m3764239858 (Il2CppObject * __this /* static, unused */, AIPath_t1930792045 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FS_ShadowSimple CombatEntity::ilo_get_shadow31(CombatEntity)
extern "C"  FS_ShadowSimple_t4208748868 * CombatEntity_ilo_get_shadow31_m2951510866 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 CombatEntity::ilo_DelayCall32(System.Single,System.Action)
extern "C"  uint32_t CombatEntity_ilo_DelayCall32_m2730346126 (Il2CppObject * __this /* static, unused */, float ___delaytime0, Action_t3771233898 * ___act1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::ilo_OnViewModel33(CombatEntity)
extern "C"  void CombatEntity_ilo_OnViewModel33_m2634072061 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::ilo_AddBuffOnShiliFuben34(CombatEntity)
extern "C"  void CombatEntity_ilo_AddBuffOnShiliFuben34_m2414942839 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FightCtrl CombatEntity::ilo_get_fightCtrl35(CombatEntity)
extern "C"  FightCtrl_t648967803 * CombatEntity_ilo_get_fightCtrl35_m3335643508 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// BuffCtrl CombatEntity::ilo_get_buffCtrl36(CombatEntity)
extern "C"  BuffCtrl_t2836564350 * CombatEntity_ilo_get_buffCtrl36_m675139077 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CSGameDataMgr CombatEntity::ilo_get_CSGameDataMgr37()
extern "C"  CSGameDataMgr_t2623305516 * CombatEntity_ilo_get_CSGameDataMgr37_m793868271 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::ilo_SetViewModel38(CombatEntity,System.Boolean)
extern "C"  void CombatEntity_ilo_SetViewModel38_m2667044890 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, bool ___isShow1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Blood CombatEntity::ilo_get_blood39(CombatEntity)
extern "C"  Blood_t64280026 * CombatEntity_ilo_get_blood39_m3658043830 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HeroMgr CombatEntity::ilo_get_instance40()
extern "C"  HeroMgr_t2475965342 * CombatEntity_ilo_get_instance40_m2495987202 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// EffectMgr CombatEntity::ilo_get_EffectMgr41()
extern "C"  EffectMgr_t535289511 * CombatEntity_ilo_get_EffectMgr41_m2669049662 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::ilo_PlayEffect42(EffectMgr,System.Int32,CombatEntity)
extern "C"  void CombatEntity_ilo_PlayEffect42_m190392333 (Il2CppObject * __this /* static, unused */, EffectMgr_t535289511 * ____this0, int32_t ___id1, CombatEntity_t684137495 * ___srcTf2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// EntityEnum.UnitType CombatEntity::ilo_get_unitType43(CombatEntity)
extern "C"  int32_t CombatEntity_ilo_get_unitType43_m31064621 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntity::ilo_get_isDeath44(CombatEntity)
extern "C"  bool CombatEntity_ilo_get_isDeath44_m75997477 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntity::ilo_get_IsPlaying45(CameraAnimationMgr)
extern "C"  bool CombatEntity_ilo_get_IsPlaying45_m3701383486 (Il2CppObject * __this /* static, unused */, CameraAnimationMgr_t3311695833 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Entity.Behavior.IBehaviorCtrl CombatEntity::ilo_get_bvrCtrl46(CombatEntity)
extern "C"  IBehaviorCtrl_t4225040900 * CombatEntity_ilo_get_bvrCtrl46_m961237628 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntity::ilo_get_isBattleStart47(GameMgr)
extern "C"  bool CombatEntity_ilo_get_isBattleStart47_m2992259319 (Il2CppObject * __this /* static, unused */, GameMgr_t1469029542 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntity::ilo_get_isBattleEndBeforWin48(GameMgr)
extern "C"  bool CombatEntity_ilo_get_isBattleEndBeforWin48_m538059783 (Il2CppObject * __this /* static, unused */, GameMgr_t1469029542 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntity::ilo_get_IsPlayPloting49(GameMgr)
extern "C"  bool CombatEntity_ilo_get_IsPlayPloting49_m628125810 (Il2CppObject * __this /* static, unused */, GameMgr_t1469029542 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CSCheckPointUnit CombatEntity::ilo_get_checkPoint50(GameMgr)
extern "C"  CSCheckPointUnit_t3129216924 * CombatEntity_ilo_get_checkPoint50_m4245422700 (Il2CppObject * __this /* static, unused */, GameMgr_t1469029542 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntity::ilo_get_canAttack51(CombatEntity)
extern "C"  bool CombatEntity_ilo_get_canAttack51_m3015756495 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::ilo_DiscreteBuffUpdate52(CombatEntity)
extern "C"  void CombatEntity_ilo_DiscreteBuffUpdate52_m228866382 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::ilo_GhostUpdate53(CombatEntity)
extern "C"  void CombatEntity_ilo_GhostUpdate53_m1917694350 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::ilo_OneSecTick54(CombatEntity)
extern "C"  void CombatEntity_ilo_OneSecTick54_m2001746755 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntity::ilo_get_isWin55()
extern "C"  bool CombatEntity_ilo_get_isWin55_m2628581924 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntity::ilo_get_superArmor56(CombatEntity)
extern "C"  int32_t CombatEntity_ilo_get_superArmor56_m676240760 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntity::ilo_get_isAttack57(CombatEntity)
extern "C"  bool CombatEntity_ilo_get_isAttack57_m1838493101 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntity::ilo_get_starAttackTime58(CombatEntity)
extern "C"  float CombatEntity_ilo_get_starAttackTime58_m3714758107 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::ilo_UpdateGodDown59(CombatEntity)
extern "C"  void CombatEntity_ilo_UpdateGodDown59_m1207332753 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::ilo_UpdateScale60(CombatEntity)
extern "C"  void CombatEntity_ilo_UpdateScale60_m3042419 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::ilo_UpdateUnitState61(CombatEntity)
extern "C"  void CombatEntity_ilo_UpdateUnitState61_m733403735 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntity::ilo_get_isStun62(CombatEntity)
extern "C"  bool CombatEntity_ilo_get_isStun62_m3035019513 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntity::ilo_get_isKnockingUp63(CombatEntity)
extern "C"  bool CombatEntity_ilo_get_isKnockingUp63_m1607697289 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::ilo_FixedUpdate64(Entity.Behavior.IBehaviorCtrl)
extern "C"  void CombatEntity_ilo_FixedUpdate64_m626254963 (Il2CppObject * __this /* static, unused */, IBehaviorCtrl_t4225040900 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntity::ilo_get_isMovement65(CombatEntity)
extern "C"  bool CombatEntity_ilo_get_isMovement65_m23093553 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntity::ilo_get_isUsingSkill66(FightCtrl)
extern "C"  bool CombatEntity_ilo_get_isUsingSkill66_m2495607824 (Il2CppObject * __this /* static, unused */, FightCtrl_t648967803 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntity::ilo_get_disMove67(CombatEntity)
extern "C"  bool CombatEntity_ilo_get_disMove67_m2423050171 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntity::ilo_get_canMove68(CombatEntity)
extern "C"  bool CombatEntity_ilo_get_canMove68_m753664062 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::ilo_UpdateMove69(CombatEntity)
extern "C"  void CombatEntity_ilo_UpdateMove69_m1791649881 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::ilo_UpdateYPos70(CombatEntity)
extern "C"  void CombatEntity_ilo_UpdateYPos70_m551990585 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntity::ilo_get_IsPlayREC71(ReplayMgr)
extern "C"  bool CombatEntity_ilo_get_IsPlayREC71_m1053039715 (Il2CppObject * __this /* static, unused */, ReplayMgr_t1549183121 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::ilo_Play72(AnimationRunner,AnimationRunner/AniType,System.Single,System.Boolean)
extern "C"  void CombatEntity_ilo_Play72_m833133035 (Il2CppObject * __this /* static, unused */, AnimationRunner_t1015409588 * ____this0, int32_t ___aniType1, float ___playTime2, bool ___isskill3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider CombatEntity::ilo_get_boxCollider73(CombatEntity)
extern "C"  Collider_t2939674232 * CombatEntity_ilo_get_boxCollider73_m1857972088 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::ilo_AddData74(ReplayMgr,ReplayExecuteType,CombatEntity,System.Object[])
extern "C"  void CombatEntity_ilo_AddData74_m1311850925 (Il2CppObject * __this /* static, unused */, ReplayMgr_t1549183121 * ____this0, int32_t ___type1, CombatEntity_t684137495 * ___entity2, ObjectU5BU5D_t1108656482* ___datas3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntity::ilo_get_lucencytime75(CombatEntity)
extern "C"  float CombatEntity_ilo_get_lucencytime75_m36559399 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.SkinnedMeshRenderer CombatEntity::ilo_get_skinMeshRender76(CombatEntity)
extern "C"  SkinnedMeshRenderer_t3986041494 * CombatEntity_ilo_get_skinMeshRender76_m4162220110 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::ilo_set_atkTarget77(CombatEntity,CombatEntity)
extern "C"  void CombatEntity_ilo_set_atkTarget77_m1202456811 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, CombatEntity_t684137495 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntity::ilo_get_rage78(CombatEntity)
extern "C"  float CombatEntity_ilo_get_rage78_m3918571647 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntity::ilo_get_maxRage79(CombatEntity)
extern "C"  float CombatEntity_ilo_get_maxRage79_m842091606 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntity::ilo_get_id80(CombatEntity)
extern "C"  int32_t CombatEntity_ilo_get_id80_m2221513510 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntity::ilo_get_reduceRageNum81(CombatEntity)
extern "C"  int32_t CombatEntity_ilo_get_reduceRageNum81_m3232362189 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// LevelConfigManager CombatEntity::ilo_get_instance82()
extern "C"  LevelConfigManager_t657947911 * CombatEntity_ilo_get_instance82_m1390568417 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntity::ilo_get_level83(GameMgr)
extern "C"  int32_t CombatEntity_ilo_get_level83_m2399847253 (Il2CppObject * __this /* static, unused */, GameMgr_t1469029542 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntity::ilo_get_curRound84(NpcMgr)
extern "C"  int32_t CombatEntity_ilo_get_curRound84_m678204989 (Il2CppObject * __this /* static, unused */, NpcMgr_t2339534743 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// NpcMgr CombatEntity::ilo_get_instance85()
extern "C"  NpcMgr_t2339534743 * CombatEntity_ilo_get_instance85_m3014724116 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntity::ilo_get_BeAttackedLater86(CombatEntity)
extern "C"  bool CombatEntity_ilo_get_BeAttackedLater86_m114286331 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntity::ilo_get_BeAttackedLaterEffectID87(CombatEntity)
extern "C"  int32_t CombatEntity_ilo_get_BeAttackedLaterEffectID87_m3960618926 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntity::ilo_get_isWhenCritAddBuff88(CombatEntity)
extern "C"  bool CombatEntity_ilo_get_isWhenCritAddBuff88_m3798503473 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntity::ilo_get_whenCritAddBuffID89(CombatEntity)
extern "C"  int32_t CombatEntity_ilo_get_whenCritAddBuffID89_m2643117949 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// BuffCtrl CombatEntity::ilo_get_buffCtrl90(CombatEntity)
extern "C"  BuffCtrl_t2836564350 * CombatEntity_ilo_get_buffCtrl90_m1451525049 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::ilo_AddBuff91(BuffCtrl,System.Int32)
extern "C"  void CombatEntity_ilo_AddBuff91_m1864758170 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, int32_t ___id1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::ilo_DispatchEvent92(CEvent.ZEvent)
extern "C"  void CombatEntity_ilo_DispatchEvent92_m1100330099 (Il2CppObject * __this /* static, unused */, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::ilo_atkedEffRefresh93(CombatEntity)
extern "C"  void CombatEntity_ilo_atkedEffRefresh93_m1963000767 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntity::ilo_get_ifattacked94(CombatEntity)
extern "C"  int32_t CombatEntity_ilo_get_ifattacked94_m431682290 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::ilo_normolPalyAttacked95(CombatEntity,FightEnum.EDamageType)
extern "C"  void CombatEntity_ilo_normolPalyAttacked95_m2395926417 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___damageType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::ilo_BreakSkillPalyAttacked96(CombatEntity,FightEnum.EDamageType)
extern "C"  void CombatEntity_ilo_BreakSkillPalyAttacked96_m2042472427 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___damageType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntity::ilo_get_superArmorTime97(CombatEntity)
extern "C"  float CombatEntity_ilo_get_superArmorTime97_m2483377504 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntity::ilo_IsPlay98(AnimationRunner,AnimationRunner/AniType)
extern "C"  bool CombatEntity_ilo_IsPlay98_m3052669483 (Il2CppObject * __this /* static, unused */, AnimationRunner_t1015409588 * ____this0, int32_t ___aniType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntity::ilo_get_isUsingNormalSkill99(FightCtrl)
extern "C"  bool CombatEntity_ilo_get_isUsingNormalSkill99_m278626487 (Il2CppObject * __this /* static, unused */, FightCtrl_t648967803 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::ilo_PlayShoot100(EffectMgr,System.Int32,CombatEntity,CombatEntity,System.Single,System.Boolean,UnityEngine.Vector3,EntityEnum.UnitTag,System.Int32)
extern "C"  void CombatEntity_ilo_PlayShoot100_m3817416643 (Il2CppObject * __this /* static, unused */, EffectMgr_t535289511 * ____this0, int32_t ___id1, CombatEntity_t684137495 * ___srcEntity2, CombatEntity_t684137495 * ___targetTf3, float ___speed4, bool ___isatk5, Vector3_t4282066566  ___targetPosition6, int32_t ___tag7, int32_t ___skillID8, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 CombatEntity::ilo_CalculateVelocity101(AIPath,UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  CombatEntity_ilo_CalculateVelocity101_m208987202 (Il2CppObject * __this /* static, unused */, AIPath_t1930792045 * ____this0, Vector3_t4282066566  ___currentPosition1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::ilo_RotateTowards102(AIPath,UnityEngine.Vector3)
extern "C"  void CombatEntity_ilo_RotateTowards102_m153283435 (Il2CppObject * __this /* static, unused */, AIPath_t1930792045 * ____this0, Vector3_t4282066566  ___dir1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntity::ilo_get_hpLineNum103(CombatEntity)
extern "C"  int32_t CombatEntity_ilo_get_hpLineNum103_m1138795901 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntity::ilo_get_maxHp104(CombatEntity)
extern "C"  float CombatEntity_ilo_get_maxHp104_m4193929594 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntity::ilo_get_hp105(CombatEntity)
extern "C"  float CombatEntity_ilo_get_hp105_m307460113 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntity::ilo_get_hpLineUpBuffID106(CombatEntity)
extern "C"  int32_t CombatEntity_ilo_get_hpLineUpBuffID106_m2298533651 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntity::ilo_get_hpLineCleaveAddBuff107(CombatEntity)
extern "C"  int32_t CombatEntity_ilo_get_hpLineCleaveAddBuff107_m2205805895 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::ilo_PlayHpTextEffect108(CombatEntity,System.Single,HeadUpBloodMgr/LabelType)
extern "C"  void CombatEntity_ilo_PlayHpTextEffect108_m795517842 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___delta1, int32_t ___type2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntity::ilo_get_isHpLineState109(CombatEntity)
extern "C"  bool CombatEntity_ilo_get_isHpLineState109_m3871783368 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntity::ilo_IsHaveBuff110(BuffCtrl,System.Int32)
extern "C"  bool CombatEntity_ilo_IsHaveBuff110_m2830378831 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, int32_t ___buffID1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntity::ilo_get_deathHarvestLine111(CombatEntity)
extern "C"  int32_t CombatEntity_ilo_get_deathHarvestLine111_m2829560041 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntity::ilo_get_antiHurt112(CombatEntity)
extern "C"  bool CombatEntity_ilo_get_antiHurt112_m972226332 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ReplayMgr CombatEntity::ilo_get_Instance113()
extern "C"  ReplayMgr_t1549183121 * CombatEntity_ilo_get_Instance113_m1031420802 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntity::ilo_get_NearDeathProtect114(CombatEntity)
extern "C"  bool CombatEntity_ilo_get_NearDeathProtect114_m1918244172 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<EnemyDropData> CombatEntity::ilo_get_dropRwards115(CombatEntity)
extern "C"  List_1_t2836773265 * CombatEntity_ilo_get_dropRwards115_m2542215982 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TeamSkillMgr CombatEntity::ilo_get_TeamSkillMgr116()
extern "C"  TeamSkillMgr_t2030650276 * CombatEntity_ilo_get_TeamSkillMgr116_m746613919 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntity::ilo_get_isUseingSkill117(TeamSkillMgr)
extern "C"  bool CombatEntity_ilo_get_isUseingSkill117_m875143703 (Il2CppObject * __this /* static, unused */, TeamSkillMgr_t2030650276 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntity::ilo_get_beHurtAnger118(CombatEntity)
extern "C"  int32_t CombatEntity_ilo_get_beHurtAnger118_m3575484502 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::ilo_AddRage119(CombatEntity,System.Single)
extern "C"  void CombatEntity_ilo_AddRage119_m1652645452 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___delta1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::ilo_set_hp120(CombatEntity,System.Single)
extern "C"  void CombatEntity_ilo_set_hp120_m1324510791 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::ilo_StopMove121(CombatEntity,System.Boolean)
extern "C"  void CombatEntity_ilo_StopMove121_m2797614216 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, bool ___isPlayAnim1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::ilo_set_NDPAddBuff122(CombatEntity,System.Int32)
extern "C"  void CombatEntity_ilo_set_NDPAddBuff122_m741294661 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntity::ilo_get_NDPAddBuff123(CombatEntity)
extern "C"  int32_t CombatEntity_ilo_get_NDPAddBuff123_m1189337785 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntity::ilo_get_NDPAddBuff02124(CombatEntity)
extern "C"  int32_t CombatEntity_ilo_get_NDPAddBuff02124_m2663622648 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::ilo_RemoveNDPBuff125(BuffCtrl,System.Int32)
extern "C"  void CombatEntity_ilo_RemoveNDPBuff125_m3194126007 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, int32_t ___id1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::ilo_Dead126(CombatEntity)
extern "C"  void CombatEntity_ilo_Dead126_m1195810633 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntity::ilo_get_isHide127(CombatEntity)
extern "C"  bool CombatEntity_ilo_get_isHide127_m2950006501 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::ilo_AddBlood128(HeadUpBloodMgr,System.Boolean,System.String,HeadUpBloodMgr/LabelType,UnityEngine.Vector3,System.Int32,System.Boolean,System.Int32)
extern "C"  void CombatEntity_ilo_AddBlood128_m3913225237 (Il2CppObject * __this /* static, unused */, HeadUpBloodMgr_t2330866585 * ____this0, bool ___isHero1, String_t* ___value2, int32_t ___type3, Vector3_t4282066566  ___pos4, int32_t ___id5, bool ___isbuff6, int32_t ___index7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HeadUpBloodMgr CombatEntity::ilo_get_Instance129()
extern "C"  HeadUpBloodMgr_t2330866585 * CombatEntity_ilo_get_Instance129_m3570384517 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::ilo_StopCoroutine130(System.UInt32)
extern "C"  void CombatEntity_ilo_StopCoroutine130_m2448272751 (Il2CppObject * __this /* static, unused */, uint32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::ilo_UnBinStaticParticle131(GameQutilyCtr,System.Int32)
extern "C"  void CombatEntity_ilo_UnBinStaticParticle131_m617678798 (Il2CppObject * __this /* static, unused */, GameQutilyCtr_t3963256169 * ____this0, int32_t ___uId1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::ilo_ClearTargetFightPoint132(TargetMgr,CombatEntity)
extern "C"  void CombatEntity_ilo_ClearTargetFightPoint132_m585806889 (Il2CppObject * __this /* static, unused */, TargetMgr_t1188374183 * ____this0, CombatEntity_t684137495 * ____entity1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Hero CombatEntity::ilo_get_captain133(HeroMgr)
extern "C"  Hero_t2245658 * CombatEntity_ilo_get_captain133_m1261908872 (Il2CppObject * __this /* static, unused */, HeroMgr_t2475965342 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntity::ilo_get_DeathLaterHeroID134(CombatEntity)
extern "C"  int32_t CombatEntity_ilo_get_DeathLaterHeroID134_m1847758470 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::ilo_ShootEffect135(CombatEntity)
extern "C"  void CombatEntity_ilo_ShootEffect135_m887499423 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::ilo_AIMonsterDead136(CombatEntity,System.Int32)
extern "C"  void CombatEntity_ilo_AIMonsterDead136_m527250845 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___murderID1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::ilo_RemoveAllStates137(CombatEntity)
extern "C"  void CombatEntity_ilo_RemoveAllStates137_m332394930 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::ilo_Clear138(FightCtrl)
extern "C"  void CombatEntity_ilo_Clear138_m1036306459 (Il2CppObject * __this /* static, unused */, FightCtrl_t648967803 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::ilo_buffClear139(BuffCtrl)
extern "C"  void CombatEntity_ilo_buffClear139_m1360440594 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PlotFightTalkMgr CombatEntity::ilo_get_PlotFightMgr140(CombatEntity)
extern "C"  PlotFightTalkMgr_t866599741 * CombatEntity_ilo_get_PlotFightMgr140_m3040409683 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::ilo_Clear141(PlotFightTalkMgr)
extern "C"  void CombatEntity_ilo_Clear141_m3808052983 (Il2CppObject * __this /* static, unused */, PlotFightTalkMgr_t866599741 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::ilo_set_position142(CombatEntity,UnityEngine.Vector3)
extern "C"  void CombatEntity_ilo_set_position142_m18203508 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, Vector3_t4282066566  ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::ilo_set_deadType143(CombatEntity,EntityEnum.DeadType)
extern "C"  void CombatEntity_ilo_set_deadType143_m2168713969 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::ilo_Clear144(Entity.Behavior.IBehaviorCtrl)
extern "C"  void CombatEntity_ilo_Clear144_m180997958 (Il2CppObject * __this /* static, unused */, IBehaviorCtrl_t4225040900 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::ilo_TranBehavior145(Entity.Behavior.IBehaviorCtrl,Entity.Behavior.EBehaviorID,System.Object[])
extern "C"  void CombatEntity_ilo_TranBehavior145_m983682800 (Il2CppObject * __this /* static, unused */, IBehaviorCtrl_t4225040900 * ____this0, uint8_t ___id1, ObjectU5BU5D_t1108656482* ___arg2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntity::ilo_get_beforModelScale146(CombatEntity)
extern "C"  float CombatEntity_ilo_get_beforModelScale146_m2454517181 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntity::ilo_get_modelScale147(CombatEntity)
extern "C"  float CombatEntity_ilo_get_modelScale147_m1654693270 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntity::ilo_get_curModelScaleValue148(CombatEntity)
extern "C"  float CombatEntity_ilo_get_curModelScaleValue148_m2489916008 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::ilo_set_curModelScaleValue149(CombatEntity,System.Single)
extern "C"  void CombatEntity_ilo_set_curModelScaleValue149_m2944925734 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::ilo_set_modelScaleTween150(CombatEntity,EntityEnum.ModelScaleTween)
extern "C"  void CombatEntity_ilo_set_modelScaleTween150_m2231377099 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::ilo_set_modelScale151(CombatEntity,System.Single)
extern "C"  void CombatEntity_ilo_set_modelScale151_m321298156 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntity::ilo_get_tauntDistance152(CombatEntity)
extern "C"  float CombatEntity_ilo_get_tauntDistance152_m872882236 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntity::ilo_GetTauntHatred153(System.Single,CombatEntity)
extern "C"  float CombatEntity_ilo_GetTauntHatred153_m1676620744 (Il2CppObject * __this /* static, unused */, float ___baseValue0, CombatEntity_t684137495 * ___entity1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntity::ilo_get_HPSecDecPer154(CombatEntity)
extern "C"  float CombatEntity_ilo_get_HPSecDecPer154_m1075242205 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::ilo_ReduceHP155(CombatEntity,System.Single,CombatEntity,FightEnum.EDamageType,System.Boolean,System.Boolean,System.Int32,System.Int32)
extern "C"  void CombatEntity_ilo_ReduceHP155_m325768778 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___delta1, CombatEntity_t684137495 * ___src2, int32_t ___damageType3, bool ___isRepaly4, bool ___isActionSkill5, int32_t ___neardeathprotect6, int32_t ___buffId7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntity::ilo_get_elite156(CombatEntity)
extern "C"  bool CombatEntity_ilo_get_elite156_m785205016 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntity::ilo_get_AuraAddHP157(CombatEntity)
extern "C"  bool CombatEntity_ilo_get_AuraAddHP157_m1923115814 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntity::ilo_get_AuraAddHPRadius158(CombatEntity)
extern "C"  int32_t CombatEntity_ilo_get_AuraAddHPRadius158_m312752143 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntity::ilo_get_AuraAddHPNum159(CombatEntity)
extern "C"  int32_t CombatEntity_ilo_get_AuraAddHPNum159_m3998812854 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntity::ilo_get_AuraAddHPPer160(CombatEntity)
extern "C"  int32_t CombatEntity_ilo_get_AuraAddHPPer160_m1923819413 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::ilo_AddHP161(CombatEntity,System.Single)
extern "C"  void CombatEntity_ilo_AddHP161_m1486849956 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___delta1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntity::ilo_get_AuraReduceHPRadius162(CombatEntity)
extern "C"  int32_t CombatEntity_ilo_get_AuraReduceHPRadius162_m2339616363 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntity::ilo_get_AuraReduceHPNum163(CombatEntity)
extern "C"  int32_t CombatEntity_ilo_get_AuraReduceHPNum163_m3018441708 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntity::ilo_get_AuraReduceHPPer164(CombatEntity)
extern "C"  int32_t CombatEntity_ilo_get_AuraReduceHPPer164_m3644598646 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::ilo_AddSkillHurt165(SkillHurtShow,CombatEntity,CombatEntity,System.Int32)
extern "C"  void CombatEntity_ilo_AddSkillHurt165_m2045467682 (Il2CppObject * __this /* static, unused */, SkillHurtShow_t1728264445 * ____this0, CombatEntity_t684137495 * ___entiy1, CombatEntity_t684137495 * ___monster2, int32_t ___hurt3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntity::ilo_get_HPRstPer166(CombatEntity)
extern "C"  float CombatEntity_ilo_get_HPRstPer166_m4200752162 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntity::ilo_get_HPSecDecNum167(CombatEntity)
extern "C"  float CombatEntity_ilo_get_HPSecDecNum167_m1673791030 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::ilo_CircleHit168(UnityEngine.Vector3,System.Single,CombatEntity,System.Collections.Generic.List`1<CombatEntity>,System.Boolean)
extern "C"  void CombatEntity_ilo_CircleHit168_m3836658310 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___centre0, float ___radius1, CombatEntity_t684137495 * ___entity2, List_1_t2052323047 * ___hitList3, bool ___iswish4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::ilo_ReduceHP169(CombatEntity,System.Single,CombatEntity,FightEnum.EDamageType,System.Boolean,System.Boolean,System.Int32,System.Int32)
extern "C"  void CombatEntity_ilo_ReduceHP169_m532005575 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___delta1, CombatEntity_t684137495 * ___src2, int32_t ___damageType3, bool ___isRepaly4, bool ___isActionSkill5, int32_t ___neardeathprotect6, int32_t ___buffId7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::ilo_PlayBvr170(Entity.Behavior.IBehaviorCtrl)
extern "C"  void CombatEntity_ilo_PlayBvr170_m4201335178 (Il2CppObject * __this /* static, unused */, IBehaviorCtrl_t4225040900 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::ilo_set_petrifiedTween171(CombatEntity,EntityEnum.PetrifiedTween)
extern "C"  void CombatEntity_ilo_set_petrifiedTween171_m4151718370 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::ilo_RefreshShaderProperty172(CombatEntity,System.String,System.Object)
extern "C"  void CombatEntity_ilo_RefreshShaderProperty172_m1527110909 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, String_t* ___key1, Il2CppObject * ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// skillCfg CombatEntity::ilo_get_mpJson173(Skill)
extern "C"  skillCfg_t2142425171 * CombatEntity_ilo_get_mpJson173_m1150822275 (Il2CppObject * __this /* static, unused */, Skill_t79944241 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntity::ilo_get_atkSpeed174(CombatEntity)
extern "C"  float CombatEntity_ilo_get_atkSpeed174_m4248178562 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntity::ilo_get_tankBuffID175(CombatEntity)
extern "C"  int32_t CombatEntity_ilo_get_tankBuffID175_m3484043256 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntity::ilo_get_HTBuffID176(CombatEntity)
extern "C"  int32_t CombatEntity_ilo_get_HTBuffID176_m3914932631 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::ilo_RemoveEventListener177(System.String,CEvent.EventFunc`1<CEvent.ZEvent>)
extern "C"  void CombatEntity_ilo_RemoveEventListener177_m3985780809 (Il2CppObject * __this /* static, unused */, String_t* ___eventType0, EventFunc_1_t1114914310 * ___func1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// EntityEnum.PetrifiedTween CombatEntity::ilo_get_petrifiedTween178(CombatEntity)
extern "C"  int32_t CombatEntity_ilo_get_petrifiedTween178_m4112394648 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntity::ilo_GetPetrifiedLastTime179(BuffCtrl)
extern "C"  bool CombatEntity_ilo_GetPetrifiedLastTime179_m766178571 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntity::ilo_GetGodDownLastTime180(BuffCtrl)
extern "C"  bool CombatEntity_ilo_GetGodDownLastTime180_m3350396763 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntity::ilo_get_runeState181(CombatEntity)
extern "C"  bool CombatEntity_ilo_get_runeState181_m2430330764 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntity::ilo_get_runeStateAllSkill182(CombatEntity)
extern "C"  bool CombatEntity_ilo_get_runeStateAllSkill182_m2255200893 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::ilo_set_runeNum183(CombatEntity,System.Int32)
extern "C"  void CombatEntity_ilo_set_runeNum183_m2954604734 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntity::ilo_get_runeTakeEffectBuffID01184(CombatEntity)
extern "C"  int32_t CombatEntity_ilo_get_runeTakeEffectBuffID01184_m3498162509 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::ilo_set_runeNumAllSkill185(CombatEntity,System.Int32)
extern "C"  void CombatEntity_ilo_set_runeNumAllSkill185_m123209676 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntity::ilo_get_runeTakeEffectBuffID186(CombatEntity)
extern "C"  int32_t CombatEntity_ilo_get_runeTakeEffectBuffID186_m126669680 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntity::ilo_HasBuffState187(BuffCtrl,BuffEnum.EBuffState)
extern "C"  bool CombatEntity_ilo_HasBuffState187_m3043350306 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, uint64_t ___state1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntity::ilo_get_IsMarshalMoving188(CombatEntity)
extern "C"  bool CombatEntity_ilo_get_IsMarshalMoving188_m2089259144 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntity::ilo_CanEnterOtherState189(UnitStateBase,UnitStateBase)
extern "C"  bool CombatEntity_ilo_CanEnterOtherState189_m3473184318 (Il2CppObject * __this /* static, unused */, UnitStateBase_t1040218558 * ____this0, UnitStateBase_t1040218558 * ___state1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::ilo_EnterState190(UnitStateBase)
extern "C"  void CombatEntity_ilo_EnterState190_m3340918518 (Il2CppObject * __this /* static, unused */, UnitStateBase_t1040218558 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::ilo_Update191(UnitStateBase,System.Single)
extern "C"  void CombatEntity_ilo_Update191_m4045245482 (Il2CppObject * __this /* static, unused */, UnitStateBase_t1040218558 * ____this0, float ___deltatime1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntity::ilo_IsFinished192(UnitStateBase)
extern "C"  bool CombatEntity_ilo_IsFinished192_m4042792651 (Il2CppObject * __this /* static, unused */, UnitStateBase_t1040218558 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::ilo_LeaveState193(UnitStateBase)
extern "C"  void CombatEntity_ilo_LeaveState193_m3585426900 (Il2CppObject * __this /* static, unused */, UnitStateBase_t1040218558 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::ilo_ExitGhost194(CombatEntity)
extern "C"  void CombatEntity_ilo_ExitGhost194_m1319535799 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity::ilo_ExitPartBloom195(CombatEntity)
extern "C"  void CombatEntity_ilo_ExitPartBloom195_m3164498391 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String CombatEntity::ilo_get_icon196(CombatEntity)
extern "C"  String_t* CombatEntity_ilo_get_icon196_m2164158433 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mihua.Asset.ABLoadOperation.AssetOperation CombatEntity::ilo_LoadAssetAsync197(System.String,Mihua.Asset.OnLoadAsset)
extern "C"  AssetOperation_t778728221 * CombatEntity_ilo_LoadAssetAsync197_m3529015183 (Il2CppObject * __this /* static, unused */, String_t* ___assetName0, OnLoadAsset_t4181543125 * ___onLoadAsset1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
