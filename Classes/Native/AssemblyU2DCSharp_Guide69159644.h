﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "AssemblyU2DCSharp_AIPath1930792045.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Guide
struct  Guide_t69159644  : public AIPath_t1930792045
{
public:
	// System.Boolean Guide::isPathCom
	bool ___isPathCom_34;

public:
	inline static int32_t get_offset_of_isPathCom_34() { return static_cast<int32_t>(offsetof(Guide_t69159644, ___isPathCom_34)); }
	inline bool get_isPathCom_34() const { return ___isPathCom_34; }
	inline bool* get_address_of_isPathCom_34() { return &___isPathCom_34; }
	inline void set_isPathCom_34(bool value)
	{
		___isPathCom_34 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
