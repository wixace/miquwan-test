﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_daircur34
struct  M_daircur34_t454400315  : public Il2CppObject
{
public:
	// System.Single GarbageiOS.M_daircur34::_dramerelSersaircea
	float ____dramerelSersaircea_0;
	// System.Single GarbageiOS.M_daircur34::_palrai
	float ____palrai_1;
	// System.UInt32 GarbageiOS.M_daircur34::_nalliserJemicir
	uint32_t ____nalliserJemicir_2;
	// System.String GarbageiOS.M_daircur34::_stelalliWhorpo
	String_t* ____stelalliWhorpo_3;
	// System.String GarbageiOS.M_daircur34::_zelouMourowgere
	String_t* ____zelouMourowgere_4;
	// System.Single GarbageiOS.M_daircur34::_fawdokasWeahar
	float ____fawdokasWeahar_5;
	// System.String GarbageiOS.M_daircur34::_poti
	String_t* ____poti_6;
	// System.String GarbageiOS.M_daircur34::_xercurcar
	String_t* ____xercurcar_7;
	// System.Boolean GarbageiOS.M_daircur34::_ribalMisiski
	bool ____ribalMisiski_8;
	// System.Int32 GarbageiOS.M_daircur34::_sahoKerwar
	int32_t ____sahoKerwar_9;
	// System.String GarbageiOS.M_daircur34::_youkeJogu
	String_t* ____youkeJogu_10;
	// System.UInt32 GarbageiOS.M_daircur34::_nayersirCisjooci
	uint32_t ____nayersirCisjooci_11;
	// System.String GarbageiOS.M_daircur34::_cese
	String_t* ____cese_12;
	// System.String GarbageiOS.M_daircur34::_bowbarto
	String_t* ____bowbarto_13;
	// System.Single GarbageiOS.M_daircur34::_curjeeleRemrakar
	float ____curjeeleRemrakar_14;
	// System.String GarbageiOS.M_daircur34::_learcayJursel
	String_t* ____learcayJursel_15;
	// System.Int32 GarbageiOS.M_daircur34::_nibereWaska
	int32_t ____nibereWaska_16;
	// System.String GarbageiOS.M_daircur34::_dehoostearSereri
	String_t* ____dehoostearSereri_17;
	// System.Int32 GarbageiOS.M_daircur34::_rellirWali
	int32_t ____rellirWali_18;
	// System.Single GarbageiOS.M_daircur34::_wairtowterTaifinee
	float ____wairtowterTaifinee_19;
	// System.Int32 GarbageiOS.M_daircur34::_qenehayJaidor
	int32_t ____qenehayJaidor_20;
	// System.Boolean GarbageiOS.M_daircur34::_degirjelMerera
	bool ____degirjelMerera_21;
	// System.UInt32 GarbageiOS.M_daircur34::_vurgem
	uint32_t ____vurgem_22;
	// System.String GarbageiOS.M_daircur34::_nawallbi
	String_t* ____nawallbi_23;
	// System.Boolean GarbageiOS.M_daircur34::_dutu
	bool ____dutu_24;
	// System.Int32 GarbageiOS.M_daircur34::_mertaCousoo
	int32_t ____mertaCousoo_25;
	// System.String GarbageiOS.M_daircur34::_callsatrair
	String_t* ____callsatrair_26;
	// System.UInt32 GarbageiOS.M_daircur34::_leewaJokee
	uint32_t ____leewaJokee_27;
	// System.Int32 GarbageiOS.M_daircur34::_jimirCadayve
	int32_t ____jimirCadayve_28;
	// System.Boolean GarbageiOS.M_daircur34::_keparwar
	bool ____keparwar_29;
	// System.UInt32 GarbageiOS.M_daircur34::_tukal
	uint32_t ____tukal_30;
	// System.Single GarbageiOS.M_daircur34::_deneejir
	float ____deneejir_31;
	// System.Int32 GarbageiOS.M_daircur34::_dive
	int32_t ____dive_32;
	// System.UInt32 GarbageiOS.M_daircur34::_seator
	uint32_t ____seator_33;
	// System.Int32 GarbageiOS.M_daircur34::_remyiHoujir
	int32_t ____remyiHoujir_34;
	// System.Single GarbageiOS.M_daircur34::_masrar
	float ____masrar_35;
	// System.String GarbageiOS.M_daircur34::_tacaiReata
	String_t* ____tacaiReata_36;
	// System.String GarbageiOS.M_daircur34::_diskasTrecapa
	String_t* ____diskasTrecapa_37;

public:
	inline static int32_t get_offset_of__dramerelSersaircea_0() { return static_cast<int32_t>(offsetof(M_daircur34_t454400315, ____dramerelSersaircea_0)); }
	inline float get__dramerelSersaircea_0() const { return ____dramerelSersaircea_0; }
	inline float* get_address_of__dramerelSersaircea_0() { return &____dramerelSersaircea_0; }
	inline void set__dramerelSersaircea_0(float value)
	{
		____dramerelSersaircea_0 = value;
	}

	inline static int32_t get_offset_of__palrai_1() { return static_cast<int32_t>(offsetof(M_daircur34_t454400315, ____palrai_1)); }
	inline float get__palrai_1() const { return ____palrai_1; }
	inline float* get_address_of__palrai_1() { return &____palrai_1; }
	inline void set__palrai_1(float value)
	{
		____palrai_1 = value;
	}

	inline static int32_t get_offset_of__nalliserJemicir_2() { return static_cast<int32_t>(offsetof(M_daircur34_t454400315, ____nalliserJemicir_2)); }
	inline uint32_t get__nalliserJemicir_2() const { return ____nalliserJemicir_2; }
	inline uint32_t* get_address_of__nalliserJemicir_2() { return &____nalliserJemicir_2; }
	inline void set__nalliserJemicir_2(uint32_t value)
	{
		____nalliserJemicir_2 = value;
	}

	inline static int32_t get_offset_of__stelalliWhorpo_3() { return static_cast<int32_t>(offsetof(M_daircur34_t454400315, ____stelalliWhorpo_3)); }
	inline String_t* get__stelalliWhorpo_3() const { return ____stelalliWhorpo_3; }
	inline String_t** get_address_of__stelalliWhorpo_3() { return &____stelalliWhorpo_3; }
	inline void set__stelalliWhorpo_3(String_t* value)
	{
		____stelalliWhorpo_3 = value;
		Il2CppCodeGenWriteBarrier(&____stelalliWhorpo_3, value);
	}

	inline static int32_t get_offset_of__zelouMourowgere_4() { return static_cast<int32_t>(offsetof(M_daircur34_t454400315, ____zelouMourowgere_4)); }
	inline String_t* get__zelouMourowgere_4() const { return ____zelouMourowgere_4; }
	inline String_t** get_address_of__zelouMourowgere_4() { return &____zelouMourowgere_4; }
	inline void set__zelouMourowgere_4(String_t* value)
	{
		____zelouMourowgere_4 = value;
		Il2CppCodeGenWriteBarrier(&____zelouMourowgere_4, value);
	}

	inline static int32_t get_offset_of__fawdokasWeahar_5() { return static_cast<int32_t>(offsetof(M_daircur34_t454400315, ____fawdokasWeahar_5)); }
	inline float get__fawdokasWeahar_5() const { return ____fawdokasWeahar_5; }
	inline float* get_address_of__fawdokasWeahar_5() { return &____fawdokasWeahar_5; }
	inline void set__fawdokasWeahar_5(float value)
	{
		____fawdokasWeahar_5 = value;
	}

	inline static int32_t get_offset_of__poti_6() { return static_cast<int32_t>(offsetof(M_daircur34_t454400315, ____poti_6)); }
	inline String_t* get__poti_6() const { return ____poti_6; }
	inline String_t** get_address_of__poti_6() { return &____poti_6; }
	inline void set__poti_6(String_t* value)
	{
		____poti_6 = value;
		Il2CppCodeGenWriteBarrier(&____poti_6, value);
	}

	inline static int32_t get_offset_of__xercurcar_7() { return static_cast<int32_t>(offsetof(M_daircur34_t454400315, ____xercurcar_7)); }
	inline String_t* get__xercurcar_7() const { return ____xercurcar_7; }
	inline String_t** get_address_of__xercurcar_7() { return &____xercurcar_7; }
	inline void set__xercurcar_7(String_t* value)
	{
		____xercurcar_7 = value;
		Il2CppCodeGenWriteBarrier(&____xercurcar_7, value);
	}

	inline static int32_t get_offset_of__ribalMisiski_8() { return static_cast<int32_t>(offsetof(M_daircur34_t454400315, ____ribalMisiski_8)); }
	inline bool get__ribalMisiski_8() const { return ____ribalMisiski_8; }
	inline bool* get_address_of__ribalMisiski_8() { return &____ribalMisiski_8; }
	inline void set__ribalMisiski_8(bool value)
	{
		____ribalMisiski_8 = value;
	}

	inline static int32_t get_offset_of__sahoKerwar_9() { return static_cast<int32_t>(offsetof(M_daircur34_t454400315, ____sahoKerwar_9)); }
	inline int32_t get__sahoKerwar_9() const { return ____sahoKerwar_9; }
	inline int32_t* get_address_of__sahoKerwar_9() { return &____sahoKerwar_9; }
	inline void set__sahoKerwar_9(int32_t value)
	{
		____sahoKerwar_9 = value;
	}

	inline static int32_t get_offset_of__youkeJogu_10() { return static_cast<int32_t>(offsetof(M_daircur34_t454400315, ____youkeJogu_10)); }
	inline String_t* get__youkeJogu_10() const { return ____youkeJogu_10; }
	inline String_t** get_address_of__youkeJogu_10() { return &____youkeJogu_10; }
	inline void set__youkeJogu_10(String_t* value)
	{
		____youkeJogu_10 = value;
		Il2CppCodeGenWriteBarrier(&____youkeJogu_10, value);
	}

	inline static int32_t get_offset_of__nayersirCisjooci_11() { return static_cast<int32_t>(offsetof(M_daircur34_t454400315, ____nayersirCisjooci_11)); }
	inline uint32_t get__nayersirCisjooci_11() const { return ____nayersirCisjooci_11; }
	inline uint32_t* get_address_of__nayersirCisjooci_11() { return &____nayersirCisjooci_11; }
	inline void set__nayersirCisjooci_11(uint32_t value)
	{
		____nayersirCisjooci_11 = value;
	}

	inline static int32_t get_offset_of__cese_12() { return static_cast<int32_t>(offsetof(M_daircur34_t454400315, ____cese_12)); }
	inline String_t* get__cese_12() const { return ____cese_12; }
	inline String_t** get_address_of__cese_12() { return &____cese_12; }
	inline void set__cese_12(String_t* value)
	{
		____cese_12 = value;
		Il2CppCodeGenWriteBarrier(&____cese_12, value);
	}

	inline static int32_t get_offset_of__bowbarto_13() { return static_cast<int32_t>(offsetof(M_daircur34_t454400315, ____bowbarto_13)); }
	inline String_t* get__bowbarto_13() const { return ____bowbarto_13; }
	inline String_t** get_address_of__bowbarto_13() { return &____bowbarto_13; }
	inline void set__bowbarto_13(String_t* value)
	{
		____bowbarto_13 = value;
		Il2CppCodeGenWriteBarrier(&____bowbarto_13, value);
	}

	inline static int32_t get_offset_of__curjeeleRemrakar_14() { return static_cast<int32_t>(offsetof(M_daircur34_t454400315, ____curjeeleRemrakar_14)); }
	inline float get__curjeeleRemrakar_14() const { return ____curjeeleRemrakar_14; }
	inline float* get_address_of__curjeeleRemrakar_14() { return &____curjeeleRemrakar_14; }
	inline void set__curjeeleRemrakar_14(float value)
	{
		____curjeeleRemrakar_14 = value;
	}

	inline static int32_t get_offset_of__learcayJursel_15() { return static_cast<int32_t>(offsetof(M_daircur34_t454400315, ____learcayJursel_15)); }
	inline String_t* get__learcayJursel_15() const { return ____learcayJursel_15; }
	inline String_t** get_address_of__learcayJursel_15() { return &____learcayJursel_15; }
	inline void set__learcayJursel_15(String_t* value)
	{
		____learcayJursel_15 = value;
		Il2CppCodeGenWriteBarrier(&____learcayJursel_15, value);
	}

	inline static int32_t get_offset_of__nibereWaska_16() { return static_cast<int32_t>(offsetof(M_daircur34_t454400315, ____nibereWaska_16)); }
	inline int32_t get__nibereWaska_16() const { return ____nibereWaska_16; }
	inline int32_t* get_address_of__nibereWaska_16() { return &____nibereWaska_16; }
	inline void set__nibereWaska_16(int32_t value)
	{
		____nibereWaska_16 = value;
	}

	inline static int32_t get_offset_of__dehoostearSereri_17() { return static_cast<int32_t>(offsetof(M_daircur34_t454400315, ____dehoostearSereri_17)); }
	inline String_t* get__dehoostearSereri_17() const { return ____dehoostearSereri_17; }
	inline String_t** get_address_of__dehoostearSereri_17() { return &____dehoostearSereri_17; }
	inline void set__dehoostearSereri_17(String_t* value)
	{
		____dehoostearSereri_17 = value;
		Il2CppCodeGenWriteBarrier(&____dehoostearSereri_17, value);
	}

	inline static int32_t get_offset_of__rellirWali_18() { return static_cast<int32_t>(offsetof(M_daircur34_t454400315, ____rellirWali_18)); }
	inline int32_t get__rellirWali_18() const { return ____rellirWali_18; }
	inline int32_t* get_address_of__rellirWali_18() { return &____rellirWali_18; }
	inline void set__rellirWali_18(int32_t value)
	{
		____rellirWali_18 = value;
	}

	inline static int32_t get_offset_of__wairtowterTaifinee_19() { return static_cast<int32_t>(offsetof(M_daircur34_t454400315, ____wairtowterTaifinee_19)); }
	inline float get__wairtowterTaifinee_19() const { return ____wairtowterTaifinee_19; }
	inline float* get_address_of__wairtowterTaifinee_19() { return &____wairtowterTaifinee_19; }
	inline void set__wairtowterTaifinee_19(float value)
	{
		____wairtowterTaifinee_19 = value;
	}

	inline static int32_t get_offset_of__qenehayJaidor_20() { return static_cast<int32_t>(offsetof(M_daircur34_t454400315, ____qenehayJaidor_20)); }
	inline int32_t get__qenehayJaidor_20() const { return ____qenehayJaidor_20; }
	inline int32_t* get_address_of__qenehayJaidor_20() { return &____qenehayJaidor_20; }
	inline void set__qenehayJaidor_20(int32_t value)
	{
		____qenehayJaidor_20 = value;
	}

	inline static int32_t get_offset_of__degirjelMerera_21() { return static_cast<int32_t>(offsetof(M_daircur34_t454400315, ____degirjelMerera_21)); }
	inline bool get__degirjelMerera_21() const { return ____degirjelMerera_21; }
	inline bool* get_address_of__degirjelMerera_21() { return &____degirjelMerera_21; }
	inline void set__degirjelMerera_21(bool value)
	{
		____degirjelMerera_21 = value;
	}

	inline static int32_t get_offset_of__vurgem_22() { return static_cast<int32_t>(offsetof(M_daircur34_t454400315, ____vurgem_22)); }
	inline uint32_t get__vurgem_22() const { return ____vurgem_22; }
	inline uint32_t* get_address_of__vurgem_22() { return &____vurgem_22; }
	inline void set__vurgem_22(uint32_t value)
	{
		____vurgem_22 = value;
	}

	inline static int32_t get_offset_of__nawallbi_23() { return static_cast<int32_t>(offsetof(M_daircur34_t454400315, ____nawallbi_23)); }
	inline String_t* get__nawallbi_23() const { return ____nawallbi_23; }
	inline String_t** get_address_of__nawallbi_23() { return &____nawallbi_23; }
	inline void set__nawallbi_23(String_t* value)
	{
		____nawallbi_23 = value;
		Il2CppCodeGenWriteBarrier(&____nawallbi_23, value);
	}

	inline static int32_t get_offset_of__dutu_24() { return static_cast<int32_t>(offsetof(M_daircur34_t454400315, ____dutu_24)); }
	inline bool get__dutu_24() const { return ____dutu_24; }
	inline bool* get_address_of__dutu_24() { return &____dutu_24; }
	inline void set__dutu_24(bool value)
	{
		____dutu_24 = value;
	}

	inline static int32_t get_offset_of__mertaCousoo_25() { return static_cast<int32_t>(offsetof(M_daircur34_t454400315, ____mertaCousoo_25)); }
	inline int32_t get__mertaCousoo_25() const { return ____mertaCousoo_25; }
	inline int32_t* get_address_of__mertaCousoo_25() { return &____mertaCousoo_25; }
	inline void set__mertaCousoo_25(int32_t value)
	{
		____mertaCousoo_25 = value;
	}

	inline static int32_t get_offset_of__callsatrair_26() { return static_cast<int32_t>(offsetof(M_daircur34_t454400315, ____callsatrair_26)); }
	inline String_t* get__callsatrair_26() const { return ____callsatrair_26; }
	inline String_t** get_address_of__callsatrair_26() { return &____callsatrair_26; }
	inline void set__callsatrair_26(String_t* value)
	{
		____callsatrair_26 = value;
		Il2CppCodeGenWriteBarrier(&____callsatrair_26, value);
	}

	inline static int32_t get_offset_of__leewaJokee_27() { return static_cast<int32_t>(offsetof(M_daircur34_t454400315, ____leewaJokee_27)); }
	inline uint32_t get__leewaJokee_27() const { return ____leewaJokee_27; }
	inline uint32_t* get_address_of__leewaJokee_27() { return &____leewaJokee_27; }
	inline void set__leewaJokee_27(uint32_t value)
	{
		____leewaJokee_27 = value;
	}

	inline static int32_t get_offset_of__jimirCadayve_28() { return static_cast<int32_t>(offsetof(M_daircur34_t454400315, ____jimirCadayve_28)); }
	inline int32_t get__jimirCadayve_28() const { return ____jimirCadayve_28; }
	inline int32_t* get_address_of__jimirCadayve_28() { return &____jimirCadayve_28; }
	inline void set__jimirCadayve_28(int32_t value)
	{
		____jimirCadayve_28 = value;
	}

	inline static int32_t get_offset_of__keparwar_29() { return static_cast<int32_t>(offsetof(M_daircur34_t454400315, ____keparwar_29)); }
	inline bool get__keparwar_29() const { return ____keparwar_29; }
	inline bool* get_address_of__keparwar_29() { return &____keparwar_29; }
	inline void set__keparwar_29(bool value)
	{
		____keparwar_29 = value;
	}

	inline static int32_t get_offset_of__tukal_30() { return static_cast<int32_t>(offsetof(M_daircur34_t454400315, ____tukal_30)); }
	inline uint32_t get__tukal_30() const { return ____tukal_30; }
	inline uint32_t* get_address_of__tukal_30() { return &____tukal_30; }
	inline void set__tukal_30(uint32_t value)
	{
		____tukal_30 = value;
	}

	inline static int32_t get_offset_of__deneejir_31() { return static_cast<int32_t>(offsetof(M_daircur34_t454400315, ____deneejir_31)); }
	inline float get__deneejir_31() const { return ____deneejir_31; }
	inline float* get_address_of__deneejir_31() { return &____deneejir_31; }
	inline void set__deneejir_31(float value)
	{
		____deneejir_31 = value;
	}

	inline static int32_t get_offset_of__dive_32() { return static_cast<int32_t>(offsetof(M_daircur34_t454400315, ____dive_32)); }
	inline int32_t get__dive_32() const { return ____dive_32; }
	inline int32_t* get_address_of__dive_32() { return &____dive_32; }
	inline void set__dive_32(int32_t value)
	{
		____dive_32 = value;
	}

	inline static int32_t get_offset_of__seator_33() { return static_cast<int32_t>(offsetof(M_daircur34_t454400315, ____seator_33)); }
	inline uint32_t get__seator_33() const { return ____seator_33; }
	inline uint32_t* get_address_of__seator_33() { return &____seator_33; }
	inline void set__seator_33(uint32_t value)
	{
		____seator_33 = value;
	}

	inline static int32_t get_offset_of__remyiHoujir_34() { return static_cast<int32_t>(offsetof(M_daircur34_t454400315, ____remyiHoujir_34)); }
	inline int32_t get__remyiHoujir_34() const { return ____remyiHoujir_34; }
	inline int32_t* get_address_of__remyiHoujir_34() { return &____remyiHoujir_34; }
	inline void set__remyiHoujir_34(int32_t value)
	{
		____remyiHoujir_34 = value;
	}

	inline static int32_t get_offset_of__masrar_35() { return static_cast<int32_t>(offsetof(M_daircur34_t454400315, ____masrar_35)); }
	inline float get__masrar_35() const { return ____masrar_35; }
	inline float* get_address_of__masrar_35() { return &____masrar_35; }
	inline void set__masrar_35(float value)
	{
		____masrar_35 = value;
	}

	inline static int32_t get_offset_of__tacaiReata_36() { return static_cast<int32_t>(offsetof(M_daircur34_t454400315, ____tacaiReata_36)); }
	inline String_t* get__tacaiReata_36() const { return ____tacaiReata_36; }
	inline String_t** get_address_of__tacaiReata_36() { return &____tacaiReata_36; }
	inline void set__tacaiReata_36(String_t* value)
	{
		____tacaiReata_36 = value;
		Il2CppCodeGenWriteBarrier(&____tacaiReata_36, value);
	}

	inline static int32_t get_offset_of__diskasTrecapa_37() { return static_cast<int32_t>(offsetof(M_daircur34_t454400315, ____diskasTrecapa_37)); }
	inline String_t* get__diskasTrecapa_37() const { return ____diskasTrecapa_37; }
	inline String_t** get_address_of__diskasTrecapa_37() { return &____diskasTrecapa_37; }
	inline void set__diskasTrecapa_37(String_t* value)
	{
		____diskasTrecapa_37 = value;
		Il2CppCodeGenWriteBarrier(&____diskasTrecapa_37, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
