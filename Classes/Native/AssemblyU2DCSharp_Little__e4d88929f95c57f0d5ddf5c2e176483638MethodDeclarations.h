﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._e4d88929f95c57f0d5ddf5c2e379a42e
struct _e4d88929f95c57f0d5ddf5c2e379a42e_t176483638;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._e4d88929f95c57f0d5ddf5c2e379a42e::.ctor()
extern "C"  void _e4d88929f95c57f0d5ddf5c2e379a42e__ctor_m3405166007 (_e4d88929f95c57f0d5ddf5c2e379a42e_t176483638 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._e4d88929f95c57f0d5ddf5c2e379a42e::_e4d88929f95c57f0d5ddf5c2e379a42em2(System.Int32)
extern "C"  int32_t _e4d88929f95c57f0d5ddf5c2e379a42e__e4d88929f95c57f0d5ddf5c2e379a42em2_m4076164569 (_e4d88929f95c57f0d5ddf5c2e379a42e_t176483638 * __this, int32_t ____e4d88929f95c57f0d5ddf5c2e379a42ea0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._e4d88929f95c57f0d5ddf5c2e379a42e::_e4d88929f95c57f0d5ddf5c2e379a42em(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _e4d88929f95c57f0d5ddf5c2e379a42e__e4d88929f95c57f0d5ddf5c2e379a42em_m3631782141 (_e4d88929f95c57f0d5ddf5c2e379a42e_t176483638 * __this, int32_t ____e4d88929f95c57f0d5ddf5c2e379a42ea0, int32_t ____e4d88929f95c57f0d5ddf5c2e379a42e981, int32_t ____e4d88929f95c57f0d5ddf5c2e379a42ec2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
