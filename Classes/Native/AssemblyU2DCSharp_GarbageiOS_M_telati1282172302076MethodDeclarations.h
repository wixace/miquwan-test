﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_telati128
struct M_telati128_t2172302076;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_telati1282172302076.h"

// System.Void GarbageiOS.M_telati128::.ctor()
extern "C"  void M_telati128__ctor_m1161693671 (M_telati128_t2172302076 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_telati128::M_walltadrowCotaldou0(System.String[],System.Int32)
extern "C"  void M_telati128_M_walltadrowCotaldou0_m1722399766 (M_telati128_t2172302076 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_telati128::M_sawole1(System.String[],System.Int32)
extern "C"  void M_telati128_M_sawole1_m1399201628 (M_telati128_t2172302076 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_telati128::M_qernawXola2(System.String[],System.Int32)
extern "C"  void M_telati128_M_qernawXola2_m758508170 (M_telati128_t2172302076 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_telati128::M_fihiTresiskir3(System.String[],System.Int32)
extern "C"  void M_telati128_M_fihiTresiskir3_m539283935 (M_telati128_t2172302076 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_telati128::M_yobur4(System.String[],System.Int32)
extern "C"  void M_telati128_M_yobur4_m1684825905 (M_telati128_t2172302076 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_telati128::M_nuyerMemraywhi5(System.String[],System.Int32)
extern "C"  void M_telati128_M_nuyerMemraywhi5_m263662331 (M_telati128_t2172302076 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_telati128::ilo_M_fihiTresiskir31(GarbageiOS.M_telati128,System.String[],System.Int32)
extern "C"  void M_telati128_ilo_M_fihiTresiskir31_m2276420633 (Il2CppObject * __this /* static, unused */, M_telati128_t2172302076 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_telati128::ilo_M_yobur42(GarbageiOS.M_telati128,System.String[],System.Int32)
extern "C"  void M_telati128_ilo_M_yobur42_m4245605642 (Il2CppObject * __this /* static, unused */, M_telati128_t2172302076 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
