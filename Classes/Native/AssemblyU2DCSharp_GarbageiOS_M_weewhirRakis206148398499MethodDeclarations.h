﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_weewhirRakis206
struct M_weewhirRakis206_t148398499;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"

// System.Void GarbageiOS.M_weewhirRakis206::.ctor()
extern "C"  void M_weewhirRakis206__ctor_m1911649568 (M_weewhirRakis206_t148398499 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_weewhirRakis206::M_lezaidall0(System.String[],System.Int32)
extern "C"  void M_weewhirRakis206_M_lezaidall0_m862724375 (M_weewhirRakis206_t148398499 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
