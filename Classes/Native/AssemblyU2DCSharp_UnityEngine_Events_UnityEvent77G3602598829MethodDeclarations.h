﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_Events_UnityEvent77Generated/<UnityEventA1_AddListener__UnityActionT1_T0>c__AnonStoreyE2
struct U3CUnityEventA1_AddListener__UnityActionT1_T0U3Ec__AnonStoreyE2_t3602598829;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine_Events_UnityEvent77Generated/<UnityEventA1_AddListener__UnityActionT1_T0>c__AnonStoreyE2::.ctor()
extern "C"  void U3CUnityEventA1_AddListener__UnityActionT1_T0U3Ec__AnonStoreyE2__ctor_m1316942222 (U3CUnityEventA1_AddListener__UnityActionT1_T0U3Ec__AnonStoreyE2_t3602598829 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_Events_UnityEvent77Generated/<UnityEventA1_AddListener__UnityActionT1_T0>c__AnonStoreyE2::<>m__1AD()
extern "C"  Il2CppObject * U3CUnityEventA1_AddListener__UnityActionT1_T0U3Ec__AnonStoreyE2_U3CU3Em__1AD_m3261560244 (U3CUnityEventA1_AddListener__UnityActionT1_T0U3Ec__AnonStoreyE2_t3602598829 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
