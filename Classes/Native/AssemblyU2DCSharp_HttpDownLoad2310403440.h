﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Threading.Thread
struct Thread_t1973216770;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HttpDownLoad
struct  HttpDownLoad_t2310403440  : public Il2CppObject
{
public:
	// System.Int32 HttpDownLoad::speed
	int32_t ___speed_0;
	// System.Boolean HttpDownLoad::isStop
	bool ___isStop_1;
	// System.Threading.Thread HttpDownLoad::thread
	Thread_t1973216770 * ___thread_2;
	// System.Single HttpDownLoad::<progress>k__BackingField
	float ___U3CprogressU3Ek__BackingField_3;
	// System.Boolean HttpDownLoad::<isDone>k__BackingField
	bool ___U3CisDoneU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_speed_0() { return static_cast<int32_t>(offsetof(HttpDownLoad_t2310403440, ___speed_0)); }
	inline int32_t get_speed_0() const { return ___speed_0; }
	inline int32_t* get_address_of_speed_0() { return &___speed_0; }
	inline void set_speed_0(int32_t value)
	{
		___speed_0 = value;
	}

	inline static int32_t get_offset_of_isStop_1() { return static_cast<int32_t>(offsetof(HttpDownLoad_t2310403440, ___isStop_1)); }
	inline bool get_isStop_1() const { return ___isStop_1; }
	inline bool* get_address_of_isStop_1() { return &___isStop_1; }
	inline void set_isStop_1(bool value)
	{
		___isStop_1 = value;
	}

	inline static int32_t get_offset_of_thread_2() { return static_cast<int32_t>(offsetof(HttpDownLoad_t2310403440, ___thread_2)); }
	inline Thread_t1973216770 * get_thread_2() const { return ___thread_2; }
	inline Thread_t1973216770 ** get_address_of_thread_2() { return &___thread_2; }
	inline void set_thread_2(Thread_t1973216770 * value)
	{
		___thread_2 = value;
		Il2CppCodeGenWriteBarrier(&___thread_2, value);
	}

	inline static int32_t get_offset_of_U3CprogressU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(HttpDownLoad_t2310403440, ___U3CprogressU3Ek__BackingField_3)); }
	inline float get_U3CprogressU3Ek__BackingField_3() const { return ___U3CprogressU3Ek__BackingField_3; }
	inline float* get_address_of_U3CprogressU3Ek__BackingField_3() { return &___U3CprogressU3Ek__BackingField_3; }
	inline void set_U3CprogressU3Ek__BackingField_3(float value)
	{
		___U3CprogressU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CisDoneU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(HttpDownLoad_t2310403440, ___U3CisDoneU3Ek__BackingField_4)); }
	inline bool get_U3CisDoneU3Ek__BackingField_4() const { return ___U3CisDoneU3Ek__BackingField_4; }
	inline bool* get_address_of_U3CisDoneU3Ek__BackingField_4() { return &___U3CisDoneU3Ek__BackingField_4; }
	inline void set_U3CisDoneU3Ek__BackingField_4(bool value)
	{
		___U3CisDoneU3Ek__BackingField_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
