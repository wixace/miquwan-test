﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<Core.RpsResult>
struct Collection_1_t3960020278;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// Core.RpsResult[]
struct RpsResultU5BU5D_t1451851929;
// System.Collections.Generic.IEnumerator`1<Core.RpsResult>
struct IEnumerator_1_t2391459873;
// System.Collections.Generic.IList`1<Core.RpsResult>
struct IList_1_t3174242027;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_Core_RpsResult479594824.h"

// System.Void System.Collections.ObjectModel.Collection`1<Core.RpsResult>::.ctor()
extern "C"  void Collection_1__ctor_m3918250922_gshared (Collection_1_t3960020278 * __this, const MethodInfo* method);
#define Collection_1__ctor_m3918250922(__this, method) ((  void (*) (Collection_1_t3960020278 *, const MethodInfo*))Collection_1__ctor_m3918250922_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Core.RpsResult>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m152745805_gshared (Collection_1_t3960020278 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m152745805(__this, method) ((  bool (*) (Collection_1_t3960020278 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m152745805_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<Core.RpsResult>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Collection_1_System_Collections_ICollection_CopyTo_m3190822106_gshared (Collection_1_t3960020278 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m3190822106(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t3960020278 *, Il2CppArray *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m3190822106_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<Core.RpsResult>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Collection_1_System_Collections_IEnumerable_GetEnumerator_m2232680809_gshared (Collection_1_t3960020278 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m2232680809(__this, method) ((  Il2CppObject * (*) (Collection_1_t3960020278 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m2232680809_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Core.RpsResult>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_Add_m1918692212_gshared (Collection_1_t3960020278 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m1918692212(__this, ___value0, method) ((  int32_t (*) (Collection_1_t3960020278 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m1918692212_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Core.RpsResult>::System.Collections.IList.Contains(System.Object)
extern "C"  bool Collection_1_System_Collections_IList_Contains_m3457496652_gshared (Collection_1_t3960020278 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m3457496652(__this, ___value0, method) ((  bool (*) (Collection_1_t3960020278 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m3457496652_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Core.RpsResult>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_IndexOf_m2308065228_gshared (Collection_1_t3960020278 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m2308065228(__this, ___value0, method) ((  int32_t (*) (Collection_1_t3960020278 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m2308065228_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Core.RpsResult>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_Insert_m2989477695_gshared (Collection_1_t3960020278 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m2989477695(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t3960020278 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m2989477695_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<Core.RpsResult>::System.Collections.IList.Remove(System.Object)
extern "C"  void Collection_1_System_Collections_IList_Remove_m2039021449_gshared (Collection_1_t3960020278 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m2039021449(__this, ___value0, method) ((  void (*) (Collection_1_t3960020278 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m2039021449_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Core.RpsResult>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m372043152_gshared (Collection_1_t3960020278 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m372043152(__this, method) ((  bool (*) (Collection_1_t3960020278 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m372043152_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<Core.RpsResult>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Collection_1_System_Collections_ICollection_get_SyncRoot_m1396067586_gshared (Collection_1_t3960020278 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m1396067586(__this, method) ((  Il2CppObject * (*) (Collection_1_t3960020278 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m1396067586_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Core.RpsResult>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool Collection_1_System_Collections_IList_get_IsFixedSize_m4171665083_gshared (Collection_1_t3960020278 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m4171665083(__this, method) ((  bool (*) (Collection_1_t3960020278 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m4171665083_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Core.RpsResult>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_IList_get_IsReadOnly_m4190809310_gshared (Collection_1_t3960020278 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m4190809310(__this, method) ((  bool (*) (Collection_1_t3960020278 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m4190809310_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<Core.RpsResult>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * Collection_1_System_Collections_IList_get_Item_m2656765449_gshared (Collection_1_t3960020278 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m2656765449(__this, ___index0, method) ((  Il2CppObject * (*) (Collection_1_t3960020278 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m2656765449_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Core.RpsResult>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_set_Item_m168896342_gshared (Collection_1_t3960020278 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m168896342(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t3960020278 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m168896342_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<Core.RpsResult>::Add(T)
extern "C"  void Collection_1_Add_m2056200085_gshared (Collection_1_t3960020278 * __this, int32_t ___item0, const MethodInfo* method);
#define Collection_1_Add_m2056200085(__this, ___item0, method) ((  void (*) (Collection_1_t3960020278 *, int32_t, const MethodInfo*))Collection_1_Add_m2056200085_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Core.RpsResult>::Clear()
extern "C"  void Collection_1_Clear_m1324384213_gshared (Collection_1_t3960020278 * __this, const MethodInfo* method);
#define Collection_1_Clear_m1324384213(__this, method) ((  void (*) (Collection_1_t3960020278 *, const MethodInfo*))Collection_1_Clear_m1324384213_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<Core.RpsResult>::ClearItems()
extern "C"  void Collection_1_ClearItems_m23772749_gshared (Collection_1_t3960020278 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m23772749(__this, method) ((  void (*) (Collection_1_t3960020278 *, const MethodInfo*))Collection_1_ClearItems_m23772749_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Core.RpsResult>::Contains(T)
extern "C"  bool Collection_1_Contains_m132421447_gshared (Collection_1_t3960020278 * __this, int32_t ___item0, const MethodInfo* method);
#define Collection_1_Contains_m132421447(__this, ___item0, method) ((  bool (*) (Collection_1_t3960020278 *, int32_t, const MethodInfo*))Collection_1_Contains_m132421447_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Core.RpsResult>::CopyTo(T[],System.Int32)
extern "C"  void Collection_1_CopyTo_m3086363717_gshared (Collection_1_t3960020278 * __this, RpsResultU5BU5D_t1451851929* ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_CopyTo_m3086363717(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t3960020278 *, RpsResultU5BU5D_t1451851929*, int32_t, const MethodInfo*))Collection_1_CopyTo_m3086363717_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<Core.RpsResult>::GetEnumerator()
extern "C"  Il2CppObject* Collection_1_GetEnumerator_m4084326430_gshared (Collection_1_t3960020278 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m4084326430(__this, method) ((  Il2CppObject* (*) (Collection_1_t3960020278 *, const MethodInfo*))Collection_1_GetEnumerator_m4084326430_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Core.RpsResult>::IndexOf(T)
extern "C"  int32_t Collection_1_IndexOf_m4207667409_gshared (Collection_1_t3960020278 * __this, int32_t ___item0, const MethodInfo* method);
#define Collection_1_IndexOf_m4207667409(__this, ___item0, method) ((  int32_t (*) (Collection_1_t3960020278 *, int32_t, const MethodInfo*))Collection_1_IndexOf_m4207667409_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Core.RpsResult>::Insert(System.Int32,T)
extern "C"  void Collection_1_Insert_m1061982204_gshared (Collection_1_t3960020278 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define Collection_1_Insert_m1061982204(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t3960020278 *, int32_t, int32_t, const MethodInfo*))Collection_1_Insert_m1061982204_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.ObjectModel.Collection`1<Core.RpsResult>::InsertItem(System.Int32,T)
extern "C"  void Collection_1_InsertItem_m134438959_gshared (Collection_1_t3960020278 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define Collection_1_InsertItem_m134438959(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t3960020278 *, int32_t, int32_t, const MethodInfo*))Collection_1_InsertItem_m134438959_gshared)(__this, ___index0, ___item1, method)
// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.Collection`1<Core.RpsResult>::get_Items()
extern "C"  Il2CppObject* Collection_1_get_Items_m1146335765_gshared (Collection_1_t3960020278 * __this, const MethodInfo* method);
#define Collection_1_get_Items_m1146335765(__this, method) ((  Il2CppObject* (*) (Collection_1_t3960020278 *, const MethodInfo*))Collection_1_get_Items_m1146335765_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Core.RpsResult>::Remove(T)
extern "C"  bool Collection_1_Remove_m437550338_gshared (Collection_1_t3960020278 * __this, int32_t ___item0, const MethodInfo* method);
#define Collection_1_Remove_m437550338(__this, ___item0, method) ((  bool (*) (Collection_1_t3960020278 *, int32_t, const MethodInfo*))Collection_1_Remove_m437550338_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Core.RpsResult>::RemoveAt(System.Int32)
extern "C"  void Collection_1_RemoveAt_m3230802370_gshared (Collection_1_t3960020278 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveAt_m3230802370(__this, ___index0, method) ((  void (*) (Collection_1_t3960020278 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m3230802370_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Core.RpsResult>::RemoveItem(System.Int32)
extern "C"  void Collection_1_RemoveItem_m2439347554_gshared (Collection_1_t3960020278 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveItem_m2439347554(__this, ___index0, method) ((  void (*) (Collection_1_t3960020278 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m2439347554_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Core.RpsResult>::get_Count()
extern "C"  int32_t Collection_1_get_Count_m1405978314_gshared (Collection_1_t3960020278 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m1405978314(__this, method) ((  int32_t (*) (Collection_1_t3960020278 *, const MethodInfo*))Collection_1_get_Count_m1405978314_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<Core.RpsResult>::get_Item(System.Int32)
extern "C"  int32_t Collection_1_get_Item_m3620902504_gshared (Collection_1_t3960020278 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_get_Item_m3620902504(__this, ___index0, method) ((  int32_t (*) (Collection_1_t3960020278 *, int32_t, const MethodInfo*))Collection_1_get_Item_m3620902504_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Core.RpsResult>::set_Item(System.Int32,T)
extern "C"  void Collection_1_set_Item_m172278227_gshared (Collection_1_t3960020278 * __this, int32_t ___index0, int32_t ___value1, const MethodInfo* method);
#define Collection_1_set_Item_m172278227(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t3960020278 *, int32_t, int32_t, const MethodInfo*))Collection_1_set_Item_m172278227_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<Core.RpsResult>::SetItem(System.Int32,T)
extern "C"  void Collection_1_SetItem_m4173267014_gshared (Collection_1_t3960020278 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define Collection_1_SetItem_m4173267014(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t3960020278 *, int32_t, int32_t, const MethodInfo*))Collection_1_SetItem_m4173267014_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Core.RpsResult>::IsValidItem(System.Object)
extern "C"  bool Collection_1_IsValidItem_m3113989_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_IsValidItem_m3113989(__this /* static, unused */, ___item0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_IsValidItem_m3113989_gshared)(__this /* static, unused */, ___item0, method)
// T System.Collections.ObjectModel.Collection`1<Core.RpsResult>::ConvertItem(System.Object)
extern "C"  int32_t Collection_1_ConvertItem_m965398855_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_ConvertItem_m965398855(__this /* static, unused */, ___item0, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_ConvertItem_m965398855_gshared)(__this /* static, unused */, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Core.RpsResult>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C"  void Collection_1_CheckWritable_m1147194501_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_CheckWritable_m1147194501(__this /* static, unused */, ___list0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_CheckWritable_m1147194501_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Core.RpsResult>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsSynchronized_m1568269215_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsSynchronized_m1568269215(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsSynchronized_m1568269215_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Core.RpsResult>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsFixedSize_m2755469152_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsFixedSize_m2755469152(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsFixedSize_m2755469152_gshared)(__this /* static, unused */, ___list0, method)
