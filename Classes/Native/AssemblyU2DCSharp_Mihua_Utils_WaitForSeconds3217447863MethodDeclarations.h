﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mihua.Utils.WaitForSeconds
struct WaitForSeconds_t3217447863;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Mihua.Utils.WaitForSeconds::.ctor()
extern "C"  void WaitForSeconds__ctor_m1561580541 (WaitForSeconds_t3217447863 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua.Utils.WaitForSeconds::.ctor(System.Single)
extern "C"  void WaitForSeconds__ctor_m2190364878 (WaitForSeconds_t3217447863 * __this, float ___seconds0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua.Utils.WaitForSeconds::System.Collections.IEnumerator.Reset()
extern "C"  void WaitForSeconds_System_Collections_IEnumerator_Reset_m551313757 (WaitForSeconds_t3217447863 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua.Utils.WaitForSeconds::Mihua.Utils.IObject.Reset()
extern "C"  void WaitForSeconds_Mihua_Utils_IObject_Reset_m949880721 (WaitForSeconds_t3217447863 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Mihua.Utils.WaitForSeconds::get_lastSeconds()
extern "C"  float WaitForSeconds_get_lastSeconds_m883355407 (WaitForSeconds_t3217447863 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua.Utils.WaitForSeconds::set_lastSeconds(System.Single)
extern "C"  void WaitForSeconds_set_lastSeconds_m1483999396 (WaitForSeconds_t3217447863 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Mihua.Utils.WaitForSeconds::get_Current()
extern "C"  Il2CppObject * WaitForSeconds_get_Current_m1756002838 (WaitForSeconds_t3217447863 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mihua.Utils.WaitForSeconds::MoveNext()
extern "C"  bool WaitForSeconds_MoveNext_m215501759 (WaitForSeconds_t3217447863 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mihua.Utils.WaitForSeconds::get_ispause()
extern "C"  bool WaitForSeconds_get_ispause_m1572683754 (WaitForSeconds_t3217447863 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua.Utils.WaitForSeconds::set_ispause(System.Boolean)
extern "C"  void WaitForSeconds_set_ispause_m645465633 (WaitForSeconds_t3217447863 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua.Utils.WaitForSeconds::Update()
extern "C"  void WaitForSeconds_Update_m2891218608 (WaitForSeconds_t3217447863 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mihua.Utils.WaitForSeconds::IsDone()
extern "C"  bool WaitForSeconds_IsDone_m1450456487 (WaitForSeconds_t3217447863 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua.Utils.WaitForSeconds::Cleanup()
extern "C"  void WaitForSeconds_Cleanup_m109107263 (WaitForSeconds_t3217447863 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
