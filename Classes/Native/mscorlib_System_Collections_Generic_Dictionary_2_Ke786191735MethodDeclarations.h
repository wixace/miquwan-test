﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K2135812561MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,Mihua.Assets.SubAssetMgr/ErrorInfo>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define KeyCollection__ctor_m3244373854(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t786191735 *, Dictionary_2_t3454399580 *, const MethodInfo*))KeyCollection__ctor_m3187802608_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1301970744(__this, ___item0, method) ((  void (*) (KeyCollection_t786191735 *, String_t*, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3980779878_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.Generic.ICollection<TKey>.Clear()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1248268975(__this, method) ((  void (*) (KeyCollection_t786191735 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m4165019741_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.String,Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1379957782(__this, ___item0, method) ((  bool (*) (KeyCollection_t786191735 *, String_t*, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1323386536_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.String,Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2978544379(__this, ___item0, method) ((  bool (*) (KeyCollection_t786191735 *, String_t*, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2594128397_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.String,Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m2573010625(__this, method) ((  Il2CppObject* (*) (KeyCollection_t786191735 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m819301999_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define KeyCollection_System_Collections_ICollection_CopyTo_m3931072673(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t786191735 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m2109761871_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.String,Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.IEnumerable.GetEnumerator()
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m472855024(__this, method) ((  Il2CppObject * (*) (KeyCollection_t786191735 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1711186462_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.String,Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m1875592695(__this, method) ((  bool (*) (KeyCollection_t786191735 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m3345200137_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.String,Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.ICollection.get_IsSynchronized()
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m121046889(__this, method) ((  bool (*) (KeyCollection_t786191735 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m4094460923_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.String,Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.ICollection.get_SyncRoot()
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m2956229915(__this, method) ((  Il2CppObject * (*) (KeyCollection_t786191735 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m1472155437_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,Mihua.Assets.SubAssetMgr/ErrorInfo>::CopyTo(TKey[],System.Int32)
#define KeyCollection_CopyTo_m1855165971(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t786191735 *, StringU5BU5D_t4054002952*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m3639814309_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.String,Mihua.Assets.SubAssetMgr/ErrorInfo>::GetEnumerator()
#define KeyCollection_GetEnumerator_m3998070560(__this, method) ((  Enumerator_t4069335634  (*) (KeyCollection_t786191735 *, const MethodInfo*))KeyCollection_GetEnumerator_m504829106_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.String,Mihua.Assets.SubAssetMgr/ErrorInfo>::get_Count()
#define KeyCollection_get_Count_m3431231139(__this, method) ((  int32_t (*) (KeyCollection_t786191735 *, const MethodInfo*))KeyCollection_get_Count_m2393859381_gshared)(__this, method)
