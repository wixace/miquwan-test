﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_qeaherner85
struct  M_qeaherner85_t2981716208  : public Il2CppObject
{
public:
	// System.Int32 GarbageiOS.M_qeaherner85::_xubel
	int32_t ____xubel_0;
	// System.Int32 GarbageiOS.M_qeaherner85::_ceahuLike
	int32_t ____ceahuLike_1;
	// System.Int32 GarbageiOS.M_qeaherner85::_puray
	int32_t ____puray_2;
	// System.Int32 GarbageiOS.M_qeaherner85::_curjir
	int32_t ____curjir_3;
	// System.Single GarbageiOS.M_qeaherner85::_ralmay
	float ____ralmay_4;
	// System.Boolean GarbageiOS.M_qeaherner85::_pearjow
	bool ____pearjow_5;
	// System.String GarbageiOS.M_qeaherner85::_zopalltee
	String_t* ____zopalltee_6;
	// System.String GarbageiOS.M_qeaherner85::_houkearNasci
	String_t* ____houkearNasci_7;
	// System.String GarbageiOS.M_qeaherner85::_buwirJejow
	String_t* ____buwirJejow_8;
	// System.Single GarbageiOS.M_qeaherner85::_rirereHersa
	float ____rirereHersa_9;
	// System.Single GarbageiOS.M_qeaherner85::_trisraSouvelaw
	float ____trisraSouvelaw_10;
	// System.Single GarbageiOS.M_qeaherner85::_cirgeRearla
	float ____cirgeRearla_11;
	// System.Int32 GarbageiOS.M_qeaherner85::_zischeanaiSokonou
	int32_t ____zischeanaiSokonou_12;
	// System.Int32 GarbageiOS.M_qeaherner85::_yeardallKesawhe
	int32_t ____yeardallKesawhe_13;
	// System.Int32 GarbageiOS.M_qeaherner85::_nerasJajoumel
	int32_t ____nerasJajoumel_14;
	// System.Single GarbageiOS.M_qeaherner85::_powtonoSifel
	float ____powtonoSifel_15;
	// System.Single GarbageiOS.M_qeaherner85::_nufereTrarkuce
	float ____nufereTrarkuce_16;
	// System.Single GarbageiOS.M_qeaherner85::_chardelqou
	float ____chardelqou_17;
	// System.UInt32 GarbageiOS.M_qeaherner85::_naneseaWemto
	uint32_t ____naneseaWemto_18;
	// System.Int32 GarbageiOS.M_qeaherner85::_drowkaStimow
	int32_t ____drowkaStimow_19;
	// System.String GarbageiOS.M_qeaherner85::_muhesaiTesere
	String_t* ____muhesaiTesere_20;
	// System.Boolean GarbageiOS.M_qeaherner85::_churlooSowdris
	bool ____churlooSowdris_21;
	// System.Int32 GarbageiOS.M_qeaherner85::_heetolisHorwair
	int32_t ____heetolisHorwair_22;
	// System.String GarbageiOS.M_qeaherner85::_diyou
	String_t* ____diyou_23;
	// System.Int32 GarbageiOS.M_qeaherner85::_bago
	int32_t ____bago_24;
	// System.UInt32 GarbageiOS.M_qeaherner85::_cairnibe
	uint32_t ____cairnibe_25;
	// System.Single GarbageiOS.M_qeaherner85::_whokerroBardallnaw
	float ____whokerroBardallnaw_26;
	// System.String GarbageiOS.M_qeaherner85::_korhanu
	String_t* ____korhanu_27;
	// System.Boolean GarbageiOS.M_qeaherner85::_xepooJalsar
	bool ____xepooJalsar_28;
	// System.String GarbageiOS.M_qeaherner85::_jamo
	String_t* ____jamo_29;
	// System.String GarbageiOS.M_qeaherner85::_lemawStujinas
	String_t* ____lemawStujinas_30;
	// System.UInt32 GarbageiOS.M_qeaherner85::_zarsir
	uint32_t ____zarsir_31;
	// System.UInt32 GarbageiOS.M_qeaherner85::_watertreeWayzota
	uint32_t ____watertreeWayzota_32;
	// System.String GarbageiOS.M_qeaherner85::_trekasHijaihis
	String_t* ____trekasHijaihis_33;
	// System.Int32 GarbageiOS.M_qeaherner85::_loxere
	int32_t ____loxere_34;
	// System.Boolean GarbageiOS.M_qeaherner85::_jawchoval
	bool ____jawchoval_35;
	// System.Single GarbageiOS.M_qeaherner85::_trasnaCaci
	float ____trasnaCaci_36;
	// System.Boolean GarbageiOS.M_qeaherner85::_sallpeballTairkee
	bool ____sallpeballTairkee_37;
	// System.Boolean GarbageiOS.M_qeaherner85::_sugou
	bool ____sugou_38;
	// System.Int32 GarbageiOS.M_qeaherner85::_hesurjiJeamir
	int32_t ____hesurjiJeamir_39;
	// System.Single GarbageiOS.M_qeaherner85::_nisbeame
	float ____nisbeame_40;

public:
	inline static int32_t get_offset_of__xubel_0() { return static_cast<int32_t>(offsetof(M_qeaherner85_t2981716208, ____xubel_0)); }
	inline int32_t get__xubel_0() const { return ____xubel_0; }
	inline int32_t* get_address_of__xubel_0() { return &____xubel_0; }
	inline void set__xubel_0(int32_t value)
	{
		____xubel_0 = value;
	}

	inline static int32_t get_offset_of__ceahuLike_1() { return static_cast<int32_t>(offsetof(M_qeaherner85_t2981716208, ____ceahuLike_1)); }
	inline int32_t get__ceahuLike_1() const { return ____ceahuLike_1; }
	inline int32_t* get_address_of__ceahuLike_1() { return &____ceahuLike_1; }
	inline void set__ceahuLike_1(int32_t value)
	{
		____ceahuLike_1 = value;
	}

	inline static int32_t get_offset_of__puray_2() { return static_cast<int32_t>(offsetof(M_qeaherner85_t2981716208, ____puray_2)); }
	inline int32_t get__puray_2() const { return ____puray_2; }
	inline int32_t* get_address_of__puray_2() { return &____puray_2; }
	inline void set__puray_2(int32_t value)
	{
		____puray_2 = value;
	}

	inline static int32_t get_offset_of__curjir_3() { return static_cast<int32_t>(offsetof(M_qeaherner85_t2981716208, ____curjir_3)); }
	inline int32_t get__curjir_3() const { return ____curjir_3; }
	inline int32_t* get_address_of__curjir_3() { return &____curjir_3; }
	inline void set__curjir_3(int32_t value)
	{
		____curjir_3 = value;
	}

	inline static int32_t get_offset_of__ralmay_4() { return static_cast<int32_t>(offsetof(M_qeaherner85_t2981716208, ____ralmay_4)); }
	inline float get__ralmay_4() const { return ____ralmay_4; }
	inline float* get_address_of__ralmay_4() { return &____ralmay_4; }
	inline void set__ralmay_4(float value)
	{
		____ralmay_4 = value;
	}

	inline static int32_t get_offset_of__pearjow_5() { return static_cast<int32_t>(offsetof(M_qeaherner85_t2981716208, ____pearjow_5)); }
	inline bool get__pearjow_5() const { return ____pearjow_5; }
	inline bool* get_address_of__pearjow_5() { return &____pearjow_5; }
	inline void set__pearjow_5(bool value)
	{
		____pearjow_5 = value;
	}

	inline static int32_t get_offset_of__zopalltee_6() { return static_cast<int32_t>(offsetof(M_qeaherner85_t2981716208, ____zopalltee_6)); }
	inline String_t* get__zopalltee_6() const { return ____zopalltee_6; }
	inline String_t** get_address_of__zopalltee_6() { return &____zopalltee_6; }
	inline void set__zopalltee_6(String_t* value)
	{
		____zopalltee_6 = value;
		Il2CppCodeGenWriteBarrier(&____zopalltee_6, value);
	}

	inline static int32_t get_offset_of__houkearNasci_7() { return static_cast<int32_t>(offsetof(M_qeaherner85_t2981716208, ____houkearNasci_7)); }
	inline String_t* get__houkearNasci_7() const { return ____houkearNasci_7; }
	inline String_t** get_address_of__houkearNasci_7() { return &____houkearNasci_7; }
	inline void set__houkearNasci_7(String_t* value)
	{
		____houkearNasci_7 = value;
		Il2CppCodeGenWriteBarrier(&____houkearNasci_7, value);
	}

	inline static int32_t get_offset_of__buwirJejow_8() { return static_cast<int32_t>(offsetof(M_qeaherner85_t2981716208, ____buwirJejow_8)); }
	inline String_t* get__buwirJejow_8() const { return ____buwirJejow_8; }
	inline String_t** get_address_of__buwirJejow_8() { return &____buwirJejow_8; }
	inline void set__buwirJejow_8(String_t* value)
	{
		____buwirJejow_8 = value;
		Il2CppCodeGenWriteBarrier(&____buwirJejow_8, value);
	}

	inline static int32_t get_offset_of__rirereHersa_9() { return static_cast<int32_t>(offsetof(M_qeaherner85_t2981716208, ____rirereHersa_9)); }
	inline float get__rirereHersa_9() const { return ____rirereHersa_9; }
	inline float* get_address_of__rirereHersa_9() { return &____rirereHersa_9; }
	inline void set__rirereHersa_9(float value)
	{
		____rirereHersa_9 = value;
	}

	inline static int32_t get_offset_of__trisraSouvelaw_10() { return static_cast<int32_t>(offsetof(M_qeaherner85_t2981716208, ____trisraSouvelaw_10)); }
	inline float get__trisraSouvelaw_10() const { return ____trisraSouvelaw_10; }
	inline float* get_address_of__trisraSouvelaw_10() { return &____trisraSouvelaw_10; }
	inline void set__trisraSouvelaw_10(float value)
	{
		____trisraSouvelaw_10 = value;
	}

	inline static int32_t get_offset_of__cirgeRearla_11() { return static_cast<int32_t>(offsetof(M_qeaherner85_t2981716208, ____cirgeRearla_11)); }
	inline float get__cirgeRearla_11() const { return ____cirgeRearla_11; }
	inline float* get_address_of__cirgeRearla_11() { return &____cirgeRearla_11; }
	inline void set__cirgeRearla_11(float value)
	{
		____cirgeRearla_11 = value;
	}

	inline static int32_t get_offset_of__zischeanaiSokonou_12() { return static_cast<int32_t>(offsetof(M_qeaherner85_t2981716208, ____zischeanaiSokonou_12)); }
	inline int32_t get__zischeanaiSokonou_12() const { return ____zischeanaiSokonou_12; }
	inline int32_t* get_address_of__zischeanaiSokonou_12() { return &____zischeanaiSokonou_12; }
	inline void set__zischeanaiSokonou_12(int32_t value)
	{
		____zischeanaiSokonou_12 = value;
	}

	inline static int32_t get_offset_of__yeardallKesawhe_13() { return static_cast<int32_t>(offsetof(M_qeaherner85_t2981716208, ____yeardallKesawhe_13)); }
	inline int32_t get__yeardallKesawhe_13() const { return ____yeardallKesawhe_13; }
	inline int32_t* get_address_of__yeardallKesawhe_13() { return &____yeardallKesawhe_13; }
	inline void set__yeardallKesawhe_13(int32_t value)
	{
		____yeardallKesawhe_13 = value;
	}

	inline static int32_t get_offset_of__nerasJajoumel_14() { return static_cast<int32_t>(offsetof(M_qeaherner85_t2981716208, ____nerasJajoumel_14)); }
	inline int32_t get__nerasJajoumel_14() const { return ____nerasJajoumel_14; }
	inline int32_t* get_address_of__nerasJajoumel_14() { return &____nerasJajoumel_14; }
	inline void set__nerasJajoumel_14(int32_t value)
	{
		____nerasJajoumel_14 = value;
	}

	inline static int32_t get_offset_of__powtonoSifel_15() { return static_cast<int32_t>(offsetof(M_qeaherner85_t2981716208, ____powtonoSifel_15)); }
	inline float get__powtonoSifel_15() const { return ____powtonoSifel_15; }
	inline float* get_address_of__powtonoSifel_15() { return &____powtonoSifel_15; }
	inline void set__powtonoSifel_15(float value)
	{
		____powtonoSifel_15 = value;
	}

	inline static int32_t get_offset_of__nufereTrarkuce_16() { return static_cast<int32_t>(offsetof(M_qeaherner85_t2981716208, ____nufereTrarkuce_16)); }
	inline float get__nufereTrarkuce_16() const { return ____nufereTrarkuce_16; }
	inline float* get_address_of__nufereTrarkuce_16() { return &____nufereTrarkuce_16; }
	inline void set__nufereTrarkuce_16(float value)
	{
		____nufereTrarkuce_16 = value;
	}

	inline static int32_t get_offset_of__chardelqou_17() { return static_cast<int32_t>(offsetof(M_qeaherner85_t2981716208, ____chardelqou_17)); }
	inline float get__chardelqou_17() const { return ____chardelqou_17; }
	inline float* get_address_of__chardelqou_17() { return &____chardelqou_17; }
	inline void set__chardelqou_17(float value)
	{
		____chardelqou_17 = value;
	}

	inline static int32_t get_offset_of__naneseaWemto_18() { return static_cast<int32_t>(offsetof(M_qeaherner85_t2981716208, ____naneseaWemto_18)); }
	inline uint32_t get__naneseaWemto_18() const { return ____naneseaWemto_18; }
	inline uint32_t* get_address_of__naneseaWemto_18() { return &____naneseaWemto_18; }
	inline void set__naneseaWemto_18(uint32_t value)
	{
		____naneseaWemto_18 = value;
	}

	inline static int32_t get_offset_of__drowkaStimow_19() { return static_cast<int32_t>(offsetof(M_qeaherner85_t2981716208, ____drowkaStimow_19)); }
	inline int32_t get__drowkaStimow_19() const { return ____drowkaStimow_19; }
	inline int32_t* get_address_of__drowkaStimow_19() { return &____drowkaStimow_19; }
	inline void set__drowkaStimow_19(int32_t value)
	{
		____drowkaStimow_19 = value;
	}

	inline static int32_t get_offset_of__muhesaiTesere_20() { return static_cast<int32_t>(offsetof(M_qeaherner85_t2981716208, ____muhesaiTesere_20)); }
	inline String_t* get__muhesaiTesere_20() const { return ____muhesaiTesere_20; }
	inline String_t** get_address_of__muhesaiTesere_20() { return &____muhesaiTesere_20; }
	inline void set__muhesaiTesere_20(String_t* value)
	{
		____muhesaiTesere_20 = value;
		Il2CppCodeGenWriteBarrier(&____muhesaiTesere_20, value);
	}

	inline static int32_t get_offset_of__churlooSowdris_21() { return static_cast<int32_t>(offsetof(M_qeaherner85_t2981716208, ____churlooSowdris_21)); }
	inline bool get__churlooSowdris_21() const { return ____churlooSowdris_21; }
	inline bool* get_address_of__churlooSowdris_21() { return &____churlooSowdris_21; }
	inline void set__churlooSowdris_21(bool value)
	{
		____churlooSowdris_21 = value;
	}

	inline static int32_t get_offset_of__heetolisHorwair_22() { return static_cast<int32_t>(offsetof(M_qeaherner85_t2981716208, ____heetolisHorwair_22)); }
	inline int32_t get__heetolisHorwair_22() const { return ____heetolisHorwair_22; }
	inline int32_t* get_address_of__heetolisHorwair_22() { return &____heetolisHorwair_22; }
	inline void set__heetolisHorwair_22(int32_t value)
	{
		____heetolisHorwair_22 = value;
	}

	inline static int32_t get_offset_of__diyou_23() { return static_cast<int32_t>(offsetof(M_qeaherner85_t2981716208, ____diyou_23)); }
	inline String_t* get__diyou_23() const { return ____diyou_23; }
	inline String_t** get_address_of__diyou_23() { return &____diyou_23; }
	inline void set__diyou_23(String_t* value)
	{
		____diyou_23 = value;
		Il2CppCodeGenWriteBarrier(&____diyou_23, value);
	}

	inline static int32_t get_offset_of__bago_24() { return static_cast<int32_t>(offsetof(M_qeaherner85_t2981716208, ____bago_24)); }
	inline int32_t get__bago_24() const { return ____bago_24; }
	inline int32_t* get_address_of__bago_24() { return &____bago_24; }
	inline void set__bago_24(int32_t value)
	{
		____bago_24 = value;
	}

	inline static int32_t get_offset_of__cairnibe_25() { return static_cast<int32_t>(offsetof(M_qeaherner85_t2981716208, ____cairnibe_25)); }
	inline uint32_t get__cairnibe_25() const { return ____cairnibe_25; }
	inline uint32_t* get_address_of__cairnibe_25() { return &____cairnibe_25; }
	inline void set__cairnibe_25(uint32_t value)
	{
		____cairnibe_25 = value;
	}

	inline static int32_t get_offset_of__whokerroBardallnaw_26() { return static_cast<int32_t>(offsetof(M_qeaherner85_t2981716208, ____whokerroBardallnaw_26)); }
	inline float get__whokerroBardallnaw_26() const { return ____whokerroBardallnaw_26; }
	inline float* get_address_of__whokerroBardallnaw_26() { return &____whokerroBardallnaw_26; }
	inline void set__whokerroBardallnaw_26(float value)
	{
		____whokerroBardallnaw_26 = value;
	}

	inline static int32_t get_offset_of__korhanu_27() { return static_cast<int32_t>(offsetof(M_qeaherner85_t2981716208, ____korhanu_27)); }
	inline String_t* get__korhanu_27() const { return ____korhanu_27; }
	inline String_t** get_address_of__korhanu_27() { return &____korhanu_27; }
	inline void set__korhanu_27(String_t* value)
	{
		____korhanu_27 = value;
		Il2CppCodeGenWriteBarrier(&____korhanu_27, value);
	}

	inline static int32_t get_offset_of__xepooJalsar_28() { return static_cast<int32_t>(offsetof(M_qeaherner85_t2981716208, ____xepooJalsar_28)); }
	inline bool get__xepooJalsar_28() const { return ____xepooJalsar_28; }
	inline bool* get_address_of__xepooJalsar_28() { return &____xepooJalsar_28; }
	inline void set__xepooJalsar_28(bool value)
	{
		____xepooJalsar_28 = value;
	}

	inline static int32_t get_offset_of__jamo_29() { return static_cast<int32_t>(offsetof(M_qeaherner85_t2981716208, ____jamo_29)); }
	inline String_t* get__jamo_29() const { return ____jamo_29; }
	inline String_t** get_address_of__jamo_29() { return &____jamo_29; }
	inline void set__jamo_29(String_t* value)
	{
		____jamo_29 = value;
		Il2CppCodeGenWriteBarrier(&____jamo_29, value);
	}

	inline static int32_t get_offset_of__lemawStujinas_30() { return static_cast<int32_t>(offsetof(M_qeaherner85_t2981716208, ____lemawStujinas_30)); }
	inline String_t* get__lemawStujinas_30() const { return ____lemawStujinas_30; }
	inline String_t** get_address_of__lemawStujinas_30() { return &____lemawStujinas_30; }
	inline void set__lemawStujinas_30(String_t* value)
	{
		____lemawStujinas_30 = value;
		Il2CppCodeGenWriteBarrier(&____lemawStujinas_30, value);
	}

	inline static int32_t get_offset_of__zarsir_31() { return static_cast<int32_t>(offsetof(M_qeaherner85_t2981716208, ____zarsir_31)); }
	inline uint32_t get__zarsir_31() const { return ____zarsir_31; }
	inline uint32_t* get_address_of__zarsir_31() { return &____zarsir_31; }
	inline void set__zarsir_31(uint32_t value)
	{
		____zarsir_31 = value;
	}

	inline static int32_t get_offset_of__watertreeWayzota_32() { return static_cast<int32_t>(offsetof(M_qeaherner85_t2981716208, ____watertreeWayzota_32)); }
	inline uint32_t get__watertreeWayzota_32() const { return ____watertreeWayzota_32; }
	inline uint32_t* get_address_of__watertreeWayzota_32() { return &____watertreeWayzota_32; }
	inline void set__watertreeWayzota_32(uint32_t value)
	{
		____watertreeWayzota_32 = value;
	}

	inline static int32_t get_offset_of__trekasHijaihis_33() { return static_cast<int32_t>(offsetof(M_qeaherner85_t2981716208, ____trekasHijaihis_33)); }
	inline String_t* get__trekasHijaihis_33() const { return ____trekasHijaihis_33; }
	inline String_t** get_address_of__trekasHijaihis_33() { return &____trekasHijaihis_33; }
	inline void set__trekasHijaihis_33(String_t* value)
	{
		____trekasHijaihis_33 = value;
		Il2CppCodeGenWriteBarrier(&____trekasHijaihis_33, value);
	}

	inline static int32_t get_offset_of__loxere_34() { return static_cast<int32_t>(offsetof(M_qeaherner85_t2981716208, ____loxere_34)); }
	inline int32_t get__loxere_34() const { return ____loxere_34; }
	inline int32_t* get_address_of__loxere_34() { return &____loxere_34; }
	inline void set__loxere_34(int32_t value)
	{
		____loxere_34 = value;
	}

	inline static int32_t get_offset_of__jawchoval_35() { return static_cast<int32_t>(offsetof(M_qeaherner85_t2981716208, ____jawchoval_35)); }
	inline bool get__jawchoval_35() const { return ____jawchoval_35; }
	inline bool* get_address_of__jawchoval_35() { return &____jawchoval_35; }
	inline void set__jawchoval_35(bool value)
	{
		____jawchoval_35 = value;
	}

	inline static int32_t get_offset_of__trasnaCaci_36() { return static_cast<int32_t>(offsetof(M_qeaherner85_t2981716208, ____trasnaCaci_36)); }
	inline float get__trasnaCaci_36() const { return ____trasnaCaci_36; }
	inline float* get_address_of__trasnaCaci_36() { return &____trasnaCaci_36; }
	inline void set__trasnaCaci_36(float value)
	{
		____trasnaCaci_36 = value;
	}

	inline static int32_t get_offset_of__sallpeballTairkee_37() { return static_cast<int32_t>(offsetof(M_qeaherner85_t2981716208, ____sallpeballTairkee_37)); }
	inline bool get__sallpeballTairkee_37() const { return ____sallpeballTairkee_37; }
	inline bool* get_address_of__sallpeballTairkee_37() { return &____sallpeballTairkee_37; }
	inline void set__sallpeballTairkee_37(bool value)
	{
		____sallpeballTairkee_37 = value;
	}

	inline static int32_t get_offset_of__sugou_38() { return static_cast<int32_t>(offsetof(M_qeaherner85_t2981716208, ____sugou_38)); }
	inline bool get__sugou_38() const { return ____sugou_38; }
	inline bool* get_address_of__sugou_38() { return &____sugou_38; }
	inline void set__sugou_38(bool value)
	{
		____sugou_38 = value;
	}

	inline static int32_t get_offset_of__hesurjiJeamir_39() { return static_cast<int32_t>(offsetof(M_qeaherner85_t2981716208, ____hesurjiJeamir_39)); }
	inline int32_t get__hesurjiJeamir_39() const { return ____hesurjiJeamir_39; }
	inline int32_t* get_address_of__hesurjiJeamir_39() { return &____hesurjiJeamir_39; }
	inline void set__hesurjiJeamir_39(int32_t value)
	{
		____hesurjiJeamir_39 = value;
	}

	inline static int32_t get_offset_of__nisbeame_40() { return static_cast<int32_t>(offsetof(M_qeaherner85_t2981716208, ____nisbeame_40)); }
	inline float get__nisbeame_40() const { return ____nisbeame_40; }
	inline float* get_address_of__nisbeame_40() { return &____nisbeame_40; }
	inline void set__nisbeame_40(float value)
	{
		____nisbeame_40 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
