﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<PointCloudRegognizer/Point>
struct List_1_t3207017302;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3226690072.h"
#include "AssemblyU2DCSharp_PointCloudRegognizer_Point1838831750.h"

// System.Void System.Collections.Generic.List`1/Enumerator<PointCloudRegognizer/Point>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m3135825689_gshared (Enumerator_t3226690072 * __this, List_1_t3207017302 * ___l0, const MethodInfo* method);
#define Enumerator__ctor_m3135825689(__this, ___l0, method) ((  void (*) (Enumerator_t3226690072 *, List_1_t3207017302 *, const MethodInfo*))Enumerator__ctor_m3135825689_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<PointCloudRegognizer/Point>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2578866585_gshared (Enumerator_t3226690072 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m2578866585(__this, method) ((  void (*) (Enumerator_t3226690072 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2578866585_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<PointCloudRegognizer/Point>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3402409871_gshared (Enumerator_t3226690072 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m3402409871(__this, method) ((  Il2CppObject * (*) (Enumerator_t3226690072 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3402409871_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<PointCloudRegognizer/Point>::Dispose()
extern "C"  void Enumerator_Dispose_m132778814_gshared (Enumerator_t3226690072 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m132778814(__this, method) ((  void (*) (Enumerator_t3226690072 *, const MethodInfo*))Enumerator_Dispose_m132778814_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<PointCloudRegognizer/Point>::VerifyState()
extern "C"  void Enumerator_VerifyState_m176814967_gshared (Enumerator_t3226690072 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m176814967(__this, method) ((  void (*) (Enumerator_t3226690072 *, const MethodInfo*))Enumerator_VerifyState_m176814967_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<PointCloudRegognizer/Point>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3403296841_gshared (Enumerator_t3226690072 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m3403296841(__this, method) ((  bool (*) (Enumerator_t3226690072 *, const MethodInfo*))Enumerator_MoveNext_m3403296841_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<PointCloudRegognizer/Point>::get_Current()
extern "C"  Point_t1838831750  Enumerator_get_Current_m1890343376_gshared (Enumerator_t3226690072 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m1890343376(__this, method) ((  Point_t1838831750  (*) (Enumerator_t3226690072 *, const MethodInfo*))Enumerator_get_Current_m1890343376_gshared)(__this, method)
