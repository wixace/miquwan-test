﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ProtoBuf.Serializers.MemberSpecifiedDecorator
struct MemberSpecifiedDecorator_t3989848127;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// ProtoBuf.Serializers.IProtoSerializer
struct IProtoSerializer_t3033312651;
// System.Type
struct Type_t;
// System.Object
struct Il2CppObject;
// ProtoBuf.ProtoWriter
struct ProtoWriter_t4117914721;
// ProtoBuf.ProtoReader
struct ProtoReader_t3962509489;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_MethodInfo318736065.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_ProtoBuf_ProtoWriter4117914721.h"
#include "AssemblyU2DCSharp_ProtoBuf_ProtoReader3962509489.h"

// System.Void ProtoBuf.Serializers.MemberSpecifiedDecorator::.ctor(System.Reflection.MethodInfo,System.Reflection.MethodInfo,ProtoBuf.Serializers.IProtoSerializer)
extern "C"  void MemberSpecifiedDecorator__ctor_m3903356699 (MemberSpecifiedDecorator_t3989848127 * __this, MethodInfo_t * ___getSpecified0, MethodInfo_t * ___setSpecified1, Il2CppObject * ___tail2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type ProtoBuf.Serializers.MemberSpecifiedDecorator::get_ExpectedType()
extern "C"  Type_t * MemberSpecifiedDecorator_get_ExpectedType_m4143929679 (MemberSpecifiedDecorator_t3989848127 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Serializers.MemberSpecifiedDecorator::get_RequiresOldValue()
extern "C"  bool MemberSpecifiedDecorator_get_RequiresOldValue_m3315495355 (MemberSpecifiedDecorator_t3989848127 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Serializers.MemberSpecifiedDecorator::get_ReturnsValue()
extern "C"  bool MemberSpecifiedDecorator_get_ReturnsValue_m2360768977 (MemberSpecifiedDecorator_t3989848127 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Serializers.MemberSpecifiedDecorator::Write(System.Object,ProtoBuf.ProtoWriter)
extern "C"  void MemberSpecifiedDecorator_Write_m3503887213 (MemberSpecifiedDecorator_t3989848127 * __this, Il2CppObject * ___value0, ProtoWriter_t4117914721 * ___dest1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProtoBuf.Serializers.MemberSpecifiedDecorator::Read(System.Object,ProtoBuf.ProtoReader)
extern "C"  Il2CppObject * MemberSpecifiedDecorator_Read_m1837006585 (MemberSpecifiedDecorator_t3989848127 * __this, Il2CppObject * ___value0, ProtoReader_t3962509489 * ___source1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Serializers.MemberSpecifiedDecorator::ilo_get_ReturnsValue1(ProtoBuf.Serializers.IProtoSerializer)
extern "C"  bool MemberSpecifiedDecorator_ilo_get_ReturnsValue1_m2839623618 (Il2CppObject * __this /* static, unused */, Il2CppObject * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Serializers.MemberSpecifiedDecorator::ilo_Write2(ProtoBuf.Serializers.IProtoSerializer,System.Object,ProtoBuf.ProtoWriter)
extern "C"  void MemberSpecifiedDecorator_ilo_Write2_m1735900387 (Il2CppObject * __this /* static, unused */, Il2CppObject * ____this0, Il2CppObject * ___value1, ProtoWriter_t4117914721 * ___dest2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProtoBuf.Serializers.MemberSpecifiedDecorator::ilo_Read3(ProtoBuf.Serializers.IProtoSerializer,System.Object,ProtoBuf.ProtoReader)
extern "C"  Il2CppObject * MemberSpecifiedDecorator_ilo_Read3_m2268151730 (Il2CppObject * __this /* static, unused */, Il2CppObject * ____this0, Il2CppObject * ___value1, ProtoReader_t3962509489 * ___source2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
