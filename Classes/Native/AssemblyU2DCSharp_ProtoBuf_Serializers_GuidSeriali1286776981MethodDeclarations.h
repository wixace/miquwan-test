﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ProtoBuf.Serializers.GuidSerializer
struct GuidSerializer_t1286776981;
// ProtoBuf.Meta.TypeModel
struct TypeModel_t2730011105;
// System.Type
struct Type_t;
// System.Object
struct Il2CppObject;
// ProtoBuf.ProtoWriter
struct ProtoWriter_t4117914721;
// ProtoBuf.ProtoReader
struct ProtoReader_t3962509489;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ProtoBuf_Meta_TypeModel2730011105.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_ProtoBuf_ProtoWriter4117914721.h"
#include "AssemblyU2DCSharp_ProtoBuf_ProtoReader3962509489.h"
#include "mscorlib_System_Guid2862754429.h"

// System.Void ProtoBuf.Serializers.GuidSerializer::.ctor(ProtoBuf.Meta.TypeModel)
extern "C"  void GuidSerializer__ctor_m3912780169 (GuidSerializer_t1286776981 * __this, TypeModel_t2730011105 * ___model0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Serializers.GuidSerializer::.cctor()
extern "C"  void GuidSerializer__cctor_m846677453 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Serializers.GuidSerializer::ProtoBuf.Serializers.IProtoSerializer.get_RequiresOldValue()
extern "C"  bool GuidSerializer_ProtoBuf_Serializers_IProtoSerializer_get_RequiresOldValue_m3628343166 (GuidSerializer_t1286776981 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Serializers.GuidSerializer::ProtoBuf.Serializers.IProtoSerializer.get_ReturnsValue()
extern "C"  bool GuidSerializer_ProtoBuf_Serializers_IProtoSerializer_get_ReturnsValue_m159875348 (GuidSerializer_t1286776981 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type ProtoBuf.Serializers.GuidSerializer::get_ExpectedType()
extern "C"  Type_t * GuidSerializer_get_ExpectedType_m3243003941 (GuidSerializer_t1286776981 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Serializers.GuidSerializer::Write(System.Object,ProtoBuf.ProtoWriter)
extern "C"  void GuidSerializer_Write_m2746159959 (GuidSerializer_t1286776981 * __this, Il2CppObject * ___value0, ProtoWriter_t4117914721 * ___dest1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProtoBuf.Serializers.GuidSerializer::Read(System.Object,ProtoBuf.ProtoReader)
extern "C"  Il2CppObject * GuidSerializer_Read_m4107821967 (GuidSerializer_t1286776981 * __this, Il2CppObject * ___value0, ProtoReader_t3962509489 * ___source1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Guid ProtoBuf.Serializers.GuidSerializer::ilo_ReadGuid1(ProtoBuf.ProtoReader)
extern "C"  Guid_t2862754429  GuidSerializer_ilo_ReadGuid1_m141000480 (Il2CppObject * __this /* static, unused */, ProtoReader_t3962509489 * ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
