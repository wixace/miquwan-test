﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// portalCfg
struct portalCfg_t1117034584;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;

#include "codegen/il2cpp-codegen.h"

// System.Void portalCfg::.ctor()
extern "C"  void portalCfg__ctor_m1970298899 (portalCfg_t1117034584 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void portalCfg::Init(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C"  void portalCfg_Init_m1599750802 (portalCfg_t1117034584 * __this, Dictionary_2_t827649927 * ____info0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
