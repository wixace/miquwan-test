﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// CombatEntity
struct CombatEntity_t684137495;
// System.Collections.Generic.List`1<Skill>
struct List_1_t1448129793;
// Skill
struct Skill_t79944241;
// System.String[]
struct StringU5BU5D_t4054002952;
// System.Single[]
struct SingleU5BU5D_t2316563989;
// System.Collections.Generic.List`1<CombatEntity>
struct List_1_t2052323047;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// System.Collections.Generic.List`1<System.UInt32>
struct List_1_t1392853533;
// ProgressBar
struct ProgressBar_t2799378054;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t1355284822;
// System.Collections.Generic.List`1<DamageSeparationSkill>
struct List_1_t3369319916;
// DamageSeparationSkill
struct DamageSeparationSkill_t2001134364;
// CEvent.ZEvent
struct ZEvent_t3638018500;

#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_FightEnum_ESkillState1611731219.h"
#include "AssemblyU2DCSharp_FightEnum_ESkillStateEvent785526141.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FightCtrl
struct  FightCtrl_t648967803  : public Il2CppObject
{
public:
	// CombatEntity FightCtrl::entity
	CombatEntity_t684137495 * ___entity_0;
	// System.Collections.Generic.List`1<Skill> FightCtrl::skillList
	List_1_t1448129793 * ___skillList_1;
	// System.Int32 FightCtrl::lastUseSkillID
	int32_t ___lastUseSkillID_2;
	// System.Int32 FightCtrl::AInextUseSkillIndex
	int32_t ___AInextUseSkillIndex_3;
	// System.Int32 FightCtrl::nextUseSkillIndex
	int32_t ___nextUseSkillIndex_4;
	// System.Int32 FightCtrl::curUseSkillIndex
	int32_t ___curUseSkillIndex_5;
	// Skill FightCtrl::curUseSkill
	Skill_t79944241 * ___curUseSkill_6;
	// UnityEngine.Vector3 FightCtrl::curSkillUsePos
	Vector3_t4282066566  ___curSkillUsePos_7;
	// System.String[] FightCtrl::curSkillActions
	StringU5BU5D_t4054002952* ___curSkillActions_8;
	// System.Single[] FightCtrl::curSkillActionTimes
	SingleU5BU5D_t2316563989* ___curSkillActionTimes_9;
	// System.Int32 FightCtrl::curSkillActionIndex
	int32_t ___curSkillActionIndex_10;
	// System.Single FightCtrl::curSkillActionStartTime
	float ___curSkillActionStartTime_11;
	// FightEnum.ESkillState FightCtrl::skillState
	int32_t ___skillState_12;
	// FightEnum.ESkillStateEvent FightCtrl::skillEffectState
	int32_t ___skillEffectState_13;
	// System.Single FightCtrl::realUseSkillTime
	float ___realUseSkillTime_14;
	// System.Single FightCtrl::curTime
	float ___curTime_15;
	// System.Single FightCtrl::skillRstTime
	float ___skillRstTime_16;
	// System.Single FightCtrl::shootSkillRstTime
	float ___shootSkillRstTime_17;
	// System.Single FightCtrl::skillRstEndTime
	float ___skillRstEndTime_18;
	// System.Single FightCtrl::endUseSkillTime
	float ___endUseSkillTime_19;
	// System.Int32 FightCtrl::activecount
	int32_t ___activecount_20;
	// System.Boolean FightCtrl::isSkillBreak
	bool ___isSkillBreak_21;
	// UnityEngine.Vector3 FightCtrl::center
	Vector3_t4282066566  ___center_22;
	// UnityEngine.Vector3 FightCtrl::lastCenter
	Vector3_t4282066566  ___lastCenter_23;
	// CombatEntity FightCtrl::atkTarget
	CombatEntity_t684137495 * ___atkTarget_24;
	// System.Collections.Generic.List`1<CombatEntity> FightCtrl::hitList
	List_1_t2052323047 * ___hitList_25;
	// System.Collections.Generic.List`1<CombatEntity> FightCtrl::sputterList
	List_1_t2052323047 * ___sputterList_26;
	// System.Boolean FightCtrl::isUsingCD
	bool ___isUsingCD_27;
	// System.Boolean FightCtrl::isProBar
	bool ___isProBar_28;
	// System.Int32 FightCtrl::curSoundIndex
	int32_t ___curSoundIndex_29;
	// System.Int32[] FightCtrl::curSoundID
	Int32U5BU5D_t3230847821* ___curSoundID_30;
	// System.Single[] FightCtrl::curSoundTimes
	SingleU5BU5D_t2316563989* ___curSoundTimes_31;
	// System.Single FightCtrl::curSoundStartTime
	float ___curSoundStartTime_32;
	// System.Collections.Generic.List`1<System.UInt32> FightCtrl::allDelayID
	List_1_t1392853533 * ___allDelayID_33;
	// System.Single FightCtrl::progressTime
	float ___progressTime_34;
	// ProgressBar FightCtrl::pb
	ProgressBar_t2799378054 * ___pb_35;
	// UnityEngine.Vector3 FightCtrl::SummontargetPos
	Vector3_t4282066566  ___SummontargetPos_36;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> FightCtrl::SummontPos
	List_1_t1355284822 * ___SummontPos_37;
	// System.Int32 FightCtrl::summonindex
	int32_t ___summonindex_38;
	// System.Int32 FightCtrl::effectCount
	int32_t ___effectCount_39;
	// System.Boolean FightCtrl::isLastHurt
	bool ___isLastHurt_40;
	// System.Collections.Generic.List`1<DamageSeparationSkill> FightCtrl::damageSeparationSkillList
	List_1_t3369319916 * ___damageSeparationSkillList_41;
	// DamageSeparationSkill FightCtrl::curDamageSeparationSkill
	DamageSeparationSkill_t2001134364 * ___curDamageSeparationSkill_42;
	// CEvent.ZEvent FightCtrl::guideUsingSkill_EV
	ZEvent_t3638018500 * ___guideUsingSkill_EV_43;
	// CEvent.ZEvent FightCtrl::skillEndEv
	ZEvent_t3638018500 * ___skillEndEv_44;
	// UnityEngine.Vector3 FightCtrl::BeforPlaySkillPos
	Vector3_t4282066566  ___BeforPlaySkillPos_45;
	// Skill FightCtrl::<activeSkill>k__BackingField
	Skill_t79944241 * ___U3CactiveSkillU3Ek__BackingField_46;
	// Skill FightCtrl::<normalAttackSkill>k__BackingField
	Skill_t79944241 * ___U3CnormalAttackSkillU3Ek__BackingField_47;
	// Skill FightCtrl::<deathSkill>k__BackingField
	Skill_t79944241 * ___U3CdeathSkillU3Ek__BackingField_48;
	// Skill FightCtrl::<wakeSkill>k__BackingField
	Skill_t79944241 * ___U3CwakeSkillU3Ek__BackingField_49;

public:
	inline static int32_t get_offset_of_entity_0() { return static_cast<int32_t>(offsetof(FightCtrl_t648967803, ___entity_0)); }
	inline CombatEntity_t684137495 * get_entity_0() const { return ___entity_0; }
	inline CombatEntity_t684137495 ** get_address_of_entity_0() { return &___entity_0; }
	inline void set_entity_0(CombatEntity_t684137495 * value)
	{
		___entity_0 = value;
		Il2CppCodeGenWriteBarrier(&___entity_0, value);
	}

	inline static int32_t get_offset_of_skillList_1() { return static_cast<int32_t>(offsetof(FightCtrl_t648967803, ___skillList_1)); }
	inline List_1_t1448129793 * get_skillList_1() const { return ___skillList_1; }
	inline List_1_t1448129793 ** get_address_of_skillList_1() { return &___skillList_1; }
	inline void set_skillList_1(List_1_t1448129793 * value)
	{
		___skillList_1 = value;
		Il2CppCodeGenWriteBarrier(&___skillList_1, value);
	}

	inline static int32_t get_offset_of_lastUseSkillID_2() { return static_cast<int32_t>(offsetof(FightCtrl_t648967803, ___lastUseSkillID_2)); }
	inline int32_t get_lastUseSkillID_2() const { return ___lastUseSkillID_2; }
	inline int32_t* get_address_of_lastUseSkillID_2() { return &___lastUseSkillID_2; }
	inline void set_lastUseSkillID_2(int32_t value)
	{
		___lastUseSkillID_2 = value;
	}

	inline static int32_t get_offset_of_AInextUseSkillIndex_3() { return static_cast<int32_t>(offsetof(FightCtrl_t648967803, ___AInextUseSkillIndex_3)); }
	inline int32_t get_AInextUseSkillIndex_3() const { return ___AInextUseSkillIndex_3; }
	inline int32_t* get_address_of_AInextUseSkillIndex_3() { return &___AInextUseSkillIndex_3; }
	inline void set_AInextUseSkillIndex_3(int32_t value)
	{
		___AInextUseSkillIndex_3 = value;
	}

	inline static int32_t get_offset_of_nextUseSkillIndex_4() { return static_cast<int32_t>(offsetof(FightCtrl_t648967803, ___nextUseSkillIndex_4)); }
	inline int32_t get_nextUseSkillIndex_4() const { return ___nextUseSkillIndex_4; }
	inline int32_t* get_address_of_nextUseSkillIndex_4() { return &___nextUseSkillIndex_4; }
	inline void set_nextUseSkillIndex_4(int32_t value)
	{
		___nextUseSkillIndex_4 = value;
	}

	inline static int32_t get_offset_of_curUseSkillIndex_5() { return static_cast<int32_t>(offsetof(FightCtrl_t648967803, ___curUseSkillIndex_5)); }
	inline int32_t get_curUseSkillIndex_5() const { return ___curUseSkillIndex_5; }
	inline int32_t* get_address_of_curUseSkillIndex_5() { return &___curUseSkillIndex_5; }
	inline void set_curUseSkillIndex_5(int32_t value)
	{
		___curUseSkillIndex_5 = value;
	}

	inline static int32_t get_offset_of_curUseSkill_6() { return static_cast<int32_t>(offsetof(FightCtrl_t648967803, ___curUseSkill_6)); }
	inline Skill_t79944241 * get_curUseSkill_6() const { return ___curUseSkill_6; }
	inline Skill_t79944241 ** get_address_of_curUseSkill_6() { return &___curUseSkill_6; }
	inline void set_curUseSkill_6(Skill_t79944241 * value)
	{
		___curUseSkill_6 = value;
		Il2CppCodeGenWriteBarrier(&___curUseSkill_6, value);
	}

	inline static int32_t get_offset_of_curSkillUsePos_7() { return static_cast<int32_t>(offsetof(FightCtrl_t648967803, ___curSkillUsePos_7)); }
	inline Vector3_t4282066566  get_curSkillUsePos_7() const { return ___curSkillUsePos_7; }
	inline Vector3_t4282066566 * get_address_of_curSkillUsePos_7() { return &___curSkillUsePos_7; }
	inline void set_curSkillUsePos_7(Vector3_t4282066566  value)
	{
		___curSkillUsePos_7 = value;
	}

	inline static int32_t get_offset_of_curSkillActions_8() { return static_cast<int32_t>(offsetof(FightCtrl_t648967803, ___curSkillActions_8)); }
	inline StringU5BU5D_t4054002952* get_curSkillActions_8() const { return ___curSkillActions_8; }
	inline StringU5BU5D_t4054002952** get_address_of_curSkillActions_8() { return &___curSkillActions_8; }
	inline void set_curSkillActions_8(StringU5BU5D_t4054002952* value)
	{
		___curSkillActions_8 = value;
		Il2CppCodeGenWriteBarrier(&___curSkillActions_8, value);
	}

	inline static int32_t get_offset_of_curSkillActionTimes_9() { return static_cast<int32_t>(offsetof(FightCtrl_t648967803, ___curSkillActionTimes_9)); }
	inline SingleU5BU5D_t2316563989* get_curSkillActionTimes_9() const { return ___curSkillActionTimes_9; }
	inline SingleU5BU5D_t2316563989** get_address_of_curSkillActionTimes_9() { return &___curSkillActionTimes_9; }
	inline void set_curSkillActionTimes_9(SingleU5BU5D_t2316563989* value)
	{
		___curSkillActionTimes_9 = value;
		Il2CppCodeGenWriteBarrier(&___curSkillActionTimes_9, value);
	}

	inline static int32_t get_offset_of_curSkillActionIndex_10() { return static_cast<int32_t>(offsetof(FightCtrl_t648967803, ___curSkillActionIndex_10)); }
	inline int32_t get_curSkillActionIndex_10() const { return ___curSkillActionIndex_10; }
	inline int32_t* get_address_of_curSkillActionIndex_10() { return &___curSkillActionIndex_10; }
	inline void set_curSkillActionIndex_10(int32_t value)
	{
		___curSkillActionIndex_10 = value;
	}

	inline static int32_t get_offset_of_curSkillActionStartTime_11() { return static_cast<int32_t>(offsetof(FightCtrl_t648967803, ___curSkillActionStartTime_11)); }
	inline float get_curSkillActionStartTime_11() const { return ___curSkillActionStartTime_11; }
	inline float* get_address_of_curSkillActionStartTime_11() { return &___curSkillActionStartTime_11; }
	inline void set_curSkillActionStartTime_11(float value)
	{
		___curSkillActionStartTime_11 = value;
	}

	inline static int32_t get_offset_of_skillState_12() { return static_cast<int32_t>(offsetof(FightCtrl_t648967803, ___skillState_12)); }
	inline int32_t get_skillState_12() const { return ___skillState_12; }
	inline int32_t* get_address_of_skillState_12() { return &___skillState_12; }
	inline void set_skillState_12(int32_t value)
	{
		___skillState_12 = value;
	}

	inline static int32_t get_offset_of_skillEffectState_13() { return static_cast<int32_t>(offsetof(FightCtrl_t648967803, ___skillEffectState_13)); }
	inline int32_t get_skillEffectState_13() const { return ___skillEffectState_13; }
	inline int32_t* get_address_of_skillEffectState_13() { return &___skillEffectState_13; }
	inline void set_skillEffectState_13(int32_t value)
	{
		___skillEffectState_13 = value;
	}

	inline static int32_t get_offset_of_realUseSkillTime_14() { return static_cast<int32_t>(offsetof(FightCtrl_t648967803, ___realUseSkillTime_14)); }
	inline float get_realUseSkillTime_14() const { return ___realUseSkillTime_14; }
	inline float* get_address_of_realUseSkillTime_14() { return &___realUseSkillTime_14; }
	inline void set_realUseSkillTime_14(float value)
	{
		___realUseSkillTime_14 = value;
	}

	inline static int32_t get_offset_of_curTime_15() { return static_cast<int32_t>(offsetof(FightCtrl_t648967803, ___curTime_15)); }
	inline float get_curTime_15() const { return ___curTime_15; }
	inline float* get_address_of_curTime_15() { return &___curTime_15; }
	inline void set_curTime_15(float value)
	{
		___curTime_15 = value;
	}

	inline static int32_t get_offset_of_skillRstTime_16() { return static_cast<int32_t>(offsetof(FightCtrl_t648967803, ___skillRstTime_16)); }
	inline float get_skillRstTime_16() const { return ___skillRstTime_16; }
	inline float* get_address_of_skillRstTime_16() { return &___skillRstTime_16; }
	inline void set_skillRstTime_16(float value)
	{
		___skillRstTime_16 = value;
	}

	inline static int32_t get_offset_of_shootSkillRstTime_17() { return static_cast<int32_t>(offsetof(FightCtrl_t648967803, ___shootSkillRstTime_17)); }
	inline float get_shootSkillRstTime_17() const { return ___shootSkillRstTime_17; }
	inline float* get_address_of_shootSkillRstTime_17() { return &___shootSkillRstTime_17; }
	inline void set_shootSkillRstTime_17(float value)
	{
		___shootSkillRstTime_17 = value;
	}

	inline static int32_t get_offset_of_skillRstEndTime_18() { return static_cast<int32_t>(offsetof(FightCtrl_t648967803, ___skillRstEndTime_18)); }
	inline float get_skillRstEndTime_18() const { return ___skillRstEndTime_18; }
	inline float* get_address_of_skillRstEndTime_18() { return &___skillRstEndTime_18; }
	inline void set_skillRstEndTime_18(float value)
	{
		___skillRstEndTime_18 = value;
	}

	inline static int32_t get_offset_of_endUseSkillTime_19() { return static_cast<int32_t>(offsetof(FightCtrl_t648967803, ___endUseSkillTime_19)); }
	inline float get_endUseSkillTime_19() const { return ___endUseSkillTime_19; }
	inline float* get_address_of_endUseSkillTime_19() { return &___endUseSkillTime_19; }
	inline void set_endUseSkillTime_19(float value)
	{
		___endUseSkillTime_19 = value;
	}

	inline static int32_t get_offset_of_activecount_20() { return static_cast<int32_t>(offsetof(FightCtrl_t648967803, ___activecount_20)); }
	inline int32_t get_activecount_20() const { return ___activecount_20; }
	inline int32_t* get_address_of_activecount_20() { return &___activecount_20; }
	inline void set_activecount_20(int32_t value)
	{
		___activecount_20 = value;
	}

	inline static int32_t get_offset_of_isSkillBreak_21() { return static_cast<int32_t>(offsetof(FightCtrl_t648967803, ___isSkillBreak_21)); }
	inline bool get_isSkillBreak_21() const { return ___isSkillBreak_21; }
	inline bool* get_address_of_isSkillBreak_21() { return &___isSkillBreak_21; }
	inline void set_isSkillBreak_21(bool value)
	{
		___isSkillBreak_21 = value;
	}

	inline static int32_t get_offset_of_center_22() { return static_cast<int32_t>(offsetof(FightCtrl_t648967803, ___center_22)); }
	inline Vector3_t4282066566  get_center_22() const { return ___center_22; }
	inline Vector3_t4282066566 * get_address_of_center_22() { return &___center_22; }
	inline void set_center_22(Vector3_t4282066566  value)
	{
		___center_22 = value;
	}

	inline static int32_t get_offset_of_lastCenter_23() { return static_cast<int32_t>(offsetof(FightCtrl_t648967803, ___lastCenter_23)); }
	inline Vector3_t4282066566  get_lastCenter_23() const { return ___lastCenter_23; }
	inline Vector3_t4282066566 * get_address_of_lastCenter_23() { return &___lastCenter_23; }
	inline void set_lastCenter_23(Vector3_t4282066566  value)
	{
		___lastCenter_23 = value;
	}

	inline static int32_t get_offset_of_atkTarget_24() { return static_cast<int32_t>(offsetof(FightCtrl_t648967803, ___atkTarget_24)); }
	inline CombatEntity_t684137495 * get_atkTarget_24() const { return ___atkTarget_24; }
	inline CombatEntity_t684137495 ** get_address_of_atkTarget_24() { return &___atkTarget_24; }
	inline void set_atkTarget_24(CombatEntity_t684137495 * value)
	{
		___atkTarget_24 = value;
		Il2CppCodeGenWriteBarrier(&___atkTarget_24, value);
	}

	inline static int32_t get_offset_of_hitList_25() { return static_cast<int32_t>(offsetof(FightCtrl_t648967803, ___hitList_25)); }
	inline List_1_t2052323047 * get_hitList_25() const { return ___hitList_25; }
	inline List_1_t2052323047 ** get_address_of_hitList_25() { return &___hitList_25; }
	inline void set_hitList_25(List_1_t2052323047 * value)
	{
		___hitList_25 = value;
		Il2CppCodeGenWriteBarrier(&___hitList_25, value);
	}

	inline static int32_t get_offset_of_sputterList_26() { return static_cast<int32_t>(offsetof(FightCtrl_t648967803, ___sputterList_26)); }
	inline List_1_t2052323047 * get_sputterList_26() const { return ___sputterList_26; }
	inline List_1_t2052323047 ** get_address_of_sputterList_26() { return &___sputterList_26; }
	inline void set_sputterList_26(List_1_t2052323047 * value)
	{
		___sputterList_26 = value;
		Il2CppCodeGenWriteBarrier(&___sputterList_26, value);
	}

	inline static int32_t get_offset_of_isUsingCD_27() { return static_cast<int32_t>(offsetof(FightCtrl_t648967803, ___isUsingCD_27)); }
	inline bool get_isUsingCD_27() const { return ___isUsingCD_27; }
	inline bool* get_address_of_isUsingCD_27() { return &___isUsingCD_27; }
	inline void set_isUsingCD_27(bool value)
	{
		___isUsingCD_27 = value;
	}

	inline static int32_t get_offset_of_isProBar_28() { return static_cast<int32_t>(offsetof(FightCtrl_t648967803, ___isProBar_28)); }
	inline bool get_isProBar_28() const { return ___isProBar_28; }
	inline bool* get_address_of_isProBar_28() { return &___isProBar_28; }
	inline void set_isProBar_28(bool value)
	{
		___isProBar_28 = value;
	}

	inline static int32_t get_offset_of_curSoundIndex_29() { return static_cast<int32_t>(offsetof(FightCtrl_t648967803, ___curSoundIndex_29)); }
	inline int32_t get_curSoundIndex_29() const { return ___curSoundIndex_29; }
	inline int32_t* get_address_of_curSoundIndex_29() { return &___curSoundIndex_29; }
	inline void set_curSoundIndex_29(int32_t value)
	{
		___curSoundIndex_29 = value;
	}

	inline static int32_t get_offset_of_curSoundID_30() { return static_cast<int32_t>(offsetof(FightCtrl_t648967803, ___curSoundID_30)); }
	inline Int32U5BU5D_t3230847821* get_curSoundID_30() const { return ___curSoundID_30; }
	inline Int32U5BU5D_t3230847821** get_address_of_curSoundID_30() { return &___curSoundID_30; }
	inline void set_curSoundID_30(Int32U5BU5D_t3230847821* value)
	{
		___curSoundID_30 = value;
		Il2CppCodeGenWriteBarrier(&___curSoundID_30, value);
	}

	inline static int32_t get_offset_of_curSoundTimes_31() { return static_cast<int32_t>(offsetof(FightCtrl_t648967803, ___curSoundTimes_31)); }
	inline SingleU5BU5D_t2316563989* get_curSoundTimes_31() const { return ___curSoundTimes_31; }
	inline SingleU5BU5D_t2316563989** get_address_of_curSoundTimes_31() { return &___curSoundTimes_31; }
	inline void set_curSoundTimes_31(SingleU5BU5D_t2316563989* value)
	{
		___curSoundTimes_31 = value;
		Il2CppCodeGenWriteBarrier(&___curSoundTimes_31, value);
	}

	inline static int32_t get_offset_of_curSoundStartTime_32() { return static_cast<int32_t>(offsetof(FightCtrl_t648967803, ___curSoundStartTime_32)); }
	inline float get_curSoundStartTime_32() const { return ___curSoundStartTime_32; }
	inline float* get_address_of_curSoundStartTime_32() { return &___curSoundStartTime_32; }
	inline void set_curSoundStartTime_32(float value)
	{
		___curSoundStartTime_32 = value;
	}

	inline static int32_t get_offset_of_allDelayID_33() { return static_cast<int32_t>(offsetof(FightCtrl_t648967803, ___allDelayID_33)); }
	inline List_1_t1392853533 * get_allDelayID_33() const { return ___allDelayID_33; }
	inline List_1_t1392853533 ** get_address_of_allDelayID_33() { return &___allDelayID_33; }
	inline void set_allDelayID_33(List_1_t1392853533 * value)
	{
		___allDelayID_33 = value;
		Il2CppCodeGenWriteBarrier(&___allDelayID_33, value);
	}

	inline static int32_t get_offset_of_progressTime_34() { return static_cast<int32_t>(offsetof(FightCtrl_t648967803, ___progressTime_34)); }
	inline float get_progressTime_34() const { return ___progressTime_34; }
	inline float* get_address_of_progressTime_34() { return &___progressTime_34; }
	inline void set_progressTime_34(float value)
	{
		___progressTime_34 = value;
	}

	inline static int32_t get_offset_of_pb_35() { return static_cast<int32_t>(offsetof(FightCtrl_t648967803, ___pb_35)); }
	inline ProgressBar_t2799378054 * get_pb_35() const { return ___pb_35; }
	inline ProgressBar_t2799378054 ** get_address_of_pb_35() { return &___pb_35; }
	inline void set_pb_35(ProgressBar_t2799378054 * value)
	{
		___pb_35 = value;
		Il2CppCodeGenWriteBarrier(&___pb_35, value);
	}

	inline static int32_t get_offset_of_SummontargetPos_36() { return static_cast<int32_t>(offsetof(FightCtrl_t648967803, ___SummontargetPos_36)); }
	inline Vector3_t4282066566  get_SummontargetPos_36() const { return ___SummontargetPos_36; }
	inline Vector3_t4282066566 * get_address_of_SummontargetPos_36() { return &___SummontargetPos_36; }
	inline void set_SummontargetPos_36(Vector3_t4282066566  value)
	{
		___SummontargetPos_36 = value;
	}

	inline static int32_t get_offset_of_SummontPos_37() { return static_cast<int32_t>(offsetof(FightCtrl_t648967803, ___SummontPos_37)); }
	inline List_1_t1355284822 * get_SummontPos_37() const { return ___SummontPos_37; }
	inline List_1_t1355284822 ** get_address_of_SummontPos_37() { return &___SummontPos_37; }
	inline void set_SummontPos_37(List_1_t1355284822 * value)
	{
		___SummontPos_37 = value;
		Il2CppCodeGenWriteBarrier(&___SummontPos_37, value);
	}

	inline static int32_t get_offset_of_summonindex_38() { return static_cast<int32_t>(offsetof(FightCtrl_t648967803, ___summonindex_38)); }
	inline int32_t get_summonindex_38() const { return ___summonindex_38; }
	inline int32_t* get_address_of_summonindex_38() { return &___summonindex_38; }
	inline void set_summonindex_38(int32_t value)
	{
		___summonindex_38 = value;
	}

	inline static int32_t get_offset_of_effectCount_39() { return static_cast<int32_t>(offsetof(FightCtrl_t648967803, ___effectCount_39)); }
	inline int32_t get_effectCount_39() const { return ___effectCount_39; }
	inline int32_t* get_address_of_effectCount_39() { return &___effectCount_39; }
	inline void set_effectCount_39(int32_t value)
	{
		___effectCount_39 = value;
	}

	inline static int32_t get_offset_of_isLastHurt_40() { return static_cast<int32_t>(offsetof(FightCtrl_t648967803, ___isLastHurt_40)); }
	inline bool get_isLastHurt_40() const { return ___isLastHurt_40; }
	inline bool* get_address_of_isLastHurt_40() { return &___isLastHurt_40; }
	inline void set_isLastHurt_40(bool value)
	{
		___isLastHurt_40 = value;
	}

	inline static int32_t get_offset_of_damageSeparationSkillList_41() { return static_cast<int32_t>(offsetof(FightCtrl_t648967803, ___damageSeparationSkillList_41)); }
	inline List_1_t3369319916 * get_damageSeparationSkillList_41() const { return ___damageSeparationSkillList_41; }
	inline List_1_t3369319916 ** get_address_of_damageSeparationSkillList_41() { return &___damageSeparationSkillList_41; }
	inline void set_damageSeparationSkillList_41(List_1_t3369319916 * value)
	{
		___damageSeparationSkillList_41 = value;
		Il2CppCodeGenWriteBarrier(&___damageSeparationSkillList_41, value);
	}

	inline static int32_t get_offset_of_curDamageSeparationSkill_42() { return static_cast<int32_t>(offsetof(FightCtrl_t648967803, ___curDamageSeparationSkill_42)); }
	inline DamageSeparationSkill_t2001134364 * get_curDamageSeparationSkill_42() const { return ___curDamageSeparationSkill_42; }
	inline DamageSeparationSkill_t2001134364 ** get_address_of_curDamageSeparationSkill_42() { return &___curDamageSeparationSkill_42; }
	inline void set_curDamageSeparationSkill_42(DamageSeparationSkill_t2001134364 * value)
	{
		___curDamageSeparationSkill_42 = value;
		Il2CppCodeGenWriteBarrier(&___curDamageSeparationSkill_42, value);
	}

	inline static int32_t get_offset_of_guideUsingSkill_EV_43() { return static_cast<int32_t>(offsetof(FightCtrl_t648967803, ___guideUsingSkill_EV_43)); }
	inline ZEvent_t3638018500 * get_guideUsingSkill_EV_43() const { return ___guideUsingSkill_EV_43; }
	inline ZEvent_t3638018500 ** get_address_of_guideUsingSkill_EV_43() { return &___guideUsingSkill_EV_43; }
	inline void set_guideUsingSkill_EV_43(ZEvent_t3638018500 * value)
	{
		___guideUsingSkill_EV_43 = value;
		Il2CppCodeGenWriteBarrier(&___guideUsingSkill_EV_43, value);
	}

	inline static int32_t get_offset_of_skillEndEv_44() { return static_cast<int32_t>(offsetof(FightCtrl_t648967803, ___skillEndEv_44)); }
	inline ZEvent_t3638018500 * get_skillEndEv_44() const { return ___skillEndEv_44; }
	inline ZEvent_t3638018500 ** get_address_of_skillEndEv_44() { return &___skillEndEv_44; }
	inline void set_skillEndEv_44(ZEvent_t3638018500 * value)
	{
		___skillEndEv_44 = value;
		Il2CppCodeGenWriteBarrier(&___skillEndEv_44, value);
	}

	inline static int32_t get_offset_of_BeforPlaySkillPos_45() { return static_cast<int32_t>(offsetof(FightCtrl_t648967803, ___BeforPlaySkillPos_45)); }
	inline Vector3_t4282066566  get_BeforPlaySkillPos_45() const { return ___BeforPlaySkillPos_45; }
	inline Vector3_t4282066566 * get_address_of_BeforPlaySkillPos_45() { return &___BeforPlaySkillPos_45; }
	inline void set_BeforPlaySkillPos_45(Vector3_t4282066566  value)
	{
		___BeforPlaySkillPos_45 = value;
	}

	inline static int32_t get_offset_of_U3CactiveSkillU3Ek__BackingField_46() { return static_cast<int32_t>(offsetof(FightCtrl_t648967803, ___U3CactiveSkillU3Ek__BackingField_46)); }
	inline Skill_t79944241 * get_U3CactiveSkillU3Ek__BackingField_46() const { return ___U3CactiveSkillU3Ek__BackingField_46; }
	inline Skill_t79944241 ** get_address_of_U3CactiveSkillU3Ek__BackingField_46() { return &___U3CactiveSkillU3Ek__BackingField_46; }
	inline void set_U3CactiveSkillU3Ek__BackingField_46(Skill_t79944241 * value)
	{
		___U3CactiveSkillU3Ek__BackingField_46 = value;
		Il2CppCodeGenWriteBarrier(&___U3CactiveSkillU3Ek__BackingField_46, value);
	}

	inline static int32_t get_offset_of_U3CnormalAttackSkillU3Ek__BackingField_47() { return static_cast<int32_t>(offsetof(FightCtrl_t648967803, ___U3CnormalAttackSkillU3Ek__BackingField_47)); }
	inline Skill_t79944241 * get_U3CnormalAttackSkillU3Ek__BackingField_47() const { return ___U3CnormalAttackSkillU3Ek__BackingField_47; }
	inline Skill_t79944241 ** get_address_of_U3CnormalAttackSkillU3Ek__BackingField_47() { return &___U3CnormalAttackSkillU3Ek__BackingField_47; }
	inline void set_U3CnormalAttackSkillU3Ek__BackingField_47(Skill_t79944241 * value)
	{
		___U3CnormalAttackSkillU3Ek__BackingField_47 = value;
		Il2CppCodeGenWriteBarrier(&___U3CnormalAttackSkillU3Ek__BackingField_47, value);
	}

	inline static int32_t get_offset_of_U3CdeathSkillU3Ek__BackingField_48() { return static_cast<int32_t>(offsetof(FightCtrl_t648967803, ___U3CdeathSkillU3Ek__BackingField_48)); }
	inline Skill_t79944241 * get_U3CdeathSkillU3Ek__BackingField_48() const { return ___U3CdeathSkillU3Ek__BackingField_48; }
	inline Skill_t79944241 ** get_address_of_U3CdeathSkillU3Ek__BackingField_48() { return &___U3CdeathSkillU3Ek__BackingField_48; }
	inline void set_U3CdeathSkillU3Ek__BackingField_48(Skill_t79944241 * value)
	{
		___U3CdeathSkillU3Ek__BackingField_48 = value;
		Il2CppCodeGenWriteBarrier(&___U3CdeathSkillU3Ek__BackingField_48, value);
	}

	inline static int32_t get_offset_of_U3CwakeSkillU3Ek__BackingField_49() { return static_cast<int32_t>(offsetof(FightCtrl_t648967803, ___U3CwakeSkillU3Ek__BackingField_49)); }
	inline Skill_t79944241 * get_U3CwakeSkillU3Ek__BackingField_49() const { return ___U3CwakeSkillU3Ek__BackingField_49; }
	inline Skill_t79944241 ** get_address_of_U3CwakeSkillU3Ek__BackingField_49() { return &___U3CwakeSkillU3Ek__BackingField_49; }
	inline void set_U3CwakeSkillU3Ek__BackingField_49(Skill_t79944241 * value)
	{
		___U3CwakeSkillU3Ek__BackingField_49 = value;
		Il2CppCodeGenWriteBarrier(&___U3CwakeSkillU3Ek__BackingField_49, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
