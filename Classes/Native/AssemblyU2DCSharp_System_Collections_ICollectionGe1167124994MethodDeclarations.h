﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System_Collections_ICollectionGenerated
struct System_Collections_ICollectionGenerated_t1167124994;
// JSVCall
struct JSVCall_t3708497963;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System_Collections_ICollectionGenerated::.ctor()
extern "C"  void System_Collections_ICollectionGenerated__ctor_m805095337 (System_Collections_ICollectionGenerated_t1167124994 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Collections_ICollectionGenerated::ICollection_Count(JSVCall)
extern "C"  void System_Collections_ICollectionGenerated_ICollection_Count_m430839625 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Collections_ICollectionGenerated::ICollection_IsSynchronized(JSVCall)
extern "C"  void System_Collections_ICollectionGenerated_ICollection_IsSynchronized_m1266267702 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Collections_ICollectionGenerated::ICollection_SyncRoot(JSVCall)
extern "C"  void System_Collections_ICollectionGenerated_ICollection_SyncRoot_m3102203095 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Collections_ICollectionGenerated::ICollection_CopyTo__Array__Int32(JSVCall,System.Int32)
extern "C"  bool System_Collections_ICollectionGenerated_ICollection_CopyTo__Array__Int32_m1512722420 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Collections_ICollectionGenerated::__Register()
extern "C"  void System_Collections_ICollectionGenerated___Register_m2280396478 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Collections_ICollectionGenerated::ilo_setInt321(System.Int32,System.Int32)
extern "C"  void System_Collections_ICollectionGenerated_ilo_setInt321_m111158605 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Collections_ICollectionGenerated::ilo_setWhatever2(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  void System_Collections_ICollectionGenerated_ilo_setWhatever2_m1197054833 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___obj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System_Collections_ICollectionGenerated::ilo_getObject3(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * System_Collections_ICollectionGenerated_ilo_getObject3_m20387102 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
