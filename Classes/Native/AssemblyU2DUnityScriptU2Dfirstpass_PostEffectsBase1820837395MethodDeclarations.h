﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PostEffectsBase
struct PostEffectsBase_t1820837395;
// UnityEngine.Material
struct Material_t3870600107;
// UnityEngine.Shader
struct Shader_t3191267369;
// UnityEngine.RenderTexture
struct RenderTexture_t1963041563;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Shader3191267369.h"
#include "UnityEngine_UnityEngine_Material3870600107.h"
#include "UnityEngine_UnityEngine_RenderTexture1963041563.h"

// System.Void PostEffectsBase::.ctor()
extern "C"  void PostEffectsBase__ctor_m3869393199 (PostEffectsBase_t1820837395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material PostEffectsBase::CheckShaderAndCreateMaterial(UnityEngine.Shader,UnityEngine.Material)
extern "C"  Material_t3870600107 * PostEffectsBase_CheckShaderAndCreateMaterial_m4154378035 (PostEffectsBase_t1820837395 * __this, Shader_t3191267369 * ___s0, Material_t3870600107 * ___m2Create1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material PostEffectsBase::CreateMaterial(UnityEngine.Shader,UnityEngine.Material)
extern "C"  Material_t3870600107 * PostEffectsBase_CreateMaterial_m2171637981 (PostEffectsBase_t1820837395 * __this, Shader_t3191267369 * ___s0, Material_t3870600107 * ___m2Create1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PostEffectsBase::OnEnable()
extern "C"  void PostEffectsBase_OnEnable_m2537112567 (PostEffectsBase_t1820837395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PostEffectsBase::CheckSupport()
extern "C"  bool PostEffectsBase_CheckSupport_m2350066934 (PostEffectsBase_t1820837395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PostEffectsBase::CheckResources()
extern "C"  bool PostEffectsBase_CheckResources_m2348731468 (PostEffectsBase_t1820837395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PostEffectsBase::Start()
extern "C"  void PostEffectsBase_Start_m2816530991 (PostEffectsBase_t1820837395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PostEffectsBase::CheckSupport(System.Boolean)
extern "C"  bool PostEffectsBase_CheckSupport_m2153053165 (PostEffectsBase_t1820837395 * __this, bool ___needDepth0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PostEffectsBase::CheckSupport(System.Boolean,System.Boolean)
extern "C"  bool PostEffectsBase_CheckSupport_m822108144 (PostEffectsBase_t1820837395 * __this, bool ___needDepth0, bool ___needHdr1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PostEffectsBase::Dx11Support()
extern "C"  bool PostEffectsBase_Dx11Support_m3110085614 (PostEffectsBase_t1820837395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PostEffectsBase::ReportAutoDisable()
extern "C"  void PostEffectsBase_ReportAutoDisable_m44450674 (PostEffectsBase_t1820837395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PostEffectsBase::CheckShader(UnityEngine.Shader)
extern "C"  bool PostEffectsBase_CheckShader_m636992228 (PostEffectsBase_t1820837395 * __this, Shader_t3191267369 * ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PostEffectsBase::NotSupported()
extern "C"  void PostEffectsBase_NotSupported_m925958800 (PostEffectsBase_t1820837395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PostEffectsBase::DrawBorder(UnityEngine.RenderTexture,UnityEngine.Material)
extern "C"  void PostEffectsBase_DrawBorder_m722325705 (PostEffectsBase_t1820837395 * __this, RenderTexture_t1963041563 * ___dest0, Material_t3870600107 * ___material1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PostEffectsBase::Main()
extern "C"  void PostEffectsBase_Main_m871597486 (PostEffectsBase_t1820837395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
