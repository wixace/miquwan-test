﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String[]
struct StringU5BU5D_t4054002952;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JSNameMgr
struct  JSNameMgr_t1403313668  : public Il2CppObject
{
public:

public:
};

struct JSNameMgr_t1403313668_StaticFields
{
public:
	// System.String[] JSNameMgr::GenTSuffix
	StringU5BU5D_t4054002952* ___GenTSuffix_0;
	// System.String[] JSNameMgr::GenTSuffixReplaceCS
	StringU5BU5D_t4054002952* ___GenTSuffixReplaceCS_1;
	// System.String[] JSNameMgr::GenTSuffixReplaceJS
	StringU5BU5D_t4054002952* ___GenTSuffixReplaceJS_2;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> JSNameMgr::jsTypeFullNameMappingDic
	Dictionary_2_t827649927 * ___jsTypeFullNameMappingDic_3;

public:
	inline static int32_t get_offset_of_GenTSuffix_0() { return static_cast<int32_t>(offsetof(JSNameMgr_t1403313668_StaticFields, ___GenTSuffix_0)); }
	inline StringU5BU5D_t4054002952* get_GenTSuffix_0() const { return ___GenTSuffix_0; }
	inline StringU5BU5D_t4054002952** get_address_of_GenTSuffix_0() { return &___GenTSuffix_0; }
	inline void set_GenTSuffix_0(StringU5BU5D_t4054002952* value)
	{
		___GenTSuffix_0 = value;
		Il2CppCodeGenWriteBarrier(&___GenTSuffix_0, value);
	}

	inline static int32_t get_offset_of_GenTSuffixReplaceCS_1() { return static_cast<int32_t>(offsetof(JSNameMgr_t1403313668_StaticFields, ___GenTSuffixReplaceCS_1)); }
	inline StringU5BU5D_t4054002952* get_GenTSuffixReplaceCS_1() const { return ___GenTSuffixReplaceCS_1; }
	inline StringU5BU5D_t4054002952** get_address_of_GenTSuffixReplaceCS_1() { return &___GenTSuffixReplaceCS_1; }
	inline void set_GenTSuffixReplaceCS_1(StringU5BU5D_t4054002952* value)
	{
		___GenTSuffixReplaceCS_1 = value;
		Il2CppCodeGenWriteBarrier(&___GenTSuffixReplaceCS_1, value);
	}

	inline static int32_t get_offset_of_GenTSuffixReplaceJS_2() { return static_cast<int32_t>(offsetof(JSNameMgr_t1403313668_StaticFields, ___GenTSuffixReplaceJS_2)); }
	inline StringU5BU5D_t4054002952* get_GenTSuffixReplaceJS_2() const { return ___GenTSuffixReplaceJS_2; }
	inline StringU5BU5D_t4054002952** get_address_of_GenTSuffixReplaceJS_2() { return &___GenTSuffixReplaceJS_2; }
	inline void set_GenTSuffixReplaceJS_2(StringU5BU5D_t4054002952* value)
	{
		___GenTSuffixReplaceJS_2 = value;
		Il2CppCodeGenWriteBarrier(&___GenTSuffixReplaceJS_2, value);
	}

	inline static int32_t get_offset_of_jsTypeFullNameMappingDic_3() { return static_cast<int32_t>(offsetof(JSNameMgr_t1403313668_StaticFields, ___jsTypeFullNameMappingDic_3)); }
	inline Dictionary_2_t827649927 * get_jsTypeFullNameMappingDic_3() const { return ___jsTypeFullNameMappingDic_3; }
	inline Dictionary_2_t827649927 ** get_address_of_jsTypeFullNameMappingDic_3() { return &___jsTypeFullNameMappingDic_3; }
	inline void set_jsTypeFullNameMappingDic_3(Dictionary_2_t827649927 * value)
	{
		___jsTypeFullNameMappingDic_3 = value;
		Il2CppCodeGenWriteBarrier(&___jsTypeFullNameMappingDic_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
