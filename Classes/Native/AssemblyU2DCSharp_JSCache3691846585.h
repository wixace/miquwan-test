﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// System.Collections.Generic.Dictionary`2<System.String,System.Boolean>
struct Dictionary_2_t1297217088;
// System.Collections.Generic.Dictionary`2<System.Type,JSCache/TypeInfo>
struct Dictionary_2_t3304235859;
// JSCache/TypeInfo
struct TypeInfo_t3198813374;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JSCache
struct  JSCache_t3691846585  : public Il2CppObject
{
public:

public:
};

struct JSCache_t3691846585_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.String> JSCache::dictMB2JSComName
	Dictionary_2_t827649927 * ___dictMB2JSComName_0;
	// System.Collections.Generic.Dictionary`2<System.String,System.Boolean> JSCache::dictClassInheritanceRel
	Dictionary_2_t1297217088 * ___dictClassInheritanceRel_1;
	// System.Collections.Generic.Dictionary`2<System.Type,JSCache/TypeInfo> JSCache::dictType2TypeInfo
	Dictionary_2_t3304235859 * ___dictType2TypeInfo_2;
	// JSCache/TypeInfo JSCache::nullTypeInfo
	TypeInfo_t3198813374 * ___nullTypeInfo_3;

public:
	inline static int32_t get_offset_of_dictMB2JSComName_0() { return static_cast<int32_t>(offsetof(JSCache_t3691846585_StaticFields, ___dictMB2JSComName_0)); }
	inline Dictionary_2_t827649927 * get_dictMB2JSComName_0() const { return ___dictMB2JSComName_0; }
	inline Dictionary_2_t827649927 ** get_address_of_dictMB2JSComName_0() { return &___dictMB2JSComName_0; }
	inline void set_dictMB2JSComName_0(Dictionary_2_t827649927 * value)
	{
		___dictMB2JSComName_0 = value;
		Il2CppCodeGenWriteBarrier(&___dictMB2JSComName_0, value);
	}

	inline static int32_t get_offset_of_dictClassInheritanceRel_1() { return static_cast<int32_t>(offsetof(JSCache_t3691846585_StaticFields, ___dictClassInheritanceRel_1)); }
	inline Dictionary_2_t1297217088 * get_dictClassInheritanceRel_1() const { return ___dictClassInheritanceRel_1; }
	inline Dictionary_2_t1297217088 ** get_address_of_dictClassInheritanceRel_1() { return &___dictClassInheritanceRel_1; }
	inline void set_dictClassInheritanceRel_1(Dictionary_2_t1297217088 * value)
	{
		___dictClassInheritanceRel_1 = value;
		Il2CppCodeGenWriteBarrier(&___dictClassInheritanceRel_1, value);
	}

	inline static int32_t get_offset_of_dictType2TypeInfo_2() { return static_cast<int32_t>(offsetof(JSCache_t3691846585_StaticFields, ___dictType2TypeInfo_2)); }
	inline Dictionary_2_t3304235859 * get_dictType2TypeInfo_2() const { return ___dictType2TypeInfo_2; }
	inline Dictionary_2_t3304235859 ** get_address_of_dictType2TypeInfo_2() { return &___dictType2TypeInfo_2; }
	inline void set_dictType2TypeInfo_2(Dictionary_2_t3304235859 * value)
	{
		___dictType2TypeInfo_2 = value;
		Il2CppCodeGenWriteBarrier(&___dictType2TypeInfo_2, value);
	}

	inline static int32_t get_offset_of_nullTypeInfo_3() { return static_cast<int32_t>(offsetof(JSCache_t3691846585_StaticFields, ___nullTypeInfo_3)); }
	inline TypeInfo_t3198813374 * get_nullTypeInfo_3() const { return ___nullTypeInfo_3; }
	inline TypeInfo_t3198813374 ** get_address_of_nullTypeInfo_3() { return &___nullTypeInfo_3; }
	inline void set_nullTypeInfo_3(TypeInfo_t3198813374 * value)
	{
		___nullTypeInfo_3 = value;
		Il2CppCodeGenWriteBarrier(&___nullTypeInfo_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
