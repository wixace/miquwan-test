﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIFrameAni
struct UIFrameAni_t95674339;
// System.Collections.Generic.List`1<UISpriteData>
struct List_1_t651564179;
// UIAtlas
struct UIAtlas_t281921111;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_UIAtlas281921111.h"

// System.Void UIFrameAni::.ctor()
extern "C"  void UIFrameAni__ctor_m3860981784 (UIFrameAni_t95674339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIFrameAni::Start()
extern "C"  void UIFrameAni_Start_m2808119576 (UIFrameAni_t95674339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIFrameAni::Update()
extern "C"  void UIFrameAni_Update_m1158213109 (UIFrameAni_t95674339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<UISpriteData> UIFrameAni::ilo_get_spriteList1(UIAtlas)
extern "C"  List_1_t651564179 * UIFrameAni_ilo_get_spriteList1_m141029545 (Il2CppObject * __this /* static, unused */, UIAtlas_t281921111 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
