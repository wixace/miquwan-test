﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Animator
struct Animator_t2776330603;
// System.String
struct String_t;
// UnityEngine.AnimatorClipInfo[]
struct AnimatorClipInfoU5BU5D_t3117370260;
// UnityEngine.AnimatorControllerParameter[]
struct AnimatorControllerParameterU5BU5D_t1853597653;
// UnityEngine.AnimatorControllerParameter
struct AnimatorControllerParameter_t3339705788;
// UnityEngine.Transform
struct Transform_t1659122786;
// UnityEngine.RuntimeAnimatorController
struct RuntimeAnimatorController_t274649809;
// UnityEngine.Avatar
struct Avatar_t2688887197;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882.h"
#include "UnityEngine_UnityEngine_AnimatorUpdateMode2936494615.h"
#include "UnityEngine_UnityEngine_AvatarIKGoal2036631794.h"
#include "UnityEngine_UnityEngine_Animator2776330603.h"
#include "UnityEngine_UnityEngine_AvatarIKHint2036656230.h"
#include "UnityEngine_UnityEngine_HumanBodyBones1606609988.h"
#include "UnityEngine_UnityEngine_AnimatorStateInfo323110318.h"
#include "UnityEngine_UnityEngine_AnimatorTransitionInfo2817229998.h"
#include "UnityEngine_UnityEngine_AvatarTarget2373143374.h"
#include "UnityEngine_UnityEngine_MatchTargetWeightMask258413904.h"
#include "UnityEngine_UnityEngine_AnimatorCullingMode3217251138.h"
#include "UnityEngine_UnityEngine_AnimatorRecorderMode1968754700.h"
#include "UnityEngine_UnityEngine_RuntimeAnimatorController274649809.h"
#include "UnityEngine_UnityEngine_Avatar2688887197.h"

// System.Void UnityEngine.Animator::.ctor()
extern "C"  void Animator__ctor_m3085734502 (Animator_t2776330603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Animator::get_isOptimizable()
extern "C"  bool Animator_get_isOptimizable_m3828643437 (Animator_t2776330603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Animator::get_isHuman()
extern "C"  bool Animator_get_isHuman_m4030001272 (Animator_t2776330603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Animator::get_hasRootMotion()
extern "C"  bool Animator_get_hasRootMotion_m2848414983 (Animator_t2776330603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Animator::get_humanScale()
extern "C"  float Animator_get_humanScale_m13697776 (Animator_t2776330603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Animator::get_isInitialized()
extern "C"  bool Animator_get_isInitialized_m4048984287 (Animator_t2776330603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Animator::GetFloat(System.String)
extern "C"  float Animator_GetFloat_m2204811058 (Animator_t2776330603 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Animator::GetFloat(System.Int32)
extern "C"  float Animator_GetFloat_m2965884705 (Animator_t2776330603 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetFloat(System.String,System.Single)
extern "C"  void Animator_SetFloat_m1775105839 (Animator_t2776330603 * __this, String_t* ___name0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetFloat(System.String,System.Single,System.Single,System.Single)
extern "C"  void Animator_SetFloat_m359795641 (Animator_t2776330603 * __this, String_t* ___name0, float ___value1, float ___dampTime2, float ___deltaTime3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetFloat(System.Int32,System.Single)
extern "C"  void Animator_SetFloat_m57191598 (Animator_t2776330603 * __this, int32_t ___id0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetFloat(System.Int32,System.Single,System.Single,System.Single)
extern "C"  void Animator_SetFloat_m2671615160 (Animator_t2776330603 * __this, int32_t ___id0, float ___value1, float ___dampTime2, float ___deltaTime3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Animator::GetBool(System.String)
extern "C"  bool Animator_GetBool_m436748612 (Animator_t2776330603 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Animator::GetBool(System.Int32)
extern "C"  bool Animator_GetBool_m1246282447 (Animator_t2776330603 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetBool(System.String,System.Boolean)
extern "C"  void Animator_SetBool_m2336836203 (Animator_t2776330603 * __this, String_t* ___name0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetBool(System.Int32,System.Boolean)
extern "C"  void Animator_SetBool_m1802007004 (Animator_t2776330603 * __this, int32_t ___id0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Animator::GetInteger(System.String)
extern "C"  int32_t Animator_GetInteger_m2467155164 (Animator_t2776330603 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Animator::GetInteger(System.Int32)
extern "C"  int32_t Animator_GetInteger_m3944178743 (Animator_t2776330603 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetInteger(System.String,System.Int32)
extern "C"  void Animator_SetInteger_m4253187183 (Animator_t2776330603 * __this, String_t* ___name0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetInteger(System.Int32,System.Int32)
extern "C"  void Animator_SetInteger_m812217484 (Animator_t2776330603 * __this, int32_t ___id0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetTrigger(System.String)
extern "C"  void Animator_SetTrigger_m514363822 (Animator_t2776330603 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetTrigger(System.Int32)
extern "C"  void Animator_SetTrigger_m2911354149 (Animator_t2776330603 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::ResetTrigger(System.String)
extern "C"  void Animator_ResetTrigger_m4152421915 (Animator_t2776330603 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::ResetTrigger(System.Int32)
extern "C"  void Animator_ResetTrigger_m1227595544 (Animator_t2776330603 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Animator::IsParameterControlledByCurve(System.String)
extern "C"  bool Animator_IsParameterControlledByCurve_m3328157587 (Animator_t2776330603 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Animator::IsParameterControlledByCurve(System.Int32)
extern "C"  bool Animator_IsParameterControlledByCurve_m1062459040 (Animator_t2776330603 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Animator::get_deltaPosition()
extern "C"  Vector3_t4282066566  Animator_get_deltaPosition_m1658225602 (Animator_t2776330603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::INTERNAL_get_deltaPosition(UnityEngine.Vector3&)
extern "C"  void Animator_INTERNAL_get_deltaPosition_m3729275047 (Animator_t2776330603 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Animator::get_deltaRotation()
extern "C"  Quaternion_t1553702882  Animator_get_deltaRotation_m1583110423 (Animator_t2776330603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::INTERNAL_get_deltaRotation(UnityEngine.Quaternion&)
extern "C"  void Animator_INTERNAL_get_deltaRotation_m3667880600 (Animator_t2776330603 * __this, Quaternion_t1553702882 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Animator::get_velocity()
extern "C"  Vector3_t4282066566  Animator_get_velocity_m4274914462 (Animator_t2776330603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::INTERNAL_get_velocity(UnityEngine.Vector3&)
extern "C"  void Animator_INTERNAL_get_velocity_m2696051359 (Animator_t2776330603 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Animator::get_angularVelocity()
extern "C"  Vector3_t4282066566  Animator_get_angularVelocity_m2440261664 (Animator_t2776330603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::INTERNAL_get_angularVelocity(UnityEngine.Vector3&)
extern "C"  void Animator_INTERNAL_get_angularVelocity_m3569671557 (Animator_t2776330603 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Animator::get_rootPosition()
extern "C"  Vector3_t4282066566  Animator_get_rootPosition_m425633900 (Animator_t2776330603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::set_rootPosition(UnityEngine.Vector3)
extern "C"  void Animator_set_rootPosition_m1253278899 (Animator_t2776330603 * __this, Vector3_t4282066566  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::INTERNAL_get_rootPosition(UnityEngine.Vector3&)
extern "C"  void Animator_INTERNAL_get_rootPosition_m623412333 (Animator_t2776330603 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::INTERNAL_set_rootPosition(UnityEngine.Vector3&)
extern "C"  void Animator_INTERNAL_set_rootPosition_m3037191137 (Animator_t2776330603 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Animator::get_rootRotation()
extern "C"  Quaternion_t1553702882  Animator_get_rootRotation_m3309843777 (Animator_t2776330603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::set_rootRotation(UnityEngine.Quaternion)
extern "C"  void Animator_set_rootRotation_m4249090054 (Animator_t2776330603 * __this, Quaternion_t1553702882  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::INTERNAL_get_rootRotation(UnityEngine.Quaternion&)
extern "C"  void Animator_INTERNAL_get_rootRotation_m3392225554 (Animator_t2776330603 * __this, Quaternion_t1553702882 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::INTERNAL_set_rootRotation(UnityEngine.Quaternion&)
extern "C"  void Animator_INTERNAL_set_rootRotation_m1639138590 (Animator_t2776330603 * __this, Quaternion_t1553702882 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Animator::get_applyRootMotion()
extern "C"  bool Animator_get_applyRootMotion_m2388146907 (Animator_t2776330603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::set_applyRootMotion(System.Boolean)
extern "C"  void Animator_set_applyRootMotion_m394805828 (Animator_t2776330603 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Animator::get_linearVelocityBlending()
extern "C"  bool Animator_get_linearVelocityBlending_m1764714208 (Animator_t2776330603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::set_linearVelocityBlending(System.Boolean)
extern "C"  void Animator_set_linearVelocityBlending_m3890989797 (Animator_t2776330603 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AnimatorUpdateMode UnityEngine.Animator::get_updateMode()
extern "C"  int32_t Animator_get_updateMode_m4099780600 (Animator_t2776330603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::set_updateMode(UnityEngine.AnimatorUpdateMode)
extern "C"  void Animator_set_updateMode_m1786110077 (Animator_t2776330603 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Animator::get_hasTransformHierarchy()
extern "C"  bool Animator_get_hasTransformHierarchy_m1887837112 (Animator_t2776330603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Animator::get_gravityWeight()
extern "C"  float Animator_get_gravityWeight_m1393695637 (Animator_t2776330603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Animator::get_bodyPosition()
extern "C"  Vector3_t4282066566  Animator_get_bodyPosition_m2404872492 (Animator_t2776330603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::set_bodyPosition(UnityEngine.Vector3)
extern "C"  void Animator_set_bodyPosition_m2325119987 (Animator_t2776330603 * __this, Vector3_t4282066566  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::INTERNAL_get_bodyPosition(UnityEngine.Vector3&)
extern "C"  void Animator_INTERNAL_get_bodyPosition_m3785714989 (Animator_t2776330603 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::INTERNAL_set_bodyPosition(UnityEngine.Vector3&)
extern "C"  void Animator_INTERNAL_set_bodyPosition_m1904526497 (Animator_t2776330603 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Animator::get_bodyRotation()
extern "C"  Quaternion_t1553702882  Animator_get_bodyRotation_m994115073 (Animator_t2776330603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::set_bodyRotation(UnityEngine.Quaternion)
extern "C"  void Animator_set_bodyRotation_m2385096902 (Animator_t2776330603 * __this, Quaternion_t1553702882  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::INTERNAL_get_bodyRotation(UnityEngine.Quaternion&)
extern "C"  void Animator_INTERNAL_get_bodyRotation_m1443012690 (Animator_t2776330603 * __this, Quaternion_t1553702882 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::INTERNAL_set_bodyRotation(UnityEngine.Quaternion&)
extern "C"  void Animator_INTERNAL_set_bodyRotation_m3984893022 (Animator_t2776330603 * __this, Quaternion_t1553702882 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Animator::GetIKPosition(UnityEngine.AvatarIKGoal)
extern "C"  Vector3_t4282066566  Animator_GetIKPosition_m385128518 (Animator_t2776330603 * __this, int32_t ___goal0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Animator::GetIKPositionInternal(UnityEngine.AvatarIKGoal)
extern "C"  Vector3_t4282066566  Animator_GetIKPositionInternal_m142003939 (Animator_t2776330603 * __this, int32_t ___goal0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::INTERNAL_CALL_GetIKPositionInternal(UnityEngine.Animator,UnityEngine.AvatarIKGoal,UnityEngine.Vector3&)
extern "C"  void Animator_INTERNAL_CALL_GetIKPositionInternal_m976909533 (Il2CppObject * __this /* static, unused */, Animator_t2776330603 * ___self0, int32_t ___goal1, Vector3_t4282066566 * ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetIKPosition(UnityEngine.AvatarIKGoal,UnityEngine.Vector3)
extern "C"  void Animator_SetIKPosition_m2542416671 (Animator_t2776330603 * __this, int32_t ___goal0, Vector3_t4282066566  ___goalPosition1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetIKPositionInternal(UnityEngine.AvatarIKGoal,UnityEngine.Vector3)
extern "C"  void Animator_SetIKPositionInternal_m335958844 (Animator_t2776330603 * __this, int32_t ___goal0, Vector3_t4282066566  ___goalPosition1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::INTERNAL_CALL_SetIKPositionInternal(UnityEngine.Animator,UnityEngine.AvatarIKGoal,UnityEngine.Vector3&)
extern "C"  void Animator_INTERNAL_CALL_SetIKPositionInternal_m647475433 (Il2CppObject * __this /* static, unused */, Animator_t2776330603 * ___self0, int32_t ___goal1, Vector3_t4282066566 * ___goalPosition2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Animator::GetIKRotation(UnityEngine.AvatarIKGoal)
extern "C"  Quaternion_t1553702882  Animator_GetIKRotation_m3297713819 (Animator_t2776330603 * __this, int32_t ___goal0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Animator::GetIKRotationInternal(UnityEngine.AvatarIKGoal)
extern "C"  Quaternion_t1553702882  Animator_GetIKRotationInternal_m2296655928 (Animator_t2776330603 * __this, int32_t ___goal0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::INTERNAL_CALL_GetIKRotationInternal(UnityEngine.Animator,UnityEngine.AvatarIKGoal,UnityEngine.Quaternion&)
extern "C"  void Animator_INTERNAL_CALL_GetIKRotationInternal_m110687330 (Il2CppObject * __this /* static, unused */, Animator_t2776330603 * ___self0, int32_t ___goal1, Quaternion_t1553702882 * ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetIKRotation(UnityEngine.AvatarIKGoal,UnityEngine.Quaternion)
extern "C"  void Animator_SetIKRotation_m2768899344 (Animator_t2776330603 * __this, int32_t ___goal0, Quaternion_t1553702882  ___goalRotation1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetIKRotationInternal(UnityEngine.AvatarIKGoal,UnityEngine.Quaternion)
extern "C"  void Animator_SetIKRotationInternal_m1596039379 (Animator_t2776330603 * __this, int32_t ___goal0, Quaternion_t1553702882  ___goalRotation1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::INTERNAL_CALL_SetIKRotationInternal(UnityEngine.Animator,UnityEngine.AvatarIKGoal,UnityEngine.Quaternion&)
extern "C"  void Animator_INTERNAL_CALL_SetIKRotationInternal_m4234652886 (Il2CppObject * __this /* static, unused */, Animator_t2776330603 * ___self0, int32_t ___goal1, Quaternion_t1553702882 * ___goalRotation2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Animator::GetIKPositionWeight(UnityEngine.AvatarIKGoal)
extern "C"  float Animator_GetIKPositionWeight_m2079943564 (Animator_t2776330603 * __this, int32_t ___goal0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Animator::GetIKPositionWeightInternal(UnityEngine.AvatarIKGoal)
extern "C"  float Animator_GetIKPositionWeightInternal_m464452649 (Animator_t2776330603 * __this, int32_t ___goal0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetIKPositionWeight(UnityEngine.AvatarIKGoal,System.Single)
extern "C"  void Animator_SetIKPositionWeight_m1110244361 (Animator_t2776330603 * __this, int32_t ___goal0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetIKPositionWeightInternal(UnityEngine.AvatarIKGoal,System.Single)
extern "C"  void Animator_SetIKPositionWeightInternal_m1498324454 (Animator_t2776330603 * __this, int32_t ___goal0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Animator::GetIKRotationWeight(UnityEngine.AvatarIKGoal)
extern "C"  float Animator_GetIKRotationWeight_m3997781473 (Animator_t2776330603 * __this, int32_t ___goal0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Animator::GetIKRotationWeightInternal(UnityEngine.AvatarIKGoal)
extern "C"  float Animator_GetIKRotationWeightInternal_m1941289342 (Animator_t2776330603 * __this, int32_t ___goal0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetIKRotationWeight(UnityEngine.AvatarIKGoal,System.Single)
extern "C"  void Animator_SetIKRotationWeight_m2279843230 (Animator_t2776330603 * __this, int32_t ___goal0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetIKRotationWeightInternal(UnityEngine.AvatarIKGoal,System.Single)
extern "C"  void Animator_SetIKRotationWeightInternal_m35709563 (Animator_t2776330603 * __this, int32_t ___goal0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Animator::GetIKHintPosition(UnityEngine.AvatarIKHint)
extern "C"  Vector3_t4282066566  Animator_GetIKHintPosition_m266045369 (Animator_t2776330603 * __this, int32_t ___hint0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Animator::GetIKHintPositionInternal(UnityEngine.AvatarIKHint)
extern "C"  Vector3_t4282066566  Animator_GetIKHintPositionInternal_m1199990614 (Animator_t2776330603 * __this, int32_t ___hint0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::INTERNAL_CALL_GetIKHintPositionInternal(UnityEngine.Animator,UnityEngine.AvatarIKHint,UnityEngine.Vector3&)
extern "C"  void Animator_INTERNAL_CALL_GetIKHintPositionInternal_m2137135224 (Il2CppObject * __this /* static, unused */, Animator_t2776330603 * ___self0, int32_t ___hint1, Vector3_t4282066566 * ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetIKHintPosition(UnityEngine.AvatarIKHint,UnityEngine.Vector3)
extern "C"  void Animator_SetIKHintPosition_m3565836306 (Animator_t2776330603 * __this, int32_t ___hint0, Vector3_t4282066566  ___hintPosition1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetIKHintPositionInternal(UnityEngine.AvatarIKHint,UnityEngine.Vector3)
extern "C"  void Animator_SetIKHintPositionInternal_m3766722863 (Animator_t2776330603 * __this, int32_t ___hint0, Vector3_t4282066566  ___hintPosition1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::INTERNAL_CALL_SetIKHintPositionInternal(UnityEngine.Animator,UnityEngine.AvatarIKHint,UnityEngine.Vector3&)
extern "C"  void Animator_INTERNAL_CALL_SetIKHintPositionInternal_m1131048580 (Il2CppObject * __this /* static, unused */, Animator_t2776330603 * ___self0, int32_t ___hint1, Vector3_t4282066566 * ___hintPosition2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Animator::GetIKHintPositionWeight(UnityEngine.AvatarIKHint)
extern "C"  float Animator_GetIKHintPositionWeight_m218720959 (Animator_t2776330603 * __this, int32_t ___hint0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Animator::GetHintWeightPositionInternal(UnityEngine.AvatarIKHint)
extern "C"  float Animator_GetHintWeightPositionInternal_m387578426 (Animator_t2776330603 * __this, int32_t ___hint0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetIKHintPositionWeight(UnityEngine.AvatarIKHint,System.Single)
extern "C"  void Animator_SetIKHintPositionWeight_m1103223292 (Animator_t2776330603 * __this, int32_t ___hint0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetIKHintPositionWeightInternal(UnityEngine.AvatarIKHint,System.Single)
extern "C"  void Animator_SetIKHintPositionWeightInternal_m3898647769 (Animator_t2776330603 * __this, int32_t ___hint0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetLookAtPosition(UnityEngine.Vector3)
extern "C"  void Animator_SetLookAtPosition_m384162552 (Animator_t2776330603 * __this, Vector3_t4282066566  ___lookAtPosition0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetLookAtPositionInternal(UnityEngine.Vector3)
extern "C"  void Animator_SetLookAtPositionInternal_m1453739067 (Animator_t2776330603 * __this, Vector3_t4282066566  ___lookAtPosition0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::INTERNAL_CALL_SetLookAtPositionInternal(UnityEngine.Animator,UnityEngine.Vector3&)
extern "C"  void Animator_INTERNAL_CALL_SetLookAtPositionInternal_m816040728 (Il2CppObject * __this /* static, unused */, Animator_t2776330603 * ___self0, Vector3_t4282066566 * ___lookAtPosition1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetLookAtWeight(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Animator_SetLookAtWeight_m881446346 (Animator_t2776330603 * __this, float ___weight0, float ___bodyWeight1, float ___headWeight2, float ___eyesWeight3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetLookAtWeight(System.Single,System.Single,System.Single)
extern "C"  void Animator_SetLookAtWeight_m96208805 (Animator_t2776330603 * __this, float ___weight0, float ___bodyWeight1, float ___headWeight2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetLookAtWeight(System.Single,System.Single)
extern "C"  void Animator_SetLookAtWeight_m3985034432 (Animator_t2776330603 * __this, float ___weight0, float ___bodyWeight1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetLookAtWeight(System.Single)
extern "C"  void Animator_SetLookAtWeight_m2035469595 (Animator_t2776330603 * __this, float ___weight0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetLookAtWeight(System.Single,System.Single,System.Single,System.Single,System.Single)
extern "C"  void Animator_SetLookAtWeight_m2843438895 (Animator_t2776330603 * __this, float ___weight0, float ___bodyWeight1, float ___headWeight2, float ___eyesWeight3, float ___clampWeight4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetLookAtWeightInternal(System.Single,System.Single,System.Single,System.Single,System.Single)
extern "C"  void Animator_SetLookAtWeightInternal_m3334195378 (Animator_t2776330603 * __this, float ___weight0, float ___bodyWeight1, float ___headWeight2, float ___eyesWeight3, float ___clampWeight4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetBoneLocalRotation(UnityEngine.HumanBodyBones,UnityEngine.Quaternion)
extern "C"  void Animator_SetBoneLocalRotation_m674237671 (Animator_t2776330603 * __this, int32_t ___humanBoneId0, Quaternion_t1553702882  ___rotation1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetBoneLocalRotationInternal(UnityEngine.HumanBodyBones,UnityEngine.Quaternion)
extern "C"  void Animator_SetBoneLocalRotationInternal_m3990136554 (Animator_t2776330603 * __this, int32_t ___humanBoneId0, Quaternion_t1553702882  ___rotation1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::INTERNAL_CALL_SetBoneLocalRotationInternal(UnityEngine.Animator,UnityEngine.HumanBodyBones,UnityEngine.Quaternion&)
extern "C"  void Animator_INTERNAL_CALL_SetBoneLocalRotationInternal_m493260331 (Il2CppObject * __this /* static, unused */, Animator_t2776330603 * ___self0, int32_t ___humanBoneId1, Quaternion_t1553702882 * ___rotation2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Animator::get_stabilizeFeet()
extern "C"  bool Animator_get_stabilizeFeet_m692080498 (Animator_t2776330603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::set_stabilizeFeet(System.Boolean)
extern "C"  void Animator_set_stabilizeFeet_m2533795611 (Animator_t2776330603 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Animator::get_layerCount()
extern "C"  int32_t Animator_get_layerCount_m3326924613 (Animator_t2776330603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Animator::GetLayerName(System.Int32)
extern "C"  String_t* Animator_GetLayerName_m3480300056 (Animator_t2776330603 * __this, int32_t ___layerIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Animator::GetLayerIndex(System.String)
extern "C"  int32_t Animator_GetLayerIndex_m4259745447 (Animator_t2776330603 * __this, String_t* ___layerName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Animator::GetLayerWeight(System.Int32)
extern "C"  float Animator_GetLayerWeight_m3878421230 (Animator_t2776330603 * __this, int32_t ___layerIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetLayerWeight(System.Int32,System.Single)
extern "C"  void Animator_SetLayerWeight_m3838560187 (Animator_t2776330603 * __this, int32_t ___layerIndex0, float ___weight1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AnimatorStateInfo UnityEngine.Animator::GetCurrentAnimatorStateInfo(System.Int32)
extern "C"  AnimatorStateInfo_t323110318  Animator_GetCurrentAnimatorStateInfo_m3061859448 (Animator_t2776330603 * __this, int32_t ___layerIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AnimatorStateInfo UnityEngine.Animator::GetNextAnimatorStateInfo(System.Int32)
extern "C"  AnimatorStateInfo_t323110318  Animator_GetNextAnimatorStateInfo_m791156688 (Animator_t2776330603 * __this, int32_t ___layerIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AnimatorTransitionInfo UnityEngine.Animator::GetAnimatorTransitionInfo(System.Int32)
extern "C"  AnimatorTransitionInfo_t2817229998  Animator_GetAnimatorTransitionInfo_m3858104711 (Animator_t2776330603 * __this, int32_t ___layerIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AnimatorClipInfo[] UnityEngine.Animator::GetCurrentAnimatorClipInfo(System.Int32)
extern "C"  AnimatorClipInfoU5BU5D_t3117370260* Animator_GetCurrentAnimatorClipInfo_m4182556072 (Animator_t2776330603 * __this, int32_t ___layerIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AnimatorClipInfo[] UnityEngine.Animator::GetNextAnimatorClipInfo(System.Int32)
extern "C"  AnimatorClipInfoU5BU5D_t3117370260* Animator_GetNextAnimatorClipInfo_m162731378 (Animator_t2776330603 * __this, int32_t ___layerIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Animator::IsInTransition(System.Int32)
extern "C"  bool Animator_IsInTransition_m2609196857 (Animator_t2776330603 * __this, int32_t ___layerIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AnimatorControllerParameter[] UnityEngine.Animator::get_parameters()
extern "C"  AnimatorControllerParameterU5BU5D_t1853597653* Animator_get_parameters_m3704707859 (Animator_t2776330603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Animator::get_parameterCount()
extern "C"  int32_t Animator_get_parameterCount_m1460564717 (Animator_t2776330603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AnimatorControllerParameter UnityEngine.Animator::GetParameter(System.Int32)
extern "C"  AnimatorControllerParameter_t3339705788 * Animator_GetParameter_m1479682598 (Animator_t2776330603 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Animator::get_feetPivotActive()
extern "C"  float Animator_get_feetPivotActive_m4087239689 (Animator_t2776330603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::set_feetPivotActive(System.Single)
extern "C"  void Animator_set_feetPivotActive_m293215914 (Animator_t2776330603 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Animator::get_pivotWeight()
extern "C"  float Animator_get_pivotWeight_m1500566793 (Animator_t2776330603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Animator::get_pivotPosition()
extern "C"  Vector3_t4282066566  Animator_get_pivotPosition_m1479929804 (Animator_t2776330603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::INTERNAL_get_pivotPosition(UnityEngine.Vector3&)
extern "C"  void Animator_INTERNAL_get_pivotPosition_m1941126065 (Animator_t2776330603 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::MatchTarget(UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.AvatarTarget,UnityEngine.MatchTargetWeightMask,System.Single,System.Single)
extern "C"  void Animator_MatchTarget_m4236652838 (Animator_t2776330603 * __this, Vector3_t4282066566  ___matchPosition0, Quaternion_t1553702882  ___matchRotation1, int32_t ___targetBodyPart2, MatchTargetWeightMask_t258413904  ___weightMask3, float ___startNormalizedTime4, float ___targetNormalizedTime5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::MatchTarget(UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.AvatarTarget,UnityEngine.MatchTargetWeightMask,System.Single)
extern "C"  void Animator_MatchTarget_m2080979969 (Animator_t2776330603 * __this, Vector3_t4282066566  ___matchPosition0, Quaternion_t1553702882  ___matchRotation1, int32_t ___targetBodyPart2, MatchTargetWeightMask_t258413904  ___weightMask3, float ___startNormalizedTime4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::INTERNAL_CALL_MatchTarget(UnityEngine.Animator,UnityEngine.Vector3&,UnityEngine.Quaternion&,UnityEngine.AvatarTarget,UnityEngine.MatchTargetWeightMask&,System.Single,System.Single)
extern "C"  void Animator_INTERNAL_CALL_MatchTarget_m2875283135 (Il2CppObject * __this /* static, unused */, Animator_t2776330603 * ___self0, Vector3_t4282066566 * ___matchPosition1, Quaternion_t1553702882 * ___matchRotation2, int32_t ___targetBodyPart3, MatchTargetWeightMask_t258413904 * ___weightMask4, float ___startNormalizedTime5, float ___targetNormalizedTime6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::InterruptMatchTarget(System.Boolean)
extern "C"  void Animator_InterruptMatchTarget_m3501603944 (Animator_t2776330603 * __this, bool ___completeMatch0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::InterruptMatchTarget()
extern "C"  void Animator_InterruptMatchTarget_m1664553137 (Animator_t2776330603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Animator::get_isMatchingTarget()
extern "C"  bool Animator_get_isMatchingTarget_m3696235301 (Animator_t2776330603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Animator::get_speed()
extern "C"  float Animator_get_speed_m1893369654 (Animator_t2776330603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::set_speed(System.Single)
extern "C"  void Animator_set_speed_m2513936029 (Animator_t2776330603 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::CrossFadeInFixedTime(System.String,System.Single,System.Int32)
extern "C"  void Animator_CrossFadeInFixedTime_m1159460910 (Animator_t2776330603 * __this, String_t* ___stateName0, float ___transitionDuration1, int32_t ___layer2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::CrossFadeInFixedTime(System.String,System.Single)
extern "C"  void Animator_CrossFadeInFixedTime_m1639088969 (Animator_t2776330603 * __this, String_t* ___stateName0, float ___transitionDuration1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::CrossFadeInFixedTime(System.String,System.Single,System.Int32,System.Single)
extern "C"  void Animator_CrossFadeInFixedTime_m1806801555 (Animator_t2776330603 * __this, String_t* ___stateName0, float ___transitionDuration1, int32_t ___layer2, float ___fixedTime3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::CrossFadeInFixedTime(System.Int32,System.Single,System.Int32,System.Single)
extern "C"  void Animator_CrossFadeInFixedTime_m791345576 (Animator_t2776330603 * __this, int32_t ___stateNameHash0, float ___transitionDuration1, int32_t ___layer2, float ___fixedTime3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::CrossFadeInFixedTime(System.Int32,System.Single,System.Int32)
extern "C"  void Animator_CrossFadeInFixedTime_m1605513219 (Animator_t2776330603 * __this, int32_t ___stateNameHash0, float ___transitionDuration1, int32_t ___layer2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::CrossFadeInFixedTime(System.Int32,System.Single)
extern "C"  void Animator_CrossFadeInFixedTime_m3239392596 (Animator_t2776330603 * __this, int32_t ___stateNameHash0, float ___transitionDuration1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::CrossFade(System.String,System.Single,System.Int32)
extern "C"  void Animator_CrossFade_m1664571312 (Animator_t2776330603 * __this, String_t* ___stateName0, float ___transitionDuration1, int32_t ___layer2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::CrossFade(System.String,System.Single)
extern "C"  void Animator_CrossFade_m1472731271 (Animator_t2776330603 * __this, String_t* ___stateName0, float ___transitionDuration1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::CrossFade(System.String,System.Single,System.Int32,System.Single)
extern "C"  void Animator_CrossFade_m1281797781 (Animator_t2776330603 * __this, String_t* ___stateName0, float ___transitionDuration1, int32_t ___layer2, float ___normalizedTime3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::CrossFade(System.Int32,System.Single,System.Int32,System.Single)
extern "C"  void Animator_CrossFade_m358767974 (Animator_t2776330603 * __this, int32_t ___stateNameHash0, float ___transitionDuration1, int32_t ___layer2, float ___normalizedTime3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::CrossFade(System.Int32,System.Single,System.Int32)
extern "C"  void Animator_CrossFade_m3838564417 (Animator_t2776330603 * __this, int32_t ___stateNameHash0, float ___transitionDuration1, int32_t ___layer2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::CrossFade(System.Int32,System.Single)
extern "C"  void Animator_CrossFade_m2541289558 (Animator_t2776330603 * __this, int32_t ___stateNameHash0, float ___transitionDuration1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::PlayInFixedTime(System.String,System.Int32)
extern "C"  void Animator_PlayInFixedTime_m2735684801 (Animator_t2776330603 * __this, String_t* ___stateName0, int32_t ___layer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::PlayInFixedTime(System.String)
extern "C"  void Animator_PlayInFixedTime_m688671702 (Animator_t2776330603 * __this, String_t* ___stateName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::PlayInFixedTime(System.String,System.Int32,System.Single)
extern "C"  void Animator_PlayInFixedTime_m3315327462 (Animator_t2776330603 * __this, String_t* ___stateName0, int32_t ___layer1, float ___fixedTime2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::PlayInFixedTime(System.Int32,System.Int32,System.Single)
extern "C"  void Animator_PlayInFixedTime_m3229413215 (Animator_t2776330603 * __this, int32_t ___stateNameHash0, int32_t ___layer1, float ___fixedTime2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::PlayInFixedTime(System.Int32,System.Int32)
extern "C"  void Animator_PlayInFixedTime_m486171130 (Animator_t2776330603 * __this, int32_t ___stateNameHash0, int32_t ___layer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::PlayInFixedTime(System.Int32)
extern "C"  void Animator_PlayInFixedTime_m4163902973 (Animator_t2776330603 * __this, int32_t ___stateNameHash0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::Play(System.String,System.Int32)
extern "C"  void Animator_Play_m3991311079 (Animator_t2776330603 * __this, String_t* ___stateName0, int32_t ___layer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::Play(System.String)
extern "C"  void Animator_Play_m2424696816 (Animator_t2776330603 * __this, String_t* ___stateName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::Play(System.String,System.Int32,System.Single)
extern "C"  void Animator_Play_m3631644044 (Animator_t2776330603 * __this, String_t* ___stateName0, int32_t ___layer1, float ___normalizedTime2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::Play(System.Int32,System.Int32,System.Single)
extern "C"  void Animator_Play_m330123001 (Animator_t2776330603 * __this, int32_t ___stateNameHash0, int32_t ___layer1, float ___normalizedTime2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::Play(System.Int32,System.Int32)
extern "C"  void Animator_Play_m1080864532 (Animator_t2776330603 * __this, int32_t ___stateNameHash0, int32_t ___layer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::Play(System.Int32)
extern "C"  void Animator_Play_m4081356451 (Animator_t2776330603 * __this, int32_t ___stateNameHash0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetTarget(UnityEngine.AvatarTarget,System.Single)
extern "C"  void Animator_SetTarget_m3335381339 (Animator_t2776330603 * __this, int32_t ___targetIndex0, float ___targetNormalizedTime1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Animator::get_targetPosition()
extern "C"  Vector3_t4282066566  Animator_get_targetPosition_m3699919451 (Animator_t2776330603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::INTERNAL_get_targetPosition(UnityEngine.Vector3&)
extern "C"  void Animator_INTERNAL_get_targetPosition_m656463196 (Animator_t2776330603 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Animator::get_targetRotation()
extern "C"  Quaternion_t1553702882  Animator_get_targetRotation_m2080040752 (Animator_t2776330603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::INTERNAL_get_targetRotation(UnityEngine.Quaternion&)
extern "C"  void Animator_INTERNAL_get_targetRotation_m168007107 (Animator_t2776330603 * __this, Quaternion_t1553702882 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.Animator::GetBoneTransform(UnityEngine.HumanBodyBones)
extern "C"  Transform_t1659122786 * Animator_GetBoneTransform_m3449809847 (Animator_t2776330603 * __this, int32_t ___humanBoneId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AnimatorCullingMode UnityEngine.Animator::get_cullingMode()
extern "C"  int32_t Animator_get_cullingMode_m1008819440 (Animator_t2776330603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::set_cullingMode(UnityEngine.AnimatorCullingMode)
extern "C"  void Animator_set_cullingMode_m773355747 (Animator_t2776330603 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::StartPlayback()
extern "C"  void Animator_StartPlayback_m3009121761 (Animator_t2776330603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::StopPlayback()
extern "C"  void Animator_StopPlayback_m3612462619 (Animator_t2776330603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Animator::get_playbackTime()
extern "C"  float Animator_get_playbackTime_m3871048475 (Animator_t2776330603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::set_playbackTime(System.Single)
extern "C"  void Animator_set_playbackTime_m281288968 (Animator_t2776330603 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::StartRecording(System.Int32)
extern "C"  void Animator_StartRecording_m2916226974 (Animator_t2776330603 * __this, int32_t ___frameCount0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::StopRecording()
extern "C"  void Animator_StopRecording_m4232410323 (Animator_t2776330603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Animator::get_recorderStartTime()
extern "C"  float Animator_get_recorderStartTime_m1936545440 (Animator_t2776330603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::set_recorderStartTime(System.Single)
extern "C"  void Animator_set_recorderStartTime_m1923305331 (Animator_t2776330603 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Animator::get_recorderStopTime()
extern "C"  float Animator_get_recorderStopTime_m3086287392 (Animator_t2776330603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::set_recorderStopTime(System.Single)
extern "C"  void Animator_set_recorderStopTime_m1593108003 (Animator_t2776330603 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AnimatorRecorderMode UnityEngine.Animator::get_recorderMode()
extern "C"  int32_t Animator_get_recorderMode_m3604968344 (Animator_t2776330603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RuntimeAnimatorController UnityEngine.Animator::get_runtimeAnimatorController()
extern "C"  RuntimeAnimatorController_t274649809 * Animator_get_runtimeAnimatorController_m1822082727 (Animator_t2776330603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::set_runtimeAnimatorController(UnityEngine.RuntimeAnimatorController)
extern "C"  void Animator_set_runtimeAnimatorController_m3435370636 (Animator_t2776330603 * __this, RuntimeAnimatorController_t274649809 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Animator::HasState(System.Int32,System.Int32)
extern "C"  bool Animator_HasState_m4176136363 (Animator_t2776330603 * __this, int32_t ___layerIndex0, int32_t ___stateID1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Animator::StringToHash(System.String)
extern "C"  int32_t Animator_StringToHash_m4020897098 (Il2CppObject * __this /* static, unused */, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Avatar UnityEngine.Animator::get_avatar()
extern "C"  Avatar_t2688887197 * Animator_get_avatar_m2481687871 (Animator_t2776330603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::set_avatar(UnityEngine.Avatar)
extern "C"  void Animator_set_avatar_m1835805316 (Animator_t2776330603 * __this, Avatar_t2688887197 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::CheckIfInIKPass()
extern "C"  void Animator_CheckIfInIKPass_m2875692449 (Animator_t2776330603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Animator::CheckIfInIKPassInternal()
extern "C"  bool Animator_CheckIfInIKPassInternal_m914167192 (Animator_t2776330603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetFloatString(System.String,System.Single)
extern "C"  void Animator_SetFloatString_m82432478 (Animator_t2776330603 * __this, String_t* ___name0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetFloatID(System.Int32,System.Single)
extern "C"  void Animator_SetFloatID_m819072393 (Animator_t2776330603 * __this, int32_t ___id0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Animator::GetFloatString(System.String)
extern "C"  float Animator_GetFloatString_m1552529953 (Animator_t2776330603 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Animator::GetFloatID(System.Int32)
extern "C"  float Animator_GetFloatID_m373159228 (Animator_t2776330603 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetBoolString(System.String,System.Boolean)
extern "C"  void Animator_SetBoolString_m275475356 (Animator_t2776330603 * __this, String_t* ___name0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetBoolID(System.Int32,System.Boolean)
extern "C"  void Animator_SetBoolID_m516262497 (Animator_t2776330603 * __this, int32_t ___id0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Animator::GetBoolString(System.String)
extern "C"  bool Animator_GetBoolString_m1806558899 (Animator_t2776330603 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Animator::GetBoolID(System.Int32)
extern "C"  bool Animator_GetBoolID_m1397798250 (Animator_t2776330603 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetIntegerString(System.String,System.Int32)
extern "C"  void Animator_SetIntegerString_m1111174752 (Animator_t2776330603 * __this, String_t* ___name0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetIntegerID(System.Int32,System.Int32)
extern "C"  void Animator_SetIntegerID_m276290769 (Animator_t2776330603 * __this, int32_t ___id0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Animator::GetIntegerString(System.String)
extern "C"  int32_t Animator_GetIntegerString_m3084017739 (Animator_t2776330603 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Animator::GetIntegerID(System.Int32)
extern "C"  int32_t Animator_GetIntegerID_m4210859218 (Animator_t2776330603 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetTriggerString(System.String)
extern "C"  void Animator_SetTriggerString_m1378271133 (Animator_t2776330603 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetTriggerID(System.Int32)
extern "C"  void Animator_SetTriggerID_m3803869760 (Animator_t2776330603 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::ResetTriggerString(System.String)
extern "C"  void Animator_ResetTriggerString_m1817269834 (Animator_t2776330603 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::ResetTriggerID(System.Int32)
extern "C"  void Animator_ResetTriggerID_m619553651 (Animator_t2776330603 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Animator::IsParameterControlledByCurveString(System.String)
extern "C"  bool Animator_IsParameterControlledByCurveString_m1231885762 (Animator_t2776330603 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Animator::IsParameterControlledByCurveID(System.Int32)
extern "C"  bool Animator_IsParameterControlledByCurveID_m837163259 (Animator_t2776330603 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetFloatStringDamp(System.String,System.Single,System.Single,System.Single)
extern "C"  void Animator_SetFloatStringDamp_m2274151144 (Animator_t2776330603 * __this, String_t* ___name0, float ___value1, float ___dampTime2, float ___deltaTime3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetFloatIDDamp(System.Int32,System.Single,System.Single,System.Single)
extern "C"  void Animator_SetFloatIDDamp_m4107886867 (Animator_t2776330603 * __this, int32_t ___id0, float ___value1, float ___dampTime2, float ___deltaTime3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Animator::get_layersAffectMassCenter()
extern "C"  bool Animator_get_layersAffectMassCenter_m3409296173 (Animator_t2776330603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::set_layersAffectMassCenter(System.Boolean)
extern "C"  void Animator_set_layersAffectMassCenter_m1296962674 (Animator_t2776330603 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Animator::get_leftFeetBottomHeight()
extern "C"  float Animator_get_leftFeetBottomHeight_m4166530042 (Animator_t2776330603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Animator::get_rightFeetBottomHeight()
extern "C"  float Animator_get_rightFeetBottomHeight_m3839016939 (Animator_t2776330603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::Update(System.Single)
extern "C"  void Animator_Update_m1710797732 (Animator_t2776330603 * __this, float ___deltaTime0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::Rebind()
extern "C"  void Animator_Rebind_m739231694 (Animator_t2776330603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::ApplyBuiltinRootMotion()
extern "C"  void Animator_ApplyBuiltinRootMotion_m566328203 (Animator_t2776330603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Animator::get_logWarnings()
extern "C"  bool Animator_get_logWarnings_m1817672208 (Animator_t2776330603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::set_logWarnings(System.Boolean)
extern "C"  void Animator_set_logWarnings_m481601977 (Animator_t2776330603 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Animator::get_fireEvents()
extern "C"  bool Animator_get_fireEvents_m414434716 (Animator_t2776330603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::set_fireEvents(System.Boolean)
extern "C"  void Animator_set_fireEvents_m3107493281 (Animator_t2776330603 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
