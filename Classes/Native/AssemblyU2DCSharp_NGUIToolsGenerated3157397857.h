﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// MethodID
struct MethodID_t3916401116;
// JSDataExchangeMgr/DGetV`1<UIWidget[]>
struct DGetV_1_t4114639542;
// JSDataExchangeMgr/DGetV`1<System.Byte[]>
struct DGetV_1_t4138411810;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NGUIToolsGenerated
struct  NGUIToolsGenerated_t3157397857  : public Il2CppObject
{
public:

public:
};

struct NGUIToolsGenerated_t3157397857_StaticFields
{
public:
	// MethodID NGUIToolsGenerated::methodID2
	MethodID_t3916401116 * ___methodID2_0;
	// MethodID NGUIToolsGenerated::methodID4
	MethodID_t3916401116 * ___methodID4_1;
	// MethodID NGUIToolsGenerated::methodID5
	MethodID_t3916401116 * ___methodID5_2;
	// MethodID NGUIToolsGenerated::methodID7
	MethodID_t3916401116 * ___methodID7_3;
	// MethodID NGUIToolsGenerated::methodID8
	MethodID_t3916401116 * ___methodID8_4;
	// MethodID NGUIToolsGenerated::methodID24
	MethodID_t3916401116 * ___methodID24_5;
	// MethodID NGUIToolsGenerated::methodID25
	MethodID_t3916401116 * ___methodID25_6;
	// MethodID NGUIToolsGenerated::methodID26
	MethodID_t3916401116 * ___methodID26_7;
	// MethodID NGUIToolsGenerated::methodID28
	MethodID_t3916401116 * ___methodID28_8;
	// MethodID NGUIToolsGenerated::methodID29
	MethodID_t3916401116 * ___methodID29_9;
	// MethodID NGUIToolsGenerated::methodID40
	MethodID_t3916401116 * ___methodID40_10;
	// JSDataExchangeMgr/DGetV`1<UIWidget[]> NGUIToolsGenerated::<>f__am$cacheB
	DGetV_1_t4114639542 * ___U3CU3Ef__amU24cacheB_11;
	// JSDataExchangeMgr/DGetV`1<System.Byte[]> NGUIToolsGenerated::<>f__am$cacheC
	DGetV_1_t4138411810 * ___U3CU3Ef__amU24cacheC_12;

public:
	inline static int32_t get_offset_of_methodID2_0() { return static_cast<int32_t>(offsetof(NGUIToolsGenerated_t3157397857_StaticFields, ___methodID2_0)); }
	inline MethodID_t3916401116 * get_methodID2_0() const { return ___methodID2_0; }
	inline MethodID_t3916401116 ** get_address_of_methodID2_0() { return &___methodID2_0; }
	inline void set_methodID2_0(MethodID_t3916401116 * value)
	{
		___methodID2_0 = value;
		Il2CppCodeGenWriteBarrier(&___methodID2_0, value);
	}

	inline static int32_t get_offset_of_methodID4_1() { return static_cast<int32_t>(offsetof(NGUIToolsGenerated_t3157397857_StaticFields, ___methodID4_1)); }
	inline MethodID_t3916401116 * get_methodID4_1() const { return ___methodID4_1; }
	inline MethodID_t3916401116 ** get_address_of_methodID4_1() { return &___methodID4_1; }
	inline void set_methodID4_1(MethodID_t3916401116 * value)
	{
		___methodID4_1 = value;
		Il2CppCodeGenWriteBarrier(&___methodID4_1, value);
	}

	inline static int32_t get_offset_of_methodID5_2() { return static_cast<int32_t>(offsetof(NGUIToolsGenerated_t3157397857_StaticFields, ___methodID5_2)); }
	inline MethodID_t3916401116 * get_methodID5_2() const { return ___methodID5_2; }
	inline MethodID_t3916401116 ** get_address_of_methodID5_2() { return &___methodID5_2; }
	inline void set_methodID5_2(MethodID_t3916401116 * value)
	{
		___methodID5_2 = value;
		Il2CppCodeGenWriteBarrier(&___methodID5_2, value);
	}

	inline static int32_t get_offset_of_methodID7_3() { return static_cast<int32_t>(offsetof(NGUIToolsGenerated_t3157397857_StaticFields, ___methodID7_3)); }
	inline MethodID_t3916401116 * get_methodID7_3() const { return ___methodID7_3; }
	inline MethodID_t3916401116 ** get_address_of_methodID7_3() { return &___methodID7_3; }
	inline void set_methodID7_3(MethodID_t3916401116 * value)
	{
		___methodID7_3 = value;
		Il2CppCodeGenWriteBarrier(&___methodID7_3, value);
	}

	inline static int32_t get_offset_of_methodID8_4() { return static_cast<int32_t>(offsetof(NGUIToolsGenerated_t3157397857_StaticFields, ___methodID8_4)); }
	inline MethodID_t3916401116 * get_methodID8_4() const { return ___methodID8_4; }
	inline MethodID_t3916401116 ** get_address_of_methodID8_4() { return &___methodID8_4; }
	inline void set_methodID8_4(MethodID_t3916401116 * value)
	{
		___methodID8_4 = value;
		Il2CppCodeGenWriteBarrier(&___methodID8_4, value);
	}

	inline static int32_t get_offset_of_methodID24_5() { return static_cast<int32_t>(offsetof(NGUIToolsGenerated_t3157397857_StaticFields, ___methodID24_5)); }
	inline MethodID_t3916401116 * get_methodID24_5() const { return ___methodID24_5; }
	inline MethodID_t3916401116 ** get_address_of_methodID24_5() { return &___methodID24_5; }
	inline void set_methodID24_5(MethodID_t3916401116 * value)
	{
		___methodID24_5 = value;
		Il2CppCodeGenWriteBarrier(&___methodID24_5, value);
	}

	inline static int32_t get_offset_of_methodID25_6() { return static_cast<int32_t>(offsetof(NGUIToolsGenerated_t3157397857_StaticFields, ___methodID25_6)); }
	inline MethodID_t3916401116 * get_methodID25_6() const { return ___methodID25_6; }
	inline MethodID_t3916401116 ** get_address_of_methodID25_6() { return &___methodID25_6; }
	inline void set_methodID25_6(MethodID_t3916401116 * value)
	{
		___methodID25_6 = value;
		Il2CppCodeGenWriteBarrier(&___methodID25_6, value);
	}

	inline static int32_t get_offset_of_methodID26_7() { return static_cast<int32_t>(offsetof(NGUIToolsGenerated_t3157397857_StaticFields, ___methodID26_7)); }
	inline MethodID_t3916401116 * get_methodID26_7() const { return ___methodID26_7; }
	inline MethodID_t3916401116 ** get_address_of_methodID26_7() { return &___methodID26_7; }
	inline void set_methodID26_7(MethodID_t3916401116 * value)
	{
		___methodID26_7 = value;
		Il2CppCodeGenWriteBarrier(&___methodID26_7, value);
	}

	inline static int32_t get_offset_of_methodID28_8() { return static_cast<int32_t>(offsetof(NGUIToolsGenerated_t3157397857_StaticFields, ___methodID28_8)); }
	inline MethodID_t3916401116 * get_methodID28_8() const { return ___methodID28_8; }
	inline MethodID_t3916401116 ** get_address_of_methodID28_8() { return &___methodID28_8; }
	inline void set_methodID28_8(MethodID_t3916401116 * value)
	{
		___methodID28_8 = value;
		Il2CppCodeGenWriteBarrier(&___methodID28_8, value);
	}

	inline static int32_t get_offset_of_methodID29_9() { return static_cast<int32_t>(offsetof(NGUIToolsGenerated_t3157397857_StaticFields, ___methodID29_9)); }
	inline MethodID_t3916401116 * get_methodID29_9() const { return ___methodID29_9; }
	inline MethodID_t3916401116 ** get_address_of_methodID29_9() { return &___methodID29_9; }
	inline void set_methodID29_9(MethodID_t3916401116 * value)
	{
		___methodID29_9 = value;
		Il2CppCodeGenWriteBarrier(&___methodID29_9, value);
	}

	inline static int32_t get_offset_of_methodID40_10() { return static_cast<int32_t>(offsetof(NGUIToolsGenerated_t3157397857_StaticFields, ___methodID40_10)); }
	inline MethodID_t3916401116 * get_methodID40_10() const { return ___methodID40_10; }
	inline MethodID_t3916401116 ** get_address_of_methodID40_10() { return &___methodID40_10; }
	inline void set_methodID40_10(MethodID_t3916401116 * value)
	{
		___methodID40_10 = value;
		Il2CppCodeGenWriteBarrier(&___methodID40_10, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheB_11() { return static_cast<int32_t>(offsetof(NGUIToolsGenerated_t3157397857_StaticFields, ___U3CU3Ef__amU24cacheB_11)); }
	inline DGetV_1_t4114639542 * get_U3CU3Ef__amU24cacheB_11() const { return ___U3CU3Ef__amU24cacheB_11; }
	inline DGetV_1_t4114639542 ** get_address_of_U3CU3Ef__amU24cacheB_11() { return &___U3CU3Ef__amU24cacheB_11; }
	inline void set_U3CU3Ef__amU24cacheB_11(DGetV_1_t4114639542 * value)
	{
		___U3CU3Ef__amU24cacheB_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheB_11, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheC_12() { return static_cast<int32_t>(offsetof(NGUIToolsGenerated_t3157397857_StaticFields, ___U3CU3Ef__amU24cacheC_12)); }
	inline DGetV_1_t4138411810 * get_U3CU3Ef__amU24cacheC_12() const { return ___U3CU3Ef__amU24cacheC_12; }
	inline DGetV_1_t4138411810 ** get_address_of_U3CU3Ef__amU24cacheC_12() { return &___U3CU3Ef__amU24cacheC_12; }
	inline void set_U3CU3Ef__amU24cacheC_12(DGetV_1_t4138411810 * value)
	{
		___U3CU3Ef__amU24cacheC_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheC_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
