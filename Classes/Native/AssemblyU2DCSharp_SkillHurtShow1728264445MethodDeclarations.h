﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SkillHurtShow
struct SkillHurtShow_t1728264445;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// CombatEntity
struct CombatEntity_t684137495;
// Mihua.Asset.ABLoadOperation.AssetOperation
struct AssetOperation_t778728221;
// UIPanel
struct UIPanel_t295209936;
// IZUpdate
struct IZUpdate_t3482043738;
// Skill
struct Skill_t79944241;
// CSDatacfgManager
struct CSDatacfgManager_t1565254243;
// System.Collections.Hashtable
struct Hashtable_t1407064410;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// UILabel
struct UILabel_t291504320;
// UIFont
struct UIFont_t2503090435;
// UISprite
struct UISprite_t661437049;
// System.String
struct String_t;
// UITweener
struct UITweener_t105489188;
// UnityEngine.Object
struct Object_t3071478659;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "AssemblyU2DCSharp_CombatEntity684137495.h"
#include "AssemblyU2DCSharp_FightEnum_ESkillFunType2084611099.h"
#include "AssemblyU2DCSharp_Mihua_Asset_ABLoadOperation_Asset778728221.h"
#include "AssemblyU2DCSharp_UIPanel295209936.h"
#include "AssemblyU2DCSharp_SkillHurtShow1728264445.h"
#include "AssemblyU2DCSharp_UILabel291504320.h"
#include "AssemblyU2DCSharp_UIFont2503090435.h"
#include "AssemblyU2DCSharp_UISprite661437049.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_UITweener105489188.h"
#include "mscorlib_System_Collections_Hashtable1407064410.h"

// System.Void SkillHurtShow::.ctor()
extern "C"  void SkillHurtShow__ctor_m812315918 (SkillHurtShow_t1728264445 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SkillHurtShow::get_isUsing()
extern "C"  bool SkillHurtShow_get_isUsing_m1331142673 (SkillHurtShow_t1728264445 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SkillHurtShow::set_isUsing(System.Boolean)
extern "C"  void SkillHurtShow_set_isUsing_m2447040288 (SkillHurtShow_t1728264445 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SkillHurtShow::OnAwake(UnityEngine.GameObject)
extern "C"  void SkillHurtShow_OnAwake_m483528394 (SkillHurtShow_t1728264445 * __this, GameObject_t3674682005 * ___viewGO0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SkillHurtShow::OnDestroy()
extern "C"  void SkillHurtShow_OnDestroy_m4169110279 (SkillHurtShow_t1728264445 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SkillHurtShow::StartUsingSkill(CEvent.ZEvent)
extern "C"  void SkillHurtShow_StartUsingSkill_m3384082816 (SkillHurtShow_t1728264445 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SkillHurtShow::ClearSkillShow(System.Int32,System.Int32)
extern "C"  void SkillHurtShow_ClearSkillShow_m3793888751 (SkillHurtShow_t1728264445 * __this, int32_t ___heroid0, int32_t ___skillid1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SkillHurtShow::RemoveShootSkillChild(CombatEntity,System.Int32)
extern "C"  void SkillHurtShow_RemoveShootSkillChild_m3890535196 (SkillHurtShow_t1728264445 * __this, CombatEntity_t684137495 * ___hero0, int32_t ___skillId1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SkillHurtShow::AddSkillHurt(CombatEntity,CombatEntity,System.Int32,System.Int32)
extern "C"  void SkillHurtShow_AddSkillHurt_m993680849 (SkillHurtShow_t1728264445 * __this, CombatEntity_t684137495 * ___hero0, CombatEntity_t684137495 * ___monster1, int32_t ___hurt2, int32_t ___skillId3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SkillHurtShow::AddSkillHurt(CombatEntity,CombatEntity,System.Int32)
extern "C"  void SkillHurtShow_AddSkillHurt_m3121656006 (SkillHurtShow_t1728264445 * __this, CombatEntity_t684137495 * ___entiy0, CombatEntity_t684137495 * ___monster1, int32_t ___hurt2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SkillHurtShow::Play(CombatEntity,FightEnum.ESkillFunType,System.Int32,System.Boolean)
extern "C"  void SkillHurtShow_Play_m827128533 (SkillHurtShow_t1728264445 * __this, CombatEntity_t684137495 * ___hero0, int32_t ___type1, int32_t ___skillId2, bool ___isLast3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SkillHurtShow::Play(CombatEntity,System.Int32,System.Int32)
extern "C"  void SkillHurtShow_Play_m3896743315 (SkillHurtShow_t1728264445 * __this, CombatEntity_t684137495 * ___hero0, int32_t ___teamSkillType1, int32_t ___timeType2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SkillHurtShow::isTeamSkill(System.Int32)
extern "C"  bool SkillHurtShow_isTeamSkill_m4080875131 (SkillHurtShow_t1728264445 * __this, int32_t ___skillid0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SkillHurtShow::SetHurtNum(System.Int32)
extern "C"  void SkillHurtShow_SetHurtNum_m645138556 (SkillHurtShow_t1728264445 * __this, int32_t ___hurt0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SkillHurtShow::ZUpdate()
extern "C"  void SkillHurtShow_ZUpdate_m2815045775 (SkillHurtShow_t1728264445 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SkillHurtShow::<OnAwake>m__489(Mihua.Asset.ABLoadOperation.AssetOperation)
extern "C"  void SkillHurtShow_U3COnAwakeU3Em__489_m2924616303 (SkillHurtShow_t1728264445 * __this, AssetOperation_t778728221 * ___ao0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SkillHurtShow::ilo_set_alpha1(UIPanel,System.Single)
extern "C"  void SkillHurtShow_ilo_set_alpha1_m2267634894 (Il2CppObject * __this /* static, unused */, UIPanel_t295209936 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SkillHurtShow::ilo_RemoveUpdate2(IZUpdate)
extern "C"  void SkillHurtShow_ilo_RemoveUpdate2_m1796232740 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___update0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Skill SkillHurtShow::ilo_GetActiveSkill3(CombatEntity)
extern "C"  Skill_t79944241 * SkillHurtShow_ilo_GetActiveSkill3_m3096862786 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CSDatacfgManager SkillHurtShow::ilo_get_CfgDataMgr4()
extern "C"  CSDatacfgManager_t1565254243 * SkillHurtShow_ilo_get_CfgDataMgr4_m653519508 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SkillHurtShow::ilo_set_isUsing5(SkillHurtShow,System.Boolean)
extern "C"  void SkillHurtShow_ilo_set_isUsing5_m2712774153 (Il2CppObject * __this /* static, unused */, SkillHurtShow_t1728264445 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SkillHurtShow::ilo_isTeamSkill6(SkillHurtShow,System.Int32)
extern "C"  bool SkillHurtShow_ilo_isTeamSkill6_m3613095299 (Il2CppObject * __this /* static, unused */, SkillHurtShow_t1728264445 * ____this0, int32_t ___skillid1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SkillHurtShow::ilo_get_isUsing7(SkillHurtShow)
extern "C"  bool SkillHurtShow_ilo_get_isUsing7_m4229475674 (Il2CppObject * __this /* static, unused */, SkillHurtShow_t1728264445 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SkillHurtShow::ilo_get_id8(CombatEntity)
extern "C"  int32_t SkillHurtShow_ilo_get_id8_m932551460 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Hashtable SkillHurtShow::ilo_Hash9(System.Object[])
extern "C"  Hashtable_t1407064410 * SkillHurtShow_ilo_Hash9_m2071195673 (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t1108656482* ___args0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SkillHurtShow::ilo_set_bitmapFont10(UILabel,UIFont)
extern "C"  void SkillHurtShow_ilo_set_bitmapFont10_m398819252 (Il2CppObject * __this /* static, unused */, UILabel_t291504320 * ____this0, UIFont_t2503090435 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SkillHurtShow::ilo_set_spriteName11(UISprite,System.String)
extern "C"  void SkillHurtShow_ilo_set_spriteName11_m3510218617 (Il2CppObject * __this /* static, unused */, UISprite_t661437049 * ____this0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SkillHurtShow::ilo_set_text12(UILabel,System.String)
extern "C"  void SkillHurtShow_ilo_set_text12_m1403802458 (Il2CppObject * __this /* static, unused */, UILabel_t291504320 * ____this0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SkillHurtShow::ilo_PlayForward13(UITweener)
extern "C"  void SkillHurtShow_ilo_PlayForward13_m3134307298 (Il2CppObject * __this /* static, unused */, UITweener_t105489188 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SkillHurtShow::ilo_ScaleTo14(UnityEngine.GameObject,System.Collections.Hashtable)
extern "C"  void SkillHurtShow_ilo_ScaleTo14_m1284582085 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___target0, Hashtable_t1407064410 * ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object SkillHurtShow::ilo_GetAsset15(Mihua.Asset.ABLoadOperation.AssetOperation)
extern "C"  Object_t3071478659 * SkillHurtShow_ilo_GetAsset15_m2721983943 (Il2CppObject * __this /* static, unused */, AssetOperation_t778728221 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
