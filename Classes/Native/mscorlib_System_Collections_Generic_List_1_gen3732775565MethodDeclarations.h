﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1244034627MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1<Pathfinding.Util.TileHandler/TileType>::.ctor()
#define List_1__ctor_m2852441500(__this, method) ((  void (*) (List_1_t3732775565 *, const MethodInfo*))List_1__ctor_m1239231859_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Util.TileHandler/TileType>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m3935423852(__this, ___collection0, method) ((  void (*) (List_1_t3732775565 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m1160795371_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Util.TileHandler/TileType>::.ctor(System.Int32)
#define List_1__ctor_m319127044(__this, ___capacity0, method) ((  void (*) (List_1_t3732775565 *, int32_t, const MethodInfo*))List_1__ctor_m3643386469_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Util.TileHandler/TileType>::.cctor()
#define List_1__cctor_m1584559322(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m3826137881_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Pathfinding.Util.TileHandler/TileType>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2299353029(__this, method) ((  Il2CppObject* (*) (List_1_t3732775565 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2808422246_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Util.TileHandler/TileType>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m963795825(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t3732775565 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m4034025648_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<Pathfinding.Util.TileHandler/TileType>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m3039918252(__this, method) ((  Il2CppObject * (*) (List_1_t3732775565 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m1841330603_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Pathfinding.Util.TileHandler/TileType>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m110428613(__this, ___item0, method) ((  int32_t (*) (List_1_t3732775565 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m3794749222_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<Pathfinding.Util.TileHandler/TileType>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m1291552551(__this, ___item0, method) ((  bool (*) (List_1_t3732775565 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m2659633254_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<Pathfinding.Util.TileHandler/TileType>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m2084883869(__this, ___item0, method) ((  int32_t (*) (List_1_t3732775565 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m3431692926_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Util.TileHandler/TileType>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m3640886856(__this, ___index0, ___item1, method) ((  void (*) (List_1_t3732775565 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m2067529129_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Util.TileHandler/TileType>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m2989198816(__this, ___item0, method) ((  void (*) (List_1_t3732775565 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m1644145887_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<Pathfinding.Util.TileHandler/TileType>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2013393512(__this, method) ((  bool (*) (List_1_t3732775565 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1299706087_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Pathfinding.Util.TileHandler/TileType>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m2319985365(__this, method) ((  bool (*) (List_1_t3732775565 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m3867536694_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Pathfinding.Util.TileHandler/TileType>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m2219322753(__this, method) ((  Il2CppObject * (*) (List_1_t3732775565 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m4244374434_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Pathfinding.Util.TileHandler/TileType>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m1936023510(__this, method) ((  bool (*) (List_1_t3732775565 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m432946261_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Pathfinding.Util.TileHandler/TileType>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m3148860515(__this, method) ((  bool (*) (List_1_t3732775565 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m2961826820_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Pathfinding.Util.TileHandler/TileType>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m3530998472(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t3732775565 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m3985478825_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Util.TileHandler/TileType>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m3402842143(__this, ___index0, ___value1, method) ((  void (*) (List_1_t3732775565 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m3234554688_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Util.TileHandler/TileType>::Add(T)
#define List_1_Add_m2546802316(__this, ___item0, method) ((  void (*) (List_1_t3732775565 *, TileType_t2364590013 *, const MethodInfo*))List_1_Add_m933592675_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Util.TileHandler/TileType>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m475369767(__this, ___newCount0, method) ((  void (*) (List_1_t3732775565 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m4122600870_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Util.TileHandler/TileType>::CheckRange(System.Int32,System.Int32)
#define List_1_CheckRange_m3887080544(__this, ___idx0, ___count1, method) ((  void (*) (List_1_t3732775565 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m590371457_gshared)(__this, ___idx0, ___count1, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Util.TileHandler/TileType>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m312445989(__this, ___collection0, method) ((  void (*) (List_1_t3732775565 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m2478449828_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Util.TileHandler/TileType>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m3868385509(__this, ___enumerable0, method) ((  void (*) (List_1_t3732775565 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m1739422052_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Util.TileHandler/TileType>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m245650066(__this, ___collection0, method) ((  void (*) (List_1_t3732775565 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m2229151411_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<Pathfinding.Util.TileHandler/TileType>::AsReadOnly()
#define List_1_AsReadOnly_m2080358743(__this, method) ((  ReadOnlyCollection_1_t3921667549 * (*) (List_1_t3732775565 *, const MethodInfo*))List_1_AsReadOnly_m769820182_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Pathfinding.Util.TileHandler/TileType>::BinarySearch(T)
#define List_1_BinarySearch_m690775944(__this, ___item0, method) ((  int32_t (*) (List_1_t3732775565 *, TileType_t2364590013 *, const MethodInfo*))List_1_BinarySearch_m2761349225_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Util.TileHandler/TileType>::Clear()
#define List_1_Clear_m1213577566(__this, method) ((  void (*) (List_1_t3732775565 *, const MethodInfo*))List_1_Clear_m2940332446_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Pathfinding.Util.TileHandler/TileType>::Contains(T)
#define List_1_Contains_m2314354060(__this, ___item0, method) ((  bool (*) (List_1_t3732775565 *, TileType_t2364590013 *, const MethodInfo*))List_1_Contains_m4186092781_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Util.TileHandler/TileType>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m3265009116(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t3732775565 *, TileTypeU5BU5D_t2868017328*, int32_t, const MethodInfo*))List_1_CopyTo_m3988356635_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<Pathfinding.Util.TileHandler/TileType>::Find(System.Predicate`1<T>)
#define List_1_Find_m287601036(__this, ___match0, method) ((  TileType_t2364590013 * (*) (List_1_t3732775565 *, Predicate_1_t1975646896 *, const MethodInfo*))List_1_Find_m3379773421_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Util.TileHandler/TileType>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m3498762375(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t1975646896 *, const MethodInfo*))List_1_CheckMatch_m3390394152_gshared)(__this /* static, unused */, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<Pathfinding.Util.TileHandler/TileType>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m1224580780(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t3732775565 *, int32_t, int32_t, Predicate_1_t1975646896 *, const MethodInfo*))List_1_GetIndex_m4275988045_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Pathfinding.Util.TileHandler/TileType>::GetEnumerator()
#define List_1_GetEnumerator_m961088713(__this, method) ((  Enumerator_t3752448335  (*) (List_1_t3732775565 *, const MethodInfo*))List_1_GetEnumerator_m1919240000_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Pathfinding.Util.TileHandler/TileType>::IndexOf(T)
#define List_1_IndexOf_m2224636000(__this, ___item0, method) ((  int32_t (*) (List_1_t3732775565 *, TileType_t2364590013 *, const MethodInfo*))List_1_IndexOf_m2776798407_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Util.TileHandler/TileType>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m3672002611(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t3732775565 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m3807054194_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Util.TileHandler/TileType>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m3011376300(__this, ___index0, method) ((  void (*) (List_1_t3732775565 *, int32_t, const MethodInfo*))List_1_CheckIndex_m3734723819_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Util.TileHandler/TileType>::Insert(System.Int32,T)
#define List_1_Insert_m3770544979(__this, ___index0, ___item1, method) ((  void (*) (List_1_t3732775565 *, int32_t, TileType_t2364590013 *, const MethodInfo*))List_1_Insert_m3427163986_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Util.TileHandler/TileType>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m139553160(__this, ___collection0, method) ((  void (*) (List_1_t3732775565 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m2905071175_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<Pathfinding.Util.TileHandler/TileType>::Remove(T)
#define List_1_Remove_m24178823(__this, ___item0, method) ((  bool (*) (List_1_t3732775565 *, TileType_t2364590013 *, const MethodInfo*))List_1_Remove_m2747911208_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<Pathfinding.Util.TileHandler/TileType>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m2447212579(__this, ___match0, method) ((  int32_t (*) (List_1_t3732775565 *, Predicate_1_t1975646896 *, const MethodInfo*))List_1_RemoveAll_m2933443938_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Util.TileHandler/TileType>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m1644397849(__this, ___index0, method) ((  void (*) (List_1_t3732775565 *, int32_t, const MethodInfo*))List_1_RemoveAt_m2208877785_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Util.TileHandler/TileType>::RemoveRange(System.Int32,System.Int32)
#define List_1_RemoveRange_m4270591804(__this, ___index0, ___count1, method) ((  void (*) (List_1_t3732775565 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m856857915_gshared)(__this, ___index0, ___count1, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Util.TileHandler/TileType>::Reverse()
#define List_1_Reverse_m3974590643(__this, method) ((  void (*) (List_1_t3732775565 *, const MethodInfo*))List_1_Reverse_m449081940_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Util.TileHandler/TileType>::Sort()
#define List_1_Sort_m1470219215(__this, method) ((  void (*) (List_1_t3732775565 *, const MethodInfo*))List_1_Sort_m1168641486_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Util.TileHandler/TileType>::Sort(System.Collections.Generic.IComparer`1<T>)
#define List_1_Sort_m2473645813(__this, ___comparer0, method) ((  void (*) (List_1_t3732775565 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m3726677974_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Util.TileHandler/TileType>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m3896408802(__this, ___comparison0, method) ((  void (*) (List_1_t3732775565 *, Comparison_1_t1080951200 *, const MethodInfo*))List_1_Sort_m4192185249_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<Pathfinding.Util.TileHandler/TileType>::ToArray()
#define List_1_ToArray_m1176615090(__this, method) ((  TileTypeU5BU5D_t2868017328* (*) (List_1_t3732775565 *, const MethodInfo*))List_1_ToArray_m1449333879_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Util.TileHandler/TileType>::TrimExcess()
#define List_1_TrimExcess_m1750895656(__this, method) ((  void (*) (List_1_t3732775565 *, const MethodInfo*))List_1_TrimExcess_m2451380967_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Pathfinding.Util.TileHandler/TileType>::get_Capacity()
#define List_1_get_Capacity_m4217505936(__this, method) ((  int32_t (*) (List_1_t3732775565 *, const MethodInfo*))List_1_get_Capacity_m543520655_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Util.TileHandler/TileType>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m1980525881(__this, ___value0, method) ((  void (*) (List_1_t3732775565 *, int32_t, const MethodInfo*))List_1_set_Capacity_m1332789688_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<Pathfinding.Util.TileHandler/TileType>::get_Count()
#define List_1_get_Count_m1403398738(__this, method) ((  int32_t (*) (List_1_t3732775565 *, const MethodInfo*))List_1_get_Count_m3549598589_gshared)(__this, method)
// T System.Collections.Generic.List`1<Pathfinding.Util.TileHandler/TileType>::get_Item(System.Int32)
#define List_1_get_Item_m3158188419(__this, ___index0, method) ((  TileType_t2364590013 * (*) (List_1_t3732775565 *, int32_t, const MethodInfo*))List_1_get_Item_m404118264_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Util.TileHandler/TileType>::set_Item(System.Int32,T)
#define List_1_set_Item_m350923626(__this, ___index0, ___value1, method) ((  void (*) (List_1_t3732775565 *, int32_t, TileType_t2364590013 *, const MethodInfo*))List_1_set_Item_m1074271145_gshared)(__this, ___index0, ___value1, method)
