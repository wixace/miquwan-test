﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_jaliPurhoucar169
struct M_jaliPurhoucar169_t2347876389;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"

// System.Void GarbageiOS.M_jaliPurhoucar169::.ctor()
extern "C"  void M_jaliPurhoucar169__ctor_m573052846 (M_jaliPurhoucar169_t2347876389 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_jaliPurhoucar169::M_haiderBukupe0(System.String[],System.Int32)
extern "C"  void M_jaliPurhoucar169_M_haiderBukupe0_m2667734702 (M_jaliPurhoucar169_t2347876389 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
