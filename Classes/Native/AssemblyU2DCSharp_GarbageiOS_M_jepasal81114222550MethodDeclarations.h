﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_jepasal8
struct M_jepasal8_t1114222550;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_jepasal81114222550.h"

// System.Void GarbageiOS.M_jepasal8::.ctor()
extern "C"  void M_jepasal8__ctor_m2791286557 (M_jepasal8_t1114222550 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_jepasal8::M_sowheHaymem0(System.String[],System.Int32)
extern "C"  void M_jepasal8_M_sowheHaymem0_m141518387 (M_jepasal8_t1114222550 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_jepasal8::M_loni1(System.String[],System.Int32)
extern "C"  void M_jepasal8_M_loni1_m2884196819 (M_jepasal8_t1114222550 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_jepasal8::M_fawvowrur2(System.String[],System.Int32)
extern "C"  void M_jepasal8_M_fawvowrur2_m818824917 (M_jepasal8_t1114222550 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_jepasal8::M_tabereFereru3(System.String[],System.Int32)
extern "C"  void M_jepasal8_M_tabereFereru3_m993332507 (M_jepasal8_t1114222550 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_jepasal8::M_mallsawniTowjayna4(System.String[],System.Int32)
extern "C"  void M_jepasal8_M_mallsawniTowjayna4_m1642957179 (M_jepasal8_t1114222550 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_jepasal8::M_coralPeborsu5(System.String[],System.Int32)
extern "C"  void M_jepasal8_M_coralPeborsu5_m3077888084 (M_jepasal8_t1114222550 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_jepasal8::ilo_M_sowheHaymem01(GarbageiOS.M_jepasal8,System.String[],System.Int32)
extern "C"  void M_jepasal8_ilo_M_sowheHaymem01_m3565125981 (Il2CppObject * __this /* static, unused */, M_jepasal8_t1114222550 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_jepasal8::ilo_M_loni12(GarbageiOS.M_jepasal8,System.String[],System.Int32)
extern "C"  void M_jepasal8_ilo_M_loni12_m2188194820 (Il2CppObject * __this /* static, unused */, M_jepasal8_t1114222550 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
