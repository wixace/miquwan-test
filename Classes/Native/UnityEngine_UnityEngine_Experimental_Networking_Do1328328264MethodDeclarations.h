﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Experimental.Networking.DownloadHandlerBuffer
struct DownloadHandlerBuffer_t1328328264;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// System.String
struct String_t;
struct DownloadHandlerBuffer_t1328328264_marshaled_pinvoke;
struct DownloadHandlerBuffer_t1328328264_marshaled_com;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.Experimental.Networking.DownloadHandlerBuffer::.ctor()
extern "C"  void DownloadHandlerBuffer__ctor_m930000971 (DownloadHandlerBuffer_t1328328264 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] UnityEngine.Experimental.Networking.DownloadHandlerBuffer::GetData()
extern "C"  ByteU5BU5D_t4260760469* DownloadHandlerBuffer_GetData_m384130995 (DownloadHandlerBuffer_t1328328264 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Experimental.Networking.DownloadHandlerBuffer::GetText()
extern "C"  String_t* DownloadHandlerBuffer_GetText_m2900259631 (DownloadHandlerBuffer_t1328328264 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] UnityEngine.Experimental.Networking.DownloadHandlerBuffer::InternalGetData()
extern "C"  ByteU5BU5D_t4260760469* DownloadHandlerBuffer_InternalGetData_m2043431798 (DownloadHandlerBuffer_t1328328264 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Experimental.Networking.DownloadHandlerBuffer::InternalGetText()
extern "C"  String_t* DownloadHandlerBuffer_InternalGetText_m67606002 (DownloadHandlerBuffer_t1328328264 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct DownloadHandlerBuffer_t1328328264;
struct DownloadHandlerBuffer_t1328328264_marshaled_pinvoke;

extern "C" void DownloadHandlerBuffer_t1328328264_marshal_pinvoke(const DownloadHandlerBuffer_t1328328264& unmarshaled, DownloadHandlerBuffer_t1328328264_marshaled_pinvoke& marshaled);
extern "C" void DownloadHandlerBuffer_t1328328264_marshal_pinvoke_back(const DownloadHandlerBuffer_t1328328264_marshaled_pinvoke& marshaled, DownloadHandlerBuffer_t1328328264& unmarshaled);
extern "C" void DownloadHandlerBuffer_t1328328264_marshal_pinvoke_cleanup(DownloadHandlerBuffer_t1328328264_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct DownloadHandlerBuffer_t1328328264;
struct DownloadHandlerBuffer_t1328328264_marshaled_com;

extern "C" void DownloadHandlerBuffer_t1328328264_marshal_com(const DownloadHandlerBuffer_t1328328264& unmarshaled, DownloadHandlerBuffer_t1328328264_marshaled_com& marshaled);
extern "C" void DownloadHandlerBuffer_t1328328264_marshal_com_back(const DownloadHandlerBuffer_t1328328264_marshaled_com& marshaled, DownloadHandlerBuffer_t1328328264& unmarshaled);
extern "C" void DownloadHandlerBuffer_t1328328264_marshal_com_cleanup(DownloadHandlerBuffer_t1328328264_marshaled_com& marshaled);
