﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ValueCollection<FLOAT_TEXT_ID,System.Object>
struct ValueCollection_t3122611053;
// System.Collections.Generic.Dictionary`2<FLOAT_TEXT_ID,System.Object>
struct Dictionary_2_t127038044;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t1787714124;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object[]
struct ObjectU5BU5D_t1108656482;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V2353838748.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<FLOAT_TEXT_ID,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ValueCollection__ctor_m2663874790_gshared (ValueCollection_t3122611053 * __this, Dictionary_2_t127038044 * ___dictionary0, const MethodInfo* method);
#define ValueCollection__ctor_m2663874790(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t3122611053 *, Dictionary_2_t127038044 *, const MethodInfo*))ValueCollection__ctor_m2663874790_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<FLOAT_TEXT_ID,System.Object>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1617954444_gshared (ValueCollection_t3122611053 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1617954444(__this, ___item0, method) ((  void (*) (ValueCollection_t3122611053 *, Il2CppObject *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1617954444_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<FLOAT_TEXT_ID,System.Object>::System.Collections.Generic.ICollection<TValue>.Clear()
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m2309429589_gshared (ValueCollection_t3122611053 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m2309429589(__this, method) ((  void (*) (ValueCollection_t3122611053 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m2309429589_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<FLOAT_TEXT_ID,System.Object>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m2786704666_gshared (ValueCollection_t3122611053 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m2786704666(__this, ___item0, method) ((  bool (*) (ValueCollection_t3122611053 *, Il2CppObject *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m2786704666_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<FLOAT_TEXT_ID,System.Object>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m915625023_gshared (ValueCollection_t3122611053 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m915625023(__this, ___item0, method) ((  bool (*) (ValueCollection_t3122611053 *, Il2CppObject *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m915625023_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<FLOAT_TEXT_ID,System.Object>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
extern "C"  Il2CppObject* ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m3096645987_gshared (ValueCollection_t3122611053 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m3096645987(__this, method) ((  Il2CppObject* (*) (ValueCollection_t3122611053 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m3096645987_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<FLOAT_TEXT_ID,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ValueCollection_System_Collections_ICollection_CopyTo_m4135703577_gshared (ValueCollection_t3122611053 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_CopyTo_m4135703577(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t3122611053 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m4135703577_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<FLOAT_TEXT_ID,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ValueCollection_System_Collections_IEnumerable_GetEnumerator_m3062241876_gshared (ValueCollection_t3122611053 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m3062241876(__this, method) ((  Il2CppObject * (*) (ValueCollection_t3122611053 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m3062241876_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<FLOAT_TEXT_ID,System.Object>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1061880525_gshared (ValueCollection_t3122611053 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1061880525(__this, method) ((  bool (*) (ValueCollection_t3122611053 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1061880525_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<FLOAT_TEXT_ID,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ValueCollection_System_Collections_ICollection_get_IsSynchronized_m2995606573_gshared (ValueCollection_t3122611053 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m2995606573(__this, method) ((  bool (*) (ValueCollection_t3122611053 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m2995606573_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<FLOAT_TEXT_ID,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ValueCollection_System_Collections_ICollection_get_SyncRoot_m2989546841_gshared (ValueCollection_t3122611053 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m2989546841(__this, method) ((  Il2CppObject * (*) (ValueCollection_t3122611053 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m2989546841_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<FLOAT_TEXT_ID,System.Object>::CopyTo(TValue[],System.Int32)
extern "C"  void ValueCollection_CopyTo_m4149263277_gshared (ValueCollection_t3122611053 * __this, ObjectU5BU5D_t1108656482* ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_CopyTo_m4149263277(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t3122611053 *, ObjectU5BU5D_t1108656482*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m4149263277_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<FLOAT_TEXT_ID,System.Object>::GetEnumerator()
extern "C"  Enumerator_t2353838748  ValueCollection_GetEnumerator_m1771510608_gshared (ValueCollection_t3122611053 * __this, const MethodInfo* method);
#define ValueCollection_GetEnumerator_m1771510608(__this, method) ((  Enumerator_t2353838748  (*) (ValueCollection_t3122611053 *, const MethodInfo*))ValueCollection_GetEnumerator_m1771510608_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<FLOAT_TEXT_ID,System.Object>::get_Count()
extern "C"  int32_t ValueCollection_get_Count_m3494312307_gshared (ValueCollection_t3122611053 * __this, const MethodInfo* method);
#define ValueCollection_get_Count_m3494312307(__this, method) ((  int32_t (*) (ValueCollection_t3122611053 *, const MethodInfo*))ValueCollection_get_Count_m3494312307_gshared)(__this, method)
