﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// JSDataExchange_Arr
struct JSDataExchange_Arr_t122463832;
// System.String
struct String_t;
// System.Type
struct Type_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Type2863145774.h"
#include "mscorlib_System_String7231557.h"

// System.Void JSDataExchange_Arr::.ctor()
extern "C"  void JSDataExchange_Arr__ctor_m3740466499 (JSDataExchange_Arr_t122463832 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String JSDataExchange_Arr::Get_GetParam(System.Type)
extern "C"  String_t* JSDataExchange_Arr_Get_GetParam_m3543194165 (JSDataExchange_Arr_t122463832 * __this, Type_t * ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String JSDataExchange_Arr::Get_GetJSReturn()
extern "C"  String_t* JSDataExchange_Arr_Get_GetJSReturn_m4085300804 (JSDataExchange_Arr_t122463832 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String JSDataExchange_Arr::Get_Return(System.String)
extern "C"  String_t* JSDataExchange_Arr_Get_Return_m2367112261 (JSDataExchange_Arr_t122463832 * __this, String_t* ___expVar0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String JSDataExchange_Arr::ilo_GetMetatypeKeyword1(System.Type)
extern "C"  String_t* JSDataExchange_Arr_ilo_GetMetatypeKeyword1_m4002462257 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
