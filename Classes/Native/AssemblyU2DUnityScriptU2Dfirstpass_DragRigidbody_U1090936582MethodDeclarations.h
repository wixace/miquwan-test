﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DragRigidbody/$DragObject$58
struct U24DragObjectU2458_t1090936582;
// DragRigidbody
struct DragRigidbody_t2531437401;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t1787714124;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_DragRigidbody2531437401.h"

// System.Void DragRigidbody/$DragObject$58::.ctor(System.Single,DragRigidbody)
extern "C"  void U24DragObjectU2458__ctor_m1025218848 (U24DragObjectU2458_t1090936582 * __this, float ___distance0, DragRigidbody_t2531437401 * ___self_1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<System.Object> DragRigidbody/$DragObject$58::GetEnumerator()
extern "C"  Il2CppObject* U24DragObjectU2458_GetEnumerator_m704288176 (U24DragObjectU2458_t1090936582 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
