﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TabData
struct TabData_t110553279;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// ByteReadArray
struct ByteReadArray_t751193627;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_TabData110553279.h"
#include "AssemblyU2DCSharp_ByteReadArray751193627.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void TabData::.ctor()
extern "C"  void TabData__ctor_m2242030988 (TabData_t110553279 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TabData::.cctor()
extern "C"  void TabData__cctor_m301387681 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TabData::Init(System.Int32)
extern "C"  void TabData_Init_m87722137 (TabData_t110553279 * __this, int32_t ___col0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String TabData::GetInfoNew(System.Int32)
extern "C"  String_t* TabData_GetInfoNew_m46768482 (TabData_t110553279 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> TabData::GetCsInfo(System.Int32)
extern "C"  Dictionary_2_t827649927 * TabData_GetCsInfo_m1435686813 (TabData_t110553279 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String TabData::ReadField(System.Int32)
extern "C"  String_t* TabData_ReadField_m1647515810 (TabData_t110553279 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TabData::ReSet()
extern "C"  void TabData_ReSet_m4153878553 (TabData_t110553279 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String TabData::ilo_ReadField1(TabData,System.Int32)
extern "C"  String_t* TabData_ilo_ReadField1_m2723343993 (Il2CppObject * __this /* static, unused */, TabData_t110553279 * ____this0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TabData::ilo_set_Postion2(ByteReadArray,System.Int32)
extern "C"  void TabData_ilo_set_Postion2_m4138192050 (Il2CppObject * __this /* static, unused */, ByteReadArray_t751193627 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TabData::ilo_LogError3(System.Object,System.Boolean)
extern "C"  void TabData_ilo_LogError3_m1673737167 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TabData::ilo_ReadFloat4(ByteReadArray)
extern "C"  float TabData_ilo_ReadFloat4_m442792234 (Il2CppObject * __this /* static, unused */, ByteReadArray_t751193627 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
