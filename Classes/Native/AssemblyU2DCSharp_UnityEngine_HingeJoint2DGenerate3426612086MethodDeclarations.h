﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_HingeJoint2DGenerated
struct UnityEngine_HingeJoint2DGenerated_t3426612086;
// JSVCall
struct JSVCall_t3708497963;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void UnityEngine_HingeJoint2DGenerated::.ctor()
extern "C"  void UnityEngine_HingeJoint2DGenerated__ctor_m2610589365 (UnityEngine_HingeJoint2DGenerated_t3426612086 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_HingeJoint2DGenerated::HingeJoint2D_HingeJoint2D1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_HingeJoint2DGenerated_HingeJoint2D_HingeJoint2D1_m1462212669 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_HingeJoint2DGenerated::HingeJoint2D_useMotor(JSVCall)
extern "C"  void UnityEngine_HingeJoint2DGenerated_HingeJoint2D_useMotor_m1311935224 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_HingeJoint2DGenerated::HingeJoint2D_useLimits(JSVCall)
extern "C"  void UnityEngine_HingeJoint2DGenerated_HingeJoint2D_useLimits_m44794631 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_HingeJoint2DGenerated::HingeJoint2D_motor(JSVCall)
extern "C"  void UnityEngine_HingeJoint2DGenerated_HingeJoint2D_motor_m2888125393 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_HingeJoint2DGenerated::HingeJoint2D_limits(JSVCall)
extern "C"  void UnityEngine_HingeJoint2DGenerated_HingeJoint2D_limits_m1662049614 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_HingeJoint2DGenerated::HingeJoint2D_limitState(JSVCall)
extern "C"  void UnityEngine_HingeJoint2DGenerated_HingeJoint2D_limitState_m145998224 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_HingeJoint2DGenerated::HingeJoint2D_referenceAngle(JSVCall)
extern "C"  void UnityEngine_HingeJoint2DGenerated_HingeJoint2D_referenceAngle_m2230894366 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_HingeJoint2DGenerated::HingeJoint2D_jointAngle(JSVCall)
extern "C"  void UnityEngine_HingeJoint2DGenerated_HingeJoint2D_jointAngle_m1012276765 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_HingeJoint2DGenerated::HingeJoint2D_jointSpeed(JSVCall)
extern "C"  void UnityEngine_HingeJoint2DGenerated_HingeJoint2D_jointSpeed_m3842423081 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_HingeJoint2DGenerated::HingeJoint2D_GetMotorTorque__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_HingeJoint2DGenerated_HingeJoint2D_GetMotorTorque__Single_m4132426550 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_HingeJoint2DGenerated::__Register()
extern "C"  void UnityEngine_HingeJoint2DGenerated___Register_m3828938546 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_HingeJoint2DGenerated::ilo_setBooleanS1(System.Int32,System.Boolean)
extern "C"  void UnityEngine_HingeJoint2DGenerated_ilo_setBooleanS1_m4032176486 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_HingeJoint2DGenerated::ilo_getBooleanS2(System.Int32)
extern "C"  bool UnityEngine_HingeJoint2DGenerated_ilo_getBooleanS2_m1565673936 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_HingeJoint2DGenerated::ilo_setObject3(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UnityEngine_HingeJoint2DGenerated_ilo_setObject3_m1125687835 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_HingeJoint2DGenerated::ilo_getObject4(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_HingeJoint2DGenerated_ilo_getObject4_m1299083475 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_HingeJoint2DGenerated::ilo_setSingle5(System.Int32,System.Single)
extern "C"  void UnityEngine_HingeJoint2DGenerated_ilo_setSingle5_m2935510947 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
