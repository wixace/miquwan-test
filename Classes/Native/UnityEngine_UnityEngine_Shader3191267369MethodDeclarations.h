﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Shader
struct Shader_t3191267369;
// System.String
struct String_t;
// UnityEngine.Texture
struct Texture_t2526458961;
// UnityEngine.ComputeBuffer
struct ComputeBuffer_t37359565;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "UnityEngine_UnityEngine_DisableBatchingType382694016.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"
#include "UnityEngine_UnityEngine_Vector44282066567.h"
#include "UnityEngine_UnityEngine_Texture2526458961.h"
#include "UnityEngine_UnityEngine_Matrix4x41651859333.h"
#include "UnityEngine_UnityEngine_ComputeBuffer37359565.h"

// System.Void UnityEngine.Shader::.ctor()
extern "C"  void Shader__ctor_m2878158248 (Shader_t3191267369 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Shader UnityEngine.Shader::Find(System.String)
extern "C"  Shader_t3191267369 * Shader_Find_m4048047578 (Il2CppObject * __this /* static, unused */, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Shader::get_isSupported()
extern "C"  bool Shader_get_isSupported_m1422621179 (Shader_t3191267369 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Shader::EnableKeyword(System.String)
extern "C"  void Shader_EnableKeyword_m944528214 (Il2CppObject * __this /* static, unused */, String_t* ___keyword0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Shader::DisableKeyword(System.String)
extern "C"  void Shader_DisableKeyword_m2163321765 (Il2CppObject * __this /* static, unused */, String_t* ___keyword0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Shader::IsKeywordEnabled(System.String)
extern "C"  bool Shader_IsKeywordEnabled_m1621717150 (Il2CppObject * __this /* static, unused */, String_t* ___keyword0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Shader::get_maximumLOD()
extern "C"  int32_t Shader_get_maximumLOD_m2404270150 (Shader_t3191267369 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Shader::set_maximumLOD(System.Int32)
extern "C"  void Shader_set_maximumLOD_m2797626507 (Shader_t3191267369 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Shader::get_globalMaximumLOD()
extern "C"  int32_t Shader_get_globalMaximumLOD_m585517193 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Shader::set_globalMaximumLOD(System.Int32)
extern "C"  void Shader_set_globalMaximumLOD_m2733529422 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Shader::get_renderQueue()
extern "C"  int32_t Shader_get_renderQueue_m801092952 (Shader_t3191267369 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.DisableBatchingType UnityEngine.Shader::get_disableBatching()
extern "C"  int32_t Shader_get_disableBatching_m2523136045 (Shader_t3191267369 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Shader::SetGlobalColor(System.String,UnityEngine.Color)
extern "C"  void Shader_SetGlobalColor_m1669397640 (Il2CppObject * __this /* static, unused */, String_t* ___propertyName0, Color_t4194546905  ___color1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Shader::SetGlobalColor(System.Int32,UnityEngine.Color)
extern "C"  void Shader_SetGlobalColor_m1709492491 (Il2CppObject * __this /* static, unused */, int32_t ___nameID0, Color_t4194546905  ___color1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Shader::INTERNAL_CALL_SetGlobalColor(System.Int32,UnityEngine.Color&)
extern "C"  void Shader_INTERNAL_CALL_SetGlobalColor_m1549785026 (Il2CppObject * __this /* static, unused */, int32_t ___nameID0, Color_t4194546905 * ___color1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Shader::SetGlobalVector(System.String,UnityEngine.Vector4)
extern "C"  void Shader_SetGlobalVector_m1990013606 (Il2CppObject * __this /* static, unused */, String_t* ___propertyName0, Vector4_t4282066567  ___vec1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Shader::SetGlobalVector(System.Int32,UnityEngine.Vector4)
extern "C"  void Shader_SetGlobalVector_m1932877649 (Il2CppObject * __this /* static, unused */, int32_t ___nameID0, Vector4_t4282066567  ___vec1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Shader::SetGlobalFloat(System.String,System.Single)
extern "C"  void Shader_SetGlobalFloat_m2785620148 (Il2CppObject * __this /* static, unused */, String_t* ___propertyName0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Shader::SetGlobalFloat(System.Int32,System.Single)
extern "C"  void Shader_SetGlobalFloat_m3553472137 (Il2CppObject * __this /* static, unused */, int32_t ___nameID0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Shader::SetGlobalInt(System.String,System.Int32)
extern "C"  void Shader_SetGlobalInt_m2536714683 (Il2CppObject * __this /* static, unused */, String_t* ___propertyName0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Shader::SetGlobalInt(System.Int32,System.Int32)
extern "C"  void Shader_SetGlobalInt_m1449584064 (Il2CppObject * __this /* static, unused */, int32_t ___nameID0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Shader::SetGlobalTexture(System.String,UnityEngine.Texture)
extern "C"  void Shader_SetGlobalTexture_m2110804504 (Il2CppObject * __this /* static, unused */, String_t* ___propertyName0, Texture_t2526458961 * ___tex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Shader::SetGlobalTexture(System.Int32,UnityEngine.Texture)
extern "C"  void Shader_SetGlobalTexture_m2332174155 (Il2CppObject * __this /* static, unused */, int32_t ___nameID0, Texture_t2526458961 * ___tex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Shader::SetGlobalMatrix(System.String,UnityEngine.Matrix4x4)
extern "C"  void Shader_SetGlobalMatrix_m3693328362 (Il2CppObject * __this /* static, unused */, String_t* ___propertyName0, Matrix4x4_t1651859333  ___mat1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Shader::SetGlobalMatrix(System.Int32,UnityEngine.Matrix4x4)
extern "C"  void Shader_SetGlobalMatrix_m4125037841 (Il2CppObject * __this /* static, unused */, int32_t ___nameID0, Matrix4x4_t1651859333  ___mat1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Shader::INTERNAL_CALL_SetGlobalMatrix(System.Int32,UnityEngine.Matrix4x4&)
extern "C"  void Shader_INTERNAL_CALL_SetGlobalMatrix_m1567336922 (Il2CppObject * __this /* static, unused */, int32_t ___nameID0, Matrix4x4_t1651859333 * ___mat1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Shader::SetGlobalBuffer(System.String,UnityEngine.ComputeBuffer)
extern "C"  void Shader_SetGlobalBuffer_m1902070403 (Il2CppObject * __this /* static, unused */, String_t* ___propertyName0, ComputeBuffer_t37359565 * ___buffer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Shader::PropertyToID(System.String)
extern "C"  int32_t Shader_PropertyToID_m3019342011 (Il2CppObject * __this /* static, unused */, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Shader::WarmupAllShaders()
extern "C"  void Shader_WarmupAllShaders_m355796777 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
