﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._8800005e7488aee8f523f87305d796cc
struct _8800005e7488aee8f523f87305d796cc_t20417072;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._8800005e7488aee8f523f87305d796cc::.ctor()
extern "C"  void _8800005e7488aee8f523f87305d796cc__ctor_m2368475645 (_8800005e7488aee8f523f87305d796cc_t20417072 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._8800005e7488aee8f523f87305d796cc::_8800005e7488aee8f523f87305d796ccm2(System.Int32)
extern "C"  int32_t _8800005e7488aee8f523f87305d796cc__8800005e7488aee8f523f87305d796ccm2_m4068041625 (_8800005e7488aee8f523f87305d796cc_t20417072 * __this, int32_t ____8800005e7488aee8f523f87305d796cca0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._8800005e7488aee8f523f87305d796cc::_8800005e7488aee8f523f87305d796ccm(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _8800005e7488aee8f523f87305d796cc__8800005e7488aee8f523f87305d796ccm_m28017469 (_8800005e7488aee8f523f87305d796cc_t20417072 * __this, int32_t ____8800005e7488aee8f523f87305d796cca0, int32_t ____8800005e7488aee8f523f87305d796cc891, int32_t ____8800005e7488aee8f523f87305d796ccc2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
