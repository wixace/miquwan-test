﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_forhu87
struct  M_forhu87_t1831502421  : public Il2CppObject
{
public:
	// System.Int32 GarbageiOS.M_forhu87::_nawjair
	int32_t ____nawjair_0;
	// System.UInt32 GarbageiOS.M_forhu87::_kairtai
	uint32_t ____kairtai_1;
	// System.Boolean GarbageiOS.M_forhu87::_nifeJoudrow
	bool ____nifeJoudrow_2;
	// System.Boolean GarbageiOS.M_forhu87::_rirhocem
	bool ____rirhocem_3;
	// System.UInt32 GarbageiOS.M_forhu87::_belcalkall
	uint32_t ____belcalkall_4;
	// System.Single GarbageiOS.M_forhu87::_saycooCegai
	float ____saycooCegai_5;
	// System.Int32 GarbageiOS.M_forhu87::_nanearLeatismou
	int32_t ____nanearLeatismou_6;

public:
	inline static int32_t get_offset_of__nawjair_0() { return static_cast<int32_t>(offsetof(M_forhu87_t1831502421, ____nawjair_0)); }
	inline int32_t get__nawjair_0() const { return ____nawjair_0; }
	inline int32_t* get_address_of__nawjair_0() { return &____nawjair_0; }
	inline void set__nawjair_0(int32_t value)
	{
		____nawjair_0 = value;
	}

	inline static int32_t get_offset_of__kairtai_1() { return static_cast<int32_t>(offsetof(M_forhu87_t1831502421, ____kairtai_1)); }
	inline uint32_t get__kairtai_1() const { return ____kairtai_1; }
	inline uint32_t* get_address_of__kairtai_1() { return &____kairtai_1; }
	inline void set__kairtai_1(uint32_t value)
	{
		____kairtai_1 = value;
	}

	inline static int32_t get_offset_of__nifeJoudrow_2() { return static_cast<int32_t>(offsetof(M_forhu87_t1831502421, ____nifeJoudrow_2)); }
	inline bool get__nifeJoudrow_2() const { return ____nifeJoudrow_2; }
	inline bool* get_address_of__nifeJoudrow_2() { return &____nifeJoudrow_2; }
	inline void set__nifeJoudrow_2(bool value)
	{
		____nifeJoudrow_2 = value;
	}

	inline static int32_t get_offset_of__rirhocem_3() { return static_cast<int32_t>(offsetof(M_forhu87_t1831502421, ____rirhocem_3)); }
	inline bool get__rirhocem_3() const { return ____rirhocem_3; }
	inline bool* get_address_of__rirhocem_3() { return &____rirhocem_3; }
	inline void set__rirhocem_3(bool value)
	{
		____rirhocem_3 = value;
	}

	inline static int32_t get_offset_of__belcalkall_4() { return static_cast<int32_t>(offsetof(M_forhu87_t1831502421, ____belcalkall_4)); }
	inline uint32_t get__belcalkall_4() const { return ____belcalkall_4; }
	inline uint32_t* get_address_of__belcalkall_4() { return &____belcalkall_4; }
	inline void set__belcalkall_4(uint32_t value)
	{
		____belcalkall_4 = value;
	}

	inline static int32_t get_offset_of__saycooCegai_5() { return static_cast<int32_t>(offsetof(M_forhu87_t1831502421, ____saycooCegai_5)); }
	inline float get__saycooCegai_5() const { return ____saycooCegai_5; }
	inline float* get_address_of__saycooCegai_5() { return &____saycooCegai_5; }
	inline void set__saycooCegai_5(float value)
	{
		____saycooCegai_5 = value;
	}

	inline static int32_t get_offset_of__nanearLeatismou_6() { return static_cast<int32_t>(offsetof(M_forhu87_t1831502421, ____nanearLeatismou_6)); }
	inline int32_t get__nanearLeatismou_6() const { return ____nanearLeatismou_6; }
	inline int32_t* get_address_of__nanearLeatismou_6() { return &____nanearLeatismou_6; }
	inline void set__nanearLeatismou_6(int32_t value)
	{
		____nanearLeatismou_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
