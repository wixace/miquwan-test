﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "Pathfinding_Poly2Tri_Pathfinding_Poly2Tri_FixedArra526322725MethodDeclarations.h"

// System.Collections.IEnumerator Pathfinding.Poly2Tri.FixedArray3`1<Pathfinding.Poly2Tri.DelaunayTriangle>::System.Collections.IEnumerable.GetEnumerator()
#define FixedArray3_1_System_Collections_IEnumerable_GetEnumerator_m791681985(__this, method) ((  Il2CppObject * (*) (FixedArray3_1_t3485577237 *, const MethodInfo*))FixedArray3_1_System_Collections_IEnumerable_GetEnumerator_m4035815537_gshared)(__this, method)
// T Pathfinding.Poly2Tri.FixedArray3`1<Pathfinding.Poly2Tri.DelaunayTriangle>::get_Item(System.Int32)
#define FixedArray3_1_get_Item_m4227602836(__this, ___index0, method) ((  DelaunayTriangle_t2835103587 * (*) (FixedArray3_1_t3485577237 *, int32_t, const MethodInfo*))FixedArray3_1_get_Item_m2156685124_gshared)(__this, ___index0, method)
// System.Void Pathfinding.Poly2Tri.FixedArray3`1<Pathfinding.Poly2Tri.DelaunayTriangle>::set_Item(System.Int32,T)
#define FixedArray3_1_set_Item_m1339370303(__this, ___index0, ___value1, method) ((  void (*) (FixedArray3_1_t3485577237 *, int32_t, DelaunayTriangle_t2835103587 *, const MethodInfo*))FixedArray3_1_set_Item_m851048943_gshared)(__this, ___index0, ___value1, method)
// System.Boolean Pathfinding.Poly2Tri.FixedArray3`1<Pathfinding.Poly2Tri.DelaunayTriangle>::Contains(T)
#define FixedArray3_1_Contains_m3628983023(__this, ___value0, method) ((  bool (*) (FixedArray3_1_t3485577237 *, DelaunayTriangle_t2835103587 *, const MethodInfo*))FixedArray3_1_Contains_m872077375_gshared)(__this, ___value0, method)
// System.Int32 Pathfinding.Poly2Tri.FixedArray3`1<Pathfinding.Poly2Tri.DelaunayTriangle>::IndexOf(T)
#define FixedArray3_1_IndexOf_m58153769(__this, ___value0, method) ((  int32_t (*) (FixedArray3_1_t3485577237 *, DelaunayTriangle_t2835103587 *, const MethodInfo*))FixedArray3_1_IndexOf_m548310489_gshared)(__this, ___value0, method)
// System.Void Pathfinding.Poly2Tri.FixedArray3`1<Pathfinding.Poly2Tri.DelaunayTriangle>::Clear()
#define FixedArray3_1_Clear_m3990085865(__this, method) ((  void (*) (FixedArray3_1_t3485577237 *, const MethodInfo*))FixedArray3_1_Clear_m1266883641_gshared)(__this, method)
// System.Collections.Generic.IEnumerable`1<T> Pathfinding.Poly2Tri.FixedArray3`1<Pathfinding.Poly2Tri.DelaunayTriangle>::Enumerate()
#define FixedArray3_1_Enumerate_m3511687648(__this, method) ((  Il2CppObject* (*) (FixedArray3_1_t3485577237 *, const MethodInfo*))FixedArray3_1_Enumerate_m2474609968_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<T> Pathfinding.Poly2Tri.FixedArray3`1<Pathfinding.Poly2Tri.DelaunayTriangle>::GetEnumerator()
#define FixedArray3_1_GetEnumerator_m2898129970(__this, method) ((  Il2CppObject* (*) (FixedArray3_1_t3485577237 *, const MethodInfo*))FixedArray3_1_GetEnumerator_m983331714_gshared)(__this, method)
