﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PluginJingqi
struct  PluginJingqi_t1604527235  : public MonoBehaviour_t667441552
{
public:
	// System.Boolean PluginJingqi::isOut
	bool ___isOut_2;
	// System.String PluginJingqi::uid
	String_t* ___uid_3;
	// System.String PluginJingqi::token
	String_t* ___token_4;
	// System.String[] PluginJingqi::sdkInfos
	StringU5BU5D_t4054002952* ___sdkInfos_5;
	// System.String PluginJingqi::configId
	String_t* ___configId_6;

public:
	inline static int32_t get_offset_of_isOut_2() { return static_cast<int32_t>(offsetof(PluginJingqi_t1604527235, ___isOut_2)); }
	inline bool get_isOut_2() const { return ___isOut_2; }
	inline bool* get_address_of_isOut_2() { return &___isOut_2; }
	inline void set_isOut_2(bool value)
	{
		___isOut_2 = value;
	}

	inline static int32_t get_offset_of_uid_3() { return static_cast<int32_t>(offsetof(PluginJingqi_t1604527235, ___uid_3)); }
	inline String_t* get_uid_3() const { return ___uid_3; }
	inline String_t** get_address_of_uid_3() { return &___uid_3; }
	inline void set_uid_3(String_t* value)
	{
		___uid_3 = value;
		Il2CppCodeGenWriteBarrier(&___uid_3, value);
	}

	inline static int32_t get_offset_of_token_4() { return static_cast<int32_t>(offsetof(PluginJingqi_t1604527235, ___token_4)); }
	inline String_t* get_token_4() const { return ___token_4; }
	inline String_t** get_address_of_token_4() { return &___token_4; }
	inline void set_token_4(String_t* value)
	{
		___token_4 = value;
		Il2CppCodeGenWriteBarrier(&___token_4, value);
	}

	inline static int32_t get_offset_of_sdkInfos_5() { return static_cast<int32_t>(offsetof(PluginJingqi_t1604527235, ___sdkInfos_5)); }
	inline StringU5BU5D_t4054002952* get_sdkInfos_5() const { return ___sdkInfos_5; }
	inline StringU5BU5D_t4054002952** get_address_of_sdkInfos_5() { return &___sdkInfos_5; }
	inline void set_sdkInfos_5(StringU5BU5D_t4054002952* value)
	{
		___sdkInfos_5 = value;
		Il2CppCodeGenWriteBarrier(&___sdkInfos_5, value);
	}

	inline static int32_t get_offset_of_configId_6() { return static_cast<int32_t>(offsetof(PluginJingqi_t1604527235, ___configId_6)); }
	inline String_t* get_configId_6() const { return ___configId_6; }
	inline String_t** get_address_of_configId_6() { return &___configId_6; }
	inline void set_configId_6(String_t* value)
	{
		___configId_6 = value;
		Il2CppCodeGenWriteBarrier(&___configId_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
