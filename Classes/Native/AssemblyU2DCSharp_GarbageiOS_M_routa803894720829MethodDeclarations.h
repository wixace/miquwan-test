﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_routa80
struct M_routa80_t3894720829;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_routa803894720829.h"

// System.Void GarbageiOS.M_routa80::.ctor()
extern "C"  void M_routa80__ctor_m4115440326 (M_routa80_t3894720829 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_routa80::M_sayteStaicheesoo0(System.String[],System.Int32)
extern "C"  void M_routa80_M_sayteStaicheesoo0_m2458727184 (M_routa80_t3894720829 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_routa80::M_pirbache1(System.String[],System.Int32)
extern "C"  void M_routa80_M_pirbache1_m1234554482 (M_routa80_t3894720829 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_routa80::M_poocerepe2(System.String[],System.Int32)
extern "C"  void M_routa80_M_poocerepe2_m2996315871 (M_routa80_t3894720829 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_routa80::ilo_M_sayteStaicheesoo01(GarbageiOS.M_routa80,System.String[],System.Int32)
extern "C"  void M_routa80_ilo_M_sayteStaicheesoo01_m149226373 (Il2CppObject * __this /* static, unused */, M_routa80_t3894720829 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
