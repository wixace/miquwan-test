﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.Ionic.Zip.BadReadException
struct BadReadException_t4166899936;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void Pathfinding.Ionic.Zip.BadReadException::.ctor(System.String)
extern "C"  void BadReadException__ctor_m3175006449 (BadReadException_t4166899936 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
