﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_ShaderGenerated
struct UnityEngine_ShaderGenerated_t3651030210;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"

// System.Void UnityEngine_ShaderGenerated::.ctor()
extern "C"  void UnityEngine_ShaderGenerated__ctor_m2253887721 (UnityEngine_ShaderGenerated_t3651030210 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ShaderGenerated::Shader_Shader1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ShaderGenerated_Shader_Shader1_m3880522377 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ShaderGenerated::Shader_isSupported(JSVCall)
extern "C"  void UnityEngine_ShaderGenerated_Shader_isSupported_m1814789506 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ShaderGenerated::Shader_maximumLOD(JSVCall)
extern "C"  void UnityEngine_ShaderGenerated_Shader_maximumLOD_m2554367877 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ShaderGenerated::Shader_globalMaximumLOD(JSVCall)
extern "C"  void UnityEngine_ShaderGenerated_Shader_globalMaximumLOD_m2651851490 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ShaderGenerated::Shader_renderQueue(JSVCall)
extern "C"  void UnityEngine_ShaderGenerated_Shader_renderQueue_m3603920107 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ShaderGenerated::Shader_DisableKeyword__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ShaderGenerated_Shader_DisableKeyword__String_m2490891639 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ShaderGenerated::Shader_EnableKeyword__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ShaderGenerated_Shader_EnableKeyword__String_m3529248596 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ShaderGenerated::Shader_Find__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ShaderGenerated_Shader_Find__String_m705006671 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ShaderGenerated::Shader_IsKeywordEnabled__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ShaderGenerated_Shader_IsKeywordEnabled__String_m2851856312 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ShaderGenerated::Shader_PropertyToID__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ShaderGenerated_Shader_PropertyToID__String_m443865953 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ShaderGenerated::Shader_SetGlobalBuffer__String__ComputeBuffer(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ShaderGenerated_Shader_SetGlobalBuffer__String__ComputeBuffer_m3544660358 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ShaderGenerated::Shader_SetGlobalColor__String__Color(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ShaderGenerated_Shader_SetGlobalColor__String__Color_m3732654417 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ShaderGenerated::Shader_SetGlobalColor__Int32__Color(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ShaderGenerated_Shader_SetGlobalColor__Int32__Color_m3864469272 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ShaderGenerated::Shader_SetGlobalFloat__Int32__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ShaderGenerated_Shader_SetGlobalFloat__Int32__Single_m3824936348 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ShaderGenerated::Shader_SetGlobalFloat__String__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ShaderGenerated_Shader_SetGlobalFloat__String__Single_m3288321557 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ShaderGenerated::Shader_SetGlobalInt__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ShaderGenerated_Shader_SetGlobalInt__Int32__Int32_m944358991 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ShaderGenerated::Shader_SetGlobalInt__String__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ShaderGenerated_Shader_SetGlobalInt__String__Int32_m654296432 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ShaderGenerated::Shader_SetGlobalMatrix__String__Matrix4x4(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ShaderGenerated_Shader_SetGlobalMatrix__String__Matrix4x4_m2222150493 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ShaderGenerated::Shader_SetGlobalMatrix__Int32__Matrix4x4(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ShaderGenerated_Shader_SetGlobalMatrix__Int32__Matrix4x4_m897506532 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ShaderGenerated::Shader_SetGlobalTexture__String__Texture(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ShaderGenerated_Shader_SetGlobalTexture__String__Texture_m1063316689 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ShaderGenerated::Shader_SetGlobalTexture__Int32__Texture(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ShaderGenerated_Shader_SetGlobalTexture__Int32__Texture_m1410766280 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ShaderGenerated::Shader_SetGlobalVector__String__Vector4(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ShaderGenerated_Shader_SetGlobalVector__String__Vector4_m3557304861 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ShaderGenerated::Shader_SetGlobalVector__Int32__Vector4(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ShaderGenerated_Shader_SetGlobalVector__Int32__Vector4_m3243874600 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ShaderGenerated::Shader_WarmupAllShaders(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ShaderGenerated_Shader_WarmupAllShaders_m2612057490 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ShaderGenerated::__Register()
extern "C"  void UnityEngine_ShaderGenerated___Register_m736234878 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ShaderGenerated::ilo_addJSCSRel1(System.Int32,System.Object)
extern "C"  void UnityEngine_ShaderGenerated_ilo_addJSCSRel1_m1035644709 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ShaderGenerated::ilo_setInt322(System.Int32,System.Int32)
extern "C"  void UnityEngine_ShaderGenerated_ilo_setInt322_m2338685804 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_ShaderGenerated::ilo_getInt323(System.Int32)
extern "C"  int32_t UnityEngine_ShaderGenerated_ilo_getInt323_m161965710 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine_ShaderGenerated::ilo_getStringS4(System.Int32)
extern "C"  String_t* UnityEngine_ShaderGenerated_ilo_getStringS4_m1401808016 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_ShaderGenerated::ilo_setObject5(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UnityEngine_ShaderGenerated_ilo_setObject5_m2452441705 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ShaderGenerated::ilo_setBooleanS6(System.Int32,System.Boolean)
extern "C"  void UnityEngine_ShaderGenerated_ilo_setBooleanS6_m3849674421 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_ShaderGenerated::ilo_getObject7(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_ShaderGenerated_ilo_getObject7_m2993697890 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine_ShaderGenerated::ilo_getSingle8(System.Int32)
extern "C"  float UnityEngine_ShaderGenerated_ilo_getSingle8_m251997325 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
