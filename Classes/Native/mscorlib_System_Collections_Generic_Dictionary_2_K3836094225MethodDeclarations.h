﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Ke655669851MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Poly2Tri.TriangulationPoint,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define KeyCollection__ctor_m2102489763(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t3836094225 *, Dictionary_2_t2209334774 *, const MethodInfo*))KeyCollection__ctor_m2092569765_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Poly2Tri.TriangulationPoint,System.Int32>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m109677779(__this, ___item0, method) ((  void (*) (KeyCollection_t3836094225 *, TriangulationPoint_t3810082933 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m199242129_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Poly2Tri.TriangulationPoint,System.Int32>::System.Collections.Generic.ICollection<TKey>.Clear()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1398983434(__this, method) ((  void (*) (KeyCollection_t3836094225 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3942090568_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Poly2Tri.TriangulationPoint,System.Int32>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m4104775259(__this, ___item0, method) ((  bool (*) (KeyCollection_t3836094225 *, TriangulationPoint_t3810082933 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m496617181_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Poly2Tri.TriangulationPoint,System.Int32>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3848417920(__this, ___item0, method) ((  bool (*) (KeyCollection_t3836094225 *, TriangulationPoint_t3810082933 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m175393666_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Poly2Tri.TriangulationPoint,System.Int32>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m4063698588(__this, method) ((  Il2CppObject* (*) (KeyCollection_t3836094225 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m2466934938_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Poly2Tri.TriangulationPoint,System.Int32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define KeyCollection_System_Collections_ICollection_CopyTo_m1208643708(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t3836094225 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m505462714_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Poly2Tri.TriangulationPoint,System.Int32>::System.Collections.IEnumerable.GetEnumerator()
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m3692537227(__this, method) ((  Il2CppObject * (*) (KeyCollection_t3836094225 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m3955992777_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Poly2Tri.TriangulationPoint,System.Int32>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m495137532(__this, method) ((  bool (*) (KeyCollection_t3836094225 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m3388799742_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Poly2Tri.TriangulationPoint,System.Int32>::System.Collections.ICollection.get_IsSynchronized()
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m102598318(__this, method) ((  bool (*) (KeyCollection_t3836094225 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m2230257968_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Poly2Tri.TriangulationPoint,System.Int32>::System.Collections.ICollection.get_SyncRoot()
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m3993780896(__this, method) ((  Il2CppObject * (*) (KeyCollection_t3836094225 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m281315426_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Poly2Tri.TriangulationPoint,System.Int32>::CopyTo(TKey[],System.Int32)
#define KeyCollection_CopyTo_m1431971160(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t3836094225 *, TriangulationPointU5BU5D_t4289080856*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m3090894682_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Poly2Tri.TriangulationPoint,System.Int32>::GetEnumerator()
#define KeyCollection_GetEnumerator_m301357925(__this, method) ((  Enumerator_t2824270828  (*) (KeyCollection_t3836094225 *, const MethodInfo*))KeyCollection_GetEnumerator_m363545767_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Poly2Tri.TriangulationPoint,System.Int32>::get_Count()
#define KeyCollection_get_Count_m161200616(__this, method) ((  int32_t (*) (KeyCollection_t3836094225 *, const MethodInfo*))KeyCollection_get_Count_m264049386_gshared)(__this, method)
