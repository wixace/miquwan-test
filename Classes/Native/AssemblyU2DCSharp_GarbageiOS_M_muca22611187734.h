﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_muca22
struct  M_muca22_t611187734  : public Il2CppObject
{
public:
	// System.Single GarbageiOS.M_muca22::_caykaHuterjair
	float ____caykaHuterjair_0;
	// System.Single GarbageiOS.M_muca22::_mumawberSelnem
	float ____mumawberSelnem_1;
	// System.UInt32 GarbageiOS.M_muca22::_whalejair
	uint32_t ____whalejair_2;
	// System.Int32 GarbageiOS.M_muca22::_qorni
	int32_t ____qorni_3;
	// System.Boolean GarbageiOS.M_muca22::_martikaJasisnou
	bool ____martikaJasisnou_4;
	// System.Single GarbageiOS.M_muca22::_tulasgir
	float ____tulasgir_5;

public:
	inline static int32_t get_offset_of__caykaHuterjair_0() { return static_cast<int32_t>(offsetof(M_muca22_t611187734, ____caykaHuterjair_0)); }
	inline float get__caykaHuterjair_0() const { return ____caykaHuterjair_0; }
	inline float* get_address_of__caykaHuterjair_0() { return &____caykaHuterjair_0; }
	inline void set__caykaHuterjair_0(float value)
	{
		____caykaHuterjair_0 = value;
	}

	inline static int32_t get_offset_of__mumawberSelnem_1() { return static_cast<int32_t>(offsetof(M_muca22_t611187734, ____mumawberSelnem_1)); }
	inline float get__mumawberSelnem_1() const { return ____mumawberSelnem_1; }
	inline float* get_address_of__mumawberSelnem_1() { return &____mumawberSelnem_1; }
	inline void set__mumawberSelnem_1(float value)
	{
		____mumawberSelnem_1 = value;
	}

	inline static int32_t get_offset_of__whalejair_2() { return static_cast<int32_t>(offsetof(M_muca22_t611187734, ____whalejair_2)); }
	inline uint32_t get__whalejair_2() const { return ____whalejair_2; }
	inline uint32_t* get_address_of__whalejair_2() { return &____whalejair_2; }
	inline void set__whalejair_2(uint32_t value)
	{
		____whalejair_2 = value;
	}

	inline static int32_t get_offset_of__qorni_3() { return static_cast<int32_t>(offsetof(M_muca22_t611187734, ____qorni_3)); }
	inline int32_t get__qorni_3() const { return ____qorni_3; }
	inline int32_t* get_address_of__qorni_3() { return &____qorni_3; }
	inline void set__qorni_3(int32_t value)
	{
		____qorni_3 = value;
	}

	inline static int32_t get_offset_of__martikaJasisnou_4() { return static_cast<int32_t>(offsetof(M_muca22_t611187734, ____martikaJasisnou_4)); }
	inline bool get__martikaJasisnou_4() const { return ____martikaJasisnou_4; }
	inline bool* get_address_of__martikaJasisnou_4() { return &____martikaJasisnou_4; }
	inline void set__martikaJasisnou_4(bool value)
	{
		____martikaJasisnou_4 = value;
	}

	inline static int32_t get_offset_of__tulasgir_5() { return static_cast<int32_t>(offsetof(M_muca22_t611187734, ____tulasgir_5)); }
	inline float get__tulasgir_5() const { return ____tulasgir_5; }
	inline float* get_address_of__tulasgir_5() { return &____tulasgir_5; }
	inline void set__tulasgir_5(float value)
	{
		____tulasgir_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
