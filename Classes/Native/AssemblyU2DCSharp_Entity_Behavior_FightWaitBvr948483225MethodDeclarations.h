﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Entity.Behavior.FightWaitBvr
struct FightWaitBvr_t948483225;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// CombatEntity
struct CombatEntity_t684137495;
// Entity.Behavior.IBehavior
struct IBehavior_t770859129;
// Entity.Behavior.IBehaviorCtrl
struct IBehaviorCtrl_t4225040900;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Entity_Behavior_EBehaviorID1238533136.h"
#include "AssemblyU2DCSharp_Entity_Behavior_IBehavior770859129.h"
#include "AssemblyU2DCSharp_CombatEntity684137495.h"

// System.Void Entity.Behavior.FightWaitBvr::.ctor()
extern "C"  void FightWaitBvr__ctor_m621844657 (FightWaitBvr_t948483225 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Entity.Behavior.EBehaviorID Entity.Behavior.FightWaitBvr::get_id()
extern "C"  uint8_t FightWaitBvr_get_id_m4288952073 (FightWaitBvr_t948483225 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.FightWaitBvr::Reason()
extern "C"  void FightWaitBvr_Reason_m1648703287 (FightWaitBvr_t948483225 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.FightWaitBvr::Action()
extern "C"  void FightWaitBvr_Action_m845409961 (FightWaitBvr_t948483225 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.FightWaitBvr::DoEntering()
extern "C"  void FightWaitBvr_DoEntering_m922745704 (FightWaitBvr_t948483225 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.FightWaitBvr::DoLeaving()
extern "C"  void FightWaitBvr_DoLeaving_m2414338872 (FightWaitBvr_t948483225 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.FightWaitBvr::SetParams(System.Object[])
extern "C"  void FightWaitBvr_SetParams_m563283323 (FightWaitBvr_t948483225 * __this, ObjectU5BU5D_t1108656482* ___args0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CombatEntity Entity.Behavior.FightWaitBvr::ilo_get_entity1(Entity.Behavior.IBehavior)
extern "C"  CombatEntity_t684137495 * FightWaitBvr_ilo_get_entity1_m297118189 (Il2CppObject * __this /* static, unused */, IBehavior_t770859129 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Entity.Behavior.IBehaviorCtrl Entity.Behavior.FightWaitBvr::ilo_get_bvrCtrl2(CombatEntity)
extern "C"  IBehaviorCtrl_t4225040900 * FightWaitBvr_ilo_get_bvrCtrl2_m1904109535 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
