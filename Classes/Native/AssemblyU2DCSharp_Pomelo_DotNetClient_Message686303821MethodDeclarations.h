﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pomelo.DotNetClient.Message
struct Message_t686303821;
// System.String
struct String_t;
// Newtonsoft.Json.Linq.JObject
struct JObject_t1798544199;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pomelo_DotNetClient_MessageType2182326183.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JObject1798544199.h"

// System.Void Pomelo.DotNetClient.Message::.ctor(Pomelo.DotNetClient.MessageType,System.UInt32,System.String,Newtonsoft.Json.Linq.JObject)
extern "C"  void Message__ctor_m3781305817 (Message_t686303821 * __this, int32_t ___type0, uint32_t ___id1, String_t* ___route2, JObject_t1798544199 * ___data3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
