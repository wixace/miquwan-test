﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EStringGenerated
struct EStringGenerated_t1904697721;
// JSVCall
struct JSVCall_t3708497963;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_String7231557.h"

// System.Void EStringGenerated::.ctor()
extern "C"  void EStringGenerated__ctor_m2302762306 (EStringGenerated_t1904697721 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean EStringGenerated::EString_EFormat__String__Object__Object__Object(JSVCall,System.Int32)
extern "C"  bool EStringGenerated_EString_EFormat__String__Object__Object__Object_m1034662461 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean EStringGenerated::EString_EFormat__String__Object__Object(JSVCall,System.Int32)
extern "C"  bool EStringGenerated_EString_EFormat__String__Object__Object_m4042163518 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean EStringGenerated::EString_EFormat__IFormatProvider__String__Object_Array(JSVCall,System.Int32)
extern "C"  bool EStringGenerated_EString_EFormat__IFormatProvider__String__Object_Array_m2283688910 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean EStringGenerated::EString_EFormat__String__Object(JSVCall,System.Int32)
extern "C"  bool EStringGenerated_EString_EFormat__String__Object_m378595647 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean EStringGenerated::EString_EFormat__String__Object_Array(JSVCall,System.Int32)
extern "C"  bool EStringGenerated_EString_EFormat__String__Object_Array_m3677121657 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean EStringGenerated::EString_EReplace__String__String__String__String(JSVCall,System.Int32)
extern "C"  bool EStringGenerated_EString_EReplace__String__String__String__String_m3843070914 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean EStringGenerated::EString_EReplace__String__String__String(JSVCall,System.Int32)
extern "C"  bool EStringGenerated_EString_EReplace__String__String__String_m638246513 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EStringGenerated::__Register()
extern "C"  void EStringGenerated___Register_m2689734853 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] EStringGenerated::<EString_EFormat__IFormatProvider__String__Object_Array>m__42()
extern "C"  ObjectU5BU5D_t1108656482* EStringGenerated_U3CEString_EFormat__IFormatProvider__String__Object_ArrayU3Em__42_m489055614 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] EStringGenerated::<EString_EFormat__String__Object_Array>m__43()
extern "C"  ObjectU5BU5D_t1108656482* EStringGenerated_U3CEString_EFormat__String__Object_ArrayU3Em__43_m4230484452 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object EStringGenerated::ilo_getWhatever1(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * EStringGenerated_ilo_getWhatever1_m515068778 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EStringGenerated::ilo_setStringS2(System.Int32,System.String)
extern "C"  void EStringGenerated_ilo_setStringS2_m1294179212 (Il2CppObject * __this /* static, unused */, int32_t ___e0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object EStringGenerated::ilo_getObject3(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * EStringGenerated_ilo_getObject3_m3848256111 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String EStringGenerated::ilo_getStringS4(System.Int32)
extern "C"  String_t* EStringGenerated_ilo_getStringS4_m3874611011 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 EStringGenerated::ilo_getArrayLength5(System.Int32)
extern "C"  int32_t EStringGenerated_ilo_getArrayLength5_m1812790876 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 EStringGenerated::ilo_getElement6(System.Int32,System.Int32)
extern "C"  int32_t EStringGenerated_ilo_getElement6_m2066128951 (Il2CppObject * __this /* static, unused */, int32_t ___id0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
