﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIEventListenerGenerated/<UIEventListener_onSelect_GetDelegate_member6_arg0>c__AnonStoreyBA
struct U3CUIEventListener_onSelect_GetDelegate_member6_arg0U3Ec__AnonStoreyBA_t1281723750;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"

// System.Void UIEventListenerGenerated/<UIEventListener_onSelect_GetDelegate_member6_arg0>c__AnonStoreyBA::.ctor()
extern "C"  void U3CUIEventListener_onSelect_GetDelegate_member6_arg0U3Ec__AnonStoreyBA__ctor_m2453246149 (U3CUIEventListener_onSelect_GetDelegate_member6_arg0U3Ec__AnonStoreyBA_t1281723750 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIEventListenerGenerated/<UIEventListener_onSelect_GetDelegate_member6_arg0>c__AnonStoreyBA::<>m__138(UnityEngine.GameObject,System.Boolean)
extern "C"  void U3CUIEventListener_onSelect_GetDelegate_member6_arg0U3Ec__AnonStoreyBA_U3CU3Em__138_m1205281051 (U3CUIEventListener_onSelect_GetDelegate_member6_arg0U3Ec__AnonStoreyBA_t1281723750 * __this, GameObject_t3674682005 * ___go0, bool ___state1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
