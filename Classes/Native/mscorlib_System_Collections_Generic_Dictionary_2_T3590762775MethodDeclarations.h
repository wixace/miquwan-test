﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/Transform`1<FLOAT_TEXT_ID,System.Object,FLOAT_TEXT_ID>
struct Transform_1_t3590762775;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "AssemblyU2DCSharp_FLOAT_TEXT_ID2199345770.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<FLOAT_TEXT_ID,System.Object,FLOAT_TEXT_ID>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m2195690155_gshared (Transform_1_t3590762775 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define Transform_1__ctor_m2195690155(__this, ___object0, ___method1, method) ((  void (*) (Transform_1_t3590762775 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m2195690155_gshared)(__this, ___object0, ___method1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<FLOAT_TEXT_ID,System.Object,FLOAT_TEXT_ID>::Invoke(TKey,TValue)
extern "C"  int32_t Transform_1_Invoke_m3203106801_gshared (Transform_1_t3590762775 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Transform_1_Invoke_m3203106801(__this, ___key0, ___value1, method) ((  int32_t (*) (Transform_1_t3590762775 *, int32_t, Il2CppObject *, const MethodInfo*))Transform_1_Invoke_m3203106801_gshared)(__this, ___key0, ___value1, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<FLOAT_TEXT_ID,System.Object,FLOAT_TEXT_ID>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m51777360_gshared (Transform_1_t3590762775 * __this, int32_t ___key0, Il2CppObject * ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method);
#define Transform_1_BeginInvoke_m51777360(__this, ___key0, ___value1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Transform_1_t3590762775 *, int32_t, Il2CppObject *, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))Transform_1_BeginInvoke_m51777360_gshared)(__this, ___key0, ___value1, ___callback2, ___object3, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<FLOAT_TEXT_ID,System.Object,FLOAT_TEXT_ID>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t Transform_1_EndInvoke_m1196517369_gshared (Transform_1_t3590762775 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define Transform_1_EndInvoke_m1196517369(__this, ___result0, method) ((  int32_t (*) (Transform_1_t3590762775 *, Il2CppObject *, const MethodInfo*))Transform_1_EndInvoke_m1196517369_gshared)(__this, ___result0, method)
