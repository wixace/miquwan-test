﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TweenAlphaGenerated
struct TweenAlphaGenerated_t891909372;
// JSVCall
struct JSVCall_t3708497963;
// TweenAlpha
struct TweenAlpha_t2920325587;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_TweenAlpha2920325587.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void TweenAlphaGenerated::.ctor()
extern "C"  void TweenAlphaGenerated__ctor_m3936516591 (TweenAlphaGenerated_t891909372 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TweenAlphaGenerated::TweenAlpha_TweenAlpha1(JSVCall,System.Int32)
extern "C"  bool TweenAlphaGenerated_TweenAlpha_TweenAlpha1_m3072824579 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenAlphaGenerated::TweenAlpha_from(JSVCall)
extern "C"  void TweenAlphaGenerated_TweenAlpha_from_m1695954692 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenAlphaGenerated::TweenAlpha_to(JSVCall)
extern "C"  void TweenAlphaGenerated_TweenAlpha_to_m2508174675 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenAlphaGenerated::TweenAlpha_value(JSVCall)
extern "C"  void TweenAlphaGenerated_TweenAlpha_value_m1920386637 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TweenAlphaGenerated::TweenAlpha_SetEndToCurrentValue(JSVCall,System.Int32)
extern "C"  bool TweenAlphaGenerated_TweenAlpha_SetEndToCurrentValue_m2835947529 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TweenAlphaGenerated::TweenAlpha_SetStartToCurrentValue(JSVCall,System.Int32)
extern "C"  bool TweenAlphaGenerated_TweenAlpha_SetStartToCurrentValue_m1067286928 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TweenAlphaGenerated::TweenAlpha_Begin__GameObject__Single__Single(JSVCall,System.Int32)
extern "C"  bool TweenAlphaGenerated_TweenAlpha_Begin__GameObject__Single__Single_m3527709103 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenAlphaGenerated::__Register()
extern "C"  void TweenAlphaGenerated___Register_m3799572536 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenAlphaGenerated::ilo_setSingle1(System.Int32,System.Single)
extern "C"  void TweenAlphaGenerated_ilo_setSingle1_m728735781 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenAlphaGenerated::ilo_set_value2(TweenAlpha,System.Single)
extern "C"  void TweenAlphaGenerated_ilo_set_value2_m4017147864 (Il2CppObject * __this /* static, unused */, TweenAlpha_t2920325587 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenAlphaGenerated::ilo_SetEndToCurrentValue3(TweenAlpha)
extern "C"  void TweenAlphaGenerated_ilo_SetEndToCurrentValue3_m2421946606 (Il2CppObject * __this /* static, unused */, TweenAlpha_t2920325587 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenAlphaGenerated::ilo_SetStartToCurrentValue4(TweenAlpha)
extern "C"  void TweenAlphaGenerated_ilo_SetStartToCurrentValue4_m2150114504 (Il2CppObject * __this /* static, unused */, TweenAlpha_t2920325587 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object TweenAlphaGenerated::ilo_getObject5(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * TweenAlphaGenerated_ilo_getObject5_m3588406170 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TweenAlphaGenerated::ilo_getSingle6(System.Int32)
extern "C"  float TweenAlphaGenerated_ilo_getSingle6_m408054469 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 TweenAlphaGenerated::ilo_setObject7(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t TweenAlphaGenerated_ilo_setObject7_m3096190885 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
