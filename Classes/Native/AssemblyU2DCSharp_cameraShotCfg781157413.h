﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "AssemblyU2DCSharp_CsCfgBase69924517.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// cameraShotCfg
struct  cameraShotCfg_t781157413  : public CsCfgBase_t69924517
{
public:
	// System.Int32 cameraShotCfg::id
	int32_t ___id_0;
	// System.Int32 cameraShotCfg::nextId
	int32_t ___nextId_1;
	// System.Int32 cameraShotCfg::storyId
	int32_t ___storyId_2;
	// System.Int32 cameraShotCfg::isNextWith
	int32_t ___isNextWith_3;
	// System.Int32 cameraShotCfg::preId
	int32_t ___preId_4;
	// System.Int32 cameraShotCfg::funType
	int32_t ___funType_5;
	// System.String cameraShotCfg::param1
	String_t* ___param1_6;
	// System.String cameraShotCfg::param2
	String_t* ___param2_7;
	// System.String cameraShotCfg::param3
	String_t* ___param3_8;
	// System.String cameraShotCfg::param4
	String_t* ___param4_9;
	// System.String cameraShotCfg::param5
	String_t* ___param5_10;
	// System.String cameraShotCfg::param6
	String_t* ___param6_11;
	// System.String cameraShotCfg::param7
	String_t* ___param7_12;
	// System.String cameraShotCfg::param8
	String_t* ___param8_13;
	// System.String cameraShotCfg::remark
	String_t* ___remark_14;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(cameraShotCfg_t781157413, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_nextId_1() { return static_cast<int32_t>(offsetof(cameraShotCfg_t781157413, ___nextId_1)); }
	inline int32_t get_nextId_1() const { return ___nextId_1; }
	inline int32_t* get_address_of_nextId_1() { return &___nextId_1; }
	inline void set_nextId_1(int32_t value)
	{
		___nextId_1 = value;
	}

	inline static int32_t get_offset_of_storyId_2() { return static_cast<int32_t>(offsetof(cameraShotCfg_t781157413, ___storyId_2)); }
	inline int32_t get_storyId_2() const { return ___storyId_2; }
	inline int32_t* get_address_of_storyId_2() { return &___storyId_2; }
	inline void set_storyId_2(int32_t value)
	{
		___storyId_2 = value;
	}

	inline static int32_t get_offset_of_isNextWith_3() { return static_cast<int32_t>(offsetof(cameraShotCfg_t781157413, ___isNextWith_3)); }
	inline int32_t get_isNextWith_3() const { return ___isNextWith_3; }
	inline int32_t* get_address_of_isNextWith_3() { return &___isNextWith_3; }
	inline void set_isNextWith_3(int32_t value)
	{
		___isNextWith_3 = value;
	}

	inline static int32_t get_offset_of_preId_4() { return static_cast<int32_t>(offsetof(cameraShotCfg_t781157413, ___preId_4)); }
	inline int32_t get_preId_4() const { return ___preId_4; }
	inline int32_t* get_address_of_preId_4() { return &___preId_4; }
	inline void set_preId_4(int32_t value)
	{
		___preId_4 = value;
	}

	inline static int32_t get_offset_of_funType_5() { return static_cast<int32_t>(offsetof(cameraShotCfg_t781157413, ___funType_5)); }
	inline int32_t get_funType_5() const { return ___funType_5; }
	inline int32_t* get_address_of_funType_5() { return &___funType_5; }
	inline void set_funType_5(int32_t value)
	{
		___funType_5 = value;
	}

	inline static int32_t get_offset_of_param1_6() { return static_cast<int32_t>(offsetof(cameraShotCfg_t781157413, ___param1_6)); }
	inline String_t* get_param1_6() const { return ___param1_6; }
	inline String_t** get_address_of_param1_6() { return &___param1_6; }
	inline void set_param1_6(String_t* value)
	{
		___param1_6 = value;
		Il2CppCodeGenWriteBarrier(&___param1_6, value);
	}

	inline static int32_t get_offset_of_param2_7() { return static_cast<int32_t>(offsetof(cameraShotCfg_t781157413, ___param2_7)); }
	inline String_t* get_param2_7() const { return ___param2_7; }
	inline String_t** get_address_of_param2_7() { return &___param2_7; }
	inline void set_param2_7(String_t* value)
	{
		___param2_7 = value;
		Il2CppCodeGenWriteBarrier(&___param2_7, value);
	}

	inline static int32_t get_offset_of_param3_8() { return static_cast<int32_t>(offsetof(cameraShotCfg_t781157413, ___param3_8)); }
	inline String_t* get_param3_8() const { return ___param3_8; }
	inline String_t** get_address_of_param3_8() { return &___param3_8; }
	inline void set_param3_8(String_t* value)
	{
		___param3_8 = value;
		Il2CppCodeGenWriteBarrier(&___param3_8, value);
	}

	inline static int32_t get_offset_of_param4_9() { return static_cast<int32_t>(offsetof(cameraShotCfg_t781157413, ___param4_9)); }
	inline String_t* get_param4_9() const { return ___param4_9; }
	inline String_t** get_address_of_param4_9() { return &___param4_9; }
	inline void set_param4_9(String_t* value)
	{
		___param4_9 = value;
		Il2CppCodeGenWriteBarrier(&___param4_9, value);
	}

	inline static int32_t get_offset_of_param5_10() { return static_cast<int32_t>(offsetof(cameraShotCfg_t781157413, ___param5_10)); }
	inline String_t* get_param5_10() const { return ___param5_10; }
	inline String_t** get_address_of_param5_10() { return &___param5_10; }
	inline void set_param5_10(String_t* value)
	{
		___param5_10 = value;
		Il2CppCodeGenWriteBarrier(&___param5_10, value);
	}

	inline static int32_t get_offset_of_param6_11() { return static_cast<int32_t>(offsetof(cameraShotCfg_t781157413, ___param6_11)); }
	inline String_t* get_param6_11() const { return ___param6_11; }
	inline String_t** get_address_of_param6_11() { return &___param6_11; }
	inline void set_param6_11(String_t* value)
	{
		___param6_11 = value;
		Il2CppCodeGenWriteBarrier(&___param6_11, value);
	}

	inline static int32_t get_offset_of_param7_12() { return static_cast<int32_t>(offsetof(cameraShotCfg_t781157413, ___param7_12)); }
	inline String_t* get_param7_12() const { return ___param7_12; }
	inline String_t** get_address_of_param7_12() { return &___param7_12; }
	inline void set_param7_12(String_t* value)
	{
		___param7_12 = value;
		Il2CppCodeGenWriteBarrier(&___param7_12, value);
	}

	inline static int32_t get_offset_of_param8_13() { return static_cast<int32_t>(offsetof(cameraShotCfg_t781157413, ___param8_13)); }
	inline String_t* get_param8_13() const { return ___param8_13; }
	inline String_t** get_address_of_param8_13() { return &___param8_13; }
	inline void set_param8_13(String_t* value)
	{
		___param8_13 = value;
		Il2CppCodeGenWriteBarrier(&___param8_13, value);
	}

	inline static int32_t get_offset_of_remark_14() { return static_cast<int32_t>(offsetof(cameraShotCfg_t781157413, ___remark_14)); }
	inline String_t* get_remark_14() const { return ___remark_14; }
	inline String_t** get_address_of_remark_14() { return &___remark_14; }
	inline void set_remark_14(String_t* value)
	{
		___remark_14 = value;
		Il2CppCodeGenWriteBarrier(&___remark_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
