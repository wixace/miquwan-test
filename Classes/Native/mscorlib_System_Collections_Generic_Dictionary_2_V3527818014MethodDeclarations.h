﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,HatredCtrl/stValue>
struct Dictionary_2_t1301017310;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V3527818014.h"
#include "AssemblyU2DCSharp_HatredCtrl_stValue3425945410.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,HatredCtrl/stValue>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m2937866719_gshared (Enumerator_t3527818014 * __this, Dictionary_2_t1301017310 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m2937866719(__this, ___host0, method) ((  void (*) (Enumerator_t3527818014 *, Dictionary_2_t1301017310 *, const MethodInfo*))Enumerator__ctor_m2937866719_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,HatredCtrl/stValue>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1938076716_gshared (Enumerator_t3527818014 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1938076716(__this, method) ((  Il2CppObject * (*) (Enumerator_t3527818014 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1938076716_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,HatredCtrl/stValue>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1620572534_gshared (Enumerator_t3527818014 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1620572534(__this, method) ((  void (*) (Enumerator_t3527818014 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1620572534_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,HatredCtrl/stValue>::Dispose()
extern "C"  void Enumerator_Dispose_m282482241_gshared (Enumerator_t3527818014 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m282482241(__this, method) ((  void (*) (Enumerator_t3527818014 *, const MethodInfo*))Enumerator_Dispose_m282482241_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,HatredCtrl/stValue>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m698093222_gshared (Enumerator_t3527818014 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m698093222(__this, method) ((  bool (*) (Enumerator_t3527818014 *, const MethodInfo*))Enumerator_MoveNext_m698093222_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,HatredCtrl/stValue>::get_Current()
extern "C"  stValue_t3425945410  Enumerator_get_Current_m4287026884_gshared (Enumerator_t3527818014 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m4287026884(__this, method) ((  stValue_t3425945410  (*) (Enumerator_t3527818014 *, const MethodInfo*))Enumerator_get_Current_m4287026884_gshared)(__this, method)
