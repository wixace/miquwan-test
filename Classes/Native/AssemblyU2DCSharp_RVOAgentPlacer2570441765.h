﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_LayerMask3236759763.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RVOAgentPlacer
struct  RVOAgentPlacer_t2570441765  : public MonoBehaviour_t667441552
{
public:
	// System.Int32 RVOAgentPlacer::agents
	int32_t ___agents_3;
	// System.Single RVOAgentPlacer::ringSize
	float ___ringSize_4;
	// UnityEngine.LayerMask RVOAgentPlacer::mask
	LayerMask_t3236759763  ___mask_5;
	// UnityEngine.GameObject RVOAgentPlacer::prefab
	GameObject_t3674682005 * ___prefab_6;
	// UnityEngine.Vector3 RVOAgentPlacer::goalOffset
	Vector3_t4282066566  ___goalOffset_7;
	// System.Single RVOAgentPlacer::repathRate
	float ___repathRate_8;

public:
	inline static int32_t get_offset_of_agents_3() { return static_cast<int32_t>(offsetof(RVOAgentPlacer_t2570441765, ___agents_3)); }
	inline int32_t get_agents_3() const { return ___agents_3; }
	inline int32_t* get_address_of_agents_3() { return &___agents_3; }
	inline void set_agents_3(int32_t value)
	{
		___agents_3 = value;
	}

	inline static int32_t get_offset_of_ringSize_4() { return static_cast<int32_t>(offsetof(RVOAgentPlacer_t2570441765, ___ringSize_4)); }
	inline float get_ringSize_4() const { return ___ringSize_4; }
	inline float* get_address_of_ringSize_4() { return &___ringSize_4; }
	inline void set_ringSize_4(float value)
	{
		___ringSize_4 = value;
	}

	inline static int32_t get_offset_of_mask_5() { return static_cast<int32_t>(offsetof(RVOAgentPlacer_t2570441765, ___mask_5)); }
	inline LayerMask_t3236759763  get_mask_5() const { return ___mask_5; }
	inline LayerMask_t3236759763 * get_address_of_mask_5() { return &___mask_5; }
	inline void set_mask_5(LayerMask_t3236759763  value)
	{
		___mask_5 = value;
	}

	inline static int32_t get_offset_of_prefab_6() { return static_cast<int32_t>(offsetof(RVOAgentPlacer_t2570441765, ___prefab_6)); }
	inline GameObject_t3674682005 * get_prefab_6() const { return ___prefab_6; }
	inline GameObject_t3674682005 ** get_address_of_prefab_6() { return &___prefab_6; }
	inline void set_prefab_6(GameObject_t3674682005 * value)
	{
		___prefab_6 = value;
		Il2CppCodeGenWriteBarrier(&___prefab_6, value);
	}

	inline static int32_t get_offset_of_goalOffset_7() { return static_cast<int32_t>(offsetof(RVOAgentPlacer_t2570441765, ___goalOffset_7)); }
	inline Vector3_t4282066566  get_goalOffset_7() const { return ___goalOffset_7; }
	inline Vector3_t4282066566 * get_address_of_goalOffset_7() { return &___goalOffset_7; }
	inline void set_goalOffset_7(Vector3_t4282066566  value)
	{
		___goalOffset_7 = value;
	}

	inline static int32_t get_offset_of_repathRate_8() { return static_cast<int32_t>(offsetof(RVOAgentPlacer_t2570441765, ___repathRate_8)); }
	inline float get_repathRate_8() const { return ___repathRate_8; }
	inline float* get_address_of_repathRate_8() { return &___repathRate_8; }
	inline void set_repathRate_8(float value)
	{
		___repathRate_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
