﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HeroMgrGenerated/<HeroMgr_LeaveBattleReqOnWin_GetDelegate_member50_arg0>c__AnonStorey69
struct U3CHeroMgr_LeaveBattleReqOnWin_GetDelegate_member50_arg0U3Ec__AnonStorey69_t3061441660;

#include "codegen/il2cpp-codegen.h"

// System.Void HeroMgrGenerated/<HeroMgr_LeaveBattleReqOnWin_GetDelegate_member50_arg0>c__AnonStorey69::.ctor()
extern "C"  void U3CHeroMgr_LeaveBattleReqOnWin_GetDelegate_member50_arg0U3Ec__AnonStorey69__ctor_m3755771503 (U3CHeroMgr_LeaveBattleReqOnWin_GetDelegate_member50_arg0U3Ec__AnonStorey69_t3061441660 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroMgrGenerated/<HeroMgr_LeaveBattleReqOnWin_GetDelegate_member50_arg0>c__AnonStorey69::<>m__66()
extern "C"  void U3CHeroMgr_LeaveBattleReqOnWin_GetDelegate_member50_arg0U3Ec__AnonStorey69_U3CU3Em__66_m3832080440 (U3CHeroMgr_LeaveBattleReqOnWin_GetDelegate_member50_arg0U3Ec__AnonStorey69_t3061441660 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
