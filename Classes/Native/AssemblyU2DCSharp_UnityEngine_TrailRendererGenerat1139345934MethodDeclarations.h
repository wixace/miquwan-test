﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_TrailRendererGenerated
struct UnityEngine_TrailRendererGenerated_t1139345934;
// JSVCall
struct JSVCall_t3708497963;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"

// System.Void UnityEngine_TrailRendererGenerated::.ctor()
extern "C"  void UnityEngine_TrailRendererGenerated__ctor_m2127115853 (UnityEngine_TrailRendererGenerated_t1139345934 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TrailRendererGenerated::TrailRenderer_TrailRenderer1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TrailRendererGenerated_TrailRenderer_TrailRenderer1_m2663542841 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TrailRendererGenerated::TrailRenderer_time(JSVCall)
extern "C"  void UnityEngine_TrailRendererGenerated_TrailRenderer_time_m2555815405 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TrailRendererGenerated::TrailRenderer_startWidth(JSVCall)
extern "C"  void UnityEngine_TrailRendererGenerated_TrailRenderer_startWidth_m2192544022 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TrailRendererGenerated::TrailRenderer_endWidth(JSVCall)
extern "C"  void UnityEngine_TrailRendererGenerated_TrailRenderer_endWidth_m967631567 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TrailRendererGenerated::TrailRenderer_autodestruct(JSVCall)
extern "C"  void UnityEngine_TrailRendererGenerated_TrailRenderer_autodestruct_m3669481845 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TrailRendererGenerated::TrailRenderer_Clear(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TrailRendererGenerated_TrailRenderer_Clear_m1129539294 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TrailRendererGenerated::__Register()
extern "C"  void UnityEngine_TrailRendererGenerated___Register_m1703882394 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine_TrailRendererGenerated::ilo_getSingle1(System.Int32)
extern "C"  float UnityEngine_TrailRendererGenerated_ilo_getSingle1_m368089594 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TrailRendererGenerated::ilo_setSingle2(System.Int32,System.Single)
extern "C"  void UnityEngine_TrailRendererGenerated_ilo_setSingle2_m3145071496 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TrailRendererGenerated::ilo_setBooleanS3(System.Int32,System.Boolean)
extern "C"  void UnityEngine_TrailRendererGenerated_ilo_setBooleanS3_m455331388 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TrailRendererGenerated::ilo_getBooleanS4(System.Int32)
extern "C"  bool UnityEngine_TrailRendererGenerated_ilo_getBooleanS4_m1161377186 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
