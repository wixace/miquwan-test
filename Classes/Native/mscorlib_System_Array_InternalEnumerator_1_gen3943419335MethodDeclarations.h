﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3943419335.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_866109363.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Pathfinding.Int2,System.Object>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1869853973_gshared (InternalEnumerator_1_t3943419335 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m1869853973(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t3943419335 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m1869853973_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Pathfinding.Int2,System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m382924395_gshared (InternalEnumerator_1_t3943419335 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m382924395(__this, method) ((  void (*) (InternalEnumerator_1_t3943419335 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m382924395_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Pathfinding.Int2,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2011783265_gshared (InternalEnumerator_1_t3943419335 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2011783265(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t3943419335 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2011783265_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Pathfinding.Int2,System.Object>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m789030188_gshared (InternalEnumerator_1_t3943419335 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m789030188(__this, method) ((  void (*) (InternalEnumerator_1_t3943419335 *, const MethodInfo*))InternalEnumerator_1_Dispose_m789030188_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Pathfinding.Int2,System.Object>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m4209670939_gshared (InternalEnumerator_1_t3943419335 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m4209670939(__this, method) ((  bool (*) (InternalEnumerator_1_t3943419335 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m4209670939_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Pathfinding.Int2,System.Object>>::get_Current()
extern "C"  KeyValuePair_2_t866109363  InternalEnumerator_1_get_Current_m2753523774_gshared (InternalEnumerator_1_t3943419335 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m2753523774(__this, method) ((  KeyValuePair_2_t866109363  (*) (InternalEnumerator_1_t3943419335 *, const MethodInfo*))InternalEnumerator_1_get_Current_m2753523774_gshared)(__this, method)
