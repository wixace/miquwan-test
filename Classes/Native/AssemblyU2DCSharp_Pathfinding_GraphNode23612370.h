﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_Pathfinding_Int31974045594.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.GraphNode
struct  GraphNode_t23612370  : public Il2CppObject
{
public:
	// System.Int32 Pathfinding.GraphNode::nodeIndex
	int32_t ___nodeIndex_10;
	// System.UInt32 Pathfinding.GraphNode::flags
	uint32_t ___flags_11;
	// System.UInt32 Pathfinding.GraphNode::penalty
	uint32_t ___penalty_12;
	// Pathfinding.Int3 Pathfinding.GraphNode::position
	Int3_t1974045594  ___position_13;
	// System.UInt32 Pathfinding.GraphNode::<id>k__BackingField
	uint32_t ___U3CidU3Ek__BackingField_14;

public:
	inline static int32_t get_offset_of_nodeIndex_10() { return static_cast<int32_t>(offsetof(GraphNode_t23612370, ___nodeIndex_10)); }
	inline int32_t get_nodeIndex_10() const { return ___nodeIndex_10; }
	inline int32_t* get_address_of_nodeIndex_10() { return &___nodeIndex_10; }
	inline void set_nodeIndex_10(int32_t value)
	{
		___nodeIndex_10 = value;
	}

	inline static int32_t get_offset_of_flags_11() { return static_cast<int32_t>(offsetof(GraphNode_t23612370, ___flags_11)); }
	inline uint32_t get_flags_11() const { return ___flags_11; }
	inline uint32_t* get_address_of_flags_11() { return &___flags_11; }
	inline void set_flags_11(uint32_t value)
	{
		___flags_11 = value;
	}

	inline static int32_t get_offset_of_penalty_12() { return static_cast<int32_t>(offsetof(GraphNode_t23612370, ___penalty_12)); }
	inline uint32_t get_penalty_12() const { return ___penalty_12; }
	inline uint32_t* get_address_of_penalty_12() { return &___penalty_12; }
	inline void set_penalty_12(uint32_t value)
	{
		___penalty_12 = value;
	}

	inline static int32_t get_offset_of_position_13() { return static_cast<int32_t>(offsetof(GraphNode_t23612370, ___position_13)); }
	inline Int3_t1974045594  get_position_13() const { return ___position_13; }
	inline Int3_t1974045594 * get_address_of_position_13() { return &___position_13; }
	inline void set_position_13(Int3_t1974045594  value)
	{
		___position_13 = value;
	}

	inline static int32_t get_offset_of_U3CidU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(GraphNode_t23612370, ___U3CidU3Ek__BackingField_14)); }
	inline uint32_t get_U3CidU3Ek__BackingField_14() const { return ___U3CidU3Ek__BackingField_14; }
	inline uint32_t* get_address_of_U3CidU3Ek__BackingField_14() { return &___U3CidU3Ek__BackingField_14; }
	inline void set_U3CidU3Ek__BackingField_14(uint32_t value)
	{
		___U3CidU3Ek__BackingField_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
