﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pomelo.DotNetClient.Transporter
struct Transporter_t1342168412;
// System.Net.Sockets.Socket
struct Socket_t2157335841;
// System.Action`1<System.Byte[]>
struct Action_1_t361609309;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Net_Sockets_Socket2157335841.h"
#include "AssemblyU2DCSharp_Pomelo_DotNetClient_Transporter1342168412.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void Pomelo.DotNetClient.Transporter::.ctor(System.Net.Sockets.Socket,System.Action`1<System.Byte[]>,System.UInt32)
extern "C"  void Transporter__ctor_m1916976126 (Transporter_t1342168412 * __this, Socket_t2157335841 * ___socket0, Action_1_t361609309 * ___processer1, uint32_t ___id2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.DotNetClient.Transporter::.cctor()
extern "C"  void Transporter__cctor_m644490813 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.DotNetClient.Transporter::start()
extern "C"  void Transporter_start_m643962000 (Transporter_t1342168412 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.DotNetClient.Transporter::send(System.Byte[])
extern "C"  void Transporter_send_m2770981901 (Transporter_t1342168412 * __this, ByteU5BU5D_t4260760469* ___buffer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.DotNetClient.Transporter::sendCallback(System.IAsyncResult)
extern "C"  void Transporter_sendCallback_m4260070114 (Transporter_t1342168412 * __this, Il2CppObject * ___asyncSend0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.DotNetClient.Transporter::receive()
extern "C"  void Transporter_receive_m3202346897 (Transporter_t1342168412 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.DotNetClient.Transporter::close()
extern "C"  void Transporter_close_m3407683750 (Transporter_t1342168412 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.DotNetClient.Transporter::endReceive(System.IAsyncResult)
extern "C"  void Transporter_endReceive_m2026504039 (Transporter_t1342168412 * __this, Il2CppObject * ___asyncReceive0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.DotNetClient.Transporter::processBytes(System.Byte[],System.Int32,System.Int32)
extern "C"  void Transporter_processBytes_m2709778681 (Transporter_t1342168412 * __this, ByteU5BU5D_t4260760469* ___bytes0, int32_t ___offset1, int32_t ___limit2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pomelo.DotNetClient.Transporter::readHead(System.Byte[],System.Int32,System.Int32)
extern "C"  bool Transporter_readHead_m129778963 (Transporter_t1342168412 * __this, ByteU5BU5D_t4260760469* ___bytes0, int32_t ___offset1, int32_t ___limit2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.DotNetClient.Transporter::readBody(System.Byte[],System.Int32,System.Int32)
extern "C"  void Transporter_readBody_m256766429 (Transporter_t1342168412 * __this, ByteU5BU5D_t4260760469* ___bytes0, int32_t ___offset1, int32_t ___limit2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.DotNetClient.Transporter::writeBytes(System.Byte[],System.Int32,System.Int32,System.Byte[])
extern "C"  void Transporter_writeBytes_m3703335660 (Transporter_t1342168412 * __this, ByteU5BU5D_t4260760469* ___source0, int32_t ___start1, int32_t ___length2, ByteU5BU5D_t4260760469* ___target3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.DotNetClient.Transporter::writeBytes(System.Byte[],System.Int32,System.Int32,System.Int32,System.Byte[])
extern "C"  void Transporter_writeBytes_m2298552913 (Transporter_t1342168412 * __this, ByteU5BU5D_t4260760469* ___source0, int32_t ___start1, int32_t ___length2, int32_t ___offset3, ByteU5BU5D_t4260760469* ___target4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.DotNetClient.Transporter::print(System.Byte[],System.Int32,System.Int32)
extern "C"  void Transporter_print_m582846318 (Transporter_t1342168412 * __this, ByteU5BU5D_t4260760469* ___bytes0, int32_t ___offset1, int32_t ___length2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.DotNetClient.Transporter::ilo_receive1(Pomelo.DotNetClient.Transporter)
extern "C"  void Transporter_ilo_receive1_m3016195714 (Il2CppObject * __this /* static, unused */, Transporter_t1342168412 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.DotNetClient.Transporter::ilo_LogError2(System.Object,System.Boolean)
extern "C"  void Transporter_ilo_LogError2_m553496434 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pomelo.DotNetClient.Transporter::ilo_readHead3(Pomelo.DotNetClient.Transporter,System.Byte[],System.Int32,System.Int32)
extern "C"  bool Transporter_ilo_readHead3_m2348791086 (Il2CppObject * __this /* static, unused */, Transporter_t1342168412 * ____this0, ByteU5BU5D_t4260760469* ___bytes1, int32_t ___offset2, int32_t ___limit3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.DotNetClient.Transporter::ilo_writeBytes4(Pomelo.DotNetClient.Transporter,System.Byte[],System.Int32,System.Int32,System.Int32,System.Byte[])
extern "C"  void Transporter_ilo_writeBytes4_m3264549251 (Il2CppObject * __this /* static, unused */, Transporter_t1342168412 * ____this0, ByteU5BU5D_t4260760469* ___source1, int32_t ___start2, int32_t ___length3, int32_t ___offset4, ByteU5BU5D_t4260760469* ___target5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.DotNetClient.Transporter::ilo_Log5(System.Object,System.Boolean)
extern "C"  void Transporter_ilo_Log5_m3076982497 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
