﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraSmoothFollow/<DoConfigDataCamera>c__Iterator30
struct U3CDoConfigDataCameraU3Ec__Iterator30_t923579805;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void CameraSmoothFollow/<DoConfigDataCamera>c__Iterator30::.ctor()
extern "C"  void U3CDoConfigDataCameraU3Ec__Iterator30__ctor_m1544766878 (U3CDoConfigDataCameraU3Ec__Iterator30_t923579805 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object CameraSmoothFollow/<DoConfigDataCamera>c__Iterator30::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDoConfigDataCameraU3Ec__Iterator30_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1520733822 (U3CDoConfigDataCameraU3Ec__Iterator30_t923579805 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object CameraSmoothFollow/<DoConfigDataCamera>c__Iterator30::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDoConfigDataCameraU3Ec__Iterator30_System_Collections_IEnumerator_get_Current_m1141853714 (U3CDoConfigDataCameraU3Ec__Iterator30_t923579805 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CameraSmoothFollow/<DoConfigDataCamera>c__Iterator30::MoveNext()
extern "C"  bool U3CDoConfigDataCameraU3Ec__Iterator30_MoveNext_m595947902 (U3CDoConfigDataCameraU3Ec__Iterator30_t923579805 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraSmoothFollow/<DoConfigDataCamera>c__Iterator30::Dispose()
extern "C"  void U3CDoConfigDataCameraU3Ec__Iterator30_Dispose_m2655535707 (U3CDoConfigDataCameraU3Ec__Iterator30_t923579805 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraSmoothFollow/<DoConfigDataCamera>c__Iterator30::Reset()
extern "C"  void U3CDoConfigDataCameraU3Ec__Iterator30_Reset_m3486167115 (U3CDoConfigDataCameraU3Ec__Iterator30_t923579805 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
