﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Int32>
struct Dictionary_2_t2245318082;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K2860254136.h"
#include "AssemblyU2DCSharp_Pathfinding_Int21974045593.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<Pathfinding.Int2,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m646260401_gshared (Enumerator_t2860254136 * __this, Dictionary_2_t2245318082 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m646260401(__this, ___host0, method) ((  void (*) (Enumerator_t2860254136 *, Dictionary_2_t2245318082 *, const MethodInfo*))Enumerator__ctor_m646260401_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<Pathfinding.Int2,System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m322251280_gshared (Enumerator_t2860254136 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m322251280(__this, method) ((  Il2CppObject * (*) (Enumerator_t2860254136 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m322251280_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<Pathfinding.Int2,System.Int32>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1984068964_gshared (Enumerator_t2860254136 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1984068964(__this, method) ((  void (*) (Enumerator_t2860254136 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1984068964_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<Pathfinding.Int2,System.Int32>::Dispose()
extern "C"  void Enumerator_Dispose_m2673798035_gshared (Enumerator_t2860254136 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m2673798035(__this, method) ((  void (*) (Enumerator_t2860254136 *, const MethodInfo*))Enumerator_Dispose_m2673798035_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<Pathfinding.Int2,System.Int32>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1605616720_gshared (Enumerator_t2860254136 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m1605616720(__this, method) ((  bool (*) (Enumerator_t2860254136 *, const MethodInfo*))Enumerator_MoveNext_m1605616720_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<Pathfinding.Int2,System.Int32>::get_Current()
extern "C"  Int2_t1974045593  Enumerator_get_Current_m1495312772_gshared (Enumerator_t2860254136 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m1495312772(__this, method) ((  Int2_t1974045593  (*) (Enumerator_t2860254136 *, const MethodInfo*))Enumerator_get_Current_m1495312772_gshared)(__this, method)
