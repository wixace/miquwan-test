﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_hayda45
struct  M_hayda45_t3212027806  : public Il2CppObject
{
public:
	// System.String GarbageiOS.M_hayda45::_steepeeDrasdre
	String_t* ____steepeeDrasdre_0;
	// System.Boolean GarbageiOS.M_hayda45::_behisRaircaqoo
	bool ____behisRaircaqoo_1;
	// System.Single GarbageiOS.M_hayda45::_jearpeafer
	float ____jearpeafer_2;
	// System.Single GarbageiOS.M_hayda45::_yawhalRawje
	float ____yawhalRawje_3;
	// System.Boolean GarbageiOS.M_hayda45::_xayvaijisSolene
	bool ____xayvaijisSolene_4;
	// System.Boolean GarbageiOS.M_hayda45::_musaJeri
	bool ____musaJeri_5;
	// System.UInt32 GarbageiOS.M_hayda45::_fiwou
	uint32_t ____fiwou_6;
	// System.Int32 GarbageiOS.M_hayda45::_pumouRorchis
	int32_t ____pumouRorchis_7;
	// System.Single GarbageiOS.M_hayda45::_huwaw
	float ____huwaw_8;
	// System.UInt32 GarbageiOS.M_hayda45::_becaswas
	uint32_t ____becaswas_9;
	// System.String GarbageiOS.M_hayda45::_neale
	String_t* ____neale_10;
	// System.String GarbageiOS.M_hayda45::_zarniJarjewer
	String_t* ____zarniJarjewer_11;
	// System.Boolean GarbageiOS.M_hayda45::_damajou
	bool ____damajou_12;
	// System.Int32 GarbageiOS.M_hayda45::_kupaGaciho
	int32_t ____kupaGaciho_13;
	// System.String GarbageiOS.M_hayda45::_boocarCogou
	String_t* ____boocarCogou_14;
	// System.Single GarbageiOS.M_hayda45::_tairkehe
	float ____tairkehe_15;
	// System.Single GarbageiOS.M_hayda45::_hiscudral
	float ____hiscudral_16;
	// System.Boolean GarbageiOS.M_hayda45::_chayrobe
	bool ____chayrobe_17;
	// System.Int32 GarbageiOS.M_hayda45::_benire
	int32_t ____benire_18;

public:
	inline static int32_t get_offset_of__steepeeDrasdre_0() { return static_cast<int32_t>(offsetof(M_hayda45_t3212027806, ____steepeeDrasdre_0)); }
	inline String_t* get__steepeeDrasdre_0() const { return ____steepeeDrasdre_0; }
	inline String_t** get_address_of__steepeeDrasdre_0() { return &____steepeeDrasdre_0; }
	inline void set__steepeeDrasdre_0(String_t* value)
	{
		____steepeeDrasdre_0 = value;
		Il2CppCodeGenWriteBarrier(&____steepeeDrasdre_0, value);
	}

	inline static int32_t get_offset_of__behisRaircaqoo_1() { return static_cast<int32_t>(offsetof(M_hayda45_t3212027806, ____behisRaircaqoo_1)); }
	inline bool get__behisRaircaqoo_1() const { return ____behisRaircaqoo_1; }
	inline bool* get_address_of__behisRaircaqoo_1() { return &____behisRaircaqoo_1; }
	inline void set__behisRaircaqoo_1(bool value)
	{
		____behisRaircaqoo_1 = value;
	}

	inline static int32_t get_offset_of__jearpeafer_2() { return static_cast<int32_t>(offsetof(M_hayda45_t3212027806, ____jearpeafer_2)); }
	inline float get__jearpeafer_2() const { return ____jearpeafer_2; }
	inline float* get_address_of__jearpeafer_2() { return &____jearpeafer_2; }
	inline void set__jearpeafer_2(float value)
	{
		____jearpeafer_2 = value;
	}

	inline static int32_t get_offset_of__yawhalRawje_3() { return static_cast<int32_t>(offsetof(M_hayda45_t3212027806, ____yawhalRawje_3)); }
	inline float get__yawhalRawje_3() const { return ____yawhalRawje_3; }
	inline float* get_address_of__yawhalRawje_3() { return &____yawhalRawje_3; }
	inline void set__yawhalRawje_3(float value)
	{
		____yawhalRawje_3 = value;
	}

	inline static int32_t get_offset_of__xayvaijisSolene_4() { return static_cast<int32_t>(offsetof(M_hayda45_t3212027806, ____xayvaijisSolene_4)); }
	inline bool get__xayvaijisSolene_4() const { return ____xayvaijisSolene_4; }
	inline bool* get_address_of__xayvaijisSolene_4() { return &____xayvaijisSolene_4; }
	inline void set__xayvaijisSolene_4(bool value)
	{
		____xayvaijisSolene_4 = value;
	}

	inline static int32_t get_offset_of__musaJeri_5() { return static_cast<int32_t>(offsetof(M_hayda45_t3212027806, ____musaJeri_5)); }
	inline bool get__musaJeri_5() const { return ____musaJeri_5; }
	inline bool* get_address_of__musaJeri_5() { return &____musaJeri_5; }
	inline void set__musaJeri_5(bool value)
	{
		____musaJeri_5 = value;
	}

	inline static int32_t get_offset_of__fiwou_6() { return static_cast<int32_t>(offsetof(M_hayda45_t3212027806, ____fiwou_6)); }
	inline uint32_t get__fiwou_6() const { return ____fiwou_6; }
	inline uint32_t* get_address_of__fiwou_6() { return &____fiwou_6; }
	inline void set__fiwou_6(uint32_t value)
	{
		____fiwou_6 = value;
	}

	inline static int32_t get_offset_of__pumouRorchis_7() { return static_cast<int32_t>(offsetof(M_hayda45_t3212027806, ____pumouRorchis_7)); }
	inline int32_t get__pumouRorchis_7() const { return ____pumouRorchis_7; }
	inline int32_t* get_address_of__pumouRorchis_7() { return &____pumouRorchis_7; }
	inline void set__pumouRorchis_7(int32_t value)
	{
		____pumouRorchis_7 = value;
	}

	inline static int32_t get_offset_of__huwaw_8() { return static_cast<int32_t>(offsetof(M_hayda45_t3212027806, ____huwaw_8)); }
	inline float get__huwaw_8() const { return ____huwaw_8; }
	inline float* get_address_of__huwaw_8() { return &____huwaw_8; }
	inline void set__huwaw_8(float value)
	{
		____huwaw_8 = value;
	}

	inline static int32_t get_offset_of__becaswas_9() { return static_cast<int32_t>(offsetof(M_hayda45_t3212027806, ____becaswas_9)); }
	inline uint32_t get__becaswas_9() const { return ____becaswas_9; }
	inline uint32_t* get_address_of__becaswas_9() { return &____becaswas_9; }
	inline void set__becaswas_9(uint32_t value)
	{
		____becaswas_9 = value;
	}

	inline static int32_t get_offset_of__neale_10() { return static_cast<int32_t>(offsetof(M_hayda45_t3212027806, ____neale_10)); }
	inline String_t* get__neale_10() const { return ____neale_10; }
	inline String_t** get_address_of__neale_10() { return &____neale_10; }
	inline void set__neale_10(String_t* value)
	{
		____neale_10 = value;
		Il2CppCodeGenWriteBarrier(&____neale_10, value);
	}

	inline static int32_t get_offset_of__zarniJarjewer_11() { return static_cast<int32_t>(offsetof(M_hayda45_t3212027806, ____zarniJarjewer_11)); }
	inline String_t* get__zarniJarjewer_11() const { return ____zarniJarjewer_11; }
	inline String_t** get_address_of__zarniJarjewer_11() { return &____zarniJarjewer_11; }
	inline void set__zarniJarjewer_11(String_t* value)
	{
		____zarniJarjewer_11 = value;
		Il2CppCodeGenWriteBarrier(&____zarniJarjewer_11, value);
	}

	inline static int32_t get_offset_of__damajou_12() { return static_cast<int32_t>(offsetof(M_hayda45_t3212027806, ____damajou_12)); }
	inline bool get__damajou_12() const { return ____damajou_12; }
	inline bool* get_address_of__damajou_12() { return &____damajou_12; }
	inline void set__damajou_12(bool value)
	{
		____damajou_12 = value;
	}

	inline static int32_t get_offset_of__kupaGaciho_13() { return static_cast<int32_t>(offsetof(M_hayda45_t3212027806, ____kupaGaciho_13)); }
	inline int32_t get__kupaGaciho_13() const { return ____kupaGaciho_13; }
	inline int32_t* get_address_of__kupaGaciho_13() { return &____kupaGaciho_13; }
	inline void set__kupaGaciho_13(int32_t value)
	{
		____kupaGaciho_13 = value;
	}

	inline static int32_t get_offset_of__boocarCogou_14() { return static_cast<int32_t>(offsetof(M_hayda45_t3212027806, ____boocarCogou_14)); }
	inline String_t* get__boocarCogou_14() const { return ____boocarCogou_14; }
	inline String_t** get_address_of__boocarCogou_14() { return &____boocarCogou_14; }
	inline void set__boocarCogou_14(String_t* value)
	{
		____boocarCogou_14 = value;
		Il2CppCodeGenWriteBarrier(&____boocarCogou_14, value);
	}

	inline static int32_t get_offset_of__tairkehe_15() { return static_cast<int32_t>(offsetof(M_hayda45_t3212027806, ____tairkehe_15)); }
	inline float get__tairkehe_15() const { return ____tairkehe_15; }
	inline float* get_address_of__tairkehe_15() { return &____tairkehe_15; }
	inline void set__tairkehe_15(float value)
	{
		____tairkehe_15 = value;
	}

	inline static int32_t get_offset_of__hiscudral_16() { return static_cast<int32_t>(offsetof(M_hayda45_t3212027806, ____hiscudral_16)); }
	inline float get__hiscudral_16() const { return ____hiscudral_16; }
	inline float* get_address_of__hiscudral_16() { return &____hiscudral_16; }
	inline void set__hiscudral_16(float value)
	{
		____hiscudral_16 = value;
	}

	inline static int32_t get_offset_of__chayrobe_17() { return static_cast<int32_t>(offsetof(M_hayda45_t3212027806, ____chayrobe_17)); }
	inline bool get__chayrobe_17() const { return ____chayrobe_17; }
	inline bool* get_address_of__chayrobe_17() { return &____chayrobe_17; }
	inline void set__chayrobe_17(bool value)
	{
		____chayrobe_17 = value;
	}

	inline static int32_t get_offset_of__benire_18() { return static_cast<int32_t>(offsetof(M_hayda45_t3212027806, ____benire_18)); }
	inline int32_t get__benire_18() const { return ____benire_18; }
	inline int32_t* get_address_of__benire_18() { return &____benire_18; }
	inline void set__benire_18(int32_t value)
	{
		____benire_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
