﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UITweenerGenerated/<UITweener_AddOnFinished_GetDelegate_member0_arg0>c__AnonStoreyD2
struct U3CUITweener_AddOnFinished_GetDelegate_member0_arg0U3Ec__AnonStoreyD2_t2127578046;

#include "codegen/il2cpp-codegen.h"

// System.Void UITweenerGenerated/<UITweener_AddOnFinished_GetDelegate_member0_arg0>c__AnonStoreyD2::.ctor()
extern "C"  void U3CUITweener_AddOnFinished_GetDelegate_member0_arg0U3Ec__AnonStoreyD2__ctor_m3151539357 (U3CUITweener_AddOnFinished_GetDelegate_member0_arg0U3Ec__AnonStoreyD2_t2127578046 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITweenerGenerated/<UITweener_AddOnFinished_GetDelegate_member0_arg0>c__AnonStoreyD2::<>m__169()
extern "C"  void U3CUITweener_AddOnFinished_GetDelegate_member0_arg0U3Ec__AnonStoreyD2_U3CU3Em__169_m2353851440 (U3CUITweener_AddOnFinished_GetDelegate_member0_arg0U3Ec__AnonStoreyD2_t2127578046 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
