﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._61ef6f723ac8033c282403730b4bc651
struct _61ef6f723ac8033c282403730b4bc651_t2627470060;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._61ef6f723ac8033c282403730b4bc651::.ctor()
extern "C"  void _61ef6f723ac8033c282403730b4bc651__ctor_m1430105281 (_61ef6f723ac8033c282403730b4bc651_t2627470060 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._61ef6f723ac8033c282403730b4bc651::_61ef6f723ac8033c282403730b4bc651m2(System.Int32)
extern "C"  int32_t _61ef6f723ac8033c282403730b4bc651__61ef6f723ac8033c282403730b4bc651m2_m674679321 (_61ef6f723ac8033c282403730b4bc651_t2627470060 * __this, int32_t ____61ef6f723ac8033c282403730b4bc651a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._61ef6f723ac8033c282403730b4bc651::_61ef6f723ac8033c282403730b4bc651m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _61ef6f723ac8033c282403730b4bc651__61ef6f723ac8033c282403730b4bc651m_m1314286781 (_61ef6f723ac8033c282403730b4bc651_t2627470060 * __this, int32_t ____61ef6f723ac8033c282403730b4bc651a0, int32_t ____61ef6f723ac8033c282403730b4bc651501, int32_t ____61ef6f723ac8033c282403730b4bc651c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
