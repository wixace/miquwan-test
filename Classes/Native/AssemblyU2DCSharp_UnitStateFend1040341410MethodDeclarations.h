﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnitStateFend
struct UnitStateFend_t1040341410;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// CombatEntity
struct CombatEntity_t684137495;
// AnimationRunner
struct AnimationRunner_t1015409588;
// TargetMgr
struct TargetMgr_t1188374183;
// System.Object
struct Il2CppObject;
// UnitStateBase
struct UnitStateBase_t1040218558;
// Entity.Behavior.IBehaviorCtrl
struct IBehaviorCtrl_t4225040900;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_UnitStateID1002198824.h"
#include "AssemblyU2DCSharp_UnitStateFend1040341410.h"
#include "AssemblyU2DCSharp_CombatEntity684137495.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_UnitStateBase1040218558.h"
#include "AssemblyU2DCSharp_Entity_Behavior_IBehaviorCtrl4225040900.h"
#include "AssemblyU2DCSharp_Entity_Behavior_EBehaviorID1238533136.h"

// System.Void UnitStateFend::.ctor()
extern "C"  void UnitStateFend__ctor_m1774566921 (UnitStateFend_t1040341410 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnitStateID UnitStateFend::get_type()
extern "C"  uint8_t UnitStateFend_get_type_m2189373363 (UnitStateFend_t1040341410 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnitStateFend::SetParams(System.Object[])
extern "C"  void UnitStateFend_SetParams_m4066079011 (UnitStateFend_t1040341410 * __this, ObjectU5BU5D_t1108656482* ___args0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnitStateFend::EnterState()
extern "C"  void UnitStateFend_EnterState_m3855507700 (UnitStateFend_t1040341410 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnitStateFend::DoEnter()
extern "C"  void UnitStateFend_DoEnter_m4093463412 (UnitStateFend_t1040341410 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnitStateFend::OnPathComplete()
extern "C"  void UnitStateFend_OnPathComplete_m2107766360 (UnitStateFend_t1040341410 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnitStateFend::Update(System.Single)
extern "C"  void UnitStateFend_Update_m2040481287 (UnitStateFend_t1040341410 * __this, float ___deltatime0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnitStateFend::LeaveState()
extern "C"  void UnitStateFend_LeaveState_m1346916405 (UnitStateFend_t1040341410 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnitStateFend::ilo_DoEnter1(UnitStateFend)
extern "C"  void UnitStateFend_ilo_DoEnter1_m75117374 (Il2CppObject * __this /* static, unused */, UnitStateFend_t1040341410 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnitStateFend::ilo_get_isDeath2(CombatEntity)
extern "C"  bool UnitStateFend_ilo_get_isDeath2_m1220361236 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// AnimationRunner UnitStateFend::ilo_get_characterAnim3(CombatEntity)
extern "C"  AnimationRunner_t1015409588 * UnitStateFend_ilo_get_characterAnim3_m3077514586 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnitStateFend::ilo_set_isFend4(CombatEntity,System.Boolean)
extern "C"  void UnitStateFend_ilo_set_isFend4_m3496044520 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnitStateFend::ilo_OnPathComplete5(UnitStateFend)
extern "C"  void UnitStateFend_ilo_OnPathComplete5_m3664013092 (Il2CppObject * __this /* static, unused */, UnitStateFend_t1040341410 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TargetMgr UnitStateFend::ilo_get_instance6()
extern "C"  TargetMgr_t1188374183 * UnitStateFend_ilo_get_instance6_m414373848 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnitStateFend::ilo_get_position7(CombatEntity)
extern "C"  Vector3_t4282066566  UnitStateFend_ilo_get_position7_m3252837308 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnitStateFend::ilo_get_RvoRadius8(CombatEntity)
extern "C"  float UnitStateFend_ilo_get_RvoRadius8_m3699208207 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnitStateFend::ilo_Log9(System.Object,System.Boolean)
extern "C"  void UnitStateFend_ilo_Log9_m3955321388 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnitStateFend::ilo_Update10(UnitStateBase,System.Single)
extern "C"  void UnitStateFend_ilo_Update10_m368337059 (Il2CppObject * __this /* static, unused */, UnitStateBase_t1040218558 * ____this0, float ___deltatime1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnitStateFend::ilo_TranBehavior11(Entity.Behavior.IBehaviorCtrl,Entity.Behavior.EBehaviorID,System.Object[])
extern "C"  void UnitStateFend_ilo_TranBehavior11_m4042687273 (Il2CppObject * __this /* static, unused */, IBehaviorCtrl_t4225040900 * ____this0, uint8_t ___id1, ObjectU5BU5D_t1108656482* ___arg2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnitStateFend::ilo_set_forceShift12(CombatEntity,System.Boolean)
extern "C"  void UnitStateFend_ilo_set_forceShift12_m1814847633 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
