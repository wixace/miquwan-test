﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ActorLine
struct ActorLine_t2375805801;
// UnityEngine.Transform
struct Transform_t1659122786;
// UnityEngine.LineRenderer
struct LineRenderer_t1892709339;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// EffectCtrl
struct EffectCtrl_t3708787644;
// IEffComponent
struct IEffComponent_t3802264961;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Transform1659122786.h"
#include "UnityEngine_UnityEngine_LineRenderer1892709339.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "AssemblyU2DCSharp_ActorLine2375805801.h"
#include "AssemblyU2DCSharp_IEffComponent3802264961.h"

// System.Void ActorLine::.ctor()
extern "C"  void ActorLine__ctor_m2866212898 (ActorLine_t2375805801 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ActorLine::SetLine(UnityEngine.Transform,UnityEngine.Transform,System.Single,UnityEngine.LineRenderer)
extern "C"  void ActorLine_SetLine_m1972537845 (ActorLine_t2375805801 * __this, Transform_t1659122786 * ___targetTr0, Transform_t1659122786 * ___host1, float ___lifetime2, LineRenderer_t1892709339 * ___lineRenderer3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ActorLine::Update()
extern "C"  void ActorLine_Update_m385148715 (ActorLine_t2375805801 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ActorLine::End()
extern "C"  void ActorLine_End_m292689691 (ActorLine_t2375805801 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ActorLine::Reset()
extern "C"  void ActorLine_Reset_m512645839 (ActorLine_t2375805801 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ActorLine::Init(UnityEngine.GameObject)
extern "C"  void ActorLine_Init_m3172868074 (ActorLine_t2375805801 * __this, GameObject_t3674682005 * ___effGO0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ActorLine::Play()
extern "C"  void ActorLine_Play_m796507926 (ActorLine_t2375805801 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ActorLine::Stop()
extern "C"  void ActorLine_Stop_m890191972 (ActorLine_t2375805801 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ActorLine::Clear()
extern "C"  void ActorLine_Clear_m272346189 (ActorLine_t2375805801 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ActorLine::ilo_End1(ActorLine)
extern "C"  void ActorLine_ilo_End1_m305143486 (Il2CppObject * __this /* static, unused */, ActorLine_t2375805801 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// EffectCtrl ActorLine::ilo_get_effCtrl2(IEffComponent)
extern "C"  EffectCtrl_t3708787644 * ActorLine_ilo_get_effCtrl2_m802770 (Il2CppObject * __this /* static, unused */, IEffComponent_t3802264961 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
