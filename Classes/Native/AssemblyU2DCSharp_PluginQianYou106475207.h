﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// Mihua.SDK.PayInfo
struct PayInfo_t1775308120;
// System.Action
struct Action_t3771233898;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PluginQianYou
struct  PluginQianYou_t106475207  : public MonoBehaviour_t667441552
{
public:
	// System.Int32 PluginQianYou::statusCode
	int32_t ___statusCode_2;
	// System.String PluginQianYou::userId
	String_t* ___userId_3;
	// System.String PluginQianYou::platformUserId
	String_t* ___platformUserId_4;
	// System.String PluginQianYou::platformId
	String_t* ___platformId_5;
	// System.String PluginQianYou::desc
	String_t* ___desc_6;
	// System.String PluginQianYou::timestamp
	String_t* ___timestamp_7;
	// System.String PluginQianYou::sign
	String_t* ___sign_8;
	// System.Boolean PluginQianYou::isOut
	bool ___isOut_9;
	// System.String PluginQianYou::configId
	String_t* ___configId_10;
	// System.String PluginQianYou::loginVerifyParm
	String_t* ___loginVerifyParm_11;
	// System.String PluginQianYou::userName
	String_t* ___userName_12;
	// System.String PluginQianYou::token
	String_t* ___token_13;
	// Mihua.SDK.PayInfo PluginQianYou::payInfo
	PayInfo_t1775308120 * ___payInfo_14;

public:
	inline static int32_t get_offset_of_statusCode_2() { return static_cast<int32_t>(offsetof(PluginQianYou_t106475207, ___statusCode_2)); }
	inline int32_t get_statusCode_2() const { return ___statusCode_2; }
	inline int32_t* get_address_of_statusCode_2() { return &___statusCode_2; }
	inline void set_statusCode_2(int32_t value)
	{
		___statusCode_2 = value;
	}

	inline static int32_t get_offset_of_userId_3() { return static_cast<int32_t>(offsetof(PluginQianYou_t106475207, ___userId_3)); }
	inline String_t* get_userId_3() const { return ___userId_3; }
	inline String_t** get_address_of_userId_3() { return &___userId_3; }
	inline void set_userId_3(String_t* value)
	{
		___userId_3 = value;
		Il2CppCodeGenWriteBarrier(&___userId_3, value);
	}

	inline static int32_t get_offset_of_platformUserId_4() { return static_cast<int32_t>(offsetof(PluginQianYou_t106475207, ___platformUserId_4)); }
	inline String_t* get_platformUserId_4() const { return ___platformUserId_4; }
	inline String_t** get_address_of_platformUserId_4() { return &___platformUserId_4; }
	inline void set_platformUserId_4(String_t* value)
	{
		___platformUserId_4 = value;
		Il2CppCodeGenWriteBarrier(&___platformUserId_4, value);
	}

	inline static int32_t get_offset_of_platformId_5() { return static_cast<int32_t>(offsetof(PluginQianYou_t106475207, ___platformId_5)); }
	inline String_t* get_platformId_5() const { return ___platformId_5; }
	inline String_t** get_address_of_platformId_5() { return &___platformId_5; }
	inline void set_platformId_5(String_t* value)
	{
		___platformId_5 = value;
		Il2CppCodeGenWriteBarrier(&___platformId_5, value);
	}

	inline static int32_t get_offset_of_desc_6() { return static_cast<int32_t>(offsetof(PluginQianYou_t106475207, ___desc_6)); }
	inline String_t* get_desc_6() const { return ___desc_6; }
	inline String_t** get_address_of_desc_6() { return &___desc_6; }
	inline void set_desc_6(String_t* value)
	{
		___desc_6 = value;
		Il2CppCodeGenWriteBarrier(&___desc_6, value);
	}

	inline static int32_t get_offset_of_timestamp_7() { return static_cast<int32_t>(offsetof(PluginQianYou_t106475207, ___timestamp_7)); }
	inline String_t* get_timestamp_7() const { return ___timestamp_7; }
	inline String_t** get_address_of_timestamp_7() { return &___timestamp_7; }
	inline void set_timestamp_7(String_t* value)
	{
		___timestamp_7 = value;
		Il2CppCodeGenWriteBarrier(&___timestamp_7, value);
	}

	inline static int32_t get_offset_of_sign_8() { return static_cast<int32_t>(offsetof(PluginQianYou_t106475207, ___sign_8)); }
	inline String_t* get_sign_8() const { return ___sign_8; }
	inline String_t** get_address_of_sign_8() { return &___sign_8; }
	inline void set_sign_8(String_t* value)
	{
		___sign_8 = value;
		Il2CppCodeGenWriteBarrier(&___sign_8, value);
	}

	inline static int32_t get_offset_of_isOut_9() { return static_cast<int32_t>(offsetof(PluginQianYou_t106475207, ___isOut_9)); }
	inline bool get_isOut_9() const { return ___isOut_9; }
	inline bool* get_address_of_isOut_9() { return &___isOut_9; }
	inline void set_isOut_9(bool value)
	{
		___isOut_9 = value;
	}

	inline static int32_t get_offset_of_configId_10() { return static_cast<int32_t>(offsetof(PluginQianYou_t106475207, ___configId_10)); }
	inline String_t* get_configId_10() const { return ___configId_10; }
	inline String_t** get_address_of_configId_10() { return &___configId_10; }
	inline void set_configId_10(String_t* value)
	{
		___configId_10 = value;
		Il2CppCodeGenWriteBarrier(&___configId_10, value);
	}

	inline static int32_t get_offset_of_loginVerifyParm_11() { return static_cast<int32_t>(offsetof(PluginQianYou_t106475207, ___loginVerifyParm_11)); }
	inline String_t* get_loginVerifyParm_11() const { return ___loginVerifyParm_11; }
	inline String_t** get_address_of_loginVerifyParm_11() { return &___loginVerifyParm_11; }
	inline void set_loginVerifyParm_11(String_t* value)
	{
		___loginVerifyParm_11 = value;
		Il2CppCodeGenWriteBarrier(&___loginVerifyParm_11, value);
	}

	inline static int32_t get_offset_of_userName_12() { return static_cast<int32_t>(offsetof(PluginQianYou_t106475207, ___userName_12)); }
	inline String_t* get_userName_12() const { return ___userName_12; }
	inline String_t** get_address_of_userName_12() { return &___userName_12; }
	inline void set_userName_12(String_t* value)
	{
		___userName_12 = value;
		Il2CppCodeGenWriteBarrier(&___userName_12, value);
	}

	inline static int32_t get_offset_of_token_13() { return static_cast<int32_t>(offsetof(PluginQianYou_t106475207, ___token_13)); }
	inline String_t* get_token_13() const { return ___token_13; }
	inline String_t** get_address_of_token_13() { return &___token_13; }
	inline void set_token_13(String_t* value)
	{
		___token_13 = value;
		Il2CppCodeGenWriteBarrier(&___token_13, value);
	}

	inline static int32_t get_offset_of_payInfo_14() { return static_cast<int32_t>(offsetof(PluginQianYou_t106475207, ___payInfo_14)); }
	inline PayInfo_t1775308120 * get_payInfo_14() const { return ___payInfo_14; }
	inline PayInfo_t1775308120 ** get_address_of_payInfo_14() { return &___payInfo_14; }
	inline void set_payInfo_14(PayInfo_t1775308120 * value)
	{
		___payInfo_14 = value;
		Il2CppCodeGenWriteBarrier(&___payInfo_14, value);
	}
};

struct PluginQianYou_t106475207_StaticFields
{
public:
	// System.Action PluginQianYou::<>f__am$cacheD
	Action_t3771233898 * ___U3CU3Ef__amU24cacheD_15;
	// System.Action PluginQianYou::<>f__am$cacheE
	Action_t3771233898 * ___U3CU3Ef__amU24cacheE_16;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheD_15() { return static_cast<int32_t>(offsetof(PluginQianYou_t106475207_StaticFields, ___U3CU3Ef__amU24cacheD_15)); }
	inline Action_t3771233898 * get_U3CU3Ef__amU24cacheD_15() const { return ___U3CU3Ef__amU24cacheD_15; }
	inline Action_t3771233898 ** get_address_of_U3CU3Ef__amU24cacheD_15() { return &___U3CU3Ef__amU24cacheD_15; }
	inline void set_U3CU3Ef__amU24cacheD_15(Action_t3771233898 * value)
	{
		___U3CU3Ef__amU24cacheD_15 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheD_15, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheE_16() { return static_cast<int32_t>(offsetof(PluginQianYou_t106475207_StaticFields, ___U3CU3Ef__amU24cacheE_16)); }
	inline Action_t3771233898 * get_U3CU3Ef__amU24cacheE_16() const { return ___U3CU3Ef__amU24cacheE_16; }
	inline Action_t3771233898 ** get_address_of_U3CU3Ef__amU24cacheE_16() { return &___U3CU3Ef__amU24cacheE_16; }
	inline void set_U3CU3Ef__amU24cacheE_16(Action_t3771233898 * value)
	{
		___U3CU3Ef__amU24cacheE_16 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheE_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
