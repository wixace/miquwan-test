﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Xml.DTDAttributeDefinition
struct DTDAttributeDefinition_t3691706913;
// System.String
struct String_t;
// System.Xml.Schema.XmlSchemaDatatype
struct XmlSchemaDatatype_t196391954;
// System.Collections.ArrayList
struct ArrayList_t3948406897;

#include "codegen/il2cpp-codegen.h"
#include "System_Xml_Mono_Xml_DTDAttributeOccurenceType1072212747.h"

// System.String Mono.Xml.DTDAttributeDefinition::get_Name()
extern "C"  String_t* DTDAttributeDefinition_get_Name_m1998089073 (DTDAttributeDefinition_t3691706913 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.Schema.XmlSchemaDatatype Mono.Xml.DTDAttributeDefinition::get_Datatype()
extern "C"  XmlSchemaDatatype_t196391954 * DTDAttributeDefinition_get_Datatype_m2869597951 (DTDAttributeDefinition_t3691706913 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Xml.DTDAttributeOccurenceType Mono.Xml.DTDAttributeDefinition::get_OccurenceType()
extern "C"  int32_t DTDAttributeDefinition_get_OccurenceType_m2929132692 (DTDAttributeDefinition_t3691706913 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.ArrayList Mono.Xml.DTDAttributeDefinition::get_EnumeratedAttributeDeclaration()
extern "C"  ArrayList_t3948406897 * DTDAttributeDefinition_get_EnumeratedAttributeDeclaration_m2704986805 (DTDAttributeDefinition_t3691706913 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.ArrayList Mono.Xml.DTDAttributeDefinition::get_EnumeratedNotations()
extern "C"  ArrayList_t3948406897 * DTDAttributeDefinition_get_EnumeratedNotations_m150762300 (DTDAttributeDefinition_t3691706913 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Xml.DTDAttributeDefinition::get_DefaultValue()
extern "C"  String_t* DTDAttributeDefinition_get_DefaultValue_m347427414 (DTDAttributeDefinition_t3691706913 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Xml.DTDAttributeDefinition::get_UnresolvedDefaultValue()
extern "C"  String_t* DTDAttributeDefinition_get_UnresolvedDefaultValue_m3220743175 (DTDAttributeDefinition_t3691706913 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Xml.DTDAttributeDefinition::ComputeDefaultValue()
extern "C"  String_t* DTDAttributeDefinition_ComputeDefaultValue_m3929866668 (DTDAttributeDefinition_t3691706913 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
