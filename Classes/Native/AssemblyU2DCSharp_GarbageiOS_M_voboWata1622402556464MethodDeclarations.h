﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_voboWata162
struct M_voboWata162_t2402556464;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_voboWata1622402556464.h"

// System.Void GarbageiOS.M_voboWata162::.ctor()
extern "C"  void M_voboWata162__ctor_m2774062899 (M_voboWata162_t2402556464 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_voboWata162::M_chousurstereTarjuxa0(System.String[],System.Int32)
extern "C"  void M_voboWata162_M_chousurstereTarjuxa0_m797129379 (M_voboWata162_t2402556464 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_voboWata162::M_jaicaMearis1(System.String[],System.Int32)
extern "C"  void M_voboWata162_M_jaicaMearis1_m3494122952 (M_voboWata162_t2402556464 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_voboWata162::ilo_M_chousurstereTarjuxa01(GarbageiOS.M_voboWata162,System.String[],System.Int32)
extern "C"  void M_voboWata162_ilo_M_chousurstereTarjuxa01_m2593384337 (Il2CppObject * __this /* static, unused */, M_voboWata162_t2402556464 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
