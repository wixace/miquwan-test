﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_AnimatorClipInfoGenerated
struct UnityEngine_AnimatorClipInfoGenerated_t2406638210;
// JSVCall
struct JSVCall_t3708497963;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"

// System.Void UnityEngine_AnimatorClipInfoGenerated::.ctor()
extern "C"  void UnityEngine_AnimatorClipInfoGenerated__ctor_m299858217 (UnityEngine_AnimatorClipInfoGenerated_t2406638210 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimatorClipInfoGenerated::.cctor()
extern "C"  void UnityEngine_AnimatorClipInfoGenerated__cctor_m223573924 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimatorClipInfoGenerated::AnimatorClipInfo_AnimatorClipInfo1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimatorClipInfoGenerated_AnimatorClipInfo_AnimatorClipInfo1_m1609848009 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimatorClipInfoGenerated::AnimatorClipInfo_clip(JSVCall)
extern "C"  void UnityEngine_AnimatorClipInfoGenerated_AnimatorClipInfo_clip_m2974258582 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimatorClipInfoGenerated::AnimatorClipInfo_weight(JSVCall)
extern "C"  void UnityEngine_AnimatorClipInfoGenerated_AnimatorClipInfo_weight_m3507661678 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimatorClipInfoGenerated::__Register()
extern "C"  void UnityEngine_AnimatorClipInfoGenerated___Register_m1736201534 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimatorClipInfoGenerated::ilo_attachFinalizerObject1(System.Int32)
extern "C"  bool UnityEngine_AnimatorClipInfoGenerated_ilo_attachFinalizerObject1_m808441070 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
