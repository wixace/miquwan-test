﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.UInt32[]
struct UInt32U5BU5D_t3230734560;

#include "mscorlib_System_ValueType1744280289.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SevenZip.Compression.RangeCoder.BitEncoder
struct  BitEncoder_t1052492065 
{
public:
	// System.UInt32 SevenZip.Compression.RangeCoder.BitEncoder::Prob
	uint32_t ___Prob_5;

public:
	inline static int32_t get_offset_of_Prob_5() { return static_cast<int32_t>(offsetof(BitEncoder_t1052492065, ___Prob_5)); }
	inline uint32_t get_Prob_5() const { return ___Prob_5; }
	inline uint32_t* get_address_of_Prob_5() { return &___Prob_5; }
	inline void set_Prob_5(uint32_t value)
	{
		___Prob_5 = value;
	}
};

struct BitEncoder_t1052492065_StaticFields
{
public:
	// System.UInt32[] SevenZip.Compression.RangeCoder.BitEncoder::ProbPrices
	UInt32U5BU5D_t3230734560* ___ProbPrices_6;

public:
	inline static int32_t get_offset_of_ProbPrices_6() { return static_cast<int32_t>(offsetof(BitEncoder_t1052492065_StaticFields, ___ProbPrices_6)); }
	inline UInt32U5BU5D_t3230734560* get_ProbPrices_6() const { return ___ProbPrices_6; }
	inline UInt32U5BU5D_t3230734560** get_address_of_ProbPrices_6() { return &___ProbPrices_6; }
	inline void set_ProbPrices_6(UInt32U5BU5D_t3230734560* value)
	{
		___ProbPrices_6 = value;
		Il2CppCodeGenWriteBarrier(&___ProbPrices_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for marshalling of: SevenZip.Compression.RangeCoder.BitEncoder
struct BitEncoder_t1052492065_marshaled_pinvoke
{
	uint32_t ___Prob_5;
};
// Native definition for marshalling of: SevenZip.Compression.RangeCoder.BitEncoder
struct BitEncoder_t1052492065_marshaled_com
{
	uint32_t ___Prob_5;
};
