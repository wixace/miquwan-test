﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Rendering.ReflectionProbeBlendInfo
struct ReflectionProbeBlendInfo_t1080597738;
struct ReflectionProbeBlendInfo_t1080597738_marshaled_pinvoke;
struct ReflectionProbeBlendInfo_t1080597738_marshaled_com;

#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct ReflectionProbeBlendInfo_t1080597738;
struct ReflectionProbeBlendInfo_t1080597738_marshaled_pinvoke;

extern "C" void ReflectionProbeBlendInfo_t1080597738_marshal_pinvoke(const ReflectionProbeBlendInfo_t1080597738& unmarshaled, ReflectionProbeBlendInfo_t1080597738_marshaled_pinvoke& marshaled);
extern "C" void ReflectionProbeBlendInfo_t1080597738_marshal_pinvoke_back(const ReflectionProbeBlendInfo_t1080597738_marshaled_pinvoke& marshaled, ReflectionProbeBlendInfo_t1080597738& unmarshaled);
extern "C" void ReflectionProbeBlendInfo_t1080597738_marshal_pinvoke_cleanup(ReflectionProbeBlendInfo_t1080597738_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct ReflectionProbeBlendInfo_t1080597738;
struct ReflectionProbeBlendInfo_t1080597738_marshaled_com;

extern "C" void ReflectionProbeBlendInfo_t1080597738_marshal_com(const ReflectionProbeBlendInfo_t1080597738& unmarshaled, ReflectionProbeBlendInfo_t1080597738_marshaled_com& marshaled);
extern "C" void ReflectionProbeBlendInfo_t1080597738_marshal_com_back(const ReflectionProbeBlendInfo_t1080597738_marshaled_com& marshaled, ReflectionProbeBlendInfo_t1080597738& unmarshaled);
extern "C" void ReflectionProbeBlendInfo_t1080597738_marshal_com_cleanup(ReflectionProbeBlendInfo_t1080597738_marshaled_com& marshaled);
