﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Comparison`1<UnityEngine.Bounds>
struct Comparison_1_t1428003036;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "UnityEngine_UnityEngine_Bounds2711641849.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void System.Comparison`1<UnityEngine.Bounds>::.ctor(System.Object,System.IntPtr)
extern "C"  void Comparison_1__ctor_m2722416893_gshared (Comparison_1_t1428003036 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define Comparison_1__ctor_m2722416893(__this, ___object0, ___method1, method) ((  void (*) (Comparison_1_t1428003036 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Comparison_1__ctor_m2722416893_gshared)(__this, ___object0, ___method1, method)
// System.Int32 System.Comparison`1<UnityEngine.Bounds>::Invoke(T,T)
extern "C"  int32_t Comparison_1_Invoke_m3526839963_gshared (Comparison_1_t1428003036 * __this, Bounds_t2711641849  ___x0, Bounds_t2711641849  ___y1, const MethodInfo* method);
#define Comparison_1_Invoke_m3526839963(__this, ___x0, ___y1, method) ((  int32_t (*) (Comparison_1_t1428003036 *, Bounds_t2711641849 , Bounds_t2711641849 , const MethodInfo*))Comparison_1_Invoke_m3526839963_gshared)(__this, ___x0, ___y1, method)
// System.IAsyncResult System.Comparison`1<UnityEngine.Bounds>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Comparison_1_BeginInvoke_m121442020_gshared (Comparison_1_t1428003036 * __this, Bounds_t2711641849  ___x0, Bounds_t2711641849  ___y1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method);
#define Comparison_1_BeginInvoke_m121442020(__this, ___x0, ___y1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Comparison_1_t1428003036 *, Bounds_t2711641849 , Bounds_t2711641849 , AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))Comparison_1_BeginInvoke_m121442020_gshared)(__this, ___x0, ___y1, ___callback2, ___object3, method)
// System.Int32 System.Comparison`1<UnityEngine.Bounds>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t Comparison_1_EndInvoke_m4208848369_gshared (Comparison_1_t1428003036 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define Comparison_1_EndInvoke_m4208848369(__this, ___result0, method) ((  int32_t (*) (Comparison_1_t1428003036 *, Il2CppObject *, const MethodInfo*))Comparison_1_EndInvoke_m4208848369_gshared)(__this, ___result0, method)
