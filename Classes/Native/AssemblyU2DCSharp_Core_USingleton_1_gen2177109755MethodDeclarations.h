﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Core.USingleton`1<System.Object>
struct USingleton_1_t2177109755;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void Core.USingleton`1<System.Object>::.ctor()
extern "C"  void USingleton_1__ctor_m3702815765_gshared (USingleton_1_t2177109755 * __this, const MethodInfo* method);
#define USingleton_1__ctor_m3702815765(__this, method) ((  void (*) (USingleton_1_t2177109755 *, const MethodInfo*))USingleton_1__ctor_m3702815765_gshared)(__this, method)
// System.Void Core.USingleton`1<System.Object>::.cctor()
extern "C"  void USingleton_1__cctor_m2636042808_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define USingleton_1__cctor_m2636042808(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))USingleton_1__cctor_m2636042808_gshared)(__this /* static, unused */, method)
// T Core.USingleton`1<System.Object>::get_Instance()
extern "C"  Il2CppObject * USingleton_1_get_Instance_m899663340_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define USingleton_1_get_Instance_m899663340(__this /* static, unused */, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))USingleton_1_get_Instance_m899663340_gshared)(__this /* static, unused */, method)
// System.Void Core.USingleton`1<System.Object>::set_Instance(T)
extern "C"  void USingleton_1_set_Instance_m1722964381_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, const MethodInfo* method);
#define USingleton_1_set_Instance_m1722964381(__this /* static, unused */, ___value0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))USingleton_1_set_Instance_m1722964381_gshared)(__this /* static, unused */, ___value0, method)
