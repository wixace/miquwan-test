﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.HumanLimit
struct HumanLimit_t3300934482;
struct HumanLimit_t3300934482_marshaled_pinvoke;
struct HumanLimit_t3300934482_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_HumanLimit3300934482.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Boolean UnityEngine.HumanLimit::get_useDefaultValues()
extern "C"  bool HumanLimit_get_useDefaultValues_m4021590672 (HumanLimit_t3300934482 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.HumanLimit::set_useDefaultValues(System.Boolean)
extern "C"  void HumanLimit_set_useDefaultValues_m3155791509 (HumanLimit_t3300934482 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.HumanLimit::get_min()
extern "C"  Vector3_t4282066566  HumanLimit_get_min_m3303484108 (HumanLimit_t3300934482 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.HumanLimit::set_min(UnityEngine.Vector3)
extern "C"  void HumanLimit_set_min_m1219726471 (HumanLimit_t3300934482 * __this, Vector3_t4282066566  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.HumanLimit::get_max()
extern "C"  Vector3_t4282066566  HumanLimit_get_max_m3303255390 (HumanLimit_t3300934482 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.HumanLimit::set_max(UnityEngine.Vector3)
extern "C"  void HumanLimit_set_max_m945818805 (HumanLimit_t3300934482 * __this, Vector3_t4282066566  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.HumanLimit::get_center()
extern "C"  Vector3_t4282066566  HumanLimit_get_center_m4078212477 (HumanLimit_t3300934482 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.HumanLimit::set_center(UnityEngine.Vector3)
extern "C"  void HumanLimit_set_center_m3733035522 (HumanLimit_t3300934482 * __this, Vector3_t4282066566  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.HumanLimit::get_axisLength()
extern "C"  float HumanLimit_get_axisLength_m3879884193 (HumanLimit_t3300934482 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.HumanLimit::set_axisLength(System.Single)
extern "C"  void HumanLimit_set_axisLength_m3092212546 (HumanLimit_t3300934482 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct HumanLimit_t3300934482;
struct HumanLimit_t3300934482_marshaled_pinvoke;

extern "C" void HumanLimit_t3300934482_marshal_pinvoke(const HumanLimit_t3300934482& unmarshaled, HumanLimit_t3300934482_marshaled_pinvoke& marshaled);
extern "C" void HumanLimit_t3300934482_marshal_pinvoke_back(const HumanLimit_t3300934482_marshaled_pinvoke& marshaled, HumanLimit_t3300934482& unmarshaled);
extern "C" void HumanLimit_t3300934482_marshal_pinvoke_cleanup(HumanLimit_t3300934482_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct HumanLimit_t3300934482;
struct HumanLimit_t3300934482_marshaled_com;

extern "C" void HumanLimit_t3300934482_marshal_com(const HumanLimit_t3300934482& unmarshaled, HumanLimit_t3300934482_marshaled_com& marshaled);
extern "C" void HumanLimit_t3300934482_marshal_com_back(const HumanLimit_t3300934482_marshaled_com& marshaled, HumanLimit_t3300934482& unmarshaled);
extern "C" void HumanLimit_t3300934482_marshal_com_cleanup(HumanLimit_t3300934482_marshaled_com& marshaled);
