﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mihua.Assets.SubAssetMgr
struct SubAssetMgr_t3564963414;
// System.String
struct String_t;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1375417109;
// FileInfoRes
struct FileInfoRes_t1217059798;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// LoadingBoardMgr
struct LoadingBoardMgr_t303632014;
// Mihua.Net.UrlLoader
struct UrlLoader_t2490729496;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "AssemblyU2DCSharp_FileInfoRes1217059798.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_Mihua_Assets_SubAssetMgr_ErrorIn2633981210.h"
#include "AssemblyU2DCSharp_Mihua_Assets_SubAssetMgr3564963414.h"
#include "AssemblyU2DCSharp_Mihua_Net_UrlLoader2490729496.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_LoadingBoardMgr303632014.h"

// System.Void Mihua.Assets.SubAssetMgr::.ctor()
extern "C"  void SubAssetMgr__ctor_m675084936 (SubAssetMgr_t3564963414 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mihua.Assets.SubAssetMgr::get_isForce()
extern "C"  bool SubAssetMgr_get_isForce_m2450598090 (SubAssetMgr_t3564963414 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua.Assets.SubAssetMgr::set_isForce(System.Boolean)
extern "C"  void SubAssetMgr_set_isForce_m910468097 (SubAssetMgr_t3564963414 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mihua.Assets.SubAssetMgr::get_isStart()
extern "C"  bool SubAssetMgr_get_isStart_m1231151233 (SubAssetMgr_t3564963414 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua.Assets.SubAssetMgr::set_isStart(System.Boolean)
extern "C"  void SubAssetMgr_set_isStart_m4133621624 (SubAssetMgr_t3564963414 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mihua.Assets.SubAssetMgr::get_TotalSize()
extern "C"  String_t* SubAssetMgr_get_TotalSize_m3144116383 (SubAssetMgr_t3564963414 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mihua.Assets.SubAssetMgr::get_isNeedDown()
extern "C"  bool SubAssetMgr_get_isNeedDown_m2360180987 (SubAssetMgr_t3564963414 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mihua.Assets.SubAssetMgr::get_isShowDialog()
extern "C"  bool SubAssetMgr_get_isShowDialog_m629091720 (SubAssetMgr_t3564963414 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua.Assets.SubAssetMgr::OnEnterLoginScene(CEvent.ZEvent)
extern "C"  void SubAssetMgr_OnEnterLoginScene_m841747161 (SubAssetMgr_t3564963414 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua.Assets.SubAssetMgr::OnStartLoadScene(CEvent.ZEvent)
extern "C"  void SubAssetMgr_OnStartLoadScene_m2162336156 (SubAssetMgr_t3564963414 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua.Assets.SubAssetMgr::OnEndLoadScene(CEvent.ZEvent)
extern "C"  void SubAssetMgr_OnEndLoadScene_m3992294549 (SubAssetMgr_t3564963414 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua.Assets.SubAssetMgr::Init()
extern "C"  void SubAssetMgr_Init_m1774428876 (SubAssetMgr_t3564963414 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua.Assets.SubAssetMgr::SetForceDown(System.Boolean)
extern "C"  void SubAssetMgr_SetForceDown_m211895582 (SubAssetMgr_t3564963414 * __this, bool ____isForce0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua.Assets.SubAssetMgr::ShowDownassetDialog()
extern "C"  void SubAssetMgr_ShowDownassetDialog_m1178244703 (SubAssetMgr_t3564963414 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua.Assets.SubAssetMgr::ShowDownassetDialog(System.Boolean)
extern "C"  void SubAssetMgr_ShowDownassetDialog_m3285137814 (SubAssetMgr_t3564963414 * __this, bool ___isInit0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua.Assets.SubAssetMgr::StartDown()
extern "C"  void SubAssetMgr_StartDown_m114321482 (SubAssetMgr_t3564963414 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua.Assets.SubAssetMgr::Stop()
extern "C"  void SubAssetMgr_Stop_m2066436414 (SubAssetMgr_t3564963414 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mihua.Assets.SubAssetMgr::get_isPause()
extern "C"  bool SubAssetMgr_get_isPause_m2338139413 (SubAssetMgr_t3564963414 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua.Assets.SubAssetMgr::set_isPause(System.Boolean)
extern "C"  void SubAssetMgr_set_isPause_m3145845004 (SubAssetMgr_t3564963414 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Mihua.Assets.SubAssetMgr::get_Progress()
extern "C"  float SubAssetMgr_get_Progress_m4253591294 (SubAssetMgr_t3564963414 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mihua.Assets.SubAssetMgr::get_ProgressString()
extern "C"  String_t* SubAssetMgr_get_ProgressString_m2510509638 (SubAssetMgr_t3564963414 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua.Assets.SubAssetMgr::Clear()
extern "C"  void SubAssetMgr_Clear_m2376185523 (SubAssetMgr_t3564963414 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua.Assets.SubAssetMgr::PriorityDown(System.Collections.Generic.List`1<System.String>)
extern "C"  void SubAssetMgr_PriorityDown_m594770390 (SubAssetMgr_t3564963414 * __this, List_1_t1375417109 * ___assetList0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mihua.Assets.SubAssetMgr::SortFun(FileInfoRes,FileInfoRes)
extern "C"  int32_t SubAssetMgr_SortFun_m2430894841 (SubAssetMgr_t3564963414 * __this, FileInfoRes_t1217059798 * ___a0, FileInfoRes_t1217059798 * ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mihua.Assets.SubAssetMgr::GetPriorityIndex(System.String)
extern "C"  int32_t SubAssetMgr_GetPriorityIndex_m2457932832 (SubAssetMgr_t3564963414 * __this, String_t* ___assetName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mihua.Assets.SubAssetMgr::DownNext()
extern "C"  bool SubAssetMgr_DownNext_m1783577189 (SubAssetMgr_t3564963414 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua.Assets.SubAssetMgr::ZUpdate()
extern "C"  void SubAssetMgr_ZUpdate_m4080058249 (SubAssetMgr_t3564963414 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mihua.Assets.SubAssetMgr/ErrorInfo Mihua.Assets.SubAssetMgr::GetError(System.String)
extern "C"  ErrorInfo_t2633981210  SubAssetMgr_GetError_m3193974129 (SubAssetMgr_t3564963414 * __this, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua.Assets.SubAssetMgr::RemoveError(System.String)
extern "C"  void SubAssetMgr_RemoveError_m2673521784 (SubAssetMgr_t3564963414 * __this, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua.Assets.SubAssetMgr::SaveBytes(System.Byte[],System.String)
extern "C"  void SubAssetMgr_SaveBytes_m4216021297 (SubAssetMgr_t3564963414 * __this, ByteU5BU5D_t4260760469* ___bytes0, String_t* ___assetName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua.Assets.SubAssetMgr::OnUnzip(System.Byte[],System.String)
extern "C"  void SubAssetMgr_OnUnzip_m1717837686 (SubAssetMgr_t3564963414 * __this, ByteU5BU5D_t4260760469* ___bytes0, String_t* ___assetName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua.Assets.SubAssetMgr::SaveBytesToFile(System.Byte[],System.String)
extern "C"  void SubAssetMgr_SaveBytesToFile_m1381188634 (SubAssetMgr_t3564963414 * __this, ByteU5BU5D_t4260760469* ___bytes0, String_t* ___assetName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua.Assets.SubAssetMgr::ilo_set_isPause1(Mihua.Assets.SubAssetMgr,System.Boolean)
extern "C"  void SubAssetMgr_ilo_set_isPause1_m4105532223 (Il2CppObject * __this /* static, unused */, SubAssetMgr_t3564963414 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua.Assets.SubAssetMgr::ilo_ShowDownassetDialog2(Mihua.Assets.SubAssetMgr,System.Boolean)
extern "C"  void SubAssetMgr_ilo_ShowDownassetDialog2_m2979373928 (Il2CppObject * __this /* static, unused */, SubAssetMgr_t3564963414 * ____this0, bool ___isInit1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua.Assets.SubAssetMgr::ilo_set_isForce3(Mihua.Assets.SubAssetMgr,System.Boolean)
extern "C"  void SubAssetMgr_ilo_set_isForce3_m2459648050 (Il2CppObject * __this /* static, unused */, SubAssetMgr_t3564963414 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mihua.Assets.SubAssetMgr::ilo_get_isForce4(Mihua.Assets.SubAssetMgr)
extern "C"  bool SubAssetMgr_ilo_get_isForce4_m1326935532 (Il2CppObject * __this /* static, unused */, SubAssetMgr_t3564963414 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// LoadingBoardMgr Mihua.Assets.SubAssetMgr::ilo_get_Instance5()
extern "C"  LoadingBoardMgr_t303632014 * SubAssetMgr_ilo_get_Instance5_m2878030661 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mihua.Assets.SubAssetMgr::ilo_get_isStart6(Mihua.Assets.SubAssetMgr)
extern "C"  bool SubAssetMgr_ilo_get_isStart6_m2435785623 (Il2CppObject * __this /* static, unused */, SubAssetMgr_t3564963414 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua.Assets.SubAssetMgr::ilo_set_isStart7(Mihua.Assets.SubAssetMgr,System.Boolean)
extern "C"  void SubAssetMgr_ilo_set_isStart7_m3703343461 (Il2CppObject * __this /* static, unused */, SubAssetMgr_t3564963414 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mihua.Assets.SubAssetMgr::ilo_DownNext8(Mihua.Assets.SubAssetMgr)
extern "C"  bool SubAssetMgr_ilo_DownNext8_m2965528495 (Il2CppObject * __this /* static, unused */, SubAssetMgr_t3564963414 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua.Assets.SubAssetMgr::ilo_DispatchEvent9(CEvent.ZEvent)
extern "C"  void SubAssetMgr_ilo_DispatchEvent9_m3989865235 (Il2CppObject * __this /* static, unused */, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mihua.Assets.SubAssetMgr::ilo_GetPriorityIndex10(Mihua.Assets.SubAssetMgr,System.String)
extern "C"  int32_t SubAssetMgr_ilo_GetPriorityIndex10_m2258801885 (Il2CppObject * __this /* static, unused */, SubAssetMgr_t3564963414 * ____this0, String_t* ___assetName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mihua.Assets.SubAssetMgr::ilo_ServerPath11(System.String)
extern "C"  String_t* SubAssetMgr_ilo_ServerPath11_m989565256 (Il2CppObject * __this /* static, unused */, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mihua.Assets.SubAssetMgr::ilo_get_error12(Mihua.Net.UrlLoader)
extern "C"  String_t* SubAssetMgr_ilo_get_error12_m1707208839 (Il2CppObject * __this /* static, unused */, UrlLoader_t2490729496 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mihua.Assets.SubAssetMgr::ilo_get_url13(Mihua.Net.UrlLoader)
extern "C"  String_t* SubAssetMgr_ilo_get_url13_m4086544991 (Il2CppObject * __this /* static, unused */, UrlLoader_t2490729496 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua.Assets.SubAssetMgr::ilo_Log14(System.Object,System.Boolean)
extern "C"  void SubAssetMgr_ilo_Log14_m620768931 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mihua.Assets.SubAssetMgr::ilo_get_isDone15(Mihua.Net.UrlLoader)
extern "C"  bool SubAssetMgr_ilo_get_isDone15_m3819823419 (Il2CppObject * __this /* static, unused */, UrlLoader_t2490729496 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mihua.Assets.SubAssetMgr::ilo_get_bytes16(Mihua.Net.UrlLoader)
extern "C"  ByteU5BU5D_t4260760469* SubAssetMgr_ilo_get_bytes16_m1978571719 (Il2CppObject * __this /* static, unused */, UrlLoader_t2490729496 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua.Assets.SubAssetMgr::ilo_SetPercent17(LoadingBoardMgr,System.Single,System.String)
extern "C"  void SubAssetMgr_ilo_SetPercent17_m2389762349 (Il2CppObject * __this /* static, unused */, LoadingBoardMgr_t303632014 * ____this0, float ___value1, String_t* ___msg2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Mihua.Assets.SubAssetMgr::ilo_get_Progress18(Mihua.Assets.SubAssetMgr)
extern "C"  float SubAssetMgr_ilo_get_Progress18_m2608229573 (Il2CppObject * __this /* static, unused */, SubAssetMgr_t3564963414 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
