﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_steadowgu132
struct M_steadowgu132_t2582810595;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"

// System.Void GarbageiOS.M_steadowgu132::.ctor()
extern "C"  void M_steadowgu132__ctor_m3415921072 (M_steadowgu132_t2582810595 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_steadowgu132::M_sejouJiwi0(System.String[],System.Int32)
extern "C"  void M_steadowgu132_M_sejouJiwi0_m2792355838 (M_steadowgu132_t2582810595 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_steadowgu132::M_trurjawmearTallhe1(System.String[],System.Int32)
extern "C"  void M_steadowgu132_M_trurjawmearTallhe1_m2996405206 (M_steadowgu132_t2582810595 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
