﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UITooltipGenerated
struct UITooltipGenerated_t3293307776;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"

// System.Void UITooltipGenerated::.ctor()
extern "C"  void UITooltipGenerated__ctor_m4232187163 (UITooltipGenerated_t3293307776 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UITooltipGenerated::UITooltip_UITooltip1(JSVCall,System.Int32)
extern "C"  bool UITooltipGenerated_UITooltip_UITooltip1_m475498911 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITooltipGenerated::UITooltip_uiCamera(JSVCall)
extern "C"  void UITooltipGenerated_UITooltip_uiCamera_m3228339893 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITooltipGenerated::UITooltip_text(JSVCall)
extern "C"  void UITooltipGenerated_UITooltip_text_m149871329 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITooltipGenerated::UITooltip_background(JSVCall)
extern "C"  void UITooltipGenerated_UITooltip_background_m4177440256 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITooltipGenerated::UITooltip_appearSpeed(JSVCall)
extern "C"  void UITooltipGenerated_UITooltip_appearSpeed_m1984600364 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITooltipGenerated::UITooltip_scalingTransitions(JSVCall)
extern "C"  void UITooltipGenerated_UITooltip_scalingTransitions_m3245745559 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITooltipGenerated::UITooltip_isVisible(JSVCall)
extern "C"  void UITooltipGenerated_UITooltip_isVisible_m388275126 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UITooltipGenerated::UITooltip_Hide(JSVCall,System.Int32)
extern "C"  bool UITooltipGenerated_UITooltip_Hide_m1109049535 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UITooltipGenerated::UITooltip_Show__String(JSVCall,System.Int32)
extern "C"  bool UITooltipGenerated_UITooltip_Show__String_m2381239435 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITooltipGenerated::__Register()
extern "C"  void UITooltipGenerated___Register_m531785868 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UITooltipGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UITooltipGenerated_ilo_getObject1_m4168987991 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UITooltipGenerated::ilo_getObject2(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UITooltipGenerated_ilo_getObject2_m2327266101 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UITooltipGenerated::ilo_getSingle3(System.Int32)
extern "C"  float UITooltipGenerated_ilo_getSingle3_m1201118318 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITooltipGenerated::ilo_setBooleanS4(System.Int32,System.Boolean)
extern "C"  void UITooltipGenerated_ilo_setBooleanS4_m1287636521 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UITooltipGenerated::ilo_getBooleanS5(System.Int32)
extern "C"  bool UITooltipGenerated_ilo_getBooleanS5_m28174677 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UITooltipGenerated::ilo_get_isVisible6()
extern "C"  bool UITooltipGenerated_ilo_get_isVisible6_m3147891111 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITooltipGenerated::ilo_Hide7()
extern "C"  void UITooltipGenerated_ilo_Hide7_m207923867 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UITooltipGenerated::ilo_getStringS8(System.Int32)
extern "C"  String_t* UITooltipGenerated_ilo_getStringS8_m1124201504 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
