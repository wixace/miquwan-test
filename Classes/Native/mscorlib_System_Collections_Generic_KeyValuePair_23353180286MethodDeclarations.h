﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_407833816MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,Mihua.Assets.SubAssetMgr/ErrorInfo>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m432389561(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t3353180286 *, String_t*, ErrorInfo_t2633981210 , const MethodInfo*))KeyValuePair_2__ctor_m333815527_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,Mihua.Assets.SubAssetMgr/ErrorInfo>::get_Key()
#define KeyValuePair_2_get_Key_m1060383247(__this, method) ((  String_t* (*) (KeyValuePair_2_t3353180286 *, const MethodInfo*))KeyValuePair_2_get_Key_m3798965537_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,Mihua.Assets.SubAssetMgr/ErrorInfo>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m4101385424(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3353180286 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m3619538658_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,Mihua.Assets.SubAssetMgr/ErrorInfo>::get_Value()
#define KeyValuePair_2_get_Value_m3944904207(__this, method) ((  ErrorInfo_t2633981210  (*) (KeyValuePair_2_t3353180286 *, const MethodInfo*))KeyValuePair_2_get_Value_m2907532449_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,Mihua.Assets.SubAssetMgr/ErrorInfo>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m894343376(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3353180286 *, ErrorInfo_t2633981210 , const MethodInfo*))KeyValuePair_2_set_Value_m2553731554_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,Mihua.Assets.SubAssetMgr/ErrorInfo>::ToString()
#define KeyValuePair_2_ToString_m3608580562(__this, method) ((  String_t* (*) (KeyValuePair_2_t3353180286 *, const MethodInfo*))KeyValuePair_2_ToString_m2605285632_gshared)(__this, method)
