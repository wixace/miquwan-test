﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_niskor60
struct  M_niskor60_t3140562176  : public Il2CppObject
{
public:
	// System.Boolean GarbageiOS.M_niskor60::_soherje
	bool ____soherje_0;
	// System.Single GarbageiOS.M_niskor60::_ceasearsaFumeray
	float ____ceasearsaFumeray_1;
	// System.Int32 GarbageiOS.M_niskor60::_meeserme
	int32_t ____meeserme_2;
	// System.Boolean GarbageiOS.M_niskor60::_vearwata
	bool ____vearwata_3;
	// System.Int32 GarbageiOS.M_niskor60::_foharKearlestall
	int32_t ____foharKearlestall_4;
	// System.Boolean GarbageiOS.M_niskor60::_raslecaLurdraytris
	bool ____raslecaLurdraytris_5;
	// System.UInt32 GarbageiOS.M_niskor60::_seballSanawtu
	uint32_t ____seballSanawtu_6;
	// System.UInt32 GarbageiOS.M_niskor60::_roovel
	uint32_t ____roovel_7;
	// System.String GarbageiOS.M_niskor60::_rairtassai
	String_t* ____rairtassai_8;
	// System.Single GarbageiOS.M_niskor60::_tapouKihurde
	float ____tapouKihurde_9;
	// System.Boolean GarbageiOS.M_niskor60::_cailehalChenur
	bool ____cailehalChenur_10;
	// System.String GarbageiOS.M_niskor60::_nilurmaiKairjel
	String_t* ____nilurmaiKairjel_11;
	// System.UInt32 GarbageiOS.M_niskor60::_nousawsiWhasjara
	uint32_t ____nousawsiWhasjara_12;
	// System.Int32 GarbageiOS.M_niskor60::_nukaJebas
	int32_t ____nukaJebas_13;
	// System.Single GarbageiOS.M_niskor60::_camairBoote
	float ____camairBoote_14;
	// System.Single GarbageiOS.M_niskor60::_reepouTromeci
	float ____reepouTromeci_15;
	// System.Boolean GarbageiOS.M_niskor60::_sisjokir
	bool ____sisjokir_16;
	// System.Single GarbageiOS.M_niskor60::_weepowLeatou
	float ____weepowLeatou_17;
	// System.Int32 GarbageiOS.M_niskor60::_roki
	int32_t ____roki_18;
	// System.Boolean GarbageiOS.M_niskor60::_fadoko
	bool ____fadoko_19;
	// System.Int32 GarbageiOS.M_niskor60::_garvehasMereje
	int32_t ____garvehasMereje_20;
	// System.Boolean GarbageiOS.M_niskor60::_tigall
	bool ____tigall_21;
	// System.Int32 GarbageiOS.M_niskor60::_seselver
	int32_t ____seselver_22;
	// System.Boolean GarbageiOS.M_niskor60::_cawburhe
	bool ____cawburhe_23;
	// System.Boolean GarbageiOS.M_niskor60::_reredimeVatirmar
	bool ____reredimeVatirmar_24;
	// System.Boolean GarbageiOS.M_niskor60::_nearbemHearde
	bool ____nearbemHearde_25;
	// System.Single GarbageiOS.M_niskor60::_pesoufo
	float ____pesoufo_26;
	// System.Int32 GarbageiOS.M_niskor60::_wallkiGowhejou
	int32_t ____wallkiGowhejou_27;
	// System.String GarbageiOS.M_niskor60::_dearce
	String_t* ____dearce_28;
	// System.Boolean GarbageiOS.M_niskor60::_qereweji
	bool ____qereweji_29;
	// System.Int32 GarbageiOS.M_niskor60::_gebai
	int32_t ____gebai_30;
	// System.Boolean GarbageiOS.M_niskor60::_nasow
	bool ____nasow_31;
	// System.UInt32 GarbageiOS.M_niskor60::_tairnaxuWouzoo
	uint32_t ____tairnaxuWouzoo_32;
	// System.Int32 GarbageiOS.M_niskor60::_raircear
	int32_t ____raircear_33;
	// System.Int32 GarbageiOS.M_niskor60::_renege
	int32_t ____renege_34;
	// System.Boolean GarbageiOS.M_niskor60::_xoryal
	bool ____xoryal_35;
	// System.Int32 GarbageiOS.M_niskor60::_sallbi
	int32_t ____sallbi_36;
	// System.Boolean GarbageiOS.M_niskor60::_rateevi
	bool ____rateevi_37;
	// System.String GarbageiOS.M_niskor60::_pejar
	String_t* ____pejar_38;
	// System.Boolean GarbageiOS.M_niskor60::_cagiFuwee
	bool ____cagiFuwee_39;
	// System.String GarbageiOS.M_niskor60::_sooceacow
	String_t* ____sooceacow_40;
	// System.UInt32 GarbageiOS.M_niskor60::_sirwallxarSurdiha
	uint32_t ____sirwallxarSurdiha_41;
	// System.UInt32 GarbageiOS.M_niskor60::_guchel
	uint32_t ____guchel_42;

public:
	inline static int32_t get_offset_of__soherje_0() { return static_cast<int32_t>(offsetof(M_niskor60_t3140562176, ____soherje_0)); }
	inline bool get__soherje_0() const { return ____soherje_0; }
	inline bool* get_address_of__soherje_0() { return &____soherje_0; }
	inline void set__soherje_0(bool value)
	{
		____soherje_0 = value;
	}

	inline static int32_t get_offset_of__ceasearsaFumeray_1() { return static_cast<int32_t>(offsetof(M_niskor60_t3140562176, ____ceasearsaFumeray_1)); }
	inline float get__ceasearsaFumeray_1() const { return ____ceasearsaFumeray_1; }
	inline float* get_address_of__ceasearsaFumeray_1() { return &____ceasearsaFumeray_1; }
	inline void set__ceasearsaFumeray_1(float value)
	{
		____ceasearsaFumeray_1 = value;
	}

	inline static int32_t get_offset_of__meeserme_2() { return static_cast<int32_t>(offsetof(M_niskor60_t3140562176, ____meeserme_2)); }
	inline int32_t get__meeserme_2() const { return ____meeserme_2; }
	inline int32_t* get_address_of__meeserme_2() { return &____meeserme_2; }
	inline void set__meeserme_2(int32_t value)
	{
		____meeserme_2 = value;
	}

	inline static int32_t get_offset_of__vearwata_3() { return static_cast<int32_t>(offsetof(M_niskor60_t3140562176, ____vearwata_3)); }
	inline bool get__vearwata_3() const { return ____vearwata_3; }
	inline bool* get_address_of__vearwata_3() { return &____vearwata_3; }
	inline void set__vearwata_3(bool value)
	{
		____vearwata_3 = value;
	}

	inline static int32_t get_offset_of__foharKearlestall_4() { return static_cast<int32_t>(offsetof(M_niskor60_t3140562176, ____foharKearlestall_4)); }
	inline int32_t get__foharKearlestall_4() const { return ____foharKearlestall_4; }
	inline int32_t* get_address_of__foharKearlestall_4() { return &____foharKearlestall_4; }
	inline void set__foharKearlestall_4(int32_t value)
	{
		____foharKearlestall_4 = value;
	}

	inline static int32_t get_offset_of__raslecaLurdraytris_5() { return static_cast<int32_t>(offsetof(M_niskor60_t3140562176, ____raslecaLurdraytris_5)); }
	inline bool get__raslecaLurdraytris_5() const { return ____raslecaLurdraytris_5; }
	inline bool* get_address_of__raslecaLurdraytris_5() { return &____raslecaLurdraytris_5; }
	inline void set__raslecaLurdraytris_5(bool value)
	{
		____raslecaLurdraytris_5 = value;
	}

	inline static int32_t get_offset_of__seballSanawtu_6() { return static_cast<int32_t>(offsetof(M_niskor60_t3140562176, ____seballSanawtu_6)); }
	inline uint32_t get__seballSanawtu_6() const { return ____seballSanawtu_6; }
	inline uint32_t* get_address_of__seballSanawtu_6() { return &____seballSanawtu_6; }
	inline void set__seballSanawtu_6(uint32_t value)
	{
		____seballSanawtu_6 = value;
	}

	inline static int32_t get_offset_of__roovel_7() { return static_cast<int32_t>(offsetof(M_niskor60_t3140562176, ____roovel_7)); }
	inline uint32_t get__roovel_7() const { return ____roovel_7; }
	inline uint32_t* get_address_of__roovel_7() { return &____roovel_7; }
	inline void set__roovel_7(uint32_t value)
	{
		____roovel_7 = value;
	}

	inline static int32_t get_offset_of__rairtassai_8() { return static_cast<int32_t>(offsetof(M_niskor60_t3140562176, ____rairtassai_8)); }
	inline String_t* get__rairtassai_8() const { return ____rairtassai_8; }
	inline String_t** get_address_of__rairtassai_8() { return &____rairtassai_8; }
	inline void set__rairtassai_8(String_t* value)
	{
		____rairtassai_8 = value;
		Il2CppCodeGenWriteBarrier(&____rairtassai_8, value);
	}

	inline static int32_t get_offset_of__tapouKihurde_9() { return static_cast<int32_t>(offsetof(M_niskor60_t3140562176, ____tapouKihurde_9)); }
	inline float get__tapouKihurde_9() const { return ____tapouKihurde_9; }
	inline float* get_address_of__tapouKihurde_9() { return &____tapouKihurde_9; }
	inline void set__tapouKihurde_9(float value)
	{
		____tapouKihurde_9 = value;
	}

	inline static int32_t get_offset_of__cailehalChenur_10() { return static_cast<int32_t>(offsetof(M_niskor60_t3140562176, ____cailehalChenur_10)); }
	inline bool get__cailehalChenur_10() const { return ____cailehalChenur_10; }
	inline bool* get_address_of__cailehalChenur_10() { return &____cailehalChenur_10; }
	inline void set__cailehalChenur_10(bool value)
	{
		____cailehalChenur_10 = value;
	}

	inline static int32_t get_offset_of__nilurmaiKairjel_11() { return static_cast<int32_t>(offsetof(M_niskor60_t3140562176, ____nilurmaiKairjel_11)); }
	inline String_t* get__nilurmaiKairjel_11() const { return ____nilurmaiKairjel_11; }
	inline String_t** get_address_of__nilurmaiKairjel_11() { return &____nilurmaiKairjel_11; }
	inline void set__nilurmaiKairjel_11(String_t* value)
	{
		____nilurmaiKairjel_11 = value;
		Il2CppCodeGenWriteBarrier(&____nilurmaiKairjel_11, value);
	}

	inline static int32_t get_offset_of__nousawsiWhasjara_12() { return static_cast<int32_t>(offsetof(M_niskor60_t3140562176, ____nousawsiWhasjara_12)); }
	inline uint32_t get__nousawsiWhasjara_12() const { return ____nousawsiWhasjara_12; }
	inline uint32_t* get_address_of__nousawsiWhasjara_12() { return &____nousawsiWhasjara_12; }
	inline void set__nousawsiWhasjara_12(uint32_t value)
	{
		____nousawsiWhasjara_12 = value;
	}

	inline static int32_t get_offset_of__nukaJebas_13() { return static_cast<int32_t>(offsetof(M_niskor60_t3140562176, ____nukaJebas_13)); }
	inline int32_t get__nukaJebas_13() const { return ____nukaJebas_13; }
	inline int32_t* get_address_of__nukaJebas_13() { return &____nukaJebas_13; }
	inline void set__nukaJebas_13(int32_t value)
	{
		____nukaJebas_13 = value;
	}

	inline static int32_t get_offset_of__camairBoote_14() { return static_cast<int32_t>(offsetof(M_niskor60_t3140562176, ____camairBoote_14)); }
	inline float get__camairBoote_14() const { return ____camairBoote_14; }
	inline float* get_address_of__camairBoote_14() { return &____camairBoote_14; }
	inline void set__camairBoote_14(float value)
	{
		____camairBoote_14 = value;
	}

	inline static int32_t get_offset_of__reepouTromeci_15() { return static_cast<int32_t>(offsetof(M_niskor60_t3140562176, ____reepouTromeci_15)); }
	inline float get__reepouTromeci_15() const { return ____reepouTromeci_15; }
	inline float* get_address_of__reepouTromeci_15() { return &____reepouTromeci_15; }
	inline void set__reepouTromeci_15(float value)
	{
		____reepouTromeci_15 = value;
	}

	inline static int32_t get_offset_of__sisjokir_16() { return static_cast<int32_t>(offsetof(M_niskor60_t3140562176, ____sisjokir_16)); }
	inline bool get__sisjokir_16() const { return ____sisjokir_16; }
	inline bool* get_address_of__sisjokir_16() { return &____sisjokir_16; }
	inline void set__sisjokir_16(bool value)
	{
		____sisjokir_16 = value;
	}

	inline static int32_t get_offset_of__weepowLeatou_17() { return static_cast<int32_t>(offsetof(M_niskor60_t3140562176, ____weepowLeatou_17)); }
	inline float get__weepowLeatou_17() const { return ____weepowLeatou_17; }
	inline float* get_address_of__weepowLeatou_17() { return &____weepowLeatou_17; }
	inline void set__weepowLeatou_17(float value)
	{
		____weepowLeatou_17 = value;
	}

	inline static int32_t get_offset_of__roki_18() { return static_cast<int32_t>(offsetof(M_niskor60_t3140562176, ____roki_18)); }
	inline int32_t get__roki_18() const { return ____roki_18; }
	inline int32_t* get_address_of__roki_18() { return &____roki_18; }
	inline void set__roki_18(int32_t value)
	{
		____roki_18 = value;
	}

	inline static int32_t get_offset_of__fadoko_19() { return static_cast<int32_t>(offsetof(M_niskor60_t3140562176, ____fadoko_19)); }
	inline bool get__fadoko_19() const { return ____fadoko_19; }
	inline bool* get_address_of__fadoko_19() { return &____fadoko_19; }
	inline void set__fadoko_19(bool value)
	{
		____fadoko_19 = value;
	}

	inline static int32_t get_offset_of__garvehasMereje_20() { return static_cast<int32_t>(offsetof(M_niskor60_t3140562176, ____garvehasMereje_20)); }
	inline int32_t get__garvehasMereje_20() const { return ____garvehasMereje_20; }
	inline int32_t* get_address_of__garvehasMereje_20() { return &____garvehasMereje_20; }
	inline void set__garvehasMereje_20(int32_t value)
	{
		____garvehasMereje_20 = value;
	}

	inline static int32_t get_offset_of__tigall_21() { return static_cast<int32_t>(offsetof(M_niskor60_t3140562176, ____tigall_21)); }
	inline bool get__tigall_21() const { return ____tigall_21; }
	inline bool* get_address_of__tigall_21() { return &____tigall_21; }
	inline void set__tigall_21(bool value)
	{
		____tigall_21 = value;
	}

	inline static int32_t get_offset_of__seselver_22() { return static_cast<int32_t>(offsetof(M_niskor60_t3140562176, ____seselver_22)); }
	inline int32_t get__seselver_22() const { return ____seselver_22; }
	inline int32_t* get_address_of__seselver_22() { return &____seselver_22; }
	inline void set__seselver_22(int32_t value)
	{
		____seselver_22 = value;
	}

	inline static int32_t get_offset_of__cawburhe_23() { return static_cast<int32_t>(offsetof(M_niskor60_t3140562176, ____cawburhe_23)); }
	inline bool get__cawburhe_23() const { return ____cawburhe_23; }
	inline bool* get_address_of__cawburhe_23() { return &____cawburhe_23; }
	inline void set__cawburhe_23(bool value)
	{
		____cawburhe_23 = value;
	}

	inline static int32_t get_offset_of__reredimeVatirmar_24() { return static_cast<int32_t>(offsetof(M_niskor60_t3140562176, ____reredimeVatirmar_24)); }
	inline bool get__reredimeVatirmar_24() const { return ____reredimeVatirmar_24; }
	inline bool* get_address_of__reredimeVatirmar_24() { return &____reredimeVatirmar_24; }
	inline void set__reredimeVatirmar_24(bool value)
	{
		____reredimeVatirmar_24 = value;
	}

	inline static int32_t get_offset_of__nearbemHearde_25() { return static_cast<int32_t>(offsetof(M_niskor60_t3140562176, ____nearbemHearde_25)); }
	inline bool get__nearbemHearde_25() const { return ____nearbemHearde_25; }
	inline bool* get_address_of__nearbemHearde_25() { return &____nearbemHearde_25; }
	inline void set__nearbemHearde_25(bool value)
	{
		____nearbemHearde_25 = value;
	}

	inline static int32_t get_offset_of__pesoufo_26() { return static_cast<int32_t>(offsetof(M_niskor60_t3140562176, ____pesoufo_26)); }
	inline float get__pesoufo_26() const { return ____pesoufo_26; }
	inline float* get_address_of__pesoufo_26() { return &____pesoufo_26; }
	inline void set__pesoufo_26(float value)
	{
		____pesoufo_26 = value;
	}

	inline static int32_t get_offset_of__wallkiGowhejou_27() { return static_cast<int32_t>(offsetof(M_niskor60_t3140562176, ____wallkiGowhejou_27)); }
	inline int32_t get__wallkiGowhejou_27() const { return ____wallkiGowhejou_27; }
	inline int32_t* get_address_of__wallkiGowhejou_27() { return &____wallkiGowhejou_27; }
	inline void set__wallkiGowhejou_27(int32_t value)
	{
		____wallkiGowhejou_27 = value;
	}

	inline static int32_t get_offset_of__dearce_28() { return static_cast<int32_t>(offsetof(M_niskor60_t3140562176, ____dearce_28)); }
	inline String_t* get__dearce_28() const { return ____dearce_28; }
	inline String_t** get_address_of__dearce_28() { return &____dearce_28; }
	inline void set__dearce_28(String_t* value)
	{
		____dearce_28 = value;
		Il2CppCodeGenWriteBarrier(&____dearce_28, value);
	}

	inline static int32_t get_offset_of__qereweji_29() { return static_cast<int32_t>(offsetof(M_niskor60_t3140562176, ____qereweji_29)); }
	inline bool get__qereweji_29() const { return ____qereweji_29; }
	inline bool* get_address_of__qereweji_29() { return &____qereweji_29; }
	inline void set__qereweji_29(bool value)
	{
		____qereweji_29 = value;
	}

	inline static int32_t get_offset_of__gebai_30() { return static_cast<int32_t>(offsetof(M_niskor60_t3140562176, ____gebai_30)); }
	inline int32_t get__gebai_30() const { return ____gebai_30; }
	inline int32_t* get_address_of__gebai_30() { return &____gebai_30; }
	inline void set__gebai_30(int32_t value)
	{
		____gebai_30 = value;
	}

	inline static int32_t get_offset_of__nasow_31() { return static_cast<int32_t>(offsetof(M_niskor60_t3140562176, ____nasow_31)); }
	inline bool get__nasow_31() const { return ____nasow_31; }
	inline bool* get_address_of__nasow_31() { return &____nasow_31; }
	inline void set__nasow_31(bool value)
	{
		____nasow_31 = value;
	}

	inline static int32_t get_offset_of__tairnaxuWouzoo_32() { return static_cast<int32_t>(offsetof(M_niskor60_t3140562176, ____tairnaxuWouzoo_32)); }
	inline uint32_t get__tairnaxuWouzoo_32() const { return ____tairnaxuWouzoo_32; }
	inline uint32_t* get_address_of__tairnaxuWouzoo_32() { return &____tairnaxuWouzoo_32; }
	inline void set__tairnaxuWouzoo_32(uint32_t value)
	{
		____tairnaxuWouzoo_32 = value;
	}

	inline static int32_t get_offset_of__raircear_33() { return static_cast<int32_t>(offsetof(M_niskor60_t3140562176, ____raircear_33)); }
	inline int32_t get__raircear_33() const { return ____raircear_33; }
	inline int32_t* get_address_of__raircear_33() { return &____raircear_33; }
	inline void set__raircear_33(int32_t value)
	{
		____raircear_33 = value;
	}

	inline static int32_t get_offset_of__renege_34() { return static_cast<int32_t>(offsetof(M_niskor60_t3140562176, ____renege_34)); }
	inline int32_t get__renege_34() const { return ____renege_34; }
	inline int32_t* get_address_of__renege_34() { return &____renege_34; }
	inline void set__renege_34(int32_t value)
	{
		____renege_34 = value;
	}

	inline static int32_t get_offset_of__xoryal_35() { return static_cast<int32_t>(offsetof(M_niskor60_t3140562176, ____xoryal_35)); }
	inline bool get__xoryal_35() const { return ____xoryal_35; }
	inline bool* get_address_of__xoryal_35() { return &____xoryal_35; }
	inline void set__xoryal_35(bool value)
	{
		____xoryal_35 = value;
	}

	inline static int32_t get_offset_of__sallbi_36() { return static_cast<int32_t>(offsetof(M_niskor60_t3140562176, ____sallbi_36)); }
	inline int32_t get__sallbi_36() const { return ____sallbi_36; }
	inline int32_t* get_address_of__sallbi_36() { return &____sallbi_36; }
	inline void set__sallbi_36(int32_t value)
	{
		____sallbi_36 = value;
	}

	inline static int32_t get_offset_of__rateevi_37() { return static_cast<int32_t>(offsetof(M_niskor60_t3140562176, ____rateevi_37)); }
	inline bool get__rateevi_37() const { return ____rateevi_37; }
	inline bool* get_address_of__rateevi_37() { return &____rateevi_37; }
	inline void set__rateevi_37(bool value)
	{
		____rateevi_37 = value;
	}

	inline static int32_t get_offset_of__pejar_38() { return static_cast<int32_t>(offsetof(M_niskor60_t3140562176, ____pejar_38)); }
	inline String_t* get__pejar_38() const { return ____pejar_38; }
	inline String_t** get_address_of__pejar_38() { return &____pejar_38; }
	inline void set__pejar_38(String_t* value)
	{
		____pejar_38 = value;
		Il2CppCodeGenWriteBarrier(&____pejar_38, value);
	}

	inline static int32_t get_offset_of__cagiFuwee_39() { return static_cast<int32_t>(offsetof(M_niskor60_t3140562176, ____cagiFuwee_39)); }
	inline bool get__cagiFuwee_39() const { return ____cagiFuwee_39; }
	inline bool* get_address_of__cagiFuwee_39() { return &____cagiFuwee_39; }
	inline void set__cagiFuwee_39(bool value)
	{
		____cagiFuwee_39 = value;
	}

	inline static int32_t get_offset_of__sooceacow_40() { return static_cast<int32_t>(offsetof(M_niskor60_t3140562176, ____sooceacow_40)); }
	inline String_t* get__sooceacow_40() const { return ____sooceacow_40; }
	inline String_t** get_address_of__sooceacow_40() { return &____sooceacow_40; }
	inline void set__sooceacow_40(String_t* value)
	{
		____sooceacow_40 = value;
		Il2CppCodeGenWriteBarrier(&____sooceacow_40, value);
	}

	inline static int32_t get_offset_of__sirwallxarSurdiha_41() { return static_cast<int32_t>(offsetof(M_niskor60_t3140562176, ____sirwallxarSurdiha_41)); }
	inline uint32_t get__sirwallxarSurdiha_41() const { return ____sirwallxarSurdiha_41; }
	inline uint32_t* get_address_of__sirwallxarSurdiha_41() { return &____sirwallxarSurdiha_41; }
	inline void set__sirwallxarSurdiha_41(uint32_t value)
	{
		____sirwallxarSurdiha_41 = value;
	}

	inline static int32_t get_offset_of__guchel_42() { return static_cast<int32_t>(offsetof(M_niskor60_t3140562176, ____guchel_42)); }
	inline uint32_t get__guchel_42() const { return ____guchel_42; }
	inline uint32_t* get_address_of__guchel_42() { return &____guchel_42; }
	inline void set__guchel_42(uint32_t value)
	{
		____guchel_42 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
