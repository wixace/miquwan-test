﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// JSDataExchangeMgr/DGetV`1<UnityEngine.Vector3[]>
struct DGetV_1_t93051952;
// JSDataExchangeMgr/DGetV`1<UnityEngine.Vector4[]>
struct DGetV_1_t579239691;
// JSDataExchangeMgr/DGetV`1<UnityEngine.Vector2[]>
struct DGetV_1_t3901831509;
// JSDataExchangeMgr/DGetV`1<UnityEngine.Color[]>
struct DGetV_1_t2319196977;
// JSDataExchangeMgr/DGetV`1<UnityEngine.Color32[]>
struct DGetV_1_t2838418294;
// JSDataExchangeMgr/DGetV`1<System.Int32[]>
struct DGetV_1_t3108499162;
// JSDataExchangeMgr/DGetV`1<UnityEngine.BoneWeight[]>
struct DGetV_1_t1381486574;
// JSDataExchangeMgr/DGetV`1<UnityEngine.Matrix4x4[]>
struct DGetV_1_t1299315797;
// JSDataExchangeMgr/DGetV`1<UnityEngine.CombineInstance[]>
struct DGetV_1_t1728074364;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine_MeshGenerated
struct  UnityEngine_MeshGenerated_t525237690  : public Il2CppObject
{
public:

public:
};

struct UnityEngine_MeshGenerated_t525237690_StaticFields
{
public:
	// JSDataExchangeMgr/DGetV`1<UnityEngine.Vector3[]> UnityEngine_MeshGenerated::<>f__am$cache0
	DGetV_1_t93051952 * ___U3CU3Ef__amU24cache0_0;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.Vector3[]> UnityEngine_MeshGenerated::<>f__am$cache1
	DGetV_1_t93051952 * ___U3CU3Ef__amU24cache1_1;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.Vector4[]> UnityEngine_MeshGenerated::<>f__am$cache2
	DGetV_1_t579239691 * ___U3CU3Ef__amU24cache2_2;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.Vector2[]> UnityEngine_MeshGenerated::<>f__am$cache3
	DGetV_1_t3901831509 * ___U3CU3Ef__amU24cache3_3;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.Vector2[]> UnityEngine_MeshGenerated::<>f__am$cache4
	DGetV_1_t3901831509 * ___U3CU3Ef__amU24cache4_4;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.Vector2[]> UnityEngine_MeshGenerated::<>f__am$cache5
	DGetV_1_t3901831509 * ___U3CU3Ef__amU24cache5_5;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.Vector2[]> UnityEngine_MeshGenerated::<>f__am$cache6
	DGetV_1_t3901831509 * ___U3CU3Ef__amU24cache6_6;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.Color[]> UnityEngine_MeshGenerated::<>f__am$cache7
	DGetV_1_t2319196977 * ___U3CU3Ef__amU24cache7_7;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.Color32[]> UnityEngine_MeshGenerated::<>f__am$cache8
	DGetV_1_t2838418294 * ___U3CU3Ef__amU24cache8_8;
	// JSDataExchangeMgr/DGetV`1<System.Int32[]> UnityEngine_MeshGenerated::<>f__am$cache9
	DGetV_1_t3108499162 * ___U3CU3Ef__amU24cache9_9;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.BoneWeight[]> UnityEngine_MeshGenerated::<>f__am$cacheA
	DGetV_1_t1381486574 * ___U3CU3Ef__amU24cacheA_10;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.Matrix4x4[]> UnityEngine_MeshGenerated::<>f__am$cacheB
	DGetV_1_t1299315797 * ___U3CU3Ef__amU24cacheB_11;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.Vector3[]> UnityEngine_MeshGenerated::<>f__am$cacheC
	DGetV_1_t93051952 * ___U3CU3Ef__amU24cacheC_12;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.Vector3[]> UnityEngine_MeshGenerated::<>f__am$cacheD
	DGetV_1_t93051952 * ___U3CU3Ef__amU24cacheD_13;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.Vector3[]> UnityEngine_MeshGenerated::<>f__am$cacheE
	DGetV_1_t93051952 * ___U3CU3Ef__amU24cacheE_14;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.CombineInstance[]> UnityEngine_MeshGenerated::<>f__am$cacheF
	DGetV_1_t1728074364 * ___U3CU3Ef__amU24cacheF_15;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.CombineInstance[]> UnityEngine_MeshGenerated::<>f__am$cache10
	DGetV_1_t1728074364 * ___U3CU3Ef__amU24cache10_16;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.CombineInstance[]> UnityEngine_MeshGenerated::<>f__am$cache11
	DGetV_1_t1728074364 * ___U3CU3Ef__amU24cache11_17;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.Vector3[]> UnityEngine_MeshGenerated::<>f__am$cache12
	DGetV_1_t93051952 * ___U3CU3Ef__amU24cache12_18;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.Vector3[]> UnityEngine_MeshGenerated::<>f__am$cache13
	DGetV_1_t93051952 * ___U3CU3Ef__amU24cache13_19;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.Vector3[]> UnityEngine_MeshGenerated::<>f__am$cache14
	DGetV_1_t93051952 * ___U3CU3Ef__amU24cache14_20;
	// JSDataExchangeMgr/DGetV`1<System.Int32[]> UnityEngine_MeshGenerated::<>f__am$cache15
	DGetV_1_t3108499162 * ___U3CU3Ef__amU24cache15_21;
	// JSDataExchangeMgr/DGetV`1<System.Int32[]> UnityEngine_MeshGenerated::<>f__am$cache16
	DGetV_1_t3108499162 * ___U3CU3Ef__amU24cache16_22;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_0() { return static_cast<int32_t>(offsetof(UnityEngine_MeshGenerated_t525237690_StaticFields, ___U3CU3Ef__amU24cache0_0)); }
	inline DGetV_1_t93051952 * get_U3CU3Ef__amU24cache0_0() const { return ___U3CU3Ef__amU24cache0_0; }
	inline DGetV_1_t93051952 ** get_address_of_U3CU3Ef__amU24cache0_0() { return &___U3CU3Ef__amU24cache0_0; }
	inline void set_U3CU3Ef__amU24cache0_0(DGetV_1_t93051952 * value)
	{
		___U3CU3Ef__amU24cache0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_1() { return static_cast<int32_t>(offsetof(UnityEngine_MeshGenerated_t525237690_StaticFields, ___U3CU3Ef__amU24cache1_1)); }
	inline DGetV_1_t93051952 * get_U3CU3Ef__amU24cache1_1() const { return ___U3CU3Ef__amU24cache1_1; }
	inline DGetV_1_t93051952 ** get_address_of_U3CU3Ef__amU24cache1_1() { return &___U3CU3Ef__amU24cache1_1; }
	inline void set_U3CU3Ef__amU24cache1_1(DGetV_1_t93051952 * value)
	{
		___U3CU3Ef__amU24cache1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1_1, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_2() { return static_cast<int32_t>(offsetof(UnityEngine_MeshGenerated_t525237690_StaticFields, ___U3CU3Ef__amU24cache2_2)); }
	inline DGetV_1_t579239691 * get_U3CU3Ef__amU24cache2_2() const { return ___U3CU3Ef__amU24cache2_2; }
	inline DGetV_1_t579239691 ** get_address_of_U3CU3Ef__amU24cache2_2() { return &___U3CU3Ef__amU24cache2_2; }
	inline void set_U3CU3Ef__amU24cache2_2(DGetV_1_t579239691 * value)
	{
		___U3CU3Ef__amU24cache2_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache2_2, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_3() { return static_cast<int32_t>(offsetof(UnityEngine_MeshGenerated_t525237690_StaticFields, ___U3CU3Ef__amU24cache3_3)); }
	inline DGetV_1_t3901831509 * get_U3CU3Ef__amU24cache3_3() const { return ___U3CU3Ef__amU24cache3_3; }
	inline DGetV_1_t3901831509 ** get_address_of_U3CU3Ef__amU24cache3_3() { return &___U3CU3Ef__amU24cache3_3; }
	inline void set_U3CU3Ef__amU24cache3_3(DGetV_1_t3901831509 * value)
	{
		___U3CU3Ef__amU24cache3_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache3_3, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache4_4() { return static_cast<int32_t>(offsetof(UnityEngine_MeshGenerated_t525237690_StaticFields, ___U3CU3Ef__amU24cache4_4)); }
	inline DGetV_1_t3901831509 * get_U3CU3Ef__amU24cache4_4() const { return ___U3CU3Ef__amU24cache4_4; }
	inline DGetV_1_t3901831509 ** get_address_of_U3CU3Ef__amU24cache4_4() { return &___U3CU3Ef__amU24cache4_4; }
	inline void set_U3CU3Ef__amU24cache4_4(DGetV_1_t3901831509 * value)
	{
		___U3CU3Ef__amU24cache4_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache4_4, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache5_5() { return static_cast<int32_t>(offsetof(UnityEngine_MeshGenerated_t525237690_StaticFields, ___U3CU3Ef__amU24cache5_5)); }
	inline DGetV_1_t3901831509 * get_U3CU3Ef__amU24cache5_5() const { return ___U3CU3Ef__amU24cache5_5; }
	inline DGetV_1_t3901831509 ** get_address_of_U3CU3Ef__amU24cache5_5() { return &___U3CU3Ef__amU24cache5_5; }
	inline void set_U3CU3Ef__amU24cache5_5(DGetV_1_t3901831509 * value)
	{
		___U3CU3Ef__amU24cache5_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache5_5, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache6_6() { return static_cast<int32_t>(offsetof(UnityEngine_MeshGenerated_t525237690_StaticFields, ___U3CU3Ef__amU24cache6_6)); }
	inline DGetV_1_t3901831509 * get_U3CU3Ef__amU24cache6_6() const { return ___U3CU3Ef__amU24cache6_6; }
	inline DGetV_1_t3901831509 ** get_address_of_U3CU3Ef__amU24cache6_6() { return &___U3CU3Ef__amU24cache6_6; }
	inline void set_U3CU3Ef__amU24cache6_6(DGetV_1_t3901831509 * value)
	{
		___U3CU3Ef__amU24cache6_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache6_6, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache7_7() { return static_cast<int32_t>(offsetof(UnityEngine_MeshGenerated_t525237690_StaticFields, ___U3CU3Ef__amU24cache7_7)); }
	inline DGetV_1_t2319196977 * get_U3CU3Ef__amU24cache7_7() const { return ___U3CU3Ef__amU24cache7_7; }
	inline DGetV_1_t2319196977 ** get_address_of_U3CU3Ef__amU24cache7_7() { return &___U3CU3Ef__amU24cache7_7; }
	inline void set_U3CU3Ef__amU24cache7_7(DGetV_1_t2319196977 * value)
	{
		___U3CU3Ef__amU24cache7_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache7_7, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache8_8() { return static_cast<int32_t>(offsetof(UnityEngine_MeshGenerated_t525237690_StaticFields, ___U3CU3Ef__amU24cache8_8)); }
	inline DGetV_1_t2838418294 * get_U3CU3Ef__amU24cache8_8() const { return ___U3CU3Ef__amU24cache8_8; }
	inline DGetV_1_t2838418294 ** get_address_of_U3CU3Ef__amU24cache8_8() { return &___U3CU3Ef__amU24cache8_8; }
	inline void set_U3CU3Ef__amU24cache8_8(DGetV_1_t2838418294 * value)
	{
		___U3CU3Ef__amU24cache8_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache8_8, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache9_9() { return static_cast<int32_t>(offsetof(UnityEngine_MeshGenerated_t525237690_StaticFields, ___U3CU3Ef__amU24cache9_9)); }
	inline DGetV_1_t3108499162 * get_U3CU3Ef__amU24cache9_9() const { return ___U3CU3Ef__amU24cache9_9; }
	inline DGetV_1_t3108499162 ** get_address_of_U3CU3Ef__amU24cache9_9() { return &___U3CU3Ef__amU24cache9_9; }
	inline void set_U3CU3Ef__amU24cache9_9(DGetV_1_t3108499162 * value)
	{
		___U3CU3Ef__amU24cache9_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache9_9, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheA_10() { return static_cast<int32_t>(offsetof(UnityEngine_MeshGenerated_t525237690_StaticFields, ___U3CU3Ef__amU24cacheA_10)); }
	inline DGetV_1_t1381486574 * get_U3CU3Ef__amU24cacheA_10() const { return ___U3CU3Ef__amU24cacheA_10; }
	inline DGetV_1_t1381486574 ** get_address_of_U3CU3Ef__amU24cacheA_10() { return &___U3CU3Ef__amU24cacheA_10; }
	inline void set_U3CU3Ef__amU24cacheA_10(DGetV_1_t1381486574 * value)
	{
		___U3CU3Ef__amU24cacheA_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheA_10, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheB_11() { return static_cast<int32_t>(offsetof(UnityEngine_MeshGenerated_t525237690_StaticFields, ___U3CU3Ef__amU24cacheB_11)); }
	inline DGetV_1_t1299315797 * get_U3CU3Ef__amU24cacheB_11() const { return ___U3CU3Ef__amU24cacheB_11; }
	inline DGetV_1_t1299315797 ** get_address_of_U3CU3Ef__amU24cacheB_11() { return &___U3CU3Ef__amU24cacheB_11; }
	inline void set_U3CU3Ef__amU24cacheB_11(DGetV_1_t1299315797 * value)
	{
		___U3CU3Ef__amU24cacheB_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheB_11, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheC_12() { return static_cast<int32_t>(offsetof(UnityEngine_MeshGenerated_t525237690_StaticFields, ___U3CU3Ef__amU24cacheC_12)); }
	inline DGetV_1_t93051952 * get_U3CU3Ef__amU24cacheC_12() const { return ___U3CU3Ef__amU24cacheC_12; }
	inline DGetV_1_t93051952 ** get_address_of_U3CU3Ef__amU24cacheC_12() { return &___U3CU3Ef__amU24cacheC_12; }
	inline void set_U3CU3Ef__amU24cacheC_12(DGetV_1_t93051952 * value)
	{
		___U3CU3Ef__amU24cacheC_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheC_12, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheD_13() { return static_cast<int32_t>(offsetof(UnityEngine_MeshGenerated_t525237690_StaticFields, ___U3CU3Ef__amU24cacheD_13)); }
	inline DGetV_1_t93051952 * get_U3CU3Ef__amU24cacheD_13() const { return ___U3CU3Ef__amU24cacheD_13; }
	inline DGetV_1_t93051952 ** get_address_of_U3CU3Ef__amU24cacheD_13() { return &___U3CU3Ef__amU24cacheD_13; }
	inline void set_U3CU3Ef__amU24cacheD_13(DGetV_1_t93051952 * value)
	{
		___U3CU3Ef__amU24cacheD_13 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheD_13, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheE_14() { return static_cast<int32_t>(offsetof(UnityEngine_MeshGenerated_t525237690_StaticFields, ___U3CU3Ef__amU24cacheE_14)); }
	inline DGetV_1_t93051952 * get_U3CU3Ef__amU24cacheE_14() const { return ___U3CU3Ef__amU24cacheE_14; }
	inline DGetV_1_t93051952 ** get_address_of_U3CU3Ef__amU24cacheE_14() { return &___U3CU3Ef__amU24cacheE_14; }
	inline void set_U3CU3Ef__amU24cacheE_14(DGetV_1_t93051952 * value)
	{
		___U3CU3Ef__amU24cacheE_14 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheE_14, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheF_15() { return static_cast<int32_t>(offsetof(UnityEngine_MeshGenerated_t525237690_StaticFields, ___U3CU3Ef__amU24cacheF_15)); }
	inline DGetV_1_t1728074364 * get_U3CU3Ef__amU24cacheF_15() const { return ___U3CU3Ef__amU24cacheF_15; }
	inline DGetV_1_t1728074364 ** get_address_of_U3CU3Ef__amU24cacheF_15() { return &___U3CU3Ef__amU24cacheF_15; }
	inline void set_U3CU3Ef__amU24cacheF_15(DGetV_1_t1728074364 * value)
	{
		___U3CU3Ef__amU24cacheF_15 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheF_15, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache10_16() { return static_cast<int32_t>(offsetof(UnityEngine_MeshGenerated_t525237690_StaticFields, ___U3CU3Ef__amU24cache10_16)); }
	inline DGetV_1_t1728074364 * get_U3CU3Ef__amU24cache10_16() const { return ___U3CU3Ef__amU24cache10_16; }
	inline DGetV_1_t1728074364 ** get_address_of_U3CU3Ef__amU24cache10_16() { return &___U3CU3Ef__amU24cache10_16; }
	inline void set_U3CU3Ef__amU24cache10_16(DGetV_1_t1728074364 * value)
	{
		___U3CU3Ef__amU24cache10_16 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache10_16, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache11_17() { return static_cast<int32_t>(offsetof(UnityEngine_MeshGenerated_t525237690_StaticFields, ___U3CU3Ef__amU24cache11_17)); }
	inline DGetV_1_t1728074364 * get_U3CU3Ef__amU24cache11_17() const { return ___U3CU3Ef__amU24cache11_17; }
	inline DGetV_1_t1728074364 ** get_address_of_U3CU3Ef__amU24cache11_17() { return &___U3CU3Ef__amU24cache11_17; }
	inline void set_U3CU3Ef__amU24cache11_17(DGetV_1_t1728074364 * value)
	{
		___U3CU3Ef__amU24cache11_17 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache11_17, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache12_18() { return static_cast<int32_t>(offsetof(UnityEngine_MeshGenerated_t525237690_StaticFields, ___U3CU3Ef__amU24cache12_18)); }
	inline DGetV_1_t93051952 * get_U3CU3Ef__amU24cache12_18() const { return ___U3CU3Ef__amU24cache12_18; }
	inline DGetV_1_t93051952 ** get_address_of_U3CU3Ef__amU24cache12_18() { return &___U3CU3Ef__amU24cache12_18; }
	inline void set_U3CU3Ef__amU24cache12_18(DGetV_1_t93051952 * value)
	{
		___U3CU3Ef__amU24cache12_18 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache12_18, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache13_19() { return static_cast<int32_t>(offsetof(UnityEngine_MeshGenerated_t525237690_StaticFields, ___U3CU3Ef__amU24cache13_19)); }
	inline DGetV_1_t93051952 * get_U3CU3Ef__amU24cache13_19() const { return ___U3CU3Ef__amU24cache13_19; }
	inline DGetV_1_t93051952 ** get_address_of_U3CU3Ef__amU24cache13_19() { return &___U3CU3Ef__amU24cache13_19; }
	inline void set_U3CU3Ef__amU24cache13_19(DGetV_1_t93051952 * value)
	{
		___U3CU3Ef__amU24cache13_19 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache13_19, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache14_20() { return static_cast<int32_t>(offsetof(UnityEngine_MeshGenerated_t525237690_StaticFields, ___U3CU3Ef__amU24cache14_20)); }
	inline DGetV_1_t93051952 * get_U3CU3Ef__amU24cache14_20() const { return ___U3CU3Ef__amU24cache14_20; }
	inline DGetV_1_t93051952 ** get_address_of_U3CU3Ef__amU24cache14_20() { return &___U3CU3Ef__amU24cache14_20; }
	inline void set_U3CU3Ef__amU24cache14_20(DGetV_1_t93051952 * value)
	{
		___U3CU3Ef__amU24cache14_20 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache14_20, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache15_21() { return static_cast<int32_t>(offsetof(UnityEngine_MeshGenerated_t525237690_StaticFields, ___U3CU3Ef__amU24cache15_21)); }
	inline DGetV_1_t3108499162 * get_U3CU3Ef__amU24cache15_21() const { return ___U3CU3Ef__amU24cache15_21; }
	inline DGetV_1_t3108499162 ** get_address_of_U3CU3Ef__amU24cache15_21() { return &___U3CU3Ef__amU24cache15_21; }
	inline void set_U3CU3Ef__amU24cache15_21(DGetV_1_t3108499162 * value)
	{
		___U3CU3Ef__amU24cache15_21 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache15_21, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache16_22() { return static_cast<int32_t>(offsetof(UnityEngine_MeshGenerated_t525237690_StaticFields, ___U3CU3Ef__amU24cache16_22)); }
	inline DGetV_1_t3108499162 * get_U3CU3Ef__amU24cache16_22() const { return ___U3CU3Ef__amU24cache16_22; }
	inline DGetV_1_t3108499162 ** get_address_of_U3CU3Ef__amU24cache16_22() { return &___U3CU3Ef__amU24cache16_22; }
	inline void set_U3CU3Ef__amU24cache16_22(DGetV_1_t3108499162 * value)
	{
		___U3CU3Ef__amU24cache16_22 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache16_22, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
