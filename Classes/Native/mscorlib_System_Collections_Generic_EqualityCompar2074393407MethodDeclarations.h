﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<SoundTypeID>
struct DefaultComparer_t2074393407;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SoundTypeID3626616740.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<SoundTypeID>::.ctor()
extern "C"  void DefaultComparer__ctor_m2611079778_gshared (DefaultComparer_t2074393407 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m2611079778(__this, method) ((  void (*) (DefaultComparer_t2074393407 *, const MethodInfo*))DefaultComparer__ctor_m2611079778_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<SoundTypeID>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m418859785_gshared (DefaultComparer_t2074393407 * __this, int32_t ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m418859785(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t2074393407 *, int32_t, const MethodInfo*))DefaultComparer_GetHashCode_m418859785_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<SoundTypeID>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m3200511027_gshared (DefaultComparer_t2074393407 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m3200511027(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t2074393407 *, int32_t, int32_t, const MethodInfo*))DefaultComparer_Equals_m3200511027_gshared)(__this, ___x0, ___y1, method)
