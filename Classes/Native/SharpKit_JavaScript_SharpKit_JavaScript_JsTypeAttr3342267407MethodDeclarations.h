﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SharpKit.JavaScript.JsTypeAttribute
struct JsTypeAttribute_t3342267407;
// System.Type
struct Type_t;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "SharpKit_JavaScript_SharpKit_JavaScript_JsMode3080509312.h"
#include "mscorlib_System_Type2863145774.h"
#include "mscorlib_System_String7231557.h"

// System.Void SharpKit.JavaScript.JsTypeAttribute::.ctor()
extern "C"  void JsTypeAttribute__ctor_m1821362512 (JsTypeAttribute_t3342267407 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SharpKit.JavaScript.JsTypeAttribute::.ctor(SharpKit.JavaScript.JsMode)
extern "C"  void JsTypeAttribute__ctor_m3970655397 (JsTypeAttribute_t3342267407 * __this, int32_t ___mode0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SharpKit.JavaScript.JsTypeAttribute::set_TargetType(System.Type)
extern "C"  void JsTypeAttribute_set_TargetType_m4137901405 (JsTypeAttribute_t3342267407 * __this, Type_t * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SharpKit.JavaScript.JsTypeAttribute::set_NativeParams(System.Boolean)
extern "C"  void JsTypeAttribute_set_NativeParams_m2413822245 (JsTypeAttribute_t3342267407 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SharpKit.JavaScript.JsTypeAttribute::set_NativeDelegates(System.Boolean)
extern "C"  void JsTypeAttribute_set_NativeDelegates_m4268830719 (JsTypeAttribute_t3342267407 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SharpKit.JavaScript.JsTypeAttribute::set_OmitCasts(System.Boolean)
extern "C"  void JsTypeAttribute_set_OmitCasts_m3509269971 (JsTypeAttribute_t3342267407 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SharpKit.JavaScript.JsTypeAttribute::set_PropertiesAsFields(System.Boolean)
extern "C"  void JsTypeAttribute_set_PropertiesAsFields_m1232173702 (JsTypeAttribute_t3342267407 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SharpKit.JavaScript.JsTypeAttribute::set_AutomaticPropertiesAsFields(System.Boolean)
extern "C"  void JsTypeAttribute_set_AutomaticPropertiesAsFields_m150870065 (JsTypeAttribute_t3342267407 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SharpKit.JavaScript.JsTypeAttribute::set_NativeEnumerator(System.Boolean)
extern "C"  void JsTypeAttribute_set_NativeEnumerator_m3276593123 (JsTypeAttribute_t3342267407 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SharpKit.JavaScript.JsTypeAttribute::set_NativeArrayEnumerator(System.Boolean)
extern "C"  void JsTypeAttribute_set_NativeArrayEnumerator_m1119156910 (JsTypeAttribute_t3342267407 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SharpKit.JavaScript.JsTypeAttribute::set_NativeConstructors(System.Boolean)
extern "C"  void JsTypeAttribute_set_NativeConstructors_m3656212504 (JsTypeAttribute_t3342267407 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SharpKit.JavaScript.JsTypeAttribute::set_NativeOverloads(System.Boolean)
extern "C"  void JsTypeAttribute_set_NativeOverloads_m397256298 (JsTypeAttribute_t3342267407 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SharpKit.JavaScript.JsTypeAttribute::set_NativeOperatorOverloads(System.Boolean)
extern "C"  void JsTypeAttribute_set_NativeOperatorOverloads_m568415526 (JsTypeAttribute_t3342267407 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SharpKit.JavaScript.JsTypeAttribute::set_Native(System.Boolean)
extern "C"  void JsTypeAttribute_set_Native_m1509132479 (JsTypeAttribute_t3342267407 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SharpKit.JavaScript.JsTypeAttribute::set_GlobalObject(System.Boolean)
extern "C"  void JsTypeAttribute_set_GlobalObject_m1207130986 (JsTypeAttribute_t3342267407 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SharpKit.JavaScript.JsTypeAttribute::set_NativeFunctions(System.Boolean)
extern "C"  void JsTypeAttribute_set_NativeFunctions_m333621772 (JsTypeAttribute_t3342267407 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SharpKit.JavaScript.JsTypeAttribute::set_NativeJsons(System.Boolean)
extern "C"  void JsTypeAttribute_set_NativeJsons_m799553404 (JsTypeAttribute_t3342267407 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SharpKit.JavaScript.JsTypeAttribute::set_Export(System.Boolean)
extern "C"  void JsTypeAttribute_set_Export_m2619437660 (JsTypeAttribute_t3342267407 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SharpKit.JavaScript.JsTypeAttribute::set_Name(System.String)
extern "C"  void JsTypeAttribute_set_Name_m3869255110 (JsTypeAttribute_t3342267407 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SharpKit.JavaScript.JsTypeAttribute::set_IgnoreGenericMethodArguments(System.Boolean)
extern "C"  void JsTypeAttribute_set_IgnoreGenericMethodArguments_m1144442296 (JsTypeAttribute_t3342267407 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SharpKit.JavaScript.JsTypeAttribute::set_Mode(SharpKit.JavaScript.JsMode)
extern "C"  void JsTypeAttribute_set_Mode_m4122075689 (JsTypeAttribute_t3342267407 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SharpKit.JavaScript.JsTypeAttribute::set_InlineFields(System.Boolean)
extern "C"  void JsTypeAttribute_set_InlineFields_m597255514 (JsTypeAttribute_t3342267407 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SharpKit.JavaScript.JsTypeAttribute::set_NativeCasts(System.Boolean)
extern "C"  void JsTypeAttribute_set_NativeCasts_m1613578757 (JsTypeAttribute_t3342267407 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SharpKit.JavaScript.JsTypeAttribute::GoNative()
extern "C"  void JsTypeAttribute_GoNative_m4128470995 (JsTypeAttribute_t3342267407 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SharpKit.JavaScript.JsTypeAttribute::ApplyJsMode()
extern "C"  void JsTypeAttribute_ApplyJsMode_m1729871304 (JsTypeAttribute_t3342267407 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
