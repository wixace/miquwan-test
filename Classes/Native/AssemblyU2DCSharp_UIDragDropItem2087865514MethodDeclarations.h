﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIDragDropItem
struct UIDragDropItem_t2087865514;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// UIButton
struct UIButton_t179429094;
// UITable
struct UITable_t298892698;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "AssemblyU2DCSharp_UIDragDropItem2087865514.h"
#include "AssemblyU2DCSharp_UIButton179429094.h"
#include "AssemblyU2DCSharp_UITable298892698.h"

// System.Void UIDragDropItem::.ctor()
extern "C"  void UIDragDropItem__ctor_m3493732145 (UIDragDropItem_t2087865514 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDragDropItem::Start()
extern "C"  void UIDragDropItem_Start_m2440869937 (UIDragDropItem_t2087865514 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDragDropItem::OnPress(System.Boolean)
extern "C"  void UIDragDropItem_OnPress_m4222630314 (UIDragDropItem_t2087865514 * __this, bool ___isPressed0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDragDropItem::Update()
extern "C"  void UIDragDropItem_Update_m2658376188 (UIDragDropItem_t2087865514 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDragDropItem::OnDragStart()
extern "C"  void UIDragDropItem_OnDragStart_m1637873278 (UIDragDropItem_t2087865514 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDragDropItem::StartDragging()
extern "C"  void UIDragDropItem_StartDragging_m1083976160 (UIDragDropItem_t2087865514 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDragDropItem::OnDrag(UnityEngine.Vector2)
extern "C"  void UIDragDropItem_OnDrag_m3796296596 (UIDragDropItem_t2087865514 * __this, Vector2_t4282066565  ___delta0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDragDropItem::OnDragEnd()
extern "C"  void UIDragDropItem_OnDragEnd_m1043344247 (UIDragDropItem_t2087865514 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDragDropItem::StopDragging(UnityEngine.GameObject)
extern "C"  void UIDragDropItem_StopDragging_m1493278108 (UIDragDropItem_t2087865514 * __this, GameObject_t3674682005 * ___go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDragDropItem::OnDragDropStart()
extern "C"  void UIDragDropItem_OnDragDropStart_m1760837391 (UIDragDropItem_t2087865514 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDragDropItem::OnDragDropMove(UnityEngine.Vector2)
extern "C"  void UIDragDropItem_OnDragDropMove_m3596760340 (UIDragDropItem_t2087865514 * __this, Vector2_t4282066565  ___delta0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDragDropItem::OnDragDropRelease(UnityEngine.GameObject)
extern "C"  void UIDragDropItem_OnDragDropRelease_m146133740 (UIDragDropItem_t2087865514 * __this, GameObject_t3674682005 * ___surface0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDragDropItem::OnDragDropEnd()
extern "C"  void UIDragDropItem_OnDragDropEnd_m3631178824 (UIDragDropItem_t2087865514 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator UIDragDropItem::EnableDragScrollView()
extern "C"  Il2CppObject * UIDragDropItem_EnableDragScrollView_m15010244 (UIDragDropItem_t2087865514 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UIDragDropItem::ilo_get_time1()
extern "C"  float UIDragDropItem_ilo_get_time1_m4046164523 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDragDropItem::ilo_StartDragging2(UIDragDropItem)
extern "C"  void UIDragDropItem_ilo_StartDragging2_m1378941693 (Il2CppObject * __this /* static, unused */, UIDragDropItem_t2087865514 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UIDragDropItem::ilo_AddChild3(UnityEngine.GameObject,UnityEngine.GameObject)
extern "C"  GameObject_t3674682005 * UIDragDropItem_ilo_AddChild3_m2817636839 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___parent0, GameObject_t3674682005 * ___prefab1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDragDropItem::ilo_OnDragDropStart4(UIDragDropItem)
extern "C"  void UIDragDropItem_ilo_OnDragDropStart4_m2328694064 (Il2CppObject * __this /* static, unused */, UIDragDropItem_t2087865514 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDragDropItem::ilo_OnDragDropRelease5(UIDragDropItem,UnityEngine.GameObject)
extern "C"  void UIDragDropItem_ilo_OnDragDropRelease5_m869698962 (Il2CppObject * __this /* static, unused */, UIDragDropItem_t2087865514 * ____this0, GameObject_t3674682005 * ___surface1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDragDropItem::ilo_set_isEnabled6(UIButton,System.Boolean)
extern "C"  void UIDragDropItem_ilo_set_isEnabled6_m2197016897 (Il2CppObject * __this /* static, unused */, UIButton_t179429094 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDragDropItem::ilo_MarkParentAsChanged7(UnityEngine.GameObject)
extern "C"  void UIDragDropItem_ilo_MarkParentAsChanged7_m1740235626 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator UIDragDropItem::ilo_EnableDragScrollView8(UIDragDropItem)
extern "C"  Il2CppObject * UIDragDropItem_ilo_EnableDragScrollView8_m649220953 (Il2CppObject * __this /* static, unused */, UIDragDropItem_t2087865514 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDragDropItem::ilo_set_repositionNow9(UITable,System.Boolean)
extern "C"  void UIDragDropItem_ilo_set_repositionNow9_m3915778055 (Il2CppObject * __this /* static, unused */, UITable_t298892698 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDragDropItem::ilo_OnDragDropEnd10(UIDragDropItem)
extern "C"  void UIDragDropItem_ilo_OnDragDropEnd10_m2942108010 (Il2CppObject * __this /* static, unused */, UIDragDropItem_t2087865514 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
