﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType1744280289.h"
#include "UnityEngine_UnityEngine_OffMeshLinkType3329924262.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.OffMeshLinkData
struct  OffMeshLinkData_t3329424662 
{
public:
	// System.Int32 UnityEngine.OffMeshLinkData::m_Valid
	int32_t ___m_Valid_0;
	// System.Int32 UnityEngine.OffMeshLinkData::m_Activated
	int32_t ___m_Activated_1;
	// System.Int32 UnityEngine.OffMeshLinkData::m_InstanceID
	int32_t ___m_InstanceID_2;
	// UnityEngine.OffMeshLinkType UnityEngine.OffMeshLinkData::m_LinkType
	int32_t ___m_LinkType_3;
	// UnityEngine.Vector3 UnityEngine.OffMeshLinkData::m_StartPos
	Vector3_t4282066566  ___m_StartPos_4;
	// UnityEngine.Vector3 UnityEngine.OffMeshLinkData::m_EndPos
	Vector3_t4282066566  ___m_EndPos_5;

public:
	inline static int32_t get_offset_of_m_Valid_0() { return static_cast<int32_t>(offsetof(OffMeshLinkData_t3329424662, ___m_Valid_0)); }
	inline int32_t get_m_Valid_0() const { return ___m_Valid_0; }
	inline int32_t* get_address_of_m_Valid_0() { return &___m_Valid_0; }
	inline void set_m_Valid_0(int32_t value)
	{
		___m_Valid_0 = value;
	}

	inline static int32_t get_offset_of_m_Activated_1() { return static_cast<int32_t>(offsetof(OffMeshLinkData_t3329424662, ___m_Activated_1)); }
	inline int32_t get_m_Activated_1() const { return ___m_Activated_1; }
	inline int32_t* get_address_of_m_Activated_1() { return &___m_Activated_1; }
	inline void set_m_Activated_1(int32_t value)
	{
		___m_Activated_1 = value;
	}

	inline static int32_t get_offset_of_m_InstanceID_2() { return static_cast<int32_t>(offsetof(OffMeshLinkData_t3329424662, ___m_InstanceID_2)); }
	inline int32_t get_m_InstanceID_2() const { return ___m_InstanceID_2; }
	inline int32_t* get_address_of_m_InstanceID_2() { return &___m_InstanceID_2; }
	inline void set_m_InstanceID_2(int32_t value)
	{
		___m_InstanceID_2 = value;
	}

	inline static int32_t get_offset_of_m_LinkType_3() { return static_cast<int32_t>(offsetof(OffMeshLinkData_t3329424662, ___m_LinkType_3)); }
	inline int32_t get_m_LinkType_3() const { return ___m_LinkType_3; }
	inline int32_t* get_address_of_m_LinkType_3() { return &___m_LinkType_3; }
	inline void set_m_LinkType_3(int32_t value)
	{
		___m_LinkType_3 = value;
	}

	inline static int32_t get_offset_of_m_StartPos_4() { return static_cast<int32_t>(offsetof(OffMeshLinkData_t3329424662, ___m_StartPos_4)); }
	inline Vector3_t4282066566  get_m_StartPos_4() const { return ___m_StartPos_4; }
	inline Vector3_t4282066566 * get_address_of_m_StartPos_4() { return &___m_StartPos_4; }
	inline void set_m_StartPos_4(Vector3_t4282066566  value)
	{
		___m_StartPos_4 = value;
	}

	inline static int32_t get_offset_of_m_EndPos_5() { return static_cast<int32_t>(offsetof(OffMeshLinkData_t3329424662, ___m_EndPos_5)); }
	inline Vector3_t4282066566  get_m_EndPos_5() const { return ___m_EndPos_5; }
	inline Vector3_t4282066566 * get_address_of_m_EndPos_5() { return &___m_EndPos_5; }
	inline void set_m_EndPos_5(Vector3_t4282066566  value)
	{
		___m_EndPos_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for marshalling of: UnityEngine.OffMeshLinkData
struct OffMeshLinkData_t3329424662_marshaled_pinvoke
{
	int32_t ___m_Valid_0;
	int32_t ___m_Activated_1;
	int32_t ___m_InstanceID_2;
	int32_t ___m_LinkType_3;
	Vector3_t4282066566_marshaled_pinvoke ___m_StartPos_4;
	Vector3_t4282066566_marshaled_pinvoke ___m_EndPos_5;
};
// Native definition for marshalling of: UnityEngine.OffMeshLinkData
struct OffMeshLinkData_t3329424662_marshaled_com
{
	int32_t ___m_Valid_0;
	int32_t ___m_Activated_1;
	int32_t ___m_InstanceID_2;
	int32_t ___m_LinkType_3;
	Vector3_t4282066566_marshaled_com ___m_StartPos_4;
	Vector3_t4282066566_marshaled_com ___m_EndPos_5;
};
