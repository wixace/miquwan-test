﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LanguageSelection
struct LanguageSelection_t764444916;
// System.String[]
struct StringU5BU5D_t4054002952;
// UIPopupList
struct UIPopupList_t1804933942;
// System.String
struct String_t;
// EventDelegate
struct EventDelegate_t4004424223;
// System.Collections.Generic.List`1<EventDelegate>
struct List_1_t1077642479;
// EventDelegate/Callback
struct Callback_t1094463061;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_UIPopupList1804933942.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_EventDelegate_Callback1094463061.h"

// System.Void LanguageSelection::.ctor()
extern "C"  void LanguageSelection__ctor_m986906359 (LanguageSelection_t764444916 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LanguageSelection::Start()
extern "C"  void LanguageSelection_Start_m4229011447 (LanguageSelection_t764444916 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LanguageSelection::OnChange()
extern "C"  void LanguageSelection_OnChange_m3738003484 (LanguageSelection_t764444916 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] LanguageSelection::ilo_get_knownLanguages1()
extern "C"  StringU5BU5D_t4054002952* LanguageSelection_ilo_get_knownLanguages1_m3841467859 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LanguageSelection::ilo_set_value2(UIPopupList,System.String)
extern "C"  void LanguageSelection_ilo_set_value2_m669906586 (Il2CppObject * __this /* static, unused */, UIPopupList_t1804933942 * ____this0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// EventDelegate LanguageSelection::ilo_Add3(System.Collections.Generic.List`1<EventDelegate>,EventDelegate/Callback)
extern "C"  EventDelegate_t4004424223 * LanguageSelection_ilo_Add3_m2909541264 (Il2CppObject * __this /* static, unused */, List_1_t1077642479 * ___list0, Callback_t1094463061 * ___callback1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LanguageSelection::ilo_set_language4(System.String)
extern "C"  void LanguageSelection_ilo_set_language4_m918651009 (Il2CppObject * __this /* static, unused */, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
