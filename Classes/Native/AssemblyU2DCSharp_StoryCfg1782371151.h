﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "AssemblyU2DCSharp_CsCfgBase69924517.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StoryCfg
struct  StoryCfg_t1782371151  : public CsCfgBase_t69924517
{
public:
	// System.Int32 StoryCfg::id
	int32_t ___id_0;
	// System.String StoryCfg::content
	String_t* ___content_1;
	// System.Int32 StoryCfg::count
	int32_t ___count_2;
	// System.Int32 StoryCfg::condition1
	int32_t ___condition1_3;
	// System.Int32 StoryCfg::parameter1
	int32_t ___parameter1_4;
	// System.Int32 StoryCfg::condition2
	int32_t ___condition2_5;
	// System.Int32 StoryCfg::parameter2
	int32_t ___parameter2_6;
	// System.Int32 StoryCfg::condition3
	int32_t ___condition3_7;
	// System.String StoryCfg::parameter3
	String_t* ___parameter3_8;
	// System.Int32 StoryCfg::condition4
	int32_t ___condition4_9;
	// System.String StoryCfg::parameter4
	String_t* ___parameter4_10;
	// System.Int32 StoryCfg::condition5
	int32_t ___condition5_11;
	// System.String StoryCfg::parameter5
	String_t* ___parameter5_12;
	// System.Int32 StoryCfg::condition6
	int32_t ___condition6_13;
	// System.String StoryCfg::parameter6
	String_t* ___parameter6_14;
	// System.Int32 StoryCfg::condition7
	int32_t ___condition7_15;
	// System.Int32 StoryCfg::parameter7
	int32_t ___parameter7_16;
	// System.Int32 StoryCfg::condition8
	int32_t ___condition8_17;
	// System.Int32 StoryCfg::parameter8
	int32_t ___parameter8_18;
	// System.Int32 StoryCfg::condition9
	int32_t ___condition9_19;
	// System.Int32 StoryCfg::parameter9
	int32_t ___parameter9_20;
	// System.Int32 StoryCfg::condition10
	int32_t ___condition10_21;
	// System.Int32 StoryCfg::parameter10
	int32_t ___parameter10_22;
	// System.Int32 StoryCfg::condition11
	int32_t ___condition11_23;
	// System.Int32 StoryCfg::parameter11
	int32_t ___parameter11_24;
	// System.Int32 StoryCfg::condition12
	int32_t ___condition12_25;
	// System.Int32 StoryCfg::parameter12
	int32_t ___parameter12_26;
	// System.Single StoryCfg::delay_time
	float ___delay_time_27;
	// System.Single StoryCfg::lefeTime
	float ___lefeTime_28;
	// System.Single StoryCfg::OnRunTime
	float ___OnRunTime_29;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(StoryCfg_t1782371151, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_content_1() { return static_cast<int32_t>(offsetof(StoryCfg_t1782371151, ___content_1)); }
	inline String_t* get_content_1() const { return ___content_1; }
	inline String_t** get_address_of_content_1() { return &___content_1; }
	inline void set_content_1(String_t* value)
	{
		___content_1 = value;
		Il2CppCodeGenWriteBarrier(&___content_1, value);
	}

	inline static int32_t get_offset_of_count_2() { return static_cast<int32_t>(offsetof(StoryCfg_t1782371151, ___count_2)); }
	inline int32_t get_count_2() const { return ___count_2; }
	inline int32_t* get_address_of_count_2() { return &___count_2; }
	inline void set_count_2(int32_t value)
	{
		___count_2 = value;
	}

	inline static int32_t get_offset_of_condition1_3() { return static_cast<int32_t>(offsetof(StoryCfg_t1782371151, ___condition1_3)); }
	inline int32_t get_condition1_3() const { return ___condition1_3; }
	inline int32_t* get_address_of_condition1_3() { return &___condition1_3; }
	inline void set_condition1_3(int32_t value)
	{
		___condition1_3 = value;
	}

	inline static int32_t get_offset_of_parameter1_4() { return static_cast<int32_t>(offsetof(StoryCfg_t1782371151, ___parameter1_4)); }
	inline int32_t get_parameter1_4() const { return ___parameter1_4; }
	inline int32_t* get_address_of_parameter1_4() { return &___parameter1_4; }
	inline void set_parameter1_4(int32_t value)
	{
		___parameter1_4 = value;
	}

	inline static int32_t get_offset_of_condition2_5() { return static_cast<int32_t>(offsetof(StoryCfg_t1782371151, ___condition2_5)); }
	inline int32_t get_condition2_5() const { return ___condition2_5; }
	inline int32_t* get_address_of_condition2_5() { return &___condition2_5; }
	inline void set_condition2_5(int32_t value)
	{
		___condition2_5 = value;
	}

	inline static int32_t get_offset_of_parameter2_6() { return static_cast<int32_t>(offsetof(StoryCfg_t1782371151, ___parameter2_6)); }
	inline int32_t get_parameter2_6() const { return ___parameter2_6; }
	inline int32_t* get_address_of_parameter2_6() { return &___parameter2_6; }
	inline void set_parameter2_6(int32_t value)
	{
		___parameter2_6 = value;
	}

	inline static int32_t get_offset_of_condition3_7() { return static_cast<int32_t>(offsetof(StoryCfg_t1782371151, ___condition3_7)); }
	inline int32_t get_condition3_7() const { return ___condition3_7; }
	inline int32_t* get_address_of_condition3_7() { return &___condition3_7; }
	inline void set_condition3_7(int32_t value)
	{
		___condition3_7 = value;
	}

	inline static int32_t get_offset_of_parameter3_8() { return static_cast<int32_t>(offsetof(StoryCfg_t1782371151, ___parameter3_8)); }
	inline String_t* get_parameter3_8() const { return ___parameter3_8; }
	inline String_t** get_address_of_parameter3_8() { return &___parameter3_8; }
	inline void set_parameter3_8(String_t* value)
	{
		___parameter3_8 = value;
		Il2CppCodeGenWriteBarrier(&___parameter3_8, value);
	}

	inline static int32_t get_offset_of_condition4_9() { return static_cast<int32_t>(offsetof(StoryCfg_t1782371151, ___condition4_9)); }
	inline int32_t get_condition4_9() const { return ___condition4_9; }
	inline int32_t* get_address_of_condition4_9() { return &___condition4_9; }
	inline void set_condition4_9(int32_t value)
	{
		___condition4_9 = value;
	}

	inline static int32_t get_offset_of_parameter4_10() { return static_cast<int32_t>(offsetof(StoryCfg_t1782371151, ___parameter4_10)); }
	inline String_t* get_parameter4_10() const { return ___parameter4_10; }
	inline String_t** get_address_of_parameter4_10() { return &___parameter4_10; }
	inline void set_parameter4_10(String_t* value)
	{
		___parameter4_10 = value;
		Il2CppCodeGenWriteBarrier(&___parameter4_10, value);
	}

	inline static int32_t get_offset_of_condition5_11() { return static_cast<int32_t>(offsetof(StoryCfg_t1782371151, ___condition5_11)); }
	inline int32_t get_condition5_11() const { return ___condition5_11; }
	inline int32_t* get_address_of_condition5_11() { return &___condition5_11; }
	inline void set_condition5_11(int32_t value)
	{
		___condition5_11 = value;
	}

	inline static int32_t get_offset_of_parameter5_12() { return static_cast<int32_t>(offsetof(StoryCfg_t1782371151, ___parameter5_12)); }
	inline String_t* get_parameter5_12() const { return ___parameter5_12; }
	inline String_t** get_address_of_parameter5_12() { return &___parameter5_12; }
	inline void set_parameter5_12(String_t* value)
	{
		___parameter5_12 = value;
		Il2CppCodeGenWriteBarrier(&___parameter5_12, value);
	}

	inline static int32_t get_offset_of_condition6_13() { return static_cast<int32_t>(offsetof(StoryCfg_t1782371151, ___condition6_13)); }
	inline int32_t get_condition6_13() const { return ___condition6_13; }
	inline int32_t* get_address_of_condition6_13() { return &___condition6_13; }
	inline void set_condition6_13(int32_t value)
	{
		___condition6_13 = value;
	}

	inline static int32_t get_offset_of_parameter6_14() { return static_cast<int32_t>(offsetof(StoryCfg_t1782371151, ___parameter6_14)); }
	inline String_t* get_parameter6_14() const { return ___parameter6_14; }
	inline String_t** get_address_of_parameter6_14() { return &___parameter6_14; }
	inline void set_parameter6_14(String_t* value)
	{
		___parameter6_14 = value;
		Il2CppCodeGenWriteBarrier(&___parameter6_14, value);
	}

	inline static int32_t get_offset_of_condition7_15() { return static_cast<int32_t>(offsetof(StoryCfg_t1782371151, ___condition7_15)); }
	inline int32_t get_condition7_15() const { return ___condition7_15; }
	inline int32_t* get_address_of_condition7_15() { return &___condition7_15; }
	inline void set_condition7_15(int32_t value)
	{
		___condition7_15 = value;
	}

	inline static int32_t get_offset_of_parameter7_16() { return static_cast<int32_t>(offsetof(StoryCfg_t1782371151, ___parameter7_16)); }
	inline int32_t get_parameter7_16() const { return ___parameter7_16; }
	inline int32_t* get_address_of_parameter7_16() { return &___parameter7_16; }
	inline void set_parameter7_16(int32_t value)
	{
		___parameter7_16 = value;
	}

	inline static int32_t get_offset_of_condition8_17() { return static_cast<int32_t>(offsetof(StoryCfg_t1782371151, ___condition8_17)); }
	inline int32_t get_condition8_17() const { return ___condition8_17; }
	inline int32_t* get_address_of_condition8_17() { return &___condition8_17; }
	inline void set_condition8_17(int32_t value)
	{
		___condition8_17 = value;
	}

	inline static int32_t get_offset_of_parameter8_18() { return static_cast<int32_t>(offsetof(StoryCfg_t1782371151, ___parameter8_18)); }
	inline int32_t get_parameter8_18() const { return ___parameter8_18; }
	inline int32_t* get_address_of_parameter8_18() { return &___parameter8_18; }
	inline void set_parameter8_18(int32_t value)
	{
		___parameter8_18 = value;
	}

	inline static int32_t get_offset_of_condition9_19() { return static_cast<int32_t>(offsetof(StoryCfg_t1782371151, ___condition9_19)); }
	inline int32_t get_condition9_19() const { return ___condition9_19; }
	inline int32_t* get_address_of_condition9_19() { return &___condition9_19; }
	inline void set_condition9_19(int32_t value)
	{
		___condition9_19 = value;
	}

	inline static int32_t get_offset_of_parameter9_20() { return static_cast<int32_t>(offsetof(StoryCfg_t1782371151, ___parameter9_20)); }
	inline int32_t get_parameter9_20() const { return ___parameter9_20; }
	inline int32_t* get_address_of_parameter9_20() { return &___parameter9_20; }
	inline void set_parameter9_20(int32_t value)
	{
		___parameter9_20 = value;
	}

	inline static int32_t get_offset_of_condition10_21() { return static_cast<int32_t>(offsetof(StoryCfg_t1782371151, ___condition10_21)); }
	inline int32_t get_condition10_21() const { return ___condition10_21; }
	inline int32_t* get_address_of_condition10_21() { return &___condition10_21; }
	inline void set_condition10_21(int32_t value)
	{
		___condition10_21 = value;
	}

	inline static int32_t get_offset_of_parameter10_22() { return static_cast<int32_t>(offsetof(StoryCfg_t1782371151, ___parameter10_22)); }
	inline int32_t get_parameter10_22() const { return ___parameter10_22; }
	inline int32_t* get_address_of_parameter10_22() { return &___parameter10_22; }
	inline void set_parameter10_22(int32_t value)
	{
		___parameter10_22 = value;
	}

	inline static int32_t get_offset_of_condition11_23() { return static_cast<int32_t>(offsetof(StoryCfg_t1782371151, ___condition11_23)); }
	inline int32_t get_condition11_23() const { return ___condition11_23; }
	inline int32_t* get_address_of_condition11_23() { return &___condition11_23; }
	inline void set_condition11_23(int32_t value)
	{
		___condition11_23 = value;
	}

	inline static int32_t get_offset_of_parameter11_24() { return static_cast<int32_t>(offsetof(StoryCfg_t1782371151, ___parameter11_24)); }
	inline int32_t get_parameter11_24() const { return ___parameter11_24; }
	inline int32_t* get_address_of_parameter11_24() { return &___parameter11_24; }
	inline void set_parameter11_24(int32_t value)
	{
		___parameter11_24 = value;
	}

	inline static int32_t get_offset_of_condition12_25() { return static_cast<int32_t>(offsetof(StoryCfg_t1782371151, ___condition12_25)); }
	inline int32_t get_condition12_25() const { return ___condition12_25; }
	inline int32_t* get_address_of_condition12_25() { return &___condition12_25; }
	inline void set_condition12_25(int32_t value)
	{
		___condition12_25 = value;
	}

	inline static int32_t get_offset_of_parameter12_26() { return static_cast<int32_t>(offsetof(StoryCfg_t1782371151, ___parameter12_26)); }
	inline int32_t get_parameter12_26() const { return ___parameter12_26; }
	inline int32_t* get_address_of_parameter12_26() { return &___parameter12_26; }
	inline void set_parameter12_26(int32_t value)
	{
		___parameter12_26 = value;
	}

	inline static int32_t get_offset_of_delay_time_27() { return static_cast<int32_t>(offsetof(StoryCfg_t1782371151, ___delay_time_27)); }
	inline float get_delay_time_27() const { return ___delay_time_27; }
	inline float* get_address_of_delay_time_27() { return &___delay_time_27; }
	inline void set_delay_time_27(float value)
	{
		___delay_time_27 = value;
	}

	inline static int32_t get_offset_of_lefeTime_28() { return static_cast<int32_t>(offsetof(StoryCfg_t1782371151, ___lefeTime_28)); }
	inline float get_lefeTime_28() const { return ___lefeTime_28; }
	inline float* get_address_of_lefeTime_28() { return &___lefeTime_28; }
	inline void set_lefeTime_28(float value)
	{
		___lefeTime_28 = value;
	}

	inline static int32_t get_offset_of_OnRunTime_29() { return static_cast<int32_t>(offsetof(StoryCfg_t1782371151, ___OnRunTime_29)); }
	inline float get_OnRunTime_29() const { return ___OnRunTime_29; }
	inline float* get_address_of_OnRunTime_29() { return &___OnRunTime_29; }
	inline void set_OnRunTime_29(float value)
	{
		___OnRunTime_29 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
