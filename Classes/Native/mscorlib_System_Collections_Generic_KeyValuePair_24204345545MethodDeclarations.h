﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24204345545.h"
#include "AssemblyU2DCSharp_AnimationRunner_AniType1006238651.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System.Collections.Generic.KeyValuePair`2<AnimationRunner/AniType,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m1933297218_gshared (KeyValuePair_2_t4204345545 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define KeyValuePair_2__ctor_m1933297218(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t4204345545 *, int32_t, Il2CppObject *, const MethodInfo*))KeyValuePair_2__ctor_m1933297218_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<AnimationRunner/AniType,System.Object>::get_Key()
extern "C"  int32_t KeyValuePair_2_get_Key_m3772685030_gshared (KeyValuePair_2_t4204345545 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m3772685030(__this, method) ((  int32_t (*) (KeyValuePair_2_t4204345545 *, const MethodInfo*))KeyValuePair_2_get_Key_m3772685030_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<AnimationRunner/AniType,System.Object>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m3195876007_gshared (KeyValuePair_2_t4204345545 * __this, int32_t ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m3195876007(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t4204345545 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m3195876007_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<AnimationRunner/AniType,System.Object>::get_Value()
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m3039564682_gshared (KeyValuePair_2_t4204345545 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m3039564682(__this, method) ((  Il2CppObject * (*) (KeyValuePair_2_t4204345545 *, const MethodInfo*))KeyValuePair_2_get_Value_m3039564682_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<AnimationRunner/AniType,System.Object>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m4129348391_gshared (KeyValuePair_2_t4204345545 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m4129348391(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t4204345545 *, Il2CppObject *, const MethodInfo*))KeyValuePair_2_set_Value_m4129348391_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<AnimationRunner/AniType,System.Object>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m3322470913_gshared (KeyValuePair_2_t4204345545 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m3322470913(__this, method) ((  String_t* (*) (KeyValuePair_2_t4204345545 *, const MethodInfo*))KeyValuePair_2_ToString_m3322470913_gshared)(__this, method)
