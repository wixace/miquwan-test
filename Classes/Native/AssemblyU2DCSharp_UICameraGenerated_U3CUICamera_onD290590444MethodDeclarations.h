﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UICameraGenerated/<UICamera_onDragStart_GetDelegate_member49_arg0>c__AnonStoreyAA
struct U3CUICamera_onDragStart_GetDelegate_member49_arg0U3Ec__AnonStoreyAA_t290590444;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"

// System.Void UICameraGenerated/<UICamera_onDragStart_GetDelegate_member49_arg0>c__AnonStoreyAA::.ctor()
extern "C"  void U3CUICamera_onDragStart_GetDelegate_member49_arg0U3Ec__AnonStoreyAA__ctor_m3037358079 (U3CUICamera_onDragStart_GetDelegate_member49_arg0U3Ec__AnonStoreyAA_t290590444 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICameraGenerated/<UICamera_onDragStart_GetDelegate_member49_arg0>c__AnonStoreyAA::<>m__118(UnityEngine.GameObject)
extern "C"  void U3CUICamera_onDragStart_GetDelegate_member49_arg0U3Ec__AnonStoreyAA_U3CU3Em__118_m1263575530 (U3CUICamera_onDragStart_GetDelegate_member49_arg0U3Ec__AnonStoreyAA_t290590444 * __this, GameObject_t3674682005 * ___go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
