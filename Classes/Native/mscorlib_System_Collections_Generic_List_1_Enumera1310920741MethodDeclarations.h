﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<Pathfinding.Voxels.ExtraMesh>
struct List_1_t1291247971;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1310920741.h"
#include "AssemblyU2DCSharp_Pathfinding_Voxels_ExtraMesh4218029715.h"

// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.Voxels.ExtraMesh>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m295747321_gshared (Enumerator_t1310920741 * __this, List_1_t1291247971 * ___l0, const MethodInfo* method);
#define Enumerator__ctor_m295747321(__this, ___l0, method) ((  void (*) (Enumerator_t1310920741 *, List_1_t1291247971 *, const MethodInfo*))Enumerator__ctor_m295747321_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.Voxels.ExtraMesh>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3283033529_gshared (Enumerator_t1310920741 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m3283033529(__this, method) ((  void (*) (Enumerator_t1310920741 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3283033529_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Pathfinding.Voxels.ExtraMesh>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1915187823_gshared (Enumerator_t1310920741 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1915187823(__this, method) ((  Il2CppObject * (*) (Enumerator_t1310920741 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1915187823_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.Voxels.ExtraMesh>::Dispose()
extern "C"  void Enumerator_Dispose_m2393758494_gshared (Enumerator_t1310920741 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m2393758494(__this, method) ((  void (*) (Enumerator_t1310920741 *, const MethodInfo*))Enumerator_Dispose_m2393758494_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.Voxels.ExtraMesh>::VerifyState()
extern "C"  void Enumerator_VerifyState_m3911375703_gshared (Enumerator_t1310920741 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m3911375703(__this, method) ((  void (*) (Enumerator_t1310920741 *, const MethodInfo*))Enumerator_VerifyState_m3911375703_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Pathfinding.Voxels.ExtraMesh>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1938416617_gshared (Enumerator_t1310920741 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m1938416617(__this, method) ((  bool (*) (Enumerator_t1310920741 *, const MethodInfo*))Enumerator_MoveNext_m1938416617_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Pathfinding.Voxels.ExtraMesh>::get_Current()
extern "C"  ExtraMesh_t4218029715  Enumerator_get_Current_m4221511024_gshared (Enumerator_t1310920741 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m4221511024(__this, method) ((  ExtraMesh_t4218029715  (*) (Enumerator_t1310920741 *, const MethodInfo*))Enumerator_get_Current_m4221511024_gshared)(__this, method)
