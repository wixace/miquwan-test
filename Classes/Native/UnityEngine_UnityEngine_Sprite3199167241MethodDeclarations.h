﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Sprite
struct Sprite_t3199167241;
// UnityEngine.Texture2D
struct Texture2D_t3884108195;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t4024180168;
// System.UInt16[]
struct UInt16U5BU5D_t801649474;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Texture2D3884108195.h"
#include "UnityEngine_UnityEngine_Rect4241904616.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "UnityEngine_UnityEngine_SpriteMeshType2570694128.h"
#include "UnityEngine_UnityEngine_Vector44282066567.h"
#include "UnityEngine_UnityEngine_Bounds2711641849.h"
#include "UnityEngine_UnityEngine_SpritePackingMode1573190749.h"
#include "UnityEngine_UnityEngine_SpritePackingRotation1204540344.h"
#include "UnityEngine_UnityEngine_Sprite3199167241.h"

// System.Void UnityEngine.Sprite::.ctor()
extern "C"  void Sprite__ctor_m156427976 (Sprite_t3199167241 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Sprite UnityEngine.Sprite::Create(UnityEngine.Texture2D,UnityEngine.Rect,UnityEngine.Vector2,System.Single,System.UInt32,UnityEngine.SpriteMeshType,UnityEngine.Vector4)
extern "C"  Sprite_t3199167241 * Sprite_Create_m1774002158 (Il2CppObject * __this /* static, unused */, Texture2D_t3884108195 * ___texture0, Rect_t4241904616  ___rect1, Vector2_t4282066565  ___pivot2, float ___pixelsPerUnit3, uint32_t ___extrude4, int32_t ___meshType5, Vector4_t4282066567  ___border6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Sprite UnityEngine.Sprite::Create(UnityEngine.Texture2D,UnityEngine.Rect,UnityEngine.Vector2,System.Single,System.UInt32,UnityEngine.SpriteMeshType)
extern "C"  Sprite_t3199167241 * Sprite_Create_m718155836 (Il2CppObject * __this /* static, unused */, Texture2D_t3884108195 * ___texture0, Rect_t4241904616  ___rect1, Vector2_t4282066565  ___pivot2, float ___pixelsPerUnit3, uint32_t ___extrude4, int32_t ___meshType5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Sprite UnityEngine.Sprite::Create(UnityEngine.Texture2D,UnityEngine.Rect,UnityEngine.Vector2,System.Single,System.UInt32)
extern "C"  Sprite_t3199167241 * Sprite_Create_m3017197671 (Il2CppObject * __this /* static, unused */, Texture2D_t3884108195 * ___texture0, Rect_t4241904616  ___rect1, Vector2_t4282066565  ___pivot2, float ___pixelsPerUnit3, uint32_t ___extrude4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Sprite UnityEngine.Sprite::Create(UnityEngine.Texture2D,UnityEngine.Rect,UnityEngine.Vector2,System.Single)
extern "C"  Sprite_t3199167241 * Sprite_Create_m2242007923 (Il2CppObject * __this /* static, unused */, Texture2D_t3884108195 * ___texture0, Rect_t4241904616  ___rect1, Vector2_t4282066565  ___pivot2, float ___pixelsPerUnit3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Sprite UnityEngine.Sprite::Create(UnityEngine.Texture2D,UnityEngine.Rect,UnityEngine.Vector2)
extern "C"  Sprite_t3199167241 * Sprite_Create_m278903054 (Il2CppObject * __this /* static, unused */, Texture2D_t3884108195 * ___texture0, Rect_t4241904616  ___rect1, Vector2_t4282066565  ___pivot2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Sprite UnityEngine.Sprite::INTERNAL_CALL_Create(UnityEngine.Texture2D,UnityEngine.Rect&,UnityEngine.Vector2&,System.Single,System.UInt32,UnityEngine.SpriteMeshType,UnityEngine.Vector4&)
extern "C"  Sprite_t3199167241 * Sprite_INTERNAL_CALL_Create_m1601644917 (Il2CppObject * __this /* static, unused */, Texture2D_t3884108195 * ___texture0, Rect_t4241904616 * ___rect1, Vector2_t4282066565 * ___pivot2, float ___pixelsPerUnit3, uint32_t ___extrude4, int32_t ___meshType5, Vector4_t4282066567 * ___border6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Bounds UnityEngine.Sprite::get_bounds()
extern "C"  Bounds_t2711641849  Sprite_get_bounds_m2293110365 (Sprite_t3199167241 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Sprite::INTERNAL_get_bounds(UnityEngine.Bounds&)
extern "C"  void Sprite_INTERNAL_get_bounds_m1917708210 (Sprite_t3199167241 * __this, Bounds_t2711641849 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect UnityEngine.Sprite::get_rect()
extern "C"  Rect_t4241904616  Sprite_get_rect_m132626493 (Sprite_t3199167241 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Sprite::INTERNAL_get_rect(UnityEngine.Rect&)
extern "C"  void Sprite_INTERNAL_get_rect_m853825042 (Sprite_t3199167241 * __this, Rect_t4241904616 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Sprite::get_pixelsPerUnit()
extern "C"  float Sprite_get_pixelsPerUnit_m2438708453 (Sprite_t3199167241 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture2D UnityEngine.Sprite::get_texture()
extern "C"  Texture2D_t3884108195 * Sprite_get_texture_m1481489947 (Sprite_t3199167241 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture2D UnityEngine.Sprite::get_associatedAlphaSplitTexture()
extern "C"  Texture2D_t3884108195 * Sprite_get_associatedAlphaSplitTexture_m4226901177 (Sprite_t3199167241 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect UnityEngine.Sprite::get_textureRect()
extern "C"  Rect_t4241904616  Sprite_get_textureRect_m3946160520 (Sprite_t3199167241 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Sprite::INTERNAL_get_textureRect(UnityEngine.Rect&)
extern "C"  void Sprite_INTERNAL_get_textureRect_m1939414807 (Sprite_t3199167241 * __this, Rect_t4241904616 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Sprite::get_textureRectOffset()
extern "C"  Vector2_t4282066565  Sprite_get_textureRectOffset_m4258956532 (Sprite_t3199167241 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Sprite::get_packed()
extern "C"  bool Sprite_get_packed_m2853429571 (Sprite_t3199167241 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.SpritePackingMode UnityEngine.Sprite::get_packingMode()
extern "C"  int32_t Sprite_get_packingMode_m2064215782 (Sprite_t3199167241 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.SpritePackingRotation UnityEngine.Sprite::get_packingRotation()
extern "C"  int32_t Sprite_get_packingRotation_m22845340 (Sprite_t3199167241 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Sprite::Internal_GetTextureRectOffset(UnityEngine.Sprite,UnityEngine.Vector2&)
extern "C"  void Sprite_Internal_GetTextureRectOffset_m3429532080 (Il2CppObject * __this /* static, unused */, Sprite_t3199167241 * ___sprite0, Vector2_t4282066565 * ___output1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Sprite::Internal_GetPivot(UnityEngine.Sprite,UnityEngine.Vector2&)
extern "C"  void Sprite_Internal_GetPivot_m468813056 (Il2CppObject * __this /* static, unused */, Sprite_t3199167241 * ___sprite0, Vector2_t4282066565 * ___output1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Sprite::get_pivot()
extern "C"  Vector2_t4282066565  Sprite_get_pivot_m2958889892 (Sprite_t3199167241 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector4 UnityEngine.Sprite::get_border()
extern "C"  Vector4_t4282066567  Sprite_get_border_m1562752938 (Sprite_t3199167241 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Sprite::INTERNAL_get_border(UnityEngine.Vector4&)
extern "C"  void Sprite_INTERNAL_get_border_m855941073 (Sprite_t3199167241 * __this, Vector4_t4282066567 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2[] UnityEngine.Sprite::get_vertices()
extern "C"  Vector2U5BU5D_t4024180168* Sprite_get_vertices_m299461399 (Sprite_t3199167241 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt16[] UnityEngine.Sprite::get_triangles()
extern "C"  UInt16U5BU5D_t801649474* Sprite_get_triangles_m4284805173 (Sprite_t3199167241 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2[] UnityEngine.Sprite::get_uv()
extern "C"  Vector2U5BU5D_t4024180168* Sprite_get_uv_m3920286079 (Sprite_t3199167241 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Sprite::OverrideGeometry(UnityEngine.Vector2[],System.UInt16[])
extern "C"  void Sprite_OverrideGeometry_m1609538954 (Sprite_t3199167241 * __this, Vector2U5BU5D_t4024180168* ___vertices0, UInt16U5BU5D_t801649474* ___triangles1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
