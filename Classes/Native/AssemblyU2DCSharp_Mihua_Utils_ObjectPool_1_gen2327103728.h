﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Stack`1<Buff>
struct Stack_1_t3100642831;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mihua.Utils.ObjectPool`1<Buff>
struct  ObjectPool_1_t2327103728  : public Il2CppObject
{
public:

public:
};

struct ObjectPool_1_t2327103728_StaticFields
{
public:
	// System.Int32 Mihua.Utils.ObjectPool`1::_maxCount
	int32_t ____maxCount_0;
	// System.Collections.Generic.Stack`1<T> Mihua.Utils.ObjectPool`1::pool
	Stack_1_t3100642831 * ___pool_1;
	// System.Int32 Mihua.Utils.ObjectPool`1::totalCreated
	int32_t ___totalCreated_2;

public:
	inline static int32_t get_offset_of__maxCount_0() { return static_cast<int32_t>(offsetof(ObjectPool_1_t2327103728_StaticFields, ____maxCount_0)); }
	inline int32_t get__maxCount_0() const { return ____maxCount_0; }
	inline int32_t* get_address_of__maxCount_0() { return &____maxCount_0; }
	inline void set__maxCount_0(int32_t value)
	{
		____maxCount_0 = value;
	}

	inline static int32_t get_offset_of_pool_1() { return static_cast<int32_t>(offsetof(ObjectPool_1_t2327103728_StaticFields, ___pool_1)); }
	inline Stack_1_t3100642831 * get_pool_1() const { return ___pool_1; }
	inline Stack_1_t3100642831 ** get_address_of_pool_1() { return &___pool_1; }
	inline void set_pool_1(Stack_1_t3100642831 * value)
	{
		___pool_1 = value;
		Il2CppCodeGenWriteBarrier(&___pool_1, value);
	}

	inline static int32_t get_offset_of_totalCreated_2() { return static_cast<int32_t>(offsetof(ObjectPool_1_t2327103728_StaticFields, ___totalCreated_2)); }
	inline int32_t get_totalCreated_2() const { return ___totalCreated_2; }
	inline int32_t* get_address_of_totalCreated_2() { return &___totalCreated_2; }
	inline void set_totalCreated_2(int32_t value)
	{
		___totalCreated_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
