﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GUIStyle
struct GUIStyle_t2990928826;
// System.Collections.Generic.List`1<RVOExampleAgent>
struct List_1_t2543093942;
// Pathfinding.RVO.Simulator
struct Simulator_t2705969170;
// UnityEngine.Camera
struct Camera_t2727095145;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GroupController
struct  GroupController_t813505371  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.GUIStyle GroupController::selectionBox
	GUIStyle_t2990928826 * ___selectionBox_3;
	// System.Boolean GroupController::adjustCamera
	bool ___adjustCamera_4;
	// UnityEngine.Vector2 GroupController::start
	Vector2_t4282066565  ___start_5;
	// UnityEngine.Vector2 GroupController::end
	Vector2_t4282066565  ___end_6;
	// System.Boolean GroupController::wasDown
	bool ___wasDown_7;
	// System.Collections.Generic.List`1<RVOExampleAgent> GroupController::selection
	List_1_t2543093942 * ___selection_8;
	// Pathfinding.RVO.Simulator GroupController::sim
	Simulator_t2705969170 * ___sim_9;
	// UnityEngine.Camera GroupController::cam
	Camera_t2727095145 * ___cam_10;

public:
	inline static int32_t get_offset_of_selectionBox_3() { return static_cast<int32_t>(offsetof(GroupController_t813505371, ___selectionBox_3)); }
	inline GUIStyle_t2990928826 * get_selectionBox_3() const { return ___selectionBox_3; }
	inline GUIStyle_t2990928826 ** get_address_of_selectionBox_3() { return &___selectionBox_3; }
	inline void set_selectionBox_3(GUIStyle_t2990928826 * value)
	{
		___selectionBox_3 = value;
		Il2CppCodeGenWriteBarrier(&___selectionBox_3, value);
	}

	inline static int32_t get_offset_of_adjustCamera_4() { return static_cast<int32_t>(offsetof(GroupController_t813505371, ___adjustCamera_4)); }
	inline bool get_adjustCamera_4() const { return ___adjustCamera_4; }
	inline bool* get_address_of_adjustCamera_4() { return &___adjustCamera_4; }
	inline void set_adjustCamera_4(bool value)
	{
		___adjustCamera_4 = value;
	}

	inline static int32_t get_offset_of_start_5() { return static_cast<int32_t>(offsetof(GroupController_t813505371, ___start_5)); }
	inline Vector2_t4282066565  get_start_5() const { return ___start_5; }
	inline Vector2_t4282066565 * get_address_of_start_5() { return &___start_5; }
	inline void set_start_5(Vector2_t4282066565  value)
	{
		___start_5 = value;
	}

	inline static int32_t get_offset_of_end_6() { return static_cast<int32_t>(offsetof(GroupController_t813505371, ___end_6)); }
	inline Vector2_t4282066565  get_end_6() const { return ___end_6; }
	inline Vector2_t4282066565 * get_address_of_end_6() { return &___end_6; }
	inline void set_end_6(Vector2_t4282066565  value)
	{
		___end_6 = value;
	}

	inline static int32_t get_offset_of_wasDown_7() { return static_cast<int32_t>(offsetof(GroupController_t813505371, ___wasDown_7)); }
	inline bool get_wasDown_7() const { return ___wasDown_7; }
	inline bool* get_address_of_wasDown_7() { return &___wasDown_7; }
	inline void set_wasDown_7(bool value)
	{
		___wasDown_7 = value;
	}

	inline static int32_t get_offset_of_selection_8() { return static_cast<int32_t>(offsetof(GroupController_t813505371, ___selection_8)); }
	inline List_1_t2543093942 * get_selection_8() const { return ___selection_8; }
	inline List_1_t2543093942 ** get_address_of_selection_8() { return &___selection_8; }
	inline void set_selection_8(List_1_t2543093942 * value)
	{
		___selection_8 = value;
		Il2CppCodeGenWriteBarrier(&___selection_8, value);
	}

	inline static int32_t get_offset_of_sim_9() { return static_cast<int32_t>(offsetof(GroupController_t813505371, ___sim_9)); }
	inline Simulator_t2705969170 * get_sim_9() const { return ___sim_9; }
	inline Simulator_t2705969170 ** get_address_of_sim_9() { return &___sim_9; }
	inline void set_sim_9(Simulator_t2705969170 * value)
	{
		___sim_9 = value;
		Il2CppCodeGenWriteBarrier(&___sim_9, value);
	}

	inline static int32_t get_offset_of_cam_10() { return static_cast<int32_t>(offsetof(GroupController_t813505371, ___cam_10)); }
	inline Camera_t2727095145 * get_cam_10() const { return ___cam_10; }
	inline Camera_t2727095145 ** get_address_of_cam_10() { return &___cam_10; }
	inline void set_cam_10(Camera_t2727095145 * value)
	{
		___cam_10 = value;
		Il2CppCodeGenWriteBarrier(&___cam_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
