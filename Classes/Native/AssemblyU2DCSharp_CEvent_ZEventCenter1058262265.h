﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// CEvent.EventDispatch`1<CEvent.ZEvent>
struct EventDispatch_1_t535157068;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CEvent.ZEventCenter
struct  ZEventCenter_t1058262265  : public Il2CppObject
{
public:

public:
};

struct ZEventCenter_t1058262265_StaticFields
{
public:
	// CEvent.EventDispatch`1<CEvent.ZEvent> CEvent.ZEventCenter::E
	EventDispatch_1_t535157068 * ___E_0;

public:
	inline static int32_t get_offset_of_E_0() { return static_cast<int32_t>(offsetof(ZEventCenter_t1058262265_StaticFields, ___E_0)); }
	inline EventDispatch_1_t535157068 * get_E_0() const { return ___E_0; }
	inline EventDispatch_1_t535157068 ** get_address_of_E_0() { return &___E_0; }
	inline void set_E_0(EventDispatch_1_t535157068 * value)
	{
		___E_0 = value;
		Il2CppCodeGenWriteBarrier(&___E_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
