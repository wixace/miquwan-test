﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.RecastGraph/SceneMesh
struct SceneMesh_t509761468;
struct SceneMesh_t509761468_marshaled_pinvoke;
struct SceneMesh_t509761468_marshaled_com;

#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct SceneMesh_t509761468;
struct SceneMesh_t509761468_marshaled_pinvoke;

extern "C" void SceneMesh_t509761468_marshal_pinvoke(const SceneMesh_t509761468& unmarshaled, SceneMesh_t509761468_marshaled_pinvoke& marshaled);
extern "C" void SceneMesh_t509761468_marshal_pinvoke_back(const SceneMesh_t509761468_marshaled_pinvoke& marshaled, SceneMesh_t509761468& unmarshaled);
extern "C" void SceneMesh_t509761468_marshal_pinvoke_cleanup(SceneMesh_t509761468_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct SceneMesh_t509761468;
struct SceneMesh_t509761468_marshaled_com;

extern "C" void SceneMesh_t509761468_marshal_com(const SceneMesh_t509761468& unmarshaled, SceneMesh_t509761468_marshaled_com& marshaled);
extern "C" void SceneMesh_t509761468_marshal_com_back(const SceneMesh_t509761468_marshaled_com& marshaled, SceneMesh_t509761468& unmarshaled);
extern "C" void SceneMesh_t509761468_marshal_com_cleanup(SceneMesh_t509761468_marshaled_com& marshaled);
