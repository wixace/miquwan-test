﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action_3_gen3896231253MethodDeclarations.h"

// System.Void System.Action`3<System.Int32,Skill,UnityEngine.Vector3>::.ctor(System.Object,System.IntPtr)
#define Action_3__ctor_m20723328(__this, ___object0, ___method1, method) ((  void (*) (Action_3_t429895135 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Action_3__ctor_m1323149667_gshared)(__this, ___object0, ___method1, method)
// System.Void System.Action`3<System.Int32,Skill,UnityEngine.Vector3>::Invoke(T1,T2,T3)
#define Action_3_Invoke_m2486765100(__this, ___arg10, ___arg21, ___arg32, method) ((  void (*) (Action_3_t429895135 *, int32_t, Skill_t79944241 *, Vector3_t4282066566 , const MethodInfo*))Action_3_Invoke_m1545623263_gshared)(__this, ___arg10, ___arg21, ___arg32, method)
// System.IAsyncResult System.Action`3<System.Int32,Skill,UnityEngine.Vector3>::BeginInvoke(T1,T2,T3,System.AsyncCallback,System.Object)
#define Action_3_BeginInvoke_m3180360155(__this, ___arg10, ___arg21, ___arg32, ___callback3, ___object4, method) ((  Il2CppObject * (*) (Action_3_t429895135 *, int32_t, Skill_t79944241 *, Vector3_t4282066566 , AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))Action_3_BeginInvoke_m333125518_gshared)(__this, ___arg10, ___arg21, ___arg32, ___callback3, ___object4, method)
// System.Void System.Action`3<System.Int32,Skill,UnityEngine.Vector3>::EndInvoke(System.IAsyncResult)
#define Action_3_EndInvoke_m4054100352(__this, ___result0, method) ((  void (*) (Action_3_t429895135 *, Il2CppObject *, const MethodInfo*))Action_3_EndInvoke_m3203089651_gshared)(__this, ___result0, method)
