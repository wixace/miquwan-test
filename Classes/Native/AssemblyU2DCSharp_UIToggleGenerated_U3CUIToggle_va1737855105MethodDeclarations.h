﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIToggleGenerated/<UIToggle_validator_GetDelegate_member11_arg0>c__AnonStoreyD1
struct U3CUIToggle_validator_GetDelegate_member11_arg0U3Ec__AnonStoreyD1_t1737855105;

#include "codegen/il2cpp-codegen.h"

// System.Void UIToggleGenerated/<UIToggle_validator_GetDelegate_member11_arg0>c__AnonStoreyD1::.ctor()
extern "C"  void U3CUIToggle_validator_GetDelegate_member11_arg0U3Ec__AnonStoreyD1__ctor_m4150982666 (U3CUIToggle_validator_GetDelegate_member11_arg0U3Ec__AnonStoreyD1_t1737855105 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIToggleGenerated/<UIToggle_validator_GetDelegate_member11_arg0>c__AnonStoreyD1::<>m__167(System.Boolean)
extern "C"  bool U3CUIToggle_validator_GetDelegate_member11_arg0U3Ec__AnonStoreyD1_U3CU3Em__167_m3241671076 (U3CUIToggle_validator_GetDelegate_member11_arg0U3Ec__AnonStoreyD1_t1737855105 * __this, bool ___choice0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
