﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.GraphNode
struct GraphNode_t23612370;
// System.Collections.Generic.List`1<Pathfinding.GraphNode>
struct List_1_t1391797922;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t1355284822;
// Pathfinding.IRaycastableGraph
struct IRaycastableGraph_t2032416694;
// Pathfinding.GraphNodeDelegate
struct GraphNodeDelegate_t1466738551;
// Pathfinding.TriangleMeshNode
struct TriangleMeshNode_t1626248749;
// Pathfinding.GridGraph
struct GridGraph_t2455707914;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphNode23612370.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphNodeDelegate1466738551.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphHitInfo3082627369.h"
#include "AssemblyU2DCSharp_Pathfinding_Int31974045594.h"
#include "AssemblyU2DCSharp_Pathfinding_TriangleMeshNode1626248749.h"

// System.Boolean Pathfinding.PathUtilities::IsPathPossible(Pathfinding.GraphNode,Pathfinding.GraphNode)
extern "C"  bool PathUtilities_IsPathPossible_m4146964754 (Il2CppObject * __this /* static, unused */, GraphNode_t23612370 * ___n10, GraphNode_t23612370 * ___n21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.PathUtilities::IsPathPossible(System.Collections.Generic.List`1<Pathfinding.GraphNode>)
extern "C"  bool PathUtilities_IsPathPossible_m3553166986 (Il2CppObject * __this /* static, unused */, List_1_t1391797922 * ___nodes0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<Pathfinding.GraphNode> Pathfinding.PathUtilities::GetReachableNodes(Pathfinding.GraphNode,System.Int32)
extern "C"  List_1_t1391797922 * PathUtilities_GetReachableNodes_m655147642 (Il2CppObject * __this /* static, unused */, GraphNode_t23612370 * ___seed0, int32_t ___tagMask1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<Pathfinding.GraphNode> Pathfinding.PathUtilities::BFS(Pathfinding.GraphNode,System.Int32,System.Int32)
extern "C"  List_1_t1391797922 * PathUtilities_BFS_m3356573672 (Il2CppObject * __this /* static, unused */, GraphNode_t23612370 * ___seed0, int32_t ___depth1, int32_t ___tagMask2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<UnityEngine.Vector3> Pathfinding.PathUtilities::GetSpiralPoints(System.Int32,System.Single)
extern "C"  List_1_t1355284822 * PathUtilities_GetSpiralPoints_m2353170760 (Il2CppObject * __this /* static, unused */, int32_t ___count0, float ___clearance1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Pathfinding.PathUtilities::InvoluteOfCircle(System.Single,System.Single)
extern "C"  Vector3_t4282066566  PathUtilities_InvoluteOfCircle_m283451191 (Il2CppObject * __this /* static, unused */, float ___a0, float ___t1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.PathUtilities::GetPointsAroundPointWorld(UnityEngine.Vector3,Pathfinding.IRaycastableGraph,System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Single,System.Single)
extern "C"  void PathUtilities_GetPointsAroundPointWorld_m1177519368 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___p0, Il2CppObject * ___g1, List_1_t1355284822 * ___previousPoints2, float ___radius3, float ___clearanceRadius4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.PathUtilities::GetPointsAroundPoint(UnityEngine.Vector3,Pathfinding.IRaycastableGraph,System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Single,System.Single)
extern "C"  void PathUtilities_GetPointsAroundPoint_m3205754672 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___p0, Il2CppObject * ___g1, List_1_t1355284822 * ___previousPoints2, float ___radius3, float ___clearanceRadius4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<UnityEngine.Vector3> Pathfinding.PathUtilities::GetPointsOnNodes(System.Collections.Generic.List`1<Pathfinding.GraphNode>,System.Int32,System.Single)
extern "C"  List_1_t1355284822 * PathUtilities_GetPointsOnNodes_m1049430623 (Il2CppObject * __this /* static, unused */, List_1_t1391797922 * ___nodes0, int32_t ___count1, float ___clearanceRadius2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Pathfinding.PathUtilities::ilo_get_Area1(Pathfinding.GraphNode)
extern "C"  uint32_t PathUtilities_ilo_get_Area1_m3968852091 (Il2CppObject * __this /* static, unused */, GraphNode_t23612370 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.PathUtilities::ilo_GetConnections2(Pathfinding.GraphNode,Pathfinding.GraphNodeDelegate)
extern "C"  void PathUtilities_ilo_GetConnections2_m3417223999 (Il2CppObject * __this /* static, unused */, GraphNode_t23612370 * ____this0, GraphNodeDelegate_t1466738551 * ___del1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.PathUtilities::ilo_Invoke3(Pathfinding.GraphNodeDelegate,Pathfinding.GraphNode)
extern "C"  void PathUtilities_ilo_Invoke3_m3217048375 (Il2CppObject * __this /* static, unused */, GraphNodeDelegate_t1466738551 * ____this0, GraphNode_t23612370 * ___node1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Pathfinding.PathUtilities::ilo_InvoluteOfCircle4(System.Single,System.Single)
extern "C"  Vector3_t4282066566  PathUtilities_ilo_InvoluteOfCircle4_m844203494 (Il2CppObject * __this /* static, unused */, float ___a0, float ___t1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.PathUtilities::ilo_Linecast5(Pathfinding.IRaycastableGraph,UnityEngine.Vector3,UnityEngine.Vector3,Pathfinding.GraphNode,Pathfinding.GraphHitInfo&)
extern "C"  bool PathUtilities_ilo_Linecast5_m1564547622 (Il2CppObject * __this /* static, unused */, Il2CppObject * ____this0, Vector3_t4282066566  ___start1, Vector3_t4282066566  ___end2, GraphNode_t23612370 * ___hint3, GraphHitInfo_t3082627369 * ___hit4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Int3 Pathfinding.PathUtilities::ilo_GetVertex6(Pathfinding.TriangleMeshNode,System.Int32)
extern "C"  Int3_t1974045594  PathUtilities_ilo_GetVertex6_m1836523716 (Il2CppObject * __this /* static, unused */, TriangleMeshNode_t1626248749 * ____this0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.GridGraph Pathfinding.PathUtilities::ilo_GetGridGraph7(System.UInt32)
extern "C"  GridGraph_t2455707914 * PathUtilities_ilo_GetGridGraph7_m1654003813 (Il2CppObject * __this /* static, unused */, uint32_t ___graphIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Pathfinding.PathUtilities::ilo_op_Explicit8(Pathfinding.Int3)
extern "C"  Vector3_t4282066566  PathUtilities_ilo_op_Explicit8_m502028677 (Il2CppObject * __this /* static, unused */, Int3_t1974045594  ___ob0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
