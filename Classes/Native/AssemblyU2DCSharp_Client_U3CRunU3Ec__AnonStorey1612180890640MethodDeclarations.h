﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Client/<Run>c__AnonStorey161
struct U3CRunU3Ec__AnonStorey161_t2180890640;
// Newtonsoft.Json.Linq.JObject
struct JObject_t1798544199;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JObject1798544199.h"

// System.Void Client/<Run>c__AnonStorey161::.ctor()
extern "C"  void U3CRunU3Ec__AnonStorey161__ctor_m3618772107 (U3CRunU3Ec__AnonStorey161_t2180890640 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Client/<Run>c__AnonStorey161::<>m__3FE()
extern "C"  void U3CRunU3Ec__AnonStorey161_U3CU3Em__3FE_m1698035584 (U3CRunU3Ec__AnonStorey161_t2180890640 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Client/<Run>c__AnonStorey161::<>m__400()
extern "C"  void U3CRunU3Ec__AnonStorey161_U3CU3Em__400_m1698283522 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Client/<Run>c__AnonStorey161::<>m__401(Newtonsoft.Json.Linq.JObject)
extern "C"  void U3CRunU3Ec__AnonStorey161_U3CU3Em__401_m1066449011 (U3CRunU3Ec__AnonStorey161_t2180890640 * __this, JObject_t1798544199 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Client/<Run>c__AnonStorey161::<>m__402()
extern "C"  void U3CRunU3Ec__AnonStorey161_U3CU3Em__402_m1698285444 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
