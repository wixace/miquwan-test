﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_ParticleEmitterGenerated
struct UnityEngine_ParticleEmitterGenerated_t319697359;
// JSVCall
struct JSVCall_t3708497963;
// UnityEngine.Particle[]
struct ParticleU5BU5D_t2879271823;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"

// System.Void UnityEngine_ParticleEmitterGenerated::.ctor()
extern "C"  void UnityEngine_ParticleEmitterGenerated__ctor_m4159476140 (UnityEngine_ParticleEmitterGenerated_t319697359 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleEmitterGenerated::ParticleEmitter_emit(JSVCall)
extern "C"  void UnityEngine_ParticleEmitterGenerated_ParticleEmitter_emit_m4079453733 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleEmitterGenerated::ParticleEmitter_minSize(JSVCall)
extern "C"  void UnityEngine_ParticleEmitterGenerated_ParticleEmitter_minSize_m2797609249 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleEmitterGenerated::ParticleEmitter_maxSize(JSVCall)
extern "C"  void UnityEngine_ParticleEmitterGenerated_ParticleEmitter_maxSize_m2861477711 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleEmitterGenerated::ParticleEmitter_minEnergy(JSVCall)
extern "C"  void UnityEngine_ParticleEmitterGenerated_ParticleEmitter_minEnergy_m988219258 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleEmitterGenerated::ParticleEmitter_maxEnergy(JSVCall)
extern "C"  void UnityEngine_ParticleEmitterGenerated_ParticleEmitter_maxEnergy_m2236269096 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleEmitterGenerated::ParticleEmitter_minEmission(JSVCall)
extern "C"  void UnityEngine_ParticleEmitterGenerated_ParticleEmitter_minEmission_m1360266331 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleEmitterGenerated::ParticleEmitter_maxEmission(JSVCall)
extern "C"  void UnityEngine_ParticleEmitterGenerated_ParticleEmitter_maxEmission_m2440285065 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleEmitterGenerated::ParticleEmitter_emitterVelocityScale(JSVCall)
extern "C"  void UnityEngine_ParticleEmitterGenerated_ParticleEmitter_emitterVelocityScale_m2721587673 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleEmitterGenerated::ParticleEmitter_worldVelocity(JSVCall)
extern "C"  void UnityEngine_ParticleEmitterGenerated_ParticleEmitter_worldVelocity_m2734431493 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleEmitterGenerated::ParticleEmitter_localVelocity(JSVCall)
extern "C"  void UnityEngine_ParticleEmitterGenerated_ParticleEmitter_localVelocity_m2533254732 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleEmitterGenerated::ParticleEmitter_rndVelocity(JSVCall)
extern "C"  void UnityEngine_ParticleEmitterGenerated_ParticleEmitter_rndVelocity_m2183209263 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleEmitterGenerated::ParticleEmitter_useWorldSpace(JSVCall)
extern "C"  void UnityEngine_ParticleEmitterGenerated_ParticleEmitter_useWorldSpace_m4178099097 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleEmitterGenerated::ParticleEmitter_rndRotation(JSVCall)
extern "C"  void UnityEngine_ParticleEmitterGenerated_ParticleEmitter_rndRotation_m4076516270 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleEmitterGenerated::ParticleEmitter_angularVelocity(JSVCall)
extern "C"  void UnityEngine_ParticleEmitterGenerated_ParticleEmitter_angularVelocity_m2130493749 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleEmitterGenerated::ParticleEmitter_rndAngularVelocity(JSVCall)
extern "C"  void UnityEngine_ParticleEmitterGenerated_ParticleEmitter_rndAngularVelocity_m3111466209 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleEmitterGenerated::ParticleEmitter_particles(JSVCall)
extern "C"  void UnityEngine_ParticleEmitterGenerated_ParticleEmitter_particles_m2966867367 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleEmitterGenerated::ParticleEmitter_particleCount(JSVCall)
extern "C"  void UnityEngine_ParticleEmitterGenerated_ParticleEmitter_particleCount_m3444473515 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleEmitterGenerated::ParticleEmitter_enabled(JSVCall)
extern "C"  void UnityEngine_ParticleEmitterGenerated_ParticleEmitter_enabled_m3370991379 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ParticleEmitterGenerated::ParticleEmitter_ClearParticles(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ParticleEmitterGenerated_ParticleEmitter_ClearParticles_m1861824115 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ParticleEmitterGenerated::ParticleEmitter_Emit__Vector3__Vector3__Single__Single__Color__Single__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ParticleEmitterGenerated_ParticleEmitter_Emit__Vector3__Vector3__Single__Single__Color__Single__Single_m4255812415 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ParticleEmitterGenerated::ParticleEmitter_Emit__Vector3__Vector3__Single__Single__Color(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ParticleEmitterGenerated_ParticleEmitter_Emit__Vector3__Vector3__Single__Single__Color_m1289876143 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ParticleEmitterGenerated::ParticleEmitter_Emit__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ParticleEmitterGenerated_ParticleEmitter_Emit__Int32_m3376390890 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ParticleEmitterGenerated::ParticleEmitter_Emit(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ParticleEmitterGenerated_ParticleEmitter_Emit_m3116334502 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ParticleEmitterGenerated::ParticleEmitter_Simulate__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ParticleEmitterGenerated_ParticleEmitter_Simulate__Single_m749290623 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleEmitterGenerated::__Register()
extern "C"  void UnityEngine_ParticleEmitterGenerated___Register_m359208603 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Particle[] UnityEngine_ParticleEmitterGenerated::<ParticleEmitter_particles>m__283()
extern "C"  ParticleU5BU5D_t2879271823* UnityEngine_ParticleEmitterGenerated_U3CParticleEmitter_particlesU3Em__283_m3157895120 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ParticleEmitterGenerated::ilo_getBooleanS1(System.Int32)
extern "C"  bool UnityEngine_ParticleEmitterGenerated_ilo_getBooleanS1_m1022075808 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleEmitterGenerated::ilo_setSingle2(System.Int32,System.Single)
extern "C"  void UnityEngine_ParticleEmitterGenerated_ilo_setSingle2_m4092767177 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine_ParticleEmitterGenerated::ilo_getSingle3(System.Int32)
extern "C"  float UnityEngine_ParticleEmitterGenerated_ilo_getSingle3_m4053741949 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleEmitterGenerated::ilo_setVector3S4(System.Int32,UnityEngine.Vector3)
extern "C"  void UnityEngine_ParticleEmitterGenerated_ilo_setVector3S4_m2309476446 (Il2CppObject * __this /* static, unused */, int32_t ___e0, Vector3_t4282066566  ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine_ParticleEmitterGenerated::ilo_getVector3S5(System.Int32)
extern "C"  Vector3_t4282066566  UnityEngine_ParticleEmitterGenerated_ilo_getVector3S5_m3118128018 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleEmitterGenerated::ilo_setBooleanS6(System.Int32,System.Boolean)
extern "C"  void UnityEngine_ParticleEmitterGenerated_ilo_setBooleanS6_m2471275832 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_ParticleEmitterGenerated::ilo_getObject7(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_ParticleEmitterGenerated_ilo_getObject7_m852117065 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_ParticleEmitterGenerated::ilo_getObject8(System.Int32)
extern "C"  int32_t UnityEngine_ParticleEmitterGenerated_ilo_getObject8_m2186683821 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
