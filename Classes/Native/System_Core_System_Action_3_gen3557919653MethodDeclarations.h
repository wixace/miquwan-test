﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action_3_gen1023436963MethodDeclarations.h"

// System.Void System.Action`3<System.String,LogLevel,System.Boolean>::.ctor(System.Object,System.IntPtr)
#define Action_3__ctor_m434268032(__this, ___object0, ___method1, method) ((  void (*) (Action_3_t3557919653 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Action_3__ctor_m3139791906_gshared)(__this, ___object0, ___method1, method)
// System.Void System.Action`3<System.String,LogLevel,System.Boolean>::Invoke(T1,T2,T3)
#define Action_3_Invoke_m3985574250(__this, ___arg10, ___arg21, ___arg32, method) ((  void (*) (Action_3_t3557919653 *, String_t*, int32_t, bool, const MethodInfo*))Action_3_Invoke_m2104138590_gshared)(__this, ___arg10, ___arg21, ___arg32, method)
// System.IAsyncResult System.Action`3<System.String,LogLevel,System.Boolean>::BeginInvoke(T1,T2,T3,System.AsyncCallback,System.Object)
#define Action_3_BeginInvoke_m3780491651(__this, ___arg10, ___arg21, ___arg32, ___callback3, ___object4, method) ((  Il2CppObject * (*) (Action_3_t3557919653 *, String_t*, int32_t, bool, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))Action_3_BeginInvoke_m2296417173_gshared)(__this, ___arg10, ___arg21, ___arg32, ___callback3, ___object4, method)
// System.Void System.Action`3<System.String,LogLevel,System.Boolean>::EndInvoke(System.IAsyncResult)
#define Action_3_EndInvoke_m1760295584(__this, ___result0, method) ((  void (*) (Action_3_t3557919653 *, Il2CppObject *, const MethodInfo*))Action_3_EndInvoke_m2999467826_gshared)(__this, ___result0, method)
