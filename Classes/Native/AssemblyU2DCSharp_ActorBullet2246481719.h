﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// CombatEntity
struct CombatEntity_t684137495;
// skillCfg
struct skillCfg_t2142425171;
// UnityEngine.Transform
struct Transform_t1659122786;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t1355284822;
// System.Collections.Generic.List`1<System.UInt32>
struct List_1_t1392853533;
// UnityEngine.Collider
struct Collider_t2939674232;
// System.Collections.Generic.List`1<CombatEntity>
struct List_1_t2052323047;

#include "AssemblyU2DCSharp_IEffComponent3802264961.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ActorBullet
struct  ActorBullet_t2246481719  : public IEffComponent_t3802264961
{
public:
	// System.Boolean ActorBullet::isatk
	bool ___isatk_1;
	// CombatEntity ActorBullet::host
	CombatEntity_t684137495 * ___host_2;
	// System.Int32 ActorBullet::skillID
	int32_t ___skillID_3;
	// skillCfg ActorBullet::mpJson
	skillCfg_t2142425171 * ___mpJson_4;
	// System.Int32 ActorBullet::Hurt_times
	int32_t ___Hurt_times_5;
	// System.Single ActorBullet::damage_time
	float ___damage_time_6;
	// System.Boolean ActorBullet::isPause
	bool ___isPause_7;
	// UnityEngine.Transform ActorBullet::targetTr
	Transform_t1659122786 * ___targetTr_8;
	// System.Single ActorBullet::speed
	float ___speed_9;
	// UnityEngine.Vector3 ActorBullet::targetPosition
	Vector3_t4282066566  ___targetPosition_10;
	// System.Boolean ActorBullet::IsDo
	bool ___IsDo_11;
	// System.Single ActorBullet::trailtime
	float ___trailtime_12;
	// CombatEntity ActorBullet::targetEntity
	CombatEntity_t684137495 * ___targetEntity_13;
	// UnityEngine.Transform ActorBullet::tr
	Transform_t1659122786 * ___tr_14;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> ActorBullet::paths
	List_1_t1355284822 * ___paths_15;
	// System.Collections.Generic.List`1<System.UInt32> ActorBullet::delayList
	List_1_t1392853533 * ___delayList_16;
	// UnityEngine.Collider ActorBullet::c
	Collider_t2939674232 * ___c_17;
	// System.Single ActorBullet::beforflyAngle
	float ___beforflyAngle_18;
	// System.Single ActorBullet::curflyAngle
	float ___curflyAngle_19;
	// System.Collections.Generic.List`1<CombatEntity> ActorBullet::triggerEntityList
	List_1_t2052323047 * ___triggerEntityList_20;

public:
	inline static int32_t get_offset_of_isatk_1() { return static_cast<int32_t>(offsetof(ActorBullet_t2246481719, ___isatk_1)); }
	inline bool get_isatk_1() const { return ___isatk_1; }
	inline bool* get_address_of_isatk_1() { return &___isatk_1; }
	inline void set_isatk_1(bool value)
	{
		___isatk_1 = value;
	}

	inline static int32_t get_offset_of_host_2() { return static_cast<int32_t>(offsetof(ActorBullet_t2246481719, ___host_2)); }
	inline CombatEntity_t684137495 * get_host_2() const { return ___host_2; }
	inline CombatEntity_t684137495 ** get_address_of_host_2() { return &___host_2; }
	inline void set_host_2(CombatEntity_t684137495 * value)
	{
		___host_2 = value;
		Il2CppCodeGenWriteBarrier(&___host_2, value);
	}

	inline static int32_t get_offset_of_skillID_3() { return static_cast<int32_t>(offsetof(ActorBullet_t2246481719, ___skillID_3)); }
	inline int32_t get_skillID_3() const { return ___skillID_3; }
	inline int32_t* get_address_of_skillID_3() { return &___skillID_3; }
	inline void set_skillID_3(int32_t value)
	{
		___skillID_3 = value;
	}

	inline static int32_t get_offset_of_mpJson_4() { return static_cast<int32_t>(offsetof(ActorBullet_t2246481719, ___mpJson_4)); }
	inline skillCfg_t2142425171 * get_mpJson_4() const { return ___mpJson_4; }
	inline skillCfg_t2142425171 ** get_address_of_mpJson_4() { return &___mpJson_4; }
	inline void set_mpJson_4(skillCfg_t2142425171 * value)
	{
		___mpJson_4 = value;
		Il2CppCodeGenWriteBarrier(&___mpJson_4, value);
	}

	inline static int32_t get_offset_of_Hurt_times_5() { return static_cast<int32_t>(offsetof(ActorBullet_t2246481719, ___Hurt_times_5)); }
	inline int32_t get_Hurt_times_5() const { return ___Hurt_times_5; }
	inline int32_t* get_address_of_Hurt_times_5() { return &___Hurt_times_5; }
	inline void set_Hurt_times_5(int32_t value)
	{
		___Hurt_times_5 = value;
	}

	inline static int32_t get_offset_of_damage_time_6() { return static_cast<int32_t>(offsetof(ActorBullet_t2246481719, ___damage_time_6)); }
	inline float get_damage_time_6() const { return ___damage_time_6; }
	inline float* get_address_of_damage_time_6() { return &___damage_time_6; }
	inline void set_damage_time_6(float value)
	{
		___damage_time_6 = value;
	}

	inline static int32_t get_offset_of_isPause_7() { return static_cast<int32_t>(offsetof(ActorBullet_t2246481719, ___isPause_7)); }
	inline bool get_isPause_7() const { return ___isPause_7; }
	inline bool* get_address_of_isPause_7() { return &___isPause_7; }
	inline void set_isPause_7(bool value)
	{
		___isPause_7 = value;
	}

	inline static int32_t get_offset_of_targetTr_8() { return static_cast<int32_t>(offsetof(ActorBullet_t2246481719, ___targetTr_8)); }
	inline Transform_t1659122786 * get_targetTr_8() const { return ___targetTr_8; }
	inline Transform_t1659122786 ** get_address_of_targetTr_8() { return &___targetTr_8; }
	inline void set_targetTr_8(Transform_t1659122786 * value)
	{
		___targetTr_8 = value;
		Il2CppCodeGenWriteBarrier(&___targetTr_8, value);
	}

	inline static int32_t get_offset_of_speed_9() { return static_cast<int32_t>(offsetof(ActorBullet_t2246481719, ___speed_9)); }
	inline float get_speed_9() const { return ___speed_9; }
	inline float* get_address_of_speed_9() { return &___speed_9; }
	inline void set_speed_9(float value)
	{
		___speed_9 = value;
	}

	inline static int32_t get_offset_of_targetPosition_10() { return static_cast<int32_t>(offsetof(ActorBullet_t2246481719, ___targetPosition_10)); }
	inline Vector3_t4282066566  get_targetPosition_10() const { return ___targetPosition_10; }
	inline Vector3_t4282066566 * get_address_of_targetPosition_10() { return &___targetPosition_10; }
	inline void set_targetPosition_10(Vector3_t4282066566  value)
	{
		___targetPosition_10 = value;
	}

	inline static int32_t get_offset_of_IsDo_11() { return static_cast<int32_t>(offsetof(ActorBullet_t2246481719, ___IsDo_11)); }
	inline bool get_IsDo_11() const { return ___IsDo_11; }
	inline bool* get_address_of_IsDo_11() { return &___IsDo_11; }
	inline void set_IsDo_11(bool value)
	{
		___IsDo_11 = value;
	}

	inline static int32_t get_offset_of_trailtime_12() { return static_cast<int32_t>(offsetof(ActorBullet_t2246481719, ___trailtime_12)); }
	inline float get_trailtime_12() const { return ___trailtime_12; }
	inline float* get_address_of_trailtime_12() { return &___trailtime_12; }
	inline void set_trailtime_12(float value)
	{
		___trailtime_12 = value;
	}

	inline static int32_t get_offset_of_targetEntity_13() { return static_cast<int32_t>(offsetof(ActorBullet_t2246481719, ___targetEntity_13)); }
	inline CombatEntity_t684137495 * get_targetEntity_13() const { return ___targetEntity_13; }
	inline CombatEntity_t684137495 ** get_address_of_targetEntity_13() { return &___targetEntity_13; }
	inline void set_targetEntity_13(CombatEntity_t684137495 * value)
	{
		___targetEntity_13 = value;
		Il2CppCodeGenWriteBarrier(&___targetEntity_13, value);
	}

	inline static int32_t get_offset_of_tr_14() { return static_cast<int32_t>(offsetof(ActorBullet_t2246481719, ___tr_14)); }
	inline Transform_t1659122786 * get_tr_14() const { return ___tr_14; }
	inline Transform_t1659122786 ** get_address_of_tr_14() { return &___tr_14; }
	inline void set_tr_14(Transform_t1659122786 * value)
	{
		___tr_14 = value;
		Il2CppCodeGenWriteBarrier(&___tr_14, value);
	}

	inline static int32_t get_offset_of_paths_15() { return static_cast<int32_t>(offsetof(ActorBullet_t2246481719, ___paths_15)); }
	inline List_1_t1355284822 * get_paths_15() const { return ___paths_15; }
	inline List_1_t1355284822 ** get_address_of_paths_15() { return &___paths_15; }
	inline void set_paths_15(List_1_t1355284822 * value)
	{
		___paths_15 = value;
		Il2CppCodeGenWriteBarrier(&___paths_15, value);
	}

	inline static int32_t get_offset_of_delayList_16() { return static_cast<int32_t>(offsetof(ActorBullet_t2246481719, ___delayList_16)); }
	inline List_1_t1392853533 * get_delayList_16() const { return ___delayList_16; }
	inline List_1_t1392853533 ** get_address_of_delayList_16() { return &___delayList_16; }
	inline void set_delayList_16(List_1_t1392853533 * value)
	{
		___delayList_16 = value;
		Il2CppCodeGenWriteBarrier(&___delayList_16, value);
	}

	inline static int32_t get_offset_of_c_17() { return static_cast<int32_t>(offsetof(ActorBullet_t2246481719, ___c_17)); }
	inline Collider_t2939674232 * get_c_17() const { return ___c_17; }
	inline Collider_t2939674232 ** get_address_of_c_17() { return &___c_17; }
	inline void set_c_17(Collider_t2939674232 * value)
	{
		___c_17 = value;
		Il2CppCodeGenWriteBarrier(&___c_17, value);
	}

	inline static int32_t get_offset_of_beforflyAngle_18() { return static_cast<int32_t>(offsetof(ActorBullet_t2246481719, ___beforflyAngle_18)); }
	inline float get_beforflyAngle_18() const { return ___beforflyAngle_18; }
	inline float* get_address_of_beforflyAngle_18() { return &___beforflyAngle_18; }
	inline void set_beforflyAngle_18(float value)
	{
		___beforflyAngle_18 = value;
	}

	inline static int32_t get_offset_of_curflyAngle_19() { return static_cast<int32_t>(offsetof(ActorBullet_t2246481719, ___curflyAngle_19)); }
	inline float get_curflyAngle_19() const { return ___curflyAngle_19; }
	inline float* get_address_of_curflyAngle_19() { return &___curflyAngle_19; }
	inline void set_curflyAngle_19(float value)
	{
		___curflyAngle_19 = value;
	}

	inline static int32_t get_offset_of_triggerEntityList_20() { return static_cast<int32_t>(offsetof(ActorBullet_t2246481719, ___triggerEntityList_20)); }
	inline List_1_t2052323047 * get_triggerEntityList_20() const { return ___triggerEntityList_20; }
	inline List_1_t2052323047 ** get_address_of_triggerEntityList_20() { return &___triggerEntityList_20; }
	inline void set_triggerEntityList_20(List_1_t2052323047 * value)
	{
		___triggerEntityList_20 = value;
		Il2CppCodeGenWriteBarrier(&___triggerEntityList_20, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
