﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// monstersCfg
struct monstersCfg_t1542396363;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;

#include "codegen/il2cpp-codegen.h"

// System.Void monstersCfg::.ctor()
extern "C"  void monstersCfg__ctor_m3596957184 (monstersCfg_t1542396363 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void monstersCfg::Init(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C"  void monstersCfg_Init_m3736334725 (monstersCfg_t1542396363 * __this, Dictionary_2_t827649927 * ____info0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
