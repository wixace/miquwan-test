﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GameObjectVisitorGenerated
struct GameObjectVisitorGenerated_t1770478802;
// JSVCall
struct JSVCall_t3708497963;
// GameObjectVisitor/condition
struct condition_t3833526985;
// CSRepresentedObject
struct CSRepresentedObject_t3994124630;
// System.Action`1<UnityEngine.GameObject>
struct Action_1_t4070498141;
// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.Delegate
struct Delegate_t3310234105;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t747900261;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_CSRepresentedObject3994124630.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Delegate3310234105.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void GameObjectVisitorGenerated::.ctor()
extern "C"  void GameObjectVisitorGenerated__ctor_m248993801 (GameObjectVisitorGenerated_t1770478802 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameObjectVisitorGenerated::.cctor()
extern "C"  void GameObjectVisitorGenerated__cctor_m2941744324 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameObjectVisitorGenerated::GameObjectVisitor_FindChild__GameObject__String(JSVCall,System.Int32)
extern "C"  bool GameObjectVisitorGenerated_GameObjectVisitor_FindChild__GameObject__String_m25591558 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GameObjectVisitor/condition GameObjectVisitorGenerated::GameObjectVisitor_FindChildInCondition_GetDelegate_member1_arg1(CSRepresentedObject)
extern "C"  condition_t3833526985 * GameObjectVisitorGenerated_GameObjectVisitor_FindChildInCondition_GetDelegate_member1_arg1_m4032671054 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameObjectVisitorGenerated::GameObjectVisitor_FindChildInCondition__GameObject__condition(JSVCall,System.Int32)
extern "C"  bool GameObjectVisitorGenerated_GameObjectVisitor_FindChildInCondition__GameObject__condition_m1916922904 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameObjectVisitorGenerated::GameObjectVisitor_FindChildren__GameObject__String__ListT1_GameObject(JSVCall,System.Int32)
extern "C"  bool GameObjectVisitorGenerated_GameObjectVisitor_FindChildren__GameObject__String__ListT1_GameObject_m379994492 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GameObjectVisitor/condition GameObjectVisitorGenerated::GameObjectVisitor_FindChildrenInCondition_GetDelegate_member3_arg2(CSRepresentedObject)
extern "C"  condition_t3833526985 * GameObjectVisitorGenerated_GameObjectVisitor_FindChildrenInCondition_GetDelegate_member3_arg2_m3049140686 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameObjectVisitorGenerated::GameObjectVisitor_FindChildrenInCondition__GameObject__ListT1_GameObject__condition(JSVCall,System.Int32)
extern "C"  bool GameObjectVisitorGenerated_GameObjectVisitor_FindChildrenInCondition__GameObject__ListT1_GameObject__condition_m4181980086 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameObjectVisitorGenerated::GameObjectVisitor_FindComponentInChildrenT1__GameObject(JSVCall,System.Int32)
extern "C"  bool GameObjectVisitorGenerated_GameObjectVisitor_FindComponentInChildrenT1__GameObject_m3644406615 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameObjectVisitorGenerated::GameObjectVisitor_RemoveAllChildren__GameObject(JSVCall,System.Int32)
extern "C"  bool GameObjectVisitorGenerated_GameObjectVisitor_RemoveAllChildren__GameObject_m3648957486 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Action`1<UnityEngine.GameObject> GameObjectVisitorGenerated::GameObjectVisitor_VisitChildren_GetDelegate_member6_arg1(CSRepresentedObject)
extern "C"  Action_1_t4070498141 * GameObjectVisitorGenerated_GameObjectVisitor_VisitChildren_GetDelegate_member6_arg1_m3196010869 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameObjectVisitorGenerated::GameObjectVisitor_VisitChildren__GameObject__ActionT1_GameObject__Int32(JSVCall,System.Int32)
extern "C"  bool GameObjectVisitorGenerated_GameObjectVisitor_VisitChildren__GameObject__ActionT1_GameObject__Int32_m2374518157 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameObjectVisitorGenerated::__Register()
extern "C"  void GameObjectVisitorGenerated___Register_m1056752222 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GameObjectVisitor/condition GameObjectVisitorGenerated::<GameObjectVisitor_FindChildInCondition__GameObject__condition>m__59()
extern "C"  condition_t3833526985 * GameObjectVisitorGenerated_U3CGameObjectVisitor_FindChildInCondition__GameObject__conditionU3Em__59_m863743633 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GameObjectVisitor/condition GameObjectVisitorGenerated::<GameObjectVisitor_FindChildrenInCondition__GameObject__ListT1_GameObject__condition>m__5B()
extern "C"  condition_t3833526985 * GameObjectVisitorGenerated_U3CGameObjectVisitor_FindChildrenInCondition__GameObject__ListT1_GameObject__conditionU3Em__5B_m348426232 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Action`1<UnityEngine.GameObject> GameObjectVisitorGenerated::<GameObjectVisitor_VisitChildren__GameObject__ActionT1_GameObject__Int32>m__5D()
extern "C"  Action_1_t4070498141 * GameObjectVisitorGenerated_U3CGameObjectVisitor_VisitChildren__GameObject__ActionT1_GameObject__Int32U3Em__5D_m522520474 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Action`1<UnityEngine.GameObject> GameObjectVisitorGenerated::<GameObjectVisitor_VisitChildren__GameObject__ActionT1_GameObject__Int32>m__5E()
extern "C"  Action_1_t4070498141 * GameObjectVisitorGenerated_U3CGameObjectVisitor_VisitChildren__GameObject__ActionT1_GameObject__Int32U3Em__5E_m522521435 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GameObjectVisitorGenerated::ilo_getStringS1(System.Int32)
extern "C"  String_t* GameObjectVisitorGenerated_ilo_getStringS1_m580043719 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject GameObjectVisitorGenerated::ilo_FindChild2(UnityEngine.GameObject,System.String)
extern "C"  GameObject_t3674682005 * GameObjectVisitorGenerated_ilo_FindChild2_m337262374 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ____this0, String_t* ___name1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameObjectVisitorGenerated::ilo_addJSFunCSDelegateRel3(System.Int32,System.Delegate)
extern "C"  void GameObjectVisitorGenerated_ilo_addJSFunCSDelegateRel3_m3800169067 (Il2CppObject * __this /* static, unused */, int32_t ___funID0, Delegate_t3310234105 * ___del1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GameObjectVisitorGenerated::ilo_getObject4(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * GameObjectVisitorGenerated_ilo_getObject4_m3932412809 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 GameObjectVisitorGenerated::ilo_setObject5(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t GameObjectVisitorGenerated_ilo_setObject5_m3656224165 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameObjectVisitorGenerated::ilo_FindChildren6(UnityEngine.GameObject,System.String,System.Collections.Generic.List`1<UnityEngine.GameObject>)
extern "C"  void GameObjectVisitorGenerated_ilo_FindChildren6_m407370266 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ____this0, String_t* ___name1, List_1_t747900261 * ___objs2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameObjectVisitorGenerated::ilo_isFunctionS7(System.Int32)
extern "C"  bool GameObjectVisitorGenerated_ilo_isFunctionS7_m194389593 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Action`1<UnityEngine.GameObject> GameObjectVisitorGenerated::ilo_GameObjectVisitor_VisitChildren_GetDelegate_member6_arg18(CSRepresentedObject)
extern "C"  Action_1_t4070498141 * GameObjectVisitorGenerated_ilo_GameObjectVisitor_VisitChildren_GetDelegate_member6_arg18_m2912249468 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
