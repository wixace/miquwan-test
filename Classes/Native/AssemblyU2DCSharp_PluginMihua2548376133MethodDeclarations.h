﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PluginMihua
struct PluginMihua_t2548376133;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// Mihua.SDK.PayInfo
struct PayInfo_t1775308120;
// System.String
struct String_t;
// WMihuaPay
struct WMihuaPay_t209753383;
// CEvent.EventFunc`1<CEvent.ZEvent>
struct EventFunc_1_t1114914310;
// VersionMgr
struct VersionMgr_t1322950208;
// AMihuaPay
struct AMihuaPay_t1319045905;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PluginMihua_PayType917897752.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "AssemblyU2DCSharp_Mihua_SDK_PayInfo1775308120.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_WMihuaPay209753383.h"
#include "AssemblyU2DCSharp_AMihuaPay1319045905.h"
#include "AssemblyU2DCSharp_PluginMihua2548376133.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void PluginMihua::.ctor()
extern "C"  void PluginMihua__ctor_m1706563270 (PluginMihua_t2548376133 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginMihua::.cctor()
extern "C"  void PluginMihua__cctor_m881757607 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginMihua::VoidFuction()
extern "C"  void PluginMihua_VoidFuction_m901865208 (PluginMihua_t2548376133 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginMihua::Init()
extern "C"  void PluginMihua_Init_m1114965710 (PluginMihua_t2548376133 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginMihua::HasPayType(PluginMihua/PayType)
extern "C"  bool PluginMihua_HasPayType_m3658185928 (PluginMihua_t2548376133 * __this, int32_t ___payType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginMihua::ReqSDKHttpLogin()
extern "C"  Dictionary_2_t827649927 * PluginMihua_ReqSDKHttpLogin_m3608007863 (PluginMihua_t2548376133 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginMihua::IsLoginSuccess(System.Boolean)
extern "C"  bool PluginMihua_IsLoginSuccess_m3168526821 (PluginMihua_t2548376133 * __this, bool ___isOpenUseLogin0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginMihua::OpenUserLogin()
extern "C"  void PluginMihua_OpenUserLogin_m4125692696 (PluginMihua_t2548376133 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginMihua::RoleEnterGame(CEvent.ZEvent)
extern "C"  void PluginMihua_RoleEnterGame_m3182711939 (PluginMihua_t2548376133 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginMihua::UserPay(CEvent.ZEvent)
extern "C"  void PluginMihua_UserPay_m2328581466 (PluginMihua_t2548376133 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginMihua::GoSelectPay(Mihua.SDK.PayInfo)
extern "C"  void PluginMihua_GoSelectPay_m4169027774 (PluginMihua_t2548376133 * __this, PayInfo_t1775308120 * ____payInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginMihua::OnSelectPay(CEvent.ZEvent)
extern "C"  void PluginMihua_OnSelectPay_m3926831818 (PluginMihua_t2548376133 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginMihua::OnLoginSuccess(System.String)
extern "C"  void PluginMihua_OnLoginSuccess_m501093707 (PluginMihua_t2548376133 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginMihua::OnLoginFail(System.String)
extern "C"  void PluginMihua_OnLoginFail_m129062870 (PluginMihua_t2548376133 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginMihua::OnPaySuccess(System.String)
extern "C"  void PluginMihua_OnPaySuccess_m4216278602 (PluginMihua_t2548376133 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginMihua::OnPayFail(System.String)
extern "C"  void PluginMihua_OnPayFail_m543676215 (PluginMihua_t2548376133 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginMihua::OnExitGameSuccess(System.String)
extern "C"  void PluginMihua_OnExitGameSuccess_m4041197514 (PluginMihua_t2548376133 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginMihua::OnLogoutSuccess(System.String)
extern "C"  void PluginMihua_OnLogoutSuccess_m2652545700 (PluginMihua_t2548376133 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginMihua::OnLogoutFail(System.String)
extern "C"  void PluginMihua_OnLogoutFail_m3813830941 (PluginMihua_t2548376133 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginMihua::__IIap(System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginMihua___IIap_m1938363445 (PluginMihua_t2548376133 * __this, String_t* ___productId0, String_t* ___roleId1, String_t* ___serverId2, String_t* ___type3, String_t* ___amount4, String_t* ___orderId5, String_t* ___pcbUrl6, String_t* ___appKey7, String_t* ___eargs8, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginMihua::<OnLogoutSuccess>m__43D()
extern "C"  void PluginMihua_U3COnLogoutSuccessU3Em__43D_m3724893468 (PluginMihua_t2548376133 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginMihua::ilo_Tiaoqian1(WMihuaPay,Mihua.SDK.PayInfo)
extern "C"  void PluginMihua_ilo_Tiaoqian1_m2176977905 (Il2CppObject * __this /* static, unused */, WMihuaPay_t209753383 * ____this0, PayInfo_t1775308120 * ___payInfo1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginMihua::ilo_AddEventListener2(System.String,CEvent.EventFunc`1<CEvent.ZEvent>)
extern "C"  void PluginMihua_ilo_AddEventListener2_m2242101133 (Il2CppObject * __this /* static, unused */, String_t* ___eventType0, EventFunc_1_t1114914310 * ___fun1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionMgr PluginMihua::ilo_get_Instance3()
extern "C"  VersionMgr_t1322950208 * PluginMihua_ilo_get_Instance3_m3739290979 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginMihua::ilo_InitPayParams4(AMihuaPay)
extern "C"  void PluginMihua_ilo_InitPayParams4_m2140782746 (Il2CppObject * __this /* static, unused */, AMihuaPay_t1319045905 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginMihua::ilo_OpenUserLogin5(PluginMihua)
extern "C"  void PluginMihua_ilo_OpenUserLogin5_m2335403163 (Il2CppObject * __this /* static, unused */, PluginMihua_t2548376133 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mihua.SDK.PayInfo PluginMihua::ilo_Parse6(System.Object[])
extern "C"  PayInfo_t1775308120 * PluginMihua_ilo_Parse6_m968531045 (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t1108656482* ___payOjs0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginMihua::ilo___IIap7(PluginMihua,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginMihua_ilo___IIap7_m2923192304 (Il2CppObject * __this /* static, unused */, PluginMihua_t2548376133 * ____this0, String_t* ___productId1, String_t* ___roleId2, String_t* ___serverId3, String_t* ___type4, String_t* ___amount5, String_t* ___orderId6, String_t* ___pcbUrl7, String_t* ___appKey8, String_t* ___eargs9, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginMihua::ilo_Log8(System.Object,System.Boolean)
extern "C"  void PluginMihua_ilo_Log8_m4265243022 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginMihua::ilo_OnSelectPay9(PluginMihua,CEvent.ZEvent)
extern "C"  void PluginMihua_ilo_OnSelectPay9_m2760145093 (Il2CppObject * __this /* static, unused */, PluginMihua_t2548376133 * ____this0, ZEvent_t3638018500 * ___ev1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginMihua::ilo_CallJsFun10(System.String,System.Object[])
extern "C"  void PluginMihua_ilo_CallJsFun10_m1266641798 (Il2CppObject * __this /* static, unused */, String_t* ___funName0, ObjectU5BU5D_t1108656482* ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginMihua::ilo_Tiaoqian11(AMihuaPay,Mihua.SDK.PayInfo)
extern "C"  void PluginMihua_ilo_Tiaoqian11_m1157042312 (Il2CppObject * __this /* static, unused */, AMihuaPay_t1319045905 * ____this0, PayInfo_t1775308120 * ___payInfo1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginMihua::ilo_DispatchEvent12(CEvent.ZEvent)
extern "C"  void PluginMihua_ilo_DispatchEvent12_m3489611977 (Il2CppObject * __this /* static, unused */, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
