﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIPlayAnimation
struct UIPlayAnimation_t2708996220;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// UIToggle
struct UIToggle_t688812808;
// System.Collections.Generic.List`1<EventDelegate>
struct List_1_t1077642479;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "AssemblyU2DCSharp_UIPlayAnimation2708996220.h"
#include "AssemblyU2DCSharp_UIToggle688812808.h"

// System.Void UIPlayAnimation::.ctor()
extern "C"  void UIPlayAnimation__ctor_m2873266799 (UIPlayAnimation_t2708996220 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPlayAnimation::.cctor()
extern "C"  void UIPlayAnimation__cctor_m2689828638 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIPlayAnimation::get_dualState()
extern "C"  bool UIPlayAnimation_get_dualState_m43233069 (UIPlayAnimation_t2708996220 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPlayAnimation::Awake()
extern "C"  void UIPlayAnimation_Awake_m3110872018 (UIPlayAnimation_t2708996220 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPlayAnimation::Start()
extern "C"  void UIPlayAnimation_Start_m1820404591 (UIPlayAnimation_t2708996220 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPlayAnimation::OnEnable()
extern "C"  void UIPlayAnimation_OnEnable_m864578231 (UIPlayAnimation_t2708996220 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPlayAnimation::OnDisable()
extern "C"  void UIPlayAnimation_OnDisable_m1473058646 (UIPlayAnimation_t2708996220 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPlayAnimation::OnHover(System.Boolean)
extern "C"  void UIPlayAnimation_OnHover_m2727445473 (UIPlayAnimation_t2708996220 * __this, bool ___isOver0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPlayAnimation::OnPress(System.Boolean)
extern "C"  void UIPlayAnimation_OnPress_m647131368 (UIPlayAnimation_t2708996220 * __this, bool ___isPressed0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPlayAnimation::OnClick()
extern "C"  void UIPlayAnimation_OnClick_m1389627702 (UIPlayAnimation_t2708996220 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPlayAnimation::OnDoubleClick()
extern "C"  void UIPlayAnimation_OnDoubleClick_m2735764069 (UIPlayAnimation_t2708996220 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPlayAnimation::OnSelect(System.Boolean)
extern "C"  void UIPlayAnimation_OnSelect_m2507968871 (UIPlayAnimation_t2708996220 * __this, bool ___isSelected0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPlayAnimation::OnToggle()
extern "C"  void UIPlayAnimation_OnToggle_m2300825672 (UIPlayAnimation_t2708996220 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPlayAnimation::OnDragOver()
extern "C"  void UIPlayAnimation_OnDragOver_m2817516380 (UIPlayAnimation_t2708996220 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPlayAnimation::OnDragOut()
extern "C"  void UIPlayAnimation_OnDragOut_m1337795944 (UIPlayAnimation_t2708996220 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPlayAnimation::OnDrop(UnityEngine.GameObject)
extern "C"  void UIPlayAnimation_OnDrop_m135855387 (UIPlayAnimation_t2708996220 * __this, GameObject_t3674682005 * ___go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPlayAnimation::Play(System.Boolean)
extern "C"  void UIPlayAnimation_Play_m637704416 (UIPlayAnimation_t2708996220 * __this, bool ___forward0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPlayAnimation::Play(System.Boolean,System.Boolean)
extern "C"  void UIPlayAnimation_Play_m3697073821 (UIPlayAnimation_t2708996220 * __this, bool ___forward0, bool ___onlyIfDifferent1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPlayAnimation::OnFinished()
extern "C"  void UIPlayAnimation_OnFinished_m1191154982 (UIPlayAnimation_t2708996220 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPlayAnimation::OnDestory()
extern "C"  void UIPlayAnimation_OnDestory_m3647909506 (UIPlayAnimation_t2708996220 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPlayAnimation::ilo_Play1(UIPlayAnimation,System.Boolean,System.Boolean)
extern "C"  void UIPlayAnimation_ilo_Play1_m1763619711 (Il2CppObject * __this /* static, unused */, UIPlayAnimation_t2708996220 * ____this0, bool ___forward1, bool ___onlyIfDifferent2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIPlayAnimation::ilo_get_dualState2(UIPlayAnimation)
extern "C"  bool UIPlayAnimation_ilo_get_dualState2_m3764168444 (Il2CppObject * __this /* static, unused */, UIPlayAnimation_t2708996220 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIPlayAnimation::ilo_get_value3(UIToggle)
extern "C"  bool UIPlayAnimation_ilo_get_value3_m1245443191 (Il2CppObject * __this /* static, unused */, UIToggle_t688812808 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UIPlayAnimation::ilo_get_selectedObject4()
extern "C"  GameObject_t3674682005 * UIPlayAnimation_ilo_get_selectedObject4_m595845984 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPlayAnimation::ilo_Execute5(System.Collections.Generic.List`1<EventDelegate>)
extern "C"  void UIPlayAnimation_ilo_Execute5_m641844395 (Il2CppObject * __this /* static, unused */, List_1_t1077642479 * ___list0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
