﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Predicate_1_gen3781873254MethodDeclarations.h"

// System.Void System.Predicate`1<FingerClusterManager/Cluster>::.ctor(System.Object,System.IntPtr)
#define Predicate_1__ctor_m2912675531(__this, ___object0, ___method1, method) ((  void (*) (Predicate_1_t3481288186 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Predicate_1__ctor_m231416842_gshared)(__this, ___object0, ___method1, method)
// System.Boolean System.Predicate`1<FingerClusterManager/Cluster>::Invoke(T)
#define Predicate_1_Invoke_m3345037742(__this, ___obj0, method) ((  bool (*) (Predicate_1_t3481288186 *, Cluster_t3870231303 *, const MethodInfo*))Predicate_1_Invoke_m1328901537_gshared)(__this, ___obj0, method)
// System.IAsyncResult System.Predicate`1<FingerClusterManager/Cluster>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Predicate_1_BeginInvoke_m335895485(__this, ___obj0, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Predicate_1_t3481288186 *, Cluster_t3870231303 *, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))Predicate_1_BeginInvoke_m2038073176_gshared)(__this, ___obj0, ___callback1, ___object2, method)
// System.Boolean System.Predicate`1<FingerClusterManager/Cluster>::EndInvoke(System.IAsyncResult)
#define Predicate_1_EndInvoke_m3280306918(__this, ___result0, method) ((  bool (*) (Predicate_1_t3481288186 *, Il2CppObject *, const MethodInfo*))Predicate_1_EndInvoke_m3970497007_gshared)(__this, ___result0, method)
