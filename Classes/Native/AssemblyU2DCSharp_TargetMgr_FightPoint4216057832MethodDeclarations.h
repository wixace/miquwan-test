﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TargetMgr/FightPoint
struct FightPoint_t4216057832;

#include "codegen/il2cpp-codegen.h"

// System.Void TargetMgr/FightPoint::.ctor()
extern "C"  void FightPoint__ctor_m2473054131 (FightPoint_t4216057832 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
