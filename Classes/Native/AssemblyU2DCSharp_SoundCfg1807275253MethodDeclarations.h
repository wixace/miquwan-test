﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SoundCfg
struct SoundCfg_t1807275253;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;

#include "codegen/il2cpp-codegen.h"

// System.Void SoundCfg::.ctor()
extern "C"  void SoundCfg__ctor_m3888098630 (SoundCfg_t1807275253 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundCfg::Init(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C"  void SoundCfg_Init_m3447528575 (SoundCfg_t1807275253 * __this, Dictionary_2_t827649927 * ____info0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
