﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_serpeMembaw6
struct  M_serpeMembaw6_t992743342  : public Il2CppObject
{
public:
	// System.Boolean GarbageiOS.M_serpeMembaw6::_neaweSousor
	bool ____neaweSousor_0;
	// System.Int32 GarbageiOS.M_serpeMembaw6::_memallpuMarnayze
	int32_t ____memallpuMarnayze_1;
	// System.Boolean GarbageiOS.M_serpeMembaw6::_chemhileDallgavur
	bool ____chemhileDallgavur_2;
	// System.Int32 GarbageiOS.M_serpeMembaw6::_dremjaytu
	int32_t ____dremjaytu_3;
	// System.UInt32 GarbageiOS.M_serpeMembaw6::_souxase
	uint32_t ____souxase_4;
	// System.String GarbageiOS.M_serpeMembaw6::_peejall
	String_t* ____peejall_5;
	// System.Boolean GarbageiOS.M_serpeMembaw6::_kelyer
	bool ____kelyer_6;
	// System.Boolean GarbageiOS.M_serpeMembaw6::_faltiMigesow
	bool ____faltiMigesow_7;
	// System.Single GarbageiOS.M_serpeMembaw6::_wawhis
	float ____wawhis_8;
	// System.String GarbageiOS.M_serpeMembaw6::_betaHise
	String_t* ____betaHise_9;
	// System.Single GarbageiOS.M_serpeMembaw6::_macarrai
	float ____macarrai_10;
	// System.Int32 GarbageiOS.M_serpeMembaw6::_sajoTrouhoo
	int32_t ____sajoTrouhoo_11;
	// System.String GarbageiOS.M_serpeMembaw6::_sacai
	String_t* ____sacai_12;
	// System.Boolean GarbageiOS.M_serpeMembaw6::_gowfas
	bool ____gowfas_13;
	// System.Boolean GarbageiOS.M_serpeMembaw6::_gousimiStairtro
	bool ____gousimiStairtro_14;
	// System.UInt32 GarbageiOS.M_serpeMembaw6::_lechelhal
	uint32_t ____lechelhal_15;
	// System.String GarbageiOS.M_serpeMembaw6::_koobaSevo
	String_t* ____koobaSevo_16;
	// System.String GarbageiOS.M_serpeMembaw6::_jezoobo
	String_t* ____jezoobo_17;
	// System.Boolean GarbageiOS.M_serpeMembaw6::_rocowDurse
	bool ____rocowDurse_18;
	// System.Boolean GarbageiOS.M_serpeMembaw6::_reepirereNemrem
	bool ____reepirereNemrem_19;
	// System.Int32 GarbageiOS.M_serpeMembaw6::_hurleHecirsi
	int32_t ____hurleHecirsi_20;
	// System.String GarbageiOS.M_serpeMembaw6::_saijir
	String_t* ____saijir_21;
	// System.Boolean GarbageiOS.M_serpeMembaw6::_mallpeza
	bool ____mallpeza_22;
	// System.String GarbageiOS.M_serpeMembaw6::_chudazaiPeljerlow
	String_t* ____chudazaiPeljerlow_23;
	// System.UInt32 GarbageiOS.M_serpeMembaw6::_larhai
	uint32_t ____larhai_24;
	// System.String GarbageiOS.M_serpeMembaw6::_tewou
	String_t* ____tewou_25;
	// System.UInt32 GarbageiOS.M_serpeMembaw6::_roseaKalsow
	uint32_t ____roseaKalsow_26;
	// System.Single GarbageiOS.M_serpeMembaw6::_tiderhay
	float ____tiderhay_27;
	// System.UInt32 GarbageiOS.M_serpeMembaw6::_treapurFaiqaydri
	uint32_t ____treapurFaiqaydri_28;
	// System.Int32 GarbageiOS.M_serpeMembaw6::_staneelearHaysalmis
	int32_t ____staneelearHaysalmis_29;
	// System.Int32 GarbageiOS.M_serpeMembaw6::_pubeSonenere
	int32_t ____pubeSonenere_30;
	// System.Int32 GarbageiOS.M_serpeMembaw6::_tawco
	int32_t ____tawco_31;
	// System.Int32 GarbageiOS.M_serpeMembaw6::_hawburhe
	int32_t ____hawburhe_32;
	// System.String GarbageiOS.M_serpeMembaw6::_terlowfor
	String_t* ____terlowfor_33;
	// System.Boolean GarbageiOS.M_serpeMembaw6::_tahayWomere
	bool ____tahayWomere_34;
	// System.Int32 GarbageiOS.M_serpeMembaw6::_rallwaCiteafi
	int32_t ____rallwaCiteafi_35;

public:
	inline static int32_t get_offset_of__neaweSousor_0() { return static_cast<int32_t>(offsetof(M_serpeMembaw6_t992743342, ____neaweSousor_0)); }
	inline bool get__neaweSousor_0() const { return ____neaweSousor_0; }
	inline bool* get_address_of__neaweSousor_0() { return &____neaweSousor_0; }
	inline void set__neaweSousor_0(bool value)
	{
		____neaweSousor_0 = value;
	}

	inline static int32_t get_offset_of__memallpuMarnayze_1() { return static_cast<int32_t>(offsetof(M_serpeMembaw6_t992743342, ____memallpuMarnayze_1)); }
	inline int32_t get__memallpuMarnayze_1() const { return ____memallpuMarnayze_1; }
	inline int32_t* get_address_of__memallpuMarnayze_1() { return &____memallpuMarnayze_1; }
	inline void set__memallpuMarnayze_1(int32_t value)
	{
		____memallpuMarnayze_1 = value;
	}

	inline static int32_t get_offset_of__chemhileDallgavur_2() { return static_cast<int32_t>(offsetof(M_serpeMembaw6_t992743342, ____chemhileDallgavur_2)); }
	inline bool get__chemhileDallgavur_2() const { return ____chemhileDallgavur_2; }
	inline bool* get_address_of__chemhileDallgavur_2() { return &____chemhileDallgavur_2; }
	inline void set__chemhileDallgavur_2(bool value)
	{
		____chemhileDallgavur_2 = value;
	}

	inline static int32_t get_offset_of__dremjaytu_3() { return static_cast<int32_t>(offsetof(M_serpeMembaw6_t992743342, ____dremjaytu_3)); }
	inline int32_t get__dremjaytu_3() const { return ____dremjaytu_3; }
	inline int32_t* get_address_of__dremjaytu_3() { return &____dremjaytu_3; }
	inline void set__dremjaytu_3(int32_t value)
	{
		____dremjaytu_3 = value;
	}

	inline static int32_t get_offset_of__souxase_4() { return static_cast<int32_t>(offsetof(M_serpeMembaw6_t992743342, ____souxase_4)); }
	inline uint32_t get__souxase_4() const { return ____souxase_4; }
	inline uint32_t* get_address_of__souxase_4() { return &____souxase_4; }
	inline void set__souxase_4(uint32_t value)
	{
		____souxase_4 = value;
	}

	inline static int32_t get_offset_of__peejall_5() { return static_cast<int32_t>(offsetof(M_serpeMembaw6_t992743342, ____peejall_5)); }
	inline String_t* get__peejall_5() const { return ____peejall_5; }
	inline String_t** get_address_of__peejall_5() { return &____peejall_5; }
	inline void set__peejall_5(String_t* value)
	{
		____peejall_5 = value;
		Il2CppCodeGenWriteBarrier(&____peejall_5, value);
	}

	inline static int32_t get_offset_of__kelyer_6() { return static_cast<int32_t>(offsetof(M_serpeMembaw6_t992743342, ____kelyer_6)); }
	inline bool get__kelyer_6() const { return ____kelyer_6; }
	inline bool* get_address_of__kelyer_6() { return &____kelyer_6; }
	inline void set__kelyer_6(bool value)
	{
		____kelyer_6 = value;
	}

	inline static int32_t get_offset_of__faltiMigesow_7() { return static_cast<int32_t>(offsetof(M_serpeMembaw6_t992743342, ____faltiMigesow_7)); }
	inline bool get__faltiMigesow_7() const { return ____faltiMigesow_7; }
	inline bool* get_address_of__faltiMigesow_7() { return &____faltiMigesow_7; }
	inline void set__faltiMigesow_7(bool value)
	{
		____faltiMigesow_7 = value;
	}

	inline static int32_t get_offset_of__wawhis_8() { return static_cast<int32_t>(offsetof(M_serpeMembaw6_t992743342, ____wawhis_8)); }
	inline float get__wawhis_8() const { return ____wawhis_8; }
	inline float* get_address_of__wawhis_8() { return &____wawhis_8; }
	inline void set__wawhis_8(float value)
	{
		____wawhis_8 = value;
	}

	inline static int32_t get_offset_of__betaHise_9() { return static_cast<int32_t>(offsetof(M_serpeMembaw6_t992743342, ____betaHise_9)); }
	inline String_t* get__betaHise_9() const { return ____betaHise_9; }
	inline String_t** get_address_of__betaHise_9() { return &____betaHise_9; }
	inline void set__betaHise_9(String_t* value)
	{
		____betaHise_9 = value;
		Il2CppCodeGenWriteBarrier(&____betaHise_9, value);
	}

	inline static int32_t get_offset_of__macarrai_10() { return static_cast<int32_t>(offsetof(M_serpeMembaw6_t992743342, ____macarrai_10)); }
	inline float get__macarrai_10() const { return ____macarrai_10; }
	inline float* get_address_of__macarrai_10() { return &____macarrai_10; }
	inline void set__macarrai_10(float value)
	{
		____macarrai_10 = value;
	}

	inline static int32_t get_offset_of__sajoTrouhoo_11() { return static_cast<int32_t>(offsetof(M_serpeMembaw6_t992743342, ____sajoTrouhoo_11)); }
	inline int32_t get__sajoTrouhoo_11() const { return ____sajoTrouhoo_11; }
	inline int32_t* get_address_of__sajoTrouhoo_11() { return &____sajoTrouhoo_11; }
	inline void set__sajoTrouhoo_11(int32_t value)
	{
		____sajoTrouhoo_11 = value;
	}

	inline static int32_t get_offset_of__sacai_12() { return static_cast<int32_t>(offsetof(M_serpeMembaw6_t992743342, ____sacai_12)); }
	inline String_t* get__sacai_12() const { return ____sacai_12; }
	inline String_t** get_address_of__sacai_12() { return &____sacai_12; }
	inline void set__sacai_12(String_t* value)
	{
		____sacai_12 = value;
		Il2CppCodeGenWriteBarrier(&____sacai_12, value);
	}

	inline static int32_t get_offset_of__gowfas_13() { return static_cast<int32_t>(offsetof(M_serpeMembaw6_t992743342, ____gowfas_13)); }
	inline bool get__gowfas_13() const { return ____gowfas_13; }
	inline bool* get_address_of__gowfas_13() { return &____gowfas_13; }
	inline void set__gowfas_13(bool value)
	{
		____gowfas_13 = value;
	}

	inline static int32_t get_offset_of__gousimiStairtro_14() { return static_cast<int32_t>(offsetof(M_serpeMembaw6_t992743342, ____gousimiStairtro_14)); }
	inline bool get__gousimiStairtro_14() const { return ____gousimiStairtro_14; }
	inline bool* get_address_of__gousimiStairtro_14() { return &____gousimiStairtro_14; }
	inline void set__gousimiStairtro_14(bool value)
	{
		____gousimiStairtro_14 = value;
	}

	inline static int32_t get_offset_of__lechelhal_15() { return static_cast<int32_t>(offsetof(M_serpeMembaw6_t992743342, ____lechelhal_15)); }
	inline uint32_t get__lechelhal_15() const { return ____lechelhal_15; }
	inline uint32_t* get_address_of__lechelhal_15() { return &____lechelhal_15; }
	inline void set__lechelhal_15(uint32_t value)
	{
		____lechelhal_15 = value;
	}

	inline static int32_t get_offset_of__koobaSevo_16() { return static_cast<int32_t>(offsetof(M_serpeMembaw6_t992743342, ____koobaSevo_16)); }
	inline String_t* get__koobaSevo_16() const { return ____koobaSevo_16; }
	inline String_t** get_address_of__koobaSevo_16() { return &____koobaSevo_16; }
	inline void set__koobaSevo_16(String_t* value)
	{
		____koobaSevo_16 = value;
		Il2CppCodeGenWriteBarrier(&____koobaSevo_16, value);
	}

	inline static int32_t get_offset_of__jezoobo_17() { return static_cast<int32_t>(offsetof(M_serpeMembaw6_t992743342, ____jezoobo_17)); }
	inline String_t* get__jezoobo_17() const { return ____jezoobo_17; }
	inline String_t** get_address_of__jezoobo_17() { return &____jezoobo_17; }
	inline void set__jezoobo_17(String_t* value)
	{
		____jezoobo_17 = value;
		Il2CppCodeGenWriteBarrier(&____jezoobo_17, value);
	}

	inline static int32_t get_offset_of__rocowDurse_18() { return static_cast<int32_t>(offsetof(M_serpeMembaw6_t992743342, ____rocowDurse_18)); }
	inline bool get__rocowDurse_18() const { return ____rocowDurse_18; }
	inline bool* get_address_of__rocowDurse_18() { return &____rocowDurse_18; }
	inline void set__rocowDurse_18(bool value)
	{
		____rocowDurse_18 = value;
	}

	inline static int32_t get_offset_of__reepirereNemrem_19() { return static_cast<int32_t>(offsetof(M_serpeMembaw6_t992743342, ____reepirereNemrem_19)); }
	inline bool get__reepirereNemrem_19() const { return ____reepirereNemrem_19; }
	inline bool* get_address_of__reepirereNemrem_19() { return &____reepirereNemrem_19; }
	inline void set__reepirereNemrem_19(bool value)
	{
		____reepirereNemrem_19 = value;
	}

	inline static int32_t get_offset_of__hurleHecirsi_20() { return static_cast<int32_t>(offsetof(M_serpeMembaw6_t992743342, ____hurleHecirsi_20)); }
	inline int32_t get__hurleHecirsi_20() const { return ____hurleHecirsi_20; }
	inline int32_t* get_address_of__hurleHecirsi_20() { return &____hurleHecirsi_20; }
	inline void set__hurleHecirsi_20(int32_t value)
	{
		____hurleHecirsi_20 = value;
	}

	inline static int32_t get_offset_of__saijir_21() { return static_cast<int32_t>(offsetof(M_serpeMembaw6_t992743342, ____saijir_21)); }
	inline String_t* get__saijir_21() const { return ____saijir_21; }
	inline String_t** get_address_of__saijir_21() { return &____saijir_21; }
	inline void set__saijir_21(String_t* value)
	{
		____saijir_21 = value;
		Il2CppCodeGenWriteBarrier(&____saijir_21, value);
	}

	inline static int32_t get_offset_of__mallpeza_22() { return static_cast<int32_t>(offsetof(M_serpeMembaw6_t992743342, ____mallpeza_22)); }
	inline bool get__mallpeza_22() const { return ____mallpeza_22; }
	inline bool* get_address_of__mallpeza_22() { return &____mallpeza_22; }
	inline void set__mallpeza_22(bool value)
	{
		____mallpeza_22 = value;
	}

	inline static int32_t get_offset_of__chudazaiPeljerlow_23() { return static_cast<int32_t>(offsetof(M_serpeMembaw6_t992743342, ____chudazaiPeljerlow_23)); }
	inline String_t* get__chudazaiPeljerlow_23() const { return ____chudazaiPeljerlow_23; }
	inline String_t** get_address_of__chudazaiPeljerlow_23() { return &____chudazaiPeljerlow_23; }
	inline void set__chudazaiPeljerlow_23(String_t* value)
	{
		____chudazaiPeljerlow_23 = value;
		Il2CppCodeGenWriteBarrier(&____chudazaiPeljerlow_23, value);
	}

	inline static int32_t get_offset_of__larhai_24() { return static_cast<int32_t>(offsetof(M_serpeMembaw6_t992743342, ____larhai_24)); }
	inline uint32_t get__larhai_24() const { return ____larhai_24; }
	inline uint32_t* get_address_of__larhai_24() { return &____larhai_24; }
	inline void set__larhai_24(uint32_t value)
	{
		____larhai_24 = value;
	}

	inline static int32_t get_offset_of__tewou_25() { return static_cast<int32_t>(offsetof(M_serpeMembaw6_t992743342, ____tewou_25)); }
	inline String_t* get__tewou_25() const { return ____tewou_25; }
	inline String_t** get_address_of__tewou_25() { return &____tewou_25; }
	inline void set__tewou_25(String_t* value)
	{
		____tewou_25 = value;
		Il2CppCodeGenWriteBarrier(&____tewou_25, value);
	}

	inline static int32_t get_offset_of__roseaKalsow_26() { return static_cast<int32_t>(offsetof(M_serpeMembaw6_t992743342, ____roseaKalsow_26)); }
	inline uint32_t get__roseaKalsow_26() const { return ____roseaKalsow_26; }
	inline uint32_t* get_address_of__roseaKalsow_26() { return &____roseaKalsow_26; }
	inline void set__roseaKalsow_26(uint32_t value)
	{
		____roseaKalsow_26 = value;
	}

	inline static int32_t get_offset_of__tiderhay_27() { return static_cast<int32_t>(offsetof(M_serpeMembaw6_t992743342, ____tiderhay_27)); }
	inline float get__tiderhay_27() const { return ____tiderhay_27; }
	inline float* get_address_of__tiderhay_27() { return &____tiderhay_27; }
	inline void set__tiderhay_27(float value)
	{
		____tiderhay_27 = value;
	}

	inline static int32_t get_offset_of__treapurFaiqaydri_28() { return static_cast<int32_t>(offsetof(M_serpeMembaw6_t992743342, ____treapurFaiqaydri_28)); }
	inline uint32_t get__treapurFaiqaydri_28() const { return ____treapurFaiqaydri_28; }
	inline uint32_t* get_address_of__treapurFaiqaydri_28() { return &____treapurFaiqaydri_28; }
	inline void set__treapurFaiqaydri_28(uint32_t value)
	{
		____treapurFaiqaydri_28 = value;
	}

	inline static int32_t get_offset_of__staneelearHaysalmis_29() { return static_cast<int32_t>(offsetof(M_serpeMembaw6_t992743342, ____staneelearHaysalmis_29)); }
	inline int32_t get__staneelearHaysalmis_29() const { return ____staneelearHaysalmis_29; }
	inline int32_t* get_address_of__staneelearHaysalmis_29() { return &____staneelearHaysalmis_29; }
	inline void set__staneelearHaysalmis_29(int32_t value)
	{
		____staneelearHaysalmis_29 = value;
	}

	inline static int32_t get_offset_of__pubeSonenere_30() { return static_cast<int32_t>(offsetof(M_serpeMembaw6_t992743342, ____pubeSonenere_30)); }
	inline int32_t get__pubeSonenere_30() const { return ____pubeSonenere_30; }
	inline int32_t* get_address_of__pubeSonenere_30() { return &____pubeSonenere_30; }
	inline void set__pubeSonenere_30(int32_t value)
	{
		____pubeSonenere_30 = value;
	}

	inline static int32_t get_offset_of__tawco_31() { return static_cast<int32_t>(offsetof(M_serpeMembaw6_t992743342, ____tawco_31)); }
	inline int32_t get__tawco_31() const { return ____tawco_31; }
	inline int32_t* get_address_of__tawco_31() { return &____tawco_31; }
	inline void set__tawco_31(int32_t value)
	{
		____tawco_31 = value;
	}

	inline static int32_t get_offset_of__hawburhe_32() { return static_cast<int32_t>(offsetof(M_serpeMembaw6_t992743342, ____hawburhe_32)); }
	inline int32_t get__hawburhe_32() const { return ____hawburhe_32; }
	inline int32_t* get_address_of__hawburhe_32() { return &____hawburhe_32; }
	inline void set__hawburhe_32(int32_t value)
	{
		____hawburhe_32 = value;
	}

	inline static int32_t get_offset_of__terlowfor_33() { return static_cast<int32_t>(offsetof(M_serpeMembaw6_t992743342, ____terlowfor_33)); }
	inline String_t* get__terlowfor_33() const { return ____terlowfor_33; }
	inline String_t** get_address_of__terlowfor_33() { return &____terlowfor_33; }
	inline void set__terlowfor_33(String_t* value)
	{
		____terlowfor_33 = value;
		Il2CppCodeGenWriteBarrier(&____terlowfor_33, value);
	}

	inline static int32_t get_offset_of__tahayWomere_34() { return static_cast<int32_t>(offsetof(M_serpeMembaw6_t992743342, ____tahayWomere_34)); }
	inline bool get__tahayWomere_34() const { return ____tahayWomere_34; }
	inline bool* get_address_of__tahayWomere_34() { return &____tahayWomere_34; }
	inline void set__tahayWomere_34(bool value)
	{
		____tahayWomere_34 = value;
	}

	inline static int32_t get_offset_of__rallwaCiteafi_35() { return static_cast<int32_t>(offsetof(M_serpeMembaw6_t992743342, ____rallwaCiteafi_35)); }
	inline int32_t get__rallwaCiteafi_35() const { return ____rallwaCiteafi_35; }
	inline int32_t* get_address_of__rallwaCiteafi_35() { return &____rallwaCiteafi_35; }
	inline void set__rallwaCiteafi_35(int32_t value)
	{
		____rallwaCiteafi_35 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
