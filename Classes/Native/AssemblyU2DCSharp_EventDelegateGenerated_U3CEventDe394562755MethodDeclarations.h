﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EventDelegateGenerated/<EventDelegate_Set_GetDelegate_member13_arg1>c__AnonStorey60
struct U3CEventDelegate_Set_GetDelegate_member13_arg1U3Ec__AnonStorey60_t394562755;

#include "codegen/il2cpp-codegen.h"

// System.Void EventDelegateGenerated/<EventDelegate_Set_GetDelegate_member13_arg1>c__AnonStorey60::.ctor()
extern "C"  void U3CEventDelegate_Set_GetDelegate_member13_arg1U3Ec__AnonStorey60__ctor_m2434593288 (U3CEventDelegate_Set_GetDelegate_member13_arg1U3Ec__AnonStorey60_t394562755 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EventDelegateGenerated/<EventDelegate_Set_GetDelegate_member13_arg1>c__AnonStorey60::<>m__4C()
extern "C"  void U3CEventDelegate_Set_GetDelegate_member13_arg1U3Ec__AnonStorey60_U3CU3Em__4C_m1195121056 (U3CEventDelegate_Set_GetDelegate_member13_arg1U3Ec__AnonStorey60_t394562755 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
