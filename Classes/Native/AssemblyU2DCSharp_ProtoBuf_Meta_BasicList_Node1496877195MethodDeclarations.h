﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ProtoBuf.Meta.BasicList/Node
struct Node_t1496877195;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// ProtoBuf.Meta.BasicList/MatchPredicate
struct MatchPredicate_t1402151099;
// System.Array
struct Il2CppArray;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_ProtoBuf_Meta_BasicList_MatchPre1402151099.h"
#include "mscorlib_System_Array1146569071.h"

// System.Void ProtoBuf.Meta.BasicList/Node::.ctor(System.Object[],System.Int32)
extern "C"  void Node__ctor_m320297173 (Node_t1496877195 * __this, ObjectU5BU5D_t1108656482* ___data0, int32_t ___length1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProtoBuf.Meta.BasicList/Node::get_Item(System.Int32)
extern "C"  Il2CppObject * Node_get_Item_m3648754742 (Node_t1496877195 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.BasicList/Node::set_Item(System.Int32,System.Object)
extern "C"  void Node_set_Item_m2883948931 (Node_t1496877195 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ProtoBuf.Meta.BasicList/Node::get_Length()
extern "C"  int32_t Node_get_Length_m2759945041 (Node_t1496877195 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.BasicList/Node::RemoveLastWithMutate()
extern "C"  void Node_RemoveLastWithMutate_m810303962 (Node_t1496877195 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.Meta.BasicList/Node ProtoBuf.Meta.BasicList/Node::Append(System.Object)
extern "C"  Node_t1496877195 * Node_Append_m1787331924 (Node_t1496877195 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.Meta.BasicList/Node ProtoBuf.Meta.BasicList/Node::Trim()
extern "C"  Node_t1496877195 * Node_Trim_m3266645128 (Node_t1496877195 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ProtoBuf.Meta.BasicList/Node::IndexOfString(System.String)
extern "C"  int32_t Node_IndexOfString_m221067464 (Node_t1496877195 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ProtoBuf.Meta.BasicList/Node::IndexOfReference(System.Object)
extern "C"  int32_t Node_IndexOfReference_m3828715312 (Node_t1496877195 * __this, Il2CppObject * ___instance0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ProtoBuf.Meta.BasicList/Node::IndexOf(ProtoBuf.Meta.BasicList/MatchPredicate,System.Object)
extern "C"  int32_t Node_IndexOf_m2304184188 (Node_t1496877195 * __this, MatchPredicate_t1402151099 * ___predicate0, Il2CppObject * ___ctx1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.BasicList/Node::CopyTo(System.Array,System.Int32)
extern "C"  void Node_CopyTo_m3485557581 (Node_t1496877195 * __this, Il2CppArray * ___array0, int32_t ___offset1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.BasicList/Node::Clear()
extern "C"  void Node_Clear_m2798019227 (Node_t1496877195 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
