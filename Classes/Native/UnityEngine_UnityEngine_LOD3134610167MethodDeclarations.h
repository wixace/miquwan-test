﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Renderer[]
struct RendererU5BU5D_t440051646;
// UnityEngine.LOD
struct LOD_t3134610167;
struct LOD_t3134610167_marshaled_pinvoke;
struct LOD_t3134610167_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_LOD3134610167.h"

// System.Void UnityEngine.LOD::.ctor(System.Single,UnityEngine.Renderer[])
extern "C"  void LOD__ctor_m1605605783 (LOD_t3134610167 * __this, float ___screenRelativeTransitionHeight0, RendererU5BU5D_t440051646* ___renderers1, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct LOD_t3134610167;
struct LOD_t3134610167_marshaled_pinvoke;

extern "C" void LOD_t3134610167_marshal_pinvoke(const LOD_t3134610167& unmarshaled, LOD_t3134610167_marshaled_pinvoke& marshaled);
extern "C" void LOD_t3134610167_marshal_pinvoke_back(const LOD_t3134610167_marshaled_pinvoke& marshaled, LOD_t3134610167& unmarshaled);
extern "C" void LOD_t3134610167_marshal_pinvoke_cleanup(LOD_t3134610167_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct LOD_t3134610167;
struct LOD_t3134610167_marshaled_com;

extern "C" void LOD_t3134610167_marshal_com(const LOD_t3134610167& unmarshaled, LOD_t3134610167_marshaled_com& marshaled);
extern "C" void LOD_t3134610167_marshal_com_back(const LOD_t3134610167_marshaled_com& marshaled, LOD_t3134610167& unmarshaled);
extern "C" void LOD_t3134610167_marshal_com_cleanup(LOD_t3134610167_marshaled_com& marshaled);
