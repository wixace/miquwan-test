﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_drakePehir216
struct M_drakePehir216_t2602409832;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_drakePehir2162602409832.h"

// System.Void GarbageiOS.M_drakePehir216::.ctor()
extern "C"  void M_drakePehir216__ctor_m3988556539 (M_drakePehir216_t2602409832 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_drakePehir216::M_jiball0(System.String[],System.Int32)
extern "C"  void M_drakePehir216_M_jiball0_m3698533936 (M_drakePehir216_t2602409832 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_drakePehir216::M_pairnuce1(System.String[],System.Int32)
extern "C"  void M_drakePehir216_M_pairnuce1_m3077207500 (M_drakePehir216_t2602409832 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_drakePehir216::M_saipawerKerenai2(System.String[],System.Int32)
extern "C"  void M_drakePehir216_M_saipawerKerenai2_m3703222355 (M_drakePehir216_t2602409832 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_drakePehir216::ilo_M_jiball01(GarbageiOS.M_drakePehir216,System.String[],System.Int32)
extern "C"  void M_drakePehir216_ilo_M_jiball01_m583543888 (Il2CppObject * __this /* static, unused */, M_drakePehir216_t2602409832 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
