﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.Path
struct Path_t1974241691;
// System.String
struct String_t;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// Pathfinding.GraphNode
struct GraphNode_t23612370;
// System.Object
struct Il2CppObject;
// Pathfinding.PathNode
struct PathNode_t417131581;
// Pathfinding.PathHandler
struct PathHandler_t918952263;
// Pathfinding.PathNNConstraint
struct PathNNConstraint_t2457425624;
// OnPathDelegate
struct OnPathDelegate_t598607977;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PathCompleteState1625108115.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphNode23612370.h"
#include "AssemblyU2DCSharp_Pathfinding_Int31974045594.h"
#include "AssemblyU2DCSharp_PathState1615290188.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_Pathfinding_PathNode417131581.h"
#include "AssemblyU2DCSharp_PathLog873181375.h"
#include "AssemblyU2DCSharp_Pathfinding_PathHandler918952263.h"
#include "AssemblyU2DCSharp_Pathfinding_Path1974241691.h"
#include "AssemblyU2DCSharp_OnPathDelegate598607977.h"

// System.Void Pathfinding.Path::.ctor()
extern "C"  void Path__ctor_m260748076 (Path_t1974241691 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Path::.cctor()
extern "C"  void Path__cctor_m3306126849 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Pathfinding.Path::get_id()
extern "C"  uint32_t Path_get_id_m2019150007 (Path_t1974241691 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Path::set_id(System.UInt32)
extern "C"  void Path_set_id_m2951082026 (Path_t1974241691 * __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PathCompleteState Pathfinding.Path::get_CompleteState()
extern "C"  int32_t Path_get_CompleteState_m747948441 (Path_t1974241691 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Path::set_CompleteState(PathCompleteState)
extern "C"  void Path_set_CompleteState_m1947399642 (Path_t1974241691 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.Path::get_error()
extern "C"  bool Path_get_error_m2055541205 (Path_t1974241691 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Pathfinding.Path::get_errorLog()
extern "C"  String_t* Path_get_errorLog_m3260356928 (Path_t1974241691 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] Pathfinding.Path::get_tagPenalties()
extern "C"  Int32U5BU5D_t3230847821* Path_get_tagPenalties_m3078379162 (Path_t1974241691 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Path::set_tagPenalties(System.Int32[])
extern "C"  void Path_set_tagPenalties_m2491824465 (Path_t1974241691 * __this, Int32U5BU5D_t3230847821* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.Path::get_FloodingPath()
extern "C"  bool Path_get_FloodingPath_m267219454 (Path_t1974241691 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Pathfinding.Path::GetTotalLength()
extern "C"  float Path_GetTotalLength_m2573209272 (Path_t1974241691 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Pathfinding.Path::WaitForPath()
extern "C"  Il2CppObject * Path_WaitForPath_m1260176731 (Path_t1974241691 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Pathfinding.Path::CalculateHScore(Pathfinding.GraphNode)
extern "C"  uint32_t Path_CalculateHScore_m2814933805 (Path_t1974241691 * __this, GraphNode_t23612370 * ___node0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Pathfinding.Path::GetTagPenalty(System.Int32)
extern "C"  uint32_t Path_GetTagPenalty_m3187446821 (Path_t1974241691 * __this, int32_t ___tag0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Int3 Pathfinding.Path::GetHTarget()
extern "C"  Int3_t1974045594  Path_GetHTarget_m1957207402 (Path_t1974241691 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.Path::CanTraverse(Pathfinding.GraphNode)
extern "C"  bool Path_CanTraverse_m2343256244 (Path_t1974241691 * __this, GraphNode_t23612370 * ___node0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Pathfinding.Path::GetTraversalCost(Pathfinding.GraphNode)
extern "C"  uint32_t Path_GetTraversalCost_m1607670404 (Path_t1974241691 * __this, GraphNode_t23612370 * ___node0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Pathfinding.Path::GetConnectionSpecialCost(Pathfinding.GraphNode,Pathfinding.GraphNode,System.UInt32)
extern "C"  uint32_t Path_GetConnectionSpecialCost_m2725876609 (Path_t1974241691 * __this, GraphNode_t23612370 * ___a0, GraphNode_t23612370 * ___b1, uint32_t ___currentCost2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.Path::IsDone()
extern "C"  bool Path_IsDone_m3725184344 (Path_t1974241691 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Path::AdvanceState(PathState)
extern "C"  void Path_AdvanceState_m1272899903 (Path_t1974241691 * __this, int32_t ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PathState Pathfinding.Path::GetState()
extern "C"  int32_t Path_GetState_m708198714 (Path_t1974241691 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Path::LogError(System.String)
extern "C"  void Path_LogError_m461131590 (Path_t1974241691 * __this, String_t* ___msg0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Path::ForceLogError(System.String)
extern "C"  void Path_ForceLogError_m484452073 (Path_t1974241691 * __this, String_t* ___msg0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Path::Log(System.String)
extern "C"  void Path_Log_m2657264884 (Path_t1974241691 * __this, String_t* ___msg0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Path::Error()
extern "C"  void Path_Error_m3921053778 (Path_t1974241691 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Path::ErrorCheck()
extern "C"  void Path_ErrorCheck_m1553155896 (Path_t1974241691 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Path::OnEnterPool()
extern "C"  void Path_OnEnterPool_m703969343 (Path_t1974241691 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Path::Reset()
extern "C"  void Path_Reset_m2202148313 (Path_t1974241691 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.Path::HasExceededTime(System.Int32,System.Int64)
extern "C"  bool Path_HasExceededTime_m944181463 (Path_t1974241691 * __this, int32_t ___searchedNodes0, int64_t ___targetTime1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Path::Claim(System.Object)
extern "C"  void Path_Claim_m587811278 (Path_t1974241691 * __this, Il2CppObject * ___o0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Path::ReleaseSilent(System.Object)
extern "C"  void Path_ReleaseSilent_m2352513998 (Path_t1974241691 * __this, Il2CppObject * ___o0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Path::Release(System.Object)
extern "C"  void Path_Release_m3110428899 (Path_t1974241691 * __this, Il2CppObject * ___o0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Path::Trace(Pathfinding.PathNode)
extern "C"  void Path_Trace_m3370179710 (Path_t1974241691 * __this, PathNode_t417131581 * ___from0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Pathfinding.Path::DebugString(PathLog)
extern "C"  String_t* Path_DebugString_m2169800424 (Path_t1974241691 * __this, int32_t ___logMode0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Path::ReturnPath()
extern "C"  void Path_ReturnPath_m1286605709 (Path_t1974241691 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Path::PrepareBase(Pathfinding.PathHandler)
extern "C"  void Path_PrepareBase_m1829675925 (Path_t1974241691 * __this, PathHandler_t918952263 * ___pathHandler0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Path::Cleanup()
extern "C"  void Path_Cleanup_m4139558830 (Path_t1974241691 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PathCompleteState Pathfinding.Path::ilo_get_CompleteState1(Pathfinding.Path)
extern "C"  int32_t Path_ilo_get_CompleteState1_m2380608542 (Il2CppObject * __this /* static, unused */, Path_t1974241691 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Int3 Pathfinding.Path::ilo_GetHTarget2(Pathfinding.Path)
extern "C"  Int3_t1974045594  Path_ilo_GetHTarget2_m1563981480 (Il2CppObject * __this /* static, unused */, Path_t1974241691 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Int3 Pathfinding.Path::ilo_op_Subtraction3(Pathfinding.Int3,Pathfinding.Int3)
extern "C"  Int3_t1974045594  Path_ilo_op_Subtraction3_m1108827617 (Il2CppObject * __this /* static, unused */, Int3_t1974045594  ___lhs0, Int3_t1974045594  ___rhs1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pathfinding.Path::ilo_get_costMagnitude4(Pathfinding.Int3&)
extern "C"  int32_t Path_ilo_get_costMagnitude4_m2392016807 (Il2CppObject * __this /* static, unused */, Int3_t1974045594 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pathfinding.Path::ilo_get_NodeIndex5(Pathfinding.GraphNode)
extern "C"  int32_t Path_ilo_get_NodeIndex5_m2285509093 (Il2CppObject * __this /* static, unused */, GraphNode_t23612370 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Pathfinding.Path::ilo_get_id6(Pathfinding.GraphNode)
extern "C"  uint32_t Path_ilo_get_id6_m1974921406 (Il2CppObject * __this /* static, unused */, GraphNode_t23612370 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Pathfinding.Path::ilo_get_Tag7(Pathfinding.GraphNode)
extern "C"  uint32_t Path_ilo_get_Tag7_m2614885536 (Il2CppObject * __this /* static, unused */, GraphNode_t23612370 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Pathfinding.Path::ilo_GetTagPenalty8(Pathfinding.Path,System.Int32)
extern "C"  uint32_t Path_ilo_GetTagPenalty8_m3737183885 (Il2CppObject * __this /* static, unused */, Path_t1974241691 * ____this0, int32_t ___tag1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Path::ilo_Error9(Pathfinding.Path)
extern "C"  void Path_ilo_Error9_m800272653 (Il2CppObject * __this /* static, unused */, Path_t1974241691 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.PathNNConstraint Pathfinding.Path::ilo_get_Default10()
extern "C"  PathNNConstraint_t2457425624 * Path_ilo_get_Default10_m2994505565 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Int3 Pathfinding.Path::ilo_get_zero11()
extern "C"  Int3_t1974045594  Path_ilo_get_zero11_m337419371 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.Path::ilo_get_error12(Pathfinding.Path)
extern "C"  bool Path_ilo_get_error12_m2541023860 (Il2CppObject * __this /* static, unused */, Path_t1974241691 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Pathfinding.Path::ilo_get_errorLog13(Pathfinding.Path)
extern "C"  String_t* Path_ilo_get_errorLog13_m537582566 (Il2CppObject * __this /* static, unused */, Path_t1974241691 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Path::ilo_Invoke14(OnPathDelegate,Pathfinding.Path)
extern "C"  void Path_ilo_Invoke14_m3252386266 (Il2CppObject * __this /* static, unused */, OnPathDelegate_t598607977 * ____this0, Path_t1974241691 * ___p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Path::ilo_ClearPathIDs15(Pathfinding.PathHandler)
extern "C"  void Path_ilo_ClearPathIDs15_m545840674 (Il2CppObject * __this /* static, unused */, PathHandler_t918952263 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Path::ilo_ForceLogError16(Pathfinding.Path,System.String)
extern "C"  void Path_ilo_ForceLogError16_m3143238168 (Il2CppObject * __this /* static, unused */, Path_t1974241691 * ____this0, String_t* ___msg1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
