﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CSCampFightAddAttributeData
struct CSCampFightAddAttributeData_t790660836;
// Newtonsoft.Json.Linq.JToken
struct JToken_t3412245951;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JToken3412245951.h"

// System.Void CSCampFightAddAttributeData::.ctor(Newtonsoft.Json.Linq.JToken)
extern "C"  void CSCampFightAddAttributeData__ctor_m1033202883 (CSCampFightAddAttributeData_t790660836 * __this, JToken_t3412245951 * ___jToken0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
