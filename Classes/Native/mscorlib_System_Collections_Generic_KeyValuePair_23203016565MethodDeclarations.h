﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21944668977MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Type,JSCache/TypeInfo>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m360350142(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t3203016565 *, Type_t *, TypeInfo_t3198813374 *, const MethodInfo*))KeyValuePair_2__ctor_m4168265535_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Type,JSCache/TypeInfo>::get_Key()
#define KeyValuePair_2_get_Key_m3242556522(__this, method) ((  Type_t * (*) (KeyValuePair_2_t3203016565 *, const MethodInfo*))KeyValuePair_2_get_Key_m3256475977_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Type,JSCache/TypeInfo>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m3785243051(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3203016565 *, Type_t *, const MethodInfo*))KeyValuePair_2_set_Key_m1278074762_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Type,JSCache/TypeInfo>::get_Value()
#define KeyValuePair_2_get_Value_m2515042474(__this, method) ((  TypeInfo_t3198813374 * (*) (KeyValuePair_2_t3203016565 *, const MethodInfo*))KeyValuePair_2_get_Value_m3899079597_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Type,JSCache/TypeInfo>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m60735531(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3203016565 *, TypeInfo_t3198813374 *, const MethodInfo*))KeyValuePair_2_set_Value_m2954518154_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Type,JSCache/TypeInfo>::ToString()
#define KeyValuePair_2_ToString_m584264535(__this, method) ((  String_t* (*) (KeyValuePair_2_t3203016565 *, const MethodInfo*))KeyValuePair_2_ToString_m1313859518_gshared)(__this, method)
