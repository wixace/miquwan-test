﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_WebCamTextureGenerated
struct UnityEngine_WebCamTextureGenerated_t1581561879;
// JSVCall
struct JSVCall_t3708497963;
// UnityEngine.Color32[]
struct Color32U5BU5D_t2960766953;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"

// System.Void UnityEngine_WebCamTextureGenerated::.ctor()
extern "C"  void UnityEngine_WebCamTextureGenerated__ctor_m1548108388 (UnityEngine_WebCamTextureGenerated_t1581561879 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_WebCamTextureGenerated::WebCamTexture_WebCamTexture1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_WebCamTextureGenerated_WebCamTexture_WebCamTexture1_m1817640084 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_WebCamTextureGenerated::WebCamTexture_WebCamTexture2(JSVCall,System.Int32)
extern "C"  bool UnityEngine_WebCamTextureGenerated_WebCamTexture_WebCamTexture2_m3062404565 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_WebCamTextureGenerated::WebCamTexture_WebCamTexture3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_WebCamTextureGenerated_WebCamTexture_WebCamTexture3_m12201750 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_WebCamTextureGenerated::WebCamTexture_WebCamTexture4(JSVCall,System.Int32)
extern "C"  bool UnityEngine_WebCamTextureGenerated_WebCamTexture_WebCamTexture4_m1256966231 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_WebCamTextureGenerated::WebCamTexture_WebCamTexture5(JSVCall,System.Int32)
extern "C"  bool UnityEngine_WebCamTextureGenerated_WebCamTexture_WebCamTexture5_m2501730712 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_WebCamTextureGenerated::WebCamTexture_WebCamTexture6(JSVCall,System.Int32)
extern "C"  bool UnityEngine_WebCamTextureGenerated_WebCamTexture_WebCamTexture6_m3746495193 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_WebCamTextureGenerated::WebCamTexture_isPlaying(JSVCall)
extern "C"  void UnityEngine_WebCamTextureGenerated_WebCamTexture_isPlaying_m3615529184 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_WebCamTextureGenerated::WebCamTexture_deviceName(JSVCall)
extern "C"  void UnityEngine_WebCamTextureGenerated_WebCamTexture_deviceName_m104468967 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_WebCamTextureGenerated::WebCamTexture_requestedFPS(JSVCall)
extern "C"  void UnityEngine_WebCamTextureGenerated_WebCamTexture_requestedFPS_m347254285 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_WebCamTextureGenerated::WebCamTexture_requestedWidth(JSVCall)
extern "C"  void UnityEngine_WebCamTextureGenerated_WebCamTexture_requestedWidth_m3353189840 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_WebCamTextureGenerated::WebCamTexture_requestedHeight(JSVCall)
extern "C"  void UnityEngine_WebCamTextureGenerated_WebCamTexture_requestedHeight_m1525113583 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_WebCamTextureGenerated::WebCamTexture_devices(JSVCall)
extern "C"  void UnityEngine_WebCamTextureGenerated_WebCamTexture_devices_m1701075783 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_WebCamTextureGenerated::WebCamTexture_videoRotationAngle(JSVCall)
extern "C"  void UnityEngine_WebCamTextureGenerated_WebCamTexture_videoRotationAngle_m1293942478 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_WebCamTextureGenerated::WebCamTexture_videoVerticallyMirrored(JSVCall)
extern "C"  void UnityEngine_WebCamTextureGenerated_WebCamTexture_videoVerticallyMirrored_m2077898216 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_WebCamTextureGenerated::WebCamTexture_didUpdateThisFrame(JSVCall)
extern "C"  void UnityEngine_WebCamTextureGenerated_WebCamTexture_didUpdateThisFrame_m3930869889 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_WebCamTextureGenerated::WebCamTexture_GetPixel__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_WebCamTextureGenerated_WebCamTexture_GetPixel__Int32__Int32_m3808343795 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_WebCamTextureGenerated::WebCamTexture_GetPixels__Int32__Int32__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_WebCamTextureGenerated_WebCamTexture_GetPixels__Int32__Int32__Int32__Int32_m1957236066 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_WebCamTextureGenerated::WebCamTexture_GetPixels(JSVCall,System.Int32)
extern "C"  bool UnityEngine_WebCamTextureGenerated_WebCamTexture_GetPixels_m1971439714 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_WebCamTextureGenerated::WebCamTexture_GetPixels32__Color32_Array(JSVCall,System.Int32)
extern "C"  bool UnityEngine_WebCamTextureGenerated_WebCamTexture_GetPixels32__Color32_Array_m1112354461 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_WebCamTextureGenerated::WebCamTexture_GetPixels32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_WebCamTextureGenerated_WebCamTexture_GetPixels32_m3618807105 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_WebCamTextureGenerated::WebCamTexture_Pause(JSVCall,System.Int32)
extern "C"  bool UnityEngine_WebCamTextureGenerated_WebCamTexture_Pause_m4053017461 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_WebCamTextureGenerated::WebCamTexture_Play(JSVCall,System.Int32)
extern "C"  bool UnityEngine_WebCamTextureGenerated_WebCamTexture_Play_m1457082007 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_WebCamTextureGenerated::WebCamTexture_Stop(JSVCall,System.Int32)
extern "C"  bool UnityEngine_WebCamTextureGenerated_WebCamTexture_Stop_m2856262885 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_WebCamTextureGenerated::__Register()
extern "C"  void UnityEngine_WebCamTextureGenerated___Register_m261691619 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color32[] UnityEngine_WebCamTextureGenerated::<WebCamTexture_GetPixels32__Color32_Array>m__2F2()
extern "C"  Color32U5BU5D_t2960766953* UnityEngine_WebCamTextureGenerated_U3CWebCamTexture_GetPixels32__Color32_ArrayU3Em__2F2_m4237878112 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_WebCamTextureGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UnityEngine_WebCamTextureGenerated_ilo_getObject1_m2536307630 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_WebCamTextureGenerated::ilo_getInt322(System.Int32)
extern "C"  int32_t UnityEngine_WebCamTextureGenerated_ilo_getInt322_m2406174316 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_WebCamTextureGenerated::ilo_addJSCSRel3(System.Int32,System.Object)
extern "C"  void UnityEngine_WebCamTextureGenerated_ilo_addJSCSRel3_m3280683042 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_WebCamTextureGenerated::ilo_attachFinalizerObject4(System.Int32)
extern "C"  bool UnityEngine_WebCamTextureGenerated_ilo_attachFinalizerObject4_m2422246334 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_WebCamTextureGenerated::ilo_moveSaveID2Arr5(System.Int32)
extern "C"  void UnityEngine_WebCamTextureGenerated_ilo_moveSaveID2Arr5_m3814941821 (Il2CppObject * __this /* static, unused */, int32_t ___arrIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_WebCamTextureGenerated::ilo_setArrayS6(System.Int32,System.Int32,System.Boolean)
extern "C"  void UnityEngine_WebCamTextureGenerated_ilo_setArrayS6_m699915492 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___count1, bool ___bClear2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_WebCamTextureGenerated::ilo_setObject7(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UnityEngine_WebCamTextureGenerated_ilo_setObject7_m3775743148 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
