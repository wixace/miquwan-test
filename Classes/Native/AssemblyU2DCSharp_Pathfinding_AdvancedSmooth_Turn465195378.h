﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Pathfinding.AdvancedSmooth/TurnConstructor
struct TurnConstructor_t3543216936;

#include "mscorlib_System_ValueType1744280289.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.AdvancedSmooth/Turn
struct  Turn_t465195378 
{
public:
	// System.Single Pathfinding.AdvancedSmooth/Turn::length
	float ___length_0;
	// System.Int32 Pathfinding.AdvancedSmooth/Turn::id
	int32_t ___id_1;
	// Pathfinding.AdvancedSmooth/TurnConstructor Pathfinding.AdvancedSmooth/Turn::constructor
	TurnConstructor_t3543216936 * ___constructor_2;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(Turn_t465195378, ___length_0)); }
	inline float get_length_0() const { return ___length_0; }
	inline float* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(float value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_id_1() { return static_cast<int32_t>(offsetof(Turn_t465195378, ___id_1)); }
	inline int32_t get_id_1() const { return ___id_1; }
	inline int32_t* get_address_of_id_1() { return &___id_1; }
	inline void set_id_1(int32_t value)
	{
		___id_1 = value;
	}

	inline static int32_t get_offset_of_constructor_2() { return static_cast<int32_t>(offsetof(Turn_t465195378, ___constructor_2)); }
	inline TurnConstructor_t3543216936 * get_constructor_2() const { return ___constructor_2; }
	inline TurnConstructor_t3543216936 ** get_address_of_constructor_2() { return &___constructor_2; }
	inline void set_constructor_2(TurnConstructor_t3543216936 * value)
	{
		___constructor_2 = value;
		Il2CppCodeGenWriteBarrier(&___constructor_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for marshalling of: Pathfinding.AdvancedSmooth/Turn
struct Turn_t465195378_marshaled_pinvoke
{
	float ___length_0;
	int32_t ___id_1;
	TurnConstructor_t3543216936 * ___constructor_2;
};
// Native definition for marshalling of: Pathfinding.AdvancedSmooth/Turn
struct Turn_t465195378_marshaled_com
{
	float ___length_0;
	int32_t ___id_1;
	TurnConstructor_t3543216936 * ___constructor_2;
};
