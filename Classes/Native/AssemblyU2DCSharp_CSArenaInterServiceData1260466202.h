﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// CSAISPlayerData
struct CSAISPlayerData_t3122557766;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CSArenaInterServiceData
struct  CSArenaInterServiceData_t1260466202  : public Il2CppObject
{
public:
	// CSAISPlayerData CSArenaInterServiceData::enemyPlayerData
	CSAISPlayerData_t3122557766 * ___enemyPlayerData_0;
	// CSAISPlayerData CSArenaInterServiceData::ourPlayerData
	CSAISPlayerData_t3122557766 * ___ourPlayerData_1;

public:
	inline static int32_t get_offset_of_enemyPlayerData_0() { return static_cast<int32_t>(offsetof(CSArenaInterServiceData_t1260466202, ___enemyPlayerData_0)); }
	inline CSAISPlayerData_t3122557766 * get_enemyPlayerData_0() const { return ___enemyPlayerData_0; }
	inline CSAISPlayerData_t3122557766 ** get_address_of_enemyPlayerData_0() { return &___enemyPlayerData_0; }
	inline void set_enemyPlayerData_0(CSAISPlayerData_t3122557766 * value)
	{
		___enemyPlayerData_0 = value;
		Il2CppCodeGenWriteBarrier(&___enemyPlayerData_0, value);
	}

	inline static int32_t get_offset_of_ourPlayerData_1() { return static_cast<int32_t>(offsetof(CSArenaInterServiceData_t1260466202, ___ourPlayerData_1)); }
	inline CSAISPlayerData_t3122557766 * get_ourPlayerData_1() const { return ___ourPlayerData_1; }
	inline CSAISPlayerData_t3122557766 ** get_address_of_ourPlayerData_1() { return &___ourPlayerData_1; }
	inline void set_ourPlayerData_1(CSAISPlayerData_t3122557766 * value)
	{
		___ourPlayerData_1 = value;
		Il2CppCodeGenWriteBarrier(&___ourPlayerData_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
