﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ShakeCamera
struct ShakeCamera_t1941686699;

#include "codegen/il2cpp-codegen.h"

// System.Void ShakeCamera::.ctor()
extern "C"  void ShakeCamera__ctor_m3098567200 (ShakeCamera_t1941686699 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ShakeCamera::get_isshake()
extern "C"  bool ShakeCamera_get_isshake_m1598133957 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShakeCamera::set_isshake(System.Boolean)
extern "C"  void ShakeCamera_set_isshake_m1921330644 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShakeCamera::Awake()
extern "C"  void ShakeCamera_Awake_m3336172419 (ShakeCamera_t1941686699 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShakeCamera::OnFinished()
extern "C"  void ShakeCamera_OnFinished_m4276415509 (ShakeCamera_t1941686699 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShakeCamera::OnUpdate(System.Single,System.Boolean)
extern "C"  void ShakeCamera_OnUpdate_m3870242718 (ShakeCamera_t1941686699 * __this, float ___factor0, bool ___isFinished1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShakeCamera::StartShake(System.Single,System.Single)
extern "C"  void ShakeCamera_StartShake_m3639145640 (Il2CppObject * __this /* static, unused */, float ___strength0, float ___time1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShakeCamera::ilo_set_isshake1(System.Boolean)
extern "C"  void ShakeCamera_ilo_set_isshake1_m772310912 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
