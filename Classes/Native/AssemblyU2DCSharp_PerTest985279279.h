﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Int32[]
struct Int32U5BU5D_t3230847821;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PerTest
struct  PerTest_t985279279  : public Il2CppObject
{
public:
	// System.Int32[] PerTest::test
	Int32U5BU5D_t3230847821* ___test_0;

public:
	inline static int32_t get_offset_of_test_0() { return static_cast<int32_t>(offsetof(PerTest_t985279279, ___test_0)); }
	inline Int32U5BU5D_t3230847821* get_test_0() const { return ___test_0; }
	inline Int32U5BU5D_t3230847821** get_address_of_test_0() { return &___test_0; }
	inline void set_test_0(Int32U5BU5D_t3230847821* value)
	{
		___test_0 = value;
		Il2CppCodeGenWriteBarrier(&___test_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
