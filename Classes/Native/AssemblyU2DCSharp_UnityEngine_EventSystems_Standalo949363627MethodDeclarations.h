﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_EventSystems_StandaloneInputModuleGenerated
struct UnityEngine_EventSystems_StandaloneInputModuleGenerated_t949363627;
// JSVCall
struct JSVCall_t3708497963;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_String7231557.h"

// System.Void UnityEngine_EventSystems_StandaloneInputModuleGenerated::.ctor()
extern "C"  void UnityEngine_EventSystems_StandaloneInputModuleGenerated__ctor_m342395936 (UnityEngine_EventSystems_StandaloneInputModuleGenerated_t949363627 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_StandaloneInputModuleGenerated::StandaloneInputModule_forceModuleActive(JSVCall)
extern "C"  void UnityEngine_EventSystems_StandaloneInputModuleGenerated_StandaloneInputModule_forceModuleActive_m3148241690 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_StandaloneInputModuleGenerated::StandaloneInputModule_inputActionsPerSecond(JSVCall)
extern "C"  void UnityEngine_EventSystems_StandaloneInputModuleGenerated_StandaloneInputModule_inputActionsPerSecond_m2444380025 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_StandaloneInputModuleGenerated::StandaloneInputModule_repeatDelay(JSVCall)
extern "C"  void UnityEngine_EventSystems_StandaloneInputModuleGenerated_StandaloneInputModule_repeatDelay_m175934127 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_StandaloneInputModuleGenerated::StandaloneInputModule_horizontalAxis(JSVCall)
extern "C"  void UnityEngine_EventSystems_StandaloneInputModuleGenerated_StandaloneInputModule_horizontalAxis_m3803039440 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_StandaloneInputModuleGenerated::StandaloneInputModule_verticalAxis(JSVCall)
extern "C"  void UnityEngine_EventSystems_StandaloneInputModuleGenerated_StandaloneInputModule_verticalAxis_m2300409982 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_StandaloneInputModuleGenerated::StandaloneInputModule_submitButton(JSVCall)
extern "C"  void UnityEngine_EventSystems_StandaloneInputModuleGenerated_StandaloneInputModule_submitButton_m3388430507 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_StandaloneInputModuleGenerated::StandaloneInputModule_cancelButton(JSVCall)
extern "C"  void UnityEngine_EventSystems_StandaloneInputModuleGenerated_StandaloneInputModule_cancelButton_m2396428809 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_EventSystems_StandaloneInputModuleGenerated::StandaloneInputModule_ActivateModule(JSVCall,System.Int32)
extern "C"  bool UnityEngine_EventSystems_StandaloneInputModuleGenerated_StandaloneInputModule_ActivateModule_m1247242445 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_EventSystems_StandaloneInputModuleGenerated::StandaloneInputModule_DeactivateModule(JSVCall,System.Int32)
extern "C"  bool UnityEngine_EventSystems_StandaloneInputModuleGenerated_StandaloneInputModule_DeactivateModule_m3679784334 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_EventSystems_StandaloneInputModuleGenerated::StandaloneInputModule_IsModuleSupported(JSVCall,System.Int32)
extern "C"  bool UnityEngine_EventSystems_StandaloneInputModuleGenerated_StandaloneInputModule_IsModuleSupported_m2895726412 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_EventSystems_StandaloneInputModuleGenerated::StandaloneInputModule_Process(JSVCall,System.Int32)
extern "C"  bool UnityEngine_EventSystems_StandaloneInputModuleGenerated_StandaloneInputModule_Process_m2339143971 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_EventSystems_StandaloneInputModuleGenerated::StandaloneInputModule_ShouldActivateModule(JSVCall,System.Int32)
extern "C"  bool UnityEngine_EventSystems_StandaloneInputModuleGenerated_StandaloneInputModule_ShouldActivateModule_m3520418656 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_EventSystems_StandaloneInputModuleGenerated::StandaloneInputModule_UpdateModule(JSVCall,System.Int32)
extern "C"  bool UnityEngine_EventSystems_StandaloneInputModuleGenerated_StandaloneInputModule_UpdateModule_m1232476899 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_StandaloneInputModuleGenerated::__Register()
extern "C"  void UnityEngine_EventSystems_StandaloneInputModuleGenerated___Register_m4014703783 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_EventSystems_StandaloneInputModuleGenerated::ilo_getBooleanS1(System.Int32)
extern "C"  bool UnityEngine_EventSystems_StandaloneInputModuleGenerated_ilo_getBooleanS1_m4107647172 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_StandaloneInputModuleGenerated::ilo_setSingle2(System.Int32,System.Single)
extern "C"  void UnityEngine_EventSystems_StandaloneInputModuleGenerated_ilo_setSingle2_m3611950293 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine_EventSystems_StandaloneInputModuleGenerated::ilo_getStringS3(System.Int32)
extern "C"  String_t* UnityEngine_EventSystems_StandaloneInputModuleGenerated_ilo_getStringS3_m4274288582 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_StandaloneInputModuleGenerated::ilo_setStringS4(System.Int32,System.String)
extern "C"  void UnityEngine_EventSystems_StandaloneInputModuleGenerated_ilo_setStringS4_m385154156 (Il2CppObject * __this /* static, unused */, int32_t ___e0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_StandaloneInputModuleGenerated::ilo_setBooleanS5(System.Int32,System.Boolean)
extern "C"  void UnityEngine_EventSystems_StandaloneInputModuleGenerated_ilo_setBooleanS5_m2160100621 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
