﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_CsCfgBase69924517.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// npcAIBehaviorCfg
struct  npcAIBehaviorCfg_t3456736809  : public CsCfgBase_t69924517
{
public:
	// System.Int32 npcAIBehaviorCfg::id
	int32_t ___id_0;
	// System.Int32 npcAIBehaviorCfg::type
	int32_t ___type_1;
	// System.Int32 npcAIBehaviorCfg::param1
	int32_t ___param1_2;
	// System.Int32 npcAIBehaviorCfg::param2
	int32_t ___param2_3;
	// System.Int32 npcAIBehaviorCfg::param3
	int32_t ___param3_4;
	// System.Int32 npcAIBehaviorCfg::param4
	int32_t ___param4_5;
	// System.Int32 npcAIBehaviorCfg::param5
	int32_t ___param5_6;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(npcAIBehaviorCfg_t3456736809, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_type_1() { return static_cast<int32_t>(offsetof(npcAIBehaviorCfg_t3456736809, ___type_1)); }
	inline int32_t get_type_1() const { return ___type_1; }
	inline int32_t* get_address_of_type_1() { return &___type_1; }
	inline void set_type_1(int32_t value)
	{
		___type_1 = value;
	}

	inline static int32_t get_offset_of_param1_2() { return static_cast<int32_t>(offsetof(npcAIBehaviorCfg_t3456736809, ___param1_2)); }
	inline int32_t get_param1_2() const { return ___param1_2; }
	inline int32_t* get_address_of_param1_2() { return &___param1_2; }
	inline void set_param1_2(int32_t value)
	{
		___param1_2 = value;
	}

	inline static int32_t get_offset_of_param2_3() { return static_cast<int32_t>(offsetof(npcAIBehaviorCfg_t3456736809, ___param2_3)); }
	inline int32_t get_param2_3() const { return ___param2_3; }
	inline int32_t* get_address_of_param2_3() { return &___param2_3; }
	inline void set_param2_3(int32_t value)
	{
		___param2_3 = value;
	}

	inline static int32_t get_offset_of_param3_4() { return static_cast<int32_t>(offsetof(npcAIBehaviorCfg_t3456736809, ___param3_4)); }
	inline int32_t get_param3_4() const { return ___param3_4; }
	inline int32_t* get_address_of_param3_4() { return &___param3_4; }
	inline void set_param3_4(int32_t value)
	{
		___param3_4 = value;
	}

	inline static int32_t get_offset_of_param4_5() { return static_cast<int32_t>(offsetof(npcAIBehaviorCfg_t3456736809, ___param4_5)); }
	inline int32_t get_param4_5() const { return ___param4_5; }
	inline int32_t* get_address_of_param4_5() { return &___param4_5; }
	inline void set_param4_5(int32_t value)
	{
		___param4_5 = value;
	}

	inline static int32_t get_offset_of_param5_6() { return static_cast<int32_t>(offsetof(npcAIBehaviorCfg_t3456736809, ___param5_6)); }
	inline int32_t get_param5_6() const { return ___param5_6; }
	inline int32_t* get_address_of_param5_6() { return &___param5_6; }
	inline void set_param5_6(int32_t value)
	{
		___param5_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
