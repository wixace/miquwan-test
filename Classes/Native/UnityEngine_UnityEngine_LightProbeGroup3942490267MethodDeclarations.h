﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.LightProbeGroup
struct LightProbeGroup_t3942490267;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t215400611;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.LightProbeGroup::.ctor()
extern "C"  void LightProbeGroup__ctor_m2628703124 (LightProbeGroup_t3942490267 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3[] UnityEngine.LightProbeGroup::get_probePositions()
extern "C"  Vector3U5BU5D_t215400611* LightProbeGroup_get_probePositions_m3334822043 (LightProbeGroup_t3942490267 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.LightProbeGroup::set_probePositions(UnityEngine.Vector3[])
extern "C"  void LightProbeGroup_set_probePositions_m1975340016 (LightProbeGroup_t3942490267 * __this, Vector3U5BU5D_t215400611* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
