﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.List`1<MHIAPMgr.PayInfo>
struct List_1_t1812615788;
// System.Collections.Generic.List`1<Mihua.Net.UrlLoader>
struct List_1_t3858915048;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1375417109;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t1974256870;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MihuaPayMgr
struct  MihuaPayMgr_t1240399912  : public Il2CppObject
{
public:
	// System.String MihuaPayMgr::path
	String_t* ___path_1;
	// System.String MihuaPayMgr::old
	String_t* ___old_2;
	// System.Collections.Generic.List`1<MHIAPMgr.PayInfo> MihuaPayMgr::payInfoList
	List_1_t1812615788 * ___payInfoList_3;
	// System.Collections.Generic.List`1<Mihua.Net.UrlLoader> MihuaPayMgr::requestList
	List_1_t3858915048 * ___requestList_4;
	// System.Collections.Generic.List`1<System.String> MihuaPayMgr::requestKeyList
	List_1_t1375417109 * ___requestKeyList_5;

public:
	inline static int32_t get_offset_of_path_1() { return static_cast<int32_t>(offsetof(MihuaPayMgr_t1240399912, ___path_1)); }
	inline String_t* get_path_1() const { return ___path_1; }
	inline String_t** get_address_of_path_1() { return &___path_1; }
	inline void set_path_1(String_t* value)
	{
		___path_1 = value;
		Il2CppCodeGenWriteBarrier(&___path_1, value);
	}

	inline static int32_t get_offset_of_old_2() { return static_cast<int32_t>(offsetof(MihuaPayMgr_t1240399912, ___old_2)); }
	inline String_t* get_old_2() const { return ___old_2; }
	inline String_t** get_address_of_old_2() { return &___old_2; }
	inline void set_old_2(String_t* value)
	{
		___old_2 = value;
		Il2CppCodeGenWriteBarrier(&___old_2, value);
	}

	inline static int32_t get_offset_of_payInfoList_3() { return static_cast<int32_t>(offsetof(MihuaPayMgr_t1240399912, ___payInfoList_3)); }
	inline List_1_t1812615788 * get_payInfoList_3() const { return ___payInfoList_3; }
	inline List_1_t1812615788 ** get_address_of_payInfoList_3() { return &___payInfoList_3; }
	inline void set_payInfoList_3(List_1_t1812615788 * value)
	{
		___payInfoList_3 = value;
		Il2CppCodeGenWriteBarrier(&___payInfoList_3, value);
	}

	inline static int32_t get_offset_of_requestList_4() { return static_cast<int32_t>(offsetof(MihuaPayMgr_t1240399912, ___requestList_4)); }
	inline List_1_t3858915048 * get_requestList_4() const { return ___requestList_4; }
	inline List_1_t3858915048 ** get_address_of_requestList_4() { return &___requestList_4; }
	inline void set_requestList_4(List_1_t3858915048 * value)
	{
		___requestList_4 = value;
		Il2CppCodeGenWriteBarrier(&___requestList_4, value);
	}

	inline static int32_t get_offset_of_requestKeyList_5() { return static_cast<int32_t>(offsetof(MihuaPayMgr_t1240399912, ___requestKeyList_5)); }
	inline List_1_t1375417109 * get_requestKeyList_5() const { return ___requestKeyList_5; }
	inline List_1_t1375417109 ** get_address_of_requestKeyList_5() { return &___requestKeyList_5; }
	inline void set_requestKeyList_5(List_1_t1375417109 * value)
	{
		___requestKeyList_5 = value;
		Il2CppCodeGenWriteBarrier(&___requestKeyList_5, value);
	}
};

struct MihuaPayMgr_t1240399912_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> MihuaPayMgr::<>f__switch$map1D
	Dictionary_2_t1974256870 * ___U3CU3Ef__switchU24map1D_6;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map1D_6() { return static_cast<int32_t>(offsetof(MihuaPayMgr_t1240399912_StaticFields, ___U3CU3Ef__switchU24map1D_6)); }
	inline Dictionary_2_t1974256870 * get_U3CU3Ef__switchU24map1D_6() const { return ___U3CU3Ef__switchU24map1D_6; }
	inline Dictionary_2_t1974256870 ** get_address_of_U3CU3Ef__switchU24map1D_6() { return &___U3CU3Ef__switchU24map1D_6; }
	inline void set_U3CU3Ef__switchU24map1D_6(Dictionary_2_t1974256870 * value)
	{
		___U3CU3Ef__switchU24map1D_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map1D_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
