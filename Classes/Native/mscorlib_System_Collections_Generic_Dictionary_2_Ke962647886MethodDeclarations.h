﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/KeyCollection<AIEnum.EAIEventtype,System.Object>
struct KeyCollection_t962647886;
// System.Collections.Generic.Dictionary`2<AIEnum.EAIEventtype,System.Object>
struct Dictionary_2_t3630855731;
// System.Collections.Generic.IEnumerator`1<AIEnum.EAIEventtype>
struct IEnumerator_1_t1513187064;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// AIEnum.EAIEventtype[]
struct EAIEventtypeU5BU5D_t2693623942;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_AIEnum_EAIEventtype3896289311.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K4245791785.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<AIEnum.EAIEventtype,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void KeyCollection__ctor_m1657465302_gshared (KeyCollection_t962647886 * __this, Dictionary_2_t3630855731 * ___dictionary0, const MethodInfo* method);
#define KeyCollection__ctor_m1657465302(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t962647886 *, Dictionary_2_t3630855731 *, const MethodInfo*))KeyCollection__ctor_m1657465302_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<AIEnum.EAIEventtype,System.Object>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1679368640_gshared (KeyCollection_t962647886 * __this, uint8_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1679368640(__this, ___item0, method) ((  void (*) (KeyCollection_t962647886 *, uint8_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1679368640_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<AIEnum.EAIEventtype,System.Object>::System.Collections.Generic.ICollection<TKey>.Clear()
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1606203191_gshared (KeyCollection_t962647886 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1606203191(__this, method) ((  void (*) (KeyCollection_t962647886 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1606203191_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<AIEnum.EAIEventtype,System.Object>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m4028784330_gshared (KeyCollection_t962647886 * __this, uint8_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m4028784330(__this, ___item0, method) ((  bool (*) (KeyCollection_t962647886 *, uint8_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m4028784330_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<AIEnum.EAIEventtype,System.Object>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1814821551_gshared (KeyCollection_t962647886 * __this, uint8_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1814821551(__this, ___item0, method) ((  bool (*) (KeyCollection_t962647886 *, uint8_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1814821551_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<AIEnum.EAIEventtype,System.Object>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m4103627315_gshared (KeyCollection_t962647886 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m4103627315(__this, method) ((  Il2CppObject* (*) (KeyCollection_t962647886 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m4103627315_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<AIEnum.EAIEventtype,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m1307214121_gshared (KeyCollection_t962647886 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_CopyTo_m1307214121(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t962647886 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m1307214121_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<AIEnum.EAIEventtype,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m2765238372_gshared (KeyCollection_t962647886 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m2765238372(__this, method) ((  Il2CppObject * (*) (KeyCollection_t962647886 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m2765238372_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<AIEnum.EAIEventtype,System.Object>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m482298795_gshared (KeyCollection_t962647886 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m482298795(__this, method) ((  bool (*) (KeyCollection_t962647886 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m482298795_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<AIEnum.EAIEventtype,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m959503901_gshared (KeyCollection_t962647886 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m959503901(__this, method) ((  bool (*) (KeyCollection_t962647886 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m959503901_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<AIEnum.EAIEventtype,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m2273377481_gshared (KeyCollection_t962647886 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m2273377481(__this, method) ((  Il2CppObject * (*) (KeyCollection_t962647886 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m2273377481_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<AIEnum.EAIEventtype,System.Object>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m2376411275_gshared (KeyCollection_t962647886 * __this, EAIEventtypeU5BU5D_t2693623942* ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_CopyTo_m2376411275(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t962647886 *, EAIEventtypeU5BU5D_t2693623942*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m2376411275_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<AIEnum.EAIEventtype,System.Object>::GetEnumerator()
extern "C"  Enumerator_t4245791785  KeyCollection_GetEnumerator_m626970286_gshared (KeyCollection_t962647886 * __this, const MethodInfo* method);
#define KeyCollection_GetEnumerator_m626970286(__this, method) ((  Enumerator_t4245791785  (*) (KeyCollection_t962647886 *, const MethodInfo*))KeyCollection_GetEnumerator_m626970286_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<AIEnum.EAIEventtype,System.Object>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m864530531_gshared (KeyCollection_t962647886 * __this, const MethodInfo* method);
#define KeyCollection_get_Count_m864530531(__this, method) ((  int32_t (*) (KeyCollection_t962647886 *, const MethodInfo*))KeyCollection_get_Count_m864530531_gshared)(__this, method)
