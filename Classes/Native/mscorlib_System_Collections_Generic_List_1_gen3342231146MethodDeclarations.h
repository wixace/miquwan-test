﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<Pathfinding.Int3>
struct List_1_t3342231146;
// System.Collections.Generic.IEnumerable`1<Pathfinding.Int3>
struct IEnumerable_1_t979991255;
// System.Collections.Generic.IEnumerator`1<Pathfinding.Int3>
struct IEnumerator_1_t3885910643;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.ICollection`1<Pathfinding.Int3>
struct ICollection_1_t2868635581;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Int3>
struct ReadOnlyCollection_1_t3531123130;
// Pathfinding.Int3[]
struct Int3U5BU5D_t516284607;
// System.Predicate`1<Pathfinding.Int3>
struct Predicate_1_t1585102477;
// System.Collections.Generic.IComparer`1<Pathfinding.Int3>
struct IComparer_1_t254092340;
// System.Comparison`1<Pathfinding.Int3>
struct Comparison_1_t690406781;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_Pathfinding_Int31974045594.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3361903916.h"

// System.Void System.Collections.Generic.List`1<Pathfinding.Int3>::.ctor()
extern "C"  void List_1__ctor_m3819120258_gshared (List_1_t3342231146 * __this, const MethodInfo* method);
#define List_1__ctor_m3819120258(__this, method) ((  void (*) (List_1_t3342231146 *, const MethodInfo*))List_1__ctor_m3819120258_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Int3>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1__ctor_m846497981_gshared (List_1_t3342231146 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1__ctor_m846497981(__this, ___collection0, method) ((  void (*) (List_1_t3342231146 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m846497981_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Int3>::.ctor(System.Int32)
extern "C"  void List_1__ctor_m791750867_gshared (List_1_t3342231146 * __this, int32_t ___capacity0, const MethodInfo* method);
#define List_1__ctor_m791750867(__this, ___capacity0, method) ((  void (*) (List_1_t3342231146 *, int32_t, const MethodInfo*))List_1__ctor_m791750867_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Int3>::.cctor()
extern "C"  void List_1__cctor_m1946514795_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m1946514795(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m1946514795_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Pathfinding.Int3>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3886645772_gshared (List_1_t3342231146 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3886645772(__this, method) ((  Il2CppObject* (*) (List_1_t3342231146 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3886645772_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Int3>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m3269358850_gshared (List_1_t3342231146 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m3269358850(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t3342231146 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m3269358850_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<Pathfinding.Int3>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m963306897_gshared (List_1_t3342231146 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m963306897(__this, method) ((  Il2CppObject * (*) (List_1_t3342231146 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m963306897_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Pathfinding.Int3>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_Add_m2400558668_gshared (List_1_t3342231146 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m2400558668(__this, ___item0, method) ((  int32_t (*) (List_1_t3342231146 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m2400558668_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<Pathfinding.Int3>::System.Collections.IList.Contains(System.Object)
extern "C"  bool List_1_System_Collections_IList_Contains_m1139803252_gshared (List_1_t3342231146 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m1139803252(__this, ___item0, method) ((  bool (*) (List_1_t3342231146 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m1139803252_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<Pathfinding.Int3>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m1652936356_gshared (List_1_t3342231146 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m1652936356(__this, ___item0, method) ((  int32_t (*) (List_1_t3342231146 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m1652936356_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Int3>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_Insert_m1077933591_gshared (List_1_t3342231146 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m1077933591(__this, ___index0, ___item1, method) ((  void (*) (List_1_t3342231146 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m1077933591_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Int3>::System.Collections.IList.Remove(System.Object)
extern "C"  void List_1_System_Collections_IList_Remove_m3674905009_gshared (List_1_t3342231146 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m3674905009(__this, ___item0, method) ((  void (*) (List_1_t3342231146 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m3674905009_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<Pathfinding.Int3>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3255300981_gshared (List_1_t3342231146 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3255300981(__this, method) ((  bool (*) (List_1_t3342231146 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3255300981_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Pathfinding.Int3>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool List_1_System_Collections_ICollection_get_IsSynchronized_m4157181544_gshared (List_1_t3342231146 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m4157181544(__this, method) ((  bool (*) (List_1_t3342231146 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m4157181544_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Pathfinding.Int3>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m1619186394_gshared (List_1_t3342231146 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m1619186394(__this, method) ((  Il2CppObject * (*) (List_1_t3342231146 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m1619186394_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Pathfinding.Int3>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool List_1_System_Collections_IList_get_IsFixedSize_m1705056995_gshared (List_1_t3342231146 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m1705056995(__this, method) ((  bool (*) (List_1_t3342231146 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m1705056995_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Pathfinding.Int3>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_IList_get_IsReadOnly_m2864315318_gshared (List_1_t3342231146 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m2864315318(__this, method) ((  bool (*) (List_1_t3342231146 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m2864315318_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Pathfinding.Int3>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m2325575137_gshared (List_1_t3342231146 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m2325575137(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t3342231146 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m2325575137_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Int3>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_set_Item_m1421015086_gshared (List_1_t3342231146 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m1421015086(__this, ___index0, ___value1, method) ((  void (*) (List_1_t3342231146 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m1421015086_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Int3>::Add(T)
extern "C"  void List_1_Add_m1976047465_gshared (List_1_t3342231146 * __this, Int3_t1974045594  ___item0, const MethodInfo* method);
#define List_1_Add_m1976047465(__this, ___item0, method) ((  void (*) (List_1_t3342231146 *, Int3_t1974045594 , const MethodInfo*))List_1_Add_m1976047465_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Int3>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m2427055992_gshared (List_1_t3342231146 * __this, int32_t ___newCount0, const MethodInfo* method);
#define List_1_GrowIfNeeded_m2427055992(__this, ___newCount0, method) ((  void (*) (List_1_t3342231146 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m2427055992_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Int3>::CheckRange(System.Int32,System.Int32)
extern "C"  void List_1_CheckRange_m3771357551_gshared (List_1_t3342231146 * __this, int32_t ___idx0, int32_t ___count1, const MethodInfo* method);
#define List_1_CheckRange_m3771357551(__this, ___idx0, ___count1, method) ((  void (*) (List_1_t3342231146 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m3771357551_gshared)(__this, ___idx0, ___count1, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Int3>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m1061054070_gshared (List_1_t3342231146 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddCollection_m1061054070(__this, ___collection0, method) ((  void (*) (List_1_t3342231146 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m1061054070_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Int3>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddEnumerable_m322026294_gshared (List_1_t3342231146 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method);
#define List_1_AddEnumerable_m322026294(__this, ___enumerable0, method) ((  void (*) (List_1_t3342231146 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m322026294_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Int3>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m2024311201_gshared (List_1_t3342231146 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddRange_m2024311201(__this, ___collection0, method) ((  void (*) (List_1_t3342231146 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m2024311201_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<Pathfinding.Int3>::AsReadOnly()
extern "C"  ReadOnlyCollection_1_t3531123130 * List_1_AsReadOnly_m3861550660_gshared (List_1_t3342231146 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m3861550660(__this, method) ((  ReadOnlyCollection_1_t3531123130 * (*) (List_1_t3342231146 *, const MethodInfo*))List_1_AsReadOnly_m3861550660_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Pathfinding.Int3>::BinarySearch(T)
extern "C"  int32_t List_1_BinarySearch_m423314575_gshared (List_1_t3342231146 * __this, Int3_t1974045594  ___item0, const MethodInfo* method);
#define List_1_BinarySearch_m423314575(__this, ___item0, method) ((  int32_t (*) (List_1_t3342231146 *, Int3_t1974045594 , const MethodInfo*))List_1_BinarySearch_m423314575_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Int3>::Clear()
extern "C"  void List_1_Clear_m1225253549_gshared (List_1_t3342231146 * __this, const MethodInfo* method);
#define List_1_Clear_m1225253549(__this, method) ((  void (*) (List_1_t3342231146 *, const MethodInfo*))List_1_Clear_m1225253549_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Pathfinding.Int3>::Contains(T)
extern "C"  bool List_1_Contains_m2744447007_gshared (List_1_t3342231146 * __this, Int3_t1974045594  ___item0, const MethodInfo* method);
#define List_1_Contains_m2744447007(__this, ___item0, method) ((  bool (*) (List_1_t3342231146 *, Int3_t1974045594 , const MethodInfo*))List_1_Contains_m2744447007_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Int3>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m40227949_gshared (List_1_t3342231146 * __this, Int3U5BU5D_t516284607* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_CopyTo_m40227949(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t3342231146 *, Int3U5BU5D_t516284607*, int32_t, const MethodInfo*))List_1_CopyTo_m40227949_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<Pathfinding.Int3>::Find(System.Predicate`1<T>)
extern "C"  Int3_t1974045594  List_1_Find_m2851936633_gshared (List_1_t3342231146 * __this, Predicate_1_t1585102477 * ___match0, const MethodInfo* method);
#define List_1_Find_m2851936633(__this, ___match0, method) ((  Int3_t1974045594  (*) (List_1_t3342231146 *, Predicate_1_t1585102477 *, const MethodInfo*))List_1_Find_m2851936633_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Int3>::CheckMatch(System.Predicate`1<T>)
extern "C"  void List_1_CheckMatch_m577762582_gshared (Il2CppObject * __this /* static, unused */, Predicate_1_t1585102477 * ___match0, const MethodInfo* method);
#define List_1_CheckMatch_m577762582(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t1585102477 *, const MethodInfo*))List_1_CheckMatch_m577762582_gshared)(__this /* static, unused */, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<Pathfinding.Int3>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C"  int32_t List_1_GetIndex_m150094579_gshared (List_1_t3342231146 * __this, int32_t ___startIndex0, int32_t ___count1, Predicate_1_t1585102477 * ___match2, const MethodInfo* method);
#define List_1_GetIndex_m150094579(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t3342231146 *, int32_t, int32_t, Predicate_1_t1585102477 *, const MethodInfo*))List_1_GetIndex_m150094579_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Pathfinding.Int3>::GetEnumerator()
extern "C"  Enumerator_t3361903916  List_1_GetEnumerator_m431983068_gshared (List_1_t3342231146 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m431983068(__this, method) ((  Enumerator_t3361903916  (*) (List_1_t3342231146 *, const MethodInfo*))List_1_GetEnumerator_m431983068_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Pathfinding.Int3>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m2430081785_gshared (List_1_t3342231146 * __this, Int3_t1974045594  ___item0, const MethodInfo* method);
#define List_1_IndexOf_m2430081785(__this, ___item0, method) ((  int32_t (*) (List_1_t3342231146 *, Int3_t1974045594 , const MethodInfo*))List_1_IndexOf_m2430081785_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Int3>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m114093508_gshared (List_1_t3342231146 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method);
#define List_1_Shift_m114093508(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t3342231146 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m114093508_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Int3>::CheckIndex(System.Int32)
extern "C"  void List_1_CheckIndex_m4081562429_gshared (List_1_t3342231146 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_CheckIndex_m4081562429(__this, ___index0, method) ((  void (*) (List_1_t3342231146 *, int32_t, const MethodInfo*))List_1_CheckIndex_m4081562429_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Int3>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m509092388_gshared (List_1_t3342231146 * __this, int32_t ___index0, Int3_t1974045594  ___item1, const MethodInfo* method);
#define List_1_Insert_m509092388(__this, ___index0, ___item1, method) ((  void (*) (List_1_t3342231146 *, int32_t, Int3_t1974045594 , const MethodInfo*))List_1_Insert_m509092388_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Int3>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_CheckCollection_m2292380569_gshared (List_1_t3342231146 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_CheckCollection_m2292380569(__this, ___collection0, method) ((  void (*) (List_1_t3342231146 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m2292380569_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<Pathfinding.Int3>::Remove(T)
extern "C"  bool List_1_Remove_m2067082202_gshared (List_1_t3342231146 * __this, Int3_t1974045594  ___item0, const MethodInfo* method);
#define List_1_Remove_m2067082202(__this, ___item0, method) ((  bool (*) (List_1_t3342231146 *, Int3_t1974045594 , const MethodInfo*))List_1_Remove_m2067082202_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<Pathfinding.Int3>::RemoveAll(System.Predicate`1<T>)
extern "C"  int32_t List_1_RemoveAll_m1338077692_gshared (List_1_t3342231146 * __this, Predicate_1_t1585102477 * ___match0, const MethodInfo* method);
#define List_1_RemoveAll_m1338077692(__this, ___match0, method) ((  int32_t (*) (List_1_t3342231146 *, Predicate_1_t1585102477 *, const MethodInfo*))List_1_RemoveAll_m1338077692_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Int3>::RemoveAt(System.Int32)
extern "C"  void List_1_RemoveAt_m2677912554_gshared (List_1_t3342231146 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_RemoveAt_m2677912554(__this, ___index0, method) ((  void (*) (List_1_t3342231146 *, int32_t, const MethodInfo*))List_1_RemoveAt_m2677912554_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Int3>::RemoveRange(System.Int32,System.Int32)
extern "C"  void List_1_RemoveRange_m683179021_gshared (List_1_t3342231146 * __this, int32_t ___index0, int32_t ___count1, const MethodInfo* method);
#define List_1_RemoveRange_m683179021(__this, ___index0, ___count1, method) ((  void (*) (List_1_t3342231146 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m683179021_gshared)(__this, ___index0, ___count1, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Int3>::Reverse()
extern "C"  void List_1_Reverse_m2310308418_gshared (List_1_t3342231146 * __this, const MethodInfo* method);
#define List_1_Reverse_m2310308418(__this, method) ((  void (*) (List_1_t3342231146 *, const MethodInfo*))List_1_Reverse_m2310308418_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Int3>::Sort()
extern "C"  void List_1_Sort_m1886237856_gshared (List_1_t3342231146 * __this, const MethodInfo* method);
#define List_1_Sort_m1886237856(__this, method) ((  void (*) (List_1_t3342231146 *, const MethodInfo*))List_1_Sort_m1886237856_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Int3>::Sort(System.Collections.Generic.IComparer`1<T>)
extern "C"  void List_1_Sort_m2255701316_gshared (List_1_t3342231146 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define List_1_Sort_m2255701316(__this, ___comparer0, method) ((  void (*) (List_1_t3342231146 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m2255701316_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Int3>::Sort(System.Comparison`1<T>)
extern "C"  void List_1_Sort_m2566162675_gshared (List_1_t3342231146 * __this, Comparison_1_t690406781 * ___comparison0, const MethodInfo* method);
#define List_1_Sort_m2566162675(__this, ___comparison0, method) ((  void (*) (List_1_t3342231146 *, Comparison_1_t690406781 *, const MethodInfo*))List_1_Sort_m2566162675_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<Pathfinding.Int3>::ToArray()
extern "C"  Int3U5BU5D_t516284607* List_1_ToArray_m2617869275_gshared (List_1_t3342231146 * __this, const MethodInfo* method);
#define List_1_ToArray_m2617869275(__this, method) ((  Int3U5BU5D_t516284607* (*) (List_1_t3342231146 *, const MethodInfo*))List_1_ToArray_m2617869275_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Int3>::TrimExcess()
extern "C"  void List_1_TrimExcess_m2221595705_gshared (List_1_t3342231146 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m2221595705(__this, method) ((  void (*) (List_1_t3342231146 *, const MethodInfo*))List_1_TrimExcess_m2221595705_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Pathfinding.Int3>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m3239046825_gshared (List_1_t3342231146 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m3239046825(__this, method) ((  int32_t (*) (List_1_t3342231146 *, const MethodInfo*))List_1_get_Capacity_m3239046825_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Int3>::set_Capacity(System.Int32)
extern "C"  void List_1_set_Capacity_m3932212106_gshared (List_1_t3342231146 * __this, int32_t ___value0, const MethodInfo* method);
#define List_1_set_Capacity_m3932212106(__this, ___value0, method) ((  void (*) (List_1_t3342231146 *, int32_t, const MethodInfo*))List_1_set_Capacity_m3932212106_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<Pathfinding.Int3>::get_Count()
extern "C"  int32_t List_1_get_Count_m745757707_gshared (List_1_t3342231146 * __this, const MethodInfo* method);
#define List_1_get_Count_m745757707(__this, method) ((  int32_t (*) (List_1_t3342231146 *, const MethodInfo*))List_1_get_Count_m745757707_gshared)(__this, method)
// T System.Collections.Generic.List`1<Pathfinding.Int3>::get_Item(System.Int32)
extern "C"  Int3_t1974045594  List_1_get_Item_m3111310826_gshared (List_1_t3342231146 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_get_Item_m3111310826(__this, ___index0, method) ((  Int3_t1974045594  (*) (List_1_t3342231146 *, int32_t, const MethodInfo*))List_1_get_Item_m3111310826_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Int3>::set_Item(System.Int32,T)
extern "C"  void List_1_set_Item_m1229932331_gshared (List_1_t3342231146 * __this, int32_t ___index0, Int3_t1974045594  ___value1, const MethodInfo* method);
#define List_1_set_Item_m1229932331(__this, ___index0, ___value1, method) ((  void (*) (List_1_t3342231146 *, int32_t, Int3_t1974045594 , const MethodInfo*))List_1_set_Item_m1229932331_gshared)(__this, ___index0, ___value1, method)
