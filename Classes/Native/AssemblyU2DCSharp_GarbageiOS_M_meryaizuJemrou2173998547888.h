﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_meryaizuJemrou217
struct  M_meryaizuJemrou217_t3998547888  : public Il2CppObject
{
public:
	// System.Single GarbageiOS.M_meryaizuJemrou217::_miqur
	float ____miqur_0;
	// System.Boolean GarbageiOS.M_meryaizuJemrou217::_soloxurHimeama
	bool ____soloxurHimeama_1;
	// System.String GarbageiOS.M_meryaizuJemrou217::_cheeto
	String_t* ____cheeto_2;
	// System.String GarbageiOS.M_meryaizuJemrou217::_lerjistaNidair
	String_t* ____lerjistaNidair_3;
	// System.UInt32 GarbageiOS.M_meryaizuJemrou217::_cerexasce
	uint32_t ____cerexasce_4;
	// System.UInt32 GarbageiOS.M_meryaizuJemrou217::_regallWarselhis
	uint32_t ____regallWarselhis_5;
	// System.Int32 GarbageiOS.M_meryaizuJemrou217::_depowFelcee
	int32_t ____depowFelcee_6;
	// System.UInt32 GarbageiOS.M_meryaizuJemrou217::_kocaigaYearhuwoo
	uint32_t ____kocaigaYearhuwoo_7;
	// System.Single GarbageiOS.M_meryaizuJemrou217::_tulemFemche
	float ____tulemFemche_8;
	// System.Single GarbageiOS.M_meryaizuJemrou217::_nasgo
	float ____nasgo_9;
	// System.Single GarbageiOS.M_meryaizuJemrou217::_pairdrerrowLemtouvair
	float ____pairdrerrowLemtouvair_10;
	// System.Int32 GarbageiOS.M_meryaizuJemrou217::_ceresisiNesiwa
	int32_t ____ceresisiNesiwa_11;
	// System.UInt32 GarbageiOS.M_meryaizuJemrou217::_kesibe
	uint32_t ____kesibe_12;
	// System.String GarbageiOS.M_meryaizuJemrou217::_dallceejasKissernai
	String_t* ____dallceejasKissernai_13;
	// System.String GarbageiOS.M_meryaizuJemrou217::_merhar
	String_t* ____merhar_14;
	// System.Boolean GarbageiOS.M_meryaizuJemrou217::_sejeahasHawnear
	bool ____sejeahasHawnear_15;

public:
	inline static int32_t get_offset_of__miqur_0() { return static_cast<int32_t>(offsetof(M_meryaizuJemrou217_t3998547888, ____miqur_0)); }
	inline float get__miqur_0() const { return ____miqur_0; }
	inline float* get_address_of__miqur_0() { return &____miqur_0; }
	inline void set__miqur_0(float value)
	{
		____miqur_0 = value;
	}

	inline static int32_t get_offset_of__soloxurHimeama_1() { return static_cast<int32_t>(offsetof(M_meryaizuJemrou217_t3998547888, ____soloxurHimeama_1)); }
	inline bool get__soloxurHimeama_1() const { return ____soloxurHimeama_1; }
	inline bool* get_address_of__soloxurHimeama_1() { return &____soloxurHimeama_1; }
	inline void set__soloxurHimeama_1(bool value)
	{
		____soloxurHimeama_1 = value;
	}

	inline static int32_t get_offset_of__cheeto_2() { return static_cast<int32_t>(offsetof(M_meryaizuJemrou217_t3998547888, ____cheeto_2)); }
	inline String_t* get__cheeto_2() const { return ____cheeto_2; }
	inline String_t** get_address_of__cheeto_2() { return &____cheeto_2; }
	inline void set__cheeto_2(String_t* value)
	{
		____cheeto_2 = value;
		Il2CppCodeGenWriteBarrier(&____cheeto_2, value);
	}

	inline static int32_t get_offset_of__lerjistaNidair_3() { return static_cast<int32_t>(offsetof(M_meryaizuJemrou217_t3998547888, ____lerjistaNidair_3)); }
	inline String_t* get__lerjistaNidair_3() const { return ____lerjistaNidair_3; }
	inline String_t** get_address_of__lerjistaNidair_3() { return &____lerjistaNidair_3; }
	inline void set__lerjistaNidair_3(String_t* value)
	{
		____lerjistaNidair_3 = value;
		Il2CppCodeGenWriteBarrier(&____lerjistaNidair_3, value);
	}

	inline static int32_t get_offset_of__cerexasce_4() { return static_cast<int32_t>(offsetof(M_meryaizuJemrou217_t3998547888, ____cerexasce_4)); }
	inline uint32_t get__cerexasce_4() const { return ____cerexasce_4; }
	inline uint32_t* get_address_of__cerexasce_4() { return &____cerexasce_4; }
	inline void set__cerexasce_4(uint32_t value)
	{
		____cerexasce_4 = value;
	}

	inline static int32_t get_offset_of__regallWarselhis_5() { return static_cast<int32_t>(offsetof(M_meryaizuJemrou217_t3998547888, ____regallWarselhis_5)); }
	inline uint32_t get__regallWarselhis_5() const { return ____regallWarselhis_5; }
	inline uint32_t* get_address_of__regallWarselhis_5() { return &____regallWarselhis_5; }
	inline void set__regallWarselhis_5(uint32_t value)
	{
		____regallWarselhis_5 = value;
	}

	inline static int32_t get_offset_of__depowFelcee_6() { return static_cast<int32_t>(offsetof(M_meryaizuJemrou217_t3998547888, ____depowFelcee_6)); }
	inline int32_t get__depowFelcee_6() const { return ____depowFelcee_6; }
	inline int32_t* get_address_of__depowFelcee_6() { return &____depowFelcee_6; }
	inline void set__depowFelcee_6(int32_t value)
	{
		____depowFelcee_6 = value;
	}

	inline static int32_t get_offset_of__kocaigaYearhuwoo_7() { return static_cast<int32_t>(offsetof(M_meryaizuJemrou217_t3998547888, ____kocaigaYearhuwoo_7)); }
	inline uint32_t get__kocaigaYearhuwoo_7() const { return ____kocaigaYearhuwoo_7; }
	inline uint32_t* get_address_of__kocaigaYearhuwoo_7() { return &____kocaigaYearhuwoo_7; }
	inline void set__kocaigaYearhuwoo_7(uint32_t value)
	{
		____kocaigaYearhuwoo_7 = value;
	}

	inline static int32_t get_offset_of__tulemFemche_8() { return static_cast<int32_t>(offsetof(M_meryaizuJemrou217_t3998547888, ____tulemFemche_8)); }
	inline float get__tulemFemche_8() const { return ____tulemFemche_8; }
	inline float* get_address_of__tulemFemche_8() { return &____tulemFemche_8; }
	inline void set__tulemFemche_8(float value)
	{
		____tulemFemche_8 = value;
	}

	inline static int32_t get_offset_of__nasgo_9() { return static_cast<int32_t>(offsetof(M_meryaizuJemrou217_t3998547888, ____nasgo_9)); }
	inline float get__nasgo_9() const { return ____nasgo_9; }
	inline float* get_address_of__nasgo_9() { return &____nasgo_9; }
	inline void set__nasgo_9(float value)
	{
		____nasgo_9 = value;
	}

	inline static int32_t get_offset_of__pairdrerrowLemtouvair_10() { return static_cast<int32_t>(offsetof(M_meryaizuJemrou217_t3998547888, ____pairdrerrowLemtouvair_10)); }
	inline float get__pairdrerrowLemtouvair_10() const { return ____pairdrerrowLemtouvair_10; }
	inline float* get_address_of__pairdrerrowLemtouvair_10() { return &____pairdrerrowLemtouvair_10; }
	inline void set__pairdrerrowLemtouvair_10(float value)
	{
		____pairdrerrowLemtouvair_10 = value;
	}

	inline static int32_t get_offset_of__ceresisiNesiwa_11() { return static_cast<int32_t>(offsetof(M_meryaizuJemrou217_t3998547888, ____ceresisiNesiwa_11)); }
	inline int32_t get__ceresisiNesiwa_11() const { return ____ceresisiNesiwa_11; }
	inline int32_t* get_address_of__ceresisiNesiwa_11() { return &____ceresisiNesiwa_11; }
	inline void set__ceresisiNesiwa_11(int32_t value)
	{
		____ceresisiNesiwa_11 = value;
	}

	inline static int32_t get_offset_of__kesibe_12() { return static_cast<int32_t>(offsetof(M_meryaizuJemrou217_t3998547888, ____kesibe_12)); }
	inline uint32_t get__kesibe_12() const { return ____kesibe_12; }
	inline uint32_t* get_address_of__kesibe_12() { return &____kesibe_12; }
	inline void set__kesibe_12(uint32_t value)
	{
		____kesibe_12 = value;
	}

	inline static int32_t get_offset_of__dallceejasKissernai_13() { return static_cast<int32_t>(offsetof(M_meryaizuJemrou217_t3998547888, ____dallceejasKissernai_13)); }
	inline String_t* get__dallceejasKissernai_13() const { return ____dallceejasKissernai_13; }
	inline String_t** get_address_of__dallceejasKissernai_13() { return &____dallceejasKissernai_13; }
	inline void set__dallceejasKissernai_13(String_t* value)
	{
		____dallceejasKissernai_13 = value;
		Il2CppCodeGenWriteBarrier(&____dallceejasKissernai_13, value);
	}

	inline static int32_t get_offset_of__merhar_14() { return static_cast<int32_t>(offsetof(M_meryaizuJemrou217_t3998547888, ____merhar_14)); }
	inline String_t* get__merhar_14() const { return ____merhar_14; }
	inline String_t** get_address_of__merhar_14() { return &____merhar_14; }
	inline void set__merhar_14(String_t* value)
	{
		____merhar_14 = value;
		Il2CppCodeGenWriteBarrier(&____merhar_14, value);
	}

	inline static int32_t get_offset_of__sejeahasHawnear_15() { return static_cast<int32_t>(offsetof(M_meryaizuJemrou217_t3998547888, ____sejeahasHawnear_15)); }
	inline bool get__sejeahasHawnear_15() const { return ____sejeahasHawnear_15; }
	inline bool* get_address_of__sejeahasHawnear_15() { return &____sejeahasHawnear_15; }
	inline void set__sejeahasHawnear_15(bool value)
	{
		____sejeahasHawnear_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
