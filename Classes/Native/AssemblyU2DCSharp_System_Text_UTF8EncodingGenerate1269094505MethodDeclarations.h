﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System_Text_UTF8EncodingGenerated
struct System_Text_UTF8EncodingGenerated_t1269094505;
// JSVCall
struct JSVCall_t3708497963;
// System.Char[]
struct CharU5BU5D_t3324145743;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"

// System.Void System_Text_UTF8EncodingGenerated::.ctor()
extern "C"  void System_Text_UTF8EncodingGenerated__ctor_m1500146466 (System_Text_UTF8EncodingGenerated_t1269094505 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_UTF8EncodingGenerated::UTF8Encoding_UTF8Encoding1(JSVCall,System.Int32)
extern "C"  bool System_Text_UTF8EncodingGenerated_UTF8Encoding_UTF8Encoding1_m61010832 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_UTF8EncodingGenerated::UTF8Encoding_UTF8Encoding2(JSVCall,System.Int32)
extern "C"  bool System_Text_UTF8EncodingGenerated_UTF8Encoding_UTF8Encoding2_m1305775313 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_UTF8EncodingGenerated::UTF8Encoding_UTF8Encoding3(JSVCall,System.Int32)
extern "C"  bool System_Text_UTF8EncodingGenerated_UTF8Encoding_UTF8Encoding3_m2550539794 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_UTF8EncodingGenerated::UTF8Encoding_Equals__Object(JSVCall,System.Int32)
extern "C"  bool System_Text_UTF8EncodingGenerated_UTF8Encoding_Equals__Object_m148696925 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_UTF8EncodingGenerated::UTF8Encoding_GetByteCount__Char_Array__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool System_Text_UTF8EncodingGenerated_UTF8Encoding_GetByteCount__Char_Array__Int32__Int32_m1394294880 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_UTF8EncodingGenerated::UTF8Encoding_GetByteCount__String(JSVCall,System.Int32)
extern "C"  bool System_Text_UTF8EncodingGenerated_UTF8Encoding_GetByteCount__String_m3286373153 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_UTF8EncodingGenerated::UTF8Encoding_GetBytes__Char_Array__Int32__Int32__Byte_Array__Int32(JSVCall,System.Int32)
extern "C"  bool System_Text_UTF8EncodingGenerated_UTF8Encoding_GetBytes__Char_Array__Int32__Int32__Byte_Array__Int32_m3018060394 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_UTF8EncodingGenerated::UTF8Encoding_GetBytes__String__Int32__Int32__Byte_Array__Int32(JSVCall,System.Int32)
extern "C"  bool System_Text_UTF8EncodingGenerated_UTF8Encoding_GetBytes__String__Int32__Int32__Byte_Array__Int32_m1778244553 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_UTF8EncodingGenerated::UTF8Encoding_GetCharCount__Byte_Array__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool System_Text_UTF8EncodingGenerated_UTF8Encoding_GetCharCount__Byte_Array__Int32__Int32_m77951492 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_UTF8EncodingGenerated::UTF8Encoding_GetChars__Byte_Array__Int32__Int32__Char_Array__Int32(JSVCall,System.Int32)
extern "C"  bool System_Text_UTF8EncodingGenerated_UTF8Encoding_GetChars__Byte_Array__Int32__Int32__Char_Array__Int32_m2419558456 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_UTF8EncodingGenerated::UTF8Encoding_GetDecoder(JSVCall,System.Int32)
extern "C"  bool System_Text_UTF8EncodingGenerated_UTF8Encoding_GetDecoder_m728858221 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_UTF8EncodingGenerated::UTF8Encoding_GetEncoder(JSVCall,System.Int32)
extern "C"  bool System_Text_UTF8EncodingGenerated_UTF8Encoding_GetEncoder_m3424298309 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_UTF8EncodingGenerated::UTF8Encoding_GetHashCode(JSVCall,System.Int32)
extern "C"  bool System_Text_UTF8EncodingGenerated_UTF8Encoding_GetHashCode_m1454468564 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_UTF8EncodingGenerated::UTF8Encoding_GetMaxByteCount__Int32(JSVCall,System.Int32)
extern "C"  bool System_Text_UTF8EncodingGenerated_UTF8Encoding_GetMaxByteCount__Int32_m1439492148 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_UTF8EncodingGenerated::UTF8Encoding_GetMaxCharCount__Int32(JSVCall,System.Int32)
extern "C"  bool System_Text_UTF8EncodingGenerated_UTF8Encoding_GetMaxCharCount__Int32_m21694210 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_UTF8EncodingGenerated::UTF8Encoding_GetPreamble(JSVCall,System.Int32)
extern "C"  bool System_Text_UTF8EncodingGenerated_UTF8Encoding_GetPreamble_m3573500901 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_UTF8EncodingGenerated::UTF8Encoding_GetString__Byte_Array__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool System_Text_UTF8EncodingGenerated_UTF8Encoding_GetString__Byte_Array__Int32__Int32_m3832472972 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Text_UTF8EncodingGenerated::__Register()
extern "C"  void System_Text_UTF8EncodingGenerated___Register_m788969189 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char[] System_Text_UTF8EncodingGenerated::<UTF8Encoding_GetByteCount__Char_Array__Int32__Int32>m__EC()
extern "C"  CharU5BU5D_t3324145743* System_Text_UTF8EncodingGenerated_U3CUTF8Encoding_GetByteCount__Char_Array__Int32__Int32U3Em__EC_m1071500691 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char[] System_Text_UTF8EncodingGenerated::<UTF8Encoding_GetBytes__Char_Array__Int32__Int32__Byte_Array__Int32>m__ED()
extern "C"  CharU5BU5D_t3324145743* System_Text_UTF8EncodingGenerated_U3CUTF8Encoding_GetBytes__Char_Array__Int32__Int32__Byte_Array__Int32U3Em__ED_m1187449714 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System_Text_UTF8EncodingGenerated::<UTF8Encoding_GetBytes__Char_Array__Int32__Int32__Byte_Array__Int32>m__EE()
extern "C"  ByteU5BU5D_t4260760469* System_Text_UTF8EncodingGenerated_U3CUTF8Encoding_GetBytes__Char_Array__Int32__Int32__Byte_Array__Int32U3Em__EE_m238613441 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System_Text_UTF8EncodingGenerated::<UTF8Encoding_GetBytes__String__Int32__Int32__Byte_Array__Int32>m__EF()
extern "C"  ByteU5BU5D_t4260760469* System_Text_UTF8EncodingGenerated_U3CUTF8Encoding_GetBytes__String__Int32__Int32__Byte_Array__Int32U3Em__EF_m2101747489 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System_Text_UTF8EncodingGenerated::<UTF8Encoding_GetCharCount__Byte_Array__Int32__Int32>m__F0()
extern "C"  ByteU5BU5D_t4260760469* System_Text_UTF8EncodingGenerated_U3CUTF8Encoding_GetCharCount__Byte_Array__Int32__Int32U3Em__F0_m3077532789 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System_Text_UTF8EncodingGenerated::<UTF8Encoding_GetChars__Byte_Array__Int32__Int32__Char_Array__Int32>m__F1()
extern "C"  ByteU5BU5D_t4260760469* System_Text_UTF8EncodingGenerated_U3CUTF8Encoding_GetChars__Byte_Array__Int32__Int32__Char_Array__Int32U3Em__F1_m174642010 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char[] System_Text_UTF8EncodingGenerated::<UTF8Encoding_GetChars__Byte_Array__Int32__Int32__Char_Array__Int32>m__F2()
extern "C"  CharU5BU5D_t3324145743* System_Text_UTF8EncodingGenerated_U3CUTF8Encoding_GetChars__Byte_Array__Int32__Int32__Char_Array__Int32U3Em__F2_m1123480205 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System_Text_UTF8EncodingGenerated::<UTF8Encoding_GetString__Byte_Array__Int32__Int32>m__F3()
extern "C"  ByteU5BU5D_t4260760469* System_Text_UTF8EncodingGenerated_U3CUTF8Encoding_GetString__Byte_Array__Int32__Int32U3Em__F3_m2363164976 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System_Text_UTF8EncodingGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t System_Text_UTF8EncodingGenerated_ilo_getObject1_m3052763092 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_UTF8EncodingGenerated::ilo_attachFinalizerObject2(System.Int32)
extern "C"  bool System_Text_UTF8EncodingGenerated_ilo_attachFinalizerObject2_m1399420502 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_UTF8EncodingGenerated::ilo_getBooleanS3(System.Int32)
extern "C"  bool System_Text_UTF8EncodingGenerated_ilo_getBooleanS3_m3050048772 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Text_UTF8EncodingGenerated::ilo_addJSCSRel4(System.Int32,System.Object)
extern "C"  void System_Text_UTF8EncodingGenerated_ilo_addJSCSRel4_m4154082913 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System_Text_UTF8EncodingGenerated::ilo_getWhatever5(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * System_Text_UTF8EncodingGenerated_ilo_getWhatever5_m3957805892 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Text_UTF8EncodingGenerated::ilo_setInt326(System.Int32,System.Int32)
extern "C"  void System_Text_UTF8EncodingGenerated_ilo_setInt326_m1355528655 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System_Text_UTF8EncodingGenerated::ilo_getInt327(System.Int32)
extern "C"  int32_t System_Text_UTF8EncodingGenerated_ilo_getInt327_m1786300619 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System_Text_UTF8EncodingGenerated::ilo_setObject8(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t System_Text_UTF8EncodingGenerated_ilo_setObject8_m2187710227 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Text_UTF8EncodingGenerated::ilo_moveSaveID2Arr9(System.Int32)
extern "C"  void System_Text_UTF8EncodingGenerated_ilo_moveSaveID2Arr9_m2545788095 (Il2CppObject * __this /* static, unused */, int32_t ___arrIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Text_UTF8EncodingGenerated::ilo_setArrayS10(System.Int32,System.Int32,System.Boolean)
extern "C"  void System_Text_UTF8EncodingGenerated_ilo_setArrayS10_m3002613343 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___count1, bool ___bClear2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System_Text_UTF8EncodingGenerated::ilo_getArrayLength11(System.Int32)
extern "C"  int32_t System_Text_UTF8EncodingGenerated_ilo_getArrayLength11_m3366840641 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System_Text_UTF8EncodingGenerated::ilo_getElement12(System.Int32,System.Int32)
extern "C"  int32_t System_Text_UTF8EncodingGenerated_ilo_getElement12_m3435995480 (Il2CppObject * __this /* static, unused */, int32_t ___id0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte System_Text_UTF8EncodingGenerated::ilo_getByte13(System.Int32)
extern "C"  uint8_t System_Text_UTF8EncodingGenerated_ilo_getByte13_m3854035274 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
