﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_Mihua_Asset_ABLoadOperation_ABOp1894014760.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mihua.Asset.ABLoadOperation.AssetOperation
struct  AssetOperation_t778728221  : public ABOperation_t1894014760
{
public:
	// System.Boolean Mihua.Asset.ABLoadOperation.AssetOperation::isAsync
	bool ___isAsync_0;
	// System.Boolean Mihua.Asset.ABLoadOperation.AssetOperation::isInit
	bool ___isInit_1;

public:
	inline static int32_t get_offset_of_isAsync_0() { return static_cast<int32_t>(offsetof(AssetOperation_t778728221, ___isAsync_0)); }
	inline bool get_isAsync_0() const { return ___isAsync_0; }
	inline bool* get_address_of_isAsync_0() { return &___isAsync_0; }
	inline void set_isAsync_0(bool value)
	{
		___isAsync_0 = value;
	}

	inline static int32_t get_offset_of_isInit_1() { return static_cast<int32_t>(offsetof(AssetOperation_t778728221, ___isInit_1)); }
	inline bool get_isInit_1() const { return ___isInit_1; }
	inline bool* get_address_of_isInit_1() { return &___isInit_1; }
	inline void set_isInit_1(bool value)
	{
		___isInit_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
