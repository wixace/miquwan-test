﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>
struct Dictionary_2_t4167966349;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Ke487935107.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.UInt32,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m3366644813_gshared (Enumerator_t487935107 * __this, Dictionary_2_t4167966349 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m3366644813(__this, ___host0, method) ((  void (*) (Enumerator_t487935107 *, Dictionary_2_t4167966349 *, const MethodInfo*))Enumerator__ctor_m3366644813_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.UInt32,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m552619700_gshared (Enumerator_t487935107 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m552619700(__this, method) ((  Il2CppObject * (*) (Enumerator_t487935107 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m552619700_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.UInt32,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1360850504_gshared (Enumerator_t487935107 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1360850504(__this, method) ((  void (*) (Enumerator_t487935107 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1360850504_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.UInt32,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m766480431_gshared (Enumerator_t487935107 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m766480431(__this, method) ((  void (*) (Enumerator_t487935107 *, const MethodInfo*))Enumerator_Dispose_m766480431_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.UInt32,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3834219188_gshared (Enumerator_t487935107 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m3834219188(__this, method) ((  bool (*) (Enumerator_t487935107 *, const MethodInfo*))Enumerator_MoveNext_m3834219188_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.UInt32,System.Object>::get_Current()
extern "C"  uint32_t Enumerator_get_Current_m3810570272_gshared (Enumerator_t487935107 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m3810570272(__this, method) ((  uint32_t (*) (Enumerator_t487935107 *, const MethodInfo*))Enumerator_get_Current_m3810570272_gshared)(__this, method)
