﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// CSCampFightMonsterData[]
struct CSCampFightMonsterDataU5BU5D_t1399553576;
// CSCampFightAddAttributeData[]
struct CSCampFightAddAttributeDataU5BU5D_t2430053645;
// CSPlusAtt
struct CSPlusAtt_t3268315159;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CSCampfightData
struct  CSCampfightData_t179152873  : public Il2CppObject
{
public:
	// System.Boolean CSCampfightData::isRealMan
	bool ___isRealMan_0;
	// CSCampFightMonsterData[] CSCampfightData::monsterList
	CSCampFightMonsterDataU5BU5D_t1399553576* ___monsterList_1;
	// CSCampFightAddAttributeData[] CSCampfightData::attributeList
	CSCampFightAddAttributeDataU5BU5D_t2430053645* ___attributeList_2;
	// CSCampFightAddAttributeData[] CSCampfightData::enemyAttributeList
	CSCampFightAddAttributeDataU5BU5D_t2430053645* ___enemyAttributeList_3;
	// System.Int32 CSCampfightData::country
	int32_t ___country_4;
	// CSPlusAtt CSCampfightData::plusAtt
	CSPlusAtt_t3268315159 * ___plusAtt_5;
	// System.Int32 CSCampfightData::enemyCountry
	int32_t ___enemyCountry_6;
	// CSPlusAtt CSCampfightData::enemyPlusAtt
	CSPlusAtt_t3268315159 * ___enemyPlusAtt_7;

public:
	inline static int32_t get_offset_of_isRealMan_0() { return static_cast<int32_t>(offsetof(CSCampfightData_t179152873, ___isRealMan_0)); }
	inline bool get_isRealMan_0() const { return ___isRealMan_0; }
	inline bool* get_address_of_isRealMan_0() { return &___isRealMan_0; }
	inline void set_isRealMan_0(bool value)
	{
		___isRealMan_0 = value;
	}

	inline static int32_t get_offset_of_monsterList_1() { return static_cast<int32_t>(offsetof(CSCampfightData_t179152873, ___monsterList_1)); }
	inline CSCampFightMonsterDataU5BU5D_t1399553576* get_monsterList_1() const { return ___monsterList_1; }
	inline CSCampFightMonsterDataU5BU5D_t1399553576** get_address_of_monsterList_1() { return &___monsterList_1; }
	inline void set_monsterList_1(CSCampFightMonsterDataU5BU5D_t1399553576* value)
	{
		___monsterList_1 = value;
		Il2CppCodeGenWriteBarrier(&___monsterList_1, value);
	}

	inline static int32_t get_offset_of_attributeList_2() { return static_cast<int32_t>(offsetof(CSCampfightData_t179152873, ___attributeList_2)); }
	inline CSCampFightAddAttributeDataU5BU5D_t2430053645* get_attributeList_2() const { return ___attributeList_2; }
	inline CSCampFightAddAttributeDataU5BU5D_t2430053645** get_address_of_attributeList_2() { return &___attributeList_2; }
	inline void set_attributeList_2(CSCampFightAddAttributeDataU5BU5D_t2430053645* value)
	{
		___attributeList_2 = value;
		Il2CppCodeGenWriteBarrier(&___attributeList_2, value);
	}

	inline static int32_t get_offset_of_enemyAttributeList_3() { return static_cast<int32_t>(offsetof(CSCampfightData_t179152873, ___enemyAttributeList_3)); }
	inline CSCampFightAddAttributeDataU5BU5D_t2430053645* get_enemyAttributeList_3() const { return ___enemyAttributeList_3; }
	inline CSCampFightAddAttributeDataU5BU5D_t2430053645** get_address_of_enemyAttributeList_3() { return &___enemyAttributeList_3; }
	inline void set_enemyAttributeList_3(CSCampFightAddAttributeDataU5BU5D_t2430053645* value)
	{
		___enemyAttributeList_3 = value;
		Il2CppCodeGenWriteBarrier(&___enemyAttributeList_3, value);
	}

	inline static int32_t get_offset_of_country_4() { return static_cast<int32_t>(offsetof(CSCampfightData_t179152873, ___country_4)); }
	inline int32_t get_country_4() const { return ___country_4; }
	inline int32_t* get_address_of_country_4() { return &___country_4; }
	inline void set_country_4(int32_t value)
	{
		___country_4 = value;
	}

	inline static int32_t get_offset_of_plusAtt_5() { return static_cast<int32_t>(offsetof(CSCampfightData_t179152873, ___plusAtt_5)); }
	inline CSPlusAtt_t3268315159 * get_plusAtt_5() const { return ___plusAtt_5; }
	inline CSPlusAtt_t3268315159 ** get_address_of_plusAtt_5() { return &___plusAtt_5; }
	inline void set_plusAtt_5(CSPlusAtt_t3268315159 * value)
	{
		___plusAtt_5 = value;
		Il2CppCodeGenWriteBarrier(&___plusAtt_5, value);
	}

	inline static int32_t get_offset_of_enemyCountry_6() { return static_cast<int32_t>(offsetof(CSCampfightData_t179152873, ___enemyCountry_6)); }
	inline int32_t get_enemyCountry_6() const { return ___enemyCountry_6; }
	inline int32_t* get_address_of_enemyCountry_6() { return &___enemyCountry_6; }
	inline void set_enemyCountry_6(int32_t value)
	{
		___enemyCountry_6 = value;
	}

	inline static int32_t get_offset_of_enemyPlusAtt_7() { return static_cast<int32_t>(offsetof(CSCampfightData_t179152873, ___enemyPlusAtt_7)); }
	inline CSPlusAtt_t3268315159 * get_enemyPlusAtt_7() const { return ___enemyPlusAtt_7; }
	inline CSPlusAtt_t3268315159 ** get_address_of_enemyPlusAtt_7() { return &___enemyPlusAtt_7; }
	inline void set_enemyPlusAtt_7(CSPlusAtt_t3268315159 * value)
	{
		___enemyPlusAtt_7 = value;
		Il2CppCodeGenWriteBarrier(&___enemyPlusAtt_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
