﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_rearneremeNairdermi234
struct  M_rearneremeNairdermi234_t620816888  : public Il2CppObject
{
public:
	// System.Boolean GarbageiOS.M_rearneremeNairdermi234::_boudis
	bool ____boudis_0;
	// System.Single GarbageiOS.M_rearneremeNairdermi234::_duvis
	float ____duvis_1;
	// System.Single GarbageiOS.M_rearneremeNairdermi234::_jeejoono
	float ____jeejoono_2;
	// System.Boolean GarbageiOS.M_rearneremeNairdermi234::_deralltoGairsire
	bool ____deralltoGairsire_3;
	// System.Int32 GarbageiOS.M_rearneremeNairdermi234::_jono
	int32_t ____jono_4;
	// System.Int32 GarbageiOS.M_rearneremeNairdermi234::_kaziCaysou
	int32_t ____kaziCaysou_5;
	// System.Int32 GarbageiOS.M_rearneremeNairdermi234::_wearyo
	int32_t ____wearyo_6;
	// System.UInt32 GarbageiOS.M_rearneremeNairdermi234::_lersearFako
	uint32_t ____lersearFako_7;
	// System.Int32 GarbageiOS.M_rearneremeNairdermi234::_nalqelRehole
	int32_t ____nalqelRehole_8;
	// System.Single GarbageiOS.M_rearneremeNairdermi234::_stestanaw
	float ____stestanaw_9;
	// System.Single GarbageiOS.M_rearneremeNairdermi234::_peetomair
	float ____peetomair_10;
	// System.Int32 GarbageiOS.M_rearneremeNairdermi234::_dismiba
	int32_t ____dismiba_11;
	// System.Int32 GarbageiOS.M_rearneremeNairdermi234::_nitrelLicerjea
	int32_t ____nitrelLicerjea_12;
	// System.String GarbageiOS.M_rearneremeNairdermi234::_troolorJune
	String_t* ____troolorJune_13;
	// System.Single GarbageiOS.M_rearneremeNairdermi234::_sereseartrirFiru
	float ____sereseartrirFiru_14;
	// System.Single GarbageiOS.M_rearneremeNairdermi234::_qemtitrarDrava
	float ____qemtitrarDrava_15;
	// System.UInt32 GarbageiOS.M_rearneremeNairdermi234::_gairtel
	uint32_t ____gairtel_16;
	// System.String GarbageiOS.M_rearneremeNairdermi234::_piswai
	String_t* ____piswai_17;
	// System.Single GarbageiOS.M_rearneremeNairdermi234::_laihe
	float ____laihe_18;
	// System.Single GarbageiOS.M_rearneremeNairdermi234::_trascaster
	float ____trascaster_19;
	// System.Boolean GarbageiOS.M_rearneremeNairdermi234::_ziswuhe
	bool ____ziswuhe_20;
	// System.Int32 GarbageiOS.M_rearneremeNairdermi234::_xecirnisSurjeacem
	int32_t ____xecirnisSurjeacem_21;
	// System.Single GarbageiOS.M_rearneremeNairdermi234::_saichora
	float ____saichora_22;
	// System.Boolean GarbageiOS.M_rearneremeNairdermi234::_seldeeto
	bool ____seldeeto_23;
	// System.String GarbageiOS.M_rearneremeNairdermi234::_keatoostiTrisbitaw
	String_t* ____keatoostiTrisbitaw_24;
	// System.String GarbageiOS.M_rearneremeNairdermi234::_drako
	String_t* ____drako_25;
	// System.String GarbageiOS.M_rearneremeNairdermi234::_drealaQeahel
	String_t* ____drealaQeahel_26;
	// System.Boolean GarbageiOS.M_rearneremeNairdermi234::_didurmi
	bool ____didurmi_27;
	// System.Boolean GarbageiOS.M_rearneremeNairdermi234::_lirtirDawdair
	bool ____lirtirDawdair_28;
	// System.Int32 GarbageiOS.M_rearneremeNairdermi234::_wheecir
	int32_t ____wheecir_29;
	// System.Int32 GarbageiOS.M_rearneremeNairdermi234::_ranallkisNotisai
	int32_t ____ranallkisNotisai_30;

public:
	inline static int32_t get_offset_of__boudis_0() { return static_cast<int32_t>(offsetof(M_rearneremeNairdermi234_t620816888, ____boudis_0)); }
	inline bool get__boudis_0() const { return ____boudis_0; }
	inline bool* get_address_of__boudis_0() { return &____boudis_0; }
	inline void set__boudis_0(bool value)
	{
		____boudis_0 = value;
	}

	inline static int32_t get_offset_of__duvis_1() { return static_cast<int32_t>(offsetof(M_rearneremeNairdermi234_t620816888, ____duvis_1)); }
	inline float get__duvis_1() const { return ____duvis_1; }
	inline float* get_address_of__duvis_1() { return &____duvis_1; }
	inline void set__duvis_1(float value)
	{
		____duvis_1 = value;
	}

	inline static int32_t get_offset_of__jeejoono_2() { return static_cast<int32_t>(offsetof(M_rearneremeNairdermi234_t620816888, ____jeejoono_2)); }
	inline float get__jeejoono_2() const { return ____jeejoono_2; }
	inline float* get_address_of__jeejoono_2() { return &____jeejoono_2; }
	inline void set__jeejoono_2(float value)
	{
		____jeejoono_2 = value;
	}

	inline static int32_t get_offset_of__deralltoGairsire_3() { return static_cast<int32_t>(offsetof(M_rearneremeNairdermi234_t620816888, ____deralltoGairsire_3)); }
	inline bool get__deralltoGairsire_3() const { return ____deralltoGairsire_3; }
	inline bool* get_address_of__deralltoGairsire_3() { return &____deralltoGairsire_3; }
	inline void set__deralltoGairsire_3(bool value)
	{
		____deralltoGairsire_3 = value;
	}

	inline static int32_t get_offset_of__jono_4() { return static_cast<int32_t>(offsetof(M_rearneremeNairdermi234_t620816888, ____jono_4)); }
	inline int32_t get__jono_4() const { return ____jono_4; }
	inline int32_t* get_address_of__jono_4() { return &____jono_4; }
	inline void set__jono_4(int32_t value)
	{
		____jono_4 = value;
	}

	inline static int32_t get_offset_of__kaziCaysou_5() { return static_cast<int32_t>(offsetof(M_rearneremeNairdermi234_t620816888, ____kaziCaysou_5)); }
	inline int32_t get__kaziCaysou_5() const { return ____kaziCaysou_5; }
	inline int32_t* get_address_of__kaziCaysou_5() { return &____kaziCaysou_5; }
	inline void set__kaziCaysou_5(int32_t value)
	{
		____kaziCaysou_5 = value;
	}

	inline static int32_t get_offset_of__wearyo_6() { return static_cast<int32_t>(offsetof(M_rearneremeNairdermi234_t620816888, ____wearyo_6)); }
	inline int32_t get__wearyo_6() const { return ____wearyo_6; }
	inline int32_t* get_address_of__wearyo_6() { return &____wearyo_6; }
	inline void set__wearyo_6(int32_t value)
	{
		____wearyo_6 = value;
	}

	inline static int32_t get_offset_of__lersearFako_7() { return static_cast<int32_t>(offsetof(M_rearneremeNairdermi234_t620816888, ____lersearFako_7)); }
	inline uint32_t get__lersearFako_7() const { return ____lersearFako_7; }
	inline uint32_t* get_address_of__lersearFako_7() { return &____lersearFako_7; }
	inline void set__lersearFako_7(uint32_t value)
	{
		____lersearFako_7 = value;
	}

	inline static int32_t get_offset_of__nalqelRehole_8() { return static_cast<int32_t>(offsetof(M_rearneremeNairdermi234_t620816888, ____nalqelRehole_8)); }
	inline int32_t get__nalqelRehole_8() const { return ____nalqelRehole_8; }
	inline int32_t* get_address_of__nalqelRehole_8() { return &____nalqelRehole_8; }
	inline void set__nalqelRehole_8(int32_t value)
	{
		____nalqelRehole_8 = value;
	}

	inline static int32_t get_offset_of__stestanaw_9() { return static_cast<int32_t>(offsetof(M_rearneremeNairdermi234_t620816888, ____stestanaw_9)); }
	inline float get__stestanaw_9() const { return ____stestanaw_9; }
	inline float* get_address_of__stestanaw_9() { return &____stestanaw_9; }
	inline void set__stestanaw_9(float value)
	{
		____stestanaw_9 = value;
	}

	inline static int32_t get_offset_of__peetomair_10() { return static_cast<int32_t>(offsetof(M_rearneremeNairdermi234_t620816888, ____peetomair_10)); }
	inline float get__peetomair_10() const { return ____peetomair_10; }
	inline float* get_address_of__peetomair_10() { return &____peetomair_10; }
	inline void set__peetomair_10(float value)
	{
		____peetomair_10 = value;
	}

	inline static int32_t get_offset_of__dismiba_11() { return static_cast<int32_t>(offsetof(M_rearneremeNairdermi234_t620816888, ____dismiba_11)); }
	inline int32_t get__dismiba_11() const { return ____dismiba_11; }
	inline int32_t* get_address_of__dismiba_11() { return &____dismiba_11; }
	inline void set__dismiba_11(int32_t value)
	{
		____dismiba_11 = value;
	}

	inline static int32_t get_offset_of__nitrelLicerjea_12() { return static_cast<int32_t>(offsetof(M_rearneremeNairdermi234_t620816888, ____nitrelLicerjea_12)); }
	inline int32_t get__nitrelLicerjea_12() const { return ____nitrelLicerjea_12; }
	inline int32_t* get_address_of__nitrelLicerjea_12() { return &____nitrelLicerjea_12; }
	inline void set__nitrelLicerjea_12(int32_t value)
	{
		____nitrelLicerjea_12 = value;
	}

	inline static int32_t get_offset_of__troolorJune_13() { return static_cast<int32_t>(offsetof(M_rearneremeNairdermi234_t620816888, ____troolorJune_13)); }
	inline String_t* get__troolorJune_13() const { return ____troolorJune_13; }
	inline String_t** get_address_of__troolorJune_13() { return &____troolorJune_13; }
	inline void set__troolorJune_13(String_t* value)
	{
		____troolorJune_13 = value;
		Il2CppCodeGenWriteBarrier(&____troolorJune_13, value);
	}

	inline static int32_t get_offset_of__sereseartrirFiru_14() { return static_cast<int32_t>(offsetof(M_rearneremeNairdermi234_t620816888, ____sereseartrirFiru_14)); }
	inline float get__sereseartrirFiru_14() const { return ____sereseartrirFiru_14; }
	inline float* get_address_of__sereseartrirFiru_14() { return &____sereseartrirFiru_14; }
	inline void set__sereseartrirFiru_14(float value)
	{
		____sereseartrirFiru_14 = value;
	}

	inline static int32_t get_offset_of__qemtitrarDrava_15() { return static_cast<int32_t>(offsetof(M_rearneremeNairdermi234_t620816888, ____qemtitrarDrava_15)); }
	inline float get__qemtitrarDrava_15() const { return ____qemtitrarDrava_15; }
	inline float* get_address_of__qemtitrarDrava_15() { return &____qemtitrarDrava_15; }
	inline void set__qemtitrarDrava_15(float value)
	{
		____qemtitrarDrava_15 = value;
	}

	inline static int32_t get_offset_of__gairtel_16() { return static_cast<int32_t>(offsetof(M_rearneremeNairdermi234_t620816888, ____gairtel_16)); }
	inline uint32_t get__gairtel_16() const { return ____gairtel_16; }
	inline uint32_t* get_address_of__gairtel_16() { return &____gairtel_16; }
	inline void set__gairtel_16(uint32_t value)
	{
		____gairtel_16 = value;
	}

	inline static int32_t get_offset_of__piswai_17() { return static_cast<int32_t>(offsetof(M_rearneremeNairdermi234_t620816888, ____piswai_17)); }
	inline String_t* get__piswai_17() const { return ____piswai_17; }
	inline String_t** get_address_of__piswai_17() { return &____piswai_17; }
	inline void set__piswai_17(String_t* value)
	{
		____piswai_17 = value;
		Il2CppCodeGenWriteBarrier(&____piswai_17, value);
	}

	inline static int32_t get_offset_of__laihe_18() { return static_cast<int32_t>(offsetof(M_rearneremeNairdermi234_t620816888, ____laihe_18)); }
	inline float get__laihe_18() const { return ____laihe_18; }
	inline float* get_address_of__laihe_18() { return &____laihe_18; }
	inline void set__laihe_18(float value)
	{
		____laihe_18 = value;
	}

	inline static int32_t get_offset_of__trascaster_19() { return static_cast<int32_t>(offsetof(M_rearneremeNairdermi234_t620816888, ____trascaster_19)); }
	inline float get__trascaster_19() const { return ____trascaster_19; }
	inline float* get_address_of__trascaster_19() { return &____trascaster_19; }
	inline void set__trascaster_19(float value)
	{
		____trascaster_19 = value;
	}

	inline static int32_t get_offset_of__ziswuhe_20() { return static_cast<int32_t>(offsetof(M_rearneremeNairdermi234_t620816888, ____ziswuhe_20)); }
	inline bool get__ziswuhe_20() const { return ____ziswuhe_20; }
	inline bool* get_address_of__ziswuhe_20() { return &____ziswuhe_20; }
	inline void set__ziswuhe_20(bool value)
	{
		____ziswuhe_20 = value;
	}

	inline static int32_t get_offset_of__xecirnisSurjeacem_21() { return static_cast<int32_t>(offsetof(M_rearneremeNairdermi234_t620816888, ____xecirnisSurjeacem_21)); }
	inline int32_t get__xecirnisSurjeacem_21() const { return ____xecirnisSurjeacem_21; }
	inline int32_t* get_address_of__xecirnisSurjeacem_21() { return &____xecirnisSurjeacem_21; }
	inline void set__xecirnisSurjeacem_21(int32_t value)
	{
		____xecirnisSurjeacem_21 = value;
	}

	inline static int32_t get_offset_of__saichora_22() { return static_cast<int32_t>(offsetof(M_rearneremeNairdermi234_t620816888, ____saichora_22)); }
	inline float get__saichora_22() const { return ____saichora_22; }
	inline float* get_address_of__saichora_22() { return &____saichora_22; }
	inline void set__saichora_22(float value)
	{
		____saichora_22 = value;
	}

	inline static int32_t get_offset_of__seldeeto_23() { return static_cast<int32_t>(offsetof(M_rearneremeNairdermi234_t620816888, ____seldeeto_23)); }
	inline bool get__seldeeto_23() const { return ____seldeeto_23; }
	inline bool* get_address_of__seldeeto_23() { return &____seldeeto_23; }
	inline void set__seldeeto_23(bool value)
	{
		____seldeeto_23 = value;
	}

	inline static int32_t get_offset_of__keatoostiTrisbitaw_24() { return static_cast<int32_t>(offsetof(M_rearneremeNairdermi234_t620816888, ____keatoostiTrisbitaw_24)); }
	inline String_t* get__keatoostiTrisbitaw_24() const { return ____keatoostiTrisbitaw_24; }
	inline String_t** get_address_of__keatoostiTrisbitaw_24() { return &____keatoostiTrisbitaw_24; }
	inline void set__keatoostiTrisbitaw_24(String_t* value)
	{
		____keatoostiTrisbitaw_24 = value;
		Il2CppCodeGenWriteBarrier(&____keatoostiTrisbitaw_24, value);
	}

	inline static int32_t get_offset_of__drako_25() { return static_cast<int32_t>(offsetof(M_rearneremeNairdermi234_t620816888, ____drako_25)); }
	inline String_t* get__drako_25() const { return ____drako_25; }
	inline String_t** get_address_of__drako_25() { return &____drako_25; }
	inline void set__drako_25(String_t* value)
	{
		____drako_25 = value;
		Il2CppCodeGenWriteBarrier(&____drako_25, value);
	}

	inline static int32_t get_offset_of__drealaQeahel_26() { return static_cast<int32_t>(offsetof(M_rearneremeNairdermi234_t620816888, ____drealaQeahel_26)); }
	inline String_t* get__drealaQeahel_26() const { return ____drealaQeahel_26; }
	inline String_t** get_address_of__drealaQeahel_26() { return &____drealaQeahel_26; }
	inline void set__drealaQeahel_26(String_t* value)
	{
		____drealaQeahel_26 = value;
		Il2CppCodeGenWriteBarrier(&____drealaQeahel_26, value);
	}

	inline static int32_t get_offset_of__didurmi_27() { return static_cast<int32_t>(offsetof(M_rearneremeNairdermi234_t620816888, ____didurmi_27)); }
	inline bool get__didurmi_27() const { return ____didurmi_27; }
	inline bool* get_address_of__didurmi_27() { return &____didurmi_27; }
	inline void set__didurmi_27(bool value)
	{
		____didurmi_27 = value;
	}

	inline static int32_t get_offset_of__lirtirDawdair_28() { return static_cast<int32_t>(offsetof(M_rearneremeNairdermi234_t620816888, ____lirtirDawdair_28)); }
	inline bool get__lirtirDawdair_28() const { return ____lirtirDawdair_28; }
	inline bool* get_address_of__lirtirDawdair_28() { return &____lirtirDawdair_28; }
	inline void set__lirtirDawdair_28(bool value)
	{
		____lirtirDawdair_28 = value;
	}

	inline static int32_t get_offset_of__wheecir_29() { return static_cast<int32_t>(offsetof(M_rearneremeNairdermi234_t620816888, ____wheecir_29)); }
	inline int32_t get__wheecir_29() const { return ____wheecir_29; }
	inline int32_t* get_address_of__wheecir_29() { return &____wheecir_29; }
	inline void set__wheecir_29(int32_t value)
	{
		____wheecir_29 = value;
	}

	inline static int32_t get_offset_of__ranallkisNotisai_30() { return static_cast<int32_t>(offsetof(M_rearneremeNairdermi234_t620816888, ____ranallkisNotisai_30)); }
	inline int32_t get__ranallkisNotisai_30() const { return ____ranallkisNotisai_30; }
	inline int32_t* get_address_of__ranallkisNotisai_30() { return &____ranallkisNotisai_30; }
	inline void set__ranallkisNotisai_30(int32_t value)
	{
		____ranallkisNotisai_30 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
