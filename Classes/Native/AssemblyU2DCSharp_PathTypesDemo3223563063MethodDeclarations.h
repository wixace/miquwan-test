﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PathTypesDemo
struct PathTypesDemo_t3223563063;
// Pathfinding.Path
struct Path_t1974241691;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// Pathfinding.ABPath
struct ABPath_t1187561148;
// OnPathDelegate
struct OnPathDelegate_t598607977;
// Pathfinding.IRaycastableGraph
struct IRaycastableGraph_t2032416694;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t1355284822;
// Pathfinding.RandomPath
struct RandomPath_t3634790782;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_Path1974241691.h"
#include "AssemblyU2DCSharp_PathTypesDemo3223563063.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_OnPathDelegate598607977.h"

// System.Void PathTypesDemo::.ctor()
extern "C"  void PathTypesDemo__ctor_m676226068 (PathTypesDemo_t3223563063 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PathTypesDemo::Update()
extern "C"  void PathTypesDemo_Update_m1215033721 (PathTypesDemo_t3223563063 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PathTypesDemo::OnGUI()
extern "C"  void PathTypesDemo_OnGUI_m171624718 (PathTypesDemo_t3223563063 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PathTypesDemo::OnPathComplete(Pathfinding.Path)
extern "C"  void PathTypesDemo_OnPathComplete_m491393598 (PathTypesDemo_t3223563063 * __this, Path_t1974241691 * ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PathTypesDemo::ClearPrevious()
extern "C"  void PathTypesDemo_ClearPrevious_m3218978710 (PathTypesDemo_t3223563063 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PathTypesDemo::OnApplicationQuit()
extern "C"  void PathTypesDemo_OnApplicationQuit_m3776795538 (PathTypesDemo_t3223563063 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PathTypesDemo::DemoPath()
extern "C"  void PathTypesDemo_DemoPath_m2472716760 (PathTypesDemo_t3223563063 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PathTypesDemo::Constant()
extern "C"  Il2CppObject * PathTypesDemo_Constant_m3056088204 (PathTypesDemo_t3223563063 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PathTypesDemo::ilo_DemoPath1(PathTypesDemo)
extern "C"  void PathTypesDemo_ilo_DemoPath1_m3853771763 (Il2CppObject * __this /* static, unused */, PathTypesDemo_t3223563063 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PathTypesDemo::ilo_IsDone2(Pathfinding.Path)
extern "C"  bool PathTypesDemo_ilo_IsDone2_m3308665418 (Il2CppObject * __this /* static, unused */, Path_t1974241691 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PathTypesDemo::ilo_ClearPrevious3(PathTypesDemo)
extern "C"  void PathTypesDemo_ilo_ClearPrevious3_m1278539113 (Il2CppObject * __this /* static, unused */, PathTypesDemo_t3223563063 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.ABPath PathTypesDemo::ilo_Construct4(UnityEngine.Vector3,UnityEngine.Vector3,OnPathDelegate)
extern "C"  ABPath_t1187561148 * PathTypesDemo_ilo_Construct4_m1171716804 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___start0, Vector3_t4282066566  ___end1, OnPathDelegate_t598607977 * ___callback2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PathTypesDemo::ilo_GetPointsAroundPoint5(UnityEngine.Vector3,Pathfinding.IRaycastableGraph,System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Single,System.Single)
extern "C"  void PathTypesDemo_ilo_GetPointsAroundPoint5_m3790896188 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___p0, Il2CppObject * ___g1, List_1_t1355284822 * ___previousPoints2, float ___radius3, float ___clearanceRadius4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.RandomPath PathTypesDemo::ilo_Construct6(UnityEngine.Vector3,System.Int32,OnPathDelegate)
extern "C"  RandomPath_t3634790782 * PathTypesDemo_ilo_Construct6_m1620295304 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___start0, int32_t ___length1, OnPathDelegate_t598607977 * ___callback2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PathTypesDemo::ilo_Constant7(PathTypesDemo)
extern "C"  Il2CppObject * PathTypesDemo_ilo_Constant7_m2719136481 (Il2CppObject * __this /* static, unused */, PathTypesDemo_t3223563063 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
