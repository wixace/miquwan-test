﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_CompassGenerated
struct UnityEngine_CompassGenerated_t2905950725;
// JSVCall
struct JSVCall_t3708497963;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"

// System.Void UnityEngine_CompassGenerated::.ctor()
extern "C"  void UnityEngine_CompassGenerated__ctor_m1915612726 (UnityEngine_CompassGenerated_t2905950725 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_CompassGenerated::Compass_Compass1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_CompassGenerated_Compass_Compass1_m747509278 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CompassGenerated::Compass_magneticHeading(JSVCall)
extern "C"  void UnityEngine_CompassGenerated_Compass_magneticHeading_m3770596226 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CompassGenerated::Compass_trueHeading(JSVCall)
extern "C"  void UnityEngine_CompassGenerated_Compass_trueHeading_m4230037516 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CompassGenerated::Compass_headingAccuracy(JSVCall)
extern "C"  void UnityEngine_CompassGenerated_Compass_headingAccuracy_m1445590629 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CompassGenerated::Compass_rawVector(JSVCall)
extern "C"  void UnityEngine_CompassGenerated_Compass_rawVector_m1455300981 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CompassGenerated::Compass_timestamp(JSVCall)
extern "C"  void UnityEngine_CompassGenerated_Compass_timestamp_m1705135178 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CompassGenerated::Compass_enabled(JSVCall)
extern "C"  void UnityEngine_CompassGenerated_Compass_enabled_m2703567103 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CompassGenerated::__Register()
extern "C"  void UnityEngine_CompassGenerated___Register_m3307077713 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_CompassGenerated::ilo_attachFinalizerObject1(System.Int32)
extern "C"  bool UnityEngine_CompassGenerated_ilo_attachFinalizerObject1_m3945429609 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CompassGenerated::ilo_setSingle2(System.Int32,System.Single)
extern "C"  void UnityEngine_CompassGenerated_ilo_setSingle2_m3966251775 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_CompassGenerated::ilo_getBooleanS3(System.Int32)
extern "C"  bool UnityEngine_CompassGenerated_ilo_getBooleanS3_m2610599000 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
