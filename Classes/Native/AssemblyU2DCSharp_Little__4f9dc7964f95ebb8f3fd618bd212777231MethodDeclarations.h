﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._4f9dc7964f95ebb8f3fd618bdbb703ab
struct _4f9dc7964f95ebb8f3fd618bdbb703ab_t212777231;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._4f9dc7964f95ebb8f3fd618bdbb703ab::.ctor()
extern "C"  void _4f9dc7964f95ebb8f3fd618bdbb703ab__ctor_m1977757182 (_4f9dc7964f95ebb8f3fd618bdbb703ab_t212777231 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._4f9dc7964f95ebb8f3fd618bdbb703ab::_4f9dc7964f95ebb8f3fd618bdbb703abm2(System.Int32)
extern "C"  int32_t _4f9dc7964f95ebb8f3fd618bdbb703ab__4f9dc7964f95ebb8f3fd618bdbb703abm2_m2892070713 (_4f9dc7964f95ebb8f3fd618bdbb703ab_t212777231 * __this, int32_t ____4f9dc7964f95ebb8f3fd618bdbb703aba0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._4f9dc7964f95ebb8f3fd618bdbb703ab::_4f9dc7964f95ebb8f3fd618bdbb703abm(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _4f9dc7964f95ebb8f3fd618bdbb703ab__4f9dc7964f95ebb8f3fd618bdbb703abm_m3495672733 (_4f9dc7964f95ebb8f3fd618bdbb703ab_t212777231 * __this, int32_t ____4f9dc7964f95ebb8f3fd618bdbb703aba0, int32_t ____4f9dc7964f95ebb8f3fd618bdbb703ab161, int32_t ____4f9dc7964f95ebb8f3fd618bdbb703abc2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
