﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Newtonsoft.Json.Linq.JObject
struct JObject_t1798544199;
// Pomelo.Protobuf.Encoder
struct Encoder_t3832447410;
// Pomelo.Protobuf.Util
struct Util_t3483766166;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t1974256870;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pomelo.Protobuf.MsgEncoder
struct  MsgEncoder_t1387649199  : public Il2CppObject
{
public:
	// Newtonsoft.Json.Linq.JObject Pomelo.Protobuf.MsgEncoder::<protos>k__BackingField
	JObject_t1798544199 * ___U3CprotosU3Ek__BackingField_0;
	// Pomelo.Protobuf.Encoder Pomelo.Protobuf.MsgEncoder::<encoder>k__BackingField
	Encoder_t3832447410 * ___U3CencoderU3Ek__BackingField_1;
	// Pomelo.Protobuf.Util Pomelo.Protobuf.MsgEncoder::<util>k__BackingField
	Util_t3483766166 * ___U3CutilU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CprotosU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(MsgEncoder_t1387649199, ___U3CprotosU3Ek__BackingField_0)); }
	inline JObject_t1798544199 * get_U3CprotosU3Ek__BackingField_0() const { return ___U3CprotosU3Ek__BackingField_0; }
	inline JObject_t1798544199 ** get_address_of_U3CprotosU3Ek__BackingField_0() { return &___U3CprotosU3Ek__BackingField_0; }
	inline void set_U3CprotosU3Ek__BackingField_0(JObject_t1798544199 * value)
	{
		___U3CprotosU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CprotosU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3CencoderU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(MsgEncoder_t1387649199, ___U3CencoderU3Ek__BackingField_1)); }
	inline Encoder_t3832447410 * get_U3CencoderU3Ek__BackingField_1() const { return ___U3CencoderU3Ek__BackingField_1; }
	inline Encoder_t3832447410 ** get_address_of_U3CencoderU3Ek__BackingField_1() { return &___U3CencoderU3Ek__BackingField_1; }
	inline void set_U3CencoderU3Ek__BackingField_1(Encoder_t3832447410 * value)
	{
		___U3CencoderU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CencoderU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_U3CutilU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(MsgEncoder_t1387649199, ___U3CutilU3Ek__BackingField_2)); }
	inline Util_t3483766166 * get_U3CutilU3Ek__BackingField_2() const { return ___U3CutilU3Ek__BackingField_2; }
	inline Util_t3483766166 ** get_address_of_U3CutilU3Ek__BackingField_2() { return &___U3CutilU3Ek__BackingField_2; }
	inline void set_U3CutilU3Ek__BackingField_2(Util_t3483766166 * value)
	{
		___U3CutilU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CutilU3Ek__BackingField_2, value);
	}
};

struct MsgEncoder_t1387649199_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Pomelo.Protobuf.MsgEncoder::<>f__switch$map21
	Dictionary_2_t1974256870 * ___U3CU3Ef__switchU24map21_3;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Pomelo.Protobuf.MsgEncoder::<>f__switch$map22
	Dictionary_2_t1974256870 * ___U3CU3Ef__switchU24map22_4;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Pomelo.Protobuf.MsgEncoder::<>f__switch$map23
	Dictionary_2_t1974256870 * ___U3CU3Ef__switchU24map23_5;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map21_3() { return static_cast<int32_t>(offsetof(MsgEncoder_t1387649199_StaticFields, ___U3CU3Ef__switchU24map21_3)); }
	inline Dictionary_2_t1974256870 * get_U3CU3Ef__switchU24map21_3() const { return ___U3CU3Ef__switchU24map21_3; }
	inline Dictionary_2_t1974256870 ** get_address_of_U3CU3Ef__switchU24map21_3() { return &___U3CU3Ef__switchU24map21_3; }
	inline void set_U3CU3Ef__switchU24map21_3(Dictionary_2_t1974256870 * value)
	{
		___U3CU3Ef__switchU24map21_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map21_3, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map22_4() { return static_cast<int32_t>(offsetof(MsgEncoder_t1387649199_StaticFields, ___U3CU3Ef__switchU24map22_4)); }
	inline Dictionary_2_t1974256870 * get_U3CU3Ef__switchU24map22_4() const { return ___U3CU3Ef__switchU24map22_4; }
	inline Dictionary_2_t1974256870 ** get_address_of_U3CU3Ef__switchU24map22_4() { return &___U3CU3Ef__switchU24map22_4; }
	inline void set_U3CU3Ef__switchU24map22_4(Dictionary_2_t1974256870 * value)
	{
		___U3CU3Ef__switchU24map22_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map22_4, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map23_5() { return static_cast<int32_t>(offsetof(MsgEncoder_t1387649199_StaticFields, ___U3CU3Ef__switchU24map23_5)); }
	inline Dictionary_2_t1974256870 * get_U3CU3Ef__switchU24map23_5() const { return ___U3CU3Ef__switchU24map23_5; }
	inline Dictionary_2_t1974256870 ** get_address_of_U3CU3Ef__switchU24map23_5() { return &___U3CU3Ef__switchU24map23_5; }
	inline void set_U3CU3Ef__switchU24map23_5(Dictionary_2_t1974256870 * value)
	{
		___U3CU3Ef__switchU24map23_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map23_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
