﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1190435706MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UpdateForJs>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m184174011(__this, ___dictionary0, method) ((  void (*) (Enumerator_t2385779856 *, Dictionary_2_t1068456464 *, const MethodInfo*))Enumerator__ctor_m2377115088_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UpdateForJs>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1946159824(__this, method) ((  Il2CppObject * (*) (Enumerator_t2385779856 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1037642267_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UpdateForJs>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2885471258(__this, method) ((  void (*) (Enumerator_t2385779856 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2809374949_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UpdateForJs>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3270148881(__this, method) ((  DictionaryEntry_t1751606614  (*) (Enumerator_t2385779856 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2434214620_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UpdateForJs>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m988801964(__this, method) ((  Il2CppObject * (*) (Enumerator_t2385779856 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3735627447_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UpdateForJs>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2099351358(__this, method) ((  Il2CppObject * (*) (Enumerator_t2385779856 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m393753481_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UpdateForJs>::MoveNext()
#define Enumerator_MoveNext_m3194283338(__this, method) ((  bool (*) (Enumerator_t2385779856 *, const MethodInfo*))Enumerator_MoveNext_m1213995029_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UpdateForJs>::get_Current()
#define Enumerator_get_Current_m2541455538(__this, method) ((  KeyValuePair_2_t967237170  (*) (Enumerator_t2385779856 *, const MethodInfo*))Enumerator_get_Current_m1399860359_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UpdateForJs>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m2558190547(__this, method) ((  int32_t (*) (Enumerator_t2385779856 *, const MethodInfo*))Enumerator_get_CurrentKey_m1767398110_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UpdateForJs>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m1372118227(__this, method) ((  UpdateForJs_t1071193225 * (*) (Enumerator_t2385779856 *, const MethodInfo*))Enumerator_get_CurrentValue_m3384846750_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UpdateForJs>::Reset()
#define Enumerator_Reset_m642545549(__this, method) ((  void (*) (Enumerator_t2385779856 *, const MethodInfo*))Enumerator_Reset_m1080084514_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UpdateForJs>::VerifyState()
#define Enumerator_VerifyState_m2545494486(__this, method) ((  void (*) (Enumerator_t2385779856 *, const MethodInfo*))Enumerator_VerifyState_m2404513451_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UpdateForJs>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m833714110(__this, method) ((  void (*) (Enumerator_t2385779856 *, const MethodInfo*))Enumerator_VerifyCurrent_m2789892947_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UpdateForJs>::Dispose()
#define Enumerator_Dispose_m1534411037(__this, method) ((  void (*) (Enumerator_t2385779856 *, const MethodInfo*))Enumerator_Dispose_m1102561394_gshared)(__this, method)
