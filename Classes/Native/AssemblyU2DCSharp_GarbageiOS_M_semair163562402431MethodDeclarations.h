﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_semair163
struct M_semair163_t562402431;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"

// System.Void GarbageiOS.M_semair163::.ctor()
extern "C"  void M_semair163__ctor_m2658479044 (M_semair163_t562402431 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_semair163::M_deataisteWairnay0(System.String[],System.Int32)
extern "C"  void M_semair163_M_deataisteWairnay0_m190638508 (M_semair163_t562402431 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_semair163::M_triturMomemsou1(System.String[],System.Int32)
extern "C"  void M_semair163_M_triturMomemsou1_m1928003532 (M_semair163_t562402431 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_semair163::M_gaysallhir2(System.String[],System.Int32)
extern "C"  void M_semair163_M_gaysallhir2_m2348990005 (M_semair163_t562402431 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
