﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// JSApi/JSNative
struct JSNative_t3654534910;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void JSApi/JSNative::.ctor(System.Object,System.IntPtr)
extern "C"  void JSNative__ctor_m1510467237 (JSNative_t3654534910 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JSApi/JSNative::Invoke(System.IntPtr,System.UInt32,System.IntPtr)
extern "C"  bool JSNative_Invoke_m875314819 (JSNative_t3654534910 * __this, IntPtr_t ___cx0, uint32_t ___argc1, IntPtr_t ___vp2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult JSApi/JSNative::BeginInvoke(System.IntPtr,System.UInt32,System.IntPtr,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * JSNative_BeginInvoke_m2762560020 (JSNative_t3654534910 * __this, IntPtr_t ___cx0, uint32_t ___argc1, IntPtr_t ___vp2, AsyncCallback_t1369114871 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JSApi/JSNative::EndInvoke(System.IAsyncResult)
extern "C"  bool JSNative_EndInvoke_m622773993 (JSNative_t3654534910 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
