﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._2e032e1a76a66392a072885063cada0a
struct _2e032e1a76a66392a072885063cada0a_t920276266;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._2e032e1a76a66392a072885063cada0a::.ctor()
extern "C"  void _2e032e1a76a66392a072885063cada0a__ctor_m3309045827 (_2e032e1a76a66392a072885063cada0a_t920276266 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._2e032e1a76a66392a072885063cada0a::_2e032e1a76a66392a072885063cada0am2(System.Int32)
extern "C"  int32_t _2e032e1a76a66392a072885063cada0a__2e032e1a76a66392a072885063cada0am2_m1776005465 (_2e032e1a76a66392a072885063cada0a_t920276266 * __this, int32_t ____2e032e1a76a66392a072885063cada0aa0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._2e032e1a76a66392a072885063cada0a::_2e032e1a76a66392a072885063cada0am(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _2e032e1a76a66392a072885063cada0a__2e032e1a76a66392a072885063cada0am_m4253560189 (_2e032e1a76a66392a072885063cada0a_t920276266 * __this, int32_t ____2e032e1a76a66392a072885063cada0aa0, int32_t ____2e032e1a76a66392a072885063cada0a591, int32_t ____2e032e1a76a66392a072885063cada0ac2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
