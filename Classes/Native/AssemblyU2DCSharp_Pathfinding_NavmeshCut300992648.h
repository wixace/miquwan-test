﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<Pathfinding.NavmeshCut>
struct List_1_t1669178200;
// UnityEngine.Mesh
struct Mesh_t4241756145;
// UnityEngine.Vector3[][]
struct Vector3U5BU5DU5BU5D_t3570135410;
// UnityEngine.Transform
struct Transform_t1659122786;
// System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Int32>
struct Dictionary_2_t2245318082;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>
struct Dictionary_2_t1151101739;
// System.Action`1<Pathfinding.NavmeshCut>
struct Action_1_t696808784;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "AssemblyU2DCSharp_Pathfinding_NavmeshCut_MeshType689170522.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882.h"
#include "UnityEngine_UnityEngine_Bounds2711641849.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.NavmeshCut
struct  NavmeshCut_t300992648  : public MonoBehaviour_t667441552
{
public:
	// Pathfinding.NavmeshCut/MeshType Pathfinding.NavmeshCut::type
	int32_t ___type_3;
	// UnityEngine.Mesh Pathfinding.NavmeshCut::mesh
	Mesh_t4241756145 * ___mesh_4;
	// UnityEngine.Vector2 Pathfinding.NavmeshCut::rectangleSize
	Vector2_t4282066565  ___rectangleSize_5;
	// System.Single Pathfinding.NavmeshCut::circleRadius
	float ___circleRadius_6;
	// System.Int32 Pathfinding.NavmeshCut::circleResolution
	int32_t ___circleResolution_7;
	// System.Single Pathfinding.NavmeshCut::height
	float ___height_8;
	// System.Single Pathfinding.NavmeshCut::meshScale
	float ___meshScale_9;
	// UnityEngine.Vector3 Pathfinding.NavmeshCut::center
	Vector3_t4282066566  ___center_10;
	// System.Single Pathfinding.NavmeshCut::updateDistance
	float ___updateDistance_11;
	// System.Boolean Pathfinding.NavmeshCut::isDual
	bool ___isDual_12;
	// System.Boolean Pathfinding.NavmeshCut::cutsAddedGeom
	bool ___cutsAddedGeom_13;
	// System.Single Pathfinding.NavmeshCut::updateRotationDistance
	float ___updateRotationDistance_14;
	// System.Boolean Pathfinding.NavmeshCut::useRotation
	bool ___useRotation_15;
	// UnityEngine.Vector3[][] Pathfinding.NavmeshCut::contours
	Vector3U5BU5DU5BU5D_t3570135410* ___contours_16;
	// UnityEngine.Transform Pathfinding.NavmeshCut::tr
	Transform_t1659122786 * ___tr_17;
	// UnityEngine.Mesh Pathfinding.NavmeshCut::lastMesh
	Mesh_t4241756145 * ___lastMesh_18;
	// UnityEngine.Vector3 Pathfinding.NavmeshCut::lastPosition
	Vector3_t4282066566  ___lastPosition_19;
	// UnityEngine.Quaternion Pathfinding.NavmeshCut::lastRotation
	Quaternion_t1553702882  ___lastRotation_20;
	// System.Boolean Pathfinding.NavmeshCut::wasEnabled
	bool ___wasEnabled_21;
	// UnityEngine.Bounds Pathfinding.NavmeshCut::bounds
	Bounds_t2711641849  ___bounds_22;
	// UnityEngine.Bounds Pathfinding.NavmeshCut::lastBounds
	Bounds_t2711641849  ___lastBounds_23;

public:
	inline static int32_t get_offset_of_type_3() { return static_cast<int32_t>(offsetof(NavmeshCut_t300992648, ___type_3)); }
	inline int32_t get_type_3() const { return ___type_3; }
	inline int32_t* get_address_of_type_3() { return &___type_3; }
	inline void set_type_3(int32_t value)
	{
		___type_3 = value;
	}

	inline static int32_t get_offset_of_mesh_4() { return static_cast<int32_t>(offsetof(NavmeshCut_t300992648, ___mesh_4)); }
	inline Mesh_t4241756145 * get_mesh_4() const { return ___mesh_4; }
	inline Mesh_t4241756145 ** get_address_of_mesh_4() { return &___mesh_4; }
	inline void set_mesh_4(Mesh_t4241756145 * value)
	{
		___mesh_4 = value;
		Il2CppCodeGenWriteBarrier(&___mesh_4, value);
	}

	inline static int32_t get_offset_of_rectangleSize_5() { return static_cast<int32_t>(offsetof(NavmeshCut_t300992648, ___rectangleSize_5)); }
	inline Vector2_t4282066565  get_rectangleSize_5() const { return ___rectangleSize_5; }
	inline Vector2_t4282066565 * get_address_of_rectangleSize_5() { return &___rectangleSize_5; }
	inline void set_rectangleSize_5(Vector2_t4282066565  value)
	{
		___rectangleSize_5 = value;
	}

	inline static int32_t get_offset_of_circleRadius_6() { return static_cast<int32_t>(offsetof(NavmeshCut_t300992648, ___circleRadius_6)); }
	inline float get_circleRadius_6() const { return ___circleRadius_6; }
	inline float* get_address_of_circleRadius_6() { return &___circleRadius_6; }
	inline void set_circleRadius_6(float value)
	{
		___circleRadius_6 = value;
	}

	inline static int32_t get_offset_of_circleResolution_7() { return static_cast<int32_t>(offsetof(NavmeshCut_t300992648, ___circleResolution_7)); }
	inline int32_t get_circleResolution_7() const { return ___circleResolution_7; }
	inline int32_t* get_address_of_circleResolution_7() { return &___circleResolution_7; }
	inline void set_circleResolution_7(int32_t value)
	{
		___circleResolution_7 = value;
	}

	inline static int32_t get_offset_of_height_8() { return static_cast<int32_t>(offsetof(NavmeshCut_t300992648, ___height_8)); }
	inline float get_height_8() const { return ___height_8; }
	inline float* get_address_of_height_8() { return &___height_8; }
	inline void set_height_8(float value)
	{
		___height_8 = value;
	}

	inline static int32_t get_offset_of_meshScale_9() { return static_cast<int32_t>(offsetof(NavmeshCut_t300992648, ___meshScale_9)); }
	inline float get_meshScale_9() const { return ___meshScale_9; }
	inline float* get_address_of_meshScale_9() { return &___meshScale_9; }
	inline void set_meshScale_9(float value)
	{
		___meshScale_9 = value;
	}

	inline static int32_t get_offset_of_center_10() { return static_cast<int32_t>(offsetof(NavmeshCut_t300992648, ___center_10)); }
	inline Vector3_t4282066566  get_center_10() const { return ___center_10; }
	inline Vector3_t4282066566 * get_address_of_center_10() { return &___center_10; }
	inline void set_center_10(Vector3_t4282066566  value)
	{
		___center_10 = value;
	}

	inline static int32_t get_offset_of_updateDistance_11() { return static_cast<int32_t>(offsetof(NavmeshCut_t300992648, ___updateDistance_11)); }
	inline float get_updateDistance_11() const { return ___updateDistance_11; }
	inline float* get_address_of_updateDistance_11() { return &___updateDistance_11; }
	inline void set_updateDistance_11(float value)
	{
		___updateDistance_11 = value;
	}

	inline static int32_t get_offset_of_isDual_12() { return static_cast<int32_t>(offsetof(NavmeshCut_t300992648, ___isDual_12)); }
	inline bool get_isDual_12() const { return ___isDual_12; }
	inline bool* get_address_of_isDual_12() { return &___isDual_12; }
	inline void set_isDual_12(bool value)
	{
		___isDual_12 = value;
	}

	inline static int32_t get_offset_of_cutsAddedGeom_13() { return static_cast<int32_t>(offsetof(NavmeshCut_t300992648, ___cutsAddedGeom_13)); }
	inline bool get_cutsAddedGeom_13() const { return ___cutsAddedGeom_13; }
	inline bool* get_address_of_cutsAddedGeom_13() { return &___cutsAddedGeom_13; }
	inline void set_cutsAddedGeom_13(bool value)
	{
		___cutsAddedGeom_13 = value;
	}

	inline static int32_t get_offset_of_updateRotationDistance_14() { return static_cast<int32_t>(offsetof(NavmeshCut_t300992648, ___updateRotationDistance_14)); }
	inline float get_updateRotationDistance_14() const { return ___updateRotationDistance_14; }
	inline float* get_address_of_updateRotationDistance_14() { return &___updateRotationDistance_14; }
	inline void set_updateRotationDistance_14(float value)
	{
		___updateRotationDistance_14 = value;
	}

	inline static int32_t get_offset_of_useRotation_15() { return static_cast<int32_t>(offsetof(NavmeshCut_t300992648, ___useRotation_15)); }
	inline bool get_useRotation_15() const { return ___useRotation_15; }
	inline bool* get_address_of_useRotation_15() { return &___useRotation_15; }
	inline void set_useRotation_15(bool value)
	{
		___useRotation_15 = value;
	}

	inline static int32_t get_offset_of_contours_16() { return static_cast<int32_t>(offsetof(NavmeshCut_t300992648, ___contours_16)); }
	inline Vector3U5BU5DU5BU5D_t3570135410* get_contours_16() const { return ___contours_16; }
	inline Vector3U5BU5DU5BU5D_t3570135410** get_address_of_contours_16() { return &___contours_16; }
	inline void set_contours_16(Vector3U5BU5DU5BU5D_t3570135410* value)
	{
		___contours_16 = value;
		Il2CppCodeGenWriteBarrier(&___contours_16, value);
	}

	inline static int32_t get_offset_of_tr_17() { return static_cast<int32_t>(offsetof(NavmeshCut_t300992648, ___tr_17)); }
	inline Transform_t1659122786 * get_tr_17() const { return ___tr_17; }
	inline Transform_t1659122786 ** get_address_of_tr_17() { return &___tr_17; }
	inline void set_tr_17(Transform_t1659122786 * value)
	{
		___tr_17 = value;
		Il2CppCodeGenWriteBarrier(&___tr_17, value);
	}

	inline static int32_t get_offset_of_lastMesh_18() { return static_cast<int32_t>(offsetof(NavmeshCut_t300992648, ___lastMesh_18)); }
	inline Mesh_t4241756145 * get_lastMesh_18() const { return ___lastMesh_18; }
	inline Mesh_t4241756145 ** get_address_of_lastMesh_18() { return &___lastMesh_18; }
	inline void set_lastMesh_18(Mesh_t4241756145 * value)
	{
		___lastMesh_18 = value;
		Il2CppCodeGenWriteBarrier(&___lastMesh_18, value);
	}

	inline static int32_t get_offset_of_lastPosition_19() { return static_cast<int32_t>(offsetof(NavmeshCut_t300992648, ___lastPosition_19)); }
	inline Vector3_t4282066566  get_lastPosition_19() const { return ___lastPosition_19; }
	inline Vector3_t4282066566 * get_address_of_lastPosition_19() { return &___lastPosition_19; }
	inline void set_lastPosition_19(Vector3_t4282066566  value)
	{
		___lastPosition_19 = value;
	}

	inline static int32_t get_offset_of_lastRotation_20() { return static_cast<int32_t>(offsetof(NavmeshCut_t300992648, ___lastRotation_20)); }
	inline Quaternion_t1553702882  get_lastRotation_20() const { return ___lastRotation_20; }
	inline Quaternion_t1553702882 * get_address_of_lastRotation_20() { return &___lastRotation_20; }
	inline void set_lastRotation_20(Quaternion_t1553702882  value)
	{
		___lastRotation_20 = value;
	}

	inline static int32_t get_offset_of_wasEnabled_21() { return static_cast<int32_t>(offsetof(NavmeshCut_t300992648, ___wasEnabled_21)); }
	inline bool get_wasEnabled_21() const { return ___wasEnabled_21; }
	inline bool* get_address_of_wasEnabled_21() { return &___wasEnabled_21; }
	inline void set_wasEnabled_21(bool value)
	{
		___wasEnabled_21 = value;
	}

	inline static int32_t get_offset_of_bounds_22() { return static_cast<int32_t>(offsetof(NavmeshCut_t300992648, ___bounds_22)); }
	inline Bounds_t2711641849  get_bounds_22() const { return ___bounds_22; }
	inline Bounds_t2711641849 * get_address_of_bounds_22() { return &___bounds_22; }
	inline void set_bounds_22(Bounds_t2711641849  value)
	{
		___bounds_22 = value;
	}

	inline static int32_t get_offset_of_lastBounds_23() { return static_cast<int32_t>(offsetof(NavmeshCut_t300992648, ___lastBounds_23)); }
	inline Bounds_t2711641849  get_lastBounds_23() const { return ___lastBounds_23; }
	inline Bounds_t2711641849 * get_address_of_lastBounds_23() { return &___lastBounds_23; }
	inline void set_lastBounds_23(Bounds_t2711641849  value)
	{
		___lastBounds_23 = value;
	}
};

struct NavmeshCut_t300992648_StaticFields
{
public:
	// System.Collections.Generic.List`1<Pathfinding.NavmeshCut> Pathfinding.NavmeshCut::allCuts
	List_1_t1669178200 * ___allCuts_2;
	// System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Int32> Pathfinding.NavmeshCut::edges
	Dictionary_2_t2245318082 * ___edges_24;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32> Pathfinding.NavmeshCut::pointers
	Dictionary_2_t1151101739 * ___pointers_25;
	// UnityEngine.Color Pathfinding.NavmeshCut::GizmoColor
	Color_t4194546905  ___GizmoColor_26;
	// System.Action`1<Pathfinding.NavmeshCut> Pathfinding.NavmeshCut::OnDestroyCallback
	Action_1_t696808784 * ___OnDestroyCallback_27;

public:
	inline static int32_t get_offset_of_allCuts_2() { return static_cast<int32_t>(offsetof(NavmeshCut_t300992648_StaticFields, ___allCuts_2)); }
	inline List_1_t1669178200 * get_allCuts_2() const { return ___allCuts_2; }
	inline List_1_t1669178200 ** get_address_of_allCuts_2() { return &___allCuts_2; }
	inline void set_allCuts_2(List_1_t1669178200 * value)
	{
		___allCuts_2 = value;
		Il2CppCodeGenWriteBarrier(&___allCuts_2, value);
	}

	inline static int32_t get_offset_of_edges_24() { return static_cast<int32_t>(offsetof(NavmeshCut_t300992648_StaticFields, ___edges_24)); }
	inline Dictionary_2_t2245318082 * get_edges_24() const { return ___edges_24; }
	inline Dictionary_2_t2245318082 ** get_address_of_edges_24() { return &___edges_24; }
	inline void set_edges_24(Dictionary_2_t2245318082 * value)
	{
		___edges_24 = value;
		Il2CppCodeGenWriteBarrier(&___edges_24, value);
	}

	inline static int32_t get_offset_of_pointers_25() { return static_cast<int32_t>(offsetof(NavmeshCut_t300992648_StaticFields, ___pointers_25)); }
	inline Dictionary_2_t1151101739 * get_pointers_25() const { return ___pointers_25; }
	inline Dictionary_2_t1151101739 ** get_address_of_pointers_25() { return &___pointers_25; }
	inline void set_pointers_25(Dictionary_2_t1151101739 * value)
	{
		___pointers_25 = value;
		Il2CppCodeGenWriteBarrier(&___pointers_25, value);
	}

	inline static int32_t get_offset_of_GizmoColor_26() { return static_cast<int32_t>(offsetof(NavmeshCut_t300992648_StaticFields, ___GizmoColor_26)); }
	inline Color_t4194546905  get_GizmoColor_26() const { return ___GizmoColor_26; }
	inline Color_t4194546905 * get_address_of_GizmoColor_26() { return &___GizmoColor_26; }
	inline void set_GizmoColor_26(Color_t4194546905  value)
	{
		___GizmoColor_26 = value;
	}

	inline static int32_t get_offset_of_OnDestroyCallback_27() { return static_cast<int32_t>(offsetof(NavmeshCut_t300992648_StaticFields, ___OnDestroyCallback_27)); }
	inline Action_1_t696808784 * get_OnDestroyCallback_27() const { return ___OnDestroyCallback_27; }
	inline Action_1_t696808784 ** get_address_of_OnDestroyCallback_27() { return &___OnDestroyCallback_27; }
	inline void set_OnDestroyCallback_27(Action_1_t696808784 * value)
	{
		___OnDestroyCallback_27 = value;
		Il2CppCodeGenWriteBarrier(&___OnDestroyCallback_27, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
