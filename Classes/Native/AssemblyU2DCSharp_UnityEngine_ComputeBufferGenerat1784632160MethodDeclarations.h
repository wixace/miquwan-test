﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_ComputeBufferGenerated
struct UnityEngine_ComputeBufferGenerated_t1784632160;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"

// System.Void UnityEngine_ComputeBufferGenerated::.ctor()
extern "C"  void UnityEngine_ComputeBufferGenerated__ctor_m1843087675 (UnityEngine_ComputeBufferGenerated_t1784632160 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ComputeBufferGenerated::ComputeBuffer_ComputeBuffer1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ComputeBufferGenerated_ComputeBuffer_ComputeBuffer1_m107192495 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ComputeBufferGenerated::ComputeBuffer_ComputeBuffer2(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ComputeBufferGenerated_ComputeBuffer_ComputeBuffer2_m1351956976 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ComputeBufferGenerated::ComputeBuffer_count(JSVCall)
extern "C"  void UnityEngine_ComputeBufferGenerated_ComputeBuffer_count_m3347213543 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ComputeBufferGenerated::ComputeBuffer_stride(JSVCall)
extern "C"  void UnityEngine_ComputeBufferGenerated_ComputeBuffer_stride_m1293325629 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ComputeBufferGenerated::ComputeBuffer_Dispose(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ComputeBufferGenerated_ComputeBuffer_Dispose_m2174832396 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ComputeBufferGenerated::ComputeBuffer_GetData__Array(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ComputeBufferGenerated_ComputeBuffer_GetData__Array_m3642140142 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ComputeBufferGenerated::ComputeBuffer_Release(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ComputeBufferGenerated_ComputeBuffer_Release_m545878772 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ComputeBufferGenerated::ComputeBuffer_SetData__Array(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ComputeBufferGenerated_ComputeBuffer_SetData__Array_m1275939426 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ComputeBufferGenerated::ComputeBuffer_CopyCount__ComputeBuffer__ComputeBuffer__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ComputeBufferGenerated_ComputeBuffer_CopyCount__ComputeBuffer__ComputeBuffer__Int32_m2177397385 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ComputeBufferGenerated::__Register()
extern "C"  void UnityEngine_ComputeBufferGenerated___Register_m1595523180 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_ComputeBufferGenerated::ilo_getInt321(System.Int32)
extern "C"  int32_t UnityEngine_ComputeBufferGenerated_ilo_getInt321_m3571194242 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_ComputeBufferGenerated::ilo_getEnum2(System.Int32)
extern "C"  int32_t UnityEngine_ComputeBufferGenerated_ilo_getEnum2_m889820726 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_ComputeBufferGenerated::ilo_getObject3(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_ComputeBufferGenerated_ilo_getObject3_m613733654 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
