﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BehaviourUtil/JSDelayFun0
struct JSDelayFun0_t1074711451;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void BehaviourUtil/JSDelayFun0::.ctor(System.Object,System.IntPtr)
extern "C"  void JSDelayFun0__ctor_m271197682 (JSDelayFun0_t1074711451 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BehaviourUtil/JSDelayFun0::Invoke()
extern "C"  void JSDelayFun0_Invoke_m921001164 (JSDelayFun0_t1074711451 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult BehaviourUtil/JSDelayFun0::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * JSDelayFun0_BeginInvoke_m297937983 (JSDelayFun0_t1074711451 * __this, AsyncCallback_t1369114871 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BehaviourUtil/JSDelayFun0::EndInvoke(System.IAsyncResult)
extern "C"  void JSDelayFun0_EndInvoke_m5398786 (JSDelayFun0_t1074711451 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
