﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Type
struct Type_t;
// System.String
struct String_t;

#include "mscorlib_System_Attribute2523058482.h"
#include "mscorlib_System_Nullable_1_gen560925241.h"
#include "mscorlib_System_Nullable_1_gen3164635835.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SharpKit.JavaScript.JsTypeAttribute
struct  JsTypeAttribute_t3342267407  : public Attribute_t2523058482
{
public:
	// System.Nullable`1<System.Boolean> SharpKit.JavaScript.JsTypeAttribute::_NativeParams
	Nullable_1_t560925241  ____NativeParams_0;
	// System.Nullable`1<System.Boolean> SharpKit.JavaScript.JsTypeAttribute::_NativeDelegates
	Nullable_1_t560925241  ____NativeDelegates_1;
	// System.Nullable`1<System.Boolean> SharpKit.JavaScript.JsTypeAttribute::_OmitCasts
	Nullable_1_t560925241  ____OmitCasts_2;
	// System.Nullable`1<System.Boolean> SharpKit.JavaScript.JsTypeAttribute::_PropertiesAsFields
	Nullable_1_t560925241  ____PropertiesAsFields_3;
	// System.Nullable`1<System.Boolean> SharpKit.JavaScript.JsTypeAttribute::_AutomaticPropertiesAsFields
	Nullable_1_t560925241  ____AutomaticPropertiesAsFields_4;
	// System.Nullable`1<System.Boolean> SharpKit.JavaScript.JsTypeAttribute::_NativeEnumerator
	Nullable_1_t560925241  ____NativeEnumerator_5;
	// System.Nullable`1<System.Boolean> SharpKit.JavaScript.JsTypeAttribute::_NativeArrayEnumerator
	Nullable_1_t560925241  ____NativeArrayEnumerator_6;
	// System.Nullable`1<System.Boolean> SharpKit.JavaScript.JsTypeAttribute::_NativeConstructors
	Nullable_1_t560925241  ____NativeConstructors_7;
	// System.Nullable`1<System.Boolean> SharpKit.JavaScript.JsTypeAttribute::_NativeOverloads
	Nullable_1_t560925241  ____NativeOverloads_8;
	// System.Nullable`1<System.Boolean> SharpKit.JavaScript.JsTypeAttribute::_NativeOperatorOverloads
	Nullable_1_t560925241  ____NativeOperatorOverloads_9;
	// System.Nullable`1<System.Boolean> SharpKit.JavaScript.JsTypeAttribute::_Native
	Nullable_1_t560925241  ____Native_10;
	// System.Nullable`1<System.Boolean> SharpKit.JavaScript.JsTypeAttribute::_GlobalObject
	Nullable_1_t560925241  ____GlobalObject_11;
	// System.Nullable`1<System.Boolean> SharpKit.JavaScript.JsTypeAttribute::_NativeFunctions
	Nullable_1_t560925241  ____NativeFunctions_12;
	// System.Nullable`1<System.Boolean> SharpKit.JavaScript.JsTypeAttribute::_NativeJsons
	Nullable_1_t560925241  ____NativeJsons_13;
	// System.Nullable`1<System.Boolean> SharpKit.JavaScript.JsTypeAttribute::_Export
	Nullable_1_t560925241  ____Export_14;
	// System.Nullable`1<System.Boolean> SharpKit.JavaScript.JsTypeAttribute::_IgnoreGenericTypeArguments
	Nullable_1_t560925241  ____IgnoreGenericTypeArguments_15;
	// System.Nullable`1<System.Boolean> SharpKit.JavaScript.JsTypeAttribute::_IgnoreGenericMethodArguments
	Nullable_1_t560925241  ____IgnoreGenericMethodArguments_16;
	// System.Nullable`1<SharpKit.JavaScript.JsMode> SharpKit.JavaScript.JsTypeAttribute::_Mode
	Nullable_1_t3164635835  ____Mode_17;
	// System.Nullable`1<System.Boolean> SharpKit.JavaScript.JsTypeAttribute::_InlineFields
	Nullable_1_t560925241  ____InlineFields_18;
	// System.Nullable`1<System.Boolean> SharpKit.JavaScript.JsTypeAttribute::_OmitDefaultConstructor
	Nullable_1_t560925241  ____OmitDefaultConstructor_19;
	// System.Nullable`1<System.Boolean> SharpKit.JavaScript.JsTypeAttribute::_NativeCasts
	Nullable_1_t560925241  ____NativeCasts_20;
	// System.Type SharpKit.JavaScript.JsTypeAttribute::<TargetType>k__BackingField
	Type_t * ___U3CTargetTypeU3Ek__BackingField_21;
	// System.String SharpKit.JavaScript.JsTypeAttribute::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_22;

public:
	inline static int32_t get_offset_of__NativeParams_0() { return static_cast<int32_t>(offsetof(JsTypeAttribute_t3342267407, ____NativeParams_0)); }
	inline Nullable_1_t560925241  get__NativeParams_0() const { return ____NativeParams_0; }
	inline Nullable_1_t560925241 * get_address_of__NativeParams_0() { return &____NativeParams_0; }
	inline void set__NativeParams_0(Nullable_1_t560925241  value)
	{
		____NativeParams_0 = value;
	}

	inline static int32_t get_offset_of__NativeDelegates_1() { return static_cast<int32_t>(offsetof(JsTypeAttribute_t3342267407, ____NativeDelegates_1)); }
	inline Nullable_1_t560925241  get__NativeDelegates_1() const { return ____NativeDelegates_1; }
	inline Nullable_1_t560925241 * get_address_of__NativeDelegates_1() { return &____NativeDelegates_1; }
	inline void set__NativeDelegates_1(Nullable_1_t560925241  value)
	{
		____NativeDelegates_1 = value;
	}

	inline static int32_t get_offset_of__OmitCasts_2() { return static_cast<int32_t>(offsetof(JsTypeAttribute_t3342267407, ____OmitCasts_2)); }
	inline Nullable_1_t560925241  get__OmitCasts_2() const { return ____OmitCasts_2; }
	inline Nullable_1_t560925241 * get_address_of__OmitCasts_2() { return &____OmitCasts_2; }
	inline void set__OmitCasts_2(Nullable_1_t560925241  value)
	{
		____OmitCasts_2 = value;
	}

	inline static int32_t get_offset_of__PropertiesAsFields_3() { return static_cast<int32_t>(offsetof(JsTypeAttribute_t3342267407, ____PropertiesAsFields_3)); }
	inline Nullable_1_t560925241  get__PropertiesAsFields_3() const { return ____PropertiesAsFields_3; }
	inline Nullable_1_t560925241 * get_address_of__PropertiesAsFields_3() { return &____PropertiesAsFields_3; }
	inline void set__PropertiesAsFields_3(Nullable_1_t560925241  value)
	{
		____PropertiesAsFields_3 = value;
	}

	inline static int32_t get_offset_of__AutomaticPropertiesAsFields_4() { return static_cast<int32_t>(offsetof(JsTypeAttribute_t3342267407, ____AutomaticPropertiesAsFields_4)); }
	inline Nullable_1_t560925241  get__AutomaticPropertiesAsFields_4() const { return ____AutomaticPropertiesAsFields_4; }
	inline Nullable_1_t560925241 * get_address_of__AutomaticPropertiesAsFields_4() { return &____AutomaticPropertiesAsFields_4; }
	inline void set__AutomaticPropertiesAsFields_4(Nullable_1_t560925241  value)
	{
		____AutomaticPropertiesAsFields_4 = value;
	}

	inline static int32_t get_offset_of__NativeEnumerator_5() { return static_cast<int32_t>(offsetof(JsTypeAttribute_t3342267407, ____NativeEnumerator_5)); }
	inline Nullable_1_t560925241  get__NativeEnumerator_5() const { return ____NativeEnumerator_5; }
	inline Nullable_1_t560925241 * get_address_of__NativeEnumerator_5() { return &____NativeEnumerator_5; }
	inline void set__NativeEnumerator_5(Nullable_1_t560925241  value)
	{
		____NativeEnumerator_5 = value;
	}

	inline static int32_t get_offset_of__NativeArrayEnumerator_6() { return static_cast<int32_t>(offsetof(JsTypeAttribute_t3342267407, ____NativeArrayEnumerator_6)); }
	inline Nullable_1_t560925241  get__NativeArrayEnumerator_6() const { return ____NativeArrayEnumerator_6; }
	inline Nullable_1_t560925241 * get_address_of__NativeArrayEnumerator_6() { return &____NativeArrayEnumerator_6; }
	inline void set__NativeArrayEnumerator_6(Nullable_1_t560925241  value)
	{
		____NativeArrayEnumerator_6 = value;
	}

	inline static int32_t get_offset_of__NativeConstructors_7() { return static_cast<int32_t>(offsetof(JsTypeAttribute_t3342267407, ____NativeConstructors_7)); }
	inline Nullable_1_t560925241  get__NativeConstructors_7() const { return ____NativeConstructors_7; }
	inline Nullable_1_t560925241 * get_address_of__NativeConstructors_7() { return &____NativeConstructors_7; }
	inline void set__NativeConstructors_7(Nullable_1_t560925241  value)
	{
		____NativeConstructors_7 = value;
	}

	inline static int32_t get_offset_of__NativeOverloads_8() { return static_cast<int32_t>(offsetof(JsTypeAttribute_t3342267407, ____NativeOverloads_8)); }
	inline Nullable_1_t560925241  get__NativeOverloads_8() const { return ____NativeOverloads_8; }
	inline Nullable_1_t560925241 * get_address_of__NativeOverloads_8() { return &____NativeOverloads_8; }
	inline void set__NativeOverloads_8(Nullable_1_t560925241  value)
	{
		____NativeOverloads_8 = value;
	}

	inline static int32_t get_offset_of__NativeOperatorOverloads_9() { return static_cast<int32_t>(offsetof(JsTypeAttribute_t3342267407, ____NativeOperatorOverloads_9)); }
	inline Nullable_1_t560925241  get__NativeOperatorOverloads_9() const { return ____NativeOperatorOverloads_9; }
	inline Nullable_1_t560925241 * get_address_of__NativeOperatorOverloads_9() { return &____NativeOperatorOverloads_9; }
	inline void set__NativeOperatorOverloads_9(Nullable_1_t560925241  value)
	{
		____NativeOperatorOverloads_9 = value;
	}

	inline static int32_t get_offset_of__Native_10() { return static_cast<int32_t>(offsetof(JsTypeAttribute_t3342267407, ____Native_10)); }
	inline Nullable_1_t560925241  get__Native_10() const { return ____Native_10; }
	inline Nullable_1_t560925241 * get_address_of__Native_10() { return &____Native_10; }
	inline void set__Native_10(Nullable_1_t560925241  value)
	{
		____Native_10 = value;
	}

	inline static int32_t get_offset_of__GlobalObject_11() { return static_cast<int32_t>(offsetof(JsTypeAttribute_t3342267407, ____GlobalObject_11)); }
	inline Nullable_1_t560925241  get__GlobalObject_11() const { return ____GlobalObject_11; }
	inline Nullable_1_t560925241 * get_address_of__GlobalObject_11() { return &____GlobalObject_11; }
	inline void set__GlobalObject_11(Nullable_1_t560925241  value)
	{
		____GlobalObject_11 = value;
	}

	inline static int32_t get_offset_of__NativeFunctions_12() { return static_cast<int32_t>(offsetof(JsTypeAttribute_t3342267407, ____NativeFunctions_12)); }
	inline Nullable_1_t560925241  get__NativeFunctions_12() const { return ____NativeFunctions_12; }
	inline Nullable_1_t560925241 * get_address_of__NativeFunctions_12() { return &____NativeFunctions_12; }
	inline void set__NativeFunctions_12(Nullable_1_t560925241  value)
	{
		____NativeFunctions_12 = value;
	}

	inline static int32_t get_offset_of__NativeJsons_13() { return static_cast<int32_t>(offsetof(JsTypeAttribute_t3342267407, ____NativeJsons_13)); }
	inline Nullable_1_t560925241  get__NativeJsons_13() const { return ____NativeJsons_13; }
	inline Nullable_1_t560925241 * get_address_of__NativeJsons_13() { return &____NativeJsons_13; }
	inline void set__NativeJsons_13(Nullable_1_t560925241  value)
	{
		____NativeJsons_13 = value;
	}

	inline static int32_t get_offset_of__Export_14() { return static_cast<int32_t>(offsetof(JsTypeAttribute_t3342267407, ____Export_14)); }
	inline Nullable_1_t560925241  get__Export_14() const { return ____Export_14; }
	inline Nullable_1_t560925241 * get_address_of__Export_14() { return &____Export_14; }
	inline void set__Export_14(Nullable_1_t560925241  value)
	{
		____Export_14 = value;
	}

	inline static int32_t get_offset_of__IgnoreGenericTypeArguments_15() { return static_cast<int32_t>(offsetof(JsTypeAttribute_t3342267407, ____IgnoreGenericTypeArguments_15)); }
	inline Nullable_1_t560925241  get__IgnoreGenericTypeArguments_15() const { return ____IgnoreGenericTypeArguments_15; }
	inline Nullable_1_t560925241 * get_address_of__IgnoreGenericTypeArguments_15() { return &____IgnoreGenericTypeArguments_15; }
	inline void set__IgnoreGenericTypeArguments_15(Nullable_1_t560925241  value)
	{
		____IgnoreGenericTypeArguments_15 = value;
	}

	inline static int32_t get_offset_of__IgnoreGenericMethodArguments_16() { return static_cast<int32_t>(offsetof(JsTypeAttribute_t3342267407, ____IgnoreGenericMethodArguments_16)); }
	inline Nullable_1_t560925241  get__IgnoreGenericMethodArguments_16() const { return ____IgnoreGenericMethodArguments_16; }
	inline Nullable_1_t560925241 * get_address_of__IgnoreGenericMethodArguments_16() { return &____IgnoreGenericMethodArguments_16; }
	inline void set__IgnoreGenericMethodArguments_16(Nullable_1_t560925241  value)
	{
		____IgnoreGenericMethodArguments_16 = value;
	}

	inline static int32_t get_offset_of__Mode_17() { return static_cast<int32_t>(offsetof(JsTypeAttribute_t3342267407, ____Mode_17)); }
	inline Nullable_1_t3164635835  get__Mode_17() const { return ____Mode_17; }
	inline Nullable_1_t3164635835 * get_address_of__Mode_17() { return &____Mode_17; }
	inline void set__Mode_17(Nullable_1_t3164635835  value)
	{
		____Mode_17 = value;
	}

	inline static int32_t get_offset_of__InlineFields_18() { return static_cast<int32_t>(offsetof(JsTypeAttribute_t3342267407, ____InlineFields_18)); }
	inline Nullable_1_t560925241  get__InlineFields_18() const { return ____InlineFields_18; }
	inline Nullable_1_t560925241 * get_address_of__InlineFields_18() { return &____InlineFields_18; }
	inline void set__InlineFields_18(Nullable_1_t560925241  value)
	{
		____InlineFields_18 = value;
	}

	inline static int32_t get_offset_of__OmitDefaultConstructor_19() { return static_cast<int32_t>(offsetof(JsTypeAttribute_t3342267407, ____OmitDefaultConstructor_19)); }
	inline Nullable_1_t560925241  get__OmitDefaultConstructor_19() const { return ____OmitDefaultConstructor_19; }
	inline Nullable_1_t560925241 * get_address_of__OmitDefaultConstructor_19() { return &____OmitDefaultConstructor_19; }
	inline void set__OmitDefaultConstructor_19(Nullable_1_t560925241  value)
	{
		____OmitDefaultConstructor_19 = value;
	}

	inline static int32_t get_offset_of__NativeCasts_20() { return static_cast<int32_t>(offsetof(JsTypeAttribute_t3342267407, ____NativeCasts_20)); }
	inline Nullable_1_t560925241  get__NativeCasts_20() const { return ____NativeCasts_20; }
	inline Nullable_1_t560925241 * get_address_of__NativeCasts_20() { return &____NativeCasts_20; }
	inline void set__NativeCasts_20(Nullable_1_t560925241  value)
	{
		____NativeCasts_20 = value;
	}

	inline static int32_t get_offset_of_U3CTargetTypeU3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(JsTypeAttribute_t3342267407, ___U3CTargetTypeU3Ek__BackingField_21)); }
	inline Type_t * get_U3CTargetTypeU3Ek__BackingField_21() const { return ___U3CTargetTypeU3Ek__BackingField_21; }
	inline Type_t ** get_address_of_U3CTargetTypeU3Ek__BackingField_21() { return &___U3CTargetTypeU3Ek__BackingField_21; }
	inline void set_U3CTargetTypeU3Ek__BackingField_21(Type_t * value)
	{
		___U3CTargetTypeU3Ek__BackingField_21 = value;
		Il2CppCodeGenWriteBarrier(&___U3CTargetTypeU3Ek__BackingField_21, value);
	}

	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_22() { return static_cast<int32_t>(offsetof(JsTypeAttribute_t3342267407, ___U3CNameU3Ek__BackingField_22)); }
	inline String_t* get_U3CNameU3Ek__BackingField_22() const { return ___U3CNameU3Ek__BackingField_22; }
	inline String_t** get_address_of_U3CNameU3Ek__BackingField_22() { return &___U3CNameU3Ek__BackingField_22; }
	inline void set_U3CNameU3Ek__BackingField_22(String_t* value)
	{
		___U3CNameU3Ek__BackingField_22 = value;
		Il2CppCodeGenWriteBarrier(&___U3CNameU3Ek__BackingField_22, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
