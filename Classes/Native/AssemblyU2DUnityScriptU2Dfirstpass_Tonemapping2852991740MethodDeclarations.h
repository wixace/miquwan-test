﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Tonemapping
struct Tonemapping_t2852991740;
// UnityEngine.RenderTexture
struct RenderTexture_t1963041563;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture1963041563.h"

// System.Void Tonemapping::.ctor()
extern "C"  void Tonemapping__ctor_m2071450086 (Tonemapping_t2852991740 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Tonemapping::CheckResources()
extern "C"  bool Tonemapping_CheckResources_m1355503221 (Tonemapping_t2852991740 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Tonemapping::UpdateCurve()
extern "C"  float Tonemapping_UpdateCurve_m1290119414 (Tonemapping_t2852991740 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Tonemapping::OnDisable()
extern "C"  void Tonemapping_OnDisable_m2211955533 (Tonemapping_t2852991740 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Tonemapping::CreateInternalRenderTexture()
extern "C"  bool Tonemapping_CreateInternalRenderTexture_m72730614 (Tonemapping_t2852991740 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Tonemapping::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void Tonemapping_OnRenderImage_m428819128 (Tonemapping_t2852991740 * __this, RenderTexture_t1963041563 * ___source0, RenderTexture_t1963041563 * ___destination1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Tonemapping::Main()
extern "C"  void Tonemapping_Main_m2891809303 (Tonemapping_t2852991740 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
