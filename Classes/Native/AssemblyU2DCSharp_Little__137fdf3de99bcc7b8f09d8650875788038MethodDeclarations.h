﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._137fdf3de99bcc7b8f09d8650229aef0
struct _137fdf3de99bcc7b8f09d8650229aef0_t875788038;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._137fdf3de99bcc7b8f09d8650229aef0::.ctor()
extern "C"  void _137fdf3de99bcc7b8f09d8650229aef0__ctor_m1849570791 (_137fdf3de99bcc7b8f09d8650229aef0_t875788038 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._137fdf3de99bcc7b8f09d8650229aef0::_137fdf3de99bcc7b8f09d8650229aef0m2(System.Int32)
extern "C"  int32_t _137fdf3de99bcc7b8f09d8650229aef0__137fdf3de99bcc7b8f09d8650229aef0m2_m3231876057 (_137fdf3de99bcc7b8f09d8650229aef0_t875788038 * __this, int32_t ____137fdf3de99bcc7b8f09d8650229aef0a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._137fdf3de99bcc7b8f09d8650229aef0::_137fdf3de99bcc7b8f09d8650229aef0m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _137fdf3de99bcc7b8f09d8650229aef0__137fdf3de99bcc7b8f09d8650229aef0m_m3831180029 (_137fdf3de99bcc7b8f09d8650229aef0_t875788038 * __this, int32_t ____137fdf3de99bcc7b8f09d8650229aef0a0, int32_t ____137fdf3de99bcc7b8f09d8650229aef0811, int32_t ____137fdf3de99bcc7b8f09d8650229aef0c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
