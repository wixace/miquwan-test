﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GameObject_HierarchyGenerated
struct GameObject_HierarchyGenerated_t2924973064;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"

// System.Void GameObject_HierarchyGenerated::.ctor()
extern "C"  void GameObject_HierarchyGenerated__ctor_m632098403 (GameObject_HierarchyGenerated_t2924973064 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameObject_HierarchyGenerated::GameObject_Hierarchy_ChangeParent__GameObject__GameObject__Boolean(JSVCall,System.Int32)
extern "C"  bool GameObject_HierarchyGenerated_GameObject_Hierarchy_ChangeParent__GameObject__GameObject__Boolean_m2084048945 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameObject_HierarchyGenerated::__Register()
extern "C"  void GameObject_HierarchyGenerated___Register_m1356657732 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GameObject_HierarchyGenerated::ilo_getObject1(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * GameObject_HierarchyGenerated_ilo_getObject1_m3841598178 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameObject_HierarchyGenerated::ilo_ChangeParent2(UnityEngine.GameObject,UnityEngine.GameObject,System.Boolean)
extern "C"  void GameObject_HierarchyGenerated_ilo_ChangeParent2_m3479416477 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ____this0, GameObject_t3674682005 * ___parent1, bool ___resetMat2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
