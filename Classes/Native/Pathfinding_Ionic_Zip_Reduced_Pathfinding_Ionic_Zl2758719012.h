﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.Ionic.Zlib.Adler
struct  Adler_t2758719012  : public Il2CppObject
{
public:

public:
};

struct Adler_t2758719012_StaticFields
{
public:
	// System.UInt32 Pathfinding.Ionic.Zlib.Adler::BASE
	uint32_t ___BASE_0;
	// System.Int32 Pathfinding.Ionic.Zlib.Adler::NMAX
	int32_t ___NMAX_1;

public:
	inline static int32_t get_offset_of_BASE_0() { return static_cast<int32_t>(offsetof(Adler_t2758719012_StaticFields, ___BASE_0)); }
	inline uint32_t get_BASE_0() const { return ___BASE_0; }
	inline uint32_t* get_address_of_BASE_0() { return &___BASE_0; }
	inline void set_BASE_0(uint32_t value)
	{
		___BASE_0 = value;
	}

	inline static int32_t get_offset_of_NMAX_1() { return static_cast<int32_t>(offsetof(Adler_t2758719012_StaticFields, ___NMAX_1)); }
	inline int32_t get_NMAX_1() const { return ___NMAX_1; }
	inline int32_t* get_address_of_NMAX_1() { return &___NMAX_1; }
	inline void set_NMAX_1(int32_t value)
	{
		___NMAX_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
