﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_519615340MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<PushType,PluginPush/NotificationData>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m2239997168(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t3933281296 *, int32_t, NotificationData_t3289515031 *, const MethodInfo*))KeyValuePair_2__ctor_m1781480841_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<PushType,PluginPush/NotificationData>::get_Key()
#define KeyValuePair_2_get_Key_m2379503864(__this, method) ((  int32_t (*) (KeyValuePair_2_t3933281296 *, const MethodInfo*))KeyValuePair_2_get_Key_m1078751295_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<PushType,PluginPush/NotificationData>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m754212665(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3933281296 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m2104494848_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<PushType,PluginPush/NotificationData>::get_Value()
#define KeyValuePair_2_get_Value_m241591224(__this, method) ((  NotificationData_t3289515031 * (*) (KeyValuePair_2_t3933281296 *, const MethodInfo*))KeyValuePair_2_get_Value_m1445662143_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<PushType,PluginPush/NotificationData>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m3307957945(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3933281296 *, NotificationData_t3289515031 *, const MethodInfo*))KeyValuePair_2_set_Value_m3570261760_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<PushType,PluginPush/NotificationData>::ToString()
#define KeyValuePair_2_ToString_m3750695817(__this, method) ((  String_t* (*) (KeyValuePair_2_t3933281296 *, const MethodInfo*))KeyValuePair_2_ToString_m1871991778_gshared)(__this, method)
