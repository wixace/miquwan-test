﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIEventListenerGenerated/<UIEventListener_onDragStart_GetDelegate_member8_arg0>c__AnonStoreyBC
struct U3CUIEventListener_onDragStart_GetDelegate_member8_arg0U3Ec__AnonStoreyBC_t1370864928;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"

// System.Void UIEventListenerGenerated/<UIEventListener_onDragStart_GetDelegate_member8_arg0>c__AnonStoreyBC::.ctor()
extern "C"  void U3CUIEventListener_onDragStart_GetDelegate_member8_arg0U3Ec__AnonStoreyBC__ctor_m2850743163 (U3CUIEventListener_onDragStart_GetDelegate_member8_arg0U3Ec__AnonStoreyBC_t1370864928 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIEventListenerGenerated/<UIEventListener_onDragStart_GetDelegate_member8_arg0>c__AnonStoreyBC::<>m__13C(UnityEngine.GameObject)
extern "C"  void U3CUIEventListener_onDragStart_GetDelegate_member8_arg0U3Ec__AnonStoreyBC_U3CU3Em__13C_m2490023735 (U3CUIEventListener_onDragStart_GetDelegate_member8_arg0U3Ec__AnonStoreyBC_t1370864928 * __this, GameObject_t3674682005 * ___go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
