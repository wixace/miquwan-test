﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3056204655MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.UInt16,System.String>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m1654568851(__this, ___dictionary0, method) ((  void (*) (Enumerator_t3187587137 *, Dictionary_2_t1870263745 *, const MethodInfo*))Enumerator__ctor_m3840966209_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.UInt16,System.String>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m211740526(__this, method) ((  Il2CppObject * (*) (Enumerator_t3187587137 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2950322816_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.UInt16,System.String>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2518803266(__this, method) ((  void (*) (Enumerator_t3187587137 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1533905876_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.UInt16,System.String>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1605566027(__this, method) ((  DictionaryEntry_t1751606614  (*) (Enumerator_t3187587137 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3264954205_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.UInt16,System.String>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3859962314(__this, method) ((  Il2CppObject * (*) (Enumerator_t3187587137 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m366720860_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.UInt16,System.String>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3915443676(__this, method) ((  Il2CppObject * (*) (Enumerator_t3187587137 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1279864558_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.UInt16,System.String>::MoveNext()
#define Enumerator_MoveNext_m629096238(__this, method) ((  bool (*) (Enumerator_t3187587137 *, const MethodInfo*))Enumerator_MoveNext_m2899515072_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.UInt16,System.String>::get_Current()
#define Enumerator_get_Current_m529849410(__this, method) ((  KeyValuePair_2_t1769044451  (*) (Enumerator_t3187587137 *, const MethodInfo*))Enumerator_get_Current_m1432355696_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.UInt16,System.String>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m564240507(__this, method) ((  uint16_t (*) (Enumerator_t3187587137 *, const MethodInfo*))Enumerator_get_CurrentKey_m633733773_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.UInt16,System.String>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m978948703(__this, method) ((  String_t* (*) (Enumerator_t3187587137 *, const MethodInfo*))Enumerator_get_CurrentValue_m3337467889_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.UInt16,System.String>::Reset()
#define Enumerator_Reset_m3535250789(__this, method) ((  void (*) (Enumerator_t3187587137 *, const MethodInfo*))Enumerator_Reset_m1828931347_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.UInt16,System.String>::VerifyState()
#define Enumerator_VerifyState_m579900846(__this, method) ((  void (*) (Enumerator_t3187587137 *, const MethodInfo*))Enumerator_VerifyState_m1482407132_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.UInt16,System.String>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m1683836310(__this, method) ((  void (*) (Enumerator_t3187587137 *, const MethodInfo*))Enumerator_VerifyCurrent_m1408983364_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.UInt16,System.String>::Dispose()
#define Enumerator_Dispose_m2580306165(__this, method) ((  void (*) (Enumerator_t3187587137 *, const MethodInfo*))Enumerator_Dispose_m3484829475_gshared)(__this, method)
