﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21209901952.h"
#include "AssemblyU2DCSharp_Entity_Behavior_EBehaviorID1238533136.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System.Collections.Generic.KeyValuePair`2<Entity.Behavior.EBehaviorID,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m907882396_gshared (KeyValuePair_2_t1209901952 * __this, uint8_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define KeyValuePair_2__ctor_m907882396(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t1209901952 *, uint8_t, Il2CppObject *, const MethodInfo*))KeyValuePair_2__ctor_m907882396_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<Entity.Behavior.EBehaviorID,System.Object>::get_Key()
extern "C"  uint8_t KeyValuePair_2_get_Key_m3205006412_gshared (KeyValuePair_2_t1209901952 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m3205006412(__this, method) ((  uint8_t (*) (KeyValuePair_2_t1209901952 *, const MethodInfo*))KeyValuePair_2_get_Key_m3205006412_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Entity.Behavior.EBehaviorID,System.Object>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m614586637_gshared (KeyValuePair_2_t1209901952 * __this, uint8_t ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m614586637(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t1209901952 *, uint8_t, const MethodInfo*))KeyValuePair_2_set_Key_m614586637_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<Entity.Behavior.EBehaviorID,System.Object>::get_Value()
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m1857364592_gshared (KeyValuePair_2_t1209901952 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m1857364592(__this, method) ((  Il2CppObject * (*) (KeyValuePair_2_t1209901952 *, const MethodInfo*))KeyValuePair_2_get_Value_m1857364592_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Entity.Behavior.EBehaviorID,System.Object>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m3542081165_gshared (KeyValuePair_2_t1209901952 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m3542081165(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t1209901952 *, Il2CppObject *, const MethodInfo*))KeyValuePair_2_set_Value_m3542081165_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<Entity.Behavior.EBehaviorID,System.Object>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m3384335323_gshared (KeyValuePair_2_t1209901952 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m3384335323(__this, method) ((  String_t* (*) (KeyValuePair_2_t1209901952 *, const MethodInfo*))KeyValuePair_2_ToString_m3384335323_gshared)(__this, method)
