﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_CursorGenerated
struct UnityEngine_CursorGenerated_t3476422769;
// JSVCall
struct JSVCall_t3708497963;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"

// System.Void UnityEngine_CursorGenerated::.ctor()
extern "C"  void UnityEngine_CursorGenerated__ctor_m1122867738 (UnityEngine_CursorGenerated_t3476422769 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_CursorGenerated::Cursor_Cursor1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_CursorGenerated_Cursor_Cursor1_m2004836376 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CursorGenerated::Cursor_visible(JSVCall)
extern "C"  void UnityEngine_CursorGenerated_Cursor_visible_m3634753364 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CursorGenerated::Cursor_lockState(JSVCall)
extern "C"  void UnityEngine_CursorGenerated_Cursor_lockState_m2617997344 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_CursorGenerated::Cursor_SetCursor__Texture2D__Vector2__CursorMode(JSVCall,System.Int32)
extern "C"  bool UnityEngine_CursorGenerated_Cursor_SetCursor__Texture2D__Vector2__CursorMode_m1327194672 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CursorGenerated::__Register()
extern "C"  void UnityEngine_CursorGenerated___Register_m3850511085 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_CursorGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UnityEngine_CursorGenerated_ilo_getObject1_m3848233308 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
