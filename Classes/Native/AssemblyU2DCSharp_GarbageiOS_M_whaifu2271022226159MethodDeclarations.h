﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_whaifu227
struct M_whaifu227_t1022226159;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_whaifu2271022226159.h"

// System.Void GarbageiOS.M_whaifu227::.ctor()
extern "C"  void M_whaifu227__ctor_m2314440532 (M_whaifu227_t1022226159 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_whaifu227::M_saynu0(System.String[],System.Int32)
extern "C"  void M_whaifu227_M_saynu0_m3479027383 (M_whaifu227_t1022226159 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_whaifu227::M_gomaMowhall1(System.String[],System.Int32)
extern "C"  void M_whaifu227_M_gomaMowhall1_m2759433336 (M_whaifu227_t1022226159 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_whaifu227::ilo_M_saynu01(GarbageiOS.M_whaifu227,System.String[],System.Int32)
extern "C"  void M_whaifu227_ilo_M_saynu01_m1396626212 (Il2CppObject * __this /* static, unused */, M_whaifu227_t1022226159 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
