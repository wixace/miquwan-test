﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Collections.Generic.IEnumerable`1<System.Boolean>
struct IEnumerable_1_t3777711675;
// System.Collections.Generic.IEnumerator`1<System.Boolean>
struct IEnumerator_1_t2388663767;
// Pathfinding.Poly2Tri.FixedBitArray3
struct FixedBitArray3_t3665369095;
struct FixedBitArray3_t3665369095_marshaled_pinvoke;
struct FixedBitArray3_t3665369095_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "Pathfinding_Poly2Tri_Pathfinding_Poly2Tri_FixedBit3665369095.h"

// System.Collections.IEnumerator Pathfinding.Poly2Tri.FixedBitArray3::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * FixedBitArray3_System_Collections_IEnumerable_GetEnumerator_m2166890663 (FixedBitArray3_t3665369095 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.Poly2Tri.FixedBitArray3::get_Item(System.Int32)
extern "C"  bool FixedBitArray3_get_Item_m141500035 (FixedBitArray3_t3665369095 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Poly2Tri.FixedBitArray3::set_Item(System.Int32,System.Boolean)
extern "C"  void FixedBitArray3_set_Item_m2925852176 (FixedBitArray3_t3665369095 * __this, int32_t ___index0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Poly2Tri.FixedBitArray3::Clear()
extern "C"  void FixedBitArray3_Clear_m4005825347 (FixedBitArray3_t3665369095 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<System.Boolean> Pathfinding.Poly2Tri.FixedBitArray3::Enumerate()
extern "C"  Il2CppObject* FixedBitArray3_Enumerate_m3597532749 (FixedBitArray3_t3665369095 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<System.Boolean> Pathfinding.Poly2Tri.FixedBitArray3::GetEnumerator()
extern "C"  Il2CppObject* FixedBitArray3_GetEnumerator_m1379323559 (FixedBitArray3_t3665369095 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct FixedBitArray3_t3665369095;
struct FixedBitArray3_t3665369095_marshaled_pinvoke;

extern "C" void FixedBitArray3_t3665369095_marshal_pinvoke(const FixedBitArray3_t3665369095& unmarshaled, FixedBitArray3_t3665369095_marshaled_pinvoke& marshaled);
extern "C" void FixedBitArray3_t3665369095_marshal_pinvoke_back(const FixedBitArray3_t3665369095_marshaled_pinvoke& marshaled, FixedBitArray3_t3665369095& unmarshaled);
extern "C" void FixedBitArray3_t3665369095_marshal_pinvoke_cleanup(FixedBitArray3_t3665369095_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct FixedBitArray3_t3665369095;
struct FixedBitArray3_t3665369095_marshaled_com;

extern "C" void FixedBitArray3_t3665369095_marshal_com(const FixedBitArray3_t3665369095& unmarshaled, FixedBitArray3_t3665369095_marshaled_com& marshaled);
extern "C" void FixedBitArray3_t3665369095_marshal_com_back(const FixedBitArray3_t3665369095_marshaled_com& marshaled, FixedBitArray3_t3665369095& unmarshaled);
extern "C" void FixedBitArray3_t3665369095_marshal_com_cleanup(FixedBitArray3_t3665369095_marshaled_com& marshaled);
