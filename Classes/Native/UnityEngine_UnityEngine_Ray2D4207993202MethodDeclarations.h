﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;
// UnityEngine.Ray2D
struct Ray2D_t4207993202;
struct Ray2D_t4207993202_marshaled_pinvoke;
struct Ray2D_t4207993202_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Ray2D4207993202.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "mscorlib_System_String7231557.h"

// System.Void UnityEngine.Ray2D::.ctor(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  void Ray2D__ctor_m3747436081 (Ray2D_t4207993202 * __this, Vector2_t4282066565  ___origin0, Vector2_t4282066565  ___direction1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Ray2D::get_origin()
extern "C"  Vector2_t4282066565  Ray2D_get_origin_m2436519003 (Ray2D_t4207993202 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Ray2D::set_origin(UnityEngine.Vector2)
extern "C"  void Ray2D_set_origin_m1451430128 (Ray2D_t4207993202 * __this, Vector2_t4282066565  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Ray2D::get_direction()
extern "C"  Vector2_t4282066565  Ray2D_get_direction_m2376632972 (Ray2D_t4207993202 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Ray2D::set_direction(UnityEngine.Vector2)
extern "C"  void Ray2D_set_direction_m3664764573 (Ray2D_t4207993202 * __this, Vector2_t4282066565  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Ray2D::GetPoint(System.Single)
extern "C"  Vector2_t4282066565  Ray2D_GetPoint_m617747269 (Ray2D_t4207993202 * __this, float ___distance0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Ray2D::ToString()
extern "C"  String_t* Ray2D_ToString_m3464444208 (Ray2D_t4207993202 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Ray2D::ToString(System.String)
extern "C"  String_t* Ray2D_ToString_m2310853586 (Ray2D_t4207993202 * __this, String_t* ___format0, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct Ray2D_t4207993202;
struct Ray2D_t4207993202_marshaled_pinvoke;

extern "C" void Ray2D_t4207993202_marshal_pinvoke(const Ray2D_t4207993202& unmarshaled, Ray2D_t4207993202_marshaled_pinvoke& marshaled);
extern "C" void Ray2D_t4207993202_marshal_pinvoke_back(const Ray2D_t4207993202_marshaled_pinvoke& marshaled, Ray2D_t4207993202& unmarshaled);
extern "C" void Ray2D_t4207993202_marshal_pinvoke_cleanup(Ray2D_t4207993202_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct Ray2D_t4207993202;
struct Ray2D_t4207993202_marshaled_com;

extern "C" void Ray2D_t4207993202_marshal_com(const Ray2D_t4207993202& unmarshaled, Ray2D_t4207993202_marshaled_com& marshaled);
extern "C" void Ray2D_t4207993202_marshal_com_back(const Ray2D_t4207993202_marshaled_com& marshaled, Ray2D_t4207993202& unmarshaled);
extern "C" void Ray2D_t4207993202_marshal_com_cleanup(Ray2D_t4207993202_marshaled_com& marshaled);
