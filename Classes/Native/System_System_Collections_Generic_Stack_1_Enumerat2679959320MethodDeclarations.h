﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_Stack_1_Enumerat2532196025MethodDeclarations.h"

// System.Void System.Collections.Generic.Stack`1/Enumerator<Pathfinding.GraphNode>::.ctor(System.Collections.Generic.Stack`1<T>)
#define Enumerator__ctor_m413455941(__this, ___t0, method) ((  void (*) (Enumerator_t2679959320 *, Stack_1_t3122173294 *, const MethodInfo*))Enumerator__ctor_m1003414509_gshared)(__this, ___t0, method)
// System.Void System.Collections.Generic.Stack`1/Enumerator<Pathfinding.GraphNode>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2642923081(__this, method) ((  void (*) (Enumerator_t2679959320 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3054975473_gshared)(__this, method)
// System.Object System.Collections.Generic.Stack`1/Enumerator<Pathfinding.GraphNode>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3483915583(__this, method) ((  Il2CppObject * (*) (Enumerator_t2679959320 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2470832103_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1/Enumerator<Pathfinding.GraphNode>::Dispose()
#define Enumerator_Dispose_m3809108110(__this, method) ((  void (*) (Enumerator_t2679959320 *, const MethodInfo*))Enumerator_Dispose_m1634653158_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Stack`1/Enumerator<Pathfinding.GraphNode>::MoveNext()
#define Enumerator_MoveNext_m1713379213(__this, method) ((  bool (*) (Enumerator_t2679959320 *, const MethodInfo*))Enumerator_MoveNext_m3012756789_gshared)(__this, method)
// T System.Collections.Generic.Stack`1/Enumerator<Pathfinding.GraphNode>::get_Current()
#define Enumerator_get_Current_m2011896864(__this, method) ((  GraphNode_t23612370 * (*) (Enumerator_t2679959320 *, const MethodInfo*))Enumerator_get_Current_m2483819640_gshared)(__this, method)
