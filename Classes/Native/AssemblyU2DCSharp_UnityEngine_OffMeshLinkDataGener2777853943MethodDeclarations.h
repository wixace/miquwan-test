﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_OffMeshLinkDataGenerated
struct UnityEngine_OffMeshLinkDataGenerated_t2777853943;
// JSVCall
struct JSVCall_t3708497963;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void UnityEngine_OffMeshLinkDataGenerated::.ctor()
extern "C"  void UnityEngine_OffMeshLinkDataGenerated__ctor_m1259307140 (UnityEngine_OffMeshLinkDataGenerated_t2777853943 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_OffMeshLinkDataGenerated::.cctor()
extern "C"  void UnityEngine_OffMeshLinkDataGenerated__cctor_m4196686761 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_OffMeshLinkDataGenerated::OffMeshLinkData_OffMeshLinkData1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_OffMeshLinkDataGenerated_OffMeshLinkData_OffMeshLinkData1_m2134449140 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_OffMeshLinkDataGenerated::OffMeshLinkData_valid(JSVCall)
extern "C"  void UnityEngine_OffMeshLinkDataGenerated_OffMeshLinkData_valid_m3981217416 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_OffMeshLinkDataGenerated::OffMeshLinkData_activated(JSVCall)
extern "C"  void UnityEngine_OffMeshLinkDataGenerated_OffMeshLinkData_activated_m2598544723 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_OffMeshLinkDataGenerated::OffMeshLinkData_linkType(JSVCall)
extern "C"  void UnityEngine_OffMeshLinkDataGenerated_OffMeshLinkData_linkType_m303191924 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_OffMeshLinkDataGenerated::OffMeshLinkData_startPos(JSVCall)
extern "C"  void UnityEngine_OffMeshLinkDataGenerated_OffMeshLinkData_startPos_m4042580214 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_OffMeshLinkDataGenerated::OffMeshLinkData_endPos(JSVCall)
extern "C"  void UnityEngine_OffMeshLinkDataGenerated_OffMeshLinkData_endPos_m669597807 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_OffMeshLinkDataGenerated::OffMeshLinkData_offMeshLink(JSVCall)
extern "C"  void UnityEngine_OffMeshLinkDataGenerated_OffMeshLinkData_offMeshLink_m829036334 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_OffMeshLinkDataGenerated::__Register()
extern "C"  void UnityEngine_OffMeshLinkDataGenerated___Register_m4186025667 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_OffMeshLinkDataGenerated::ilo_setVector3S1(System.Int32,UnityEngine.Vector3)
extern "C"  void UnityEngine_OffMeshLinkDataGenerated_ilo_setVector3S1_m1646971715 (Il2CppObject * __this /* static, unused */, int32_t ___e0, Vector3_t4282066566  ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
