﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIToggleGenerated
struct UIToggleGenerated_t1331375975;
// JSVCall
struct JSVCall_t3708497963;
// UIToggle/Validate
struct Validate_t2927776861;
// CSRepresentedObject
struct CSRepresentedObject_t3994124630;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Delegate
struct Delegate_t3310234105;
// UIToggle
struct UIToggle_t688812808;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_CSRepresentedObject3994124630.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Delegate3310234105.h"

// System.Void UIToggleGenerated::.ctor()
extern "C"  void UIToggleGenerated__ctor_m606724580 (UIToggleGenerated_t1331375975 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIToggleGenerated::UIToggle_UIToggle1(JSVCall,System.Int32)
extern "C"  bool UIToggleGenerated_UIToggle_UIToggle1_m2637154318 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIToggleGenerated::UIToggle_list(JSVCall)
extern "C"  void UIToggleGenerated_UIToggle_list_m1090868592 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIToggleGenerated::UIToggle_current(JSVCall)
extern "C"  void UIToggleGenerated_UIToggle_current_m2122230885 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIToggleGenerated::UIToggle_group(JSVCall)
extern "C"  void UIToggleGenerated_UIToggle_group_m733866015 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIToggleGenerated::UIToggle_activeSprite(JSVCall)
extern "C"  void UIToggleGenerated_UIToggle_activeSprite_m3847799619 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIToggleGenerated::UIToggle_activeAnimation(JSVCall)
extern "C"  void UIToggleGenerated_UIToggle_activeAnimation_m4090820704 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIToggleGenerated::UIToggle_animator(JSVCall)
extern "C"  void UIToggleGenerated_UIToggle_animator_m1139259719 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIToggleGenerated::UIToggle_tween(JSVCall)
extern "C"  void UIToggleGenerated_UIToggle_tween_m3368141235 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIToggleGenerated::UIToggle_startsActive(JSVCall)
extern "C"  void UIToggleGenerated_UIToggle_startsActive_m3239712631 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIToggleGenerated::UIToggle_instantTween(JSVCall)
extern "C"  void UIToggleGenerated_UIToggle_instantTween_m2061386372 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIToggleGenerated::UIToggle_optionCanBeNone(JSVCall)
extern "C"  void UIToggleGenerated_UIToggle_optionCanBeNone_m1228308840 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIToggleGenerated::UIToggle_onChange(JSVCall)
extern "C"  void UIToggleGenerated_UIToggle_onChange_m2957046463 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIToggle/Validate UIToggleGenerated::UIToggle_validator_GetDelegate_member11_arg0(CSRepresentedObject)
extern "C"  Validate_t2927776861 * UIToggleGenerated_UIToggle_validator_GetDelegate_member11_arg0_m917530587 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIToggleGenerated::UIToggle_validator(JSVCall)
extern "C"  void UIToggleGenerated_UIToggle_validator_m2627712172 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIToggleGenerated::UIToggle_value(JSVCall)
extern "C"  void UIToggleGenerated_UIToggle_value_m4207705453 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIToggleGenerated::UIToggle_isColliderEnabled(JSVCall)
extern "C"  void UIToggleGenerated_UIToggle_isColliderEnabled_m2700361979 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIToggleGenerated::UIToggle_Set__Boolean(JSVCall,System.Int32)
extern "C"  bool UIToggleGenerated_UIToggle_Set__Boolean_m4143451299 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIToggleGenerated::UIToggle_GetActiveToggle__Int32(JSVCall,System.Int32)
extern "C"  bool UIToggleGenerated_UIToggle_GetActiveToggle__Int32_m3862294971 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIToggleGenerated::__Register()
extern "C"  void UIToggleGenerated___Register_m3935119715 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIToggle/Validate UIToggleGenerated::<UIToggle_validator>m__168()
extern "C"  Validate_t2927776861 * UIToggleGenerated_U3CUIToggle_validatorU3Em__168_m469937463 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UIToggleGenerated::ilo_getObject1(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UIToggleGenerated_ilo_getObject1_m3625900225 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIToggleGenerated::ilo_setObject2(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UIToggleGenerated_ilo_setObject2_m4069510155 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIToggleGenerated::ilo_getBooleanS3(System.Int32)
extern "C"  bool UIToggleGenerated_ilo_getBooleanS3_m2096575490 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIToggleGenerated::ilo_addJSFunCSDelegateRel4(System.Int32,System.Delegate)
extern "C"  void UIToggleGenerated_ilo_addJSFunCSDelegateRel4_m1046509105 (Il2CppObject * __this /* static, unused */, int32_t ___funID0, Delegate_t3310234105 * ___del1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIToggleGenerated::ilo_getInt325(System.Int32)
extern "C"  int32_t UIToggleGenerated_ilo_getInt325_m3815592459 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIToggle UIToggleGenerated::ilo_GetActiveToggle6(System.Int32)
extern "C"  UIToggle_t688812808 * UIToggleGenerated_ilo_GetActiveToggle6_m3767133397 (Il2CppObject * __this /* static, unused */, int32_t ___group0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIToggleGenerated::ilo_isFunctionS7(System.Int32)
extern "C"  bool UIToggleGenerated_ilo_isFunctionS7_m3092705334 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
