﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIEffect
struct UIEffect_t251031877;
// UIDrawCall
struct UIDrawCall_t913273974;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_UIEffect251031877.h"
#include "AssemblyU2DCSharp_UIDrawCall913273974.h"

// System.Void UIEffect::.ctor()
extern "C"  void UIEffect__ctor_m2969889526 (UIEffect_t251031877 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIEffect::Awake()
extern "C"  void UIEffect_Awake_m3207494745 (UIEffect_t251031877 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIEffect::Init()
extern "C"  void UIEffect_Init_m1294265502 (UIEffect_t251031877 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIEffect::LateUpdate()
extern "C"  void UIEffect_LateUpdate_m3155872029 (UIEffect_t251031877 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIEffect::SetRenderQueue()
extern "C"  void UIEffect_SetRenderQueue_m1197816935 (UIEffect_t251031877 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIEffect::ilo_Init1(UIEffect)
extern "C"  void UIEffect_ilo_Init1_m3668817981 (Il2CppObject * __this /* static, unused */, UIEffect_t251031877 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIEffect::ilo_SetRenderQueue2(UIEffect)
extern "C"  void UIEffect_ilo_SetRenderQueue2_m2378598677 (Il2CppObject * __this /* static, unused */, UIEffect_t251031877 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIEffect::ilo_get_renderQueue3(UIDrawCall)
extern "C"  int32_t UIEffect_ilo_get_renderQueue3_m962841850 (Il2CppObject * __this /* static, unused */, UIDrawCall_t913273974 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
