﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Common.UTF8Code
struct UTF8Code_t2349867786;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void Common.UTF8Code::.ctor()
extern "C"  void UTF8Code__ctor_m3391273706 (UTF8Code_t2349867786 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Common.UTF8Code::UTF8String(System.String)
extern "C"  String_t* UTF8Code_UTF8String_m651387433 (Il2CppObject * __this /* static, unused */, String_t* ___input0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
