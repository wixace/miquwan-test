﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._21ae0265da197c7542418ff4b04fc419
struct _21ae0265da197c7542418ff4b04fc419_t4150550812;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._21ae0265da197c7542418ff4b04fc419::.ctor()
extern "C"  void _21ae0265da197c7542418ff4b04fc419__ctor_m1623475345 (_21ae0265da197c7542418ff4b04fc419_t4150550812 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._21ae0265da197c7542418ff4b04fc419::_21ae0265da197c7542418ff4b04fc419m2(System.Int32)
extern "C"  int32_t _21ae0265da197c7542418ff4b04fc419__21ae0265da197c7542418ff4b04fc419m2_m2480970777 (_21ae0265da197c7542418ff4b04fc419_t4150550812 * __this, int32_t ____21ae0265da197c7542418ff4b04fc419a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._21ae0265da197c7542418ff4b04fc419::_21ae0265da197c7542418ff4b04fc419m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _21ae0265da197c7542418ff4b04fc419__21ae0265da197c7542418ff4b04fc419m_m1761401533 (_21ae0265da197c7542418ff4b04fc419_t4150550812 * __this, int32_t ____21ae0265da197c7542418ff4b04fc419a0, int32_t ____21ae0265da197c7542418ff4b04fc419501, int32_t ____21ae0265da197c7542418ff4b04fc419c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
