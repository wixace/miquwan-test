﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Comparison`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>
struct Comparison_1_t4091926221;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "UnityEngine_UnityEngine_Rendering_ReflectionProbeB1080597738.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void System.Comparison`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::.ctor(System.Object,System.IntPtr)
extern "C"  void Comparison_1__ctor_m1324859766_gshared (Comparison_1_t4091926221 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define Comparison_1__ctor_m1324859766(__this, ___object0, ___method1, method) ((  void (*) (Comparison_1_t4091926221 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Comparison_1__ctor_m1324859766_gshared)(__this, ___object0, ___method1, method)
// System.Int32 System.Comparison`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::Invoke(T,T)
extern "C"  int32_t Comparison_1_Invoke_m2174363714_gshared (Comparison_1_t4091926221 * __this, ReflectionProbeBlendInfo_t1080597738  ___x0, ReflectionProbeBlendInfo_t1080597738  ___y1, const MethodInfo* method);
#define Comparison_1_Invoke_m2174363714(__this, ___x0, ___y1, method) ((  int32_t (*) (Comparison_1_t4091926221 *, ReflectionProbeBlendInfo_t1080597738 , ReflectionProbeBlendInfo_t1080597738 , const MethodInfo*))Comparison_1_Invoke_m2174363714_gshared)(__this, ___x0, ___y1, method)
// System.IAsyncResult System.Comparison`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Comparison_1_BeginInvoke_m3012741515_gshared (Comparison_1_t4091926221 * __this, ReflectionProbeBlendInfo_t1080597738  ___x0, ReflectionProbeBlendInfo_t1080597738  ___y1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method);
#define Comparison_1_BeginInvoke_m3012741515(__this, ___x0, ___y1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Comparison_1_t4091926221 *, ReflectionProbeBlendInfo_t1080597738 , ReflectionProbeBlendInfo_t1080597738 , AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))Comparison_1_BeginInvoke_m3012741515_gshared)(__this, ___x0, ___y1, ___callback2, ___object3, method)
// System.Int32 System.Comparison`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t Comparison_1_EndInvoke_m1166363882_gshared (Comparison_1_t4091926221 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define Comparison_1_EndInvoke_m1166363882(__this, ___result0, method) ((  int32_t (*) (Comparison_1_t4091926221 *, Il2CppObject *, const MethodInfo*))Comparison_1_EndInvoke_m1166363882_gshared)(__this, ___result0, method)
