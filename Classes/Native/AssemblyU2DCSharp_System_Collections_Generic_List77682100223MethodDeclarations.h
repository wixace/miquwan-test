﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System_Collections_Generic_List77Generated/<ListA1_FindLastIndex__Int32__PredicateT1_T>c__AnonStorey8E
struct U3CListA1_FindLastIndex__Int32__PredicateT1_TU3Ec__AnonStorey8E_t682100223;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void System_Collections_Generic_List77Generated/<ListA1_FindLastIndex__Int32__PredicateT1_T>c__AnonStorey8E::.ctor()
extern "C"  void U3CListA1_FindLastIndex__Int32__PredicateT1_TU3Ec__AnonStorey8E__ctor_m3201928572 (U3CListA1_FindLastIndex__Int32__PredicateT1_TU3Ec__AnonStorey8E_t682100223 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System_Collections_Generic_List77Generated/<ListA1_FindLastIndex__Int32__PredicateT1_T>c__AnonStorey8E::<>m__B3()
extern "C"  Il2CppObject * U3CListA1_FindLastIndex__Int32__PredicateT1_TU3Ec__AnonStorey8E_U3CU3Em__B3_m3920890145 (U3CListA1_FindLastIndex__Int32__PredicateT1_TU3Ec__AnonStorey8E_t682100223 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
