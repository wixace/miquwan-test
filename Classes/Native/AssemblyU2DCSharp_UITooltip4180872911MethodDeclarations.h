﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UITooltip
struct UITooltip_t4180872911;
// System.String
struct String_t;
// UIWidget
struct UIWidget_t769069560;
// UILabel
struct UILabel_t291504320;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_UITooltip4180872911.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"
#include "AssemblyU2DCSharp_UIWidget769069560.h"
#include "AssemblyU2DCSharp_UILabel291504320.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"

// System.Void UITooltip::.ctor()
extern "C"  void UITooltip__ctor_m3101310844 (UITooltip_t4180872911 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UITooltip::get_isVisible()
extern "C"  bool UITooltip_get_isVisible_m3118396397 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITooltip::Awake()
extern "C"  void UITooltip_Awake_m3338916063 (UITooltip_t4180872911 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITooltip::OnDestroy()
extern "C"  void UITooltip_OnDestroy_m3393713781 (UITooltip_t4180872911 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITooltip::Start()
extern "C"  void UITooltip_Start_m2048448636 (UITooltip_t4180872911 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITooltip::Update()
extern "C"  void UITooltip_Update_m3378217745 (UITooltip_t4180872911 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITooltip::SetAlpha(System.Single)
extern "C"  void UITooltip_SetAlpha_m2402469447 (UITooltip_t4180872911 * __this, float ___val0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITooltip::SetText(System.String)
extern "C"  void UITooltip_SetText_m2521650777 (UITooltip_t4180872911 * __this, String_t* ___tooltipText0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITooltip::ShowText(System.String)
extern "C"  void UITooltip_ShowText_m3776738864 (Il2CppObject * __this /* static, unused */, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITooltip::Show(System.String)
extern "C"  void UITooltip_Show_m2641932573 (Il2CppObject * __this /* static, unused */, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITooltip::Hide()
extern "C"  void UITooltip_Hide_m1957831434 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITooltip::ilo_SetAlpha1(UITooltip,System.Single)
extern "C"  void UITooltip_ilo_SetAlpha1_m2141564812 (Il2CppObject * __this /* static, unused */, UITooltip_t4180872911 * ____this0, float ___val1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UITooltip::ilo_get_deltaTime2()
extern "C"  float UITooltip_ilo_get_deltaTime2_m443547909 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UITooltip::ilo_get_color3(UIWidget)
extern "C"  Color_t4194546905  UITooltip_ilo_get_color3_m1250550645 (Il2CppObject * __this /* static, unused */, UIWidget_t769069560 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITooltip::ilo_set_text4(UILabel,System.String)
extern "C"  void UITooltip_ilo_set_text4_m2406010205 (Il2CppObject * __this /* static, unused */, UILabel_t291504320 * ____this0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UITooltip::ilo_get_printedSize5(UILabel)
extern "C"  Vector2_t4282066565  UITooltip_ilo_get_printedSize5_m2132262721 (Il2CppObject * __this /* static, unused */, UILabel_t291504320 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITooltip::ilo_SetText6(UITooltip,System.String)
extern "C"  void UITooltip_ilo_SetText6_m587903133 (Il2CppObject * __this /* static, unused */, UITooltip_t4180872911 * ____this0, String_t* ___tooltipText1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
