﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIModelDisplayMgrGenerated
struct UIModelDisplayMgrGenerated_t1949809924;
// JSVCall
struct JSVCall_t3708497963;
// System.Action
struct Action_t3771233898;
// CSRepresentedObject
struct CSRepresentedObject_t3994124630;
// UIModelDisplayMgr
struct UIModelDisplayMgr_t1446490315;
// UIFBXDisplaySprite
struct UIFBXDisplaySprite_t2233392895;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_CSRepresentedObject3994124630.h"
#include "AssemblyU2DCSharp_UIModelDisplayMgr1446490315.h"
#include "AssemblyU2DCSharp_UIModelDisplayType1891752679.h"
#include "System_Core_System_Action3771233898.h"
#include "AssemblyU2DCSharp_UIFBXDisplaySprite2233392895.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void UIModelDisplayMgrGenerated::.ctor()
extern "C"  void UIModelDisplayMgrGenerated__ctor_m1256919063 (UIModelDisplayMgrGenerated_t1949809924 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIModelDisplayMgrGenerated::UIModelDisplayMgr_UIModelDisplayMgr1(JSVCall,System.Int32)
extern "C"  bool UIModelDisplayMgrGenerated_UIModelDisplayMgr_UIModelDisplayMgr1_m788352299 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIModelDisplayMgrGenerated::UIModelDisplayMgr_Instance(JSVCall)
extern "C"  void UIModelDisplayMgrGenerated_UIModelDisplayMgr_Instance_m3205636177 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Action UIModelDisplayMgrGenerated::UIModelDisplayMgr_AttachObject_GetDelegate_member0_arg2(CSRepresentedObject)
extern "C"  Action_t3771233898 * UIModelDisplayMgrGenerated_UIModelDisplayMgr_AttachObject_GetDelegate_member0_arg2_m1654062037 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIModelDisplayMgrGenerated::UIModelDisplayMgr_AttachObject__UIModelDisplayType__Int32__Action__Boolean(JSVCall,System.Int32)
extern "C"  bool UIModelDisplayMgrGenerated_UIModelDisplayMgr_AttachObject__UIModelDisplayType__Int32__Action__Boolean_m1576929364 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIModelDisplayMgrGenerated::UIModelDisplayMgr_BindUIFBXSprite__UIModelDisplayType__UIFBXDisplaySprite(JSVCall,System.Int32)
extern "C"  bool UIModelDisplayMgrGenerated_UIModelDisplayMgr_BindUIFBXSprite__UIModelDisplayType__UIFBXDisplaySprite_m1741852115 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIModelDisplayMgrGenerated::UIModelDisplayMgr_ChangeGaoliangShader__UIModelDisplayType__Int32__Boolean(JSVCall,System.Int32)
extern "C"  bool UIModelDisplayMgrGenerated_UIModelDisplayMgr_ChangeGaoliangShader__UIModelDisplayType__Int32__Boolean_m3584668291 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIModelDisplayMgrGenerated::UIModelDisplayMgr_Clear(JSVCall,System.Int32)
extern "C"  bool UIModelDisplayMgrGenerated_UIModelDisplayMgr_Clear_m3760939818 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIModelDisplayMgrGenerated::UIModelDisplayMgr_ClearModel(JSVCall,System.Int32)
extern "C"  bool UIModelDisplayMgrGenerated_UIModelDisplayMgr_ClearModel_m2274387169 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIModelDisplayMgrGenerated::UIModelDisplayMgr_GetCurModelAnimation__UIModelDisplayType__Int32(JSVCall,System.Int32)
extern "C"  bool UIModelDisplayMgrGenerated_UIModelDisplayMgr_GetCurModelAnimation__UIModelDisplayType__Int32_m869190367 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIModelDisplayMgrGenerated::UIModelDisplayMgr_GetModel__UIModelDisplayType__Int32(JSVCall,System.Int32)
extern "C"  bool UIModelDisplayMgrGenerated_UIModelDisplayMgr_GetModel__UIModelDisplayType__Int32_m3394781489 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIModelDisplayMgrGenerated::UIModelDisplayMgr_Init(JSVCall,System.Int32)
extern "C"  bool UIModelDisplayMgrGenerated_UIModelDisplayMgr_Init_m3900028181 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIModelDisplayMgrGenerated::UIModelDisplayMgr_IsThere__UIModelDisplayType__Int32(JSVCall,System.Int32)
extern "C"  bool UIModelDisplayMgrGenerated_UIModelDisplayMgr_IsThere__UIModelDisplayType__Int32_m1378437010 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIModelDisplayMgrGenerated::UIModelDisplayMgr_PlayAniamtion__UIModelDisplayType__Int32__String__Boolean(JSVCall,System.Int32)
extern "C"  bool UIModelDisplayMgrGenerated_UIModelDisplayMgr_PlayAniamtion__UIModelDisplayType__Int32__String__Boolean_m1364215317 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIModelDisplayMgrGenerated::UIModelDisplayMgr_PlayAniamtion__UIModelDisplayType__Int32__AniType(JSVCall,System.Int32)
extern "C"  bool UIModelDisplayMgrGenerated_UIModelDisplayMgr_PlayAniamtion__UIModelDisplayType__Int32__AniType_m2527082964 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIModelDisplayMgrGenerated::UIModelDisplayMgr_PlayMonsterAniamtion__UIModelDisplayType__Int32__AniType(JSVCall,System.Int32)
extern "C"  bool UIModelDisplayMgrGenerated_UIModelDisplayMgr_PlayMonsterAniamtion__UIModelDisplayType__Int32__AniType_m2370285866 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIModelDisplayMgrGenerated::UIModelDisplayMgr_RelationSpin__SpinWithMouse__UIModelDisplayType__Int32(JSVCall,System.Int32)
extern "C"  bool UIModelDisplayMgrGenerated_UIModelDisplayMgr_RelationSpin__SpinWithMouse__UIModelDisplayType__Int32_m1481951821 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIModelDisplayMgrGenerated::UIModelDisplayMgr_SetActive__UIModelDisplayType__Int32__Boolean(JSVCall,System.Int32)
extern "C"  bool UIModelDisplayMgrGenerated_UIModelDisplayMgr_SetActive__UIModelDisplayType__Int32__Boolean_m3438502918 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIModelDisplayMgrGenerated::UIModelDisplayMgr_SetTransform__Vector3__Vector3__UIModelDisplayType__Int32__Single(JSVCall,System.Int32)
extern "C"  bool UIModelDisplayMgrGenerated_UIModelDisplayMgr_SetTransform__Vector3__Vector3__UIModelDisplayType__Int32__Single_m3732436770 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIModelDisplayMgrGenerated::UIModelDisplayMgr_SetUIModelCameraFOV__UIModelDisplayType__Single(JSVCall,System.Int32)
extern "C"  bool UIModelDisplayMgrGenerated_UIModelDisplayMgr_SetUIModelCameraFOV__UIModelDisplayType__Single_m3362171777 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIModelDisplayMgrGenerated::UIModelDisplayMgr_SetUIModelDisplay__UIModelDisplayType__Boolean(JSVCall,System.Int32)
extern "C"  bool UIModelDisplayMgrGenerated_UIModelDisplayMgr_SetUIModelDisplay__UIModelDisplayType__Boolean_m215015991 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIModelDisplayMgrGenerated::UIModelDisplayMgr_UnbindUIFBXSprite__UIModelDisplayType(JSVCall,System.Int32)
extern "C"  bool UIModelDisplayMgrGenerated_UIModelDisplayMgr_UnbindUIFBXSprite__UIModelDisplayType_m2087820347 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIModelDisplayMgrGenerated::__Register()
extern "C"  void UIModelDisplayMgrGenerated___Register_m2858410768 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Action UIModelDisplayMgrGenerated::<UIModelDisplayMgr_AttachObject__UIModelDisplayType__Int32__Action__Boolean>m__153()
extern "C"  Action_t3771233898 * UIModelDisplayMgrGenerated_U3CUIModelDisplayMgr_AttachObject__UIModelDisplayType__Int32__Action__BooleanU3Em__153_m902244076 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Action UIModelDisplayMgrGenerated::<UIModelDisplayMgr_AttachObject__UIModelDisplayType__Int32__Action__Boolean>m__154()
extern "C"  Action_t3771233898 * UIModelDisplayMgrGenerated_U3CUIModelDisplayMgr_AttachObject__UIModelDisplayType__Int32__Action__BooleanU3Em__154_m902245037 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIModelDisplayMgrGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UIModelDisplayMgrGenerated_ilo_getObject1_m3638409691 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIModelDisplayMgrGenerated::ilo_AttachObject2(UIModelDisplayMgr,UIModelDisplayType,System.Int32,System.Action,System.Boolean)
extern "C"  void UIModelDisplayMgrGenerated_ilo_AttachObject2_m2616005751 (Il2CppObject * __this /* static, unused */, UIModelDisplayMgr_t1446490315 * ____this0, int32_t ___type1, int32_t ___id2, Action_t3771233898 * ___action3, bool ___isHero4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIModelDisplayMgrGenerated::ilo_BindUIFBXSprite3(UIModelDisplayMgr,UIModelDisplayType,UIFBXDisplaySprite)
extern "C"  bool UIModelDisplayMgrGenerated_ilo_BindUIFBXSprite3_m1400007384 (Il2CppObject * __this /* static, unused */, UIModelDisplayMgr_t1446490315 * ____this0, int32_t ___type1, UIFBXDisplaySprite_t2233392895 * ___sp2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIModelDisplayMgrGenerated::ilo_getEnum4(System.Int32)
extern "C"  int32_t UIModelDisplayMgrGenerated_ilo_getEnum4_m1035504732 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIModelDisplayMgrGenerated::ilo_ChangeGaoliangShader5(UIModelDisplayMgr,UIModelDisplayType,System.Int32,System.Boolean)
extern "C"  void UIModelDisplayMgrGenerated_ilo_ChangeGaoliangShader5_m3672011768 (Il2CppObject * __this /* static, unused */, UIModelDisplayMgr_t1446490315 * ____this0, int32_t ___type1, int32_t ___id2, bool ___isHero3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIModelDisplayMgrGenerated::ilo_ClearModel6(UIModelDisplayMgr)
extern "C"  void UIModelDisplayMgrGenerated_ilo_ClearModel6_m591200267 (Il2CppObject * __this /* static, unused */, UIModelDisplayMgr_t1446490315 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIModelDisplayMgrGenerated::ilo_getInt327(System.Int32)
extern "C"  int32_t UIModelDisplayMgrGenerated_ilo_getInt327_m973908452 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIModelDisplayMgrGenerated::ilo_IsThere8(UIModelDisplayMgr,UIModelDisplayType,System.Int32)
extern "C"  bool UIModelDisplayMgrGenerated_ilo_IsThere8_m798029549 (Il2CppObject * __this /* static, unused */, UIModelDisplayMgr_t1446490315 * ____this0, int32_t ___type1, int32_t ___id2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIModelDisplayMgrGenerated::ilo_PlayAniamtion9(UIModelDisplayMgr,UIModelDisplayType,System.Int32,System.String,System.Boolean)
extern "C"  void UIModelDisplayMgrGenerated_ilo_PlayAniamtion9_m3099402195 (Il2CppObject * __this /* static, unused */, UIModelDisplayMgr_t1446490315 * ____this0, int32_t ___type1, int32_t ___id2, String_t* ___ani3, bool ___isHero4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UIModelDisplayMgrGenerated::ilo_getObject10(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UIModelDisplayMgrGenerated_ilo_getObject10_m608412808 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UIModelDisplayMgrGenerated::ilo_getVector3S11(System.Int32)
extern "C"  Vector3_t4282066566  UIModelDisplayMgrGenerated_ilo_getVector3S11_m223488498 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UIModelDisplayMgrGenerated::ilo_getSingle12(System.Int32)
extern "C"  float UIModelDisplayMgrGenerated_ilo_getSingle12_m2119550886 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Action UIModelDisplayMgrGenerated::ilo_UIModelDisplayMgr_AttachObject_GetDelegate_member0_arg213(CSRepresentedObject)
extern "C"  Action_t3771233898 * UIModelDisplayMgrGenerated_ilo_UIModelDisplayMgr_AttachObject_GetDelegate_member0_arg213_m1506483302 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIModelDisplayMgrGenerated::ilo_isFunctionS14(System.Int32)
extern "C"  bool UIModelDisplayMgrGenerated_ilo_isFunctionS14_m3852426291 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CSRepresentedObject UIModelDisplayMgrGenerated::ilo_getFunctionS15(System.Int32)
extern "C"  CSRepresentedObject_t3994124630 * UIModelDisplayMgrGenerated_ilo_getFunctionS15_m2763755799 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
