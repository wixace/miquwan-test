﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.ConfigurableJoint
struct ConfigurableJoint_t2962812103;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_ConfigurableJointMotion3577926941.h"
#include "UnityEngine_UnityEngine_SoftJointLimitSpring4258895628.h"
#include "UnityEngine_UnityEngine_SoftJointLimit2747465311.h"
#include "UnityEngine_UnityEngine_JointDrive1303722980.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882.h"
#include "UnityEngine_UnityEngine_RotationDriveMode1324228709.h"
#include "UnityEngine_UnityEngine_JointProjectionMode2993315186.h"

// System.Void UnityEngine.ConfigurableJoint::.ctor()
extern "C"  void ConfigurableJoint__ctor_m1002638632 (ConfigurableJoint_t2962812103 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.ConfigurableJoint::get_secondaryAxis()
extern "C"  Vector3_t4282066566  ConfigurableJoint_get_secondaryAxis_m605936108 (ConfigurableJoint_t2962812103 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ConfigurableJoint::set_secondaryAxis(UnityEngine.Vector3)
extern "C"  void ConfigurableJoint_set_secondaryAxis_m1662038043 (ConfigurableJoint_t2962812103 * __this, Vector3_t4282066566  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ConfigurableJoint::INTERNAL_get_secondaryAxis(UnityEngine.Vector3&)
extern "C"  void ConfigurableJoint_INTERNAL_get_secondaryAxis_m1426740057 (ConfigurableJoint_t2962812103 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ConfigurableJoint::INTERNAL_set_secondaryAxis(UnityEngine.Vector3&)
extern "C"  void ConfigurableJoint_INTERNAL_set_secondaryAxis_m3239438949 (ConfigurableJoint_t2962812103 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ConfigurableJointMotion UnityEngine.ConfigurableJoint::get_xMotion()
extern "C"  int32_t ConfigurableJoint_get_xMotion_m1504386574 (ConfigurableJoint_t2962812103 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ConfigurableJoint::set_xMotion(UnityEngine.ConfigurableJointMotion)
extern "C"  void ConfigurableJoint_set_xMotion_m3380684011 (ConfigurableJoint_t2962812103 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ConfigurableJointMotion UnityEngine.ConfigurableJoint::get_yMotion()
extern "C"  int32_t ConfigurableJoint_get_yMotion_m3991899407 (ConfigurableJoint_t2962812103 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ConfigurableJoint::set_yMotion(UnityEngine.ConfigurableJointMotion)
extern "C"  void ConfigurableJoint_set_yMotion_m2355990090 (ConfigurableJoint_t2962812103 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ConfigurableJointMotion UnityEngine.ConfigurableJoint::get_zMotion()
extern "C"  int32_t ConfigurableJoint_get_zMotion_m2184444944 (ConfigurableJoint_t2962812103 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ConfigurableJoint::set_zMotion(UnityEngine.ConfigurableJointMotion)
extern "C"  void ConfigurableJoint_set_zMotion_m1331296169 (ConfigurableJoint_t2962812103 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ConfigurableJointMotion UnityEngine.ConfigurableJoint::get_angularXMotion()
extern "C"  int32_t ConfigurableJoint_get_angularXMotion_m2856563950 (ConfigurableJoint_t2962812103 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ConfigurableJoint::set_angularXMotion(UnityEngine.ConfigurableJointMotion)
extern "C"  void ConfigurableJoint_set_angularXMotion_m67281661 (ConfigurableJoint_t2962812103 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ConfigurableJointMotion UnityEngine.ConfigurableJoint::get_angularYMotion()
extern "C"  int32_t ConfigurableJoint_get_angularYMotion_m1049109487 (ConfigurableJoint_t2962812103 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ConfigurableJoint::set_angularYMotion(UnityEngine.ConfigurableJointMotion)
extern "C"  void ConfigurableJoint_set_angularYMotion_m3337555036 (ConfigurableJoint_t2962812103 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ConfigurableJointMotion UnityEngine.ConfigurableJoint::get_angularZMotion()
extern "C"  int32_t ConfigurableJoint_get_angularZMotion_m3536622320 (ConfigurableJoint_t2962812103 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ConfigurableJoint::set_angularZMotion(UnityEngine.ConfigurableJointMotion)
extern "C"  void ConfigurableJoint_set_angularZMotion_m2312861115 (ConfigurableJoint_t2962812103 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.SoftJointLimitSpring UnityEngine.ConfigurableJoint::get_linearLimitSpring()
extern "C"  SoftJointLimitSpring_t4258895628  ConfigurableJoint_get_linearLimitSpring_m2832423248 (ConfigurableJoint_t2962812103 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ConfigurableJoint::set_linearLimitSpring(UnityEngine.SoftJointLimitSpring)
extern "C"  void ConfigurableJoint_set_linearLimitSpring_m3959958189 (ConfigurableJoint_t2962812103 * __this, SoftJointLimitSpring_t4258895628  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ConfigurableJoint::INTERNAL_get_linearLimitSpring(UnityEngine.SoftJointLimitSpring&)
extern "C"  void ConfigurableJoint_INTERNAL_get_linearLimitSpring_m2996814711 (ConfigurableJoint_t2962812103 * __this, SoftJointLimitSpring_t4258895628 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ConfigurableJoint::INTERNAL_set_linearLimitSpring(UnityEngine.SoftJointLimitSpring&)
extern "C"  void ConfigurableJoint_INTERNAL_set_linearLimitSpring_m3240863211 (ConfigurableJoint_t2962812103 * __this, SoftJointLimitSpring_t4258895628 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.SoftJointLimitSpring UnityEngine.ConfigurableJoint::get_angularXLimitSpring()
extern "C"  SoftJointLimitSpring_t4258895628  ConfigurableJoint_get_angularXLimitSpring_m2637613663 (ConfigurableJoint_t2962812103 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ConfigurableJoint::set_angularXLimitSpring(UnityEngine.SoftJointLimitSpring)
extern "C"  void ConfigurableJoint_set_angularXLimitSpring_m1877639356 (ConfigurableJoint_t2962812103 * __this, SoftJointLimitSpring_t4258895628  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ConfigurableJoint::INTERNAL_get_angularXLimitSpring(UnityEngine.SoftJointLimitSpring&)
extern "C"  void ConfigurableJoint_INTERNAL_get_angularXLimitSpring_m3435454792 (ConfigurableJoint_t2962812103 * __this, SoftJointLimitSpring_t4258895628 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ConfigurableJoint::INTERNAL_set_angularXLimitSpring(UnityEngine.SoftJointLimitSpring&)
extern "C"  void ConfigurableJoint_INTERNAL_set_angularXLimitSpring_m1742862012 (ConfigurableJoint_t2962812103 * __this, SoftJointLimitSpring_t4258895628 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.SoftJointLimitSpring UnityEngine.ConfigurableJoint::get_angularYZLimitSpring()
extern "C"  SoftJointLimitSpring_t4258895628  ConfigurableJoint_get_angularYZLimitSpring_m1812501914 (ConfigurableJoint_t2962812103 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ConfigurableJoint::set_angularYZLimitSpring(UnityEngine.SoftJointLimitSpring)
extern "C"  void ConfigurableJoint_set_angularYZLimitSpring_m800453887 (ConfigurableJoint_t2962812103 * __this, SoftJointLimitSpring_t4258895628  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ConfigurableJoint::INTERNAL_get_angularYZLimitSpring(UnityEngine.SoftJointLimitSpring&)
extern "C"  void ConfigurableJoint_INTERNAL_get_angularYZLimitSpring_m1938994933 (ConfigurableJoint_t2962812103 * __this, SoftJointLimitSpring_t4258895628 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ConfigurableJoint::INTERNAL_set_angularYZLimitSpring(UnityEngine.SoftJointLimitSpring&)
extern "C"  void ConfigurableJoint_INTERNAL_set_angularYZLimitSpring_m1008226305 (ConfigurableJoint_t2962812103 * __this, SoftJointLimitSpring_t4258895628 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.SoftJointLimit UnityEngine.ConfigurableJoint::get_linearLimit()
extern "C"  SoftJointLimit_t2747465311  ConfigurableJoint_get_linearLimit_m4264518512 (ConfigurableJoint_t2962812103 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ConfigurableJoint::set_linearLimit(UnityEngine.SoftJointLimit)
extern "C"  void ConfigurableJoint_set_linearLimit_m3297184205 (ConfigurableJoint_t2962812103 * __this, SoftJointLimit_t2747465311  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ConfigurableJoint::INTERNAL_get_linearLimit(UnityEngine.SoftJointLimit&)
extern "C"  void ConfigurableJoint_INTERNAL_get_linearLimit_m3964804183 (ConfigurableJoint_t2962812103 * __this, SoftJointLimit_t2747465311 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ConfigurableJoint::INTERNAL_set_linearLimit(UnityEngine.SoftJointLimit&)
extern "C"  void ConfigurableJoint_INTERNAL_set_linearLimit_m258411723 (ConfigurableJoint_t2962812103 * __this, SoftJointLimit_t2747465311 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.SoftJointLimit UnityEngine.ConfigurableJoint::get_lowAngularXLimit()
extern "C"  SoftJointLimit_t2747465311  ConfigurableJoint_get_lowAngularXLimit_m3795287993 (ConfigurableJoint_t2962812103 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ConfigurableJoint::set_lowAngularXLimit(UnityEngine.SoftJointLimit)
extern "C"  void ConfigurableJoint_set_lowAngularXLimit_m961476696 (ConfigurableJoint_t2962812103 * __this, SoftJointLimit_t2747465311  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ConfigurableJoint::INTERNAL_get_lowAngularXLimit(UnityEngine.SoftJointLimit&)
extern "C"  void ConfigurableJoint_INTERNAL_get_lowAngularXLimit_m614947004 (ConfigurableJoint_t2962812103 * __this, SoftJointLimit_t2747465311 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ConfigurableJoint::INTERNAL_set_lowAngularXLimit(UnityEngine.SoftJointLimit&)
extern "C"  void ConfigurableJoint_INTERNAL_set_lowAngularXLimit_m521834696 (ConfigurableJoint_t2962812103 * __this, SoftJointLimit_t2747465311 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.SoftJointLimit UnityEngine.ConfigurableJoint::get_highAngularXLimit()
extern "C"  SoftJointLimit_t2747465311  ConfigurableJoint_get_highAngularXLimit_m2526202365 (ConfigurableJoint_t2962812103 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ConfigurableJoint::set_highAngularXLimit(UnityEngine.SoftJointLimit)
extern "C"  void ConfigurableJoint_set_highAngularXLimit_m3975969498 (ConfigurableJoint_t2962812103 * __this, SoftJointLimit_t2747465311  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ConfigurableJoint::INTERNAL_get_highAngularXLimit(UnityEngine.SoftJointLimit&)
extern "C"  void ConfigurableJoint_INTERNAL_get_highAngularXLimit_m23356138 (ConfigurableJoint_t2962812103 * __this, SoftJointLimit_t2747465311 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ConfigurableJoint::INTERNAL_set_highAngularXLimit(UnityEngine.SoftJointLimit&)
extern "C"  void ConfigurableJoint_INTERNAL_set_highAngularXLimit_m1431841886 (ConfigurableJoint_t2962812103 * __this, SoftJointLimit_t2747465311 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.SoftJointLimit UnityEngine.ConfigurableJoint::get_angularYLimit()
extern "C"  SoftJointLimit_t2747465311  ConfigurableJoint_get_angularYLimit_m2439514462 (ConfigurableJoint_t2962812103 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ConfigurableJoint::set_angularYLimit(UnityEngine.SoftJointLimit)
extern "C"  void ConfigurableJoint_set_angularYLimit_m541491835 (ConfigurableJoint_t2962812103 * __this, SoftJointLimit_t2747465311  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ConfigurableJoint::INTERNAL_get_angularYLimit(UnityEngine.SoftJointLimit&)
extern "C"  void ConfigurableJoint_INTERNAL_get_angularYLimit_m3929631593 (ConfigurableJoint_t2962812103 * __this, SoftJointLimit_t2747465311 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ConfigurableJoint::INTERNAL_set_angularYLimit(UnityEngine.SoftJointLimit&)
extern "C"  void ConfigurableJoint_INTERNAL_set_angularYLimit_m2614365917 (ConfigurableJoint_t2962812103 * __this, SoftJointLimit_t2747465311 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.SoftJointLimit UnityEngine.ConfigurableJoint::get_angularZLimit()
extern "C"  SoftJointLimit_t2747465311  ConfigurableJoint_get_angularZLimit_m4182324797 (ConfigurableJoint_t2962812103 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ConfigurableJoint::set_angularZLimit(UnityEngine.SoftJointLimit)
extern "C"  void ConfigurableJoint_set_angularZLimit_m1566983834 (ConfigurableJoint_t2962812103 * __this, SoftJointLimit_t2747465311  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ConfigurableJoint::INTERNAL_get_angularZLimit(UnityEngine.SoftJointLimit&)
extern "C"  void ConfigurableJoint_INTERNAL_get_angularZLimit_m1360145194 (ConfigurableJoint_t2962812103 * __this, SoftJointLimit_t2747465311 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ConfigurableJoint::INTERNAL_set_angularZLimit(UnityEngine.SoftJointLimit&)
extern "C"  void ConfigurableJoint_INTERNAL_set_angularZLimit_m44879518 (ConfigurableJoint_t2962812103 * __this, SoftJointLimit_t2747465311 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.ConfigurableJoint::get_targetPosition()
extern "C"  Vector3_t4282066566  ConfigurableJoint_get_targetPosition_m2196031525 (ConfigurableJoint_t2962812103 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ConfigurableJoint::set_targetPosition(UnityEngine.Vector3)
extern "C"  void ConfigurableJoint_set_targetPosition_m1385054118 (ConfigurableJoint_t2962812103 * __this, Vector3_t4282066566  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ConfigurableJoint::INTERNAL_get_targetPosition(UnityEngine.Vector3&)
extern "C"  void ConfigurableJoint_INTERNAL_get_targetPosition_m1775549854 (ConfigurableJoint_t2962812103 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ConfigurableJoint::INTERNAL_set_targetPosition(UnityEngine.Vector3&)
extern "C"  void ConfigurableJoint_INTERNAL_set_targetPosition_m2134640658 (ConfigurableJoint_t2962812103 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.ConfigurableJoint::get_targetVelocity()
extern "C"  Vector3_t4282066566  ConfigurableJoint_get_targetVelocity_m3140374233 (ConfigurableJoint_t2962812103 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ConfigurableJoint::set_targetVelocity(UnityEngine.Vector3)
extern "C"  void ConfigurableJoint_set_targetVelocity_m3483743346 (ConfigurableJoint_t2962812103 * __this, Vector3_t4282066566  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ConfigurableJoint::INTERNAL_get_targetVelocity(UnityEngine.Vector3&)
extern "C"  void ConfigurableJoint_INTERNAL_get_targetVelocity_m2410406482 (ConfigurableJoint_t2962812103 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ConfigurableJoint::INTERNAL_set_targetVelocity(UnityEngine.Vector3&)
extern "C"  void ConfigurableJoint_INTERNAL_set_targetVelocity_m2769497286 (ConfigurableJoint_t2962812103 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.JointDrive UnityEngine.ConfigurableJoint::get_xDrive()
extern "C"  JointDrive_t1303722980  ConfigurableJoint_get_xDrive_m2500502399 (ConfigurableJoint_t2962812103 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ConfigurableJoint::set_xDrive(UnityEngine.JointDrive)
extern "C"  void ConfigurableJoint_set_xDrive_m1782638388 (ConfigurableJoint_t2962812103 * __this, JointDrive_t1303722980  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ConfigurableJoint::INTERNAL_get_xDrive(UnityEngine.JointDrive&)
extern "C"  void ConfigurableJoint_INTERNAL_get_xDrive_m515662688 (ConfigurableJoint_t2962812103 * __this, JointDrive_t1303722980 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ConfigurableJoint::INTERNAL_set_xDrive(UnityEngine.JointDrive&)
extern "C"  void ConfigurableJoint_INTERNAL_set_xDrive_m4041564268 (ConfigurableJoint_t2962812103 * __this, JointDrive_t1303722980 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.JointDrive UnityEngine.ConfigurableJoint::get_yDrive()
extern "C"  JointDrive_t1303722980  ConfigurableJoint_get_yDrive_m4243312734 (ConfigurableJoint_t2962812103 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ConfigurableJoint::set_yDrive(UnityEngine.JointDrive)
extern "C"  void ConfigurableJoint_set_yDrive_m1793954515 (ConfigurableJoint_t2962812103 * __this, JointDrive_t1303722980  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ConfigurableJoint::INTERNAL_get_yDrive(UnityEngine.JointDrive&)
extern "C"  void ConfigurableJoint_INTERNAL_get_yDrive_m866462625 (ConfigurableJoint_t2962812103 * __this, JointDrive_t1303722980 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ConfigurableJoint::INTERNAL_set_yDrive(UnityEngine.JointDrive&)
extern "C"  void ConfigurableJoint_INTERNAL_set_yDrive_m97396909 (ConfigurableJoint_t2962812103 * __this, JointDrive_t1303722980 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.JointDrive UnityEngine.ConfigurableJoint::get_zDrive()
extern "C"  JointDrive_t1303722980  ConfigurableJoint_get_zDrive_m1691155773 (ConfigurableJoint_t2962812103 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ConfigurableJoint::set_zDrive(UnityEngine.JointDrive)
extern "C"  void ConfigurableJoint_set_zDrive_m1805270642 (ConfigurableJoint_t2962812103 * __this, JointDrive_t1303722980  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ConfigurableJoint::INTERNAL_get_zDrive(UnityEngine.JointDrive&)
extern "C"  void ConfigurableJoint_INTERNAL_get_zDrive_m1217262562 (ConfigurableJoint_t2962812103 * __this, JointDrive_t1303722980 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ConfigurableJoint::INTERNAL_set_zDrive(UnityEngine.JointDrive&)
extern "C"  void ConfigurableJoint_INTERNAL_set_zDrive_m448196846 (ConfigurableJoint_t2962812103 * __this, JointDrive_t1303722980 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.ConfigurableJoint::get_targetRotation()
extern "C"  Quaternion_t1553702882  ConfigurableJoint_get_targetRotation_m4019146106 (ConfigurableJoint_t2962812103 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ConfigurableJoint::set_targetRotation(UnityEngine.Quaternion)
extern "C"  void ConfigurableJoint_set_targetRotation_m69563443 (ConfigurableJoint_t2962812103 * __this, Quaternion_t1553702882  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ConfigurableJoint::INTERNAL_get_targetRotation(UnityEngine.Quaternion&)
extern "C"  void ConfigurableJoint_INTERNAL_get_targetRotation_m1342484033 (ConfigurableJoint_t2962812103 * __this, Quaternion_t1553702882 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ConfigurableJoint::INTERNAL_set_targetRotation(UnityEngine.Quaternion&)
extern "C"  void ConfigurableJoint_INTERNAL_set_targetRotation_m253091661 (ConfigurableJoint_t2962812103 * __this, Quaternion_t1553702882 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.ConfigurableJoint::get_targetAngularVelocity()
extern "C"  Vector3_t4282066566  ConfigurableJoint_get_targetAngularVelocity_m821116293 (ConfigurableJoint_t2962812103 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ConfigurableJoint::set_targetAngularVelocity(UnityEngine.Vector3)
extern "C"  void ConfigurableJoint_set_targetAngularVelocity_m806878690 (ConfigurableJoint_t2962812103 * __this, Vector3_t4282066566  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ConfigurableJoint::INTERNAL_get_targetAngularVelocity(UnityEngine.Vector3&)
extern "C"  void ConfigurableJoint_INTERNAL_get_targetAngularVelocity_m120321650 (ConfigurableJoint_t2962812103 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ConfigurableJoint::INTERNAL_set_targetAngularVelocity(UnityEngine.Vector3&)
extern "C"  void ConfigurableJoint_INTERNAL_set_targetAngularVelocity_m2296758654 (ConfigurableJoint_t2962812103 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RotationDriveMode UnityEngine.ConfigurableJoint::get_rotationDriveMode()
extern "C"  int32_t ConfigurableJoint_get_rotationDriveMode_m2827751015 (ConfigurableJoint_t2962812103 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ConfigurableJoint::set_rotationDriveMode(UnityEngine.RotationDriveMode)
extern "C"  void ConfigurableJoint_set_rotationDriveMode_m2692440034 (ConfigurableJoint_t2962812103 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.JointDrive UnityEngine.ConfigurableJoint::get_angularXDrive()
extern "C"  JointDrive_t1303722980  ConfigurableJoint_get_angularXDrive_m2920665065 (ConfigurableJoint_t2962812103 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ConfigurableJoint::set_angularXDrive(UnityEngine.JointDrive)
extern "C"  void ConfigurableJoint_set_angularXDrive_m862227654 (ConfigurableJoint_t2962812103 * __this, JointDrive_t1303722980  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ConfigurableJoint::INTERNAL_get_angularXDrive(UnityEngine.JointDrive&)
extern "C"  void ConfigurableJoint_INTERNAL_get_angularXDrive_m1764279934 (ConfigurableJoint_t2962812103 * __this, JointDrive_t1303722980 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ConfigurableJoint::INTERNAL_set_angularXDrive(UnityEngine.JointDrive&)
extern "C"  void ConfigurableJoint_INTERNAL_set_angularXDrive_m3253158898 (ConfigurableJoint_t2962812103 * __this, JointDrive_t1303722980 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.JointDrive UnityEngine.ConfigurableJoint::get_angularYZDrive()
extern "C"  JointDrive_t1303722980  ConfigurableJoint_get_angularYZDrive_m596696340 (ConfigurableJoint_t2962812103 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ConfigurableJoint::set_angularYZDrive(UnityEngine.JointDrive)
extern "C"  void ConfigurableJoint_set_angularYZDrive_m4071548681 (ConfigurableJoint_t2962812103 * __this, JointDrive_t1303722980  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ConfigurableJoint::INTERNAL_get_angularYZDrive(UnityEngine.JointDrive&)
extern "C"  void ConfigurableJoint_INTERNAL_get_angularYZDrive_m849974827 (ConfigurableJoint_t2962812103 * __this, JointDrive_t1303722980 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ConfigurableJoint::INTERNAL_set_angularYZDrive(UnityEngine.JointDrive&)
extern "C"  void ConfigurableJoint_INTERNAL_set_angularYZDrive_m4055549751 (ConfigurableJoint_t2962812103 * __this, JointDrive_t1303722980 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.JointDrive UnityEngine.ConfigurableJoint::get_slerpDrive()
extern "C"  JointDrive_t1303722980  ConfigurableJoint_get_slerpDrive_m754882893 (ConfigurableJoint_t2962812103 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ConfigurableJoint::set_slerpDrive(UnityEngine.JointDrive)
extern "C"  void ConfigurableJoint_set_slerpDrive_m1907366018 (ConfigurableJoint_t2962812103 * __this, JointDrive_t1303722980  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ConfigurableJoint::INTERNAL_get_slerpDrive(UnityEngine.JointDrive&)
extern "C"  void ConfigurableJoint_INTERNAL_get_slerpDrive_m441972690 (ConfigurableJoint_t2962812103 * __this, JointDrive_t1303722980 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ConfigurableJoint::INTERNAL_set_slerpDrive(UnityEngine.JointDrive&)
extern "C"  void ConfigurableJoint_INTERNAL_set_slerpDrive_m2254671582 (ConfigurableJoint_t2962812103 * __this, JointDrive_t1303722980 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.JointProjectionMode UnityEngine.ConfigurableJoint::get_projectionMode()
extern "C"  int32_t ConfigurableJoint_get_projectionMode_m602368201 (ConfigurableJoint_t2962812103 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ConfigurableJoint::set_projectionMode(UnityEngine.JointProjectionMode)
extern "C"  void ConfigurableJoint_set_projectionMode_m4267606402 (ConfigurableJoint_t2962812103 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.ConfigurableJoint::get_projectionDistance()
extern "C"  float ConfigurableJoint_get_projectionDistance_m3076320605 (ConfigurableJoint_t2962812103 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ConfigurableJoint::set_projectionDistance(System.Single)
extern "C"  void ConfigurableJoint_set_projectionDistance_m801716334 (ConfigurableJoint_t2962812103 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.ConfigurableJoint::get_projectionAngle()
extern "C"  float ConfigurableJoint_get_projectionAngle_m3517204685 (ConfigurableJoint_t2962812103 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ConfigurableJoint::set_projectionAngle(System.Single)
extern "C"  void ConfigurableJoint_set_projectionAngle_m3894708478 (ConfigurableJoint_t2962812103 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.ConfigurableJoint::get_configuredInWorldSpace()
extern "C"  bool ConfigurableJoint_get_configuredInWorldSpace_m4136931862 (ConfigurableJoint_t2962812103 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ConfigurableJoint::set_configuredInWorldSpace(System.Boolean)
extern "C"  void ConfigurableJoint_set_configuredInWorldSpace_m2606647975 (ConfigurableJoint_t2962812103 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.ConfigurableJoint::get_swapBodies()
extern "C"  bool ConfigurableJoint_get_swapBodies_m2357103698 (ConfigurableJoint_t2962812103 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ConfigurableJoint::set_swapBodies(System.Boolean)
extern "C"  void ConfigurableJoint_set_swapBodies_m1717158627 (ConfigurableJoint_t2962812103 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
