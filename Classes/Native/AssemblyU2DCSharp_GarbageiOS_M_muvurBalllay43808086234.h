﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_muvurBalllay4
struct  M_muvurBalllay4_t3808086234  : public Il2CppObject
{
public:
	// System.String GarbageiOS.M_muvurBalllay4::_qumowRejouhas
	String_t* ____qumowRejouhas_0;
	// System.UInt32 GarbageiOS.M_muvurBalllay4::_cirpooTalse
	uint32_t ____cirpooTalse_1;
	// System.Int32 GarbageiOS.M_muvurBalllay4::_disuDutere
	int32_t ____disuDutere_2;
	// System.Single GarbageiOS.M_muvurBalllay4::_marfurBaswelli
	float ____marfurBaswelli_3;
	// System.Boolean GarbageiOS.M_muvurBalllay4::_stemwhamay
	bool ____stemwhamay_4;
	// System.Int32 GarbageiOS.M_muvurBalllay4::_touceBairme
	int32_t ____touceBairme_5;
	// System.Int32 GarbageiOS.M_muvurBalllay4::_walljougi
	int32_t ____walljougi_6;
	// System.String GarbageiOS.M_muvurBalllay4::_morpirer
	String_t* ____morpirer_7;
	// System.Boolean GarbageiOS.M_muvurBalllay4::_reawha
	bool ____reawha_8;
	// System.Int32 GarbageiOS.M_muvurBalllay4::_leeje
	int32_t ____leeje_9;
	// System.UInt32 GarbageiOS.M_muvurBalllay4::_fisposalNoudas
	uint32_t ____fisposalNoudas_10;
	// System.UInt32 GarbageiOS.M_muvurBalllay4::_rosereTrelrurcar
	uint32_t ____rosereTrelrurcar_11;
	// System.Int32 GarbageiOS.M_muvurBalllay4::_noosuHechou
	int32_t ____noosuHechou_12;
	// System.String GarbageiOS.M_muvurBalllay4::_zuleLaletear
	String_t* ____zuleLaletear_13;
	// System.Single GarbageiOS.M_muvurBalllay4::_dateewhemTemikear
	float ____dateewhemTemikear_14;
	// System.Single GarbageiOS.M_muvurBalllay4::_whelelisJido
	float ____whelelisJido_15;
	// System.Int32 GarbageiOS.M_muvurBalllay4::_zepuvow
	int32_t ____zepuvow_16;
	// System.UInt32 GarbageiOS.M_muvurBalllay4::_drajelrirKidoupay
	uint32_t ____drajelrirKidoupay_17;
	// System.String GarbageiOS.M_muvurBalllay4::_bounasaiDupaha
	String_t* ____bounasaiDupaha_18;
	// System.Int32 GarbageiOS.M_muvurBalllay4::_nelsurkaPerekea
	int32_t ____nelsurkaPerekea_19;
	// System.Single GarbageiOS.M_muvurBalllay4::_berstee
	float ____berstee_20;
	// System.Single GarbageiOS.M_muvurBalllay4::_chavapuFairsai
	float ____chavapuFairsai_21;

public:
	inline static int32_t get_offset_of__qumowRejouhas_0() { return static_cast<int32_t>(offsetof(M_muvurBalllay4_t3808086234, ____qumowRejouhas_0)); }
	inline String_t* get__qumowRejouhas_0() const { return ____qumowRejouhas_0; }
	inline String_t** get_address_of__qumowRejouhas_0() { return &____qumowRejouhas_0; }
	inline void set__qumowRejouhas_0(String_t* value)
	{
		____qumowRejouhas_0 = value;
		Il2CppCodeGenWriteBarrier(&____qumowRejouhas_0, value);
	}

	inline static int32_t get_offset_of__cirpooTalse_1() { return static_cast<int32_t>(offsetof(M_muvurBalllay4_t3808086234, ____cirpooTalse_1)); }
	inline uint32_t get__cirpooTalse_1() const { return ____cirpooTalse_1; }
	inline uint32_t* get_address_of__cirpooTalse_1() { return &____cirpooTalse_1; }
	inline void set__cirpooTalse_1(uint32_t value)
	{
		____cirpooTalse_1 = value;
	}

	inline static int32_t get_offset_of__disuDutere_2() { return static_cast<int32_t>(offsetof(M_muvurBalllay4_t3808086234, ____disuDutere_2)); }
	inline int32_t get__disuDutere_2() const { return ____disuDutere_2; }
	inline int32_t* get_address_of__disuDutere_2() { return &____disuDutere_2; }
	inline void set__disuDutere_2(int32_t value)
	{
		____disuDutere_2 = value;
	}

	inline static int32_t get_offset_of__marfurBaswelli_3() { return static_cast<int32_t>(offsetof(M_muvurBalllay4_t3808086234, ____marfurBaswelli_3)); }
	inline float get__marfurBaswelli_3() const { return ____marfurBaswelli_3; }
	inline float* get_address_of__marfurBaswelli_3() { return &____marfurBaswelli_3; }
	inline void set__marfurBaswelli_3(float value)
	{
		____marfurBaswelli_3 = value;
	}

	inline static int32_t get_offset_of__stemwhamay_4() { return static_cast<int32_t>(offsetof(M_muvurBalllay4_t3808086234, ____stemwhamay_4)); }
	inline bool get__stemwhamay_4() const { return ____stemwhamay_4; }
	inline bool* get_address_of__stemwhamay_4() { return &____stemwhamay_4; }
	inline void set__stemwhamay_4(bool value)
	{
		____stemwhamay_4 = value;
	}

	inline static int32_t get_offset_of__touceBairme_5() { return static_cast<int32_t>(offsetof(M_muvurBalllay4_t3808086234, ____touceBairme_5)); }
	inline int32_t get__touceBairme_5() const { return ____touceBairme_5; }
	inline int32_t* get_address_of__touceBairme_5() { return &____touceBairme_5; }
	inline void set__touceBairme_5(int32_t value)
	{
		____touceBairme_5 = value;
	}

	inline static int32_t get_offset_of__walljougi_6() { return static_cast<int32_t>(offsetof(M_muvurBalllay4_t3808086234, ____walljougi_6)); }
	inline int32_t get__walljougi_6() const { return ____walljougi_6; }
	inline int32_t* get_address_of__walljougi_6() { return &____walljougi_6; }
	inline void set__walljougi_6(int32_t value)
	{
		____walljougi_6 = value;
	}

	inline static int32_t get_offset_of__morpirer_7() { return static_cast<int32_t>(offsetof(M_muvurBalllay4_t3808086234, ____morpirer_7)); }
	inline String_t* get__morpirer_7() const { return ____morpirer_7; }
	inline String_t** get_address_of__morpirer_7() { return &____morpirer_7; }
	inline void set__morpirer_7(String_t* value)
	{
		____morpirer_7 = value;
		Il2CppCodeGenWriteBarrier(&____morpirer_7, value);
	}

	inline static int32_t get_offset_of__reawha_8() { return static_cast<int32_t>(offsetof(M_muvurBalllay4_t3808086234, ____reawha_8)); }
	inline bool get__reawha_8() const { return ____reawha_8; }
	inline bool* get_address_of__reawha_8() { return &____reawha_8; }
	inline void set__reawha_8(bool value)
	{
		____reawha_8 = value;
	}

	inline static int32_t get_offset_of__leeje_9() { return static_cast<int32_t>(offsetof(M_muvurBalllay4_t3808086234, ____leeje_9)); }
	inline int32_t get__leeje_9() const { return ____leeje_9; }
	inline int32_t* get_address_of__leeje_9() { return &____leeje_9; }
	inline void set__leeje_9(int32_t value)
	{
		____leeje_9 = value;
	}

	inline static int32_t get_offset_of__fisposalNoudas_10() { return static_cast<int32_t>(offsetof(M_muvurBalllay4_t3808086234, ____fisposalNoudas_10)); }
	inline uint32_t get__fisposalNoudas_10() const { return ____fisposalNoudas_10; }
	inline uint32_t* get_address_of__fisposalNoudas_10() { return &____fisposalNoudas_10; }
	inline void set__fisposalNoudas_10(uint32_t value)
	{
		____fisposalNoudas_10 = value;
	}

	inline static int32_t get_offset_of__rosereTrelrurcar_11() { return static_cast<int32_t>(offsetof(M_muvurBalllay4_t3808086234, ____rosereTrelrurcar_11)); }
	inline uint32_t get__rosereTrelrurcar_11() const { return ____rosereTrelrurcar_11; }
	inline uint32_t* get_address_of__rosereTrelrurcar_11() { return &____rosereTrelrurcar_11; }
	inline void set__rosereTrelrurcar_11(uint32_t value)
	{
		____rosereTrelrurcar_11 = value;
	}

	inline static int32_t get_offset_of__noosuHechou_12() { return static_cast<int32_t>(offsetof(M_muvurBalllay4_t3808086234, ____noosuHechou_12)); }
	inline int32_t get__noosuHechou_12() const { return ____noosuHechou_12; }
	inline int32_t* get_address_of__noosuHechou_12() { return &____noosuHechou_12; }
	inline void set__noosuHechou_12(int32_t value)
	{
		____noosuHechou_12 = value;
	}

	inline static int32_t get_offset_of__zuleLaletear_13() { return static_cast<int32_t>(offsetof(M_muvurBalllay4_t3808086234, ____zuleLaletear_13)); }
	inline String_t* get__zuleLaletear_13() const { return ____zuleLaletear_13; }
	inline String_t** get_address_of__zuleLaletear_13() { return &____zuleLaletear_13; }
	inline void set__zuleLaletear_13(String_t* value)
	{
		____zuleLaletear_13 = value;
		Il2CppCodeGenWriteBarrier(&____zuleLaletear_13, value);
	}

	inline static int32_t get_offset_of__dateewhemTemikear_14() { return static_cast<int32_t>(offsetof(M_muvurBalllay4_t3808086234, ____dateewhemTemikear_14)); }
	inline float get__dateewhemTemikear_14() const { return ____dateewhemTemikear_14; }
	inline float* get_address_of__dateewhemTemikear_14() { return &____dateewhemTemikear_14; }
	inline void set__dateewhemTemikear_14(float value)
	{
		____dateewhemTemikear_14 = value;
	}

	inline static int32_t get_offset_of__whelelisJido_15() { return static_cast<int32_t>(offsetof(M_muvurBalllay4_t3808086234, ____whelelisJido_15)); }
	inline float get__whelelisJido_15() const { return ____whelelisJido_15; }
	inline float* get_address_of__whelelisJido_15() { return &____whelelisJido_15; }
	inline void set__whelelisJido_15(float value)
	{
		____whelelisJido_15 = value;
	}

	inline static int32_t get_offset_of__zepuvow_16() { return static_cast<int32_t>(offsetof(M_muvurBalllay4_t3808086234, ____zepuvow_16)); }
	inline int32_t get__zepuvow_16() const { return ____zepuvow_16; }
	inline int32_t* get_address_of__zepuvow_16() { return &____zepuvow_16; }
	inline void set__zepuvow_16(int32_t value)
	{
		____zepuvow_16 = value;
	}

	inline static int32_t get_offset_of__drajelrirKidoupay_17() { return static_cast<int32_t>(offsetof(M_muvurBalllay4_t3808086234, ____drajelrirKidoupay_17)); }
	inline uint32_t get__drajelrirKidoupay_17() const { return ____drajelrirKidoupay_17; }
	inline uint32_t* get_address_of__drajelrirKidoupay_17() { return &____drajelrirKidoupay_17; }
	inline void set__drajelrirKidoupay_17(uint32_t value)
	{
		____drajelrirKidoupay_17 = value;
	}

	inline static int32_t get_offset_of__bounasaiDupaha_18() { return static_cast<int32_t>(offsetof(M_muvurBalllay4_t3808086234, ____bounasaiDupaha_18)); }
	inline String_t* get__bounasaiDupaha_18() const { return ____bounasaiDupaha_18; }
	inline String_t** get_address_of__bounasaiDupaha_18() { return &____bounasaiDupaha_18; }
	inline void set__bounasaiDupaha_18(String_t* value)
	{
		____bounasaiDupaha_18 = value;
		Il2CppCodeGenWriteBarrier(&____bounasaiDupaha_18, value);
	}

	inline static int32_t get_offset_of__nelsurkaPerekea_19() { return static_cast<int32_t>(offsetof(M_muvurBalllay4_t3808086234, ____nelsurkaPerekea_19)); }
	inline int32_t get__nelsurkaPerekea_19() const { return ____nelsurkaPerekea_19; }
	inline int32_t* get_address_of__nelsurkaPerekea_19() { return &____nelsurkaPerekea_19; }
	inline void set__nelsurkaPerekea_19(int32_t value)
	{
		____nelsurkaPerekea_19 = value;
	}

	inline static int32_t get_offset_of__berstee_20() { return static_cast<int32_t>(offsetof(M_muvurBalllay4_t3808086234, ____berstee_20)); }
	inline float get__berstee_20() const { return ____berstee_20; }
	inline float* get_address_of__berstee_20() { return &____berstee_20; }
	inline void set__berstee_20(float value)
	{
		____berstee_20 = value;
	}

	inline static int32_t get_offset_of__chavapuFairsai_21() { return static_cast<int32_t>(offsetof(M_muvurBalllay4_t3808086234, ____chavapuFairsai_21)); }
	inline float get__chavapuFairsai_21() const { return ____chavapuFairsai_21; }
	inline float* get_address_of__chavapuFairsai_21() { return &____chavapuFairsai_21; }
	inline void set__chavapuFairsai_21(float value)
	{
		____chavapuFairsai_21 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
