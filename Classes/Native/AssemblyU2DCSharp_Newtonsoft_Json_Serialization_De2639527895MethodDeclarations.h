﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Serialization.DefaultContractResolver/<CreateShouldSerializeTest>c__AnonStorey132
struct U3CCreateShouldSerializeTestU3Ec__AnonStorey132_t2639527895;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void Newtonsoft.Json.Serialization.DefaultContractResolver/<CreateShouldSerializeTest>c__AnonStorey132::.ctor()
extern "C"  void U3CCreateShouldSerializeTestU3Ec__AnonStorey132__ctor_m1565163380 (U3CCreateShouldSerializeTestU3Ec__AnonStorey132_t2639527895 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.DefaultContractResolver/<CreateShouldSerializeTest>c__AnonStorey132::<>m__387(System.Object)
extern "C"  bool U3CCreateShouldSerializeTestU3Ec__AnonStorey132_U3CU3Em__387_m4117426801 (U3CCreateShouldSerializeTestU3Ec__AnonStorey132_t2639527895 * __this, Il2CppObject * ___o0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
