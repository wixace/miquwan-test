﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._28171467fb6b935f42365bd72fbfa10a
struct _28171467fb6b935f42365bd72fbfa10a_t2750821910;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__28171467fb6b935f42365bd72750821910.h"

// System.Void Little._28171467fb6b935f42365bd72fbfa10a::.ctor()
extern "C"  void _28171467fb6b935f42365bd72fbfa10a__ctor_m2830723287 (_28171467fb6b935f42365bd72fbfa10a_t2750821910 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._28171467fb6b935f42365bd72fbfa10a::_28171467fb6b935f42365bd72fbfa10am2(System.Int32)
extern "C"  int32_t _28171467fb6b935f42365bd72fbfa10a__28171467fb6b935f42365bd72fbfa10am2_m3646532057 (_28171467fb6b935f42365bd72fbfa10a_t2750821910 * __this, int32_t ____28171467fb6b935f42365bd72fbfa10aa0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._28171467fb6b935f42365bd72fbfa10a::_28171467fb6b935f42365bd72fbfa10am(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _28171467fb6b935f42365bd72fbfa10a__28171467fb6b935f42365bd72fbfa10am_m2044380413 (_28171467fb6b935f42365bd72fbfa10a_t2750821910 * __this, int32_t ____28171467fb6b935f42365bd72fbfa10aa0, int32_t ____28171467fb6b935f42365bd72fbfa10a661, int32_t ____28171467fb6b935f42365bd72fbfa10ac2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._28171467fb6b935f42365bd72fbfa10a::ilo__28171467fb6b935f42365bd72fbfa10am21(Little._28171467fb6b935f42365bd72fbfa10a,System.Int32)
extern "C"  int32_t _28171467fb6b935f42365bd72fbfa10a_ilo__28171467fb6b935f42365bd72fbfa10am21_m2035474589 (Il2CppObject * __this /* static, unused */, _28171467fb6b935f42365bd72fbfa10a_t2750821910 * ____this0, int32_t ____28171467fb6b935f42365bd72fbfa10aa1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
