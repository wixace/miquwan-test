﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_FlowControl_FlowContainerBase4203254394.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FlowControl.FlowContainerLine
struct  FlowContainerLine_t4203559837  : public FlowContainerBase_t4203254394
{
public:
	// System.Int32 FlowControl.FlowContainerLine::currIndex
	int32_t ___currIndex_9;

public:
	inline static int32_t get_offset_of_currIndex_9() { return static_cast<int32_t>(offsetof(FlowContainerLine_t4203559837, ___currIndex_9)); }
	inline int32_t get_currIndex_9() const { return ___currIndex_9; }
	inline int32_t* get_address_of_currIndex_9() { return &___currIndex_9; }
	inline void set_currIndex_9(int32_t value)
	{
		___currIndex_9 = value;
	}
};

struct FlowContainerLine_t4203559837_StaticFields
{
public:
	// System.Boolean FlowControl.FlowContainerLine::isNext
	bool ___isNext_8;

public:
	inline static int32_t get_offset_of_isNext_8() { return static_cast<int32_t>(offsetof(FlowContainerLine_t4203559837_StaticFields, ___isNext_8)); }
	inline bool get_isNext_8() const { return ___isNext_8; }
	inline bool* get_address_of_isNext_8() { return &___isNext_8; }
	inline void set_isNext_8(bool value)
	{
		___isNext_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
