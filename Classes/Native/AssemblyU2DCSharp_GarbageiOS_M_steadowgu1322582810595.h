﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_steadowgu132
struct  M_steadowgu132_t2582810595  : public Il2CppObject
{
public:
	// System.Single GarbageiOS.M_steadowgu132::_tekeesuFeredupal
	float ____tekeesuFeredupal_0;
	// System.Single GarbageiOS.M_steadowgu132::_fihucaiKairleavu
	float ____fihucaiKairleavu_1;
	// System.String GarbageiOS.M_steadowgu132::_trigaSayfo
	String_t* ____trigaSayfo_2;
	// System.String GarbageiOS.M_steadowgu132::_lersatrearRawwurge
	String_t* ____lersatrearRawwurge_3;
	// System.Int32 GarbageiOS.M_steadowgu132::_seardrea
	int32_t ____seardrea_4;
	// System.Int32 GarbageiOS.M_steadowgu132::_mallwhelseBooma
	int32_t ____mallwhelseBooma_5;
	// System.Boolean GarbageiOS.M_steadowgu132::_cosear
	bool ____cosear_6;
	// System.Int32 GarbageiOS.M_steadowgu132::_rerebar
	int32_t ____rerebar_7;
	// System.String GarbageiOS.M_steadowgu132::_debeCelfall
	String_t* ____debeCelfall_8;
	// System.String GarbageiOS.M_steadowgu132::_pearcir
	String_t* ____pearcir_9;
	// System.Int32 GarbageiOS.M_steadowgu132::_beva
	int32_t ____beva_10;
	// System.UInt32 GarbageiOS.M_steadowgu132::_cairboqalWhurdahor
	uint32_t ____cairboqalWhurdahor_11;
	// System.String GarbageiOS.M_steadowgu132::_jovouSafachu
	String_t* ____jovouSafachu_12;
	// System.String GarbageiOS.M_steadowgu132::_kaseamo
	String_t* ____kaseamo_13;
	// System.String GarbageiOS.M_steadowgu132::_callpesou
	String_t* ____callpesou_14;
	// System.Int32 GarbageiOS.M_steadowgu132::_baigouRasjusay
	int32_t ____baigouRasjusay_15;
	// System.Single GarbageiOS.M_steadowgu132::_rayorvor
	float ____rayorvor_16;
	// System.Boolean GarbageiOS.M_steadowgu132::_sairtemTerejir
	bool ____sairtemTerejir_17;
	// System.Single GarbageiOS.M_steadowgu132::_halfurcall
	float ____halfurcall_18;
	// System.Boolean GarbageiOS.M_steadowgu132::_geretetrallBuhaw
	bool ____geretetrallBuhaw_19;

public:
	inline static int32_t get_offset_of__tekeesuFeredupal_0() { return static_cast<int32_t>(offsetof(M_steadowgu132_t2582810595, ____tekeesuFeredupal_0)); }
	inline float get__tekeesuFeredupal_0() const { return ____tekeesuFeredupal_0; }
	inline float* get_address_of__tekeesuFeredupal_0() { return &____tekeesuFeredupal_0; }
	inline void set__tekeesuFeredupal_0(float value)
	{
		____tekeesuFeredupal_0 = value;
	}

	inline static int32_t get_offset_of__fihucaiKairleavu_1() { return static_cast<int32_t>(offsetof(M_steadowgu132_t2582810595, ____fihucaiKairleavu_1)); }
	inline float get__fihucaiKairleavu_1() const { return ____fihucaiKairleavu_1; }
	inline float* get_address_of__fihucaiKairleavu_1() { return &____fihucaiKairleavu_1; }
	inline void set__fihucaiKairleavu_1(float value)
	{
		____fihucaiKairleavu_1 = value;
	}

	inline static int32_t get_offset_of__trigaSayfo_2() { return static_cast<int32_t>(offsetof(M_steadowgu132_t2582810595, ____trigaSayfo_2)); }
	inline String_t* get__trigaSayfo_2() const { return ____trigaSayfo_2; }
	inline String_t** get_address_of__trigaSayfo_2() { return &____trigaSayfo_2; }
	inline void set__trigaSayfo_2(String_t* value)
	{
		____trigaSayfo_2 = value;
		Il2CppCodeGenWriteBarrier(&____trigaSayfo_2, value);
	}

	inline static int32_t get_offset_of__lersatrearRawwurge_3() { return static_cast<int32_t>(offsetof(M_steadowgu132_t2582810595, ____lersatrearRawwurge_3)); }
	inline String_t* get__lersatrearRawwurge_3() const { return ____lersatrearRawwurge_3; }
	inline String_t** get_address_of__lersatrearRawwurge_3() { return &____lersatrearRawwurge_3; }
	inline void set__lersatrearRawwurge_3(String_t* value)
	{
		____lersatrearRawwurge_3 = value;
		Il2CppCodeGenWriteBarrier(&____lersatrearRawwurge_3, value);
	}

	inline static int32_t get_offset_of__seardrea_4() { return static_cast<int32_t>(offsetof(M_steadowgu132_t2582810595, ____seardrea_4)); }
	inline int32_t get__seardrea_4() const { return ____seardrea_4; }
	inline int32_t* get_address_of__seardrea_4() { return &____seardrea_4; }
	inline void set__seardrea_4(int32_t value)
	{
		____seardrea_4 = value;
	}

	inline static int32_t get_offset_of__mallwhelseBooma_5() { return static_cast<int32_t>(offsetof(M_steadowgu132_t2582810595, ____mallwhelseBooma_5)); }
	inline int32_t get__mallwhelseBooma_5() const { return ____mallwhelseBooma_5; }
	inline int32_t* get_address_of__mallwhelseBooma_5() { return &____mallwhelseBooma_5; }
	inline void set__mallwhelseBooma_5(int32_t value)
	{
		____mallwhelseBooma_5 = value;
	}

	inline static int32_t get_offset_of__cosear_6() { return static_cast<int32_t>(offsetof(M_steadowgu132_t2582810595, ____cosear_6)); }
	inline bool get__cosear_6() const { return ____cosear_6; }
	inline bool* get_address_of__cosear_6() { return &____cosear_6; }
	inline void set__cosear_6(bool value)
	{
		____cosear_6 = value;
	}

	inline static int32_t get_offset_of__rerebar_7() { return static_cast<int32_t>(offsetof(M_steadowgu132_t2582810595, ____rerebar_7)); }
	inline int32_t get__rerebar_7() const { return ____rerebar_7; }
	inline int32_t* get_address_of__rerebar_7() { return &____rerebar_7; }
	inline void set__rerebar_7(int32_t value)
	{
		____rerebar_7 = value;
	}

	inline static int32_t get_offset_of__debeCelfall_8() { return static_cast<int32_t>(offsetof(M_steadowgu132_t2582810595, ____debeCelfall_8)); }
	inline String_t* get__debeCelfall_8() const { return ____debeCelfall_8; }
	inline String_t** get_address_of__debeCelfall_8() { return &____debeCelfall_8; }
	inline void set__debeCelfall_8(String_t* value)
	{
		____debeCelfall_8 = value;
		Il2CppCodeGenWriteBarrier(&____debeCelfall_8, value);
	}

	inline static int32_t get_offset_of__pearcir_9() { return static_cast<int32_t>(offsetof(M_steadowgu132_t2582810595, ____pearcir_9)); }
	inline String_t* get__pearcir_9() const { return ____pearcir_9; }
	inline String_t** get_address_of__pearcir_9() { return &____pearcir_9; }
	inline void set__pearcir_9(String_t* value)
	{
		____pearcir_9 = value;
		Il2CppCodeGenWriteBarrier(&____pearcir_9, value);
	}

	inline static int32_t get_offset_of__beva_10() { return static_cast<int32_t>(offsetof(M_steadowgu132_t2582810595, ____beva_10)); }
	inline int32_t get__beva_10() const { return ____beva_10; }
	inline int32_t* get_address_of__beva_10() { return &____beva_10; }
	inline void set__beva_10(int32_t value)
	{
		____beva_10 = value;
	}

	inline static int32_t get_offset_of__cairboqalWhurdahor_11() { return static_cast<int32_t>(offsetof(M_steadowgu132_t2582810595, ____cairboqalWhurdahor_11)); }
	inline uint32_t get__cairboqalWhurdahor_11() const { return ____cairboqalWhurdahor_11; }
	inline uint32_t* get_address_of__cairboqalWhurdahor_11() { return &____cairboqalWhurdahor_11; }
	inline void set__cairboqalWhurdahor_11(uint32_t value)
	{
		____cairboqalWhurdahor_11 = value;
	}

	inline static int32_t get_offset_of__jovouSafachu_12() { return static_cast<int32_t>(offsetof(M_steadowgu132_t2582810595, ____jovouSafachu_12)); }
	inline String_t* get__jovouSafachu_12() const { return ____jovouSafachu_12; }
	inline String_t** get_address_of__jovouSafachu_12() { return &____jovouSafachu_12; }
	inline void set__jovouSafachu_12(String_t* value)
	{
		____jovouSafachu_12 = value;
		Il2CppCodeGenWriteBarrier(&____jovouSafachu_12, value);
	}

	inline static int32_t get_offset_of__kaseamo_13() { return static_cast<int32_t>(offsetof(M_steadowgu132_t2582810595, ____kaseamo_13)); }
	inline String_t* get__kaseamo_13() const { return ____kaseamo_13; }
	inline String_t** get_address_of__kaseamo_13() { return &____kaseamo_13; }
	inline void set__kaseamo_13(String_t* value)
	{
		____kaseamo_13 = value;
		Il2CppCodeGenWriteBarrier(&____kaseamo_13, value);
	}

	inline static int32_t get_offset_of__callpesou_14() { return static_cast<int32_t>(offsetof(M_steadowgu132_t2582810595, ____callpesou_14)); }
	inline String_t* get__callpesou_14() const { return ____callpesou_14; }
	inline String_t** get_address_of__callpesou_14() { return &____callpesou_14; }
	inline void set__callpesou_14(String_t* value)
	{
		____callpesou_14 = value;
		Il2CppCodeGenWriteBarrier(&____callpesou_14, value);
	}

	inline static int32_t get_offset_of__baigouRasjusay_15() { return static_cast<int32_t>(offsetof(M_steadowgu132_t2582810595, ____baigouRasjusay_15)); }
	inline int32_t get__baigouRasjusay_15() const { return ____baigouRasjusay_15; }
	inline int32_t* get_address_of__baigouRasjusay_15() { return &____baigouRasjusay_15; }
	inline void set__baigouRasjusay_15(int32_t value)
	{
		____baigouRasjusay_15 = value;
	}

	inline static int32_t get_offset_of__rayorvor_16() { return static_cast<int32_t>(offsetof(M_steadowgu132_t2582810595, ____rayorvor_16)); }
	inline float get__rayorvor_16() const { return ____rayorvor_16; }
	inline float* get_address_of__rayorvor_16() { return &____rayorvor_16; }
	inline void set__rayorvor_16(float value)
	{
		____rayorvor_16 = value;
	}

	inline static int32_t get_offset_of__sairtemTerejir_17() { return static_cast<int32_t>(offsetof(M_steadowgu132_t2582810595, ____sairtemTerejir_17)); }
	inline bool get__sairtemTerejir_17() const { return ____sairtemTerejir_17; }
	inline bool* get_address_of__sairtemTerejir_17() { return &____sairtemTerejir_17; }
	inline void set__sairtemTerejir_17(bool value)
	{
		____sairtemTerejir_17 = value;
	}

	inline static int32_t get_offset_of__halfurcall_18() { return static_cast<int32_t>(offsetof(M_steadowgu132_t2582810595, ____halfurcall_18)); }
	inline float get__halfurcall_18() const { return ____halfurcall_18; }
	inline float* get_address_of__halfurcall_18() { return &____halfurcall_18; }
	inline void set__halfurcall_18(float value)
	{
		____halfurcall_18 = value;
	}

	inline static int32_t get_offset_of__geretetrallBuhaw_19() { return static_cast<int32_t>(offsetof(M_steadowgu132_t2582810595, ____geretetrallBuhaw_19)); }
	inline bool get__geretetrallBuhaw_19() const { return ____geretetrallBuhaw_19; }
	inline bool* get_address_of__geretetrallBuhaw_19() { return &____geretetrallBuhaw_19; }
	inline void set__geretetrallBuhaw_19(bool value)
	{
		____geretetrallBuhaw_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
