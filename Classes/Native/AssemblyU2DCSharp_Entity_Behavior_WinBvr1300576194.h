﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_Entity_Behavior_IBehavior770859129.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entity.Behavior.WinBvr
struct  WinBvr_t1300576194  : public IBehavior_t770859129
{
public:
	// System.Single Entity.Behavior.WinBvr::winTime
	float ___winTime_2;
	// System.Single Entity.Behavior.WinBvr::idleTime
	float ___idleTime_3;

public:
	inline static int32_t get_offset_of_winTime_2() { return static_cast<int32_t>(offsetof(WinBvr_t1300576194, ___winTime_2)); }
	inline float get_winTime_2() const { return ___winTime_2; }
	inline float* get_address_of_winTime_2() { return &___winTime_2; }
	inline void set_winTime_2(float value)
	{
		___winTime_2 = value;
	}

	inline static int32_t get_offset_of_idleTime_3() { return static_cast<int32_t>(offsetof(WinBvr_t1300576194, ___idleTime_3)); }
	inline float get_idleTime_3() const { return ___idleTime_3; }
	inline float* get_address_of_idleTime_3() { return &___idleTime_3; }
	inline void set_idleTime_3(float value)
	{
		___idleTime_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
