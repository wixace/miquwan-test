﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// InputItem
struct InputItem_t3710611933;
// System.String
struct String_t;
// PuzzleManager
struct PuzzleManager_t2329339247;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PuzzleManager_ResultType1561842103.h"
#include "AssemblyU2DCSharp_PuzzleManager_PropertyType901913455.h"
#include "AssemblyU2DCSharp_InputItem3710611933.h"
#include "AssemblyU2DCSharp_PuzzleManager2329339247.h"

// System.Void InputItem::.ctor()
extern "C"  void InputItem__ctor_m2393205806 (InputItem_t3710611933 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean InputItem::get_IsOpened()
extern "C"  bool InputItem_get_IsOpened_m1489444894 (InputItem_t3710611933 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InputItem::set_IsOpened(System.Boolean)
extern "C"  void InputItem_set_IsOpened_m2072834749 (InputItem_t3710611933 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PuzzleManager/ResultType InputItem::get_resultType()
extern "C"  int32_t InputItem_get_resultType_m1554283216 (InputItem_t3710611933 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InputItem::set_resultType(PuzzleManager/ResultType)
extern "C"  void InputItem_set_resultType_m3036183315 (InputItem_t3710611933 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 InputItem::get_TotalPoint()
extern "C"  int32_t InputItem_get_TotalPoint_m3889324605 (InputItem_t3710611933 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InputItem::set_TotalPoint(System.Int32)
extern "C"  void InputItem_set_TotalPoint_m3936629328 (InputItem_t3710611933 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InputItem::Start()
extern "C"  void InputItem_Start_m1340343598 (InputItem_t3710611933 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InputItem::Init()
extern "C"  void InputItem_Init_m1968399462 (InputItem_t3710611933 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InputItem::SetBasicPoint(System.Int32)
extern "C"  void InputItem_SetBasicPoint_m2575541921 (InputItem_t3710611933 * __this, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InputItem::SetThinkPoint(System.Int32)
extern "C"  void InputItem_SetThinkPoint_m2479691613 (InputItem_t3710611933 * __this, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InputItem::UpdateView()
extern "C"  void InputItem_UpdateView_m2199191332 (InputItem_t3710611933 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InputItem::Show()
extern "C"  void InputItem_Show_m2249331475 (InputItem_t3710611933 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InputItem::Hide()
extern "C"  void InputItem_Hide_m1934989336 (InputItem_t3710611933 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String InputItem::GetChineseName(PuzzleManager/PropertyType)
extern "C"  String_t* InputItem_GetChineseName_m2312294374 (InputItem_t3710611933 * __this, int32_t ___pt0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InputItem::<Start>m__3CB()
extern "C"  void InputItem_U3CStartU3Em__3CB_m771150921 (InputItem_t3710611933 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String InputItem::ilo_GetChineseName1(InputItem,PuzzleManager/PropertyType)
extern "C"  String_t* InputItem_ilo_GetChineseName1_m2528949581 (Il2CppObject * __this /* static, unused */, InputItem_t3710611933 * ____this0, int32_t ___pt1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PuzzleManager/ResultType InputItem::ilo_get_resultType2(InputItem)
extern "C"  int32_t InputItem_ilo_get_resultType2_m792631460 (Il2CppObject * __this /* static, unused */, InputItem_t3710611933 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InputItem::ilo_Open3(PuzzleManager,System.Int32)
extern "C"  void InputItem_ilo_Open3_m964189238 (Il2CppObject * __this /* static, unused */, PuzzleManager_t2329339247 * ____this0, int32_t ___id1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
