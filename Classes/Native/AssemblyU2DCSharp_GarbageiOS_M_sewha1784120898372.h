﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_sewha178
struct  M_sewha178_t4120898372  : public Il2CppObject
{
public:
	// System.UInt32 GarbageiOS.M_sewha178::_mersouWinou
	uint32_t ____mersouWinou_0;
	// System.Single GarbageiOS.M_sewha178::_henuBawhir
	float ____henuBawhir_1;
	// System.Single GarbageiOS.M_sewha178::_dallvawhuDemnadu
	float ____dallvawhuDemnadu_2;
	// System.Boolean GarbageiOS.M_sewha178::_xezair
	bool ____xezair_3;
	// System.String GarbageiOS.M_sewha178::_rasstaymi
	String_t* ____rasstaymi_4;
	// System.UInt32 GarbageiOS.M_sewha178::_qaydrou
	uint32_t ____qaydrou_5;
	// System.Int32 GarbageiOS.M_sewha178::_hallloger
	int32_t ____hallloger_6;
	// System.Single GarbageiOS.M_sewha178::_pidairtuHallno
	float ____pidairtuHallno_7;
	// System.Int32 GarbageiOS.M_sewha178::_wibis
	int32_t ____wibis_8;
	// System.Int32 GarbageiOS.M_sewha178::_zerbidro
	int32_t ____zerbidro_9;
	// System.Single GarbageiOS.M_sewha178::_zurpereXecou
	float ____zurpereXecou_10;
	// System.Single GarbageiOS.M_sewha178::_hakalRoonira
	float ____hakalRoonira_11;
	// System.Int32 GarbageiOS.M_sewha178::_cheeca
	int32_t ____cheeca_12;
	// System.Int32 GarbageiOS.M_sewha178::_pursefurDemtrorqa
	int32_t ____pursefurDemtrorqa_13;
	// System.UInt32 GarbageiOS.M_sewha178::_zurgeaCerdel
	uint32_t ____zurgeaCerdel_14;
	// System.Boolean GarbageiOS.M_sewha178::_woupiryi
	bool ____woupiryi_15;
	// System.Boolean GarbageiOS.M_sewha178::_restear
	bool ____restear_16;
	// System.UInt32 GarbageiOS.M_sewha178::_boubearJace
	uint32_t ____boubearJace_17;
	// System.Boolean GarbageiOS.M_sewha178::_dreresuparBomeqea
	bool ____dreresuparBomeqea_18;
	// System.String GarbageiOS.M_sewha178::_sawhowlo
	String_t* ____sawhowlo_19;
	// System.Single GarbageiOS.M_sewha178::_kotruduSote
	float ____kotruduSote_20;
	// System.Boolean GarbageiOS.M_sewha178::_fayqairerMistayma
	bool ____fayqairerMistayma_21;
	// System.UInt32 GarbageiOS.M_sewha178::_risdaycelSotebi
	uint32_t ____risdaycelSotebi_22;
	// System.Single GarbageiOS.M_sewha178::_peageayemBikere
	float ____peageayemBikere_23;
	// System.UInt32 GarbageiOS.M_sewha178::_zomezisWhowrorti
	uint32_t ____zomezisWhowrorti_24;
	// System.Single GarbageiOS.M_sewha178::_boodircelStastoo
	float ____boodircelStastoo_25;
	// System.UInt32 GarbageiOS.M_sewha178::_fouger
	uint32_t ____fouger_26;
	// System.Int32 GarbageiOS.M_sewha178::_fole
	int32_t ____fole_27;
	// System.Boolean GarbageiOS.M_sewha178::_deno
	bool ____deno_28;
	// System.Single GarbageiOS.M_sewha178::_bonelbiQorai
	float ____bonelbiQorai_29;
	// System.Boolean GarbageiOS.M_sewha178::_kasir
	bool ____kasir_30;
	// System.UInt32 GarbageiOS.M_sewha178::_yemturhirHemu
	uint32_t ____yemturhirHemu_31;
	// System.Int32 GarbageiOS.M_sewha178::_koodowhuNepee
	int32_t ____koodowhuNepee_32;
	// System.UInt32 GarbageiOS.M_sewha178::_saitrear
	uint32_t ____saitrear_33;
	// System.Boolean GarbageiOS.M_sewha178::_qemsoCeba
	bool ____qemsoCeba_34;
	// System.Int32 GarbageiOS.M_sewha178::_whalterLineri
	int32_t ____whalterLineri_35;
	// System.Boolean GarbageiOS.M_sewha178::_pideBairji
	bool ____pideBairji_36;
	// System.Boolean GarbageiOS.M_sewha178::_pairpe
	bool ____pairpe_37;
	// System.Single GarbageiOS.M_sewha178::_vohowSawbou
	float ____vohowSawbou_38;
	// System.Int32 GarbageiOS.M_sewha178::_pelja
	int32_t ____pelja_39;
	// System.Boolean GarbageiOS.M_sewha178::_memtinereZernourair
	bool ____memtinereZernourair_40;
	// System.Int32 GarbageiOS.M_sewha178::_draineeBawdooti
	int32_t ____draineeBawdooti_41;
	// System.Int32 GarbageiOS.M_sewha178::_sedurjas
	int32_t ____sedurjas_42;
	// System.Boolean GarbageiOS.M_sewha178::_marretre
	bool ____marretre_43;
	// System.String GarbageiOS.M_sewha178::_laguboJepeeca
	String_t* ____laguboJepeeca_44;
	// System.UInt32 GarbageiOS.M_sewha178::_gallkawwhur
	uint32_t ____gallkawwhur_45;
	// System.UInt32 GarbageiOS.M_sewha178::_hairjalere
	uint32_t ____hairjalere_46;

public:
	inline static int32_t get_offset_of__mersouWinou_0() { return static_cast<int32_t>(offsetof(M_sewha178_t4120898372, ____mersouWinou_0)); }
	inline uint32_t get__mersouWinou_0() const { return ____mersouWinou_0; }
	inline uint32_t* get_address_of__mersouWinou_0() { return &____mersouWinou_0; }
	inline void set__mersouWinou_0(uint32_t value)
	{
		____mersouWinou_0 = value;
	}

	inline static int32_t get_offset_of__henuBawhir_1() { return static_cast<int32_t>(offsetof(M_sewha178_t4120898372, ____henuBawhir_1)); }
	inline float get__henuBawhir_1() const { return ____henuBawhir_1; }
	inline float* get_address_of__henuBawhir_1() { return &____henuBawhir_1; }
	inline void set__henuBawhir_1(float value)
	{
		____henuBawhir_1 = value;
	}

	inline static int32_t get_offset_of__dallvawhuDemnadu_2() { return static_cast<int32_t>(offsetof(M_sewha178_t4120898372, ____dallvawhuDemnadu_2)); }
	inline float get__dallvawhuDemnadu_2() const { return ____dallvawhuDemnadu_2; }
	inline float* get_address_of__dallvawhuDemnadu_2() { return &____dallvawhuDemnadu_2; }
	inline void set__dallvawhuDemnadu_2(float value)
	{
		____dallvawhuDemnadu_2 = value;
	}

	inline static int32_t get_offset_of__xezair_3() { return static_cast<int32_t>(offsetof(M_sewha178_t4120898372, ____xezair_3)); }
	inline bool get__xezair_3() const { return ____xezair_3; }
	inline bool* get_address_of__xezair_3() { return &____xezair_3; }
	inline void set__xezair_3(bool value)
	{
		____xezair_3 = value;
	}

	inline static int32_t get_offset_of__rasstaymi_4() { return static_cast<int32_t>(offsetof(M_sewha178_t4120898372, ____rasstaymi_4)); }
	inline String_t* get__rasstaymi_4() const { return ____rasstaymi_4; }
	inline String_t** get_address_of__rasstaymi_4() { return &____rasstaymi_4; }
	inline void set__rasstaymi_4(String_t* value)
	{
		____rasstaymi_4 = value;
		Il2CppCodeGenWriteBarrier(&____rasstaymi_4, value);
	}

	inline static int32_t get_offset_of__qaydrou_5() { return static_cast<int32_t>(offsetof(M_sewha178_t4120898372, ____qaydrou_5)); }
	inline uint32_t get__qaydrou_5() const { return ____qaydrou_5; }
	inline uint32_t* get_address_of__qaydrou_5() { return &____qaydrou_5; }
	inline void set__qaydrou_5(uint32_t value)
	{
		____qaydrou_5 = value;
	}

	inline static int32_t get_offset_of__hallloger_6() { return static_cast<int32_t>(offsetof(M_sewha178_t4120898372, ____hallloger_6)); }
	inline int32_t get__hallloger_6() const { return ____hallloger_6; }
	inline int32_t* get_address_of__hallloger_6() { return &____hallloger_6; }
	inline void set__hallloger_6(int32_t value)
	{
		____hallloger_6 = value;
	}

	inline static int32_t get_offset_of__pidairtuHallno_7() { return static_cast<int32_t>(offsetof(M_sewha178_t4120898372, ____pidairtuHallno_7)); }
	inline float get__pidairtuHallno_7() const { return ____pidairtuHallno_7; }
	inline float* get_address_of__pidairtuHallno_7() { return &____pidairtuHallno_7; }
	inline void set__pidairtuHallno_7(float value)
	{
		____pidairtuHallno_7 = value;
	}

	inline static int32_t get_offset_of__wibis_8() { return static_cast<int32_t>(offsetof(M_sewha178_t4120898372, ____wibis_8)); }
	inline int32_t get__wibis_8() const { return ____wibis_8; }
	inline int32_t* get_address_of__wibis_8() { return &____wibis_8; }
	inline void set__wibis_8(int32_t value)
	{
		____wibis_8 = value;
	}

	inline static int32_t get_offset_of__zerbidro_9() { return static_cast<int32_t>(offsetof(M_sewha178_t4120898372, ____zerbidro_9)); }
	inline int32_t get__zerbidro_9() const { return ____zerbidro_9; }
	inline int32_t* get_address_of__zerbidro_9() { return &____zerbidro_9; }
	inline void set__zerbidro_9(int32_t value)
	{
		____zerbidro_9 = value;
	}

	inline static int32_t get_offset_of__zurpereXecou_10() { return static_cast<int32_t>(offsetof(M_sewha178_t4120898372, ____zurpereXecou_10)); }
	inline float get__zurpereXecou_10() const { return ____zurpereXecou_10; }
	inline float* get_address_of__zurpereXecou_10() { return &____zurpereXecou_10; }
	inline void set__zurpereXecou_10(float value)
	{
		____zurpereXecou_10 = value;
	}

	inline static int32_t get_offset_of__hakalRoonira_11() { return static_cast<int32_t>(offsetof(M_sewha178_t4120898372, ____hakalRoonira_11)); }
	inline float get__hakalRoonira_11() const { return ____hakalRoonira_11; }
	inline float* get_address_of__hakalRoonira_11() { return &____hakalRoonira_11; }
	inline void set__hakalRoonira_11(float value)
	{
		____hakalRoonira_11 = value;
	}

	inline static int32_t get_offset_of__cheeca_12() { return static_cast<int32_t>(offsetof(M_sewha178_t4120898372, ____cheeca_12)); }
	inline int32_t get__cheeca_12() const { return ____cheeca_12; }
	inline int32_t* get_address_of__cheeca_12() { return &____cheeca_12; }
	inline void set__cheeca_12(int32_t value)
	{
		____cheeca_12 = value;
	}

	inline static int32_t get_offset_of__pursefurDemtrorqa_13() { return static_cast<int32_t>(offsetof(M_sewha178_t4120898372, ____pursefurDemtrorqa_13)); }
	inline int32_t get__pursefurDemtrorqa_13() const { return ____pursefurDemtrorqa_13; }
	inline int32_t* get_address_of__pursefurDemtrorqa_13() { return &____pursefurDemtrorqa_13; }
	inline void set__pursefurDemtrorqa_13(int32_t value)
	{
		____pursefurDemtrorqa_13 = value;
	}

	inline static int32_t get_offset_of__zurgeaCerdel_14() { return static_cast<int32_t>(offsetof(M_sewha178_t4120898372, ____zurgeaCerdel_14)); }
	inline uint32_t get__zurgeaCerdel_14() const { return ____zurgeaCerdel_14; }
	inline uint32_t* get_address_of__zurgeaCerdel_14() { return &____zurgeaCerdel_14; }
	inline void set__zurgeaCerdel_14(uint32_t value)
	{
		____zurgeaCerdel_14 = value;
	}

	inline static int32_t get_offset_of__woupiryi_15() { return static_cast<int32_t>(offsetof(M_sewha178_t4120898372, ____woupiryi_15)); }
	inline bool get__woupiryi_15() const { return ____woupiryi_15; }
	inline bool* get_address_of__woupiryi_15() { return &____woupiryi_15; }
	inline void set__woupiryi_15(bool value)
	{
		____woupiryi_15 = value;
	}

	inline static int32_t get_offset_of__restear_16() { return static_cast<int32_t>(offsetof(M_sewha178_t4120898372, ____restear_16)); }
	inline bool get__restear_16() const { return ____restear_16; }
	inline bool* get_address_of__restear_16() { return &____restear_16; }
	inline void set__restear_16(bool value)
	{
		____restear_16 = value;
	}

	inline static int32_t get_offset_of__boubearJace_17() { return static_cast<int32_t>(offsetof(M_sewha178_t4120898372, ____boubearJace_17)); }
	inline uint32_t get__boubearJace_17() const { return ____boubearJace_17; }
	inline uint32_t* get_address_of__boubearJace_17() { return &____boubearJace_17; }
	inline void set__boubearJace_17(uint32_t value)
	{
		____boubearJace_17 = value;
	}

	inline static int32_t get_offset_of__dreresuparBomeqea_18() { return static_cast<int32_t>(offsetof(M_sewha178_t4120898372, ____dreresuparBomeqea_18)); }
	inline bool get__dreresuparBomeqea_18() const { return ____dreresuparBomeqea_18; }
	inline bool* get_address_of__dreresuparBomeqea_18() { return &____dreresuparBomeqea_18; }
	inline void set__dreresuparBomeqea_18(bool value)
	{
		____dreresuparBomeqea_18 = value;
	}

	inline static int32_t get_offset_of__sawhowlo_19() { return static_cast<int32_t>(offsetof(M_sewha178_t4120898372, ____sawhowlo_19)); }
	inline String_t* get__sawhowlo_19() const { return ____sawhowlo_19; }
	inline String_t** get_address_of__sawhowlo_19() { return &____sawhowlo_19; }
	inline void set__sawhowlo_19(String_t* value)
	{
		____sawhowlo_19 = value;
		Il2CppCodeGenWriteBarrier(&____sawhowlo_19, value);
	}

	inline static int32_t get_offset_of__kotruduSote_20() { return static_cast<int32_t>(offsetof(M_sewha178_t4120898372, ____kotruduSote_20)); }
	inline float get__kotruduSote_20() const { return ____kotruduSote_20; }
	inline float* get_address_of__kotruduSote_20() { return &____kotruduSote_20; }
	inline void set__kotruduSote_20(float value)
	{
		____kotruduSote_20 = value;
	}

	inline static int32_t get_offset_of__fayqairerMistayma_21() { return static_cast<int32_t>(offsetof(M_sewha178_t4120898372, ____fayqairerMistayma_21)); }
	inline bool get__fayqairerMistayma_21() const { return ____fayqairerMistayma_21; }
	inline bool* get_address_of__fayqairerMistayma_21() { return &____fayqairerMistayma_21; }
	inline void set__fayqairerMistayma_21(bool value)
	{
		____fayqairerMistayma_21 = value;
	}

	inline static int32_t get_offset_of__risdaycelSotebi_22() { return static_cast<int32_t>(offsetof(M_sewha178_t4120898372, ____risdaycelSotebi_22)); }
	inline uint32_t get__risdaycelSotebi_22() const { return ____risdaycelSotebi_22; }
	inline uint32_t* get_address_of__risdaycelSotebi_22() { return &____risdaycelSotebi_22; }
	inline void set__risdaycelSotebi_22(uint32_t value)
	{
		____risdaycelSotebi_22 = value;
	}

	inline static int32_t get_offset_of__peageayemBikere_23() { return static_cast<int32_t>(offsetof(M_sewha178_t4120898372, ____peageayemBikere_23)); }
	inline float get__peageayemBikere_23() const { return ____peageayemBikere_23; }
	inline float* get_address_of__peageayemBikere_23() { return &____peageayemBikere_23; }
	inline void set__peageayemBikere_23(float value)
	{
		____peageayemBikere_23 = value;
	}

	inline static int32_t get_offset_of__zomezisWhowrorti_24() { return static_cast<int32_t>(offsetof(M_sewha178_t4120898372, ____zomezisWhowrorti_24)); }
	inline uint32_t get__zomezisWhowrorti_24() const { return ____zomezisWhowrorti_24; }
	inline uint32_t* get_address_of__zomezisWhowrorti_24() { return &____zomezisWhowrorti_24; }
	inline void set__zomezisWhowrorti_24(uint32_t value)
	{
		____zomezisWhowrorti_24 = value;
	}

	inline static int32_t get_offset_of__boodircelStastoo_25() { return static_cast<int32_t>(offsetof(M_sewha178_t4120898372, ____boodircelStastoo_25)); }
	inline float get__boodircelStastoo_25() const { return ____boodircelStastoo_25; }
	inline float* get_address_of__boodircelStastoo_25() { return &____boodircelStastoo_25; }
	inline void set__boodircelStastoo_25(float value)
	{
		____boodircelStastoo_25 = value;
	}

	inline static int32_t get_offset_of__fouger_26() { return static_cast<int32_t>(offsetof(M_sewha178_t4120898372, ____fouger_26)); }
	inline uint32_t get__fouger_26() const { return ____fouger_26; }
	inline uint32_t* get_address_of__fouger_26() { return &____fouger_26; }
	inline void set__fouger_26(uint32_t value)
	{
		____fouger_26 = value;
	}

	inline static int32_t get_offset_of__fole_27() { return static_cast<int32_t>(offsetof(M_sewha178_t4120898372, ____fole_27)); }
	inline int32_t get__fole_27() const { return ____fole_27; }
	inline int32_t* get_address_of__fole_27() { return &____fole_27; }
	inline void set__fole_27(int32_t value)
	{
		____fole_27 = value;
	}

	inline static int32_t get_offset_of__deno_28() { return static_cast<int32_t>(offsetof(M_sewha178_t4120898372, ____deno_28)); }
	inline bool get__deno_28() const { return ____deno_28; }
	inline bool* get_address_of__deno_28() { return &____deno_28; }
	inline void set__deno_28(bool value)
	{
		____deno_28 = value;
	}

	inline static int32_t get_offset_of__bonelbiQorai_29() { return static_cast<int32_t>(offsetof(M_sewha178_t4120898372, ____bonelbiQorai_29)); }
	inline float get__bonelbiQorai_29() const { return ____bonelbiQorai_29; }
	inline float* get_address_of__bonelbiQorai_29() { return &____bonelbiQorai_29; }
	inline void set__bonelbiQorai_29(float value)
	{
		____bonelbiQorai_29 = value;
	}

	inline static int32_t get_offset_of__kasir_30() { return static_cast<int32_t>(offsetof(M_sewha178_t4120898372, ____kasir_30)); }
	inline bool get__kasir_30() const { return ____kasir_30; }
	inline bool* get_address_of__kasir_30() { return &____kasir_30; }
	inline void set__kasir_30(bool value)
	{
		____kasir_30 = value;
	}

	inline static int32_t get_offset_of__yemturhirHemu_31() { return static_cast<int32_t>(offsetof(M_sewha178_t4120898372, ____yemturhirHemu_31)); }
	inline uint32_t get__yemturhirHemu_31() const { return ____yemturhirHemu_31; }
	inline uint32_t* get_address_of__yemturhirHemu_31() { return &____yemturhirHemu_31; }
	inline void set__yemturhirHemu_31(uint32_t value)
	{
		____yemturhirHemu_31 = value;
	}

	inline static int32_t get_offset_of__koodowhuNepee_32() { return static_cast<int32_t>(offsetof(M_sewha178_t4120898372, ____koodowhuNepee_32)); }
	inline int32_t get__koodowhuNepee_32() const { return ____koodowhuNepee_32; }
	inline int32_t* get_address_of__koodowhuNepee_32() { return &____koodowhuNepee_32; }
	inline void set__koodowhuNepee_32(int32_t value)
	{
		____koodowhuNepee_32 = value;
	}

	inline static int32_t get_offset_of__saitrear_33() { return static_cast<int32_t>(offsetof(M_sewha178_t4120898372, ____saitrear_33)); }
	inline uint32_t get__saitrear_33() const { return ____saitrear_33; }
	inline uint32_t* get_address_of__saitrear_33() { return &____saitrear_33; }
	inline void set__saitrear_33(uint32_t value)
	{
		____saitrear_33 = value;
	}

	inline static int32_t get_offset_of__qemsoCeba_34() { return static_cast<int32_t>(offsetof(M_sewha178_t4120898372, ____qemsoCeba_34)); }
	inline bool get__qemsoCeba_34() const { return ____qemsoCeba_34; }
	inline bool* get_address_of__qemsoCeba_34() { return &____qemsoCeba_34; }
	inline void set__qemsoCeba_34(bool value)
	{
		____qemsoCeba_34 = value;
	}

	inline static int32_t get_offset_of__whalterLineri_35() { return static_cast<int32_t>(offsetof(M_sewha178_t4120898372, ____whalterLineri_35)); }
	inline int32_t get__whalterLineri_35() const { return ____whalterLineri_35; }
	inline int32_t* get_address_of__whalterLineri_35() { return &____whalterLineri_35; }
	inline void set__whalterLineri_35(int32_t value)
	{
		____whalterLineri_35 = value;
	}

	inline static int32_t get_offset_of__pideBairji_36() { return static_cast<int32_t>(offsetof(M_sewha178_t4120898372, ____pideBairji_36)); }
	inline bool get__pideBairji_36() const { return ____pideBairji_36; }
	inline bool* get_address_of__pideBairji_36() { return &____pideBairji_36; }
	inline void set__pideBairji_36(bool value)
	{
		____pideBairji_36 = value;
	}

	inline static int32_t get_offset_of__pairpe_37() { return static_cast<int32_t>(offsetof(M_sewha178_t4120898372, ____pairpe_37)); }
	inline bool get__pairpe_37() const { return ____pairpe_37; }
	inline bool* get_address_of__pairpe_37() { return &____pairpe_37; }
	inline void set__pairpe_37(bool value)
	{
		____pairpe_37 = value;
	}

	inline static int32_t get_offset_of__vohowSawbou_38() { return static_cast<int32_t>(offsetof(M_sewha178_t4120898372, ____vohowSawbou_38)); }
	inline float get__vohowSawbou_38() const { return ____vohowSawbou_38; }
	inline float* get_address_of__vohowSawbou_38() { return &____vohowSawbou_38; }
	inline void set__vohowSawbou_38(float value)
	{
		____vohowSawbou_38 = value;
	}

	inline static int32_t get_offset_of__pelja_39() { return static_cast<int32_t>(offsetof(M_sewha178_t4120898372, ____pelja_39)); }
	inline int32_t get__pelja_39() const { return ____pelja_39; }
	inline int32_t* get_address_of__pelja_39() { return &____pelja_39; }
	inline void set__pelja_39(int32_t value)
	{
		____pelja_39 = value;
	}

	inline static int32_t get_offset_of__memtinereZernourair_40() { return static_cast<int32_t>(offsetof(M_sewha178_t4120898372, ____memtinereZernourair_40)); }
	inline bool get__memtinereZernourair_40() const { return ____memtinereZernourair_40; }
	inline bool* get_address_of__memtinereZernourair_40() { return &____memtinereZernourair_40; }
	inline void set__memtinereZernourair_40(bool value)
	{
		____memtinereZernourair_40 = value;
	}

	inline static int32_t get_offset_of__draineeBawdooti_41() { return static_cast<int32_t>(offsetof(M_sewha178_t4120898372, ____draineeBawdooti_41)); }
	inline int32_t get__draineeBawdooti_41() const { return ____draineeBawdooti_41; }
	inline int32_t* get_address_of__draineeBawdooti_41() { return &____draineeBawdooti_41; }
	inline void set__draineeBawdooti_41(int32_t value)
	{
		____draineeBawdooti_41 = value;
	}

	inline static int32_t get_offset_of__sedurjas_42() { return static_cast<int32_t>(offsetof(M_sewha178_t4120898372, ____sedurjas_42)); }
	inline int32_t get__sedurjas_42() const { return ____sedurjas_42; }
	inline int32_t* get_address_of__sedurjas_42() { return &____sedurjas_42; }
	inline void set__sedurjas_42(int32_t value)
	{
		____sedurjas_42 = value;
	}

	inline static int32_t get_offset_of__marretre_43() { return static_cast<int32_t>(offsetof(M_sewha178_t4120898372, ____marretre_43)); }
	inline bool get__marretre_43() const { return ____marretre_43; }
	inline bool* get_address_of__marretre_43() { return &____marretre_43; }
	inline void set__marretre_43(bool value)
	{
		____marretre_43 = value;
	}

	inline static int32_t get_offset_of__laguboJepeeca_44() { return static_cast<int32_t>(offsetof(M_sewha178_t4120898372, ____laguboJepeeca_44)); }
	inline String_t* get__laguboJepeeca_44() const { return ____laguboJepeeca_44; }
	inline String_t** get_address_of__laguboJepeeca_44() { return &____laguboJepeeca_44; }
	inline void set__laguboJepeeca_44(String_t* value)
	{
		____laguboJepeeca_44 = value;
		Il2CppCodeGenWriteBarrier(&____laguboJepeeca_44, value);
	}

	inline static int32_t get_offset_of__gallkawwhur_45() { return static_cast<int32_t>(offsetof(M_sewha178_t4120898372, ____gallkawwhur_45)); }
	inline uint32_t get__gallkawwhur_45() const { return ____gallkawwhur_45; }
	inline uint32_t* get_address_of__gallkawwhur_45() { return &____gallkawwhur_45; }
	inline void set__gallkawwhur_45(uint32_t value)
	{
		____gallkawwhur_45 = value;
	}

	inline static int32_t get_offset_of__hairjalere_46() { return static_cast<int32_t>(offsetof(M_sewha178_t4120898372, ____hairjalere_46)); }
	inline uint32_t get__hairjalere_46() const { return ____hairjalere_46; }
	inline uint32_t* get_address_of__hairjalere_46() { return &____hairjalere_46; }
	inline void set__hairjalere_46(uint32_t value)
	{
		____hairjalere_46 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
