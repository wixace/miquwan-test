﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Entity.Behavior.NpcAutoFightBvr
struct NpcAutoFightBvr_t3149258780;
// NpcMgr
struct NpcMgr_t2339534743;
// System.Collections.Generic.List`1<CombatEntity>
struct List_1_t2052323047;
// CombatEntity
struct CombatEntity_t684137495;
// Entity.Behavior.IBehavior
struct IBehavior_t770859129;
// LevelConfigManager
struct LevelConfigManager_t657947911;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Entity_Behavior_EBehaviorID1238533136.h"
#include "AssemblyU2DCSharp_NpcMgr2339534743.h"
#include "AssemblyU2DCSharp_CombatEntity684137495.h"
#include "AssemblyU2DCSharp_Entity_Behavior_IBehavior770859129.h"
#include "AssemblyU2DCSharp_LevelConfigManager657947911.h"

// System.Void Entity.Behavior.NpcAutoFightBvr::.ctor()
extern "C"  void NpcAutoFightBvr__ctor_m508880222 (NpcAutoFightBvr_t3149258780 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Entity.Behavior.EBehaviorID Entity.Behavior.NpcAutoFightBvr::get_id()
extern "C"  uint8_t NpcAutoFightBvr_get_id_m3702855576 (NpcAutoFightBvr_t3149258780 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.NpcAutoFightBvr::Reason()
extern "C"  void NpcAutoFightBvr_Reason_m2441773098 (NpcAutoFightBvr_t3149258780 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.NpcAutoFightBvr::Action()
extern "C"  void NpcAutoFightBvr_Action_m1638479772 (NpcAutoFightBvr_t3149258780 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.NpcAutoFightBvr::DoEntering()
extern "C"  void NpcAutoFightBvr_DoEntering_m1069650651 (NpcAutoFightBvr_t3149258780 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.NpcAutoFightBvr::DoLeaving()
extern "C"  void NpcAutoFightBvr_DoLeaving_m2141983077 (NpcAutoFightBvr_t3149258780 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// NpcMgr Entity.Behavior.NpcAutoFightBvr::ilo_get_instance1()
extern "C"  NpcMgr_t2339534743 * NpcAutoFightBvr_ilo_get_instance1_m287736226 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CombatEntity> Entity.Behavior.NpcAutoFightBvr::ilo_GetAllMonsters2(NpcMgr)
extern "C"  List_1_t2052323047 * NpcAutoFightBvr_ilo_GetAllMonsters2_m3744667306 (Il2CppObject * __this /* static, unused */, NpcMgr_t2339534743 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Entity.Behavior.NpcAutoFightBvr::ilo_get_isDeath3(CombatEntity)
extern "C"  bool NpcAutoFightBvr_ilo_get_isDeath3_m684242016 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CombatEntity Entity.Behavior.NpcAutoFightBvr::ilo_get_entity4(Entity.Behavior.IBehavior)
extern "C"  CombatEntity_t684137495 * NpcAutoFightBvr_ilo_get_entity4_m2098430065 (Il2CppObject * __this /* static, unused */, IBehavior_t770859129 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Entity.Behavior.NpcAutoFightBvr::ilo_GetTotalRounds5(LevelConfigManager,System.Int32)
extern "C"  int32_t NpcAutoFightBvr_ilo_GetTotalRounds5_m1104487173 (Il2CppObject * __this /* static, unused */, LevelConfigManager_t657947911 * ____this0, int32_t ___level1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
