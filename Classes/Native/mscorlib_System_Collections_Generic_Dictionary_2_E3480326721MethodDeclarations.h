﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>
struct Dictionary_2_t2163003329;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3480326721.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22061784035.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24287931429.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m2292393966_gshared (Enumerator_t3480326721 * __this, Dictionary_2_t2163003329 * ___dictionary0, const MethodInfo* method);
#define Enumerator__ctor_m2292393966(__this, ___dictionary0, method) ((  void (*) (Enumerator_t3480326721 *, Dictionary_2_t2163003329 *, const MethodInfo*))Enumerator__ctor_m2292393966_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2350395251_gshared (Enumerator_t3480326721 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m2350395251(__this, method) ((  Il2CppObject * (*) (Enumerator_t3480326721 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2350395251_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1288278023_gshared (Enumerator_t3480326721 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1288278023(__this, method) ((  void (*) (Enumerator_t3480326721 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1288278023_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t1751606614  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m290819920_gshared (Enumerator_t3480326721 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m290819920(__this, method) ((  DictionaryEntry_t1751606614  (*) (Enumerator_t3480326721 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m290819920_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m4025797135_gshared (Enumerator_t3480326721 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m4025797135(__this, method) ((  Il2CppObject * (*) (Enumerator_t3480326721 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m4025797135_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m73949409_gshared (Enumerator_t3480326721 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m73949409(__this, method) ((  Il2CppObject * (*) (Enumerator_t3480326721 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m73949409_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3325100915_gshared (Enumerator_t3480326721 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m3325100915(__this, method) ((  bool (*) (Enumerator_t3480326721 *, const MethodInfo*))Enumerator_MoveNext_m3325100915_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::get_Current()
extern "C"  KeyValuePair_2_t2061784035  Enumerator_get_Current_m3112427485_gshared (Enumerator_t3480326721 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m3112427485(__this, method) ((  KeyValuePair_2_t2061784035  (*) (Enumerator_t3480326721 *, const MethodInfo*))Enumerator_get_Current_m3112427485_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::get_CurrentKey()
extern "C"  Il2CppObject * Enumerator_get_CurrentKey_m1064890112_gshared (Enumerator_t3480326721 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m1064890112(__this, method) ((  Il2CppObject * (*) (Enumerator_t3480326721 *, const MethodInfo*))Enumerator_get_CurrentKey_m1064890112_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::get_CurrentValue()
extern "C"  KeyValuePair_2_t4287931429  Enumerator_get_CurrentValue_m2121659812_gshared (Enumerator_t3480326721 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m2121659812(__this, method) ((  KeyValuePair_2_t4287931429  (*) (Enumerator_t3480326721 *, const MethodInfo*))Enumerator_get_CurrentValue_m2121659812_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::Reset()
extern "C"  void Enumerator_Reset_m1469300032_gshared (Enumerator_t3480326721 * __this, const MethodInfo* method);
#define Enumerator_Reset_m1469300032(__this, method) ((  void (*) (Enumerator_t3480326721 *, const MethodInfo*))Enumerator_Reset_m1469300032_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::VerifyState()
extern "C"  void Enumerator_VerifyState_m2192842057_gshared (Enumerator_t3480326721 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m2192842057(__this, method) ((  void (*) (Enumerator_t3480326721 *, const MethodInfo*))Enumerator_VerifyState_m2192842057_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::VerifyCurrent()
extern "C"  void Enumerator_VerifyCurrent_m1237146225_gshared (Enumerator_t3480326721 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m1237146225(__this, method) ((  void (*) (Enumerator_t3480326721 *, const MethodInfo*))Enumerator_VerifyCurrent_m1237146225_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::Dispose()
extern "C"  void Enumerator_Dispose_m1476519440_gshared (Enumerator_t3480326721 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1476519440(__this, method) ((  void (*) (Enumerator_t3480326721 *, const MethodInfo*))Enumerator_Dispose_m1476519440_gshared)(__this, method)
