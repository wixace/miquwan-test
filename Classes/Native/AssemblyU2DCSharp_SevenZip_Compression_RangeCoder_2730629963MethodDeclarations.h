﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SevenZip.Compression.RangeCoder.Decoder
struct Decoder_t1102840654;
// SevenZip.Compression.RangeCoder.BitDecoder[]
struct BitDecoderU5BU5D_t1049749620;
// SevenZip.Compression.RangeCoder.BitTreeDecoder
struct BitTreeDecoder_t2730629963;
struct BitTreeDecoder_t2730629963_marshaled_pinvoke;
struct BitTreeDecoder_t2730629963_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SevenZip_Compression_RangeCoder_2730629963.h"
#include "AssemblyU2DCSharp_SevenZip_Compression_RangeCoder_1102840654.h"
#include "AssemblyU2DCSharp_SevenZip_Compression_RangeCoder_4202293321.h"

// System.Void SevenZip.Compression.RangeCoder.BitTreeDecoder::.ctor(System.Int32)
extern "C"  void BitTreeDecoder__ctor_m2304127343 (BitTreeDecoder_t2730629963 * __this, int32_t ___numBitLevels0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.RangeCoder.BitTreeDecoder::Init()
extern "C"  void BitTreeDecoder_Init_m3373807990 (BitTreeDecoder_t2730629963 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 SevenZip.Compression.RangeCoder.BitTreeDecoder::Decode(SevenZip.Compression.RangeCoder.Decoder)
extern "C"  uint32_t BitTreeDecoder_Decode_m4206382275 (BitTreeDecoder_t2730629963 * __this, Decoder_t1102840654 * ___rangeDecoder0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 SevenZip.Compression.RangeCoder.BitTreeDecoder::ReverseDecode(SevenZip.Compression.RangeCoder.Decoder)
extern "C"  uint32_t BitTreeDecoder_ReverseDecode_m2231053313 (BitTreeDecoder_t2730629963 * __this, Decoder_t1102840654 * ___rangeDecoder0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 SevenZip.Compression.RangeCoder.BitTreeDecoder::ReverseDecode(SevenZip.Compression.RangeCoder.BitDecoder[],System.UInt32,SevenZip.Compression.RangeCoder.Decoder,System.Int32)
extern "C"  uint32_t BitTreeDecoder_ReverseDecode_m492949507 (Il2CppObject * __this /* static, unused */, BitDecoderU5BU5D_t1049749620* ___Models0, uint32_t ___startIndex1, Decoder_t1102840654 * ___rangeDecoder2, int32_t ___NumBitLevels3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.RangeCoder.BitTreeDecoder::ilo_Init1(SevenZip.Compression.RangeCoder.BitDecoder&)
extern "C"  void BitTreeDecoder_ilo_Init1_m2314302621 (Il2CppObject * __this /* static, unused */, BitDecoder_t4202293321 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct BitTreeDecoder_t2730629963;
struct BitTreeDecoder_t2730629963_marshaled_pinvoke;

extern "C" void BitTreeDecoder_t2730629963_marshal_pinvoke(const BitTreeDecoder_t2730629963& unmarshaled, BitTreeDecoder_t2730629963_marshaled_pinvoke& marshaled);
extern "C" void BitTreeDecoder_t2730629963_marshal_pinvoke_back(const BitTreeDecoder_t2730629963_marshaled_pinvoke& marshaled, BitTreeDecoder_t2730629963& unmarshaled);
extern "C" void BitTreeDecoder_t2730629963_marshal_pinvoke_cleanup(BitTreeDecoder_t2730629963_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct BitTreeDecoder_t2730629963;
struct BitTreeDecoder_t2730629963_marshaled_com;

extern "C" void BitTreeDecoder_t2730629963_marshal_com(const BitTreeDecoder_t2730629963& unmarshaled, BitTreeDecoder_t2730629963_marshaled_com& marshaled);
extern "C" void BitTreeDecoder_t2730629963_marshal_com_back(const BitTreeDecoder_t2730629963_marshaled_com& marshaled, BitTreeDecoder_t2730629963& unmarshaled);
extern "C" void BitTreeDecoder_t2730629963_marshal_com_cleanup(BitTreeDecoder_t2730629963_marshaled_com& marshaled);
