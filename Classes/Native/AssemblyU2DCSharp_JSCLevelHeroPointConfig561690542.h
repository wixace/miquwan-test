﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ProtoBuf.IExtension
struct IExtension_t1606339106;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JSCLevelHeroPointConfig
struct  JSCLevelHeroPointConfig_t561690542  : public Il2CppObject
{
public:
	// System.Single JSCLevelHeroPointConfig::_x
	float ____x_0;
	// System.Single JSCLevelHeroPointConfig::_y
	float ____y_1;
	// System.Single JSCLevelHeroPointConfig::_z
	float ____z_2;
	// System.Single JSCLevelHeroPointConfig::_rot
	float ____rot_3;
	// ProtoBuf.IExtension JSCLevelHeroPointConfig::extensionObject
	Il2CppObject * ___extensionObject_4;

public:
	inline static int32_t get_offset_of__x_0() { return static_cast<int32_t>(offsetof(JSCLevelHeroPointConfig_t561690542, ____x_0)); }
	inline float get__x_0() const { return ____x_0; }
	inline float* get_address_of__x_0() { return &____x_0; }
	inline void set__x_0(float value)
	{
		____x_0 = value;
	}

	inline static int32_t get_offset_of__y_1() { return static_cast<int32_t>(offsetof(JSCLevelHeroPointConfig_t561690542, ____y_1)); }
	inline float get__y_1() const { return ____y_1; }
	inline float* get_address_of__y_1() { return &____y_1; }
	inline void set__y_1(float value)
	{
		____y_1 = value;
	}

	inline static int32_t get_offset_of__z_2() { return static_cast<int32_t>(offsetof(JSCLevelHeroPointConfig_t561690542, ____z_2)); }
	inline float get__z_2() const { return ____z_2; }
	inline float* get_address_of__z_2() { return &____z_2; }
	inline void set__z_2(float value)
	{
		____z_2 = value;
	}

	inline static int32_t get_offset_of__rot_3() { return static_cast<int32_t>(offsetof(JSCLevelHeroPointConfig_t561690542, ____rot_3)); }
	inline float get__rot_3() const { return ____rot_3; }
	inline float* get_address_of__rot_3() { return &____rot_3; }
	inline void set__rot_3(float value)
	{
		____rot_3 = value;
	}

	inline static int32_t get_offset_of_extensionObject_4() { return static_cast<int32_t>(offsetof(JSCLevelHeroPointConfig_t561690542, ___extensionObject_4)); }
	inline Il2CppObject * get_extensionObject_4() const { return ___extensionObject_4; }
	inline Il2CppObject ** get_address_of_extensionObject_4() { return &___extensionObject_4; }
	inline void set_extensionObject_4(Il2CppObject * value)
	{
		___extensionObject_4 = value;
		Il2CppCodeGenWriteBarrier(&___extensionObject_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
