﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PluginJingqi
struct PluginJingqi_t1604527235;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// System.String
struct String_t;
// PluginsSdkMgr
struct PluginsSdkMgr_t3884624670;
// VersionMgr
struct VersionMgr_t1322950208;
// Mihua.SDK.PayInfo
struct PayInfo_t1775308120;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// VersionInfo
struct VersionInfo_t2356638086;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_PluginJingqi1604527235.h"
#include "AssemblyU2DCSharp_VersionMgr1322950208.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void PluginJingqi::.ctor()
extern "C"  void PluginJingqi__ctor_m1694642552 (PluginJingqi_t1604527235 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJingqi::Init()
extern "C"  void PluginJingqi_Init_m2500054492 (PluginJingqi_t1604527235 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginJingqi::ReqSDKHttpLogin()
extern "C"  Dictionary_2_t827649927 * PluginJingqi_ReqSDKHttpLogin_m2949477325 (PluginJingqi_t1604527235 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginJingqi::IsLoginSuccess(System.Boolean)
extern "C"  bool PluginJingqi_IsLoginSuccess_m4162332379 (PluginJingqi_t1604527235 * __this, bool ___isOpenUseLogin0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJingqi::OpenUserLogin()
extern "C"  void PluginJingqi_OpenUserLogin_m370039754 (PluginJingqi_t1604527235 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJingqi::RolelvUpinfo(CEvent.ZEvent)
extern "C"  void PluginJingqi_RolelvUpinfo_m1342811302 (PluginJingqi_t1604527235 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJingqi::UserPay(CEvent.ZEvent)
extern "C"  void PluginJingqi_UserPay_m1983266920 (PluginJingqi_t1604527235 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJingqi::OnInitSuccess(System.String)
extern "C"  void PluginJingqi_OnInitSuccess_m3421729080 (PluginJingqi_t1604527235 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJingqi::OnInitFail(System.String)
extern "C"  void PluginJingqi_OnInitFail_m891964169 (PluginJingqi_t1604527235 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJingqi::OnLoginSuccess(System.String)
extern "C"  void PluginJingqi_OnLoginSuccess_m3384098429 (PluginJingqi_t1604527235 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJingqi::OnLoginFail(System.String)
extern "C"  void PluginJingqi_OnLoginFail_m510921700 (PluginJingqi_t1604527235 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJingqi::OnPaySuccess(System.String)
extern "C"  void PluginJingqi_OnPaySuccess_m3169000444 (PluginJingqi_t1604527235 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJingqi::OnPayFail(System.String)
extern "C"  void PluginJingqi_OnPayFail_m3703846597 (PluginJingqi_t1604527235 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJingqi::OnExitGameSuccess(System.String)
extern "C"  void PluginJingqi_OnExitGameSuccess_m878885208 (PluginJingqi_t1604527235 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJingqi::LogoutSuccess()
extern "C"  void PluginJingqi_LogoutSuccess_m321331823 (PluginJingqi_t1604527235 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJingqi::OnLogoutSuccess(System.String)
extern "C"  void PluginJingqi_OnLogoutSuccess_m1831378866 (PluginJingqi_t1604527235 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJingqi::OnLogoutFail(System.String)
extern "C"  void PluginJingqi_OnLogoutFail_m2766552783 (PluginJingqi_t1604527235 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJingqi::JQSDKInit()
extern "C"  void PluginJingqi_JQSDKInit_m2257331353 (PluginJingqi_t1604527235 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJingqi::JQSDKLogin()
extern "C"  void PluginJingqi_JQSDKLogin_m3946827938 (PluginJingqi_t1604527235 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJingqi::JQSDKPay(System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginJingqi_JQSDKPay_m2614494437 (PluginJingqi_t1604527235 * __this, String_t* ___orderId0, String_t* ___productCode1, String_t* ___amount2, String_t* ___productName3, String_t* ___serverId4, String_t* ___roleName5, String_t* ___roleLv6, String_t* ___extendInfo7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJingqi::JQSDKRole(System.String,System.String,System.String)
extern "C"  void PluginJingqi_JQSDKRole_m978992091 (PluginJingqi_t1604527235 * __this, String_t* ___roleName0, String_t* ___roleLevel1, String_t* ___serverId2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJingqi::<OnLogoutSuccess>m__433()
extern "C"  void PluginJingqi_U3COnLogoutSuccessU3Em__433_m2905911165 (PluginJingqi_t1604527235 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PluginsSdkMgr PluginJingqi::ilo_get_PluginsSdkMgr1()
extern "C"  PluginsSdkMgr_t3884624670 * PluginJingqi_ilo_get_PluginsSdkMgr1_m4076705328 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionMgr PluginJingqi::ilo_get_Instance2()
extern "C"  VersionMgr_t1322950208 * PluginJingqi_ilo_get_Instance2_m2757994106 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJingqi::ilo_JQSDKRole3(PluginJingqi,System.String,System.String,System.String)
extern "C"  void PluginJingqi_ilo_JQSDKRole3_m4207565050 (Il2CppObject * __this /* static, unused */, PluginJingqi_t1604527235 * ____this0, String_t* ___roleName1, String_t* ___roleLevel2, String_t* ___serverId3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mihua.SDK.PayInfo PluginJingqi::ilo_Parse4(System.Object[])
extern "C"  PayInfo_t1775308120 * PluginJingqi_ilo_Parse4_m1897407403 (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t1108656482* ___payOjs0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionInfo PluginJingqi::ilo_get_currentVS5(VersionMgr)
extern "C"  VersionInfo_t2356638086 * PluginJingqi_ilo_get_currentVS5_m1258419380 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJingqi::ilo_Log6(System.Object,System.Boolean)
extern "C"  void PluginJingqi_ilo_Log6_m65531930 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJingqi::ilo_DispatchEvent7(CEvent.ZEvent)
extern "C"  void PluginJingqi_ilo_DispatchEvent7_m549073477 (Il2CppObject * __this /* static, unused */, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJingqi::ilo_OpenUserLogin8(PluginJingqi)
extern "C"  void PluginJingqi_ilo_OpenUserLogin8_m2380994848 (Il2CppObject * __this /* static, unused */, PluginJingqi_t1604527235 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
