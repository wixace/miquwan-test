﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// barrierGateCfg
struct barrierGateCfg_t3705088994;
// UnityEngine.ParticleSystem
struct ParticleSystem_t381473177;
// UnityEngine.BoxCollider
struct BoxCollider_t2538127765;

#include "AssemblyU2DCSharp_Monster2901270458.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BarrierGat
struct  BarrierGat_t2996477891  : public Monster_t2901270458
{
public:
	// System.Boolean BarrierGat::IsState
	bool ___IsState_337;
	// barrierGateCfg BarrierGat::_barrierGateCfg
	barrierGateCfg_t3705088994 * ____barrierGateCfg_338;
	// UnityEngine.ParticleSystem BarrierGat::_particle
	ParticleSystem_t381473177 * ____particle_339;
	// UnityEngine.BoxCollider BarrierGat::_col
	BoxCollider_t2538127765 * ____col_340;

public:
	inline static int32_t get_offset_of_IsState_337() { return static_cast<int32_t>(offsetof(BarrierGat_t2996477891, ___IsState_337)); }
	inline bool get_IsState_337() const { return ___IsState_337; }
	inline bool* get_address_of_IsState_337() { return &___IsState_337; }
	inline void set_IsState_337(bool value)
	{
		___IsState_337 = value;
	}

	inline static int32_t get_offset_of__barrierGateCfg_338() { return static_cast<int32_t>(offsetof(BarrierGat_t2996477891, ____barrierGateCfg_338)); }
	inline barrierGateCfg_t3705088994 * get__barrierGateCfg_338() const { return ____barrierGateCfg_338; }
	inline barrierGateCfg_t3705088994 ** get_address_of__barrierGateCfg_338() { return &____barrierGateCfg_338; }
	inline void set__barrierGateCfg_338(barrierGateCfg_t3705088994 * value)
	{
		____barrierGateCfg_338 = value;
		Il2CppCodeGenWriteBarrier(&____barrierGateCfg_338, value);
	}

	inline static int32_t get_offset_of__particle_339() { return static_cast<int32_t>(offsetof(BarrierGat_t2996477891, ____particle_339)); }
	inline ParticleSystem_t381473177 * get__particle_339() const { return ____particle_339; }
	inline ParticleSystem_t381473177 ** get_address_of__particle_339() { return &____particle_339; }
	inline void set__particle_339(ParticleSystem_t381473177 * value)
	{
		____particle_339 = value;
		Il2CppCodeGenWriteBarrier(&____particle_339, value);
	}

	inline static int32_t get_offset_of__col_340() { return static_cast<int32_t>(offsetof(BarrierGat_t2996477891, ____col_340)); }
	inline BoxCollider_t2538127765 * get__col_340() const { return ____col_340; }
	inline BoxCollider_t2538127765 ** get_address_of__col_340() { return &____col_340; }
	inline void set__col_340(BoxCollider_t2538127765 * value)
	{
		____col_340 = value;
		Il2CppCodeGenWriteBarrier(&____col_340, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
