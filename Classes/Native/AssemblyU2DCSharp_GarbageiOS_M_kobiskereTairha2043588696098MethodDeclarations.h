﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_kobiskereTairha204
struct M_kobiskereTairha204_t3588696098;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"

// System.Void GarbageiOS.M_kobiskereTairha204::.ctor()
extern "C"  void M_kobiskereTairha204__ctor_m1422705233 (M_kobiskereTairha204_t3588696098 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_kobiskereTairha204::M_virpariYalnay0(System.String[],System.Int32)
extern "C"  void M_kobiskereTairha204_M_virpariYalnay0_m2039394435 (M_kobiskereTairha204_t3588696098 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_kobiskereTairha204::M_koudem1(System.String[],System.Int32)
extern "C"  void M_kobiskereTairha204_M_koudem1_m4130084618 (M_kobiskereTairha204_t3588696098 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
