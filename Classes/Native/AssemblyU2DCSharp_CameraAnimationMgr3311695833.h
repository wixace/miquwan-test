﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.Collections.Generic.List`1<BAMCameras>
struct List_1_t3029200112;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraAnimationMgr
struct  CameraAnimationMgr_t3311695833  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.GameObject CameraAnimationMgr::gameObject
	GameObject_t3674682005 * ___gameObject_5;
	// System.Collections.Generic.List`1<BAMCameras> CameraAnimationMgr::cameraList
	List_1_t3029200112 * ___cameraList_6;
	// UnityEngine.GameObject CameraAnimationMgr::objAnimator
	GameObject_t3674682005 * ___objAnimator_7;
	// System.Boolean CameraAnimationMgr::<IsPlaying>k__BackingField
	bool ___U3CIsPlayingU3Ek__BackingField_8;

public:
	inline static int32_t get_offset_of_gameObject_5() { return static_cast<int32_t>(offsetof(CameraAnimationMgr_t3311695833, ___gameObject_5)); }
	inline GameObject_t3674682005 * get_gameObject_5() const { return ___gameObject_5; }
	inline GameObject_t3674682005 ** get_address_of_gameObject_5() { return &___gameObject_5; }
	inline void set_gameObject_5(GameObject_t3674682005 * value)
	{
		___gameObject_5 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_5, value);
	}

	inline static int32_t get_offset_of_cameraList_6() { return static_cast<int32_t>(offsetof(CameraAnimationMgr_t3311695833, ___cameraList_6)); }
	inline List_1_t3029200112 * get_cameraList_6() const { return ___cameraList_6; }
	inline List_1_t3029200112 ** get_address_of_cameraList_6() { return &___cameraList_6; }
	inline void set_cameraList_6(List_1_t3029200112 * value)
	{
		___cameraList_6 = value;
		Il2CppCodeGenWriteBarrier(&___cameraList_6, value);
	}

	inline static int32_t get_offset_of_objAnimator_7() { return static_cast<int32_t>(offsetof(CameraAnimationMgr_t3311695833, ___objAnimator_7)); }
	inline GameObject_t3674682005 * get_objAnimator_7() const { return ___objAnimator_7; }
	inline GameObject_t3674682005 ** get_address_of_objAnimator_7() { return &___objAnimator_7; }
	inline void set_objAnimator_7(GameObject_t3674682005 * value)
	{
		___objAnimator_7 = value;
		Il2CppCodeGenWriteBarrier(&___objAnimator_7, value);
	}

	inline static int32_t get_offset_of_U3CIsPlayingU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(CameraAnimationMgr_t3311695833, ___U3CIsPlayingU3Ek__BackingField_8)); }
	inline bool get_U3CIsPlayingU3Ek__BackingField_8() const { return ___U3CIsPlayingU3Ek__BackingField_8; }
	inline bool* get_address_of_U3CIsPlayingU3Ek__BackingField_8() { return &___U3CIsPlayingU3Ek__BackingField_8; }
	inline void set_U3CIsPlayingU3Ek__BackingField_8(bool value)
	{
		___U3CIsPlayingU3Ek__BackingField_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
