﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// Pathfinding.Poly2Tri.AdvancingFront
struct AdvancingFront_t240556382;
// Pathfinding.Poly2Tri.AdvancingFrontNode
struct AdvancingFrontNode_t688967424;
// System.String
struct String_t;
// Pathfinding.Poly2Tri.TriangulationPoint
struct TriangulationPoint_t3810082933;
// Pathfinding.Poly2Tri.DelaunayTriangle
struct DelaunayTriangle_t2835103587;
// Pathfinding.Poly2Tri.DTSweepContext
struct DTSweepContext_t543731303;
// Pathfinding.Poly2Tri.DTSweepConstraint
struct DTSweepConstraint_t3360279023;
// Pathfinding.Poly2Tri.DTSweepBasin
struct DTSweepBasin_t2452859313;
// Pathfinding.Poly2Tri.Triangulatable
struct Triangulatable_t923279207;
// Pathfinding.Poly2Tri.TriangulationConstraint
struct TriangulationConstraint_t137695394;
// Pathfinding.Poly2Tri.DTSweepDebugContext
struct DTSweepDebugContext_t3829537966;
// Pathfinding.Poly2Tri.DTSweepEdgeEvent
struct DTSweepEdgeEvent_t3441061589;
// Pathfinding.Poly2Tri.DTSweepPointComparator
struct DTSweepPointComparator_t3174805566;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Collections.Generic.IEnumerable`1<System.Boolean>
struct IEnumerable_1_t3777711675;
// System.Collections.Generic.IEnumerator`1<System.Boolean>
struct IEnumerator_1_t2388663767;
// Pathfinding.Poly2Tri.FixedBitArray3/<Enumerate>c__Iterator1
struct U3CEnumerateU3Ec__Iterator1_t530243353;
// System.Object
struct Il2CppObject;
// Pathfinding.Poly2Tri.Polygon
struct Polygon_t1047479568;
// Pathfinding.Poly2Tri.TriangulationContext
struct TriangulationContext_t3528662164;
// Pathfinding.Poly2Tri.PointOnEdgeException
struct PointOnEdgeException_t989229367;
// System.Collections.Generic.IList`1<Pathfinding.Poly2Tri.PolygonPoint>
struct IList_1_t622507661;
// System.Collections.Generic.IEnumerable`1<Pathfinding.Poly2Tri.PolygonPoint>
struct IEnumerable_1_t1228773415;
// System.Collections.Generic.IList`1<Pathfinding.Poly2Tri.TriangulationPoint>
struct IList_1_t2209762840;
// System.Collections.Generic.IList`1<Pathfinding.Poly2Tri.DelaunayTriangle>
struct IList_1_t1234783494;
// System.Collections.Generic.IList`1<Pathfinding.Poly2Tri.Polygon>
struct IList_1_t3742126771;
// System.Collections.Generic.IEnumerable`1<Pathfinding.Poly2Tri.DelaunayTriangle>
struct IEnumerable_1_t1841049248;
// Pathfinding.Poly2Tri.PolygonPoint
struct PolygonPoint_t2222827754;
// Pathfinding.Poly2Tri.TriangulationDebugContext
struct TriangulationDebugContext_t3598090913;
// System.Collections.Generic.List`1<Pathfinding.Poly2Tri.DTSweepConstraint>
struct List_1_t433497279;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "Pathfinding_Poly2Tri_U3CModuleU3E86524790.h"
#include "Pathfinding_Poly2Tri_U3CModuleU3E86524790MethodDeclarations.h"
#include "Pathfinding_Poly2Tri_Pathfinding_Poly2Tri_Advancing240556382.h"
#include "Pathfinding_Poly2Tri_Pathfinding_Poly2Tri_Advancing240556382MethodDeclarations.h"
#include "Pathfinding_Poly2Tri_Pathfinding_Poly2Tri_Advancing688967424.h"
#include "mscorlib_System_Void2863195528.h"
#include "mscorlib_System_Object4170816371MethodDeclarations.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Text_StringBuilder243639308MethodDeclarations.h"
#include "mscorlib_System_Text_StringBuilder243639308.h"
#include "Pathfinding_Poly2Tri_Pathfinding_Poly2Tri_Triangul3810082933.h"
#include "mscorlib_System_Double3868226565.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Exception3991598821MethodDeclarations.h"
#include "mscorlib_System_Exception3991598821.h"
#include "Pathfinding_Poly2Tri_Pathfinding_Poly2Tri_Advancing688967424MethodDeclarations.h"
#include "mscorlib_System_Boolean476798718.h"
#include "Pathfinding_Poly2Tri_Pathfinding_Poly2Tri_Delaunay2835103587.h"
#include "Pathfinding_Poly2Tri_Pathfinding_Poly2Tri_Delaunay2835103587MethodDeclarations.h"
#include "Pathfinding_Poly2Tri_Pathfinding_Poly2Tri_FixedArra165589287MethodDeclarations.h"
#include "Pathfinding_Poly2Tri_Pathfinding_Poly2Tri_FixedArra165589287.h"
#include "mscorlib_System_Int321153838500.h"
#include "Pathfinding_Poly2Tri_Pathfinding_Poly2Tri_FixedArr3485577237MethodDeclarations.h"
#include "Pathfinding_Poly2Tri_Pathfinding_Poly2Tri_FixedArr3485577237.h"
#include "mscorlib_System_String7231557MethodDeclarations.h"
#include "mscorlib_ArrayTypes.h"
#include "Pathfinding_Poly2Tri_Pathfinding_Poly2Tri_FixedBit3665369095MethodDeclarations.h"
#include "Pathfinding_Poly2Tri_Pathfinding_Poly2Tri_FixedBit3665369095.h"
#include "Pathfinding_Poly2Tri_Pathfinding_Poly2Tri_DTSweep2486200370.h"
#include "Pathfinding_Poly2Tri_Pathfinding_Poly2Tri_DTSweep2486200370MethodDeclarations.h"
#include "Pathfinding_Poly2Tri_Pathfinding_Poly2Tri_DTSweepCo543731303.h"
#include "Pathfinding_Poly2Tri_Pathfinding_Poly2Tri_DTSweepCo543731303MethodDeclarations.h"
#include "Pathfinding_Poly2Tri_Pathfinding_Poly2Tri_Triangul3528662164MethodDeclarations.h"
#include "Pathfinding_Poly2Tri_Pathfinding_Poly2Tri_Triangul1170581608.h"
#include "mscorlib_System_Collections_Generic_List_1_gen883301189MethodDeclarations.h"
#include "Pathfinding_Poly2Tri_Pathfinding_Poly2Tri_Triangul3810082933MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen433497279MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat453170049MethodDeclarations.h"
#include "Pathfinding_Poly2Tri_Pathfinding_Poly2Tri_DTSweepD3829537966MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen883301189.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat453170049.h"
#include "Pathfinding_Poly2Tri_Pathfinding_Poly2Tri_DTSweepC3360279023.h"
#include "Pathfinding_Poly2Tri_Pathfinding_Poly2Tri_Triangul3528662164.h"
#include "mscorlib_System_Collections_Generic_List_1_gen433497279.h"
#include "Pathfinding_Poly2Tri_Pathfinding_Poly2Tri_DTSweepD3829537966.h"
#include "Pathfinding_Poly2Tri_Pathfinding_Poly2Tri_Triangul1170824903MethodDeclarations.h"
#include "Pathfinding_Poly2Tri_Pathfinding_Poly2Tri_Orientat3899194662.h"
#include "Pathfinding_Poly2Tri_Pathfinding_Poly2Tri_Triangul1170824903.h"
#include "mscorlib_System_Collections_Generic_List_1_gen4203289139MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen4203289139.h"
#include "Pathfinding_Poly2Tri_Pathfinding_Poly2Tri_PointOnEd989229367.h"
#include "Pathfinding_Poly2Tri_Pathfinding_Poly2Tri_DTSweepE3441061589.h"
#include "Pathfinding_Poly2Tri_Pathfinding_Poly2Tri_Triangula137695394.h"
#include "Pathfinding_Poly2Tri_Pathfinding_Poly2Tri_PointOnEd989229367MethodDeclarations.h"
#include "mscorlib_System_InvalidOperationException1589641621MethodDeclarations.h"
#include "mscorlib_System_InvalidOperationException1589641621.h"
#include "mscorlib_System_NotImplementedException1912495542MethodDeclarations.h"
#include "mscorlib_System_NotImplementedException1912495542.h"
#include "Pathfinding_Poly2Tri_Pathfinding_Poly2Tri_DTSweepB2452859313.h"
#include "mscorlib_System_Math2862914300MethodDeclarations.h"
#include "Pathfinding_Poly2Tri_Pathfinding_Poly2Tri_DTSweepB2452859313MethodDeclarations.h"
#include "Pathfinding_Poly2Tri_Pathfinding_Poly2Tri_DTSweepC3360279023MethodDeclarations.h"
#include "Pathfinding_Poly2Tri_Pathfinding_Poly2Tri_Triangula137695394MethodDeclarations.h"
#include "Pathfinding_Poly2Tri_Pathfinding_Poly2Tri_DTSweepE3441061589MethodDeclarations.h"
#include "Pathfinding_Poly2Tri_Pathfinding_Poly2Tri_DTSweepP3174805566MethodDeclarations.h"
#include "mscorlib_System_Single4291918972.h"
#include "Pathfinding_Poly2Tri_Pathfinding_Poly2Tri_DTSweepP3174805566.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat902973959MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat902973959.h"
#include "Pathfinding_Poly2Tri_Pathfinding_Poly2Tri_Triangul2946406228.h"
#include "Pathfinding_Poly2Tri_Pathfinding_Poly2Tri_Triangul3598090913.h"
#include "mscorlib_System_IndexOutOfRangeException3456360697MethodDeclarations.h"
#include "mscorlib_System_IndexOutOfRangeException3456360697.h"
#include "Pathfinding_Poly2Tri_Pathfinding_Poly2Tri_FixedBitA530243353MethodDeclarations.h"
#include "Pathfinding_Poly2Tri_Pathfinding_Poly2Tri_FixedBitA530243353.h"
#include "mscorlib_System_Threading_Interlocked373807572MethodDeclarations.h"
#include "mscorlib_System_UInt3224667981.h"
#include "mscorlib_System_NotSupportedException1732551818MethodDeclarations.h"
#include "mscorlib_System_NotSupportedException1732551818.h"
#include "Pathfinding_Poly2Tri_Pathfinding_Poly2Tri_Orientat3899194662MethodDeclarations.h"
#include "Pathfinding_Poly2Tri_Pathfinding_Poly2Tri_P2T3302343464.h"
#include "Pathfinding_Poly2Tri_Pathfinding_Poly2Tri_P2T3302343464MethodDeclarations.h"
#include "Pathfinding_Poly2Tri_Pathfinding_Poly2Tri_Polygon1047479568.h"
#include "Pathfinding_Poly2Tri_Pathfinding_Poly2Tri_Polygon1047479568MethodDeclarations.h"
#include "mscorlib_System_ArgumentException928607144MethodDeclarations.h"
#include "mscorlib_System_ArgumentException928607144.h"
#include "Pathfinding_Poly2Tri_Pathfinding_Poly2Tri_PolygonP2222827754.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2415665120MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2415665120.h"
#include "Pathfinding_Poly2Tri_Pathfinding_Poly2Tri_PolygonP2222827754MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2435337890MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2435337890.h"
#include "Pathfinding_Poly2Tri_Pathfinding_Poly2Tri_Triangul2946406228MethodDeclarations.h"
#include "Pathfinding_Poly2Tri_Pathfinding_Poly2Tri_Triangul3598090913MethodDeclarations.h"
#include "Pathfinding_Poly2Tri_Pathfinding_Poly2Tri_Triangul1170581608MethodDeclarations.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Pathfinding.Poly2Tri.AdvancingFront::.ctor(Pathfinding.Poly2Tri.AdvancingFrontNode,Pathfinding.Poly2Tri.AdvancingFrontNode)
extern "C"  void AdvancingFront__ctor_m2136454471 (AdvancingFront_t240556382 * __this, AdvancingFrontNode_t688967424 * ___head0, AdvancingFrontNode_t688967424 * ___tail1, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		AdvancingFrontNode_t688967424 * L_0 = ___head0;
		__this->set_Head_0(L_0);
		AdvancingFrontNode_t688967424 * L_1 = ___tail1;
		__this->set_Tail_1(L_1);
		AdvancingFrontNode_t688967424 * L_2 = ___head0;
		__this->set_Search_2(L_2);
		AdvancingFrontNode_t688967424 * L_3 = ___head0;
		AdvancingFront_AddNode_m1666580389(__this, L_3, /*hidden argument*/NULL);
		AdvancingFrontNode_t688967424 * L_4 = ___tail1;
		AdvancingFront_AddNode_m1666580389(__this, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Pathfinding.Poly2Tri.AdvancingFront::AddNode(Pathfinding.Poly2Tri.AdvancingFrontNode)
extern "C"  void AdvancingFront_AddNode_m1666580389 (AdvancingFront_t240556382 * __this, AdvancingFrontNode_t688967424 * ___node0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Pathfinding.Poly2Tri.AdvancingFront::RemoveNode(Pathfinding.Poly2Tri.AdvancingFrontNode)
extern "C"  void AdvancingFront_RemoveNode_m1553367390 (AdvancingFront_t240556382 * __this, AdvancingFrontNode_t688967424 * ___node0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.String Pathfinding.Poly2Tri.AdvancingFront::ToString()
extern Il2CppClass* StringBuilder_t243639308_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1457;
extern const uint32_t AdvancingFront_ToString_m1353602860_MetadataUsageId;
extern "C"  String_t* AdvancingFront_ToString_m1353602860 (AdvancingFront_t240556382 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AdvancingFront_ToString_m1353602860_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	StringBuilder_t243639308 * V_0 = NULL;
	AdvancingFrontNode_t688967424 * V_1 = NULL;
	{
		StringBuilder_t243639308 * L_0 = (StringBuilder_t243639308 *)il2cpp_codegen_object_new(StringBuilder_t243639308_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m135953004(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		AdvancingFrontNode_t688967424 * L_1 = __this->get_Head_0();
		V_1 = L_1;
		goto IL_0035;
	}

IL_0012:
	{
		StringBuilder_t243639308 * L_2 = V_0;
		AdvancingFrontNode_t688967424 * L_3 = V_1;
		NullCheck(L_3);
		TriangulationPoint_t3810082933 * L_4 = L_3->get_Point_3();
		NullCheck(L_4);
		double L_5 = L_4->get_X_0();
		NullCheck(L_2);
		StringBuilder_t243639308 * L_6 = StringBuilder_Append_m3329851035(L_2, L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		StringBuilder_Append_m3898090075(L_6, _stringLiteral1457, /*hidden argument*/NULL);
		AdvancingFrontNode_t688967424 * L_7 = V_1;
		NullCheck(L_7);
		AdvancingFrontNode_t688967424 * L_8 = L_7->get_Next_0();
		V_1 = L_8;
	}

IL_0035:
	{
		AdvancingFrontNode_t688967424 * L_9 = V_1;
		AdvancingFrontNode_t688967424 * L_10 = __this->get_Tail_1();
		if ((!(((Il2CppObject*)(AdvancingFrontNode_t688967424 *)L_9) == ((Il2CppObject*)(AdvancingFrontNode_t688967424 *)L_10))))
		{
			goto IL_0012;
		}
	}
	{
		StringBuilder_t243639308 * L_11 = V_0;
		AdvancingFrontNode_t688967424 * L_12 = __this->get_Tail_1();
		NullCheck(L_12);
		TriangulationPoint_t3810082933 * L_13 = L_12->get_Point_3();
		NullCheck(L_13);
		double L_14 = L_13->get_X_0();
		NullCheck(L_11);
		StringBuilder_Append_m3329851035(L_11, L_14, /*hidden argument*/NULL);
		StringBuilder_t243639308 * L_15 = V_0;
		NullCheck(L_15);
		String_t* L_16 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_15);
		return L_16;
	}
}
// Pathfinding.Poly2Tri.AdvancingFrontNode Pathfinding.Poly2Tri.AdvancingFront::FindSearchNode(System.Double)
extern "C"  AdvancingFrontNode_t688967424 * AdvancingFront_FindSearchNode_m3770481444 (AdvancingFront_t240556382 * __this, double ___x0, const MethodInfo* method)
{
	{
		AdvancingFrontNode_t688967424 * L_0 = __this->get_Search_2();
		return L_0;
	}
}
// Pathfinding.Poly2Tri.AdvancingFrontNode Pathfinding.Poly2Tri.AdvancingFront::LocateNode(Pathfinding.Poly2Tri.TriangulationPoint)
extern "C"  AdvancingFrontNode_t688967424 * AdvancingFront_LocateNode_m3646659395 (AdvancingFront_t240556382 * __this, TriangulationPoint_t3810082933 * ___point0, const MethodInfo* method)
{
	{
		TriangulationPoint_t3810082933 * L_0 = ___point0;
		NullCheck(L_0);
		double L_1 = L_0->get_X_0();
		AdvancingFrontNode_t688967424 * L_2 = AdvancingFront_LocateNode_m4055557107(__this, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// Pathfinding.Poly2Tri.AdvancingFrontNode Pathfinding.Poly2Tri.AdvancingFront::LocateNode(System.Double)
extern "C"  AdvancingFrontNode_t688967424 * AdvancingFront_LocateNode_m4055557107 (AdvancingFront_t240556382 * __this, double ___x0, const MethodInfo* method)
{
	AdvancingFrontNode_t688967424 * V_0 = NULL;
	{
		double L_0 = ___x0;
		AdvancingFrontNode_t688967424 * L_1 = AdvancingFront_FindSearchNode_m3770481444(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		double L_2 = ___x0;
		AdvancingFrontNode_t688967424 * L_3 = V_0;
		NullCheck(L_3);
		double L_4 = L_3->get_Value_2();
		if ((!(((double)L_2) < ((double)L_4))))
		{
			goto IL_0040;
		}
	}
	{
		goto IL_002e;
	}

IL_0019:
	{
		double L_5 = ___x0;
		AdvancingFrontNode_t688967424 * L_6 = V_0;
		NullCheck(L_6);
		double L_7 = L_6->get_Value_2();
		if ((!(((double)L_5) >= ((double)L_7))))
		{
			goto IL_002e;
		}
	}
	{
		AdvancingFrontNode_t688967424 * L_8 = V_0;
		__this->set_Search_2(L_8);
		AdvancingFrontNode_t688967424 * L_9 = V_0;
		return L_9;
	}

IL_002e:
	{
		AdvancingFrontNode_t688967424 * L_10 = V_0;
		NullCheck(L_10);
		AdvancingFrontNode_t688967424 * L_11 = L_10->get_Prev_1();
		AdvancingFrontNode_t688967424 * L_12 = L_11;
		V_0 = L_12;
		if (L_12)
		{
			goto IL_0019;
		}
	}
	{
		goto IL_0071;
	}

IL_0040:
	{
		goto IL_0064;
	}

IL_0045:
	{
		double L_13 = ___x0;
		AdvancingFrontNode_t688967424 * L_14 = V_0;
		NullCheck(L_14);
		double L_15 = L_14->get_Value_2();
		if ((!(((double)L_13) < ((double)L_15))))
		{
			goto IL_0064;
		}
	}
	{
		AdvancingFrontNode_t688967424 * L_16 = V_0;
		NullCheck(L_16);
		AdvancingFrontNode_t688967424 * L_17 = L_16->get_Prev_1();
		__this->set_Search_2(L_17);
		AdvancingFrontNode_t688967424 * L_18 = V_0;
		NullCheck(L_18);
		AdvancingFrontNode_t688967424 * L_19 = L_18->get_Prev_1();
		return L_19;
	}

IL_0064:
	{
		AdvancingFrontNode_t688967424 * L_20 = V_0;
		NullCheck(L_20);
		AdvancingFrontNode_t688967424 * L_21 = L_20->get_Next_0();
		AdvancingFrontNode_t688967424 * L_22 = L_21;
		V_0 = L_22;
		if (L_22)
		{
			goto IL_0045;
		}
	}

IL_0071:
	{
		return (AdvancingFrontNode_t688967424 *)NULL;
	}
}
// Pathfinding.Poly2Tri.AdvancingFrontNode Pathfinding.Poly2Tri.AdvancingFront::LocatePoint(Pathfinding.Poly2Tri.TriangulationPoint)
extern Il2CppClass* Exception_t3991598821_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3365218027;
extern const uint32_t AdvancingFront_LocatePoint_m1012370253_MetadataUsageId;
extern "C"  AdvancingFrontNode_t688967424 * AdvancingFront_LocatePoint_m1012370253 (AdvancingFront_t240556382 * __this, TriangulationPoint_t3810082933 * ___point0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AdvancingFront_LocatePoint_m1012370253_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	double V_0 = 0.0;
	AdvancingFrontNode_t688967424 * V_1 = NULL;
	double V_2 = 0.0;
	{
		TriangulationPoint_t3810082933 * L_0 = ___point0;
		NullCheck(L_0);
		double L_1 = L_0->get_X_0();
		V_0 = L_1;
		double L_2 = V_0;
		AdvancingFrontNode_t688967424 * L_3 = AdvancingFront_FindSearchNode_m3770481444(__this, L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		AdvancingFrontNode_t688967424 * L_4 = V_1;
		NullCheck(L_4);
		TriangulationPoint_t3810082933 * L_5 = L_4->get_Point_3();
		NullCheck(L_5);
		double L_6 = L_5->get_X_0();
		V_2 = L_6;
		double L_7 = V_0;
		double L_8 = V_2;
		if ((!(((double)L_7) == ((double)L_8))))
		{
			goto IL_0078;
		}
	}
	{
		TriangulationPoint_t3810082933 * L_9 = ___point0;
		AdvancingFrontNode_t688967424 * L_10 = V_1;
		NullCheck(L_10);
		TriangulationPoint_t3810082933 * L_11 = L_10->get_Point_3();
		if ((((Il2CppObject*)(TriangulationPoint_t3810082933 *)L_9) == ((Il2CppObject*)(TriangulationPoint_t3810082933 *)L_11)))
		{
			goto IL_0073;
		}
	}
	{
		TriangulationPoint_t3810082933 * L_12 = ___point0;
		AdvancingFrontNode_t688967424 * L_13 = V_1;
		NullCheck(L_13);
		AdvancingFrontNode_t688967424 * L_14 = L_13->get_Prev_1();
		NullCheck(L_14);
		TriangulationPoint_t3810082933 * L_15 = L_14->get_Point_3();
		if ((!(((Il2CppObject*)(TriangulationPoint_t3810082933 *)L_12) == ((Il2CppObject*)(TriangulationPoint_t3810082933 *)L_15))))
		{
			goto IL_004b;
		}
	}
	{
		AdvancingFrontNode_t688967424 * L_16 = V_1;
		NullCheck(L_16);
		AdvancingFrontNode_t688967424 * L_17 = L_16->get_Prev_1();
		V_1 = L_17;
		goto IL_0073;
	}

IL_004b:
	{
		TriangulationPoint_t3810082933 * L_18 = ___point0;
		AdvancingFrontNode_t688967424 * L_19 = V_1;
		NullCheck(L_19);
		AdvancingFrontNode_t688967424 * L_20 = L_19->get_Next_0();
		NullCheck(L_20);
		TriangulationPoint_t3810082933 * L_21 = L_20->get_Point_3();
		if ((!(((Il2CppObject*)(TriangulationPoint_t3810082933 *)L_18) == ((Il2CppObject*)(TriangulationPoint_t3810082933 *)L_21))))
		{
			goto IL_0068;
		}
	}
	{
		AdvancingFrontNode_t688967424 * L_22 = V_1;
		NullCheck(L_22);
		AdvancingFrontNode_t688967424 * L_23 = L_22->get_Next_0();
		V_1 = L_23;
		goto IL_0073;
	}

IL_0068:
	{
		Exception_t3991598821 * L_24 = (Exception_t3991598821 *)il2cpp_codegen_object_new(Exception_t3991598821_il2cpp_TypeInfo_var);
		Exception__ctor_m3870771296(L_24, _stringLiteral3365218027, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_24);
	}

IL_0073:
	{
		goto IL_00ca;
	}

IL_0078:
	{
		double L_25 = V_0;
		double L_26 = V_2;
		if ((!(((double)L_25) < ((double)L_26))))
		{
			goto IL_00a7;
		}
	}
	{
		goto IL_0095;
	}

IL_0084:
	{
		TriangulationPoint_t3810082933 * L_27 = ___point0;
		AdvancingFrontNode_t688967424 * L_28 = V_1;
		NullCheck(L_28);
		TriangulationPoint_t3810082933 * L_29 = L_28->get_Point_3();
		if ((!(((Il2CppObject*)(TriangulationPoint_t3810082933 *)L_27) == ((Il2CppObject*)(TriangulationPoint_t3810082933 *)L_29))))
		{
			goto IL_0095;
		}
	}
	{
		goto IL_00a2;
	}

IL_0095:
	{
		AdvancingFrontNode_t688967424 * L_30 = V_1;
		NullCheck(L_30);
		AdvancingFrontNode_t688967424 * L_31 = L_30->get_Prev_1();
		AdvancingFrontNode_t688967424 * L_32 = L_31;
		V_1 = L_32;
		if (L_32)
		{
			goto IL_0084;
		}
	}

IL_00a2:
	{
		goto IL_00ca;
	}

IL_00a7:
	{
		goto IL_00bd;
	}

IL_00ac:
	{
		TriangulationPoint_t3810082933 * L_33 = ___point0;
		AdvancingFrontNode_t688967424 * L_34 = V_1;
		NullCheck(L_34);
		TriangulationPoint_t3810082933 * L_35 = L_34->get_Point_3();
		if ((!(((Il2CppObject*)(TriangulationPoint_t3810082933 *)L_33) == ((Il2CppObject*)(TriangulationPoint_t3810082933 *)L_35))))
		{
			goto IL_00bd;
		}
	}
	{
		goto IL_00ca;
	}

IL_00bd:
	{
		AdvancingFrontNode_t688967424 * L_36 = V_1;
		NullCheck(L_36);
		AdvancingFrontNode_t688967424 * L_37 = L_36->get_Next_0();
		AdvancingFrontNode_t688967424 * L_38 = L_37;
		V_1 = L_38;
		if (L_38)
		{
			goto IL_00ac;
		}
	}

IL_00ca:
	{
		AdvancingFrontNode_t688967424 * L_39 = V_1;
		__this->set_Search_2(L_39);
		AdvancingFrontNode_t688967424 * L_40 = V_1;
		return L_40;
	}
}
// System.Void Pathfinding.Poly2Tri.AdvancingFrontNode::.ctor(Pathfinding.Poly2Tri.TriangulationPoint)
extern "C"  void AdvancingFrontNode__ctor_m294490163 (AdvancingFrontNode_t688967424 * __this, TriangulationPoint_t3810082933 * ___point0, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		TriangulationPoint_t3810082933 * L_0 = ___point0;
		__this->set_Point_3(L_0);
		TriangulationPoint_t3810082933 * L_1 = ___point0;
		NullCheck(L_1);
		double L_2 = L_1->get_X_0();
		__this->set_Value_2(L_2);
		return;
	}
}
// System.Boolean Pathfinding.Poly2Tri.AdvancingFrontNode::get_HasNext()
extern "C"  bool AdvancingFrontNode_get_HasNext_m1764143671 (AdvancingFrontNode_t688967424 * __this, const MethodInfo* method)
{
	{
		AdvancingFrontNode_t688967424 * L_0 = __this->get_Next_0();
		return (bool)((((int32_t)((((Il2CppObject*)(AdvancingFrontNode_t688967424 *)L_0) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean Pathfinding.Poly2Tri.AdvancingFrontNode::get_HasPrev()
extern "C"  bool AdvancingFrontNode_get_HasPrev_m1832843639 (AdvancingFrontNode_t688967424 * __this, const MethodInfo* method)
{
	{
		AdvancingFrontNode_t688967424 * L_0 = __this->get_Prev_1();
		return (bool)((((int32_t)((((Il2CppObject*)(AdvancingFrontNode_t688967424 *)L_0) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void Pathfinding.Poly2Tri.DelaunayTriangle::.ctor(Pathfinding.Poly2Tri.TriangulationPoint,Pathfinding.Poly2Tri.TriangulationPoint,Pathfinding.Poly2Tri.TriangulationPoint)
extern const MethodInfo* FixedArray3_1_set_Item_m461223341_MethodInfo_var;
extern const uint32_t DelaunayTriangle__ctor_m1610534382_MetadataUsageId;
extern "C"  void DelaunayTriangle__ctor_m1610534382 (DelaunayTriangle_t2835103587 * __this, TriangulationPoint_t3810082933 * ___p10, TriangulationPoint_t3810082933 * ___p21, TriangulationPoint_t3810082933 * ___p32, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DelaunayTriangle__ctor_m1610534382_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		FixedArray3_1_t165589287 * L_0 = __this->get_address_of_Points_0();
		TriangulationPoint_t3810082933 * L_1 = ___p10;
		FixedArray3_1_set_Item_m461223341(L_0, 0, L_1, /*hidden argument*/FixedArray3_1_set_Item_m461223341_MethodInfo_var);
		FixedArray3_1_t165589287 * L_2 = __this->get_address_of_Points_0();
		TriangulationPoint_t3810082933 * L_3 = ___p21;
		FixedArray3_1_set_Item_m461223341(L_2, 1, L_3, /*hidden argument*/FixedArray3_1_set_Item_m461223341_MethodInfo_var);
		FixedArray3_1_t165589287 * L_4 = __this->get_address_of_Points_0();
		TriangulationPoint_t3810082933 * L_5 = ___p32;
		FixedArray3_1_set_Item_m461223341(L_4, 2, L_5, /*hidden argument*/FixedArray3_1_set_Item_m461223341_MethodInfo_var);
		return;
	}
}
// System.Boolean Pathfinding.Poly2Tri.DelaunayTriangle::get_IsInterior()
extern "C"  bool DelaunayTriangle_get_IsInterior_m1627348341 (DelaunayTriangle_t2835103587 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_U3CIsInteriorU3Ek__BackingField_4();
		return L_0;
	}
}
// System.Void Pathfinding.Poly2Tri.DelaunayTriangle::set_IsInterior(System.Boolean)
extern "C"  void DelaunayTriangle_set_IsInterior_m1507960310 (DelaunayTriangle_t2835103587 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set_U3CIsInteriorU3Ek__BackingField_4(L_0);
		return;
	}
}
// System.Int32 Pathfinding.Poly2Tri.DelaunayTriangle::IndexOf(Pathfinding.Poly2Tri.TriangulationPoint)
extern Il2CppClass* Exception_t3991598821_il2cpp_TypeInfo_var;
extern const MethodInfo* FixedArray3_1_IndexOf_m584960535_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3608857826;
extern const uint32_t DelaunayTriangle_IndexOf_m1914770527_MetadataUsageId;
extern "C"  int32_t DelaunayTriangle_IndexOf_m1914770527 (DelaunayTriangle_t2835103587 * __this, TriangulationPoint_t3810082933 * ___p0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DelaunayTriangle_IndexOf_m1914770527_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		FixedArray3_1_t165589287 * L_0 = __this->get_address_of_Points_0();
		TriangulationPoint_t3810082933 * L_1 = ___p0;
		int32_t L_2 = FixedArray3_1_IndexOf_m584960535(L_0, L_1, /*hidden argument*/FixedArray3_1_IndexOf_m584960535_MethodInfo_var);
		V_0 = L_2;
		int32_t L_3 = V_0;
		if ((!(((uint32_t)L_3) == ((uint32_t)(-1)))))
		{
			goto IL_001f;
		}
	}
	{
		Exception_t3991598821 * L_4 = (Exception_t3991598821 *)il2cpp_codegen_object_new(Exception_t3991598821_il2cpp_TypeInfo_var);
		Exception__ctor_m3870771296(L_4, _stringLiteral3608857826, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_001f:
	{
		int32_t L_5 = V_0;
		return L_5;
	}
}
// System.Int32 Pathfinding.Poly2Tri.DelaunayTriangle::IndexCCWFrom(Pathfinding.Poly2Tri.TriangulationPoint)
extern "C"  int32_t DelaunayTriangle_IndexCCWFrom_m3252439787 (DelaunayTriangle_t2835103587 * __this, TriangulationPoint_t3810082933 * ___p0, const MethodInfo* method)
{
	{
		TriangulationPoint_t3810082933 * L_0 = ___p0;
		int32_t L_1 = DelaunayTriangle_IndexOf_m1914770527(__this, L_0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)((int32_t)((int32_t)L_1+(int32_t)1))%(int32_t)3));
	}
}
// System.Boolean Pathfinding.Poly2Tri.DelaunayTriangle::Contains(Pathfinding.Poly2Tri.TriangulationPoint)
extern const MethodInfo* FixedArray3_1_Contains_m2839867457_MethodInfo_var;
extern const uint32_t DelaunayTriangle_Contains_m3545485537_MetadataUsageId;
extern "C"  bool DelaunayTriangle_Contains_m3545485537 (DelaunayTriangle_t2835103587 * __this, TriangulationPoint_t3810082933 * ___p0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DelaunayTriangle_Contains_m3545485537_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FixedArray3_1_t165589287 * L_0 = __this->get_address_of_Points_0();
		TriangulationPoint_t3810082933 * L_1 = ___p0;
		bool L_2 = FixedArray3_1_Contains_m2839867457(L_0, L_1, /*hidden argument*/FixedArray3_1_Contains_m2839867457_MethodInfo_var);
		return L_2;
	}
}
// System.Void Pathfinding.Poly2Tri.DelaunayTriangle::MarkNeighbor(Pathfinding.Poly2Tri.TriangulationPoint,Pathfinding.Poly2Tri.TriangulationPoint,Pathfinding.Poly2Tri.DelaunayTriangle)
extern Il2CppClass* Exception_t3991598821_il2cpp_TypeInfo_var;
extern const MethodInfo* FixedArray3_1_set_Item_m1339370303_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1612851074;
extern const uint32_t DelaunayTriangle_MarkNeighbor_m2976269813_MetadataUsageId;
extern "C"  void DelaunayTriangle_MarkNeighbor_m2976269813 (DelaunayTriangle_t2835103587 * __this, TriangulationPoint_t3810082933 * ___p10, TriangulationPoint_t3810082933 * ___p21, DelaunayTriangle_t2835103587 * ___t2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DelaunayTriangle_MarkNeighbor_m2976269813_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		TriangulationPoint_t3810082933 * L_0 = ___p10;
		TriangulationPoint_t3810082933 * L_1 = ___p21;
		int32_t L_2 = DelaunayTriangle_EdgeIndex_m78242015(__this, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		int32_t L_3 = V_0;
		if ((!(((uint32_t)L_3) == ((uint32_t)(-1)))))
		{
			goto IL_001b;
		}
	}
	{
		Exception_t3991598821 * L_4 = (Exception_t3991598821 *)il2cpp_codegen_object_new(Exception_t3991598821_il2cpp_TypeInfo_var);
		Exception__ctor_m3870771296(L_4, _stringLiteral1612851074, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_001b:
	{
		FixedArray3_1_t3485577237 * L_5 = __this->get_address_of_Neighbors_1();
		int32_t L_6 = V_0;
		DelaunayTriangle_t2835103587 * L_7 = ___t2;
		FixedArray3_1_set_Item_m1339370303(L_5, L_6, L_7, /*hidden argument*/FixedArray3_1_set_Item_m1339370303_MethodInfo_var);
		return;
	}
}
// System.Void Pathfinding.Poly2Tri.DelaunayTriangle::MarkNeighbor(Pathfinding.Poly2Tri.DelaunayTriangle)
extern Il2CppClass* Exception_t3991598821_il2cpp_TypeInfo_var;
extern const MethodInfo* FixedArray3_1_get_Item_m7701314_MethodInfo_var;
extern const MethodInfo* FixedArray3_1_set_Item_m1339370303_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3206156071;
extern const uint32_t DelaunayTriangle_MarkNeighbor_m119719133_MetadataUsageId;
extern "C"  void DelaunayTriangle_MarkNeighbor_m119719133 (DelaunayTriangle_t2835103587 * __this, DelaunayTriangle_t2835103587 * ___t0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DelaunayTriangle_MarkNeighbor_m119719133_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	bool V_1 = false;
	bool V_2 = false;
	{
		DelaunayTriangle_t2835103587 * L_0 = ___t0;
		FixedArray3_1_t165589287 * L_1 = __this->get_address_of_Points_0();
		TriangulationPoint_t3810082933 * L_2 = FixedArray3_1_get_Item_m7701314(L_1, 0, /*hidden argument*/FixedArray3_1_get_Item_m7701314_MethodInfo_var);
		NullCheck(L_0);
		bool L_3 = DelaunayTriangle_Contains_m3545485537(L_0, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		DelaunayTriangle_t2835103587 * L_4 = ___t0;
		FixedArray3_1_t165589287 * L_5 = __this->get_address_of_Points_0();
		TriangulationPoint_t3810082933 * L_6 = FixedArray3_1_get_Item_m7701314(L_5, 1, /*hidden argument*/FixedArray3_1_get_Item_m7701314_MethodInfo_var);
		NullCheck(L_4);
		bool L_7 = DelaunayTriangle_Contains_m3545485537(L_4, L_6, /*hidden argument*/NULL);
		V_1 = L_7;
		DelaunayTriangle_t2835103587 * L_8 = ___t0;
		FixedArray3_1_t165589287 * L_9 = __this->get_address_of_Points_0();
		TriangulationPoint_t3810082933 * L_10 = FixedArray3_1_get_Item_m7701314(L_9, 2, /*hidden argument*/FixedArray3_1_get_Item_m7701314_MethodInfo_var);
		NullCheck(L_8);
		bool L_11 = DelaunayTriangle_Contains_m3545485537(L_8, L_10, /*hidden argument*/NULL);
		V_2 = L_11;
		bool L_12 = V_1;
		if (!L_12)
		{
			goto IL_0076;
		}
	}
	{
		bool L_13 = V_2;
		if (!L_13)
		{
			goto IL_0076;
		}
	}
	{
		FixedArray3_1_t3485577237 * L_14 = __this->get_address_of_Neighbors_1();
		DelaunayTriangle_t2835103587 * L_15 = ___t0;
		FixedArray3_1_set_Item_m1339370303(L_14, 0, L_15, /*hidden argument*/FixedArray3_1_set_Item_m1339370303_MethodInfo_var);
		DelaunayTriangle_t2835103587 * L_16 = ___t0;
		FixedArray3_1_t165589287 * L_17 = __this->get_address_of_Points_0();
		TriangulationPoint_t3810082933 * L_18 = FixedArray3_1_get_Item_m7701314(L_17, 1, /*hidden argument*/FixedArray3_1_get_Item_m7701314_MethodInfo_var);
		FixedArray3_1_t165589287 * L_19 = __this->get_address_of_Points_0();
		TriangulationPoint_t3810082933 * L_20 = FixedArray3_1_get_Item_m7701314(L_19, 2, /*hidden argument*/FixedArray3_1_get_Item_m7701314_MethodInfo_var);
		NullCheck(L_16);
		DelaunayTriangle_MarkNeighbor_m2976269813(L_16, L_18, L_20, __this, /*hidden argument*/NULL);
		goto IL_00fb;
	}

IL_0076:
	{
		bool L_21 = V_0;
		if (!L_21)
		{
			goto IL_00b3;
		}
	}
	{
		bool L_22 = V_2;
		if (!L_22)
		{
			goto IL_00b3;
		}
	}
	{
		FixedArray3_1_t3485577237 * L_23 = __this->get_address_of_Neighbors_1();
		DelaunayTriangle_t2835103587 * L_24 = ___t0;
		FixedArray3_1_set_Item_m1339370303(L_23, 1, L_24, /*hidden argument*/FixedArray3_1_set_Item_m1339370303_MethodInfo_var);
		DelaunayTriangle_t2835103587 * L_25 = ___t0;
		FixedArray3_1_t165589287 * L_26 = __this->get_address_of_Points_0();
		TriangulationPoint_t3810082933 * L_27 = FixedArray3_1_get_Item_m7701314(L_26, 0, /*hidden argument*/FixedArray3_1_get_Item_m7701314_MethodInfo_var);
		FixedArray3_1_t165589287 * L_28 = __this->get_address_of_Points_0();
		TriangulationPoint_t3810082933 * L_29 = FixedArray3_1_get_Item_m7701314(L_28, 2, /*hidden argument*/FixedArray3_1_get_Item_m7701314_MethodInfo_var);
		NullCheck(L_25);
		DelaunayTriangle_MarkNeighbor_m2976269813(L_25, L_27, L_29, __this, /*hidden argument*/NULL);
		goto IL_00fb;
	}

IL_00b3:
	{
		bool L_30 = V_0;
		if (!L_30)
		{
			goto IL_00f0;
		}
	}
	{
		bool L_31 = V_1;
		if (!L_31)
		{
			goto IL_00f0;
		}
	}
	{
		FixedArray3_1_t3485577237 * L_32 = __this->get_address_of_Neighbors_1();
		DelaunayTriangle_t2835103587 * L_33 = ___t0;
		FixedArray3_1_set_Item_m1339370303(L_32, 2, L_33, /*hidden argument*/FixedArray3_1_set_Item_m1339370303_MethodInfo_var);
		DelaunayTriangle_t2835103587 * L_34 = ___t0;
		FixedArray3_1_t165589287 * L_35 = __this->get_address_of_Points_0();
		TriangulationPoint_t3810082933 * L_36 = FixedArray3_1_get_Item_m7701314(L_35, 0, /*hidden argument*/FixedArray3_1_get_Item_m7701314_MethodInfo_var);
		FixedArray3_1_t165589287 * L_37 = __this->get_address_of_Points_0();
		TriangulationPoint_t3810082933 * L_38 = FixedArray3_1_get_Item_m7701314(L_37, 1, /*hidden argument*/FixedArray3_1_get_Item_m7701314_MethodInfo_var);
		NullCheck(L_34);
		DelaunayTriangle_MarkNeighbor_m2976269813(L_34, L_36, L_38, __this, /*hidden argument*/NULL);
		goto IL_00fb;
	}

IL_00f0:
	{
		Exception_t3991598821 * L_39 = (Exception_t3991598821 *)il2cpp_codegen_object_new(Exception_t3991598821_il2cpp_TypeInfo_var);
		Exception__ctor_m3870771296(L_39, _stringLiteral3206156071, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_39);
	}

IL_00fb:
	{
		return;
	}
}
// Pathfinding.Poly2Tri.TriangulationPoint Pathfinding.Poly2Tri.DelaunayTriangle::OppositePoint(Pathfinding.Poly2Tri.DelaunayTriangle,Pathfinding.Poly2Tri.TriangulationPoint)
extern "C"  TriangulationPoint_t3810082933 * DelaunayTriangle_OppositePoint_m3734492474 (DelaunayTriangle_t2835103587 * __this, DelaunayTriangle_t2835103587 * ___t0, TriangulationPoint_t3810082933 * ___p1, const MethodInfo* method)
{
	{
		DelaunayTriangle_t2835103587 * L_0 = ___t0;
		TriangulationPoint_t3810082933 * L_1 = ___p1;
		NullCheck(L_0);
		TriangulationPoint_t3810082933 * L_2 = DelaunayTriangle_PointCWFrom_m3064891095(L_0, L_1, /*hidden argument*/NULL);
		TriangulationPoint_t3810082933 * L_3 = DelaunayTriangle_PointCWFrom_m3064891095(__this, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// Pathfinding.Poly2Tri.DelaunayTriangle Pathfinding.Poly2Tri.DelaunayTriangle::NeighborCWFrom(Pathfinding.Poly2Tri.TriangulationPoint)
extern const MethodInfo* FixedArray3_1_IndexOf_m584960535_MethodInfo_var;
extern const MethodInfo* FixedArray3_1_get_Item_m4227602836_MethodInfo_var;
extern const uint32_t DelaunayTriangle_NeighborCWFrom_m3856958495_MetadataUsageId;
extern "C"  DelaunayTriangle_t2835103587 * DelaunayTriangle_NeighborCWFrom_m3856958495 (DelaunayTriangle_t2835103587 * __this, TriangulationPoint_t3810082933 * ___point0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DelaunayTriangle_NeighborCWFrom_m3856958495_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FixedArray3_1_t3485577237 * L_0 = __this->get_address_of_Neighbors_1();
		FixedArray3_1_t165589287 * L_1 = __this->get_address_of_Points_0();
		TriangulationPoint_t3810082933 * L_2 = ___point0;
		int32_t L_3 = FixedArray3_1_IndexOf_m584960535(L_1, L_2, /*hidden argument*/FixedArray3_1_IndexOf_m584960535_MethodInfo_var);
		DelaunayTriangle_t2835103587 * L_4 = FixedArray3_1_get_Item_m4227602836(L_0, ((int32_t)((int32_t)((int32_t)((int32_t)L_3+(int32_t)1))%(int32_t)3)), /*hidden argument*/FixedArray3_1_get_Item_m4227602836_MethodInfo_var);
		return L_4;
	}
}
// Pathfinding.Poly2Tri.DelaunayTriangle Pathfinding.Poly2Tri.DelaunayTriangle::NeighborCCWFrom(Pathfinding.Poly2Tri.TriangulationPoint)
extern const MethodInfo* FixedArray3_1_IndexOf_m584960535_MethodInfo_var;
extern const MethodInfo* FixedArray3_1_get_Item_m4227602836_MethodInfo_var;
extern const uint32_t DelaunayTriangle_NeighborCCWFrom_m3747172644_MetadataUsageId;
extern "C"  DelaunayTriangle_t2835103587 * DelaunayTriangle_NeighborCCWFrom_m3747172644 (DelaunayTriangle_t2835103587 * __this, TriangulationPoint_t3810082933 * ___point0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DelaunayTriangle_NeighborCCWFrom_m3747172644_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FixedArray3_1_t3485577237 * L_0 = __this->get_address_of_Neighbors_1();
		FixedArray3_1_t165589287 * L_1 = __this->get_address_of_Points_0();
		TriangulationPoint_t3810082933 * L_2 = ___point0;
		int32_t L_3 = FixedArray3_1_IndexOf_m584960535(L_1, L_2, /*hidden argument*/FixedArray3_1_IndexOf_m584960535_MethodInfo_var);
		DelaunayTriangle_t2835103587 * L_4 = FixedArray3_1_get_Item_m4227602836(L_0, ((int32_t)((int32_t)((int32_t)((int32_t)L_3+(int32_t)2))%(int32_t)3)), /*hidden argument*/FixedArray3_1_get_Item_m4227602836_MethodInfo_var);
		return L_4;
	}
}
// Pathfinding.Poly2Tri.DelaunayTriangle Pathfinding.Poly2Tri.DelaunayTriangle::NeighborAcrossFrom(Pathfinding.Poly2Tri.TriangulationPoint)
extern const MethodInfo* FixedArray3_1_IndexOf_m584960535_MethodInfo_var;
extern const MethodInfo* FixedArray3_1_get_Item_m4227602836_MethodInfo_var;
extern const uint32_t DelaunayTriangle_NeighborAcrossFrom_m3663882196_MetadataUsageId;
extern "C"  DelaunayTriangle_t2835103587 * DelaunayTriangle_NeighborAcrossFrom_m3663882196 (DelaunayTriangle_t2835103587 * __this, TriangulationPoint_t3810082933 * ___point0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DelaunayTriangle_NeighborAcrossFrom_m3663882196_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FixedArray3_1_t3485577237 * L_0 = __this->get_address_of_Neighbors_1();
		FixedArray3_1_t165589287 * L_1 = __this->get_address_of_Points_0();
		TriangulationPoint_t3810082933 * L_2 = ___point0;
		int32_t L_3 = FixedArray3_1_IndexOf_m584960535(L_1, L_2, /*hidden argument*/FixedArray3_1_IndexOf_m584960535_MethodInfo_var);
		DelaunayTriangle_t2835103587 * L_4 = FixedArray3_1_get_Item_m4227602836(L_0, L_3, /*hidden argument*/FixedArray3_1_get_Item_m4227602836_MethodInfo_var);
		return L_4;
	}
}
// Pathfinding.Poly2Tri.TriangulationPoint Pathfinding.Poly2Tri.DelaunayTriangle::PointCCWFrom(Pathfinding.Poly2Tri.TriangulationPoint)
extern const MethodInfo* FixedArray3_1_get_Item_m7701314_MethodInfo_var;
extern const uint32_t DelaunayTriangle_PointCCWFrom_m667919724_MetadataUsageId;
extern "C"  TriangulationPoint_t3810082933 * DelaunayTriangle_PointCCWFrom_m667919724 (DelaunayTriangle_t2835103587 * __this, TriangulationPoint_t3810082933 * ___point0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DelaunayTriangle_PointCCWFrom_m667919724_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FixedArray3_1_t165589287 * L_0 = __this->get_address_of_Points_0();
		TriangulationPoint_t3810082933 * L_1 = ___point0;
		int32_t L_2 = DelaunayTriangle_IndexOf_m1914770527(__this, L_1, /*hidden argument*/NULL);
		TriangulationPoint_t3810082933 * L_3 = FixedArray3_1_get_Item_m7701314(L_0, ((int32_t)((int32_t)((int32_t)((int32_t)L_2+(int32_t)1))%(int32_t)3)), /*hidden argument*/FixedArray3_1_get_Item_m7701314_MethodInfo_var);
		return L_3;
	}
}
// Pathfinding.Poly2Tri.TriangulationPoint Pathfinding.Poly2Tri.DelaunayTriangle::PointCWFrom(Pathfinding.Poly2Tri.TriangulationPoint)
extern const MethodInfo* FixedArray3_1_get_Item_m7701314_MethodInfo_var;
extern const uint32_t DelaunayTriangle_PointCWFrom_m3064891095_MetadataUsageId;
extern "C"  TriangulationPoint_t3810082933 * DelaunayTriangle_PointCWFrom_m3064891095 (DelaunayTriangle_t2835103587 * __this, TriangulationPoint_t3810082933 * ___point0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DelaunayTriangle_PointCWFrom_m3064891095_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FixedArray3_1_t165589287 * L_0 = __this->get_address_of_Points_0();
		TriangulationPoint_t3810082933 * L_1 = ___point0;
		int32_t L_2 = DelaunayTriangle_IndexOf_m1914770527(__this, L_1, /*hidden argument*/NULL);
		TriangulationPoint_t3810082933 * L_3 = FixedArray3_1_get_Item_m7701314(L_0, ((int32_t)((int32_t)((int32_t)((int32_t)L_2+(int32_t)2))%(int32_t)3)), /*hidden argument*/FixedArray3_1_get_Item_m7701314_MethodInfo_var);
		return L_3;
	}
}
// System.Void Pathfinding.Poly2Tri.DelaunayTriangle::RotateCW()
extern const MethodInfo* FixedArray3_1_get_Item_m7701314_MethodInfo_var;
extern const MethodInfo* FixedArray3_1_set_Item_m461223341_MethodInfo_var;
extern const uint32_t DelaunayTriangle_RotateCW_m1049320471_MetadataUsageId;
extern "C"  void DelaunayTriangle_RotateCW_m1049320471 (DelaunayTriangle_t2835103587 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DelaunayTriangle_RotateCW_m1049320471_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	TriangulationPoint_t3810082933 * V_0 = NULL;
	{
		FixedArray3_1_t165589287 * L_0 = __this->get_address_of_Points_0();
		TriangulationPoint_t3810082933 * L_1 = FixedArray3_1_get_Item_m7701314(L_0, 2, /*hidden argument*/FixedArray3_1_get_Item_m7701314_MethodInfo_var);
		V_0 = L_1;
		FixedArray3_1_t165589287 * L_2 = __this->get_address_of_Points_0();
		FixedArray3_1_t165589287 * L_3 = __this->get_address_of_Points_0();
		TriangulationPoint_t3810082933 * L_4 = FixedArray3_1_get_Item_m7701314(L_3, 1, /*hidden argument*/FixedArray3_1_get_Item_m7701314_MethodInfo_var);
		FixedArray3_1_set_Item_m461223341(L_2, 2, L_4, /*hidden argument*/FixedArray3_1_set_Item_m461223341_MethodInfo_var);
		FixedArray3_1_t165589287 * L_5 = __this->get_address_of_Points_0();
		FixedArray3_1_t165589287 * L_6 = __this->get_address_of_Points_0();
		TriangulationPoint_t3810082933 * L_7 = FixedArray3_1_get_Item_m7701314(L_6, 0, /*hidden argument*/FixedArray3_1_get_Item_m7701314_MethodInfo_var);
		FixedArray3_1_set_Item_m461223341(L_5, 1, L_7, /*hidden argument*/FixedArray3_1_set_Item_m461223341_MethodInfo_var);
		FixedArray3_1_t165589287 * L_8 = __this->get_address_of_Points_0();
		TriangulationPoint_t3810082933 * L_9 = V_0;
		FixedArray3_1_set_Item_m461223341(L_8, 0, L_9, /*hidden argument*/FixedArray3_1_set_Item_m461223341_MethodInfo_var);
		return;
	}
}
// System.Void Pathfinding.Poly2Tri.DelaunayTriangle::Legalize(Pathfinding.Poly2Tri.TriangulationPoint,Pathfinding.Poly2Tri.TriangulationPoint)
extern const MethodInfo* FixedArray3_1_set_Item_m461223341_MethodInfo_var;
extern const uint32_t DelaunayTriangle_Legalize_m2367693307_MetadataUsageId;
extern "C"  void DelaunayTriangle_Legalize_m2367693307 (DelaunayTriangle_t2835103587 * __this, TriangulationPoint_t3810082933 * ___oPoint0, TriangulationPoint_t3810082933 * ___nPoint1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DelaunayTriangle_Legalize_m2367693307_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		DelaunayTriangle_RotateCW_m1049320471(__this, /*hidden argument*/NULL);
		FixedArray3_1_t165589287 * L_0 = __this->get_address_of_Points_0();
		TriangulationPoint_t3810082933 * L_1 = ___oPoint0;
		int32_t L_2 = DelaunayTriangle_IndexCCWFrom_m3252439787(__this, L_1, /*hidden argument*/NULL);
		TriangulationPoint_t3810082933 * L_3 = ___nPoint1;
		FixedArray3_1_set_Item_m461223341(L_0, L_2, L_3, /*hidden argument*/FixedArray3_1_set_Item_m461223341_MethodInfo_var);
		return;
	}
}
// System.String Pathfinding.Poly2Tri.DelaunayTriangle::ToString()
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* FixedArray3_1_get_Item_m7701314_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral44;
extern const uint32_t DelaunayTriangle_ToString_m3784777777_MetadataUsageId;
extern "C"  String_t* DelaunayTriangle_ToString_m3784777777 (DelaunayTriangle_t2835103587 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DelaunayTriangle_ToString_m3784777777_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1108656482* L_0 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)5));
		FixedArray3_1_t165589287 * L_1 = __this->get_address_of_Points_0();
		TriangulationPoint_t3810082933 * L_2 = FixedArray3_1_get_Item_m7701314(L_1, 0, /*hidden argument*/FixedArray3_1_get_Item_m7701314_MethodInfo_var);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_2);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_2);
		ObjectU5BU5D_t1108656482* L_3 = L_0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 1);
		ArrayElementTypeCheck (L_3, _stringLiteral44);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)_stringLiteral44);
		ObjectU5BU5D_t1108656482* L_4 = L_3;
		FixedArray3_1_t165589287 * L_5 = __this->get_address_of_Points_0();
		TriangulationPoint_t3810082933 * L_6 = FixedArray3_1_get_Item_m7701314(L_5, 1, /*hidden argument*/FixedArray3_1_get_Item_m7701314_MethodInfo_var);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 2);
		ArrayElementTypeCheck (L_4, L_6);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_6);
		ObjectU5BU5D_t1108656482* L_7 = L_4;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 3);
		ArrayElementTypeCheck (L_7, _stringLiteral44);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)_stringLiteral44);
		ObjectU5BU5D_t1108656482* L_8 = L_7;
		FixedArray3_1_t165589287 * L_9 = __this->get_address_of_Points_0();
		TriangulationPoint_t3810082933 * L_10 = FixedArray3_1_get_Item_m7701314(L_9, 2, /*hidden argument*/FixedArray3_1_get_Item_m7701314_MethodInfo_var);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 4);
		ArrayElementTypeCheck (L_8, L_10);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)L_10);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = String_Concat_m3016520001(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		return L_11;
	}
}
// System.Void Pathfinding.Poly2Tri.DelaunayTriangle::MarkConstrainedEdge(System.Int32)
extern "C"  void DelaunayTriangle_MarkConstrainedEdge_m3767794513 (DelaunayTriangle_t2835103587 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		FixedBitArray3_t3665369095 * L_0 = __this->get_address_of_EdgeIsConstrained_2();
		int32_t L_1 = ___index0;
		FixedBitArray3_set_Item_m2925852176(L_0, L_1, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Pathfinding.Poly2Tri.DelaunayTriangle::MarkConstrainedEdge(Pathfinding.Poly2Tri.TriangulationPoint,Pathfinding.Poly2Tri.TriangulationPoint)
extern "C"  void DelaunayTriangle_MarkConstrainedEdge_m3313274974 (DelaunayTriangle_t2835103587 * __this, TriangulationPoint_t3810082933 * ___p0, TriangulationPoint_t3810082933 * ___q1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		TriangulationPoint_t3810082933 * L_0 = ___p0;
		TriangulationPoint_t3810082933 * L_1 = ___q1;
		int32_t L_2 = DelaunayTriangle_EdgeIndex_m78242015(__this, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_001d;
		}
	}
	{
		FixedBitArray3_t3665369095 * L_4 = __this->get_address_of_EdgeIsConstrained_2();
		int32_t L_5 = V_0;
		FixedBitArray3_set_Item_m2925852176(L_4, L_5, (bool)1, /*hidden argument*/NULL);
	}

IL_001d:
	{
		return;
	}
}
// System.Int32 Pathfinding.Poly2Tri.DelaunayTriangle::EdgeIndex(Pathfinding.Poly2Tri.TriangulationPoint,Pathfinding.Poly2Tri.TriangulationPoint)
extern const MethodInfo* FixedArray3_1_IndexOf_m584960535_MethodInfo_var;
extern const uint32_t DelaunayTriangle_EdgeIndex_m78242015_MetadataUsageId;
extern "C"  int32_t DelaunayTriangle_EdgeIndex_m78242015 (DelaunayTriangle_t2835103587 * __this, TriangulationPoint_t3810082933 * ___p10, TriangulationPoint_t3810082933 * ___p21, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DelaunayTriangle_EdgeIndex_m78242015_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	bool V_2 = false;
	bool V_3 = false;
	bool V_4 = false;
	int32_t G_B3_0 = 0;
	int32_t G_B6_0 = 0;
	int32_t G_B9_0 = 0;
	{
		FixedArray3_1_t165589287 * L_0 = __this->get_address_of_Points_0();
		TriangulationPoint_t3810082933 * L_1 = ___p10;
		int32_t L_2 = FixedArray3_1_IndexOf_m584960535(L_0, L_1, /*hidden argument*/FixedArray3_1_IndexOf_m584960535_MethodInfo_var);
		V_0 = L_2;
		FixedArray3_1_t165589287 * L_3 = __this->get_address_of_Points_0();
		TriangulationPoint_t3810082933 * L_4 = ___p21;
		int32_t L_5 = FixedArray3_1_IndexOf_m584960535(L_3, L_4, /*hidden argument*/FixedArray3_1_IndexOf_m584960535_MethodInfo_var);
		V_1 = L_5;
		int32_t L_6 = V_0;
		if (!L_6)
		{
			goto IL_0026;
		}
	}
	{
		int32_t L_7 = V_1;
		G_B3_0 = ((((int32_t)L_7) == ((int32_t)0))? 1 : 0);
		goto IL_0027;
	}

IL_0026:
	{
		G_B3_0 = 1;
	}

IL_0027:
	{
		V_2 = (bool)G_B3_0;
		int32_t L_8 = V_0;
		if ((((int32_t)L_8) == ((int32_t)1)))
		{
			goto IL_0035;
		}
	}
	{
		int32_t L_9 = V_1;
		G_B6_0 = ((((int32_t)L_9) == ((int32_t)1))? 1 : 0);
		goto IL_0036;
	}

IL_0035:
	{
		G_B6_0 = 1;
	}

IL_0036:
	{
		V_3 = (bool)G_B6_0;
		int32_t L_10 = V_0;
		if ((((int32_t)L_10) == ((int32_t)2)))
		{
			goto IL_0044;
		}
	}
	{
		int32_t L_11 = V_1;
		G_B9_0 = ((((int32_t)L_11) == ((int32_t)2))? 1 : 0);
		goto IL_0045;
	}

IL_0044:
	{
		G_B9_0 = 1;
	}

IL_0045:
	{
		V_4 = (bool)G_B9_0;
		bool L_12 = V_3;
		if (!L_12)
		{
			goto IL_0056;
		}
	}
	{
		bool L_13 = V_4;
		if (!L_13)
		{
			goto IL_0056;
		}
	}
	{
		return 0;
	}

IL_0056:
	{
		bool L_14 = V_2;
		if (!L_14)
		{
			goto IL_0065;
		}
	}
	{
		bool L_15 = V_4;
		if (!L_15)
		{
			goto IL_0065;
		}
	}
	{
		return 1;
	}

IL_0065:
	{
		bool L_16 = V_2;
		if (!L_16)
		{
			goto IL_0073;
		}
	}
	{
		bool L_17 = V_3;
		if (!L_17)
		{
			goto IL_0073;
		}
	}
	{
		return 2;
	}

IL_0073:
	{
		return (-1);
	}
}
// System.Boolean Pathfinding.Poly2Tri.DelaunayTriangle::GetConstrainedEdgeCCW(Pathfinding.Poly2Tri.TriangulationPoint)
extern "C"  bool DelaunayTriangle_GetConstrainedEdgeCCW_m1246493448 (DelaunayTriangle_t2835103587 * __this, TriangulationPoint_t3810082933 * ___p0, const MethodInfo* method)
{
	{
		FixedBitArray3_t3665369095 * L_0 = __this->get_address_of_EdgeIsConstrained_2();
		TriangulationPoint_t3810082933 * L_1 = ___p0;
		int32_t L_2 = DelaunayTriangle_IndexOf_m1914770527(__this, L_1, /*hidden argument*/NULL);
		bool L_3 = FixedBitArray3_get_Item_m141500035(L_0, ((int32_t)((int32_t)((int32_t)((int32_t)L_2+(int32_t)2))%(int32_t)3)), /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Boolean Pathfinding.Poly2Tri.DelaunayTriangle::GetConstrainedEdgeCW(Pathfinding.Poly2Tri.TriangulationPoint)
extern "C"  bool DelaunayTriangle_GetConstrainedEdgeCW_m1169250959 (DelaunayTriangle_t2835103587 * __this, TriangulationPoint_t3810082933 * ___p0, const MethodInfo* method)
{
	{
		FixedBitArray3_t3665369095 * L_0 = __this->get_address_of_EdgeIsConstrained_2();
		TriangulationPoint_t3810082933 * L_1 = ___p0;
		int32_t L_2 = DelaunayTriangle_IndexOf_m1914770527(__this, L_1, /*hidden argument*/NULL);
		bool L_3 = FixedBitArray3_get_Item_m141500035(L_0, ((int32_t)((int32_t)((int32_t)((int32_t)L_2+(int32_t)1))%(int32_t)3)), /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Void Pathfinding.Poly2Tri.DelaunayTriangle::SetConstrainedEdgeCCW(Pathfinding.Poly2Tri.TriangulationPoint,System.Boolean)
extern "C"  void DelaunayTriangle_SetConstrainedEdgeCCW_m3838082667 (DelaunayTriangle_t2835103587 * __this, TriangulationPoint_t3810082933 * ___p0, bool ___ce1, const MethodInfo* method)
{
	{
		FixedBitArray3_t3665369095 * L_0 = __this->get_address_of_EdgeIsConstrained_2();
		TriangulationPoint_t3810082933 * L_1 = ___p0;
		int32_t L_2 = DelaunayTriangle_IndexOf_m1914770527(__this, L_1, /*hidden argument*/NULL);
		bool L_3 = ___ce1;
		FixedBitArray3_set_Item_m2925852176(L_0, ((int32_t)((int32_t)((int32_t)((int32_t)L_2+(int32_t)2))%(int32_t)3)), L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Pathfinding.Poly2Tri.DelaunayTriangle::SetConstrainedEdgeCW(Pathfinding.Poly2Tri.TriangulationPoint,System.Boolean)
extern "C"  void DelaunayTriangle_SetConstrainedEdgeCW_m3217709912 (DelaunayTriangle_t2835103587 * __this, TriangulationPoint_t3810082933 * ___p0, bool ___ce1, const MethodInfo* method)
{
	{
		FixedBitArray3_t3665369095 * L_0 = __this->get_address_of_EdgeIsConstrained_2();
		TriangulationPoint_t3810082933 * L_1 = ___p0;
		int32_t L_2 = DelaunayTriangle_IndexOf_m1914770527(__this, L_1, /*hidden argument*/NULL);
		bool L_3 = ___ce1;
		FixedBitArray3_set_Item_m2925852176(L_0, ((int32_t)((int32_t)((int32_t)((int32_t)L_2+(int32_t)1))%(int32_t)3)), L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Pathfinding.Poly2Tri.DelaunayTriangle::GetDelaunayEdgeCCW(Pathfinding.Poly2Tri.TriangulationPoint)
extern "C"  bool DelaunayTriangle_GetDelaunayEdgeCCW_m1588591715 (DelaunayTriangle_t2835103587 * __this, TriangulationPoint_t3810082933 * ___p0, const MethodInfo* method)
{
	{
		FixedBitArray3_t3665369095 * L_0 = __this->get_address_of_EdgeIsDelaunay_3();
		TriangulationPoint_t3810082933 * L_1 = ___p0;
		int32_t L_2 = DelaunayTriangle_IndexOf_m1914770527(__this, L_1, /*hidden argument*/NULL);
		bool L_3 = FixedBitArray3_get_Item_m141500035(L_0, ((int32_t)((int32_t)((int32_t)((int32_t)L_2+(int32_t)2))%(int32_t)3)), /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Boolean Pathfinding.Poly2Tri.DelaunayTriangle::GetDelaunayEdgeCW(Pathfinding.Poly2Tri.TriangulationPoint)
extern "C"  bool DelaunayTriangle_GetDelaunayEdgeCW_m2288665044 (DelaunayTriangle_t2835103587 * __this, TriangulationPoint_t3810082933 * ___p0, const MethodInfo* method)
{
	{
		FixedBitArray3_t3665369095 * L_0 = __this->get_address_of_EdgeIsDelaunay_3();
		TriangulationPoint_t3810082933 * L_1 = ___p0;
		int32_t L_2 = DelaunayTriangle_IndexOf_m1914770527(__this, L_1, /*hidden argument*/NULL);
		bool L_3 = FixedBitArray3_get_Item_m141500035(L_0, ((int32_t)((int32_t)((int32_t)((int32_t)L_2+(int32_t)1))%(int32_t)3)), /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Void Pathfinding.Poly2Tri.DelaunayTriangle::SetDelaunayEdgeCCW(Pathfinding.Poly2Tri.TriangulationPoint,System.Boolean)
extern "C"  void DelaunayTriangle_SetDelaunayEdgeCCW_m1549623684 (DelaunayTriangle_t2835103587 * __this, TriangulationPoint_t3810082933 * ___p0, bool ___ce1, const MethodInfo* method)
{
	{
		FixedBitArray3_t3665369095 * L_0 = __this->get_address_of_EdgeIsDelaunay_3();
		TriangulationPoint_t3810082933 * L_1 = ___p0;
		int32_t L_2 = DelaunayTriangle_IndexOf_m1914770527(__this, L_1, /*hidden argument*/NULL);
		bool L_3 = ___ce1;
		FixedBitArray3_set_Item_m2925852176(L_0, ((int32_t)((int32_t)((int32_t)((int32_t)L_2+(int32_t)2))%(int32_t)3)), L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Pathfinding.Poly2Tri.DelaunayTriangle::SetDelaunayEdgeCW(Pathfinding.Poly2Tri.TriangulationPoint,System.Boolean)
extern "C"  void DelaunayTriangle_SetDelaunayEdgeCW_m3698077983 (DelaunayTriangle_t2835103587 * __this, TriangulationPoint_t3810082933 * ___p0, bool ___ce1, const MethodInfo* method)
{
	{
		FixedBitArray3_t3665369095 * L_0 = __this->get_address_of_EdgeIsDelaunay_3();
		TriangulationPoint_t3810082933 * L_1 = ___p0;
		int32_t L_2 = DelaunayTriangle_IndexOf_m1914770527(__this, L_1, /*hidden argument*/NULL);
		bool L_3 = ___ce1;
		FixedBitArray3_set_Item_m2925852176(L_0, ((int32_t)((int32_t)((int32_t)((int32_t)L_2+(int32_t)1))%(int32_t)3)), L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Pathfinding.Poly2Tri.DTSweep::Triangulate(Pathfinding.Poly2Tri.DTSweepContext)
extern "C"  void DTSweep_Triangulate_m2688155415 (Il2CppObject * __this /* static, unused */, DTSweepContext_t543731303 * ___tcx0, const MethodInfo* method)
{
	{
		DTSweepContext_t543731303 * L_0 = ___tcx0;
		NullCheck(L_0);
		DTSweepContext_CreateAdvancingFront_m616900306(L_0, /*hidden argument*/NULL);
		DTSweepContext_t543731303 * L_1 = ___tcx0;
		DTSweep_Sweep_m212349143(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		DTSweepContext_t543731303 * L_2 = ___tcx0;
		NullCheck(L_2);
		int32_t L_3 = TriangulationContext_get_TriangulationMode_m3320394526(L_2, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_3) == ((uint32_t)2))))
		{
			goto IL_0023;
		}
	}
	{
		DTSweepContext_t543731303 * L_4 = ___tcx0;
		DTSweep_FinalizationPolygon_m2007995799(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		goto IL_0029;
	}

IL_0023:
	{
		DTSweepContext_t543731303 * L_5 = ___tcx0;
		DTSweep_FinalizationConvexHull_m4213943769(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
	}

IL_0029:
	{
		DTSweepContext_t543731303 * L_6 = ___tcx0;
		NullCheck(L_6);
		TriangulationContext_Done_m1328379643(L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Pathfinding.Poly2Tri.DTSweep::Sweep(Pathfinding.Poly2Tri.DTSweepContext)
extern const MethodInfo* List_1_get_Item_m2083438529_MethodInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m4157700883_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m1311676573_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m2462297823_MethodInfo_var;
extern const MethodInfo* Enumerator_Dispose_m2833563602_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m3716150292_MethodInfo_var;
extern const uint32_t DTSweep_Sweep_m212349143_MetadataUsageId;
extern "C"  void DTSweep_Sweep_m212349143 (Il2CppObject * __this /* static, unused */, DTSweepContext_t543731303 * ___tcx0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DTSweep_Sweep_m212349143_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t883301189 * V_0 = NULL;
	TriangulationPoint_t3810082933 * V_1 = NULL;
	AdvancingFrontNode_t688967424 * V_2 = NULL;
	int32_t V_3 = 0;
	Enumerator_t453170049  V_4;
	memset(&V_4, 0, sizeof(V_4));
	DTSweepConstraint_t3360279023 * V_5 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		DTSweepContext_t543731303 * L_0 = ___tcx0;
		NullCheck(L_0);
		List_1_t883301189 * L_1 = ((TriangulationContext_t3528662164 *)L_0)->get_Points_1();
		V_0 = L_1;
		V_3 = 1;
		goto IL_008f;
	}

IL_000e:
	{
		List_1_t883301189 * L_2 = V_0;
		int32_t L_3 = V_3;
		NullCheck(L_2);
		TriangulationPoint_t3810082933 * L_4 = List_1_get_Item_m2083438529(L_2, L_3, /*hidden argument*/List_1_get_Item_m2083438529_MethodInfo_var);
		V_1 = L_4;
		DTSweepContext_t543731303 * L_5 = ___tcx0;
		TriangulationPoint_t3810082933 * L_6 = V_1;
		AdvancingFrontNode_t688967424 * L_7 = DTSweep_PointEvent_m1518293845(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		V_2 = L_7;
		TriangulationPoint_t3810082933 * L_8 = V_1;
		NullCheck(L_8);
		bool L_9 = TriangulationPoint_get_HasEdges_m1603684009(L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0084;
		}
	}
	{
		TriangulationPoint_t3810082933 * L_10 = V_1;
		NullCheck(L_10);
		List_1_t433497279 * L_11 = TriangulationPoint_get_Edges_m2952612962(L_10, /*hidden argument*/NULL);
		NullCheck(L_11);
		Enumerator_t453170049  L_12 = List_1_GetEnumerator_m4157700883(L_11, /*hidden argument*/List_1_GetEnumerator_m4157700883_MethodInfo_var);
		V_4 = L_12;
	}

IL_0036:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0065;
		}

IL_003b:
		{
			DTSweepConstraint_t3360279023 * L_13 = Enumerator_get_Current_m1311676573((&V_4), /*hidden argument*/Enumerator_get_Current_m1311676573_MethodInfo_var);
			V_5 = L_13;
			DTSweepContext_t543731303 * L_14 = ___tcx0;
			NullCheck(L_14);
			bool L_15 = VirtFuncInvoker0< bool >::Invoke(8 /* System.Boolean Pathfinding.Poly2Tri.TriangulationContext::get_IsDebugEnabled() */, L_14);
			if (!L_15)
			{
				goto IL_005c;
			}
		}

IL_004f:
		{
			DTSweepContext_t543731303 * L_16 = ___tcx0;
			NullCheck(L_16);
			DTSweepDebugContext_t3829537966 * L_17 = TriangulationContext_get_DTDebugContext_m2409356104(L_16, /*hidden argument*/NULL);
			DTSweepConstraint_t3360279023 * L_18 = V_5;
			NullCheck(L_17);
			DTSweepDebugContext_set_ActiveConstraint_m915019815(L_17, L_18, /*hidden argument*/NULL);
		}

IL_005c:
		{
			DTSweepContext_t543731303 * L_19 = ___tcx0;
			DTSweepConstraint_t3360279023 * L_20 = V_5;
			AdvancingFrontNode_t688967424 * L_21 = V_2;
			DTSweep_EdgeEvent_m1794910471(NULL /*static, unused*/, L_19, L_20, L_21, /*hidden argument*/NULL);
		}

IL_0065:
		{
			bool L_22 = Enumerator_MoveNext_m2462297823((&V_4), /*hidden argument*/Enumerator_MoveNext_m2462297823_MethodInfo_var);
			if (L_22)
			{
				goto IL_003b;
			}
		}

IL_0071:
		{
			IL2CPP_LEAVE(0x84, FINALLY_0076);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0076;
	}

FINALLY_0076:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m2833563602((&V_4), /*hidden argument*/Enumerator_Dispose_m2833563602_MethodInfo_var);
		IL2CPP_END_FINALLY(118)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(118)
	{
		IL2CPP_JUMP_TBL(0x84, IL_0084)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0084:
	{
		DTSweepContext_t543731303 * L_23 = ___tcx0;
		NullCheck(L_23);
		TriangulationContext_Update_m3089402304(L_23, (String_t*)NULL, /*hidden argument*/NULL);
		int32_t L_24 = V_3;
		V_3 = ((int32_t)((int32_t)L_24+(int32_t)1));
	}

IL_008f:
	{
		int32_t L_25 = V_3;
		List_1_t883301189 * L_26 = V_0;
		NullCheck(L_26);
		int32_t L_27 = List_1_get_Count_m3716150292(L_26, /*hidden argument*/List_1_get_Count_m3716150292_MethodInfo_var);
		if ((((int32_t)L_25) < ((int32_t)L_27)))
		{
			goto IL_000e;
		}
	}
	{
		return;
	}
}
// System.Void Pathfinding.Poly2Tri.DTSweep::FinalizationConvexHull(Pathfinding.Poly2Tri.DTSweepContext)
extern "C"  void DTSweep_FinalizationConvexHull_m4213943769 (Il2CppObject * __this /* static, unused */, DTSweepContext_t543731303 * ___tcx0, const MethodInfo* method)
{
	AdvancingFrontNode_t688967424 * V_0 = NULL;
	AdvancingFrontNode_t688967424 * V_1 = NULL;
	AdvancingFrontNode_t688967424 * V_2 = NULL;
	DelaunayTriangle_t2835103587 * V_3 = NULL;
	TriangulationPoint_t3810082933 * V_4 = NULL;
	TriangulationPoint_t3810082933 * V_5 = NULL;
	{
		DTSweepContext_t543731303 * L_0 = ___tcx0;
		NullCheck(L_0);
		AdvancingFront_t240556382 * L_1 = L_0->get_Front_8();
		NullCheck(L_1);
		AdvancingFrontNode_t688967424 * L_2 = L_1->get_Head_0();
		NullCheck(L_2);
		AdvancingFrontNode_t688967424 * L_3 = L_2->get_Next_0();
		V_0 = L_3;
		AdvancingFrontNode_t688967424 * L_4 = V_0;
		NullCheck(L_4);
		AdvancingFrontNode_t688967424 * L_5 = L_4->get_Next_0();
		V_1 = L_5;
		AdvancingFrontNode_t688967424 * L_6 = V_1;
		NullCheck(L_6);
		AdvancingFrontNode_t688967424 * L_7 = L_6->get_Next_0();
		V_2 = L_7;
		AdvancingFrontNode_t688967424 * L_8 = V_0;
		NullCheck(L_8);
		TriangulationPoint_t3810082933 * L_9 = L_8->get_Point_3();
		V_4 = L_9;
		DTSweepContext_t543731303 * L_10 = ___tcx0;
		AdvancingFrontNode_t688967424 * L_11 = V_0;
		AdvancingFrontNode_t688967424 * L_12 = V_1;
		DTSweep_TurnAdvancingFrontConvex_m2832606575(NULL /*static, unused*/, L_10, L_11, L_12, /*hidden argument*/NULL);
		DTSweepContext_t543731303 * L_13 = ___tcx0;
		NullCheck(L_13);
		AdvancingFront_t240556382 * L_14 = L_13->get_Front_8();
		NullCheck(L_14);
		AdvancingFrontNode_t688967424 * L_15 = L_14->get_Tail_1();
		NullCheck(L_15);
		AdvancingFrontNode_t688967424 * L_16 = L_15->get_Prev_1();
		V_0 = L_16;
		AdvancingFrontNode_t688967424 * L_17 = V_0;
		NullCheck(L_17);
		DelaunayTriangle_t2835103587 * L_18 = L_17->get_Triangle_4();
		AdvancingFrontNode_t688967424 * L_19 = V_0;
		NullCheck(L_19);
		AdvancingFrontNode_t688967424 * L_20 = L_19->get_Next_0();
		NullCheck(L_20);
		TriangulationPoint_t3810082933 * L_21 = L_20->get_Point_3();
		NullCheck(L_18);
		bool L_22 = DelaunayTriangle_Contains_m3545485537(L_18, L_21, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_00bf;
		}
	}
	{
		AdvancingFrontNode_t688967424 * L_23 = V_0;
		NullCheck(L_23);
		DelaunayTriangle_t2835103587 * L_24 = L_23->get_Triangle_4();
		AdvancingFrontNode_t688967424 * L_25 = V_0;
		NullCheck(L_25);
		AdvancingFrontNode_t688967424 * L_26 = L_25->get_Prev_1();
		NullCheck(L_26);
		TriangulationPoint_t3810082933 * L_27 = L_26->get_Point_3();
		NullCheck(L_24);
		bool L_28 = DelaunayTriangle_Contains_m3545485537(L_24, L_27, /*hidden argument*/NULL);
		if (!L_28)
		{
			goto IL_00bf;
		}
	}
	{
		AdvancingFrontNode_t688967424 * L_29 = V_0;
		NullCheck(L_29);
		DelaunayTriangle_t2835103587 * L_30 = L_29->get_Triangle_4();
		AdvancingFrontNode_t688967424 * L_31 = V_0;
		NullCheck(L_31);
		TriangulationPoint_t3810082933 * L_32 = L_31->get_Point_3();
		NullCheck(L_30);
		DelaunayTriangle_t2835103587 * L_33 = DelaunayTriangle_NeighborAcrossFrom_m3663882196(L_30, L_32, /*hidden argument*/NULL);
		V_3 = L_33;
		AdvancingFrontNode_t688967424 * L_34 = V_0;
		NullCheck(L_34);
		DelaunayTriangle_t2835103587 * L_35 = L_34->get_Triangle_4();
		AdvancingFrontNode_t688967424 * L_36 = V_0;
		NullCheck(L_36);
		TriangulationPoint_t3810082933 * L_37 = L_36->get_Point_3();
		DelaunayTriangle_t2835103587 * L_38 = V_3;
		DelaunayTriangle_t2835103587 * L_39 = V_3;
		AdvancingFrontNode_t688967424 * L_40 = V_0;
		NullCheck(L_40);
		DelaunayTriangle_t2835103587 * L_41 = L_40->get_Triangle_4();
		AdvancingFrontNode_t688967424 * L_42 = V_0;
		NullCheck(L_42);
		TriangulationPoint_t3810082933 * L_43 = L_42->get_Point_3();
		NullCheck(L_39);
		TriangulationPoint_t3810082933 * L_44 = DelaunayTriangle_OppositePoint_m3734492474(L_39, L_41, L_43, /*hidden argument*/NULL);
		DTSweep_RotateTrianglePair_m2948867544(NULL /*static, unused*/, L_35, L_37, L_38, L_44, /*hidden argument*/NULL);
		DTSweepContext_t543731303 * L_45 = ___tcx0;
		AdvancingFrontNode_t688967424 * L_46 = V_0;
		NullCheck(L_46);
		DelaunayTriangle_t2835103587 * L_47 = L_46->get_Triangle_4();
		NullCheck(L_45);
		DTSweepContext_MapTriangleToNodes_m727745286(L_45, L_47, /*hidden argument*/NULL);
		DTSweepContext_t543731303 * L_48 = ___tcx0;
		DelaunayTriangle_t2835103587 * L_49 = V_3;
		NullCheck(L_48);
		DTSweepContext_MapTriangleToNodes_m727745286(L_48, L_49, /*hidden argument*/NULL);
	}

IL_00bf:
	{
		DTSweepContext_t543731303 * L_50 = ___tcx0;
		NullCheck(L_50);
		AdvancingFront_t240556382 * L_51 = L_50->get_Front_8();
		NullCheck(L_51);
		AdvancingFrontNode_t688967424 * L_52 = L_51->get_Head_0();
		NullCheck(L_52);
		AdvancingFrontNode_t688967424 * L_53 = L_52->get_Next_0();
		V_0 = L_53;
		AdvancingFrontNode_t688967424 * L_54 = V_0;
		NullCheck(L_54);
		DelaunayTriangle_t2835103587 * L_55 = L_54->get_Triangle_4();
		AdvancingFrontNode_t688967424 * L_56 = V_0;
		NullCheck(L_56);
		AdvancingFrontNode_t688967424 * L_57 = L_56->get_Prev_1();
		NullCheck(L_57);
		TriangulationPoint_t3810082933 * L_58 = L_57->get_Point_3();
		NullCheck(L_55);
		bool L_59 = DelaunayTriangle_Contains_m3545485537(L_55, L_58, /*hidden argument*/NULL);
		if (!L_59)
		{
			goto IL_014f;
		}
	}
	{
		AdvancingFrontNode_t688967424 * L_60 = V_0;
		NullCheck(L_60);
		DelaunayTriangle_t2835103587 * L_61 = L_60->get_Triangle_4();
		AdvancingFrontNode_t688967424 * L_62 = V_0;
		NullCheck(L_62);
		AdvancingFrontNode_t688967424 * L_63 = L_62->get_Next_0();
		NullCheck(L_63);
		TriangulationPoint_t3810082933 * L_64 = L_63->get_Point_3();
		NullCheck(L_61);
		bool L_65 = DelaunayTriangle_Contains_m3545485537(L_61, L_64, /*hidden argument*/NULL);
		if (!L_65)
		{
			goto IL_014f;
		}
	}
	{
		AdvancingFrontNode_t688967424 * L_66 = V_0;
		NullCheck(L_66);
		DelaunayTriangle_t2835103587 * L_67 = L_66->get_Triangle_4();
		AdvancingFrontNode_t688967424 * L_68 = V_0;
		NullCheck(L_68);
		TriangulationPoint_t3810082933 * L_69 = L_68->get_Point_3();
		NullCheck(L_67);
		DelaunayTriangle_t2835103587 * L_70 = DelaunayTriangle_NeighborAcrossFrom_m3663882196(L_67, L_69, /*hidden argument*/NULL);
		V_3 = L_70;
		AdvancingFrontNode_t688967424 * L_71 = V_0;
		NullCheck(L_71);
		DelaunayTriangle_t2835103587 * L_72 = L_71->get_Triangle_4();
		AdvancingFrontNode_t688967424 * L_73 = V_0;
		NullCheck(L_73);
		TriangulationPoint_t3810082933 * L_74 = L_73->get_Point_3();
		DelaunayTriangle_t2835103587 * L_75 = V_3;
		DelaunayTriangle_t2835103587 * L_76 = V_3;
		AdvancingFrontNode_t688967424 * L_77 = V_0;
		NullCheck(L_77);
		DelaunayTriangle_t2835103587 * L_78 = L_77->get_Triangle_4();
		AdvancingFrontNode_t688967424 * L_79 = V_0;
		NullCheck(L_79);
		TriangulationPoint_t3810082933 * L_80 = L_79->get_Point_3();
		NullCheck(L_76);
		TriangulationPoint_t3810082933 * L_81 = DelaunayTriangle_OppositePoint_m3734492474(L_76, L_78, L_80, /*hidden argument*/NULL);
		DTSweep_RotateTrianglePair_m2948867544(NULL /*static, unused*/, L_72, L_74, L_75, L_81, /*hidden argument*/NULL);
		DTSweepContext_t543731303 * L_82 = ___tcx0;
		AdvancingFrontNode_t688967424 * L_83 = V_0;
		NullCheck(L_83);
		DelaunayTriangle_t2835103587 * L_84 = L_83->get_Triangle_4();
		NullCheck(L_82);
		DTSweepContext_MapTriangleToNodes_m727745286(L_82, L_84, /*hidden argument*/NULL);
		DTSweepContext_t543731303 * L_85 = ___tcx0;
		DelaunayTriangle_t2835103587 * L_86 = V_3;
		NullCheck(L_85);
		DTSweepContext_MapTriangleToNodes_m727745286(L_85, L_86, /*hidden argument*/NULL);
	}

IL_014f:
	{
		DTSweepContext_t543731303 * L_87 = ___tcx0;
		NullCheck(L_87);
		AdvancingFront_t240556382 * L_88 = L_87->get_Front_8();
		NullCheck(L_88);
		AdvancingFrontNode_t688967424 * L_89 = L_88->get_Head_0();
		NullCheck(L_89);
		TriangulationPoint_t3810082933 * L_90 = L_89->get_Point_3();
		V_4 = L_90;
		DTSweepContext_t543731303 * L_91 = ___tcx0;
		NullCheck(L_91);
		AdvancingFront_t240556382 * L_92 = L_91->get_Front_8();
		NullCheck(L_92);
		AdvancingFrontNode_t688967424 * L_93 = L_92->get_Tail_1();
		NullCheck(L_93);
		AdvancingFrontNode_t688967424 * L_94 = L_93->get_Prev_1();
		V_1 = L_94;
		AdvancingFrontNode_t688967424 * L_95 = V_1;
		NullCheck(L_95);
		DelaunayTriangle_t2835103587 * L_96 = L_95->get_Triangle_4();
		V_3 = L_96;
		AdvancingFrontNode_t688967424 * L_97 = V_1;
		NullCheck(L_97);
		TriangulationPoint_t3810082933 * L_98 = L_97->get_Point_3();
		V_5 = L_98;
	}

IL_0181:
	{
		DTSweepContext_t543731303 * L_99 = ___tcx0;
		DelaunayTriangle_t2835103587 * L_100 = V_3;
		NullCheck(L_99);
		DTSweepContext_RemoveFromList_m3222250764(L_99, L_100, /*hidden argument*/NULL);
		DelaunayTriangle_t2835103587 * L_101 = V_3;
		TriangulationPoint_t3810082933 * L_102 = V_5;
		NullCheck(L_101);
		TriangulationPoint_t3810082933 * L_103 = DelaunayTriangle_PointCCWFrom_m667919724(L_101, L_102, /*hidden argument*/NULL);
		V_5 = L_103;
		TriangulationPoint_t3810082933 * L_104 = V_5;
		TriangulationPoint_t3810082933 * L_105 = V_4;
		if ((!(((Il2CppObject*)(TriangulationPoint_t3810082933 *)L_104) == ((Il2CppObject*)(TriangulationPoint_t3810082933 *)L_105))))
		{
			goto IL_01a0;
		}
	}
	{
		goto IL_01ae;
	}

IL_01a0:
	{
		DelaunayTriangle_t2835103587 * L_106 = V_3;
		TriangulationPoint_t3810082933 * L_107 = V_5;
		NullCheck(L_106);
		DelaunayTriangle_t2835103587 * L_108 = DelaunayTriangle_NeighborCCWFrom_m3747172644(L_106, L_107, /*hidden argument*/NULL);
		V_3 = L_108;
		goto IL_0181;
	}

IL_01ae:
	{
		DTSweepContext_t543731303 * L_109 = ___tcx0;
		NullCheck(L_109);
		AdvancingFront_t240556382 * L_110 = L_109->get_Front_8();
		NullCheck(L_110);
		AdvancingFrontNode_t688967424 * L_111 = L_110->get_Head_0();
		NullCheck(L_111);
		AdvancingFrontNode_t688967424 * L_112 = L_111->get_Next_0();
		NullCheck(L_112);
		TriangulationPoint_t3810082933 * L_113 = L_112->get_Point_3();
		V_4 = L_113;
		DelaunayTriangle_t2835103587 * L_114 = V_3;
		DTSweepContext_t543731303 * L_115 = ___tcx0;
		NullCheck(L_115);
		AdvancingFront_t240556382 * L_116 = L_115->get_Front_8();
		NullCheck(L_116);
		AdvancingFrontNode_t688967424 * L_117 = L_116->get_Head_0();
		NullCheck(L_117);
		TriangulationPoint_t3810082933 * L_118 = L_117->get_Point_3();
		NullCheck(L_114);
		TriangulationPoint_t3810082933 * L_119 = DelaunayTriangle_PointCWFrom_m3064891095(L_114, L_118, /*hidden argument*/NULL);
		V_5 = L_119;
		DelaunayTriangle_t2835103587 * L_120 = V_3;
		DTSweepContext_t543731303 * L_121 = ___tcx0;
		NullCheck(L_121);
		AdvancingFront_t240556382 * L_122 = L_121->get_Front_8();
		NullCheck(L_122);
		AdvancingFrontNode_t688967424 * L_123 = L_122->get_Head_0();
		NullCheck(L_123);
		TriangulationPoint_t3810082933 * L_124 = L_123->get_Point_3();
		NullCheck(L_120);
		DelaunayTriangle_t2835103587 * L_125 = DelaunayTriangle_NeighborCWFrom_m3856958495(L_120, L_124, /*hidden argument*/NULL);
		V_3 = L_125;
	}

IL_01f4:
	{
		DTSweepContext_t543731303 * L_126 = ___tcx0;
		DelaunayTriangle_t2835103587 * L_127 = V_3;
		NullCheck(L_126);
		DTSweepContext_RemoveFromList_m3222250764(L_126, L_127, /*hidden argument*/NULL);
		DelaunayTriangle_t2835103587 * L_128 = V_3;
		TriangulationPoint_t3810082933 * L_129 = V_5;
		NullCheck(L_128);
		TriangulationPoint_t3810082933 * L_130 = DelaunayTriangle_PointCCWFrom_m667919724(L_128, L_129, /*hidden argument*/NULL);
		V_5 = L_130;
		DelaunayTriangle_t2835103587 * L_131 = V_3;
		TriangulationPoint_t3810082933 * L_132 = V_5;
		NullCheck(L_131);
		DelaunayTriangle_t2835103587 * L_133 = DelaunayTriangle_NeighborCCWFrom_m3747172644(L_131, L_132, /*hidden argument*/NULL);
		V_3 = L_133;
		TriangulationPoint_t3810082933 * L_134 = V_5;
		TriangulationPoint_t3810082933 * L_135 = V_4;
		if ((!(((Il2CppObject*)(TriangulationPoint_t3810082933 *)L_134) == ((Il2CppObject*)(TriangulationPoint_t3810082933 *)L_135))))
		{
			goto IL_01f4;
		}
	}
	{
		DTSweepContext_t543731303 * L_136 = ___tcx0;
		NullCheck(L_136);
		DTSweepContext_FinalizeTriangulation_m615836583(L_136, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Pathfinding.Poly2Tri.DTSweep::TurnAdvancingFrontConvex(Pathfinding.Poly2Tri.DTSweepContext,Pathfinding.Poly2Tri.AdvancingFrontNode,Pathfinding.Poly2Tri.AdvancingFrontNode)
extern Il2CppClass* TriangulationUtil_t1170824903_il2cpp_TypeInfo_var;
extern const uint32_t DTSweep_TurnAdvancingFrontConvex_m2832606575_MetadataUsageId;
extern "C"  void DTSweep_TurnAdvancingFrontConvex_m2832606575 (Il2CppObject * __this /* static, unused */, DTSweepContext_t543731303 * ___tcx0, AdvancingFrontNode_t688967424 * ___b1, AdvancingFrontNode_t688967424 * ___c2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DTSweep_TurnAdvancingFrontConvex_m2832606575_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	AdvancingFrontNode_t688967424 * V_0 = NULL;
	{
		AdvancingFrontNode_t688967424 * L_0 = ___b1;
		V_0 = L_0;
		goto IL_009c;
	}

IL_0007:
	{
		DTSweepContext_t543731303 * L_1 = ___tcx0;
		NullCheck(L_1);
		bool L_2 = VirtFuncInvoker0< bool >::Invoke(8 /* System.Boolean Pathfinding.Poly2Tri.TriangulationContext::get_IsDebugEnabled() */, L_1);
		if (!L_2)
		{
			goto IL_001e;
		}
	}
	{
		DTSweepContext_t543731303 * L_3 = ___tcx0;
		NullCheck(L_3);
		DTSweepDebugContext_t3829537966 * L_4 = TriangulationContext_get_DTDebugContext_m2409356104(L_3, /*hidden argument*/NULL);
		AdvancingFrontNode_t688967424 * L_5 = ___c2;
		NullCheck(L_4);
		DTSweepDebugContext_set_ActiveNode_m1532614081(L_4, L_5, /*hidden argument*/NULL);
	}

IL_001e:
	{
		AdvancingFrontNode_t688967424 * L_6 = ___b1;
		NullCheck(L_6);
		TriangulationPoint_t3810082933 * L_7 = L_6->get_Point_3();
		AdvancingFrontNode_t688967424 * L_8 = ___c2;
		NullCheck(L_8);
		TriangulationPoint_t3810082933 * L_9 = L_8->get_Point_3();
		AdvancingFrontNode_t688967424 * L_10 = ___c2;
		NullCheck(L_10);
		AdvancingFrontNode_t688967424 * L_11 = L_10->get_Next_0();
		NullCheck(L_11);
		TriangulationPoint_t3810082933 * L_12 = L_11->get_Point_3();
		IL2CPP_RUNTIME_CLASS_INIT(TriangulationUtil_t1170824903_il2cpp_TypeInfo_var);
		int32_t L_13 = TriangulationUtil_Orient2d_m3842222219(NULL /*static, unused*/, L_7, L_9, L_12, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_13) == ((uint32_t)1))))
		{
			goto IL_0054;
		}
	}
	{
		DTSweepContext_t543731303 * L_14 = ___tcx0;
		AdvancingFrontNode_t688967424 * L_15 = ___c2;
		DTSweep_Fill_m2383500537(NULL /*static, unused*/, L_14, L_15, /*hidden argument*/NULL);
		AdvancingFrontNode_t688967424 * L_16 = ___c2;
		NullCheck(L_16);
		AdvancingFrontNode_t688967424 * L_17 = L_16->get_Next_0();
		___c2 = L_17;
		goto IL_009c;
	}

IL_0054:
	{
		AdvancingFrontNode_t688967424 * L_18 = ___b1;
		AdvancingFrontNode_t688967424 * L_19 = V_0;
		if ((((Il2CppObject*)(AdvancingFrontNode_t688967424 *)L_18) == ((Il2CppObject*)(AdvancingFrontNode_t688967424 *)L_19)))
		{
			goto IL_0091;
		}
	}
	{
		AdvancingFrontNode_t688967424 * L_20 = ___b1;
		NullCheck(L_20);
		AdvancingFrontNode_t688967424 * L_21 = L_20->get_Prev_1();
		NullCheck(L_21);
		TriangulationPoint_t3810082933 * L_22 = L_21->get_Point_3();
		AdvancingFrontNode_t688967424 * L_23 = ___b1;
		NullCheck(L_23);
		TriangulationPoint_t3810082933 * L_24 = L_23->get_Point_3();
		AdvancingFrontNode_t688967424 * L_25 = ___c2;
		NullCheck(L_25);
		TriangulationPoint_t3810082933 * L_26 = L_25->get_Point_3();
		IL2CPP_RUNTIME_CLASS_INIT(TriangulationUtil_t1170824903_il2cpp_TypeInfo_var);
		int32_t L_27 = TriangulationUtil_Orient2d_m3842222219(NULL /*static, unused*/, L_22, L_24, L_26, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_27) == ((uint32_t)1))))
		{
			goto IL_0091;
		}
	}
	{
		DTSweepContext_t543731303 * L_28 = ___tcx0;
		AdvancingFrontNode_t688967424 * L_29 = ___b1;
		DTSweep_Fill_m2383500537(NULL /*static, unused*/, L_28, L_29, /*hidden argument*/NULL);
		AdvancingFrontNode_t688967424 * L_30 = ___b1;
		NullCheck(L_30);
		AdvancingFrontNode_t688967424 * L_31 = L_30->get_Prev_1();
		___b1 = L_31;
		goto IL_009c;
	}

IL_0091:
	{
		AdvancingFrontNode_t688967424 * L_32 = ___c2;
		___b1 = L_32;
		AdvancingFrontNode_t688967424 * L_33 = ___c2;
		NullCheck(L_33);
		AdvancingFrontNode_t688967424 * L_34 = L_33->get_Next_0();
		___c2 = L_34;
	}

IL_009c:
	{
		AdvancingFrontNode_t688967424 * L_35 = ___c2;
		DTSweepContext_t543731303 * L_36 = ___tcx0;
		NullCheck(L_36);
		AdvancingFront_t240556382 * L_37 = L_36->get_Front_8();
		NullCheck(L_37);
		AdvancingFrontNode_t688967424 * L_38 = L_37->get_Tail_1();
		if ((!(((Il2CppObject*)(AdvancingFrontNode_t688967424 *)L_35) == ((Il2CppObject*)(AdvancingFrontNode_t688967424 *)L_38))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void Pathfinding.Poly2Tri.DTSweep::FinalizationPolygon(Pathfinding.Poly2Tri.DTSweepContext)
extern "C"  void DTSweep_FinalizationPolygon_m2007995799 (Il2CppObject * __this /* static, unused */, DTSweepContext_t543731303 * ___tcx0, const MethodInfo* method)
{
	DelaunayTriangle_t2835103587 * V_0 = NULL;
	TriangulationPoint_t3810082933 * V_1 = NULL;
	{
		DTSweepContext_t543731303 * L_0 = ___tcx0;
		NullCheck(L_0);
		AdvancingFront_t240556382 * L_1 = L_0->get_Front_8();
		NullCheck(L_1);
		AdvancingFrontNode_t688967424 * L_2 = L_1->get_Head_0();
		NullCheck(L_2);
		AdvancingFrontNode_t688967424 * L_3 = L_2->get_Next_0();
		NullCheck(L_3);
		DelaunayTriangle_t2835103587 * L_4 = L_3->get_Triangle_4();
		V_0 = L_4;
		DTSweepContext_t543731303 * L_5 = ___tcx0;
		NullCheck(L_5);
		AdvancingFront_t240556382 * L_6 = L_5->get_Front_8();
		NullCheck(L_6);
		AdvancingFrontNode_t688967424 * L_7 = L_6->get_Head_0();
		NullCheck(L_7);
		AdvancingFrontNode_t688967424 * L_8 = L_7->get_Next_0();
		NullCheck(L_8);
		TriangulationPoint_t3810082933 * L_9 = L_8->get_Point_3();
		V_1 = L_9;
		goto IL_0039;
	}

IL_0031:
	{
		DelaunayTriangle_t2835103587 * L_10 = V_0;
		TriangulationPoint_t3810082933 * L_11 = V_1;
		NullCheck(L_10);
		DelaunayTriangle_t2835103587 * L_12 = DelaunayTriangle_NeighborCCWFrom_m3747172644(L_10, L_11, /*hidden argument*/NULL);
		V_0 = L_12;
	}

IL_0039:
	{
		DelaunayTriangle_t2835103587 * L_13 = V_0;
		TriangulationPoint_t3810082933 * L_14 = V_1;
		NullCheck(L_13);
		bool L_15 = DelaunayTriangle_GetConstrainedEdgeCW_m1169250959(L_13, L_14, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_0031;
		}
	}
	{
		DTSweepContext_t543731303 * L_16 = ___tcx0;
		DelaunayTriangle_t2835103587 * L_17 = V_0;
		NullCheck(L_16);
		DTSweepContext_MeshClean_m3777320306(L_16, L_17, /*hidden argument*/NULL);
		return;
	}
}
// Pathfinding.Poly2Tri.AdvancingFrontNode Pathfinding.Poly2Tri.DTSweep::PointEvent(Pathfinding.Poly2Tri.DTSweepContext,Pathfinding.Poly2Tri.TriangulationPoint)
extern Il2CppClass* TriangulationUtil_t1170824903_il2cpp_TypeInfo_var;
extern const uint32_t DTSweep_PointEvent_m1518293845_MetadataUsageId;
extern "C"  AdvancingFrontNode_t688967424 * DTSweep_PointEvent_m1518293845 (Il2CppObject * __this /* static, unused */, DTSweepContext_t543731303 * ___tcx0, TriangulationPoint_t3810082933 * ___point1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DTSweep_PointEvent_m1518293845_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	AdvancingFrontNode_t688967424 * V_0 = NULL;
	AdvancingFrontNode_t688967424 * V_1 = NULL;
	{
		DTSweepContext_t543731303 * L_0 = ___tcx0;
		TriangulationPoint_t3810082933 * L_1 = ___point1;
		NullCheck(L_0);
		AdvancingFrontNode_t688967424 * L_2 = DTSweepContext_LocateNode_m451156442(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		DTSweepContext_t543731303 * L_3 = ___tcx0;
		NullCheck(L_3);
		bool L_4 = VirtFuncInvoker0< bool >::Invoke(8 /* System.Boolean Pathfinding.Poly2Tri.TriangulationContext::get_IsDebugEnabled() */, L_3);
		if (!L_4)
		{
			goto IL_001f;
		}
	}
	{
		DTSweepContext_t543731303 * L_5 = ___tcx0;
		NullCheck(L_5);
		DTSweepDebugContext_t3829537966 * L_6 = TriangulationContext_get_DTDebugContext_m2409356104(L_5, /*hidden argument*/NULL);
		AdvancingFrontNode_t688967424 * L_7 = V_0;
		NullCheck(L_6);
		DTSweepDebugContext_set_ActiveNode_m1532614081(L_6, L_7, /*hidden argument*/NULL);
	}

IL_001f:
	{
		DTSweepContext_t543731303 * L_8 = ___tcx0;
		TriangulationPoint_t3810082933 * L_9 = ___point1;
		AdvancingFrontNode_t688967424 * L_10 = V_0;
		AdvancingFrontNode_t688967424 * L_11 = DTSweep_NewFrontTriangle_m128967791(NULL /*static, unused*/, L_8, L_9, L_10, /*hidden argument*/NULL);
		V_1 = L_11;
		TriangulationPoint_t3810082933 * L_12 = ___point1;
		NullCheck(L_12);
		double L_13 = L_12->get_X_0();
		AdvancingFrontNode_t688967424 * L_14 = V_0;
		NullCheck(L_14);
		TriangulationPoint_t3810082933 * L_15 = L_14->get_Point_3();
		NullCheck(L_15);
		double L_16 = L_15->get_X_0();
		IL2CPP_RUNTIME_CLASS_INIT(TriangulationUtil_t1170824903_il2cpp_TypeInfo_var);
		double L_17 = ((TriangulationUtil_t1170824903_StaticFields*)TriangulationUtil_t1170824903_il2cpp_TypeInfo_var->static_fields)->get_EPSILON_0();
		if ((!(((double)L_13) <= ((double)((double)((double)L_16+(double)L_17))))))
		{
			goto IL_004b;
		}
	}
	{
		DTSweepContext_t543731303 * L_18 = ___tcx0;
		AdvancingFrontNode_t688967424 * L_19 = V_0;
		DTSweep_Fill_m2383500537(NULL /*static, unused*/, L_18, L_19, /*hidden argument*/NULL);
	}

IL_004b:
	{
		DTSweepContext_t543731303 * L_20 = ___tcx0;
		AdvancingFrontNode_t688967424 * L_21 = V_1;
		NullCheck(L_20);
		DTSweepContext_AddNode_m3594313838(L_20, L_21, /*hidden argument*/NULL);
		DTSweepContext_t543731303 * L_22 = ___tcx0;
		AdvancingFrontNode_t688967424 * L_23 = V_1;
		DTSweep_FillAdvancingFront_m2175963311(NULL /*static, unused*/, L_22, L_23, /*hidden argument*/NULL);
		AdvancingFrontNode_t688967424 * L_24 = V_1;
		return L_24;
	}
}
// Pathfinding.Poly2Tri.AdvancingFrontNode Pathfinding.Poly2Tri.DTSweep::NewFrontTriangle(Pathfinding.Poly2Tri.DTSweepContext,Pathfinding.Poly2Tri.TriangulationPoint,Pathfinding.Poly2Tri.AdvancingFrontNode)
extern Il2CppClass* DelaunayTriangle_t2835103587_il2cpp_TypeInfo_var;
extern Il2CppClass* AdvancingFrontNode_t688967424_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_Add_m1995366622_MethodInfo_var;
extern const uint32_t DTSweep_NewFrontTriangle_m128967791_MetadataUsageId;
extern "C"  AdvancingFrontNode_t688967424 * DTSweep_NewFrontTriangle_m128967791 (Il2CppObject * __this /* static, unused */, DTSweepContext_t543731303 * ___tcx0, TriangulationPoint_t3810082933 * ___point1, AdvancingFrontNode_t688967424 * ___node2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DTSweep_NewFrontTriangle_m128967791_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	AdvancingFrontNode_t688967424 * V_0 = NULL;
	DelaunayTriangle_t2835103587 * V_1 = NULL;
	{
		TriangulationPoint_t3810082933 * L_0 = ___point1;
		AdvancingFrontNode_t688967424 * L_1 = ___node2;
		NullCheck(L_1);
		TriangulationPoint_t3810082933 * L_2 = L_1->get_Point_3();
		AdvancingFrontNode_t688967424 * L_3 = ___node2;
		NullCheck(L_3);
		AdvancingFrontNode_t688967424 * L_4 = L_3->get_Next_0();
		NullCheck(L_4);
		TriangulationPoint_t3810082933 * L_5 = L_4->get_Point_3();
		DelaunayTriangle_t2835103587 * L_6 = (DelaunayTriangle_t2835103587 *)il2cpp_codegen_object_new(DelaunayTriangle_t2835103587_il2cpp_TypeInfo_var);
		DelaunayTriangle__ctor_m1610534382(L_6, L_0, L_2, L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		DelaunayTriangle_t2835103587 * L_7 = V_1;
		AdvancingFrontNode_t688967424 * L_8 = ___node2;
		NullCheck(L_8);
		DelaunayTriangle_t2835103587 * L_9 = L_8->get_Triangle_4();
		NullCheck(L_7);
		DelaunayTriangle_MarkNeighbor_m119719133(L_7, L_9, /*hidden argument*/NULL);
		DTSweepContext_t543731303 * L_10 = ___tcx0;
		NullCheck(L_10);
		List_1_t4203289139 * L_11 = ((TriangulationContext_t3528662164 *)L_10)->get_Triangles_0();
		DelaunayTriangle_t2835103587 * L_12 = V_1;
		NullCheck(L_11);
		List_1_Add_m1995366622(L_11, L_12, /*hidden argument*/List_1_Add_m1995366622_MethodInfo_var);
		TriangulationPoint_t3810082933 * L_13 = ___point1;
		AdvancingFrontNode_t688967424 * L_14 = (AdvancingFrontNode_t688967424 *)il2cpp_codegen_object_new(AdvancingFrontNode_t688967424_il2cpp_TypeInfo_var);
		AdvancingFrontNode__ctor_m294490163(L_14, L_13, /*hidden argument*/NULL);
		V_0 = L_14;
		AdvancingFrontNode_t688967424 * L_15 = V_0;
		AdvancingFrontNode_t688967424 * L_16 = ___node2;
		NullCheck(L_16);
		AdvancingFrontNode_t688967424 * L_17 = L_16->get_Next_0();
		NullCheck(L_15);
		L_15->set_Next_0(L_17);
		AdvancingFrontNode_t688967424 * L_18 = V_0;
		AdvancingFrontNode_t688967424 * L_19 = ___node2;
		NullCheck(L_18);
		L_18->set_Prev_1(L_19);
		AdvancingFrontNode_t688967424 * L_20 = ___node2;
		NullCheck(L_20);
		AdvancingFrontNode_t688967424 * L_21 = L_20->get_Next_0();
		AdvancingFrontNode_t688967424 * L_22 = V_0;
		NullCheck(L_21);
		L_21->set_Prev_1(L_22);
		AdvancingFrontNode_t688967424 * L_23 = ___node2;
		AdvancingFrontNode_t688967424 * L_24 = V_0;
		NullCheck(L_23);
		L_23->set_Next_0(L_24);
		DTSweepContext_t543731303 * L_25 = ___tcx0;
		AdvancingFrontNode_t688967424 * L_26 = V_0;
		NullCheck(L_25);
		DTSweepContext_AddNode_m3594313838(L_25, L_26, /*hidden argument*/NULL);
		DTSweepContext_t543731303 * L_27 = ___tcx0;
		NullCheck(L_27);
		bool L_28 = VirtFuncInvoker0< bool >::Invoke(8 /* System.Boolean Pathfinding.Poly2Tri.TriangulationContext::get_IsDebugEnabled() */, L_27);
		if (!L_28)
		{
			goto IL_007b;
		}
	}
	{
		DTSweepContext_t543731303 * L_29 = ___tcx0;
		NullCheck(L_29);
		DTSweepDebugContext_t3829537966 * L_30 = TriangulationContext_get_DTDebugContext_m2409356104(L_29, /*hidden argument*/NULL);
		AdvancingFrontNode_t688967424 * L_31 = V_0;
		NullCheck(L_30);
		DTSweepDebugContext_set_ActiveNode_m1532614081(L_30, L_31, /*hidden argument*/NULL);
	}

IL_007b:
	{
		DTSweepContext_t543731303 * L_32 = ___tcx0;
		DelaunayTriangle_t2835103587 * L_33 = V_1;
		bool L_34 = DTSweep_Legalize_m2173221096(NULL /*static, unused*/, L_32, L_33, /*hidden argument*/NULL);
		if (L_34)
		{
			goto IL_008e;
		}
	}
	{
		DTSweepContext_t543731303 * L_35 = ___tcx0;
		DelaunayTriangle_t2835103587 * L_36 = V_1;
		NullCheck(L_35);
		DTSweepContext_MapTriangleToNodes_m727745286(L_35, L_36, /*hidden argument*/NULL);
	}

IL_008e:
	{
		AdvancingFrontNode_t688967424 * L_37 = V_0;
		return L_37;
	}
}
// System.Void Pathfinding.Poly2Tri.DTSweep::EdgeEvent(Pathfinding.Poly2Tri.DTSweepContext,Pathfinding.Poly2Tri.DTSweepConstraint,Pathfinding.Poly2Tri.AdvancingFrontNode)
extern Il2CppClass* PointOnEdgeException_t989229367_il2cpp_TypeInfo_var;
extern const uint32_t DTSweep_EdgeEvent_m1794910471_MetadataUsageId;
extern "C"  void DTSweep_EdgeEvent_m1794910471 (Il2CppObject * __this /* static, unused */, DTSweepContext_t543731303 * ___tcx0, DTSweepConstraint_t3360279023 * ___edge1, AdvancingFrontNode_t688967424 * ___node2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DTSweep_EdgeEvent_m1794910471_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	PointOnEdgeException_t989229367 * V_0 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			DTSweepContext_t543731303 * L_0 = ___tcx0;
			NullCheck(L_0);
			DTSweepEdgeEvent_t3441061589 * L_1 = L_0->get_EdgeEvent_10();
			DTSweepConstraint_t3360279023 * L_2 = ___edge1;
			NullCheck(L_1);
			L_1->set_ConstrainedEdge_0(L_2);
			DTSweepContext_t543731303 * L_3 = ___tcx0;
			NullCheck(L_3);
			DTSweepEdgeEvent_t3441061589 * L_4 = L_3->get_EdgeEvent_10();
			DTSweepConstraint_t3360279023 * L_5 = ___edge1;
			NullCheck(L_5);
			TriangulationPoint_t3810082933 * L_6 = ((TriangulationConstraint_t137695394 *)L_5)->get_P_0();
			NullCheck(L_6);
			double L_7 = L_6->get_X_0();
			DTSweepConstraint_t3360279023 * L_8 = ___edge1;
			NullCheck(L_8);
			TriangulationPoint_t3810082933 * L_9 = ((TriangulationConstraint_t137695394 *)L_8)->get_Q_1();
			NullCheck(L_9);
			double L_10 = L_9->get_X_0();
			NullCheck(L_4);
			L_4->set_Right_1((bool)((((double)L_7) > ((double)L_10))? 1 : 0));
			DTSweepContext_t543731303 * L_11 = ___tcx0;
			NullCheck(L_11);
			bool L_12 = VirtFuncInvoker0< bool >::Invoke(8 /* System.Boolean Pathfinding.Poly2Tri.TriangulationContext::get_IsDebugEnabled() */, L_11);
			if (!L_12)
			{
				goto IL_004b;
			}
		}

IL_003a:
		{
			DTSweepContext_t543731303 * L_13 = ___tcx0;
			NullCheck(L_13);
			DTSweepDebugContext_t3829537966 * L_14 = TriangulationContext_get_DTDebugContext_m2409356104(L_13, /*hidden argument*/NULL);
			AdvancingFrontNode_t688967424 * L_15 = ___node2;
			NullCheck(L_15);
			DelaunayTriangle_t2835103587 * L_16 = L_15->get_Triangle_4();
			NullCheck(L_14);
			DTSweepDebugContext_set_PrimaryTriangle_m527373270(L_14, L_16, /*hidden argument*/NULL);
		}

IL_004b:
		{
			AdvancingFrontNode_t688967424 * L_17 = ___node2;
			NullCheck(L_17);
			DelaunayTriangle_t2835103587 * L_18 = L_17->get_Triangle_4();
			DTSweepConstraint_t3360279023 * L_19 = ___edge1;
			NullCheck(L_19);
			TriangulationPoint_t3810082933 * L_20 = ((TriangulationConstraint_t137695394 *)L_19)->get_P_0();
			DTSweepConstraint_t3360279023 * L_21 = ___edge1;
			NullCheck(L_21);
			TriangulationPoint_t3810082933 * L_22 = ((TriangulationConstraint_t137695394 *)L_21)->get_Q_1();
			bool L_23 = DTSweep_IsEdgeSideOfTriangle_m3418112580(NULL /*static, unused*/, L_18, L_20, L_22, /*hidden argument*/NULL);
			if (!L_23)
			{
				goto IL_006c;
			}
		}

IL_0067:
		{
			goto IL_009f;
		}

IL_006c:
		{
			DTSweepContext_t543731303 * L_24 = ___tcx0;
			DTSweepConstraint_t3360279023 * L_25 = ___edge1;
			AdvancingFrontNode_t688967424 * L_26 = ___node2;
			DTSweep_FillEdgeEvent_m3097164868(NULL /*static, unused*/, L_24, L_25, L_26, /*hidden argument*/NULL);
			DTSweepContext_t543731303 * L_27 = ___tcx0;
			DTSweepConstraint_t3360279023 * L_28 = ___edge1;
			NullCheck(L_28);
			TriangulationPoint_t3810082933 * L_29 = ((TriangulationConstraint_t137695394 *)L_28)->get_P_0();
			DTSweepConstraint_t3360279023 * L_30 = ___edge1;
			NullCheck(L_30);
			TriangulationPoint_t3810082933 * L_31 = ((TriangulationConstraint_t137695394 *)L_30)->get_Q_1();
			AdvancingFrontNode_t688967424 * L_32 = ___node2;
			NullCheck(L_32);
			DelaunayTriangle_t2835103587 * L_33 = L_32->get_Triangle_4();
			DTSweepConstraint_t3360279023 * L_34 = ___edge1;
			NullCheck(L_34);
			TriangulationPoint_t3810082933 * L_35 = ((TriangulationConstraint_t137695394 *)L_34)->get_Q_1();
			DTSweep_EdgeEvent_m1498452168(NULL /*static, unused*/, L_27, L_29, L_31, L_33, L_35, /*hidden argument*/NULL);
			goto IL_009f;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3991598821 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (PointOnEdgeException_t989229367_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0097;
		throw e;
	}

CATCH_0097:
	{ // begin catch(Pathfinding.Poly2Tri.PointOnEdgeException)
		{
			V_0 = ((PointOnEdgeException_t989229367 *)__exception_local);
			IL2CPP_RAISE_MANAGED_EXCEPTION(__exception_local);
		}

IL_009a:
		{
			goto IL_009f;
		}
	} // end catch (depth: 1)

IL_009f:
	{
		return;
	}
}
// System.Void Pathfinding.Poly2Tri.DTSweep::FillEdgeEvent(Pathfinding.Poly2Tri.DTSweepContext,Pathfinding.Poly2Tri.DTSweepConstraint,Pathfinding.Poly2Tri.AdvancingFrontNode)
extern "C"  void DTSweep_FillEdgeEvent_m3097164868 (Il2CppObject * __this /* static, unused */, DTSweepContext_t543731303 * ___tcx0, DTSweepConstraint_t3360279023 * ___edge1, AdvancingFrontNode_t688967424 * ___node2, const MethodInfo* method)
{
	{
		DTSweepContext_t543731303 * L_0 = ___tcx0;
		NullCheck(L_0);
		DTSweepEdgeEvent_t3441061589 * L_1 = L_0->get_EdgeEvent_10();
		NullCheck(L_1);
		bool L_2 = L_1->get_Right_1();
		if (!L_2)
		{
			goto IL_001d;
		}
	}
	{
		DTSweepContext_t543731303 * L_3 = ___tcx0;
		DTSweepConstraint_t3360279023 * L_4 = ___edge1;
		AdvancingFrontNode_t688967424 * L_5 = ___node2;
		DTSweep_FillRightAboveEdgeEvent_m470772131(NULL /*static, unused*/, L_3, L_4, L_5, /*hidden argument*/NULL);
		goto IL_0025;
	}

IL_001d:
	{
		DTSweepContext_t543731303 * L_6 = ___tcx0;
		DTSweepConstraint_t3360279023 * L_7 = ___edge1;
		AdvancingFrontNode_t688967424 * L_8 = ___node2;
		DTSweep_FillLeftAboveEdgeEvent_m2067732284(NULL /*static, unused*/, L_6, L_7, L_8, /*hidden argument*/NULL);
	}

IL_0025:
	{
		return;
	}
}
// System.Void Pathfinding.Poly2Tri.DTSweep::FillRightConcaveEdgeEvent(Pathfinding.Poly2Tri.DTSweepContext,Pathfinding.Poly2Tri.DTSweepConstraint,Pathfinding.Poly2Tri.AdvancingFrontNode)
extern Il2CppClass* TriangulationUtil_t1170824903_il2cpp_TypeInfo_var;
extern const uint32_t DTSweep_FillRightConcaveEdgeEvent_m868064561_MetadataUsageId;
extern "C"  void DTSweep_FillRightConcaveEdgeEvent_m868064561 (Il2CppObject * __this /* static, unused */, DTSweepContext_t543731303 * ___tcx0, DTSweepConstraint_t3360279023 * ___edge1, AdvancingFrontNode_t688967424 * ___node2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DTSweep_FillRightConcaveEdgeEvent_m868064561_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		DTSweepContext_t543731303 * L_0 = ___tcx0;
		AdvancingFrontNode_t688967424 * L_1 = ___node2;
		NullCheck(L_1);
		AdvancingFrontNode_t688967424 * L_2 = L_1->get_Next_0();
		DTSweep_Fill_m2383500537(NULL /*static, unused*/, L_0, L_2, /*hidden argument*/NULL);
		AdvancingFrontNode_t688967424 * L_3 = ___node2;
		NullCheck(L_3);
		AdvancingFrontNode_t688967424 * L_4 = L_3->get_Next_0();
		NullCheck(L_4);
		TriangulationPoint_t3810082933 * L_5 = L_4->get_Point_3();
		DTSweepConstraint_t3360279023 * L_6 = ___edge1;
		NullCheck(L_6);
		TriangulationPoint_t3810082933 * L_7 = ((TriangulationConstraint_t137695394 *)L_6)->get_P_0();
		if ((((Il2CppObject*)(TriangulationPoint_t3810082933 *)L_5) == ((Il2CppObject*)(TriangulationPoint_t3810082933 *)L_7)))
		{
			goto IL_007d;
		}
	}
	{
		DTSweepConstraint_t3360279023 * L_8 = ___edge1;
		NullCheck(L_8);
		TriangulationPoint_t3810082933 * L_9 = ((TriangulationConstraint_t137695394 *)L_8)->get_Q_1();
		AdvancingFrontNode_t688967424 * L_10 = ___node2;
		NullCheck(L_10);
		AdvancingFrontNode_t688967424 * L_11 = L_10->get_Next_0();
		NullCheck(L_11);
		TriangulationPoint_t3810082933 * L_12 = L_11->get_Point_3();
		DTSweepConstraint_t3360279023 * L_13 = ___edge1;
		NullCheck(L_13);
		TriangulationPoint_t3810082933 * L_14 = ((TriangulationConstraint_t137695394 *)L_13)->get_P_0();
		IL2CPP_RUNTIME_CLASS_INIT(TriangulationUtil_t1170824903_il2cpp_TypeInfo_var);
		int32_t L_15 = TriangulationUtil_Orient2d_m3842222219(NULL /*static, unused*/, L_9, L_12, L_14, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_15) == ((uint32_t)1))))
		{
			goto IL_007d;
		}
	}
	{
		AdvancingFrontNode_t688967424 * L_16 = ___node2;
		NullCheck(L_16);
		TriangulationPoint_t3810082933 * L_17 = L_16->get_Point_3();
		AdvancingFrontNode_t688967424 * L_18 = ___node2;
		NullCheck(L_18);
		AdvancingFrontNode_t688967424 * L_19 = L_18->get_Next_0();
		NullCheck(L_19);
		TriangulationPoint_t3810082933 * L_20 = L_19->get_Point_3();
		AdvancingFrontNode_t688967424 * L_21 = ___node2;
		NullCheck(L_21);
		AdvancingFrontNode_t688967424 * L_22 = L_21->get_Next_0();
		NullCheck(L_22);
		AdvancingFrontNode_t688967424 * L_23 = L_22->get_Next_0();
		NullCheck(L_23);
		TriangulationPoint_t3810082933 * L_24 = L_23->get_Point_3();
		IL2CPP_RUNTIME_CLASS_INIT(TriangulationUtil_t1170824903_il2cpp_TypeInfo_var);
		int32_t L_25 = TriangulationUtil_Orient2d_m3842222219(NULL /*static, unused*/, L_17, L_20, L_24, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_25) == ((uint32_t)1))))
		{
			goto IL_007d;
		}
	}
	{
		DTSweepContext_t543731303 * L_26 = ___tcx0;
		DTSweepConstraint_t3360279023 * L_27 = ___edge1;
		AdvancingFrontNode_t688967424 * L_28 = ___node2;
		DTSweep_FillRightConcaveEdgeEvent_m868064561(NULL /*static, unused*/, L_26, L_27, L_28, /*hidden argument*/NULL);
		goto IL_007d;
	}

IL_007d:
	{
		return;
	}
}
// System.Void Pathfinding.Poly2Tri.DTSweep::FillRightConvexEdgeEvent(Pathfinding.Poly2Tri.DTSweepContext,Pathfinding.Poly2Tri.DTSweepConstraint,Pathfinding.Poly2Tri.AdvancingFrontNode)
extern Il2CppClass* TriangulationUtil_t1170824903_il2cpp_TypeInfo_var;
extern const uint32_t DTSweep_FillRightConvexEdgeEvent_m226090063_MetadataUsageId;
extern "C"  void DTSweep_FillRightConvexEdgeEvent_m226090063 (Il2CppObject * __this /* static, unused */, DTSweepContext_t543731303 * ___tcx0, DTSweepConstraint_t3360279023 * ___edge1, AdvancingFrontNode_t688967424 * ___node2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DTSweep_FillRightConvexEdgeEvent_m226090063_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		AdvancingFrontNode_t688967424 * L_0 = ___node2;
		NullCheck(L_0);
		AdvancingFrontNode_t688967424 * L_1 = L_0->get_Next_0();
		NullCheck(L_1);
		TriangulationPoint_t3810082933 * L_2 = L_1->get_Point_3();
		AdvancingFrontNode_t688967424 * L_3 = ___node2;
		NullCheck(L_3);
		AdvancingFrontNode_t688967424 * L_4 = L_3->get_Next_0();
		NullCheck(L_4);
		AdvancingFrontNode_t688967424 * L_5 = L_4->get_Next_0();
		NullCheck(L_5);
		TriangulationPoint_t3810082933 * L_6 = L_5->get_Point_3();
		AdvancingFrontNode_t688967424 * L_7 = ___node2;
		NullCheck(L_7);
		AdvancingFrontNode_t688967424 * L_8 = L_7->get_Next_0();
		NullCheck(L_8);
		AdvancingFrontNode_t688967424 * L_9 = L_8->get_Next_0();
		NullCheck(L_9);
		AdvancingFrontNode_t688967424 * L_10 = L_9->get_Next_0();
		NullCheck(L_10);
		TriangulationPoint_t3810082933 * L_11 = L_10->get_Point_3();
		IL2CPP_RUNTIME_CLASS_INIT(TriangulationUtil_t1170824903_il2cpp_TypeInfo_var);
		int32_t L_12 = TriangulationUtil_Orient2d_m3842222219(NULL /*static, unused*/, L_2, L_6, L_11, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_12) == ((uint32_t)1))))
		{
			goto IL_004d;
		}
	}
	{
		DTSweepContext_t543731303 * L_13 = ___tcx0;
		DTSweepConstraint_t3360279023 * L_14 = ___edge1;
		AdvancingFrontNode_t688967424 * L_15 = ___node2;
		NullCheck(L_15);
		AdvancingFrontNode_t688967424 * L_16 = L_15->get_Next_0();
		DTSweep_FillRightConcaveEdgeEvent_m868064561(NULL /*static, unused*/, L_13, L_14, L_16, /*hidden argument*/NULL);
		goto IL_0086;
	}

IL_004d:
	{
		DTSweepConstraint_t3360279023 * L_17 = ___edge1;
		NullCheck(L_17);
		TriangulationPoint_t3810082933 * L_18 = ((TriangulationConstraint_t137695394 *)L_17)->get_Q_1();
		AdvancingFrontNode_t688967424 * L_19 = ___node2;
		NullCheck(L_19);
		AdvancingFrontNode_t688967424 * L_20 = L_19->get_Next_0();
		NullCheck(L_20);
		AdvancingFrontNode_t688967424 * L_21 = L_20->get_Next_0();
		NullCheck(L_21);
		TriangulationPoint_t3810082933 * L_22 = L_21->get_Point_3();
		DTSweepConstraint_t3360279023 * L_23 = ___edge1;
		NullCheck(L_23);
		TriangulationPoint_t3810082933 * L_24 = ((TriangulationConstraint_t137695394 *)L_23)->get_P_0();
		IL2CPP_RUNTIME_CLASS_INIT(TriangulationUtil_t1170824903_il2cpp_TypeInfo_var);
		int32_t L_25 = TriangulationUtil_Orient2d_m3842222219(NULL /*static, unused*/, L_18, L_22, L_24, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_25) == ((uint32_t)1))))
		{
			goto IL_0086;
		}
	}
	{
		DTSweepContext_t543731303 * L_26 = ___tcx0;
		DTSweepConstraint_t3360279023 * L_27 = ___edge1;
		AdvancingFrontNode_t688967424 * L_28 = ___node2;
		NullCheck(L_28);
		AdvancingFrontNode_t688967424 * L_29 = L_28->get_Next_0();
		DTSweep_FillRightConvexEdgeEvent_m226090063(NULL /*static, unused*/, L_26, L_27, L_29, /*hidden argument*/NULL);
		goto IL_0086;
	}

IL_0086:
	{
		return;
	}
}
// System.Void Pathfinding.Poly2Tri.DTSweep::FillRightBelowEdgeEvent(Pathfinding.Poly2Tri.DTSweepContext,Pathfinding.Poly2Tri.DTSweepConstraint,Pathfinding.Poly2Tri.AdvancingFrontNode)
extern Il2CppClass* TriangulationUtil_t1170824903_il2cpp_TypeInfo_var;
extern const uint32_t DTSweep_FillRightBelowEdgeEvent_m2212243855_MetadataUsageId;
extern "C"  void DTSweep_FillRightBelowEdgeEvent_m2212243855 (Il2CppObject * __this /* static, unused */, DTSweepContext_t543731303 * ___tcx0, DTSweepConstraint_t3360279023 * ___edge1, AdvancingFrontNode_t688967424 * ___node2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DTSweep_FillRightBelowEdgeEvent_m2212243855_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		DTSweepContext_t543731303 * L_0 = ___tcx0;
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(8 /* System.Boolean Pathfinding.Poly2Tri.TriangulationContext::get_IsDebugEnabled() */, L_0);
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		DTSweepContext_t543731303 * L_2 = ___tcx0;
		NullCheck(L_2);
		DTSweepDebugContext_t3829537966 * L_3 = TriangulationContext_get_DTDebugContext_m2409356104(L_2, /*hidden argument*/NULL);
		AdvancingFrontNode_t688967424 * L_4 = ___node2;
		NullCheck(L_3);
		DTSweepDebugContext_set_ActiveNode_m1532614081(L_3, L_4, /*hidden argument*/NULL);
	}

IL_0017:
	{
		AdvancingFrontNode_t688967424 * L_5 = ___node2;
		NullCheck(L_5);
		TriangulationPoint_t3810082933 * L_6 = L_5->get_Point_3();
		NullCheck(L_6);
		double L_7 = L_6->get_X_0();
		DTSweepConstraint_t3360279023 * L_8 = ___edge1;
		NullCheck(L_8);
		TriangulationPoint_t3810082933 * L_9 = ((TriangulationConstraint_t137695394 *)L_8)->get_P_0();
		NullCheck(L_9);
		double L_10 = L_9->get_X_0();
		if ((!(((double)L_7) < ((double)L_10))))
		{
			goto IL_007b;
		}
	}
	{
		AdvancingFrontNode_t688967424 * L_11 = ___node2;
		NullCheck(L_11);
		TriangulationPoint_t3810082933 * L_12 = L_11->get_Point_3();
		AdvancingFrontNode_t688967424 * L_13 = ___node2;
		NullCheck(L_13);
		AdvancingFrontNode_t688967424 * L_14 = L_13->get_Next_0();
		NullCheck(L_14);
		TriangulationPoint_t3810082933 * L_15 = L_14->get_Point_3();
		AdvancingFrontNode_t688967424 * L_16 = ___node2;
		NullCheck(L_16);
		AdvancingFrontNode_t688967424 * L_17 = L_16->get_Next_0();
		NullCheck(L_17);
		AdvancingFrontNode_t688967424 * L_18 = L_17->get_Next_0();
		NullCheck(L_18);
		TriangulationPoint_t3810082933 * L_19 = L_18->get_Point_3();
		IL2CPP_RUNTIME_CLASS_INIT(TriangulationUtil_t1170824903_il2cpp_TypeInfo_var);
		int32_t L_20 = TriangulationUtil_Orient2d_m3842222219(NULL /*static, unused*/, L_12, L_15, L_19, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_20) == ((uint32_t)1))))
		{
			goto IL_006b;
		}
	}
	{
		DTSweepContext_t543731303 * L_21 = ___tcx0;
		DTSweepConstraint_t3360279023 * L_22 = ___edge1;
		AdvancingFrontNode_t688967424 * L_23 = ___node2;
		DTSweep_FillRightConcaveEdgeEvent_m868064561(NULL /*static, unused*/, L_21, L_22, L_23, /*hidden argument*/NULL);
		goto IL_007b;
	}

IL_006b:
	{
		DTSweepContext_t543731303 * L_24 = ___tcx0;
		DTSweepConstraint_t3360279023 * L_25 = ___edge1;
		AdvancingFrontNode_t688967424 * L_26 = ___node2;
		DTSweep_FillRightConvexEdgeEvent_m226090063(NULL /*static, unused*/, L_24, L_25, L_26, /*hidden argument*/NULL);
		DTSweepContext_t543731303 * L_27 = ___tcx0;
		DTSweepConstraint_t3360279023 * L_28 = ___edge1;
		AdvancingFrontNode_t688967424 * L_29 = ___node2;
		DTSweep_FillRightBelowEdgeEvent_m2212243855(NULL /*static, unused*/, L_27, L_28, L_29, /*hidden argument*/NULL);
	}

IL_007b:
	{
		return;
	}
}
// System.Void Pathfinding.Poly2Tri.DTSweep::FillRightAboveEdgeEvent(Pathfinding.Poly2Tri.DTSweepContext,Pathfinding.Poly2Tri.DTSweepConstraint,Pathfinding.Poly2Tri.AdvancingFrontNode)
extern Il2CppClass* TriangulationUtil_t1170824903_il2cpp_TypeInfo_var;
extern const uint32_t DTSweep_FillRightAboveEdgeEvent_m470772131_MetadataUsageId;
extern "C"  void DTSweep_FillRightAboveEdgeEvent_m470772131 (Il2CppObject * __this /* static, unused */, DTSweepContext_t543731303 * ___tcx0, DTSweepConstraint_t3360279023 * ___edge1, AdvancingFrontNode_t688967424 * ___node2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DTSweep_FillRightAboveEdgeEvent_m470772131_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		goto IL_0055;
	}

IL_0005:
	{
		DTSweepContext_t543731303 * L_0 = ___tcx0;
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(8 /* System.Boolean Pathfinding.Poly2Tri.TriangulationContext::get_IsDebugEnabled() */, L_0);
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		DTSweepContext_t543731303 * L_2 = ___tcx0;
		NullCheck(L_2);
		DTSweepDebugContext_t3829537966 * L_3 = TriangulationContext_get_DTDebugContext_m2409356104(L_2, /*hidden argument*/NULL);
		AdvancingFrontNode_t688967424 * L_4 = ___node2;
		NullCheck(L_3);
		DTSweepDebugContext_set_ActiveNode_m1532614081(L_3, L_4, /*hidden argument*/NULL);
	}

IL_001c:
	{
		DTSweepConstraint_t3360279023 * L_5 = ___edge1;
		NullCheck(L_5);
		TriangulationPoint_t3810082933 * L_6 = ((TriangulationConstraint_t137695394 *)L_5)->get_Q_1();
		AdvancingFrontNode_t688967424 * L_7 = ___node2;
		NullCheck(L_7);
		AdvancingFrontNode_t688967424 * L_8 = L_7->get_Next_0();
		NullCheck(L_8);
		TriangulationPoint_t3810082933 * L_9 = L_8->get_Point_3();
		DTSweepConstraint_t3360279023 * L_10 = ___edge1;
		NullCheck(L_10);
		TriangulationPoint_t3810082933 * L_11 = ((TriangulationConstraint_t137695394 *)L_10)->get_P_0();
		IL2CPP_RUNTIME_CLASS_INIT(TriangulationUtil_t1170824903_il2cpp_TypeInfo_var);
		int32_t L_12 = TriangulationUtil_Orient2d_m3842222219(NULL /*static, unused*/, L_6, L_9, L_11, /*hidden argument*/NULL);
		V_0 = L_12;
		int32_t L_13 = V_0;
		if ((!(((uint32_t)L_13) == ((uint32_t)1))))
		{
			goto IL_004d;
		}
	}
	{
		DTSweepContext_t543731303 * L_14 = ___tcx0;
		DTSweepConstraint_t3360279023 * L_15 = ___edge1;
		AdvancingFrontNode_t688967424 * L_16 = ___node2;
		DTSweep_FillRightBelowEdgeEvent_m2212243855(NULL /*static, unused*/, L_14, L_15, L_16, /*hidden argument*/NULL);
		goto IL_0055;
	}

IL_004d:
	{
		AdvancingFrontNode_t688967424 * L_17 = ___node2;
		NullCheck(L_17);
		AdvancingFrontNode_t688967424 * L_18 = L_17->get_Next_0();
		___node2 = L_18;
	}

IL_0055:
	{
		AdvancingFrontNode_t688967424 * L_19 = ___node2;
		NullCheck(L_19);
		AdvancingFrontNode_t688967424 * L_20 = L_19->get_Next_0();
		NullCheck(L_20);
		TriangulationPoint_t3810082933 * L_21 = L_20->get_Point_3();
		NullCheck(L_21);
		double L_22 = L_21->get_X_0();
		DTSweepConstraint_t3360279023 * L_23 = ___edge1;
		NullCheck(L_23);
		TriangulationPoint_t3810082933 * L_24 = ((TriangulationConstraint_t137695394 *)L_23)->get_P_0();
		NullCheck(L_24);
		double L_25 = L_24->get_X_0();
		if ((((double)L_22) < ((double)L_25)))
		{
			goto IL_0005;
		}
	}
	{
		return;
	}
}
// System.Void Pathfinding.Poly2Tri.DTSweep::FillLeftConvexEdgeEvent(Pathfinding.Poly2Tri.DTSweepContext,Pathfinding.Poly2Tri.DTSweepConstraint,Pathfinding.Poly2Tri.AdvancingFrontNode)
extern Il2CppClass* TriangulationUtil_t1170824903_il2cpp_TypeInfo_var;
extern const uint32_t DTSweep_FillLeftConvexEdgeEvent_m2487214550_MetadataUsageId;
extern "C"  void DTSweep_FillLeftConvexEdgeEvent_m2487214550 (Il2CppObject * __this /* static, unused */, DTSweepContext_t543731303 * ___tcx0, DTSweepConstraint_t3360279023 * ___edge1, AdvancingFrontNode_t688967424 * ___node2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DTSweep_FillLeftConvexEdgeEvent_m2487214550_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		AdvancingFrontNode_t688967424 * L_0 = ___node2;
		NullCheck(L_0);
		AdvancingFrontNode_t688967424 * L_1 = L_0->get_Prev_1();
		NullCheck(L_1);
		TriangulationPoint_t3810082933 * L_2 = L_1->get_Point_3();
		AdvancingFrontNode_t688967424 * L_3 = ___node2;
		NullCheck(L_3);
		AdvancingFrontNode_t688967424 * L_4 = L_3->get_Prev_1();
		NullCheck(L_4);
		AdvancingFrontNode_t688967424 * L_5 = L_4->get_Prev_1();
		NullCheck(L_5);
		TriangulationPoint_t3810082933 * L_6 = L_5->get_Point_3();
		AdvancingFrontNode_t688967424 * L_7 = ___node2;
		NullCheck(L_7);
		AdvancingFrontNode_t688967424 * L_8 = L_7->get_Prev_1();
		NullCheck(L_8);
		AdvancingFrontNode_t688967424 * L_9 = L_8->get_Prev_1();
		NullCheck(L_9);
		AdvancingFrontNode_t688967424 * L_10 = L_9->get_Prev_1();
		NullCheck(L_10);
		TriangulationPoint_t3810082933 * L_11 = L_10->get_Point_3();
		IL2CPP_RUNTIME_CLASS_INIT(TriangulationUtil_t1170824903_il2cpp_TypeInfo_var);
		int32_t L_12 = TriangulationUtil_Orient2d_m3842222219(NULL /*static, unused*/, L_2, L_6, L_11, /*hidden argument*/NULL);
		if (L_12)
		{
			goto IL_004c;
		}
	}
	{
		DTSweepContext_t543731303 * L_13 = ___tcx0;
		DTSweepConstraint_t3360279023 * L_14 = ___edge1;
		AdvancingFrontNode_t688967424 * L_15 = ___node2;
		NullCheck(L_15);
		AdvancingFrontNode_t688967424 * L_16 = L_15->get_Prev_1();
		DTSweep_FillLeftConcaveEdgeEvent_m2243446922(NULL /*static, unused*/, L_13, L_14, L_16, /*hidden argument*/NULL);
		goto IL_0084;
	}

IL_004c:
	{
		DTSweepConstraint_t3360279023 * L_17 = ___edge1;
		NullCheck(L_17);
		TriangulationPoint_t3810082933 * L_18 = ((TriangulationConstraint_t137695394 *)L_17)->get_Q_1();
		AdvancingFrontNode_t688967424 * L_19 = ___node2;
		NullCheck(L_19);
		AdvancingFrontNode_t688967424 * L_20 = L_19->get_Prev_1();
		NullCheck(L_20);
		AdvancingFrontNode_t688967424 * L_21 = L_20->get_Prev_1();
		NullCheck(L_21);
		TriangulationPoint_t3810082933 * L_22 = L_21->get_Point_3();
		DTSweepConstraint_t3360279023 * L_23 = ___edge1;
		NullCheck(L_23);
		TriangulationPoint_t3810082933 * L_24 = ((TriangulationConstraint_t137695394 *)L_23)->get_P_0();
		IL2CPP_RUNTIME_CLASS_INIT(TriangulationUtil_t1170824903_il2cpp_TypeInfo_var);
		int32_t L_25 = TriangulationUtil_Orient2d_m3842222219(NULL /*static, unused*/, L_18, L_22, L_24, /*hidden argument*/NULL);
		if (L_25)
		{
			goto IL_0084;
		}
	}
	{
		DTSweepContext_t543731303 * L_26 = ___tcx0;
		DTSweepConstraint_t3360279023 * L_27 = ___edge1;
		AdvancingFrontNode_t688967424 * L_28 = ___node2;
		NullCheck(L_28);
		AdvancingFrontNode_t688967424 * L_29 = L_28->get_Prev_1();
		DTSweep_FillLeftConvexEdgeEvent_m2487214550(NULL /*static, unused*/, L_26, L_27, L_29, /*hidden argument*/NULL);
		goto IL_0084;
	}

IL_0084:
	{
		return;
	}
}
// System.Void Pathfinding.Poly2Tri.DTSweep::FillLeftConcaveEdgeEvent(Pathfinding.Poly2Tri.DTSweepContext,Pathfinding.Poly2Tri.DTSweepConstraint,Pathfinding.Poly2Tri.AdvancingFrontNode)
extern Il2CppClass* TriangulationUtil_t1170824903_il2cpp_TypeInfo_var;
extern const uint32_t DTSweep_FillLeftConcaveEdgeEvent_m2243446922_MetadataUsageId;
extern "C"  void DTSweep_FillLeftConcaveEdgeEvent_m2243446922 (Il2CppObject * __this /* static, unused */, DTSweepContext_t543731303 * ___tcx0, DTSweepConstraint_t3360279023 * ___edge1, AdvancingFrontNode_t688967424 * ___node2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DTSweep_FillLeftConcaveEdgeEvent_m2243446922_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		DTSweepContext_t543731303 * L_0 = ___tcx0;
		AdvancingFrontNode_t688967424 * L_1 = ___node2;
		NullCheck(L_1);
		AdvancingFrontNode_t688967424 * L_2 = L_1->get_Prev_1();
		DTSweep_Fill_m2383500537(NULL /*static, unused*/, L_0, L_2, /*hidden argument*/NULL);
		AdvancingFrontNode_t688967424 * L_3 = ___node2;
		NullCheck(L_3);
		AdvancingFrontNode_t688967424 * L_4 = L_3->get_Prev_1();
		NullCheck(L_4);
		TriangulationPoint_t3810082933 * L_5 = L_4->get_Point_3();
		DTSweepConstraint_t3360279023 * L_6 = ___edge1;
		NullCheck(L_6);
		TriangulationPoint_t3810082933 * L_7 = ((TriangulationConstraint_t137695394 *)L_6)->get_P_0();
		if ((((Il2CppObject*)(TriangulationPoint_t3810082933 *)L_5) == ((Il2CppObject*)(TriangulationPoint_t3810082933 *)L_7)))
		{
			goto IL_007b;
		}
	}
	{
		DTSweepConstraint_t3360279023 * L_8 = ___edge1;
		NullCheck(L_8);
		TriangulationPoint_t3810082933 * L_9 = ((TriangulationConstraint_t137695394 *)L_8)->get_Q_1();
		AdvancingFrontNode_t688967424 * L_10 = ___node2;
		NullCheck(L_10);
		AdvancingFrontNode_t688967424 * L_11 = L_10->get_Prev_1();
		NullCheck(L_11);
		TriangulationPoint_t3810082933 * L_12 = L_11->get_Point_3();
		DTSweepConstraint_t3360279023 * L_13 = ___edge1;
		NullCheck(L_13);
		TriangulationPoint_t3810082933 * L_14 = ((TriangulationConstraint_t137695394 *)L_13)->get_P_0();
		IL2CPP_RUNTIME_CLASS_INIT(TriangulationUtil_t1170824903_il2cpp_TypeInfo_var);
		int32_t L_15 = TriangulationUtil_Orient2d_m3842222219(NULL /*static, unused*/, L_9, L_12, L_14, /*hidden argument*/NULL);
		if (L_15)
		{
			goto IL_007b;
		}
	}
	{
		AdvancingFrontNode_t688967424 * L_16 = ___node2;
		NullCheck(L_16);
		TriangulationPoint_t3810082933 * L_17 = L_16->get_Point_3();
		AdvancingFrontNode_t688967424 * L_18 = ___node2;
		NullCheck(L_18);
		AdvancingFrontNode_t688967424 * L_19 = L_18->get_Prev_1();
		NullCheck(L_19);
		TriangulationPoint_t3810082933 * L_20 = L_19->get_Point_3();
		AdvancingFrontNode_t688967424 * L_21 = ___node2;
		NullCheck(L_21);
		AdvancingFrontNode_t688967424 * L_22 = L_21->get_Prev_1();
		NullCheck(L_22);
		AdvancingFrontNode_t688967424 * L_23 = L_22->get_Prev_1();
		NullCheck(L_23);
		TriangulationPoint_t3810082933 * L_24 = L_23->get_Point_3();
		IL2CPP_RUNTIME_CLASS_INIT(TriangulationUtil_t1170824903_il2cpp_TypeInfo_var);
		int32_t L_25 = TriangulationUtil_Orient2d_m3842222219(NULL /*static, unused*/, L_17, L_20, L_24, /*hidden argument*/NULL);
		if (L_25)
		{
			goto IL_007b;
		}
	}
	{
		DTSweepContext_t543731303 * L_26 = ___tcx0;
		DTSweepConstraint_t3360279023 * L_27 = ___edge1;
		AdvancingFrontNode_t688967424 * L_28 = ___node2;
		DTSweep_FillLeftConcaveEdgeEvent_m2243446922(NULL /*static, unused*/, L_26, L_27, L_28, /*hidden argument*/NULL);
		goto IL_007b;
	}

IL_007b:
	{
		return;
	}
}
// System.Void Pathfinding.Poly2Tri.DTSweep::FillLeftBelowEdgeEvent(Pathfinding.Poly2Tri.DTSweepContext,Pathfinding.Poly2Tri.DTSweepConstraint,Pathfinding.Poly2Tri.AdvancingFrontNode)
extern Il2CppClass* TriangulationUtil_t1170824903_il2cpp_TypeInfo_var;
extern const uint32_t DTSweep_FillLeftBelowEdgeEvent_m3809204008_MetadataUsageId;
extern "C"  void DTSweep_FillLeftBelowEdgeEvent_m3809204008 (Il2CppObject * __this /* static, unused */, DTSweepContext_t543731303 * ___tcx0, DTSweepConstraint_t3360279023 * ___edge1, AdvancingFrontNode_t688967424 * ___node2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DTSweep_FillLeftBelowEdgeEvent_m3809204008_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		DTSweepContext_t543731303 * L_0 = ___tcx0;
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(8 /* System.Boolean Pathfinding.Poly2Tri.TriangulationContext::get_IsDebugEnabled() */, L_0);
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		DTSweepContext_t543731303 * L_2 = ___tcx0;
		NullCheck(L_2);
		DTSweepDebugContext_t3829537966 * L_3 = TriangulationContext_get_DTDebugContext_m2409356104(L_2, /*hidden argument*/NULL);
		AdvancingFrontNode_t688967424 * L_4 = ___node2;
		NullCheck(L_3);
		DTSweepDebugContext_set_ActiveNode_m1532614081(L_3, L_4, /*hidden argument*/NULL);
	}

IL_0017:
	{
		AdvancingFrontNode_t688967424 * L_5 = ___node2;
		NullCheck(L_5);
		TriangulationPoint_t3810082933 * L_6 = L_5->get_Point_3();
		NullCheck(L_6);
		double L_7 = L_6->get_X_0();
		DTSweepConstraint_t3360279023 * L_8 = ___edge1;
		NullCheck(L_8);
		TriangulationPoint_t3810082933 * L_9 = ((TriangulationConstraint_t137695394 *)L_8)->get_P_0();
		NullCheck(L_9);
		double L_10 = L_9->get_X_0();
		if ((!(((double)L_7) > ((double)L_10))))
		{
			goto IL_007a;
		}
	}
	{
		AdvancingFrontNode_t688967424 * L_11 = ___node2;
		NullCheck(L_11);
		TriangulationPoint_t3810082933 * L_12 = L_11->get_Point_3();
		AdvancingFrontNode_t688967424 * L_13 = ___node2;
		NullCheck(L_13);
		AdvancingFrontNode_t688967424 * L_14 = L_13->get_Prev_1();
		NullCheck(L_14);
		TriangulationPoint_t3810082933 * L_15 = L_14->get_Point_3();
		AdvancingFrontNode_t688967424 * L_16 = ___node2;
		NullCheck(L_16);
		AdvancingFrontNode_t688967424 * L_17 = L_16->get_Prev_1();
		NullCheck(L_17);
		AdvancingFrontNode_t688967424 * L_18 = L_17->get_Prev_1();
		NullCheck(L_18);
		TriangulationPoint_t3810082933 * L_19 = L_18->get_Point_3();
		IL2CPP_RUNTIME_CLASS_INIT(TriangulationUtil_t1170824903_il2cpp_TypeInfo_var);
		int32_t L_20 = TriangulationUtil_Orient2d_m3842222219(NULL /*static, unused*/, L_12, L_15, L_19, /*hidden argument*/NULL);
		if (L_20)
		{
			goto IL_006a;
		}
	}
	{
		DTSweepContext_t543731303 * L_21 = ___tcx0;
		DTSweepConstraint_t3360279023 * L_22 = ___edge1;
		AdvancingFrontNode_t688967424 * L_23 = ___node2;
		DTSweep_FillLeftConcaveEdgeEvent_m2243446922(NULL /*static, unused*/, L_21, L_22, L_23, /*hidden argument*/NULL);
		goto IL_007a;
	}

IL_006a:
	{
		DTSweepContext_t543731303 * L_24 = ___tcx0;
		DTSweepConstraint_t3360279023 * L_25 = ___edge1;
		AdvancingFrontNode_t688967424 * L_26 = ___node2;
		DTSweep_FillLeftConvexEdgeEvent_m2487214550(NULL /*static, unused*/, L_24, L_25, L_26, /*hidden argument*/NULL);
		DTSweepContext_t543731303 * L_27 = ___tcx0;
		DTSweepConstraint_t3360279023 * L_28 = ___edge1;
		AdvancingFrontNode_t688967424 * L_29 = ___node2;
		DTSweep_FillLeftBelowEdgeEvent_m3809204008(NULL /*static, unused*/, L_27, L_28, L_29, /*hidden argument*/NULL);
	}

IL_007a:
	{
		return;
	}
}
// System.Void Pathfinding.Poly2Tri.DTSweep::FillLeftAboveEdgeEvent(Pathfinding.Poly2Tri.DTSweepContext,Pathfinding.Poly2Tri.DTSweepConstraint,Pathfinding.Poly2Tri.AdvancingFrontNode)
extern Il2CppClass* TriangulationUtil_t1170824903_il2cpp_TypeInfo_var;
extern const uint32_t DTSweep_FillLeftAboveEdgeEvent_m2067732284_MetadataUsageId;
extern "C"  void DTSweep_FillLeftAboveEdgeEvent_m2067732284 (Il2CppObject * __this /* static, unused */, DTSweepContext_t543731303 * ___tcx0, DTSweepConstraint_t3360279023 * ___edge1, AdvancingFrontNode_t688967424 * ___node2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DTSweep_FillLeftAboveEdgeEvent_m2067732284_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		goto IL_0054;
	}

IL_0005:
	{
		DTSweepContext_t543731303 * L_0 = ___tcx0;
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(8 /* System.Boolean Pathfinding.Poly2Tri.TriangulationContext::get_IsDebugEnabled() */, L_0);
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		DTSweepContext_t543731303 * L_2 = ___tcx0;
		NullCheck(L_2);
		DTSweepDebugContext_t3829537966 * L_3 = TriangulationContext_get_DTDebugContext_m2409356104(L_2, /*hidden argument*/NULL);
		AdvancingFrontNode_t688967424 * L_4 = ___node2;
		NullCheck(L_3);
		DTSweepDebugContext_set_ActiveNode_m1532614081(L_3, L_4, /*hidden argument*/NULL);
	}

IL_001c:
	{
		DTSweepConstraint_t3360279023 * L_5 = ___edge1;
		NullCheck(L_5);
		TriangulationPoint_t3810082933 * L_6 = ((TriangulationConstraint_t137695394 *)L_5)->get_Q_1();
		AdvancingFrontNode_t688967424 * L_7 = ___node2;
		NullCheck(L_7);
		AdvancingFrontNode_t688967424 * L_8 = L_7->get_Prev_1();
		NullCheck(L_8);
		TriangulationPoint_t3810082933 * L_9 = L_8->get_Point_3();
		DTSweepConstraint_t3360279023 * L_10 = ___edge1;
		NullCheck(L_10);
		TriangulationPoint_t3810082933 * L_11 = ((TriangulationConstraint_t137695394 *)L_10)->get_P_0();
		IL2CPP_RUNTIME_CLASS_INIT(TriangulationUtil_t1170824903_il2cpp_TypeInfo_var);
		int32_t L_12 = TriangulationUtil_Orient2d_m3842222219(NULL /*static, unused*/, L_6, L_9, L_11, /*hidden argument*/NULL);
		V_0 = L_12;
		int32_t L_13 = V_0;
		if (L_13)
		{
			goto IL_004c;
		}
	}
	{
		DTSweepContext_t543731303 * L_14 = ___tcx0;
		DTSweepConstraint_t3360279023 * L_15 = ___edge1;
		AdvancingFrontNode_t688967424 * L_16 = ___node2;
		DTSweep_FillLeftBelowEdgeEvent_m3809204008(NULL /*static, unused*/, L_14, L_15, L_16, /*hidden argument*/NULL);
		goto IL_0054;
	}

IL_004c:
	{
		AdvancingFrontNode_t688967424 * L_17 = ___node2;
		NullCheck(L_17);
		AdvancingFrontNode_t688967424 * L_18 = L_17->get_Prev_1();
		___node2 = L_18;
	}

IL_0054:
	{
		AdvancingFrontNode_t688967424 * L_19 = ___node2;
		NullCheck(L_19);
		AdvancingFrontNode_t688967424 * L_20 = L_19->get_Prev_1();
		NullCheck(L_20);
		TriangulationPoint_t3810082933 * L_21 = L_20->get_Point_3();
		NullCheck(L_21);
		double L_22 = L_21->get_X_0();
		DTSweepConstraint_t3360279023 * L_23 = ___edge1;
		NullCheck(L_23);
		TriangulationPoint_t3810082933 * L_24 = ((TriangulationConstraint_t137695394 *)L_23)->get_P_0();
		NullCheck(L_24);
		double L_25 = L_24->get_X_0();
		if ((((double)L_22) > ((double)L_25)))
		{
			goto IL_0005;
		}
	}
	{
		return;
	}
}
// System.Boolean Pathfinding.Poly2Tri.DTSweep::IsEdgeSideOfTriangle(Pathfinding.Poly2Tri.DelaunayTriangle,Pathfinding.Poly2Tri.TriangulationPoint,Pathfinding.Poly2Tri.TriangulationPoint)
extern const MethodInfo* FixedArray3_1_get_Item_m4227602836_MethodInfo_var;
extern const uint32_t DTSweep_IsEdgeSideOfTriangle_m3418112580_MetadataUsageId;
extern "C"  bool DTSweep_IsEdgeSideOfTriangle_m3418112580 (Il2CppObject * __this /* static, unused */, DelaunayTriangle_t2835103587 * ___triangle0, TriangulationPoint_t3810082933 * ___ep1, TriangulationPoint_t3810082933 * ___eq2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DTSweep_IsEdgeSideOfTriangle_m3418112580_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		DelaunayTriangle_t2835103587 * L_0 = ___triangle0;
		TriangulationPoint_t3810082933 * L_1 = ___ep1;
		TriangulationPoint_t3810082933 * L_2 = ___eq2;
		NullCheck(L_0);
		int32_t L_3 = DelaunayTriangle_EdgeIndex_m78242015(L_0, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		int32_t L_4 = V_0;
		if ((!(((uint32_t)L_4) == ((uint32_t)(-1)))))
		{
			goto IL_0012;
		}
	}
	{
		return (bool)0;
	}

IL_0012:
	{
		DelaunayTriangle_t2835103587 * L_5 = ___triangle0;
		int32_t L_6 = V_0;
		NullCheck(L_5);
		DelaunayTriangle_MarkConstrainedEdge_m3767794513(L_5, L_6, /*hidden argument*/NULL);
		DelaunayTriangle_t2835103587 * L_7 = ___triangle0;
		NullCheck(L_7);
		FixedArray3_1_t3485577237 * L_8 = L_7->get_address_of_Neighbors_1();
		int32_t L_9 = V_0;
		DelaunayTriangle_t2835103587 * L_10 = FixedArray3_1_get_Item_m4227602836(L_8, L_9, /*hidden argument*/FixedArray3_1_get_Item_m4227602836_MethodInfo_var);
		___triangle0 = L_10;
		DelaunayTriangle_t2835103587 * L_11 = ___triangle0;
		if (!L_11)
		{
			goto IL_0035;
		}
	}
	{
		DelaunayTriangle_t2835103587 * L_12 = ___triangle0;
		TriangulationPoint_t3810082933 * L_13 = ___ep1;
		TriangulationPoint_t3810082933 * L_14 = ___eq2;
		NullCheck(L_12);
		DelaunayTriangle_MarkConstrainedEdge_m3313274974(L_12, L_13, L_14, /*hidden argument*/NULL);
	}

IL_0035:
	{
		return (bool)1;
	}
}
// System.Void Pathfinding.Poly2Tri.DTSweep::EdgeEvent(Pathfinding.Poly2Tri.DTSweepContext,Pathfinding.Poly2Tri.TriangulationPoint,Pathfinding.Poly2Tri.TriangulationPoint,Pathfinding.Poly2Tri.DelaunayTriangle,Pathfinding.Poly2Tri.TriangulationPoint)
extern Il2CppClass* TriangulationUtil_t1170824903_il2cpp_TypeInfo_var;
extern Il2CppClass* PointOnEdgeException_t989229367_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1843927275;
extern const uint32_t DTSweep_EdgeEvent_m1498452168_MetadataUsageId;
extern "C"  void DTSweep_EdgeEvent_m1498452168 (Il2CppObject * __this /* static, unused */, DTSweepContext_t543731303 * ___tcx0, TriangulationPoint_t3810082933 * ___ep1, TriangulationPoint_t3810082933 * ___eq2, DelaunayTriangle_t2835103587 * ___triangle3, TriangulationPoint_t3810082933 * ___point4, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DTSweep_EdgeEvent_m1498452168_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	TriangulationPoint_t3810082933 * V_0 = NULL;
	TriangulationPoint_t3810082933 * V_1 = NULL;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	{
		DTSweepContext_t543731303 * L_0 = ___tcx0;
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(8 /* System.Boolean Pathfinding.Poly2Tri.TriangulationContext::get_IsDebugEnabled() */, L_0);
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		DTSweepContext_t543731303 * L_2 = ___tcx0;
		NullCheck(L_2);
		DTSweepDebugContext_t3829537966 * L_3 = TriangulationContext_get_DTDebugContext_m2409356104(L_2, /*hidden argument*/NULL);
		DelaunayTriangle_t2835103587 * L_4 = ___triangle3;
		NullCheck(L_3);
		DTSweepDebugContext_set_PrimaryTriangle_m527373270(L_3, L_4, /*hidden argument*/NULL);
	}

IL_0017:
	{
		DelaunayTriangle_t2835103587 * L_5 = ___triangle3;
		TriangulationPoint_t3810082933 * L_6 = ___ep1;
		TriangulationPoint_t3810082933 * L_7 = ___eq2;
		bool L_8 = DTSweep_IsEdgeSideOfTriangle_m3418112580(NULL /*static, unused*/, L_5, L_6, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0025;
		}
	}
	{
		return;
	}

IL_0025:
	{
		DelaunayTriangle_t2835103587 * L_9 = ___triangle3;
		TriangulationPoint_t3810082933 * L_10 = ___point4;
		NullCheck(L_9);
		TriangulationPoint_t3810082933 * L_11 = DelaunayTriangle_PointCCWFrom_m667919724(L_9, L_10, /*hidden argument*/NULL);
		V_0 = L_11;
		TriangulationPoint_t3810082933 * L_12 = ___eq2;
		TriangulationPoint_t3810082933 * L_13 = V_0;
		TriangulationPoint_t3810082933 * L_14 = ___ep1;
		IL2CPP_RUNTIME_CLASS_INIT(TriangulationUtil_t1170824903_il2cpp_TypeInfo_var);
		int32_t L_15 = TriangulationUtil_Orient2d_m3842222219(NULL /*static, unused*/, L_12, L_13, L_14, /*hidden argument*/NULL);
		V_2 = L_15;
		int32_t L_16 = V_2;
		if ((!(((uint32_t)L_16) == ((uint32_t)2))))
		{
			goto IL_004c;
		}
	}
	{
		TriangulationPoint_t3810082933 * L_17 = ___eq2;
		TriangulationPoint_t3810082933 * L_18 = V_0;
		TriangulationPoint_t3810082933 * L_19 = ___ep1;
		PointOnEdgeException_t989229367 * L_20 = (PointOnEdgeException_t989229367 *)il2cpp_codegen_object_new(PointOnEdgeException_t989229367_il2cpp_TypeInfo_var);
		PointOnEdgeException__ctor_m41689854(L_20, _stringLiteral1843927275, L_17, L_18, L_19, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_20);
	}

IL_004c:
	{
		DelaunayTriangle_t2835103587 * L_21 = ___triangle3;
		TriangulationPoint_t3810082933 * L_22 = ___point4;
		NullCheck(L_21);
		TriangulationPoint_t3810082933 * L_23 = DelaunayTriangle_PointCWFrom_m3064891095(L_21, L_22, /*hidden argument*/NULL);
		V_1 = L_23;
		TriangulationPoint_t3810082933 * L_24 = ___eq2;
		TriangulationPoint_t3810082933 * L_25 = V_1;
		TriangulationPoint_t3810082933 * L_26 = ___ep1;
		IL2CPP_RUNTIME_CLASS_INIT(TriangulationUtil_t1170824903_il2cpp_TypeInfo_var);
		int32_t L_27 = TriangulationUtil_Orient2d_m3842222219(NULL /*static, unused*/, L_24, L_25, L_26, /*hidden argument*/NULL);
		V_3 = L_27;
		int32_t L_28 = V_3;
		if ((!(((uint32_t)L_28) == ((uint32_t)2))))
		{
			goto IL_0073;
		}
	}
	{
		TriangulationPoint_t3810082933 * L_29 = ___eq2;
		TriangulationPoint_t3810082933 * L_30 = V_1;
		TriangulationPoint_t3810082933 * L_31 = ___ep1;
		PointOnEdgeException_t989229367 * L_32 = (PointOnEdgeException_t989229367 *)il2cpp_codegen_object_new(PointOnEdgeException_t989229367_il2cpp_TypeInfo_var);
		PointOnEdgeException__ctor_m41689854(L_32, _stringLiteral1843927275, L_29, L_30, L_31, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_32);
	}

IL_0073:
	{
		int32_t L_33 = V_2;
		int32_t L_34 = V_3;
		if ((!(((uint32_t)L_33) == ((uint32_t)L_34))))
		{
			goto IL_00a9;
		}
	}
	{
		int32_t L_35 = V_2;
		if (L_35)
		{
			goto IL_008f;
		}
	}
	{
		DelaunayTriangle_t2835103587 * L_36 = ___triangle3;
		TriangulationPoint_t3810082933 * L_37 = ___point4;
		NullCheck(L_36);
		DelaunayTriangle_t2835103587 * L_38 = DelaunayTriangle_NeighborCCWFrom_m3747172644(L_36, L_37, /*hidden argument*/NULL);
		___triangle3 = L_38;
		goto IL_0099;
	}

IL_008f:
	{
		DelaunayTriangle_t2835103587 * L_39 = ___triangle3;
		TriangulationPoint_t3810082933 * L_40 = ___point4;
		NullCheck(L_39);
		DelaunayTriangle_t2835103587 * L_41 = DelaunayTriangle_NeighborCWFrom_m3856958495(L_39, L_40, /*hidden argument*/NULL);
		___triangle3 = L_41;
	}

IL_0099:
	{
		DTSweepContext_t543731303 * L_42 = ___tcx0;
		TriangulationPoint_t3810082933 * L_43 = ___ep1;
		TriangulationPoint_t3810082933 * L_44 = ___eq2;
		DelaunayTriangle_t2835103587 * L_45 = ___triangle3;
		TriangulationPoint_t3810082933 * L_46 = ___point4;
		DTSweep_EdgeEvent_m1498452168(NULL /*static, unused*/, L_42, L_43, L_44, L_45, L_46, /*hidden argument*/NULL);
		goto IL_00b4;
	}

IL_00a9:
	{
		DTSweepContext_t543731303 * L_47 = ___tcx0;
		TriangulationPoint_t3810082933 * L_48 = ___ep1;
		TriangulationPoint_t3810082933 * L_49 = ___eq2;
		DelaunayTriangle_t2835103587 * L_50 = ___triangle3;
		TriangulationPoint_t3810082933 * L_51 = ___point4;
		DTSweep_FlipEdgeEvent_m1349562325(NULL /*static, unused*/, L_47, L_48, L_49, L_50, L_51, /*hidden argument*/NULL);
	}

IL_00b4:
	{
		return;
	}
}
// System.Void Pathfinding.Poly2Tri.DTSweep::FlipEdgeEvent(Pathfinding.Poly2Tri.DTSweepContext,Pathfinding.Poly2Tri.TriangulationPoint,Pathfinding.Poly2Tri.TriangulationPoint,Pathfinding.Poly2Tri.DelaunayTriangle,Pathfinding.Poly2Tri.TriangulationPoint)
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppClass* TriangulationUtil_t1170824903_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral505650460;
extern const uint32_t DTSweep_FlipEdgeEvent_m1349562325_MetadataUsageId;
extern "C"  void DTSweep_FlipEdgeEvent_m1349562325 (Il2CppObject * __this /* static, unused */, DTSweepContext_t543731303 * ___tcx0, TriangulationPoint_t3810082933 * ___ep1, TriangulationPoint_t3810082933 * ___eq2, DelaunayTriangle_t2835103587 * ___t3, TriangulationPoint_t3810082933 * ___p4, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DTSweep_FlipEdgeEvent_m1349562325_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DelaunayTriangle_t2835103587 * V_0 = NULL;
	TriangulationPoint_t3810082933 * V_1 = NULL;
	bool V_2 = false;
	int32_t V_3 = 0;
	TriangulationPoint_t3810082933 * V_4 = NULL;
	{
		DelaunayTriangle_t2835103587 * L_0 = ___t3;
		TriangulationPoint_t3810082933 * L_1 = ___p4;
		NullCheck(L_0);
		DelaunayTriangle_t2835103587 * L_2 = DelaunayTriangle_NeighborAcrossFrom_m3663882196(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		DelaunayTriangle_t2835103587 * L_3 = V_0;
		DelaunayTriangle_t2835103587 * L_4 = ___t3;
		TriangulationPoint_t3810082933 * L_5 = ___p4;
		NullCheck(L_3);
		TriangulationPoint_t3810082933 * L_6 = DelaunayTriangle_OppositePoint_m3734492474(L_3, L_4, L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		DelaunayTriangle_t2835103587 * L_7 = V_0;
		if (L_7)
		{
			goto IL_0024;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_8 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_8, _stringLiteral505650460, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}

IL_0024:
	{
		DTSweepContext_t543731303 * L_9 = ___tcx0;
		NullCheck(L_9);
		bool L_10 = VirtFuncInvoker0< bool >::Invoke(8 /* System.Boolean Pathfinding.Poly2Tri.TriangulationContext::get_IsDebugEnabled() */, L_9);
		if (!L_10)
		{
			goto IL_0047;
		}
	}
	{
		DTSweepContext_t543731303 * L_11 = ___tcx0;
		NullCheck(L_11);
		DTSweepDebugContext_t3829537966 * L_12 = TriangulationContext_get_DTDebugContext_m2409356104(L_11, /*hidden argument*/NULL);
		DelaunayTriangle_t2835103587 * L_13 = ___t3;
		NullCheck(L_12);
		DTSweepDebugContext_set_PrimaryTriangle_m527373270(L_12, L_13, /*hidden argument*/NULL);
		DTSweepContext_t543731303 * L_14 = ___tcx0;
		NullCheck(L_14);
		DTSweepDebugContext_t3829537966 * L_15 = TriangulationContext_get_DTDebugContext_m2409356104(L_14, /*hidden argument*/NULL);
		DelaunayTriangle_t2835103587 * L_16 = V_0;
		NullCheck(L_15);
		DTSweepDebugContext_set_SecondaryTriangle_m1903864548(L_15, L_16, /*hidden argument*/NULL);
	}

IL_0047:
	{
		TriangulationPoint_t3810082933 * L_17 = ___p4;
		DelaunayTriangle_t2835103587 * L_18 = ___t3;
		TriangulationPoint_t3810082933 * L_19 = ___p4;
		NullCheck(L_18);
		TriangulationPoint_t3810082933 * L_20 = DelaunayTriangle_PointCCWFrom_m667919724(L_18, L_19, /*hidden argument*/NULL);
		DelaunayTriangle_t2835103587 * L_21 = ___t3;
		TriangulationPoint_t3810082933 * L_22 = ___p4;
		NullCheck(L_21);
		TriangulationPoint_t3810082933 * L_23 = DelaunayTriangle_PointCWFrom_m3064891095(L_21, L_22, /*hidden argument*/NULL);
		TriangulationPoint_t3810082933 * L_24 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(TriangulationUtil_t1170824903_il2cpp_TypeInfo_var);
		bool L_25 = TriangulationUtil_InScanArea_m680109047(NULL /*static, unused*/, L_17, L_20, L_23, L_24, /*hidden argument*/NULL);
		V_2 = L_25;
		bool L_26 = V_2;
		if (!L_26)
		{
			goto IL_010a;
		}
	}
	{
		DelaunayTriangle_t2835103587 * L_27 = ___t3;
		TriangulationPoint_t3810082933 * L_28 = ___p4;
		DelaunayTriangle_t2835103587 * L_29 = V_0;
		TriangulationPoint_t3810082933 * L_30 = V_1;
		DTSweep_RotateTrianglePair_m2948867544(NULL /*static, unused*/, L_27, L_28, L_29, L_30, /*hidden argument*/NULL);
		DTSweepContext_t543731303 * L_31 = ___tcx0;
		DelaunayTriangle_t2835103587 * L_32 = ___t3;
		NullCheck(L_31);
		DTSweepContext_MapTriangleToNodes_m727745286(L_31, L_32, /*hidden argument*/NULL);
		DTSweepContext_t543731303 * L_33 = ___tcx0;
		DelaunayTriangle_t2835103587 * L_34 = V_0;
		NullCheck(L_33);
		DTSweepContext_MapTriangleToNodes_m727745286(L_33, L_34, /*hidden argument*/NULL);
		TriangulationPoint_t3810082933 * L_35 = ___p4;
		TriangulationPoint_t3810082933 * L_36 = ___eq2;
		if ((!(((Il2CppObject*)(TriangulationPoint_t3810082933 *)L_35) == ((Il2CppObject*)(TriangulationPoint_t3810082933 *)L_36))))
		{
			goto IL_00e3;
		}
	}
	{
		TriangulationPoint_t3810082933 * L_37 = V_1;
		TriangulationPoint_t3810082933 * L_38 = ___ep1;
		if ((!(((Il2CppObject*)(TriangulationPoint_t3810082933 *)L_37) == ((Il2CppObject*)(TriangulationPoint_t3810082933 *)L_38))))
		{
			goto IL_00e3;
		}
	}
	{
		TriangulationPoint_t3810082933 * L_39 = ___eq2;
		DTSweepContext_t543731303 * L_40 = ___tcx0;
		NullCheck(L_40);
		DTSweepEdgeEvent_t3441061589 * L_41 = L_40->get_EdgeEvent_10();
		NullCheck(L_41);
		DTSweepConstraint_t3360279023 * L_42 = L_41->get_ConstrainedEdge_0();
		NullCheck(L_42);
		TriangulationPoint_t3810082933 * L_43 = ((TriangulationConstraint_t137695394 *)L_42)->get_Q_1();
		if ((!(((Il2CppObject*)(TriangulationPoint_t3810082933 *)L_39) == ((Il2CppObject*)(TriangulationPoint_t3810082933 *)L_43))))
		{
			goto IL_00de;
		}
	}
	{
		TriangulationPoint_t3810082933 * L_44 = ___ep1;
		DTSweepContext_t543731303 * L_45 = ___tcx0;
		NullCheck(L_45);
		DTSweepEdgeEvent_t3441061589 * L_46 = L_45->get_EdgeEvent_10();
		NullCheck(L_46);
		DTSweepConstraint_t3360279023 * L_47 = L_46->get_ConstrainedEdge_0();
		NullCheck(L_47);
		TriangulationPoint_t3810082933 * L_48 = ((TriangulationConstraint_t137695394 *)L_47)->get_P_0();
		if ((!(((Il2CppObject*)(TriangulationPoint_t3810082933 *)L_44) == ((Il2CppObject*)(TriangulationPoint_t3810082933 *)L_48))))
		{
			goto IL_00de;
		}
	}
	{
		DelaunayTriangle_t2835103587 * L_49 = ___t3;
		TriangulationPoint_t3810082933 * L_50 = ___ep1;
		TriangulationPoint_t3810082933 * L_51 = ___eq2;
		NullCheck(L_49);
		DelaunayTriangle_MarkConstrainedEdge_m3313274974(L_49, L_50, L_51, /*hidden argument*/NULL);
		DelaunayTriangle_t2835103587 * L_52 = V_0;
		TriangulationPoint_t3810082933 * L_53 = ___ep1;
		TriangulationPoint_t3810082933 * L_54 = ___eq2;
		NullCheck(L_52);
		DelaunayTriangle_MarkConstrainedEdge_m3313274974(L_52, L_53, L_54, /*hidden argument*/NULL);
		DTSweepContext_t543731303 * L_55 = ___tcx0;
		DelaunayTriangle_t2835103587 * L_56 = ___t3;
		DTSweep_Legalize_m2173221096(NULL /*static, unused*/, L_55, L_56, /*hidden argument*/NULL);
		DTSweepContext_t543731303 * L_57 = ___tcx0;
		DelaunayTriangle_t2835103587 * L_58 = V_0;
		DTSweep_Legalize_m2173221096(NULL /*static, unused*/, L_57, L_58, /*hidden argument*/NULL);
		goto IL_00de;
	}

IL_00de:
	{
		goto IL_0105;
	}

IL_00e3:
	{
		TriangulationPoint_t3810082933 * L_59 = ___eq2;
		TriangulationPoint_t3810082933 * L_60 = V_1;
		TriangulationPoint_t3810082933 * L_61 = ___ep1;
		IL2CPP_RUNTIME_CLASS_INIT(TriangulationUtil_t1170824903_il2cpp_TypeInfo_var);
		int32_t L_62 = TriangulationUtil_Orient2d_m3842222219(NULL /*static, unused*/, L_59, L_60, L_61, /*hidden argument*/NULL);
		V_3 = L_62;
		DTSweepContext_t543731303 * L_63 = ___tcx0;
		int32_t L_64 = V_3;
		DelaunayTriangle_t2835103587 * L_65 = ___t3;
		DelaunayTriangle_t2835103587 * L_66 = V_0;
		TriangulationPoint_t3810082933 * L_67 = ___p4;
		TriangulationPoint_t3810082933 * L_68 = V_1;
		DelaunayTriangle_t2835103587 * L_69 = DTSweep_NextFlipTriangle_m3763013819(NULL /*static, unused*/, L_63, L_64, L_65, L_66, L_67, L_68, /*hidden argument*/NULL);
		___t3 = L_69;
		DTSweepContext_t543731303 * L_70 = ___tcx0;
		TriangulationPoint_t3810082933 * L_71 = ___ep1;
		TriangulationPoint_t3810082933 * L_72 = ___eq2;
		DelaunayTriangle_t2835103587 * L_73 = ___t3;
		TriangulationPoint_t3810082933 * L_74 = ___p4;
		DTSweep_FlipEdgeEvent_m1349562325(NULL /*static, unused*/, L_70, L_71, L_72, L_73, L_74, /*hidden argument*/NULL);
	}

IL_0105:
	{
		goto IL_012c;
	}

IL_010a:
	{
		TriangulationPoint_t3810082933 * L_75 = ___ep1;
		TriangulationPoint_t3810082933 * L_76 = ___eq2;
		DelaunayTriangle_t2835103587 * L_77 = V_0;
		TriangulationPoint_t3810082933 * L_78 = V_1;
		TriangulationPoint_t3810082933 * L_79 = DTSweep_NextFlipPoint_m2493387054(NULL /*static, unused*/, L_75, L_76, L_77, L_78, /*hidden argument*/NULL);
		V_4 = L_79;
		DTSweepContext_t543731303 * L_80 = ___tcx0;
		TriangulationPoint_t3810082933 * L_81 = ___ep1;
		TriangulationPoint_t3810082933 * L_82 = ___eq2;
		DelaunayTriangle_t2835103587 * L_83 = ___t3;
		DelaunayTriangle_t2835103587 * L_84 = V_0;
		TriangulationPoint_t3810082933 * L_85 = V_4;
		DTSweep_FlipScanEdgeEvent_m1758040592(NULL /*static, unused*/, L_80, L_81, L_82, L_83, L_84, L_85, /*hidden argument*/NULL);
		DTSweepContext_t543731303 * L_86 = ___tcx0;
		TriangulationPoint_t3810082933 * L_87 = ___ep1;
		TriangulationPoint_t3810082933 * L_88 = ___eq2;
		DelaunayTriangle_t2835103587 * L_89 = ___t3;
		TriangulationPoint_t3810082933 * L_90 = ___p4;
		DTSweep_EdgeEvent_m1498452168(NULL /*static, unused*/, L_86, L_87, L_88, L_89, L_90, /*hidden argument*/NULL);
	}

IL_012c:
	{
		return;
	}
}
// Pathfinding.Poly2Tri.TriangulationPoint Pathfinding.Poly2Tri.DTSweep::NextFlipPoint(Pathfinding.Poly2Tri.TriangulationPoint,Pathfinding.Poly2Tri.TriangulationPoint,Pathfinding.Poly2Tri.DelaunayTriangle,Pathfinding.Poly2Tri.TriangulationPoint)
extern Il2CppClass* TriangulationUtil_t1170824903_il2cpp_TypeInfo_var;
extern Il2CppClass* PointOnEdgeException_t989229367_il2cpp_TypeInfo_var;
extern Il2CppClass* NotImplementedException_t1912495542_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4181067905;
extern Il2CppCodeGenString* _stringLiteral158489727;
extern const uint32_t DTSweep_NextFlipPoint_m2493387054_MetadataUsageId;
extern "C"  TriangulationPoint_t3810082933 * DTSweep_NextFlipPoint_m2493387054 (Il2CppObject * __this /* static, unused */, TriangulationPoint_t3810082933 * ___ep0, TriangulationPoint_t3810082933 * ___eq1, DelaunayTriangle_t2835103587 * ___ot2, TriangulationPoint_t3810082933 * ___op3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DTSweep_NextFlipPoint_m2493387054_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		TriangulationPoint_t3810082933 * L_0 = ___eq1;
		TriangulationPoint_t3810082933 * L_1 = ___op3;
		TriangulationPoint_t3810082933 * L_2 = ___ep0;
		IL2CPP_RUNTIME_CLASS_INIT(TriangulationUtil_t1170824903_il2cpp_TypeInfo_var);
		int32_t L_3 = TriangulationUtil_Orient2d_m3842222219(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		int32_t L_4 = V_0;
		V_1 = L_4;
		int32_t L_5 = V_1;
		if (L_5 == 0)
		{
			goto IL_0022;
		}
		if (L_5 == 1)
		{
			goto IL_002a;
		}
		if (L_5 == 2)
		{
			goto IL_0032;
		}
	}
	{
		goto IL_0040;
	}

IL_0022:
	{
		DelaunayTriangle_t2835103587 * L_6 = ___ot2;
		TriangulationPoint_t3810082933 * L_7 = ___op3;
		NullCheck(L_6);
		TriangulationPoint_t3810082933 * L_8 = DelaunayTriangle_PointCCWFrom_m667919724(L_6, L_7, /*hidden argument*/NULL);
		return L_8;
	}

IL_002a:
	{
		DelaunayTriangle_t2835103587 * L_9 = ___ot2;
		TriangulationPoint_t3810082933 * L_10 = ___op3;
		NullCheck(L_9);
		TriangulationPoint_t3810082933 * L_11 = DelaunayTriangle_PointCWFrom_m3064891095(L_9, L_10, /*hidden argument*/NULL);
		return L_11;
	}

IL_0032:
	{
		TriangulationPoint_t3810082933 * L_12 = ___eq1;
		TriangulationPoint_t3810082933 * L_13 = ___op3;
		TriangulationPoint_t3810082933 * L_14 = ___ep0;
		PointOnEdgeException_t989229367 * L_15 = (PointOnEdgeException_t989229367 *)il2cpp_codegen_object_new(PointOnEdgeException_t989229367_il2cpp_TypeInfo_var);
		PointOnEdgeException__ctor_m41689854(L_15, _stringLiteral4181067905, L_12, L_13, L_14, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_15);
	}

IL_0040:
	{
		NotImplementedException_t1912495542 * L_16 = (NotImplementedException_t1912495542 *)il2cpp_codegen_object_new(NotImplementedException_t1912495542_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m495190705(L_16, _stringLiteral158489727, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_16);
	}
}
// Pathfinding.Poly2Tri.DelaunayTriangle Pathfinding.Poly2Tri.DTSweep::NextFlipTriangle(Pathfinding.Poly2Tri.DTSweepContext,Pathfinding.Poly2Tri.Orientation,Pathfinding.Poly2Tri.DelaunayTriangle,Pathfinding.Poly2Tri.DelaunayTriangle,Pathfinding.Poly2Tri.TriangulationPoint,Pathfinding.Poly2Tri.TriangulationPoint)
extern "C"  DelaunayTriangle_t2835103587 * DTSweep_NextFlipTriangle_m3763013819 (Il2CppObject * __this /* static, unused */, DTSweepContext_t543731303 * ___tcx0, int32_t ___o1, DelaunayTriangle_t2835103587 * ___t2, DelaunayTriangle_t2835103587 * ___ot3, TriangulationPoint_t3810082933 * ___p4, TriangulationPoint_t3810082933 * ___op5, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___o1;
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_0034;
		}
	}
	{
		DelaunayTriangle_t2835103587 * L_1 = ___ot3;
		TriangulationPoint_t3810082933 * L_2 = ___p4;
		TriangulationPoint_t3810082933 * L_3 = ___op5;
		NullCheck(L_1);
		int32_t L_4 = DelaunayTriangle_EdgeIndex_m78242015(L_1, L_2, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		DelaunayTriangle_t2835103587 * L_5 = ___ot3;
		NullCheck(L_5);
		FixedBitArray3_t3665369095 * L_6 = L_5->get_address_of_EdgeIsDelaunay_3();
		int32_t L_7 = V_0;
		FixedBitArray3_set_Item_m2925852176(L_6, L_7, (bool)1, /*hidden argument*/NULL);
		DTSweepContext_t543731303 * L_8 = ___tcx0;
		DelaunayTriangle_t2835103587 * L_9 = ___ot3;
		DTSweep_Legalize_m2173221096(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
		DelaunayTriangle_t2835103587 * L_10 = ___ot3;
		NullCheck(L_10);
		FixedBitArray3_t3665369095 * L_11 = L_10->get_address_of_EdgeIsDelaunay_3();
		FixedBitArray3_Clear_m4005825347(L_11, /*hidden argument*/NULL);
		DelaunayTriangle_t2835103587 * L_12 = ___t2;
		return L_12;
	}

IL_0034:
	{
		DelaunayTriangle_t2835103587 * L_13 = ___t2;
		TriangulationPoint_t3810082933 * L_14 = ___p4;
		TriangulationPoint_t3810082933 * L_15 = ___op5;
		NullCheck(L_13);
		int32_t L_16 = DelaunayTriangle_EdgeIndex_m78242015(L_13, L_14, L_15, /*hidden argument*/NULL);
		V_0 = L_16;
		DelaunayTriangle_t2835103587 * L_17 = ___t2;
		NullCheck(L_17);
		FixedBitArray3_t3665369095 * L_18 = L_17->get_address_of_EdgeIsDelaunay_3();
		int32_t L_19 = V_0;
		FixedBitArray3_set_Item_m2925852176(L_18, L_19, (bool)1, /*hidden argument*/NULL);
		DTSweepContext_t543731303 * L_20 = ___tcx0;
		DelaunayTriangle_t2835103587 * L_21 = ___t2;
		DTSweep_Legalize_m2173221096(NULL /*static, unused*/, L_20, L_21, /*hidden argument*/NULL);
		DelaunayTriangle_t2835103587 * L_22 = ___t2;
		NullCheck(L_22);
		FixedBitArray3_t3665369095 * L_23 = L_22->get_address_of_EdgeIsDelaunay_3();
		FixedBitArray3_Clear_m4005825347(L_23, /*hidden argument*/NULL);
		DelaunayTriangle_t2835103587 * L_24 = ___ot3;
		return L_24;
	}
}
// System.Void Pathfinding.Poly2Tri.DTSweep::FlipScanEdgeEvent(Pathfinding.Poly2Tri.DTSweepContext,Pathfinding.Poly2Tri.TriangulationPoint,Pathfinding.Poly2Tri.TriangulationPoint,Pathfinding.Poly2Tri.DelaunayTriangle,Pathfinding.Poly2Tri.DelaunayTriangle,Pathfinding.Poly2Tri.TriangulationPoint)
extern Il2CppClass* Exception_t3991598821_il2cpp_TypeInfo_var;
extern Il2CppClass* TriangulationUtil_t1170824903_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral505650460;
extern const uint32_t DTSweep_FlipScanEdgeEvent_m1758040592_MetadataUsageId;
extern "C"  void DTSweep_FlipScanEdgeEvent_m1758040592 (Il2CppObject * __this /* static, unused */, DTSweepContext_t543731303 * ___tcx0, TriangulationPoint_t3810082933 * ___ep1, TriangulationPoint_t3810082933 * ___eq2, DelaunayTriangle_t2835103587 * ___flipTriangle3, DelaunayTriangle_t2835103587 * ___t4, TriangulationPoint_t3810082933 * ___p5, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DTSweep_FlipScanEdgeEvent_m1758040592_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DelaunayTriangle_t2835103587 * V_0 = NULL;
	TriangulationPoint_t3810082933 * V_1 = NULL;
	TriangulationPoint_t3810082933 * V_2 = NULL;
	bool V_3 = false;
	{
		DelaunayTriangle_t2835103587 * L_0 = ___t4;
		TriangulationPoint_t3810082933 * L_1 = ___p5;
		NullCheck(L_0);
		DelaunayTriangle_t2835103587 * L_2 = DelaunayTriangle_NeighborAcrossFrom_m3663882196(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		DelaunayTriangle_t2835103587 * L_3 = V_0;
		DelaunayTriangle_t2835103587 * L_4 = ___t4;
		TriangulationPoint_t3810082933 * L_5 = ___p5;
		NullCheck(L_3);
		TriangulationPoint_t3810082933 * L_6 = DelaunayTriangle_OppositePoint_m3734492474(L_3, L_4, L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		DelaunayTriangle_t2835103587 * L_7 = V_0;
		if (L_7)
		{
			goto IL_0026;
		}
	}
	{
		Exception_t3991598821 * L_8 = (Exception_t3991598821 *)il2cpp_codegen_object_new(Exception_t3991598821_il2cpp_TypeInfo_var);
		Exception__ctor_m3870771296(L_8, _stringLiteral505650460, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}

IL_0026:
	{
		DTSweepContext_t543731303 * L_9 = ___tcx0;
		NullCheck(L_9);
		bool L_10 = VirtFuncInvoker0< bool >::Invoke(8 /* System.Boolean Pathfinding.Poly2Tri.TriangulationContext::get_IsDebugEnabled() */, L_9);
		if (!L_10)
		{
			goto IL_004a;
		}
	}
	{
		DTSweepContext_t543731303 * L_11 = ___tcx0;
		NullCheck(L_11);
		DTSweepDebugContext_t3829537966 * L_12 = TriangulationContext_get_DTDebugContext_m2409356104(L_11, /*hidden argument*/NULL);
		DelaunayTriangle_t2835103587 * L_13 = ___t4;
		NullCheck(L_12);
		DTSweepDebugContext_set_PrimaryTriangle_m527373270(L_12, L_13, /*hidden argument*/NULL);
		DTSweepContext_t543731303 * L_14 = ___tcx0;
		NullCheck(L_14);
		DTSweepDebugContext_t3829537966 * L_15 = TriangulationContext_get_DTDebugContext_m2409356104(L_14, /*hidden argument*/NULL);
		DelaunayTriangle_t2835103587 * L_16 = V_0;
		NullCheck(L_15);
		DTSweepDebugContext_set_SecondaryTriangle_m1903864548(L_15, L_16, /*hidden argument*/NULL);
	}

IL_004a:
	{
		TriangulationPoint_t3810082933 * L_17 = ___eq2;
		DelaunayTriangle_t2835103587 * L_18 = ___flipTriangle3;
		TriangulationPoint_t3810082933 * L_19 = ___eq2;
		NullCheck(L_18);
		TriangulationPoint_t3810082933 * L_20 = DelaunayTriangle_PointCCWFrom_m667919724(L_18, L_19, /*hidden argument*/NULL);
		DelaunayTriangle_t2835103587 * L_21 = ___flipTriangle3;
		TriangulationPoint_t3810082933 * L_22 = ___eq2;
		NullCheck(L_21);
		TriangulationPoint_t3810082933 * L_23 = DelaunayTriangle_PointCWFrom_m3064891095(L_21, L_22, /*hidden argument*/NULL);
		TriangulationPoint_t3810082933 * L_24 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(TriangulationUtil_t1170824903_il2cpp_TypeInfo_var);
		bool L_25 = TriangulationUtil_InScanArea_m680109047(NULL /*static, unused*/, L_17, L_20, L_23, L_24, /*hidden argument*/NULL);
		V_3 = L_25;
		bool L_26 = V_3;
		if (!L_26)
		{
			goto IL_0075;
		}
	}
	{
		DTSweepContext_t543731303 * L_27 = ___tcx0;
		TriangulationPoint_t3810082933 * L_28 = ___eq2;
		TriangulationPoint_t3810082933 * L_29 = V_1;
		DelaunayTriangle_t2835103587 * L_30 = V_0;
		TriangulationPoint_t3810082933 * L_31 = V_1;
		DTSweep_FlipEdgeEvent_m1349562325(NULL /*static, unused*/, L_27, L_28, L_29, L_30, L_31, /*hidden argument*/NULL);
		goto IL_008a;
	}

IL_0075:
	{
		TriangulationPoint_t3810082933 * L_32 = ___ep1;
		TriangulationPoint_t3810082933 * L_33 = ___eq2;
		DelaunayTriangle_t2835103587 * L_34 = V_0;
		TriangulationPoint_t3810082933 * L_35 = V_1;
		TriangulationPoint_t3810082933 * L_36 = DTSweep_NextFlipPoint_m2493387054(NULL /*static, unused*/, L_32, L_33, L_34, L_35, /*hidden argument*/NULL);
		V_2 = L_36;
		DTSweepContext_t543731303 * L_37 = ___tcx0;
		TriangulationPoint_t3810082933 * L_38 = ___ep1;
		TriangulationPoint_t3810082933 * L_39 = ___eq2;
		DelaunayTriangle_t2835103587 * L_40 = ___flipTriangle3;
		DelaunayTriangle_t2835103587 * L_41 = V_0;
		TriangulationPoint_t3810082933 * L_42 = V_2;
		DTSweep_FlipScanEdgeEvent_m1758040592(NULL /*static, unused*/, L_37, L_38, L_39, L_40, L_41, L_42, /*hidden argument*/NULL);
	}

IL_008a:
	{
		return;
	}
}
// System.Void Pathfinding.Poly2Tri.DTSweep::FillAdvancingFront(Pathfinding.Poly2Tri.DTSweepContext,Pathfinding.Poly2Tri.AdvancingFrontNode)
extern "C"  void DTSweep_FillAdvancingFront_m2175963311 (Il2CppObject * __this /* static, unused */, DTSweepContext_t543731303 * ___tcx0, AdvancingFrontNode_t688967424 * ___n1, const MethodInfo* method)
{
	AdvancingFrontNode_t688967424 * V_0 = NULL;
	double V_1 = 0.0;
	{
		AdvancingFrontNode_t688967424 * L_0 = ___n1;
		NullCheck(L_0);
		AdvancingFrontNode_t688967424 * L_1 = L_0->get_Next_0();
		V_0 = L_1;
		goto IL_0044;
	}

IL_000c:
	{
		AdvancingFrontNode_t688967424 * L_2 = V_0;
		double L_3 = DTSweep_HoleAngle_m3245728986(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		double L_4 = V_1;
		if ((((double)L_4) > ((double)(1.5707963267948966))))
		{
			goto IL_0031;
		}
	}
	{
		double L_5 = V_1;
		if ((!(((double)L_5) < ((double)(-1.5707963267948966)))))
		{
			goto IL_0036;
		}
	}

IL_0031:
	{
		goto IL_004f;
	}

IL_0036:
	{
		DTSweepContext_t543731303 * L_6 = ___tcx0;
		AdvancingFrontNode_t688967424 * L_7 = V_0;
		DTSweep_Fill_m2383500537(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		AdvancingFrontNode_t688967424 * L_8 = V_0;
		NullCheck(L_8);
		AdvancingFrontNode_t688967424 * L_9 = L_8->get_Next_0();
		V_0 = L_9;
	}

IL_0044:
	{
		AdvancingFrontNode_t688967424 * L_10 = V_0;
		NullCheck(L_10);
		bool L_11 = AdvancingFrontNode_get_HasNext_m1764143671(L_10, /*hidden argument*/NULL);
		if (L_11)
		{
			goto IL_000c;
		}
	}

IL_004f:
	{
		AdvancingFrontNode_t688967424 * L_12 = ___n1;
		NullCheck(L_12);
		AdvancingFrontNode_t688967424 * L_13 = L_12->get_Prev_1();
		V_0 = L_13;
		goto IL_0093;
	}

IL_005b:
	{
		AdvancingFrontNode_t688967424 * L_14 = V_0;
		double L_15 = DTSweep_HoleAngle_m3245728986(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		V_1 = L_15;
		double L_16 = V_1;
		if ((((double)L_16) > ((double)(1.5707963267948966))))
		{
			goto IL_0080;
		}
	}
	{
		double L_17 = V_1;
		if ((!(((double)L_17) < ((double)(-1.5707963267948966)))))
		{
			goto IL_0085;
		}
	}

IL_0080:
	{
		goto IL_009e;
	}

IL_0085:
	{
		DTSweepContext_t543731303 * L_18 = ___tcx0;
		AdvancingFrontNode_t688967424 * L_19 = V_0;
		DTSweep_Fill_m2383500537(NULL /*static, unused*/, L_18, L_19, /*hidden argument*/NULL);
		AdvancingFrontNode_t688967424 * L_20 = V_0;
		NullCheck(L_20);
		AdvancingFrontNode_t688967424 * L_21 = L_20->get_Prev_1();
		V_0 = L_21;
	}

IL_0093:
	{
		AdvancingFrontNode_t688967424 * L_22 = V_0;
		NullCheck(L_22);
		bool L_23 = AdvancingFrontNode_get_HasPrev_m1832843639(L_22, /*hidden argument*/NULL);
		if (L_23)
		{
			goto IL_005b;
		}
	}

IL_009e:
	{
		AdvancingFrontNode_t688967424 * L_24 = ___n1;
		NullCheck(L_24);
		bool L_25 = AdvancingFrontNode_get_HasNext_m1764143671(L_24, /*hidden argument*/NULL);
		if (!L_25)
		{
			goto IL_00d6;
		}
	}
	{
		AdvancingFrontNode_t688967424 * L_26 = ___n1;
		NullCheck(L_26);
		AdvancingFrontNode_t688967424 * L_27 = L_26->get_Next_0();
		NullCheck(L_27);
		bool L_28 = AdvancingFrontNode_get_HasNext_m1764143671(L_27, /*hidden argument*/NULL);
		if (!L_28)
		{
			goto IL_00d6;
		}
	}
	{
		AdvancingFrontNode_t688967424 * L_29 = ___n1;
		double L_30 = DTSweep_BasinAngle_m1054115749(NULL /*static, unused*/, L_29, /*hidden argument*/NULL);
		V_1 = L_30;
		double L_31 = V_1;
		if ((!(((double)L_31) < ((double)(2.3561944901923448)))))
		{
			goto IL_00d6;
		}
	}
	{
		DTSweepContext_t543731303 * L_32 = ___tcx0;
		AdvancingFrontNode_t688967424 * L_33 = ___n1;
		DTSweep_FillBasin_m2083514350(NULL /*static, unused*/, L_32, L_33, /*hidden argument*/NULL);
	}

IL_00d6:
	{
		return;
	}
}
// System.Void Pathfinding.Poly2Tri.DTSweep::FillBasin(Pathfinding.Poly2Tri.DTSweepContext,Pathfinding.Poly2Tri.AdvancingFrontNode)
extern Il2CppClass* TriangulationUtil_t1170824903_il2cpp_TypeInfo_var;
extern const uint32_t DTSweep_FillBasin_m2083514350_MetadataUsageId;
extern "C"  void DTSweep_FillBasin_m2083514350 (Il2CppObject * __this /* static, unused */, DTSweepContext_t543731303 * ___tcx0, AdvancingFrontNode_t688967424 * ___node1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DTSweep_FillBasin_m2083514350_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		AdvancingFrontNode_t688967424 * L_0 = ___node1;
		NullCheck(L_0);
		TriangulationPoint_t3810082933 * L_1 = L_0->get_Point_3();
		AdvancingFrontNode_t688967424 * L_2 = ___node1;
		NullCheck(L_2);
		AdvancingFrontNode_t688967424 * L_3 = L_2->get_Next_0();
		NullCheck(L_3);
		TriangulationPoint_t3810082933 * L_4 = L_3->get_Point_3();
		AdvancingFrontNode_t688967424 * L_5 = ___node1;
		NullCheck(L_5);
		AdvancingFrontNode_t688967424 * L_6 = L_5->get_Next_0();
		NullCheck(L_6);
		AdvancingFrontNode_t688967424 * L_7 = L_6->get_Next_0();
		NullCheck(L_7);
		TriangulationPoint_t3810082933 * L_8 = L_7->get_Point_3();
		IL2CPP_RUNTIME_CLASS_INIT(TriangulationUtil_t1170824903_il2cpp_TypeInfo_var);
		int32_t L_9 = TriangulationUtil_Orient2d_m3842222219(NULL /*static, unused*/, L_1, L_4, L_8, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_9) == ((uint32_t)1))))
		{
			goto IL_003d;
		}
	}
	{
		DTSweepContext_t543731303 * L_10 = ___tcx0;
		NullCheck(L_10);
		DTSweepBasin_t2452859313 * L_11 = L_10->get_Basin_9();
		AdvancingFrontNode_t688967424 * L_12 = ___node1;
		NullCheck(L_11);
		L_11->set_leftNode_0(L_12);
		goto IL_004e;
	}

IL_003d:
	{
		DTSweepContext_t543731303 * L_13 = ___tcx0;
		NullCheck(L_13);
		DTSweepBasin_t2452859313 * L_14 = L_13->get_Basin_9();
		AdvancingFrontNode_t688967424 * L_15 = ___node1;
		NullCheck(L_15);
		AdvancingFrontNode_t688967424 * L_16 = L_15->get_Next_0();
		NullCheck(L_14);
		L_14->set_leftNode_0(L_16);
	}

IL_004e:
	{
		DTSweepContext_t543731303 * L_17 = ___tcx0;
		NullCheck(L_17);
		DTSweepBasin_t2452859313 * L_18 = L_17->get_Basin_9();
		DTSweepContext_t543731303 * L_19 = ___tcx0;
		NullCheck(L_19);
		DTSweepBasin_t2452859313 * L_20 = L_19->get_Basin_9();
		NullCheck(L_20);
		AdvancingFrontNode_t688967424 * L_21 = L_20->get_leftNode_0();
		NullCheck(L_18);
		L_18->set_bottomNode_1(L_21);
		goto IL_0084;
	}

IL_0069:
	{
		DTSweepContext_t543731303 * L_22 = ___tcx0;
		NullCheck(L_22);
		DTSweepBasin_t2452859313 * L_23 = L_22->get_Basin_9();
		DTSweepContext_t543731303 * L_24 = ___tcx0;
		NullCheck(L_24);
		DTSweepBasin_t2452859313 * L_25 = L_24->get_Basin_9();
		NullCheck(L_25);
		AdvancingFrontNode_t688967424 * L_26 = L_25->get_bottomNode_1();
		NullCheck(L_26);
		AdvancingFrontNode_t688967424 * L_27 = L_26->get_Next_0();
		NullCheck(L_23);
		L_23->set_bottomNode_1(L_27);
	}

IL_0084:
	{
		DTSweepContext_t543731303 * L_28 = ___tcx0;
		NullCheck(L_28);
		DTSweepBasin_t2452859313 * L_29 = L_28->get_Basin_9();
		NullCheck(L_29);
		AdvancingFrontNode_t688967424 * L_30 = L_29->get_bottomNode_1();
		NullCheck(L_30);
		bool L_31 = AdvancingFrontNode_get_HasNext_m1764143671(L_30, /*hidden argument*/NULL);
		if (!L_31)
		{
			goto IL_00cd;
		}
	}
	{
		DTSweepContext_t543731303 * L_32 = ___tcx0;
		NullCheck(L_32);
		DTSweepBasin_t2452859313 * L_33 = L_32->get_Basin_9();
		NullCheck(L_33);
		AdvancingFrontNode_t688967424 * L_34 = L_33->get_bottomNode_1();
		NullCheck(L_34);
		TriangulationPoint_t3810082933 * L_35 = L_34->get_Point_3();
		NullCheck(L_35);
		double L_36 = L_35->get_Y_1();
		DTSweepContext_t543731303 * L_37 = ___tcx0;
		NullCheck(L_37);
		DTSweepBasin_t2452859313 * L_38 = L_37->get_Basin_9();
		NullCheck(L_38);
		AdvancingFrontNode_t688967424 * L_39 = L_38->get_bottomNode_1();
		NullCheck(L_39);
		AdvancingFrontNode_t688967424 * L_40 = L_39->get_Next_0();
		NullCheck(L_40);
		TriangulationPoint_t3810082933 * L_41 = L_40->get_Point_3();
		NullCheck(L_41);
		double L_42 = L_41->get_Y_1();
		if ((((double)L_36) >= ((double)L_42)))
		{
			goto IL_0069;
		}
	}

IL_00cd:
	{
		DTSweepContext_t543731303 * L_43 = ___tcx0;
		NullCheck(L_43);
		DTSweepBasin_t2452859313 * L_44 = L_43->get_Basin_9();
		NullCheck(L_44);
		AdvancingFrontNode_t688967424 * L_45 = L_44->get_bottomNode_1();
		DTSweepContext_t543731303 * L_46 = ___tcx0;
		NullCheck(L_46);
		DTSweepBasin_t2452859313 * L_47 = L_46->get_Basin_9();
		NullCheck(L_47);
		AdvancingFrontNode_t688967424 * L_48 = L_47->get_leftNode_0();
		if ((!(((Il2CppObject*)(AdvancingFrontNode_t688967424 *)L_45) == ((Il2CppObject*)(AdvancingFrontNode_t688967424 *)L_48))))
		{
			goto IL_00e9;
		}
	}
	{
		return;
	}

IL_00e9:
	{
		DTSweepContext_t543731303 * L_49 = ___tcx0;
		NullCheck(L_49);
		DTSweepBasin_t2452859313 * L_50 = L_49->get_Basin_9();
		DTSweepContext_t543731303 * L_51 = ___tcx0;
		NullCheck(L_51);
		DTSweepBasin_t2452859313 * L_52 = L_51->get_Basin_9();
		NullCheck(L_52);
		AdvancingFrontNode_t688967424 * L_53 = L_52->get_bottomNode_1();
		NullCheck(L_50);
		L_50->set_rightNode_2(L_53);
		goto IL_011f;
	}

IL_0104:
	{
		DTSweepContext_t543731303 * L_54 = ___tcx0;
		NullCheck(L_54);
		DTSweepBasin_t2452859313 * L_55 = L_54->get_Basin_9();
		DTSweepContext_t543731303 * L_56 = ___tcx0;
		NullCheck(L_56);
		DTSweepBasin_t2452859313 * L_57 = L_56->get_Basin_9();
		NullCheck(L_57);
		AdvancingFrontNode_t688967424 * L_58 = L_57->get_rightNode_2();
		NullCheck(L_58);
		AdvancingFrontNode_t688967424 * L_59 = L_58->get_Next_0();
		NullCheck(L_55);
		L_55->set_rightNode_2(L_59);
	}

IL_011f:
	{
		DTSweepContext_t543731303 * L_60 = ___tcx0;
		NullCheck(L_60);
		DTSweepBasin_t2452859313 * L_61 = L_60->get_Basin_9();
		NullCheck(L_61);
		AdvancingFrontNode_t688967424 * L_62 = L_61->get_rightNode_2();
		NullCheck(L_62);
		bool L_63 = AdvancingFrontNode_get_HasNext_m1764143671(L_62, /*hidden argument*/NULL);
		if (!L_63)
		{
			goto IL_0168;
		}
	}
	{
		DTSweepContext_t543731303 * L_64 = ___tcx0;
		NullCheck(L_64);
		DTSweepBasin_t2452859313 * L_65 = L_64->get_Basin_9();
		NullCheck(L_65);
		AdvancingFrontNode_t688967424 * L_66 = L_65->get_rightNode_2();
		NullCheck(L_66);
		TriangulationPoint_t3810082933 * L_67 = L_66->get_Point_3();
		NullCheck(L_67);
		double L_68 = L_67->get_Y_1();
		DTSweepContext_t543731303 * L_69 = ___tcx0;
		NullCheck(L_69);
		DTSweepBasin_t2452859313 * L_70 = L_69->get_Basin_9();
		NullCheck(L_70);
		AdvancingFrontNode_t688967424 * L_71 = L_70->get_rightNode_2();
		NullCheck(L_71);
		AdvancingFrontNode_t688967424 * L_72 = L_71->get_Next_0();
		NullCheck(L_72);
		TriangulationPoint_t3810082933 * L_73 = L_72->get_Point_3();
		NullCheck(L_73);
		double L_74 = L_73->get_Y_1();
		if ((((double)L_68) < ((double)L_74)))
		{
			goto IL_0104;
		}
	}

IL_0168:
	{
		DTSweepContext_t543731303 * L_75 = ___tcx0;
		NullCheck(L_75);
		DTSweepBasin_t2452859313 * L_76 = L_75->get_Basin_9();
		NullCheck(L_76);
		AdvancingFrontNode_t688967424 * L_77 = L_76->get_rightNode_2();
		DTSweepContext_t543731303 * L_78 = ___tcx0;
		NullCheck(L_78);
		DTSweepBasin_t2452859313 * L_79 = L_78->get_Basin_9();
		NullCheck(L_79);
		AdvancingFrontNode_t688967424 * L_80 = L_79->get_bottomNode_1();
		if ((!(((Il2CppObject*)(AdvancingFrontNode_t688967424 *)L_77) == ((Il2CppObject*)(AdvancingFrontNode_t688967424 *)L_80))))
		{
			goto IL_0184;
		}
	}
	{
		return;
	}

IL_0184:
	{
		DTSweepContext_t543731303 * L_81 = ___tcx0;
		NullCheck(L_81);
		DTSweepBasin_t2452859313 * L_82 = L_81->get_Basin_9();
		DTSweepContext_t543731303 * L_83 = ___tcx0;
		NullCheck(L_83);
		DTSweepBasin_t2452859313 * L_84 = L_83->get_Basin_9();
		NullCheck(L_84);
		AdvancingFrontNode_t688967424 * L_85 = L_84->get_rightNode_2();
		NullCheck(L_85);
		TriangulationPoint_t3810082933 * L_86 = L_85->get_Point_3();
		NullCheck(L_86);
		double L_87 = L_86->get_X_0();
		DTSweepContext_t543731303 * L_88 = ___tcx0;
		NullCheck(L_88);
		DTSweepBasin_t2452859313 * L_89 = L_88->get_Basin_9();
		NullCheck(L_89);
		AdvancingFrontNode_t688967424 * L_90 = L_89->get_leftNode_0();
		NullCheck(L_90);
		TriangulationPoint_t3810082933 * L_91 = L_90->get_Point_3();
		NullCheck(L_91);
		double L_92 = L_91->get_X_0();
		NullCheck(L_82);
		L_82->set_width_3(((double)((double)L_87-(double)L_92)));
		DTSweepContext_t543731303 * L_93 = ___tcx0;
		NullCheck(L_93);
		DTSweepBasin_t2452859313 * L_94 = L_93->get_Basin_9();
		DTSweepContext_t543731303 * L_95 = ___tcx0;
		NullCheck(L_95);
		DTSweepBasin_t2452859313 * L_96 = L_95->get_Basin_9();
		NullCheck(L_96);
		AdvancingFrontNode_t688967424 * L_97 = L_96->get_leftNode_0();
		NullCheck(L_97);
		TriangulationPoint_t3810082933 * L_98 = L_97->get_Point_3();
		NullCheck(L_98);
		double L_99 = L_98->get_Y_1();
		DTSweepContext_t543731303 * L_100 = ___tcx0;
		NullCheck(L_100);
		DTSweepBasin_t2452859313 * L_101 = L_100->get_Basin_9();
		NullCheck(L_101);
		AdvancingFrontNode_t688967424 * L_102 = L_101->get_rightNode_2();
		NullCheck(L_102);
		TriangulationPoint_t3810082933 * L_103 = L_102->get_Point_3();
		NullCheck(L_103);
		double L_104 = L_103->get_Y_1();
		NullCheck(L_94);
		L_94->set_leftHighest_4((bool)((((double)L_99) > ((double)L_104))? 1 : 0));
		DTSweepContext_t543731303 * L_105 = ___tcx0;
		DTSweepContext_t543731303 * L_106 = ___tcx0;
		NullCheck(L_106);
		DTSweepBasin_t2452859313 * L_107 = L_106->get_Basin_9();
		NullCheck(L_107);
		AdvancingFrontNode_t688967424 * L_108 = L_107->get_bottomNode_1();
		DTSweep_FillBasinReq_m472506964(NULL /*static, unused*/, L_105, L_108, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Pathfinding.Poly2Tri.DTSweep::FillBasinReq(Pathfinding.Poly2Tri.DTSweepContext,Pathfinding.Poly2Tri.AdvancingFrontNode)
extern Il2CppClass* TriangulationUtil_t1170824903_il2cpp_TypeInfo_var;
extern const uint32_t DTSweep_FillBasinReq_m472506964_MetadataUsageId;
extern "C"  void DTSweep_FillBasinReq_m472506964 (Il2CppObject * __this /* static, unused */, DTSweepContext_t543731303 * ___tcx0, AdvancingFrontNode_t688967424 * ___node1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DTSweep_FillBasinReq_m472506964_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		DTSweepContext_t543731303 * L_0 = ___tcx0;
		AdvancingFrontNode_t688967424 * L_1 = ___node1;
		bool L_2 = DTSweep_IsShallow_m3574832752(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_000d;
		}
	}
	{
		return;
	}

IL_000d:
	{
		DTSweepContext_t543731303 * L_3 = ___tcx0;
		AdvancingFrontNode_t688967424 * L_4 = ___node1;
		DTSweep_Fill_m2383500537(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		AdvancingFrontNode_t688967424 * L_5 = ___node1;
		NullCheck(L_5);
		AdvancingFrontNode_t688967424 * L_6 = L_5->get_Prev_1();
		DTSweepContext_t543731303 * L_7 = ___tcx0;
		NullCheck(L_7);
		DTSweepBasin_t2452859313 * L_8 = L_7->get_Basin_9();
		NullCheck(L_8);
		AdvancingFrontNode_t688967424 * L_9 = L_8->get_leftNode_0();
		if ((!(((Il2CppObject*)(AdvancingFrontNode_t688967424 *)L_6) == ((Il2CppObject*)(AdvancingFrontNode_t688967424 *)L_9))))
		{
			goto IL_0041;
		}
	}
	{
		AdvancingFrontNode_t688967424 * L_10 = ___node1;
		NullCheck(L_10);
		AdvancingFrontNode_t688967424 * L_11 = L_10->get_Next_0();
		DTSweepContext_t543731303 * L_12 = ___tcx0;
		NullCheck(L_12);
		DTSweepBasin_t2452859313 * L_13 = L_12->get_Basin_9();
		NullCheck(L_13);
		AdvancingFrontNode_t688967424 * L_14 = L_13->get_rightNode_2();
		if ((!(((Il2CppObject*)(AdvancingFrontNode_t688967424 *)L_11) == ((Il2CppObject*)(AdvancingFrontNode_t688967424 *)L_14))))
		{
			goto IL_0041;
		}
	}
	{
		return;
	}

IL_0041:
	{
		AdvancingFrontNode_t688967424 * L_15 = ___node1;
		NullCheck(L_15);
		AdvancingFrontNode_t688967424 * L_16 = L_15->get_Prev_1();
		DTSweepContext_t543731303 * L_17 = ___tcx0;
		NullCheck(L_17);
		DTSweepBasin_t2452859313 * L_18 = L_17->get_Basin_9();
		NullCheck(L_18);
		AdvancingFrontNode_t688967424 * L_19 = L_18->get_leftNode_0();
		if ((!(((Il2CppObject*)(AdvancingFrontNode_t688967424 *)L_16) == ((Il2CppObject*)(AdvancingFrontNode_t688967424 *)L_19))))
		{
			goto IL_0092;
		}
	}
	{
		AdvancingFrontNode_t688967424 * L_20 = ___node1;
		NullCheck(L_20);
		TriangulationPoint_t3810082933 * L_21 = L_20->get_Point_3();
		AdvancingFrontNode_t688967424 * L_22 = ___node1;
		NullCheck(L_22);
		AdvancingFrontNode_t688967424 * L_23 = L_22->get_Next_0();
		NullCheck(L_23);
		TriangulationPoint_t3810082933 * L_24 = L_23->get_Point_3();
		AdvancingFrontNode_t688967424 * L_25 = ___node1;
		NullCheck(L_25);
		AdvancingFrontNode_t688967424 * L_26 = L_25->get_Next_0();
		NullCheck(L_26);
		AdvancingFrontNode_t688967424 * L_27 = L_26->get_Next_0();
		NullCheck(L_27);
		TriangulationPoint_t3810082933 * L_28 = L_27->get_Point_3();
		IL2CPP_RUNTIME_CLASS_INIT(TriangulationUtil_t1170824903_il2cpp_TypeInfo_var);
		int32_t L_29 = TriangulationUtil_Orient2d_m3842222219(NULL /*static, unused*/, L_21, L_24, L_28, /*hidden argument*/NULL);
		V_0 = L_29;
		int32_t L_30 = V_0;
		if (L_30)
		{
			goto IL_0085;
		}
	}
	{
		return;
	}

IL_0085:
	{
		AdvancingFrontNode_t688967424 * L_31 = ___node1;
		NullCheck(L_31);
		AdvancingFrontNode_t688967424 * L_32 = L_31->get_Next_0();
		___node1 = L_32;
		goto IL_011e;
	}

IL_0092:
	{
		AdvancingFrontNode_t688967424 * L_33 = ___node1;
		NullCheck(L_33);
		AdvancingFrontNode_t688967424 * L_34 = L_33->get_Next_0();
		DTSweepContext_t543731303 * L_35 = ___tcx0;
		NullCheck(L_35);
		DTSweepBasin_t2452859313 * L_36 = L_35->get_Basin_9();
		NullCheck(L_36);
		AdvancingFrontNode_t688967424 * L_37 = L_36->get_rightNode_2();
		if ((!(((Il2CppObject*)(AdvancingFrontNode_t688967424 *)L_34) == ((Il2CppObject*)(AdvancingFrontNode_t688967424 *)L_37))))
		{
			goto IL_00e4;
		}
	}
	{
		AdvancingFrontNode_t688967424 * L_38 = ___node1;
		NullCheck(L_38);
		TriangulationPoint_t3810082933 * L_39 = L_38->get_Point_3();
		AdvancingFrontNode_t688967424 * L_40 = ___node1;
		NullCheck(L_40);
		AdvancingFrontNode_t688967424 * L_41 = L_40->get_Prev_1();
		NullCheck(L_41);
		TriangulationPoint_t3810082933 * L_42 = L_41->get_Point_3();
		AdvancingFrontNode_t688967424 * L_43 = ___node1;
		NullCheck(L_43);
		AdvancingFrontNode_t688967424 * L_44 = L_43->get_Prev_1();
		NullCheck(L_44);
		AdvancingFrontNode_t688967424 * L_45 = L_44->get_Prev_1();
		NullCheck(L_45);
		TriangulationPoint_t3810082933 * L_46 = L_45->get_Point_3();
		IL2CPP_RUNTIME_CLASS_INIT(TriangulationUtil_t1170824903_il2cpp_TypeInfo_var);
		int32_t L_47 = TriangulationUtil_Orient2d_m3842222219(NULL /*static, unused*/, L_39, L_42, L_46, /*hidden argument*/NULL);
		V_1 = L_47;
		int32_t L_48 = V_1;
		if ((!(((uint32_t)L_48) == ((uint32_t)1))))
		{
			goto IL_00d7;
		}
	}
	{
		return;
	}

IL_00d7:
	{
		AdvancingFrontNode_t688967424 * L_49 = ___node1;
		NullCheck(L_49);
		AdvancingFrontNode_t688967424 * L_50 = L_49->get_Prev_1();
		___node1 = L_50;
		goto IL_011e;
	}

IL_00e4:
	{
		AdvancingFrontNode_t688967424 * L_51 = ___node1;
		NullCheck(L_51);
		AdvancingFrontNode_t688967424 * L_52 = L_51->get_Prev_1();
		NullCheck(L_52);
		TriangulationPoint_t3810082933 * L_53 = L_52->get_Point_3();
		NullCheck(L_53);
		double L_54 = L_53->get_Y_1();
		AdvancingFrontNode_t688967424 * L_55 = ___node1;
		NullCheck(L_55);
		AdvancingFrontNode_t688967424 * L_56 = L_55->get_Next_0();
		NullCheck(L_56);
		TriangulationPoint_t3810082933 * L_57 = L_56->get_Point_3();
		NullCheck(L_57);
		double L_58 = L_57->get_Y_1();
		if ((!(((double)L_54) < ((double)L_58))))
		{
			goto IL_0116;
		}
	}
	{
		AdvancingFrontNode_t688967424 * L_59 = ___node1;
		NullCheck(L_59);
		AdvancingFrontNode_t688967424 * L_60 = L_59->get_Prev_1();
		___node1 = L_60;
		goto IL_011e;
	}

IL_0116:
	{
		AdvancingFrontNode_t688967424 * L_61 = ___node1;
		NullCheck(L_61);
		AdvancingFrontNode_t688967424 * L_62 = L_61->get_Next_0();
		___node1 = L_62;
	}

IL_011e:
	{
		DTSweepContext_t543731303 * L_63 = ___tcx0;
		AdvancingFrontNode_t688967424 * L_64 = ___node1;
		DTSweep_FillBasinReq_m472506964(NULL /*static, unused*/, L_63, L_64, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Pathfinding.Poly2Tri.DTSweep::IsShallow(Pathfinding.Poly2Tri.DTSweepContext,Pathfinding.Poly2Tri.AdvancingFrontNode)
extern "C"  bool DTSweep_IsShallow_m3574832752 (Il2CppObject * __this /* static, unused */, DTSweepContext_t543731303 * ___tcx0, AdvancingFrontNode_t688967424 * ___node1, const MethodInfo* method)
{
	double V_0 = 0.0;
	{
		DTSweepContext_t543731303 * L_0 = ___tcx0;
		NullCheck(L_0);
		DTSweepBasin_t2452859313 * L_1 = L_0->get_Basin_9();
		NullCheck(L_1);
		bool L_2 = L_1->get_leftHighest_4();
		if (!L_2)
		{
			goto IL_0037;
		}
	}
	{
		DTSweepContext_t543731303 * L_3 = ___tcx0;
		NullCheck(L_3);
		DTSweepBasin_t2452859313 * L_4 = L_3->get_Basin_9();
		NullCheck(L_4);
		AdvancingFrontNode_t688967424 * L_5 = L_4->get_leftNode_0();
		NullCheck(L_5);
		TriangulationPoint_t3810082933 * L_6 = L_5->get_Point_3();
		NullCheck(L_6);
		double L_7 = L_6->get_Y_1();
		AdvancingFrontNode_t688967424 * L_8 = ___node1;
		NullCheck(L_8);
		TriangulationPoint_t3810082933 * L_9 = L_8->get_Point_3();
		NullCheck(L_9);
		double L_10 = L_9->get_Y_1();
		V_0 = ((double)((double)L_7-(double)L_10));
		goto IL_0059;
	}

IL_0037:
	{
		DTSweepContext_t543731303 * L_11 = ___tcx0;
		NullCheck(L_11);
		DTSweepBasin_t2452859313 * L_12 = L_11->get_Basin_9();
		NullCheck(L_12);
		AdvancingFrontNode_t688967424 * L_13 = L_12->get_rightNode_2();
		NullCheck(L_13);
		TriangulationPoint_t3810082933 * L_14 = L_13->get_Point_3();
		NullCheck(L_14);
		double L_15 = L_14->get_Y_1();
		AdvancingFrontNode_t688967424 * L_16 = ___node1;
		NullCheck(L_16);
		TriangulationPoint_t3810082933 * L_17 = L_16->get_Point_3();
		NullCheck(L_17);
		double L_18 = L_17->get_Y_1();
		V_0 = ((double)((double)L_15-(double)L_18));
	}

IL_0059:
	{
		DTSweepContext_t543731303 * L_19 = ___tcx0;
		NullCheck(L_19);
		DTSweepBasin_t2452859313 * L_20 = L_19->get_Basin_9();
		NullCheck(L_20);
		double L_21 = L_20->get_width_3();
		double L_22 = V_0;
		if ((!(((double)L_21) > ((double)L_22))))
		{
			goto IL_006c;
		}
	}
	{
		return (bool)1;
	}

IL_006c:
	{
		return (bool)0;
	}
}
// System.Double Pathfinding.Poly2Tri.DTSweep::HoleAngle(Pathfinding.Poly2Tri.AdvancingFrontNode)
extern "C"  double DTSweep_HoleAngle_m3245728986 (Il2CppObject * __this /* static, unused */, AdvancingFrontNode_t688967424 * ___node0, const MethodInfo* method)
{
	double V_0 = 0.0;
	double V_1 = 0.0;
	double V_2 = 0.0;
	double V_3 = 0.0;
	double V_4 = 0.0;
	double V_5 = 0.0;
	{
		AdvancingFrontNode_t688967424 * L_0 = ___node0;
		NullCheck(L_0);
		TriangulationPoint_t3810082933 * L_1 = L_0->get_Point_3();
		NullCheck(L_1);
		double L_2 = L_1->get_X_0();
		V_0 = L_2;
		AdvancingFrontNode_t688967424 * L_3 = ___node0;
		NullCheck(L_3);
		TriangulationPoint_t3810082933 * L_4 = L_3->get_Point_3();
		NullCheck(L_4);
		double L_5 = L_4->get_Y_1();
		V_1 = L_5;
		AdvancingFrontNode_t688967424 * L_6 = ___node0;
		NullCheck(L_6);
		AdvancingFrontNode_t688967424 * L_7 = L_6->get_Next_0();
		NullCheck(L_7);
		TriangulationPoint_t3810082933 * L_8 = L_7->get_Point_3();
		NullCheck(L_8);
		double L_9 = L_8->get_X_0();
		double L_10 = V_0;
		V_2 = ((double)((double)L_9-(double)L_10));
		AdvancingFrontNode_t688967424 * L_11 = ___node0;
		NullCheck(L_11);
		AdvancingFrontNode_t688967424 * L_12 = L_11->get_Next_0();
		NullCheck(L_12);
		TriangulationPoint_t3810082933 * L_13 = L_12->get_Point_3();
		NullCheck(L_13);
		double L_14 = L_13->get_Y_1();
		double L_15 = V_1;
		V_3 = ((double)((double)L_14-(double)L_15));
		AdvancingFrontNode_t688967424 * L_16 = ___node0;
		NullCheck(L_16);
		AdvancingFrontNode_t688967424 * L_17 = L_16->get_Prev_1();
		NullCheck(L_17);
		TriangulationPoint_t3810082933 * L_18 = L_17->get_Point_3();
		NullCheck(L_18);
		double L_19 = L_18->get_X_0();
		double L_20 = V_0;
		V_4 = ((double)((double)L_19-(double)L_20));
		AdvancingFrontNode_t688967424 * L_21 = ___node0;
		NullCheck(L_21);
		AdvancingFrontNode_t688967424 * L_22 = L_21->get_Prev_1();
		NullCheck(L_22);
		TriangulationPoint_t3810082933 * L_23 = L_22->get_Point_3();
		NullCheck(L_23);
		double L_24 = L_23->get_Y_1();
		double L_25 = V_1;
		V_5 = ((double)((double)L_24-(double)L_25));
		double L_26 = V_2;
		double L_27 = V_5;
		double L_28 = V_3;
		double L_29 = V_4;
		double L_30 = V_2;
		double L_31 = V_4;
		double L_32 = V_3;
		double L_33 = V_5;
		double L_34 = atan2(((double)((double)((double)((double)L_26*(double)L_27))-(double)((double)((double)L_28*(double)L_29)))), ((double)((double)((double)((double)L_30*(double)L_31))+(double)((double)((double)L_32*(double)L_33)))));
		return L_34;
	}
}
// System.Double Pathfinding.Poly2Tri.DTSweep::BasinAngle(Pathfinding.Poly2Tri.AdvancingFrontNode)
extern "C"  double DTSweep_BasinAngle_m1054115749 (Il2CppObject * __this /* static, unused */, AdvancingFrontNode_t688967424 * ___node0, const MethodInfo* method)
{
	double V_0 = 0.0;
	double V_1 = 0.0;
	{
		AdvancingFrontNode_t688967424 * L_0 = ___node0;
		NullCheck(L_0);
		TriangulationPoint_t3810082933 * L_1 = L_0->get_Point_3();
		NullCheck(L_1);
		double L_2 = L_1->get_X_0();
		AdvancingFrontNode_t688967424 * L_3 = ___node0;
		NullCheck(L_3);
		AdvancingFrontNode_t688967424 * L_4 = L_3->get_Next_0();
		NullCheck(L_4);
		AdvancingFrontNode_t688967424 * L_5 = L_4->get_Next_0();
		NullCheck(L_5);
		TriangulationPoint_t3810082933 * L_6 = L_5->get_Point_3();
		NullCheck(L_6);
		double L_7 = L_6->get_X_0();
		V_0 = ((double)((double)L_2-(double)L_7));
		AdvancingFrontNode_t688967424 * L_8 = ___node0;
		NullCheck(L_8);
		TriangulationPoint_t3810082933 * L_9 = L_8->get_Point_3();
		NullCheck(L_9);
		double L_10 = L_9->get_Y_1();
		AdvancingFrontNode_t688967424 * L_11 = ___node0;
		NullCheck(L_11);
		AdvancingFrontNode_t688967424 * L_12 = L_11->get_Next_0();
		NullCheck(L_12);
		AdvancingFrontNode_t688967424 * L_13 = L_12->get_Next_0();
		NullCheck(L_13);
		TriangulationPoint_t3810082933 * L_14 = L_13->get_Point_3();
		NullCheck(L_14);
		double L_15 = L_14->get_Y_1();
		V_1 = ((double)((double)L_10-(double)L_15));
		double L_16 = V_1;
		double L_17 = V_0;
		double L_18 = atan2(L_16, L_17);
		return L_18;
	}
}
// System.Void Pathfinding.Poly2Tri.DTSweep::Fill(Pathfinding.Poly2Tri.DTSweepContext,Pathfinding.Poly2Tri.AdvancingFrontNode)
extern Il2CppClass* DelaunayTriangle_t2835103587_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_Add_m1995366622_MethodInfo_var;
extern const uint32_t DTSweep_Fill_m2383500537_MetadataUsageId;
extern "C"  void DTSweep_Fill_m2383500537 (Il2CppObject * __this /* static, unused */, DTSweepContext_t543731303 * ___tcx0, AdvancingFrontNode_t688967424 * ___node1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DTSweep_Fill_m2383500537_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DelaunayTriangle_t2835103587 * V_0 = NULL;
	{
		AdvancingFrontNode_t688967424 * L_0 = ___node1;
		NullCheck(L_0);
		AdvancingFrontNode_t688967424 * L_1 = L_0->get_Prev_1();
		NullCheck(L_1);
		TriangulationPoint_t3810082933 * L_2 = L_1->get_Point_3();
		AdvancingFrontNode_t688967424 * L_3 = ___node1;
		NullCheck(L_3);
		TriangulationPoint_t3810082933 * L_4 = L_3->get_Point_3();
		AdvancingFrontNode_t688967424 * L_5 = ___node1;
		NullCheck(L_5);
		AdvancingFrontNode_t688967424 * L_6 = L_5->get_Next_0();
		NullCheck(L_6);
		TriangulationPoint_t3810082933 * L_7 = L_6->get_Point_3();
		DelaunayTriangle_t2835103587 * L_8 = (DelaunayTriangle_t2835103587 *)il2cpp_codegen_object_new(DelaunayTriangle_t2835103587_il2cpp_TypeInfo_var);
		DelaunayTriangle__ctor_m1610534382(L_8, L_2, L_4, L_7, /*hidden argument*/NULL);
		V_0 = L_8;
		DelaunayTriangle_t2835103587 * L_9 = V_0;
		AdvancingFrontNode_t688967424 * L_10 = ___node1;
		NullCheck(L_10);
		AdvancingFrontNode_t688967424 * L_11 = L_10->get_Prev_1();
		NullCheck(L_11);
		DelaunayTriangle_t2835103587 * L_12 = L_11->get_Triangle_4();
		NullCheck(L_9);
		DelaunayTriangle_MarkNeighbor_m119719133(L_9, L_12, /*hidden argument*/NULL);
		DelaunayTriangle_t2835103587 * L_13 = V_0;
		AdvancingFrontNode_t688967424 * L_14 = ___node1;
		NullCheck(L_14);
		DelaunayTriangle_t2835103587 * L_15 = L_14->get_Triangle_4();
		NullCheck(L_13);
		DelaunayTriangle_MarkNeighbor_m119719133(L_13, L_15, /*hidden argument*/NULL);
		DTSweepContext_t543731303 * L_16 = ___tcx0;
		NullCheck(L_16);
		List_1_t4203289139 * L_17 = ((TriangulationContext_t3528662164 *)L_16)->get_Triangles_0();
		DelaunayTriangle_t2835103587 * L_18 = V_0;
		NullCheck(L_17);
		List_1_Add_m1995366622(L_17, L_18, /*hidden argument*/List_1_Add_m1995366622_MethodInfo_var);
		AdvancingFrontNode_t688967424 * L_19 = ___node1;
		NullCheck(L_19);
		AdvancingFrontNode_t688967424 * L_20 = L_19->get_Prev_1();
		AdvancingFrontNode_t688967424 * L_21 = ___node1;
		NullCheck(L_21);
		AdvancingFrontNode_t688967424 * L_22 = L_21->get_Next_0();
		NullCheck(L_20);
		L_20->set_Next_0(L_22);
		AdvancingFrontNode_t688967424 * L_23 = ___node1;
		NullCheck(L_23);
		AdvancingFrontNode_t688967424 * L_24 = L_23->get_Next_0();
		AdvancingFrontNode_t688967424 * L_25 = ___node1;
		NullCheck(L_25);
		AdvancingFrontNode_t688967424 * L_26 = L_25->get_Prev_1();
		NullCheck(L_24);
		L_24->set_Prev_1(L_26);
		DTSweepContext_t543731303 * L_27 = ___tcx0;
		AdvancingFrontNode_t688967424 * L_28 = ___node1;
		NullCheck(L_27);
		DTSweepContext_RemoveNode_m2652831733(L_27, L_28, /*hidden argument*/NULL);
		DTSweepContext_t543731303 * L_29 = ___tcx0;
		DelaunayTriangle_t2835103587 * L_30 = V_0;
		bool L_31 = DTSweep_Legalize_m2173221096(NULL /*static, unused*/, L_29, L_30, /*hidden argument*/NULL);
		if (L_31)
		{
			goto IL_0087;
		}
	}
	{
		DTSweepContext_t543731303 * L_32 = ___tcx0;
		DelaunayTriangle_t2835103587 * L_33 = V_0;
		NullCheck(L_32);
		DTSweepContext_MapTriangleToNodes_m727745286(L_32, L_33, /*hidden argument*/NULL);
	}

IL_0087:
	{
		return;
	}
}
// System.Boolean Pathfinding.Poly2Tri.DTSweep::Legalize(Pathfinding.Poly2Tri.DTSweepContext,Pathfinding.Poly2Tri.DelaunayTriangle)
extern Il2CppClass* TriangulationUtil_t1170824903_il2cpp_TypeInfo_var;
extern const MethodInfo* FixedArray3_1_get_Item_m4227602836_MethodInfo_var;
extern const MethodInfo* FixedArray3_1_get_Item_m7701314_MethodInfo_var;
extern const uint32_t DTSweep_Legalize_m2173221096_MetadataUsageId;
extern "C"  bool DTSweep_Legalize_m2173221096 (Il2CppObject * __this /* static, unused */, DTSweepContext_t543731303 * ___tcx0, DelaunayTriangle_t2835103587 * ___t1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DTSweep_Legalize_m2173221096_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	DelaunayTriangle_t2835103587 * V_1 = NULL;
	TriangulationPoint_t3810082933 * V_2 = NULL;
	TriangulationPoint_t3810082933 * V_3 = NULL;
	int32_t V_4 = 0;
	{
		V_0 = 0;
		goto IL_0120;
	}

IL_0007:
	{
		DelaunayTriangle_t2835103587 * L_0 = ___t1;
		NullCheck(L_0);
		FixedBitArray3_t3665369095 * L_1 = L_0->get_address_of_EdgeIsDelaunay_3();
		int32_t L_2 = V_0;
		bool L_3 = FixedBitArray3_get_Item_m141500035(L_1, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_001d;
		}
	}
	{
		goto IL_011c;
	}

IL_001d:
	{
		DelaunayTriangle_t2835103587 * L_4 = ___t1;
		NullCheck(L_4);
		FixedArray3_1_t3485577237 * L_5 = L_4->get_address_of_Neighbors_1();
		int32_t L_6 = V_0;
		DelaunayTriangle_t2835103587 * L_7 = FixedArray3_1_get_Item_m4227602836(L_5, L_6, /*hidden argument*/FixedArray3_1_get_Item_m4227602836_MethodInfo_var);
		V_1 = L_7;
		DelaunayTriangle_t2835103587 * L_8 = V_1;
		if (L_8)
		{
			goto IL_0035;
		}
	}
	{
		goto IL_011c;
	}

IL_0035:
	{
		DelaunayTriangle_t2835103587 * L_9 = ___t1;
		NullCheck(L_9);
		FixedArray3_1_t165589287 * L_10 = L_9->get_address_of_Points_0();
		int32_t L_11 = V_0;
		TriangulationPoint_t3810082933 * L_12 = FixedArray3_1_get_Item_m7701314(L_10, L_11, /*hidden argument*/FixedArray3_1_get_Item_m7701314_MethodInfo_var);
		V_2 = L_12;
		DelaunayTriangle_t2835103587 * L_13 = V_1;
		DelaunayTriangle_t2835103587 * L_14 = ___t1;
		TriangulationPoint_t3810082933 * L_15 = V_2;
		NullCheck(L_13);
		TriangulationPoint_t3810082933 * L_16 = DelaunayTriangle_OppositePoint_m3734492474(L_13, L_14, L_15, /*hidden argument*/NULL);
		V_3 = L_16;
		DelaunayTriangle_t2835103587 * L_17 = V_1;
		TriangulationPoint_t3810082933 * L_18 = V_3;
		NullCheck(L_17);
		int32_t L_19 = DelaunayTriangle_IndexOf_m1914770527(L_17, L_18, /*hidden argument*/NULL);
		V_4 = L_19;
		DelaunayTriangle_t2835103587 * L_20 = V_1;
		NullCheck(L_20);
		FixedBitArray3_t3665369095 * L_21 = L_20->get_address_of_EdgeIsConstrained_2();
		int32_t L_22 = V_4;
		bool L_23 = FixedBitArray3_get_Item_m141500035(L_21, L_22, /*hidden argument*/NULL);
		if (L_23)
		{
			goto IL_0078;
		}
	}
	{
		DelaunayTriangle_t2835103587 * L_24 = V_1;
		NullCheck(L_24);
		FixedBitArray3_t3665369095 * L_25 = L_24->get_address_of_EdgeIsDelaunay_3();
		int32_t L_26 = V_4;
		bool L_27 = FixedBitArray3_get_Item_m141500035(L_25, L_26, /*hidden argument*/NULL);
		if (!L_27)
		{
			goto IL_0096;
		}
	}

IL_0078:
	{
		DelaunayTriangle_t2835103587 * L_28 = ___t1;
		NullCheck(L_28);
		FixedBitArray3_t3665369095 * L_29 = L_28->get_address_of_EdgeIsConstrained_2();
		int32_t L_30 = V_0;
		DelaunayTriangle_t2835103587 * L_31 = V_1;
		NullCheck(L_31);
		FixedBitArray3_t3665369095 * L_32 = L_31->get_address_of_EdgeIsConstrained_2();
		int32_t L_33 = V_4;
		bool L_34 = FixedBitArray3_get_Item_m141500035(L_32, L_33, /*hidden argument*/NULL);
		FixedBitArray3_set_Item_m2925852176(L_29, L_30, L_34, /*hidden argument*/NULL);
		goto IL_011c;
	}

IL_0096:
	{
		TriangulationPoint_t3810082933 * L_35 = V_2;
		DelaunayTriangle_t2835103587 * L_36 = ___t1;
		TriangulationPoint_t3810082933 * L_37 = V_2;
		NullCheck(L_36);
		TriangulationPoint_t3810082933 * L_38 = DelaunayTriangle_PointCCWFrom_m667919724(L_36, L_37, /*hidden argument*/NULL);
		DelaunayTriangle_t2835103587 * L_39 = ___t1;
		TriangulationPoint_t3810082933 * L_40 = V_2;
		NullCheck(L_39);
		TriangulationPoint_t3810082933 * L_41 = DelaunayTriangle_PointCWFrom_m3064891095(L_39, L_40, /*hidden argument*/NULL);
		TriangulationPoint_t3810082933 * L_42 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(TriangulationUtil_t1170824903_il2cpp_TypeInfo_var);
		bool L_43 = TriangulationUtil_SmartIncircle_m2900908198(NULL /*static, unused*/, L_35, L_38, L_41, L_42, /*hidden argument*/NULL);
		if (L_43)
		{
			goto IL_00b5;
		}
	}
	{
		goto IL_011c;
	}

IL_00b5:
	{
		DelaunayTriangle_t2835103587 * L_44 = ___t1;
		NullCheck(L_44);
		FixedBitArray3_t3665369095 * L_45 = L_44->get_address_of_EdgeIsDelaunay_3();
		int32_t L_46 = V_0;
		FixedBitArray3_set_Item_m2925852176(L_45, L_46, (bool)1, /*hidden argument*/NULL);
		DelaunayTriangle_t2835103587 * L_47 = V_1;
		NullCheck(L_47);
		FixedBitArray3_t3665369095 * L_48 = L_47->get_address_of_EdgeIsDelaunay_3();
		int32_t L_49 = V_4;
		FixedBitArray3_set_Item_m2925852176(L_48, L_49, (bool)1, /*hidden argument*/NULL);
		DelaunayTriangle_t2835103587 * L_50 = ___t1;
		TriangulationPoint_t3810082933 * L_51 = V_2;
		DelaunayTriangle_t2835103587 * L_52 = V_1;
		TriangulationPoint_t3810082933 * L_53 = V_3;
		DTSweep_RotateTrianglePair_m2948867544(NULL /*static, unused*/, L_50, L_51, L_52, L_53, /*hidden argument*/NULL);
		DTSweepContext_t543731303 * L_54 = ___tcx0;
		DelaunayTriangle_t2835103587 * L_55 = ___t1;
		bool L_56 = DTSweep_Legalize_m2173221096(NULL /*static, unused*/, L_54, L_55, /*hidden argument*/NULL);
		if (L_56)
		{
			goto IL_00ec;
		}
	}
	{
		DTSweepContext_t543731303 * L_57 = ___tcx0;
		DelaunayTriangle_t2835103587 * L_58 = ___t1;
		NullCheck(L_57);
		DTSweepContext_MapTriangleToNodes_m727745286(L_57, L_58, /*hidden argument*/NULL);
	}

IL_00ec:
	{
		DTSweepContext_t543731303 * L_59 = ___tcx0;
		DelaunayTriangle_t2835103587 * L_60 = V_1;
		bool L_61 = DTSweep_Legalize_m2173221096(NULL /*static, unused*/, L_59, L_60, /*hidden argument*/NULL);
		if (L_61)
		{
			goto IL_00ff;
		}
	}
	{
		DTSweepContext_t543731303 * L_62 = ___tcx0;
		DelaunayTriangle_t2835103587 * L_63 = V_1;
		NullCheck(L_62);
		DTSweepContext_MapTriangleToNodes_m727745286(L_62, L_63, /*hidden argument*/NULL);
	}

IL_00ff:
	{
		DelaunayTriangle_t2835103587 * L_64 = ___t1;
		NullCheck(L_64);
		FixedBitArray3_t3665369095 * L_65 = L_64->get_address_of_EdgeIsDelaunay_3();
		int32_t L_66 = V_0;
		FixedBitArray3_set_Item_m2925852176(L_65, L_66, (bool)0, /*hidden argument*/NULL);
		DelaunayTriangle_t2835103587 * L_67 = V_1;
		NullCheck(L_67);
		FixedBitArray3_t3665369095 * L_68 = L_67->get_address_of_EdgeIsDelaunay_3();
		int32_t L_69 = V_4;
		FixedBitArray3_set_Item_m2925852176(L_68, L_69, (bool)0, /*hidden argument*/NULL);
		return (bool)1;
	}

IL_011c:
	{
		int32_t L_70 = V_0;
		V_0 = ((int32_t)((int32_t)L_70+(int32_t)1));
	}

IL_0120:
	{
		int32_t L_71 = V_0;
		if ((((int32_t)L_71) < ((int32_t)3)))
		{
			goto IL_0007;
		}
	}
	{
		return (bool)0;
	}
}
// System.Void Pathfinding.Poly2Tri.DTSweep::RotateTrianglePair(Pathfinding.Poly2Tri.DelaunayTriangle,Pathfinding.Poly2Tri.TriangulationPoint,Pathfinding.Poly2Tri.DelaunayTriangle,Pathfinding.Poly2Tri.TriangulationPoint)
extern const MethodInfo* FixedArray3_1_Clear_m3990085865_MethodInfo_var;
extern const uint32_t DTSweep_RotateTrianglePair_m2948867544_MetadataUsageId;
extern "C"  void DTSweep_RotateTrianglePair_m2948867544 (Il2CppObject * __this /* static, unused */, DelaunayTriangle_t2835103587 * ___t0, TriangulationPoint_t3810082933 * ___p1, DelaunayTriangle_t2835103587 * ___ot2, TriangulationPoint_t3810082933 * ___op3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DTSweep_RotateTrianglePair_m2948867544_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DelaunayTriangle_t2835103587 * V_0 = NULL;
	DelaunayTriangle_t2835103587 * V_1 = NULL;
	DelaunayTriangle_t2835103587 * V_2 = NULL;
	DelaunayTriangle_t2835103587 * V_3 = NULL;
	bool V_4 = false;
	bool V_5 = false;
	bool V_6 = false;
	bool V_7 = false;
	bool V_8 = false;
	bool V_9 = false;
	bool V_10 = false;
	bool V_11 = false;
	{
		DelaunayTriangle_t2835103587 * L_0 = ___t0;
		TriangulationPoint_t3810082933 * L_1 = ___p1;
		NullCheck(L_0);
		DelaunayTriangle_t2835103587 * L_2 = DelaunayTriangle_NeighborCCWFrom_m3747172644(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		DelaunayTriangle_t2835103587 * L_3 = ___t0;
		TriangulationPoint_t3810082933 * L_4 = ___p1;
		NullCheck(L_3);
		DelaunayTriangle_t2835103587 * L_5 = DelaunayTriangle_NeighborCWFrom_m3856958495(L_3, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		DelaunayTriangle_t2835103587 * L_6 = ___ot2;
		TriangulationPoint_t3810082933 * L_7 = ___op3;
		NullCheck(L_6);
		DelaunayTriangle_t2835103587 * L_8 = DelaunayTriangle_NeighborCCWFrom_m3747172644(L_6, L_7, /*hidden argument*/NULL);
		V_2 = L_8;
		DelaunayTriangle_t2835103587 * L_9 = ___ot2;
		TriangulationPoint_t3810082933 * L_10 = ___op3;
		NullCheck(L_9);
		DelaunayTriangle_t2835103587 * L_11 = DelaunayTriangle_NeighborCWFrom_m3856958495(L_9, L_10, /*hidden argument*/NULL);
		V_3 = L_11;
		DelaunayTriangle_t2835103587 * L_12 = ___t0;
		TriangulationPoint_t3810082933 * L_13 = ___p1;
		NullCheck(L_12);
		bool L_14 = DelaunayTriangle_GetConstrainedEdgeCCW_m1246493448(L_12, L_13, /*hidden argument*/NULL);
		V_4 = L_14;
		DelaunayTriangle_t2835103587 * L_15 = ___t0;
		TriangulationPoint_t3810082933 * L_16 = ___p1;
		NullCheck(L_15);
		bool L_17 = DelaunayTriangle_GetConstrainedEdgeCW_m1169250959(L_15, L_16, /*hidden argument*/NULL);
		V_5 = L_17;
		DelaunayTriangle_t2835103587 * L_18 = ___ot2;
		TriangulationPoint_t3810082933 * L_19 = ___op3;
		NullCheck(L_18);
		bool L_20 = DelaunayTriangle_GetConstrainedEdgeCCW_m1246493448(L_18, L_19, /*hidden argument*/NULL);
		V_6 = L_20;
		DelaunayTriangle_t2835103587 * L_21 = ___ot2;
		TriangulationPoint_t3810082933 * L_22 = ___op3;
		NullCheck(L_21);
		bool L_23 = DelaunayTriangle_GetConstrainedEdgeCW_m1169250959(L_21, L_22, /*hidden argument*/NULL);
		V_7 = L_23;
		DelaunayTriangle_t2835103587 * L_24 = ___t0;
		TriangulationPoint_t3810082933 * L_25 = ___p1;
		NullCheck(L_24);
		bool L_26 = DelaunayTriangle_GetDelaunayEdgeCCW_m1588591715(L_24, L_25, /*hidden argument*/NULL);
		V_8 = L_26;
		DelaunayTriangle_t2835103587 * L_27 = ___t0;
		TriangulationPoint_t3810082933 * L_28 = ___p1;
		NullCheck(L_27);
		bool L_29 = DelaunayTriangle_GetDelaunayEdgeCW_m2288665044(L_27, L_28, /*hidden argument*/NULL);
		V_9 = L_29;
		DelaunayTriangle_t2835103587 * L_30 = ___ot2;
		TriangulationPoint_t3810082933 * L_31 = ___op3;
		NullCheck(L_30);
		bool L_32 = DelaunayTriangle_GetDelaunayEdgeCCW_m1588591715(L_30, L_31, /*hidden argument*/NULL);
		V_10 = L_32;
		DelaunayTriangle_t2835103587 * L_33 = ___ot2;
		TriangulationPoint_t3810082933 * L_34 = ___op3;
		NullCheck(L_33);
		bool L_35 = DelaunayTriangle_GetDelaunayEdgeCW_m2288665044(L_33, L_34, /*hidden argument*/NULL);
		V_11 = L_35;
		DelaunayTriangle_t2835103587 * L_36 = ___t0;
		TriangulationPoint_t3810082933 * L_37 = ___p1;
		TriangulationPoint_t3810082933 * L_38 = ___op3;
		NullCheck(L_36);
		DelaunayTriangle_Legalize_m2367693307(L_36, L_37, L_38, /*hidden argument*/NULL);
		DelaunayTriangle_t2835103587 * L_39 = ___ot2;
		TriangulationPoint_t3810082933 * L_40 = ___op3;
		TriangulationPoint_t3810082933 * L_41 = ___p1;
		NullCheck(L_39);
		DelaunayTriangle_Legalize_m2367693307(L_39, L_40, L_41, /*hidden argument*/NULL);
		DelaunayTriangle_t2835103587 * L_42 = ___ot2;
		TriangulationPoint_t3810082933 * L_43 = ___p1;
		bool L_44 = V_8;
		NullCheck(L_42);
		DelaunayTriangle_SetDelaunayEdgeCCW_m1549623684(L_42, L_43, L_44, /*hidden argument*/NULL);
		DelaunayTriangle_t2835103587 * L_45 = ___t0;
		TriangulationPoint_t3810082933 * L_46 = ___p1;
		bool L_47 = V_9;
		NullCheck(L_45);
		DelaunayTriangle_SetDelaunayEdgeCW_m3698077983(L_45, L_46, L_47, /*hidden argument*/NULL);
		DelaunayTriangle_t2835103587 * L_48 = ___t0;
		TriangulationPoint_t3810082933 * L_49 = ___op3;
		bool L_50 = V_10;
		NullCheck(L_48);
		DelaunayTriangle_SetDelaunayEdgeCCW_m1549623684(L_48, L_49, L_50, /*hidden argument*/NULL);
		DelaunayTriangle_t2835103587 * L_51 = ___ot2;
		TriangulationPoint_t3810082933 * L_52 = ___op3;
		bool L_53 = V_11;
		NullCheck(L_51);
		DelaunayTriangle_SetDelaunayEdgeCW_m3698077983(L_51, L_52, L_53, /*hidden argument*/NULL);
		DelaunayTriangle_t2835103587 * L_54 = ___ot2;
		TriangulationPoint_t3810082933 * L_55 = ___p1;
		bool L_56 = V_4;
		NullCheck(L_54);
		DelaunayTriangle_SetConstrainedEdgeCCW_m3838082667(L_54, L_55, L_56, /*hidden argument*/NULL);
		DelaunayTriangle_t2835103587 * L_57 = ___t0;
		TriangulationPoint_t3810082933 * L_58 = ___p1;
		bool L_59 = V_5;
		NullCheck(L_57);
		DelaunayTriangle_SetConstrainedEdgeCW_m3217709912(L_57, L_58, L_59, /*hidden argument*/NULL);
		DelaunayTriangle_t2835103587 * L_60 = ___t0;
		TriangulationPoint_t3810082933 * L_61 = ___op3;
		bool L_62 = V_6;
		NullCheck(L_60);
		DelaunayTriangle_SetConstrainedEdgeCCW_m3838082667(L_60, L_61, L_62, /*hidden argument*/NULL);
		DelaunayTriangle_t2835103587 * L_63 = ___ot2;
		TriangulationPoint_t3810082933 * L_64 = ___op3;
		bool L_65 = V_7;
		NullCheck(L_63);
		DelaunayTriangle_SetConstrainedEdgeCW_m3217709912(L_63, L_64, L_65, /*hidden argument*/NULL);
		DelaunayTriangle_t2835103587 * L_66 = ___t0;
		NullCheck(L_66);
		FixedArray3_1_t3485577237 * L_67 = L_66->get_address_of_Neighbors_1();
		FixedArray3_1_Clear_m3990085865(L_67, /*hidden argument*/FixedArray3_1_Clear_m3990085865_MethodInfo_var);
		DelaunayTriangle_t2835103587 * L_68 = ___ot2;
		NullCheck(L_68);
		FixedArray3_1_t3485577237 * L_69 = L_68->get_address_of_Neighbors_1();
		FixedArray3_1_Clear_m3990085865(L_69, /*hidden argument*/FixedArray3_1_Clear_m3990085865_MethodInfo_var);
		DelaunayTriangle_t2835103587 * L_70 = V_0;
		if (!L_70)
		{
			goto IL_00e3;
		}
	}
	{
		DelaunayTriangle_t2835103587 * L_71 = ___ot2;
		DelaunayTriangle_t2835103587 * L_72 = V_0;
		NullCheck(L_71);
		DelaunayTriangle_MarkNeighbor_m119719133(L_71, L_72, /*hidden argument*/NULL);
	}

IL_00e3:
	{
		DelaunayTriangle_t2835103587 * L_73 = V_1;
		if (!L_73)
		{
			goto IL_00f0;
		}
	}
	{
		DelaunayTriangle_t2835103587 * L_74 = ___t0;
		DelaunayTriangle_t2835103587 * L_75 = V_1;
		NullCheck(L_74);
		DelaunayTriangle_MarkNeighbor_m119719133(L_74, L_75, /*hidden argument*/NULL);
	}

IL_00f0:
	{
		DelaunayTriangle_t2835103587 * L_76 = V_2;
		if (!L_76)
		{
			goto IL_00fd;
		}
	}
	{
		DelaunayTriangle_t2835103587 * L_77 = ___t0;
		DelaunayTriangle_t2835103587 * L_78 = V_2;
		NullCheck(L_77);
		DelaunayTriangle_MarkNeighbor_m119719133(L_77, L_78, /*hidden argument*/NULL);
	}

IL_00fd:
	{
		DelaunayTriangle_t2835103587 * L_79 = V_3;
		if (!L_79)
		{
			goto IL_010a;
		}
	}
	{
		DelaunayTriangle_t2835103587 * L_80 = ___ot2;
		DelaunayTriangle_t2835103587 * L_81 = V_3;
		NullCheck(L_80);
		DelaunayTriangle_MarkNeighbor_m119719133(L_80, L_81, /*hidden argument*/NULL);
	}

IL_010a:
	{
		DelaunayTriangle_t2835103587 * L_82 = ___t0;
		DelaunayTriangle_t2835103587 * L_83 = ___ot2;
		NullCheck(L_82);
		DelaunayTriangle_MarkNeighbor_m119719133(L_82, L_83, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Pathfinding.Poly2Tri.DTSweepBasin::.ctor()
extern "C"  void DTSweepBasin__ctor_m328954478 (DTSweepBasin_t2452859313 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Pathfinding.Poly2Tri.DTSweepConstraint::.ctor(Pathfinding.Poly2Tri.TriangulationPoint,Pathfinding.Poly2Tri.TriangulationPoint)
extern "C"  void DTSweepConstraint__ctor_m259070556 (DTSweepConstraint_t3360279023 * __this, TriangulationPoint_t3810082933 * ___p10, TriangulationPoint_t3810082933 * ___p21, const MethodInfo* method)
{
	{
		TriangulationConstraint__ctor_m2519444399(__this, /*hidden argument*/NULL);
		TriangulationPoint_t3810082933 * L_0 = ___p10;
		((TriangulationConstraint_t137695394 *)__this)->set_P_0(L_0);
		TriangulationPoint_t3810082933 * L_1 = ___p21;
		((TriangulationConstraint_t137695394 *)__this)->set_Q_1(L_1);
		TriangulationPoint_t3810082933 * L_2 = ___p10;
		NullCheck(L_2);
		double L_3 = L_2->get_Y_1();
		TriangulationPoint_t3810082933 * L_4 = ___p21;
		NullCheck(L_4);
		double L_5 = L_4->get_Y_1();
		if ((!(((double)L_3) > ((double)L_5))))
		{
			goto IL_0038;
		}
	}
	{
		TriangulationPoint_t3810082933 * L_6 = ___p10;
		((TriangulationConstraint_t137695394 *)__this)->set_Q_1(L_6);
		TriangulationPoint_t3810082933 * L_7 = ___p21;
		((TriangulationConstraint_t137695394 *)__this)->set_P_0(L_7);
		goto IL_007e;
	}

IL_0038:
	{
		TriangulationPoint_t3810082933 * L_8 = ___p10;
		NullCheck(L_8);
		double L_9 = L_8->get_Y_1();
		TriangulationPoint_t3810082933 * L_10 = ___p21;
		NullCheck(L_10);
		double L_11 = L_10->get_Y_1();
		if ((!(((double)L_9) == ((double)L_11))))
		{
			goto IL_007e;
		}
	}
	{
		TriangulationPoint_t3810082933 * L_12 = ___p10;
		NullCheck(L_12);
		double L_13 = L_12->get_X_0();
		TriangulationPoint_t3810082933 * L_14 = ___p21;
		NullCheck(L_14);
		double L_15 = L_14->get_X_0();
		if ((!(((double)L_13) > ((double)L_15))))
		{
			goto IL_006d;
		}
	}
	{
		TriangulationPoint_t3810082933 * L_16 = ___p10;
		((TriangulationConstraint_t137695394 *)__this)->set_Q_1(L_16);
		TriangulationPoint_t3810082933 * L_17 = ___p21;
		((TriangulationConstraint_t137695394 *)__this)->set_P_0(L_17);
		goto IL_007e;
	}

IL_006d:
	{
		TriangulationPoint_t3810082933 * L_18 = ___p10;
		NullCheck(L_18);
		double L_19 = L_18->get_X_0();
		TriangulationPoint_t3810082933 * L_20 = ___p21;
		NullCheck(L_20);
		double L_21 = L_20->get_X_0();
		if ((!(((double)L_19) == ((double)L_21))))
		{
			goto IL_007e;
		}
	}

IL_007e:
	{
		TriangulationPoint_t3810082933 * L_22 = ((TriangulationConstraint_t137695394 *)__this)->get_Q_1();
		NullCheck(L_22);
		TriangulationPoint_AddEdge_m2282912300(L_22, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Pathfinding.Poly2Tri.DTSweepContext::.ctor()
extern Il2CppClass* DTSweepBasin_t2452859313_il2cpp_TypeInfo_var;
extern Il2CppClass* DTSweepEdgeEvent_t3441061589_il2cpp_TypeInfo_var;
extern Il2CppClass* DTSweepPointComparator_t3174805566_il2cpp_TypeInfo_var;
extern const uint32_t DTSweepContext__ctor_m606266552_MetadataUsageId;
extern "C"  void DTSweepContext__ctor_m606266552 (DTSweepContext_t543731303 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DTSweepContext__ctor_m606266552_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_ALPHA_7((0.3f));
		DTSweepBasin_t2452859313 * L_0 = (DTSweepBasin_t2452859313 *)il2cpp_codegen_object_new(DTSweepBasin_t2452859313_il2cpp_TypeInfo_var);
		DTSweepBasin__ctor_m328954478(L_0, /*hidden argument*/NULL);
		__this->set_Basin_9(L_0);
		DTSweepEdgeEvent_t3441061589 * L_1 = (DTSweepEdgeEvent_t3441061589 *)il2cpp_codegen_object_new(DTSweepEdgeEvent_t3441061589_il2cpp_TypeInfo_var);
		DTSweepEdgeEvent__ctor_m434092618(L_1, /*hidden argument*/NULL);
		__this->set_EdgeEvent_10(L_1);
		DTSweepPointComparator_t3174805566 * L_2 = (DTSweepPointComparator_t3174805566 *)il2cpp_codegen_object_new(DTSweepPointComparator_t3174805566_il2cpp_TypeInfo_var);
		DTSweepPointComparator__ctor_m444435905(L_2, /*hidden argument*/NULL);
		__this->set__comparator_11(L_2);
		TriangulationContext__ctor_m4137179115(__this, /*hidden argument*/NULL);
		VirtActionInvoker0::Invoke(7 /* System.Void Pathfinding.Poly2Tri.TriangulationContext::Clear() */, __this);
		return;
	}
}
// Pathfinding.Poly2Tri.TriangulationPoint Pathfinding.Poly2Tri.DTSweepContext::get_Head()
extern "C"  TriangulationPoint_t3810082933 * DTSweepContext_get_Head_m84775074 (DTSweepContext_t543731303 * __this, const MethodInfo* method)
{
	{
		TriangulationPoint_t3810082933 * L_0 = __this->get_U3CHeadU3Ek__BackingField_12();
		return L_0;
	}
}
// System.Void Pathfinding.Poly2Tri.DTSweepContext::set_Head(Pathfinding.Poly2Tri.TriangulationPoint)
extern "C"  void DTSweepContext_set_Head_m3637117257 (DTSweepContext_t543731303 * __this, TriangulationPoint_t3810082933 * ___value0, const MethodInfo* method)
{
	{
		TriangulationPoint_t3810082933 * L_0 = ___value0;
		__this->set_U3CHeadU3Ek__BackingField_12(L_0);
		return;
	}
}
// Pathfinding.Poly2Tri.TriangulationPoint Pathfinding.Poly2Tri.DTSweepContext::get_Tail()
extern "C"  TriangulationPoint_t3810082933 * DTSweepContext_get_Tail_m424876818 (DTSweepContext_t543731303 * __this, const MethodInfo* method)
{
	{
		TriangulationPoint_t3810082933 * L_0 = __this->get_U3CTailU3Ek__BackingField_13();
		return L_0;
	}
}
// System.Void Pathfinding.Poly2Tri.DTSweepContext::set_Tail(Pathfinding.Poly2Tri.TriangulationPoint)
extern "C"  void DTSweepContext_set_Tail_m1909889241 (DTSweepContext_t543731303 * __this, TriangulationPoint_t3810082933 * ___value0, const MethodInfo* method)
{
	{
		TriangulationPoint_t3810082933 * L_0 = ___value0;
		__this->set_U3CTailU3Ek__BackingField_13(L_0);
		return;
	}
}
// System.Boolean Pathfinding.Poly2Tri.DTSweepContext::get_IsDebugEnabled()
extern "C"  bool DTSweepContext_get_IsDebugEnabled_m2721378423 (DTSweepContext_t543731303 * __this, const MethodInfo* method)
{
	{
		bool L_0 = TriangulationContext_get_IsDebugEnabled_m1815371940(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void Pathfinding.Poly2Tri.DTSweepContext::RemoveFromList(Pathfinding.Poly2Tri.DelaunayTriangle)
extern const MethodInfo* List_1_Remove_m3287549537_MethodInfo_var;
extern const uint32_t DTSweepContext_RemoveFromList_m3222250764_MetadataUsageId;
extern "C"  void DTSweepContext_RemoveFromList_m3222250764 (DTSweepContext_t543731303 * __this, DelaunayTriangle_t2835103587 * ___triangle0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DTSweepContext_RemoveFromList_m3222250764_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t4203289139 * L_0 = ((TriangulationContext_t3528662164 *)__this)->get_Triangles_0();
		DelaunayTriangle_t2835103587 * L_1 = ___triangle0;
		NullCheck(L_0);
		List_1_Remove_m3287549537(L_0, L_1, /*hidden argument*/List_1_Remove_m3287549537_MethodInfo_var);
		return;
	}
}
// System.Void Pathfinding.Poly2Tri.DTSweepContext::MeshClean(Pathfinding.Poly2Tri.DelaunayTriangle)
extern "C"  void DTSweepContext_MeshClean_m3777320306 (DTSweepContext_t543731303 * __this, DelaunayTriangle_t2835103587 * ___triangle0, const MethodInfo* method)
{
	{
		DelaunayTriangle_t2835103587 * L_0 = ___triangle0;
		DTSweepContext_MeshCleanReq_m3751789302(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Pathfinding.Poly2Tri.DTSweepContext::MeshCleanReq(Pathfinding.Poly2Tri.DelaunayTriangle)
extern Il2CppClass* Triangulatable_t923279207_il2cpp_TypeInfo_var;
extern const MethodInfo* FixedArray3_1_get_Item_m4227602836_MethodInfo_var;
extern const uint32_t DTSweepContext_MeshCleanReq_m3751789302_MetadataUsageId;
extern "C"  void DTSweepContext_MeshCleanReq_m3751789302 (DTSweepContext_t543731303 * __this, DelaunayTriangle_t2835103587 * ___triangle0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DTSweepContext_MeshCleanReq_m3751789302_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		DelaunayTriangle_t2835103587 * L_0 = ___triangle0;
		if (!L_0)
		{
			goto IL_0059;
		}
	}
	{
		DelaunayTriangle_t2835103587 * L_1 = ___triangle0;
		NullCheck(L_1);
		bool L_2 = DelaunayTriangle_get_IsInterior_m1627348341(L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0059;
		}
	}
	{
		DelaunayTriangle_t2835103587 * L_3 = ___triangle0;
		NullCheck(L_3);
		DelaunayTriangle_set_IsInterior_m1507960310(L_3, (bool)1, /*hidden argument*/NULL);
		Il2CppObject * L_4 = TriangulationContext_get_Triangulatable_m1563965268(__this, /*hidden argument*/NULL);
		DelaunayTriangle_t2835103587 * L_5 = ___triangle0;
		NullCheck(L_4);
		InterfaceActionInvoker1< DelaunayTriangle_t2835103587 * >::Invoke(1 /* System.Void Pathfinding.Poly2Tri.Triangulatable::AddTriangle(Pathfinding.Poly2Tri.DelaunayTriangle) */, Triangulatable_t923279207_il2cpp_TypeInfo_var, L_4, L_5);
		V_0 = 0;
		goto IL_0052;
	}

IL_002b:
	{
		DelaunayTriangle_t2835103587 * L_6 = ___triangle0;
		NullCheck(L_6);
		FixedBitArray3_t3665369095 * L_7 = L_6->get_address_of_EdgeIsConstrained_2();
		int32_t L_8 = V_0;
		bool L_9 = FixedBitArray3_get_Item_m141500035(L_7, L_8, /*hidden argument*/NULL);
		if (L_9)
		{
			goto IL_004e;
		}
	}
	{
		DelaunayTriangle_t2835103587 * L_10 = ___triangle0;
		NullCheck(L_10);
		FixedArray3_1_t3485577237 * L_11 = L_10->get_address_of_Neighbors_1();
		int32_t L_12 = V_0;
		DelaunayTriangle_t2835103587 * L_13 = FixedArray3_1_get_Item_m4227602836(L_11, L_12, /*hidden argument*/FixedArray3_1_get_Item_m4227602836_MethodInfo_var);
		DTSweepContext_MeshCleanReq_m3751789302(__this, L_13, /*hidden argument*/NULL);
	}

IL_004e:
	{
		int32_t L_14 = V_0;
		V_0 = ((int32_t)((int32_t)L_14+(int32_t)1));
	}

IL_0052:
	{
		int32_t L_15 = V_0;
		if ((((int32_t)L_15) < ((int32_t)3)))
		{
			goto IL_002b;
		}
	}

IL_0059:
	{
		return;
	}
}
// System.Void Pathfinding.Poly2Tri.DTSweepContext::Clear()
extern const MethodInfo* List_1_Clear_m4002106393_MethodInfo_var;
extern const uint32_t DTSweepContext_Clear_m2307367139_MetadataUsageId;
extern "C"  void DTSweepContext_Clear_m2307367139 (DTSweepContext_t543731303 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DTSweepContext_Clear_m2307367139_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		TriangulationContext_Clear_m1543312406(__this, /*hidden argument*/NULL);
		List_1_t4203289139 * L_0 = ((TriangulationContext_t3528662164 *)__this)->get_Triangles_0();
		NullCheck(L_0);
		List_1_Clear_m4002106393(L_0, /*hidden argument*/List_1_Clear_m4002106393_MethodInfo_var);
		return;
	}
}
// System.Void Pathfinding.Poly2Tri.DTSweepContext::AddNode(Pathfinding.Poly2Tri.AdvancingFrontNode)
extern "C"  void DTSweepContext_AddNode_m3594313838 (DTSweepContext_t543731303 * __this, AdvancingFrontNode_t688967424 * ___node0, const MethodInfo* method)
{
	{
		AdvancingFront_t240556382 * L_0 = __this->get_Front_8();
		AdvancingFrontNode_t688967424 * L_1 = ___node0;
		NullCheck(L_0);
		AdvancingFront_AddNode_m1666580389(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Pathfinding.Poly2Tri.DTSweepContext::RemoveNode(Pathfinding.Poly2Tri.AdvancingFrontNode)
extern "C"  void DTSweepContext_RemoveNode_m2652831733 (DTSweepContext_t543731303 * __this, AdvancingFrontNode_t688967424 * ___node0, const MethodInfo* method)
{
	{
		AdvancingFront_t240556382 * L_0 = __this->get_Front_8();
		AdvancingFrontNode_t688967424 * L_1 = ___node0;
		NullCheck(L_0);
		AdvancingFront_RemoveNode_m1553367390(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// Pathfinding.Poly2Tri.AdvancingFrontNode Pathfinding.Poly2Tri.DTSweepContext::LocateNode(Pathfinding.Poly2Tri.TriangulationPoint)
extern "C"  AdvancingFrontNode_t688967424 * DTSweepContext_LocateNode_m451156442 (DTSweepContext_t543731303 * __this, TriangulationPoint_t3810082933 * ___point0, const MethodInfo* method)
{
	{
		AdvancingFront_t240556382 * L_0 = __this->get_Front_8();
		TriangulationPoint_t3810082933 * L_1 = ___point0;
		NullCheck(L_0);
		AdvancingFrontNode_t688967424 * L_2 = AdvancingFront_LocateNode_m3646659395(L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void Pathfinding.Poly2Tri.DTSweepContext::CreateAdvancingFront()
extern Il2CppClass* DelaunayTriangle_t2835103587_il2cpp_TypeInfo_var;
extern Il2CppClass* AdvancingFrontNode_t688967424_il2cpp_TypeInfo_var;
extern Il2CppClass* AdvancingFront_t240556382_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Item_m2083438529_MethodInfo_var;
extern const MethodInfo* List_1_Add_m1995366622_MethodInfo_var;
extern const MethodInfo* FixedArray3_1_get_Item_m7701314_MethodInfo_var;
extern const uint32_t DTSweepContext_CreateAdvancingFront_m616900306_MetadataUsageId;
extern "C"  void DTSweepContext_CreateAdvancingFront_m616900306 (DTSweepContext_t543731303 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DTSweepContext_CreateAdvancingFront_m616900306_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	AdvancingFrontNode_t688967424 * V_0 = NULL;
	AdvancingFrontNode_t688967424 * V_1 = NULL;
	AdvancingFrontNode_t688967424 * V_2 = NULL;
	DelaunayTriangle_t2835103587 * V_3 = NULL;
	{
		List_1_t883301189 * L_0 = ((TriangulationContext_t3528662164 *)__this)->get_Points_1();
		NullCheck(L_0);
		TriangulationPoint_t3810082933 * L_1 = List_1_get_Item_m2083438529(L_0, 0, /*hidden argument*/List_1_get_Item_m2083438529_MethodInfo_var);
		TriangulationPoint_t3810082933 * L_2 = DTSweepContext_get_Tail_m424876818(__this, /*hidden argument*/NULL);
		TriangulationPoint_t3810082933 * L_3 = DTSweepContext_get_Head_m84775074(__this, /*hidden argument*/NULL);
		DelaunayTriangle_t2835103587 * L_4 = (DelaunayTriangle_t2835103587 *)il2cpp_codegen_object_new(DelaunayTriangle_t2835103587_il2cpp_TypeInfo_var);
		DelaunayTriangle__ctor_m1610534382(L_4, L_1, L_2, L_3, /*hidden argument*/NULL);
		V_3 = L_4;
		List_1_t4203289139 * L_5 = ((TriangulationContext_t3528662164 *)__this)->get_Triangles_0();
		DelaunayTriangle_t2835103587 * L_6 = V_3;
		NullCheck(L_5);
		List_1_Add_m1995366622(L_5, L_6, /*hidden argument*/List_1_Add_m1995366622_MethodInfo_var);
		DelaunayTriangle_t2835103587 * L_7 = V_3;
		NullCheck(L_7);
		FixedArray3_1_t165589287 * L_8 = L_7->get_address_of_Points_0();
		TriangulationPoint_t3810082933 * L_9 = FixedArray3_1_get_Item_m7701314(L_8, 1, /*hidden argument*/FixedArray3_1_get_Item_m7701314_MethodInfo_var);
		AdvancingFrontNode_t688967424 * L_10 = (AdvancingFrontNode_t688967424 *)il2cpp_codegen_object_new(AdvancingFrontNode_t688967424_il2cpp_TypeInfo_var);
		AdvancingFrontNode__ctor_m294490163(L_10, L_9, /*hidden argument*/NULL);
		V_0 = L_10;
		AdvancingFrontNode_t688967424 * L_11 = V_0;
		DelaunayTriangle_t2835103587 * L_12 = V_3;
		NullCheck(L_11);
		L_11->set_Triangle_4(L_12);
		DelaunayTriangle_t2835103587 * L_13 = V_3;
		NullCheck(L_13);
		FixedArray3_1_t165589287 * L_14 = L_13->get_address_of_Points_0();
		TriangulationPoint_t3810082933 * L_15 = FixedArray3_1_get_Item_m7701314(L_14, 0, /*hidden argument*/FixedArray3_1_get_Item_m7701314_MethodInfo_var);
		AdvancingFrontNode_t688967424 * L_16 = (AdvancingFrontNode_t688967424 *)il2cpp_codegen_object_new(AdvancingFrontNode_t688967424_il2cpp_TypeInfo_var);
		AdvancingFrontNode__ctor_m294490163(L_16, L_15, /*hidden argument*/NULL);
		V_2 = L_16;
		AdvancingFrontNode_t688967424 * L_17 = V_2;
		DelaunayTriangle_t2835103587 * L_18 = V_3;
		NullCheck(L_17);
		L_17->set_Triangle_4(L_18);
		DelaunayTriangle_t2835103587 * L_19 = V_3;
		NullCheck(L_19);
		FixedArray3_1_t165589287 * L_20 = L_19->get_address_of_Points_0();
		TriangulationPoint_t3810082933 * L_21 = FixedArray3_1_get_Item_m7701314(L_20, 2, /*hidden argument*/FixedArray3_1_get_Item_m7701314_MethodInfo_var);
		AdvancingFrontNode_t688967424 * L_22 = (AdvancingFrontNode_t688967424 *)il2cpp_codegen_object_new(AdvancingFrontNode_t688967424_il2cpp_TypeInfo_var);
		AdvancingFrontNode__ctor_m294490163(L_22, L_21, /*hidden argument*/NULL);
		V_1 = L_22;
		AdvancingFrontNode_t688967424 * L_23 = V_0;
		AdvancingFrontNode_t688967424 * L_24 = V_1;
		AdvancingFront_t240556382 * L_25 = (AdvancingFront_t240556382 *)il2cpp_codegen_object_new(AdvancingFront_t240556382_il2cpp_TypeInfo_var);
		AdvancingFront__ctor_m2136454471(L_25, L_23, L_24, /*hidden argument*/NULL);
		__this->set_Front_8(L_25);
		AdvancingFront_t240556382 * L_26 = __this->get_Front_8();
		AdvancingFrontNode_t688967424 * L_27 = V_2;
		NullCheck(L_26);
		AdvancingFront_AddNode_m1666580389(L_26, L_27, /*hidden argument*/NULL);
		AdvancingFront_t240556382 * L_28 = __this->get_Front_8();
		NullCheck(L_28);
		AdvancingFrontNode_t688967424 * L_29 = L_28->get_Head_0();
		AdvancingFrontNode_t688967424 * L_30 = V_2;
		NullCheck(L_29);
		L_29->set_Next_0(L_30);
		AdvancingFrontNode_t688967424 * L_31 = V_2;
		AdvancingFront_t240556382 * L_32 = __this->get_Front_8();
		NullCheck(L_32);
		AdvancingFrontNode_t688967424 * L_33 = L_32->get_Tail_1();
		NullCheck(L_31);
		L_31->set_Next_0(L_33);
		AdvancingFrontNode_t688967424 * L_34 = V_2;
		AdvancingFront_t240556382 * L_35 = __this->get_Front_8();
		NullCheck(L_35);
		AdvancingFrontNode_t688967424 * L_36 = L_35->get_Head_0();
		NullCheck(L_34);
		L_34->set_Prev_1(L_36);
		AdvancingFront_t240556382 * L_37 = __this->get_Front_8();
		NullCheck(L_37);
		AdvancingFrontNode_t688967424 * L_38 = L_37->get_Tail_1();
		AdvancingFrontNode_t688967424 * L_39 = V_2;
		NullCheck(L_38);
		L_38->set_Prev_1(L_39);
		return;
	}
}
// System.Void Pathfinding.Poly2Tri.DTSweepContext::MapTriangleToNodes(Pathfinding.Poly2Tri.DelaunayTriangle)
extern const MethodInfo* FixedArray3_1_get_Item_m4227602836_MethodInfo_var;
extern const MethodInfo* FixedArray3_1_get_Item_m7701314_MethodInfo_var;
extern const uint32_t DTSweepContext_MapTriangleToNodes_m727745286_MetadataUsageId;
extern "C"  void DTSweepContext_MapTriangleToNodes_m727745286 (DTSweepContext_t543731303 * __this, DelaunayTriangle_t2835103587 * ___t0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DTSweepContext_MapTriangleToNodes_m727745286_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	AdvancingFrontNode_t688967424 * V_1 = NULL;
	{
		V_0 = 0;
		goto IL_0047;
	}

IL_0007:
	{
		DelaunayTriangle_t2835103587 * L_0 = ___t0;
		NullCheck(L_0);
		FixedArray3_1_t3485577237 * L_1 = L_0->get_address_of_Neighbors_1();
		int32_t L_2 = V_0;
		DelaunayTriangle_t2835103587 * L_3 = FixedArray3_1_get_Item_m4227602836(L_1, L_2, /*hidden argument*/FixedArray3_1_get_Item_m4227602836_MethodInfo_var);
		if (L_3)
		{
			goto IL_0043;
		}
	}
	{
		AdvancingFront_t240556382 * L_4 = __this->get_Front_8();
		DelaunayTriangle_t2835103587 * L_5 = ___t0;
		DelaunayTriangle_t2835103587 * L_6 = ___t0;
		NullCheck(L_6);
		FixedArray3_1_t165589287 * L_7 = L_6->get_address_of_Points_0();
		int32_t L_8 = V_0;
		TriangulationPoint_t3810082933 * L_9 = FixedArray3_1_get_Item_m7701314(L_7, L_8, /*hidden argument*/FixedArray3_1_get_Item_m7701314_MethodInfo_var);
		NullCheck(L_5);
		TriangulationPoint_t3810082933 * L_10 = DelaunayTriangle_PointCWFrom_m3064891095(L_5, L_9, /*hidden argument*/NULL);
		NullCheck(L_4);
		AdvancingFrontNode_t688967424 * L_11 = AdvancingFront_LocatePoint_m1012370253(L_4, L_10, /*hidden argument*/NULL);
		V_1 = L_11;
		AdvancingFrontNode_t688967424 * L_12 = V_1;
		if (!L_12)
		{
			goto IL_0043;
		}
	}
	{
		AdvancingFrontNode_t688967424 * L_13 = V_1;
		DelaunayTriangle_t2835103587 * L_14 = ___t0;
		NullCheck(L_13);
		L_13->set_Triangle_4(L_14);
	}

IL_0043:
	{
		int32_t L_15 = V_0;
		V_0 = ((int32_t)((int32_t)L_15+(int32_t)1));
	}

IL_0047:
	{
		int32_t L_16 = V_0;
		if ((((int32_t)L_16) < ((int32_t)3)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void Pathfinding.Poly2Tri.DTSweepContext::PrepareTriangulation(Pathfinding.Poly2Tri.Triangulatable)
extern Il2CppClass* TriangulationPoint_t3810082933_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Item_m2083438529_MethodInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m2487349601_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m1146135171_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m2107822445_MethodInfo_var;
extern const MethodInfo* Enumerator_Dispose_m3085523116_MethodInfo_var;
extern const MethodInfo* List_1_Sort_m1125338871_MethodInfo_var;
extern const uint32_t DTSweepContext_PrepareTriangulation_m2503426508_MetadataUsageId;
extern "C"  void DTSweepContext_PrepareTriangulation_m2503426508 (DTSweepContext_t543731303 * __this, Il2CppObject * ___t0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DTSweepContext_PrepareTriangulation_m2503426508_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	double V_0 = 0.0;
	double V_1 = 0.0;
	double V_2 = 0.0;
	double V_3 = 0.0;
	Enumerator_t902973959  V_4;
	memset(&V_4, 0, sizeof(V_4));
	TriangulationPoint_t3810082933 * V_5 = NULL;
	double V_6 = 0.0;
	double V_7 = 0.0;
	TriangulationPoint_t3810082933 * V_8 = NULL;
	TriangulationPoint_t3810082933 * V_9 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = ___t0;
		TriangulationContext_PrepareTriangulation_m1937826367(__this, L_0, /*hidden argument*/NULL);
		List_1_t883301189 * L_1 = ((TriangulationContext_t3528662164 *)__this)->get_Points_1();
		NullCheck(L_1);
		TriangulationPoint_t3810082933 * L_2 = List_1_get_Item_m2083438529(L_1, 0, /*hidden argument*/List_1_get_Item_m2083438529_MethodInfo_var);
		NullCheck(L_2);
		double L_3 = L_2->get_X_0();
		double L_4 = L_3;
		V_1 = L_4;
		V_0 = L_4;
		List_1_t883301189 * L_5 = ((TriangulationContext_t3528662164 *)__this)->get_Points_1();
		NullCheck(L_5);
		TriangulationPoint_t3810082933 * L_6 = List_1_get_Item_m2083438529(L_5, 0, /*hidden argument*/List_1_get_Item_m2083438529_MethodInfo_var);
		NullCheck(L_6);
		double L_7 = L_6->get_Y_1();
		double L_8 = L_7;
		V_3 = L_8;
		V_2 = L_8;
		List_1_t883301189 * L_9 = ((TriangulationContext_t3528662164 *)__this)->get_Points_1();
		NullCheck(L_9);
		Enumerator_t902973959  L_10 = List_1_GetEnumerator_m2487349601(L_9, /*hidden argument*/List_1_GetEnumerator_m2487349601_MethodInfo_var);
		V_4 = L_10;
	}

IL_003c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_009e;
		}

IL_0041:
		{
			TriangulationPoint_t3810082933 * L_11 = Enumerator_get_Current_m1146135171((&V_4), /*hidden argument*/Enumerator_get_Current_m1146135171_MethodInfo_var);
			V_5 = L_11;
			TriangulationPoint_t3810082933 * L_12 = V_5;
			NullCheck(L_12);
			double L_13 = L_12->get_X_0();
			double L_14 = V_0;
			if ((!(((double)L_13) > ((double)L_14))))
			{
				goto IL_005f;
			}
		}

IL_0057:
		{
			TriangulationPoint_t3810082933 * L_15 = V_5;
			NullCheck(L_15);
			double L_16 = L_15->get_X_0();
			V_0 = L_16;
		}

IL_005f:
		{
			TriangulationPoint_t3810082933 * L_17 = V_5;
			NullCheck(L_17);
			double L_18 = L_17->get_X_0();
			double L_19 = V_1;
			if ((!(((double)L_18) < ((double)L_19))))
			{
				goto IL_0074;
			}
		}

IL_006c:
		{
			TriangulationPoint_t3810082933 * L_20 = V_5;
			NullCheck(L_20);
			double L_21 = L_20->get_X_0();
			V_1 = L_21;
		}

IL_0074:
		{
			TriangulationPoint_t3810082933 * L_22 = V_5;
			NullCheck(L_22);
			double L_23 = L_22->get_Y_1();
			double L_24 = V_2;
			if ((!(((double)L_23) > ((double)L_24))))
			{
				goto IL_0089;
			}
		}

IL_0081:
		{
			TriangulationPoint_t3810082933 * L_25 = V_5;
			NullCheck(L_25);
			double L_26 = L_25->get_Y_1();
			V_2 = L_26;
		}

IL_0089:
		{
			TriangulationPoint_t3810082933 * L_27 = V_5;
			NullCheck(L_27);
			double L_28 = L_27->get_Y_1();
			double L_29 = V_3;
			if ((!(((double)L_28) < ((double)L_29))))
			{
				goto IL_009e;
			}
		}

IL_0096:
		{
			TriangulationPoint_t3810082933 * L_30 = V_5;
			NullCheck(L_30);
			double L_31 = L_30->get_Y_1();
			V_3 = L_31;
		}

IL_009e:
		{
			bool L_32 = Enumerator_MoveNext_m2107822445((&V_4), /*hidden argument*/Enumerator_MoveNext_m2107822445_MethodInfo_var);
			if (L_32)
			{
				goto IL_0041;
			}
		}

IL_00aa:
		{
			IL2CPP_LEAVE(0xBD, FINALLY_00af);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_00af;
	}

FINALLY_00af:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m3085523116((&V_4), /*hidden argument*/Enumerator_Dispose_m3085523116_MethodInfo_var);
		IL2CPP_END_FINALLY(175)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(175)
	{
		IL2CPP_JUMP_TBL(0xBD, IL_00bd)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_00bd:
	{
		float L_33 = __this->get_ALPHA_7();
		double L_34 = V_0;
		double L_35 = V_1;
		V_6 = ((double)((double)(((double)((double)L_33)))*(double)((double)((double)L_34-(double)L_35))));
		float L_36 = __this->get_ALPHA_7();
		double L_37 = V_2;
		double L_38 = V_3;
		V_7 = ((double)((double)(((double)((double)L_36)))*(double)((double)((double)L_37-(double)L_38))));
		double L_39 = V_0;
		double L_40 = V_6;
		double L_41 = V_3;
		double L_42 = V_7;
		TriangulationPoint_t3810082933 * L_43 = (TriangulationPoint_t3810082933 *)il2cpp_codegen_object_new(TriangulationPoint_t3810082933_il2cpp_TypeInfo_var);
		TriangulationPoint__ctor_m1454340884(L_43, ((double)((double)L_39+(double)L_40)), ((double)((double)L_41-(double)L_42)), /*hidden argument*/NULL);
		V_8 = L_43;
		double L_44 = V_1;
		double L_45 = V_6;
		double L_46 = V_3;
		double L_47 = V_7;
		TriangulationPoint_t3810082933 * L_48 = (TriangulationPoint_t3810082933 *)il2cpp_codegen_object_new(TriangulationPoint_t3810082933_il2cpp_TypeInfo_var);
		TriangulationPoint__ctor_m1454340884(L_48, ((double)((double)L_44-(double)L_45)), ((double)((double)L_46-(double)L_47)), /*hidden argument*/NULL);
		V_9 = L_48;
		TriangulationPoint_t3810082933 * L_49 = V_8;
		DTSweepContext_set_Head_m3637117257(__this, L_49, /*hidden argument*/NULL);
		TriangulationPoint_t3810082933 * L_50 = V_9;
		DTSweepContext_set_Tail_m1909889241(__this, L_50, /*hidden argument*/NULL);
		List_1_t883301189 * L_51 = ((TriangulationContext_t3528662164 *)__this)->get_Points_1();
		DTSweepPointComparator_t3174805566 * L_52 = __this->get__comparator_11();
		NullCheck(L_51);
		List_1_Sort_m1125338871(L_51, L_52, /*hidden argument*/List_1_Sort_m1125338871_MethodInfo_var);
		return;
	}
}
// System.Void Pathfinding.Poly2Tri.DTSweepContext::FinalizeTriangulation()
extern Il2CppClass* Triangulatable_t923279207_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_Clear_m4002106393_MethodInfo_var;
extern const uint32_t DTSweepContext_FinalizeTriangulation_m615836583_MetadataUsageId;
extern "C"  void DTSweepContext_FinalizeTriangulation_m615836583 (DTSweepContext_t543731303 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DTSweepContext_FinalizeTriangulation_m615836583_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = TriangulationContext_get_Triangulatable_m1563965268(__this, /*hidden argument*/NULL);
		List_1_t4203289139 * L_1 = ((TriangulationContext_t3528662164 *)__this)->get_Triangles_0();
		NullCheck(L_0);
		InterfaceActionInvoker1< Il2CppObject* >::Invoke(2 /* System.Void Pathfinding.Poly2Tri.Triangulatable::AddTriangles(System.Collections.Generic.IEnumerable`1<Pathfinding.Poly2Tri.DelaunayTriangle>) */, Triangulatable_t923279207_il2cpp_TypeInfo_var, L_0, L_1);
		List_1_t4203289139 * L_2 = ((TriangulationContext_t3528662164 *)__this)->get_Triangles_0();
		NullCheck(L_2);
		List_1_Clear_m4002106393(L_2, /*hidden argument*/List_1_Clear_m4002106393_MethodInfo_var);
		return;
	}
}
// Pathfinding.Poly2Tri.TriangulationConstraint Pathfinding.Poly2Tri.DTSweepContext::NewConstraint(Pathfinding.Poly2Tri.TriangulationPoint,Pathfinding.Poly2Tri.TriangulationPoint)
extern Il2CppClass* DTSweepConstraint_t3360279023_il2cpp_TypeInfo_var;
extern const uint32_t DTSweepContext_NewConstraint_m994251481_MetadataUsageId;
extern "C"  TriangulationConstraint_t137695394 * DTSweepContext_NewConstraint_m994251481 (DTSweepContext_t543731303 * __this, TriangulationPoint_t3810082933 * ___a0, TriangulationPoint_t3810082933 * ___b1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DTSweepContext_NewConstraint_m994251481_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		TriangulationPoint_t3810082933 * L_0 = ___a0;
		TriangulationPoint_t3810082933 * L_1 = ___b1;
		DTSweepConstraint_t3360279023 * L_2 = (DTSweepConstraint_t3360279023 *)il2cpp_codegen_object_new(DTSweepConstraint_t3360279023_il2cpp_TypeInfo_var);
		DTSweepConstraint__ctor_m259070556(L_2, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// Pathfinding.Poly2Tri.TriangulationAlgorithm Pathfinding.Poly2Tri.DTSweepContext::get_Algorithm()
extern "C"  int32_t DTSweepContext_get_Algorithm_m2488598032 (DTSweepContext_t543731303 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(0);
	}
}
// System.Void Pathfinding.Poly2Tri.DTSweepDebugContext::set_PrimaryTriangle(Pathfinding.Poly2Tri.DelaunayTriangle)
extern Il2CppCodeGenString* _stringLiteral416777804;
extern const uint32_t DTSweepDebugContext_set_PrimaryTriangle_m527373270_MetadataUsageId;
extern "C"  void DTSweepDebugContext_set_PrimaryTriangle_m527373270 (DTSweepDebugContext_t3829537966 * __this, DelaunayTriangle_t2835103587 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DTSweepDebugContext_set_PrimaryTriangle_m527373270_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		DelaunayTriangle_t2835103587 * L_0 = ___value0;
		__this->set__primaryTriangle_1(L_0);
		TriangulationContext_t3528662164 * L_1 = ((TriangulationDebugContext_t3598090913 *)__this)->get__tcx_0();
		NullCheck(L_1);
		TriangulationContext_Update_m3089402304(L_1, _stringLiteral416777804, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Pathfinding.Poly2Tri.DTSweepDebugContext::set_SecondaryTriangle(Pathfinding.Poly2Tri.DelaunayTriangle)
extern Il2CppCodeGenString* _stringLiteral3324696382;
extern const uint32_t DTSweepDebugContext_set_SecondaryTriangle_m1903864548_MetadataUsageId;
extern "C"  void DTSweepDebugContext_set_SecondaryTriangle_m1903864548 (DTSweepDebugContext_t3829537966 * __this, DelaunayTriangle_t2835103587 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DTSweepDebugContext_set_SecondaryTriangle_m1903864548_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		DelaunayTriangle_t2835103587 * L_0 = ___value0;
		__this->set__secondaryTriangle_2(L_0);
		TriangulationContext_t3528662164 * L_1 = ((TriangulationDebugContext_t3598090913 *)__this)->get__tcx_0();
		NullCheck(L_1);
		TriangulationContext_Update_m3089402304(L_1, _stringLiteral3324696382, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Pathfinding.Poly2Tri.DTSweepDebugContext::set_ActivePoint(Pathfinding.Poly2Tri.TriangulationPoint)
extern Il2CppCodeGenString* _stringLiteral2811160396;
extern const uint32_t DTSweepDebugContext_set_ActivePoint_m1406306500_MetadataUsageId;
extern "C"  void DTSweepDebugContext_set_ActivePoint_m1406306500 (DTSweepDebugContext_t3829537966 * __this, TriangulationPoint_t3810082933 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DTSweepDebugContext_set_ActivePoint_m1406306500_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		TriangulationPoint_t3810082933 * L_0 = ___value0;
		__this->set__activePoint_3(L_0);
		TriangulationContext_t3528662164 * L_1 = ((TriangulationDebugContext_t3598090913 *)__this)->get__tcx_0();
		NullCheck(L_1);
		TriangulationContext_Update_m3089402304(L_1, _stringLiteral2811160396, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Pathfinding.Poly2Tri.DTSweepDebugContext::set_ActiveNode(Pathfinding.Poly2Tri.AdvancingFrontNode)
extern Il2CppCodeGenString* _stringLiteral2584474822;
extern const uint32_t DTSweepDebugContext_set_ActiveNode_m1532614081_MetadataUsageId;
extern "C"  void DTSweepDebugContext_set_ActiveNode_m1532614081 (DTSweepDebugContext_t3829537966 * __this, AdvancingFrontNode_t688967424 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DTSweepDebugContext_set_ActiveNode_m1532614081_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		AdvancingFrontNode_t688967424 * L_0 = ___value0;
		__this->set__activeNode_4(L_0);
		TriangulationContext_t3528662164 * L_1 = ((TriangulationDebugContext_t3598090913 *)__this)->get__tcx_0();
		NullCheck(L_1);
		TriangulationContext_Update_m3089402304(L_1, _stringLiteral2584474822, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Pathfinding.Poly2Tri.DTSweepDebugContext::set_ActiveConstraint(Pathfinding.Poly2Tri.DTSweepConstraint)
extern Il2CppCodeGenString* _stringLiteral1090009505;
extern const uint32_t DTSweepDebugContext_set_ActiveConstraint_m915019815_MetadataUsageId;
extern "C"  void DTSweepDebugContext_set_ActiveConstraint_m915019815 (DTSweepDebugContext_t3829537966 * __this, DTSweepConstraint_t3360279023 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DTSweepDebugContext_set_ActiveConstraint_m915019815_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		DTSweepConstraint_t3360279023 * L_0 = ___value0;
		__this->set__activeConstraint_5(L_0);
		TriangulationContext_t3528662164 * L_1 = ((TriangulationDebugContext_t3598090913 *)__this)->get__tcx_0();
		NullCheck(L_1);
		TriangulationContext_Update_m3089402304(L_1, _stringLiteral1090009505, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Pathfinding.Poly2Tri.DTSweepDebugContext::Clear()
extern "C"  void DTSweepDebugContext_Clear_m592559566 (DTSweepDebugContext_t3829537966 * __this, const MethodInfo* method)
{
	{
		DTSweepDebugContext_set_PrimaryTriangle_m527373270(__this, (DelaunayTriangle_t2835103587 *)NULL, /*hidden argument*/NULL);
		DTSweepDebugContext_set_SecondaryTriangle_m1903864548(__this, (DelaunayTriangle_t2835103587 *)NULL, /*hidden argument*/NULL);
		DTSweepDebugContext_set_ActivePoint_m1406306500(__this, (TriangulationPoint_t3810082933 *)NULL, /*hidden argument*/NULL);
		DTSweepDebugContext_set_ActiveNode_m1532614081(__this, (AdvancingFrontNode_t688967424 *)NULL, /*hidden argument*/NULL);
		DTSweepDebugContext_set_ActiveConstraint_m915019815(__this, (DTSweepConstraint_t3360279023 *)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Pathfinding.Poly2Tri.DTSweepEdgeEvent::.ctor()
extern "C"  void DTSweepEdgeEvent__ctor_m434092618 (DTSweepEdgeEvent_t3441061589 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Pathfinding.Poly2Tri.DTSweepPointComparator::.ctor()
extern "C"  void DTSweepPointComparator__ctor_m444435905 (DTSweepPointComparator_t3174805566 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 Pathfinding.Poly2Tri.DTSweepPointComparator::Compare(Pathfinding.Poly2Tri.TriangulationPoint,Pathfinding.Poly2Tri.TriangulationPoint)
extern "C"  int32_t DTSweepPointComparator_Compare_m3867896618 (DTSweepPointComparator_t3174805566 * __this, TriangulationPoint_t3810082933 * ___p10, TriangulationPoint_t3810082933 * ___p21, const MethodInfo* method)
{
	{
		TriangulationPoint_t3810082933 * L_0 = ___p10;
		NullCheck(L_0);
		double L_1 = L_0->get_Y_1();
		TriangulationPoint_t3810082933 * L_2 = ___p21;
		NullCheck(L_2);
		double L_3 = L_2->get_Y_1();
		if ((!(((double)L_1) < ((double)L_3))))
		{
			goto IL_0013;
		}
	}
	{
		return (-1);
	}

IL_0013:
	{
		TriangulationPoint_t3810082933 * L_4 = ___p10;
		NullCheck(L_4);
		double L_5 = L_4->get_Y_1();
		TriangulationPoint_t3810082933 * L_6 = ___p21;
		NullCheck(L_6);
		double L_7 = L_6->get_Y_1();
		if ((!(((double)L_5) > ((double)L_7))))
		{
			goto IL_0026;
		}
	}
	{
		return 1;
	}

IL_0026:
	{
		TriangulationPoint_t3810082933 * L_8 = ___p10;
		NullCheck(L_8);
		double L_9 = L_8->get_X_0();
		TriangulationPoint_t3810082933 * L_10 = ___p21;
		NullCheck(L_10);
		double L_11 = L_10->get_X_0();
		if ((!(((double)L_9) < ((double)L_11))))
		{
			goto IL_0039;
		}
	}
	{
		return (-1);
	}

IL_0039:
	{
		TriangulationPoint_t3810082933 * L_12 = ___p10;
		NullCheck(L_12);
		double L_13 = L_12->get_X_0();
		TriangulationPoint_t3810082933 * L_14 = ___p21;
		NullCheck(L_14);
		double L_15 = L_14->get_X_0();
		if ((!(((double)L_13) > ((double)L_15))))
		{
			goto IL_004c;
		}
	}
	{
		return 1;
	}

IL_004c:
	{
		return 0;
	}
}
// System.Collections.IEnumerator Pathfinding.Poly2Tri.FixedBitArray3::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * FixedBitArray3_System_Collections_IEnumerable_GetEnumerator_m2166890663 (FixedBitArray3_t3665369095 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = FixedBitArray3_GetEnumerator_m1379323559(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
extern "C"  Il2CppObject * FixedBitArray3_System_Collections_IEnumerable_GetEnumerator_m2166890663_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	FixedBitArray3_t3665369095 * _thisAdjusted = reinterpret_cast<FixedBitArray3_t3665369095 *>(__this + 1);
	return FixedBitArray3_System_Collections_IEnumerable_GetEnumerator_m2166890663(_thisAdjusted, method);
}
// System.Boolean Pathfinding.Poly2Tri.FixedBitArray3::get_Item(System.Int32)
extern Il2CppClass* IndexOutOfRangeException_t3456360697_il2cpp_TypeInfo_var;
extern const uint32_t FixedBitArray3_get_Item_m141500035_MetadataUsageId;
extern "C"  bool FixedBitArray3_get_Item_m141500035 (FixedBitArray3_t3665369095 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FixedBitArray3_get_Item_m141500035_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index0;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0019;
		}
		if (L_1 == 1)
		{
			goto IL_0020;
		}
		if (L_1 == 2)
		{
			goto IL_0027;
		}
	}
	{
		goto IL_002e;
	}

IL_0019:
	{
		bool L_2 = __this->get__0_0();
		return L_2;
	}

IL_0020:
	{
		bool L_3 = __this->get__1_1();
		return L_3;
	}

IL_0027:
	{
		bool L_4 = __this->get__2_2();
		return L_4;
	}

IL_002e:
	{
		IndexOutOfRangeException_t3456360697 * L_5 = (IndexOutOfRangeException_t3456360697 *)il2cpp_codegen_object_new(IndexOutOfRangeException_t3456360697_il2cpp_TypeInfo_var);
		IndexOutOfRangeException__ctor_m707236112(L_5, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}
}
extern "C"  bool FixedBitArray3_get_Item_m141500035_AdjustorThunk (Il2CppObject * __this, int32_t ___index0, const MethodInfo* method)
{
	FixedBitArray3_t3665369095 * _thisAdjusted = reinterpret_cast<FixedBitArray3_t3665369095 *>(__this + 1);
	return FixedBitArray3_get_Item_m141500035(_thisAdjusted, ___index0, method);
}
// System.Void Pathfinding.Poly2Tri.FixedBitArray3::set_Item(System.Int32,System.Boolean)
extern Il2CppClass* IndexOutOfRangeException_t3456360697_il2cpp_TypeInfo_var;
extern const uint32_t FixedBitArray3_set_Item_m2925852176_MetadataUsageId;
extern "C"  void FixedBitArray3_set_Item_m2925852176 (FixedBitArray3_t3665369095 * __this, int32_t ___index0, bool ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FixedBitArray3_set_Item_m2925852176_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index0;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0019;
		}
		if (L_1 == 1)
		{
			goto IL_0025;
		}
		if (L_1 == 2)
		{
			goto IL_0031;
		}
	}
	{
		goto IL_003d;
	}

IL_0019:
	{
		bool L_2 = ___value1;
		__this->set__0_0(L_2);
		goto IL_0043;
	}

IL_0025:
	{
		bool L_3 = ___value1;
		__this->set__1_1(L_3);
		goto IL_0043;
	}

IL_0031:
	{
		bool L_4 = ___value1;
		__this->set__2_2(L_4);
		goto IL_0043;
	}

IL_003d:
	{
		IndexOutOfRangeException_t3456360697 * L_5 = (IndexOutOfRangeException_t3456360697 *)il2cpp_codegen_object_new(IndexOutOfRangeException_t3456360697_il2cpp_TypeInfo_var);
		IndexOutOfRangeException__ctor_m707236112(L_5, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0043:
	{
		return;
	}
}
extern "C"  void FixedBitArray3_set_Item_m2925852176_AdjustorThunk (Il2CppObject * __this, int32_t ___index0, bool ___value1, const MethodInfo* method)
{
	FixedBitArray3_t3665369095 * _thisAdjusted = reinterpret_cast<FixedBitArray3_t3665369095 *>(__this + 1);
	FixedBitArray3_set_Item_m2925852176(_thisAdjusted, ___index0, ___value1, method);
}
// System.Void Pathfinding.Poly2Tri.FixedBitArray3::Clear()
extern "C"  void FixedBitArray3_Clear_m4005825347 (FixedBitArray3_t3665369095 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	{
		int32_t L_0 = 0;
		V_0 = (bool)L_0;
		__this->set__2_2((bool)L_0);
		bool L_1 = V_0;
		bool L_2 = L_1;
		V_0 = L_2;
		__this->set__1_1(L_2);
		bool L_3 = V_0;
		__this->set__0_0(L_3);
		return;
	}
}
extern "C"  void FixedBitArray3_Clear_m4005825347_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	FixedBitArray3_t3665369095 * _thisAdjusted = reinterpret_cast<FixedBitArray3_t3665369095 *>(__this + 1);
	FixedBitArray3_Clear_m4005825347(_thisAdjusted, method);
}
// System.Collections.Generic.IEnumerable`1<System.Boolean> Pathfinding.Poly2Tri.FixedBitArray3::Enumerate()
extern Il2CppClass* U3CEnumerateU3Ec__Iterator1_t530243353_il2cpp_TypeInfo_var;
extern const uint32_t FixedBitArray3_Enumerate_m3597532749_MetadataUsageId;
extern "C"  Il2CppObject* FixedBitArray3_Enumerate_m3597532749 (FixedBitArray3_t3665369095 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FixedBitArray3_Enumerate_m3597532749_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CEnumerateU3Ec__Iterator1_t530243353 * V_0 = NULL;
	{
		U3CEnumerateU3Ec__Iterator1_t530243353 * L_0 = (U3CEnumerateU3Ec__Iterator1_t530243353 *)il2cpp_codegen_object_new(U3CEnumerateU3Ec__Iterator1_t530243353_il2cpp_TypeInfo_var);
		U3CEnumerateU3Ec__Iterator1__ctor_m4172945681(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CEnumerateU3Ec__Iterator1_t530243353 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_3((*(FixedBitArray3_t3665369095 *)__this));
		U3CEnumerateU3Ec__Iterator1_t530243353 * L_2 = V_0;
		U3CEnumerateU3Ec__Iterator1_t530243353 * L_3 = L_2;
		NullCheck(L_3);
		L_3->set_U24PC_1(((int32_t)-2));
		return L_3;
	}
}
extern "C"  Il2CppObject* FixedBitArray3_Enumerate_m3597532749_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	FixedBitArray3_t3665369095 * _thisAdjusted = reinterpret_cast<FixedBitArray3_t3665369095 *>(__this + 1);
	return FixedBitArray3_Enumerate_m3597532749(_thisAdjusted, method);
}
// System.Collections.Generic.IEnumerator`1<System.Boolean> Pathfinding.Poly2Tri.FixedBitArray3::GetEnumerator()
extern Il2CppClass* IEnumerable_1_t3777711675_il2cpp_TypeInfo_var;
extern const uint32_t FixedBitArray3_GetEnumerator_m1379323559_MetadataUsageId;
extern "C"  Il2CppObject* FixedBitArray3_GetEnumerator_m1379323559 (FixedBitArray3_t3665369095 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FixedBitArray3_GetEnumerator_m1379323559_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = FixedBitArray3_Enumerate_m3597532749(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Il2CppObject* L_1 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Boolean>::GetEnumerator() */, IEnumerable_1_t3777711675_il2cpp_TypeInfo_var, L_0);
		return L_1;
	}
}
extern "C"  Il2CppObject* FixedBitArray3_GetEnumerator_m1379323559_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	FixedBitArray3_t3665369095 * _thisAdjusted = reinterpret_cast<FixedBitArray3_t3665369095 *>(__this + 1);
	return FixedBitArray3_GetEnumerator_m1379323559(_thisAdjusted, method);
}
// Conversion methods for marshalling of: Pathfinding.Poly2Tri.FixedBitArray3
extern "C" void FixedBitArray3_t3665369095_marshal_pinvoke(const FixedBitArray3_t3665369095& unmarshaled, FixedBitArray3_t3665369095_marshaled_pinvoke& marshaled)
{
	marshaled.____0_0 = unmarshaled.get__0_0();
	marshaled.____1_1 = unmarshaled.get__1_1();
	marshaled.____2_2 = unmarshaled.get__2_2();
}
extern "C" void FixedBitArray3_t3665369095_marshal_pinvoke_back(const FixedBitArray3_t3665369095_marshaled_pinvoke& marshaled, FixedBitArray3_t3665369095& unmarshaled)
{
	bool unmarshaled__0_temp_0 = false;
	unmarshaled__0_temp_0 = marshaled.____0_0;
	unmarshaled.set__0_0(unmarshaled__0_temp_0);
	bool unmarshaled__1_temp_1 = false;
	unmarshaled__1_temp_1 = marshaled.____1_1;
	unmarshaled.set__1_1(unmarshaled__1_temp_1);
	bool unmarshaled__2_temp_2 = false;
	unmarshaled__2_temp_2 = marshaled.____2_2;
	unmarshaled.set__2_2(unmarshaled__2_temp_2);
}
// Conversion method for clean up from marshalling of: Pathfinding.Poly2Tri.FixedBitArray3
extern "C" void FixedBitArray3_t3665369095_marshal_pinvoke_cleanup(FixedBitArray3_t3665369095_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: Pathfinding.Poly2Tri.FixedBitArray3
extern "C" void FixedBitArray3_t3665369095_marshal_com(const FixedBitArray3_t3665369095& unmarshaled, FixedBitArray3_t3665369095_marshaled_com& marshaled)
{
	marshaled.____0_0 = unmarshaled.get__0_0();
	marshaled.____1_1 = unmarshaled.get__1_1();
	marshaled.____2_2 = unmarshaled.get__2_2();
}
extern "C" void FixedBitArray3_t3665369095_marshal_com_back(const FixedBitArray3_t3665369095_marshaled_com& marshaled, FixedBitArray3_t3665369095& unmarshaled)
{
	bool unmarshaled__0_temp_0 = false;
	unmarshaled__0_temp_0 = marshaled.____0_0;
	unmarshaled.set__0_0(unmarshaled__0_temp_0);
	bool unmarshaled__1_temp_1 = false;
	unmarshaled__1_temp_1 = marshaled.____1_1;
	unmarshaled.set__1_1(unmarshaled__1_temp_1);
	bool unmarshaled__2_temp_2 = false;
	unmarshaled__2_temp_2 = marshaled.____2_2;
	unmarshaled.set__2_2(unmarshaled__2_temp_2);
}
// Conversion method for clean up from marshalling of: Pathfinding.Poly2Tri.FixedBitArray3
extern "C" void FixedBitArray3_t3665369095_marshal_com_cleanup(FixedBitArray3_t3665369095_marshaled_com& marshaled)
{
}
// System.Void Pathfinding.Poly2Tri.FixedBitArray3/<Enumerate>c__Iterator1::.ctor()
extern "C"  void U3CEnumerateU3Ec__Iterator1__ctor_m4172945681 (U3CEnumerateU3Ec__Iterator1_t530243353 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Pathfinding.Poly2Tri.FixedBitArray3/<Enumerate>c__Iterator1::System.Collections.Generic.IEnumerator<bool>.get_Current()
extern "C"  bool U3CEnumerateU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CboolU3E_get_Current_m877570709 (U3CEnumerateU3Ec__Iterator1_t530243353 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Object Pathfinding.Poly2Tri.FixedBitArray3/<Enumerate>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* Boolean_t476798718_il2cpp_TypeInfo_var;
extern const uint32_t U3CEnumerateU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m2547457461_MetadataUsageId;
extern "C"  Il2CppObject * U3CEnumerateU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m2547457461 (U3CEnumerateU3Ec__Iterator1_t530243353 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CEnumerateU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m2547457461_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = __this->get_U24current_2();
		bool L_1 = L_0;
		Il2CppObject * L_2 = Box(Boolean_t476798718_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
// System.Collections.IEnumerator Pathfinding.Poly2Tri.FixedBitArray3/<Enumerate>c__Iterator1::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CEnumerateU3Ec__Iterator1_System_Collections_IEnumerable_GetEnumerator_m430644878 (U3CEnumerateU3Ec__Iterator1_t530243353 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = U3CEnumerateU3Ec__Iterator1_System_Collections_Generic_IEnumerableU3CboolU3E_GetEnumerator_m2073272258(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<System.Boolean> Pathfinding.Poly2Tri.FixedBitArray3/<Enumerate>c__Iterator1::System.Collections.Generic.IEnumerable<bool>.GetEnumerator()
extern Il2CppClass* U3CEnumerateU3Ec__Iterator1_t530243353_il2cpp_TypeInfo_var;
extern const uint32_t U3CEnumerateU3Ec__Iterator1_System_Collections_Generic_IEnumerableU3CboolU3E_GetEnumerator_m2073272258_MetadataUsageId;
extern "C"  Il2CppObject* U3CEnumerateU3Ec__Iterator1_System_Collections_Generic_IEnumerableU3CboolU3E_GetEnumerator_m2073272258 (U3CEnumerateU3Ec__Iterator1_t530243353 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CEnumerateU3Ec__Iterator1_System_Collections_Generic_IEnumerableU3CboolU3E_GetEnumerator_m2073272258_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CEnumerateU3Ec__Iterator1_t530243353 * V_0 = NULL;
	{
		int32_t* L_0 = __this->get_address_of_U24PC_1();
		int32_t L_1 = Interlocked_CompareExchange_m1859820752(NULL /*static, unused*/, L_0, 0, ((int32_t)-2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		U3CEnumerateU3Ec__Iterator1_t530243353 * L_2 = (U3CEnumerateU3Ec__Iterator1_t530243353 *)il2cpp_codegen_object_new(U3CEnumerateU3Ec__Iterator1_t530243353_il2cpp_TypeInfo_var);
		U3CEnumerateU3Ec__Iterator1__ctor_m4172945681(L_2, /*hidden argument*/NULL);
		V_0 = L_2;
		U3CEnumerateU3Ec__Iterator1_t530243353 * L_3 = V_0;
		FixedBitArray3_t3665369095  L_4 = __this->get_U3CU3Ef__this_3();
		NullCheck(L_3);
		L_3->set_U3CU3Ef__this_3(L_4);
		U3CEnumerateU3Ec__Iterator1_t530243353 * L_5 = V_0;
		return L_5;
	}
}
// System.Boolean Pathfinding.Poly2Tri.FixedBitArray3/<Enumerate>c__Iterator1::MoveNext()
extern "C"  bool U3CEnumerateU3Ec__Iterator1_MoveNext_m3244669857 (U3CEnumerateU3Ec__Iterator1_t530243353 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_1();
		V_0 = L_0;
		__this->set_U24PC_1((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0050;
		}
	}
	{
		goto IL_0071;
	}

IL_0021:
	{
		__this->set_U3CiU3E__0_0(0);
		goto IL_005e;
	}

IL_002d:
	{
		FixedBitArray3_t3665369095 * L_2 = __this->get_address_of_U3CU3Ef__this_3();
		int32_t L_3 = __this->get_U3CiU3E__0_0();
		bool L_4 = FixedBitArray3_get_Item_m141500035(L_2, L_3, /*hidden argument*/NULL);
		__this->set_U24current_2(L_4);
		__this->set_U24PC_1(1);
		goto IL_0073;
	}

IL_0050:
	{
		int32_t L_5 = __this->get_U3CiU3E__0_0();
		__this->set_U3CiU3E__0_0(((int32_t)((int32_t)L_5+(int32_t)1)));
	}

IL_005e:
	{
		int32_t L_6 = __this->get_U3CiU3E__0_0();
		if ((((int32_t)L_6) < ((int32_t)3)))
		{
			goto IL_002d;
		}
	}
	{
		__this->set_U24PC_1((-1));
	}

IL_0071:
	{
		return (bool)0;
	}

IL_0073:
	{
		return (bool)1;
	}
}
// System.Void Pathfinding.Poly2Tri.FixedBitArray3/<Enumerate>c__Iterator1::Dispose()
extern "C"  void U3CEnumerateU3Ec__Iterator1_Dispose_m2894595342 (U3CEnumerateU3Ec__Iterator1_t530243353 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_1((-1));
		return;
	}
}
// System.Void Pathfinding.Poly2Tri.FixedBitArray3/<Enumerate>c__Iterator1::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CEnumerateU3Ec__Iterator1_Reset_m1819378622_MetadataUsageId;
extern "C"  void U3CEnumerateU3Ec__Iterator1_Reset_m1819378622 (U3CEnumerateU3Ec__Iterator1_t530243353 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CEnumerateU3Ec__Iterator1_Reset_m1819378622_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void Pathfinding.Poly2Tri.P2T::.cctor()
extern "C"  void P2T__cctor_m3244147300 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Pathfinding.Poly2Tri.P2T::Triangulate(Pathfinding.Poly2Tri.Polygon)
extern Il2CppClass* P2T_t3302343464_il2cpp_TypeInfo_var;
extern const uint32_t P2T_Triangulate_m3525691096_MetadataUsageId;
extern "C"  void P2T_Triangulate_m3525691096 (Il2CppObject * __this /* static, unused */, Polygon_t1047479568 * ___p0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (P2T_Triangulate_m3525691096_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(P2T_t3302343464_il2cpp_TypeInfo_var);
		int32_t L_0 = ((P2T_t3302343464_StaticFields*)P2T_t3302343464_il2cpp_TypeInfo_var->static_fields)->get__defaultAlgorithm_0();
		Polygon_t1047479568 * L_1 = ___p0;
		P2T_Triangulate_m3381876250(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// Pathfinding.Poly2Tri.TriangulationContext Pathfinding.Poly2Tri.P2T::CreateContext(Pathfinding.Poly2Tri.TriangulationAlgorithm)
extern Il2CppClass* DTSweepContext_t543731303_il2cpp_TypeInfo_var;
extern const uint32_t P2T_CreateContext_m2227060077_MetadataUsageId;
extern "C"  TriangulationContext_t3528662164 * P2T_CreateContext_m2227060077 (Il2CppObject * __this /* static, unused */, int32_t ___algorithm0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (P2T_CreateContext_m2227060077_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___algorithm0;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (!L_1)
		{
			goto IL_000d;
		}
	}
	{
		goto IL_000d;
	}

IL_000d:
	{
		DTSweepContext_t543731303 * L_2 = (DTSweepContext_t543731303 *)il2cpp_codegen_object_new(DTSweepContext_t543731303_il2cpp_TypeInfo_var);
		DTSweepContext__ctor_m606266552(L_2, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void Pathfinding.Poly2Tri.P2T::Triangulate(Pathfinding.Poly2Tri.TriangulationAlgorithm,Pathfinding.Poly2Tri.Triangulatable)
extern Il2CppClass* P2T_t3302343464_il2cpp_TypeInfo_var;
extern const uint32_t P2T_Triangulate_m3381876250_MetadataUsageId;
extern "C"  void P2T_Triangulate_m3381876250 (Il2CppObject * __this /* static, unused */, int32_t ___algorithm0, Il2CppObject * ___t1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (P2T_Triangulate_m3381876250_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	TriangulationContext_t3528662164 * V_0 = NULL;
	{
		int32_t L_0 = ___algorithm0;
		IL2CPP_RUNTIME_CLASS_INIT(P2T_t3302343464_il2cpp_TypeInfo_var);
		TriangulationContext_t3528662164 * L_1 = P2T_CreateContext_m2227060077(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		TriangulationContext_t3528662164 * L_2 = V_0;
		Il2CppObject * L_3 = ___t1;
		NullCheck(L_2);
		VirtActionInvoker1< Il2CppObject * >::Invoke(5 /* System.Void Pathfinding.Poly2Tri.TriangulationContext::PrepareTriangulation(Pathfinding.Poly2Tri.Triangulatable) */, L_2, L_3);
		TriangulationContext_t3528662164 * L_4 = V_0;
		P2T_Triangulate_m3976741632(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Pathfinding.Poly2Tri.P2T::Triangulate(Pathfinding.Poly2Tri.TriangulationContext)
extern Il2CppClass* DTSweepContext_t543731303_il2cpp_TypeInfo_var;
extern const uint32_t P2T_Triangulate_m3976741632_MetadataUsageId;
extern "C"  void P2T_Triangulate_m3976741632 (Il2CppObject * __this /* static, unused */, TriangulationContext_t3528662164 * ___tcx0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (P2T_Triangulate_m3976741632_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		TriangulationContext_t3528662164 * L_0 = ___tcx0;
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(4 /* Pathfinding.Poly2Tri.TriangulationAlgorithm Pathfinding.Poly2Tri.TriangulationContext::get_Algorithm() */, L_0);
		V_0 = L_1;
		int32_t L_2 = V_0;
		if (!L_2)
		{
			goto IL_0012;
		}
	}
	{
		goto IL_0012;
	}

IL_0012:
	{
		TriangulationContext_t3528662164 * L_3 = ___tcx0;
		DTSweep_Triangulate_m2688155415(NULL /*static, unused*/, ((DTSweepContext_t543731303 *)CastclassClass(L_3, DTSweepContext_t543731303_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		goto IL_0022;
	}

IL_0022:
	{
		return;
	}
}
// System.Void Pathfinding.Poly2Tri.PointOnEdgeException::.ctor(System.String,Pathfinding.Poly2Tri.TriangulationPoint,Pathfinding.Poly2Tri.TriangulationPoint,Pathfinding.Poly2Tri.TriangulationPoint)
extern Il2CppClass* StringU5BU5D_t4054002952_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral10;
extern const uint32_t PointOnEdgeException__ctor_m41689854_MetadataUsageId;
extern "C"  void PointOnEdgeException__ctor_m41689854 (PointOnEdgeException_t989229367 * __this, String_t* ___message0, TriangulationPoint_t3810082933 * ___a1, TriangulationPoint_t3810082933 * ___b2, TriangulationPoint_t3810082933 * ___c3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PointOnEdgeException__ctor_m41689854_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		StringU5BU5D_t4054002952* L_0 = ((StringU5BU5D_t4054002952*)SZArrayNew(StringU5BU5D_t4054002952_il2cpp_TypeInfo_var, (uint32_t)7));
		String_t* L_1 = ___message0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)L_1);
		StringU5BU5D_t4054002952* L_2 = L_0;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		ArrayElementTypeCheck (L_2, _stringLiteral10);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral10);
		StringU5BU5D_t4054002952* L_3 = L_2;
		TriangulationPoint_t3810082933 * L_4 = ___a1;
		NullCheck(L_4);
		String_t* L_5 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_4);
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 2);
		ArrayElementTypeCheck (L_3, L_5);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)L_5);
		StringU5BU5D_t4054002952* L_6 = L_3;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 3);
		ArrayElementTypeCheck (L_6, _stringLiteral10);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral10);
		StringU5BU5D_t4054002952* L_7 = L_6;
		TriangulationPoint_t3810082933 * L_8 = ___b2;
		NullCheck(L_8);
		String_t* L_9 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_8);
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 4);
		ArrayElementTypeCheck (L_7, L_9);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)L_9);
		StringU5BU5D_t4054002952* L_10 = L_7;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 5);
		ArrayElementTypeCheck (L_10, _stringLiteral10);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(5), (String_t*)_stringLiteral10);
		StringU5BU5D_t4054002952* L_11 = L_10;
		TriangulationPoint_t3810082933 * L_12 = ___c3;
		NullCheck(L_12);
		String_t* L_13 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_12);
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 6);
		ArrayElementTypeCheck (L_11, L_13);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(6), (String_t*)L_13);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m21867311(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		NotImplementedException__ctor_m495190705(__this, L_14, /*hidden argument*/NULL);
		TriangulationPoint_t3810082933 * L_15 = ___a1;
		__this->set_A_11(L_15);
		TriangulationPoint_t3810082933 * L_16 = ___b2;
		__this->set_B_12(L_16);
		TriangulationPoint_t3810082933 * L_17 = ___c3;
		__this->set_C_13(L_17);
		return;
	}
}
// System.Void Pathfinding.Poly2Tri.Polygon::.ctor(System.Collections.Generic.IList`1<Pathfinding.Poly2Tri.PolygonPoint>)
extern Il2CppClass* List_1_t883301189_il2cpp_TypeInfo_var;
extern Il2CppClass* ICollection_1_t3117417741_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t928607144_il2cpp_TypeInfo_var;
extern Il2CppClass* IList_1_t622507661_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m2511067072_MethodInfo_var;
extern const MethodInfo* List_1_get_Capacity_m1089808119_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m3716150292_MethodInfo_var;
extern const MethodInfo* List_1_set_Capacity_m2848089484_MethodInfo_var;
extern const MethodInfo* List_1_Add_m2205427888_MethodInfo_var;
extern const MethodInfo* List_1_RemoveAt_m407936748_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1669769868;
extern Il2CppCodeGenString* _stringLiteral3312213219;
extern const uint32_t Polygon__ctor_m3492825397_MetadataUsageId;
extern "C"  void Polygon__ctor_m3492825397 (Polygon_t1047479568 * __this, Il2CppObject* ___points0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Polygon__ctor_m3492825397_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		List_1_t883301189 * L_0 = (List_1_t883301189 *)il2cpp_codegen_object_new(List_1_t883301189_il2cpp_TypeInfo_var);
		List_1__ctor_m2511067072(L_0, /*hidden argument*/List_1__ctor_m2511067072_MethodInfo_var);
		__this->set__points_0(L_0);
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		Il2CppObject* L_1 = ___points0;
		NullCheck(L_1);
		int32_t L_2 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<Pathfinding.Poly2Tri.PolygonPoint>::get_Count() */, ICollection_1_t3117417741_il2cpp_TypeInfo_var, L_1);
		if ((((int32_t)L_2) >= ((int32_t)3)))
		{
			goto IL_002d;
		}
	}
	{
		ArgumentException_t928607144 * L_3 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m732321503(L_3, _stringLiteral1669769868, _stringLiteral3312213219, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002d:
	{
		List_1_t883301189 * L_4 = __this->get__points_0();
		List_1_t883301189 * L_5 = __this->get__points_0();
		NullCheck(L_5);
		int32_t L_6 = List_1_get_Capacity_m1089808119(L_5, /*hidden argument*/List_1_get_Capacity_m1089808119_MethodInfo_var);
		List_1_t883301189 * L_7 = __this->get__points_0();
		NullCheck(L_7);
		int32_t L_8 = List_1_get_Count_m3716150292(L_7, /*hidden argument*/List_1_get_Count_m3716150292_MethodInfo_var);
		Il2CppObject* L_9 = ___points0;
		NullCheck(L_9);
		int32_t L_10 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<Pathfinding.Poly2Tri.PolygonPoint>::get_Count() */, ICollection_1_t3117417741_il2cpp_TypeInfo_var, L_9);
		int32_t L_11 = Math_Max_m1309380475(NULL /*static, unused*/, L_6, ((int32_t)((int32_t)L_8+(int32_t)L_10)), /*hidden argument*/NULL);
		NullCheck(L_4);
		List_1_set_Capacity_m2848089484(L_4, L_11, /*hidden argument*/List_1_set_Capacity_m2848089484_MethodInfo_var);
		V_0 = 0;
		goto IL_0077;
	}

IL_0061:
	{
		List_1_t883301189 * L_12 = __this->get__points_0();
		Il2CppObject* L_13 = ___points0;
		int32_t L_14 = V_0;
		NullCheck(L_13);
		PolygonPoint_t2222827754 * L_15 = InterfaceFuncInvoker1< PolygonPoint_t2222827754 *, int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<Pathfinding.Poly2Tri.PolygonPoint>::get_Item(System.Int32) */, IList_1_t622507661_il2cpp_TypeInfo_var, L_13, L_14);
		NullCheck(L_12);
		List_1_Add_m2205427888(L_12, L_15, /*hidden argument*/List_1_Add_m2205427888_MethodInfo_var);
		int32_t L_16 = V_0;
		V_0 = ((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_0077:
	{
		int32_t L_17 = V_0;
		Il2CppObject* L_18 = ___points0;
		NullCheck(L_18);
		int32_t L_19 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<Pathfinding.Poly2Tri.PolygonPoint>::get_Count() */, ICollection_1_t3117417741_il2cpp_TypeInfo_var, L_18);
		if ((((int32_t)L_17) < ((int32_t)L_19)))
		{
			goto IL_0061;
		}
	}
	{
		Il2CppObject* L_20 = ___points0;
		NullCheck(L_20);
		PolygonPoint_t2222827754 * L_21 = InterfaceFuncInvoker1< PolygonPoint_t2222827754 *, int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<Pathfinding.Poly2Tri.PolygonPoint>::get_Item(System.Int32) */, IList_1_t622507661_il2cpp_TypeInfo_var, L_20, 0);
		Il2CppObject* L_22 = ___points0;
		Il2CppObject* L_23 = ___points0;
		NullCheck(L_23);
		int32_t L_24 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<Pathfinding.Poly2Tri.PolygonPoint>::get_Count() */, ICollection_1_t3117417741_il2cpp_TypeInfo_var, L_23);
		NullCheck(L_22);
		PolygonPoint_t2222827754 * L_25 = InterfaceFuncInvoker1< PolygonPoint_t2222827754 *, int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<Pathfinding.Poly2Tri.PolygonPoint>::get_Item(System.Int32) */, IList_1_t622507661_il2cpp_TypeInfo_var, L_22, ((int32_t)((int32_t)L_24-(int32_t)1)));
		NullCheck(L_21);
		bool L_26 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, L_21, L_25);
		if (!L_26)
		{
			goto IL_00ba;
		}
	}
	{
		List_1_t883301189 * L_27 = __this->get__points_0();
		List_1_t883301189 * L_28 = __this->get__points_0();
		NullCheck(L_28);
		int32_t L_29 = List_1_get_Count_m3716150292(L_28, /*hidden argument*/List_1_get_Count_m3716150292_MethodInfo_var);
		NullCheck(L_27);
		List_1_RemoveAt_m407936748(L_27, ((int32_t)((int32_t)L_29-(int32_t)1)), /*hidden argument*/List_1_RemoveAt_m407936748_MethodInfo_var);
	}

IL_00ba:
	{
		return;
	}
}
// Pathfinding.Poly2Tri.TriangulationMode Pathfinding.Poly2Tri.Polygon::get_TriangulationMode()
extern "C"  int32_t Polygon_get_TriangulationMode_m2314587740 (Polygon_t1047479568 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(2);
	}
}
// System.Void Pathfinding.Poly2Tri.Polygon::AddHole(Pathfinding.Poly2Tri.Polygon)
extern Il2CppClass* List_1_t2415665120_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m4049689413_MethodInfo_var;
extern const MethodInfo* List_1_Add_m3744050229_MethodInfo_var;
extern const uint32_t Polygon_AddHole_m3361488645_MetadataUsageId;
extern "C"  void Polygon_AddHole_m3361488645 (Polygon_t1047479568 * __this, Polygon_t1047479568 * ___poly0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Polygon_AddHole_m3361488645_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t2415665120 * L_0 = __this->get__holes_2();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		List_1_t2415665120 * L_1 = (List_1_t2415665120 *)il2cpp_codegen_object_new(List_1_t2415665120_il2cpp_TypeInfo_var);
		List_1__ctor_m4049689413(L_1, /*hidden argument*/List_1__ctor_m4049689413_MethodInfo_var);
		__this->set__holes_2(L_1);
	}

IL_0016:
	{
		List_1_t2415665120 * L_2 = __this->get__holes_2();
		Polygon_t1047479568 * L_3 = ___poly0;
		NullCheck(L_2);
		List_1_Add_m3744050229(L_2, L_3, /*hidden argument*/List_1_Add_m3744050229_MethodInfo_var);
		return;
	}
}
// System.Void Pathfinding.Poly2Tri.Polygon::AddPoints(System.Collections.Generic.IEnumerable`1<Pathfinding.Poly2Tri.PolygonPoint>)
extern Il2CppClass* IEnumerable_1_t1228773415_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_1_t4134692803_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t3464575207_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1423340799_il2cpp_TypeInfo_var;
extern Il2CppClass* PolygonPoint_t2222827754_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_Add_m2205427888_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m2083438529_MethodInfo_var;
extern const uint32_t Polygon_AddPoints_m1159662621_MetadataUsageId;
extern "C"  void Polygon_AddPoints_m1159662621 (Polygon_t1047479568 * __this, Il2CppObject* ___list0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Polygon_AddPoints_m1159662621_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	PolygonPoint_t2222827754 * V_0 = NULL;
	Il2CppObject* V_1 = NULL;
	PolygonPoint_t2222827754 * V_2 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject* L_0 = ___list0;
		NullCheck(L_0);
		Il2CppObject* L_1 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<Pathfinding.Poly2Tri.PolygonPoint>::GetEnumerator() */, IEnumerable_1_t1228773415_il2cpp_TypeInfo_var, L_0);
		V_1 = L_1;
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		{
			goto IL_005a;
		}

IL_000c:
		{
			Il2CppObject* L_2 = V_1;
			NullCheck(L_2);
			PolygonPoint_t2222827754 * L_3 = InterfaceFuncInvoker0< PolygonPoint_t2222827754 * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<Pathfinding.Poly2Tri.PolygonPoint>::get_Current() */, IEnumerator_1_t4134692803_il2cpp_TypeInfo_var, L_2);
			V_2 = L_3;
			PolygonPoint_t2222827754 * L_4 = V_2;
			PolygonPoint_t2222827754 * L_5 = __this->get__last_4();
			NullCheck(L_4);
			PolygonPoint_set_Previous_m2758999194(L_4, L_5, /*hidden argument*/NULL);
			PolygonPoint_t2222827754 * L_6 = __this->get__last_4();
			if (!L_6)
			{
				goto IL_0047;
			}
		}

IL_002a:
		{
			PolygonPoint_t2222827754 * L_7 = V_2;
			PolygonPoint_t2222827754 * L_8 = __this->get__last_4();
			NullCheck(L_8);
			PolygonPoint_t2222827754 * L_9 = PolygonPoint_get_Next_m66309293(L_8, /*hidden argument*/NULL);
			NullCheck(L_7);
			PolygonPoint_set_Next_m2943622558(L_7, L_9, /*hidden argument*/NULL);
			PolygonPoint_t2222827754 * L_10 = __this->get__last_4();
			PolygonPoint_t2222827754 * L_11 = V_2;
			NullCheck(L_10);
			PolygonPoint_set_Next_m2943622558(L_10, L_11, /*hidden argument*/NULL);
		}

IL_0047:
		{
			PolygonPoint_t2222827754 * L_12 = V_2;
			__this->set__last_4(L_12);
			List_1_t883301189 * L_13 = __this->get__points_0();
			PolygonPoint_t2222827754 * L_14 = V_2;
			NullCheck(L_13);
			List_1_Add_m2205427888(L_13, L_14, /*hidden argument*/List_1_Add_m2205427888_MethodInfo_var);
		}

IL_005a:
		{
			Il2CppObject* L_15 = V_1;
			NullCheck(L_15);
			bool L_16 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t3464575207_il2cpp_TypeInfo_var, L_15);
			if (L_16)
			{
				goto IL_000c;
			}
		}

IL_0065:
		{
			IL2CPP_LEAVE(0x77, FINALLY_006a);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_006a;
	}

FINALLY_006a:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_17 = V_1;
			if (!L_17)
			{
				goto IL_0076;
			}
		}

IL_0070:
		{
			Il2CppObject* L_18 = V_1;
			NullCheck(L_18);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1423340799_il2cpp_TypeInfo_var, L_18);
		}

IL_0076:
		{
			IL2CPP_END_FINALLY(106)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(106)
	{
		IL2CPP_JUMP_TBL(0x77, IL_0077)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0077:
	{
		List_1_t883301189 * L_19 = __this->get__points_0();
		NullCheck(L_19);
		TriangulationPoint_t3810082933 * L_20 = List_1_get_Item_m2083438529(L_19, 0, /*hidden argument*/List_1_get_Item_m2083438529_MethodInfo_var);
		V_0 = ((PolygonPoint_t2222827754 *)CastclassClass(L_20, PolygonPoint_t2222827754_il2cpp_TypeInfo_var));
		PolygonPoint_t2222827754 * L_21 = __this->get__last_4();
		PolygonPoint_t2222827754 * L_22 = V_0;
		NullCheck(L_21);
		PolygonPoint_set_Next_m2943622558(L_21, L_22, /*hidden argument*/NULL);
		PolygonPoint_t2222827754 * L_23 = V_0;
		PolygonPoint_t2222827754 * L_24 = __this->get__last_4();
		NullCheck(L_23);
		PolygonPoint_set_Previous_m2758999194(L_23, L_24, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.IList`1<Pathfinding.Poly2Tri.TriangulationPoint> Pathfinding.Poly2Tri.Polygon::get_Points()
extern "C"  Il2CppObject* Polygon_get_Points_m1391827089 (Polygon_t1047479568 * __this, const MethodInfo* method)
{
	{
		List_1_t883301189 * L_0 = __this->get__points_0();
		return L_0;
	}
}
// System.Collections.Generic.IList`1<Pathfinding.Poly2Tri.DelaunayTriangle> Pathfinding.Poly2Tri.Polygon::get_Triangles()
extern "C"  Il2CppObject* Polygon_get_Triangles_m1867712689 (Polygon_t1047479568 * __this, const MethodInfo* method)
{
	{
		List_1_t4203289139 * L_0 = __this->get__triangles_3();
		return L_0;
	}
}
// System.Collections.Generic.IList`1<Pathfinding.Poly2Tri.Polygon> Pathfinding.Poly2Tri.Polygon::get_Holes()
extern "C"  Il2CppObject* Polygon_get_Holes_m4062490074 (Polygon_t1047479568 * __this, const MethodInfo* method)
{
	{
		List_1_t2415665120 * L_0 = __this->get__holes_2();
		return L_0;
	}
}
// System.Void Pathfinding.Poly2Tri.Polygon::AddTriangle(Pathfinding.Poly2Tri.DelaunayTriangle)
extern const MethodInfo* List_1_Add_m1995366622_MethodInfo_var;
extern const uint32_t Polygon_AddTriangle_m2118021308_MetadataUsageId;
extern "C"  void Polygon_AddTriangle_m2118021308 (Polygon_t1047479568 * __this, DelaunayTriangle_t2835103587 * ___t0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Polygon_AddTriangle_m2118021308_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t4203289139 * L_0 = __this->get__triangles_3();
		DelaunayTriangle_t2835103587 * L_1 = ___t0;
		NullCheck(L_0);
		List_1_Add_m1995366622(L_0, L_1, /*hidden argument*/List_1_Add_m1995366622_MethodInfo_var);
		return;
	}
}
// System.Void Pathfinding.Poly2Tri.Polygon::AddTriangles(System.Collections.Generic.IEnumerable`1<Pathfinding.Poly2Tri.DelaunayTriangle>)
extern const MethodInfo* List_1_AddRange_m2857306828_MethodInfo_var;
extern const uint32_t Polygon_AddTriangles_m1676954668_MetadataUsageId;
extern "C"  void Polygon_AddTriangles_m1676954668 (Polygon_t1047479568 * __this, Il2CppObject* ___list0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Polygon_AddTriangles_m1676954668_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t4203289139 * L_0 = __this->get__triangles_3();
		Il2CppObject* L_1 = ___list0;
		NullCheck(L_0);
		List_1_AddRange_m2857306828(L_0, L_1, /*hidden argument*/List_1_AddRange_m2857306828_MethodInfo_var);
		return;
	}
}
// System.Void Pathfinding.Poly2Tri.Polygon::ClearTriangles()
extern const MethodInfo* List_1_Clear_m4002106393_MethodInfo_var;
extern const uint32_t Polygon_ClearTriangles_m397875649_MetadataUsageId;
extern "C"  void Polygon_ClearTriangles_m397875649 (Polygon_t1047479568 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Polygon_ClearTriangles_m397875649_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t4203289139 * L_0 = __this->get__triangles_3();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		List_1_t4203289139 * L_1 = __this->get__triangles_3();
		NullCheck(L_1);
		List_1_Clear_m4002106393(L_1, /*hidden argument*/List_1_Clear_m4002106393_MethodInfo_var);
	}

IL_0016:
	{
		return;
	}
}
// System.Void Pathfinding.Poly2Tri.Polygon::Prepare(Pathfinding.Poly2Tri.TriangulationContext)
extern Il2CppClass* List_1_t4203289139_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Count_m3716150292_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m86364223_MethodInfo_var;
extern const MethodInfo* List_1_Clear_m4002106393_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m2083438529_MethodInfo_var;
extern const MethodInfo* List_1_AddRange_m3527912634_MethodInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m1149354740_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m2743276990_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m561126622_MethodInfo_var;
extern const MethodInfo* Enumerator_Dispose_m1699159283_MethodInfo_var;
extern const uint32_t Polygon_Prepare_m463720493_MetadataUsageId;
extern "C"  void Polygon_Prepare_m463720493 (Polygon_t1047479568 * __this, TriangulationContext_t3528662164 * ___tcx0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Polygon_Prepare_m463720493_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Enumerator_t2435337890  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Polygon_t1047479568 * V_2 = NULL;
	int32_t V_3 = 0;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		List_1_t4203289139 * L_0 = __this->get__triangles_3();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		List_1_t883301189 * L_1 = __this->get__points_0();
		NullCheck(L_1);
		int32_t L_2 = List_1_get_Count_m3716150292(L_1, /*hidden argument*/List_1_get_Count_m3716150292_MethodInfo_var);
		List_1_t4203289139 * L_3 = (List_1_t4203289139 *)il2cpp_codegen_object_new(List_1_t4203289139_il2cpp_TypeInfo_var);
		List_1__ctor_m86364223(L_3, L_2, /*hidden argument*/List_1__ctor_m86364223_MethodInfo_var);
		__this->set__triangles_3(L_3);
		goto IL_0031;
	}

IL_0026:
	{
		List_1_t4203289139 * L_4 = __this->get__triangles_3();
		NullCheck(L_4);
		List_1_Clear_m4002106393(L_4, /*hidden argument*/List_1_Clear_m4002106393_MethodInfo_var);
	}

IL_0031:
	{
		V_0 = 0;
		goto IL_005d;
	}

IL_0038:
	{
		TriangulationContext_t3528662164 * L_5 = ___tcx0;
		List_1_t883301189 * L_6 = __this->get__points_0();
		int32_t L_7 = V_0;
		NullCheck(L_6);
		TriangulationPoint_t3810082933 * L_8 = List_1_get_Item_m2083438529(L_6, L_7, /*hidden argument*/List_1_get_Item_m2083438529_MethodInfo_var);
		List_1_t883301189 * L_9 = __this->get__points_0();
		int32_t L_10 = V_0;
		NullCheck(L_9);
		TriangulationPoint_t3810082933 * L_11 = List_1_get_Item_m2083438529(L_9, ((int32_t)((int32_t)L_10+(int32_t)1)), /*hidden argument*/List_1_get_Item_m2083438529_MethodInfo_var);
		NullCheck(L_5);
		VirtFuncInvoker2< TriangulationConstraint_t137695394 *, TriangulationPoint_t3810082933 *, TriangulationPoint_t3810082933 * >::Invoke(6 /* Pathfinding.Poly2Tri.TriangulationConstraint Pathfinding.Poly2Tri.TriangulationContext::NewConstraint(Pathfinding.Poly2Tri.TriangulationPoint,Pathfinding.Poly2Tri.TriangulationPoint) */, L_5, L_8, L_11);
		int32_t L_12 = V_0;
		V_0 = ((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_005d:
	{
		int32_t L_13 = V_0;
		List_1_t883301189 * L_14 = __this->get__points_0();
		NullCheck(L_14);
		int32_t L_15 = List_1_get_Count_m3716150292(L_14, /*hidden argument*/List_1_get_Count_m3716150292_MethodInfo_var);
		if ((((int32_t)L_13) < ((int32_t)((int32_t)((int32_t)L_15-(int32_t)1)))))
		{
			goto IL_0038;
		}
	}
	{
		TriangulationContext_t3528662164 * L_16 = ___tcx0;
		List_1_t883301189 * L_17 = __this->get__points_0();
		NullCheck(L_17);
		TriangulationPoint_t3810082933 * L_18 = List_1_get_Item_m2083438529(L_17, 0, /*hidden argument*/List_1_get_Item_m2083438529_MethodInfo_var);
		List_1_t883301189 * L_19 = __this->get__points_0();
		List_1_t883301189 * L_20 = __this->get__points_0();
		NullCheck(L_20);
		int32_t L_21 = List_1_get_Count_m3716150292(L_20, /*hidden argument*/List_1_get_Count_m3716150292_MethodInfo_var);
		NullCheck(L_19);
		TriangulationPoint_t3810082933 * L_22 = List_1_get_Item_m2083438529(L_19, ((int32_t)((int32_t)L_21-(int32_t)1)), /*hidden argument*/List_1_get_Item_m2083438529_MethodInfo_var);
		NullCheck(L_16);
		VirtFuncInvoker2< TriangulationConstraint_t137695394 *, TriangulationPoint_t3810082933 *, TriangulationPoint_t3810082933 * >::Invoke(6 /* Pathfinding.Poly2Tri.TriangulationConstraint Pathfinding.Poly2Tri.TriangulationContext::NewConstraint(Pathfinding.Poly2Tri.TriangulationPoint,Pathfinding.Poly2Tri.TriangulationPoint) */, L_16, L_18, L_22);
		TriangulationContext_t3528662164 * L_23 = ___tcx0;
		NullCheck(L_23);
		List_1_t883301189 * L_24 = L_23->get_Points_1();
		List_1_t883301189 * L_25 = __this->get__points_0();
		NullCheck(L_24);
		List_1_AddRange_m3527912634(L_24, L_25, /*hidden argument*/List_1_AddRange_m3527912634_MethodInfo_var);
		List_1_t2415665120 * L_26 = __this->get__holes_2();
		if (!L_26)
		{
			goto IL_016a;
		}
	}
	{
		List_1_t2415665120 * L_27 = __this->get__holes_2();
		NullCheck(L_27);
		Enumerator_t2435337890  L_28 = List_1_GetEnumerator_m1149354740(L_27, /*hidden argument*/List_1_GetEnumerator_m1149354740_MethodInfo_var);
		V_1 = L_28;
	}

IL_00c3:
	try
	{ // begin try (depth: 1)
		{
			goto IL_014b;
		}

IL_00c8:
		{
			Polygon_t1047479568 * L_29 = Enumerator_get_Current_m2743276990((&V_1), /*hidden argument*/Enumerator_get_Current_m2743276990_MethodInfo_var);
			V_2 = L_29;
			V_3 = 0;
			goto IL_00fc;
		}

IL_00d7:
		{
			TriangulationContext_t3528662164 * L_30 = ___tcx0;
			Polygon_t1047479568 * L_31 = V_2;
			NullCheck(L_31);
			List_1_t883301189 * L_32 = L_31->get__points_0();
			int32_t L_33 = V_3;
			NullCheck(L_32);
			TriangulationPoint_t3810082933 * L_34 = List_1_get_Item_m2083438529(L_32, L_33, /*hidden argument*/List_1_get_Item_m2083438529_MethodInfo_var);
			Polygon_t1047479568 * L_35 = V_2;
			NullCheck(L_35);
			List_1_t883301189 * L_36 = L_35->get__points_0();
			int32_t L_37 = V_3;
			NullCheck(L_36);
			TriangulationPoint_t3810082933 * L_38 = List_1_get_Item_m2083438529(L_36, ((int32_t)((int32_t)L_37+(int32_t)1)), /*hidden argument*/List_1_get_Item_m2083438529_MethodInfo_var);
			NullCheck(L_30);
			VirtFuncInvoker2< TriangulationConstraint_t137695394 *, TriangulationPoint_t3810082933 *, TriangulationPoint_t3810082933 * >::Invoke(6 /* Pathfinding.Poly2Tri.TriangulationConstraint Pathfinding.Poly2Tri.TriangulationContext::NewConstraint(Pathfinding.Poly2Tri.TriangulationPoint,Pathfinding.Poly2Tri.TriangulationPoint) */, L_30, L_34, L_38);
			int32_t L_39 = V_3;
			V_3 = ((int32_t)((int32_t)L_39+(int32_t)1));
		}

IL_00fc:
		{
			int32_t L_40 = V_3;
			Polygon_t1047479568 * L_41 = V_2;
			NullCheck(L_41);
			List_1_t883301189 * L_42 = L_41->get__points_0();
			NullCheck(L_42);
			int32_t L_43 = List_1_get_Count_m3716150292(L_42, /*hidden argument*/List_1_get_Count_m3716150292_MethodInfo_var);
			if ((((int32_t)L_40) < ((int32_t)((int32_t)((int32_t)L_43-(int32_t)1)))))
			{
				goto IL_00d7;
			}
		}

IL_010f:
		{
			TriangulationContext_t3528662164 * L_44 = ___tcx0;
			Polygon_t1047479568 * L_45 = V_2;
			NullCheck(L_45);
			List_1_t883301189 * L_46 = L_45->get__points_0();
			NullCheck(L_46);
			TriangulationPoint_t3810082933 * L_47 = List_1_get_Item_m2083438529(L_46, 0, /*hidden argument*/List_1_get_Item_m2083438529_MethodInfo_var);
			Polygon_t1047479568 * L_48 = V_2;
			NullCheck(L_48);
			List_1_t883301189 * L_49 = L_48->get__points_0();
			Polygon_t1047479568 * L_50 = V_2;
			NullCheck(L_50);
			List_1_t883301189 * L_51 = L_50->get__points_0();
			NullCheck(L_51);
			int32_t L_52 = List_1_get_Count_m3716150292(L_51, /*hidden argument*/List_1_get_Count_m3716150292_MethodInfo_var);
			NullCheck(L_49);
			TriangulationPoint_t3810082933 * L_53 = List_1_get_Item_m2083438529(L_49, ((int32_t)((int32_t)L_52-(int32_t)1)), /*hidden argument*/List_1_get_Item_m2083438529_MethodInfo_var);
			NullCheck(L_44);
			VirtFuncInvoker2< TriangulationConstraint_t137695394 *, TriangulationPoint_t3810082933 *, TriangulationPoint_t3810082933 * >::Invoke(6 /* Pathfinding.Poly2Tri.TriangulationConstraint Pathfinding.Poly2Tri.TriangulationContext::NewConstraint(Pathfinding.Poly2Tri.TriangulationPoint,Pathfinding.Poly2Tri.TriangulationPoint) */, L_44, L_47, L_53);
			TriangulationContext_t3528662164 * L_54 = ___tcx0;
			NullCheck(L_54);
			List_1_t883301189 * L_55 = L_54->get_Points_1();
			Polygon_t1047479568 * L_56 = V_2;
			NullCheck(L_56);
			List_1_t883301189 * L_57 = L_56->get__points_0();
			NullCheck(L_55);
			List_1_AddRange_m3527912634(L_55, L_57, /*hidden argument*/List_1_AddRange_m3527912634_MethodInfo_var);
		}

IL_014b:
		{
			bool L_58 = Enumerator_MoveNext_m561126622((&V_1), /*hidden argument*/Enumerator_MoveNext_m561126622_MethodInfo_var);
			if (L_58)
			{
				goto IL_00c8;
			}
		}

IL_0157:
		{
			IL2CPP_LEAVE(0x16A, FINALLY_015c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_015c;
	}

FINALLY_015c:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m1699159283((&V_1), /*hidden argument*/Enumerator_Dispose_m1699159283_MethodInfo_var);
		IL2CPP_END_FINALLY(348)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(348)
	{
		IL2CPP_JUMP_TBL(0x16A, IL_016a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_016a:
	{
		List_1_t883301189 * L_59 = __this->get__steinerPoints_1();
		if (!L_59)
		{
			goto IL_0186;
		}
	}
	{
		TriangulationContext_t3528662164 * L_60 = ___tcx0;
		NullCheck(L_60);
		List_1_t883301189 * L_61 = L_60->get_Points_1();
		List_1_t883301189 * L_62 = __this->get__steinerPoints_1();
		NullCheck(L_61);
		List_1_AddRange_m3527912634(L_61, L_62, /*hidden argument*/List_1_AddRange_m3527912634_MethodInfo_var);
	}

IL_0186:
	{
		return;
	}
}
// System.Void Pathfinding.Poly2Tri.PolygonPoint::.ctor(System.Double,System.Double)
extern "C"  void PolygonPoint__ctor_m3285103561 (PolygonPoint_t2222827754 * __this, double ___x0, double ___y1, const MethodInfo* method)
{
	{
		double L_0 = ___x0;
		double L_1 = ___y1;
		TriangulationPoint__ctor_m1454340884(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// Pathfinding.Poly2Tri.PolygonPoint Pathfinding.Poly2Tri.PolygonPoint::get_Next()
extern "C"  PolygonPoint_t2222827754 * PolygonPoint_get_Next_m66309293 (PolygonPoint_t2222827754 * __this, const MethodInfo* method)
{
	{
		PolygonPoint_t2222827754 * L_0 = __this->get_U3CNextU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void Pathfinding.Poly2Tri.PolygonPoint::set_Next(Pathfinding.Poly2Tri.PolygonPoint)
extern "C"  void PolygonPoint_set_Next_m2943622558 (PolygonPoint_t2222827754 * __this, PolygonPoint_t2222827754 * ___value0, const MethodInfo* method)
{
	{
		PolygonPoint_t2222827754 * L_0 = ___value0;
		__this->set_U3CNextU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Void Pathfinding.Poly2Tri.PolygonPoint::set_Previous(Pathfinding.Poly2Tri.PolygonPoint)
extern "C"  void PolygonPoint_set_Previous_m2758999194 (PolygonPoint_t2222827754 * __this, PolygonPoint_t2222827754 * ___value0, const MethodInfo* method)
{
	{
		PolygonPoint_t2222827754 * L_0 = ___value0;
		__this->set_U3CPreviousU3Ek__BackingField_4(L_0);
		return;
	}
}
// System.Void Pathfinding.Poly2Tri.TriangulationConstraint::.ctor()
extern "C"  void TriangulationConstraint__ctor_m2519444399 (TriangulationConstraint_t137695394 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Pathfinding.Poly2Tri.TriangulationContext::.ctor()
extern Il2CppClass* List_1_t4203289139_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t883301189_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m2301005806_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m1886645521_MethodInfo_var;
extern const uint32_t TriangulationContext__ctor_m4137179115_MetadataUsageId;
extern "C"  void TriangulationContext__ctor_m4137179115 (TriangulationContext_t3528662164 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TriangulationContext__ctor_m4137179115_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t4203289139 * L_0 = (List_1_t4203289139 *)il2cpp_codegen_object_new(List_1_t4203289139_il2cpp_TypeInfo_var);
		List_1__ctor_m2301005806(L_0, /*hidden argument*/List_1__ctor_m2301005806_MethodInfo_var);
		__this->set_Triangles_0(L_0);
		List_1_t883301189 * L_1 = (List_1_t883301189 *)il2cpp_codegen_object_new(List_1_t883301189_il2cpp_TypeInfo_var);
		List_1__ctor_m1886645521(L_1, ((int32_t)200), /*hidden argument*/List_1__ctor_m1886645521_MethodInfo_var);
		__this->set_Points_1(L_1);
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// Pathfinding.Poly2Tri.TriangulationDebugContext Pathfinding.Poly2Tri.TriangulationContext::get_DebugContext()
extern "C"  TriangulationDebugContext_t3598090913 * TriangulationContext_get_DebugContext_m3106753707 (TriangulationContext_t3528662164 * __this, const MethodInfo* method)
{
	{
		TriangulationDebugContext_t3598090913 * L_0 = __this->get_U3CDebugContextU3Ek__BackingField_2();
		return L_0;
	}
}
// Pathfinding.Poly2Tri.TriangulationMode Pathfinding.Poly2Tri.TriangulationContext::get_TriangulationMode()
extern "C"  int32_t TriangulationContext_get_TriangulationMode_m3320394526 (TriangulationContext_t3528662164 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_U3CTriangulationModeU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void Pathfinding.Poly2Tri.TriangulationContext::set_TriangulationMode(Pathfinding.Poly2Tri.TriangulationMode)
extern "C"  void TriangulationContext_set_TriangulationMode_m1869986571 (TriangulationContext_t3528662164 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CTriangulationModeU3Ek__BackingField_3(L_0);
		return;
	}
}
// Pathfinding.Poly2Tri.Triangulatable Pathfinding.Poly2Tri.TriangulationContext::get_Triangulatable()
extern "C"  Il2CppObject * TriangulationContext_get_Triangulatable_m1563965268 (TriangulationContext_t3528662164 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U3CTriangulatableU3Ek__BackingField_4();
		return L_0;
	}
}
// System.Void Pathfinding.Poly2Tri.TriangulationContext::set_Triangulatable(Pathfinding.Poly2Tri.Triangulatable)
extern "C"  void TriangulationContext_set_Triangulatable_m1472563927 (TriangulationContext_t3528662164 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_U3CTriangulatableU3Ek__BackingField_4(L_0);
		return;
	}
}
// System.Int32 Pathfinding.Poly2Tri.TriangulationContext::get_StepCount()
extern "C"  int32_t TriangulationContext_get_StepCount_m1428097075 (TriangulationContext_t3528662164 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_U3CStepCountU3Ek__BackingField_5();
		return L_0;
	}
}
// System.Void Pathfinding.Poly2Tri.TriangulationContext::set_StepCount(System.Int32)
extern "C"  void TriangulationContext_set_StepCount_m1610772576 (TriangulationContext_t3528662164 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CStepCountU3Ek__BackingField_5(L_0);
		return;
	}
}
// System.Void Pathfinding.Poly2Tri.TriangulationContext::Done()
extern "C"  void TriangulationContext_Done_m1328379643 (TriangulationContext_t3528662164 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = TriangulationContext_get_StepCount_m1428097075(__this, /*hidden argument*/NULL);
		TriangulationContext_set_StepCount_m1610772576(__this, ((int32_t)((int32_t)L_0+(int32_t)1)), /*hidden argument*/NULL);
		return;
	}
}
// System.Void Pathfinding.Poly2Tri.TriangulationContext::PrepareTriangulation(Pathfinding.Poly2Tri.Triangulatable)
extern Il2CppClass* Triangulatable_t923279207_il2cpp_TypeInfo_var;
extern const uint32_t TriangulationContext_PrepareTriangulation_m1937826367_MetadataUsageId;
extern "C"  void TriangulationContext_PrepareTriangulation_m1937826367 (TriangulationContext_t3528662164 * __this, Il2CppObject * ___t0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TriangulationContext_PrepareTriangulation_m1937826367_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___t0;
		TriangulationContext_set_Triangulatable_m1472563927(__this, L_0, /*hidden argument*/NULL);
		Il2CppObject * L_1 = ___t0;
		NullCheck(L_1);
		int32_t L_2 = InterfaceFuncInvoker0< int32_t >::Invoke(3 /* Pathfinding.Poly2Tri.TriangulationMode Pathfinding.Poly2Tri.Triangulatable::get_TriangulationMode() */, Triangulatable_t923279207_il2cpp_TypeInfo_var, L_1);
		TriangulationContext_set_TriangulationMode_m1869986571(__this, L_2, /*hidden argument*/NULL);
		Il2CppObject * L_3 = ___t0;
		NullCheck(L_3);
		InterfaceActionInvoker1< TriangulationContext_t3528662164 * >::Invoke(0 /* System.Void Pathfinding.Poly2Tri.Triangulatable::Prepare(Pathfinding.Poly2Tri.TriangulationContext) */, Triangulatable_t923279207_il2cpp_TypeInfo_var, L_3, __this);
		return;
	}
}
// System.Void Pathfinding.Poly2Tri.TriangulationContext::Update(System.String)
extern "C"  void TriangulationContext_Update_m3089402304 (TriangulationContext_t3528662164 * __this, String_t* ___message0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Pathfinding.Poly2Tri.TriangulationContext::Clear()
extern const MethodInfo* List_1_Clear_m4212167659_MethodInfo_var;
extern const uint32_t TriangulationContext_Clear_m1543312406_MetadataUsageId;
extern "C"  void TriangulationContext_Clear_m1543312406 (TriangulationContext_t3528662164 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TriangulationContext_Clear_m1543312406_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t883301189 * L_0 = __this->get_Points_1();
		NullCheck(L_0);
		List_1_Clear_m4212167659(L_0, /*hidden argument*/List_1_Clear_m4212167659_MethodInfo_var);
		TriangulationDebugContext_t3598090913 * L_1 = TriangulationContext_get_DebugContext_m3106753707(__this, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		TriangulationDebugContext_t3598090913 * L_2 = TriangulationContext_get_DebugContext_m3106753707(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		VirtActionInvoker0::Invoke(4 /* System.Void Pathfinding.Poly2Tri.TriangulationDebugContext::Clear() */, L_2);
	}

IL_0021:
	{
		TriangulationContext_set_StepCount_m1610772576(__this, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Pathfinding.Poly2Tri.TriangulationContext::get_IsDebugEnabled()
extern "C"  bool TriangulationContext_get_IsDebugEnabled_m1815371940 (TriangulationContext_t3528662164 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_U3CIsDebugEnabledU3Ek__BackingField_6();
		return L_0;
	}
}
// Pathfinding.Poly2Tri.DTSweepDebugContext Pathfinding.Poly2Tri.TriangulationContext::get_DTDebugContext()
extern Il2CppClass* DTSweepDebugContext_t3829537966_il2cpp_TypeInfo_var;
extern const uint32_t TriangulationContext_get_DTDebugContext_m2409356104_MetadataUsageId;
extern "C"  DTSweepDebugContext_t3829537966 * TriangulationContext_get_DTDebugContext_m2409356104 (TriangulationContext_t3528662164 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TriangulationContext_get_DTDebugContext_m2409356104_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		TriangulationDebugContext_t3598090913 * L_0 = TriangulationContext_get_DebugContext_m3106753707(__this, /*hidden argument*/NULL);
		return ((DTSweepDebugContext_t3829537966 *)IsInstClass(L_0, DTSweepDebugContext_t3829537966_il2cpp_TypeInfo_var));
	}
}
// System.Void Pathfinding.Poly2Tri.TriangulationPoint::.ctor(System.Double,System.Double)
extern "C"  void TriangulationPoint__ctor_m1454340884 (TriangulationPoint_t3810082933 * __this, double ___x0, double ___y1, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		double L_0 = ___x0;
		__this->set_X_0(L_0);
		double L_1 = ___y1;
		__this->set_Y_1(L_1);
		return;
	}
}
// System.Collections.Generic.List`1<Pathfinding.Poly2Tri.DTSweepConstraint> Pathfinding.Poly2Tri.TriangulationPoint::get_Edges()
extern "C"  List_1_t433497279 * TriangulationPoint_get_Edges_m2952612962 (TriangulationPoint_t3810082933 * __this, const MethodInfo* method)
{
	{
		List_1_t433497279 * L_0 = __this->get_U3CEdgesU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void Pathfinding.Poly2Tri.TriangulationPoint::set_Edges(System.Collections.Generic.List`1<Pathfinding.Poly2Tri.DTSweepConstraint>)
extern "C"  void TriangulationPoint_set_Edges_m47341099 (TriangulationPoint_t3810082933 * __this, List_1_t433497279 * ___value0, const MethodInfo* method)
{
	{
		List_1_t433497279 * L_0 = ___value0;
		__this->set_U3CEdgesU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.String Pathfinding.Poly2Tri.TriangulationPoint::ToString()
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* Double_t3868226565_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral91;
extern Il2CppCodeGenString* _stringLiteral44;
extern Il2CppCodeGenString* _stringLiteral93;
extern const uint32_t TriangulationPoint_ToString_m2084010179_MetadataUsageId;
extern "C"  String_t* TriangulationPoint_ToString_m2084010179 (TriangulationPoint_t3810082933 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TriangulationPoint_ToString_m2084010179_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1108656482* L_0 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral91);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral91);
		ObjectU5BU5D_t1108656482* L_1 = L_0;
		double L_2 = __this->get_X_0();
		double L_3 = L_2;
		Il2CppObject * L_4 = Box(Double_t3868226565_il2cpp_TypeInfo_var, &L_3);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		ArrayElementTypeCheck (L_1, L_4);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_4);
		ObjectU5BU5D_t1108656482* L_5 = L_1;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 2);
		ArrayElementTypeCheck (L_5, _stringLiteral44);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral44);
		ObjectU5BU5D_t1108656482* L_6 = L_5;
		double L_7 = __this->get_Y_1();
		double L_8 = L_7;
		Il2CppObject * L_9 = Box(Double_t3868226565_il2cpp_TypeInfo_var, &L_8);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 3);
		ArrayElementTypeCheck (L_6, L_9);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_9);
		ObjectU5BU5D_t1108656482* L_10 = L_6;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 4);
		ArrayElementTypeCheck (L_10, _stringLiteral93);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral93);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = String_Concat_m3016520001(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		return L_11;
	}
}
// System.Void Pathfinding.Poly2Tri.TriangulationPoint::AddEdge(Pathfinding.Poly2Tri.DTSweepConstraint)
extern Il2CppClass* List_1_t433497279_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m3746587428_MethodInfo_var;
extern const MethodInfo* List_1_Add_m3440948244_MethodInfo_var;
extern const uint32_t TriangulationPoint_AddEdge_m2282912300_MetadataUsageId;
extern "C"  void TriangulationPoint_AddEdge_m2282912300 (TriangulationPoint_t3810082933 * __this, DTSweepConstraint_t3360279023 * ___e0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TriangulationPoint_AddEdge_m2282912300_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t433497279 * L_0 = TriangulationPoint_get_Edges_m2952612962(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		List_1_t433497279 * L_1 = (List_1_t433497279 *)il2cpp_codegen_object_new(List_1_t433497279_il2cpp_TypeInfo_var);
		List_1__ctor_m3746587428(L_1, /*hidden argument*/List_1__ctor_m3746587428_MethodInfo_var);
		TriangulationPoint_set_Edges_m47341099(__this, L_1, /*hidden argument*/NULL);
	}

IL_0016:
	{
		List_1_t433497279 * L_2 = TriangulationPoint_get_Edges_m2952612962(__this, /*hidden argument*/NULL);
		DTSweepConstraint_t3360279023 * L_3 = ___e0;
		NullCheck(L_2);
		List_1_Add_m3440948244(L_2, L_3, /*hidden argument*/List_1_Add_m3440948244_MethodInfo_var);
		return;
	}
}
// System.Boolean Pathfinding.Poly2Tri.TriangulationPoint::get_HasEdges()
extern "C"  bool TriangulationPoint_get_HasEdges_m1603684009 (TriangulationPoint_t3810082933 * __this, const MethodInfo* method)
{
	{
		List_1_t433497279 * L_0 = TriangulationPoint_get_Edges_m2952612962(__this, /*hidden argument*/NULL);
		return (bool)((((int32_t)((((Il2CppObject*)(List_1_t433497279 *)L_0) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void Pathfinding.Poly2Tri.TriangulationUtil::.cctor()
extern Il2CppClass* TriangulationUtil_t1170824903_il2cpp_TypeInfo_var;
extern const uint32_t TriangulationUtil__cctor_m3997719299_MetadataUsageId;
extern "C"  void TriangulationUtil__cctor_m3997719299 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TriangulationUtil__cctor_m3997719299_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		((TriangulationUtil_t1170824903_StaticFields*)TriangulationUtil_t1170824903_il2cpp_TypeInfo_var->static_fields)->set_EPSILON_0((1.0E-12));
		return;
	}
}
// System.Boolean Pathfinding.Poly2Tri.TriangulationUtil::SmartIncircle(Pathfinding.Poly2Tri.TriangulationPoint,Pathfinding.Poly2Tri.TriangulationPoint,Pathfinding.Poly2Tri.TriangulationPoint,Pathfinding.Poly2Tri.TriangulationPoint)
extern "C"  bool TriangulationUtil_SmartIncircle_m2900908198 (Il2CppObject * __this /* static, unused */, TriangulationPoint_t3810082933 * ___pa0, TriangulationPoint_t3810082933 * ___pb1, TriangulationPoint_t3810082933 * ___pc2, TriangulationPoint_t3810082933 * ___pd3, const MethodInfo* method)
{
	double V_0 = 0.0;
	double V_1 = 0.0;
	double V_2 = 0.0;
	double V_3 = 0.0;
	double V_4 = 0.0;
	double V_5 = 0.0;
	double V_6 = 0.0;
	double V_7 = 0.0;
	double V_8 = 0.0;
	double V_9 = 0.0;
	double V_10 = 0.0;
	double V_11 = 0.0;
	double V_12 = 0.0;
	double V_13 = 0.0;
	double V_14 = 0.0;
	double V_15 = 0.0;
	double V_16 = 0.0;
	double V_17 = 0.0;
	double V_18 = 0.0;
	double V_19 = 0.0;
	{
		TriangulationPoint_t3810082933 * L_0 = ___pd3;
		NullCheck(L_0);
		double L_1 = L_0->get_X_0();
		V_0 = L_1;
		TriangulationPoint_t3810082933 * L_2 = ___pd3;
		NullCheck(L_2);
		double L_3 = L_2->get_Y_1();
		V_1 = L_3;
		TriangulationPoint_t3810082933 * L_4 = ___pa0;
		NullCheck(L_4);
		double L_5 = L_4->get_X_0();
		double L_6 = V_0;
		V_2 = ((double)((double)L_5-(double)L_6));
		TriangulationPoint_t3810082933 * L_7 = ___pa0;
		NullCheck(L_7);
		double L_8 = L_7->get_Y_1();
		double L_9 = V_1;
		V_3 = ((double)((double)L_8-(double)L_9));
		TriangulationPoint_t3810082933 * L_10 = ___pb1;
		NullCheck(L_10);
		double L_11 = L_10->get_X_0();
		double L_12 = V_0;
		V_4 = ((double)((double)L_11-(double)L_12));
		TriangulationPoint_t3810082933 * L_13 = ___pb1;
		NullCheck(L_13);
		double L_14 = L_13->get_Y_1();
		double L_15 = V_1;
		V_5 = ((double)((double)L_14-(double)L_15));
		double L_16 = V_2;
		double L_17 = V_5;
		V_6 = ((double)((double)L_16*(double)L_17));
		double L_18 = V_4;
		double L_19 = V_3;
		V_7 = ((double)((double)L_18*(double)L_19));
		double L_20 = V_6;
		double L_21 = V_7;
		V_8 = ((double)((double)L_20-(double)L_21));
		double L_22 = V_8;
		if ((!(((double)L_22) <= ((double)(0.0)))))
		{
			goto IL_0059;
		}
	}
	{
		return (bool)0;
	}

IL_0059:
	{
		TriangulationPoint_t3810082933 * L_23 = ___pc2;
		NullCheck(L_23);
		double L_24 = L_23->get_X_0();
		double L_25 = V_0;
		V_9 = ((double)((double)L_24-(double)L_25));
		TriangulationPoint_t3810082933 * L_26 = ___pc2;
		NullCheck(L_26);
		double L_27 = L_26->get_Y_1();
		double L_28 = V_1;
		V_10 = ((double)((double)L_27-(double)L_28));
		double L_29 = V_9;
		double L_30 = V_3;
		V_11 = ((double)((double)L_29*(double)L_30));
		double L_31 = V_2;
		double L_32 = V_10;
		V_12 = ((double)((double)L_31*(double)L_32));
		double L_33 = V_11;
		double L_34 = V_12;
		V_13 = ((double)((double)L_33-(double)L_34));
		double L_35 = V_13;
		if ((!(((double)L_35) <= ((double)(0.0)))))
		{
			goto IL_0092;
		}
	}
	{
		return (bool)0;
	}

IL_0092:
	{
		double L_36 = V_4;
		double L_37 = V_10;
		V_14 = ((double)((double)L_36*(double)L_37));
		double L_38 = V_9;
		double L_39 = V_5;
		V_15 = ((double)((double)L_38*(double)L_39));
		double L_40 = V_2;
		double L_41 = V_2;
		double L_42 = V_3;
		double L_43 = V_3;
		V_16 = ((double)((double)((double)((double)L_40*(double)L_41))+(double)((double)((double)L_42*(double)L_43))));
		double L_44 = V_4;
		double L_45 = V_4;
		double L_46 = V_5;
		double L_47 = V_5;
		V_17 = ((double)((double)((double)((double)L_44*(double)L_45))+(double)((double)((double)L_46*(double)L_47))));
		double L_48 = V_9;
		double L_49 = V_9;
		double L_50 = V_10;
		double L_51 = V_10;
		V_18 = ((double)((double)((double)((double)L_48*(double)L_49))+(double)((double)((double)L_50*(double)L_51))));
		double L_52 = V_16;
		double L_53 = V_14;
		double L_54 = V_15;
		double L_55 = V_17;
		double L_56 = V_13;
		double L_57 = V_18;
		double L_58 = V_8;
		V_19 = ((double)((double)((double)((double)((double)((double)L_52*(double)((double)((double)L_53-(double)L_54))))+(double)((double)((double)L_55*(double)L_56))))+(double)((double)((double)L_57*(double)L_58))));
		double L_59 = V_19;
		return (bool)((((double)L_59) > ((double)(0.0)))? 1 : 0);
	}
}
// System.Boolean Pathfinding.Poly2Tri.TriangulationUtil::InScanArea(Pathfinding.Poly2Tri.TriangulationPoint,Pathfinding.Poly2Tri.TriangulationPoint,Pathfinding.Poly2Tri.TriangulationPoint,Pathfinding.Poly2Tri.TriangulationPoint)
extern "C"  bool TriangulationUtil_InScanArea_m680109047 (Il2CppObject * __this /* static, unused */, TriangulationPoint_t3810082933 * ___pa0, TriangulationPoint_t3810082933 * ___pb1, TriangulationPoint_t3810082933 * ___pc2, TriangulationPoint_t3810082933 * ___pd3, const MethodInfo* method)
{
	double V_0 = 0.0;
	double V_1 = 0.0;
	double V_2 = 0.0;
	double V_3 = 0.0;
	double V_4 = 0.0;
	double V_5 = 0.0;
	double V_6 = 0.0;
	double V_7 = 0.0;
	double V_8 = 0.0;
	double V_9 = 0.0;
	double V_10 = 0.0;
	double V_11 = 0.0;
	double V_12 = 0.0;
	double V_13 = 0.0;
	{
		TriangulationPoint_t3810082933 * L_0 = ___pd3;
		NullCheck(L_0);
		double L_1 = L_0->get_X_0();
		V_0 = L_1;
		TriangulationPoint_t3810082933 * L_2 = ___pd3;
		NullCheck(L_2);
		double L_3 = L_2->get_Y_1();
		V_1 = L_3;
		TriangulationPoint_t3810082933 * L_4 = ___pa0;
		NullCheck(L_4);
		double L_5 = L_4->get_X_0();
		double L_6 = V_0;
		V_2 = ((double)((double)L_5-(double)L_6));
		TriangulationPoint_t3810082933 * L_7 = ___pa0;
		NullCheck(L_7);
		double L_8 = L_7->get_Y_1();
		double L_9 = V_1;
		V_3 = ((double)((double)L_8-(double)L_9));
		TriangulationPoint_t3810082933 * L_10 = ___pb1;
		NullCheck(L_10);
		double L_11 = L_10->get_X_0();
		double L_12 = V_0;
		V_4 = ((double)((double)L_11-(double)L_12));
		TriangulationPoint_t3810082933 * L_13 = ___pb1;
		NullCheck(L_13);
		double L_14 = L_13->get_Y_1();
		double L_15 = V_1;
		V_5 = ((double)((double)L_14-(double)L_15));
		double L_16 = V_2;
		double L_17 = V_5;
		V_6 = ((double)((double)L_16*(double)L_17));
		double L_18 = V_4;
		double L_19 = V_3;
		V_7 = ((double)((double)L_18*(double)L_19));
		double L_20 = V_6;
		double L_21 = V_7;
		V_8 = ((double)((double)L_20-(double)L_21));
		double L_22 = V_8;
		if ((!(((double)L_22) <= ((double)(0.0)))))
		{
			goto IL_0059;
		}
	}
	{
		return (bool)0;
	}

IL_0059:
	{
		TriangulationPoint_t3810082933 * L_23 = ___pc2;
		NullCheck(L_23);
		double L_24 = L_23->get_X_0();
		double L_25 = V_0;
		V_9 = ((double)((double)L_24-(double)L_25));
		TriangulationPoint_t3810082933 * L_26 = ___pc2;
		NullCheck(L_26);
		double L_27 = L_26->get_Y_1();
		double L_28 = V_1;
		V_10 = ((double)((double)L_27-(double)L_28));
		double L_29 = V_9;
		double L_30 = V_3;
		V_11 = ((double)((double)L_29*(double)L_30));
		double L_31 = V_2;
		double L_32 = V_10;
		V_12 = ((double)((double)L_31*(double)L_32));
		double L_33 = V_11;
		double L_34 = V_12;
		V_13 = ((double)((double)L_33-(double)L_34));
		double L_35 = V_13;
		if ((!(((double)L_35) <= ((double)(0.0)))))
		{
			goto IL_0092;
		}
	}
	{
		return (bool)0;
	}

IL_0092:
	{
		return (bool)1;
	}
}
// Pathfinding.Poly2Tri.Orientation Pathfinding.Poly2Tri.TriangulationUtil::Orient2d(Pathfinding.Poly2Tri.TriangulationPoint,Pathfinding.Poly2Tri.TriangulationPoint,Pathfinding.Poly2Tri.TriangulationPoint)
extern Il2CppClass* TriangulationUtil_t1170824903_il2cpp_TypeInfo_var;
extern const uint32_t TriangulationUtil_Orient2d_m3842222219_MetadataUsageId;
extern "C"  int32_t TriangulationUtil_Orient2d_m3842222219 (Il2CppObject * __this /* static, unused */, TriangulationPoint_t3810082933 * ___pa0, TriangulationPoint_t3810082933 * ___pb1, TriangulationPoint_t3810082933 * ___pc2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TriangulationUtil_Orient2d_m3842222219_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	double V_0 = 0.0;
	double V_1 = 0.0;
	double V_2 = 0.0;
	{
		TriangulationPoint_t3810082933 * L_0 = ___pa0;
		NullCheck(L_0);
		double L_1 = L_0->get_X_0();
		TriangulationPoint_t3810082933 * L_2 = ___pc2;
		NullCheck(L_2);
		double L_3 = L_2->get_X_0();
		TriangulationPoint_t3810082933 * L_4 = ___pb1;
		NullCheck(L_4);
		double L_5 = L_4->get_Y_1();
		TriangulationPoint_t3810082933 * L_6 = ___pc2;
		NullCheck(L_6);
		double L_7 = L_6->get_Y_1();
		V_0 = ((double)((double)((double)((double)L_1-(double)L_3))*(double)((double)((double)L_5-(double)L_7))));
		TriangulationPoint_t3810082933 * L_8 = ___pa0;
		NullCheck(L_8);
		double L_9 = L_8->get_Y_1();
		TriangulationPoint_t3810082933 * L_10 = ___pc2;
		NullCheck(L_10);
		double L_11 = L_10->get_Y_1();
		TriangulationPoint_t3810082933 * L_12 = ___pb1;
		NullCheck(L_12);
		double L_13 = L_12->get_X_0();
		TriangulationPoint_t3810082933 * L_14 = ___pc2;
		NullCheck(L_14);
		double L_15 = L_14->get_X_0();
		V_1 = ((double)((double)((double)((double)L_9-(double)L_11))*(double)((double)((double)L_13-(double)L_15))));
		double L_16 = V_0;
		double L_17 = V_1;
		V_2 = ((double)((double)L_16-(double)L_17));
		double L_18 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(TriangulationUtil_t1170824903_il2cpp_TypeInfo_var);
		double L_19 = ((TriangulationUtil_t1170824903_StaticFields*)TriangulationUtil_t1170824903_il2cpp_TypeInfo_var->static_fields)->get_EPSILON_0();
		if ((!(((double)L_18) > ((double)((-L_19))))))
		{
			goto IL_0055;
		}
	}
	{
		double L_20 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(TriangulationUtil_t1170824903_il2cpp_TypeInfo_var);
		double L_21 = ((TriangulationUtil_t1170824903_StaticFields*)TriangulationUtil_t1170824903_il2cpp_TypeInfo_var->static_fields)->get_EPSILON_0();
		if ((!(((double)L_20) < ((double)L_21))))
		{
			goto IL_0055;
		}
	}
	{
		return (int32_t)(2);
	}

IL_0055:
	{
		double L_22 = V_2;
		if ((!(((double)L_22) > ((double)(0.0)))))
		{
			goto IL_0066;
		}
	}
	{
		return (int32_t)(1);
	}

IL_0066:
	{
		return (int32_t)(0);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
