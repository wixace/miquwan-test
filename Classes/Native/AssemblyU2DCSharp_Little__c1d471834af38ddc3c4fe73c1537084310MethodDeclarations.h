﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._c1d471834af38ddc3c4fe73c461aabd2
struct _c1d471834af38ddc3c4fe73c461aabd2_t1537084310;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._c1d471834af38ddc3c4fe73c461aabd2::.ctor()
extern "C"  void _c1d471834af38ddc3c4fe73c461aabd2__ctor_m3704210263 (_c1d471834af38ddc3c4fe73c461aabd2_t1537084310 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._c1d471834af38ddc3c4fe73c461aabd2::_c1d471834af38ddc3c4fe73c461aabd2m2(System.Int32)
extern "C"  int32_t _c1d471834af38ddc3c4fe73c461aabd2__c1d471834af38ddc3c4fe73c461aabd2m2_m3479026137 (_c1d471834af38ddc3c4fe73c461aabd2_t1537084310 * __this, int32_t ____c1d471834af38ddc3c4fe73c461aabd2a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._c1d471834af38ddc3c4fe73c461aabd2::_c1d471834af38ddc3c4fe73c461aabd2m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _c1d471834af38ddc3c4fe73c461aabd2__c1d471834af38ddc3c4fe73c461aabd2m_m629716221 (_c1d471834af38ddc3c4fe73c461aabd2_t1537084310 * __this, int32_t ____c1d471834af38ddc3c4fe73c461aabd2a0, int32_t ____c1d471834af38ddc3c4fe73c461aabd2361, int32_t ____c1d471834af38ddc3c4fe73c461aabd2c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
