﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Converters.RegexConverter
struct RegexConverter_t1616016887;
// Newtonsoft.Json.JsonWriter
struct JsonWriter_t972330355;
// System.Object
struct Il2CppObject;
// Newtonsoft.Json.JsonSerializer
struct JsonSerializer_t251850770;
// Newtonsoft.Json.Bson.BsonWriter
struct BsonWriter_t2987529331;
// System.Text.RegularExpressions.Regex
struct Regex_t2161232213;
// Newtonsoft.Json.JsonReader
struct JsonReader_t816925123;
// System.Type
struct Type_t;
// Newtonsoft.Json.Bson.BsonReader
struct BsonReader_t2832124099;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonWriter972330355.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonSerializer251850770.h"
#include "System_System_Text_RegularExpressions_RegexOptions3066443743.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Bson_BsonWriter2987529331.h"
#include "System_System_Text_RegularExpressions_Regex2161232213.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonReader816925123.h"
#include "mscorlib_System_Type2863145774.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Bson_BsonReader2832124099.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Converters_Regex1616016887.h"
#include "mscorlib_System_String7231557.h"

// System.Void Newtonsoft.Json.Converters.RegexConverter::.ctor()
extern "C"  void RegexConverter__ctor_m2715490956 (RegexConverter_t1616016887 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Converters.RegexConverter::WriteJson(Newtonsoft.Json.JsonWriter,System.Object,Newtonsoft.Json.JsonSerializer)
extern "C"  void RegexConverter_WriteJson_m2298162504 (RegexConverter_t1616016887 * __this, JsonWriter_t972330355 * ___writer0, Il2CppObject * ___value1, JsonSerializer_t251850770 * ___serializer2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Converters.RegexConverter::HasFlag(System.Text.RegularExpressions.RegexOptions,System.Text.RegularExpressions.RegexOptions)
extern "C"  bool RegexConverter_HasFlag_m2302150034 (RegexConverter_t1616016887 * __this, int32_t ___options0, int32_t ___flag1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Converters.RegexConverter::WriteBson(Newtonsoft.Json.Bson.BsonWriter,System.Text.RegularExpressions.Regex)
extern "C"  void RegexConverter_WriteBson_m2549815103 (RegexConverter_t1616016887 * __this, BsonWriter_t2987529331 * ___writer0, Regex_t2161232213 * ___regex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Converters.RegexConverter::WriteJson(Newtonsoft.Json.JsonWriter,System.Text.RegularExpressions.Regex)
extern "C"  void RegexConverter_WriteJson_m176754315 (RegexConverter_t1616016887 * __this, JsonWriter_t972330355 * ___writer0, Regex_t2161232213 * ___regex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Converters.RegexConverter::ReadJson(Newtonsoft.Json.JsonReader,System.Type,System.Object,Newtonsoft.Json.JsonSerializer)
extern "C"  Il2CppObject * RegexConverter_ReadJson_m1236088661 (RegexConverter_t1616016887 * __this, JsonReader_t816925123 * ___reader0, Type_t * ___objectType1, Il2CppObject * ___existingValue2, JsonSerializer_t251850770 * ___serializer3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Converters.RegexConverter::ReadBson(Newtonsoft.Json.Bson.BsonReader)
extern "C"  Il2CppObject * RegexConverter_ReadBson_m536711437 (RegexConverter_t1616016887 * __this, BsonReader_t2832124099 * ___reader0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.RegularExpressions.Regex Newtonsoft.Json.Converters.RegexConverter::ReadJson(Newtonsoft.Json.JsonReader)
extern "C"  Regex_t2161232213 * RegexConverter_ReadJson_m32288499 (RegexConverter_t1616016887 * __this, JsonReader_t816925123 * ___reader0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Converters.RegexConverter::CanConvert(System.Type)
extern "C"  bool RegexConverter_CanConvert_m1888658258 (RegexConverter_t1616016887 * __this, Type_t * ___objectType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Converters.RegexConverter::ilo_HasFlag1(Newtonsoft.Json.Converters.RegexConverter,System.Text.RegularExpressions.RegexOptions,System.Text.RegularExpressions.RegexOptions)
extern "C"  bool RegexConverter_ilo_HasFlag1_m3625815713 (Il2CppObject * __this /* static, unused */, RegexConverter_t1616016887 * ____this0, int32_t ___options1, int32_t ___flag2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Converters.RegexConverter::ilo_WritePropertyName2(Newtonsoft.Json.JsonWriter,System.String)
extern "C"  void RegexConverter_ilo_WritePropertyName2_m357121706 (Il2CppObject * __this /* static, unused */, JsonWriter_t972330355 * ____this0, String_t* ___name1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Converters.RegexConverter::ilo_ReadBson3(Newtonsoft.Json.Converters.RegexConverter,Newtonsoft.Json.Bson.BsonReader)
extern "C"  Il2CppObject * RegexConverter_ilo_ReadBson3_m380130442 (Il2CppObject * __this /* static, unused */, RegexConverter_t1616016887 * ____this0, BsonReader_t2832124099 * ___reader1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Converters.RegexConverter::ilo_get_Value4(Newtonsoft.Json.JsonReader)
extern "C"  Il2CppObject * RegexConverter_ilo_get_Value4_m1411295970 (Il2CppObject * __this /* static, unused */, JsonReader_t816925123 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Converters.RegexConverter::ilo_Read5(Newtonsoft.Json.JsonReader)
extern "C"  bool RegexConverter_ilo_Read5_m2386028682 (Il2CppObject * __this /* static, unused */, JsonReader_t816925123 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
