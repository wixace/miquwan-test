﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.AstarData/<GetUpdateableGraphs>c__Iterator9
struct U3CGetUpdateableGraphsU3Ec__Iterator9_t4058775880;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t1787714124;

#include "codegen/il2cpp-codegen.h"

// System.Void Pathfinding.AstarData/<GetUpdateableGraphs>c__Iterator9::.ctor()
extern "C"  void U3CGetUpdateableGraphsU3Ec__Iterator9__ctor_m1113960227 (U3CGetUpdateableGraphsU3Ec__Iterator9_t4058775880 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Pathfinding.AstarData/<GetUpdateableGraphs>c__Iterator9::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CGetUpdateableGraphsU3Ec__Iterator9_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1838621455 (U3CGetUpdateableGraphsU3Ec__Iterator9_t4058775880 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Pathfinding.AstarData/<GetUpdateableGraphs>c__Iterator9::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetUpdateableGraphsU3Ec__Iterator9_System_Collections_IEnumerator_get_Current_m3577160867 (U3CGetUpdateableGraphsU3Ec__Iterator9_t4058775880 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Pathfinding.AstarData/<GetUpdateableGraphs>c__Iterator9::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CGetUpdateableGraphsU3Ec__Iterator9_System_Collections_IEnumerable_GetEnumerator_m2429895390 (U3CGetUpdateableGraphsU3Ec__Iterator9_t4058775880 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<System.Object> Pathfinding.AstarData/<GetUpdateableGraphs>c__Iterator9::System.Collections.Generic.IEnumerable<object>.GetEnumerator()
extern "C"  Il2CppObject* U3CGetUpdateableGraphsU3Ec__Iterator9_System_Collections_Generic_IEnumerableU3CobjectU3E_GetEnumerator_m2977046956 (U3CGetUpdateableGraphsU3Ec__Iterator9_t4058775880 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.AstarData/<GetUpdateableGraphs>c__Iterator9::MoveNext()
extern "C"  bool U3CGetUpdateableGraphsU3Ec__Iterator9_MoveNext_m346903537 (U3CGetUpdateableGraphsU3Ec__Iterator9_t4058775880 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.AstarData/<GetUpdateableGraphs>c__Iterator9::Dispose()
extern "C"  void U3CGetUpdateableGraphsU3Ec__Iterator9_Dispose_m967204512 (U3CGetUpdateableGraphsU3Ec__Iterator9_t4058775880 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.AstarData/<GetUpdateableGraphs>c__Iterator9::Reset()
extern "C"  void U3CGetUpdateableGraphsU3Ec__Iterator9_Reset_m3055360464 (U3CGetUpdateableGraphsU3Ec__Iterator9_t4058775880 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
