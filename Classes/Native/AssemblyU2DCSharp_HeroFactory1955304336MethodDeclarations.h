﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HeroFactory
struct HeroFactory_t1955304336;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void HeroFactory::.ctor()
extern "C"  void HeroFactory__ctor_m1615129051 (HeroFactory_t1955304336 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroFactory::.cctor()
extern "C"  void HeroFactory__cctor_m2342264114 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HeroFactory::Create(System.Int32,System.Int32)
extern "C"  String_t* HeroFactory_Create_m1983749028 (Il2CppObject * __this /* static, unused */, int32_t ___id0, int32_t ___atktype1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
