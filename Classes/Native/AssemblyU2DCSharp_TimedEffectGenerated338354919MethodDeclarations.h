﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TimedEffectGenerated
struct TimedEffectGenerated_t338354919;
// JSVCall
struct JSVCall_t3708497963;
// System.Action
struct Action_t3771233898;
// CSRepresentedObject
struct CSRepresentedObject_t3994124630;
// System.Object
struct Il2CppObject;
// TimedEffect
struct TimedEffect_t1335807880;
// System.Delegate
struct Delegate_t3310234105;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_CSRepresentedObject3994124630.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_TimedEffect1335807880.h"
#include "mscorlib_System_Delegate3310234105.h"
#include "System_Core_System_Action3771233898.h"

// System.Void TimedEffectGenerated::.ctor()
extern "C"  void TimedEffectGenerated__ctor_m1449184148 (TimedEffectGenerated_t338354919 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TimedEffectGenerated::TimedEffect_TimedEffect1(JSVCall,System.Int32)
extern "C"  bool TimedEffectGenerated_TimedEffect_TimedEffect1_m2427492820 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TimedEffectGenerated::TimedEffect_lasttime(JSVCall)
extern "C"  void TimedEffectGenerated_TimedEffect_lasttime_m422173341 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TimedEffectGenerated::TimedEffect_delaytime(JSVCall)
extern "C"  void TimedEffectGenerated_TimedEffect_delaytime_m3804590044 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TimedEffectGenerated::TimedEffect_DestroyGameObjcet(JSVCall,System.Int32)
extern "C"  bool TimedEffectGenerated_TimedEffect_DestroyGameObjcet_m1378504766 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Action TimedEffectGenerated::TimedEffect_SetData_GetDelegate_member1_arg0(CSRepresentedObject)
extern "C"  Action_t3771233898 * TimedEffectGenerated_TimedEffect_SetData_GetDelegate_member1_arg0_m2003869790 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TimedEffectGenerated::TimedEffect_SetData__Action(JSVCall,System.Int32)
extern "C"  bool TimedEffectGenerated_TimedEffect_SetData__Action_m3929021113 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TimedEffectGenerated::__Register()
extern "C"  void TimedEffectGenerated___Register_m1872022963 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Action TimedEffectGenerated::<TimedEffect_SetData__Action>m__FC()
extern "C"  Action_t3771233898 * TimedEffectGenerated_U3CTimedEffect_SetData__ActionU3Em__FC_m3088890313 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TimedEffectGenerated::ilo_attachFinalizerObject1(System.Int32)
extern "C"  bool TimedEffectGenerated_ilo_attachFinalizerObject1_m2259563723 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TimedEffectGenerated::ilo_addJSCSRel2(System.Int32,System.Object)
extern "C"  void TimedEffectGenerated_ilo_addJSCSRel2_m2398948561 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TimedEffectGenerated::ilo_getSingle3(System.Int32)
extern "C"  float TimedEffectGenerated_ilo_getSingle3_m1674498197 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TimedEffectGenerated::ilo_set_lasttime4(TimedEffect,System.Single)
extern "C"  void TimedEffectGenerated_ilo_set_lasttime4_m699347932 (Il2CppObject * __this /* static, unused */, TimedEffect_t1335807880 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TimedEffectGenerated::ilo_setSingle5(System.Int32,System.Single)
extern "C"  void TimedEffectGenerated_ilo_setSingle5_m1237751396 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TimedEffectGenerated::ilo_set_delaytime6(TimedEffect,System.Single)
extern "C"  void TimedEffectGenerated_ilo_set_delaytime6_m2142227945 (Il2CppObject * __this /* static, unused */, TimedEffect_t1335807880 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TimedEffectGenerated::ilo_addJSFunCSDelegateRel7(System.Int32,System.Delegate)
extern "C"  void TimedEffectGenerated_ilo_addJSFunCSDelegateRel7_m578370372 (Il2CppObject * __this /* static, unused */, int32_t ___funID0, Delegate_t3310234105 * ___del1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TimedEffectGenerated::ilo_SetData8(TimedEffect,System.Action)
extern "C"  void TimedEffectGenerated_ilo_SetData8_m2509526866 (Il2CppObject * __this /* static, unused */, TimedEffect_t1335807880 * ____this0, Action_t3771233898 * ___timeOutCall1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CSRepresentedObject TimedEffectGenerated::ilo_getFunctionS9(System.Int32)
extern "C"  CSRepresentedObject_t3994124630 * TimedEffectGenerated_ilo_getFunctionS9_m1498522311 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Action TimedEffectGenerated::ilo_TimedEffect_SetData_GetDelegate_member1_arg010(CSRepresentedObject)
extern "C"  Action_t3771233898 * TimedEffectGenerated_ilo_TimedEffect_SetData_GetDelegate_member1_arg010_m3178827340 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
