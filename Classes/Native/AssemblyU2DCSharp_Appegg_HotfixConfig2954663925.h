﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Appegg/HotfixConfig
struct  HotfixConfig_t2954663925  : public Il2CppObject
{
public:
	// System.String Appegg/HotfixConfig::catalogUrl
	String_t* ___catalogUrl_0;
	// System.String Appegg/HotfixConfig::dllUrl
	String_t* ___dllUrl_1;
	// System.String Appegg/HotfixConfig::dllMD5
	String_t* ___dllMD5_2;
	// System.String Appegg/HotfixConfig::addressableScene
	String_t* ___addressableScene_3;
	// System.String Appegg/HotfixConfig::addressablePrefab
	String_t* ___addressablePrefab_4;

public:
	inline static int32_t get_offset_of_catalogUrl_0() { return static_cast<int32_t>(offsetof(HotfixConfig_t2954663925, ___catalogUrl_0)); }
	inline String_t* get_catalogUrl_0() const { return ___catalogUrl_0; }
	inline String_t** get_address_of_catalogUrl_0() { return &___catalogUrl_0; }
	inline void set_catalogUrl_0(String_t* value)
	{
		___catalogUrl_0 = value;
		Il2CppCodeGenWriteBarrier(&___catalogUrl_0, value);
	}

	inline static int32_t get_offset_of_dllUrl_1() { return static_cast<int32_t>(offsetof(HotfixConfig_t2954663925, ___dllUrl_1)); }
	inline String_t* get_dllUrl_1() const { return ___dllUrl_1; }
	inline String_t** get_address_of_dllUrl_1() { return &___dllUrl_1; }
	inline void set_dllUrl_1(String_t* value)
	{
		___dllUrl_1 = value;
		Il2CppCodeGenWriteBarrier(&___dllUrl_1, value);
	}

	inline static int32_t get_offset_of_dllMD5_2() { return static_cast<int32_t>(offsetof(HotfixConfig_t2954663925, ___dllMD5_2)); }
	inline String_t* get_dllMD5_2() const { return ___dllMD5_2; }
	inline String_t** get_address_of_dllMD5_2() { return &___dllMD5_2; }
	inline void set_dllMD5_2(String_t* value)
	{
		___dllMD5_2 = value;
		Il2CppCodeGenWriteBarrier(&___dllMD5_2, value);
	}

	inline static int32_t get_offset_of_addressableScene_3() { return static_cast<int32_t>(offsetof(HotfixConfig_t2954663925, ___addressableScene_3)); }
	inline String_t* get_addressableScene_3() const { return ___addressableScene_3; }
	inline String_t** get_address_of_addressableScene_3() { return &___addressableScene_3; }
	inline void set_addressableScene_3(String_t* value)
	{
		___addressableScene_3 = value;
		Il2CppCodeGenWriteBarrier(&___addressableScene_3, value);
	}

	inline static int32_t get_offset_of_addressablePrefab_4() { return static_cast<int32_t>(offsetof(HotfixConfig_t2954663925, ___addressablePrefab_4)); }
	inline String_t* get_addressablePrefab_4() const { return ___addressablePrefab_4; }
	inline String_t** get_address_of_addressablePrefab_4() { return &___addressablePrefab_4; }
	inline void set_addressablePrefab_4(String_t* value)
	{
		___addressablePrefab_4 = value;
		Il2CppCodeGenWriteBarrier(&___addressablePrefab_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
