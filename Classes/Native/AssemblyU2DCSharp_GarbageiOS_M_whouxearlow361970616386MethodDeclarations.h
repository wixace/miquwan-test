﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_whouxearlow36
struct M_whouxearlow36_t1970616386;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_whouxearlow361970616386.h"

// System.Void GarbageiOS.M_whouxearlow36::.ctor()
extern "C"  void M_whouxearlow36__ctor_m1727678305 (M_whouxearlow36_t1970616386 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_whouxearlow36::M_salsairnu0(System.String[],System.Int32)
extern "C"  void M_whouxearlow36_M_salsairnu0_m3280281440 (M_whouxearlow36_t1970616386 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_whouxearlow36::M_borrallrer1(System.String[],System.Int32)
extern "C"  void M_whouxearlow36_M_borrallrer1_m1919022602 (M_whouxearlow36_t1970616386 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_whouxearlow36::M_ferema2(System.String[],System.Int32)
extern "C"  void M_whouxearlow36_M_ferema2_m3283826064 (M_whouxearlow36_t1970616386 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_whouxearlow36::M_tecoutallTowbou3(System.String[],System.Int32)
extern "C"  void M_whouxearlow36_M_tecoutallTowbou3_m2443721326 (M_whouxearlow36_t1970616386 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_whouxearlow36::ilo_M_salsairnu01(GarbageiOS.M_whouxearlow36,System.String[],System.Int32)
extern "C"  void M_whouxearlow36_ilo_M_salsairnu01_m3851240160 (Il2CppObject * __this /* static, unused */, M_whouxearlow36_t1970616386 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_whouxearlow36::ilo_M_ferema22(GarbageiOS.M_whouxearlow36,System.String[],System.Int32)
extern "C"  void M_whouxearlow36_ilo_M_ferema22_m3082583849 (Il2CppObject * __this /* static, unused */, M_whouxearlow36_t1970616386 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
