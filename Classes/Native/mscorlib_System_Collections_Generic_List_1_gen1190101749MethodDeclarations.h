﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1244034627MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1<CSGuideVO>::.ctor()
#define List_1__ctor_m2450036484(__this, method) ((  void (*) (List_1_t1190101749 *, const MethodInfo*))List_1__ctor_m1239231859_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<CSGuideVO>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m3882904196(__this, ___collection0, method) ((  void (*) (List_1_t1190101749 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m1160795371_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<CSGuideVO>::.ctor(System.Int32)
#define List_1__ctor_m843850732(__this, ___capacity0, method) ((  void (*) (List_1_t1190101749 *, int32_t, const MethodInfo*))List_1__ctor_m3643386469_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<CSGuideVO>::.cctor()
#define List_1__cctor_m1138257906(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m3826137881_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<CSGuideVO>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1811058093(__this, method) ((  Il2CppObject* (*) (List_1_t1190101749 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2808422246_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<CSGuideVO>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m1452027529(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t1190101749 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m4034025648_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<CSGuideVO>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m2941583044(__this, method) ((  Il2CppObject * (*) (List_1_t1190101749 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m1841330603_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<CSGuideVO>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m2358488493(__this, ___item0, method) ((  int32_t (*) (List_1_t1190101749 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m3794749222_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<CSGuideVO>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m3843096895(__this, ___item0, method) ((  bool (*) (List_1_t1190101749 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m2659633254_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<CSGuideVO>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m1237009797(__this, ___item0, method) ((  int32_t (*) (List_1_t1190101749 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m3431692926_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<CSGuideVO>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m1730683952(__this, ___index0, ___item1, method) ((  void (*) (List_1_t1190101749 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m2067529129_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<CSGuideVO>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m777287416(__this, ___item0, method) ((  void (*) (List_1_t1190101749 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m1644145887_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<CSGuideVO>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2893524608(__this, method) ((  bool (*) (List_1_t1190101749 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1299706087_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<CSGuideVO>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m3046332861(__this, method) ((  bool (*) (List_1_t1190101749 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m3867536694_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<CSGuideVO>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m2583248873(__this, method) ((  Il2CppObject * (*) (List_1_t1190101749 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m4244374434_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<CSGuideVO>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m1061571566(__this, method) ((  bool (*) (List_1_t1190101749 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m432946261_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<CSGuideVO>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m2289368395(__this, method) ((  bool (*) (List_1_t1190101749 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m2961826820_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<CSGuideVO>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m1076681520(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t1190101749 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m3985478825_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<CSGuideVO>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m1648886791(__this, ___index0, ___value1, method) ((  void (*) (List_1_t1190101749 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m3234554688_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<CSGuideVO>::Add(T)
#define List_1_Add_m2144397300(__this, ___item0, method) ((  void (*) (List_1_t1190101749 *, CSGuideVO_t4116883493 *, const MethodInfo*))List_1_Add_m933592675_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<CSGuideVO>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m2823550527(__this, ___newCount0, method) ((  void (*) (List_1_t1190101749 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m4122600870_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<CSGuideVO>::CheckRange(System.Int32,System.Int32)
#define List_1_CheckRange_m887103560(__this, ___idx0, ___count1, method) ((  void (*) (List_1_t1190101749 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m590371457_gshared)(__this, ___idx0, ___count1, method)
// System.Void System.Collections.Generic.List`1<CSGuideVO>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m805015869(__this, ___collection0, method) ((  void (*) (List_1_t1190101749 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m2478449828_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<CSGuideVO>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m65988093(__this, ___enumerable0, method) ((  void (*) (List_1_t1190101749 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m1739422052_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<CSGuideVO>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m3295641210(__this, ___collection0, method) ((  void (*) (List_1_t1190101749 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m2229151411_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<CSGuideVO>::AsReadOnly()
#define List_1_AsReadOnly_m176910191(__this, method) ((  ReadOnlyCollection_1_t1378993733 * (*) (List_1_t1190101749 *, const MethodInfo*))List_1_AsReadOnly_m769820182_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<CSGuideVO>::BinarySearch(T)
#define List_1_BinarySearch_m3376185200(__this, ___item0, method) ((  int32_t (*) (List_1_t1190101749 *, CSGuideVO_t4116883493 *, const MethodInfo*))List_1_BinarySearch_m2761349225_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<CSGuideVO>::Clear()
#define List_1_Clear_m1060633414(__this, method) ((  void (*) (List_1_t1190101749 *, const MethodInfo*))List_1_Clear_m2940332446_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<CSGuideVO>::Contains(T)
#define List_1_Contains_m1963226228(__this, ___item0, method) ((  bool (*) (List_1_t1190101749 *, CSGuideVO_t4116883493 *, const MethodInfo*))List_1_Contains_m4186092781_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<CSGuideVO>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m1519968500(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t1190101749 *, CSGuideVOU5BU5D_t2582593960*, int32_t, const MethodInfo*))List_1_CopyTo_m3988356635_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<CSGuideVO>::Find(System.Predicate`1<T>)
#define List_1_Find_m496515828(__this, ___match0, method) ((  CSGuideVO_t4116883493 * (*) (List_1_t1190101749 *, Predicate_1_t3727940376 *, const MethodInfo*))List_1_Find_m3379773421_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<CSGuideVO>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m2544177263(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t3727940376 *, const MethodInfo*))List_1_CheckMatch_m3390394152_gshared)(__this /* static, unused */, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<CSGuideVO>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m2035111060(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t1190101749 *, int32_t, int32_t, Predicate_1_t3727940376 *, const MethodInfo*))List_1_GetIndex_m4275988045_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<CSGuideVO>::GetEnumerator()
#define List_1_GetEnumerator_m2001768369(__this, method) ((  Enumerator_t1209774519  (*) (List_1_t1190101749 *, const MethodInfo*))List_1_GetEnumerator_m1919240000_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<CSGuideVO>::IndexOf(T)
#define List_1_IndexOf_m2072237944(__this, ___item0, method) ((  int32_t (*) (List_1_t1190101749 *, CSGuideVO_t4116883493 *, const MethodInfo*))List_1_IndexOf_m2776798407_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<CSGuideVO>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m3779756875(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t1190101749 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m3807054194_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<CSGuideVO>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m1266335684(__this, ___index0, method) ((  void (*) (List_1_t1190101749 *, int32_t, const MethodInfo*))List_1_CheckIndex_m3734723819_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<CSGuideVO>::Insert(System.Int32,T)
#define List_1_Insert_m2132976747(__this, ___index0, ___item1, method) ((  void (*) (List_1_t1190101749 *, int32_t, CSGuideVO_t4116883493 *, const MethodInfo*))List_1_Insert_m3427163986_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<CSGuideVO>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m1052805280(__this, ___collection0, method) ((  void (*) (List_1_t1190101749 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m2905071175_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<CSGuideVO>::Remove(T)
#define List_1_Remove_m32751983(__this, ___item0, method) ((  bool (*) (List_1_t1190101749 *, CSGuideVO_t4116883493 *, const MethodInfo*))List_1_Remove_m2747911208_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<CSGuideVO>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m1397962043(__this, ___match0, method) ((  int32_t (*) (List_1_t1190101749 *, Predicate_1_t3727940376 *, const MethodInfo*))List_1_RemoveAll_m2933443938_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<CSGuideVO>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m398257960(__this, ___index0, method) ((  void (*) (List_1_t1190101749 *, int32_t, const MethodInfo*))List_1_RemoveAt_m2208877785_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<CSGuideVO>::RemoveRange(System.Int32,System.Int32)
#define List_1_RemoveRange_m1465618516(__this, ___index0, ___count1, method) ((  void (*) (List_1_t1190101749 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m856857915_gshared)(__this, ___index0, ___count1, method)
// System.Void System.Collections.Generic.List`1<CSGuideVO>::Reverse()
#define List_1_Reverse_m3024148635(__this, method) ((  void (*) (List_1_t1190101749 *, const MethodInfo*))List_1_Reverse_m449081940_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<CSGuideVO>::Sort()
#define List_1_Sort_m3959137511(__this, method) ((  void (*) (List_1_t1190101749 *, const MethodInfo*))List_1_Sort_m1168641486_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<CSGuideVO>::Sort(System.Collections.Generic.IComparer`1<T>)
#define List_1_Sort_m2623869149(__this, ___comparer0, method) ((  void (*) (List_1_t1190101749 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m3726677974_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<CSGuideVO>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m1345321466(__this, ___comparison0, method) ((  void (*) (List_1_t1190101749 *, Comparison_1_t2833244680 *, const MethodInfo*))List_1_Sort_m4192185249_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<CSGuideVO>::ToArray()
#define List_1_ToArray_m1288868634(__this, method) ((  CSGuideVOU5BU5D_t2582593960* (*) (List_1_t1190101749 *, const MethodInfo*))List_1_ToArray_m1449333879_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<CSGuideVO>::TrimExcess()
#define List_1_TrimExcess_m3852417856(__this, method) ((  void (*) (List_1_t1190101749 *, const MethodInfo*))List_1_TrimExcess_m2451380967_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<CSGuideVO>::get_Capacity()
#define List_1_get_Capacity_m3195753384(__this, method) ((  int32_t (*) (List_1_t1190101749 *, const MethodInfo*))List_1_get_Capacity_m543520655_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<CSGuideVO>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m33739345(__this, ___value0, method) ((  void (*) (List_1_t1190101749 *, int32_t, const MethodInfo*))List_1_set_Capacity_m1332789688_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<CSGuideVO>::get_Count()
#define List_1_get_Count_m238395066(__this, method) ((  int32_t (*) (List_1_t1190101749 *, const MethodInfo*))List_1_get_Count_m3549598589_gshared)(__this, method)
// T System.Collections.Generic.List`1<CSGuideVO>::get_Item(System.Int32)
#define List_1_get_Item_m2629487643(__this, ___index0, method) ((  CSGuideVO_t4116883493 * (*) (List_1_t1190101749 *, int32_t, const MethodInfo*))List_1_get_Item_m404118264_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<CSGuideVO>::set_Item(System.Int32,T)
#define List_1_set_Item_m2900850306(__this, ___index0, ___value1, method) ((  void (*) (List_1_t1190101749 *, int32_t, CSGuideVO_t4116883493 *, const MethodInfo*))List_1_set_Item_m1074271145_gshared)(__this, ___index0, ___value1, method)
