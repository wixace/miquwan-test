﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_routa80
struct  M_routa80_t3894720829  : public Il2CppObject
{
public:
	// System.UInt32 GarbageiOS.M_routa80::_noopeeNayte
	uint32_t ____noopeeNayte_0;
	// System.Boolean GarbageiOS.M_routa80::_maitrenerHihacal
	bool ____maitrenerHihacal_1;
	// System.Single GarbageiOS.M_routa80::_hageJinair
	float ____hageJinair_2;
	// System.Single GarbageiOS.M_routa80::_cawnekowTrasridrou
	float ____cawnekowTrasridrou_3;
	// System.String GarbageiOS.M_routa80::_cofaitawCowpal
	String_t* ____cofaitawCowpal_4;
	// System.Single GarbageiOS.M_routa80::_marvemseNallbula
	float ____marvemseNallbula_5;
	// System.Single GarbageiOS.M_routa80::_mizirmay
	float ____mizirmay_6;
	// System.Single GarbageiOS.M_routa80::_mirowkarWerkaltrou
	float ____mirowkarWerkaltrou_7;
	// System.Int32 GarbageiOS.M_routa80::_burtavear
	int32_t ____burtavear_8;
	// System.UInt32 GarbageiOS.M_routa80::_towqerelawNace
	uint32_t ____towqerelawNace_9;
	// System.Boolean GarbageiOS.M_routa80::_sallcarYoujallrel
	bool ____sallcarYoujallrel_10;
	// System.UInt32 GarbageiOS.M_routa80::_gezelDissejoo
	uint32_t ____gezelDissejoo_11;
	// System.Int32 GarbageiOS.M_routa80::_pomu
	int32_t ____pomu_12;
	// System.Single GarbageiOS.M_routa80::_merbelre
	float ____merbelre_13;
	// System.UInt32 GarbageiOS.M_routa80::_searzoohoo
	uint32_t ____searzoohoo_14;
	// System.Single GarbageiOS.M_routa80::_kallmeefallWidro
	float ____kallmeefallWidro_15;
	// System.Int32 GarbageiOS.M_routa80::_denawo
	int32_t ____denawo_16;
	// System.Boolean GarbageiOS.M_routa80::_nortakor
	bool ____nortakor_17;
	// System.Single GarbageiOS.M_routa80::_cicipere
	float ____cicipere_18;
	// System.Int32 GarbageiOS.M_routa80::_lilem
	int32_t ____lilem_19;
	// System.Boolean GarbageiOS.M_routa80::_remar
	bool ____remar_20;
	// System.Single GarbageiOS.M_routa80::_bixoochiXoudra
	float ____bixoochiXoudra_21;
	// System.Int32 GarbageiOS.M_routa80::_rotere
	int32_t ____rotere_22;
	// System.Int32 GarbageiOS.M_routa80::_mooraSallwher
	int32_t ____mooraSallwher_23;
	// System.Boolean GarbageiOS.M_routa80::_dempaRasarras
	bool ____dempaRasarras_24;
	// System.String GarbageiOS.M_routa80::_toqo
	String_t* ____toqo_25;
	// System.Single GarbageiOS.M_routa80::_roqir
	float ____roqir_26;
	// System.UInt32 GarbageiOS.M_routa80::_nihaPisreldrel
	uint32_t ____nihaPisreldrel_27;
	// System.String GarbageiOS.M_routa80::_sesacha
	String_t* ____sesacha_28;
	// System.UInt32 GarbageiOS.M_routa80::_hownea
	uint32_t ____hownea_29;
	// System.Int32 GarbageiOS.M_routa80::_merve
	int32_t ____merve_30;

public:
	inline static int32_t get_offset_of__noopeeNayte_0() { return static_cast<int32_t>(offsetof(M_routa80_t3894720829, ____noopeeNayte_0)); }
	inline uint32_t get__noopeeNayte_0() const { return ____noopeeNayte_0; }
	inline uint32_t* get_address_of__noopeeNayte_0() { return &____noopeeNayte_0; }
	inline void set__noopeeNayte_0(uint32_t value)
	{
		____noopeeNayte_0 = value;
	}

	inline static int32_t get_offset_of__maitrenerHihacal_1() { return static_cast<int32_t>(offsetof(M_routa80_t3894720829, ____maitrenerHihacal_1)); }
	inline bool get__maitrenerHihacal_1() const { return ____maitrenerHihacal_1; }
	inline bool* get_address_of__maitrenerHihacal_1() { return &____maitrenerHihacal_1; }
	inline void set__maitrenerHihacal_1(bool value)
	{
		____maitrenerHihacal_1 = value;
	}

	inline static int32_t get_offset_of__hageJinair_2() { return static_cast<int32_t>(offsetof(M_routa80_t3894720829, ____hageJinair_2)); }
	inline float get__hageJinair_2() const { return ____hageJinair_2; }
	inline float* get_address_of__hageJinair_2() { return &____hageJinair_2; }
	inline void set__hageJinair_2(float value)
	{
		____hageJinair_2 = value;
	}

	inline static int32_t get_offset_of__cawnekowTrasridrou_3() { return static_cast<int32_t>(offsetof(M_routa80_t3894720829, ____cawnekowTrasridrou_3)); }
	inline float get__cawnekowTrasridrou_3() const { return ____cawnekowTrasridrou_3; }
	inline float* get_address_of__cawnekowTrasridrou_3() { return &____cawnekowTrasridrou_3; }
	inline void set__cawnekowTrasridrou_3(float value)
	{
		____cawnekowTrasridrou_3 = value;
	}

	inline static int32_t get_offset_of__cofaitawCowpal_4() { return static_cast<int32_t>(offsetof(M_routa80_t3894720829, ____cofaitawCowpal_4)); }
	inline String_t* get__cofaitawCowpal_4() const { return ____cofaitawCowpal_4; }
	inline String_t** get_address_of__cofaitawCowpal_4() { return &____cofaitawCowpal_4; }
	inline void set__cofaitawCowpal_4(String_t* value)
	{
		____cofaitawCowpal_4 = value;
		Il2CppCodeGenWriteBarrier(&____cofaitawCowpal_4, value);
	}

	inline static int32_t get_offset_of__marvemseNallbula_5() { return static_cast<int32_t>(offsetof(M_routa80_t3894720829, ____marvemseNallbula_5)); }
	inline float get__marvemseNallbula_5() const { return ____marvemseNallbula_5; }
	inline float* get_address_of__marvemseNallbula_5() { return &____marvemseNallbula_5; }
	inline void set__marvemseNallbula_5(float value)
	{
		____marvemseNallbula_5 = value;
	}

	inline static int32_t get_offset_of__mizirmay_6() { return static_cast<int32_t>(offsetof(M_routa80_t3894720829, ____mizirmay_6)); }
	inline float get__mizirmay_6() const { return ____mizirmay_6; }
	inline float* get_address_of__mizirmay_6() { return &____mizirmay_6; }
	inline void set__mizirmay_6(float value)
	{
		____mizirmay_6 = value;
	}

	inline static int32_t get_offset_of__mirowkarWerkaltrou_7() { return static_cast<int32_t>(offsetof(M_routa80_t3894720829, ____mirowkarWerkaltrou_7)); }
	inline float get__mirowkarWerkaltrou_7() const { return ____mirowkarWerkaltrou_7; }
	inline float* get_address_of__mirowkarWerkaltrou_7() { return &____mirowkarWerkaltrou_7; }
	inline void set__mirowkarWerkaltrou_7(float value)
	{
		____mirowkarWerkaltrou_7 = value;
	}

	inline static int32_t get_offset_of__burtavear_8() { return static_cast<int32_t>(offsetof(M_routa80_t3894720829, ____burtavear_8)); }
	inline int32_t get__burtavear_8() const { return ____burtavear_8; }
	inline int32_t* get_address_of__burtavear_8() { return &____burtavear_8; }
	inline void set__burtavear_8(int32_t value)
	{
		____burtavear_8 = value;
	}

	inline static int32_t get_offset_of__towqerelawNace_9() { return static_cast<int32_t>(offsetof(M_routa80_t3894720829, ____towqerelawNace_9)); }
	inline uint32_t get__towqerelawNace_9() const { return ____towqerelawNace_9; }
	inline uint32_t* get_address_of__towqerelawNace_9() { return &____towqerelawNace_9; }
	inline void set__towqerelawNace_9(uint32_t value)
	{
		____towqerelawNace_9 = value;
	}

	inline static int32_t get_offset_of__sallcarYoujallrel_10() { return static_cast<int32_t>(offsetof(M_routa80_t3894720829, ____sallcarYoujallrel_10)); }
	inline bool get__sallcarYoujallrel_10() const { return ____sallcarYoujallrel_10; }
	inline bool* get_address_of__sallcarYoujallrel_10() { return &____sallcarYoujallrel_10; }
	inline void set__sallcarYoujallrel_10(bool value)
	{
		____sallcarYoujallrel_10 = value;
	}

	inline static int32_t get_offset_of__gezelDissejoo_11() { return static_cast<int32_t>(offsetof(M_routa80_t3894720829, ____gezelDissejoo_11)); }
	inline uint32_t get__gezelDissejoo_11() const { return ____gezelDissejoo_11; }
	inline uint32_t* get_address_of__gezelDissejoo_11() { return &____gezelDissejoo_11; }
	inline void set__gezelDissejoo_11(uint32_t value)
	{
		____gezelDissejoo_11 = value;
	}

	inline static int32_t get_offset_of__pomu_12() { return static_cast<int32_t>(offsetof(M_routa80_t3894720829, ____pomu_12)); }
	inline int32_t get__pomu_12() const { return ____pomu_12; }
	inline int32_t* get_address_of__pomu_12() { return &____pomu_12; }
	inline void set__pomu_12(int32_t value)
	{
		____pomu_12 = value;
	}

	inline static int32_t get_offset_of__merbelre_13() { return static_cast<int32_t>(offsetof(M_routa80_t3894720829, ____merbelre_13)); }
	inline float get__merbelre_13() const { return ____merbelre_13; }
	inline float* get_address_of__merbelre_13() { return &____merbelre_13; }
	inline void set__merbelre_13(float value)
	{
		____merbelre_13 = value;
	}

	inline static int32_t get_offset_of__searzoohoo_14() { return static_cast<int32_t>(offsetof(M_routa80_t3894720829, ____searzoohoo_14)); }
	inline uint32_t get__searzoohoo_14() const { return ____searzoohoo_14; }
	inline uint32_t* get_address_of__searzoohoo_14() { return &____searzoohoo_14; }
	inline void set__searzoohoo_14(uint32_t value)
	{
		____searzoohoo_14 = value;
	}

	inline static int32_t get_offset_of__kallmeefallWidro_15() { return static_cast<int32_t>(offsetof(M_routa80_t3894720829, ____kallmeefallWidro_15)); }
	inline float get__kallmeefallWidro_15() const { return ____kallmeefallWidro_15; }
	inline float* get_address_of__kallmeefallWidro_15() { return &____kallmeefallWidro_15; }
	inline void set__kallmeefallWidro_15(float value)
	{
		____kallmeefallWidro_15 = value;
	}

	inline static int32_t get_offset_of__denawo_16() { return static_cast<int32_t>(offsetof(M_routa80_t3894720829, ____denawo_16)); }
	inline int32_t get__denawo_16() const { return ____denawo_16; }
	inline int32_t* get_address_of__denawo_16() { return &____denawo_16; }
	inline void set__denawo_16(int32_t value)
	{
		____denawo_16 = value;
	}

	inline static int32_t get_offset_of__nortakor_17() { return static_cast<int32_t>(offsetof(M_routa80_t3894720829, ____nortakor_17)); }
	inline bool get__nortakor_17() const { return ____nortakor_17; }
	inline bool* get_address_of__nortakor_17() { return &____nortakor_17; }
	inline void set__nortakor_17(bool value)
	{
		____nortakor_17 = value;
	}

	inline static int32_t get_offset_of__cicipere_18() { return static_cast<int32_t>(offsetof(M_routa80_t3894720829, ____cicipere_18)); }
	inline float get__cicipere_18() const { return ____cicipere_18; }
	inline float* get_address_of__cicipere_18() { return &____cicipere_18; }
	inline void set__cicipere_18(float value)
	{
		____cicipere_18 = value;
	}

	inline static int32_t get_offset_of__lilem_19() { return static_cast<int32_t>(offsetof(M_routa80_t3894720829, ____lilem_19)); }
	inline int32_t get__lilem_19() const { return ____lilem_19; }
	inline int32_t* get_address_of__lilem_19() { return &____lilem_19; }
	inline void set__lilem_19(int32_t value)
	{
		____lilem_19 = value;
	}

	inline static int32_t get_offset_of__remar_20() { return static_cast<int32_t>(offsetof(M_routa80_t3894720829, ____remar_20)); }
	inline bool get__remar_20() const { return ____remar_20; }
	inline bool* get_address_of__remar_20() { return &____remar_20; }
	inline void set__remar_20(bool value)
	{
		____remar_20 = value;
	}

	inline static int32_t get_offset_of__bixoochiXoudra_21() { return static_cast<int32_t>(offsetof(M_routa80_t3894720829, ____bixoochiXoudra_21)); }
	inline float get__bixoochiXoudra_21() const { return ____bixoochiXoudra_21; }
	inline float* get_address_of__bixoochiXoudra_21() { return &____bixoochiXoudra_21; }
	inline void set__bixoochiXoudra_21(float value)
	{
		____bixoochiXoudra_21 = value;
	}

	inline static int32_t get_offset_of__rotere_22() { return static_cast<int32_t>(offsetof(M_routa80_t3894720829, ____rotere_22)); }
	inline int32_t get__rotere_22() const { return ____rotere_22; }
	inline int32_t* get_address_of__rotere_22() { return &____rotere_22; }
	inline void set__rotere_22(int32_t value)
	{
		____rotere_22 = value;
	}

	inline static int32_t get_offset_of__mooraSallwher_23() { return static_cast<int32_t>(offsetof(M_routa80_t3894720829, ____mooraSallwher_23)); }
	inline int32_t get__mooraSallwher_23() const { return ____mooraSallwher_23; }
	inline int32_t* get_address_of__mooraSallwher_23() { return &____mooraSallwher_23; }
	inline void set__mooraSallwher_23(int32_t value)
	{
		____mooraSallwher_23 = value;
	}

	inline static int32_t get_offset_of__dempaRasarras_24() { return static_cast<int32_t>(offsetof(M_routa80_t3894720829, ____dempaRasarras_24)); }
	inline bool get__dempaRasarras_24() const { return ____dempaRasarras_24; }
	inline bool* get_address_of__dempaRasarras_24() { return &____dempaRasarras_24; }
	inline void set__dempaRasarras_24(bool value)
	{
		____dempaRasarras_24 = value;
	}

	inline static int32_t get_offset_of__toqo_25() { return static_cast<int32_t>(offsetof(M_routa80_t3894720829, ____toqo_25)); }
	inline String_t* get__toqo_25() const { return ____toqo_25; }
	inline String_t** get_address_of__toqo_25() { return &____toqo_25; }
	inline void set__toqo_25(String_t* value)
	{
		____toqo_25 = value;
		Il2CppCodeGenWriteBarrier(&____toqo_25, value);
	}

	inline static int32_t get_offset_of__roqir_26() { return static_cast<int32_t>(offsetof(M_routa80_t3894720829, ____roqir_26)); }
	inline float get__roqir_26() const { return ____roqir_26; }
	inline float* get_address_of__roqir_26() { return &____roqir_26; }
	inline void set__roqir_26(float value)
	{
		____roqir_26 = value;
	}

	inline static int32_t get_offset_of__nihaPisreldrel_27() { return static_cast<int32_t>(offsetof(M_routa80_t3894720829, ____nihaPisreldrel_27)); }
	inline uint32_t get__nihaPisreldrel_27() const { return ____nihaPisreldrel_27; }
	inline uint32_t* get_address_of__nihaPisreldrel_27() { return &____nihaPisreldrel_27; }
	inline void set__nihaPisreldrel_27(uint32_t value)
	{
		____nihaPisreldrel_27 = value;
	}

	inline static int32_t get_offset_of__sesacha_28() { return static_cast<int32_t>(offsetof(M_routa80_t3894720829, ____sesacha_28)); }
	inline String_t* get__sesacha_28() const { return ____sesacha_28; }
	inline String_t** get_address_of__sesacha_28() { return &____sesacha_28; }
	inline void set__sesacha_28(String_t* value)
	{
		____sesacha_28 = value;
		Il2CppCodeGenWriteBarrier(&____sesacha_28, value);
	}

	inline static int32_t get_offset_of__hownea_29() { return static_cast<int32_t>(offsetof(M_routa80_t3894720829, ____hownea_29)); }
	inline uint32_t get__hownea_29() const { return ____hownea_29; }
	inline uint32_t* get_address_of__hownea_29() { return &____hownea_29; }
	inline void set__hownea_29(uint32_t value)
	{
		____hownea_29 = value;
	}

	inline static int32_t get_offset_of__merve_30() { return static_cast<int32_t>(offsetof(M_routa80_t3894720829, ____merve_30)); }
	inline int32_t get__merve_30() const { return ____merve_30; }
	inline int32_t* get_address_of__merve_30() { return &____merve_30; }
	inline void set__merve_30(int32_t value)
	{
		____merve_30 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
