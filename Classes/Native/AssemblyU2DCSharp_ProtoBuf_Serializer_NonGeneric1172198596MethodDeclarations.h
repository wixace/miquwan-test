﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.IO.Stream
struct Stream_t1561764144;
// System.Type
struct Type_t;
// ProtoBuf.Serializer/TypeResolver
struct TypeResolver_t356109658;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IO_Stream1561764144.h"
#include "mscorlib_System_Type2863145774.h"
#include "AssemblyU2DCSharp_ProtoBuf_PrefixStyle1974492709.h"
#include "AssemblyU2DCSharp_ProtoBuf_Serializer_TypeResolver356109658.h"

// System.Object ProtoBuf.Serializer/NonGeneric::DeepClone(System.Object)
extern "C"  Il2CppObject * NonGeneric_DeepClone_m3564810755 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___instance0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Serializer/NonGeneric::Serialize(System.IO.Stream,System.Object)
extern "C"  void NonGeneric_Serialize_m3958266042 (Il2CppObject * __this /* static, unused */, Stream_t1561764144 * ___dest0, Il2CppObject * ___instance1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProtoBuf.Serializer/NonGeneric::Deserialize(System.Type,System.IO.Stream)
extern "C"  Il2CppObject * NonGeneric_Deserialize_m2737702883 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, Stream_t1561764144 * ___source1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProtoBuf.Serializer/NonGeneric::Merge(System.IO.Stream,System.Object)
extern "C"  Il2CppObject * NonGeneric_Merge_m1709311101 (Il2CppObject * __this /* static, unused */, Stream_t1561764144 * ___source0, Il2CppObject * ___instance1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Serializer/NonGeneric::SerializeWithLengthPrefix(System.IO.Stream,System.Object,ProtoBuf.PrefixStyle,System.Int32)
extern "C"  void NonGeneric_SerializeWithLengthPrefix_m233593253 (Il2CppObject * __this /* static, unused */, Stream_t1561764144 * ___destination0, Il2CppObject * ___instance1, int32_t ___style2, int32_t ___fieldNumber3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Serializer/NonGeneric::TryDeserializeWithLengthPrefix(System.IO.Stream,ProtoBuf.PrefixStyle,ProtoBuf.Serializer/TypeResolver,System.Object&)
extern "C"  bool NonGeneric_TryDeserializeWithLengthPrefix_m393737378 (Il2CppObject * __this /* static, unused */, Stream_t1561764144 * ___source0, int32_t ___style1, TypeResolver_t356109658 * ___resolver2, Il2CppObject ** ___value3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Serializer/NonGeneric::CanSerialize(System.Type)
extern "C"  bool NonGeneric_CanSerialize_m4205303848 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
