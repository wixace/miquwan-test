﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_ParticleRendererGenerated
struct UnityEngine_ParticleRendererGenerated_t2703715774;
// JSVCall
struct JSVCall_t3708497963;
// UnityEngine.Rect[]
struct RectU5BU5D_t1023580025;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"

// System.Void UnityEngine_ParticleRendererGenerated::.ctor()
extern "C"  void UnityEngine_ParticleRendererGenerated__ctor_m2229096813 (UnityEngine_ParticleRendererGenerated_t2703715774 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ParticleRendererGenerated::ParticleRenderer_ParticleRenderer1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ParticleRendererGenerated_ParticleRenderer_ParticleRenderer1_m2474433669 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleRendererGenerated::ParticleRenderer_particleRenderMode(JSVCall)
extern "C"  void UnityEngine_ParticleRendererGenerated_ParticleRenderer_particleRenderMode_m512760775 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleRendererGenerated::ParticleRenderer_lengthScale(JSVCall)
extern "C"  void UnityEngine_ParticleRendererGenerated_ParticleRenderer_lengthScale_m2423681314 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleRendererGenerated::ParticleRenderer_velocityScale(JSVCall)
extern "C"  void UnityEngine_ParticleRendererGenerated_ParticleRenderer_velocityScale_m600927961 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleRendererGenerated::ParticleRenderer_cameraVelocityScale(JSVCall)
extern "C"  void UnityEngine_ParticleRendererGenerated_ParticleRenderer_cameraVelocityScale_m996568158 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleRendererGenerated::ParticleRenderer_maxParticleSize(JSVCall)
extern "C"  void UnityEngine_ParticleRendererGenerated_ParticleRenderer_maxParticleSize_m516581339 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleRendererGenerated::ParticleRenderer_uvAnimationXTile(JSVCall)
extern "C"  void UnityEngine_ParticleRendererGenerated_ParticleRenderer_uvAnimationXTile_m3747986595 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleRendererGenerated::ParticleRenderer_uvAnimationYTile(JSVCall)
extern "C"  void UnityEngine_ParticleRendererGenerated_ParticleRenderer_uvAnimationYTile_m3242427970 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleRendererGenerated::ParticleRenderer_uvAnimationCycles(JSVCall)
extern "C"  void UnityEngine_ParticleRendererGenerated_ParticleRenderer_uvAnimationCycles_m1977522326 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleRendererGenerated::ParticleRenderer_maxPartileSize(JSVCall)
extern "C"  void UnityEngine_ParticleRendererGenerated_ParticleRenderer_maxPartileSize_m1543543418 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleRendererGenerated::ParticleRenderer_uvTiles(JSVCall)
extern "C"  void UnityEngine_ParticleRendererGenerated_ParticleRenderer_uvTiles_m1482852802 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleRendererGenerated::__Register()
extern "C"  void UnityEngine_ParticleRendererGenerated___Register_m1433339770 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect[] UnityEngine_ParticleRendererGenerated::<ParticleRenderer_uvTiles>m__284()
extern "C"  RectU5BU5D_t1023580025* UnityEngine_ParticleRendererGenerated_U3CParticleRenderer_uvTilesU3Em__284_m1520468492 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_ParticleRendererGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UnityEngine_ParticleRendererGenerated_ilo_getObject1_m1540527465 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleRendererGenerated::ilo_addJSCSRel2(System.Int32,System.Object)
extern "C"  void UnityEngine_ParticleRendererGenerated_ilo_addJSCSRel2_m161307178 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_ParticleRendererGenerated::ilo_getEnum3(System.Int32)
extern "C"  int32_t UnityEngine_ParticleRendererGenerated_ilo_getEnum3_m433031465 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleRendererGenerated::ilo_setSingle4(System.Int32,System.Single)
extern "C"  void UnityEngine_ParticleRendererGenerated_ilo_setSingle4_m3797603690 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleRendererGenerated::ilo_setInt325(System.Int32,System.Int32)
extern "C"  void UnityEngine_ParticleRendererGenerated_ilo_setInt325_m1565196677 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_ParticleRendererGenerated::ilo_getInt326(System.Int32)
extern "C"  int32_t UnityEngine_ParticleRendererGenerated_ilo_getInt326_m1753987669 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine_ParticleRendererGenerated::ilo_getSingle7(System.Int32)
extern "C"  float UnityEngine_ParticleRendererGenerated_ilo_getSingle7_m1667560264 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleRendererGenerated::ilo_moveSaveID2Arr8(System.Int32)
extern "C"  void UnityEngine_ParticleRendererGenerated_ilo_moveSaveID2Arr8_m3198257417 (Il2CppObject * __this /* static, unused */, int32_t ___arrIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_ParticleRendererGenerated::ilo_getArrayLength9(System.Int32)
extern "C"  int32_t UnityEngine_ParticleRendererGenerated_ilo_getArrayLength9_m4006420647 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_ParticleRendererGenerated::ilo_getObject10(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_ParticleRendererGenerated_ilo_getObject10_m600157864 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
