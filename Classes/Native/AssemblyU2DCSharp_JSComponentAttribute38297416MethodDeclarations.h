﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// JSComponentAttribute
struct JSComponentAttribute_t38297416;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void JSComponentAttribute::.ctor()
extern "C"  void JSComponentAttribute__ctor_m461548627 (JSComponentAttribute_t38297416 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String JSComponentAttribute::get_Name()
extern "C"  String_t* JSComponentAttribute_get_Name_m13927816 (JSComponentAttribute_t38297416 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSComponentAttribute::set_Name(System.String)
extern "C"  void JSComponentAttribute_set_Name_m57320137 (JSComponentAttribute_t38297416 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
