﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.QuadtreeNodeHolder
struct QuadtreeNodeHolder_t2134093193;
// Pathfinding.GraphNodeDelegateCancelable
struct GraphNodeDelegateCancelable_t3591372971;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphNodeDelegateCan3591372971.h"
#include "AssemblyU2DCSharp_Pathfinding_QuadtreeNodeHolder2134093193.h"

// System.Void Pathfinding.QuadtreeNodeHolder::.ctor()
extern "C"  void QuadtreeNodeHolder__ctor_m1799852286 (QuadtreeNodeHolder_t2134093193 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.QuadtreeNodeHolder::GetNodes(Pathfinding.GraphNodeDelegateCancelable)
extern "C"  void QuadtreeNodeHolder_GetNodes_m2251328914 (QuadtreeNodeHolder_t2134093193 * __this, GraphNodeDelegateCancelable_t3591372971 * ___del0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.QuadtreeNodeHolder::ilo_GetNodes1(Pathfinding.QuadtreeNodeHolder,Pathfinding.GraphNodeDelegateCancelable)
extern "C"  void QuadtreeNodeHolder_ilo_GetNodes1_m1405004975 (Il2CppObject * __this /* static, unused */, QuadtreeNodeHolder_t2134093193 * ____this0, GraphNodeDelegateCancelable_t3591372971 * ___del1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
