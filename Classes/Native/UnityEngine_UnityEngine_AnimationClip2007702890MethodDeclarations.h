﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.AnimationClip
struct AnimationClip_t2007702890;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.String
struct String_t;
// System.Type
struct Type_t;
// UnityEngine.AnimationCurve
struct AnimationCurve_t3667593487;
// UnityEngine.AnimationEvent
struct AnimationEvent_t3669457594;
// System.Object
struct Il2CppObject;
// UnityEngine.AnimationEvent[]
struct AnimationEventU5BU5D_t1749266719;
// System.Array
struct Il2CppArray;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "UnityEngine_UnityEngine_AnimationClip2007702890.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Type2863145774.h"
#include "UnityEngine_UnityEngine_AnimationCurve3667593487.h"
#include "UnityEngine_UnityEngine_WrapMode1491636113.h"
#include "UnityEngine_UnityEngine_Bounds2711641849.h"
#include "UnityEngine_UnityEngine_AnimationEvent3669457594.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Array1146569071.h"

// System.Void UnityEngine.AnimationClip::.ctor()
extern "C"  void AnimationClip__ctor_m3734809445 (AnimationClip_t2007702890 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationClip::SampleAnimation(UnityEngine.GameObject,System.Single)
extern "C"  void AnimationClip_SampleAnimation_m4092531290 (AnimationClip_t2007702890 * __this, GameObject_t3674682005 * ___go0, float ___time1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationClip::Internal_CreateAnimationClip(UnityEngine.AnimationClip)
extern "C"  void AnimationClip_Internal_CreateAnimationClip_m771412448 (Il2CppObject * __this /* static, unused */, AnimationClip_t2007702890 * ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.AnimationClip::get_length()
extern "C"  float AnimationClip_get_length_m4218495554 (AnimationClip_t2007702890 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.AnimationClip::get_frameRate()
extern "C"  float AnimationClip_get_frameRate_m686576627 (AnimationClip_t2007702890 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationClip::set_frameRate(System.Single)
extern "C"  void AnimationClip_set_frameRate_m3394351512 (AnimationClip_t2007702890 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationClip::SetCurve(System.String,System.Type,System.String,UnityEngine.AnimationCurve)
extern "C"  void AnimationClip_SetCurve_m4139164191 (AnimationClip_t2007702890 * __this, String_t* ___relativePath0, Type_t * ___type1, String_t* ___propertyName2, AnimationCurve_t3667593487 * ___curve3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationClip::EnsureQuaternionContinuity()
extern "C"  void AnimationClip_EnsureQuaternionContinuity_m628950859 (AnimationClip_t2007702890 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationClip::INTERNAL_CALL_EnsureQuaternionContinuity(UnityEngine.AnimationClip)
extern "C"  void AnimationClip_INTERNAL_CALL_EnsureQuaternionContinuity_m3707458027 (Il2CppObject * __this /* static, unused */, AnimationClip_t2007702890 * ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationClip::ClearCurves()
extern "C"  void AnimationClip_ClearCurves_m1424603348 (AnimationClip_t2007702890 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationClip::INTERNAL_CALL_ClearCurves(UnityEngine.AnimationClip)
extern "C"  void AnimationClip_INTERNAL_CALL_ClearCurves_m3635648096 (Il2CppObject * __this /* static, unused */, AnimationClip_t2007702890 * ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.WrapMode UnityEngine.AnimationClip::get_wrapMode()
extern "C"  int32_t AnimationClip_get_wrapMode_m3215116778 (AnimationClip_t2007702890 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationClip::set_wrapMode(UnityEngine.WrapMode)
extern "C"  void AnimationClip_set_wrapMode_m3209139749 (AnimationClip_t2007702890 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Bounds UnityEngine.AnimationClip::get_localBounds()
extern "C"  Bounds_t2711641849  AnimationClip_get_localBounds_m2446231421 (AnimationClip_t2007702890 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationClip::set_localBounds(UnityEngine.Bounds)
extern "C"  void AnimationClip_set_localBounds_m3323597978 (AnimationClip_t2007702890 * __this, Bounds_t2711641849  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationClip::INTERNAL_get_localBounds(UnityEngine.Bounds&)
extern "C"  void AnimationClip_INTERNAL_get_localBounds_m996844740 (AnimationClip_t2007702890 * __this, Bounds_t2711641849 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationClip::INTERNAL_set_localBounds(UnityEngine.Bounds&)
extern "C"  void AnimationClip_INTERNAL_set_localBounds_m2925611320 (AnimationClip_t2007702890 * __this, Bounds_t2711641849 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.AnimationClip::get_legacy()
extern "C"  bool AnimationClip_get_legacy_m1578066251 (AnimationClip_t2007702890 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationClip::set_legacy(System.Boolean)
extern "C"  void AnimationClip_set_legacy_m266620956 (AnimationClip_t2007702890 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.AnimationClip::get_humanMotion()
extern "C"  bool AnimationClip_get_humanMotion_m232378723 (AnimationClip_t2007702890 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationClip::AddEvent(UnityEngine.AnimationEvent)
extern "C"  void AnimationClip_AddEvent_m3284148459 (AnimationClip_t2007702890 * __this, AnimationEvent_t3669457594 * ___evt0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationClip::AddEventInternal(System.Object)
extern "C"  void AnimationClip_AddEventInternal_m3257953439 (AnimationClip_t2007702890 * __this, Il2CppObject * ___evt0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AnimationEvent[] UnityEngine.AnimationClip::get_events()
extern "C"  AnimationEventU5BU5D_t1749266719* AnimationClip_get_events_m1903664193 (AnimationClip_t2007702890 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationClip::set_events(UnityEngine.AnimationEvent[])
extern "C"  void AnimationClip_set_events_m3295139334 (AnimationClip_t2007702890 * __this, AnimationEventU5BU5D_t1749266719* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationClip::SetEventsInternal(System.Array)
extern "C"  void AnimationClip_SetEventsInternal_m785486305 (AnimationClip_t2007702890 * __this, Il2CppArray * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Array UnityEngine.AnimationClip::GetEventsInternal()
extern "C"  Il2CppArray * AnimationClip_GetEventsInternal_m2715483076 (AnimationClip_t2007702890 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
