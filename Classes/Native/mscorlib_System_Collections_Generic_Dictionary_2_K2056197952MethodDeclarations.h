﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K1499758504MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.UInt32,CSSkillData>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define KeyCollection__ctor_m1182398543(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t2056197952 *, Dictionary_2_t429438501 *, const MethodInfo*))KeyCollection__ctor_m4137106658_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.UInt32,CSSkillData>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3237424039(__this, ___item0, method) ((  void (*) (KeyCollection_t2056197952 *, uint32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3142588212_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.UInt32,CSSkillData>::System.Collections.Generic.ICollection<TKey>.Clear()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m2626817758(__this, method) ((  void (*) (KeyCollection_t2056197952 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m660240811_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.UInt32,CSSkillData>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m2754775875(__this, ___item0, method) ((  bool (*) (KeyCollection_t2056197952 *, uint32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m3107174486_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.UInt32,CSSkillData>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2434724200(__this, ___item0, method) ((  bool (*) (KeyCollection_t2056197952 *, uint32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2292074299_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.UInt32,CSSkillData>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1754535898(__this, method) ((  Il2CppObject* (*) (KeyCollection_t2056197952 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m3531386983_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.UInt32,CSSkillData>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define KeyCollection_System_Collections_ICollection_CopyTo_m300848720(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t2056197952 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m4032050589_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.UInt32,CSSkillData>::System.Collections.IEnumerable.GetEnumerator()
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1186400075(__this, method) ((  Il2CppObject * (*) (KeyCollection_t2056197952 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m2531852888_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.UInt32,CSSkillData>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m225852900(__this, method) ((  bool (*) (KeyCollection_t2056197952 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m3873468983_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.UInt32,CSSkillData>::System.Collections.ICollection.get_IsSynchronized()
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1919783830(__this, method) ((  bool (*) (KeyCollection_t2056197952 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m4199187369_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.UInt32,CSSkillData>::System.Collections.ICollection.get_SyncRoot()
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m3883662850(__this, method) ((  Il2CppObject * (*) (KeyCollection_t2056197952 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m3281150229_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.UInt32,CSSkillData>::CopyTo(TKey[],System.Int32)
#define KeyCollection_CopyTo_m2409913092(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t2056197952 *, UInt32U5BU5D_t3230734560*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m2183179159_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.UInt32,CSSkillData>::GetEnumerator()
#define KeyCollection_GetEnumerator_m719468967(__this, method) ((  Enumerator_t1044374555  (*) (KeyCollection_t2056197952 *, const MethodInfo*))KeyCollection_GetEnumerator_m4267000826_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.UInt32,CSSkillData>::get_Count()
#define KeyCollection_get_Count_m2753730780(__this, method) ((  int32_t (*) (KeyCollection_t2056197952 *, const MethodInfo*))KeyCollection_get_Count_m168011375_gshared)(__this, method)
