﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UILoadScene
struct  UILoadScene_t3799334674  : public MonoBehaviour_t667441552
{
public:
	// System.String UILoadScene::scene
	String_t* ___scene_2;

public:
	inline static int32_t get_offset_of_scene_2() { return static_cast<int32_t>(offsetof(UILoadScene_t3799334674, ___scene_2)); }
	inline String_t* get_scene_2() const { return ___scene_2; }
	inline String_t** get_address_of_scene_2() { return &___scene_2; }
	inline void set_scene_2(String_t* value)
	{
		___scene_2 = value;
		Il2CppCodeGenWriteBarrier(&___scene_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
