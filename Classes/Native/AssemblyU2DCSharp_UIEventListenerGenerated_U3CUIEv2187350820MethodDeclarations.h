﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIEventListenerGenerated/<UIEventListener_onPress_GetDelegate_member5_arg0>c__AnonStoreyB9
struct U3CUIEventListener_onPress_GetDelegate_member5_arg0U3Ec__AnonStoreyB9_t2187350820;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"

// System.Void UIEventListenerGenerated/<UIEventListener_onPress_GetDelegate_member5_arg0>c__AnonStoreyB9::.ctor()
extern "C"  void U3CUIEventListener_onPress_GetDelegate_member5_arg0U3Ec__AnonStoreyB9__ctor_m640458231 (U3CUIEventListener_onPress_GetDelegate_member5_arg0U3Ec__AnonStoreyB9_t2187350820 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIEventListenerGenerated/<UIEventListener_onPress_GetDelegate_member5_arg0>c__AnonStoreyB9::<>m__136(UnityEngine.GameObject,System.Boolean)
extern "C"  void U3CUIEventListener_onPress_GetDelegate_member5_arg0U3Ec__AnonStoreyB9_U3CU3Em__136_m2550510223 (U3CUIEventListener_onPress_GetDelegate_member5_arg0U3Ec__AnonStoreyB9_t2187350820 * __this, GameObject_t3674682005 * ___go0, bool ___state1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
