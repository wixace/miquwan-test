﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// XiMaNewPay
struct  XiMaNewPay_t2653418445  : public Il2CppObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.String> XiMaNewPay::payParams
	Dictionary_2_t827649927 * ___payParams_0;

public:
	inline static int32_t get_offset_of_payParams_0() { return static_cast<int32_t>(offsetof(XiMaNewPay_t2653418445, ___payParams_0)); }
	inline Dictionary_2_t827649927 * get_payParams_0() const { return ___payParams_0; }
	inline Dictionary_2_t827649927 ** get_address_of_payParams_0() { return &___payParams_0; }
	inline void set_payParams_0(Dictionary_2_t827649927 * value)
	{
		___payParams_0 = value;
		Il2CppCodeGenWriteBarrier(&___payParams_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
