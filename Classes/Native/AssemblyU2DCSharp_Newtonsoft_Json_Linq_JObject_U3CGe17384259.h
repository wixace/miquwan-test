﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.Linq.JToken>
struct IEnumerator_1_t1029143704;
// Newtonsoft.Json.Linq.JProperty
struct JProperty_t2616415645;
// Newtonsoft.Json.Linq.JObject
struct JObject_t1798544199;

#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24131445027.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JObject/<GetEnumerator>c__Iterator1D
struct  U3CGetEnumeratorU3Ec__Iterator1D_t17384259  : public Il2CppObject
{
public:
	// System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JObject/<GetEnumerator>c__Iterator1D::<$s_114>__0
	Il2CppObject* ___U3CU24s_114U3E__0_0;
	// Newtonsoft.Json.Linq.JProperty Newtonsoft.Json.Linq.JObject/<GetEnumerator>c__Iterator1D::<property>__1
	JProperty_t2616415645 * ___U3CpropertyU3E__1_1;
	// System.Int32 Newtonsoft.Json.Linq.JObject/<GetEnumerator>c__Iterator1D::$PC
	int32_t ___U24PC_2;
	// System.Collections.Generic.KeyValuePair`2<System.String,Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JObject/<GetEnumerator>c__Iterator1D::$current
	KeyValuePair_2_t4131445027  ___U24current_3;
	// Newtonsoft.Json.Linq.JObject Newtonsoft.Json.Linq.JObject/<GetEnumerator>c__Iterator1D::<>f__this
	JObject_t1798544199 * ___U3CU3Ef__this_4;

public:
	inline static int32_t get_offset_of_U3CU24s_114U3E__0_0() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator1D_t17384259, ___U3CU24s_114U3E__0_0)); }
	inline Il2CppObject* get_U3CU24s_114U3E__0_0() const { return ___U3CU24s_114U3E__0_0; }
	inline Il2CppObject** get_address_of_U3CU24s_114U3E__0_0() { return &___U3CU24s_114U3E__0_0; }
	inline void set_U3CU24s_114U3E__0_0(Il2CppObject* value)
	{
		___U3CU24s_114U3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24s_114U3E__0_0, value);
	}

	inline static int32_t get_offset_of_U3CpropertyU3E__1_1() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator1D_t17384259, ___U3CpropertyU3E__1_1)); }
	inline JProperty_t2616415645 * get_U3CpropertyU3E__1_1() const { return ___U3CpropertyU3E__1_1; }
	inline JProperty_t2616415645 ** get_address_of_U3CpropertyU3E__1_1() { return &___U3CpropertyU3E__1_1; }
	inline void set_U3CpropertyU3E__1_1(JProperty_t2616415645 * value)
	{
		___U3CpropertyU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CpropertyU3E__1_1, value);
	}

	inline static int32_t get_offset_of_U24PC_2() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator1D_t17384259, ___U24PC_2)); }
	inline int32_t get_U24PC_2() const { return ___U24PC_2; }
	inline int32_t* get_address_of_U24PC_2() { return &___U24PC_2; }
	inline void set_U24PC_2(int32_t value)
	{
		___U24PC_2 = value;
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator1D_t17384259, ___U24current_3)); }
	inline KeyValuePair_2_t4131445027  get_U24current_3() const { return ___U24current_3; }
	inline KeyValuePair_2_t4131445027 * get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(KeyValuePair_2_t4131445027  value)
	{
		___U24current_3 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_4() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator1D_t17384259, ___U3CU3Ef__this_4)); }
	inline JObject_t1798544199 * get_U3CU3Ef__this_4() const { return ___U3CU3Ef__this_4; }
	inline JObject_t1798544199 ** get_address_of_U3CU3Ef__this_4() { return &___U3CU3Ef__this_4; }
	inline void set_U3CU3Ef__this_4(JObject_t1798544199 * value)
	{
		___U3CU3Ef__this_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
