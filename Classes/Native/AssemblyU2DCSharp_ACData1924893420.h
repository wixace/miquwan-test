﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ACData
struct  ACData_t1924893420  : public Il2CppObject
{
public:
	// System.UInt32 ACData::value
	uint32_t ___value_0;
	// System.UInt32 ACData::acValue
	uint32_t ___acValue_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(ACData_t1924893420, ___value_0)); }
	inline uint32_t get_value_0() const { return ___value_0; }
	inline uint32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(uint32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_acValue_1() { return static_cast<int32_t>(offsetof(ACData_t1924893420, ___acValue_1)); }
	inline uint32_t get_acValue_1() const { return ___acValue_1; }
	inline uint32_t* get_address_of_acValue_1() { return &___acValue_1; }
	inline void set_acValue_1(uint32_t value)
	{
		___acValue_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
