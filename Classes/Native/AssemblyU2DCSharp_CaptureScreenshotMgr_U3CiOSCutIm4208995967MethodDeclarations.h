﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CaptureScreenshotMgr/<iOSCutImage>c__Iterator32
struct U3CiOSCutImageU3Ec__Iterator32_t4208995967;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void CaptureScreenshotMgr/<iOSCutImage>c__Iterator32::.ctor()
extern "C"  void U3CiOSCutImageU3Ec__Iterator32__ctor_m2366953420 (U3CiOSCutImageU3Ec__Iterator32_t4208995967 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object CaptureScreenshotMgr/<iOSCutImage>c__Iterator32::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CiOSCutImageU3Ec__Iterator32_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1859792134 (U3CiOSCutImageU3Ec__Iterator32_t4208995967 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object CaptureScreenshotMgr/<iOSCutImage>c__Iterator32::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CiOSCutImageU3Ec__Iterator32_System_Collections_IEnumerator_get_Current_m2401434778 (U3CiOSCutImageU3Ec__Iterator32_t4208995967 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CaptureScreenshotMgr/<iOSCutImage>c__Iterator32::MoveNext()
extern "C"  bool U3CiOSCutImageU3Ec__Iterator32_MoveNext_m3437333160 (U3CiOSCutImageU3Ec__Iterator32_t4208995967 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CaptureScreenshotMgr/<iOSCutImage>c__Iterator32::Dispose()
extern "C"  void U3CiOSCutImageU3Ec__Iterator32_Dispose_m2502820105 (U3CiOSCutImageU3Ec__Iterator32_t4208995967 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CaptureScreenshotMgr/<iOSCutImage>c__Iterator32::Reset()
extern "C"  void U3CiOSCutImageU3Ec__Iterator32_Reset_m13386361 (U3CiOSCutImageU3Ec__Iterator32_t4208995967 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
