﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TBQuickSetup
struct TBQuickSetup_t3279947166;
// FingerDownDetector
struct FingerDownDetector_t1553505457;
// FingerUpDetector
struct FingerUpDetector_t544994282;
// FingerHoverDetector
struct FingerHoverDetector_t92426265;
// FingerMotionDetector
struct FingerMotionDetector_t1792212357;
// DragRecognizer
struct DragRecognizer_t2550591000;
// LongPressRecognizer
struct LongPressRecognizer_t1602536459;
// SwipeRecognizer
struct SwipeRecognizer_t2690489438;
// TapRecognizer
struct TapRecognizer_t3788301703;
// PinchRecognizer
struct PinchRecognizer_t1004677598;
// TwistRecognizer
struct TwistRecognizer_t3400822795;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_FingerDownDetector1553505457.h"
#include "AssemblyU2DCSharp_FingerUpDetector544994282.h"
#include "AssemblyU2DCSharp_FingerHoverDetector92426265.h"
#include "AssemblyU2DCSharp_FingerMotionDetector1792212357.h"
#include "AssemblyU2DCSharp_DragRecognizer2550591000.h"
#include "AssemblyU2DCSharp_LongPressRecognizer1602536459.h"
#include "AssemblyU2DCSharp_SwipeRecognizer2690489438.h"
#include "AssemblyU2DCSharp_TapRecognizer3788301703.h"
#include "AssemblyU2DCSharp_PinchRecognizer1004677598.h"
#include "AssemblyU2DCSharp_TwistRecognizer3400822795.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_TBQuickSetup3279947166.h"

// System.Void TBQuickSetup::.ctor()
extern "C"  void TBQuickSetup__ctor_m3906035389 (TBQuickSetup_t3279947166 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FingerDownDetector TBQuickSetup::get_FingerDown()
extern "C"  FingerDownDetector_t1553505457 * TBQuickSetup_get_FingerDown_m3405218183 (TBQuickSetup_t3279947166 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBQuickSetup::set_FingerDown(FingerDownDetector)
extern "C"  void TBQuickSetup_set_FingerDown_m866948350 (TBQuickSetup_t3279947166 * __this, FingerDownDetector_t1553505457 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FingerUpDetector TBQuickSetup::get_FingerUp()
extern "C"  FingerUpDetector_t544994282 * TBQuickSetup_get_FingerUp_m2024802023 (TBQuickSetup_t3279947166 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBQuickSetup::set_FingerUp(FingerUpDetector)
extern "C"  void TBQuickSetup_set_FingerUp_m2231537438 (TBQuickSetup_t3279947166 * __this, FingerUpDetector_t544994282 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FingerHoverDetector TBQuickSetup::get_FingerHover()
extern "C"  FingerHoverDetector_t92426265 * TBQuickSetup_get_FingerHover_m652324331 (TBQuickSetup_t3279947166 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBQuickSetup::set_FingerHover(FingerHoverDetector)
extern "C"  void TBQuickSetup_set_FingerHover_m1815328136 (TBQuickSetup_t3279947166 * __this, FingerHoverDetector_t92426265 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FingerMotionDetector TBQuickSetup::get_FingerMotion()
extern "C"  FingerMotionDetector_t1792212357 * TBQuickSetup_get_FingerMotion_m1713236743 (TBQuickSetup_t3279947166 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBQuickSetup::set_FingerMotion(FingerMotionDetector)
extern "C"  void TBQuickSetup_set_FingerMotion_m563451454 (TBQuickSetup_t3279947166 * __this, FingerMotionDetector_t1792212357 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DragRecognizer TBQuickSetup::get_Drag()
extern "C"  DragRecognizer_t2550591000 * TBQuickSetup_get_Drag_m515238921 (TBQuickSetup_t3279947166 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBQuickSetup::set_Drag(DragRecognizer)
extern "C"  void TBQuickSetup_set_Drag_m190362944 (TBQuickSetup_t3279947166 * __this, DragRecognizer_t2550591000 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// LongPressRecognizer TBQuickSetup::get_LongPress()
extern "C"  LongPressRecognizer_t1602536459 * TBQuickSetup_get_LongPress_m2826682097 (TBQuickSetup_t3279947166 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBQuickSetup::set_LongPress(LongPressRecognizer)
extern "C"  void TBQuickSetup_set_LongPress_m1260596546 (TBQuickSetup_t3279947166 * __this, LongPressRecognizer_t1602536459 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SwipeRecognizer TBQuickSetup::get_Swipe()
extern "C"  SwipeRecognizer_t2690489438 * TBQuickSetup_get_Swipe_m2063001559 (TBQuickSetup_t3279947166 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBQuickSetup::set_Swipe(SwipeRecognizer)
extern "C"  void TBQuickSetup_set_Swipe_m2131554076 (TBQuickSetup_t3279947166 * __this, SwipeRecognizer_t2690489438 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TapRecognizer TBQuickSetup::get_Tap()
extern "C"  TapRecognizer_t3788301703 * TBQuickSetup_get_Tap_m3069236329 (TBQuickSetup_t3279947166 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBQuickSetup::set_Tap(TapRecognizer)
extern "C"  void TBQuickSetup_set_Tap_m2987763850 (TBQuickSetup_t3279947166 * __this, TapRecognizer_t3788301703 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TapRecognizer TBQuickSetup::get_DoubleTap()
extern "C"  TapRecognizer_t3788301703 * TBQuickSetup_get_DoubleTap_m602663768 (TBQuickSetup_t3279947166 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBQuickSetup::set_DoubleTap(TapRecognizer)
extern "C"  void TBQuickSetup_set_DoubleTap_m56850427 (TBQuickSetup_t3279947166 * __this, TapRecognizer_t3788301703 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PinchRecognizer TBQuickSetup::get_Pinch()
extern "C"  PinchRecognizer_t1004677598 * TBQuickSetup_get_Pinch_m2447876823 (TBQuickSetup_t3279947166 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBQuickSetup::set_Pinch(PinchRecognizer)
extern "C"  void TBQuickSetup_set_Pinch_m1919112220 (TBQuickSetup_t3279947166 * __this, PinchRecognizer_t1004677598 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TwistRecognizer TBQuickSetup::get_Twist()
extern "C"  TwistRecognizer_t3400822795 * TBQuickSetup_get_Twist_m2269912561 (TBQuickSetup_t3279947166 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBQuickSetup::set_Twist(TwistRecognizer)
extern "C"  void TBQuickSetup_set_Twist_m329363650 (TBQuickSetup_t3279947166 * __this, TwistRecognizer_t3400822795 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DragRecognizer TBQuickSetup::get_TwoFingerDrag()
extern "C"  DragRecognizer_t2550591000 * TBQuickSetup_get_TwoFingerDrag_m1314418198 (TBQuickSetup_t3279947166 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBQuickSetup::set_TwoFingerDrag(DragRecognizer)
extern "C"  void TBQuickSetup_set_TwoFingerDrag_m4285861071 (TBQuickSetup_t3279947166 * __this, DragRecognizer_t2550591000 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TapRecognizer TBQuickSetup::get_TwoFingerTap()
extern "C"  TapRecognizer_t3788301703 * TBQuickSetup_get_TwoFingerTap_m1052984618 (TBQuickSetup_t3279947166 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBQuickSetup::set_TwoFingerTap(TapRecognizer)
extern "C"  void TBQuickSetup_set_TwoFingerTap_m19961817 (TBQuickSetup_t3279947166 * __this, TapRecognizer_t3788301703 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SwipeRecognizer TBQuickSetup::get_TwoFingerSwipe()
extern "C"  SwipeRecognizer_t2690489438 * TBQuickSetup_get_TwoFingerSwipe_m2883655818 (TBQuickSetup_t3279947166 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBQuickSetup::set_TwoFingerSwipe(SwipeRecognizer)
extern "C"  void TBQuickSetup_set_TwoFingerSwipe_m3715210987 (TBQuickSetup_t3279947166 * __this, SwipeRecognizer_t2690489438 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// LongPressRecognizer TBQuickSetup::get_TwoFingerLongPress()
extern "C"  LongPressRecognizer_t1602536459 * TBQuickSetup_get_TwoFingerLongPress_m339421162 (TBQuickSetup_t3279947166 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBQuickSetup::set_TwoFingerLongPress(LongPressRecognizer)
extern "C"  void TBQuickSetup_set_TwoFingerLongPress_m2965282321 (TBQuickSetup_t3279947166 * __this, LongPressRecognizer_t1602536459 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject TBQuickSetup::CreateChildNode(System.String)
extern "C"  GameObject_t3674682005 * TBQuickSetup_CreateChildNode_m3075199706 (TBQuickSetup_t3279947166 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBQuickSetup::Start()
extern "C"  void TBQuickSetup_Start_m2853173181 (TBQuickSetup_t3279947166 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBQuickSetup::ilo_set_FingerHover1(TBQuickSetup,FingerHoverDetector)
extern "C"  void TBQuickSetup_ilo_set_FingerHover1_m1627230698 (Il2CppObject * __this /* static, unused */, TBQuickSetup_t3279947166 * ____this0, FingerHoverDetector_t92426265 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject TBQuickSetup::ilo_CreateChildNode2(TBQuickSetup,System.String)
extern "C"  GameObject_t3674682005 * TBQuickSetup_ilo_CreateChildNode2_m1514061565 (Il2CppObject * __this /* static, unused */, TBQuickSetup_t3279947166 * ____this0, String_t* ___name1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBQuickSetup::ilo_set_DoubleTap3(TBQuickSetup,TapRecognizer)
extern "C"  void TBQuickSetup_ilo_set_DoubleTap3_m2037273055 (Il2CppObject * __this /* static, unused */, TBQuickSetup_t3279947166 * ____this0, TapRecognizer_t3788301703 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
