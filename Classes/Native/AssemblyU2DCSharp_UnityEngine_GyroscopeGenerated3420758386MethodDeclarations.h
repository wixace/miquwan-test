﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_GyroscopeGenerated
struct UnityEngine_GyroscopeGenerated_t3420758386;
// JSVCall
struct JSVCall_t3708497963;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void UnityEngine_GyroscopeGenerated::.ctor()
extern "C"  void UnityEngine_GyroscopeGenerated__ctor_m2892987241 (UnityEngine_GyroscopeGenerated_t3420758386 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GyroscopeGenerated::Gyroscope_rotationRate(JSVCall)
extern "C"  void UnityEngine_GyroscopeGenerated_Gyroscope_rotationRate_m1704734612 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GyroscopeGenerated::Gyroscope_rotationRateUnbiased(JSVCall)
extern "C"  void UnityEngine_GyroscopeGenerated_Gyroscope_rotationRateUnbiased_m950018979 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GyroscopeGenerated::Gyroscope_gravity(JSVCall)
extern "C"  void UnityEngine_GyroscopeGenerated_Gyroscope_gravity_m2821451180 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GyroscopeGenerated::Gyroscope_userAcceleration(JSVCall)
extern "C"  void UnityEngine_GyroscopeGenerated_Gyroscope_userAcceleration_m2383247815 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GyroscopeGenerated::Gyroscope_attitude(JSVCall)
extern "C"  void UnityEngine_GyroscopeGenerated_Gyroscope_attitude_m3718022120 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GyroscopeGenerated::Gyroscope_enabled(JSVCall)
extern "C"  void UnityEngine_GyroscopeGenerated_Gyroscope_enabled_m788936217 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GyroscopeGenerated::Gyroscope_updateInterval(JSVCall)
extern "C"  void UnityEngine_GyroscopeGenerated_Gyroscope_updateInterval_m666188644 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GyroscopeGenerated::__Register()
extern "C"  void UnityEngine_GyroscopeGenerated___Register_m3184769790 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GyroscopeGenerated::ilo_setVector3S1(System.Int32,UnityEngine.Vector3)
extern "C"  void UnityEngine_GyroscopeGenerated_ilo_setVector3S1_m3083653950 (Il2CppObject * __this /* static, unused */, int32_t ___e0, Vector3_t4282066566  ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_GyroscopeGenerated::ilo_setObject2(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UnityEngine_GyroscopeGenerated_ilo_setObject2_m152542338 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GyroscopeGenerated::ilo_setSingle3(System.Int32,System.Single)
extern "C"  void UnityEngine_GyroscopeGenerated_ilo_setSingle3_m3866862957 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine_GyroscopeGenerated::ilo_getSingle4(System.Int32)
extern "C"  float UnityEngine_GyroscopeGenerated_ilo_getSingle4_m1822450465 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
