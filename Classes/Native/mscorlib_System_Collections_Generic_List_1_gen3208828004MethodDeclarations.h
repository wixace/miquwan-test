﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<PushType>
struct List_1_t3208828004;
// System.Collections.Generic.IEnumerable`1<PushType>
struct IEnumerable_1_t846588113;
// System.Collections.Generic.IEnumerator`1<PushType>
struct IEnumerator_1_t3752507501;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.ICollection`1<PushType>
struct ICollection_1_t2735232439;
// System.Collections.ObjectModel.ReadOnlyCollection`1<PushType>
struct ReadOnlyCollection_1_t3397719988;
// PushType[]
struct PushTypeU5BU5D_t3978570141;
// System.Predicate`1<PushType>
struct Predicate_1_t1451699335;
// System.Collections.Generic.IComparer`1<PushType>
struct IComparer_1_t120689198;
// System.Comparison`1<PushType>
struct Comparison_1_t557003639;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_PushType1840642452.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3228500774.h"

// System.Void System.Collections.Generic.List`1<PushType>::.ctor()
extern "C"  void List_1__ctor_m21856584_gshared (List_1_t3208828004 * __this, const MethodInfo* method);
#define List_1__ctor_m21856584(__this, method) ((  void (*) (List_1_t3208828004 *, const MethodInfo*))List_1__ctor_m21856584_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<PushType>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1__ctor_m3641518169_gshared (List_1_t3208828004 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1__ctor_m3641518169(__this, ___collection0, method) ((  void (*) (List_1_t3208828004 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m3641518169_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<PushType>::.ctor(System.Int32)
extern "C"  void List_1__ctor_m2809891481_gshared (List_1_t3208828004 * __this, int32_t ___capacity0, const MethodInfo* method);
#define List_1__ctor_m2809891481(__this, ___capacity0, method) ((  void (*) (List_1_t3208828004 *, int32_t, const MethodInfo*))List_1__ctor_m2809891481_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<PushType>::.cctor()
extern "C"  void List_1__cctor_m195457893_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m195457893(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m195457893_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<PushType>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m964705106_gshared (List_1_t3208828004 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m964705106(__this, method) ((  Il2CppObject* (*) (List_1_t3208828004 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m964705106_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<PushType>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m4193598716_gshared (List_1_t3208828004 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m4193598716(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t3208828004 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m4193598716_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<PushType>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m303489547_gshared (List_1_t3208828004 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m303489547(__this, method) ((  Il2CppObject * (*) (List_1_t3208828004 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m303489547_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<PushType>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_Add_m2903796242_gshared (List_1_t3208828004 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m2903796242(__this, ___item0, method) ((  int32_t (*) (List_1_t3208828004 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m2903796242_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<PushType>::System.Collections.IList.Contains(System.Object)
extern "C"  bool List_1_System_Collections_IList_Contains_m3905887854_gshared (List_1_t3208828004 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m3905887854(__this, ___item0, method) ((  bool (*) (List_1_t3208828004 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m3905887854_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<PushType>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m2299348842_gshared (List_1_t3208828004 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m2299348842(__this, ___item0, method) ((  int32_t (*) (List_1_t3208828004 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m2299348842_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<PushType>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_Insert_m2595658205_gshared (List_1_t3208828004 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m2595658205(__this, ___index0, ___item1, method) ((  void (*) (List_1_t3208828004 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m2595658205_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<PushType>::System.Collections.IList.Remove(System.Object)
extern "C"  void List_1_System_Collections_IList_Remove_m4193733675_gshared (List_1_t3208828004 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m4193733675(__this, ___item0, method) ((  void (*) (List_1_t3208828004 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m4193733675_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<PushType>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3639762159_gshared (List_1_t3208828004 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3639762159(__this, method) ((  bool (*) (List_1_t3208828004 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3639762159_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<PushType>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool List_1_System_Collections_ICollection_get_IsSynchronized_m1046051374_gshared (List_1_t3208828004 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m1046051374(__this, method) ((  bool (*) (List_1_t3208828004 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m1046051374_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<PushType>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m1311501856_gshared (List_1_t3208828004 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m1311501856(__this, method) ((  Il2CppObject * (*) (List_1_t3208828004 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m1311501856_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<PushType>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool List_1_System_Collections_IList_get_IsFixedSize_m2121381981_gshared (List_1_t3208828004 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m2121381981(__this, method) ((  bool (*) (List_1_t3208828004 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m2121381981_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<PushType>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_IList_get_IsReadOnly_m2600650492_gshared (List_1_t3208828004 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m2600650492(__this, method) ((  bool (*) (List_1_t3208828004 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m2600650492_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<PushType>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m2993477543_gshared (List_1_t3208828004 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m2993477543(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t3208828004 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m2993477543_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<PushType>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_set_Item_m3960455796_gshared (List_1_t3208828004 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m3960455796(__this, ___index0, ___value1, method) ((  void (*) (List_1_t3208828004 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m3960455796_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<PushType>::Add(T)
extern "C"  void List_1_Add_m4008719927_gshared (List_1_t3208828004 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_Add_m4008719927(__this, ___item0, method) ((  void (*) (List_1_t3208828004 *, int32_t, const MethodInfo*))List_1_Add_m4008719927_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<PushType>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m3142132466_gshared (List_1_t3208828004 * __this, int32_t ___newCount0, const MethodInfo* method);
#define List_1_GrowIfNeeded_m3142132466(__this, ___newCount0, method) ((  void (*) (List_1_t3208828004 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m3142132466_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<PushType>::CheckRange(System.Int32,System.Int32)
extern "C"  void List_1_CheckRange_m996545973_gshared (List_1_t3208828004 * __this, int32_t ___idx0, int32_t ___count1, const MethodInfo* method);
#define List_1_CheckRange_m996545973(__this, ___idx0, ___count1, method) ((  void (*) (List_1_t3208828004 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m996545973_gshared)(__this, ___idx0, ___count1, method)
// System.Void System.Collections.Generic.List`1<PushType>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m555823600_gshared (List_1_t3208828004 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddCollection_m555823600(__this, ___collection0, method) ((  void (*) (List_1_t3208828004 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m555823600_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<PushType>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddEnumerable_m4111763120_gshared (List_1_t3208828004 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method);
#define List_1_AddEnumerable_m4111763120(__this, ___enumerable0, method) ((  void (*) (List_1_t3208828004 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m4111763120_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<PushType>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m180287975_gshared (List_1_t3208828004 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddRange_m180287975(__this, ___collection0, method) ((  void (*) (List_1_t3208828004 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m180287975_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<PushType>::AsReadOnly()
extern "C"  ReadOnlyCollection_1_t3397719988 * List_1_AsReadOnly_m3818698046_gshared (List_1_t3208828004 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m3818698046(__this, method) ((  ReadOnlyCollection_1_t3397719988 * (*) (List_1_t3208828004 *, const MethodInfo*))List_1_AsReadOnly_m3818698046_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<PushType>::BinarySearch(T)
extern "C"  int32_t List_1_BinarySearch_m522671445_gshared (List_1_t3208828004 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_BinarySearch_m522671445(__this, ___item0, method) ((  int32_t (*) (List_1_t3208828004 *, int32_t, const MethodInfo*))List_1_BinarySearch_m522671445_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<PushType>::Clear()
extern "C"  void List_1_Clear_m1722957171_gshared (List_1_t3208828004 * __this, const MethodInfo* method);
#define List_1_Clear_m1722957171(__this, method) ((  void (*) (List_1_t3208828004 *, const MethodInfo*))List_1_Clear_m1722957171_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<PushType>::Contains(T)
extern "C"  bool List_1_Contains_m3504380389_gshared (List_1_t3208828004 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_Contains_m3504380389(__this, ___item0, method) ((  bool (*) (List_1_t3208828004 *, int32_t, const MethodInfo*))List_1_Contains_m3504380389_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<PushType>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m5217895_gshared (List_1_t3208828004 * __this, PushTypeU5BU5D_t3978570141* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_CopyTo_m5217895(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t3208828004 *, PushTypeU5BU5D_t3978570141*, int32_t, const MethodInfo*))List_1_CopyTo_m5217895_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<PushType>::Find(System.Predicate`1<T>)
extern "C"  int32_t List_1_Find_m2953872447_gshared (List_1_t3208828004 * __this, Predicate_1_t1451699335 * ___match0, const MethodInfo* method);
#define List_1_Find_m2953872447(__this, ___match0, method) ((  int32_t (*) (List_1_t3208828004 *, Predicate_1_t1451699335 *, const MethodInfo*))List_1_Find_m2953872447_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<PushType>::CheckMatch(System.Predicate`1<T>)
extern "C"  void List_1_CheckMatch_m4096836700_gshared (Il2CppObject * __this /* static, unused */, Predicate_1_t1451699335 * ___match0, const MethodInfo* method);
#define List_1_CheckMatch_m4096836700(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t1451699335 *, const MethodInfo*))List_1_CheckMatch_m4096836700_gshared)(__this /* static, unused */, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<PushType>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C"  int32_t List_1_GetIndex_m1332083769_gshared (List_1_t3208828004 * __this, int32_t ___startIndex0, int32_t ___count1, Predicate_1_t1451699335 * ___match2, const MethodInfo* method);
#define List_1_GetIndex_m1332083769(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t3208828004 *, int32_t, int32_t, Predicate_1_t1451699335 *, const MethodInfo*))List_1_GetIndex_m1332083769_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<PushType>::GetEnumerator()
extern "C"  Enumerator_t3228500774  List_1_GetEnumerator_m3218792610_gshared (List_1_t3208828004 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m3218792610(__this, method) ((  Enumerator_t3228500774  (*) (List_1_t3208828004 *, const MethodInfo*))List_1_GetEnumerator_m3218792610_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<PushType>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m329918067_gshared (List_1_t3208828004 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_IndexOf_m329918067(__this, ___item0, method) ((  int32_t (*) (List_1_t3208828004 *, int32_t, const MethodInfo*))List_1_IndexOf_m329918067_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<PushType>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m2444369342_gshared (List_1_t3208828004 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method);
#define List_1_Shift_m2444369342(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t3208828004 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m2444369342_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<PushType>::CheckIndex(System.Int32)
extern "C"  void List_1_CheckIndex_m4046552375_gshared (List_1_t3208828004 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_CheckIndex_m4046552375(__this, ___index0, method) ((  void (*) (List_1_t3208828004 *, int32_t, const MethodInfo*))List_1_CheckIndex_m4046552375_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<PushType>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m1983914654_gshared (List_1_t3208828004 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define List_1_Insert_m1983914654(__this, ___index0, ___item1, method) ((  void (*) (List_1_t3208828004 *, int32_t, int32_t, const MethodInfo*))List_1_Insert_m1983914654_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<PushType>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_CheckCollection_m2097203347_gshared (List_1_t3208828004 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_CheckCollection_m2097203347(__this, ___collection0, method) ((  void (*) (List_1_t3208828004 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m2097203347_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<PushType>::Remove(T)
extern "C"  bool List_1_Remove_m2966196000_gshared (List_1_t3208828004 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_Remove_m2966196000(__this, ___item0, method) ((  bool (*) (List_1_t3208828004 *, int32_t, const MethodInfo*))List_1_Remove_m2966196000_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<PushType>::RemoveAll(System.Predicate`1<T>)
extern "C"  int32_t List_1_RemoveAll_m2253145590_gshared (List_1_t3208828004 * __this, Predicate_1_t1451699335 * ___match0, const MethodInfo* method);
#define List_1_RemoveAll_m2253145590(__this, ___match0, method) ((  int32_t (*) (List_1_t3208828004 *, Predicate_1_t1451699335 *, const MethodInfo*))List_1_RemoveAll_m2253145590_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<PushType>::RemoveAt(System.Int32)
extern "C"  void List_1_RemoveAt_m4152734820_gshared (List_1_t3208828004 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_RemoveAt_m4152734820(__this, ___index0, method) ((  void (*) (List_1_t3208828004 *, int32_t, const MethodInfo*))List_1_RemoveAt_m4152734820_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<PushType>::RemoveRange(System.Int32,System.Int32)
extern "C"  void List_1_RemoveRange_m563366023_gshared (List_1_t3208828004 * __this, int32_t ___index0, int32_t ___count1, const MethodInfo* method);
#define List_1_RemoveRange_m563366023(__this, ___index0, ___count1, method) ((  void (*) (List_1_t3208828004 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m563366023_gshared)(__this, ___index0, ___count1, method)
// System.Void System.Collections.Generic.List`1<PushType>::Reverse()
extern "C"  void List_1_Reverse_m3862119304_gshared (List_1_t3208828004 * __this, const MethodInfo* method);
#define List_1_Reverse_m3862119304(__this, method) ((  void (*) (List_1_t3208828004 *, const MethodInfo*))List_1_Reverse_m3862119304_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<PushType>::Sort()
extern "C"  void List_1_Sort_m239724826_gshared (List_1_t3208828004 * __this, const MethodInfo* method);
#define List_1_Sort_m239724826(__this, method) ((  void (*) (List_1_t3208828004 *, const MethodInfo*))List_1_Sort_m239724826_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<PushType>::Sort(System.Collections.Generic.IComparer`1<T>)
extern "C"  void List_1_Sort_m1159520778_gshared (List_1_t3208828004 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define List_1_Sort_m1159520778(__this, ___comparer0, method) ((  void (*) (List_1_t3208828004 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m1159520778_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<PushType>::Sort(System.Comparison`1<T>)
extern "C"  void List_1_Sort_m2559886829_gshared (List_1_t3208828004 * __this, Comparison_1_t557003639 * ___comparison0, const MethodInfo* method);
#define List_1_Sort_m2559886829(__this, ___comparison0, method) ((  void (*) (List_1_t3208828004 *, Comparison_1_t557003639 *, const MethodInfo*))List_1_Sort_m2559886829_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<PushType>::ToArray()
extern "C"  PushTypeU5BU5D_t3978570141* List_1_ToArray_m2216391201_gshared (List_1_t3208828004 * __this, const MethodInfo* method);
#define List_1_ToArray_m2216391201(__this, method) ((  PushTypeU5BU5D_t3978570141* (*) (List_1_t3208828004 *, const MethodInfo*))List_1_ToArray_m2216391201_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<PushType>::TrimExcess()
extern "C"  void List_1_TrimExcess_m1191726387_gshared (List_1_t3208828004 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m1191726387(__this, method) ((  void (*) (List_1_t3208828004 *, const MethodInfo*))List_1_TrimExcess_m1191726387_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<PushType>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m748399907_gshared (List_1_t3208828004 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m748399907(__this, method) ((  int32_t (*) (List_1_t3208828004 *, const MethodInfo*))List_1_get_Capacity_m748399907_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<PushType>::set_Capacity(System.Int32)
extern "C"  void List_1_set_Capacity_m352321284_gshared (List_1_t3208828004 * __this, int32_t ___value0, const MethodInfo* method);
#define List_1_set_Capacity_m352321284(__this, ___value0, method) ((  void (*) (List_1_t3208828004 *, int32_t, const MethodInfo*))List_1_set_Capacity_m352321284_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<PushType>::get_Count()
extern "C"  int32_t List_1_get_Count_m3275503825_gshared (List_1_t3208828004 * __this, const MethodInfo* method);
#define List_1_get_Count_m3275503825(__this, method) ((  int32_t (*) (List_1_t3208828004 *, const MethodInfo*))List_1_get_Count_m3275503825_gshared)(__this, method)
// T System.Collections.Generic.List`1<PushType>::get_Item(System.Int32)
extern "C"  int32_t List_1_get_Item_m2650030436_gshared (List_1_t3208828004 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_get_Item_m2650030436(__this, ___index0, method) ((  int32_t (*) (List_1_t3208828004 *, int32_t, const MethodInfo*))List_1_get_Item_m2650030436_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<PushType>::set_Item(System.Int32,T)
extern "C"  void List_1_set_Item_m1386099701_gshared (List_1_t3208828004 * __this, int32_t ___index0, int32_t ___value1, const MethodInfo* method);
#define List_1_set_Item_m1386099701(__this, ___index0, ___value1, method) ((  void (*) (List_1_t3208828004 *, int32_t, int32_t, const MethodInfo*))List_1_set_Item_m1386099701_gshared)(__this, ___index0, ___value1, method)
