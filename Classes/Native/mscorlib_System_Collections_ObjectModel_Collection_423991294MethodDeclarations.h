﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<Entity.Behavior.EBehaviorID>
struct Collection_1_t423991294;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// Entity.Behavior.EBehaviorID[]
struct EBehaviorIDU5BU5D_t373889457;
// System.Collections.Generic.IEnumerator`1<Entity.Behavior.EBehaviorID>
struct IEnumerator_1_t3150398185;
// System.Collections.Generic.IList`1<Entity.Behavior.EBehaviorID>
struct IList_1_t3933180339;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_Entity_Behavior_EBehaviorID1238533136.h"

// System.Void System.Collections.ObjectModel.Collection`1<Entity.Behavior.EBehaviorID>::.ctor()
extern "C"  void Collection_1__ctor_m2272781860_gshared (Collection_1_t423991294 * __this, const MethodInfo* method);
#define Collection_1__ctor_m2272781860(__this, method) ((  void (*) (Collection_1_t423991294 *, const MethodInfo*))Collection_1__ctor_m2272781860_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Entity.Behavior.EBehaviorID>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2525715927_gshared (Collection_1_t423991294 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2525715927(__this, method) ((  bool (*) (Collection_1_t423991294 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2525715927_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<Entity.Behavior.EBehaviorID>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Collection_1_System_Collections_ICollection_CopyTo_m3084072864_gshared (Collection_1_t423991294 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m3084072864(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t423991294 *, Il2CppArray *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m3084072864_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<Entity.Behavior.EBehaviorID>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Collection_1_System_Collections_IEnumerable_GetEnumerator_m3412415131_gshared (Collection_1_t423991294 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m3412415131(__this, method) ((  Il2CppObject * (*) (Collection_1_t423991294 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m3412415131_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Entity.Behavior.EBehaviorID>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_Add_m4173727286_gshared (Collection_1_t423991294 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m4173727286(__this, ___value0, method) ((  int32_t (*) (Collection_1_t423991294 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m4173727286_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Entity.Behavior.EBehaviorID>::System.Collections.IList.Contains(System.Object)
extern "C"  bool Collection_1_System_Collections_IList_Contains_m2778767702_gshared (Collection_1_t423991294 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m2778767702(__this, ___value0, method) ((  bool (*) (Collection_1_t423991294 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m2778767702_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Entity.Behavior.EBehaviorID>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_IndexOf_m747385230_gshared (Collection_1_t423991294 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m747385230(__this, ___value0, method) ((  int32_t (*) (Collection_1_t423991294 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m747385230_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Entity.Behavior.EBehaviorID>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_Insert_m2343012537_gshared (Collection_1_t423991294 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m2343012537(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t423991294 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m2343012537_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<Entity.Behavior.EBehaviorID>::System.Collections.IList.Remove(System.Object)
extern "C"  void Collection_1_System_Collections_IList_Remove_m3361695695_gshared (Collection_1_t423991294 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m3361695695(__this, ___value0, method) ((  void (*) (Collection_1_t423991294 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m3361695695_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Entity.Behavior.EBehaviorID>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m1063923270_gshared (Collection_1_t423991294 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m1063923270(__this, method) ((  bool (*) (Collection_1_t423991294 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m1063923270_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<Entity.Behavior.EBehaviorID>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Collection_1_System_Collections_ICollection_get_SyncRoot_m2924075442_gshared (Collection_1_t423991294 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m2924075442(__this, method) ((  Il2CppObject * (*) (Collection_1_t423991294 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m2924075442_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Entity.Behavior.EBehaviorID>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool Collection_1_System_Collections_IList_get_IsFixedSize_m2344192325_gshared (Collection_1_t423991294 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m2344192325(__this, method) ((  bool (*) (Collection_1_t423991294 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m2344192325_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Entity.Behavior.EBehaviorID>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_IList_get_IsReadOnly_m4270405908_gshared (Collection_1_t423991294 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m4270405908(__this, method) ((  bool (*) (Collection_1_t423991294 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m4270405908_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<Entity.Behavior.EBehaviorID>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * Collection_1_System_Collections_IList_get_Item_m2193499833_gshared (Collection_1_t423991294 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m2193499833(__this, ___index0, method) ((  Il2CppObject * (*) (Collection_1_t423991294 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m2193499833_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Entity.Behavior.EBehaviorID>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_set_Item_m1686137424_gshared (Collection_1_t423991294 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m1686137424(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t423991294 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m1686137424_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<Entity.Behavior.EBehaviorID>::Add(T)
extern "C"  void Collection_1_Add_m3250046427_gshared (Collection_1_t423991294 * __this, uint8_t ___item0, const MethodInfo* method);
#define Collection_1_Add_m3250046427(__this, ___item0, method) ((  void (*) (Collection_1_t423991294 *, uint8_t, const MethodInfo*))Collection_1_Add_m3250046427_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Entity.Behavior.EBehaviorID>::Clear()
extern "C"  void Collection_1_Clear_m3973882447_gshared (Collection_1_t423991294 * __this, const MethodInfo* method);
#define Collection_1_Clear_m3973882447(__this, method) ((  void (*) (Collection_1_t423991294 *, const MethodInfo*))Collection_1_Clear_m3973882447_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<Entity.Behavior.EBehaviorID>::ClearItems()
extern "C"  void Collection_1_ClearItems_m200480787_gshared (Collection_1_t423991294 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m200480787(__this, method) ((  void (*) (Collection_1_t423991294 *, const MethodInfo*))Collection_1_ClearItems_m200480787_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Entity.Behavior.EBehaviorID>::Contains(T)
extern "C"  bool Collection_1_Contains_m1309230589_gshared (Collection_1_t423991294 * __this, uint8_t ___item0, const MethodInfo* method);
#define Collection_1_Contains_m1309230589(__this, ___item0, method) ((  bool (*) (Collection_1_t423991294 *, uint8_t, const MethodInfo*))Collection_1_Contains_m1309230589_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Entity.Behavior.EBehaviorID>::CopyTo(T[],System.Int32)
extern "C"  void Collection_1_CopyTo_m1024986379_gshared (Collection_1_t423991294 * __this, EBehaviorIDU5BU5D_t373889457* ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_CopyTo_m1024986379(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t423991294 *, EBehaviorIDU5BU5D_t373889457*, int32_t, const MethodInfo*))Collection_1_CopyTo_m1024986379_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<Entity.Behavior.EBehaviorID>::GetEnumerator()
extern "C"  Il2CppObject* Collection_1_GetEnumerator_m2387037152_gshared (Collection_1_t423991294 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m2387037152(__this, method) ((  Il2CppObject* (*) (Collection_1_t423991294 *, const MethodInfo*))Collection_1_GetEnumerator_m2387037152_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Entity.Behavior.EBehaviorID>::IndexOf(T)
extern "C"  int32_t Collection_1_IndexOf_m3023496655_gshared (Collection_1_t423991294 * __this, uint8_t ___item0, const MethodInfo* method);
#define Collection_1_IndexOf_m3023496655(__this, ___item0, method) ((  int32_t (*) (Collection_1_t423991294 *, uint8_t, const MethodInfo*))Collection_1_IndexOf_m3023496655_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Entity.Behavior.EBehaviorID>::Insert(System.Int32,T)
extern "C"  void Collection_1_Insert_m1792797250_gshared (Collection_1_t423991294 * __this, int32_t ___index0, uint8_t ___item1, const MethodInfo* method);
#define Collection_1_Insert_m1792797250(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t423991294 *, int32_t, uint8_t, const MethodInfo*))Collection_1_Insert_m1792797250_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.ObjectModel.Collection`1<Entity.Behavior.EBehaviorID>::InsertItem(System.Int32,T)
extern "C"  void Collection_1_InsertItem_m3425707893_gshared (Collection_1_t423991294 * __this, int32_t ___index0, uint8_t ___item1, const MethodInfo* method);
#define Collection_1_InsertItem_m3425707893(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t423991294 *, int32_t, uint8_t, const MethodInfo*))Collection_1_InsertItem_m3425707893_gshared)(__this, ___index0, ___item1, method)
// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.Collection`1<Entity.Behavior.EBehaviorID>::get_Items()
extern "C"  Il2CppObject* Collection_1_get_Items_m471502179_gshared (Collection_1_t423991294 * __this, const MethodInfo* method);
#define Collection_1_get_Items_m471502179(__this, method) ((  Il2CppObject* (*) (Collection_1_t423991294 *, const MethodInfo*))Collection_1_get_Items_m471502179_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Entity.Behavior.EBehaviorID>::Remove(T)
extern "C"  bool Collection_1_Remove_m2682347832_gshared (Collection_1_t423991294 * __this, uint8_t ___item0, const MethodInfo* method);
#define Collection_1_Remove_m2682347832(__this, ___item0, method) ((  bool (*) (Collection_1_t423991294 *, uint8_t, const MethodInfo*))Collection_1_Remove_m2682347832_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Entity.Behavior.EBehaviorID>::RemoveAt(System.Int32)
extern "C"  void Collection_1_RemoveAt_m3961617416_gshared (Collection_1_t423991294 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveAt_m3961617416(__this, ___index0, method) ((  void (*) (Collection_1_t423991294 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m3961617416_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Entity.Behavior.EBehaviorID>::RemoveItem(System.Int32)
extern "C"  void Collection_1_RemoveItem_m377970216_gshared (Collection_1_t423991294 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveItem_m377970216(__this, ___index0, method) ((  void (*) (Collection_1_t423991294 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m377970216_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Entity.Behavior.EBehaviorID>::get_Count()
extern "C"  int32_t Collection_1_get_Count_m3351390604_gshared (Collection_1_t423991294 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m3351390604(__this, method) ((  int32_t (*) (Collection_1_t423991294 *, const MethodInfo*))Collection_1_get_Count_m3351390604_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<Entity.Behavior.EBehaviorID>::get_Item(System.Int32)
extern "C"  uint8_t Collection_1_get_Item_m2436774028_gshared (Collection_1_t423991294 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_get_Item_m2436774028(__this, ___index0, method) ((  uint8_t (*) (Collection_1_t423991294 *, int32_t, const MethodInfo*))Collection_1_get_Item_m2436774028_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Entity.Behavior.EBehaviorID>::set_Item(System.Int32,T)
extern "C"  void Collection_1_set_Item_m2405868185_gshared (Collection_1_t423991294 * __this, int32_t ___index0, uint8_t ___value1, const MethodInfo* method);
#define Collection_1_set_Item_m2405868185(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t423991294 *, int32_t, uint8_t, const MethodInfo*))Collection_1_set_Item_m2405868185_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<Entity.Behavior.EBehaviorID>::SetItem(System.Int32,T)
extern "C"  void Collection_1_SetItem_m1058729664_gshared (Collection_1_t423991294 * __this, int32_t ___index0, uint8_t ___item1, const MethodInfo* method);
#define Collection_1_SetItem_m1058729664(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t423991294 *, int32_t, uint8_t, const MethodInfo*))Collection_1_SetItem_m1058729664_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Entity.Behavior.EBehaviorID>::IsValidItem(System.Object)
extern "C"  bool Collection_1_IsValidItem_m2953630351_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_IsValidItem_m2953630351(__this /* static, unused */, ___item0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_IsValidItem_m2953630351_gshared)(__this /* static, unused */, ___item0, method)
// T System.Collections.ObjectModel.Collection`1<Entity.Behavior.EBehaviorID>::ConvertItem(System.Object)
extern "C"  uint8_t Collection_1_ConvertItem_m844153195_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_ConvertItem_m844153195(__this /* static, unused */, ___item0, method) ((  uint8_t (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_ConvertItem_m844153195_gshared)(__this /* static, unused */, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Entity.Behavior.EBehaviorID>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C"  void Collection_1_CheckWritable_m2579327051_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_CheckWritable_m2579327051(__this /* static, unused */, ___list0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_CheckWritable_m2579327051_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Entity.Behavior.EBehaviorID>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsSynchronized_m745131733_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsSynchronized_m745131733(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsSynchronized_m745131733_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Entity.Behavior.EBehaviorID>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsFixedSize_m3331977194_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsFixedSize_m3331977194(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsFixedSize_m3331977194_gshared)(__this /* static, unused */, ___list0, method)
