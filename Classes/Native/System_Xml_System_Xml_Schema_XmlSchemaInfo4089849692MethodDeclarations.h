﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Xml.Schema.XmlSchemaInfo
struct XmlSchemaInfo_t4089849692;
// System.Xml.Schema.IXmlSchemaInfo
struct IXmlSchemaInfo_t4085280001;
// System.Xml.Schema.XmlSchemaSimpleType
struct XmlSchemaSimpleType_t3060492794;
// System.Xml.Schema.XmlSchemaAttribute
struct XmlSchemaAttribute_t2904598248;
// System.Xml.Schema.XmlSchemaElement
struct XmlSchemaElement_t3664762632;
// System.Xml.Schema.XmlSchemaType
struct XmlSchemaType_t4090188264;

#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaSimpleType3060492794.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaAttribute2904598248.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaElement3664762632.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaType4090188264.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaValidity1280069984.h"

// System.Void System.Xml.Schema.XmlSchemaInfo::.ctor()
extern "C"  void XmlSchemaInfo__ctor_m699610770 (XmlSchemaInfo_t4089849692 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Schema.XmlSchemaInfo::.ctor(System.Xml.Schema.IXmlSchemaInfo)
extern "C"  void XmlSchemaInfo__ctor_m167721790 (XmlSchemaInfo_t4089849692 * __this, Il2CppObject * ___info0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.Schema.XmlSchemaInfo::get_IsDefault()
extern "C"  bool XmlSchemaInfo_get_IsDefault_m766054214 (XmlSchemaInfo_t4089849692 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Schema.XmlSchemaInfo::set_IsDefault(System.Boolean)
extern "C"  void XmlSchemaInfo_set_IsDefault_m3042442305 (XmlSchemaInfo_t4089849692 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.Schema.XmlSchemaInfo::get_IsNil()
extern "C"  bool XmlSchemaInfo_get_IsNil_m3284398358 (XmlSchemaInfo_t4089849692 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Schema.XmlSchemaInfo::set_IsNil(System.Boolean)
extern "C"  void XmlSchemaInfo_set_IsNil_m2378765329 (XmlSchemaInfo_t4089849692 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaInfo::get_MemberType()
extern "C"  XmlSchemaSimpleType_t3060492794 * XmlSchemaInfo_get_MemberType_m642917129 (XmlSchemaInfo_t4089849692 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Schema.XmlSchemaInfo::set_MemberType(System.Xml.Schema.XmlSchemaSimpleType)
extern "C"  void XmlSchemaInfo_set_MemberType_m73073538 (XmlSchemaInfo_t4089849692 * __this, XmlSchemaSimpleType_t3060492794 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.Schema.XmlSchemaAttribute System.Xml.Schema.XmlSchemaInfo::get_SchemaAttribute()
extern "C"  XmlSchemaAttribute_t2904598248 * XmlSchemaInfo_get_SchemaAttribute_m3550303800 (XmlSchemaInfo_t4089849692 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Schema.XmlSchemaInfo::set_SchemaAttribute(System.Xml.Schema.XmlSchemaAttribute)
extern "C"  void XmlSchemaInfo_set_SchemaAttribute_m3641058099 (XmlSchemaInfo_t4089849692 * __this, XmlSchemaAttribute_t2904598248 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.Schema.XmlSchemaElement System.Xml.Schema.XmlSchemaInfo::get_SchemaElement()
extern "C"  XmlSchemaElement_t3664762632 * XmlSchemaInfo_get_SchemaElement_m4072892664 (XmlSchemaInfo_t4089849692 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Schema.XmlSchemaInfo::set_SchemaElement(System.Xml.Schema.XmlSchemaElement)
extern "C"  void XmlSchemaInfo_set_SchemaElement_m1452992755 (XmlSchemaInfo_t4089849692 * __this, XmlSchemaElement_t3664762632 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.Schema.XmlSchemaType System.Xml.Schema.XmlSchemaInfo::get_SchemaType()
extern "C"  XmlSchemaType_t4090188264 * XmlSchemaInfo_get_SchemaType_m3793507454 (XmlSchemaInfo_t4089849692 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Schema.XmlSchemaInfo::set_SchemaType(System.Xml.Schema.XmlSchemaType)
extern "C"  void XmlSchemaInfo_set_SchemaType_m130609965 (XmlSchemaInfo_t4089849692 * __this, XmlSchemaType_t4090188264 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.Schema.XmlSchemaValidity System.Xml.Schema.XmlSchemaInfo::get_Validity()
extern "C"  int32_t XmlSchemaInfo_get_Validity_m298160621 (XmlSchemaInfo_t4089849692 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
