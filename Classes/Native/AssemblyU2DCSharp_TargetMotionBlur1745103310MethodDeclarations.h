﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TargetMotionBlur
struct TargetMotionBlur_t1745103310;
// UnityEngine.Transform
struct Transform_t1659122786;
// UnityEngine.Camera
struct Camera_t2727095145;
// TweenPosition
struct TweenPosition_t3684358292;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// TimeMgr
struct TimeMgr_t350708715;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Transform1659122786.h"
#include "UnityEngine_UnityEngine_Camera2727095145.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_TimeMgr350708715.h"
#include "mscorlib_System_String7231557.h"

// System.Void TargetMotionBlur::.ctor()
extern "C"  void TargetMotionBlur__ctor_m2601002125 (TargetMotionBlur_t1745103310 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TargetMotionBlur::Init(UnityEngine.Transform,UnityEngine.Camera,System.Single,System.Single,System.Single)
extern "C"  void TargetMotionBlur_Init_m848132067 (TargetMotionBlur_t1745103310 * __this, Transform_t1659122786 * ___target0, Camera_t2727095145 * ___camera1, float ___minDistance2, float ___startVelocity3, float ___endVelocity4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TargetMotionBlur::Start()
extern "C"  void TargetMotionBlur_Start_m1548139917 (TargetMotionBlur_t1745103310 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TargetMotionBlur::Update()
extern "C"  void TargetMotionBlur_Update_m753549344 (TargetMotionBlur_t1745103310 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TargetMotionBlur::OnEnable()
extern "C"  void TargetMotionBlur_OnEnable_m3020897241 (TargetMotionBlur_t1745103310 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TargetMotionBlur::OnDisable()
extern "C"  void TargetMotionBlur_OnDisable_m3894438516 (TargetMotionBlur_t1745103310 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TargetMotionBlur::updateCameraMotion()
extern "C"  void TargetMotionBlur_updateCameraMotion_m2570059291 (TargetMotionBlur_t1745103310 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TweenPosition TargetMotionBlur::ilo_Begin1(UnityEngine.GameObject,System.Single,UnityEngine.Vector3)
extern "C"  TweenPosition_t3684358292 * TargetMotionBlur_ilo_Begin1_m2092302689 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___go0, float ___duration1, Vector3_t4282066566  ___pos2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TimeMgr TargetMotionBlur::ilo_get_TimeMgr2()
extern "C"  TimeMgr_t350708715 * TargetMotionBlur_ilo_get_TimeMgr2_m537474530 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TargetMotionBlur::ilo_AddTimeScale3(TimeMgr,System.Single,System.String)
extern "C"  void TargetMotionBlur_ilo_AddTimeScale3_m1979824697 (Il2CppObject * __this /* static, unused */, TimeMgr_t350708715 * ____this0, float ___timeScale1, String_t* ___timeKey2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TargetMotionBlur::ilo_DelTimeScale4(TimeMgr,System.Single,System.String)
extern "C"  void TargetMotionBlur_ilo_DelTimeScale4_m2049962542 (Il2CppObject * __this /* static, unused */, TimeMgr_t350708715 * ____this0, float ___timeScale1, String_t* ___timeKey2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
