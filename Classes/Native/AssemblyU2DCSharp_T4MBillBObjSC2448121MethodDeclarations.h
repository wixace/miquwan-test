﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// T4MBillBObjSC
struct T4MBillBObjSC_t2448121;

#include "codegen/il2cpp-codegen.h"

// System.Void T4MBillBObjSC::.ctor()
extern "C"  void T4MBillBObjSC__ctor_m2909359762 (T4MBillBObjSC_t2448121 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
