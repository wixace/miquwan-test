﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_baydee91
struct  M_baydee91_t1061612434  : public Il2CppObject
{
public:
	// System.UInt32 GarbageiOS.M_baydee91::_zomekiTapasmi
	uint32_t ____zomekiTapasmi_0;
	// System.String GarbageiOS.M_baydee91::_bohi
	String_t* ____bohi_1;
	// System.String GarbageiOS.M_baydee91::_rornurgeaWasaw
	String_t* ____rornurgeaWasaw_2;
	// System.Boolean GarbageiOS.M_baydee91::_janear
	bool ____janear_3;
	// System.Boolean GarbageiOS.M_baydee91::_secorsiGeagow
	bool ____secorsiGeagow_4;
	// System.Single GarbageiOS.M_baydee91::_teldija
	float ____teldija_5;
	// System.Boolean GarbageiOS.M_baydee91::_hairsaihoPeyo
	bool ____hairsaihoPeyo_6;

public:
	inline static int32_t get_offset_of__zomekiTapasmi_0() { return static_cast<int32_t>(offsetof(M_baydee91_t1061612434, ____zomekiTapasmi_0)); }
	inline uint32_t get__zomekiTapasmi_0() const { return ____zomekiTapasmi_0; }
	inline uint32_t* get_address_of__zomekiTapasmi_0() { return &____zomekiTapasmi_0; }
	inline void set__zomekiTapasmi_0(uint32_t value)
	{
		____zomekiTapasmi_0 = value;
	}

	inline static int32_t get_offset_of__bohi_1() { return static_cast<int32_t>(offsetof(M_baydee91_t1061612434, ____bohi_1)); }
	inline String_t* get__bohi_1() const { return ____bohi_1; }
	inline String_t** get_address_of__bohi_1() { return &____bohi_1; }
	inline void set__bohi_1(String_t* value)
	{
		____bohi_1 = value;
		Il2CppCodeGenWriteBarrier(&____bohi_1, value);
	}

	inline static int32_t get_offset_of__rornurgeaWasaw_2() { return static_cast<int32_t>(offsetof(M_baydee91_t1061612434, ____rornurgeaWasaw_2)); }
	inline String_t* get__rornurgeaWasaw_2() const { return ____rornurgeaWasaw_2; }
	inline String_t** get_address_of__rornurgeaWasaw_2() { return &____rornurgeaWasaw_2; }
	inline void set__rornurgeaWasaw_2(String_t* value)
	{
		____rornurgeaWasaw_2 = value;
		Il2CppCodeGenWriteBarrier(&____rornurgeaWasaw_2, value);
	}

	inline static int32_t get_offset_of__janear_3() { return static_cast<int32_t>(offsetof(M_baydee91_t1061612434, ____janear_3)); }
	inline bool get__janear_3() const { return ____janear_3; }
	inline bool* get_address_of__janear_3() { return &____janear_3; }
	inline void set__janear_3(bool value)
	{
		____janear_3 = value;
	}

	inline static int32_t get_offset_of__secorsiGeagow_4() { return static_cast<int32_t>(offsetof(M_baydee91_t1061612434, ____secorsiGeagow_4)); }
	inline bool get__secorsiGeagow_4() const { return ____secorsiGeagow_4; }
	inline bool* get_address_of__secorsiGeagow_4() { return &____secorsiGeagow_4; }
	inline void set__secorsiGeagow_4(bool value)
	{
		____secorsiGeagow_4 = value;
	}

	inline static int32_t get_offset_of__teldija_5() { return static_cast<int32_t>(offsetof(M_baydee91_t1061612434, ____teldija_5)); }
	inline float get__teldija_5() const { return ____teldija_5; }
	inline float* get_address_of__teldija_5() { return &____teldija_5; }
	inline void set__teldija_5(float value)
	{
		____teldija_5 = value;
	}

	inline static int32_t get_offset_of__hairsaihoPeyo_6() { return static_cast<int32_t>(offsetof(M_baydee91_t1061612434, ____hairsaihoPeyo_6)); }
	inline bool get__hairsaihoPeyo_6() const { return ____hairsaihoPeyo_6; }
	inline bool* get_address_of__hairsaihoPeyo_6() { return &____hairsaihoPeyo_6; }
	inline void set__hairsaihoPeyo_6(bool value)
	{
		____hairsaihoPeyo_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
