﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_trearvade130
struct  M_trearvade130_t1924839578  : public Il2CppObject
{
public:
	// System.UInt32 GarbageiOS.M_trearvade130::_hayjeyow
	uint32_t ____hayjeyow_0;
	// System.Boolean GarbageiOS.M_trearvade130::_fosu
	bool ____fosu_1;
	// System.Int32 GarbageiOS.M_trearvade130::_pemgarDraroo
	int32_t ____pemgarDraroo_2;
	// System.String GarbageiOS.M_trearvade130::_rawse
	String_t* ____rawse_3;
	// System.Boolean GarbageiOS.M_trearvade130::_mukay
	bool ____mukay_4;
	// System.String GarbageiOS.M_trearvade130::_narxesisCearsallcea
	String_t* ____narxesisCearsallcea_5;
	// System.Single GarbageiOS.M_trearvade130::_rairjouDrasjearsor
	float ____rairjouDrasjearsor_6;
	// System.String GarbageiOS.M_trearvade130::_nebir
	String_t* ____nebir_7;
	// System.UInt32 GarbageiOS.M_trearvade130::_drearavelLirle
	uint32_t ____drearavelLirle_8;
	// System.Single GarbageiOS.M_trearvade130::_sekeqear
	float ____sekeqear_9;
	// System.String GarbageiOS.M_trearvade130::_nejorNowtere
	String_t* ____nejorNowtere_10;
	// System.Single GarbageiOS.M_trearvade130::_saydurpelKehaw
	float ____saydurpelKehaw_11;
	// System.Boolean GarbageiOS.M_trearvade130::_douledi
	bool ____douledi_12;
	// System.Int32 GarbageiOS.M_trearvade130::_deazouLowgi
	int32_t ____deazouLowgi_13;
	// System.Int32 GarbageiOS.M_trearvade130::_bagor
	int32_t ____bagor_14;
	// System.Single GarbageiOS.M_trearvade130::_laydraicemNearnerher
	float ____laydraicemNearnerher_15;
	// System.String GarbageiOS.M_trearvade130::_tojallta
	String_t* ____tojallta_16;
	// System.Boolean GarbageiOS.M_trearvade130::_misdaPorhallsair
	bool ____misdaPorhallsair_17;
	// System.Boolean GarbageiOS.M_trearvade130::_jeefairme
	bool ____jeefairme_18;
	// System.Boolean GarbageiOS.M_trearvade130::_kedujemSorraile
	bool ____kedujemSorraile_19;
	// System.String GarbageiOS.M_trearvade130::_whawheTatrall
	String_t* ____whawheTatrall_20;
	// System.Int32 GarbageiOS.M_trearvade130::_hedosasDrowwhow
	int32_t ____hedosasDrowwhow_21;
	// System.Int32 GarbageiOS.M_trearvade130::_wayhoCaldee
	int32_t ____wayhoCaldee_22;
	// System.Single GarbageiOS.M_trearvade130::_rasitere
	float ____rasitere_23;
	// System.Boolean GarbageiOS.M_trearvade130::_lerporlaGotelco
	bool ____lerporlaGotelco_24;
	// System.String GarbageiOS.M_trearvade130::_mearbairBaytebo
	String_t* ____mearbairBaytebo_25;
	// System.Int32 GarbageiOS.M_trearvade130::_xerdata
	int32_t ____xerdata_26;
	// System.Single GarbageiOS.M_trearvade130::_donorXayoo
	float ____donorXayoo_27;
	// System.Int32 GarbageiOS.M_trearvade130::_cadreesi
	int32_t ____cadreesi_28;
	// System.Boolean GarbageiOS.M_trearvade130::_murwhis
	bool ____murwhis_29;
	// System.Single GarbageiOS.M_trearvade130::_nuvudalCewaw
	float ____nuvudalCewaw_30;
	// System.Boolean GarbageiOS.M_trearvade130::_relsa
	bool ____relsa_31;
	// System.UInt32 GarbageiOS.M_trearvade130::_lawtoloTitis
	uint32_t ____lawtoloTitis_32;
	// System.Int32 GarbageiOS.M_trearvade130::_gerewee
	int32_t ____gerewee_33;
	// System.Single GarbageiOS.M_trearvade130::_rikuhu
	float ____rikuhu_34;
	// System.UInt32 GarbageiOS.M_trearvade130::_patete
	uint32_t ____patete_35;

public:
	inline static int32_t get_offset_of__hayjeyow_0() { return static_cast<int32_t>(offsetof(M_trearvade130_t1924839578, ____hayjeyow_0)); }
	inline uint32_t get__hayjeyow_0() const { return ____hayjeyow_0; }
	inline uint32_t* get_address_of__hayjeyow_0() { return &____hayjeyow_0; }
	inline void set__hayjeyow_0(uint32_t value)
	{
		____hayjeyow_0 = value;
	}

	inline static int32_t get_offset_of__fosu_1() { return static_cast<int32_t>(offsetof(M_trearvade130_t1924839578, ____fosu_1)); }
	inline bool get__fosu_1() const { return ____fosu_1; }
	inline bool* get_address_of__fosu_1() { return &____fosu_1; }
	inline void set__fosu_1(bool value)
	{
		____fosu_1 = value;
	}

	inline static int32_t get_offset_of__pemgarDraroo_2() { return static_cast<int32_t>(offsetof(M_trearvade130_t1924839578, ____pemgarDraroo_2)); }
	inline int32_t get__pemgarDraroo_2() const { return ____pemgarDraroo_2; }
	inline int32_t* get_address_of__pemgarDraroo_2() { return &____pemgarDraroo_2; }
	inline void set__pemgarDraroo_2(int32_t value)
	{
		____pemgarDraroo_2 = value;
	}

	inline static int32_t get_offset_of__rawse_3() { return static_cast<int32_t>(offsetof(M_trearvade130_t1924839578, ____rawse_3)); }
	inline String_t* get__rawse_3() const { return ____rawse_3; }
	inline String_t** get_address_of__rawse_3() { return &____rawse_3; }
	inline void set__rawse_3(String_t* value)
	{
		____rawse_3 = value;
		Il2CppCodeGenWriteBarrier(&____rawse_3, value);
	}

	inline static int32_t get_offset_of__mukay_4() { return static_cast<int32_t>(offsetof(M_trearvade130_t1924839578, ____mukay_4)); }
	inline bool get__mukay_4() const { return ____mukay_4; }
	inline bool* get_address_of__mukay_4() { return &____mukay_4; }
	inline void set__mukay_4(bool value)
	{
		____mukay_4 = value;
	}

	inline static int32_t get_offset_of__narxesisCearsallcea_5() { return static_cast<int32_t>(offsetof(M_trearvade130_t1924839578, ____narxesisCearsallcea_5)); }
	inline String_t* get__narxesisCearsallcea_5() const { return ____narxesisCearsallcea_5; }
	inline String_t** get_address_of__narxesisCearsallcea_5() { return &____narxesisCearsallcea_5; }
	inline void set__narxesisCearsallcea_5(String_t* value)
	{
		____narxesisCearsallcea_5 = value;
		Il2CppCodeGenWriteBarrier(&____narxesisCearsallcea_5, value);
	}

	inline static int32_t get_offset_of__rairjouDrasjearsor_6() { return static_cast<int32_t>(offsetof(M_trearvade130_t1924839578, ____rairjouDrasjearsor_6)); }
	inline float get__rairjouDrasjearsor_6() const { return ____rairjouDrasjearsor_6; }
	inline float* get_address_of__rairjouDrasjearsor_6() { return &____rairjouDrasjearsor_6; }
	inline void set__rairjouDrasjearsor_6(float value)
	{
		____rairjouDrasjearsor_6 = value;
	}

	inline static int32_t get_offset_of__nebir_7() { return static_cast<int32_t>(offsetof(M_trearvade130_t1924839578, ____nebir_7)); }
	inline String_t* get__nebir_7() const { return ____nebir_7; }
	inline String_t** get_address_of__nebir_7() { return &____nebir_7; }
	inline void set__nebir_7(String_t* value)
	{
		____nebir_7 = value;
		Il2CppCodeGenWriteBarrier(&____nebir_7, value);
	}

	inline static int32_t get_offset_of__drearavelLirle_8() { return static_cast<int32_t>(offsetof(M_trearvade130_t1924839578, ____drearavelLirle_8)); }
	inline uint32_t get__drearavelLirle_8() const { return ____drearavelLirle_8; }
	inline uint32_t* get_address_of__drearavelLirle_8() { return &____drearavelLirle_8; }
	inline void set__drearavelLirle_8(uint32_t value)
	{
		____drearavelLirle_8 = value;
	}

	inline static int32_t get_offset_of__sekeqear_9() { return static_cast<int32_t>(offsetof(M_trearvade130_t1924839578, ____sekeqear_9)); }
	inline float get__sekeqear_9() const { return ____sekeqear_9; }
	inline float* get_address_of__sekeqear_9() { return &____sekeqear_9; }
	inline void set__sekeqear_9(float value)
	{
		____sekeqear_9 = value;
	}

	inline static int32_t get_offset_of__nejorNowtere_10() { return static_cast<int32_t>(offsetof(M_trearvade130_t1924839578, ____nejorNowtere_10)); }
	inline String_t* get__nejorNowtere_10() const { return ____nejorNowtere_10; }
	inline String_t** get_address_of__nejorNowtere_10() { return &____nejorNowtere_10; }
	inline void set__nejorNowtere_10(String_t* value)
	{
		____nejorNowtere_10 = value;
		Il2CppCodeGenWriteBarrier(&____nejorNowtere_10, value);
	}

	inline static int32_t get_offset_of__saydurpelKehaw_11() { return static_cast<int32_t>(offsetof(M_trearvade130_t1924839578, ____saydurpelKehaw_11)); }
	inline float get__saydurpelKehaw_11() const { return ____saydurpelKehaw_11; }
	inline float* get_address_of__saydurpelKehaw_11() { return &____saydurpelKehaw_11; }
	inline void set__saydurpelKehaw_11(float value)
	{
		____saydurpelKehaw_11 = value;
	}

	inline static int32_t get_offset_of__douledi_12() { return static_cast<int32_t>(offsetof(M_trearvade130_t1924839578, ____douledi_12)); }
	inline bool get__douledi_12() const { return ____douledi_12; }
	inline bool* get_address_of__douledi_12() { return &____douledi_12; }
	inline void set__douledi_12(bool value)
	{
		____douledi_12 = value;
	}

	inline static int32_t get_offset_of__deazouLowgi_13() { return static_cast<int32_t>(offsetof(M_trearvade130_t1924839578, ____deazouLowgi_13)); }
	inline int32_t get__deazouLowgi_13() const { return ____deazouLowgi_13; }
	inline int32_t* get_address_of__deazouLowgi_13() { return &____deazouLowgi_13; }
	inline void set__deazouLowgi_13(int32_t value)
	{
		____deazouLowgi_13 = value;
	}

	inline static int32_t get_offset_of__bagor_14() { return static_cast<int32_t>(offsetof(M_trearvade130_t1924839578, ____bagor_14)); }
	inline int32_t get__bagor_14() const { return ____bagor_14; }
	inline int32_t* get_address_of__bagor_14() { return &____bagor_14; }
	inline void set__bagor_14(int32_t value)
	{
		____bagor_14 = value;
	}

	inline static int32_t get_offset_of__laydraicemNearnerher_15() { return static_cast<int32_t>(offsetof(M_trearvade130_t1924839578, ____laydraicemNearnerher_15)); }
	inline float get__laydraicemNearnerher_15() const { return ____laydraicemNearnerher_15; }
	inline float* get_address_of__laydraicemNearnerher_15() { return &____laydraicemNearnerher_15; }
	inline void set__laydraicemNearnerher_15(float value)
	{
		____laydraicemNearnerher_15 = value;
	}

	inline static int32_t get_offset_of__tojallta_16() { return static_cast<int32_t>(offsetof(M_trearvade130_t1924839578, ____tojallta_16)); }
	inline String_t* get__tojallta_16() const { return ____tojallta_16; }
	inline String_t** get_address_of__tojallta_16() { return &____tojallta_16; }
	inline void set__tojallta_16(String_t* value)
	{
		____tojallta_16 = value;
		Il2CppCodeGenWriteBarrier(&____tojallta_16, value);
	}

	inline static int32_t get_offset_of__misdaPorhallsair_17() { return static_cast<int32_t>(offsetof(M_trearvade130_t1924839578, ____misdaPorhallsair_17)); }
	inline bool get__misdaPorhallsair_17() const { return ____misdaPorhallsair_17; }
	inline bool* get_address_of__misdaPorhallsair_17() { return &____misdaPorhallsair_17; }
	inline void set__misdaPorhallsair_17(bool value)
	{
		____misdaPorhallsair_17 = value;
	}

	inline static int32_t get_offset_of__jeefairme_18() { return static_cast<int32_t>(offsetof(M_trearvade130_t1924839578, ____jeefairme_18)); }
	inline bool get__jeefairme_18() const { return ____jeefairme_18; }
	inline bool* get_address_of__jeefairme_18() { return &____jeefairme_18; }
	inline void set__jeefairme_18(bool value)
	{
		____jeefairme_18 = value;
	}

	inline static int32_t get_offset_of__kedujemSorraile_19() { return static_cast<int32_t>(offsetof(M_trearvade130_t1924839578, ____kedujemSorraile_19)); }
	inline bool get__kedujemSorraile_19() const { return ____kedujemSorraile_19; }
	inline bool* get_address_of__kedujemSorraile_19() { return &____kedujemSorraile_19; }
	inline void set__kedujemSorraile_19(bool value)
	{
		____kedujemSorraile_19 = value;
	}

	inline static int32_t get_offset_of__whawheTatrall_20() { return static_cast<int32_t>(offsetof(M_trearvade130_t1924839578, ____whawheTatrall_20)); }
	inline String_t* get__whawheTatrall_20() const { return ____whawheTatrall_20; }
	inline String_t** get_address_of__whawheTatrall_20() { return &____whawheTatrall_20; }
	inline void set__whawheTatrall_20(String_t* value)
	{
		____whawheTatrall_20 = value;
		Il2CppCodeGenWriteBarrier(&____whawheTatrall_20, value);
	}

	inline static int32_t get_offset_of__hedosasDrowwhow_21() { return static_cast<int32_t>(offsetof(M_trearvade130_t1924839578, ____hedosasDrowwhow_21)); }
	inline int32_t get__hedosasDrowwhow_21() const { return ____hedosasDrowwhow_21; }
	inline int32_t* get_address_of__hedosasDrowwhow_21() { return &____hedosasDrowwhow_21; }
	inline void set__hedosasDrowwhow_21(int32_t value)
	{
		____hedosasDrowwhow_21 = value;
	}

	inline static int32_t get_offset_of__wayhoCaldee_22() { return static_cast<int32_t>(offsetof(M_trearvade130_t1924839578, ____wayhoCaldee_22)); }
	inline int32_t get__wayhoCaldee_22() const { return ____wayhoCaldee_22; }
	inline int32_t* get_address_of__wayhoCaldee_22() { return &____wayhoCaldee_22; }
	inline void set__wayhoCaldee_22(int32_t value)
	{
		____wayhoCaldee_22 = value;
	}

	inline static int32_t get_offset_of__rasitere_23() { return static_cast<int32_t>(offsetof(M_trearvade130_t1924839578, ____rasitere_23)); }
	inline float get__rasitere_23() const { return ____rasitere_23; }
	inline float* get_address_of__rasitere_23() { return &____rasitere_23; }
	inline void set__rasitere_23(float value)
	{
		____rasitere_23 = value;
	}

	inline static int32_t get_offset_of__lerporlaGotelco_24() { return static_cast<int32_t>(offsetof(M_trearvade130_t1924839578, ____lerporlaGotelco_24)); }
	inline bool get__lerporlaGotelco_24() const { return ____lerporlaGotelco_24; }
	inline bool* get_address_of__lerporlaGotelco_24() { return &____lerporlaGotelco_24; }
	inline void set__lerporlaGotelco_24(bool value)
	{
		____lerporlaGotelco_24 = value;
	}

	inline static int32_t get_offset_of__mearbairBaytebo_25() { return static_cast<int32_t>(offsetof(M_trearvade130_t1924839578, ____mearbairBaytebo_25)); }
	inline String_t* get__mearbairBaytebo_25() const { return ____mearbairBaytebo_25; }
	inline String_t** get_address_of__mearbairBaytebo_25() { return &____mearbairBaytebo_25; }
	inline void set__mearbairBaytebo_25(String_t* value)
	{
		____mearbairBaytebo_25 = value;
		Il2CppCodeGenWriteBarrier(&____mearbairBaytebo_25, value);
	}

	inline static int32_t get_offset_of__xerdata_26() { return static_cast<int32_t>(offsetof(M_trearvade130_t1924839578, ____xerdata_26)); }
	inline int32_t get__xerdata_26() const { return ____xerdata_26; }
	inline int32_t* get_address_of__xerdata_26() { return &____xerdata_26; }
	inline void set__xerdata_26(int32_t value)
	{
		____xerdata_26 = value;
	}

	inline static int32_t get_offset_of__donorXayoo_27() { return static_cast<int32_t>(offsetof(M_trearvade130_t1924839578, ____donorXayoo_27)); }
	inline float get__donorXayoo_27() const { return ____donorXayoo_27; }
	inline float* get_address_of__donorXayoo_27() { return &____donorXayoo_27; }
	inline void set__donorXayoo_27(float value)
	{
		____donorXayoo_27 = value;
	}

	inline static int32_t get_offset_of__cadreesi_28() { return static_cast<int32_t>(offsetof(M_trearvade130_t1924839578, ____cadreesi_28)); }
	inline int32_t get__cadreesi_28() const { return ____cadreesi_28; }
	inline int32_t* get_address_of__cadreesi_28() { return &____cadreesi_28; }
	inline void set__cadreesi_28(int32_t value)
	{
		____cadreesi_28 = value;
	}

	inline static int32_t get_offset_of__murwhis_29() { return static_cast<int32_t>(offsetof(M_trearvade130_t1924839578, ____murwhis_29)); }
	inline bool get__murwhis_29() const { return ____murwhis_29; }
	inline bool* get_address_of__murwhis_29() { return &____murwhis_29; }
	inline void set__murwhis_29(bool value)
	{
		____murwhis_29 = value;
	}

	inline static int32_t get_offset_of__nuvudalCewaw_30() { return static_cast<int32_t>(offsetof(M_trearvade130_t1924839578, ____nuvudalCewaw_30)); }
	inline float get__nuvudalCewaw_30() const { return ____nuvudalCewaw_30; }
	inline float* get_address_of__nuvudalCewaw_30() { return &____nuvudalCewaw_30; }
	inline void set__nuvudalCewaw_30(float value)
	{
		____nuvudalCewaw_30 = value;
	}

	inline static int32_t get_offset_of__relsa_31() { return static_cast<int32_t>(offsetof(M_trearvade130_t1924839578, ____relsa_31)); }
	inline bool get__relsa_31() const { return ____relsa_31; }
	inline bool* get_address_of__relsa_31() { return &____relsa_31; }
	inline void set__relsa_31(bool value)
	{
		____relsa_31 = value;
	}

	inline static int32_t get_offset_of__lawtoloTitis_32() { return static_cast<int32_t>(offsetof(M_trearvade130_t1924839578, ____lawtoloTitis_32)); }
	inline uint32_t get__lawtoloTitis_32() const { return ____lawtoloTitis_32; }
	inline uint32_t* get_address_of__lawtoloTitis_32() { return &____lawtoloTitis_32; }
	inline void set__lawtoloTitis_32(uint32_t value)
	{
		____lawtoloTitis_32 = value;
	}

	inline static int32_t get_offset_of__gerewee_33() { return static_cast<int32_t>(offsetof(M_trearvade130_t1924839578, ____gerewee_33)); }
	inline int32_t get__gerewee_33() const { return ____gerewee_33; }
	inline int32_t* get_address_of__gerewee_33() { return &____gerewee_33; }
	inline void set__gerewee_33(int32_t value)
	{
		____gerewee_33 = value;
	}

	inline static int32_t get_offset_of__rikuhu_34() { return static_cast<int32_t>(offsetof(M_trearvade130_t1924839578, ____rikuhu_34)); }
	inline float get__rikuhu_34() const { return ____rikuhu_34; }
	inline float* get_address_of__rikuhu_34() { return &____rikuhu_34; }
	inline void set__rikuhu_34(float value)
	{
		____rikuhu_34 = value;
	}

	inline static int32_t get_offset_of__patete_35() { return static_cast<int32_t>(offsetof(M_trearvade130_t1924839578, ____patete_35)); }
	inline uint32_t get__patete_35() const { return ____patete_35; }
	inline uint32_t* get_address_of__patete_35() { return &____patete_35; }
	inline void set__patete_35(uint32_t value)
	{
		____patete_35 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
