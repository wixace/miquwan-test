﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Object>
struct Dictionary_2_t1453516396;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K2068452450.h"
#include "AssemblyU2DCSharp_Pathfinding_Int31974045594.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<Pathfinding.Int3,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m1559684605_gshared (Enumerator_t2068452450 * __this, Dictionary_2_t1453516396 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m1559684605(__this, ___host0, method) ((  void (*) (Enumerator_t2068452450 *, Dictionary_2_t1453516396 *, const MethodInfo*))Enumerator__ctor_m1559684605_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<Pathfinding.Int3,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3614769742_gshared (Enumerator_t2068452450 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m3614769742(__this, method) ((  Il2CppObject * (*) (Enumerator_t2068452450 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3614769742_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<Pathfinding.Int3,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2615297688_gshared (Enumerator_t2068452450 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m2615297688(__this, method) ((  void (*) (Enumerator_t2068452450 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2615297688_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<Pathfinding.Int3,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m1492276703_gshared (Enumerator_t2068452450 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1492276703(__this, method) ((  void (*) (Enumerator_t2068452450 *, const MethodInfo*))Enumerator_Dispose_m1492276703_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<Pathfinding.Int3,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2385368008_gshared (Enumerator_t2068452450 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m2385368008(__this, method) ((  bool (*) (Enumerator_t2068452450 *, const MethodInfo*))Enumerator_MoveNext_m2385368008_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<Pathfinding.Int3,System.Object>::get_Current()
extern "C"  Int3_t1974045594  Enumerator_get_Current_m3039941968_gshared (Enumerator_t2068452450 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m3039941968(__this, method) ((  Int3_t1974045594  (*) (Enumerator_t2068452450 *, const MethodInfo*))Enumerator_get_Current_m3039941968_gshared)(__this, method)
