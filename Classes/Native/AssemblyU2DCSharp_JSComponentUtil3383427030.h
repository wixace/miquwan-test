﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_JSComponent1642894772.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JSComponentUtil
struct  JSComponentUtil_t3383427030  : public JSComponent_t1642894772
{
public:
	// System.Int32 JSComponentUtil::idIsInheritanceRel
	int32_t ___idIsInheritanceRel_16;

public:
	inline static int32_t get_offset_of_idIsInheritanceRel_16() { return static_cast<int32_t>(offsetof(JSComponentUtil_t3383427030, ___idIsInheritanceRel_16)); }
	inline int32_t get_idIsInheritanceRel_16() const { return ___idIsInheritanceRel_16; }
	inline int32_t* get_address_of_idIsInheritanceRel_16() { return &___idIsInheritanceRel_16; }
	inline void set_idIsInheritanceRel_16(int32_t value)
	{
		___idIsInheritanceRel_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
