﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.SimpleSmoothModifier
struct SimpleSmoothModifier_t1067843181;
// Pathfinding.Path
struct Path_t1974241691;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t1355284822;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_ModifierData4046421655.h"
#include "AssemblyU2DCSharp_Pathfinding_Path1974241691.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_Pathfinding_SimpleSmoothModifier1067843181.h"

// System.Void Pathfinding.SimpleSmoothModifier::.ctor()
extern "C"  void SimpleSmoothModifier__ctor_m1814256282 (SimpleSmoothModifier_t1067843181 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.ModifierData Pathfinding.SimpleSmoothModifier::get_input()
extern "C"  int32_t SimpleSmoothModifier_get_input_m3389850663 (SimpleSmoothModifier_t1067843181 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.ModifierData Pathfinding.SimpleSmoothModifier::get_output()
extern "C"  int32_t SimpleSmoothModifier_get_output_m1605675878 (SimpleSmoothModifier_t1067843181 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.SimpleSmoothModifier::Apply(Pathfinding.Path,Pathfinding.ModifierData)
extern "C"  void SimpleSmoothModifier_Apply_m3387251268 (SimpleSmoothModifier_t1067843181 * __this, Path_t1974241691 * ___p0, int32_t ___source1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<UnityEngine.Vector3> Pathfinding.SimpleSmoothModifier::CurvedNonuniform(System.Collections.Generic.List`1<UnityEngine.Vector3>)
extern "C"  List_1_t1355284822 * SimpleSmoothModifier_CurvedNonuniform_m2902919197 (SimpleSmoothModifier_t1067843181 * __this, List_1_t1355284822 * ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Pathfinding.SimpleSmoothModifier::GetPointOnCubic(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t4282066566  SimpleSmoothModifier_GetPointOnCubic_m353527272 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___a0, Vector3_t4282066566  ___b1, Vector3_t4282066566  ___tan12, Vector3_t4282066566  ___tan23, float ___t4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<UnityEngine.Vector3> Pathfinding.SimpleSmoothModifier::SmoothOffsetSimple(System.Collections.Generic.List`1<UnityEngine.Vector3>)
extern "C"  List_1_t1355284822 * SimpleSmoothModifier_SmoothOffsetSimple_m2927699412 (SimpleSmoothModifier_t1067843181 * __this, List_1_t1355284822 * ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<UnityEngine.Vector3> Pathfinding.SimpleSmoothModifier::SmoothSimple(System.Collections.Generic.List`1<UnityEngine.Vector3>)
extern "C"  List_1_t1355284822 * SimpleSmoothModifier_SmoothSimple_m3185581345 (SimpleSmoothModifier_t1067843181 * __this, List_1_t1355284822 * ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<UnityEngine.Vector3> Pathfinding.SimpleSmoothModifier::SmoothBezier(System.Collections.Generic.List`1<UnityEngine.Vector3>)
extern "C"  List_1_t1355284822 * SimpleSmoothModifier_SmoothBezier_m3810901838 (SimpleSmoothModifier_t1067843181 * __this, List_1_t1355284822 * ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<UnityEngine.Vector3> Pathfinding.SimpleSmoothModifier::ilo_SmoothSimple1(Pathfinding.SimpleSmoothModifier,System.Collections.Generic.List`1<UnityEngine.Vector3>)
extern "C"  List_1_t1355284822 * SimpleSmoothModifier_ilo_SmoothSimple1_m3163586952 (Il2CppObject * __this /* static, unused */, SimpleSmoothModifier_t1067843181 * ____this0, List_1_t1355284822 * ___path1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.SimpleSmoothModifier::ilo_Left2(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  bool SimpleSmoothModifier_ilo_Left2_m3747119683 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___a0, Vector3_t4282066566  ___b1, Vector3_t4282066566  ___p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
