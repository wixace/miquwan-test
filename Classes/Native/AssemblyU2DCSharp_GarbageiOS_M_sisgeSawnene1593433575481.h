﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_sisgeSawnene159
struct  M_sisgeSawnene159_t3433575481  : public Il2CppObject
{
public:
	// System.Int32 GarbageiOS.M_sisgeSawnene159::_jeze
	int32_t ____jeze_0;
	// System.Int32 GarbageiOS.M_sisgeSawnene159::_xege
	int32_t ____xege_1;
	// System.Boolean GarbageiOS.M_sisgeSawnene159::_vistrekeQallvis
	bool ____vistrekeQallvis_2;
	// System.Single GarbageiOS.M_sisgeSawnene159::_serebemWearyou
	float ____serebemWearyou_3;
	// System.Boolean GarbageiOS.M_sisgeSawnene159::_seeje
	bool ____seeje_4;
	// System.Int32 GarbageiOS.M_sisgeSawnene159::_lairere
	int32_t ____lairere_5;
	// System.Boolean GarbageiOS.M_sisgeSawnene159::_muji
	bool ____muji_6;
	// System.String GarbageiOS.M_sisgeSawnene159::_dipir
	String_t* ____dipir_7;
	// System.String GarbageiOS.M_sisgeSawnene159::_ramear
	String_t* ____ramear_8;
	// System.Single GarbageiOS.M_sisgeSawnene159::_towsemjis
	float ____towsemjis_9;
	// System.UInt32 GarbageiOS.M_sisgeSawnene159::_boluFawhur
	uint32_t ____boluFawhur_10;
	// System.Single GarbageiOS.M_sisgeSawnene159::_bamayjaRodursti
	float ____bamayjaRodursti_11;
	// System.String GarbageiOS.M_sisgeSawnene159::_qesirNanisgair
	String_t* ____qesirNanisgair_12;
	// System.UInt32 GarbageiOS.M_sisgeSawnene159::_huchiSooline
	uint32_t ____huchiSooline_13;
	// System.UInt32 GarbageiOS.M_sisgeSawnene159::_gimiHelou
	uint32_t ____gimiHelou_14;
	// System.UInt32 GarbageiOS.M_sisgeSawnene159::_yallmezeJisube
	uint32_t ____yallmezeJisube_15;
	// System.String GarbageiOS.M_sisgeSawnene159::_tujoupi
	String_t* ____tujoupi_16;
	// System.Int32 GarbageiOS.M_sisgeSawnene159::_drero
	int32_t ____drero_17;
	// System.Single GarbageiOS.M_sisgeSawnene159::_fabe
	float ____fabe_18;
	// System.Int32 GarbageiOS.M_sisgeSawnene159::_jejereSemxatu
	int32_t ____jejereSemxatu_19;
	// System.UInt32 GarbageiOS.M_sisgeSawnene159::_chupas
	uint32_t ____chupas_20;
	// System.Single GarbageiOS.M_sisgeSawnene159::_dise
	float ____dise_21;
	// System.UInt32 GarbageiOS.M_sisgeSawnene159::_cawnartrarPusta
	uint32_t ____cawnartrarPusta_22;
	// System.Single GarbageiOS.M_sisgeSawnene159::_lagerJeljow
	float ____lagerJeljow_23;

public:
	inline static int32_t get_offset_of__jeze_0() { return static_cast<int32_t>(offsetof(M_sisgeSawnene159_t3433575481, ____jeze_0)); }
	inline int32_t get__jeze_0() const { return ____jeze_0; }
	inline int32_t* get_address_of__jeze_0() { return &____jeze_0; }
	inline void set__jeze_0(int32_t value)
	{
		____jeze_0 = value;
	}

	inline static int32_t get_offset_of__xege_1() { return static_cast<int32_t>(offsetof(M_sisgeSawnene159_t3433575481, ____xege_1)); }
	inline int32_t get__xege_1() const { return ____xege_1; }
	inline int32_t* get_address_of__xege_1() { return &____xege_1; }
	inline void set__xege_1(int32_t value)
	{
		____xege_1 = value;
	}

	inline static int32_t get_offset_of__vistrekeQallvis_2() { return static_cast<int32_t>(offsetof(M_sisgeSawnene159_t3433575481, ____vistrekeQallvis_2)); }
	inline bool get__vistrekeQallvis_2() const { return ____vistrekeQallvis_2; }
	inline bool* get_address_of__vistrekeQallvis_2() { return &____vistrekeQallvis_2; }
	inline void set__vistrekeQallvis_2(bool value)
	{
		____vistrekeQallvis_2 = value;
	}

	inline static int32_t get_offset_of__serebemWearyou_3() { return static_cast<int32_t>(offsetof(M_sisgeSawnene159_t3433575481, ____serebemWearyou_3)); }
	inline float get__serebemWearyou_3() const { return ____serebemWearyou_3; }
	inline float* get_address_of__serebemWearyou_3() { return &____serebemWearyou_3; }
	inline void set__serebemWearyou_3(float value)
	{
		____serebemWearyou_3 = value;
	}

	inline static int32_t get_offset_of__seeje_4() { return static_cast<int32_t>(offsetof(M_sisgeSawnene159_t3433575481, ____seeje_4)); }
	inline bool get__seeje_4() const { return ____seeje_4; }
	inline bool* get_address_of__seeje_4() { return &____seeje_4; }
	inline void set__seeje_4(bool value)
	{
		____seeje_4 = value;
	}

	inline static int32_t get_offset_of__lairere_5() { return static_cast<int32_t>(offsetof(M_sisgeSawnene159_t3433575481, ____lairere_5)); }
	inline int32_t get__lairere_5() const { return ____lairere_5; }
	inline int32_t* get_address_of__lairere_5() { return &____lairere_5; }
	inline void set__lairere_5(int32_t value)
	{
		____lairere_5 = value;
	}

	inline static int32_t get_offset_of__muji_6() { return static_cast<int32_t>(offsetof(M_sisgeSawnene159_t3433575481, ____muji_6)); }
	inline bool get__muji_6() const { return ____muji_6; }
	inline bool* get_address_of__muji_6() { return &____muji_6; }
	inline void set__muji_6(bool value)
	{
		____muji_6 = value;
	}

	inline static int32_t get_offset_of__dipir_7() { return static_cast<int32_t>(offsetof(M_sisgeSawnene159_t3433575481, ____dipir_7)); }
	inline String_t* get__dipir_7() const { return ____dipir_7; }
	inline String_t** get_address_of__dipir_7() { return &____dipir_7; }
	inline void set__dipir_7(String_t* value)
	{
		____dipir_7 = value;
		Il2CppCodeGenWriteBarrier(&____dipir_7, value);
	}

	inline static int32_t get_offset_of__ramear_8() { return static_cast<int32_t>(offsetof(M_sisgeSawnene159_t3433575481, ____ramear_8)); }
	inline String_t* get__ramear_8() const { return ____ramear_8; }
	inline String_t** get_address_of__ramear_8() { return &____ramear_8; }
	inline void set__ramear_8(String_t* value)
	{
		____ramear_8 = value;
		Il2CppCodeGenWriteBarrier(&____ramear_8, value);
	}

	inline static int32_t get_offset_of__towsemjis_9() { return static_cast<int32_t>(offsetof(M_sisgeSawnene159_t3433575481, ____towsemjis_9)); }
	inline float get__towsemjis_9() const { return ____towsemjis_9; }
	inline float* get_address_of__towsemjis_9() { return &____towsemjis_9; }
	inline void set__towsemjis_9(float value)
	{
		____towsemjis_9 = value;
	}

	inline static int32_t get_offset_of__boluFawhur_10() { return static_cast<int32_t>(offsetof(M_sisgeSawnene159_t3433575481, ____boluFawhur_10)); }
	inline uint32_t get__boluFawhur_10() const { return ____boluFawhur_10; }
	inline uint32_t* get_address_of__boluFawhur_10() { return &____boluFawhur_10; }
	inline void set__boluFawhur_10(uint32_t value)
	{
		____boluFawhur_10 = value;
	}

	inline static int32_t get_offset_of__bamayjaRodursti_11() { return static_cast<int32_t>(offsetof(M_sisgeSawnene159_t3433575481, ____bamayjaRodursti_11)); }
	inline float get__bamayjaRodursti_11() const { return ____bamayjaRodursti_11; }
	inline float* get_address_of__bamayjaRodursti_11() { return &____bamayjaRodursti_11; }
	inline void set__bamayjaRodursti_11(float value)
	{
		____bamayjaRodursti_11 = value;
	}

	inline static int32_t get_offset_of__qesirNanisgair_12() { return static_cast<int32_t>(offsetof(M_sisgeSawnene159_t3433575481, ____qesirNanisgair_12)); }
	inline String_t* get__qesirNanisgair_12() const { return ____qesirNanisgair_12; }
	inline String_t** get_address_of__qesirNanisgair_12() { return &____qesirNanisgair_12; }
	inline void set__qesirNanisgair_12(String_t* value)
	{
		____qesirNanisgair_12 = value;
		Il2CppCodeGenWriteBarrier(&____qesirNanisgair_12, value);
	}

	inline static int32_t get_offset_of__huchiSooline_13() { return static_cast<int32_t>(offsetof(M_sisgeSawnene159_t3433575481, ____huchiSooline_13)); }
	inline uint32_t get__huchiSooline_13() const { return ____huchiSooline_13; }
	inline uint32_t* get_address_of__huchiSooline_13() { return &____huchiSooline_13; }
	inline void set__huchiSooline_13(uint32_t value)
	{
		____huchiSooline_13 = value;
	}

	inline static int32_t get_offset_of__gimiHelou_14() { return static_cast<int32_t>(offsetof(M_sisgeSawnene159_t3433575481, ____gimiHelou_14)); }
	inline uint32_t get__gimiHelou_14() const { return ____gimiHelou_14; }
	inline uint32_t* get_address_of__gimiHelou_14() { return &____gimiHelou_14; }
	inline void set__gimiHelou_14(uint32_t value)
	{
		____gimiHelou_14 = value;
	}

	inline static int32_t get_offset_of__yallmezeJisube_15() { return static_cast<int32_t>(offsetof(M_sisgeSawnene159_t3433575481, ____yallmezeJisube_15)); }
	inline uint32_t get__yallmezeJisube_15() const { return ____yallmezeJisube_15; }
	inline uint32_t* get_address_of__yallmezeJisube_15() { return &____yallmezeJisube_15; }
	inline void set__yallmezeJisube_15(uint32_t value)
	{
		____yallmezeJisube_15 = value;
	}

	inline static int32_t get_offset_of__tujoupi_16() { return static_cast<int32_t>(offsetof(M_sisgeSawnene159_t3433575481, ____tujoupi_16)); }
	inline String_t* get__tujoupi_16() const { return ____tujoupi_16; }
	inline String_t** get_address_of__tujoupi_16() { return &____tujoupi_16; }
	inline void set__tujoupi_16(String_t* value)
	{
		____tujoupi_16 = value;
		Il2CppCodeGenWriteBarrier(&____tujoupi_16, value);
	}

	inline static int32_t get_offset_of__drero_17() { return static_cast<int32_t>(offsetof(M_sisgeSawnene159_t3433575481, ____drero_17)); }
	inline int32_t get__drero_17() const { return ____drero_17; }
	inline int32_t* get_address_of__drero_17() { return &____drero_17; }
	inline void set__drero_17(int32_t value)
	{
		____drero_17 = value;
	}

	inline static int32_t get_offset_of__fabe_18() { return static_cast<int32_t>(offsetof(M_sisgeSawnene159_t3433575481, ____fabe_18)); }
	inline float get__fabe_18() const { return ____fabe_18; }
	inline float* get_address_of__fabe_18() { return &____fabe_18; }
	inline void set__fabe_18(float value)
	{
		____fabe_18 = value;
	}

	inline static int32_t get_offset_of__jejereSemxatu_19() { return static_cast<int32_t>(offsetof(M_sisgeSawnene159_t3433575481, ____jejereSemxatu_19)); }
	inline int32_t get__jejereSemxatu_19() const { return ____jejereSemxatu_19; }
	inline int32_t* get_address_of__jejereSemxatu_19() { return &____jejereSemxatu_19; }
	inline void set__jejereSemxatu_19(int32_t value)
	{
		____jejereSemxatu_19 = value;
	}

	inline static int32_t get_offset_of__chupas_20() { return static_cast<int32_t>(offsetof(M_sisgeSawnene159_t3433575481, ____chupas_20)); }
	inline uint32_t get__chupas_20() const { return ____chupas_20; }
	inline uint32_t* get_address_of__chupas_20() { return &____chupas_20; }
	inline void set__chupas_20(uint32_t value)
	{
		____chupas_20 = value;
	}

	inline static int32_t get_offset_of__dise_21() { return static_cast<int32_t>(offsetof(M_sisgeSawnene159_t3433575481, ____dise_21)); }
	inline float get__dise_21() const { return ____dise_21; }
	inline float* get_address_of__dise_21() { return &____dise_21; }
	inline void set__dise_21(float value)
	{
		____dise_21 = value;
	}

	inline static int32_t get_offset_of__cawnartrarPusta_22() { return static_cast<int32_t>(offsetof(M_sisgeSawnene159_t3433575481, ____cawnartrarPusta_22)); }
	inline uint32_t get__cawnartrarPusta_22() const { return ____cawnartrarPusta_22; }
	inline uint32_t* get_address_of__cawnartrarPusta_22() { return &____cawnartrarPusta_22; }
	inline void set__cawnartrarPusta_22(uint32_t value)
	{
		____cawnartrarPusta_22 = value;
	}

	inline static int32_t get_offset_of__lagerJeljow_23() { return static_cast<int32_t>(offsetof(M_sisgeSawnene159_t3433575481, ____lagerJeljow_23)); }
	inline float get__lagerJeljow_23() const { return ____lagerJeljow_23; }
	inline float* get_address_of__lagerJeljow_23() { return &____lagerJeljow_23; }
	inline void set__lagerJeljow_23(float value)
	{
		____lagerJeljow_23 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
