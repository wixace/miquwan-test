﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TimeUpdateMgr
struct TimeUpdateMgr_t880289826;
// TimeUpdateVo
struct TimeUpdateVo_t1829512047;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_TimeUpdateVo1829512047.h"

// System.Void TimeUpdateMgr::.ctor()
extern "C"  void TimeUpdateMgr__ctor_m615530377 (TimeUpdateMgr_t880289826 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TimeUpdateMgr::Init()
extern "C"  void TimeUpdateMgr_Init_m3712170411 (TimeUpdateMgr_t880289826 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TimeUpdateMgr::AddTimeVo(TimeUpdateVo)
extern "C"  void TimeUpdateMgr_AddTimeVo_m1916085567 (TimeUpdateMgr_t880289826 * __this, TimeUpdateVo_t1829512047 * ___vo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TimeUpdateMgr::RemoveVo(TimeUpdateVo)
extern "C"  void TimeUpdateMgr_RemoveVo_m2536847017 (TimeUpdateMgr_t880289826 * __this, TimeUpdateVo_t1829512047 * ___vo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TimeUpdateMgr::ZUpdate()
extern "C"  void TimeUpdateMgr_ZUpdate_m2682701898 (TimeUpdateMgr_t880289826 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TimeUpdateMgr::Clear()
extern "C"  void TimeUpdateMgr_Clear_m2316630964 (TimeUpdateMgr_t880289826 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TimeUpdateMgr::ilo_Update1(TimeUpdateVo)
extern "C"  void TimeUpdateMgr_ilo_Update1_m2787696589 (Il2CppObject * __this /* static, unused */, TimeUpdateVo_t1829512047 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
