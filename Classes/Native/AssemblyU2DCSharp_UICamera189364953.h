﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// BetterList`1<UICamera>
struct BetterList_1_t1686332965;
// UICamera/GetKeyStateFunc
struct GetKeyStateFunc_t2745734774;
// UICamera/GetAxisFunc
struct GetAxisFunc_t783615269;
// UICamera/OnScreenResize
struct OnScreenResize_t3828578773;
// System.String
struct String_t;
// UICamera/OnCustomInput
struct OnCustomInput_t736305828;
// UICamera
struct UICamera_t189364953;
// UnityEngine.Camera
struct Camera_t2727095145;
// UICamera/MouseOrTouch
struct MouseOrTouch_t2057376333;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// UICamera/VoidDelegate
struct VoidDelegate_t3135042255;
// UICamera/BoolDelegate
struct BoolDelegate_t2094540325;
// UICamera/FloatDelegate
struct FloatDelegate_t1146521387;
// UICamera/VectorDelegate
struct VectorDelegate_t2747913726;
// UICamera/ObjectDelegate
struct ObjectDelegate_t3703364602;
// UICamera/KeyCodeDelegate
struct KeyCodeDelegate_t4120455995;
// UICamera/MoveDelegate
struct MoveDelegate_t1906135052;
// UICamera/MouseOrTouch[]
struct MouseOrTouchU5BU5D_t546398688;
// System.Collections.Generic.Dictionary`2<System.Int32,UICamera/MouseOrTouch>
struct Dictionary_2_t2054639572;
// BetterList`1<UICamera/DepthEntry>
struct BetterList_1_t2642582481;
// BetterList`1/CompareFunc<UICamera/DepthEntry>
struct CompareFunc_t1687333339;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "AssemblyU2DCSharp_UICamera_EventType3387030814.h"
#include "UnityEngine_UnityEngine_LayerMask3236759763.h"
#include "UnityEngine_UnityEngine_KeyCode3128317986.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_RaycastHit4003175726.h"
#include "AssemblyU2DCSharp_UICamera_ControlScheme2923531404.h"
#include "AssemblyU2DCSharp_UICamera_DepthEntry1145614469.h"
#include "UnityEngine_UnityEngine_Plane4206452690.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UICamera
struct  UICamera_t189364953  : public MonoBehaviour_t667441552
{
public:
	// UICamera/EventType UICamera::eventType
	int32_t ___eventType_8;
	// System.Boolean UICamera::eventsGoToColliders
	bool ___eventsGoToColliders_9;
	// UnityEngine.LayerMask UICamera::eventReceiverMask
	LayerMask_t3236759763  ___eventReceiverMask_10;
	// System.Boolean UICamera::debug
	bool ___debug_11;
	// System.Boolean UICamera::useMouse
	bool ___useMouse_12;
	// System.Boolean UICamera::useTouch
	bool ___useTouch_13;
	// System.Boolean UICamera::allowMultiTouch
	bool ___allowMultiTouch_14;
	// System.Boolean UICamera::useKeyboard
	bool ___useKeyboard_15;
	// System.Boolean UICamera::useController
	bool ___useController_16;
	// System.Boolean UICamera::stickyTooltip
	bool ___stickyTooltip_17;
	// System.Single UICamera::tooltipDelay
	float ___tooltipDelay_18;
	// System.Single UICamera::mouseDragThreshold
	float ___mouseDragThreshold_19;
	// System.Single UICamera::mouseClickThreshold
	float ___mouseClickThreshold_20;
	// System.Single UICamera::touchDragThreshold
	float ___touchDragThreshold_21;
	// System.Single UICamera::touchClickThreshold
	float ___touchClickThreshold_22;
	// System.Single UICamera::rangeDistance
	float ___rangeDistance_23;
	// System.String UICamera::scrollAxisName
	String_t* ___scrollAxisName_24;
	// System.String UICamera::verticalAxisName
	String_t* ___verticalAxisName_25;
	// System.String UICamera::horizontalAxisName
	String_t* ___horizontalAxisName_26;
	// UnityEngine.KeyCode UICamera::submitKey0
	int32_t ___submitKey0_27;
	// UnityEngine.KeyCode UICamera::submitKey1
	int32_t ___submitKey1_28;
	// UnityEngine.KeyCode UICamera::cancelKey0
	int32_t ___cancelKey0_29;
	// UnityEngine.KeyCode UICamera::cancelKey1
	int32_t ___cancelKey1_30;
	// UnityEngine.GameObject UICamera::mTooltip
	GameObject_t3674682005 * ___mTooltip_72;
	// UnityEngine.Camera UICamera::mCam
	Camera_t2727095145 * ___mCam_73;
	// System.Single UICamera::mTooltipTime
	float ___mTooltipTime_74;
	// System.Single UICamera::mNextRaycast
	float ___mNextRaycast_75;

public:
	inline static int32_t get_offset_of_eventType_8() { return static_cast<int32_t>(offsetof(UICamera_t189364953, ___eventType_8)); }
	inline int32_t get_eventType_8() const { return ___eventType_8; }
	inline int32_t* get_address_of_eventType_8() { return &___eventType_8; }
	inline void set_eventType_8(int32_t value)
	{
		___eventType_8 = value;
	}

	inline static int32_t get_offset_of_eventsGoToColliders_9() { return static_cast<int32_t>(offsetof(UICamera_t189364953, ___eventsGoToColliders_9)); }
	inline bool get_eventsGoToColliders_9() const { return ___eventsGoToColliders_9; }
	inline bool* get_address_of_eventsGoToColliders_9() { return &___eventsGoToColliders_9; }
	inline void set_eventsGoToColliders_9(bool value)
	{
		___eventsGoToColliders_9 = value;
	}

	inline static int32_t get_offset_of_eventReceiverMask_10() { return static_cast<int32_t>(offsetof(UICamera_t189364953, ___eventReceiverMask_10)); }
	inline LayerMask_t3236759763  get_eventReceiverMask_10() const { return ___eventReceiverMask_10; }
	inline LayerMask_t3236759763 * get_address_of_eventReceiverMask_10() { return &___eventReceiverMask_10; }
	inline void set_eventReceiverMask_10(LayerMask_t3236759763  value)
	{
		___eventReceiverMask_10 = value;
	}

	inline static int32_t get_offset_of_debug_11() { return static_cast<int32_t>(offsetof(UICamera_t189364953, ___debug_11)); }
	inline bool get_debug_11() const { return ___debug_11; }
	inline bool* get_address_of_debug_11() { return &___debug_11; }
	inline void set_debug_11(bool value)
	{
		___debug_11 = value;
	}

	inline static int32_t get_offset_of_useMouse_12() { return static_cast<int32_t>(offsetof(UICamera_t189364953, ___useMouse_12)); }
	inline bool get_useMouse_12() const { return ___useMouse_12; }
	inline bool* get_address_of_useMouse_12() { return &___useMouse_12; }
	inline void set_useMouse_12(bool value)
	{
		___useMouse_12 = value;
	}

	inline static int32_t get_offset_of_useTouch_13() { return static_cast<int32_t>(offsetof(UICamera_t189364953, ___useTouch_13)); }
	inline bool get_useTouch_13() const { return ___useTouch_13; }
	inline bool* get_address_of_useTouch_13() { return &___useTouch_13; }
	inline void set_useTouch_13(bool value)
	{
		___useTouch_13 = value;
	}

	inline static int32_t get_offset_of_allowMultiTouch_14() { return static_cast<int32_t>(offsetof(UICamera_t189364953, ___allowMultiTouch_14)); }
	inline bool get_allowMultiTouch_14() const { return ___allowMultiTouch_14; }
	inline bool* get_address_of_allowMultiTouch_14() { return &___allowMultiTouch_14; }
	inline void set_allowMultiTouch_14(bool value)
	{
		___allowMultiTouch_14 = value;
	}

	inline static int32_t get_offset_of_useKeyboard_15() { return static_cast<int32_t>(offsetof(UICamera_t189364953, ___useKeyboard_15)); }
	inline bool get_useKeyboard_15() const { return ___useKeyboard_15; }
	inline bool* get_address_of_useKeyboard_15() { return &___useKeyboard_15; }
	inline void set_useKeyboard_15(bool value)
	{
		___useKeyboard_15 = value;
	}

	inline static int32_t get_offset_of_useController_16() { return static_cast<int32_t>(offsetof(UICamera_t189364953, ___useController_16)); }
	inline bool get_useController_16() const { return ___useController_16; }
	inline bool* get_address_of_useController_16() { return &___useController_16; }
	inline void set_useController_16(bool value)
	{
		___useController_16 = value;
	}

	inline static int32_t get_offset_of_stickyTooltip_17() { return static_cast<int32_t>(offsetof(UICamera_t189364953, ___stickyTooltip_17)); }
	inline bool get_stickyTooltip_17() const { return ___stickyTooltip_17; }
	inline bool* get_address_of_stickyTooltip_17() { return &___stickyTooltip_17; }
	inline void set_stickyTooltip_17(bool value)
	{
		___stickyTooltip_17 = value;
	}

	inline static int32_t get_offset_of_tooltipDelay_18() { return static_cast<int32_t>(offsetof(UICamera_t189364953, ___tooltipDelay_18)); }
	inline float get_tooltipDelay_18() const { return ___tooltipDelay_18; }
	inline float* get_address_of_tooltipDelay_18() { return &___tooltipDelay_18; }
	inline void set_tooltipDelay_18(float value)
	{
		___tooltipDelay_18 = value;
	}

	inline static int32_t get_offset_of_mouseDragThreshold_19() { return static_cast<int32_t>(offsetof(UICamera_t189364953, ___mouseDragThreshold_19)); }
	inline float get_mouseDragThreshold_19() const { return ___mouseDragThreshold_19; }
	inline float* get_address_of_mouseDragThreshold_19() { return &___mouseDragThreshold_19; }
	inline void set_mouseDragThreshold_19(float value)
	{
		___mouseDragThreshold_19 = value;
	}

	inline static int32_t get_offset_of_mouseClickThreshold_20() { return static_cast<int32_t>(offsetof(UICamera_t189364953, ___mouseClickThreshold_20)); }
	inline float get_mouseClickThreshold_20() const { return ___mouseClickThreshold_20; }
	inline float* get_address_of_mouseClickThreshold_20() { return &___mouseClickThreshold_20; }
	inline void set_mouseClickThreshold_20(float value)
	{
		___mouseClickThreshold_20 = value;
	}

	inline static int32_t get_offset_of_touchDragThreshold_21() { return static_cast<int32_t>(offsetof(UICamera_t189364953, ___touchDragThreshold_21)); }
	inline float get_touchDragThreshold_21() const { return ___touchDragThreshold_21; }
	inline float* get_address_of_touchDragThreshold_21() { return &___touchDragThreshold_21; }
	inline void set_touchDragThreshold_21(float value)
	{
		___touchDragThreshold_21 = value;
	}

	inline static int32_t get_offset_of_touchClickThreshold_22() { return static_cast<int32_t>(offsetof(UICamera_t189364953, ___touchClickThreshold_22)); }
	inline float get_touchClickThreshold_22() const { return ___touchClickThreshold_22; }
	inline float* get_address_of_touchClickThreshold_22() { return &___touchClickThreshold_22; }
	inline void set_touchClickThreshold_22(float value)
	{
		___touchClickThreshold_22 = value;
	}

	inline static int32_t get_offset_of_rangeDistance_23() { return static_cast<int32_t>(offsetof(UICamera_t189364953, ___rangeDistance_23)); }
	inline float get_rangeDistance_23() const { return ___rangeDistance_23; }
	inline float* get_address_of_rangeDistance_23() { return &___rangeDistance_23; }
	inline void set_rangeDistance_23(float value)
	{
		___rangeDistance_23 = value;
	}

	inline static int32_t get_offset_of_scrollAxisName_24() { return static_cast<int32_t>(offsetof(UICamera_t189364953, ___scrollAxisName_24)); }
	inline String_t* get_scrollAxisName_24() const { return ___scrollAxisName_24; }
	inline String_t** get_address_of_scrollAxisName_24() { return &___scrollAxisName_24; }
	inline void set_scrollAxisName_24(String_t* value)
	{
		___scrollAxisName_24 = value;
		Il2CppCodeGenWriteBarrier(&___scrollAxisName_24, value);
	}

	inline static int32_t get_offset_of_verticalAxisName_25() { return static_cast<int32_t>(offsetof(UICamera_t189364953, ___verticalAxisName_25)); }
	inline String_t* get_verticalAxisName_25() const { return ___verticalAxisName_25; }
	inline String_t** get_address_of_verticalAxisName_25() { return &___verticalAxisName_25; }
	inline void set_verticalAxisName_25(String_t* value)
	{
		___verticalAxisName_25 = value;
		Il2CppCodeGenWriteBarrier(&___verticalAxisName_25, value);
	}

	inline static int32_t get_offset_of_horizontalAxisName_26() { return static_cast<int32_t>(offsetof(UICamera_t189364953, ___horizontalAxisName_26)); }
	inline String_t* get_horizontalAxisName_26() const { return ___horizontalAxisName_26; }
	inline String_t** get_address_of_horizontalAxisName_26() { return &___horizontalAxisName_26; }
	inline void set_horizontalAxisName_26(String_t* value)
	{
		___horizontalAxisName_26 = value;
		Il2CppCodeGenWriteBarrier(&___horizontalAxisName_26, value);
	}

	inline static int32_t get_offset_of_submitKey0_27() { return static_cast<int32_t>(offsetof(UICamera_t189364953, ___submitKey0_27)); }
	inline int32_t get_submitKey0_27() const { return ___submitKey0_27; }
	inline int32_t* get_address_of_submitKey0_27() { return &___submitKey0_27; }
	inline void set_submitKey0_27(int32_t value)
	{
		___submitKey0_27 = value;
	}

	inline static int32_t get_offset_of_submitKey1_28() { return static_cast<int32_t>(offsetof(UICamera_t189364953, ___submitKey1_28)); }
	inline int32_t get_submitKey1_28() const { return ___submitKey1_28; }
	inline int32_t* get_address_of_submitKey1_28() { return &___submitKey1_28; }
	inline void set_submitKey1_28(int32_t value)
	{
		___submitKey1_28 = value;
	}

	inline static int32_t get_offset_of_cancelKey0_29() { return static_cast<int32_t>(offsetof(UICamera_t189364953, ___cancelKey0_29)); }
	inline int32_t get_cancelKey0_29() const { return ___cancelKey0_29; }
	inline int32_t* get_address_of_cancelKey0_29() { return &___cancelKey0_29; }
	inline void set_cancelKey0_29(int32_t value)
	{
		___cancelKey0_29 = value;
	}

	inline static int32_t get_offset_of_cancelKey1_30() { return static_cast<int32_t>(offsetof(UICamera_t189364953, ___cancelKey1_30)); }
	inline int32_t get_cancelKey1_30() const { return ___cancelKey1_30; }
	inline int32_t* get_address_of_cancelKey1_30() { return &___cancelKey1_30; }
	inline void set_cancelKey1_30(int32_t value)
	{
		___cancelKey1_30 = value;
	}

	inline static int32_t get_offset_of_mTooltip_72() { return static_cast<int32_t>(offsetof(UICamera_t189364953, ___mTooltip_72)); }
	inline GameObject_t3674682005 * get_mTooltip_72() const { return ___mTooltip_72; }
	inline GameObject_t3674682005 ** get_address_of_mTooltip_72() { return &___mTooltip_72; }
	inline void set_mTooltip_72(GameObject_t3674682005 * value)
	{
		___mTooltip_72 = value;
		Il2CppCodeGenWriteBarrier(&___mTooltip_72, value);
	}

	inline static int32_t get_offset_of_mCam_73() { return static_cast<int32_t>(offsetof(UICamera_t189364953, ___mCam_73)); }
	inline Camera_t2727095145 * get_mCam_73() const { return ___mCam_73; }
	inline Camera_t2727095145 ** get_address_of_mCam_73() { return &___mCam_73; }
	inline void set_mCam_73(Camera_t2727095145 * value)
	{
		___mCam_73 = value;
		Il2CppCodeGenWriteBarrier(&___mCam_73, value);
	}

	inline static int32_t get_offset_of_mTooltipTime_74() { return static_cast<int32_t>(offsetof(UICamera_t189364953, ___mTooltipTime_74)); }
	inline float get_mTooltipTime_74() const { return ___mTooltipTime_74; }
	inline float* get_address_of_mTooltipTime_74() { return &___mTooltipTime_74; }
	inline void set_mTooltipTime_74(float value)
	{
		___mTooltipTime_74 = value;
	}

	inline static int32_t get_offset_of_mNextRaycast_75() { return static_cast<int32_t>(offsetof(UICamera_t189364953, ___mNextRaycast_75)); }
	inline float get_mNextRaycast_75() const { return ___mNextRaycast_75; }
	inline float* get_address_of_mNextRaycast_75() { return &___mNextRaycast_75; }
	inline void set_mNextRaycast_75(float value)
	{
		___mNextRaycast_75 = value;
	}
};

struct UICamera_t189364953_StaticFields
{
public:
	// BetterList`1<UICamera> UICamera::list
	BetterList_1_t1686332965 * ___list_2;
	// UICamera/GetKeyStateFunc UICamera::GetKeyDown
	GetKeyStateFunc_t2745734774 * ___GetKeyDown_3;
	// UICamera/GetKeyStateFunc UICamera::GetKeyUp
	GetKeyStateFunc_t2745734774 * ___GetKeyUp_4;
	// UICamera/GetKeyStateFunc UICamera::GetKey
	GetKeyStateFunc_t2745734774 * ___GetKey_5;
	// UICamera/GetAxisFunc UICamera::GetAxis
	GetAxisFunc_t783615269 * ___GetAxis_6;
	// UICamera/OnScreenResize UICamera::onScreenResize
	OnScreenResize_t3828578773 * ___onScreenResize_7;
	// UICamera/OnCustomInput UICamera::onCustomInput
	OnCustomInput_t736305828 * ___onCustomInput_31;
	// System.Boolean UICamera::showTooltips
	bool ___showTooltips_32;
	// System.Boolean UICamera::mDisableController
	bool ___mDisableController_33;
	// UnityEngine.Vector2 UICamera::mLastPos
	Vector2_t4282066565  ___mLastPos_34;
	// UnityEngine.Vector2 UICamera::lastTouchPosition
	Vector2_t4282066565  ___lastTouchPosition_35;
	// UnityEngine.Vector3 UICamera::lastWorldPosition
	Vector3_t4282066566  ___lastWorldPosition_36;
	// UnityEngine.RaycastHit UICamera::lastHit
	RaycastHit_t4003175726  ___lastHit_37;
	// UICamera UICamera::current
	UICamera_t189364953 * ___current_38;
	// UnityEngine.Camera UICamera::currentCamera
	Camera_t2727095145 * ___currentCamera_39;
	// UICamera/ControlScheme UICamera::currentScheme
	int32_t ___currentScheme_40;
	// System.Int32 UICamera::currentTouchID
	int32_t ___currentTouchID_41;
	// UnityEngine.KeyCode UICamera::currentKey
	int32_t ___currentKey_42;
	// UICamera/MouseOrTouch UICamera::currentTouch
	MouseOrTouch_t2057376333 * ___currentTouch_43;
	// System.Boolean UICamera::inputHasFocus
	bool ___inputHasFocus_44;
	// UnityEngine.GameObject UICamera::mGenericHandler
	GameObject_t3674682005 * ___mGenericHandler_45;
	// UnityEngine.GameObject UICamera::fallThrough
	GameObject_t3674682005 * ___fallThrough_46;
	// UICamera/VoidDelegate UICamera::onClick
	VoidDelegate_t3135042255 * ___onClick_47;
	// UICamera/VoidDelegate UICamera::onDoubleClick
	VoidDelegate_t3135042255 * ___onDoubleClick_48;
	// UICamera/BoolDelegate UICamera::onHover
	BoolDelegate_t2094540325 * ___onHover_49;
	// UICamera/BoolDelegate UICamera::onPress
	BoolDelegate_t2094540325 * ___onPress_50;
	// UICamera/BoolDelegate UICamera::onSelect
	BoolDelegate_t2094540325 * ___onSelect_51;
	// UICamera/FloatDelegate UICamera::onScroll
	FloatDelegate_t1146521387 * ___onScroll_52;
	// UICamera/VectorDelegate UICamera::onDrag
	VectorDelegate_t2747913726 * ___onDrag_53;
	// UICamera/VoidDelegate UICamera::onDragStart
	VoidDelegate_t3135042255 * ___onDragStart_54;
	// UICamera/ObjectDelegate UICamera::onDragOver
	ObjectDelegate_t3703364602 * ___onDragOver_55;
	// UICamera/ObjectDelegate UICamera::onDragOut
	ObjectDelegate_t3703364602 * ___onDragOut_56;
	// UICamera/VoidDelegate UICamera::onDragEnd
	VoidDelegate_t3135042255 * ___onDragEnd_57;
	// UICamera/ObjectDelegate UICamera::onDrop
	ObjectDelegate_t3703364602 * ___onDrop_58;
	// UICamera/KeyCodeDelegate UICamera::onKey
	KeyCodeDelegate_t4120455995 * ___onKey_59;
	// UICamera/BoolDelegate UICamera::onTooltip
	BoolDelegate_t2094540325 * ___onTooltip_60;
	// UICamera/MoveDelegate UICamera::onMouseMove
	MoveDelegate_t1906135052 * ___onMouseMove_61;
	// UnityEngine.GameObject UICamera::mCurrentSelection
	GameObject_t3674682005 * ___mCurrentSelection_62;
	// UnityEngine.GameObject UICamera::mNextSelection
	GameObject_t3674682005 * ___mNextSelection_63;
	// UICamera/ControlScheme UICamera::mNextScheme
	int32_t ___mNextScheme_64;
	// UICamera/MouseOrTouch[] UICamera::mMouse
	MouseOrTouchU5BU5D_t546398688* ___mMouse_65;
	// UnityEngine.GameObject UICamera::mHover
	GameObject_t3674682005 * ___mHover_66;
	// UICamera/MouseOrTouch UICamera::controller
	MouseOrTouch_t2057376333 * ___controller_67;
	// System.Single UICamera::mNextEvent
	float ___mNextEvent_68;
	// System.Collections.Generic.Dictionary`2<System.Int32,UICamera/MouseOrTouch> UICamera::mTouches
	Dictionary_2_t2054639572 * ___mTouches_69;
	// System.Int32 UICamera::mWidth
	int32_t ___mWidth_70;
	// System.Int32 UICamera::mHeight
	int32_t ___mHeight_71;
	// System.Boolean UICamera::isDragging
	bool ___isDragging_76;
	// UnityEngine.GameObject UICamera::hoveredObject
	GameObject_t3674682005 * ___hoveredObject_77;
	// UICamera/DepthEntry UICamera::mHit
	DepthEntry_t1145614469  ___mHit_78;
	// BetterList`1<UICamera/DepthEntry> UICamera::mHits
	BetterList_1_t2642582481 * ___mHits_79;
	// UnityEngine.Plane UICamera::m2DPlane
	Plane_t4206452690  ___m2DPlane_80;
	// System.Boolean UICamera::mNotifying
	bool ___mNotifying_81;
	// System.Boolean UICamera::mUsingTouchEvents
	bool ___mUsingTouchEvents_82;
	// BetterList`1/CompareFunc<UICamera/DepthEntry> UICamera::<>f__am$cache51
	CompareFunc_t1687333339 * ___U3CU3Ef__amU24cache51_83;
	// BetterList`1/CompareFunc<UICamera/DepthEntry> UICamera::<>f__am$cache52
	CompareFunc_t1687333339 * ___U3CU3Ef__amU24cache52_84;

public:
	inline static int32_t get_offset_of_list_2() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___list_2)); }
	inline BetterList_1_t1686332965 * get_list_2() const { return ___list_2; }
	inline BetterList_1_t1686332965 ** get_address_of_list_2() { return &___list_2; }
	inline void set_list_2(BetterList_1_t1686332965 * value)
	{
		___list_2 = value;
		Il2CppCodeGenWriteBarrier(&___list_2, value);
	}

	inline static int32_t get_offset_of_GetKeyDown_3() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___GetKeyDown_3)); }
	inline GetKeyStateFunc_t2745734774 * get_GetKeyDown_3() const { return ___GetKeyDown_3; }
	inline GetKeyStateFunc_t2745734774 ** get_address_of_GetKeyDown_3() { return &___GetKeyDown_3; }
	inline void set_GetKeyDown_3(GetKeyStateFunc_t2745734774 * value)
	{
		___GetKeyDown_3 = value;
		Il2CppCodeGenWriteBarrier(&___GetKeyDown_3, value);
	}

	inline static int32_t get_offset_of_GetKeyUp_4() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___GetKeyUp_4)); }
	inline GetKeyStateFunc_t2745734774 * get_GetKeyUp_4() const { return ___GetKeyUp_4; }
	inline GetKeyStateFunc_t2745734774 ** get_address_of_GetKeyUp_4() { return &___GetKeyUp_4; }
	inline void set_GetKeyUp_4(GetKeyStateFunc_t2745734774 * value)
	{
		___GetKeyUp_4 = value;
		Il2CppCodeGenWriteBarrier(&___GetKeyUp_4, value);
	}

	inline static int32_t get_offset_of_GetKey_5() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___GetKey_5)); }
	inline GetKeyStateFunc_t2745734774 * get_GetKey_5() const { return ___GetKey_5; }
	inline GetKeyStateFunc_t2745734774 ** get_address_of_GetKey_5() { return &___GetKey_5; }
	inline void set_GetKey_5(GetKeyStateFunc_t2745734774 * value)
	{
		___GetKey_5 = value;
		Il2CppCodeGenWriteBarrier(&___GetKey_5, value);
	}

	inline static int32_t get_offset_of_GetAxis_6() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___GetAxis_6)); }
	inline GetAxisFunc_t783615269 * get_GetAxis_6() const { return ___GetAxis_6; }
	inline GetAxisFunc_t783615269 ** get_address_of_GetAxis_6() { return &___GetAxis_6; }
	inline void set_GetAxis_6(GetAxisFunc_t783615269 * value)
	{
		___GetAxis_6 = value;
		Il2CppCodeGenWriteBarrier(&___GetAxis_6, value);
	}

	inline static int32_t get_offset_of_onScreenResize_7() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___onScreenResize_7)); }
	inline OnScreenResize_t3828578773 * get_onScreenResize_7() const { return ___onScreenResize_7; }
	inline OnScreenResize_t3828578773 ** get_address_of_onScreenResize_7() { return &___onScreenResize_7; }
	inline void set_onScreenResize_7(OnScreenResize_t3828578773 * value)
	{
		___onScreenResize_7 = value;
		Il2CppCodeGenWriteBarrier(&___onScreenResize_7, value);
	}

	inline static int32_t get_offset_of_onCustomInput_31() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___onCustomInput_31)); }
	inline OnCustomInput_t736305828 * get_onCustomInput_31() const { return ___onCustomInput_31; }
	inline OnCustomInput_t736305828 ** get_address_of_onCustomInput_31() { return &___onCustomInput_31; }
	inline void set_onCustomInput_31(OnCustomInput_t736305828 * value)
	{
		___onCustomInput_31 = value;
		Il2CppCodeGenWriteBarrier(&___onCustomInput_31, value);
	}

	inline static int32_t get_offset_of_showTooltips_32() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___showTooltips_32)); }
	inline bool get_showTooltips_32() const { return ___showTooltips_32; }
	inline bool* get_address_of_showTooltips_32() { return &___showTooltips_32; }
	inline void set_showTooltips_32(bool value)
	{
		___showTooltips_32 = value;
	}

	inline static int32_t get_offset_of_mDisableController_33() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___mDisableController_33)); }
	inline bool get_mDisableController_33() const { return ___mDisableController_33; }
	inline bool* get_address_of_mDisableController_33() { return &___mDisableController_33; }
	inline void set_mDisableController_33(bool value)
	{
		___mDisableController_33 = value;
	}

	inline static int32_t get_offset_of_mLastPos_34() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___mLastPos_34)); }
	inline Vector2_t4282066565  get_mLastPos_34() const { return ___mLastPos_34; }
	inline Vector2_t4282066565 * get_address_of_mLastPos_34() { return &___mLastPos_34; }
	inline void set_mLastPos_34(Vector2_t4282066565  value)
	{
		___mLastPos_34 = value;
	}

	inline static int32_t get_offset_of_lastTouchPosition_35() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___lastTouchPosition_35)); }
	inline Vector2_t4282066565  get_lastTouchPosition_35() const { return ___lastTouchPosition_35; }
	inline Vector2_t4282066565 * get_address_of_lastTouchPosition_35() { return &___lastTouchPosition_35; }
	inline void set_lastTouchPosition_35(Vector2_t4282066565  value)
	{
		___lastTouchPosition_35 = value;
	}

	inline static int32_t get_offset_of_lastWorldPosition_36() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___lastWorldPosition_36)); }
	inline Vector3_t4282066566  get_lastWorldPosition_36() const { return ___lastWorldPosition_36; }
	inline Vector3_t4282066566 * get_address_of_lastWorldPosition_36() { return &___lastWorldPosition_36; }
	inline void set_lastWorldPosition_36(Vector3_t4282066566  value)
	{
		___lastWorldPosition_36 = value;
	}

	inline static int32_t get_offset_of_lastHit_37() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___lastHit_37)); }
	inline RaycastHit_t4003175726  get_lastHit_37() const { return ___lastHit_37; }
	inline RaycastHit_t4003175726 * get_address_of_lastHit_37() { return &___lastHit_37; }
	inline void set_lastHit_37(RaycastHit_t4003175726  value)
	{
		___lastHit_37 = value;
	}

	inline static int32_t get_offset_of_current_38() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___current_38)); }
	inline UICamera_t189364953 * get_current_38() const { return ___current_38; }
	inline UICamera_t189364953 ** get_address_of_current_38() { return &___current_38; }
	inline void set_current_38(UICamera_t189364953 * value)
	{
		___current_38 = value;
		Il2CppCodeGenWriteBarrier(&___current_38, value);
	}

	inline static int32_t get_offset_of_currentCamera_39() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___currentCamera_39)); }
	inline Camera_t2727095145 * get_currentCamera_39() const { return ___currentCamera_39; }
	inline Camera_t2727095145 ** get_address_of_currentCamera_39() { return &___currentCamera_39; }
	inline void set_currentCamera_39(Camera_t2727095145 * value)
	{
		___currentCamera_39 = value;
		Il2CppCodeGenWriteBarrier(&___currentCamera_39, value);
	}

	inline static int32_t get_offset_of_currentScheme_40() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___currentScheme_40)); }
	inline int32_t get_currentScheme_40() const { return ___currentScheme_40; }
	inline int32_t* get_address_of_currentScheme_40() { return &___currentScheme_40; }
	inline void set_currentScheme_40(int32_t value)
	{
		___currentScheme_40 = value;
	}

	inline static int32_t get_offset_of_currentTouchID_41() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___currentTouchID_41)); }
	inline int32_t get_currentTouchID_41() const { return ___currentTouchID_41; }
	inline int32_t* get_address_of_currentTouchID_41() { return &___currentTouchID_41; }
	inline void set_currentTouchID_41(int32_t value)
	{
		___currentTouchID_41 = value;
	}

	inline static int32_t get_offset_of_currentKey_42() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___currentKey_42)); }
	inline int32_t get_currentKey_42() const { return ___currentKey_42; }
	inline int32_t* get_address_of_currentKey_42() { return &___currentKey_42; }
	inline void set_currentKey_42(int32_t value)
	{
		___currentKey_42 = value;
	}

	inline static int32_t get_offset_of_currentTouch_43() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___currentTouch_43)); }
	inline MouseOrTouch_t2057376333 * get_currentTouch_43() const { return ___currentTouch_43; }
	inline MouseOrTouch_t2057376333 ** get_address_of_currentTouch_43() { return &___currentTouch_43; }
	inline void set_currentTouch_43(MouseOrTouch_t2057376333 * value)
	{
		___currentTouch_43 = value;
		Il2CppCodeGenWriteBarrier(&___currentTouch_43, value);
	}

	inline static int32_t get_offset_of_inputHasFocus_44() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___inputHasFocus_44)); }
	inline bool get_inputHasFocus_44() const { return ___inputHasFocus_44; }
	inline bool* get_address_of_inputHasFocus_44() { return &___inputHasFocus_44; }
	inline void set_inputHasFocus_44(bool value)
	{
		___inputHasFocus_44 = value;
	}

	inline static int32_t get_offset_of_mGenericHandler_45() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___mGenericHandler_45)); }
	inline GameObject_t3674682005 * get_mGenericHandler_45() const { return ___mGenericHandler_45; }
	inline GameObject_t3674682005 ** get_address_of_mGenericHandler_45() { return &___mGenericHandler_45; }
	inline void set_mGenericHandler_45(GameObject_t3674682005 * value)
	{
		___mGenericHandler_45 = value;
		Il2CppCodeGenWriteBarrier(&___mGenericHandler_45, value);
	}

	inline static int32_t get_offset_of_fallThrough_46() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___fallThrough_46)); }
	inline GameObject_t3674682005 * get_fallThrough_46() const { return ___fallThrough_46; }
	inline GameObject_t3674682005 ** get_address_of_fallThrough_46() { return &___fallThrough_46; }
	inline void set_fallThrough_46(GameObject_t3674682005 * value)
	{
		___fallThrough_46 = value;
		Il2CppCodeGenWriteBarrier(&___fallThrough_46, value);
	}

	inline static int32_t get_offset_of_onClick_47() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___onClick_47)); }
	inline VoidDelegate_t3135042255 * get_onClick_47() const { return ___onClick_47; }
	inline VoidDelegate_t3135042255 ** get_address_of_onClick_47() { return &___onClick_47; }
	inline void set_onClick_47(VoidDelegate_t3135042255 * value)
	{
		___onClick_47 = value;
		Il2CppCodeGenWriteBarrier(&___onClick_47, value);
	}

	inline static int32_t get_offset_of_onDoubleClick_48() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___onDoubleClick_48)); }
	inline VoidDelegate_t3135042255 * get_onDoubleClick_48() const { return ___onDoubleClick_48; }
	inline VoidDelegate_t3135042255 ** get_address_of_onDoubleClick_48() { return &___onDoubleClick_48; }
	inline void set_onDoubleClick_48(VoidDelegate_t3135042255 * value)
	{
		___onDoubleClick_48 = value;
		Il2CppCodeGenWriteBarrier(&___onDoubleClick_48, value);
	}

	inline static int32_t get_offset_of_onHover_49() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___onHover_49)); }
	inline BoolDelegate_t2094540325 * get_onHover_49() const { return ___onHover_49; }
	inline BoolDelegate_t2094540325 ** get_address_of_onHover_49() { return &___onHover_49; }
	inline void set_onHover_49(BoolDelegate_t2094540325 * value)
	{
		___onHover_49 = value;
		Il2CppCodeGenWriteBarrier(&___onHover_49, value);
	}

	inline static int32_t get_offset_of_onPress_50() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___onPress_50)); }
	inline BoolDelegate_t2094540325 * get_onPress_50() const { return ___onPress_50; }
	inline BoolDelegate_t2094540325 ** get_address_of_onPress_50() { return &___onPress_50; }
	inline void set_onPress_50(BoolDelegate_t2094540325 * value)
	{
		___onPress_50 = value;
		Il2CppCodeGenWriteBarrier(&___onPress_50, value);
	}

	inline static int32_t get_offset_of_onSelect_51() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___onSelect_51)); }
	inline BoolDelegate_t2094540325 * get_onSelect_51() const { return ___onSelect_51; }
	inline BoolDelegate_t2094540325 ** get_address_of_onSelect_51() { return &___onSelect_51; }
	inline void set_onSelect_51(BoolDelegate_t2094540325 * value)
	{
		___onSelect_51 = value;
		Il2CppCodeGenWriteBarrier(&___onSelect_51, value);
	}

	inline static int32_t get_offset_of_onScroll_52() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___onScroll_52)); }
	inline FloatDelegate_t1146521387 * get_onScroll_52() const { return ___onScroll_52; }
	inline FloatDelegate_t1146521387 ** get_address_of_onScroll_52() { return &___onScroll_52; }
	inline void set_onScroll_52(FloatDelegate_t1146521387 * value)
	{
		___onScroll_52 = value;
		Il2CppCodeGenWriteBarrier(&___onScroll_52, value);
	}

	inline static int32_t get_offset_of_onDrag_53() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___onDrag_53)); }
	inline VectorDelegate_t2747913726 * get_onDrag_53() const { return ___onDrag_53; }
	inline VectorDelegate_t2747913726 ** get_address_of_onDrag_53() { return &___onDrag_53; }
	inline void set_onDrag_53(VectorDelegate_t2747913726 * value)
	{
		___onDrag_53 = value;
		Il2CppCodeGenWriteBarrier(&___onDrag_53, value);
	}

	inline static int32_t get_offset_of_onDragStart_54() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___onDragStart_54)); }
	inline VoidDelegate_t3135042255 * get_onDragStart_54() const { return ___onDragStart_54; }
	inline VoidDelegate_t3135042255 ** get_address_of_onDragStart_54() { return &___onDragStart_54; }
	inline void set_onDragStart_54(VoidDelegate_t3135042255 * value)
	{
		___onDragStart_54 = value;
		Il2CppCodeGenWriteBarrier(&___onDragStart_54, value);
	}

	inline static int32_t get_offset_of_onDragOver_55() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___onDragOver_55)); }
	inline ObjectDelegate_t3703364602 * get_onDragOver_55() const { return ___onDragOver_55; }
	inline ObjectDelegate_t3703364602 ** get_address_of_onDragOver_55() { return &___onDragOver_55; }
	inline void set_onDragOver_55(ObjectDelegate_t3703364602 * value)
	{
		___onDragOver_55 = value;
		Il2CppCodeGenWriteBarrier(&___onDragOver_55, value);
	}

	inline static int32_t get_offset_of_onDragOut_56() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___onDragOut_56)); }
	inline ObjectDelegate_t3703364602 * get_onDragOut_56() const { return ___onDragOut_56; }
	inline ObjectDelegate_t3703364602 ** get_address_of_onDragOut_56() { return &___onDragOut_56; }
	inline void set_onDragOut_56(ObjectDelegate_t3703364602 * value)
	{
		___onDragOut_56 = value;
		Il2CppCodeGenWriteBarrier(&___onDragOut_56, value);
	}

	inline static int32_t get_offset_of_onDragEnd_57() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___onDragEnd_57)); }
	inline VoidDelegate_t3135042255 * get_onDragEnd_57() const { return ___onDragEnd_57; }
	inline VoidDelegate_t3135042255 ** get_address_of_onDragEnd_57() { return &___onDragEnd_57; }
	inline void set_onDragEnd_57(VoidDelegate_t3135042255 * value)
	{
		___onDragEnd_57 = value;
		Il2CppCodeGenWriteBarrier(&___onDragEnd_57, value);
	}

	inline static int32_t get_offset_of_onDrop_58() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___onDrop_58)); }
	inline ObjectDelegate_t3703364602 * get_onDrop_58() const { return ___onDrop_58; }
	inline ObjectDelegate_t3703364602 ** get_address_of_onDrop_58() { return &___onDrop_58; }
	inline void set_onDrop_58(ObjectDelegate_t3703364602 * value)
	{
		___onDrop_58 = value;
		Il2CppCodeGenWriteBarrier(&___onDrop_58, value);
	}

	inline static int32_t get_offset_of_onKey_59() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___onKey_59)); }
	inline KeyCodeDelegate_t4120455995 * get_onKey_59() const { return ___onKey_59; }
	inline KeyCodeDelegate_t4120455995 ** get_address_of_onKey_59() { return &___onKey_59; }
	inline void set_onKey_59(KeyCodeDelegate_t4120455995 * value)
	{
		___onKey_59 = value;
		Il2CppCodeGenWriteBarrier(&___onKey_59, value);
	}

	inline static int32_t get_offset_of_onTooltip_60() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___onTooltip_60)); }
	inline BoolDelegate_t2094540325 * get_onTooltip_60() const { return ___onTooltip_60; }
	inline BoolDelegate_t2094540325 ** get_address_of_onTooltip_60() { return &___onTooltip_60; }
	inline void set_onTooltip_60(BoolDelegate_t2094540325 * value)
	{
		___onTooltip_60 = value;
		Il2CppCodeGenWriteBarrier(&___onTooltip_60, value);
	}

	inline static int32_t get_offset_of_onMouseMove_61() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___onMouseMove_61)); }
	inline MoveDelegate_t1906135052 * get_onMouseMove_61() const { return ___onMouseMove_61; }
	inline MoveDelegate_t1906135052 ** get_address_of_onMouseMove_61() { return &___onMouseMove_61; }
	inline void set_onMouseMove_61(MoveDelegate_t1906135052 * value)
	{
		___onMouseMove_61 = value;
		Il2CppCodeGenWriteBarrier(&___onMouseMove_61, value);
	}

	inline static int32_t get_offset_of_mCurrentSelection_62() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___mCurrentSelection_62)); }
	inline GameObject_t3674682005 * get_mCurrentSelection_62() const { return ___mCurrentSelection_62; }
	inline GameObject_t3674682005 ** get_address_of_mCurrentSelection_62() { return &___mCurrentSelection_62; }
	inline void set_mCurrentSelection_62(GameObject_t3674682005 * value)
	{
		___mCurrentSelection_62 = value;
		Il2CppCodeGenWriteBarrier(&___mCurrentSelection_62, value);
	}

	inline static int32_t get_offset_of_mNextSelection_63() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___mNextSelection_63)); }
	inline GameObject_t3674682005 * get_mNextSelection_63() const { return ___mNextSelection_63; }
	inline GameObject_t3674682005 ** get_address_of_mNextSelection_63() { return &___mNextSelection_63; }
	inline void set_mNextSelection_63(GameObject_t3674682005 * value)
	{
		___mNextSelection_63 = value;
		Il2CppCodeGenWriteBarrier(&___mNextSelection_63, value);
	}

	inline static int32_t get_offset_of_mNextScheme_64() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___mNextScheme_64)); }
	inline int32_t get_mNextScheme_64() const { return ___mNextScheme_64; }
	inline int32_t* get_address_of_mNextScheme_64() { return &___mNextScheme_64; }
	inline void set_mNextScheme_64(int32_t value)
	{
		___mNextScheme_64 = value;
	}

	inline static int32_t get_offset_of_mMouse_65() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___mMouse_65)); }
	inline MouseOrTouchU5BU5D_t546398688* get_mMouse_65() const { return ___mMouse_65; }
	inline MouseOrTouchU5BU5D_t546398688** get_address_of_mMouse_65() { return &___mMouse_65; }
	inline void set_mMouse_65(MouseOrTouchU5BU5D_t546398688* value)
	{
		___mMouse_65 = value;
		Il2CppCodeGenWriteBarrier(&___mMouse_65, value);
	}

	inline static int32_t get_offset_of_mHover_66() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___mHover_66)); }
	inline GameObject_t3674682005 * get_mHover_66() const { return ___mHover_66; }
	inline GameObject_t3674682005 ** get_address_of_mHover_66() { return &___mHover_66; }
	inline void set_mHover_66(GameObject_t3674682005 * value)
	{
		___mHover_66 = value;
		Il2CppCodeGenWriteBarrier(&___mHover_66, value);
	}

	inline static int32_t get_offset_of_controller_67() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___controller_67)); }
	inline MouseOrTouch_t2057376333 * get_controller_67() const { return ___controller_67; }
	inline MouseOrTouch_t2057376333 ** get_address_of_controller_67() { return &___controller_67; }
	inline void set_controller_67(MouseOrTouch_t2057376333 * value)
	{
		___controller_67 = value;
		Il2CppCodeGenWriteBarrier(&___controller_67, value);
	}

	inline static int32_t get_offset_of_mNextEvent_68() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___mNextEvent_68)); }
	inline float get_mNextEvent_68() const { return ___mNextEvent_68; }
	inline float* get_address_of_mNextEvent_68() { return &___mNextEvent_68; }
	inline void set_mNextEvent_68(float value)
	{
		___mNextEvent_68 = value;
	}

	inline static int32_t get_offset_of_mTouches_69() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___mTouches_69)); }
	inline Dictionary_2_t2054639572 * get_mTouches_69() const { return ___mTouches_69; }
	inline Dictionary_2_t2054639572 ** get_address_of_mTouches_69() { return &___mTouches_69; }
	inline void set_mTouches_69(Dictionary_2_t2054639572 * value)
	{
		___mTouches_69 = value;
		Il2CppCodeGenWriteBarrier(&___mTouches_69, value);
	}

	inline static int32_t get_offset_of_mWidth_70() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___mWidth_70)); }
	inline int32_t get_mWidth_70() const { return ___mWidth_70; }
	inline int32_t* get_address_of_mWidth_70() { return &___mWidth_70; }
	inline void set_mWidth_70(int32_t value)
	{
		___mWidth_70 = value;
	}

	inline static int32_t get_offset_of_mHeight_71() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___mHeight_71)); }
	inline int32_t get_mHeight_71() const { return ___mHeight_71; }
	inline int32_t* get_address_of_mHeight_71() { return &___mHeight_71; }
	inline void set_mHeight_71(int32_t value)
	{
		___mHeight_71 = value;
	}

	inline static int32_t get_offset_of_isDragging_76() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___isDragging_76)); }
	inline bool get_isDragging_76() const { return ___isDragging_76; }
	inline bool* get_address_of_isDragging_76() { return &___isDragging_76; }
	inline void set_isDragging_76(bool value)
	{
		___isDragging_76 = value;
	}

	inline static int32_t get_offset_of_hoveredObject_77() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___hoveredObject_77)); }
	inline GameObject_t3674682005 * get_hoveredObject_77() const { return ___hoveredObject_77; }
	inline GameObject_t3674682005 ** get_address_of_hoveredObject_77() { return &___hoveredObject_77; }
	inline void set_hoveredObject_77(GameObject_t3674682005 * value)
	{
		___hoveredObject_77 = value;
		Il2CppCodeGenWriteBarrier(&___hoveredObject_77, value);
	}

	inline static int32_t get_offset_of_mHit_78() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___mHit_78)); }
	inline DepthEntry_t1145614469  get_mHit_78() const { return ___mHit_78; }
	inline DepthEntry_t1145614469 * get_address_of_mHit_78() { return &___mHit_78; }
	inline void set_mHit_78(DepthEntry_t1145614469  value)
	{
		___mHit_78 = value;
	}

	inline static int32_t get_offset_of_mHits_79() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___mHits_79)); }
	inline BetterList_1_t2642582481 * get_mHits_79() const { return ___mHits_79; }
	inline BetterList_1_t2642582481 ** get_address_of_mHits_79() { return &___mHits_79; }
	inline void set_mHits_79(BetterList_1_t2642582481 * value)
	{
		___mHits_79 = value;
		Il2CppCodeGenWriteBarrier(&___mHits_79, value);
	}

	inline static int32_t get_offset_of_m2DPlane_80() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___m2DPlane_80)); }
	inline Plane_t4206452690  get_m2DPlane_80() const { return ___m2DPlane_80; }
	inline Plane_t4206452690 * get_address_of_m2DPlane_80() { return &___m2DPlane_80; }
	inline void set_m2DPlane_80(Plane_t4206452690  value)
	{
		___m2DPlane_80 = value;
	}

	inline static int32_t get_offset_of_mNotifying_81() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___mNotifying_81)); }
	inline bool get_mNotifying_81() const { return ___mNotifying_81; }
	inline bool* get_address_of_mNotifying_81() { return &___mNotifying_81; }
	inline void set_mNotifying_81(bool value)
	{
		___mNotifying_81 = value;
	}

	inline static int32_t get_offset_of_mUsingTouchEvents_82() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___mUsingTouchEvents_82)); }
	inline bool get_mUsingTouchEvents_82() const { return ___mUsingTouchEvents_82; }
	inline bool* get_address_of_mUsingTouchEvents_82() { return &___mUsingTouchEvents_82; }
	inline void set_mUsingTouchEvents_82(bool value)
	{
		___mUsingTouchEvents_82 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache51_83() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___U3CU3Ef__amU24cache51_83)); }
	inline CompareFunc_t1687333339 * get_U3CU3Ef__amU24cache51_83() const { return ___U3CU3Ef__amU24cache51_83; }
	inline CompareFunc_t1687333339 ** get_address_of_U3CU3Ef__amU24cache51_83() { return &___U3CU3Ef__amU24cache51_83; }
	inline void set_U3CU3Ef__amU24cache51_83(CompareFunc_t1687333339 * value)
	{
		___U3CU3Ef__amU24cache51_83 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache51_83, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache52_84() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___U3CU3Ef__amU24cache52_84)); }
	inline CompareFunc_t1687333339 * get_U3CU3Ef__amU24cache52_84() const { return ___U3CU3Ef__amU24cache52_84; }
	inline CompareFunc_t1687333339 ** get_address_of_U3CU3Ef__amU24cache52_84() { return &___U3CU3Ef__amU24cache52_84; }
	inline void set_U3CU3Ef__amU24cache52_84(CompareFunc_t1687333339 * value)
	{
		___U3CU3Ef__amU24cache52_84 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache52_84, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
