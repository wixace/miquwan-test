﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mihua.Asset.ABLoadOperation.AssetOperationSimulation
struct AssetOperationSimulation_t2083017220;
// UnityEngine.Object
struct Object_t3071478659;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Object3071478659.h"

// System.Void Mihua.Asset.ABLoadOperation.AssetOperationSimulation::.ctor(UnityEngine.Object)
extern "C"  void AssetOperationSimulation__ctor_m3278817733 (AssetOperationSimulation_t2083017220 * __this, Object_t3071478659 * ___simulatedObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object Mihua.Asset.ABLoadOperation.AssetOperationSimulation::GetAsset()
extern "C"  Object_t3071478659 * AssetOperationSimulation_GetAsset_m1223477226 (AssetOperationSimulation_t2083017220 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mihua.Asset.ABLoadOperation.AssetOperationSimulation::IsDone()
extern "C"  bool AssetOperationSimulation_IsDone_m2704760233 (AssetOperationSimulation_t2083017220 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Mihua.Asset.ABLoadOperation.AssetOperationSimulation::get_progress()
extern "C"  float AssetOperationSimulation_get_progress_m2868216139 (AssetOperationSimulation_t2083017220 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua.Asset.ABLoadOperation.AssetOperationSimulation::Clear()
extern "C"  void AssetOperationSimulation_Clear_m3263104742 (AssetOperationSimulation_t2083017220 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
