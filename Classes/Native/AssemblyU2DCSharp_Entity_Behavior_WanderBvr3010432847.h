﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "AssemblyU2DCSharp_Entity_Behavior_IBehavior770859129.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entity.Behavior.WanderBvr
struct  WanderBvr_t3010432847  : public IBehavior_t770859129
{
public:
	// UnityEngine.Vector3 Entity.Behavior.WanderBvr::init_pos
	Vector3_t4282066566  ___init_pos_3;
	// System.Single Entity.Behavior.WanderBvr::interval_time
	float ___interval_time_4;
	// System.Single Entity.Behavior.WanderBvr::time
	float ___time_5;
	// System.Boolean Entity.Behavior.WanderBvr::isMoving
	bool ___isMoving_6;

public:
	inline static int32_t get_offset_of_init_pos_3() { return static_cast<int32_t>(offsetof(WanderBvr_t3010432847, ___init_pos_3)); }
	inline Vector3_t4282066566  get_init_pos_3() const { return ___init_pos_3; }
	inline Vector3_t4282066566 * get_address_of_init_pos_3() { return &___init_pos_3; }
	inline void set_init_pos_3(Vector3_t4282066566  value)
	{
		___init_pos_3 = value;
	}

	inline static int32_t get_offset_of_interval_time_4() { return static_cast<int32_t>(offsetof(WanderBvr_t3010432847, ___interval_time_4)); }
	inline float get_interval_time_4() const { return ___interval_time_4; }
	inline float* get_address_of_interval_time_4() { return &___interval_time_4; }
	inline void set_interval_time_4(float value)
	{
		___interval_time_4 = value;
	}

	inline static int32_t get_offset_of_time_5() { return static_cast<int32_t>(offsetof(WanderBvr_t3010432847, ___time_5)); }
	inline float get_time_5() const { return ___time_5; }
	inline float* get_address_of_time_5() { return &___time_5; }
	inline void set_time_5(float value)
	{
		___time_5 = value;
	}

	inline static int32_t get_offset_of_isMoving_6() { return static_cast<int32_t>(offsetof(WanderBvr_t3010432847, ___isMoving_6)); }
	inline bool get_isMoving_6() const { return ___isMoving_6; }
	inline bool* get_address_of_isMoving_6() { return &___isMoving_6; }
	inline void set_isMoving_6(bool value)
	{
		___isMoving_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
