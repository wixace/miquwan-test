﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_GUILayoutGenerated/<GUILayout_Window_GetDelegate_member105_arg2>c__AnonStoreyF7
struct U3CGUILayout_Window_GetDelegate_member105_arg2U3Ec__AnonStoreyF7_t2670494003;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine_GUILayoutGenerated/<GUILayout_Window_GetDelegate_member105_arg2>c__AnonStoreyF7::.ctor()
extern "C"  void U3CGUILayout_Window_GetDelegate_member105_arg2U3Ec__AnonStoreyF7__ctor_m1399034776 (U3CGUILayout_Window_GetDelegate_member105_arg2U3Ec__AnonStoreyF7_t2670494003 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUILayoutGenerated/<GUILayout_Window_GetDelegate_member105_arg2>c__AnonStoreyF7::<>m__242(System.Int32)
extern "C"  void U3CGUILayout_Window_GetDelegate_member105_arg2U3Ec__AnonStoreyF7_U3CU3Em__242_m2256598946 (U3CGUILayout_Window_GetDelegate_member105_arg2U3Ec__AnonStoreyF7_t2670494003 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
