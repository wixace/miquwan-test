﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ScreenOverlay
struct ScreenOverlay_t1089288740;
// UnityEngine.RenderTexture
struct RenderTexture_t1963041563;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture1963041563.h"

// System.Void ScreenOverlay::.ctor()
extern "C"  void ScreenOverlay__ctor_m3824458750 (ScreenOverlay_t1089288740 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ScreenOverlay::CheckResources()
extern "C"  bool ScreenOverlay_CheckResources_m3838015709 (ScreenOverlay_t1089288740 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScreenOverlay::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void ScreenOverlay_OnRenderImage_m3261548960 (ScreenOverlay_t1089288740 * __this, RenderTexture_t1963041563 * ___source0, RenderTexture_t1963041563 * ___destination1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScreenOverlay::Main()
extern "C"  void ScreenOverlay_Main_m177411327 (ScreenOverlay_t1089288740 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
