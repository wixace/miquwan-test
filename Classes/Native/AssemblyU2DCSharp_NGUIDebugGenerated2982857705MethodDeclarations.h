﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// NGUIDebugGenerated
struct NGUIDebugGenerated_t2982857705;
// JSVCall
struct JSVCall_t3708497963;
// System.Object[]
struct ObjectU5BU5D_t1108656482;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "UnityEngine_UnityEngine_Bounds2711641849.h"

// System.Void NGUIDebugGenerated::.ctor()
extern "C"  void NGUIDebugGenerated__ctor_m489130706 (NGUIDebugGenerated_t2982857705 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIDebugGenerated::NGUIDebug_NGUIDebug1(JSVCall,System.Int32)
extern "C"  bool NGUIDebugGenerated_NGUIDebug_NGUIDebug1_m1583887002 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUIDebugGenerated::NGUIDebug_debugRaycast(JSVCall)
extern "C"  void NGUIDebugGenerated_NGUIDebug_debugRaycast_m2929882246 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIDebugGenerated::NGUIDebug_Clear(JSVCall,System.Int32)
extern "C"  bool NGUIDebugGenerated_NGUIDebug_Clear_m1451050400 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIDebugGenerated::NGUIDebug_CreateInstance(JSVCall,System.Int32)
extern "C"  bool NGUIDebugGenerated_NGUIDebug_CreateInstance_m791539776 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIDebugGenerated::NGUIDebug_DrawBounds__Bounds(JSVCall,System.Int32)
extern "C"  bool NGUIDebugGenerated_NGUIDebug_DrawBounds__Bounds_m939575965 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIDebugGenerated::NGUIDebug_Log__Object_Array(JSVCall,System.Int32)
extern "C"  bool NGUIDebugGenerated_NGUIDebug_Log__Object_Array_m2582585808 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUIDebugGenerated::__Register()
extern "C"  void NGUIDebugGenerated___Register_m1274668341 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] NGUIDebugGenerated::<NGUIDebug_Log__Object_Array>m__85()
extern "C"  ObjectU5BU5D_t1108656482* NGUIDebugGenerated_U3CNGUIDebug_Log__Object_ArrayU3Em__85_m3100568025 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUIDebugGenerated::ilo_setBooleanS1(System.Int32,System.Boolean)
extern "C"  void NGUIDebugGenerated_ilo_setBooleanS1_m3226091459 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUIDebugGenerated::ilo_DrawBounds2(UnityEngine.Bounds)
extern "C"  void NGUIDebugGenerated_ilo_DrawBounds2_m2582635370 (Il2CppObject * __this /* static, unused */, Bounds_t2711641849  ___b0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUIDebugGenerated::ilo_Log3(System.Object[])
extern "C"  void NGUIDebugGenerated_ilo_Log3_m1360510942 (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t1108656482* ___objs0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 NGUIDebugGenerated::ilo_getObject4(System.Int32)
extern "C"  int32_t NGUIDebugGenerated_ilo_getObject4_m1275264707 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
