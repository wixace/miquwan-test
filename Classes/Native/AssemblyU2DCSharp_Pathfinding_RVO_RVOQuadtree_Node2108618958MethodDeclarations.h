﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.RVO.Sampled.Agent
struct Agent_t1290054243;
// Pathfinding.RVO.RVOQuadtree/Node[]
struct NodeU5BU5D_t3209507899;
// Pathfinding.RVO.RVOQuadtree/Node
struct Node_t2108618958;
struct Node_t2108618958_marshaled_pinvoke;
struct Node_t2108618958_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_RVO_RVOQuadtree_Node2108618958.h"
#include "AssemblyU2DCSharp_Pathfinding_RVO_Sampled_Agent1290054243.h"
#include "UnityEngine_UnityEngine_Rect4241904616.h"

// System.Void Pathfinding.RVO.RVOQuadtree/Node::Add(Pathfinding.RVO.Sampled.Agent)
extern "C"  void Node_Add_m1071007810 (Node_t2108618958 * __this, Agent_t1290054243 * ___agent0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RVO.RVOQuadtree/Node::Distribute(Pathfinding.RVO.RVOQuadtree/Node[],UnityEngine.Rect)
extern "C"  void Node_Distribute_m159612547 (Node_t2108618958 * __this, NodeU5BU5D_t3209507899* ___nodes0, Rect_t4241904616  ___r1, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct Node_t2108618958;
struct Node_t2108618958_marshaled_pinvoke;

extern "C" void Node_t2108618958_marshal_pinvoke(const Node_t2108618958& unmarshaled, Node_t2108618958_marshaled_pinvoke& marshaled);
extern "C" void Node_t2108618958_marshal_pinvoke_back(const Node_t2108618958_marshaled_pinvoke& marshaled, Node_t2108618958& unmarshaled);
extern "C" void Node_t2108618958_marshal_pinvoke_cleanup(Node_t2108618958_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct Node_t2108618958;
struct Node_t2108618958_marshaled_com;

extern "C" void Node_t2108618958_marshal_com(const Node_t2108618958& unmarshaled, Node_t2108618958_marshaled_com& marshaled);
extern "C" void Node_t2108618958_marshal_com_back(const Node_t2108618958_marshaled_com& marshaled, Node_t2108618958& unmarshaled);
extern "C" void Node_t2108618958_marshal_com_cleanup(Node_t2108618958_marshaled_com& marshaled);
