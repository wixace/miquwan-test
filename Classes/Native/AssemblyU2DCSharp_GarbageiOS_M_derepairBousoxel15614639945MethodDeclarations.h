﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_derepairBousoxel156
struct M_derepairBousoxel156_t14639945;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_derepairBousoxel15614639945.h"

// System.Void GarbageiOS.M_derepairBousoxel156::.ctor()
extern "C"  void M_derepairBousoxel156__ctor_m1786782266 (M_derepairBousoxel156_t14639945 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_derepairBousoxel156::M_nasvouNemel0(System.String[],System.Int32)
extern "C"  void M_derepairBousoxel156_M_nasvouNemel0_m3954100610 (M_derepairBousoxel156_t14639945 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_derepairBousoxel156::M_jastayGeljereve1(System.String[],System.Int32)
extern "C"  void M_derepairBousoxel156_M_jastayGeljereve1_m1844996329 (M_derepairBousoxel156_t14639945 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_derepairBousoxel156::M_founalMawnor2(System.String[],System.Int32)
extern "C"  void M_derepairBousoxel156_M_founalMawnor2_m2336716948 (M_derepairBousoxel156_t14639945 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_derepairBousoxel156::M_helxeaTreekurvow3(System.String[],System.Int32)
extern "C"  void M_derepairBousoxel156_M_helxeaTreekurvow3_m1701362711 (M_derepairBousoxel156_t14639945 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_derepairBousoxel156::M_miborpu4(System.String[],System.Int32)
extern "C"  void M_derepairBousoxel156_M_miborpu4_m3677113945 (M_derepairBousoxel156_t14639945 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_derepairBousoxel156::M_rairmayDopemmere5(System.String[],System.Int32)
extern "C"  void M_derepairBousoxel156_M_rairmayDopemmere5_m2046678759 (M_derepairBousoxel156_t14639945 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_derepairBousoxel156::M_masdor6(System.String[],System.Int32)
extern "C"  void M_derepairBousoxel156_M_masdor6_m303113707 (M_derepairBousoxel156_t14639945 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_derepairBousoxel156::M_sedairRajay7(System.String[],System.Int32)
extern "C"  void M_derepairBousoxel156_M_sedairRajay7_m739856815 (M_derepairBousoxel156_t14639945 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_derepairBousoxel156::ilo_M_nasvouNemel01(GarbageiOS.M_derepairBousoxel156,System.String[],System.Int32)
extern "C"  void M_derepairBousoxel156_ilo_M_nasvouNemel01_m746373001 (Il2CppObject * __this /* static, unused */, M_derepairBousoxel156_t14639945 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
