﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Byte[]
struct ByteU5BU5D_t4260760469;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pomelo.DotNetClient.StateObject
struct  StateObject_t4115849974  : public Il2CppObject
{
public:
	// System.Byte[] Pomelo.DotNetClient.StateObject::buffer
	ByteU5BU5D_t4260760469* ___buffer_1;

public:
	inline static int32_t get_offset_of_buffer_1() { return static_cast<int32_t>(offsetof(StateObject_t4115849974, ___buffer_1)); }
	inline ByteU5BU5D_t4260760469* get_buffer_1() const { return ___buffer_1; }
	inline ByteU5BU5D_t4260760469** get_address_of_buffer_1() { return &___buffer_1; }
	inline void set_buffer_1(ByteU5BU5D_t4260760469* value)
	{
		___buffer_1 = value;
		Il2CppCodeGenWriteBarrier(&___buffer_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
