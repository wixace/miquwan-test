﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// effectCfg
struct effectCfg_t2826279187;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;

#include "codegen/il2cpp-codegen.h"

// System.Void effectCfg::.ctor()
extern "C"  void effectCfg__ctor_m2387935672 (effectCfg_t2826279187 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void effectCfg::Init(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C"  void effectCfg_Init_m177318605 (effectCfg_t2826279187 * __this, Dictionary_2_t827649927 * ____info0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
