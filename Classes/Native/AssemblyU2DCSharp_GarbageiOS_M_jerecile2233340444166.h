﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_jerecile223
struct  M_jerecile223_t3340444166  : public Il2CppObject
{
public:
	// System.UInt32 GarbageiOS.M_jerecile223::_xostaw
	uint32_t ____xostaw_0;
	// System.Boolean GarbageiOS.M_jerecile223::_nasel
	bool ____nasel_1;
	// System.UInt32 GarbageiOS.M_jerecile223::_corkedowLeremoudrer
	uint32_t ____corkedowLeremoudrer_2;
	// System.Boolean GarbageiOS.M_jerecile223::_rezeBawme
	bool ____rezeBawme_3;
	// System.String GarbageiOS.M_jerecile223::_yerterelooJairchemso
	String_t* ____yerterelooJairchemso_4;
	// System.Single GarbageiOS.M_jerecile223::_toumaikorQeray
	float ____toumaikorQeray_5;
	// System.Boolean GarbageiOS.M_jerecile223::_gashersowSootitai
	bool ____gashersowSootitai_6;
	// System.Single GarbageiOS.M_jerecile223::_pahallSereme
	float ____pahallSereme_7;
	// System.Boolean GarbageiOS.M_jerecile223::_ruzownar
	bool ____ruzownar_8;
	// System.Single GarbageiOS.M_jerecile223::_kedaw
	float ____kedaw_9;
	// System.String GarbageiOS.M_jerecile223::_dousor
	String_t* ____dousor_10;
	// System.Boolean GarbageiOS.M_jerecile223::_trowfajearNorpair
	bool ____trowfajearNorpair_11;
	// System.Single GarbageiOS.M_jerecile223::_havai
	float ____havai_12;
	// System.Single GarbageiOS.M_jerecile223::_berera
	float ____berera_13;
	// System.Boolean GarbageiOS.M_jerecile223::_souzeltree
	bool ____souzeltree_14;
	// System.Boolean GarbageiOS.M_jerecile223::_starsi
	bool ____starsi_15;
	// System.Boolean GarbageiOS.M_jerecile223::_dejar
	bool ____dejar_16;
	// System.UInt32 GarbageiOS.M_jerecile223::_chorcalgi
	uint32_t ____chorcalgi_17;
	// System.Boolean GarbageiOS.M_jerecile223::_johooMawjihas
	bool ____johooMawjihas_18;
	// System.Single GarbageiOS.M_jerecile223::_kusiza
	float ____kusiza_19;

public:
	inline static int32_t get_offset_of__xostaw_0() { return static_cast<int32_t>(offsetof(M_jerecile223_t3340444166, ____xostaw_0)); }
	inline uint32_t get__xostaw_0() const { return ____xostaw_0; }
	inline uint32_t* get_address_of__xostaw_0() { return &____xostaw_0; }
	inline void set__xostaw_0(uint32_t value)
	{
		____xostaw_0 = value;
	}

	inline static int32_t get_offset_of__nasel_1() { return static_cast<int32_t>(offsetof(M_jerecile223_t3340444166, ____nasel_1)); }
	inline bool get__nasel_1() const { return ____nasel_1; }
	inline bool* get_address_of__nasel_1() { return &____nasel_1; }
	inline void set__nasel_1(bool value)
	{
		____nasel_1 = value;
	}

	inline static int32_t get_offset_of__corkedowLeremoudrer_2() { return static_cast<int32_t>(offsetof(M_jerecile223_t3340444166, ____corkedowLeremoudrer_2)); }
	inline uint32_t get__corkedowLeremoudrer_2() const { return ____corkedowLeremoudrer_2; }
	inline uint32_t* get_address_of__corkedowLeremoudrer_2() { return &____corkedowLeremoudrer_2; }
	inline void set__corkedowLeremoudrer_2(uint32_t value)
	{
		____corkedowLeremoudrer_2 = value;
	}

	inline static int32_t get_offset_of__rezeBawme_3() { return static_cast<int32_t>(offsetof(M_jerecile223_t3340444166, ____rezeBawme_3)); }
	inline bool get__rezeBawme_3() const { return ____rezeBawme_3; }
	inline bool* get_address_of__rezeBawme_3() { return &____rezeBawme_3; }
	inline void set__rezeBawme_3(bool value)
	{
		____rezeBawme_3 = value;
	}

	inline static int32_t get_offset_of__yerterelooJairchemso_4() { return static_cast<int32_t>(offsetof(M_jerecile223_t3340444166, ____yerterelooJairchemso_4)); }
	inline String_t* get__yerterelooJairchemso_4() const { return ____yerterelooJairchemso_4; }
	inline String_t** get_address_of__yerterelooJairchemso_4() { return &____yerterelooJairchemso_4; }
	inline void set__yerterelooJairchemso_4(String_t* value)
	{
		____yerterelooJairchemso_4 = value;
		Il2CppCodeGenWriteBarrier(&____yerterelooJairchemso_4, value);
	}

	inline static int32_t get_offset_of__toumaikorQeray_5() { return static_cast<int32_t>(offsetof(M_jerecile223_t3340444166, ____toumaikorQeray_5)); }
	inline float get__toumaikorQeray_5() const { return ____toumaikorQeray_5; }
	inline float* get_address_of__toumaikorQeray_5() { return &____toumaikorQeray_5; }
	inline void set__toumaikorQeray_5(float value)
	{
		____toumaikorQeray_5 = value;
	}

	inline static int32_t get_offset_of__gashersowSootitai_6() { return static_cast<int32_t>(offsetof(M_jerecile223_t3340444166, ____gashersowSootitai_6)); }
	inline bool get__gashersowSootitai_6() const { return ____gashersowSootitai_6; }
	inline bool* get_address_of__gashersowSootitai_6() { return &____gashersowSootitai_6; }
	inline void set__gashersowSootitai_6(bool value)
	{
		____gashersowSootitai_6 = value;
	}

	inline static int32_t get_offset_of__pahallSereme_7() { return static_cast<int32_t>(offsetof(M_jerecile223_t3340444166, ____pahallSereme_7)); }
	inline float get__pahallSereme_7() const { return ____pahallSereme_7; }
	inline float* get_address_of__pahallSereme_7() { return &____pahallSereme_7; }
	inline void set__pahallSereme_7(float value)
	{
		____pahallSereme_7 = value;
	}

	inline static int32_t get_offset_of__ruzownar_8() { return static_cast<int32_t>(offsetof(M_jerecile223_t3340444166, ____ruzownar_8)); }
	inline bool get__ruzownar_8() const { return ____ruzownar_8; }
	inline bool* get_address_of__ruzownar_8() { return &____ruzownar_8; }
	inline void set__ruzownar_8(bool value)
	{
		____ruzownar_8 = value;
	}

	inline static int32_t get_offset_of__kedaw_9() { return static_cast<int32_t>(offsetof(M_jerecile223_t3340444166, ____kedaw_9)); }
	inline float get__kedaw_9() const { return ____kedaw_9; }
	inline float* get_address_of__kedaw_9() { return &____kedaw_9; }
	inline void set__kedaw_9(float value)
	{
		____kedaw_9 = value;
	}

	inline static int32_t get_offset_of__dousor_10() { return static_cast<int32_t>(offsetof(M_jerecile223_t3340444166, ____dousor_10)); }
	inline String_t* get__dousor_10() const { return ____dousor_10; }
	inline String_t** get_address_of__dousor_10() { return &____dousor_10; }
	inline void set__dousor_10(String_t* value)
	{
		____dousor_10 = value;
		Il2CppCodeGenWriteBarrier(&____dousor_10, value);
	}

	inline static int32_t get_offset_of__trowfajearNorpair_11() { return static_cast<int32_t>(offsetof(M_jerecile223_t3340444166, ____trowfajearNorpair_11)); }
	inline bool get__trowfajearNorpair_11() const { return ____trowfajearNorpair_11; }
	inline bool* get_address_of__trowfajearNorpair_11() { return &____trowfajearNorpair_11; }
	inline void set__trowfajearNorpair_11(bool value)
	{
		____trowfajearNorpair_11 = value;
	}

	inline static int32_t get_offset_of__havai_12() { return static_cast<int32_t>(offsetof(M_jerecile223_t3340444166, ____havai_12)); }
	inline float get__havai_12() const { return ____havai_12; }
	inline float* get_address_of__havai_12() { return &____havai_12; }
	inline void set__havai_12(float value)
	{
		____havai_12 = value;
	}

	inline static int32_t get_offset_of__berera_13() { return static_cast<int32_t>(offsetof(M_jerecile223_t3340444166, ____berera_13)); }
	inline float get__berera_13() const { return ____berera_13; }
	inline float* get_address_of__berera_13() { return &____berera_13; }
	inline void set__berera_13(float value)
	{
		____berera_13 = value;
	}

	inline static int32_t get_offset_of__souzeltree_14() { return static_cast<int32_t>(offsetof(M_jerecile223_t3340444166, ____souzeltree_14)); }
	inline bool get__souzeltree_14() const { return ____souzeltree_14; }
	inline bool* get_address_of__souzeltree_14() { return &____souzeltree_14; }
	inline void set__souzeltree_14(bool value)
	{
		____souzeltree_14 = value;
	}

	inline static int32_t get_offset_of__starsi_15() { return static_cast<int32_t>(offsetof(M_jerecile223_t3340444166, ____starsi_15)); }
	inline bool get__starsi_15() const { return ____starsi_15; }
	inline bool* get_address_of__starsi_15() { return &____starsi_15; }
	inline void set__starsi_15(bool value)
	{
		____starsi_15 = value;
	}

	inline static int32_t get_offset_of__dejar_16() { return static_cast<int32_t>(offsetof(M_jerecile223_t3340444166, ____dejar_16)); }
	inline bool get__dejar_16() const { return ____dejar_16; }
	inline bool* get_address_of__dejar_16() { return &____dejar_16; }
	inline void set__dejar_16(bool value)
	{
		____dejar_16 = value;
	}

	inline static int32_t get_offset_of__chorcalgi_17() { return static_cast<int32_t>(offsetof(M_jerecile223_t3340444166, ____chorcalgi_17)); }
	inline uint32_t get__chorcalgi_17() const { return ____chorcalgi_17; }
	inline uint32_t* get_address_of__chorcalgi_17() { return &____chorcalgi_17; }
	inline void set__chorcalgi_17(uint32_t value)
	{
		____chorcalgi_17 = value;
	}

	inline static int32_t get_offset_of__johooMawjihas_18() { return static_cast<int32_t>(offsetof(M_jerecile223_t3340444166, ____johooMawjihas_18)); }
	inline bool get__johooMawjihas_18() const { return ____johooMawjihas_18; }
	inline bool* get_address_of__johooMawjihas_18() { return &____johooMawjihas_18; }
	inline void set__johooMawjihas_18(bool value)
	{
		____johooMawjihas_18 = value;
	}

	inline static int32_t get_offset_of__kusiza_19() { return static_cast<int32_t>(offsetof(M_jerecile223_t3340444166, ____kusiza_19)); }
	inline float get__kusiza_19() const { return ____kusiza_19; }
	inline float* get_address_of__kusiza_19() { return &____kusiza_19; }
	inline void set__kusiza_19(float value)
	{
		____kusiza_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
