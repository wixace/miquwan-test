﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K3301300326MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<UIModelDisplayType,UIModelDisplay>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define KeyCollection__ctor_m4013875050(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t861004544 *, Dictionary_2_t3529212389 *, const MethodInfo*))KeyCollection__ctor_m1848708705_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<UIModelDisplayType,UIModelDisplay>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m2155806636(__this, ___item0, method) ((  void (*) (KeyCollection_t861004544 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m2494239317_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<UIModelDisplayType,UIModelDisplay>::System.Collections.Generic.ICollection<TKey>.Clear()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m2174296099(__this, method) ((  void (*) (KeyCollection_t861004544 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1472973068_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<UIModelDisplayType,UIModelDisplay>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m2090226782(__this, ___item0, method) ((  bool (*) (KeyCollection_t861004544 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m3964704537_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<UIModelDisplayType,UIModelDisplay>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m512247107(__this, ___item0, method) ((  bool (*) (KeyCollection_t861004544 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2614753982_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<UIModelDisplayType,UIModelDisplay>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m4137885471(__this, method) ((  Il2CppObject* (*) (KeyCollection_t861004544 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m37742110_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<UIModelDisplayType,UIModelDisplay>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define KeyCollection_System_Collections_ICollection_CopyTo_m2704599061(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t861004544 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m3454827390_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<UIModelDisplayType,UIModelDisplay>::System.Collections.IEnumerable.GetEnumerator()
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m3669670480(__this, method) ((  Il2CppObject * (*) (KeyCollection_t861004544 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1451607565_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<UIModelDisplayType,UIModelDisplay>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m1544301631(__this, method) ((  bool (*) (KeyCollection_t861004544 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m3326127162_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<UIModelDisplayType,UIModelDisplay>::System.Collections.ICollection.get_IsSynchronized()
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m3471681457(__this, method) ((  bool (*) (KeyCollection_t861004544 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m3274167660_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<UIModelDisplayType,UIModelDisplay>::System.Collections.ICollection.get_SyncRoot()
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m917079389(__this, method) ((  Il2CppObject * (*) (KeyCollection_t861004544 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m3354365790_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<UIModelDisplayType,UIModelDisplay>::CopyTo(TKey[],System.Int32)
#define KeyCollection_CopyTo_m800353823(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t861004544 *, UIModelDisplayTypeU5BU5D_t737309086*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m2502441238_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<UIModelDisplayType,UIModelDisplay>::GetEnumerator()
#define KeyCollection_GetEnumerator_m1419432514(__this, method) ((  Enumerator_t4144148443  (*) (KeyCollection_t861004544 *, const MethodInfo*))KeyCollection_GetEnumerator_m2827792547_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<UIModelDisplayType,UIModelDisplay>::get_Count()
#define KeyCollection_get_Count_m417111543(__this, method) ((  int32_t (*) (KeyCollection_t861004544 *, const MethodInfo*))KeyCollection_get_Count_m3442814886_gshared)(__this, method)
