﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_ConnectionType268218200.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.UserConnection
struct  UserConnection_t885308927  : public Il2CppObject
{
public:
	// UnityEngine.Vector3 Pathfinding.UserConnection::p1
	Vector3_t4282066566  ___p1_0;
	// UnityEngine.Vector3 Pathfinding.UserConnection::p2
	Vector3_t4282066566  ___p2_1;
	// ConnectionType Pathfinding.UserConnection::type
	int32_t ___type_2;
	// System.Boolean Pathfinding.UserConnection::doOverrideCost
	bool ___doOverrideCost_3;
	// System.Int32 Pathfinding.UserConnection::overrideCost
	int32_t ___overrideCost_4;
	// System.Boolean Pathfinding.UserConnection::oneWay
	bool ___oneWay_5;
	// System.Boolean Pathfinding.UserConnection::enable
	bool ___enable_6;
	// System.Single Pathfinding.UserConnection::width
	float ___width_7;
	// System.Boolean Pathfinding.UserConnection::doOverrideWalkability
	bool ___doOverrideWalkability_8;
	// System.Boolean Pathfinding.UserConnection::doOverridePenalty
	bool ___doOverridePenalty_9;
	// System.UInt32 Pathfinding.UserConnection::overridePenalty
	uint32_t ___overridePenalty_10;

public:
	inline static int32_t get_offset_of_p1_0() { return static_cast<int32_t>(offsetof(UserConnection_t885308927, ___p1_0)); }
	inline Vector3_t4282066566  get_p1_0() const { return ___p1_0; }
	inline Vector3_t4282066566 * get_address_of_p1_0() { return &___p1_0; }
	inline void set_p1_0(Vector3_t4282066566  value)
	{
		___p1_0 = value;
	}

	inline static int32_t get_offset_of_p2_1() { return static_cast<int32_t>(offsetof(UserConnection_t885308927, ___p2_1)); }
	inline Vector3_t4282066566  get_p2_1() const { return ___p2_1; }
	inline Vector3_t4282066566 * get_address_of_p2_1() { return &___p2_1; }
	inline void set_p2_1(Vector3_t4282066566  value)
	{
		___p2_1 = value;
	}

	inline static int32_t get_offset_of_type_2() { return static_cast<int32_t>(offsetof(UserConnection_t885308927, ___type_2)); }
	inline int32_t get_type_2() const { return ___type_2; }
	inline int32_t* get_address_of_type_2() { return &___type_2; }
	inline void set_type_2(int32_t value)
	{
		___type_2 = value;
	}

	inline static int32_t get_offset_of_doOverrideCost_3() { return static_cast<int32_t>(offsetof(UserConnection_t885308927, ___doOverrideCost_3)); }
	inline bool get_doOverrideCost_3() const { return ___doOverrideCost_3; }
	inline bool* get_address_of_doOverrideCost_3() { return &___doOverrideCost_3; }
	inline void set_doOverrideCost_3(bool value)
	{
		___doOverrideCost_3 = value;
	}

	inline static int32_t get_offset_of_overrideCost_4() { return static_cast<int32_t>(offsetof(UserConnection_t885308927, ___overrideCost_4)); }
	inline int32_t get_overrideCost_4() const { return ___overrideCost_4; }
	inline int32_t* get_address_of_overrideCost_4() { return &___overrideCost_4; }
	inline void set_overrideCost_4(int32_t value)
	{
		___overrideCost_4 = value;
	}

	inline static int32_t get_offset_of_oneWay_5() { return static_cast<int32_t>(offsetof(UserConnection_t885308927, ___oneWay_5)); }
	inline bool get_oneWay_5() const { return ___oneWay_5; }
	inline bool* get_address_of_oneWay_5() { return &___oneWay_5; }
	inline void set_oneWay_5(bool value)
	{
		___oneWay_5 = value;
	}

	inline static int32_t get_offset_of_enable_6() { return static_cast<int32_t>(offsetof(UserConnection_t885308927, ___enable_6)); }
	inline bool get_enable_6() const { return ___enable_6; }
	inline bool* get_address_of_enable_6() { return &___enable_6; }
	inline void set_enable_6(bool value)
	{
		___enable_6 = value;
	}

	inline static int32_t get_offset_of_width_7() { return static_cast<int32_t>(offsetof(UserConnection_t885308927, ___width_7)); }
	inline float get_width_7() const { return ___width_7; }
	inline float* get_address_of_width_7() { return &___width_7; }
	inline void set_width_7(float value)
	{
		___width_7 = value;
	}

	inline static int32_t get_offset_of_doOverrideWalkability_8() { return static_cast<int32_t>(offsetof(UserConnection_t885308927, ___doOverrideWalkability_8)); }
	inline bool get_doOverrideWalkability_8() const { return ___doOverrideWalkability_8; }
	inline bool* get_address_of_doOverrideWalkability_8() { return &___doOverrideWalkability_8; }
	inline void set_doOverrideWalkability_8(bool value)
	{
		___doOverrideWalkability_8 = value;
	}

	inline static int32_t get_offset_of_doOverridePenalty_9() { return static_cast<int32_t>(offsetof(UserConnection_t885308927, ___doOverridePenalty_9)); }
	inline bool get_doOverridePenalty_9() const { return ___doOverridePenalty_9; }
	inline bool* get_address_of_doOverridePenalty_9() { return &___doOverridePenalty_9; }
	inline void set_doOverridePenalty_9(bool value)
	{
		___doOverridePenalty_9 = value;
	}

	inline static int32_t get_offset_of_overridePenalty_10() { return static_cast<int32_t>(offsetof(UserConnection_t885308927, ___overridePenalty_10)); }
	inline uint32_t get_overridePenalty_10() const { return ___overridePenalty_10; }
	inline uint32_t* get_address_of_overridePenalty_10() { return &___overridePenalty_10; }
	inline void set_overridePenalty_10(uint32_t value)
	{
		___overridePenalty_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
