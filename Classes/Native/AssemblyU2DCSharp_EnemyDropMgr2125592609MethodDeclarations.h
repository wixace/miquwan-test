﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EnemyDropMgr
struct EnemyDropMgr_t2125592609;
// EnemyDropData
struct EnemyDropData_t1468587713;
// CombatEntity
struct CombatEntity_t684137495;
// EnemyDropItem
struct EnemyDropItem_t1468754474;
// CSDatacfgManager
struct CSDatacfgManager_t1565254243;
// enemy_dropCfg
struct enemy_dropCfg_t1026960702;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_EnemyDropData1468587713.h"
#include "AssemblyU2DCSharp_CombatEntity684137495.h"
#include "AssemblyU2DCSharp_EnemyDropItem1468754474.h"
#include "AssemblyU2DCSharp_EnemyDropMgr2125592609.h"
#include "AssemblyU2DCSharp_CSDatacfgManager1565254243.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void EnemyDropMgr::.ctor()
extern "C"  void EnemyDropMgr__ctor_m2739179418 (EnemyDropMgr_t2125592609 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// EnemyDropMgr EnemyDropMgr::get_Instance()
extern "C"  EnemyDropMgr_t2125592609 * EnemyDropMgr_get_Instance_m3907849604 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EnemyDropMgr::AddDrop(EnemyDropData,CombatEntity)
extern "C"  void EnemyDropMgr_AddDrop_m127840390 (EnemyDropMgr_t2125592609 * __this, EnemyDropData_t1468587713 * ___data0, CombatEntity_t684137495 * ___entity1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EnemyDropMgr::AddDrop(EnemyDropData,CombatEntity,System.Single)
extern "C"  void EnemyDropMgr_AddDrop_m981391595 (EnemyDropMgr_t2125592609 * __this, EnemyDropData_t1468587713 * ___data0, CombatEntity_t684137495 * ___entity1, float ___hurt2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EnemyDropMgr::RemoveDrop(EnemyDropItem)
extern "C"  void EnemyDropMgr_RemoveDrop_m844501867 (EnemyDropMgr_t2125592609 * __this, EnemyDropItem_t1468754474 * ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EnemyDropMgr::ClearDrop()
extern "C"  void EnemyDropMgr_ClearDrop_m3957613844 (EnemyDropMgr_t2125592609 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EnemyDropMgr::Clear()
extern "C"  void EnemyDropMgr_Clear_m145312709 (EnemyDropMgr_t2125592609 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// EnemyDropItem EnemyDropMgr::CreatDropItem(System.Int32)
extern "C"  EnemyDropItem_t1468754474 * EnemyDropMgr_CreatDropItem_m432023627 (EnemyDropMgr_t2125592609 * __this, int32_t ___dropId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EnemyDropMgr::ResetDropItem(EnemyDropItem)
extern "C"  void EnemyDropMgr_ResetDropItem_m199563487 (EnemyDropMgr_t2125592609 * __this, EnemyDropItem_t1468754474 * ___drop0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// EnemyDropItem EnemyDropMgr::GetDropItem()
extern "C"  EnemyDropItem_t1468754474 * EnemyDropMgr_GetDropItem_m3783516167 (EnemyDropMgr_t2125592609 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// EnemyDropItem EnemyDropMgr::ilo_GetDropItem1(EnemyDropMgr)
extern "C"  EnemyDropItem_t1468754474 * EnemyDropMgr_ilo_GetDropItem1_m2194714014 (Il2CppObject * __this /* static, unused */, EnemyDropMgr_t2125592609 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CSDatacfgManager EnemyDropMgr::ilo_get_CfgDataMgr2()
extern "C"  CSDatacfgManager_t1565254243 * EnemyDropMgr_ilo_get_CfgDataMgr2_m1894432746 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 EnemyDropMgr::ilo_get_dropId3(EnemyDropData)
extern "C"  int32_t EnemyDropMgr_ilo_get_dropId3_m2928286298 (Il2CppObject * __this /* static, unused */, EnemyDropData_t1468587713 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// enemy_dropCfg EnemyDropMgr::ilo_Getenemy_dropCfg4(CSDatacfgManager,System.Int32)
extern "C"  enemy_dropCfg_t1026960702 * EnemyDropMgr_ilo_Getenemy_dropCfg4_m3267864958 (Il2CppObject * __this /* static, unused */, CSDatacfgManager_t1565254243 * ____this0, int32_t ___id1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 EnemyDropMgr::ilo_get_dropNum5(EnemyDropData)
extern "C"  int32_t EnemyDropMgr_ilo_get_dropNum5_m4205906559 (Il2CppObject * __this /* static, unused */, EnemyDropData_t1468587713 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EnemyDropMgr::ilo_Init6(EnemyDropItem,System.Int32,System.Single,CombatEntity,UnityEngine.Vector3)
extern "C"  void EnemyDropMgr_ilo_Init6_m1305930179 (Il2CppObject * __this /* static, unused */, EnemyDropItem_t1468754474 * ____this0, int32_t ___id1, float ___num2, CombatEntity_t684137495 * ___akill3, Vector3_t4282066566  ___targetPos4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
