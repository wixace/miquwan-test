﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WKWebView
struct WKWebView_t2120165029;
// System.String
struct String_t;
// WebViewObject
struct WebViewObject_t388577432;
// System.Action`1<System.String>
struct Action_1_t403047693;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_WKWebView2120165029.h"
#include "AssemblyU2DCSharp_WebViewObject388577432.h"

// System.Void WKWebView::.ctor()
extern "C"  void WKWebView__ctor_m331126886 (WKWebView_t2120165029 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String WKWebView::get_Url()
extern "C"  String_t* WKWebView_get_Url_m2647778029 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WKWebView::set_Url(System.String)
extern "C"  void WKWebView_set_Url_m2619739756 (Il2CppObject * __this /* static, unused */, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WKWebView::Start()
extern "C"  void WKWebView_Start_m3573231974 (WKWebView_t2120165029 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WKWebView::Init()
extern "C"  void WKWebView_Init_m654954798 (WKWebView_t2120165029 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WKWebView::<Init>m__329(System.String)
extern "C"  void WKWebView_U3CInitU3Em__329_m2589348997 (Il2CppObject * __this /* static, unused */, String_t* ___msg0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WKWebView::<Init>m__32A(System.String)
extern "C"  void WKWebView_U3CInitU3Em__32A_m2800042877 (Il2CppObject * __this /* static, unused */, String_t* ___msg0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WKWebView::<Init>m__32B(System.String)
extern "C"  void WKWebView_U3CInitU3Em__32B_m2289508700 (Il2CppObject * __this /* static, unused */, String_t* ___msg0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WKWebView::<Init>m__32C(System.String)
extern "C"  void WKWebView_U3CInitU3Em__32C_m1778974523 (WKWebView_t2120165029 * __this, String_t* ___msg0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WKWebView::ilo_Init1(WKWebView)
extern "C"  void WKWebView_ilo_Init1_m1315007771 (Il2CppObject * __this /* static, unused */, WKWebView_t2120165029 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WKWebView::ilo_Init2(WebViewObject,System.Action`1<System.String>,System.Boolean,System.String,System.Action`1<System.String>,System.Action`1<System.String>,System.Action`1<System.String>,System.Boolean,System.Action`1<System.String>)
extern "C"  void WKWebView_ilo_Init2_m2852558889 (Il2CppObject * __this /* static, unused */, WebViewObject_t388577432 * ____this0, Action_1_t403047693 * ___cb1, bool ___transparent2, String_t* ___ua3, Action_1_t403047693 * ___err4, Action_1_t403047693 * ___httpErr5, Action_1_t403047693 * ___ld6, bool ___enableWKWebView7, Action_1_t403047693 * ___started8, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WKWebView::ilo_SetVisibility3(WebViewObject,System.Boolean)
extern "C"  void WKWebView_ilo_SetVisibility3_m352613267 (Il2CppObject * __this /* static, unused */, WebViewObject_t388577432 * ____this0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
