﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Entity.Behavior.AIMoveBvr
struct AIMoveBvr_t2054365827;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// Entity.Behavior.IBehaviorCtrl
struct IBehaviorCtrl_t4225040900;
// CombatEntity
struct CombatEntity_t684137495;
// Entity.Behavior.IBehavior
struct IBehavior_t770859129;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Entity_Behavior_EBehaviorID1238533136.h"
#include "AssemblyU2DCSharp_CombatEntity684137495.h"
#include "AssemblyU2DCSharp_Entity_Behavior_IBehavior770859129.h"

// System.Void Entity.Behavior.AIMoveBvr::.ctor()
extern "C"  void AIMoveBvr__ctor_m59094231 (AIMoveBvr_t2054365827 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Entity.Behavior.EBehaviorID Entity.Behavior.AIMoveBvr::get_id()
extern "C"  uint8_t AIMoveBvr_get_id_m2423894463 (AIMoveBvr_t2054365827 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.AIMoveBvr::Reason()
extern "C"  void AIMoveBvr_Reason_m1383309265 (AIMoveBvr_t2054365827 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.AIMoveBvr::Action()
extern "C"  void AIMoveBvr_Action_m580015939 (AIMoveBvr_t2054365827 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.AIMoveBvr::SetParams(System.Object[])
extern "C"  void AIMoveBvr_SetParams_m2457222037 (AIMoveBvr_t2054365827 * __this, ObjectU5BU5D_t1108656482* ___args0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.AIMoveBvr::DoEntering()
extern "C"  void AIMoveBvr_DoEntering_m573867778 (AIMoveBvr_t2054365827 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.AIMoveBvr::DoLeaving()
extern "C"  void AIMoveBvr_DoLeaving_m3095821406 (AIMoveBvr_t2054365827 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.AIMoveBvr::Pause()
extern "C"  void AIMoveBvr_Pause_m113220203 (AIMoveBvr_t2054365827 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.AIMoveBvr::Play()
extern "C"  void AIMoveBvr_Play_m2922713025 (AIMoveBvr_t2054365827 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Entity.Behavior.IBehaviorCtrl Entity.Behavior.AIMoveBvr::ilo_get_bvrCtrl1(CombatEntity)
extern "C"  IBehaviorCtrl_t4225040900 * AIMoveBvr_ilo_get_bvrCtrl1_m2614257596 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CombatEntity Entity.Behavior.AIMoveBvr::ilo_get_entity2(Entity.Behavior.IBehavior)
extern "C"  CombatEntity_t684137495 * AIMoveBvr_ilo_get_entity2_m1324112666 (Il2CppObject * __this /* static, unused */, IBehavior_t770859129 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.AIMoveBvr::ilo_Pause3(Entity.Behavior.IBehavior)
extern "C"  void AIMoveBvr_ilo_Pause3_m549437835 (Il2CppObject * __this /* static, unused */, IBehavior_t770859129 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.AIMoveBvr::ilo_Play4(Entity.Behavior.IBehavior)
extern "C"  void AIMoveBvr_ilo_Play4_m1356632262 (Il2CppObject * __this /* static, unused */, IBehavior_t770859129 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
