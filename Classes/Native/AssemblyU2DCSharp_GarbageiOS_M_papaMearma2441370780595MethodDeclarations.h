﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_papaMearma244
struct M_papaMearma244_t1370780595;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_papaMearma2441370780595.h"

// System.Void GarbageiOS.M_papaMearma244::.ctor()
extern "C"  void M_papaMearma244__ctor_m2736282384 (M_papaMearma244_t1370780595 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_papaMearma244::M_draipa0(System.String[],System.Int32)
extern "C"  void M_papaMearma244_M_draipa0_m1090321212 (M_papaMearma244_t1370780595 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_papaMearma244::M_memxoo1(System.String[],System.Int32)
extern "C"  void M_papaMearma244_M_memxoo1_m2070385473 (M_papaMearma244_t1370780595 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_papaMearma244::M_jowmi2(System.String[],System.Int32)
extern "C"  void M_papaMearma244_M_jowmi2_m820464257 (M_papaMearma244_t1370780595 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_papaMearma244::M_pisxaijee3(System.String[],System.Int32)
extern "C"  void M_papaMearma244_M_pisxaijee3_m2137498732 (M_papaMearma244_t1370780595 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_papaMearma244::M_woniQearjis4(System.String[],System.Int32)
extern "C"  void M_papaMearma244_M_woniQearjis4_m2928890069 (M_papaMearma244_t1370780595 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_papaMearma244::ilo_M_memxoo11(GarbageiOS.M_papaMearma244,System.String[],System.Int32)
extern "C"  void M_papaMearma244_ilo_M_memxoo11_m4011487852 (Il2CppObject * __this /* static, unused */, M_papaMearma244_t1370780595 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_papaMearma244::ilo_M_pisxaijee32(GarbageiOS.M_papaMearma244,System.String[],System.Int32)
extern "C"  void M_papaMearma244_ilo_M_pisxaijee32_m2114442300 (Il2CppObject * __this /* static, unused */, M_papaMearma244_t1370780595 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
