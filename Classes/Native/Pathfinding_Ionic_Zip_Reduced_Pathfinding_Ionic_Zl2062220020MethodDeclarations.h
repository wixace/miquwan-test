﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.Ionic.Zlib.StaticTree
struct StaticTree_t2062220020;
// System.Int16[]
struct Int16U5BU5D_t801762735;
// System.Int32[]
struct Int32U5BU5D_t3230847821;

#include "codegen/il2cpp-codegen.h"

// System.Void Pathfinding.Ionic.Zlib.StaticTree::.ctor(System.Int16[],System.Int32[],System.Int32,System.Int32,System.Int32)
extern "C"  void StaticTree__ctor_m2056368912 (StaticTree_t2062220020 * __this, Int16U5BU5D_t801762735* ___treeCodes0, Int32U5BU5D_t3230847821* ___extraBits1, int32_t ___extraBase2, int32_t ___elems3, int32_t ___maxLength4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Ionic.Zlib.StaticTree::.cctor()
extern "C"  void StaticTree__cctor_m230249544 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
