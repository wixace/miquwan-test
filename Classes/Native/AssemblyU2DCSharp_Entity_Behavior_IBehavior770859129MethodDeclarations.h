﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Entity.Behavior.IBehavior
struct IBehavior_t770859129;
// CombatEntity
struct CombatEntity_t684137495;
// System.Object[]
struct ObjectU5BU5D_t1108656482;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CombatEntity684137495.h"
#include "AssemblyU2DCSharp_Entity_Behavior_IBehavior770859129.h"

// System.Void Entity.Behavior.IBehavior::.ctor()
extern "C"  void IBehavior__ctor_m225299361 (IBehavior_t770859129 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CombatEntity Entity.Behavior.IBehavior::get_entity()
extern "C"  CombatEntity_t684137495 * IBehavior_get_entity_m341959177 (IBehavior_t770859129 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.IBehavior::set_entity(CombatEntity)
extern "C"  void IBehavior_set_entity_m4007261708 (IBehavior_t770859129 * __this, CombatEntity_t684137495 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Entity.Behavior.IBehavior::CanTran2OtherBehavior(Entity.Behavior.IBehavior)
extern "C"  bool IBehavior_CanTran2OtherBehavior_m403076138 (IBehavior_t770859129 * __this, IBehavior_t770859129 * ___behavior0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.IBehavior::SetParams(System.Object[])
extern "C"  void IBehavior_SetParams_m3719138443 (IBehavior_t770859129 * __this, ObjectU5BU5D_t1108656482* ___args0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.IBehavior::Pause()
extern "C"  void IBehavior_Pause_m279425333 (IBehavior_t770859129 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.IBehavior::Play()
extern "C"  void IBehavior_Play_m988411831 (IBehavior_t770859129 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Entity.Behavior.IBehavior::IsLockingBehavior()
extern "C"  bool IBehavior_IsLockingBehavior_m3082085234 (IBehavior_t770859129 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
