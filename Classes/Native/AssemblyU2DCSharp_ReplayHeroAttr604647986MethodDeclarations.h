﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ReplayHeroAttr
struct ReplayHeroAttr_t604647986;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// System.String[]
struct StringU5BU5D_t4054002952;
// System.String
struct String_t;
// ReplayBase
struct ReplayBase_t779703160;
// CombatEntity
struct CombatEntity_t684137495;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_ReplayBase779703160.h"

// System.Void ReplayHeroAttr::.ctor()
extern "C"  void ReplayHeroAttr__ctor_m1002173097 (ReplayHeroAttr_t604647986 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReplayHeroAttr::.ctor(System.Object[])
extern "C"  void ReplayHeroAttr__ctor_m3135420905 (ReplayHeroAttr_t604647986 * __this, ObjectU5BU5D_t1108656482* ___datas0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] ReplayHeroAttr::ParserJsonStr(System.String)
extern "C"  StringU5BU5D_t4054002952* ReplayHeroAttr_ParserJsonStr_m2382718738 (ReplayHeroAttr_t604647986 * __this, String_t* ___json0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ReplayHeroAttr::GetJsonStr()
extern "C"  String_t* ReplayHeroAttr_GetJsonStr_m2199245009 (ReplayHeroAttr_t604647986 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReplayHeroAttr::Execute()
extern "C"  void ReplayHeroAttr_Execute_m3867061372 (ReplayHeroAttr_t604647986 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReplayHeroAttr::ilo_Execute1(ReplayBase)
extern "C"  void ReplayHeroAttr_ilo_Execute1_m2087643282 (Il2CppObject * __this /* static, unused */, ReplayBase_t779703160 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CombatEntity ReplayHeroAttr::ilo_get_Entity2(ReplayBase)
extern "C"  CombatEntity_t684137495 * ReplayHeroAttr_ilo_get_Entity2_m124751292 (Il2CppObject * __this /* static, unused */, ReplayBase_t779703160 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
