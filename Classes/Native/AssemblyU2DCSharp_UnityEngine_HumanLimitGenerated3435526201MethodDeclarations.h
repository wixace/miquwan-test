﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_HumanLimitGenerated
struct UnityEngine_HumanLimitGenerated_t3435526201;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void UnityEngine_HumanLimitGenerated::.ctor()
extern "C"  void UnityEngine_HumanLimitGenerated__ctor_m1084763986 (UnityEngine_HumanLimitGenerated_t3435526201 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_HumanLimitGenerated::.cctor()
extern "C"  void UnityEngine_HumanLimitGenerated__cctor_m3080816283 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_HumanLimitGenerated::HumanLimit_HumanLimit1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_HumanLimitGenerated_HumanLimit_HumanLimit1_m95060576 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_HumanLimitGenerated::HumanLimit_useDefaultValues(JSVCall)
extern "C"  void UnityEngine_HumanLimitGenerated_HumanLimit_useDefaultValues_m4271488266 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_HumanLimitGenerated::HumanLimit_min(JSVCall)
extern "C"  void UnityEngine_HumanLimitGenerated_HumanLimit_min_m2328995572 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_HumanLimitGenerated::HumanLimit_max(JSVCall)
extern "C"  void UnityEngine_HumanLimitGenerated_HumanLimit_max_m1854569506 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_HumanLimitGenerated::HumanLimit_center(JSVCall)
extern "C"  void UnityEngine_HumanLimitGenerated_HumanLimit_center_m834575697 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_HumanLimitGenerated::HumanLimit_axisLength(JSVCall)
extern "C"  void UnityEngine_HumanLimitGenerated_HumanLimit_axisLength_m1111768063 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_HumanLimitGenerated::__Register()
extern "C"  void UnityEngine_HumanLimitGenerated___Register_m229380277 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_HumanLimitGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UnityEngine_HumanLimitGenerated_ilo_getObject1_m544773668 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_HumanLimitGenerated::ilo_attachFinalizerObject2(System.Int32)
extern "C"  bool UnityEngine_HumanLimitGenerated_ilo_attachFinalizerObject2_m2318973734 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_HumanLimitGenerated::ilo_addJSCSRel3(System.Int32,System.Object)
extern "C"  void UnityEngine_HumanLimitGenerated_ilo_addJSCSRel3_m3362639376 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_HumanLimitGenerated::ilo_getBooleanS4(System.Int32)
extern "C"  bool UnityEngine_HumanLimitGenerated_ilo_getBooleanS4_m3373236245 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_HumanLimitGenerated::ilo_setVector3S5(System.Int32,UnityEngine.Vector3)
extern "C"  void UnityEngine_HumanLimitGenerated_ilo_setVector3S5_m47261369 (Il2CppObject * __this /* static, unused */, int32_t ___e0, Vector3_t4282066566  ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine_HumanLimitGenerated::ilo_getVector3S6(System.Int32)
extern "C"  Vector3_t4282066566  UnityEngine_HumanLimitGenerated_ilo_getVector3S6_m365828153 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_HumanLimitGenerated::ilo_changeJSObj7(System.Int32,System.Object)
extern "C"  void UnityEngine_HumanLimitGenerated_ilo_changeJSObj7_m1736039517 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObjNew1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
