﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_mafoli136
struct M_mafoli136_t2921639866;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"

// System.Void GarbageiOS.M_mafoli136::.ctor()
extern "C"  void M_mafoli136__ctor_m3767632617 (M_mafoli136_t2921639866 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_mafoli136::M_fecasair0(System.String[],System.Int32)
extern "C"  void M_mafoli136_M_fecasair0_m893145928 (M_mafoli136_t2921639866 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_mafoli136::M_pilawStalje1(System.String[],System.Int32)
extern "C"  void M_mafoli136_M_pilawStalje1_m1532001285 (M_mafoli136_t2921639866 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
