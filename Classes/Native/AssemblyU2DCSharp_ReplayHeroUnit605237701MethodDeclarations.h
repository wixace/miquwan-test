﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ReplayHeroUnit
struct ReplayHeroUnit_t605237701;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void ReplayHeroUnit::.ctor()
extern "C"  void ReplayHeroUnit__ctor_m848152694 (ReplayHeroUnit_t605237701 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReplayHeroUnit::ParserJson(System.String)
extern "C"  void ReplayHeroUnit_ParserJson_m2740352237 (ReplayHeroUnit_t605237701 * __this, String_t* ___json0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ReplayHeroUnit::GetJsonStr()
extern "C"  String_t* ReplayHeroUnit_GetJsonStr_m4243755812 (ReplayHeroUnit_t605237701 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
