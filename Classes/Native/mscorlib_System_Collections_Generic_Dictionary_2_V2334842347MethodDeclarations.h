﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Va746493984MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Pathfinding.GraphNode,Pathfinding.NodeLink3>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define ValueCollection__ctor_m664945041(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t2334842347 *, Dictionary_2_t3634236634 *, const MethodInfo*))ValueCollection__ctor_m4177258586_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Pathfinding.GraphNode,Pathfinding.NodeLink3>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m3493135489(__this, ___item0, method) ((  void (*) (ValueCollection_t2334842347 *, NodeLink3_t1645404665 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1528225944_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Pathfinding.GraphNode,Pathfinding.NodeLink3>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1192974794(__this, method) ((  void (*) (ValueCollection_t2334842347 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m2827278689_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<Pathfinding.GraphNode,Pathfinding.NodeLink3>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m305525829(__this, ___item0, method) ((  bool (*) (ValueCollection_t2334842347 *, NodeLink3_t1645404665 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m2015709838_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<Pathfinding.GraphNode,Pathfinding.NodeLink3>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m3961084458(__this, ___item0, method) ((  bool (*) (ValueCollection_t2334842347 *, NodeLink3_t1645404665 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m3578506931_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<Pathfinding.GraphNode,Pathfinding.NodeLink3>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m3035741912(__this, method) ((  Il2CppObject* (*) (ValueCollection_t2334842347 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m4041606511_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Pathfinding.GraphNode,Pathfinding.NodeLink3>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m2888549966(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t2334842347 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m1709700389_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<Pathfinding.GraphNode,Pathfinding.NodeLink3>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m409383881(__this, method) ((  Il2CppObject * (*) (ValueCollection_t2334842347 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m2843387488_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<Pathfinding.GraphNode,Pathfinding.NodeLink3>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m2875668984(__this, method) ((  bool (*) (ValueCollection_t2334842347 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m290885697_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<Pathfinding.GraphNode,Pathfinding.NodeLink3>::System.Collections.ICollection.get_IsSynchronized()
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m3376067544(__this, method) ((  bool (*) (ValueCollection_t2334842347 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m1930063777_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<Pathfinding.GraphNode,Pathfinding.NodeLink3>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m3906674500(__this, method) ((  Il2CppObject * (*) (ValueCollection_t2334842347 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m1874108365_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Pathfinding.GraphNode,Pathfinding.NodeLink3>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m2171103640(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t2334842347 *, NodeLink3U5BU5D_t3260080644*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m1735386657_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<Pathfinding.GraphNode,Pathfinding.NodeLink3>::GetEnumerator()
#define ValueCollection_GetEnumerator_m3275110011(__this, method) ((  Enumerator_t1566070042  (*) (ValueCollection_t2334842347 *, const MethodInfo*))ValueCollection_GetEnumerator_m1204216004_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<Pathfinding.GraphNode,Pathfinding.NodeLink3>::get_Count()
#define ValueCollection_get_Count_m2437977118(__this, method) ((  int32_t (*) (ValueCollection_t2334842347 *, const MethodInfo*))ValueCollection_get_Count_m2709231847_gshared)(__this, method)
