﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BehaviourUtil/<DelayCallExec>c__Iterator41`3<System.Boolean,System.Object,System.Int32>
struct U3CDelayCallExecU3Ec__Iterator41_3_t4065262153;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void BehaviourUtil/<DelayCallExec>c__Iterator41`3<System.Boolean,System.Object,System.Int32>::.ctor()
extern "C"  void U3CDelayCallExecU3Ec__Iterator41_3__ctor_m3944863546_gshared (U3CDelayCallExecU3Ec__Iterator41_3_t4065262153 * __this, const MethodInfo* method);
#define U3CDelayCallExecU3Ec__Iterator41_3__ctor_m3944863546(__this, method) ((  void (*) (U3CDelayCallExecU3Ec__Iterator41_3_t4065262153 *, const MethodInfo*))U3CDelayCallExecU3Ec__Iterator41_3__ctor_m3944863546_gshared)(__this, method)
// System.Object BehaviourUtil/<DelayCallExec>c__Iterator41`3<System.Boolean,System.Object,System.Int32>::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDelayCallExecU3Ec__Iterator41_3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m33550168_gshared (U3CDelayCallExecU3Ec__Iterator41_3_t4065262153 * __this, const MethodInfo* method);
#define U3CDelayCallExecU3Ec__Iterator41_3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m33550168(__this, method) ((  Il2CppObject * (*) (U3CDelayCallExecU3Ec__Iterator41_3_t4065262153 *, const MethodInfo*))U3CDelayCallExecU3Ec__Iterator41_3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m33550168_gshared)(__this, method)
// System.Object BehaviourUtil/<DelayCallExec>c__Iterator41`3<System.Boolean,System.Object,System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDelayCallExecU3Ec__Iterator41_3_System_Collections_IEnumerator_get_Current_m3052118764_gshared (U3CDelayCallExecU3Ec__Iterator41_3_t4065262153 * __this, const MethodInfo* method);
#define U3CDelayCallExecU3Ec__Iterator41_3_System_Collections_IEnumerator_get_Current_m3052118764(__this, method) ((  Il2CppObject * (*) (U3CDelayCallExecU3Ec__Iterator41_3_t4065262153 *, const MethodInfo*))U3CDelayCallExecU3Ec__Iterator41_3_System_Collections_IEnumerator_get_Current_m3052118764_gshared)(__this, method)
// System.Boolean BehaviourUtil/<DelayCallExec>c__Iterator41`3<System.Boolean,System.Object,System.Int32>::MoveNext()
extern "C"  bool U3CDelayCallExecU3Ec__Iterator41_3_MoveNext_m3632558202_gshared (U3CDelayCallExecU3Ec__Iterator41_3_t4065262153 * __this, const MethodInfo* method);
#define U3CDelayCallExecU3Ec__Iterator41_3_MoveNext_m3632558202(__this, method) ((  bool (*) (U3CDelayCallExecU3Ec__Iterator41_3_t4065262153 *, const MethodInfo*))U3CDelayCallExecU3Ec__Iterator41_3_MoveNext_m3632558202_gshared)(__this, method)
// System.Void BehaviourUtil/<DelayCallExec>c__Iterator41`3<System.Boolean,System.Object,System.Int32>::Dispose()
extern "C"  void U3CDelayCallExecU3Ec__Iterator41_3_Dispose_m2750995703_gshared (U3CDelayCallExecU3Ec__Iterator41_3_t4065262153 * __this, const MethodInfo* method);
#define U3CDelayCallExecU3Ec__Iterator41_3_Dispose_m2750995703(__this, method) ((  void (*) (U3CDelayCallExecU3Ec__Iterator41_3_t4065262153 *, const MethodInfo*))U3CDelayCallExecU3Ec__Iterator41_3_Dispose_m2750995703_gshared)(__this, method)
// System.Void BehaviourUtil/<DelayCallExec>c__Iterator41`3<System.Boolean,System.Object,System.Int32>::Reset()
extern "C"  void U3CDelayCallExecU3Ec__Iterator41_3_Reset_m1591296487_gshared (U3CDelayCallExecU3Ec__Iterator41_3_t4065262153 * __this, const MethodInfo* method);
#define U3CDelayCallExecU3Ec__Iterator41_3_Reset_m1591296487(__this, method) ((  void (*) (U3CDelayCallExecU3Ec__Iterator41_3_t4065262153 *, const MethodInfo*))U3CDelayCallExecU3Ec__Iterator41_3_Reset_m1591296487_gshared)(__this, method)
