﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._e8eb5619f1d235579def6e4059a29389
struct _e8eb5619f1d235579def6e4059a29389_t2754089430;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._e8eb5619f1d235579def6e4059a29389::.ctor()
extern "C"  void _e8eb5619f1d235579def6e4059a29389__ctor_m1518519575 (_e8eb5619f1d235579def6e4059a29389_t2754089430 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._e8eb5619f1d235579def6e4059a29389::_e8eb5619f1d235579def6e4059a29389m2(System.Int32)
extern "C"  int32_t _e8eb5619f1d235579def6e4059a29389__e8eb5619f1d235579def6e4059a29389m2_m1653477849 (_e8eb5619f1d235579def6e4059a29389_t2754089430 * __this, int32_t ____e8eb5619f1d235579def6e4059a29389a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._e8eb5619f1d235579def6e4059a29389::_e8eb5619f1d235579def6e4059a29389m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _e8eb5619f1d235579def6e4059a29389__e8eb5619f1d235579def6e4059a29389m_m1022057725 (_e8eb5619f1d235579def6e4059a29389_t2754089430 * __this, int32_t ____e8eb5619f1d235579def6e4059a29389a0, int32_t ____e8eb5619f1d235579def6e4059a29389971, int32_t ____e8eb5619f1d235579def6e4059a29389c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
