﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_GUIGenerated/<GUI_Window_GetDelegate_member107_arg2>c__AnonStoreyEF
struct U3CGUI_Window_GetDelegate_member107_arg2U3Ec__AnonStoreyEF_t972967309;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine_GUIGenerated/<GUI_Window_GetDelegate_member107_arg2>c__AnonStoreyEF::.ctor()
extern "C"  void U3CGUI_Window_GetDelegate_member107_arg2U3Ec__AnonStoreyEF__ctor_m100149374 (U3CGUI_Window_GetDelegate_member107_arg2U3Ec__AnonStoreyEF_t972967309 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUIGenerated/<GUI_Window_GetDelegate_member107_arg2>c__AnonStoreyEF::<>m__1D4(System.Int32)
extern "C"  void U3CGUI_Window_GetDelegate_member107_arg2U3Ec__AnonStoreyEF_U3CU3Em__1D4_m1391070829 (U3CGUI_Window_GetDelegate_member107_arg2U3Ec__AnonStoreyEF_t972967309 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
