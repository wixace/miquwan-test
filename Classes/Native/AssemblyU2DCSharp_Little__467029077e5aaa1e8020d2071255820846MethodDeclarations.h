﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._467029077e5aaa1e8020d207976ba8d6
struct _467029077e5aaa1e8020d207976ba8d6_t1255820846;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._467029077e5aaa1e8020d207976ba8d6::.ctor()
extern "C"  void _467029077e5aaa1e8020d207976ba8d6__ctor_m1272114111 (_467029077e5aaa1e8020d207976ba8d6_t1255820846 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._467029077e5aaa1e8020d207976ba8d6::_467029077e5aaa1e8020d207976ba8d6m2(System.Int32)
extern "C"  int32_t _467029077e5aaa1e8020d207976ba8d6__467029077e5aaa1e8020d207976ba8d6m2_m1717433049 (_467029077e5aaa1e8020d207976ba8d6_t1255820846 * __this, int32_t ____467029077e5aaa1e8020d207976ba8d6a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._467029077e5aaa1e8020d207976ba8d6::_467029077e5aaa1e8020d207976ba8d6m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _467029077e5aaa1e8020d207976ba8d6__467029077e5aaa1e8020d207976ba8d6m_m3523910653 (_467029077e5aaa1e8020d207976ba8d6_t1255820846 * __this, int32_t ____467029077e5aaa1e8020d207976ba8d6a0, int32_t ____467029077e5aaa1e8020d207976ba8d6201, int32_t ____467029077e5aaa1e8020d207976ba8d6c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
