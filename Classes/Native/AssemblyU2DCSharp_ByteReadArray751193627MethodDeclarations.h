﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ByteReadArray
struct ByteReadArray_t751193627;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// System.String
struct String_t;
// System.IO.MemoryStream
struct MemoryStream_t418716369;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ByteReadArray751193627.h"

// System.Void ByteReadArray::.ctor(System.Byte[])
extern "C"  void ByteReadArray__ctor_m3364584473 (ByteReadArray_t751193627 * __this, ByteU5BU5D_t4260760469* ___buffer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ByteReadArray::ReadBoolean()
extern "C"  bool ByteReadArray_ReadBoolean_m2524357364 (ByteReadArray_t751193627 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] ByteReadArray::inverse(System.Byte[])
extern "C"  ByteU5BU5D_t4260760469* ByteReadArray_inverse_m165499137 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte ByteReadArray::ReadByte()
extern "C"  uint8_t ByteReadArray_ReadByte_m3066156742 (ByteReadArray_t751193627 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ByteReadArray::ReadBytes(System.Byte[],System.UInt32,System.Int32)
extern "C"  void ByteReadArray_ReadBytes_m3568629309 (ByteReadArray_t751193627 * __this, ByteU5BU5D_t4260760469* ___bytes0, uint32_t ___offset1, int32_t ___length2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char ByteReadArray::ReadChar()
extern "C"  Il2CppChar ByteReadArray_ReadChar_m58161762 (ByteReadArray_t751193627 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double ByteReadArray::ReadDouble()
extern "C"  double ByteReadArray_ReadDouble_m891851352 (ByteReadArray_t751193627 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single ByteReadArray::ReadFloat()
extern "C"  float ByteReadArray_ReadFloat_m4250030848 (ByteReadArray_t751193627 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ByteReadArray::ReadInt()
extern "C"  int32_t ByteReadArray_ReadInt_m3767000021 (ByteReadArray_t751193627 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 ByteReadArray::ReadUInt32()
extern "C"  uint32_t ByteReadArray_ReadUInt32_m4104555240 (ByteReadArray_t751193627 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 ByteReadArray::ReadLong()
extern "C"  int64_t ByteReadArray_ReadLong_m2058594839 (ByteReadArray_t751193627 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 ByteReadArray::ReadUlong()
extern "C"  uint64_t ByteReadArray_ReadUlong_m1181031077 (ByteReadArray_t751193627 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int16 ByteReadArray::ReadShort()
extern "C"  int16_t ByteReadArray_ReadShort_m1430394972 (ByteReadArray_t751193627 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ByteReadArray::ReadUTF()
extern "C"  String_t* ByteReadArray_ReadUTF_m1655005346 (ByteReadArray_t751193627 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ByteReadArray::ReadUTFBytes(System.UInt32)
extern "C"  String_t* ByteReadArray_ReadUTFBytes_m547003215 (ByteReadArray_t751193627 * __this, uint32_t ___length0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] ByteReadArray::get_Buffer()
extern "C"  ByteU5BU5D_t4260760469* ByteReadArray_get_Buffer_m936998195 (ByteReadArray_t751193627 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ByteReadArray::get_Length()
extern "C"  int32_t ByteReadArray_get_Length_m346074293 (ByteReadArray_t751193627 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.MemoryStream ByteReadArray::get_MemoryStream()
extern "C"  MemoryStream_t418716369 * ByteReadArray_get_MemoryStream_m3031009291 (ByteReadArray_t751193627 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ByteReadArray::get_Postion()
extern "C"  int32_t ByteReadArray_get_Postion_m4251566555 (ByteReadArray_t751193627 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ByteReadArray::set_Postion(System.Int32)
extern "C"  void ByteReadArray_set_Postion_m1799136938 (ByteReadArray_t751193627 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] ByteReadArray::ilo_inverse1(System.Byte[])
extern "C"  ByteU5BU5D_t4260760469* ByteReadArray_ilo_inverse1_m264001419 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ByteReadArray::ilo_ReadInt2(ByteReadArray)
extern "C"  int32_t ByteReadArray_ilo_ReadInt2_m1214045157 (Il2CppObject * __this /* static, unused */, ByteReadArray_t751193627 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int16 ByteReadArray::ilo_ReadShort3(ByteReadArray)
extern "C"  int16_t ByteReadArray_ilo_ReadShort3_m2946492875 (Il2CppObject * __this /* static, unused */, ByteReadArray_t751193627 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
