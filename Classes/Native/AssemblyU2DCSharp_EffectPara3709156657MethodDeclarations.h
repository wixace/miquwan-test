﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EffectPara
struct EffectPara_t3709156657;
// CombatEntity
struct CombatEntity_t684137495;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CombatEntity684137495.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void EffectPara::.ctor(System.Int32,CombatEntity,CombatEntity,UnityEngine.Vector3,System.Boolean,System.Single)
extern "C"  void EffectPara__ctor_m1769415380 (EffectPara_t3709156657 * __this, int32_t ____id0, CombatEntity_t684137495 * ____srcTf1, CombatEntity_t684137495 * ____host2, Vector3_t4282066566  ____pos3, bool ____isBlackInShow4, float ____palySpeed5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
