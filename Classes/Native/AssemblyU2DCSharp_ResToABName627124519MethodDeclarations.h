﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ResToABName
struct ResToABName_t627124519;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void ResToABName::.ctor()
extern "C"  void ResToABName__ctor_m2771955236 (ResToABName_t627124519 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ResToABName::GetABNameByLoad(System.String)
extern "C"  String_t* ResToABName_GetABNameByLoad_m2816644798 (ResToABName_t627124519 * __this, String_t* ___loadName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ResToABName::LoadPathToABFullPath(System.String)
extern "C"  String_t* ResToABName_LoadPathToABFullPath_m1944107018 (ResToABName_t627124519 * __this, String_t* ___loadName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ResToABName::IsNullAsset(System.String)
extern "C"  bool ResToABName_IsNullAsset_m3797118221 (ResToABName_t627124519 * __this, String_t* ___loadName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ResToABName::GetABNameByAssetName(System.String)
extern "C"  String_t* ResToABName_GetABNameByAssetName_m268949955 (ResToABName_t627124519 * __this, String_t* ___assetName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ResToABName::GetAssetNameByLoad(System.String)
extern "C"  String_t* ResToABName_GetAssetNameByLoad_m2961251875 (ResToABName_t627124519 * __this, String_t* ___loadName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ResToABName::GetIsCompressByABName(System.String)
extern "C"  bool ResToABName_GetIsCompressByABName_m674111495 (ResToABName_t627124519 * __this, String_t* ___abName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
