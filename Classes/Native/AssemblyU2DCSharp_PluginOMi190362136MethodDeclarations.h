﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PluginOMi
struct PluginOMi_t190362136;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// System.String
struct String_t;
// UnityEngine.WWWForm
struct WWWForm_t461342257;
// Mihua.SDK.PayInfo
struct PayInfo_t1775308120;
// CEvent.EventFunc`1<CEvent.ZEvent>
struct EventFunc_1_t1114914310;
// VersionMgr
struct VersionMgr_t1322950208;
// VersionInfo
struct VersionInfo_t2356638086;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// Mihua.SDK.SdkwwwCallBack
struct SdkwwwCallBack_t3004559384;
// Newtonsoft.Json.Linq.JToken
struct JToken_t3412245951;
// Newtonsoft.Json.Linq.JObject
struct JObject_t1798544199;
// System.Object
struct Il2CppObject;
// FloatTextMgr
struct FloatTextMgr_t630384591;
// PluginsSdkMgr
struct PluginsSdkMgr_t3884624670;
// System.Action
struct Action_t3771233898;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_Mihua_SDK_PayInfo1775308120.h"
#include "AssemblyU2DCSharp_VersionMgr1322950208.h"
#include "AssemblyU2DCSharp_PluginOMi190362136.h"
#include "UnityEngine_UnityEngine_WWWForm461342257.h"
#include "AssemblyU2DCSharp_Mihua_SDK_SdkwwwCallBack3004559384.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JObject1798544199.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_PluginsSdkMgr3884624670.h"
#include "System_Core_System_Action3771233898.h"

// System.Void PluginOMi::.ctor()
extern "C"  void PluginOMi__ctor_m2890743379 (PluginOMi_t190362136 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginOMi::Init()
extern "C"  void PluginOMi_Init_m1845901729 (PluginOMi_t190362136 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginOMi::ReqSDKHttpLogin()
extern "C"  Dictionary_2_t827649927 * PluginOMi_ReqSDKHttpLogin_m1613771652 (PluginOMi_t190362136 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginOMi::IsLoginSuccess(System.Boolean)
extern "C"  bool PluginOMi_IsLoginSuccess_m2053263032 (PluginOMi_t190362136 * __this, bool ___isOpenUseLogin0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginOMi::OpenUserLogin()
extern "C"  void PluginOMi_OpenUserLogin_m3994574245 (PluginOMi_t190362136 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginOMi::UserPay(CEvent.ZEvent)
extern "C"  void PluginOMi_UserPay_m498762285 (PluginOMi_t190362136 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginOMi::SignCallBack(System.Boolean,System.String)
extern "C"  void PluginOMi_SignCallBack_m2750144582 (PluginOMi_t190362136 * __this, bool ___success0, String_t* ___arg1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.WWWForm PluginOMi::BuildOrderParam2WWWForm(Mihua.SDK.PayInfo)
extern "C"  WWWForm_t461342257 * PluginOMi_BuildOrderParam2WWWForm_m3405124400 (PluginOMi_t190362136 * __this, PayInfo_t1775308120 * ___info0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginOMi::OnLoginSuccess(System.String)
extern "C"  void PluginOMi_OnLoginSuccess_m2187158808 (PluginOMi_t190362136 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginOMi::OnLogoutSuccess(System.String)
extern "C"  void PluginOMi_OnLogoutSuccess_m3380956279 (PluginOMi_t190362136 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginOMi::OnLogout(System.String)
extern "C"  void PluginOMi_OnLogout_m20650664 (PluginOMi_t190362136 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginOMi::initSdk(System.String)
extern "C"  void PluginOMi_initSdk_m4040069319 (PluginOMi_t190362136 * __this, String_t* ___appid0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginOMi::login()
extern "C"  void PluginOMi_login_m2412758202 (PluginOMi_t190362136 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginOMi::logout()
extern "C"  void PluginOMi_logout_m1786882939 (PluginOMi_t190362136 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginOMi::pay(System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginOMi_pay_m1029030357 (PluginOMi_t190362136 * __this, String_t* ___num0, String_t* ___productid1, String_t* ___productname2, String_t* ___oderid3, String_t* ___paybackurl4, String_t* ___price5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginOMi::<OnLogoutSuccess>m__445()
extern "C"  void PluginOMi_U3COnLogoutSuccessU3Em__445_m3306608505 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginOMi::<OnLogout>m__446()
extern "C"  void PluginOMi_U3COnLogoutU3Em__446_m1165814515 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginOMi::ilo_AddEventListener1(System.String,CEvent.EventFunc`1<CEvent.ZEvent>)
extern "C"  void PluginOMi_ilo_AddEventListener1_m835400769 (Il2CppObject * __this /* static, unused */, String_t* ___eventType0, EventFunc_1_t1114914310 * ___fun1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionMgr PluginOMi::ilo_get_Instance2()
extern "C"  VersionMgr_t1322950208 * PluginOMi_ilo_get_Instance2_m1784443119 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionInfo PluginOMi::ilo_get_currentVS3(VersionMgr)
extern "C"  VersionInfo_t2356638086 * PluginOMi_ilo_get_currentVS3_m3573491965 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginOMi::ilo_get_isSdkLogin4(VersionMgr)
extern "C"  bool PluginOMi_ilo_get_isSdkLogin4_m2472641860 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mihua.SDK.PayInfo PluginOMi::ilo_Parse5(System.Object[])
extern "C"  PayInfo_t1775308120 * PluginOMi_ilo_Parse5_m2919911187 (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t1108656482* ___payOjs0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.WWWForm PluginOMi::ilo_BuildOrderParam2WWWForm6(PluginOMi,Mihua.SDK.PayInfo)
extern "C"  WWWForm_t461342257 * PluginOMi_ilo_BuildOrderParam2WWWForm6_m3665988805 (Il2CppObject * __this /* static, unused */, PluginOMi_t190362136 * ____this0, PayInfo_t1775308120 * ___info1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginOMi::ilo_Request7(System.String,UnityEngine.WWWForm,Mihua.SDK.SdkwwwCallBack)
extern "C"  void PluginOMi_ilo_Request7_m3693878140 (Il2CppObject * __this /* static, unused */, String_t* ___url0, WWWForm_t461342257 * ___wwwForm1, SdkwwwCallBack_t3004559384 * ___callBack2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken PluginOMi::ilo_get_Item8(Newtonsoft.Json.Linq.JObject,System.String)
extern "C"  JToken_t3412245951 * PluginOMi_ilo_get_Item8_m4022227185 (Il2CppObject * __this /* static, unused */, JObject_t1798544199 * ____this0, String_t* ___propertyName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginOMi::ilo_Log9(System.Object,System.Boolean)
extern "C"  void PluginOMi_ilo_Log9_m2520539298 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FloatTextMgr PluginOMi::ilo_get_FloatTextMgr10()
extern "C"  FloatTextMgr_t630384591 * PluginOMi_ilo_get_FloatTextMgr10_m3735780493 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PluginsSdkMgr PluginOMi::ilo_get_PluginsSdkMgr11()
extern "C"  PluginsSdkMgr_t3884624670 * PluginOMi_ilo_get_PluginsSdkMgr11_m2968929224 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginOMi::ilo_ReqSDKHttpLogin12(PluginsSdkMgr)
extern "C"  void PluginOMi_ilo_ReqSDKHttpLogin12_m819253936 (Il2CppObject * __this /* static, unused */, PluginsSdkMgr_t3884624670 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginOMi::ilo_Logout13(System.Action)
extern "C"  void PluginOMi_ilo_Logout13_m129538637 (Il2CppObject * __this /* static, unused */, Action_t3771233898 * ___call0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
