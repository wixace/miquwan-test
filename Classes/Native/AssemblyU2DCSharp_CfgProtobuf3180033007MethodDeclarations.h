﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Linq.JObject
struct JObject_t1798544199;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// Pomelo.Protobuf.MsgDecoder
struct MsgDecoder_t242483159;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JObject1798544199.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_Pomelo_Protobuf_MsgDecoder242483159.h"

// System.Void CfgProtobuf::InitDecoder(Newtonsoft.Json.Linq.JObject)
extern "C"  void CfgProtobuf_InitDecoder_m1392298238 (Il2CppObject * __this /* static, unused */, JObject_t1798544199 * ___decodeProtos0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JObject CfgProtobuf::decode(System.String,System.Byte[])
extern "C"  JObject_t1798544199 * CfgProtobuf_decode_m4080460476 (Il2CppObject * __this /* static, unused */, String_t* ___route0, ByteU5BU5D_t4260760469* ___buffer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JObject CfgProtobuf::ilo_decode1(Pomelo.Protobuf.MsgDecoder,System.String,System.Byte[])
extern "C"  JObject_t1798544199 * CfgProtobuf_ilo_decode1_m451873508 (Il2CppObject * __this /* static, unused */, MsgDecoder_t242483159 * ____this0, String_t* ___route1, ByteU5BU5D_t4260760469* ___buf2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
