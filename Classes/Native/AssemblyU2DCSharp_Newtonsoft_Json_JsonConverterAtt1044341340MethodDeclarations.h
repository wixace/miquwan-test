﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.JsonConverterAttribute
struct JsonConverterAttribute_t1044341340;
// System.Type
struct Type_t;
// Newtonsoft.Json.JsonConverter
struct JsonConverter_t2159686854;
// System.String
struct String_t;
// System.IFormatProvider
struct IFormatProvider_t192740775;
// System.Object[]
struct ObjectU5BU5D_t1108656482;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Type2863145774.h"
#include "mscorlib_System_String7231557.h"

// System.Void Newtonsoft.Json.JsonConverterAttribute::.ctor(System.Type)
extern "C"  void JsonConverterAttribute__ctor_m3522878775 (JsonConverterAttribute_t1044341340 * __this, Type_t * ___converterType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Newtonsoft.Json.JsonConverterAttribute::get_ConverterType()
extern "C"  Type_t * JsonConverterAttribute_get_ConverterType_m2656934871 (JsonConverterAttribute_t1044341340 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.JsonConverter Newtonsoft.Json.JsonConverterAttribute::CreateJsonConverterInstance(System.Type)
extern "C"  JsonConverter_t2159686854 * JsonConverterAttribute_CreateJsonConverterInstance_m158206104 (Il2CppObject * __this /* static, unused */, Type_t * ___converterType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.JsonConverterAttribute::ilo_FormatWith1(System.String,System.IFormatProvider,System.Object[])
extern "C"  String_t* JsonConverterAttribute_ilo_FormatWith1_m2821931996 (Il2CppObject * __this /* static, unused */, String_t* ___format0, Il2CppObject * ___provider1, ObjectU5BU5D_t1108656482* ___args2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
