﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._f66865a0df2861cb87ed16998107282e
struct _f66865a0df2861cb87ed16998107282e_t2169008919;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__f66865a0df2861cb87ed16992169008919.h"

// System.Void Little._f66865a0df2861cb87ed16998107282e::.ctor()
extern "C"  void _f66865a0df2861cb87ed16998107282e__ctor_m468008182 (_f66865a0df2861cb87ed16998107282e_t2169008919 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._f66865a0df2861cb87ed16998107282e::_f66865a0df2861cb87ed16998107282em2(System.Int32)
extern "C"  int32_t _f66865a0df2861cb87ed16998107282e__f66865a0df2861cb87ed16998107282em2_m3712680505 (_f66865a0df2861cb87ed16998107282e_t2169008919 * __this, int32_t ____f66865a0df2861cb87ed16998107282ea0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._f66865a0df2861cb87ed16998107282e::_f66865a0df2861cb87ed16998107282em(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _f66865a0df2861cb87ed16998107282e__f66865a0df2861cb87ed16998107282em_m2098960541 (_f66865a0df2861cb87ed16998107282e_t2169008919 * __this, int32_t ____f66865a0df2861cb87ed16998107282ea0, int32_t ____f66865a0df2861cb87ed16998107282e201, int32_t ____f66865a0df2861cb87ed16998107282ec2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._f66865a0df2861cb87ed16998107282e::ilo__f66865a0df2861cb87ed16998107282em21(Little._f66865a0df2861cb87ed16998107282e,System.Int32)
extern "C"  int32_t _f66865a0df2861cb87ed16998107282e_ilo__f66865a0df2861cb87ed16998107282em21_m132399294 (Il2CppObject * __this /* static, unused */, _f66865a0df2861cb87ed16998107282e_t2169008919 * ____this0, int32_t ____f66865a0df2861cb87ed16998107282ea1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
