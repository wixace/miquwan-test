﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnitStateBase
struct UnitStateBase_t1040218558;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_UnitStateBase1040218558.h"

// System.Void UnitStateBase::.ctor()
extern "C"  void UnitStateBase__ctor_m1840512365 (UnitStateBase_t1040218558 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnitStateBase::CanEnterOtherState(UnitStateBase)
extern "C"  bool UnitStateBase_CanEnterOtherState_m2016764552 (UnitStateBase_t1040218558 * __this, UnitStateBase_t1040218558 * ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnitStateBase::NeedRemoveOtherState(UnitStateBase)
extern "C"  bool UnitStateBase_NeedRemoveOtherState_m1309396854 (UnitStateBase_t1040218558 * __this, UnitStateBase_t1040218558 * ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnitStateBase::Update(System.Single)
extern "C"  void UnitStateBase_Update_m501794411 (UnitStateBase_t1040218558 * __this, float ___deltatime0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnitStateBase::IsFinished()
extern "C"  bool UnitStateBase_IsFinished_m102725439 (UnitStateBase_t1040218558 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
