﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_faca1
struct  M_faca1_t82178104  : public Il2CppObject
{
public:
	// System.Single GarbageiOS.M_faca1::_joudirGojeqe
	float ____joudirGojeqe_0;
	// System.Int32 GarbageiOS.M_faca1::_stolere
	int32_t ____stolere_1;
	// System.UInt32 GarbageiOS.M_faca1::_mertearmirReakunea
	uint32_t ____mertearmirReakunea_2;
	// System.Boolean GarbageiOS.M_faca1::_gehisTouka
	bool ____gehisTouka_3;
	// System.Single GarbageiOS.M_faca1::_leretereHallpa
	float ____leretereHallpa_4;
	// System.Int32 GarbageiOS.M_faca1::_haylobearSearmal
	int32_t ____haylobearSearmal_5;
	// System.String GarbageiOS.M_faca1::_curmepouPukeca
	String_t* ____curmepouPukeca_6;
	// System.String GarbageiOS.M_faca1::_miskirSairyea
	String_t* ____miskirSairyea_7;
	// System.UInt32 GarbageiOS.M_faca1::_lalwhas
	uint32_t ____lalwhas_8;
	// System.String GarbageiOS.M_faca1::_cemini
	String_t* ____cemini_9;
	// System.Single GarbageiOS.M_faca1::_stoohi
	float ____stoohi_10;
	// System.Single GarbageiOS.M_faca1::_vanoodas
	float ____vanoodas_11;
	// System.Int32 GarbageiOS.M_faca1::_trafairKemstairou
	int32_t ____trafairKemstairou_12;
	// System.String GarbageiOS.M_faca1::_curnoZoulay
	String_t* ____curnoZoulay_13;
	// System.Single GarbageiOS.M_faca1::_wewhargoHorba
	float ____wewhargoHorba_14;
	// System.Single GarbageiOS.M_faca1::_rowhelbereNeelede
	float ____rowhelbereNeelede_15;
	// System.UInt32 GarbageiOS.M_faca1::_hatelkere
	uint32_t ____hatelkere_16;
	// System.String GarbageiOS.M_faca1::_trayalpuRousas
	String_t* ____trayalpuRousas_17;
	// System.Int32 GarbageiOS.M_faca1::_caynarjo
	int32_t ____caynarjo_18;
	// System.UInt32 GarbageiOS.M_faca1::_jurhoutarSezousem
	uint32_t ____jurhoutarSezousem_19;
	// System.String GarbageiOS.M_faca1::_keasurdem
	String_t* ____keasurdem_20;
	// System.Int32 GarbageiOS.M_faca1::_sevarwereBefaitear
	int32_t ____sevarwereBefaitear_21;
	// System.Single GarbageiOS.M_faca1::_jasyiJoustis
	float ____jasyiJoustis_22;
	// System.Single GarbageiOS.M_faca1::_pismasdereTixalsel
	float ____pismasdereTixalsel_23;
	// System.Boolean GarbageiOS.M_faca1::_xonearjoNalroo
	bool ____xonearjoNalroo_24;
	// System.Int32 GarbageiOS.M_faca1::_cowlaPejerhaw
	int32_t ____cowlaPejerhaw_25;
	// System.Boolean GarbageiOS.M_faca1::_vurtrar
	bool ____vurtrar_26;
	// System.Int32 GarbageiOS.M_faca1::_surjas
	int32_t ____surjas_27;
	// System.Boolean GarbageiOS.M_faca1::_jadimoo
	bool ____jadimoo_28;
	// System.Boolean GarbageiOS.M_faca1::_vedawSawpalsem
	bool ____vedawSawpalsem_29;
	// System.UInt32 GarbageiOS.M_faca1::_rearnigeeLutuhi
	uint32_t ____rearnigeeLutuhi_30;
	// System.String GarbageiOS.M_faca1::_morcorkuHemwhas
	String_t* ____morcorkuHemwhas_31;
	// System.UInt32 GarbageiOS.M_faca1::_birterTurir
	uint32_t ____birterTurir_32;
	// System.Int32 GarbageiOS.M_faca1::_delsemGoopeabi
	int32_t ____delsemGoopeabi_33;
	// System.Single GarbageiOS.M_faca1::_mismirpas
	float ____mismirpas_34;
	// System.Single GarbageiOS.M_faca1::_lirmepow
	float ____lirmepow_35;
	// System.Boolean GarbageiOS.M_faca1::_xootiKawdee
	bool ____xootiKawdee_36;
	// System.Int32 GarbageiOS.M_faca1::_staballrouBamar
	int32_t ____staballrouBamar_37;

public:
	inline static int32_t get_offset_of__joudirGojeqe_0() { return static_cast<int32_t>(offsetof(M_faca1_t82178104, ____joudirGojeqe_0)); }
	inline float get__joudirGojeqe_0() const { return ____joudirGojeqe_0; }
	inline float* get_address_of__joudirGojeqe_0() { return &____joudirGojeqe_0; }
	inline void set__joudirGojeqe_0(float value)
	{
		____joudirGojeqe_0 = value;
	}

	inline static int32_t get_offset_of__stolere_1() { return static_cast<int32_t>(offsetof(M_faca1_t82178104, ____stolere_1)); }
	inline int32_t get__stolere_1() const { return ____stolere_1; }
	inline int32_t* get_address_of__stolere_1() { return &____stolere_1; }
	inline void set__stolere_1(int32_t value)
	{
		____stolere_1 = value;
	}

	inline static int32_t get_offset_of__mertearmirReakunea_2() { return static_cast<int32_t>(offsetof(M_faca1_t82178104, ____mertearmirReakunea_2)); }
	inline uint32_t get__mertearmirReakunea_2() const { return ____mertearmirReakunea_2; }
	inline uint32_t* get_address_of__mertearmirReakunea_2() { return &____mertearmirReakunea_2; }
	inline void set__mertearmirReakunea_2(uint32_t value)
	{
		____mertearmirReakunea_2 = value;
	}

	inline static int32_t get_offset_of__gehisTouka_3() { return static_cast<int32_t>(offsetof(M_faca1_t82178104, ____gehisTouka_3)); }
	inline bool get__gehisTouka_3() const { return ____gehisTouka_3; }
	inline bool* get_address_of__gehisTouka_3() { return &____gehisTouka_3; }
	inline void set__gehisTouka_3(bool value)
	{
		____gehisTouka_3 = value;
	}

	inline static int32_t get_offset_of__leretereHallpa_4() { return static_cast<int32_t>(offsetof(M_faca1_t82178104, ____leretereHallpa_4)); }
	inline float get__leretereHallpa_4() const { return ____leretereHallpa_4; }
	inline float* get_address_of__leretereHallpa_4() { return &____leretereHallpa_4; }
	inline void set__leretereHallpa_4(float value)
	{
		____leretereHallpa_4 = value;
	}

	inline static int32_t get_offset_of__haylobearSearmal_5() { return static_cast<int32_t>(offsetof(M_faca1_t82178104, ____haylobearSearmal_5)); }
	inline int32_t get__haylobearSearmal_5() const { return ____haylobearSearmal_5; }
	inline int32_t* get_address_of__haylobearSearmal_5() { return &____haylobearSearmal_5; }
	inline void set__haylobearSearmal_5(int32_t value)
	{
		____haylobearSearmal_5 = value;
	}

	inline static int32_t get_offset_of__curmepouPukeca_6() { return static_cast<int32_t>(offsetof(M_faca1_t82178104, ____curmepouPukeca_6)); }
	inline String_t* get__curmepouPukeca_6() const { return ____curmepouPukeca_6; }
	inline String_t** get_address_of__curmepouPukeca_6() { return &____curmepouPukeca_6; }
	inline void set__curmepouPukeca_6(String_t* value)
	{
		____curmepouPukeca_6 = value;
		Il2CppCodeGenWriteBarrier(&____curmepouPukeca_6, value);
	}

	inline static int32_t get_offset_of__miskirSairyea_7() { return static_cast<int32_t>(offsetof(M_faca1_t82178104, ____miskirSairyea_7)); }
	inline String_t* get__miskirSairyea_7() const { return ____miskirSairyea_7; }
	inline String_t** get_address_of__miskirSairyea_7() { return &____miskirSairyea_7; }
	inline void set__miskirSairyea_7(String_t* value)
	{
		____miskirSairyea_7 = value;
		Il2CppCodeGenWriteBarrier(&____miskirSairyea_7, value);
	}

	inline static int32_t get_offset_of__lalwhas_8() { return static_cast<int32_t>(offsetof(M_faca1_t82178104, ____lalwhas_8)); }
	inline uint32_t get__lalwhas_8() const { return ____lalwhas_8; }
	inline uint32_t* get_address_of__lalwhas_8() { return &____lalwhas_8; }
	inline void set__lalwhas_8(uint32_t value)
	{
		____lalwhas_8 = value;
	}

	inline static int32_t get_offset_of__cemini_9() { return static_cast<int32_t>(offsetof(M_faca1_t82178104, ____cemini_9)); }
	inline String_t* get__cemini_9() const { return ____cemini_9; }
	inline String_t** get_address_of__cemini_9() { return &____cemini_9; }
	inline void set__cemini_9(String_t* value)
	{
		____cemini_9 = value;
		Il2CppCodeGenWriteBarrier(&____cemini_9, value);
	}

	inline static int32_t get_offset_of__stoohi_10() { return static_cast<int32_t>(offsetof(M_faca1_t82178104, ____stoohi_10)); }
	inline float get__stoohi_10() const { return ____stoohi_10; }
	inline float* get_address_of__stoohi_10() { return &____stoohi_10; }
	inline void set__stoohi_10(float value)
	{
		____stoohi_10 = value;
	}

	inline static int32_t get_offset_of__vanoodas_11() { return static_cast<int32_t>(offsetof(M_faca1_t82178104, ____vanoodas_11)); }
	inline float get__vanoodas_11() const { return ____vanoodas_11; }
	inline float* get_address_of__vanoodas_11() { return &____vanoodas_11; }
	inline void set__vanoodas_11(float value)
	{
		____vanoodas_11 = value;
	}

	inline static int32_t get_offset_of__trafairKemstairou_12() { return static_cast<int32_t>(offsetof(M_faca1_t82178104, ____trafairKemstairou_12)); }
	inline int32_t get__trafairKemstairou_12() const { return ____trafairKemstairou_12; }
	inline int32_t* get_address_of__trafairKemstairou_12() { return &____trafairKemstairou_12; }
	inline void set__trafairKemstairou_12(int32_t value)
	{
		____trafairKemstairou_12 = value;
	}

	inline static int32_t get_offset_of__curnoZoulay_13() { return static_cast<int32_t>(offsetof(M_faca1_t82178104, ____curnoZoulay_13)); }
	inline String_t* get__curnoZoulay_13() const { return ____curnoZoulay_13; }
	inline String_t** get_address_of__curnoZoulay_13() { return &____curnoZoulay_13; }
	inline void set__curnoZoulay_13(String_t* value)
	{
		____curnoZoulay_13 = value;
		Il2CppCodeGenWriteBarrier(&____curnoZoulay_13, value);
	}

	inline static int32_t get_offset_of__wewhargoHorba_14() { return static_cast<int32_t>(offsetof(M_faca1_t82178104, ____wewhargoHorba_14)); }
	inline float get__wewhargoHorba_14() const { return ____wewhargoHorba_14; }
	inline float* get_address_of__wewhargoHorba_14() { return &____wewhargoHorba_14; }
	inline void set__wewhargoHorba_14(float value)
	{
		____wewhargoHorba_14 = value;
	}

	inline static int32_t get_offset_of__rowhelbereNeelede_15() { return static_cast<int32_t>(offsetof(M_faca1_t82178104, ____rowhelbereNeelede_15)); }
	inline float get__rowhelbereNeelede_15() const { return ____rowhelbereNeelede_15; }
	inline float* get_address_of__rowhelbereNeelede_15() { return &____rowhelbereNeelede_15; }
	inline void set__rowhelbereNeelede_15(float value)
	{
		____rowhelbereNeelede_15 = value;
	}

	inline static int32_t get_offset_of__hatelkere_16() { return static_cast<int32_t>(offsetof(M_faca1_t82178104, ____hatelkere_16)); }
	inline uint32_t get__hatelkere_16() const { return ____hatelkere_16; }
	inline uint32_t* get_address_of__hatelkere_16() { return &____hatelkere_16; }
	inline void set__hatelkere_16(uint32_t value)
	{
		____hatelkere_16 = value;
	}

	inline static int32_t get_offset_of__trayalpuRousas_17() { return static_cast<int32_t>(offsetof(M_faca1_t82178104, ____trayalpuRousas_17)); }
	inline String_t* get__trayalpuRousas_17() const { return ____trayalpuRousas_17; }
	inline String_t** get_address_of__trayalpuRousas_17() { return &____trayalpuRousas_17; }
	inline void set__trayalpuRousas_17(String_t* value)
	{
		____trayalpuRousas_17 = value;
		Il2CppCodeGenWriteBarrier(&____trayalpuRousas_17, value);
	}

	inline static int32_t get_offset_of__caynarjo_18() { return static_cast<int32_t>(offsetof(M_faca1_t82178104, ____caynarjo_18)); }
	inline int32_t get__caynarjo_18() const { return ____caynarjo_18; }
	inline int32_t* get_address_of__caynarjo_18() { return &____caynarjo_18; }
	inline void set__caynarjo_18(int32_t value)
	{
		____caynarjo_18 = value;
	}

	inline static int32_t get_offset_of__jurhoutarSezousem_19() { return static_cast<int32_t>(offsetof(M_faca1_t82178104, ____jurhoutarSezousem_19)); }
	inline uint32_t get__jurhoutarSezousem_19() const { return ____jurhoutarSezousem_19; }
	inline uint32_t* get_address_of__jurhoutarSezousem_19() { return &____jurhoutarSezousem_19; }
	inline void set__jurhoutarSezousem_19(uint32_t value)
	{
		____jurhoutarSezousem_19 = value;
	}

	inline static int32_t get_offset_of__keasurdem_20() { return static_cast<int32_t>(offsetof(M_faca1_t82178104, ____keasurdem_20)); }
	inline String_t* get__keasurdem_20() const { return ____keasurdem_20; }
	inline String_t** get_address_of__keasurdem_20() { return &____keasurdem_20; }
	inline void set__keasurdem_20(String_t* value)
	{
		____keasurdem_20 = value;
		Il2CppCodeGenWriteBarrier(&____keasurdem_20, value);
	}

	inline static int32_t get_offset_of__sevarwereBefaitear_21() { return static_cast<int32_t>(offsetof(M_faca1_t82178104, ____sevarwereBefaitear_21)); }
	inline int32_t get__sevarwereBefaitear_21() const { return ____sevarwereBefaitear_21; }
	inline int32_t* get_address_of__sevarwereBefaitear_21() { return &____sevarwereBefaitear_21; }
	inline void set__sevarwereBefaitear_21(int32_t value)
	{
		____sevarwereBefaitear_21 = value;
	}

	inline static int32_t get_offset_of__jasyiJoustis_22() { return static_cast<int32_t>(offsetof(M_faca1_t82178104, ____jasyiJoustis_22)); }
	inline float get__jasyiJoustis_22() const { return ____jasyiJoustis_22; }
	inline float* get_address_of__jasyiJoustis_22() { return &____jasyiJoustis_22; }
	inline void set__jasyiJoustis_22(float value)
	{
		____jasyiJoustis_22 = value;
	}

	inline static int32_t get_offset_of__pismasdereTixalsel_23() { return static_cast<int32_t>(offsetof(M_faca1_t82178104, ____pismasdereTixalsel_23)); }
	inline float get__pismasdereTixalsel_23() const { return ____pismasdereTixalsel_23; }
	inline float* get_address_of__pismasdereTixalsel_23() { return &____pismasdereTixalsel_23; }
	inline void set__pismasdereTixalsel_23(float value)
	{
		____pismasdereTixalsel_23 = value;
	}

	inline static int32_t get_offset_of__xonearjoNalroo_24() { return static_cast<int32_t>(offsetof(M_faca1_t82178104, ____xonearjoNalroo_24)); }
	inline bool get__xonearjoNalroo_24() const { return ____xonearjoNalroo_24; }
	inline bool* get_address_of__xonearjoNalroo_24() { return &____xonearjoNalroo_24; }
	inline void set__xonearjoNalroo_24(bool value)
	{
		____xonearjoNalroo_24 = value;
	}

	inline static int32_t get_offset_of__cowlaPejerhaw_25() { return static_cast<int32_t>(offsetof(M_faca1_t82178104, ____cowlaPejerhaw_25)); }
	inline int32_t get__cowlaPejerhaw_25() const { return ____cowlaPejerhaw_25; }
	inline int32_t* get_address_of__cowlaPejerhaw_25() { return &____cowlaPejerhaw_25; }
	inline void set__cowlaPejerhaw_25(int32_t value)
	{
		____cowlaPejerhaw_25 = value;
	}

	inline static int32_t get_offset_of__vurtrar_26() { return static_cast<int32_t>(offsetof(M_faca1_t82178104, ____vurtrar_26)); }
	inline bool get__vurtrar_26() const { return ____vurtrar_26; }
	inline bool* get_address_of__vurtrar_26() { return &____vurtrar_26; }
	inline void set__vurtrar_26(bool value)
	{
		____vurtrar_26 = value;
	}

	inline static int32_t get_offset_of__surjas_27() { return static_cast<int32_t>(offsetof(M_faca1_t82178104, ____surjas_27)); }
	inline int32_t get__surjas_27() const { return ____surjas_27; }
	inline int32_t* get_address_of__surjas_27() { return &____surjas_27; }
	inline void set__surjas_27(int32_t value)
	{
		____surjas_27 = value;
	}

	inline static int32_t get_offset_of__jadimoo_28() { return static_cast<int32_t>(offsetof(M_faca1_t82178104, ____jadimoo_28)); }
	inline bool get__jadimoo_28() const { return ____jadimoo_28; }
	inline bool* get_address_of__jadimoo_28() { return &____jadimoo_28; }
	inline void set__jadimoo_28(bool value)
	{
		____jadimoo_28 = value;
	}

	inline static int32_t get_offset_of__vedawSawpalsem_29() { return static_cast<int32_t>(offsetof(M_faca1_t82178104, ____vedawSawpalsem_29)); }
	inline bool get__vedawSawpalsem_29() const { return ____vedawSawpalsem_29; }
	inline bool* get_address_of__vedawSawpalsem_29() { return &____vedawSawpalsem_29; }
	inline void set__vedawSawpalsem_29(bool value)
	{
		____vedawSawpalsem_29 = value;
	}

	inline static int32_t get_offset_of__rearnigeeLutuhi_30() { return static_cast<int32_t>(offsetof(M_faca1_t82178104, ____rearnigeeLutuhi_30)); }
	inline uint32_t get__rearnigeeLutuhi_30() const { return ____rearnigeeLutuhi_30; }
	inline uint32_t* get_address_of__rearnigeeLutuhi_30() { return &____rearnigeeLutuhi_30; }
	inline void set__rearnigeeLutuhi_30(uint32_t value)
	{
		____rearnigeeLutuhi_30 = value;
	}

	inline static int32_t get_offset_of__morcorkuHemwhas_31() { return static_cast<int32_t>(offsetof(M_faca1_t82178104, ____morcorkuHemwhas_31)); }
	inline String_t* get__morcorkuHemwhas_31() const { return ____morcorkuHemwhas_31; }
	inline String_t** get_address_of__morcorkuHemwhas_31() { return &____morcorkuHemwhas_31; }
	inline void set__morcorkuHemwhas_31(String_t* value)
	{
		____morcorkuHemwhas_31 = value;
		Il2CppCodeGenWriteBarrier(&____morcorkuHemwhas_31, value);
	}

	inline static int32_t get_offset_of__birterTurir_32() { return static_cast<int32_t>(offsetof(M_faca1_t82178104, ____birterTurir_32)); }
	inline uint32_t get__birterTurir_32() const { return ____birterTurir_32; }
	inline uint32_t* get_address_of__birterTurir_32() { return &____birterTurir_32; }
	inline void set__birterTurir_32(uint32_t value)
	{
		____birterTurir_32 = value;
	}

	inline static int32_t get_offset_of__delsemGoopeabi_33() { return static_cast<int32_t>(offsetof(M_faca1_t82178104, ____delsemGoopeabi_33)); }
	inline int32_t get__delsemGoopeabi_33() const { return ____delsemGoopeabi_33; }
	inline int32_t* get_address_of__delsemGoopeabi_33() { return &____delsemGoopeabi_33; }
	inline void set__delsemGoopeabi_33(int32_t value)
	{
		____delsemGoopeabi_33 = value;
	}

	inline static int32_t get_offset_of__mismirpas_34() { return static_cast<int32_t>(offsetof(M_faca1_t82178104, ____mismirpas_34)); }
	inline float get__mismirpas_34() const { return ____mismirpas_34; }
	inline float* get_address_of__mismirpas_34() { return &____mismirpas_34; }
	inline void set__mismirpas_34(float value)
	{
		____mismirpas_34 = value;
	}

	inline static int32_t get_offset_of__lirmepow_35() { return static_cast<int32_t>(offsetof(M_faca1_t82178104, ____lirmepow_35)); }
	inline float get__lirmepow_35() const { return ____lirmepow_35; }
	inline float* get_address_of__lirmepow_35() { return &____lirmepow_35; }
	inline void set__lirmepow_35(float value)
	{
		____lirmepow_35 = value;
	}

	inline static int32_t get_offset_of__xootiKawdee_36() { return static_cast<int32_t>(offsetof(M_faca1_t82178104, ____xootiKawdee_36)); }
	inline bool get__xootiKawdee_36() const { return ____xootiKawdee_36; }
	inline bool* get_address_of__xootiKawdee_36() { return &____xootiKawdee_36; }
	inline void set__xootiKawdee_36(bool value)
	{
		____xootiKawdee_36 = value;
	}

	inline static int32_t get_offset_of__staballrouBamar_37() { return static_cast<int32_t>(offsetof(M_faca1_t82178104, ____staballrouBamar_37)); }
	inline int32_t get__staballrouBamar_37() const { return ____staballrouBamar_37; }
	inline int32_t* get_address_of__staballrouBamar_37() { return &____staballrouBamar_37; }
	inline void set__staballrouBamar_37(int32_t value)
	{
		____staballrouBamar_37 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
