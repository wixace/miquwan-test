﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Vector3[]
struct Vector3U5BU5D_t215400611;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// Pathfinding.Voxels.ExtraMesh
struct ExtraMesh_t4218029715;
struct ExtraMesh_t4218029715_marshaled_pinvoke;
struct ExtraMesh_t4218029715_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_Voxels_ExtraMesh4218029715.h"
#include "UnityEngine_UnityEngine_Bounds2711641849.h"
#include "UnityEngine_UnityEngine_Matrix4x41651859333.h"

// System.Void Pathfinding.Voxels.ExtraMesh::.ctor(UnityEngine.Vector3[],System.Int32[],UnityEngine.Bounds)
extern "C"  void ExtraMesh__ctor_m70088103 (ExtraMesh_t4218029715 * __this, Vector3U5BU5D_t215400611* ___v0, Int32U5BU5D_t3230847821* ___t1, Bounds_t2711641849  ___b2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Voxels.ExtraMesh::.ctor(UnityEngine.Vector3[],System.Int32[],UnityEngine.Bounds,UnityEngine.Matrix4x4)
extern "C"  void ExtraMesh__ctor_m3535201819 (ExtraMesh_t4218029715 * __this, Vector3U5BU5D_t215400611* ___v0, Int32U5BU5D_t3230847821* ___t1, Bounds_t2711641849  ___b2, Matrix4x4_t1651859333  ___matrix3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Voxels.ExtraMesh::RecalculateBounds()
extern "C"  void ExtraMesh_RecalculateBounds_m1757351611 (ExtraMesh_t4218029715 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct ExtraMesh_t4218029715;
struct ExtraMesh_t4218029715_marshaled_pinvoke;

extern "C" void ExtraMesh_t4218029715_marshal_pinvoke(const ExtraMesh_t4218029715& unmarshaled, ExtraMesh_t4218029715_marshaled_pinvoke& marshaled);
extern "C" void ExtraMesh_t4218029715_marshal_pinvoke_back(const ExtraMesh_t4218029715_marshaled_pinvoke& marshaled, ExtraMesh_t4218029715& unmarshaled);
extern "C" void ExtraMesh_t4218029715_marshal_pinvoke_cleanup(ExtraMesh_t4218029715_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct ExtraMesh_t4218029715;
struct ExtraMesh_t4218029715_marshaled_com;

extern "C" void ExtraMesh_t4218029715_marshal_com(const ExtraMesh_t4218029715& unmarshaled, ExtraMesh_t4218029715_marshaled_com& marshaled);
extern "C" void ExtraMesh_t4218029715_marshal_com_back(const ExtraMesh_t4218029715_marshaled_com& marshaled, ExtraMesh_t4218029715& unmarshaled);
extern "C" void ExtraMesh_t4218029715_marshal_com_cleanup(ExtraMesh_t4218029715_marshaled_com& marshaled);
