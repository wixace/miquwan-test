﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_RaycastHit2DGenerated
struct UnityEngine_RaycastHit2DGenerated_t2852882507;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"

// System.Void UnityEngine_RaycastHit2DGenerated::.ctor()
extern "C"  void UnityEngine_RaycastHit2DGenerated__ctor_m2867755392 (UnityEngine_RaycastHit2DGenerated_t2852882507 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RaycastHit2DGenerated::.cctor()
extern "C"  void UnityEngine_RaycastHit2DGenerated__cctor_m2518975021 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RaycastHit2DGenerated::RaycastHit2D_RaycastHit2D1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RaycastHit2DGenerated_RaycastHit2D_RaycastHit2D1_m1475540978 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RaycastHit2DGenerated::RaycastHit2D_centroid(JSVCall)
extern "C"  void UnityEngine_RaycastHit2DGenerated_RaycastHit2D_centroid_m2412299334 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RaycastHit2DGenerated::RaycastHit2D_point(JSVCall)
extern "C"  void UnityEngine_RaycastHit2DGenerated_RaycastHit2D_point_m316560758 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RaycastHit2DGenerated::RaycastHit2D_normal(JSVCall)
extern "C"  void UnityEngine_RaycastHit2DGenerated_RaycastHit2D_normal_m4004788479 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RaycastHit2DGenerated::RaycastHit2D_distance(JSVCall)
extern "C"  void UnityEngine_RaycastHit2DGenerated_RaycastHit2D_distance_m3230357937 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RaycastHit2DGenerated::RaycastHit2D_fraction(JSVCall)
extern "C"  void UnityEngine_RaycastHit2DGenerated_RaycastHit2D_fraction_m493265476 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RaycastHit2DGenerated::RaycastHit2D_collider(JSVCall)
extern "C"  void UnityEngine_RaycastHit2DGenerated_RaycastHit2D_collider_m3993656466 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RaycastHit2DGenerated::RaycastHit2D_rigidbody(JSVCall)
extern "C"  void UnityEngine_RaycastHit2DGenerated_RaycastHit2D_rigidbody_m1405321561 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RaycastHit2DGenerated::RaycastHit2D_transform(JSVCall)
extern "C"  void UnityEngine_RaycastHit2DGenerated_RaycastHit2D_transform_m3520681754 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RaycastHit2DGenerated::RaycastHit2D_CompareTo__RaycastHit2D(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RaycastHit2DGenerated_RaycastHit2D_CompareTo__RaycastHit2D_m4166014137 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RaycastHit2DGenerated::RaycastHit2D_op_Implicit__RaycastHit2D_to_Boolean(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RaycastHit2DGenerated_RaycastHit2D_op_Implicit__RaycastHit2D_to_Boolean_m316097577 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RaycastHit2DGenerated::__Register()
extern "C"  void UnityEngine_RaycastHit2DGenerated___Register_m3024286535 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_RaycastHit2DGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UnityEngine_RaycastHit2DGenerated_ilo_getObject1_m4097880118 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine_RaycastHit2DGenerated::ilo_getVector2S2(System.Int32)
extern "C"  Vector2_t4282066565  UnityEngine_RaycastHit2DGenerated_ilo_getVector2S2_m2126478853 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RaycastHit2DGenerated::ilo_changeJSObj3(System.Int32,System.Object)
extern "C"  void UnityEngine_RaycastHit2DGenerated_ilo_changeJSObj3_m3386008811 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObjNew1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine_RaycastHit2DGenerated::ilo_getSingle4(System.Int32)
extern "C"  float UnityEngine_RaycastHit2DGenerated_ilo_getSingle4_m611541074 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RaycastHit2DGenerated::ilo_setSingle5(System.Int32,System.Single)
extern "C"  void UnityEngine_RaycastHit2DGenerated_ilo_setSingle5_m1702478072 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_RaycastHit2DGenerated::ilo_setObject6(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UnityEngine_RaycastHit2DGenerated_ilo_setObject6_m1198699251 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_RaycastHit2DGenerated::ilo_getObject7(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_RaycastHit2DGenerated_ilo_getObject7_m558390187 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
