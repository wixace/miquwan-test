﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.ModifierConverter
struct ModifierConverter_t2749968875;
// Pathfinding.Path
struct Path_t1974241691;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_ModifierData4046421655.h"
#include "AssemblyU2DCSharp_Pathfinding_Path1974241691.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_Pathfinding_Int31974045594.h"

// System.Void Pathfinding.ModifierConverter::.ctor()
extern "C"  void ModifierConverter__ctor_m2611269548 (ModifierConverter_t2749968875 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.ModifierConverter::AllBits(Pathfinding.ModifierData,Pathfinding.ModifierData)
extern "C"  bool ModifierConverter_AllBits_m3840770305 (Il2CppObject * __this /* static, unused */, int32_t ___a0, int32_t ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.ModifierConverter::AnyBits(Pathfinding.ModifierData,Pathfinding.ModifierData)
extern "C"  bool ModifierConverter_AnyBits_m1071106134 (Il2CppObject * __this /* static, unused */, int32_t ___a0, int32_t ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.ModifierData Pathfinding.ModifierConverter::Convert(Pathfinding.Path,Pathfinding.ModifierData,Pathfinding.ModifierData)
extern "C"  int32_t ModifierConverter_Convert_m3637666368 (Il2CppObject * __this /* static, unused */, Path_t1974241691 * ___p0, int32_t ___input1, int32_t ___output2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.ModifierConverter::CanConvert(Pathfinding.ModifierData,Pathfinding.ModifierData)
extern "C"  bool ModifierConverter_CanConvert_m3691152479 (Il2CppObject * __this /* static, unused */, int32_t ___input0, int32_t ___output1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.ModifierData Pathfinding.ModifierConverter::CanConvertTo(Pathfinding.ModifierData)
extern "C"  int32_t ModifierConverter_CanConvertTo_m1923456185 (Il2CppObject * __this /* static, unused */, int32_t ___a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.ModifierConverter::ilo_AnyBits1(Pathfinding.ModifierData,Pathfinding.ModifierData)
extern "C"  bool ModifierConverter_ilo_AnyBits1_m2553985008 (Il2CppObject * __this /* static, unused */, int32_t ___a0, int32_t ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Pathfinding.ModifierConverter::ilo_op_Explicit2(Pathfinding.Int3)
extern "C"  Vector3_t4282066566  ModifierConverter_ilo_op_Explicit2_m1334234915 (Il2CppObject * __this /* static, unused */, Int3_t1974045594  ___ob0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
