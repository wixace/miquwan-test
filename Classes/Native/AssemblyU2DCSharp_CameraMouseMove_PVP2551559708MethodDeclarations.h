﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraMouseMove_PVP
struct CameraMouseMove_PVP_t2551559708;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void CameraMouseMove_PVP::.ctor()
extern "C"  void CameraMouseMove_PVP__ctor_m1075674319 (CameraMouseMove_PVP_t2551559708 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraMouseMove_PVP::Init(System.String)
extern "C"  void CameraMouseMove_PVP_Init_m4015404925 (CameraMouseMove_PVP_t2551559708 * __this, String_t* ___camera_limit0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraMouseMove_PVP::Start()
extern "C"  void CameraMouseMove_PVP_Start_m22812111 (CameraMouseMove_PVP_t2551559708 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraMouseMove_PVP::Update()
extern "C"  void CameraMouseMove_PVP_Update_m713027614 (CameraMouseMove_PVP_t2551559708 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 CameraMouseMove_PVP::LimitPosition(UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  CameraMouseMove_PVP_LimitPosition_m622495374 (CameraMouseMove_PVP_t2551559708 * __this, Vector3_t4282066566  ___pos0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
