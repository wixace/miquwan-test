﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_whedreacereCedri62
struct  M_whedreacereCedri62_t2036747218  : public Il2CppObject
{
public:
	// System.String GarbageiOS.M_whedreacereCedri62::_jucaKerenir
	String_t* ____jucaKerenir_0;
	// System.Single GarbageiOS.M_whedreacereCedri62::_dreratearTajefel
	float ____dreratearTajefel_1;
	// System.Single GarbageiOS.M_whedreacereCedri62::_youstir
	float ____youstir_2;
	// System.Single GarbageiOS.M_whedreacereCedri62::_baysaiXorge
	float ____baysaiXorge_3;
	// System.Boolean GarbageiOS.M_whedreacereCedri62::_sawmexaiFeazelpis
	bool ____sawmexaiFeazelpis_4;
	// System.Single GarbageiOS.M_whedreacereCedri62::_lertrerePowveyere
	float ____lertrerePowveyere_5;
	// System.Int32 GarbageiOS.M_whedreacereCedri62::_turseediSene
	int32_t ____turseediSene_6;
	// System.Int32 GarbageiOS.M_whedreacereCedri62::_sairceyis
	int32_t ____sairceyis_7;
	// System.UInt32 GarbageiOS.M_whedreacereCedri62::_drairsairfou
	uint32_t ____drairsairfou_8;
	// System.Int32 GarbageiOS.M_whedreacereCedri62::_corcer
	int32_t ____corcer_9;
	// System.Single GarbageiOS.M_whedreacereCedri62::_halheapas
	float ____halheapas_10;
	// System.UInt32 GarbageiOS.M_whedreacereCedri62::_maymeaNaira
	uint32_t ____maymeaNaira_11;
	// System.UInt32 GarbageiOS.M_whedreacereCedri62::_visukooYikere
	uint32_t ____visukooYikere_12;
	// System.String GarbageiOS.M_whedreacereCedri62::_zemtaha
	String_t* ____zemtaha_13;
	// System.String GarbageiOS.M_whedreacereCedri62::_jelte
	String_t* ____jelte_14;
	// System.Boolean GarbageiOS.M_whedreacereCedri62::_wounesoFouco
	bool ____wounesoFouco_15;
	// System.Boolean GarbageiOS.M_whedreacereCedri62::_rebesarDairja
	bool ____rebesarDairja_16;
	// System.String GarbageiOS.M_whedreacereCedri62::_cubulall
	String_t* ____cubulall_17;
	// System.String GarbageiOS.M_whedreacereCedri62::_saircharqoo
	String_t* ____saircharqoo_18;

public:
	inline static int32_t get_offset_of__jucaKerenir_0() { return static_cast<int32_t>(offsetof(M_whedreacereCedri62_t2036747218, ____jucaKerenir_0)); }
	inline String_t* get__jucaKerenir_0() const { return ____jucaKerenir_0; }
	inline String_t** get_address_of__jucaKerenir_0() { return &____jucaKerenir_0; }
	inline void set__jucaKerenir_0(String_t* value)
	{
		____jucaKerenir_0 = value;
		Il2CppCodeGenWriteBarrier(&____jucaKerenir_0, value);
	}

	inline static int32_t get_offset_of__dreratearTajefel_1() { return static_cast<int32_t>(offsetof(M_whedreacereCedri62_t2036747218, ____dreratearTajefel_1)); }
	inline float get__dreratearTajefel_1() const { return ____dreratearTajefel_1; }
	inline float* get_address_of__dreratearTajefel_1() { return &____dreratearTajefel_1; }
	inline void set__dreratearTajefel_1(float value)
	{
		____dreratearTajefel_1 = value;
	}

	inline static int32_t get_offset_of__youstir_2() { return static_cast<int32_t>(offsetof(M_whedreacereCedri62_t2036747218, ____youstir_2)); }
	inline float get__youstir_2() const { return ____youstir_2; }
	inline float* get_address_of__youstir_2() { return &____youstir_2; }
	inline void set__youstir_2(float value)
	{
		____youstir_2 = value;
	}

	inline static int32_t get_offset_of__baysaiXorge_3() { return static_cast<int32_t>(offsetof(M_whedreacereCedri62_t2036747218, ____baysaiXorge_3)); }
	inline float get__baysaiXorge_3() const { return ____baysaiXorge_3; }
	inline float* get_address_of__baysaiXorge_3() { return &____baysaiXorge_3; }
	inline void set__baysaiXorge_3(float value)
	{
		____baysaiXorge_3 = value;
	}

	inline static int32_t get_offset_of__sawmexaiFeazelpis_4() { return static_cast<int32_t>(offsetof(M_whedreacereCedri62_t2036747218, ____sawmexaiFeazelpis_4)); }
	inline bool get__sawmexaiFeazelpis_4() const { return ____sawmexaiFeazelpis_4; }
	inline bool* get_address_of__sawmexaiFeazelpis_4() { return &____sawmexaiFeazelpis_4; }
	inline void set__sawmexaiFeazelpis_4(bool value)
	{
		____sawmexaiFeazelpis_4 = value;
	}

	inline static int32_t get_offset_of__lertrerePowveyere_5() { return static_cast<int32_t>(offsetof(M_whedreacereCedri62_t2036747218, ____lertrerePowveyere_5)); }
	inline float get__lertrerePowveyere_5() const { return ____lertrerePowveyere_5; }
	inline float* get_address_of__lertrerePowveyere_5() { return &____lertrerePowveyere_5; }
	inline void set__lertrerePowveyere_5(float value)
	{
		____lertrerePowveyere_5 = value;
	}

	inline static int32_t get_offset_of__turseediSene_6() { return static_cast<int32_t>(offsetof(M_whedreacereCedri62_t2036747218, ____turseediSene_6)); }
	inline int32_t get__turseediSene_6() const { return ____turseediSene_6; }
	inline int32_t* get_address_of__turseediSene_6() { return &____turseediSene_6; }
	inline void set__turseediSene_6(int32_t value)
	{
		____turseediSene_6 = value;
	}

	inline static int32_t get_offset_of__sairceyis_7() { return static_cast<int32_t>(offsetof(M_whedreacereCedri62_t2036747218, ____sairceyis_7)); }
	inline int32_t get__sairceyis_7() const { return ____sairceyis_7; }
	inline int32_t* get_address_of__sairceyis_7() { return &____sairceyis_7; }
	inline void set__sairceyis_7(int32_t value)
	{
		____sairceyis_7 = value;
	}

	inline static int32_t get_offset_of__drairsairfou_8() { return static_cast<int32_t>(offsetof(M_whedreacereCedri62_t2036747218, ____drairsairfou_8)); }
	inline uint32_t get__drairsairfou_8() const { return ____drairsairfou_8; }
	inline uint32_t* get_address_of__drairsairfou_8() { return &____drairsairfou_8; }
	inline void set__drairsairfou_8(uint32_t value)
	{
		____drairsairfou_8 = value;
	}

	inline static int32_t get_offset_of__corcer_9() { return static_cast<int32_t>(offsetof(M_whedreacereCedri62_t2036747218, ____corcer_9)); }
	inline int32_t get__corcer_9() const { return ____corcer_9; }
	inline int32_t* get_address_of__corcer_9() { return &____corcer_9; }
	inline void set__corcer_9(int32_t value)
	{
		____corcer_9 = value;
	}

	inline static int32_t get_offset_of__halheapas_10() { return static_cast<int32_t>(offsetof(M_whedreacereCedri62_t2036747218, ____halheapas_10)); }
	inline float get__halheapas_10() const { return ____halheapas_10; }
	inline float* get_address_of__halheapas_10() { return &____halheapas_10; }
	inline void set__halheapas_10(float value)
	{
		____halheapas_10 = value;
	}

	inline static int32_t get_offset_of__maymeaNaira_11() { return static_cast<int32_t>(offsetof(M_whedreacereCedri62_t2036747218, ____maymeaNaira_11)); }
	inline uint32_t get__maymeaNaira_11() const { return ____maymeaNaira_11; }
	inline uint32_t* get_address_of__maymeaNaira_11() { return &____maymeaNaira_11; }
	inline void set__maymeaNaira_11(uint32_t value)
	{
		____maymeaNaira_11 = value;
	}

	inline static int32_t get_offset_of__visukooYikere_12() { return static_cast<int32_t>(offsetof(M_whedreacereCedri62_t2036747218, ____visukooYikere_12)); }
	inline uint32_t get__visukooYikere_12() const { return ____visukooYikere_12; }
	inline uint32_t* get_address_of__visukooYikere_12() { return &____visukooYikere_12; }
	inline void set__visukooYikere_12(uint32_t value)
	{
		____visukooYikere_12 = value;
	}

	inline static int32_t get_offset_of__zemtaha_13() { return static_cast<int32_t>(offsetof(M_whedreacereCedri62_t2036747218, ____zemtaha_13)); }
	inline String_t* get__zemtaha_13() const { return ____zemtaha_13; }
	inline String_t** get_address_of__zemtaha_13() { return &____zemtaha_13; }
	inline void set__zemtaha_13(String_t* value)
	{
		____zemtaha_13 = value;
		Il2CppCodeGenWriteBarrier(&____zemtaha_13, value);
	}

	inline static int32_t get_offset_of__jelte_14() { return static_cast<int32_t>(offsetof(M_whedreacereCedri62_t2036747218, ____jelte_14)); }
	inline String_t* get__jelte_14() const { return ____jelte_14; }
	inline String_t** get_address_of__jelte_14() { return &____jelte_14; }
	inline void set__jelte_14(String_t* value)
	{
		____jelte_14 = value;
		Il2CppCodeGenWriteBarrier(&____jelte_14, value);
	}

	inline static int32_t get_offset_of__wounesoFouco_15() { return static_cast<int32_t>(offsetof(M_whedreacereCedri62_t2036747218, ____wounesoFouco_15)); }
	inline bool get__wounesoFouco_15() const { return ____wounesoFouco_15; }
	inline bool* get_address_of__wounesoFouco_15() { return &____wounesoFouco_15; }
	inline void set__wounesoFouco_15(bool value)
	{
		____wounesoFouco_15 = value;
	}

	inline static int32_t get_offset_of__rebesarDairja_16() { return static_cast<int32_t>(offsetof(M_whedreacereCedri62_t2036747218, ____rebesarDairja_16)); }
	inline bool get__rebesarDairja_16() const { return ____rebesarDairja_16; }
	inline bool* get_address_of__rebesarDairja_16() { return &____rebesarDairja_16; }
	inline void set__rebesarDairja_16(bool value)
	{
		____rebesarDairja_16 = value;
	}

	inline static int32_t get_offset_of__cubulall_17() { return static_cast<int32_t>(offsetof(M_whedreacereCedri62_t2036747218, ____cubulall_17)); }
	inline String_t* get__cubulall_17() const { return ____cubulall_17; }
	inline String_t** get_address_of__cubulall_17() { return &____cubulall_17; }
	inline void set__cubulall_17(String_t* value)
	{
		____cubulall_17 = value;
		Il2CppCodeGenWriteBarrier(&____cubulall_17, value);
	}

	inline static int32_t get_offset_of__saircharqoo_18() { return static_cast<int32_t>(offsetof(M_whedreacereCedri62_t2036747218, ____saircharqoo_18)); }
	inline String_t* get__saircharqoo_18() const { return ____saircharqoo_18; }
	inline String_t** get_address_of__saircharqoo_18() { return &____saircharqoo_18; }
	inline void set__saircharqoo_18(String_t* value)
	{
		____saircharqoo_18 = value;
		Il2CppCodeGenWriteBarrier(&____saircharqoo_18, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
