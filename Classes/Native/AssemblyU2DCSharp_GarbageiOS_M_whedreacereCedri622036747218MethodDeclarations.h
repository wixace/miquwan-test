﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_whedreacereCedri62
struct M_whedreacereCedri62_t2036747218;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_whedreacereCedri622036747218.h"

// System.Void GarbageiOS.M_whedreacereCedri62::.ctor()
extern "C"  void M_whedreacereCedri62__ctor_m757253281 (M_whedreacereCedri62_t2036747218 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_whedreacereCedri62::M_saseqallPeajis0(System.String[],System.Int32)
extern "C"  void M_whedreacereCedri62_M_saseqallPeajis0_m1991841020 (M_whedreacereCedri62_t2036747218 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_whedreacereCedri62::M_beechi1(System.String[],System.Int32)
extern "C"  void M_whedreacereCedri62_M_beechi1_m1814080115 (M_whedreacereCedri62_t2036747218 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_whedreacereCedri62::M_nisstaymo2(System.String[],System.Int32)
extern "C"  void M_whedreacereCedri62_M_nisstaymo2_m3372731211 (M_whedreacereCedri62_t2036747218 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_whedreacereCedri62::ilo_M_saseqallPeajis01(GarbageiOS.M_whedreacereCedri62,System.String[],System.Int32)
extern "C"  void M_whedreacereCedri62_ilo_M_saseqallPeajis01_m2324716662 (Il2CppObject * __this /* static, unused */, M_whedreacereCedri62_t2036747218 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
