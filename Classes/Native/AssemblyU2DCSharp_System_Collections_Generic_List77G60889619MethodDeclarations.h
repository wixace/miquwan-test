﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System_Collections_Generic_List77Generated/<ListA1_FindIndex_GetDelegate_member16_arg1>c__AnonStorey85`1<System.Object>
struct U3CListA1_FindIndex_GetDelegate_member16_arg1U3Ec__AnonStorey85_1_t60889619;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System_Collections_Generic_List77Generated/<ListA1_FindIndex_GetDelegate_member16_arg1>c__AnonStorey85`1<System.Object>::.ctor()
extern "C"  void U3CListA1_FindIndex_GetDelegate_member16_arg1U3Ec__AnonStorey85_1__ctor_m1241409826_gshared (U3CListA1_FindIndex_GetDelegate_member16_arg1U3Ec__AnonStorey85_1_t60889619 * __this, const MethodInfo* method);
#define U3CListA1_FindIndex_GetDelegate_member16_arg1U3Ec__AnonStorey85_1__ctor_m1241409826(__this, method) ((  void (*) (U3CListA1_FindIndex_GetDelegate_member16_arg1U3Ec__AnonStorey85_1_t60889619 *, const MethodInfo*))U3CListA1_FindIndex_GetDelegate_member16_arg1U3Ec__AnonStorey85_1__ctor_m1241409826_gshared)(__this, method)
// System.Boolean System_Collections_Generic_List77Generated/<ListA1_FindIndex_GetDelegate_member16_arg1>c__AnonStorey85`1<System.Object>::<>m__AA(T)
extern "C"  bool U3CListA1_FindIndex_GetDelegate_member16_arg1U3Ec__AnonStorey85_1_U3CU3Em__AA_m599312383_gshared (U3CListA1_FindIndex_GetDelegate_member16_arg1U3Ec__AnonStorey85_1_t60889619 * __this, Il2CppObject * ___obj0, const MethodInfo* method);
#define U3CListA1_FindIndex_GetDelegate_member16_arg1U3Ec__AnonStorey85_1_U3CU3Em__AA_m599312383(__this, ___obj0, method) ((  bool (*) (U3CListA1_FindIndex_GetDelegate_member16_arg1U3Ec__AnonStorey85_1_t60889619 *, Il2CppObject *, const MethodInfo*))U3CListA1_FindIndex_GetDelegate_member16_arg1U3Ec__AnonStorey85_1_U3CU3Em__AA_m599312383_gshared)(__this, ___obj0, method)
