﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_riserMurai108
struct M_riserMurai108_t3459941488;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"

// System.Void GarbageiOS.M_riserMurai108::.ctor()
extern "C"  void M_riserMurai108__ctor_m3466383091 (M_riserMurai108_t3459941488 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_riserMurai108::M_jejurYaichouwel0(System.String[],System.Int32)
extern "C"  void M_riserMurai108_M_jejurYaichouwel0_m3464720044 (M_riserMurai108_t3459941488 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
