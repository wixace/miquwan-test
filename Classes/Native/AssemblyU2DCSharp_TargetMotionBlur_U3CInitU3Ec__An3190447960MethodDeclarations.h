﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TargetMotionBlur/<Init>c__AnonStorey150
struct U3CInitU3Ec__AnonStorey150_t3190447960;

#include "codegen/il2cpp-codegen.h"

// System.Void TargetMotionBlur/<Init>c__AnonStorey150::.ctor()
extern "C"  void U3CInitU3Ec__AnonStorey150__ctor_m4108514067 (U3CInitU3Ec__AnonStorey150_t3190447960 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TargetMotionBlur/<Init>c__AnonStorey150::<>m__3C1()
extern "C"  void U3CInitU3Ec__AnonStorey150_U3CU3Em__3C1_m1596752839 (U3CInitU3Ec__AnonStorey150_t3190447960 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
