﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Byte[]
struct ByteU5BU5D_t4260760469;

#include "codegen/il2cpp-codegen.h"

// System.Void Pathfinding.Util.Memory::MemSet(System.Byte[],System.Byte)
extern "C"  void Memory_MemSet_m624624293 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___array0, uint8_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
