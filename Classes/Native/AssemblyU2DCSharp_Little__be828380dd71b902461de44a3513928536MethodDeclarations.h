﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._be828380dd71b902461de44aa6967044
struct _be828380dd71b902461de44aa6967044_t3513928536;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__be828380dd71b902461de44a3513928536.h"

// System.Void Little._be828380dd71b902461de44aa6967044::.ctor()
extern "C"  void _be828380dd71b902461de44aa6967044__ctor_m3891535317 (_be828380dd71b902461de44aa6967044_t3513928536 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._be828380dd71b902461de44aa6967044::_be828380dd71b902461de44aa6967044m2(System.Int32)
extern "C"  int32_t _be828380dd71b902461de44aa6967044__be828380dd71b902461de44aa6967044m2_m1499239065 (_be828380dd71b902461de44aa6967044_t3513928536 * __this, int32_t ____be828380dd71b902461de44aa6967044a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._be828380dd71b902461de44aa6967044::_be828380dd71b902461de44aa6967044m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _be828380dd71b902461de44aa6967044__be828380dd71b902461de44aa6967044m_m4134220861 (_be828380dd71b902461de44aa6967044_t3513928536 * __this, int32_t ____be828380dd71b902461de44aa6967044a0, int32_t ____be828380dd71b902461de44aa6967044401, int32_t ____be828380dd71b902461de44aa6967044c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._be828380dd71b902461de44aa6967044::ilo__be828380dd71b902461de44aa6967044m21(Little._be828380dd71b902461de44aa6967044,System.Int32)
extern "C"  int32_t _be828380dd71b902461de44aa6967044_ilo__be828380dd71b902461de44aa6967044m21_m1756521503 (Il2CppObject * __this /* static, unused */, _be828380dd71b902461de44aa6967044_t3513928536 * ____this0, int32_t ____be828380dd71b902461de44aa6967044a1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
