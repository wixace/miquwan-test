﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_MathfGenerated
struct UnityEngine_MathfGenerated_t1827413049;
// JSVCall
struct JSVCall_t3708497963;
// System.Single[]
struct SingleU5BU5D_t2316563989;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void UnityEngine_MathfGenerated::.ctor()
extern "C"  void UnityEngine_MathfGenerated__ctor_m2967862402 (UnityEngine_MathfGenerated_t1827413049 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_MathfGenerated::.cctor()
extern "C"  void UnityEngine_MathfGenerated__cctor_m1327325035 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MathfGenerated::Mathf_Mathf1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MathfGenerated_Mathf_Mathf1_m3581982842 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_MathfGenerated::Mathf_PI(JSVCall)
extern "C"  void UnityEngine_MathfGenerated_Mathf_PI_m3874900267 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_MathfGenerated::Mathf_Infinity(JSVCall)
extern "C"  void UnityEngine_MathfGenerated_Mathf_Infinity_m2574210876 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_MathfGenerated::Mathf_NegativeInfinity(JSVCall)
extern "C"  void UnityEngine_MathfGenerated_Mathf_NegativeInfinity_m810294663 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_MathfGenerated::Mathf_Deg2Rad(JSVCall)
extern "C"  void UnityEngine_MathfGenerated_Mathf_Deg2Rad_m2332430719 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_MathfGenerated::Mathf_Rad2Deg(JSVCall)
extern "C"  void UnityEngine_MathfGenerated_Mathf_Rad2Deg_m2561753343 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_MathfGenerated::Mathf_Epsilon(JSVCall)
extern "C"  void UnityEngine_MathfGenerated_Mathf_Epsilon_m2499565246 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MathfGenerated::Mathf_Abs__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MathfGenerated_Mathf_Abs__Int32_m1659982083 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MathfGenerated::Mathf_Abs__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MathfGenerated_Mathf_Abs__Single_m1102928885 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MathfGenerated::Mathf_Acos__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MathfGenerated_Mathf_Acos__Single_m2253354997 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MathfGenerated::Mathf_Approximately__Single__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MathfGenerated_Mathf_Approximately__Single__Single_m3749497260 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MathfGenerated::Mathf_Asin__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MathfGenerated_Mathf_Asin__Single_m3390951302 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MathfGenerated::Mathf_Atan__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MathfGenerated_Mathf_Atan__Single_m108235919 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MathfGenerated::Mathf_Atan2__Single__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MathfGenerated_Mathf_Atan2__Single__Single_m2958307293 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MathfGenerated::Mathf_Ceil__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MathfGenerated_Mathf_Ceil__Single_m952074036 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MathfGenerated::Mathf_CeilToInt__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MathfGenerated_Mathf_CeilToInt__Single_m2248245426 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MathfGenerated::Mathf_Clamp__Int32__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MathfGenerated_Mathf_Clamp__Int32__Int32__Int32_m3121284186 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MathfGenerated::Mathf_Clamp__Single__Single__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MathfGenerated_Mathf_Clamp__Single__Single__Single_m1799585422 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MathfGenerated::Mathf_Clamp01__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MathfGenerated_Mathf_Clamp01__Single_m2063015167 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MathfGenerated::Mathf_ClosestPowerOfTwo__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MathfGenerated_Mathf_ClosestPowerOfTwo__Int32_m834400300 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MathfGenerated::Mathf_Cos__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MathfGenerated_Mathf_Cos__Single_m668996170 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MathfGenerated::Mathf_DeltaAngle__Single__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MathfGenerated_Mathf_DeltaAngle__Single__Single_m1077624946 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MathfGenerated::Mathf_Exp__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MathfGenerated_Mathf_Exp__Single_m2928111712 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MathfGenerated::Mathf_FloatToHalf__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MathfGenerated_Mathf_FloatToHalf__Single_m1203787341 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MathfGenerated::Mathf_Floor__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MathfGenerated_Mathf_Floor__Single_m1771699087 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MathfGenerated::Mathf_FloorToInt__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MathfGenerated_Mathf_FloorToInt__Single_m27100471 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MathfGenerated::Mathf_Gamma__Single__Single__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MathfGenerated_Mathf_Gamma__Single__Single__Single_m795921530 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MathfGenerated::Mathf_GammaToLinearSpace__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MathfGenerated_Mathf_GammaToLinearSpace__Single_m1117067566 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MathfGenerated::Mathf_HalfToFloat__UInt16(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MathfGenerated_Mathf_HalfToFloat__UInt16_m45361352 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MathfGenerated::Mathf_InverseLerp__Single__Single__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MathfGenerated_Mathf_InverseLerp__Single__Single__Single_m2026336794 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MathfGenerated::Mathf_IsPowerOfTwo__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MathfGenerated_Mathf_IsPowerOfTwo__Int32_m792750415 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MathfGenerated::Mathf_Lerp__Single__Single__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MathfGenerated_Mathf_Lerp__Single__Single__Single_m3123678902 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MathfGenerated::Mathf_LerpAngle__Single__Single__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MathfGenerated_Mathf_LerpAngle__Single__Single__Single_m230361263 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MathfGenerated::Mathf_LerpUnclamped__Single__Single__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MathfGenerated_Mathf_LerpUnclamped__Single__Single__Single_m3988425661 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MathfGenerated::Mathf_LinearToGammaSpace__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MathfGenerated_Mathf_LinearToGammaSpace__Single_m3135943790 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MathfGenerated::Mathf_Log__Single__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MathfGenerated_Mathf_Log__Single__Single_m83026831 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MathfGenerated::Mathf_Log__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MathfGenerated_Mathf_Log__Single_m2576108359 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MathfGenerated::Mathf_Log10__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MathfGenerated_Mathf_Log10__Single_m1000132006 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MathfGenerated::Mathf_Max__Single__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MathfGenerated_Mathf_Max__Single__Single_m654331311 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MathfGenerated::Mathf_Max__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MathfGenerated_Mathf_Max__Int32__Int32_m1488443615 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MathfGenerated::Mathf_Max__Single_Array(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MathfGenerated_Mathf_Max__Single_Array_m2221513377 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MathfGenerated::Mathf_Max__Int32_Array(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MathfGenerated_Mathf_Max__Int32_Array_m1125945099 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MathfGenerated::Mathf_Min__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MathfGenerated_Mathf_Min__Int32__Int32_m2663283405 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MathfGenerated::Mathf_Min__Single__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MathfGenerated_Mathf_Min__Single__Single_m98970653 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MathfGenerated::Mathf_Min__Int32_Array(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MathfGenerated_Mathf_Min__Int32_Array_m1440937821 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MathfGenerated::Mathf_Min__Single_Array(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MathfGenerated_Mathf_Min__Single_Array_m3396353167 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MathfGenerated::Mathf_MoveTowards__Single__Single__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MathfGenerated_Mathf_MoveTowards__Single__Single__Single_m2417218846 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MathfGenerated::Mathf_MoveTowardsAngle__Single__Single__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MathfGenerated_Mathf_MoveTowardsAngle__Single__Single__Single_m1795851079 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MathfGenerated::Mathf_NextPowerOfTwo__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MathfGenerated_Mathf_NextPowerOfTwo__Int32_m2746853606 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MathfGenerated::Mathf_PerlinNoise__Single__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MathfGenerated_Mathf_PerlinNoise__Single__Single_m550881265 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MathfGenerated::Mathf_PingPong__Single__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MathfGenerated_Mathf_PingPong__Single__Single_m4273360705 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MathfGenerated::Mathf_Pow__Single__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MathfGenerated_Mathf_Pow__Single__Single_m3347252131 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MathfGenerated::Mathf_Repeat__Single__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MathfGenerated_Mathf_Repeat__Single__Single_m3232485650 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MathfGenerated::Mathf_Round__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MathfGenerated_Mathf_Round__Single_m960854417 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MathfGenerated::Mathf_RoundToInt__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MathfGenerated_Mathf_RoundToInt__Single_m1255831669 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MathfGenerated::Mathf_Sign__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MathfGenerated_Mathf_Sign__Single_m462143980 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MathfGenerated::Mathf_Sin__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MathfGenerated_Mathf_Sin__Single_m1806592475 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MathfGenerated::Mathf_SmoothDamp__Single__Single__Single__Single__Single__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MathfGenerated_Mathf_SmoothDamp__Single__Single__Single__Single__Single__Single_m4113239301 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MathfGenerated::Mathf_SmoothDamp__Single__Single__Single__Single__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MathfGenerated_Mathf_SmoothDamp__Single__Single__Single__Single__Single_m1386760893 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MathfGenerated::Mathf_SmoothDamp__Single__Single__Single__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MathfGenerated_Mathf_SmoothDamp__Single__Single__Single__Single_m2319106165 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MathfGenerated::Mathf_SmoothDampAngle__Single__Single__Single__Single__Single__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MathfGenerated_Mathf_SmoothDampAngle__Single__Single__Single__Single__Single__Single_m2495482224 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MathfGenerated::Mathf_SmoothDampAngle__Single__Single__Single__Single__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MathfGenerated_Mathf_SmoothDampAngle__Single__Single__Single__Single__Single_m2334646824 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MathfGenerated::Mathf_SmoothDampAngle__Single__Single__Single__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MathfGenerated_Mathf_SmoothDampAngle__Single__Single__Single__Single_m2473718496 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MathfGenerated::Mathf_SmoothStep__Single__Single__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MathfGenerated_Mathf_SmoothStep__Single__Single__Single_m309102873 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MathfGenerated::Mathf_Sqrt__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MathfGenerated_Mathf_Sqrt__Single_m1641091599 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MathfGenerated::Mathf_Tan__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MathfGenerated_Mathf_Tan__Single_m2818844388 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_MathfGenerated::__Register()
extern "C"  void UnityEngine_MathfGenerated___Register_m1592721797 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single[] UnityEngine_MathfGenerated::<Mathf_Max__Single_Array>m__263()
extern "C"  SingleU5BU5D_t2316563989* UnityEngine_MathfGenerated_U3CMathf_Max__Single_ArrayU3Em__263_m2060581997 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] UnityEngine_MathfGenerated::<Mathf_Max__Int32_Array>m__264()
extern "C"  Int32U5BU5D_t3230847821* UnityEngine_MathfGenerated_U3CMathf_Max__Int32_ArrayU3Em__264_m2590191566 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] UnityEngine_MathfGenerated::<Mathf_Min__Int32_Array>m__265()
extern "C"  Int32U5BU5D_t3230847821* UnityEngine_MathfGenerated_U3CMathf_Min__Int32_ArrayU3Em__265_m2491395581 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single[] UnityEngine_MathfGenerated::<Mathf_Min__Single_Array>m__266()
extern "C"  SingleU5BU5D_t2316563989* UnityEngine_MathfGenerated_U3CMathf_Min__Single_ArrayU3Em__266_m3292846850 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_MathfGenerated::ilo_addJSCSRel1(System.Int32,System.Object)
extern "C"  void UnityEngine_MathfGenerated_ilo_addJSCSRel1_m1600191038 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_MathfGenerated::ilo_setSingle2(System.Int32,System.Single)
extern "C"  void UnityEngine_MathfGenerated_ilo_setSingle2_m1787437363 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine_MathfGenerated::ilo_getSingle3(System.Int32)
extern "C"  float UnityEngine_MathfGenerated_ilo_getSingle3_m2370934631 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_MathfGenerated::ilo_getInt324(System.Int32)
extern "C"  int32_t UnityEngine_MathfGenerated_ilo_getInt324_m397800460 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_MathfGenerated::ilo_setInt325(System.Int32,System.Int32)
extern "C"  void UnityEngine_MathfGenerated_ilo_setInt325_m232705296 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt16 UnityEngine_MathfGenerated::ilo_getUInt166(System.Int32)
extern "C"  uint16_t UnityEngine_MathfGenerated_ilo_getUInt166_m2926216188 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_MathfGenerated::ilo_getArgIndex7()
extern "C"  int32_t UnityEngine_MathfGenerated_ilo_getArgIndex7_m1176596744 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_MathfGenerated::ilo_getElement8(System.Int32,System.Int32)
extern "C"  int32_t UnityEngine_MathfGenerated_ilo_getElement8_m3878178357 (Il2CppObject * __this /* static, unused */, int32_t ___id0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_MathfGenerated::ilo_getObject9(System.Int32)
extern "C"  int32_t UnityEngine_MathfGenerated_ilo_getObject9_m602841176 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_MathfGenerated::ilo_getArrayLength10(System.Int32)
extern "C"  int32_t UnityEngine_MathfGenerated_ilo_getArrayLength10_m2570881148 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
