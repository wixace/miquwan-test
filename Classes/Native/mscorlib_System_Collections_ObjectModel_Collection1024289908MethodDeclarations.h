﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<PointCloudRegognizer/Point>
struct Collection_1_t1024289908;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// PointCloudRegognizer/Point[]
struct PointU5BU5D_t2313848483;
// System.Collections.Generic.IEnumerator`1<PointCloudRegognizer/Point>
struct IEnumerator_1_t3750696799;
// System.Collections.Generic.IList`1<PointCloudRegognizer/Point>
struct IList_1_t238511657;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_PointCloudRegognizer_Point1838831750.h"

// System.Void System.Collections.ObjectModel.Collection`1<PointCloudRegognizer/Point>::.ctor()
extern "C"  void Collection_1__ctor_m1240490637_gshared (Collection_1_t1024289908 * __this, const MethodInfo* method);
#define Collection_1__ctor_m1240490637(__this, method) ((  void (*) (Collection_1_t1024289908 *, const MethodInfo*))Collection_1__ctor_m1240490637_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<PointCloudRegognizer/Point>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2831945354_gshared (Collection_1_t1024289908 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2831945354(__this, method) ((  bool (*) (Collection_1_t1024289908 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2831945354_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<PointCloudRegognizer/Point>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Collection_1_System_Collections_ICollection_CopyTo_m2966206295_gshared (Collection_1_t1024289908 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m2966206295(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t1024289908 *, Il2CppArray *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m2966206295_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<PointCloudRegognizer/Point>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Collection_1_System_Collections_IEnumerable_GetEnumerator_m1228737062_gshared (Collection_1_t1024289908 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m1228737062(__this, method) ((  Il2CppObject * (*) (Collection_1_t1024289908 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m1228737062_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<PointCloudRegognizer/Point>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_Add_m860342999_gshared (Collection_1_t1024289908 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m860342999(__this, ___value0, method) ((  int32_t (*) (Collection_1_t1024289908 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m860342999_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<PointCloudRegognizer/Point>::System.Collections.IList.Contains(System.Object)
extern "C"  bool Collection_1_System_Collections_IList_Contains_m3096577481_gshared (Collection_1_t1024289908 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m3096577481(__this, ___value0, method) ((  bool (*) (Collection_1_t1024289908 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m3096577481_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<PointCloudRegognizer/Point>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_IndexOf_m292076975_gshared (Collection_1_t1024289908 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m292076975(__this, ___value0, method) ((  int32_t (*) (Collection_1_t1024289908 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m292076975_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.Collection`1<PointCloudRegognizer/Point>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_Insert_m3710702242_gshared (Collection_1_t1024289908 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m3710702242(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t1024289908 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m3710702242_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<PointCloudRegognizer/Point>::System.Collections.IList.Remove(System.Object)
extern "C"  void Collection_1_System_Collections_IList_Remove_m3940235078_gshared (Collection_1_t1024289908 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m3940235078(__this, ___value0, method) ((  void (*) (Collection_1_t1024289908 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m3940235078_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<PointCloudRegognizer/Point>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m2827129075_gshared (Collection_1_t1024289908 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m2827129075(__this, method) ((  bool (*) (Collection_1_t1024289908 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m2827129075_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<PointCloudRegognizer/Point>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Collection_1_System_Collections_ICollection_get_SyncRoot_m1588724261_gshared (Collection_1_t1024289908 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m1588724261(__this, method) ((  Il2CppObject * (*) (Collection_1_t1024289908 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m1588724261_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<PointCloudRegognizer/Point>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool Collection_1_System_Collections_IList_get_IsFixedSize_m4141875192_gshared (Collection_1_t1024289908 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m4141875192(__this, method) ((  bool (*) (Collection_1_t1024289908 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m4141875192_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<PointCloudRegognizer/Point>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_IList_get_IsReadOnly_m3358564353_gshared (Collection_1_t1024289908 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m3358564353(__this, method) ((  bool (*) (Collection_1_t1024289908 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m3358564353_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<PointCloudRegognizer/Point>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * Collection_1_System_Collections_IList_get_Item_m3116236396_gshared (Collection_1_t1024289908 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m3116236396(__this, ___index0, method) ((  Il2CppObject * (*) (Collection_1_t1024289908 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m3116236396_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<PointCloudRegognizer/Point>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_set_Item_m1775951353_gshared (Collection_1_t1024289908 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m1775951353(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t1024289908 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m1775951353_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<PointCloudRegognizer/Point>::Add(T)
extern "C"  void Collection_1_Add_m3078199378_gshared (Collection_1_t1024289908 * __this, Point_t1838831750  ___item0, const MethodInfo* method);
#define Collection_1_Add_m3078199378(__this, ___item0, method) ((  void (*) (Collection_1_t1024289908 *, Point_t1838831750 , const MethodInfo*))Collection_1_Add_m3078199378_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<PointCloudRegognizer/Point>::Clear()
extern "C"  void Collection_1_Clear_m2941591224_gshared (Collection_1_t1024289908 * __this, const MethodInfo* method);
#define Collection_1_Clear_m2941591224(__this, method) ((  void (*) (Collection_1_t1024289908 *, const MethodInfo*))Collection_1_Clear_m2941591224_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<PointCloudRegognizer/Point>::ClearItems()
extern "C"  void Collection_1_ClearItems_m1620374858_gshared (Collection_1_t1024289908 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m1620374858(__this, method) ((  void (*) (Collection_1_t1024289908 *, const MethodInfo*))Collection_1_ClearItems_m1620374858_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<PointCloudRegognizer/Point>::Contains(T)
extern "C"  bool Collection_1_Contains_m1928567210_gshared (Collection_1_t1024289908 * __this, Point_t1838831750  ___item0, const MethodInfo* method);
#define Collection_1_Contains_m1928567210(__this, ___item0, method) ((  bool (*) (Collection_1_t1024289908 *, Point_t1838831750 , const MethodInfo*))Collection_1_Contains_m1928567210_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<PointCloudRegognizer/Point>::CopyTo(T[],System.Int32)
extern "C"  void Collection_1_CopyTo_m50122690_gshared (Collection_1_t1024289908 * __this, PointU5BU5D_t2313848483* ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_CopyTo_m50122690(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t1024289908 *, PointU5BU5D_t2313848483*, int32_t, const MethodInfo*))Collection_1_CopyTo_m50122690_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<PointCloudRegognizer/Point>::GetEnumerator()
extern "C"  Il2CppObject* Collection_1_GetEnumerator_m1437344257_gshared (Collection_1_t1024289908 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m1437344257(__this, method) ((  Il2CppObject* (*) (Collection_1_t1024289908 *, const MethodInfo*))Collection_1_GetEnumerator_m1437344257_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<PointCloudRegognizer/Point>::IndexOf(T)
extern "C"  int32_t Collection_1_IndexOf_m154027278_gshared (Collection_1_t1024289908 * __this, Point_t1838831750  ___item0, const MethodInfo* method);
#define Collection_1_IndexOf_m154027278(__this, ___item0, method) ((  int32_t (*) (Collection_1_t1024289908 *, Point_t1838831750 , const MethodInfo*))Collection_1_IndexOf_m154027278_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<PointCloudRegognizer/Point>::Insert(System.Int32,T)
extern "C"  void Collection_1_Insert_m2511335097_gshared (Collection_1_t1024289908 * __this, int32_t ___index0, Point_t1838831750  ___item1, const MethodInfo* method);
#define Collection_1_Insert_m2511335097(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t1024289908 *, int32_t, Point_t1838831750 , const MethodInfo*))Collection_1_Insert_m2511335097_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.ObjectModel.Collection`1<PointCloudRegognizer/Point>::InsertItem(System.Int32,T)
extern "C"  void Collection_1_InsertItem_m2884573292_gshared (Collection_1_t1024289908 * __this, int32_t ___index0, Point_t1838831750  ___item1, const MethodInfo* method);
#define Collection_1_InsertItem_m2884573292(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t1024289908 *, int32_t, Point_t1838831750 , const MethodInfo*))Collection_1_InsertItem_m2884573292_gshared)(__this, ___index0, ___item1, method)
// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.Collection`1<PointCloudRegognizer/Point>::get_Items()
extern "C"  Il2CppObject* Collection_1_get_Items_m771122808_gshared (Collection_1_t1024289908 * __this, const MethodInfo* method);
#define Collection_1_get_Items_m771122808(__this, method) ((  Il2CppObject* (*) (Collection_1_t1024289908 *, const MethodInfo*))Collection_1_get_Items_m771122808_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<PointCloudRegognizer/Point>::Remove(T)
extern "C"  bool Collection_1_Remove_m2763439141_gshared (Collection_1_t1024289908 * __this, Point_t1838831750  ___item0, const MethodInfo* method);
#define Collection_1_Remove_m2763439141(__this, ___item0, method) ((  bool (*) (Collection_1_t1024289908 *, Point_t1838831750 , const MethodInfo*))Collection_1_Remove_m2763439141_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<PointCloudRegognizer/Point>::RemoveAt(System.Int32)
extern "C"  void Collection_1_RemoveAt_m385187967_gshared (Collection_1_t1024289908 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveAt_m385187967(__this, ___index0, method) ((  void (*) (Collection_1_t1024289908 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m385187967_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<PointCloudRegognizer/Point>::RemoveItem(System.Int32)
extern "C"  void Collection_1_RemoveItem_m3698073823_gshared (Collection_1_t1024289908 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveItem_m3698073823(__this, ___index0, method) ((  void (*) (Collection_1_t1024289908 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m3698073823_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<PointCloudRegognizer/Point>::get_Count()
extern "C"  int32_t Collection_1_get_Count_m297185837_gshared (Collection_1_t1024289908 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m297185837(__this, method) ((  int32_t (*) (Collection_1_t1024289908 *, const MethodInfo*))Collection_1_get_Count_m297185837_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<PointCloudRegognizer/Point>::get_Item(System.Int32)
extern "C"  Point_t1838831750  Collection_1_get_Item_m1103997861_gshared (Collection_1_t1024289908 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_get_Item_m1103997861(__this, ___index0, method) ((  Point_t1838831750  (*) (Collection_1_t1024289908 *, int32_t, const MethodInfo*))Collection_1_get_Item_m1103997861_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<PointCloudRegognizer/Point>::set_Item(System.Int32,T)
extern "C"  void Collection_1_set_Item_m1431004496_gshared (Collection_1_t1024289908 * __this, int32_t ___index0, Point_t1838831750  ___value1, const MethodInfo* method);
#define Collection_1_set_Item_m1431004496(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t1024289908 *, int32_t, Point_t1838831750 , const MethodInfo*))Collection_1_set_Item_m1431004496_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<PointCloudRegognizer/Point>::SetItem(System.Int32,T)
extern "C"  void Collection_1_SetItem_m1858566441_gshared (Collection_1_t1024289908 * __this, int32_t ___index0, Point_t1838831750  ___item1, const MethodInfo* method);
#define Collection_1_SetItem_m1858566441(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t1024289908 *, int32_t, Point_t1838831750 , const MethodInfo*))Collection_1_SetItem_m1858566441_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<PointCloudRegognizer/Point>::IsValidItem(System.Object)
extern "C"  bool Collection_1_IsValidItem_m2489651010_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_IsValidItem_m2489651010(__this /* static, unused */, ___item0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_IsValidItem_m2489651010_gshared)(__this /* static, unused */, ___item0, method)
// T System.Collections.ObjectModel.Collection`1<PointCloudRegognizer/Point>::ConvertItem(System.Object)
extern "C"  Point_t1838831750  Collection_1_ConvertItem_m1498349572_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_ConvertItem_m1498349572(__this /* static, unused */, ___item0, method) ((  Point_t1838831750  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_ConvertItem_m1498349572_gshared)(__this /* static, unused */, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<PointCloudRegognizer/Point>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C"  void Collection_1_CheckWritable_m1961329282_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_CheckWritable_m1961329282(__this /* static, unused */, ___list0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_CheckWritable_m1961329282_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<PointCloudRegognizer/Point>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsSynchronized_m2968795714_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsSynchronized_m2968795714(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsSynchronized_m2968795714_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<PointCloudRegognizer/Point>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsFixedSize_m3804496797_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsFixedSize_m3804496797(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsFixedSize_m3804496797_gshared)(__this /* static, unused */, ___list0, method)
