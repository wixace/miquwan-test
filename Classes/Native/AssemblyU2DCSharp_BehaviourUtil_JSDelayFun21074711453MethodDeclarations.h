﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BehaviourUtil/JSDelayFun2
struct JSDelayFun2_t1074711453;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void BehaviourUtil/JSDelayFun2::.ctor(System.Object,System.IntPtr)
extern "C"  void JSDelayFun2__ctor_m930729204 (JSDelayFun2_t1074711453 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BehaviourUtil/JSDelayFun2::Invoke(System.Object,System.Object)
extern "C"  void JSDelayFun2_Invoke_m480310868 (JSDelayFun2_t1074711453 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult BehaviourUtil/JSDelayFun2::BeginInvoke(System.Object,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * JSDelayFun2_BeginInvoke_m4245790561 (JSDelayFun2_t1074711453 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BehaviourUtil/JSDelayFun2::EndInvoke(System.IAsyncResult)
extern "C"  void JSDelayFun2_EndInvoke_m4227979524 (JSDelayFun2_t1074711453 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
