﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Queue`1<AstarPath/GUOSingle>
struct Queue_1_t1598615119;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_Queue_1_Enumerat2887700631.h"
#include "AssemblyU2DCSharp_AstarPath_GUOSingle3657339986.h"

// System.Void System.Collections.Generic.Queue`1/Enumerator<AstarPath/GUOSingle>::.ctor(System.Collections.Generic.Queue`1<T>)
extern "C"  void Enumerator__ctor_m398126539_gshared (Enumerator_t2887700631 * __this, Queue_1_t1598615119 * ___q0, const MethodInfo* method);
#define Enumerator__ctor_m398126539(__this, ___q0, method) ((  void (*) (Enumerator_t2887700631 *, Queue_1_t1598615119 *, const MethodInfo*))Enumerator__ctor_m398126539_gshared)(__this, ___q0, method)
// System.Void System.Collections.Generic.Queue`1/Enumerator<AstarPath/GUOSingle>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3471518662_gshared (Enumerator_t2887700631 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m3471518662(__this, method) ((  void (*) (Enumerator_t2887700631 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3471518662_gshared)(__this, method)
// System.Object System.Collections.Generic.Queue`1/Enumerator<AstarPath/GUOSingle>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2303183420_gshared (Enumerator_t2887700631 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m2303183420(__this, method) ((  Il2CppObject * (*) (Enumerator_t2887700631 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2303183420_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1/Enumerator<AstarPath/GUOSingle>::Dispose()
extern "C"  void Enumerator_Dispose_m2912748017_gshared (Enumerator_t2887700631 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m2912748017(__this, method) ((  void (*) (Enumerator_t2887700631 *, const MethodInfo*))Enumerator_Dispose_m2912748017_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Queue`1/Enumerator<AstarPath/GUOSingle>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2220347402_gshared (Enumerator_t2887700631 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m2220347402(__this, method) ((  bool (*) (Enumerator_t2887700631 *, const MethodInfo*))Enumerator_MoveNext_m2220347402_gshared)(__this, method)
// T System.Collections.Generic.Queue`1/Enumerator<AstarPath/GUOSingle>::get_Current()
extern "C"  GUOSingle_t3657339986  Enumerator_get_Current_m901232707_gshared (Enumerator_t2887700631 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m901232707(__this, method) ((  GUOSingle_t3657339986  (*) (Enumerator_t2887700631 *, const MethodInfo*))Enumerator_get_Current_m901232707_gshared)(__this, method)
