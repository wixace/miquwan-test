﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pomelo.DotNetClient.ObjPool`1<System.Object>
struct ObjPool_1_t286528818;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void Pomelo.DotNetClient.ObjPool`1<System.Object>::.ctor(System.Int32)
extern "C"  void ObjPool_1__ctor_m2650734553_gshared (ObjPool_1_t286528818 * __this, int32_t ___maxNum0, const MethodInfo* method);
#define ObjPool_1__ctor_m2650734553(__this, ___maxNum0, method) ((  void (*) (ObjPool_1_t286528818 *, int32_t, const MethodInfo*))ObjPool_1__ctor_m2650734553_gshared)(__this, ___maxNum0, method)
// T Pomelo.DotNetClient.ObjPool`1<System.Object>::PopObj()
extern "C"  Il2CppObject * ObjPool_1_PopObj_m3392359585_gshared (ObjPool_1_t286528818 * __this, const MethodInfo* method);
#define ObjPool_1_PopObj_m3392359585(__this, method) ((  Il2CppObject * (*) (ObjPool_1_t286528818 *, const MethodInfo*))ObjPool_1_PopObj_m3392359585_gshared)(__this, method)
// System.Void Pomelo.DotNetClient.ObjPool`1<System.Object>::PushObj(T)
extern "C"  void ObjPool_1_PushObj_m2019108635_gshared (ObjPool_1_t286528818 * __this, Il2CppObject * ___obj0, const MethodInfo* method);
#define ObjPool_1_PushObj_m2019108635(__this, ___obj0, method) ((  void (*) (ObjPool_1_t286528818 *, Il2CppObject *, const MethodInfo*))ObjPool_1_PushObj_m2019108635_gshared)(__this, ___obj0, method)
// System.Void Pomelo.DotNetClient.ObjPool`1<System.Object>::Dispose()
extern "C"  void ObjPool_1_Dispose_m4009443525_gshared (ObjPool_1_t286528818 * __this, const MethodInfo* method);
#define ObjPool_1_Dispose_m4009443525(__this, method) ((  void (*) (ObjPool_1_t286528818 *, const MethodInfo*))ObjPool_1_Dispose_m4009443525_gshared)(__this, method)
