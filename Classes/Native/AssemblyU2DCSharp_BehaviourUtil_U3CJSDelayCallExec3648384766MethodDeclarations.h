﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BehaviourUtil/<JSDelayCallExec>c__Iterator3A
struct U3CJSDelayCallExecU3Ec__Iterator3A_t3648384766;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void BehaviourUtil/<JSDelayCallExec>c__Iterator3A::.ctor()
extern "C"  void U3CJSDelayCallExecU3Ec__Iterator3A__ctor_m2517475677 (U3CJSDelayCallExecU3Ec__Iterator3A_t3648384766 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object BehaviourUtil/<JSDelayCallExec>c__Iterator3A::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CJSDelayCallExecU3Ec__Iterator3A_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1649135903 (U3CJSDelayCallExecU3Ec__Iterator3A_t3648384766 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object BehaviourUtil/<JSDelayCallExec>c__Iterator3A::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CJSDelayCallExecU3Ec__Iterator3A_System_Collections_IEnumerator_get_Current_m849998515 (U3CJSDelayCallExecU3Ec__Iterator3A_t3648384766 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BehaviourUtil/<JSDelayCallExec>c__Iterator3A::MoveNext()
extern "C"  bool U3CJSDelayCallExecU3Ec__Iterator3A_MoveNext_m3125017951 (U3CJSDelayCallExecU3Ec__Iterator3A_t3648384766 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BehaviourUtil/<JSDelayCallExec>c__Iterator3A::Dispose()
extern "C"  void U3CJSDelayCallExecU3Ec__Iterator3A_Dispose_m1125821018 (U3CJSDelayCallExecU3Ec__Iterator3A_t3648384766 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BehaviourUtil/<JSDelayCallExec>c__Iterator3A::Reset()
extern "C"  void U3CJSDelayCallExecU3Ec__Iterator3A_Reset_m163908618 (U3CJSDelayCallExecU3Ec__Iterator3A_t3648384766 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
