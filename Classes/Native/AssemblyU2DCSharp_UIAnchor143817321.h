﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Camera
struct Camera_t2727095145;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// UIWidget
struct UIWidget_t769069560;
// UnityEngine.Transform
struct Transform_t1659122786;
// UnityEngine.Animation
struct Animation_t1724966010;
// UIRoot
struct UIRoot_t2503447958;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_Rect4241904616.h"
#include "AssemblyU2DCSharp_UIAnchor_Side1741963613.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIAnchor
struct  UIAnchor_t143817321  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.Camera UIAnchor::uiCamera
	Camera_t2727095145 * ___uiCamera_4;
	// UnityEngine.GameObject UIAnchor::container
	GameObject_t3674682005 * ___container_5;
	// UIAnchor/Side UIAnchor::side
	int32_t ___side_6;
	// System.Boolean UIAnchor::runOnlyOnce
	bool ___runOnlyOnce_7;
	// UnityEngine.Vector2 UIAnchor::relativeOffset
	Vector2_t4282066565  ___relativeOffset_8;
	// UnityEngine.Vector2 UIAnchor::pixelOffset
	Vector2_t4282066565  ___pixelOffset_9;
	// UIWidget UIAnchor::widgetContainer
	UIWidget_t769069560 * ___widgetContainer_10;
	// UnityEngine.Transform UIAnchor::mTrans
	Transform_t1659122786 * ___mTrans_11;
	// UnityEngine.Animation UIAnchor::mAnim
	Animation_t1724966010 * ___mAnim_12;
	// UnityEngine.Rect UIAnchor::mRect
	Rect_t4241904616  ___mRect_13;
	// UIRoot UIAnchor::mRoot
	UIRoot_t2503447958 * ___mRoot_14;
	// System.Boolean UIAnchor::mStarted
	bool ___mStarted_15;

public:
	inline static int32_t get_offset_of_uiCamera_4() { return static_cast<int32_t>(offsetof(UIAnchor_t143817321, ___uiCamera_4)); }
	inline Camera_t2727095145 * get_uiCamera_4() const { return ___uiCamera_4; }
	inline Camera_t2727095145 ** get_address_of_uiCamera_4() { return &___uiCamera_4; }
	inline void set_uiCamera_4(Camera_t2727095145 * value)
	{
		___uiCamera_4 = value;
		Il2CppCodeGenWriteBarrier(&___uiCamera_4, value);
	}

	inline static int32_t get_offset_of_container_5() { return static_cast<int32_t>(offsetof(UIAnchor_t143817321, ___container_5)); }
	inline GameObject_t3674682005 * get_container_5() const { return ___container_5; }
	inline GameObject_t3674682005 ** get_address_of_container_5() { return &___container_5; }
	inline void set_container_5(GameObject_t3674682005 * value)
	{
		___container_5 = value;
		Il2CppCodeGenWriteBarrier(&___container_5, value);
	}

	inline static int32_t get_offset_of_side_6() { return static_cast<int32_t>(offsetof(UIAnchor_t143817321, ___side_6)); }
	inline int32_t get_side_6() const { return ___side_6; }
	inline int32_t* get_address_of_side_6() { return &___side_6; }
	inline void set_side_6(int32_t value)
	{
		___side_6 = value;
	}

	inline static int32_t get_offset_of_runOnlyOnce_7() { return static_cast<int32_t>(offsetof(UIAnchor_t143817321, ___runOnlyOnce_7)); }
	inline bool get_runOnlyOnce_7() const { return ___runOnlyOnce_7; }
	inline bool* get_address_of_runOnlyOnce_7() { return &___runOnlyOnce_7; }
	inline void set_runOnlyOnce_7(bool value)
	{
		___runOnlyOnce_7 = value;
	}

	inline static int32_t get_offset_of_relativeOffset_8() { return static_cast<int32_t>(offsetof(UIAnchor_t143817321, ___relativeOffset_8)); }
	inline Vector2_t4282066565  get_relativeOffset_8() const { return ___relativeOffset_8; }
	inline Vector2_t4282066565 * get_address_of_relativeOffset_8() { return &___relativeOffset_8; }
	inline void set_relativeOffset_8(Vector2_t4282066565  value)
	{
		___relativeOffset_8 = value;
	}

	inline static int32_t get_offset_of_pixelOffset_9() { return static_cast<int32_t>(offsetof(UIAnchor_t143817321, ___pixelOffset_9)); }
	inline Vector2_t4282066565  get_pixelOffset_9() const { return ___pixelOffset_9; }
	inline Vector2_t4282066565 * get_address_of_pixelOffset_9() { return &___pixelOffset_9; }
	inline void set_pixelOffset_9(Vector2_t4282066565  value)
	{
		___pixelOffset_9 = value;
	}

	inline static int32_t get_offset_of_widgetContainer_10() { return static_cast<int32_t>(offsetof(UIAnchor_t143817321, ___widgetContainer_10)); }
	inline UIWidget_t769069560 * get_widgetContainer_10() const { return ___widgetContainer_10; }
	inline UIWidget_t769069560 ** get_address_of_widgetContainer_10() { return &___widgetContainer_10; }
	inline void set_widgetContainer_10(UIWidget_t769069560 * value)
	{
		___widgetContainer_10 = value;
		Il2CppCodeGenWriteBarrier(&___widgetContainer_10, value);
	}

	inline static int32_t get_offset_of_mTrans_11() { return static_cast<int32_t>(offsetof(UIAnchor_t143817321, ___mTrans_11)); }
	inline Transform_t1659122786 * get_mTrans_11() const { return ___mTrans_11; }
	inline Transform_t1659122786 ** get_address_of_mTrans_11() { return &___mTrans_11; }
	inline void set_mTrans_11(Transform_t1659122786 * value)
	{
		___mTrans_11 = value;
		Il2CppCodeGenWriteBarrier(&___mTrans_11, value);
	}

	inline static int32_t get_offset_of_mAnim_12() { return static_cast<int32_t>(offsetof(UIAnchor_t143817321, ___mAnim_12)); }
	inline Animation_t1724966010 * get_mAnim_12() const { return ___mAnim_12; }
	inline Animation_t1724966010 ** get_address_of_mAnim_12() { return &___mAnim_12; }
	inline void set_mAnim_12(Animation_t1724966010 * value)
	{
		___mAnim_12 = value;
		Il2CppCodeGenWriteBarrier(&___mAnim_12, value);
	}

	inline static int32_t get_offset_of_mRect_13() { return static_cast<int32_t>(offsetof(UIAnchor_t143817321, ___mRect_13)); }
	inline Rect_t4241904616  get_mRect_13() const { return ___mRect_13; }
	inline Rect_t4241904616 * get_address_of_mRect_13() { return &___mRect_13; }
	inline void set_mRect_13(Rect_t4241904616  value)
	{
		___mRect_13 = value;
	}

	inline static int32_t get_offset_of_mRoot_14() { return static_cast<int32_t>(offsetof(UIAnchor_t143817321, ___mRoot_14)); }
	inline UIRoot_t2503447958 * get_mRoot_14() const { return ___mRoot_14; }
	inline UIRoot_t2503447958 ** get_address_of_mRoot_14() { return &___mRoot_14; }
	inline void set_mRoot_14(UIRoot_t2503447958 * value)
	{
		___mRoot_14 = value;
		Il2CppCodeGenWriteBarrier(&___mRoot_14, value);
	}

	inline static int32_t get_offset_of_mStarted_15() { return static_cast<int32_t>(offsetof(UIAnchor_t143817321, ___mStarted_15)); }
	inline bool get_mStarted_15() const { return ___mStarted_15; }
	inline bool* get_address_of_mStarted_15() { return &___mStarted_15; }
	inline void set_mStarted_15(bool value)
	{
		___mStarted_15 = value;
	}
};

struct UIAnchor_t143817321_StaticFields
{
public:
	// UnityEngine.Rect UIAnchor::safeArea
	Rect_t4241904616  ___safeArea_2;
	// System.Boolean UIAnchor::isInitSafeArea
	bool ___isInitSafeArea_3;

public:
	inline static int32_t get_offset_of_safeArea_2() { return static_cast<int32_t>(offsetof(UIAnchor_t143817321_StaticFields, ___safeArea_2)); }
	inline Rect_t4241904616  get_safeArea_2() const { return ___safeArea_2; }
	inline Rect_t4241904616 * get_address_of_safeArea_2() { return &___safeArea_2; }
	inline void set_safeArea_2(Rect_t4241904616  value)
	{
		___safeArea_2 = value;
	}

	inline static int32_t get_offset_of_isInitSafeArea_3() { return static_cast<int32_t>(offsetof(UIAnchor_t143817321_StaticFields, ___isInitSafeArea_3)); }
	inline bool get_isInitSafeArea_3() const { return ___isInitSafeArea_3; }
	inline bool* get_address_of_isInitSafeArea_3() { return &___isInitSafeArea_3; }
	inline void set_isInitSafeArea_3(bool value)
	{
		___isInitSafeArea_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
