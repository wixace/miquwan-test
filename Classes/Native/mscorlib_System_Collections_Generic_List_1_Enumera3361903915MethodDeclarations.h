﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<Pathfinding.Int2>
struct List_1_t3342231145;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3361903915.h"
#include "AssemblyU2DCSharp_Pathfinding_Int21974045593.h"

// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.Int2>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m44454944_gshared (Enumerator_t3361903915 * __this, List_1_t3342231145 * ___l0, const MethodInfo* method);
#define Enumerator__ctor_m44454944(__this, ___l0, method) ((  void (*) (Enumerator_t3361903915 *, List_1_t3342231145 *, const MethodInfo*))Enumerator__ctor_m44454944_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.Int2>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3211570226_gshared (Enumerator_t3361903915 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m3211570226(__this, method) ((  void (*) (Enumerator_t3361903915 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3211570226_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Pathfinding.Int2>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1063668136_gshared (Enumerator_t3361903915 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1063668136(__this, method) ((  Il2CppObject * (*) (Enumerator_t3361903915 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1063668136_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.Int2>::Dispose()
extern "C"  void Enumerator_Dispose_m1419897349_gshared (Enumerator_t3361903915 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1419897349(__this, method) ((  void (*) (Enumerator_t3361903915 *, const MethodInfo*))Enumerator_Dispose_m1419897349_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.Int2>::VerifyState()
extern "C"  void Enumerator_VerifyState_m1729568446_gshared (Enumerator_t3361903915 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m1729568446(__this, method) ((  void (*) (Enumerator_t3361903915 *, const MethodInfo*))Enumerator_VerifyState_m1729568446_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Pathfinding.Int2>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m249324770_gshared (Enumerator_t3361903915 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m249324770(__this, method) ((  bool (*) (Enumerator_t3361903915 *, const MethodInfo*))Enumerator_MoveNext_m249324770_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Pathfinding.Int2>::get_Current()
extern "C"  Int2_t1974045593  Enumerator_get_Current_m3327185495_gshared (Enumerator_t3361903915 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m3327185495(__this, method) ((  Int2_t1974045593  (*) (Enumerator_t3361903915 *, const MethodInfo*))Enumerator_get_Current_m3327185495_gshared)(__this, method)
