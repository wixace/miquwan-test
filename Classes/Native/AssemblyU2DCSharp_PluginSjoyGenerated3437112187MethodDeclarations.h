﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PluginSjoyGenerated
struct PluginSjoyGenerated_t3437112187;
// JSVCall
struct JSVCall_t3708497963;
// PluginSjoy
struct PluginSjoy_t1606406260;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;
// CEvent.ZEvent
struct ZEvent_t3638018500;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_PluginSjoy1606406260.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"

// System.Void PluginSjoyGenerated::.ctor()
extern "C"  void PluginSjoyGenerated__ctor_m608805456 (PluginSjoyGenerated_t3437112187 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginSjoyGenerated::PluginSjoy_PluginSjoy1(JSVCall,System.Int32)
extern "C"  bool PluginSjoyGenerated_PluginSjoy_PluginSjoy1_m3614636002 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginSjoyGenerated::PluginSjoy_tokenId(JSVCall)
extern "C"  void PluginSjoyGenerated_PluginSjoy_tokenId_m724664714 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginSjoyGenerated::PluginSjoy_CreatRole__ZEvent(JSVCall,System.Int32)
extern "C"  bool PluginSjoyGenerated_PluginSjoy_CreatRole__ZEvent_m1142207716 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginSjoyGenerated::PluginSjoy_Init(JSVCall,System.Int32)
extern "C"  bool PluginSjoyGenerated_PluginSjoy_Init_m1204209773 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginSjoyGenerated::PluginSjoy_IsLoginSuccess__Boolean(JSVCall,System.Int32)
extern "C"  bool PluginSjoyGenerated_PluginSjoy_IsLoginSuccess__Boolean_m710319689 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginSjoyGenerated::PluginSjoy_OnDestory(JSVCall,System.Int32)
extern "C"  bool PluginSjoyGenerated_PluginSjoy_OnDestory_m2805826234 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginSjoyGenerated::PluginSjoy_OpenUserLogin(JSVCall,System.Int32)
extern "C"  bool PluginSjoyGenerated_PluginSjoy_OpenUserLogin_m1081737337 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginSjoyGenerated::PluginSjoy_OpenUserSwitch(JSVCall,System.Int32)
extern "C"  bool PluginSjoyGenerated_PluginSjoy_OpenUserSwitch_m19721318 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginSjoyGenerated::PluginSjoy_ReqSDKHttpLogin(JSVCall,System.Int32)
extern "C"  bool PluginSjoyGenerated_PluginSjoy_ReqSDKHttpLogin_m3039008234 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginSjoyGenerated::PluginSjoy_RoleEnterGame__ZEvent(JSVCall,System.Int32)
extern "C"  bool PluginSjoyGenerated_PluginSjoy_RoleEnterGame__ZEvent_m113141401 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginSjoyGenerated::PluginSjoy_RoleUpgrade__ZEvent(JSVCall,System.Int32)
extern "C"  bool PluginSjoyGenerated_PluginSjoy_RoleUpgrade__ZEvent_m1749361099 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginSjoyGenerated::PluginSjoy_UserPay__ZEvent(JSVCall,System.Int32)
extern "C"  bool PluginSjoyGenerated_PluginSjoy_UserPay__ZEvent_m1944890786 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginSjoyGenerated::__Register()
extern "C"  void PluginSjoyGenerated___Register_m2156973175 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginSjoyGenerated::ilo_attachFinalizerObject1(System.Int32)
extern "C"  bool PluginSjoyGenerated_ilo_attachFinalizerObject1_m3872982951 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginSjoyGenerated::ilo_getBooleanS2(System.Int32)
extern "C"  bool PluginSjoyGenerated_ilo_getBooleanS2_m2219019477 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginSjoyGenerated::ilo_OpenUserSwitch3(PluginSjoy)
extern "C"  void PluginSjoyGenerated_ilo_OpenUserSwitch3_m1124944881 (Il2CppObject * __this /* static, unused */, PluginSjoy_t1606406260 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PluginSjoyGenerated::ilo_setObject4(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t PluginSjoyGenerated_ilo_setObject4_m2299166497 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginSjoyGenerated::ilo_RoleEnterGame5(PluginSjoy,CEvent.ZEvent)
extern "C"  void PluginSjoyGenerated_ilo_RoleEnterGame5_m958468425 (Il2CppObject * __this /* static, unused */, PluginSjoy_t1606406260 * ____this0, ZEvent_t3638018500 * ___ev1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PluginSjoyGenerated::ilo_getObject6(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * PluginSjoyGenerated_ilo_getObject6_m3425604634 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginSjoyGenerated::ilo_RoleUpgrade7(PluginSjoy,CEvent.ZEvent)
extern "C"  void PluginSjoyGenerated_ilo_RoleUpgrade7_m4092179289 (Il2CppObject * __this /* static, unused */, PluginSjoy_t1606406260 * ____this0, ZEvent_t3638018500 * ___ev1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginSjoyGenerated::ilo_UserPay8(PluginSjoy,CEvent.ZEvent)
extern "C"  void PluginSjoyGenerated_ilo_UserPay8_m3554458787 (Il2CppObject * __this /* static, unused */, PluginSjoy_t1606406260 * ____this0, ZEvent_t3638018500 * ___ev1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
