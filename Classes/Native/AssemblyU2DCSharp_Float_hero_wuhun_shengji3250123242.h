﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UILabel
struct UILabel_t291504320;

#include "AssemblyU2DCSharp_FloatTextUnit2362298029.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Float_hero_wuhun_shengji
struct  Float_hero_wuhun_shengji_t3250123242  : public FloatTextUnit_t2362298029
{
public:
	// UILabel Float_hero_wuhun_shengji::_label_file
	UILabel_t291504320 * ____label_file_3;

public:
	inline static int32_t get_offset_of__label_file_3() { return static_cast<int32_t>(offsetof(Float_hero_wuhun_shengji_t3250123242, ____label_file_3)); }
	inline UILabel_t291504320 * get__label_file_3() const { return ____label_file_3; }
	inline UILabel_t291504320 ** get_address_of__label_file_3() { return &____label_file_3; }
	inline void set__label_file_3(UILabel_t291504320 * value)
	{
		____label_file_3 = value;
		Il2CppCodeGenWriteBarrier(&____label_file_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
