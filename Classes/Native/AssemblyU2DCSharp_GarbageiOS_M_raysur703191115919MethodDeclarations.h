﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_raysur70
struct M_raysur70_t3191115919;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"

// System.Void GarbageiOS.M_raysur70::.ctor()
extern "C"  void M_raysur70__ctor_m2581825156 (M_raysur70_t3191115919 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_raysur70::M_youtirHurja0(System.String[],System.Int32)
extern "C"  void M_raysur70_M_youtirHurja0_m830277307 (M_raysur70_t3191115919 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_raysur70::M_gourisarGipitas1(System.String[],System.Int32)
extern "C"  void M_raysur70_M_gourisarGipitas1_m1216234223 (M_raysur70_t3191115919 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_raysur70::M_barni2(System.String[],System.Int32)
extern "C"  void M_raysur70_M_barni2_m4223162477 (M_raysur70_t3191115919 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_raysur70::M_somowdrear3(System.String[],System.Int32)
extern "C"  void M_raysur70_M_somowdrear3_m4140590123 (M_raysur70_t3191115919 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_raysur70::M_harmem4(System.String[],System.Int32)
extern "C"  void M_raysur70_M_harmem4_m990821919 (M_raysur70_t3191115919 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_raysur70::M_wekici5(System.String[],System.Int32)
extern "C"  void M_raysur70_M_wekici5_m1726809642 (M_raysur70_t3191115919 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
