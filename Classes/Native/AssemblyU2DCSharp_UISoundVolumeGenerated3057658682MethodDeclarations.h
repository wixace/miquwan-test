﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UISoundVolumeGenerated
struct UISoundVolumeGenerated_t3057658682;
// JSVCall
struct JSVCall_t3708497963;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"

// System.Void UISoundVolumeGenerated::.ctor()
extern "C"  void UISoundVolumeGenerated__ctor_m2673871521 (UISoundVolumeGenerated_t3057658682 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UISoundVolumeGenerated::UISoundVolume_UISoundVolume1(JSVCall,System.Int32)
extern "C"  bool UISoundVolumeGenerated_UISoundVolume_UISoundVolume1_m1929792845 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UISoundVolumeGenerated::__Register()
extern "C"  void UISoundVolumeGenerated___Register_m2238967494 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UISoundVolumeGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UISoundVolumeGenerated_ilo_getObject1_m2017317009 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
