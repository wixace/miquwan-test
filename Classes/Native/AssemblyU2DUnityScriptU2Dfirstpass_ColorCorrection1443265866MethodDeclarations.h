﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ColorCorrectionLut
struct ColorCorrectionLut_t1443265866;
// UnityEngine.Texture2D
struct Texture2D_t3884108195;
// System.String
struct String_t;
// UnityEngine.RenderTexture
struct RenderTexture_t1963041563;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Texture2D3884108195.h"
#include "mscorlib_System_String7231557.h"
#include "UnityEngine_UnityEngine_RenderTexture1963041563.h"

// System.Void ColorCorrectionLut::.ctor()
extern "C"  void ColorCorrectionLut__ctor_m2519635130 (ColorCorrectionLut_t1443265866 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ColorCorrectionLut::CheckResources()
extern "C"  bool ColorCorrectionLut_CheckResources_m1196389933 (ColorCorrectionLut_t1443265866 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ColorCorrectionLut::OnDisable()
extern "C"  void ColorCorrectionLut_OnDisable_m218692641 (ColorCorrectionLut_t1443265866 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ColorCorrectionLut::OnDestroy()
extern "C"  void ColorCorrectionLut_OnDestroy_m2396224691 (ColorCorrectionLut_t1443265866 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ColorCorrectionLut::SetIdentityLut()
extern "C"  void ColorCorrectionLut_SetIdentityLut_m3536906229 (ColorCorrectionLut_t1443265866 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ColorCorrectionLut::ValidDimensions(UnityEngine.Texture2D)
extern "C"  bool ColorCorrectionLut_ValidDimensions_m3371052513 (ColorCorrectionLut_t1443265866 * __this, Texture2D_t3884108195 * ___tex2d0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ColorCorrectionLut::Convert(UnityEngine.Texture2D,System.String)
extern "C"  void ColorCorrectionLut_Convert_m4237580397 (ColorCorrectionLut_t1443265866 * __this, Texture2D_t3884108195 * ___temp2DTex0, String_t* ___path1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ColorCorrectionLut::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void ColorCorrectionLut_OnRenderImage_m811314020 (ColorCorrectionLut_t1443265866 * __this, RenderTexture_t1963041563 * ___source0, RenderTexture_t1963041563 * ___destination1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ColorCorrectionLut::Main()
extern "C"  void ColorCorrectionLut_Main_m689509571 (ColorCorrectionLut_t1443265866 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
