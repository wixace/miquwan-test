﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Collider
struct Collider_t2939674232;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_Bounds2711641849.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DynamicGridObstacle
struct  DynamicGridObstacle_t1961628084  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.Collider DynamicGridObstacle::col
	Collider_t2939674232 * ___col_2;
	// System.Single DynamicGridObstacle::updateError
	float ___updateError_3;
	// System.Single DynamicGridObstacle::checkTime
	float ___checkTime_4;
	// UnityEngine.Bounds DynamicGridObstacle::prevBounds
	Bounds_t2711641849  ___prevBounds_5;
	// System.Boolean DynamicGridObstacle::isWaitingForUpdate
	bool ___isWaitingForUpdate_6;

public:
	inline static int32_t get_offset_of_col_2() { return static_cast<int32_t>(offsetof(DynamicGridObstacle_t1961628084, ___col_2)); }
	inline Collider_t2939674232 * get_col_2() const { return ___col_2; }
	inline Collider_t2939674232 ** get_address_of_col_2() { return &___col_2; }
	inline void set_col_2(Collider_t2939674232 * value)
	{
		___col_2 = value;
		Il2CppCodeGenWriteBarrier(&___col_2, value);
	}

	inline static int32_t get_offset_of_updateError_3() { return static_cast<int32_t>(offsetof(DynamicGridObstacle_t1961628084, ___updateError_3)); }
	inline float get_updateError_3() const { return ___updateError_3; }
	inline float* get_address_of_updateError_3() { return &___updateError_3; }
	inline void set_updateError_3(float value)
	{
		___updateError_3 = value;
	}

	inline static int32_t get_offset_of_checkTime_4() { return static_cast<int32_t>(offsetof(DynamicGridObstacle_t1961628084, ___checkTime_4)); }
	inline float get_checkTime_4() const { return ___checkTime_4; }
	inline float* get_address_of_checkTime_4() { return &___checkTime_4; }
	inline void set_checkTime_4(float value)
	{
		___checkTime_4 = value;
	}

	inline static int32_t get_offset_of_prevBounds_5() { return static_cast<int32_t>(offsetof(DynamicGridObstacle_t1961628084, ___prevBounds_5)); }
	inline Bounds_t2711641849  get_prevBounds_5() const { return ___prevBounds_5; }
	inline Bounds_t2711641849 * get_address_of_prevBounds_5() { return &___prevBounds_5; }
	inline void set_prevBounds_5(Bounds_t2711641849  value)
	{
		___prevBounds_5 = value;
	}

	inline static int32_t get_offset_of_isWaitingForUpdate_6() { return static_cast<int32_t>(offsetof(DynamicGridObstacle_t1961628084, ___isWaitingForUpdate_6)); }
	inline bool get_isWaitingForUpdate_6() const { return ___isWaitingForUpdate_6; }
	inline bool* get_address_of_isWaitingForUpdate_6() { return &___isWaitingForUpdate_6; }
	inline void set_isWaitingForUpdate_6(bool value)
	{
		___isWaitingForUpdate_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
