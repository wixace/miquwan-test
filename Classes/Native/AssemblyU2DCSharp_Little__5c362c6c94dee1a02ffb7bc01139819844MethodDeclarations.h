﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._5c362c6c94dee1a02ffb7bc0c96fa998
struct _5c362c6c94dee1a02ffb7bc0c96fa998_t1139819844;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._5c362c6c94dee1a02ffb7bc0c96fa998::.ctor()
extern "C"  void _5c362c6c94dee1a02ffb7bc0c96fa998__ctor_m2496826729 (_5c362c6c94dee1a02ffb7bc0c96fa998_t1139819844 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._5c362c6c94dee1a02ffb7bc0c96fa998::_5c362c6c94dee1a02ffb7bc0c96fa998m2(System.Int32)
extern "C"  int32_t _5c362c6c94dee1a02ffb7bc0c96fa998__5c362c6c94dee1a02ffb7bc0c96fa998m2_m1738074905 (_5c362c6c94dee1a02ffb7bc0c96fa998_t1139819844 * __this, int32_t ____5c362c6c94dee1a02ffb7bc0c96fa998a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._5c362c6c94dee1a02ffb7bc0c96fa998::_5c362c6c94dee1a02ffb7bc0c96fa998m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _5c362c6c94dee1a02ffb7bc0c96fa998__5c362c6c94dee1a02ffb7bc0c96fa998m_m3303762877 (_5c362c6c94dee1a02ffb7bc0c96fa998_t1139819844 * __this, int32_t ____5c362c6c94dee1a02ffb7bc0c96fa998a0, int32_t ____5c362c6c94dee1a02ffb7bc0c96fa998691, int32_t ____5c362c6c94dee1a02ffb7bc0c96fa998c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
