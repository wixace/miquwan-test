﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>
struct Dictionary_2_t509053110;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V2735853814.h"
#include "AssemblyU2DCSharp_Mihua_Assets_SubAssetMgr_ErrorIn2633981210.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m1326160823_gshared (Enumerator_t2735853814 * __this, Dictionary_2_t509053110 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m1326160823(__this, ___host0, method) ((  void (*) (Enumerator_t2735853814 *, Dictionary_2_t509053110 *, const MethodInfo*))Enumerator__ctor_m1326160823_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2421941076_gshared (Enumerator_t2735853814 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m2421941076(__this, method) ((  Il2CppObject * (*) (Enumerator_t2735853814 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2421941076_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m761671838_gshared (Enumerator_t2735853814 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m761671838(__this, method) ((  void (*) (Enumerator_t2735853814 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m761671838_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::Dispose()
extern "C"  void Enumerator_Dispose_m3522186777_gshared (Enumerator_t2735853814 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m3522186777(__this, method) ((  void (*) (Enumerator_t2735853814 *, const MethodInfo*))Enumerator_Dispose_m3522186777_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m296668622_gshared (Enumerator_t2735853814 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m296668622(__this, method) ((  bool (*) (Enumerator_t2735853814 *, const MethodInfo*))Enumerator_MoveNext_m296668622_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::get_Current()
extern "C"  ErrorInfo_t2633981210  Enumerator_get_Current_m3910614684_gshared (Enumerator_t2735853814 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m3910614684(__this, method) ((  ErrorInfo_t2633981210  (*) (Enumerator_t2735853814 *, const MethodInfo*))Enumerator_get_Current_m3910614684_gshared)(__this, method)
