﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Seeker
struct Seeker_t2472610117;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.PathModifier
struct  PathModifier_t2857972114  : public Il2CppObject
{
public:
	// System.Int32 Pathfinding.PathModifier::priority
	int32_t ___priority_0;
	// Seeker Pathfinding.PathModifier::seeker
	Seeker_t2472610117 * ___seeker_1;

public:
	inline static int32_t get_offset_of_priority_0() { return static_cast<int32_t>(offsetof(PathModifier_t2857972114, ___priority_0)); }
	inline int32_t get_priority_0() const { return ___priority_0; }
	inline int32_t* get_address_of_priority_0() { return &___priority_0; }
	inline void set_priority_0(int32_t value)
	{
		___priority_0 = value;
	}

	inline static int32_t get_offset_of_seeker_1() { return static_cast<int32_t>(offsetof(PathModifier_t2857972114, ___seeker_1)); }
	inline Seeker_t2472610117 * get_seeker_1() const { return ___seeker_1; }
	inline Seeker_t2472610117 ** get_address_of_seeker_1() { return &___seeker_1; }
	inline void set_seeker_1(Seeker_t2472610117 * value)
	{
		___seeker_1 = value;
		Il2CppCodeGenWriteBarrier(&___seeker_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
