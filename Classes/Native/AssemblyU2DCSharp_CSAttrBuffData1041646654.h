﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CSAttrBuffData
struct  CSAttrBuffData_t1041646654  : public Il2CppObject
{
public:
	// System.UInt32 CSAttrBuffData::buffID
	uint32_t ___buffID_0;
	// System.UInt32 CSAttrBuffData::buffCount
	uint32_t ___buffCount_1;

public:
	inline static int32_t get_offset_of_buffID_0() { return static_cast<int32_t>(offsetof(CSAttrBuffData_t1041646654, ___buffID_0)); }
	inline uint32_t get_buffID_0() const { return ___buffID_0; }
	inline uint32_t* get_address_of_buffID_0() { return &___buffID_0; }
	inline void set_buffID_0(uint32_t value)
	{
		___buffID_0 = value;
	}

	inline static int32_t get_offset_of_buffCount_1() { return static_cast<int32_t>(offsetof(CSAttrBuffData_t1041646654, ___buffCount_1)); }
	inline uint32_t get_buffCount_1() const { return ___buffCount_1; }
	inline uint32_t* get_address_of_buffCount_1() { return &___buffCount_1; }
	inline void set_buffCount_1(uint32_t value)
	{
		___buffCount_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
