﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_palljorkearNoulai64
struct M_palljorkearNoulai64_t2138697573;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_palljorkearNoulai642138697573.h"

// System.Void GarbageiOS.M_palljorkearNoulai64::.ctor()
extern "C"  void M_palljorkearNoulai64__ctor_m2601219486 (M_palljorkearNoulai64_t2138697573 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_palljorkearNoulai64::M_helduweTogija0(System.String[],System.Int32)
extern "C"  void M_palljorkearNoulai64_M_helduweTogija0_m2392687165 (M_palljorkearNoulai64_t2138697573 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_palljorkearNoulai64::M_gofaiTembis1(System.String[],System.Int32)
extern "C"  void M_palljorkearNoulai64_M_gofaiTembis1_m596840554 (M_palljorkearNoulai64_t2138697573 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_palljorkearNoulai64::M_jirqiCalwhalray2(System.String[],System.Int32)
extern "C"  void M_palljorkearNoulai64_M_jirqiCalwhalray2_m1662674838 (M_palljorkearNoulai64_t2138697573 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_palljorkearNoulai64::M_lukapis3(System.String[],System.Int32)
extern "C"  void M_palljorkearNoulai64_M_lukapis3_m1165676391 (M_palljorkearNoulai64_t2138697573 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_palljorkearNoulai64::ilo_M_gofaiTembis11(GarbageiOS.M_palljorkearNoulai64,System.String[],System.Int32)
extern "C"  void M_palljorkearNoulai64_ilo_M_gofaiTembis11_m2495697293 (Il2CppObject * __this /* static, unused */, M_palljorkearNoulai64_t2138697573 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_palljorkearNoulai64::ilo_M_jirqiCalwhalray22(GarbageiOS.M_palljorkearNoulai64,System.String[],System.Int32)
extern "C"  void M_palljorkearNoulai64_ilo_M_jirqiCalwhalray22_m1349939224 (Il2CppObject * __this /* static, unused */, M_palljorkearNoulai64_t2138697573 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
