﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1/DefaultComparer<Pathfinding.LocalAvoidance/VOLine>
struct DefaultComparer_t538177578;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_LocalAvoidance_VOLin2029931801.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<Pathfinding.LocalAvoidance/VOLine>::.ctor()
extern "C"  void DefaultComparer__ctor_m3749105981_gshared (DefaultComparer_t538177578 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m3749105981(__this, method) ((  void (*) (DefaultComparer_t538177578 *, const MethodInfo*))DefaultComparer__ctor_m3749105981_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<Pathfinding.LocalAvoidance/VOLine>::Compare(T,T)
extern "C"  int32_t DefaultComparer_Compare_m1423635218_gshared (DefaultComparer_t538177578 * __this, VOLine_t2029931801  ___x0, VOLine_t2029931801  ___y1, const MethodInfo* method);
#define DefaultComparer_Compare_m1423635218(__this, ___x0, ___y1, method) ((  int32_t (*) (DefaultComparer_t538177578 *, VOLine_t2029931801 , VOLine_t2029931801 , const MethodInfo*))DefaultComparer_Compare_m1423635218_gshared)(__this, ___x0, ___y1, method)
