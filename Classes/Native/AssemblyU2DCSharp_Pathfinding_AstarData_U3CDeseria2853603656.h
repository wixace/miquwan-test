﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.AstarData/<DeserializeGraphsPartAdditive>c__AnonStorey103
struct  U3CDeserializeGraphsPartAdditiveU3Ec__AnonStorey103_t2853603656  : public Il2CppObject
{
public:
	// System.Int32 Pathfinding.AstarData/<DeserializeGraphsPartAdditive>c__AnonStorey103::i
	int32_t ___i_0;

public:
	inline static int32_t get_offset_of_i_0() { return static_cast<int32_t>(offsetof(U3CDeserializeGraphsPartAdditiveU3Ec__AnonStorey103_t2853603656, ___i_0)); }
	inline int32_t get_i_0() const { return ___i_0; }
	inline int32_t* get_address_of_i_0() { return &___i_0; }
	inline void set_i_0(int32_t value)
	{
		___i_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
