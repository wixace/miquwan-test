﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action_3_gen2073482325MethodDeclarations.h"

// System.Void System.Action`3<CombatEntity,System.Int32,System.Int32>::.ctor(System.Object,System.IntPtr)
#define Action_3__ctor_m3561158634(__this, ___object0, ___method1, method) ((  void (*) (Action_3_t1827703129 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Action_3__ctor_m3769249099_gshared)(__this, ___object0, ___method1, method)
// System.Void System.Action`3<CombatEntity,System.Int32,System.Int32>::Invoke(T1,T2,T3)
#define Action_3_Invoke_m870198806(__this, ___arg10, ___arg21, ___arg32, method) ((  void (*) (Action_3_t1827703129 *, CombatEntity_t684137495 *, int32_t, int32_t, const MethodInfo*))Action_3_Invoke_m2878702791_gshared)(__this, ___arg10, ___arg21, ___arg32, method)
// System.IAsyncResult System.Action`3<CombatEntity,System.Int32,System.Int32>::BeginInvoke(T1,T2,T3,System.AsyncCallback,System.Object)
#define Action_3_BeginInvoke_m86632389(__this, ___arg10, ___arg21, ___arg32, ___callback3, ___object4, method) ((  Il2CppObject * (*) (Action_3_t1827703129 *, CombatEntity_t684137495 *, int32_t, int32_t, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))Action_3_BeginInvoke_m1610368766_gshared)(__this, ___arg10, ___arg21, ___arg32, ___callback3, ___object4, method)
// System.Void System.Action`3<CombatEntity,System.Int32,System.Int32>::EndInvoke(System.IAsyncResult)
#define Action_3_EndInvoke_m1548267498(__this, ___result0, method) ((  void (*) (Action_3_t1827703129 *, Il2CppObject *, const MethodInfo*))Action_3_EndInvoke_m1579547867_gshared)(__this, ___result0, method)
