﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_gowhe30
struct M_gowhe30_t2723608169;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"

// System.Void GarbageiOS.M_gowhe30::.ctor()
extern "C"  void M_gowhe30__ctor_m267286298 (M_gowhe30_t2723608169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_gowhe30::M_meetaseGelsi0(System.String[],System.Int32)
extern "C"  void M_gowhe30_M_meetaseGelsi0_m480677461 (M_gowhe30_t2723608169 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
