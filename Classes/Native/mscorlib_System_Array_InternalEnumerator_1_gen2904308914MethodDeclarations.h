﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2904308914.h"
#include "mscorlib_System_Array1146569071.h"
#include "UnityEngine_UnityEngine_MeshSubsetCombineUtility_M4121966238.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3346397859_gshared (InternalEnumerator_1_t2904308914 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m3346397859(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t2904308914 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m3346397859_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2250078749_gshared (InternalEnumerator_1_t2904308914 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2250078749(__this, method) ((  void (*) (InternalEnumerator_1_t2904308914 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2250078749_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2493113747_gshared (InternalEnumerator_1_t2904308914 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2493113747(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t2904308914 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2493113747_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m306897978_gshared (InternalEnumerator_1_t2904308914 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m306897978(__this, method) ((  void (*) (InternalEnumerator_1_t2904308914 *, const MethodInfo*))InternalEnumerator_1_Dispose_m306897978_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m634618829_gshared (InternalEnumerator_1_t2904308914 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m634618829(__this, method) ((  bool (*) (InternalEnumerator_1_t2904308914 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m634618829_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::get_Current()
extern "C"  MeshInstance_t4121966238  InternalEnumerator_1_get_Current_m3097021260_gshared (InternalEnumerator_1_t2904308914 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m3097021260(__this, method) ((  MeshInstance_t4121966238  (*) (InternalEnumerator_1_t2904308914 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3097021260_gshared)(__this, method)
