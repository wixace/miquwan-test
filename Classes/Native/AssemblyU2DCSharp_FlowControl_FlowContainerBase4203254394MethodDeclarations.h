﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FlowControl.FlowContainerBase
struct FlowContainerBase_t4203254394;
// FlowControl.IFlowBase
struct IFlowBase_t1464761278;
// System.Collections.Generic.List`1<FlowControl.IFlowBase>
struct List_1_t2832946830;
// FlowControl.IContainer
struct IContainer_t4103575436;
// IZUpdate
struct IZUpdate_t3482043738;

#include "codegen/il2cpp-codegen.h"

// System.Void FlowControl.FlowContainerBase::.ctor()
extern "C"  void FlowContainerBase__ctor_m977206982 (FlowContainerBase_t4203254394 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlowControl.FlowContainerBase::AddFlow(FlowControl.IFlowBase)
extern "C"  void FlowContainerBase_AddFlow_m2247252310 (FlowContainerBase_t4203254394 * __this, Il2CppObject * ___flow0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<FlowControl.IFlowBase> FlowControl.FlowContainerBase::GetChilds()
extern "C"  List_1_t2832946830 * FlowContainerBase_GetChilds_m2334753275 (FlowContainerBase_t4203254394 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlowControl.FlowContainerBase::RemoveFlow(FlowControl.IFlowBase)
extern "C"  void FlowContainerBase_RemoveFlow_m236989177 (FlowContainerBase_t4203254394 * __this, Il2CppObject * ___flow0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 FlowControl.FlowContainerBase::get_NumChildren()
extern "C"  int32_t FlowContainerBase_get_NumChildren_m997255022 (FlowContainerBase_t4203254394 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlowControl.FlowContainerBase::OnComplete()
extern "C"  void FlowContainerBase_OnComplete_m812622390 (FlowContainerBase_t4203254394 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlowControl.FlowContainerBase::FlowUpdate(System.Single)
extern "C"  void FlowContainerBase_FlowUpdate_m2797594102 (FlowContainerBase_t4203254394 * __this, float ___duringTime0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlowControl.FlowContainerBase::ZUpdate()
extern "C"  void FlowContainerBase_ZUpdate_m2361568327 (FlowContainerBase_t4203254394 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<FlowControl.IFlowBase> FlowControl.FlowContainerBase::ilo_GetChilds1(FlowControl.IContainer)
extern "C"  List_1_t2832946830 * FlowContainerBase_ilo_GetChilds1_m3263537012 (Il2CppObject * __this /* static, unused */, Il2CppObject * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlowControl.FlowContainerBase::ilo_RemoveUpdate2(IZUpdate)
extern "C"  void FlowContainerBase_ilo_RemoveUpdate2_m2977321948 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___update0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
