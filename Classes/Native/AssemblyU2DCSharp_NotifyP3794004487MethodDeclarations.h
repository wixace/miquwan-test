﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// NotifyP
struct NotifyP_t3794004487;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_NotifyP3794004487.h"

// System.Void NotifyP::.ctor()
extern "C"  void NotifyP__ctor_m1456381252 (NotifyP_t3794004487 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NotifyP::.cctor()
extern "C"  void NotifyP__cctor_m1716049641 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 NotifyP::get_ID()
extern "C"  uint32_t NotifyP_get_ID_m4187986569 (NotifyP_t3794004487 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NotifyP::set_ID(System.UInt32)
extern "C"  void NotifyP_set_ID_m975361090 (NotifyP_t3794004487 * __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NotifyP::OnDestroy()
extern "C"  void NotifyP_OnDestroy_m2009608253 (NotifyP_t3794004487 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 NotifyP::ilo_get_ID1(NotifyP)
extern "C"  uint32_t NotifyP_ilo_get_ID1_m9890260 (Il2CppObject * __this /* static, unused */, NotifyP_t3794004487 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
