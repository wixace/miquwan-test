﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._31b8973e4e93620bc70503232dab3578
struct _31b8973e4e93620bc70503232dab3578_t3300672265;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._31b8973e4e93620bc70503232dab3578::.ctor()
extern "C"  void _31b8973e4e93620bc70503232dab3578__ctor_m1505180996 (_31b8973e4e93620bc70503232dab3578_t3300672265 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._31b8973e4e93620bc70503232dab3578::_31b8973e4e93620bc70503232dab3578m2(System.Int32)
extern "C"  int32_t _31b8973e4e93620bc70503232dab3578__31b8973e4e93620bc70503232dab3578m2_m655781113 (_31b8973e4e93620bc70503232dab3578_t3300672265 * __this, int32_t ____31b8973e4e93620bc70503232dab3578a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._31b8973e4e93620bc70503232dab3578::_31b8973e4e93620bc70503232dab3578m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _31b8973e4e93620bc70503232dab3578__31b8973e4e93620bc70503232dab3578m_m2635711965 (_31b8973e4e93620bc70503232dab3578_t3300672265 * __this, int32_t ____31b8973e4e93620bc70503232dab3578a0, int32_t ____31b8973e4e93620bc70503232dab3578301, int32_t ____31b8973e4e93620bc70503232dab3578c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
