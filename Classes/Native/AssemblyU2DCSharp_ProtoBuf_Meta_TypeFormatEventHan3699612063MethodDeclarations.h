﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ProtoBuf.Meta.TypeFormatEventHandler
struct TypeFormatEventHandler_t3699612063;
// System.Object
struct Il2CppObject;
// ProtoBuf.Meta.TypeFormatEventArgs
struct TypeFormatEventArgs_t2866968344;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "AssemblyU2DCSharp_ProtoBuf_Meta_TypeFormatEventArg2866968344.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void ProtoBuf.Meta.TypeFormatEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void TypeFormatEventHandler__ctor_m989654286 (TypeFormatEventHandler_t3699612063 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.TypeFormatEventHandler::Invoke(System.Object,ProtoBuf.Meta.TypeFormatEventArgs)
extern "C"  void TypeFormatEventHandler_Invoke_m3726680760 (TypeFormatEventHandler_t3699612063 * __this, Il2CppObject * ___sender0, TypeFormatEventArgs_t2866968344 * ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult ProtoBuf.Meta.TypeFormatEventHandler::BeginInvoke(System.Object,ProtoBuf.Meta.TypeFormatEventArgs,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * TypeFormatEventHandler_BeginInvoke_m10732929 (TypeFormatEventHandler_t3699612063 * __this, Il2CppObject * ___sender0, TypeFormatEventArgs_t2866968344 * ___args1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.TypeFormatEventHandler::EndInvoke(System.IAsyncResult)
extern "C"  void TypeFormatEventHandler_EndInvoke_m402462238 (TypeFormatEventHandler_t3699612063 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
