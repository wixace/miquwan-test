﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SevenZip.CommandLineParser.Parser
struct Parser_t3511445845;
// System.String
struct String_t;
// SevenZip.CommandLineParser.SwitchForm[]
struct SwitchFormU5BU5D_t778498235;
// System.String[]
struct StringU5BU5D_t4054002952;
// SevenZip.CommandLineParser.SwitchResult
struct SwitchResult_t80294439;
// SevenZip.CommandLineParser.CommandForm[]
struct CommandFormU5BU5D_t1880299148;
// SevenZip.CommandLineParser.CommandSubCharsSet[]
struct CommandSubCharsSetU5BU5D_t4194914353;
// System.Collections.ArrayList
struct ArrayList_t3948406897;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Collections_ArrayList3948406897.h"
#include "AssemblyU2DCSharp_SevenZip_CommandLineParser_Parse3511445845.h"

// System.Void SevenZip.CommandLineParser.Parser::.ctor(System.Int32)
extern "C"  void Parser__ctor_m310654899 (Parser_t3511445845 * __this, int32_t ___numSwitches0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SevenZip.CommandLineParser.Parser::ParseString(System.String,SevenZip.CommandLineParser.SwitchForm[])
extern "C"  bool Parser_ParseString_m224024082 (Parser_t3511445845 * __this, String_t* ___srcString0, SwitchFormU5BU5D_t778498235* ___switchForms1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.CommandLineParser.Parser::ParseStrings(SevenZip.CommandLineParser.SwitchForm[],System.String[])
extern "C"  void Parser_ParseStrings_m1168934135 (Parser_t3511445845 * __this, SwitchFormU5BU5D_t778498235* ___switchForms0, StringU5BU5D_t4054002952* ___commandStrings1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SevenZip.CommandLineParser.SwitchResult SevenZip.CommandLineParser.Parser::get_Item(System.Int32)
extern "C"  SwitchResult_t80294439 * Parser_get_Item_m3528112215 (Parser_t3511445845 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SevenZip.CommandLineParser.Parser::ParseCommand(SevenZip.CommandLineParser.CommandForm[],System.String,System.String&)
extern "C"  int32_t Parser_ParseCommand_m1492201263 (Il2CppObject * __this /* static, unused */, CommandFormU5BU5D_t1880299148* ___commandForms0, String_t* ___commandString1, String_t** ___postString2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SevenZip.CommandLineParser.Parser::ParseSubCharsCommand(System.Int32,SevenZip.CommandLineParser.CommandSubCharsSet[],System.String,System.Collections.ArrayList)
extern "C"  bool Parser_ParseSubCharsCommand_m1806103243 (Il2CppObject * __this /* static, unused */, int32_t ___numForms0, CommandSubCharsSetU5BU5D_t4194914353* ___forms1, String_t* ___commandString2, ArrayList_t3948406897 * ___indices3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SevenZip.CommandLineParser.Parser::IsItSwitchChar(System.Char)
extern "C"  bool Parser_IsItSwitchChar_m3194338000 (Il2CppObject * __this /* static, unused */, Il2CppChar ___c0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SevenZip.CommandLineParser.Parser::ilo_IsItSwitchChar1(System.Char)
extern "C"  bool Parser_ilo_IsItSwitchChar1_m4085785290 (Il2CppObject * __this /* static, unused */, Il2CppChar ___c0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SevenZip.CommandLineParser.Parser::ilo_ParseString2(SevenZip.CommandLineParser.Parser,System.String,SevenZip.CommandLineParser.SwitchForm[])
extern "C"  bool Parser_ilo_ParseString2_m2336487166 (Il2CppObject * __this /* static, unused */, Parser_t3511445845 * ____this0, String_t* ___srcString1, SwitchFormU5BU5D_t778498235* ___switchForms2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
