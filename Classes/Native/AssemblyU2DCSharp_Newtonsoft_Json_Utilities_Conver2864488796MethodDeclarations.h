﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Utilities.ConvertUtils/<TryConvert>c__AnonStorey13C
struct U3CTryConvertU3Ec__AnonStorey13C_t2864488796;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Newtonsoft.Json.Utilities.ConvertUtils/<TryConvert>c__AnonStorey13C::.ctor()
extern "C"  void U3CTryConvertU3Ec__AnonStorey13C__ctor_m3038955919 (U3CTryConvertU3Ec__AnonStorey13C_t2864488796 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Utilities.ConvertUtils/<TryConvert>c__AnonStorey13C::<>m__398()
extern "C"  Il2CppObject * U3CTryConvertU3Ec__AnonStorey13C_U3CU3Em__398_m3344532743 (U3CTryConvertU3Ec__AnonStorey13C_t2864488796 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
