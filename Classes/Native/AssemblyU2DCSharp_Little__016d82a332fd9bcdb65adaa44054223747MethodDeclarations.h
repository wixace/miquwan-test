﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._016d82a332fd9bcdb65adaa4bba12e1f
struct _016d82a332fd9bcdb65adaa4bba12e1f_t4054223747;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._016d82a332fd9bcdb65adaa4bba12e1f::.ctor()
extern "C"  void _016d82a332fd9bcdb65adaa4bba12e1f__ctor_m652074506 (_016d82a332fd9bcdb65adaa4bba12e1f_t4054223747 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._016d82a332fd9bcdb65adaa4bba12e1f::_016d82a332fd9bcdb65adaa4bba12e1fm2(System.Int32)
extern "C"  int32_t _016d82a332fd9bcdb65adaa4bba12e1f__016d82a332fd9bcdb65adaa4bba12e1fm2_m2866727609 (_016d82a332fd9bcdb65adaa4bba12e1f_t4054223747 * __this, int32_t ____016d82a332fd9bcdb65adaa4bba12e1fa0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._016d82a332fd9bcdb65adaa4bba12e1f::_016d82a332fd9bcdb65adaa4bba12e1fm(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _016d82a332fd9bcdb65adaa4bba12e1f__016d82a332fd9bcdb65adaa4bba12e1fm_m3229769757 (_016d82a332fd9bcdb65adaa4bba12e1f_t4054223747 * __this, int32_t ____016d82a332fd9bcdb65adaa4bba12e1fa0, int32_t ____016d82a332fd9bcdb65adaa4bba12e1f791, int32_t ____016d82a332fd9bcdb65adaa4bba12e1fc2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
