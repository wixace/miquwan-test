﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Xml.XmlElement
struct XmlElement_t280387747;
// System.String
struct String_t;
// System.Xml.XmlDocument
struct XmlDocument_t730752740;
// System.Xml.XmlLinkedNode
struct XmlLinkedNode_t901819716;
// System.Xml.XmlAttributeCollection
struct XmlAttributeCollection_t3012627841;
// System.Xml.XmlNode
struct XmlNode_t856910923;
// System.Xml.Schema.IXmlSchemaInfo
struct IXmlSchemaInfo_t4085280001;
// System.Xml.XmlAttribute
struct XmlAttribute_t6647939;
// System.Xml.XmlNodeList
struct XmlNodeList_t991860617;
// System.Xml.XmlWriter
struct XmlWriter_t4278601340;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "System_Xml_System_Xml_XmlDocument730752740.h"
#include "System_Xml_System_Xml_XmlLinkedNode901819716.h"
#include "System_Xml_System_Xml_XmlNodeType992114213.h"
#include "System_Xml_System_Xml_XPath_XPathNodeType3637370479.h"
#include "System_Xml_System_Xml_XmlAttribute6647939.h"
#include "System_Xml_System_Xml_XmlWriter4278601340.h"

// System.Void System.Xml.XmlElement::.ctor(System.String,System.String,System.String,System.Xml.XmlDocument,System.Boolean)
extern "C"  void XmlElement__ctor_m1054306151 (XmlElement_t280387747 * __this, String_t* ___prefix0, String_t* ___localName1, String_t* ___namespaceURI2, XmlDocument_t730752740 * ___doc3, bool ___atomizedNames4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlLinkedNode System.Xml.XmlElement::System.Xml.IHasXmlChildNode.get_LastLinkedChild()
extern "C"  XmlLinkedNode_t901819716 * XmlElement_System_Xml_IHasXmlChildNode_get_LastLinkedChild_m737144701 (XmlElement_t280387747 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.XmlElement::System.Xml.IHasXmlChildNode.set_LastLinkedChild(System.Xml.XmlLinkedNode)
extern "C"  void XmlElement_System_Xml_IHasXmlChildNode_set_LastLinkedChild_m2196767736 (XmlElement_t280387747 * __this, XmlLinkedNode_t901819716 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlAttributeCollection System.Xml.XmlElement::get_Attributes()
extern "C"  XmlAttributeCollection_t3012627841 * XmlElement_get_Attributes_m925376964 (XmlElement_t280387747 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.XmlElement::get_HasAttributes()
extern "C"  bool XmlElement_get_HasAttributes_m2492649460 (XmlElement_t280387747 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.XmlElement::get_InnerText()
extern "C"  String_t* XmlElement_get_InnerText_m535347169 (XmlElement_t280387747 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.XmlElement::set_InnerText(System.String)
extern "C"  void XmlElement_set_InnerText_m3881727672 (XmlElement_t280387747 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.XmlElement::get_InnerXml()
extern "C"  String_t* XmlElement_get_InnerXml_m1960850437 (XmlElement_t280387747 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.XmlElement::set_InnerXml(System.String)
extern "C"  void XmlElement_set_InnerXml_m3853401670 (XmlElement_t280387747 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.XmlElement::get_IsEmpty()
extern "C"  bool XmlElement_get_IsEmpty_m1811931046 (XmlElement_t280387747 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.XmlElement::set_IsEmpty(System.Boolean)
extern "C"  void XmlElement_set_IsEmpty_m3658279841 (XmlElement_t280387747 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.XmlElement::get_LocalName()
extern "C"  String_t* XmlElement_get_LocalName_m2357904148 (XmlElement_t280387747 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.XmlElement::get_Name()
extern "C"  String_t* XmlElement_get_Name_m1366553519 (XmlElement_t280387747 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.XmlElement::get_NamespaceURI()
extern "C"  String_t* XmlElement_get_NamespaceURI_m2980073141 (XmlElement_t280387747 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlNode System.Xml.XmlElement::get_NextSibling()
extern "C"  XmlNode_t856910923 * XmlElement_get_NextSibling_m114548824 (XmlElement_t280387747 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlNodeType System.Xml.XmlElement::get_NodeType()
extern "C"  int32_t XmlElement_get_NodeType_m1843892847 (XmlElement_t280387747 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XPath.XPathNodeType System.Xml.XmlElement::get_XPathNodeType()
extern "C"  int32_t XmlElement_get_XPathNodeType_m2169456371 (XmlElement_t280387747 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlDocument System.Xml.XmlElement::get_OwnerDocument()
extern "C"  XmlDocument_t730752740 * XmlElement_get_OwnerDocument_m3486800766 (XmlElement_t280387747 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.XmlElement::get_Prefix()
extern "C"  String_t* XmlElement_get_Prefix_m165157174 (XmlElement_t280387747 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.XmlElement::set_Prefix(System.String)
extern "C"  void XmlElement_set_Prefix_m2340865781 (XmlElement_t280387747 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlNode System.Xml.XmlElement::get_ParentNode()
extern "C"  XmlNode_t856910923 * XmlElement_get_ParentNode_m686319621 (XmlElement_t280387747 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.Schema.IXmlSchemaInfo System.Xml.XmlElement::get_SchemaInfo()
extern "C"  Il2CppObject * XmlElement_get_SchemaInfo_m952006649 (XmlElement_t280387747 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.XmlElement::set_SchemaInfo(System.Xml.Schema.IXmlSchemaInfo)
extern "C"  void XmlElement_set_SchemaInfo_m260485366 (XmlElement_t280387747 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlNode System.Xml.XmlElement::CloneNode(System.Boolean)
extern "C"  XmlNode_t856910923 * XmlElement_CloneNode_m252539560 (XmlElement_t280387747 * __this, bool ___deep0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.XmlElement::GetAttribute(System.String)
extern "C"  String_t* XmlElement_GetAttribute_m4083471457 (XmlElement_t280387747 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.XmlElement::GetAttribute(System.String,System.String)
extern "C"  String_t* XmlElement_GetAttribute_m83771101 (XmlElement_t280387747 * __this, String_t* ___localName0, String_t* ___namespaceURI1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlAttribute System.Xml.XmlElement::GetAttributeNode(System.String)
extern "C"  XmlAttribute_t6647939 * XmlElement_GetAttributeNode_m68350548 (XmlElement_t280387747 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlAttribute System.Xml.XmlElement::GetAttributeNode(System.String,System.String)
extern "C"  XmlAttribute_t6647939 * XmlElement_GetAttributeNode_m1937471376 (XmlElement_t280387747 * __this, String_t* ___localName0, String_t* ___namespaceURI1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlNodeList System.Xml.XmlElement::GetElementsByTagName(System.String)
extern "C"  XmlNodeList_t991860617 * XmlElement_GetElementsByTagName_m4282726579 (XmlElement_t280387747 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlNodeList System.Xml.XmlElement::GetElementsByTagName(System.String,System.String)
extern "C"  XmlNodeList_t991860617 * XmlElement_GetElementsByTagName_m1185393583 (XmlElement_t280387747 * __this, String_t* ___localName0, String_t* ___namespaceURI1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.XmlElement::HasAttribute(System.String)
extern "C"  bool XmlElement_HasAttribute_m1391590858 (XmlElement_t280387747 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.XmlElement::HasAttribute(System.String,System.String)
extern "C"  bool XmlElement_HasAttribute_m2832088198 (XmlElement_t280387747 * __this, String_t* ___localName0, String_t* ___namespaceURI1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.XmlElement::RemoveAll()
extern "C"  void XmlElement_RemoveAll_m891345345 (XmlElement_t280387747 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.XmlElement::RemoveAllAttributes()
extern "C"  void XmlElement_RemoveAllAttributes_m702672248 (XmlElement_t280387747 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.XmlElement::RemoveAttribute(System.String)
extern "C"  void XmlElement_RemoveAttribute_m2207955910 (XmlElement_t280387747 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.XmlElement::RemoveAttribute(System.String,System.String)
extern "C"  void XmlElement_RemoveAttribute_m82104706 (XmlElement_t280387747 * __this, String_t* ___localName0, String_t* ___namespaceURI1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlNode System.Xml.XmlElement::RemoveAttributeAt(System.Int32)
extern "C"  XmlNode_t856910923 * XmlElement_RemoveAttributeAt_m3771473038 (XmlElement_t280387747 * __this, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlAttribute System.Xml.XmlElement::RemoveAttributeNode(System.Xml.XmlAttribute)
extern "C"  XmlAttribute_t6647939 * XmlElement_RemoveAttributeNode_m4101316289 (XmlElement_t280387747 * __this, XmlAttribute_t6647939 * ___oldAttr0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlAttribute System.Xml.XmlElement::RemoveAttributeNode(System.String,System.String)
extern "C"  XmlAttribute_t6647939 * XmlElement_RemoveAttributeNode_m2121163368 (XmlElement_t280387747 * __this, String_t* ___localName0, String_t* ___namespaceURI1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.XmlElement::SetAttribute(System.String,System.String)
extern "C"  void XmlElement_SetAttribute_m3688654406 (XmlElement_t280387747 * __this, String_t* ___name0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.XmlElement::SetAttribute(System.String,System.String,System.String)
extern "C"  String_t* XmlElement_SetAttribute_m655466341 (XmlElement_t280387747 * __this, String_t* ___localName0, String_t* ___namespaceURI1, String_t* ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlAttribute System.Xml.XmlElement::SetAttributeNode(System.Xml.XmlAttribute)
extern "C"  XmlAttribute_t6647939 * XmlElement_SetAttributeNode_m2917993461 (XmlElement_t280387747 * __this, XmlAttribute_t6647939 * ___newAttr0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlAttribute System.Xml.XmlElement::SetAttributeNode(System.String,System.String)
extern "C"  XmlAttribute_t6647939 * XmlElement_SetAttributeNode_m3003422108 (XmlElement_t280387747 * __this, String_t* ___localName0, String_t* ___namespaceURI1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.XmlElement::WriteContentTo(System.Xml.XmlWriter)
extern "C"  void XmlElement_WriteContentTo_m1801530527 (XmlElement_t280387747 * __this, XmlWriter_t4278601340 * ___w0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.XmlElement::WriteTo(System.Xml.XmlWriter)
extern "C"  void XmlElement_WriteTo_m1695490250 (XmlElement_t280387747 * __this, XmlWriter_t4278601340 * ___w0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
