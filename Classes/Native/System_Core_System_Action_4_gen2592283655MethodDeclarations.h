﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action_4_gen3253885269MethodDeclarations.h"

// System.Void System.Action`4<System.Int32,Skill,UnityEngine.Vector3,CombatEntity>::.ctor(System.Object,System.IntPtr)
#define Action_4__ctor_m1478351638(__this, ___object0, ___method1, method) ((  void (*) (Action_4_t2592283655 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Action_4__ctor_m4168226482_gshared)(__this, ___object0, ___method1, method)
// System.Void System.Action`4<System.Int32,Skill,UnityEngine.Vector3,CombatEntity>::Invoke(T1,T2,T3,T4)
#define Action_4_Invoke_m1998596932(__this, ___arg10, ___arg21, ___arg32, ___arg43, method) ((  void (*) (Action_4_t2592283655 *, int32_t, Skill_t79944241 *, Vector3_t4282066566 , CombatEntity_t684137495 *, const MethodInfo*))Action_4_Invoke_m3504236088_gshared)(__this, ___arg10, ___arg21, ___arg32, ___arg43, method)
// System.IAsyncResult System.Action`4<System.Int32,Skill,UnityEngine.Vector3,CombatEntity>::BeginInvoke(T1,T2,T3,T4,System.AsyncCallback,System.Object)
#define Action_4_BeginInvoke_m787528133(__this, ___arg10, ___arg21, ___arg32, ___arg43, ___callback4, ___object5, method) ((  Il2CppObject * (*) (Action_4_t2592283655 *, int32_t, Skill_t79944241 *, Vector3_t4282066566 , CombatEntity_t684137495 *, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))Action_4_BeginInvoke_m1187015233_gshared)(__this, ___arg10, ___arg21, ___arg32, ___arg43, ___callback4, ___object5, method)
// System.Void System.Action`4<System.Int32,Skill,UnityEngine.Vector3,CombatEntity>::EndInvoke(System.IAsyncResult)
#define Action_4_EndInvoke_m4251290934(__this, ___result0, method) ((  void (*) (Action_4_t2592283655 *, Il2CppObject *, const MethodInfo*))Action_4_EndInvoke_m1437643202_gshared)(__this, ___result0, method)
