﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ReplayDead
struct ReplayDead_t779766027;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// System.String[]
struct StringU5BU5D_t4054002952;
// System.String
struct String_t;
// ReplayBase
struct ReplayBase_t779703160;
// CombatEntity
struct CombatEntity_t684137495;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_ReplayBase779703160.h"
#include "AssemblyU2DCSharp_CombatEntity684137495.h"

// System.Void ReplayDead::.ctor()
extern "C"  void ReplayDead__ctor_m2264615920 (ReplayDead_t779766027 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReplayDead::.ctor(System.Object[])
extern "C"  void ReplayDead__ctor_m4015704514 (ReplayDead_t779766027 * __this, ObjectU5BU5D_t1108656482* ___datas0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] ReplayDead::ParserJsonStr(System.String)
extern "C"  StringU5BU5D_t4054002952* ReplayDead_ParserJsonStr_m320731307 (ReplayDead_t779766027 * __this, String_t* ___json0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ReplayDead::GetJsonStr()
extern "C"  String_t* ReplayDead_GetJsonStr_m2865329514 (ReplayDead_t779766027 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReplayDead::Execute()
extern "C"  void ReplayDead_Execute_m1598869507 (ReplayDead_t779766027 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] ReplayDead::ilo_ParserJsonStr1(ReplayBase,System.String)
extern "C"  StringU5BU5D_t4054002952* ReplayDead_ilo_ParserJsonStr1_m1712920371 (Il2CppObject * __this /* static, unused */, ReplayBase_t779703160 * ____this0, String_t* ___json1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ReplayDead::ilo_get_isDeath2(CombatEntity)
extern "C"  bool ReplayDead_ilo_get_isDeath2_m4173521461 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReplayDead::ilo_ReplayDead3(CombatEntity)
extern "C"  void ReplayDead_ilo_ReplayDead3_m961554412 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
