﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.Predicate`1<Core.RpsChoice>
struct Predicate_1_t3958821311;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;
// System.Predicate`1<Core.RpsResult>
struct Predicate_1_t90651707;
// System.Predicate`1<Entity.Behavior.EBehaviorID>
struct Predicate_1_t849590019;
// System.Predicate`1<HatredCtrl/stValue>
struct Predicate_1_t3037002293;
// System.Predicate`1<Mihua.Assets.SubAssetMgr/ErrorInfo>
struct Predicate_1_t2245038093;
// System.Predicate`1<Newtonsoft.Json.Linq.JTokenType>
struct Predicate_1_t3527954444;
// System.Predicate`1<Newtonsoft.Json.Schema.JsonSchemaType>
struct Predicate_1_t1726472704;
// System.Predicate`1<Pathfinding.AdvancedSmooth/Turn>
struct Predicate_1_t76252261;
// System.Predicate`1<Pathfinding.ClipperLib.IntPoint>
struct Predicate_1_t2937183062;
// System.Predicate`1<Pathfinding.Int2>
struct Predicate_1_t1585102476;
// System.Predicate`1<Pathfinding.Int3>
struct Predicate_1_t1585102477;
// System.Predicate`1<Pathfinding.IntRect>
struct Predicate_1_t2626115144;
// System.Predicate`1<Pathfinding.LocalAvoidance/IntersectionPair>
struct Predicate_1_t2432376802;
// System.Predicate`1<Pathfinding.LocalAvoidance/VOLine>
struct Predicate_1_t1640988684;
// System.Predicate`1<Pathfinding.Voxels.ExtraMesh>
struct Predicate_1_t3829086598;
// System.Predicate`1<Pathfinding.Voxels.VoxelContour>
struct Predicate_1_t3208257899;
// System.Predicate`1<PointCloudRegognizer/Point>
struct Predicate_1_t1449888633;
// System.Predicate`1<PushType>
struct Predicate_1_t1451699335;
// System.Predicate`1<System.Boolean>
struct Predicate_1_t87855601;
// System.Predicate`1<System.Byte>
struct Predicate_1_t2473666543;
// System.Predicate`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct Predicate_1_t1555725860;
// System.Predicate`1<System.Int32>
struct Predicate_1_t764895383;
// System.Predicate`1<System.Object>
struct Predicate_1_t3781873254;
// System.Predicate`1<System.Reflection.CustomAttributeNamedArgument>
struct Predicate_1_t2670669872;
// System.Predicate`1<System.Reflection.CustomAttributeTypedArgument>
struct Predicate_1_t2912350305;
// System.Predicate`1<System.Single>
struct Predicate_1_t3902975855;
// System.Predicate`1<System.UInt32>
struct Predicate_1_t3930692160;
// System.Predicate`1<UnityEngine.Bounds>
struct Predicate_1_t2322698732;
// System.Predicate`1<UnityEngine.Color>
struct Predicate_1_t3805603788;
// System.Predicate`1<UnityEngine.Color32>
struct Predicate_1_t209910571;
// System.Predicate`1<UnityEngine.EventSystems.RaycastResult>
struct Predicate_1_t3373718247;
// System.Predicate`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>
struct Predicate_1_t3733023121;
// System.Predicate`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>
struct Predicate_1_t1985896569;
// System.Predicate`1<UnityEngine.RaycastHit>
struct Predicate_1_t3614232609;
// System.Predicate`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>
struct Predicate_1_t691654621;
// System.Predicate`1<UnityEngine.UICharInfo>
struct Predicate_1_t3971831663;
// System.Predicate`1<UnityEngine.UILineInfo>
struct Predicate_1_t3724932365;
// System.Predicate`1<UnityEngine.UIVertex>
struct Predicate_1_t3855122095;
// System.Predicate`1<UnityEngine.Vector2>
struct Predicate_1_t3893123448;
// System.Predicate`1<UnityEngine.Vector3>
struct Predicate_1_t3893123449;
// System.Predicate`1<UnityEngine.Vector4>
struct Predicate_1_t3893123450;
// System.Reflection.MonoProperty/Getter`2<System.Object,System.Object>
struct Getter_2_t2712548107;
// System.Reflection.MonoProperty/StaticGetter`1<System.Object>
struct StaticGetter_1_t3245381133;
// System_Collections_Generic_List77Generated/<ListA1_ConvertAll_GetDelegate_member8_arg0>c__AnonStorey7B`2<System.Object,System.Object>
struct U3CListA1_ConvertAll_GetDelegate_member8_arg0U3Ec__AnonStorey7B_2_t1251072158;
// System_Collections_Generic_List77Generated/<ListA1_Exists_GetDelegate_member12_arg0>c__AnonStorey7D`1<System.Object>
struct U3CListA1_Exists_GetDelegate_member12_arg0U3Ec__AnonStorey7D_1_t3879607955;
// System_Collections_Generic_List77Generated/<ListA1_Find_GetDelegate_member13_arg0>c__AnonStorey7F`1<System.Object>
struct U3CListA1_Find_GetDelegate_member13_arg0U3Ec__AnonStorey7F_1_t2977357153;
// System_Collections_Generic_List77Generated/<ListA1_FindAll_GetDelegate_member14_arg0>c__AnonStorey81`1<System.Object>
struct U3CListA1_FindAll_GetDelegate_member14_arg0U3Ec__AnonStorey81_1_t3940615295;
// System_Collections_Generic_List77Generated/<ListA1_FindIndex_GetDelegate_member15_arg2>c__AnonStorey83`1<System.Object>
struct U3CListA1_FindIndex_GetDelegate_member15_arg2U3Ec__AnonStorey83_1_t3719523955;
// System_Collections_Generic_List77Generated/<ListA1_FindIndex_GetDelegate_member16_arg1>c__AnonStorey85`1<System.Object>
struct U3CListA1_FindIndex_GetDelegate_member16_arg1U3Ec__AnonStorey85_1_t60889619;
// System_Collections_Generic_List77Generated/<ListA1_FindIndex_GetDelegate_member17_arg0>c__AnonStorey87`1<System.Object>
struct U3CListA1_FindIndex_GetDelegate_member17_arg0U3Ec__AnonStorey87_1_t697222579;
// System_Collections_Generic_List77Generated/<ListA1_FindLast_GetDelegate_member18_arg0>c__AnonStorey89`1<System.Object>
struct U3CListA1_FindLast_GetDelegate_member18_arg0U3Ec__AnonStorey89_1_t3174686476;
// System_Collections_Generic_List77Generated/<ListA1_FindLastIndex_GetDelegate_member19_arg2>c__AnonStorey8B`1<System.Object>
struct U3CListA1_FindLastIndex_GetDelegate_member19_arg2U3Ec__AnonStorey8B_1_t3672975744;
// System_Collections_Generic_List77Generated/<ListA1_FindLastIndex_GetDelegate_member20_arg1>c__AnonStorey8D`1<System.Object>
struct U3CListA1_FindLastIndex_GetDelegate_member20_arg1U3Ec__AnonStorey8D_1_t2423100467;
// System_Collections_Generic_List77Generated/<ListA1_FindLastIndex_GetDelegate_member21_arg0>c__AnonStorey8F`1<System.Object>
struct U3CListA1_FindLastIndex_GetDelegate_member21_arg0U3Ec__AnonStorey8F_1_t3059433427;
// System_Collections_Generic_List77Generated/<ListA1_ForEach_GetDelegate_member22_arg0>c__AnonStorey91`1<System.Object>
struct U3CListA1_ForEach_GetDelegate_member22_arg0U3Ec__AnonStorey91_1_t3044851907;
// System_Collections_Generic_List77Generated/<ListA1_RemoveAll_GetDelegate_member34_arg0>c__AnonStorey93`1<System.Object>
struct U3CListA1_RemoveAll_GetDelegate_member34_arg0U3Ec__AnonStorey93_1_t3917406903;
// System_Collections_Generic_List77Generated/<ListA1_Sort_GetDelegate_member40_arg0>c__AnonStorey95`1<System.Object>
struct U3CListA1_Sort_GetDelegate_member40_arg0U3Ec__AnonStorey95_1_t3640470617;
// System_Collections_Generic_List77Generated/<ListA1_TrueForAll_GetDelegate_member45_arg0>c__AnonStorey97`1<System.Object>
struct U3CListA1_TrueForAll_GetDelegate_member45_arg0U3Ec__AnonStorey97_1_t3358868182;
// UnityEngine.Advertisements.UnityAdsDelegate`2<System.Object,System.Boolean>
struct UnityAdsDelegate_2_t92490124;
// UnityEngine.Advertisements.UnityAdsDelegate`2<System.Object,System.Object>
struct UnityAdsDelegate_2_t3786507777;
// UnityEngine.Events.CachedInvokableCall`1<System.Boolean>
struct CachedInvokableCall_1_t65035577;
// UnityEngine.Object
struct Object_t3071478659;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// UnityEngine.Events.CachedInvokableCall`1<System.Int32>
struct CachedInvokableCall_1_t742075359;
// UnityEngine.Events.CachedInvokableCall`1<System.Object>
struct CachedInvokableCall_1_t3759053230;
// UnityEngine.Events.CachedInvokableCall`1<System.Single>
struct CachedInvokableCall_1_t3880155831;
// UnityEngine.Events.InvokableCall`1<System.Boolean>
struct InvokableCall_1_t2626711275;
// UnityEngine.Events.UnityAction`1<System.Boolean>
struct UnityAction_1_t370978721;
// UnityEngine.Events.InvokableCall`1<System.Int32>
struct InvokableCall_1_t3303751057;
// UnityEngine.Events.UnityAction`1<System.Int32>
struct UnityAction_1_t1048018503;
// UnityEngine.Events.InvokableCall`1<System.Object>
struct InvokableCall_1_t2025761632;
// UnityEngine.Events.UnityAction`1<System.Object>
struct UnityAction_1_t4064996374;
// UnityEngine.Events.InvokableCall`1<System.Single>
struct InvokableCall_1_t2146864233;
// UnityEngine.Events.UnityAction`1<System.Single>
struct UnityAction_1_t4186098975;
// UnityEngine.Events.InvokableCall`1<UnityEngine.Color>
struct InvokableCall_1_t2049492166;
// UnityEngine.Events.UnityAction`1<UnityEngine.Color>
struct UnityAction_1_t4088726908;
// UnityEngine.Events.InvokableCall`1<UnityEngine.Vector2>
struct InvokableCall_1_t2137011826;
// UnityEngine.Events.UnityAction`1<UnityEngine.Vector2>
struct UnityAction_1_t4176246568;
// UnityEngine.Events.InvokableCall`2<System.Object,System.Object>
struct InvokableCall_2_t3350606262;
// UnityEngine.Events.InvokableCall`3<System.Object,System.Object,System.Object>
struct InvokableCall_3_t3637709590;
// UnityEngine.Events.InvokableCall`4<System.Object,System.Object,System.Object,System.Object>
struct InvokableCall_4_t2281252720;
// UnityEngine.Events.UnityAction`2<System.Object,System.Object>
struct UnityAction_2_t2640361832;
// UnityEngine.Events.UnityAction`3<System.Object,System.Object,System.Object>
struct UnityAction_3_t2271724636;
// UnityEngine.Events.UnityAction`4<System.Object,System.Object,System.Object,System.Object>
struct UnityAction_4_t2496524882;
// UnityEngine.Events.UnityEvent`1<System.Boolean>
struct UnityEvent_1_t663396231;
// UnityEngine.Events.BaseInvokableCall
struct BaseInvokableCall_t1559630662;
// UnityEngine.Events.UnityEvent`1<System.Int32>
struct UnityEvent_1_t1340436013;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Nullable_1_gen72820554.h"
#include "mscorlib_System_Nullable_1_gen72820554MethodDeclarations.h"
#include "mscorlib_System_DateTime4283661327.h"
#include "mscorlib_System_Void2863195528.h"
#include "mscorlib_System_Boolean476798718.h"
#include "mscorlib_System_InvalidOperationException1589641621MethodDeclarations.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_InvalidOperationException1589641621.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_DateTime4283661327MethodDeclarations.h"
#include "mscorlib_System_ValueType1744280289MethodDeclarations.h"
#include "mscorlib_System_ValueType1744280289.h"
#include "mscorlib_System_Int321153838500.h"
#include "mscorlib_System_String7231557MethodDeclarations.h"
#include "mscorlib_System_Nullable_1_gen3968840829.h"
#include "mscorlib_System_Nullable_1_gen3968840829MethodDeclarations.h"
#include "mscorlib_System_DateTimeOffset3884714306.h"
#include "mscorlib_System_DateTimeOffset3884714306MethodDeclarations.h"
#include "mscorlib_System_Nullable_1_gen2038477154.h"
#include "mscorlib_System_Nullable_1_gen2038477154MethodDeclarations.h"
#include "mscorlib_System_Decimal1954350631.h"
#include "mscorlib_System_Decimal1954350631MethodDeclarations.h"
#include "mscorlib_System_Nullable_1_gen3952353088.h"
#include "mscorlib_System_Nullable_1_gen3952353088MethodDeclarations.h"
#include "mscorlib_System_Double3868226565.h"
#include "mscorlib_System_Double3868226565MethodDeclarations.h"
#include "mscorlib_System_Nullable_1_gen2946880952.h"
#include "mscorlib_System_Nullable_1_gen2946880952MethodDeclarations.h"
#include "mscorlib_System_Guid2862754429.h"
#include "mscorlib_System_Guid2862754429MethodDeclarations.h"
#include "mscorlib_System_Nullable_1_gen1237964965.h"
#include "mscorlib_System_Nullable_1_gen1237964965MethodDeclarations.h"
#include "mscorlib_System_Int161153838442.h"
#include "mscorlib_System_Int161153838442MethodDeclarations.h"
#include "mscorlib_System_Nullable_1_gen1237965023.h"
#include "mscorlib_System_Nullable_1_gen1237965023MethodDeclarations.h"
#include "mscorlib_System_Int321153838500MethodDeclarations.h"
#include "mscorlib_System_Nullable_1_gen1237965118.h"
#include "mscorlib_System_Nullable_1_gen1237965118MethodDeclarations.h"
#include "mscorlib_System_Int641153838595.h"
#include "mscorlib_System_Int641153838595MethodDeclarations.h"
#include "mscorlib_System_Nullable_1_gen1245896300.h"
#include "mscorlib_System_Nullable_1_gen1245896300MethodDeclarations.h"
#include "mscorlib_System_SByte1161769777.h"
#include "mscorlib_System_SByte1161769777MethodDeclarations.h"
#include "mscorlib_System_Nullable_1_gen81078199.h"
#include "mscorlib_System_Nullable_1_gen81078199MethodDeclarations.h"
#include "mscorlib_System_Single4291918972.h"
#include "mscorlib_System_Single4291918972MethodDeclarations.h"
#include "mscorlib_System_Nullable_1_gen497649510.h"
#include "mscorlib_System_Nullable_1_gen497649510MethodDeclarations.h"
#include "mscorlib_System_TimeSpan413522987.h"
#include "mscorlib_System_TimeSpan413522987MethodDeclarations.h"
#include "mscorlib_System_Nullable_1_gen108794446.h"
#include "mscorlib_System_Nullable_1_gen108794446MethodDeclarations.h"
#include "mscorlib_System_UInt1624667923.h"
#include "mscorlib_System_UInt1624667923MethodDeclarations.h"
#include "mscorlib_System_Nullable_1_gen108794504.h"
#include "mscorlib_System_Nullable_1_gen108794504MethodDeclarations.h"
#include "mscorlib_System_UInt3224667981.h"
#include "mscorlib_System_UInt3224667981MethodDeclarations.h"
#include "mscorlib_System_Nullable_1_gen108794599.h"
#include "mscorlib_System_Nullable_1_gen108794599MethodDeclarations.h"
#include "mscorlib_System_UInt6424668076.h"
#include "mscorlib_System_UInt6424668076MethodDeclarations.h"
#include "mscorlib_System_Nullable_1_gen71225793.h"
#include "mscorlib_System_Nullable_1_gen71225793MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_Vector34282066566MethodDeclarations.h"
#include "mscorlib_System_Predicate_1_gen3958821311.h"
#include "mscorlib_System_Predicate_1_gen3958821311MethodDeclarations.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "AssemblyU2DCSharp_Core_RpsChoice52797132.h"
#include "mscorlib_System_AsyncCallback1369114871.h"
#include "mscorlib_System_Predicate_1_gen90651707.h"
#include "mscorlib_System_Predicate_1_gen90651707MethodDeclarations.h"
#include "AssemblyU2DCSharp_Core_RpsResult479594824.h"
#include "mscorlib_System_Predicate_1_gen849590019.h"
#include "mscorlib_System_Predicate_1_gen849590019MethodDeclarations.h"
#include "AssemblyU2DCSharp_Entity_Behavior_EBehaviorID1238533136.h"
#include "mscorlib_System_Predicate_1_gen3037002293.h"
#include "mscorlib_System_Predicate_1_gen3037002293MethodDeclarations.h"
#include "AssemblyU2DCSharp_HatredCtrl_stValue3425945410.h"
#include "mscorlib_System_Predicate_1_gen2245038093.h"
#include "mscorlib_System_Predicate_1_gen2245038093MethodDeclarations.h"
#include "AssemblyU2DCSharp_Mihua_Assets_SubAssetMgr_ErrorIn2633981210.h"
#include "mscorlib_System_Predicate_1_gen3527954444.h"
#include "mscorlib_System_Predicate_1_gen3527954444MethodDeclarations.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JTokenType3916897561.h"
#include "mscorlib_System_Predicate_1_gen1726472704.h"
#include "mscorlib_System_Predicate_1_gen1726472704MethodDeclarations.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Schema_JsonSchem2115415821.h"
#include "mscorlib_System_Predicate_1_gen76252261.h"
#include "mscorlib_System_Predicate_1_gen76252261MethodDeclarations.h"
#include "AssemblyU2DCSharp_Pathfinding_AdvancedSmooth_Turn465195378.h"
#include "mscorlib_System_Predicate_1_gen2937183062.h"
#include "mscorlib_System_Predicate_1_gen2937183062MethodDeclarations.h"
#include "Pathfinding_ClipperLib_Pathfinding_ClipperLib_IntP3326126179.h"
#include "mscorlib_System_Predicate_1_gen1585102476.h"
#include "mscorlib_System_Predicate_1_gen1585102476MethodDeclarations.h"
#include "AssemblyU2DCSharp_Pathfinding_Int21974045593.h"
#include "mscorlib_System_Predicate_1_gen1585102477.h"
#include "mscorlib_System_Predicate_1_gen1585102477MethodDeclarations.h"
#include "AssemblyU2DCSharp_Pathfinding_Int31974045594.h"
#include "mscorlib_System_Predicate_1_gen2626115144.h"
#include "mscorlib_System_Predicate_1_gen2626115144MethodDeclarations.h"
#include "AssemblyU2DCSharp_Pathfinding_IntRect3015058261.h"
#include "mscorlib_System_Predicate_1_gen2432376802.h"
#include "mscorlib_System_Predicate_1_gen2432376802MethodDeclarations.h"
#include "AssemblyU2DCSharp_Pathfinding_LocalAvoidance_Inter2821319919.h"
#include "mscorlib_System_Predicate_1_gen1640988684.h"
#include "mscorlib_System_Predicate_1_gen1640988684MethodDeclarations.h"
#include "AssemblyU2DCSharp_Pathfinding_LocalAvoidance_VOLin2029931801.h"
#include "mscorlib_System_Predicate_1_gen3829086598.h"
#include "mscorlib_System_Predicate_1_gen3829086598MethodDeclarations.h"
#include "AssemblyU2DCSharp_Pathfinding_Voxels_ExtraMesh4218029715.h"
#include "mscorlib_System_Predicate_1_gen3208257899.h"
#include "mscorlib_System_Predicate_1_gen3208257899MethodDeclarations.h"
#include "AssemblyU2DCSharp_Pathfinding_Voxels_VoxelContour3597201016.h"
#include "mscorlib_System_Predicate_1_gen1449888633.h"
#include "mscorlib_System_Predicate_1_gen1449888633MethodDeclarations.h"
#include "AssemblyU2DCSharp_PointCloudRegognizer_Point1838831750.h"
#include "mscorlib_System_Predicate_1_gen1451699335.h"
#include "mscorlib_System_Predicate_1_gen1451699335MethodDeclarations.h"
#include "AssemblyU2DCSharp_PushType1840642452.h"
#include "mscorlib_System_Predicate_1_gen87855601.h"
#include "mscorlib_System_Predicate_1_gen87855601MethodDeclarations.h"
#include "mscorlib_System_Predicate_1_gen2473666543.h"
#include "mscorlib_System_Predicate_1_gen2473666543MethodDeclarations.h"
#include "mscorlib_System_Byte2862609660.h"
#include "mscorlib_System_Predicate_1_gen1555725860.h"
#include "mscorlib_System_Predicate_1_gen1555725860MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21944668977.h"
#include "mscorlib_System_Predicate_1_gen764895383.h"
#include "mscorlib_System_Predicate_1_gen764895383MethodDeclarations.h"
#include "mscorlib_System_Predicate_1_gen3781873254.h"
#include "mscorlib_System_Predicate_1_gen3781873254MethodDeclarations.h"
#include "mscorlib_System_Predicate_1_gen2670669872.h"
#include "mscorlib_System_Predicate_1_gen2670669872MethodDeclarations.h"
#include "mscorlib_System_Reflection_CustomAttributeNamedArg3059612989.h"
#include "mscorlib_System_Predicate_1_gen2912350305.h"
#include "mscorlib_System_Predicate_1_gen2912350305MethodDeclarations.h"
#include "mscorlib_System_Reflection_CustomAttributeTypedArg3301293422.h"
#include "mscorlib_System_Predicate_1_gen3902975855.h"
#include "mscorlib_System_Predicate_1_gen3902975855MethodDeclarations.h"
#include "mscorlib_System_Predicate_1_gen3930692160.h"
#include "mscorlib_System_Predicate_1_gen3930692160MethodDeclarations.h"
#include "mscorlib_System_Predicate_1_gen2322698732.h"
#include "mscorlib_System_Predicate_1_gen2322698732MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Bounds2711641849.h"
#include "mscorlib_System_Predicate_1_gen3805603788.h"
#include "mscorlib_System_Predicate_1_gen3805603788MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"
#include "mscorlib_System_Predicate_1_gen209910571.h"
#include "mscorlib_System_Predicate_1_gen209910571MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color32598853688.h"
#include "mscorlib_System_Predicate_1_gen3373718247.h"
#include "mscorlib_System_Predicate_1_gen3373718247MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastRes3762661364.h"
#include "mscorlib_System_Predicate_1_gen3733023121.h"
#include "mscorlib_System_Predicate_1_gen3733023121MethodDeclarations.h"
#include "UnityEngine_UnityEngine_MeshSubsetCombineUtility_M4121966238.h"
#include "mscorlib_System_Predicate_1_gen1985896569.h"
#include "mscorlib_System_Predicate_1_gen1985896569MethodDeclarations.h"
#include "UnityEngine_UnityEngine_MeshSubsetCombineUtility_S2374839686.h"
#include "mscorlib_System_Predicate_1_gen3614232609.h"
#include "mscorlib_System_Predicate_1_gen3614232609MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RaycastHit4003175726.h"
#include "mscorlib_System_Predicate_1_gen691654621.h"
#include "mscorlib_System_Predicate_1_gen691654621MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rendering_ReflectionProbeB1080597738.h"
#include "mscorlib_System_Predicate_1_gen3971831663.h"
#include "mscorlib_System_Predicate_1_gen3971831663MethodDeclarations.h"
#include "UnityEngine_UnityEngine_UICharInfo65807484.h"
#include "mscorlib_System_Predicate_1_gen3724932365.h"
#include "mscorlib_System_Predicate_1_gen3724932365MethodDeclarations.h"
#include "UnityEngine_UnityEngine_UILineInfo4113875482.h"
#include "mscorlib_System_Predicate_1_gen3855122095.h"
#include "mscorlib_System_Predicate_1_gen3855122095MethodDeclarations.h"
#include "UnityEngine_UnityEngine_UIVertex4244065212.h"
#include "mscorlib_System_Predicate_1_gen3893123448.h"
#include "mscorlib_System_Predicate_1_gen3893123448MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "mscorlib_System_Predicate_1_gen3893123449.h"
#include "mscorlib_System_Predicate_1_gen3893123449MethodDeclarations.h"
#include "mscorlib_System_Predicate_1_gen3893123450.h"
#include "mscorlib_System_Predicate_1_gen3893123450MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector44282066567.h"
#include "mscorlib_System_Reflection_MonoProperty_Getter_2_g2712548107.h"
#include "mscorlib_System_Reflection_MonoProperty_Getter_2_g2712548107MethodDeclarations.h"
#include "mscorlib_System_Reflection_MonoProperty_StaticGett3245381133.h"
#include "mscorlib_System_Reflection_MonoProperty_StaticGett3245381133MethodDeclarations.h"
#include "AssemblyU2DCSharp_System_Collections_Generic_List71251072158.h"
#include "AssemblyU2DCSharp_System_Collections_Generic_List71251072158MethodDeclarations.h"
#include "mscorlib_System_Object4170816371MethodDeclarations.h"
#include "AssemblyU2DCSharp_JSVCall3708497963MethodDeclarations.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290MethodDeclarations.h"
#include "AssemblyU2DCSharp_JSMgr70890511.h"
#include "AssemblyU2DCSharp_JSMgr70890511MethodDeclarations.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_CSRepresentedObject3994124630.h"
#include "mscorlib_ArrayTypes.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "AssemblyU2DCSharp_System_Collections_Generic_List73879607955.h"
#include "AssemblyU2DCSharp_System_Collections_Generic_List73879607955MethodDeclarations.h"
#include "AssemblyU2DCSharp_JSApi70879249MethodDeclarations.h"
#include "AssemblyU2DCSharp_System_Collections_Generic_List72977357153.h"
#include "AssemblyU2DCSharp_System_Collections_Generic_List72977357153MethodDeclarations.h"
#include "AssemblyU2DCSharp_System_Collections_Generic_List73940615295.h"
#include "AssemblyU2DCSharp_System_Collections_Generic_List73940615295MethodDeclarations.h"
#include "AssemblyU2DCSharp_System_Collections_Generic_List73719523955.h"
#include "AssemblyU2DCSharp_System_Collections_Generic_List73719523955MethodDeclarations.h"
#include "AssemblyU2DCSharp_System_Collections_Generic_List77G60889619.h"
#include "AssemblyU2DCSharp_System_Collections_Generic_List77G60889619MethodDeclarations.h"
#include "AssemblyU2DCSharp_System_Collections_Generic_List77697222579.h"
#include "AssemblyU2DCSharp_System_Collections_Generic_List77697222579MethodDeclarations.h"
#include "AssemblyU2DCSharp_System_Collections_Generic_List73174686476.h"
#include "AssemblyU2DCSharp_System_Collections_Generic_List73174686476MethodDeclarations.h"
#include "AssemblyU2DCSharp_System_Collections_Generic_List73672975744.h"
#include "AssemblyU2DCSharp_System_Collections_Generic_List73672975744MethodDeclarations.h"
#include "AssemblyU2DCSharp_System_Collections_Generic_List72423100467.h"
#include "AssemblyU2DCSharp_System_Collections_Generic_List72423100467MethodDeclarations.h"
#include "AssemblyU2DCSharp_System_Collections_Generic_List73059433427.h"
#include "AssemblyU2DCSharp_System_Collections_Generic_List73059433427MethodDeclarations.h"
#include "AssemblyU2DCSharp_System_Collections_Generic_List73044851907.h"
#include "AssemblyU2DCSharp_System_Collections_Generic_List73044851907MethodDeclarations.h"
#include "AssemblyU2DCSharp_System_Collections_Generic_List73917406903.h"
#include "AssemblyU2DCSharp_System_Collections_Generic_List73917406903MethodDeclarations.h"
#include "AssemblyU2DCSharp_System_Collections_Generic_List73640470617.h"
#include "AssemblyU2DCSharp_System_Collections_Generic_List73640470617MethodDeclarations.h"
#include "AssemblyU2DCSharp_System_Collections_Generic_List73358868182.h"
#include "AssemblyU2DCSharp_System_Collections_Generic_List73358868182MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Advertisements_UnityAdsDeleg92490124.h"
#include "UnityEngine_UnityEngine_Advertisements_UnityAdsDeleg92490124MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Advertisements_UnityAdsDel3786507777.h"
#include "UnityEngine_UnityEngine_Advertisements_UnityAdsDel3786507777MethodDeclarations.h"
#include "UnityEngine_UnityEngine_CastHelper_1_gen1152456458.h"
#include "UnityEngine_UnityEngine_CastHelper_1_gen1152456458MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_165035577.h"
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_165035577MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object3071478659.h"
#include "mscorlib_System_Reflection_MethodInfo318736065.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen2626711275MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen2626711275.h"
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_742075359.h"
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_742075359MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen3303751057MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen3303751057.h"
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall3759053230.h"
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall3759053230MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen2025761632MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen2025761632.h"
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall3880155831.h"
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall3880155831MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen2146864233MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen2146864233.h"
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall1559630662MethodDeclarations.h"
#include "mscorlib_System_Type2863145774MethodDeclarations.h"
#include "UnityEngine_UnityEngineInternal_NetFxCoreExtension2541795172MethodDeclarations.h"
#include "mscorlib_System_Delegate3310234105MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen370978721.h"
#include "mscorlib_System_Type2863145774.h"
#include "mscorlib_System_RuntimeTypeHandle2669177232.h"
#include "mscorlib_System_Delegate3310234105.h"
#include "mscorlib_System_ArgumentException928607144MethodDeclarations.h"
#include "mscorlib_System_ArgumentException928607144.h"
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall1559630662.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen370978721MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen1048018503.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen1048018503MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen4064996374.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen4064996374MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen4186098975.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen4186098975MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen2049492166.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen2049492166MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen4088726908.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen4088726908MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen2137011826.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen2137011826MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen4176246568.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen4176246568MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_2_gen3350606262.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_2_gen3350606262MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_2_gen2640361832.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_2_gen2640361832MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_3_gen3637709590.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_3_gen3637709590MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_3_gen2271724636.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_3_gen2271724636MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_4_gen2281252720.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_4_gen2281252720MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_4_gen2496524882.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_4_gen2496524882MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_1_gen663396231.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_1_gen663396231MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityEventBase1020378628MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_1_gen1340436013.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_1_gen1340436013MethodDeclarations.h"

// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<System.Boolean>(System.Object)
extern "C"  void BaseInvokableCall_ThrowOnInvalidArg_TisBoolean_t476798718_m2294441611_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method);
#define BaseInvokableCall_ThrowOnInvalidArg_TisBoolean_t476798718_m2294441611(__this /* static, unused */, p0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))BaseInvokableCall_ThrowOnInvalidArg_TisBoolean_t476798718_m2294441611_gshared)(__this /* static, unused */, p0, method)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<System.Int32>(System.Object)
extern "C"  void BaseInvokableCall_ThrowOnInvalidArg_TisInt32_t1153838500_m1459945585_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method);
#define BaseInvokableCall_ThrowOnInvalidArg_TisInt32_t1153838500_m1459945585(__this /* static, unused */, p0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))BaseInvokableCall_ThrowOnInvalidArg_TisInt32_t1153838500_m1459945585_gshared)(__this /* static, unused */, p0, method)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<System.Object>(System.Object)
extern "C"  void BaseInvokableCall_ThrowOnInvalidArg_TisIl2CppObject_m1692402054_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method);
#define BaseInvokableCall_ThrowOnInvalidArg_TisIl2CppObject_m1692402054(__this /* static, unused */, p0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))BaseInvokableCall_ThrowOnInvalidArg_TisIl2CppObject_m1692402054_gshared)(__this /* static, unused */, p0, method)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<System.Single>(System.Object)
extern "C"  void BaseInvokableCall_ThrowOnInvalidArg_TisSingle_t4291918972_m1076801679_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method);
#define BaseInvokableCall_ThrowOnInvalidArg_TisSingle_t4291918972_m1076801679(__this /* static, unused */, p0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))BaseInvokableCall_ThrowOnInvalidArg_TisSingle_t4291918972_m1076801679_gshared)(__this /* static, unused */, p0, method)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<UnityEngine.Color>(System.Object)
extern "C"  void BaseInvokableCall_ThrowOnInvalidArg_TisColor_t4194546905_m2441106612_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method);
#define BaseInvokableCall_ThrowOnInvalidArg_TisColor_t4194546905_m2441106612(__this /* static, unused */, p0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))BaseInvokableCall_ThrowOnInvalidArg_TisColor_t4194546905_m2441106612_gshared)(__this /* static, unused */, p0, method)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<UnityEngine.Vector2>(System.Object)
extern "C"  void BaseInvokableCall_ThrowOnInvalidArg_TisVector2_t4282066565_m1471167904_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method);
#define BaseInvokableCall_ThrowOnInvalidArg_TisVector2_t4282066565_m1471167904(__this /* static, unused */, p0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))BaseInvokableCall_ThrowOnInvalidArg_TisVector2_t4282066565_m1471167904_gshared)(__this /* static, unused */, p0, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Nullable`1<System.DateTime>::.ctor(T)
extern "C"  void Nullable_1__ctor_m973943483_gshared (Nullable_1_t72820554 * __this, DateTime_t4283661327  ___value0, const MethodInfo* method)
{
	{
		__this->set_has_value_1((bool)1);
		DateTime_t4283661327  L_0 = ___value0;
		__this->set_value_0(L_0);
		return;
	}
}
extern "C"  void Nullable_1__ctor_m973943483_AdjustorThunk (Il2CppObject * __this, DateTime_t4283661327  ___value0, const MethodInfo* method)
{
	Nullable_1_t72820554  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<DateTime_t4283661327 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	Nullable_1__ctor_m973943483(&_thisAdjusted, ___value0, method);
	*reinterpret_cast<DateTime_t4283661327 *>(__this + 1) = _thisAdjusted.get_value_0();
}
// System.Boolean System.Nullable`1<System.DateTime>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m4245309987_gshared (Nullable_1_t72820554 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
extern "C"  bool Nullable_1_get_HasValue_m4245309987_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t72820554  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<DateTime_t4283661327 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_get_HasValue_m4245309987(&_thisAdjusted, method);
	*reinterpret_cast<DateTime_t4283661327 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.DateTime>::get_Value()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral78850559;
extern const uint32_t Nullable_1_get_Value_m1229956298_MetadataUsageId;
extern "C"  DateTime_t4283661327  Nullable_1_get_Value_m1229956298_gshared (Nullable_1_t72820554 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_get_Value_m1229956298_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral78850559, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		DateTime_t4283661327  L_2 = (DateTime_t4283661327 )__this->get_value_0();
		return L_2;
	}
}
extern "C"  DateTime_t4283661327  Nullable_1_get_Value_m1229956298_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t72820554  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<DateTime_t4283661327 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	DateTime_t4283661327  _returnValue = Nullable_1_get_Value_m1229956298(&_thisAdjusted, method);
	*reinterpret_cast<DateTime_t4283661327 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<System.DateTime>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m724703986_gshared (Nullable_1_t72820554 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = (bool)__this->get_has_value_1();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0010:
	{
		Il2CppObject * L_2 = ___other0;
		if (((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))
		{
			goto IL_001d;
		}
	}
	{
		return (bool)0;
	}

IL_001d:
	{
		Il2CppObject * L_3 = ___other0;
		void* L_4 = alloca(sizeof(Nullable_1_t72820554 ));
		UnBoxNullable(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), L_4);
		bool L_5 = ((  bool (*) (Il2CppObject *, Nullable_1_t72820554 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(il2cpp_codegen_fake_box((Nullable_1_t72820554 *)__this), (Nullable_1_t72820554 )((*(Nullable_1_t72820554 *)((Nullable_1_t72820554 *)L_4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_5;
	}
}
extern "C"  bool Nullable_1_Equals_m724703986_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Nullable_1_t72820554  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<DateTime_t4283661327 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m724703986(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<DateTime_t4283661327 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<System.DateTime>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m2642060301_gshared (Nullable_1_t72820554 * __this, Nullable_1_t72820554  ___other0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(&___other0)->get_has_value_1();
		bool L_1 = (bool)__this->get_has_value_1();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_has_value_1();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		return (bool)1;
	}

IL_0021:
	{
		DateTime_t4283661327 * L_3 = (DateTime_t4283661327 *)(&___other0)->get_address_of_value_0();
		DateTime_t4283661327  L_4 = (DateTime_t4283661327 )__this->get_value_0();
		DateTime_t4283661327  L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		bool L_7 = DateTime_Equals_m13666989((DateTime_t4283661327 *)L_3, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
extern "C"  bool Nullable_1_Equals_m2642060301_AdjustorThunk (Il2CppObject * __this, Nullable_1_t72820554  ___other0, const MethodInfo* method)
{
	Nullable_1_t72820554  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<DateTime_t4283661327 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m2642060301(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<DateTime_t4283661327 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Int32 System.Nullable`1<System.DateTime>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m4112160982_gshared (Nullable_1_t72820554 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		DateTime_t4283661327 * L_1 = (DateTime_t4283661327 *)__this->get_address_of_value_0();
		int32_t L_2 = DateTime_GetHashCode_m2255586565((DateTime_t4283661327 *)L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
extern "C"  int32_t Nullable_1_GetHashCode_m4112160982_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t72820554  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<DateTime_t4283661327 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_GetHashCode_m4112160982(&_thisAdjusted, method);
	*reinterpret_cast<DateTime_t4283661327 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.DateTime>::GetValueOrDefault()
extern Il2CppClass* DateTime_t4283661327_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_GetValueOrDefault_m1694192421_MetadataUsageId;
extern "C"  DateTime_t4283661327  Nullable_1_GetValueOrDefault_m1694192421_gshared (Nullable_1_t72820554 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_GetValueOrDefault_m1694192421_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DateTime_t4283661327  V_0;
	memset(&V_0, 0, sizeof(V_0));
	DateTime_t4283661327  G_B3_0;
	memset(&G_B3_0, 0, sizeof(G_B3_0));
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		DateTime_t4283661327  L_1 = (DateTime_t4283661327 )__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_001f;
	}

IL_0016:
	{
		Initobj (DateTime_t4283661327_il2cpp_TypeInfo_var, (&V_0));
		DateTime_t4283661327  L_2 = V_0;
		G_B3_0 = L_2;
	}

IL_001f:
	{
		return G_B3_0;
	}
}
extern "C"  DateTime_t4283661327  Nullable_1_GetValueOrDefault_m1694192421_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t72820554  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<DateTime_t4283661327 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	DateTime_t4283661327  _returnValue = Nullable_1_GetValueOrDefault_m1694192421(&_thisAdjusted, method);
	*reinterpret_cast<DateTime_t4283661327 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.DateTime>::GetValueOrDefault(T)
extern "C"  DateTime_t4283661327  Nullable_1_GetValueOrDefault_m980358873_gshared (Nullable_1_t72820554 * __this, DateTime_t4283661327  ___defaultValue0, const MethodInfo* method)
{
	DateTime_t4283661327  G_B3_0;
	memset(&G_B3_0, 0, sizeof(G_B3_0));
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		DateTime_t4283661327  L_1 = (DateTime_t4283661327 )__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_0017;
	}

IL_0016:
	{
		DateTime_t4283661327  L_2 = ___defaultValue0;
		G_B3_0 = L_2;
	}

IL_0017:
	{
		return G_B3_0;
	}
}
extern "C"  DateTime_t4283661327  Nullable_1_GetValueOrDefault_m980358873_AdjustorThunk (Il2CppObject * __this, DateTime_t4283661327  ___defaultValue0, const MethodInfo* method)
{
	Nullable_1_t72820554  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<DateTime_t4283661327 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	DateTime_t4283661327  _returnValue = Nullable_1_GetValueOrDefault_m980358873(&_thisAdjusted, ___defaultValue0, method);
	*reinterpret_cast<DateTime_t4283661327 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.String System.Nullable`1<System.DateTime>::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_ToString_m3130403824_MetadataUsageId;
extern "C"  String_t* Nullable_1_ToString_m3130403824_gshared (Nullable_1_t72820554 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_ToString_m3130403824_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		DateTime_t4283661327 * L_1 = (DateTime_t4283661327 *)__this->get_address_of_value_0();
		String_t* L_2 = DateTime_ToString_m3221907059((DateTime_t4283661327 *)L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_3;
	}
}
extern "C"  String_t* Nullable_1_ToString_m3130403824_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t72820554  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<DateTime_t4283661327 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	String_t* _returnValue = Nullable_1_ToString_m3130403824(&_thisAdjusted, method);
	*reinterpret_cast<DateTime_t4283661327 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Void System.Nullable`1<System.DateTimeOffset>::.ctor(T)
extern "C"  void Nullable_1__ctor_m3569740304_gshared (Nullable_1_t3968840829 * __this, DateTimeOffset_t3884714306  ___value0, const MethodInfo* method)
{
	{
		__this->set_has_value_1((bool)1);
		DateTimeOffset_t3884714306  L_0 = ___value0;
		__this->set_value_0(L_0);
		return;
	}
}
extern "C"  void Nullable_1__ctor_m3569740304_AdjustorThunk (Il2CppObject * __this, DateTimeOffset_t3884714306  ___value0, const MethodInfo* method)
{
	Nullable_1_t3968840829  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<DateTimeOffset_t3884714306 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	Nullable_1__ctor_m3569740304(&_thisAdjusted, ___value0, method);
	*reinterpret_cast<DateTimeOffset_t3884714306 *>(__this + 1) = _thisAdjusted.get_value_0();
}
// System.Boolean System.Nullable`1<System.DateTimeOffset>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m3877779449_gshared (Nullable_1_t3968840829 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
extern "C"  bool Nullable_1_get_HasValue_m3877779449_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t3968840829  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<DateTimeOffset_t3884714306 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_get_HasValue_m3877779449(&_thisAdjusted, method);
	*reinterpret_cast<DateTimeOffset_t3884714306 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.DateTimeOffset>::get_Value()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral78850559;
extern const uint32_t Nullable_1_get_Value_m3745872087_MetadataUsageId;
extern "C"  DateTimeOffset_t3884714306  Nullable_1_get_Value_m3745872087_gshared (Nullable_1_t3968840829 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_get_Value_m3745872087_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral78850559, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		DateTimeOffset_t3884714306  L_2 = (DateTimeOffset_t3884714306 )__this->get_value_0();
		return L_2;
	}
}
extern "C"  DateTimeOffset_t3884714306  Nullable_1_get_Value_m3745872087_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t3968840829  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<DateTimeOffset_t3884714306 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	DateTimeOffset_t3884714306  _returnValue = Nullable_1_get_Value_m3745872087(&_thisAdjusted, method);
	*reinterpret_cast<DateTimeOffset_t3884714306 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<System.DateTimeOffset>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m1572111781_gshared (Nullable_1_t3968840829 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = (bool)__this->get_has_value_1();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0010:
	{
		Il2CppObject * L_2 = ___other0;
		if (((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))
		{
			goto IL_001d;
		}
	}
	{
		return (bool)0;
	}

IL_001d:
	{
		Il2CppObject * L_3 = ___other0;
		void* L_4 = alloca(sizeof(Nullable_1_t3968840829 ));
		UnBoxNullable(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), L_4);
		bool L_5 = ((  bool (*) (Il2CppObject *, Nullable_1_t3968840829 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(il2cpp_codegen_fake_box((Nullable_1_t3968840829 *)__this), (Nullable_1_t3968840829 )((*(Nullable_1_t3968840829 *)((Nullable_1_t3968840829 *)L_4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_5;
	}
}
extern "C"  bool Nullable_1_Equals_m1572111781_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Nullable_1_t3968840829  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<DateTimeOffset_t3884714306 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m1572111781(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<DateTimeOffset_t3884714306 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<System.DateTimeOffset>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m3552532986_gshared (Nullable_1_t3968840829 * __this, Nullable_1_t3968840829  ___other0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(&___other0)->get_has_value_1();
		bool L_1 = (bool)__this->get_has_value_1();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_has_value_1();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		return (bool)1;
	}

IL_0021:
	{
		DateTimeOffset_t3884714306 * L_3 = (DateTimeOffset_t3884714306 *)(&___other0)->get_address_of_value_0();
		DateTimeOffset_t3884714306  L_4 = (DateTimeOffset_t3884714306 )__this->get_value_0();
		DateTimeOffset_t3884714306  L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		bool L_7 = DateTimeOffset_Equals_m1431331290((DateTimeOffset_t3884714306 *)L_3, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
extern "C"  bool Nullable_1_Equals_m3552532986_AdjustorThunk (Il2CppObject * __this, Nullable_1_t3968840829  ___other0, const MethodInfo* method)
{
	Nullable_1_t3968840829  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<DateTimeOffset_t3884714306 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m3552532986(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<DateTimeOffset_t3884714306 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Int32 System.Nullable`1<System.DateTimeOffset>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m2941497865_gshared (Nullable_1_t3968840829 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		DateTimeOffset_t3884714306 * L_1 = (DateTimeOffset_t3884714306 *)__this->get_address_of_value_0();
		int32_t L_2 = DateTimeOffset_GetHashCode_m1972583858((DateTimeOffset_t3884714306 *)L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
extern "C"  int32_t Nullable_1_GetHashCode_m2941497865_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t3968840829  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<DateTimeOffset_t3884714306 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_GetHashCode_m2941497865(&_thisAdjusted, method);
	*reinterpret_cast<DateTimeOffset_t3884714306 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.DateTimeOffset>::GetValueOrDefault()
extern Il2CppClass* DateTimeOffset_t3884714306_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_GetValueOrDefault_m959858776_MetadataUsageId;
extern "C"  DateTimeOffset_t3884714306  Nullable_1_GetValueOrDefault_m959858776_gshared (Nullable_1_t3968840829 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_GetValueOrDefault_m959858776_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DateTimeOffset_t3884714306  V_0;
	memset(&V_0, 0, sizeof(V_0));
	DateTimeOffset_t3884714306  G_B3_0;
	memset(&G_B3_0, 0, sizeof(G_B3_0));
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		DateTimeOffset_t3884714306  L_1 = (DateTimeOffset_t3884714306 )__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_001f;
	}

IL_0016:
	{
		Initobj (DateTimeOffset_t3884714306_il2cpp_TypeInfo_var, (&V_0));
		DateTimeOffset_t3884714306  L_2 = V_0;
		G_B3_0 = L_2;
	}

IL_001f:
	{
		return G_B3_0;
	}
}
extern "C"  DateTimeOffset_t3884714306  Nullable_1_GetValueOrDefault_m959858776_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t3968840829  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<DateTimeOffset_t3884714306 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	DateTimeOffset_t3884714306  _returnValue = Nullable_1_GetValueOrDefault_m959858776(&_thisAdjusted, method);
	*reinterpret_cast<DateTimeOffset_t3884714306 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.DateTimeOffset>::GetValueOrDefault(T)
extern "C"  DateTimeOffset_t3884714306  Nullable_1_GetValueOrDefault_m3985819654_gshared (Nullable_1_t3968840829 * __this, DateTimeOffset_t3884714306  ___defaultValue0, const MethodInfo* method)
{
	DateTimeOffset_t3884714306  G_B3_0;
	memset(&G_B3_0, 0, sizeof(G_B3_0));
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		DateTimeOffset_t3884714306  L_1 = (DateTimeOffset_t3884714306 )__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_0017;
	}

IL_0016:
	{
		DateTimeOffset_t3884714306  L_2 = ___defaultValue0;
		G_B3_0 = L_2;
	}

IL_0017:
	{
		return G_B3_0;
	}
}
extern "C"  DateTimeOffset_t3884714306  Nullable_1_GetValueOrDefault_m3985819654_AdjustorThunk (Il2CppObject * __this, DateTimeOffset_t3884714306  ___defaultValue0, const MethodInfo* method)
{
	Nullable_1_t3968840829  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<DateTimeOffset_t3884714306 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	DateTimeOffset_t3884714306  _returnValue = Nullable_1_GetValueOrDefault_m3985819654(&_thisAdjusted, ___defaultValue0, method);
	*reinterpret_cast<DateTimeOffset_t3884714306 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.String System.Nullable`1<System.DateTimeOffset>::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_ToString_m2837501533_MetadataUsageId;
extern "C"  String_t* Nullable_1_ToString_m2837501533_gshared (Nullable_1_t3968840829 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_ToString_m2837501533_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		DateTimeOffset_t3884714306 * L_1 = (DateTimeOffset_t3884714306 *)__this->get_address_of_value_0();
		String_t* L_2 = DateTimeOffset_ToString_m983707174((DateTimeOffset_t3884714306 *)L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_3;
	}
}
extern "C"  String_t* Nullable_1_ToString_m2837501533_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t3968840829  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<DateTimeOffset_t3884714306 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	String_t* _returnValue = Nullable_1_ToString_m2837501533(&_thisAdjusted, method);
	*reinterpret_cast<DateTimeOffset_t3884714306 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Void System.Nullable`1<System.Decimal>::.ctor(T)
extern "C"  void Nullable_1__ctor_m1221738939_gshared (Nullable_1_t2038477154 * __this, Decimal_t1954350631  ___value0, const MethodInfo* method)
{
	{
		__this->set_has_value_1((bool)1);
		Decimal_t1954350631  L_0 = ___value0;
		__this->set_value_0(L_0);
		return;
	}
}
extern "C"  void Nullable_1__ctor_m1221738939_AdjustorThunk (Il2CppObject * __this, Decimal_t1954350631  ___value0, const MethodInfo* method)
{
	Nullable_1_t2038477154  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<Decimal_t1954350631 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	Nullable_1__ctor_m1221738939(&_thisAdjusted, ___value0, method);
	*reinterpret_cast<Decimal_t1954350631 *>(__this + 1) = _thisAdjusted.get_value_0();
}
// System.Boolean System.Nullable`1<System.Decimal>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m922964038_gshared (Nullable_1_t2038477154 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
extern "C"  bool Nullable_1_get_HasValue_m922964038_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t2038477154  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<Decimal_t1954350631 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_get_HasValue_m922964038(&_thisAdjusted, method);
	*reinterpret_cast<Decimal_t1954350631 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.Decimal>::get_Value()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral78850559;
extern const uint32_t Nullable_1_get_Value_m3531562206_MetadataUsageId;
extern "C"  Decimal_t1954350631  Nullable_1_get_Value_m3531562206_gshared (Nullable_1_t2038477154 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_get_Value_m3531562206_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral78850559, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		Decimal_t1954350631  L_2 = (Decimal_t1954350631 )__this->get_value_0();
		return L_2;
	}
}
extern "C"  Decimal_t1954350631  Nullable_1_get_Value_m3531562206_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t2038477154  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<Decimal_t1954350631 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	Decimal_t1954350631  _returnValue = Nullable_1_get_Value_m3531562206(&_thisAdjusted, method);
	*reinterpret_cast<Decimal_t1954350631 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<System.Decimal>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m2446029382_gshared (Nullable_1_t2038477154 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = (bool)__this->get_has_value_1();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0010:
	{
		Il2CppObject * L_2 = ___other0;
		if (((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))
		{
			goto IL_001d;
		}
	}
	{
		return (bool)0;
	}

IL_001d:
	{
		Il2CppObject * L_3 = ___other0;
		void* L_4 = alloca(sizeof(Nullable_1_t2038477154 ));
		UnBoxNullable(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), L_4);
		bool L_5 = ((  bool (*) (Il2CppObject *, Nullable_1_t2038477154 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(il2cpp_codegen_fake_box((Nullable_1_t2038477154 *)__this), (Nullable_1_t2038477154 )((*(Nullable_1_t2038477154 *)((Nullable_1_t2038477154 *)L_4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_5;
	}
}
extern "C"  bool Nullable_1_Equals_m2446029382_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Nullable_1_t2038477154  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<Decimal_t1954350631 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m2446029382(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<Decimal_t1954350631 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<System.Decimal>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m3840157241_gshared (Nullable_1_t2038477154 * __this, Nullable_1_t2038477154  ___other0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(&___other0)->get_has_value_1();
		bool L_1 = (bool)__this->get_has_value_1();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_has_value_1();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		return (bool)1;
	}

IL_0021:
	{
		Decimal_t1954350631 * L_3 = (Decimal_t1954350631 *)(&___other0)->get_address_of_value_0();
		Decimal_t1954350631  L_4 = (Decimal_t1954350631 )__this->get_value_0();
		Decimal_t1954350631  L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		bool L_7 = Decimal_Equals_m3414174927((Decimal_t1954350631 *)L_3, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
extern "C"  bool Nullable_1_Equals_m3840157241_AdjustorThunk (Il2CppObject * __this, Nullable_1_t2038477154  ___other0, const MethodInfo* method)
{
	Nullable_1_t2038477154  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<Decimal_t1954350631 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m3840157241(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<Decimal_t1954350631 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Int32 System.Nullable`1<System.Decimal>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m1348870942_gshared (Nullable_1_t2038477154 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		Decimal_t1954350631 * L_1 = (Decimal_t1954350631 *)__this->get_address_of_value_0();
		int32_t L_2 = Decimal_GetHashCode_m3725649587((Decimal_t1954350631 *)L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
extern "C"  int32_t Nullable_1_GetHashCode_m1348870942_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t2038477154  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<Decimal_t1954350631 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_GetHashCode_m1348870942(&_thisAdjusted, method);
	*reinterpret_cast<Decimal_t1954350631 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.Decimal>::GetValueOrDefault()
extern Il2CppClass* Decimal_t1954350631_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_GetValueOrDefault_m212940755_MetadataUsageId;
extern "C"  Decimal_t1954350631  Nullable_1_GetValueOrDefault_m212940755_gshared (Nullable_1_t2038477154 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_GetValueOrDefault_m212940755_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Decimal_t1954350631  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Decimal_t1954350631  G_B3_0;
	memset(&G_B3_0, 0, sizeof(G_B3_0));
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Decimal_t1954350631  L_1 = (Decimal_t1954350631 )__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_001f;
	}

IL_0016:
	{
		Initobj (Decimal_t1954350631_il2cpp_TypeInfo_var, (&V_0));
		Decimal_t1954350631  L_2 = V_0;
		G_B3_0 = L_2;
	}

IL_001f:
	{
		return G_B3_0;
	}
}
extern "C"  Decimal_t1954350631  Nullable_1_GetValueOrDefault_m212940755_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t2038477154  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<Decimal_t1954350631 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	Decimal_t1954350631  _returnValue = Nullable_1_GetValueOrDefault_m212940755(&_thisAdjusted, method);
	*reinterpret_cast<Decimal_t1954350631 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.Decimal>::GetValueOrDefault(T)
extern "C"  Decimal_t1954350631  Nullable_1_GetValueOrDefault_m2306197483_gshared (Nullable_1_t2038477154 * __this, Decimal_t1954350631  ___defaultValue0, const MethodInfo* method)
{
	Decimal_t1954350631  G_B3_0;
	memset(&G_B3_0, 0, sizeof(G_B3_0));
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Decimal_t1954350631  L_1 = (Decimal_t1954350631 )__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_0017;
	}

IL_0016:
	{
		Decimal_t1954350631  L_2 = ___defaultValue0;
		G_B3_0 = L_2;
	}

IL_0017:
	{
		return G_B3_0;
	}
}
extern "C"  Decimal_t1954350631  Nullable_1_GetValueOrDefault_m2306197483_AdjustorThunk (Il2CppObject * __this, Decimal_t1954350631  ___defaultValue0, const MethodInfo* method)
{
	Nullable_1_t2038477154  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<Decimal_t1954350631 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	Decimal_t1954350631  _returnValue = Nullable_1_GetValueOrDefault_m2306197483(&_thisAdjusted, ___defaultValue0, method);
	*reinterpret_cast<Decimal_t1954350631 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.String System.Nullable`1<System.Decimal>::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_ToString_m2866758330_MetadataUsageId;
extern "C"  String_t* Nullable_1_ToString_m2866758330_gshared (Nullable_1_t2038477154 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_ToString_m2866758330_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		Decimal_t1954350631 * L_1 = (Decimal_t1954350631 *)__this->get_address_of_value_0();
		String_t* L_2 = Decimal_ToString_m143310003((Decimal_t1954350631 *)L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_3;
	}
}
extern "C"  String_t* Nullable_1_ToString_m2866758330_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t2038477154  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<Decimal_t1954350631 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	String_t* _returnValue = Nullable_1_ToString_m2866758330(&_thisAdjusted, method);
	*reinterpret_cast<Decimal_t1954350631 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Void System.Nullable`1<System.Double>::.ctor(T)
extern "C"  void Nullable_1__ctor_m800965971_gshared (Nullable_1_t3952353088 * __this, double ___value0, const MethodInfo* method)
{
	{
		__this->set_has_value_1((bool)1);
		double L_0 = ___value0;
		__this->set_value_0(L_0);
		return;
	}
}
extern "C"  void Nullable_1__ctor_m800965971_AdjustorThunk (Il2CppObject * __this, double ___value0, const MethodInfo* method)
{
	Nullable_1_t3952353088  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<double*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	Nullable_1__ctor_m800965971(&_thisAdjusted, ___value0, method);
	*reinterpret_cast<double*>(__this + 1) = _thisAdjusted.get_value_0();
}
// System.Boolean System.Nullable`1<System.Double>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m3299336854_gshared (Nullable_1_t3952353088 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
extern "C"  bool Nullable_1_get_HasValue_m3299336854_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t3952353088  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<double*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_get_HasValue_m3299336854(&_thisAdjusted, method);
	*reinterpret_cast<double*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.Double>::get_Value()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral78850559;
extern const uint32_t Nullable_1_get_Value_m1042825562_MetadataUsageId;
extern "C"  double Nullable_1_get_Value_m1042825562_gshared (Nullable_1_t3952353088 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_get_Value_m1042825562_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral78850559, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		double L_2 = (double)__this->get_value_0();
		return L_2;
	}
}
extern "C"  double Nullable_1_get_Value_m1042825562_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t3952353088  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<double*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	double _returnValue = Nullable_1_get_Value_m1042825562(&_thisAdjusted, method);
	*reinterpret_cast<double*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<System.Double>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m1100904808_gshared (Nullable_1_t3952353088 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = (bool)__this->get_has_value_1();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0010:
	{
		Il2CppObject * L_2 = ___other0;
		if (((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))
		{
			goto IL_001d;
		}
	}
	{
		return (bool)0;
	}

IL_001d:
	{
		Il2CppObject * L_3 = ___other0;
		void* L_4 = alloca(sizeof(Nullable_1_t3952353088 ));
		UnBoxNullable(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), L_4);
		bool L_5 = ((  bool (*) (Il2CppObject *, Nullable_1_t3952353088 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(il2cpp_codegen_fake_box((Nullable_1_t3952353088 *)__this), (Nullable_1_t3952353088 )((*(Nullable_1_t3952353088 *)((Nullable_1_t3952353088 *)L_4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_5;
	}
}
extern "C"  bool Nullable_1_Equals_m1100904808_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Nullable_1_t3952353088  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<double*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m1100904808(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<double*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<System.Double>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m1171469527_gshared (Nullable_1_t3952353088 * __this, Nullable_1_t3952353088  ___other0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(&___other0)->get_has_value_1();
		bool L_1 = (bool)__this->get_has_value_1();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_has_value_1();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		return (bool)1;
	}

IL_0021:
	{
		double* L_3 = (double*)(&___other0)->get_address_of_value_0();
		double L_4 = (double)__this->get_value_0();
		double L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		bool L_7 = Double_Equals_m1597124279((double*)L_3, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
extern "C"  bool Nullable_1_Equals_m1171469527_AdjustorThunk (Il2CppObject * __this, Nullable_1_t3952353088  ___other0, const MethodInfo* method)
{
	Nullable_1_t3952353088  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<double*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m1171469527(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<double*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Int32 System.Nullable`1<System.Double>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m1539710668_gshared (Nullable_1_t3952353088 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		double* L_1 = (double*)__this->get_address_of_value_0();
		int32_t L_2 = Double_GetHashCode_m2106126735((double*)L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
extern "C"  int32_t Nullable_1_GetHashCode_m1539710668_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t3952353088  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<double*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_GetHashCode_m1539710668(&_thisAdjusted, method);
	*reinterpret_cast<double*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.Double>::GetValueOrDefault()
extern Il2CppClass* Double_t3868226565_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_GetValueOrDefault_m469131189_MetadataUsageId;
extern "C"  double Nullable_1_GetValueOrDefault_m469131189_gshared (Nullable_1_t3952353088 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_GetValueOrDefault_m469131189_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	double V_0 = 0.0;
	double G_B3_0 = 0.0;
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		double L_1 = (double)__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_001f;
	}

IL_0016:
	{
		Initobj (Double_t3868226565_il2cpp_TypeInfo_var, (&V_0));
		double L_2 = V_0;
		G_B3_0 = L_2;
	}

IL_001f:
	{
		return G_B3_0;
	}
}
extern "C"  double Nullable_1_GetValueOrDefault_m469131189_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t3952353088  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<double*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	double _returnValue = Nullable_1_GetValueOrDefault_m469131189(&_thisAdjusted, method);
	*reinterpret_cast<double*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.Double>::GetValueOrDefault(T)
extern "C"  double Nullable_1_GetValueOrDefault_m1530600675_gshared (Nullable_1_t3952353088 * __this, double ___defaultValue0, const MethodInfo* method)
{
	double G_B3_0 = 0.0;
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		double L_1 = (double)__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_0017;
	}

IL_0016:
	{
		double L_2 = ___defaultValue0;
		G_B3_0 = L_2;
	}

IL_0017:
	{
		return G_B3_0;
	}
}
extern "C"  double Nullable_1_GetValueOrDefault_m1530600675_AdjustorThunk (Il2CppObject * __this, double ___defaultValue0, const MethodInfo* method)
{
	Nullable_1_t3952353088  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<double*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	double _returnValue = Nullable_1_GetValueOrDefault_m1530600675(&_thisAdjusted, ___defaultValue0, method);
	*reinterpret_cast<double*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.String System.Nullable`1<System.Double>::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_ToString_m2509934458_MetadataUsageId;
extern "C"  String_t* Nullable_1_ToString_m2509934458_gshared (Nullable_1_t3952353088 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_ToString_m2509934458_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		double* L_1 = (double*)__this->get_address_of_value_0();
		String_t* L_2 = Double_ToString_m3380246633((double*)L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_3;
	}
}
extern "C"  String_t* Nullable_1_ToString_m2509934458_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t3952353088  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<double*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	String_t* _returnValue = Nullable_1_ToString_m2509934458(&_thisAdjusted, method);
	*reinterpret_cast<double*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Void System.Nullable`1<System.Guid>::.ctor(T)
extern "C"  void Nullable_1__ctor_m1464594829_gshared (Nullable_1_t2946880952 * __this, Guid_t2862754429  ___value0, const MethodInfo* method)
{
	{
		__this->set_has_value_1((bool)1);
		Guid_t2862754429  L_0 = ___value0;
		__this->set_value_0(L_0);
		return;
	}
}
extern "C"  void Nullable_1__ctor_m1464594829_AdjustorThunk (Il2CppObject * __this, Guid_t2862754429  ___value0, const MethodInfo* method)
{
	Nullable_1_t2946880952  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<Guid_t2862754429 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	Nullable_1__ctor_m1464594829(&_thisAdjusted, ___value0, method);
	*reinterpret_cast<Guid_t2862754429 *>(__this + 1) = _thisAdjusted.get_value_0();
}
// System.Boolean System.Nullable`1<System.Guid>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m403466462_gshared (Nullable_1_t2946880952 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
extern "C"  bool Nullable_1_get_HasValue_m403466462_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t2946880952  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<Guid_t2862754429 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_get_HasValue_m403466462(&_thisAdjusted, method);
	*reinterpret_cast<Guid_t2862754429 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.Guid>::get_Value()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral78850559;
extern const uint32_t Nullable_1_get_Value_m2548406674_MetadataUsageId;
extern "C"  Guid_t2862754429  Nullable_1_get_Value_m2548406674_gshared (Nullable_1_t2946880952 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_get_Value_m2548406674_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral78850559, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		Guid_t2862754429  L_2 = (Guid_t2862754429 )__this->get_value_0();
		return L_2;
	}
}
extern "C"  Guid_t2862754429  Nullable_1_get_Value_m2548406674_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t2946880952  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<Guid_t2862754429 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	Guid_t2862754429  _returnValue = Nullable_1_get_Value_m2548406674(&_thisAdjusted, method);
	*reinterpret_cast<Guid_t2862754429 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<System.Guid>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m3339505760_gshared (Nullable_1_t2946880952 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = (bool)__this->get_has_value_1();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0010:
	{
		Il2CppObject * L_2 = ___other0;
		if (((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))
		{
			goto IL_001d;
		}
	}
	{
		return (bool)0;
	}

IL_001d:
	{
		Il2CppObject * L_3 = ___other0;
		void* L_4 = alloca(sizeof(Nullable_1_t2946880952 ));
		UnBoxNullable(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), L_4);
		bool L_5 = ((  bool (*) (Il2CppObject *, Nullable_1_t2946880952 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(il2cpp_codegen_fake_box((Nullable_1_t2946880952 *)__this), (Nullable_1_t2946880952 )((*(Nullable_1_t2946880952 *)((Nullable_1_t2946880952 *)L_4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_5;
	}
}
extern "C"  bool Nullable_1_Equals_m3339505760_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Nullable_1_t2946880952  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<Guid_t2862754429 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m3339505760(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<Guid_t2862754429 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<System.Guid>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m2950403807_gshared (Nullable_1_t2946880952 * __this, Nullable_1_t2946880952  ___other0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(&___other0)->get_has_value_1();
		bool L_1 = (bool)__this->get_has_value_1();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_has_value_1();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		return (bool)1;
	}

IL_0021:
	{
		Guid_t2862754429 * L_3 = (Guid_t2862754429 *)(&___other0)->get_address_of_value_0();
		Guid_t2862754429  L_4 = (Guid_t2862754429 )__this->get_value_0();
		Guid_t2862754429  L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		bool L_7 = Guid_Equals_m1613304319((Guid_t2862754429 *)L_3, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
extern "C"  bool Nullable_1_Equals_m2950403807_AdjustorThunk (Il2CppObject * __this, Nullable_1_t2946880952  ___other0, const MethodInfo* method)
{
	Nullable_1_t2946880952  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<Guid_t2862754429 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m2950403807(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<Guid_t2862754429 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Int32 System.Nullable`1<System.Guid>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m3813908292_gshared (Nullable_1_t2946880952 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		Guid_t2862754429 * L_1 = (Guid_t2862754429 *)__this->get_address_of_value_0();
		int32_t L_2 = Guid_GetHashCode_m885349207((Guid_t2862754429 *)L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
extern "C"  int32_t Nullable_1_GetHashCode_m3813908292_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t2946880952  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<Guid_t2862754429 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_GetHashCode_m3813908292(&_thisAdjusted, method);
	*reinterpret_cast<Guid_t2862754429 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.Guid>::GetValueOrDefault()
extern Il2CppClass* Guid_t2862754429_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_GetValueOrDefault_m1676531091_MetadataUsageId;
extern "C"  Guid_t2862754429  Nullable_1_GetValueOrDefault_m1676531091_gshared (Nullable_1_t2946880952 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_GetValueOrDefault_m1676531091_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Guid_t2862754429  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Guid_t2862754429  G_B3_0;
	memset(&G_B3_0, 0, sizeof(G_B3_0));
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Guid_t2862754429  L_1 = (Guid_t2862754429 )__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_001f;
	}

IL_0016:
	{
		Initobj (Guid_t2862754429_il2cpp_TypeInfo_var, (&V_0));
		Guid_t2862754429  L_2 = V_0;
		G_B3_0 = L_2;
	}

IL_001f:
	{
		return G_B3_0;
	}
}
extern "C"  Guid_t2862754429  Nullable_1_GetValueOrDefault_m1676531091_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t2946880952  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<Guid_t2862754429 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	Guid_t2862754429  _returnValue = Nullable_1_GetValueOrDefault_m1676531091(&_thisAdjusted, method);
	*reinterpret_cast<Guid_t2862754429 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.Guid>::GetValueOrDefault(T)
extern "C"  Guid_t2862754429  Nullable_1_GetValueOrDefault_m432857643_gshared (Nullable_1_t2946880952 * __this, Guid_t2862754429  ___defaultValue0, const MethodInfo* method)
{
	Guid_t2862754429  G_B3_0;
	memset(&G_B3_0, 0, sizeof(G_B3_0));
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Guid_t2862754429  L_1 = (Guid_t2862754429 )__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_0017;
	}

IL_0016:
	{
		Guid_t2862754429  L_2 = ___defaultValue0;
		G_B3_0 = L_2;
	}

IL_0017:
	{
		return G_B3_0;
	}
}
extern "C"  Guid_t2862754429  Nullable_1_GetValueOrDefault_m432857643_AdjustorThunk (Il2CppObject * __this, Guid_t2862754429  ___defaultValue0, const MethodInfo* method)
{
	Nullable_1_t2946880952  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<Guid_t2862754429 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	Guid_t2862754429  _returnValue = Nullable_1_GetValueOrDefault_m432857643(&_thisAdjusted, ___defaultValue0, method);
	*reinterpret_cast<Guid_t2862754429 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.String System.Nullable`1<System.Guid>::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_ToString_m2800437186_MetadataUsageId;
extern "C"  String_t* Nullable_1_ToString_m2800437186_gshared (Nullable_1_t2946880952 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_ToString_m2800437186_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		Guid_t2862754429 * L_1 = (Guid_t2862754429 *)__this->get_address_of_value_0();
		String_t* L_2 = Guid_ToString_m2528531937((Guid_t2862754429 *)L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_3;
	}
}
extern "C"  String_t* Nullable_1_ToString_m2800437186_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t2946880952  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<Guid_t2862754429 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	String_t* _returnValue = Nullable_1_ToString_m2800437186(&_thisAdjusted, method);
	*reinterpret_cast<Guid_t2862754429 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Void System.Nullable`1<System.Int16>::.ctor(T)
extern "C"  void Nullable_1__ctor_m782064958_gshared (Nullable_1_t1237964965 * __this, int16_t ___value0, const MethodInfo* method)
{
	{
		__this->set_has_value_1((bool)1);
		int16_t L_0 = ___value0;
		__this->set_value_0(L_0);
		return;
	}
}
extern "C"  void Nullable_1__ctor_m782064958_AdjustorThunk (Il2CppObject * __this, int16_t ___value0, const MethodInfo* method)
{
	Nullable_1_t1237964965  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int16_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	Nullable_1__ctor_m782064958(&_thisAdjusted, ___value0, method);
	*reinterpret_cast<int16_t*>(__this + 1) = _thisAdjusted.get_value_0();
}
// System.Boolean System.Nullable`1<System.Int16>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m3682169251_gshared (Nullable_1_t1237964965 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
extern "C"  bool Nullable_1_get_HasValue_m3682169251_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t1237964965  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int16_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_get_HasValue_m3682169251(&_thisAdjusted, method);
	*reinterpret_cast<int16_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.Int16>::get_Value()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral78850559;
extern const uint32_t Nullable_1_get_Value_m3601282849_MetadataUsageId;
extern "C"  int16_t Nullable_1_get_Value_m3601282849_gshared (Nullable_1_t1237964965 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_get_Value_m3601282849_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral78850559, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		int16_t L_2 = (int16_t)__this->get_value_0();
		return L_2;
	}
}
extern "C"  int16_t Nullable_1_get_Value_m3601282849_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t1237964965  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int16_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int16_t _returnValue = Nullable_1_get_Value_m3601282849(&_thisAdjusted, method);
	*reinterpret_cast<int16_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<System.Int16>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m3553249289_gshared (Nullable_1_t1237964965 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = (bool)__this->get_has_value_1();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0010:
	{
		Il2CppObject * L_2 = ___other0;
		if (((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))
		{
			goto IL_001d;
		}
	}
	{
		return (bool)0;
	}

IL_001d:
	{
		Il2CppObject * L_3 = ___other0;
		void* L_4 = alloca(sizeof(Nullable_1_t1237964965 ));
		UnBoxNullable(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), L_4);
		bool L_5 = ((  bool (*) (Il2CppObject *, Nullable_1_t1237964965 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(il2cpp_codegen_fake_box((Nullable_1_t1237964965 *)__this), (Nullable_1_t1237964965 )((*(Nullable_1_t1237964965 *)((Nullable_1_t1237964965 *)L_4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_5;
	}
}
extern "C"  bool Nullable_1_Equals_m3553249289_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Nullable_1_t1237964965  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int16_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m3553249289(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<int16_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<System.Int16>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m2214649622_gshared (Nullable_1_t1237964965 * __this, Nullable_1_t1237964965  ___other0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(&___other0)->get_has_value_1();
		bool L_1 = (bool)__this->get_has_value_1();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_has_value_1();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		return (bool)1;
	}

IL_0021:
	{
		int16_t* L_3 = (int16_t*)(&___other0)->get_address_of_value_0();
		int16_t L_4 = (int16_t)__this->get_value_0();
		int16_t L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		bool L_7 = Int16_Equals_m3652534636((int16_t*)L_3, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
extern "C"  bool Nullable_1_Equals_m2214649622_AdjustorThunk (Il2CppObject * __this, Nullable_1_t1237964965  ___other0, const MethodInfo* method)
{
	Nullable_1_t1237964965  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int16_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m2214649622(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<int16_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Int32 System.Nullable`1<System.Int16>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m988146273_gshared (Nullable_1_t1237964965 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		int16_t* L_1 = (int16_t*)__this->get_address_of_value_0();
		int32_t L_2 = Int16_GetHashCode_m2943154640((int16_t*)L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
extern "C"  int32_t Nullable_1_GetHashCode_m988146273_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t1237964965  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int16_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_GetHashCode_m988146273(&_thisAdjusted, method);
	*reinterpret_cast<int16_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.Int16>::GetValueOrDefault()
extern Il2CppClass* Int16_t1153838442_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_GetValueOrDefault_m3540221334_MetadataUsageId;
extern "C"  int16_t Nullable_1_GetValueOrDefault_m3540221334_gshared (Nullable_1_t1237964965 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_GetValueOrDefault_m3540221334_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int16_t V_0 = 0;
	int16_t G_B3_0 = 0;
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		int16_t L_1 = (int16_t)__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_001f;
	}

IL_0016:
	{
		Initobj (Int16_t1153838442_il2cpp_TypeInfo_var, (&V_0));
		int16_t L_2 = V_0;
		G_B3_0 = L_2;
	}

IL_001f:
	{
		return G_B3_0;
	}
}
extern "C"  int16_t Nullable_1_GetValueOrDefault_m3540221334_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t1237964965  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int16_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int16_t _returnValue = Nullable_1_GetValueOrDefault_m3540221334(&_thisAdjusted, method);
	*reinterpret_cast<int16_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.Int16>::GetValueOrDefault(T)
extern "C"  int16_t Nullable_1_GetValueOrDefault_m2372680328_gshared (Nullable_1_t1237964965 * __this, int16_t ___defaultValue0, const MethodInfo* method)
{
	int16_t G_B3_0 = 0;
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		int16_t L_1 = (int16_t)__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_0017;
	}

IL_0016:
	{
		int16_t L_2 = ___defaultValue0;
		G_B3_0 = L_2;
	}

IL_0017:
	{
		return G_B3_0;
	}
}
extern "C"  int16_t Nullable_1_GetValueOrDefault_m2372680328_AdjustorThunk (Il2CppObject * __this, int16_t ___defaultValue0, const MethodInfo* method)
{
	Nullable_1_t1237964965  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int16_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int16_t _returnValue = Nullable_1_GetValueOrDefault_m2372680328(&_thisAdjusted, ___defaultValue0, method);
	*reinterpret_cast<int16_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.String System.Nullable`1<System.Int16>::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_ToString_m1779076247_MetadataUsageId;
extern "C"  String_t* Nullable_1_ToString_m1779076247_gshared (Nullable_1_t1237964965 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_ToString_m1779076247_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		int16_t* L_1 = (int16_t*)__this->get_address_of_value_0();
		String_t* L_2 = Int16_ToString_m1124031606((int16_t*)L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_3;
	}
}
extern "C"  String_t* Nullable_1_ToString_m1779076247_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t1237964965  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int16_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	String_t* _returnValue = Nullable_1_ToString_m1779076247(&_thisAdjusted, method);
	*reinterpret_cast<int16_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Void System.Nullable`1<System.Int32>::.ctor(T)
extern "C"  void Nullable_1__ctor_m944559736_gshared (Nullable_1_t1237965023 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		__this->set_has_value_1((bool)1);
		int32_t L_0 = ___value0;
		__this->set_value_0(L_0);
		return;
	}
}
extern "C"  void Nullable_1__ctor_m944559736_AdjustorThunk (Il2CppObject * __this, int32_t ___value0, const MethodInfo* method)
{
	Nullable_1_t1237965023  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	Nullable_1__ctor_m944559736(&_thisAdjusted, ___value0, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
}
// System.Boolean System.Nullable`1<System.Int32>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m1686547625_gshared (Nullable_1_t1237965023 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
extern "C"  bool Nullable_1_get_HasValue_m1686547625_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t1237965023  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_get_HasValue_m1686547625(&_thisAdjusted, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.Int32>::get_Value()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral78850559;
extern const uint32_t Nullable_1_get_Value_m844974555_MetadataUsageId;
extern "C"  int32_t Nullable_1_get_Value_m844974555_gshared (Nullable_1_t1237965023 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_get_Value_m844974555_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral78850559, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		int32_t L_2 = (int32_t)__this->get_value_0();
		return L_2;
	}
}
extern "C"  int32_t Nullable_1_get_Value_m844974555_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t1237965023  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_get_Value_m844974555(&_thisAdjusted, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<System.Int32>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m3334191683_gshared (Nullable_1_t1237965023 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = (bool)__this->get_has_value_1();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0010:
	{
		Il2CppObject * L_2 = ___other0;
		if (((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))
		{
			goto IL_001d;
		}
	}
	{
		return (bool)0;
	}

IL_001d:
	{
		Il2CppObject * L_3 = ___other0;
		void* L_4 = alloca(sizeof(Nullable_1_t1237965023 ));
		UnBoxNullable(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), L_4);
		bool L_5 = ((  bool (*) (Il2CppObject *, Nullable_1_t1237965023 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(il2cpp_codegen_fake_box((Nullable_1_t1237965023 *)__this), (Nullable_1_t1237965023 )((*(Nullable_1_t1237965023 *)((Nullable_1_t1237965023 *)L_4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_5;
	}
}
extern "C"  bool Nullable_1_Equals_m3334191683_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Nullable_1_t1237965023  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m3334191683(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<System.Int32>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m1592743836_gshared (Nullable_1_t1237965023 * __this, Nullable_1_t1237965023  ___other0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(&___other0)->get_has_value_1();
		bool L_1 = (bool)__this->get_has_value_1();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_has_value_1();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		return (bool)1;
	}

IL_0021:
	{
		int32_t* L_3 = (int32_t*)(&___other0)->get_address_of_value_0();
		int32_t L_4 = (int32_t)__this->get_value_0();
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		bool L_7 = Int32_Equals_m4061110258((int32_t*)L_3, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
extern "C"  bool Nullable_1_Equals_m1592743836_AdjustorThunk (Il2CppObject * __this, Nullable_1_t1237965023  ___other0, const MethodInfo* method)
{
	Nullable_1_t1237965023  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m1592743836(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Int32 System.Nullable`1<System.Int32>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m2170697371_gshared (Nullable_1_t1237965023 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		int32_t* L_1 = (int32_t*)__this->get_address_of_value_0();
		int32_t L_2 = Int32_GetHashCode_m3396943446((int32_t*)L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
extern "C"  int32_t Nullable_1_GetHashCode_m2170697371_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t1237965023  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_GetHashCode_m2170697371(&_thisAdjusted, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.Int32>::GetValueOrDefault()
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_GetValueOrDefault_m3844036406_MetadataUsageId;
extern "C"  int32_t Nullable_1_GetValueOrDefault_m3844036406_gshared (Nullable_1_t1237965023 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_GetValueOrDefault_m3844036406_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t G_B3_0 = 0;
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_001f;
	}

IL_0016:
	{
		Initobj (Int32_t1153838500_il2cpp_TypeInfo_var, (&V_0));
		int32_t L_2 = V_0;
		G_B3_0 = L_2;
	}

IL_001f:
	{
		return G_B3_0;
	}
}
extern "C"  int32_t Nullable_1_GetValueOrDefault_m3844036406_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t1237965023  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_GetValueOrDefault_m3844036406(&_thisAdjusted, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.Int32>::GetValueOrDefault(T)
extern "C"  int32_t Nullable_1_GetValueOrDefault_m2781255950_gshared (Nullable_1_t1237965023 * __this, int32_t ___defaultValue0, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_0017;
	}

IL_0016:
	{
		int32_t L_2 = ___defaultValue0;
		G_B3_0 = L_2;
	}

IL_0017:
	{
		return G_B3_0;
	}
}
extern "C"  int32_t Nullable_1_GetValueOrDefault_m2781255950_AdjustorThunk (Il2CppObject * __this, int32_t ___defaultValue0, const MethodInfo* method)
{
	Nullable_1_t1237965023  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_GetValueOrDefault_m2781255950(&_thisAdjusted, ___defaultValue0, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.String System.Nullable`1<System.Int32>::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_ToString_m2521447069_MetadataUsageId;
extern "C"  String_t* Nullable_1_ToString_m2521447069_gshared (Nullable_1_t1237965023 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_ToString_m2521447069_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		int32_t* L_1 = (int32_t*)__this->get_address_of_value_0();
		String_t* L_2 = Int32_ToString_m1286526384((int32_t*)L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_3;
	}
}
extern "C"  String_t* Nullable_1_ToString_m2521447069_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t1237965023  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	String_t* _returnValue = Nullable_1_ToString_m2521447069(&_thisAdjusted, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Void System.Nullable`1<System.Int64>::.ctor(T)
extern "C"  void Nullable_1__ctor_m2237001290_gshared (Nullable_1_t1237965118 * __this, int64_t ___value0, const MethodInfo* method)
{
	{
		__this->set_has_value_1((bool)1);
		int64_t L_0 = ___value0;
		__this->set_value_0(L_0);
		return;
	}
}
extern "C"  void Nullable_1__ctor_m2237001290_AdjustorThunk (Il2CppObject * __this, int64_t ___value0, const MethodInfo* method)
{
	Nullable_1_t1237965118  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int64_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	Nullable_1__ctor_m2237001290(&_thisAdjusted, ___value0, method);
	*reinterpret_cast<int64_t*>(__this + 1) = _thisAdjusted.get_value_0();
}
// System.Boolean System.Nullable`1<System.Int64>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m2752225565_gshared (Nullable_1_t1237965118 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
extern "C"  bool Nullable_1_get_HasValue_m2752225565_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t1237965118  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int64_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_get_HasValue_m2752225565(&_thisAdjusted, method);
	*reinterpret_cast<int64_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.Int64>::get_Value()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral78850559;
extern const uint32_t Nullable_1_get_Value_m3029304679_MetadataUsageId;
extern "C"  int64_t Nullable_1_get_Value_m3029304679_gshared (Nullable_1_t1237965118 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_get_Value_m3029304679_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral78850559, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		int64_t L_2 = (int64_t)__this->get_value_0();
		return L_2;
	}
}
extern "C"  int64_t Nullable_1_get_Value_m3029304679_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t1237965118  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int64_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int64_t _returnValue = Nullable_1_get_Value_m3029304679(&_thisAdjusted, method);
	*reinterpret_cast<int64_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<System.Int64>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m3567799714_gshared (Nullable_1_t1237965118 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = (bool)__this->get_has_value_1();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0010:
	{
		Il2CppObject * L_2 = ___other0;
		if (((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))
		{
			goto IL_001d;
		}
	}
	{
		return (bool)0;
	}

IL_001d:
	{
		Il2CppObject * L_3 = ___other0;
		void* L_4 = alloca(sizeof(Nullable_1_t1237965118 ));
		UnBoxNullable(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), L_4);
		bool L_5 = ((  bool (*) (Il2CppObject *, Nullable_1_t1237965118 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(il2cpp_codegen_fake_box((Nullable_1_t1237965118 *)__this), (Nullable_1_t1237965118 )((*(Nullable_1_t1237965118 *)((Nullable_1_t1237965118 *)L_4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_5;
	}
}
extern "C"  bool Nullable_1_Equals_m3567799714_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Nullable_1_t1237965118  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int64_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m3567799714(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<int64_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<System.Int64>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m3906407261_gshared (Nullable_1_t1237965118 * __this, Nullable_1_t1237965118  ___other0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(&___other0)->get_has_value_1();
		bool L_1 = (bool)__this->get_has_value_1();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_has_value_1();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		return (bool)1;
	}

IL_0021:
	{
		int64_t* L_3 = (int64_t*)(&___other0)->get_address_of_value_0();
		int64_t L_4 = (int64_t)__this->get_value_0();
		int64_t L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		bool L_7 = Int64_Equals_m1990436019((int64_t*)L_3, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
extern "C"  bool Nullable_1_Equals_m3906407261_AdjustorThunk (Il2CppObject * __this, Nullable_1_t1237965118  ___other0, const MethodInfo* method)
{
	Nullable_1_t1237965118  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int64_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m3906407261(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<int64_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Int32 System.Nullable`1<System.Int64>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m1886099706_gshared (Nullable_1_t1237965118 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		int64_t* L_1 = (int64_t*)__this->get_address_of_value_0();
		int32_t L_2 = Int64_GetHashCode_m2140836887((int64_t*)L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
extern "C"  int32_t Nullable_1_GetHashCode_m1886099706_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t1237965118  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int64_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_GetHashCode_m1886099706(&_thisAdjusted, method);
	*reinterpret_cast<int64_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.Int64>::GetValueOrDefault()
extern Il2CppClass* Int64_t1153838595_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_GetValueOrDefault_m715658607_MetadataUsageId;
extern "C"  int64_t Nullable_1_GetValueOrDefault_m715658607_gshared (Nullable_1_t1237965118 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_GetValueOrDefault_m715658607_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int64_t V_0 = 0;
	int64_t G_B3_0 = 0;
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		int64_t L_1 = (int64_t)__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_001f;
	}

IL_0016:
	{
		Initobj (Int64_t1153838595_il2cpp_TypeInfo_var, (&V_0));
		int64_t L_2 = V_0;
		G_B3_0 = L_2;
	}

IL_001f:
	{
		return G_B3_0;
	}
}
extern "C"  int64_t Nullable_1_GetValueOrDefault_m715658607_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t1237965118  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int64_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int64_t _returnValue = Nullable_1_GetValueOrDefault_m715658607(&_thisAdjusted, method);
	*reinterpret_cast<int64_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.Int64>::GetValueOrDefault(T)
extern "C"  int64_t Nullable_1_GetValueOrDefault_m710581711_gshared (Nullable_1_t1237965118 * __this, int64_t ___defaultValue0, const MethodInfo* method)
{
	int64_t G_B3_0 = 0;
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		int64_t L_1 = (int64_t)__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_0017;
	}

IL_0016:
	{
		int64_t L_2 = ___defaultValue0;
		G_B3_0 = L_2;
	}

IL_0017:
	{
		return G_B3_0;
	}
}
extern "C"  int64_t Nullable_1_GetValueOrDefault_m710581711_AdjustorThunk (Il2CppObject * __this, int64_t ___defaultValue0, const MethodInfo* method)
{
	Nullable_1_t1237965118  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int64_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int64_t _returnValue = Nullable_1_GetValueOrDefault_m710581711(&_thisAdjusted, ___defaultValue0, method);
	*reinterpret_cast<int64_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.String System.Nullable`1<System.Int64>::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_ToString_m1738017950_MetadataUsageId;
extern "C"  String_t* Nullable_1_ToString_m1738017950_gshared (Nullable_1_t1237965118 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_ToString_m1738017950_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		int64_t* L_1 = (int64_t*)__this->get_address_of_value_0();
		String_t* L_2 = Int64_ToString_m3478011791((int64_t*)L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_3;
	}
}
extern "C"  String_t* Nullable_1_ToString_m1738017950_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t1237965118  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int64_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	String_t* _returnValue = Nullable_1_ToString_m1738017950(&_thisAdjusted, method);
	*reinterpret_cast<int64_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Void System.Nullable`1<System.SByte>::.ctor(T)
extern "C"  void Nullable_1__ctor_m3134090465_gshared (Nullable_1_t1245896300 * __this, int8_t ___value0, const MethodInfo* method)
{
	{
		__this->set_has_value_1((bool)1);
		int8_t L_0 = ___value0;
		__this->set_value_0(L_0);
		return;
	}
}
extern "C"  void Nullable_1__ctor_m3134090465_AdjustorThunk (Il2CppObject * __this, int8_t ___value0, const MethodInfo* method)
{
	Nullable_1_t1245896300  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int8_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	Nullable_1__ctor_m3134090465(&_thisAdjusted, ___value0, method);
	*reinterpret_cast<int8_t*>(__this + 1) = _thisAdjusted.get_value_0();
}
// System.Boolean System.Nullable`1<System.SByte>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m1660015292_gshared (Nullable_1_t1245896300 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
extern "C"  bool Nullable_1_get_HasValue_m1660015292_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t1245896300  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int8_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_get_HasValue_m1660015292(&_thisAdjusted, method);
	*reinterpret_cast<int8_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.SByte>::get_Value()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral78850559;
extern const uint32_t Nullable_1_get_Value_m839062696_MetadataUsageId;
extern "C"  int8_t Nullable_1_get_Value_m839062696_gshared (Nullable_1_t1245896300 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_get_Value_m839062696_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral78850559, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		int8_t L_2 = (int8_t)__this->get_value_0();
		return L_2;
	}
}
extern "C"  int8_t Nullable_1_get_Value_m839062696_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t1245896300  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int8_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int8_t _returnValue = Nullable_1_get_Value_m839062696(&_thisAdjusted, method);
	*reinterpret_cast<int8_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<System.SByte>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m3964676304_gshared (Nullable_1_t1245896300 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = (bool)__this->get_has_value_1();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0010:
	{
		Il2CppObject * L_2 = ___other0;
		if (((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))
		{
			goto IL_001d;
		}
	}
	{
		return (bool)0;
	}

IL_001d:
	{
		Il2CppObject * L_3 = ___other0;
		void* L_4 = alloca(sizeof(Nullable_1_t1245896300 ));
		UnBoxNullable(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), L_4);
		bool L_5 = ((  bool (*) (Il2CppObject *, Nullable_1_t1245896300 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(il2cpp_codegen_fake_box((Nullable_1_t1245896300 *)__this), (Nullable_1_t1245896300 )((*(Nullable_1_t1245896300 *)((Nullable_1_t1245896300 *)L_4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_5;
	}
}
extern "C"  bool Nullable_1_Equals_m3964676304_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Nullable_1_t1245896300  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int8_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m3964676304(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<int8_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<System.SByte>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m2200494191_gshared (Nullable_1_t1245896300 * __this, Nullable_1_t1245896300  ___other0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(&___other0)->get_has_value_1();
		bool L_1 = (bool)__this->get_has_value_1();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_has_value_1();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		return (bool)1;
	}

IL_0021:
	{
		int8_t* L_3 = (int8_t*)(&___other0)->get_address_of_value_0();
		int8_t L_4 = (int8_t)__this->get_value_0();
		int8_t L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		bool L_7 = SByte_Equals_m1310501829((int8_t*)L_3, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
extern "C"  bool Nullable_1_Equals_m2200494191_AdjustorThunk (Il2CppObject * __this, Nullable_1_t1245896300  ___other0, const MethodInfo* method)
{
	Nullable_1_t1245896300  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int8_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m2200494191(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<int8_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Int32 System.Nullable`1<System.SByte>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m784368168_gshared (Nullable_1_t1245896300 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		int8_t* L_1 = (int8_t*)__this->get_address_of_value_0();
		int32_t L_2 = SByte_GetHashCode_m3213675817((int8_t*)L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
extern "C"  int32_t Nullable_1_GetHashCode_m784368168_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t1245896300  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int8_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_GetHashCode_m784368168(&_thisAdjusted, method);
	*reinterpret_cast<int8_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.SByte>::GetValueOrDefault()
extern Il2CppClass* SByte_t1161769777_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_GetValueOrDefault_m3880313885_MetadataUsageId;
extern "C"  int8_t Nullable_1_GetValueOrDefault_m3880313885_gshared (Nullable_1_t1245896300 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_GetValueOrDefault_m3880313885_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int8_t V_0 = 0x0;
	int8_t G_B3_0 = 0x0;
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		int8_t L_1 = (int8_t)__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_001f;
	}

IL_0016:
	{
		Initobj (SByte_t1161769777_il2cpp_TypeInfo_var, (&V_0));
		int8_t L_2 = V_0;
		G_B3_0 = L_2;
	}

IL_001f:
	{
		return G_B3_0;
	}
}
extern "C"  int8_t Nullable_1_GetValueOrDefault_m3880313885_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t1245896300  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int8_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int8_t _returnValue = Nullable_1_GetValueOrDefault_m3880313885(&_thisAdjusted, method);
	*reinterpret_cast<int8_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.SByte>::GetValueOrDefault(T)
extern "C"  int8_t Nullable_1_GetValueOrDefault_m30647521_gshared (Nullable_1_t1245896300 * __this, int8_t ___defaultValue0, const MethodInfo* method)
{
	int8_t G_B3_0 = 0x0;
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		int8_t L_1 = (int8_t)__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_0017;
	}

IL_0016:
	{
		int8_t L_2 = ___defaultValue0;
		G_B3_0 = L_2;
	}

IL_0017:
	{
		return G_B3_0;
	}
}
extern "C"  int8_t Nullable_1_GetValueOrDefault_m30647521_AdjustorThunk (Il2CppObject * __this, int8_t ___defaultValue0, const MethodInfo* method)
{
	Nullable_1_t1245896300  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int8_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int8_t _returnValue = Nullable_1_GetValueOrDefault_m30647521(&_thisAdjusted, ___defaultValue0, method);
	*reinterpret_cast<int8_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.String System.Nullable`1<System.SByte>::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_ToString_m2659803696_MetadataUsageId;
extern "C"  String_t* Nullable_1_ToString_m2659803696_gshared (Nullable_1_t1245896300 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_ToString_m2659803696_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		int8_t* L_1 = (int8_t*)__this->get_address_of_value_0();
		String_t* L_2 = SByte_ToString_m1290989501((int8_t*)L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_3;
	}
}
extern "C"  String_t* Nullable_1_ToString_m2659803696_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t1245896300  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int8_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	String_t* _returnValue = Nullable_1_ToString_m2659803696(&_thisAdjusted, method);
	*reinterpret_cast<int8_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Void System.Nullable`1<System.Single>::.ctor(T)
extern "C"  void Nullable_1__ctor_m1721422666_gshared (Nullable_1_t81078199 * __this, float ___value0, const MethodInfo* method)
{
	{
		__this->set_has_value_1((bool)1);
		float L_0 = ___value0;
		__this->set_value_0(L_0);
		return;
	}
}
extern "C"  void Nullable_1__ctor_m1721422666_AdjustorThunk (Il2CppObject * __this, float ___value0, const MethodInfo* method)
{
	Nullable_1_t81078199  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<float*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	Nullable_1__ctor_m1721422666(&_thisAdjusted, ___value0, method);
	*reinterpret_cast<float*>(__this + 1) = _thisAdjusted.get_value_0();
}
// System.Boolean System.Nullable`1<System.Single>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m610793215_gshared (Nullable_1_t81078199 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
extern "C"  bool Nullable_1_get_HasValue_m610793215_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t81078199  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<float*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_get_HasValue_m610793215(&_thisAdjusted, method);
	*reinterpret_cast<float*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.Single>::get_Value()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral78850559;
extern const uint32_t Nullable_1_get_Value_m838446481_MetadataUsageId;
extern "C"  float Nullable_1_get_Value_m838446481_gshared (Nullable_1_t81078199 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_get_Value_m838446481_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral78850559, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		float L_2 = (float)__this->get_value_0();
		return L_2;
	}
}
extern "C"  float Nullable_1_get_Value_m838446481_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t81078199  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<float*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	float _returnValue = Nullable_1_get_Value_m838446481(&_thisAdjusted, method);
	*reinterpret_cast<float*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<System.Single>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m3703262431_gshared (Nullable_1_t81078199 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = (bool)__this->get_has_value_1();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0010:
	{
		Il2CppObject * L_2 = ___other0;
		if (((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))
		{
			goto IL_001d;
		}
	}
	{
		return (bool)0;
	}

IL_001d:
	{
		Il2CppObject * L_3 = ___other0;
		void* L_4 = alloca(sizeof(Nullable_1_t81078199 ));
		UnBoxNullable(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), L_4);
		bool L_5 = ((  bool (*) (Il2CppObject *, Nullable_1_t81078199 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(il2cpp_codegen_fake_box((Nullable_1_t81078199 *)__this), (Nullable_1_t81078199 )((*(Nullable_1_t81078199 *)((Nullable_1_t81078199 *)L_4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_5;
	}
}
extern "C"  bool Nullable_1_Equals_m3703262431_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Nullable_1_t81078199  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<float*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m3703262431(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<float*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<System.Single>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m350150016_gshared (Nullable_1_t81078199 * __this, Nullable_1_t81078199  ___other0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(&___other0)->get_has_value_1();
		bool L_1 = (bool)__this->get_has_value_1();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_has_value_1();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		return (bool)1;
	}

IL_0021:
	{
		float* L_3 = (float*)(&___other0)->get_address_of_value_0();
		float L_4 = (float)__this->get_value_0();
		float L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		bool L_7 = Single_Equals_m2650902624((float*)L_3, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
extern "C"  bool Nullable_1_Equals_m350150016_AdjustorThunk (Il2CppObject * __this, Nullable_1_t81078199  ___other0, const MethodInfo* method)
{
	Nullable_1_t81078199  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<float*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m350150016(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<float*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Int32 System.Nullable`1<System.Single>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m2699909443_gshared (Nullable_1_t81078199 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		float* L_1 = (float*)__this->get_address_of_value_0();
		int32_t L_2 = Single_GetHashCode_m65342520((float*)L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
extern "C"  int32_t Nullable_1_GetHashCode_m2699909443_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t81078199  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<float*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_GetHashCode_m2699909443(&_thisAdjusted, method);
	*reinterpret_cast<float*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.Single>::GetValueOrDefault()
extern Il2CppClass* Single_t4291918972_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_GetValueOrDefault_m1330293010_MetadataUsageId;
extern "C"  float Nullable_1_GetValueOrDefault_m1330293010_gshared (Nullable_1_t81078199 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_GetValueOrDefault_m1330293010_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float G_B3_0 = 0.0f;
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		float L_1 = (float)__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_001f;
	}

IL_0016:
	{
		Initobj (Single_t4291918972_il2cpp_TypeInfo_var, (&V_0));
		float L_2 = V_0;
		G_B3_0 = L_2;
	}

IL_001f:
	{
		return G_B3_0;
	}
}
extern "C"  float Nullable_1_GetValueOrDefault_m1330293010_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t81078199  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<float*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	float _returnValue = Nullable_1_GetValueOrDefault_m1330293010(&_thisAdjusted, method);
	*reinterpret_cast<float*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.Single>::GetValueOrDefault(T)
extern "C"  float Nullable_1_GetValueOrDefault_m2584379020_gshared (Nullable_1_t81078199 * __this, float ___defaultValue0, const MethodInfo* method)
{
	float G_B3_0 = 0.0f;
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		float L_1 = (float)__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_0017;
	}

IL_0016:
	{
		float L_2 = ___defaultValue0;
		G_B3_0 = L_2;
	}

IL_0017:
	{
		return G_B3_0;
	}
}
extern "C"  float Nullable_1_GetValueOrDefault_m2584379020_AdjustorThunk (Il2CppObject * __this, float ___defaultValue0, const MethodInfo* method)
{
	Nullable_1_t81078199  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<float*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	float _returnValue = Nullable_1_GetValueOrDefault_m2584379020(&_thisAdjusted, ___defaultValue0, method);
	*reinterpret_cast<float*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.String System.Nullable`1<System.Single>::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_ToString_m979320931_MetadataUsageId;
extern "C"  String_t* Nullable_1_ToString_m979320931_gshared (Nullable_1_t81078199 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_ToString_m979320931_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		float* L_1 = (float*)__this->get_address_of_value_0();
		String_t* L_2 = Single_ToString_m5736032((float*)L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_3;
	}
}
extern "C"  String_t* Nullable_1_ToString_m979320931_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t81078199  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<float*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	String_t* _returnValue = Nullable_1_ToString_m979320931(&_thisAdjusted, method);
	*reinterpret_cast<float*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Void System.Nullable`1<System.TimeSpan>::.ctor(T)
extern "C"  void Nullable_1__ctor_m4008503583_gshared (Nullable_1_t497649510 * __this, TimeSpan_t413522987  ___value0, const MethodInfo* method)
{
	{
		__this->set_has_value_1((bool)1);
		TimeSpan_t413522987  L_0 = ___value0;
		__this->set_value_0(L_0);
		return;
	}
}
extern "C"  void Nullable_1__ctor_m4008503583_AdjustorThunk (Il2CppObject * __this, TimeSpan_t413522987  ___value0, const MethodInfo* method)
{
	Nullable_1_t497649510  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<TimeSpan_t413522987 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	Nullable_1__ctor_m4008503583(&_thisAdjusted, ___value0, method);
	*reinterpret_cast<TimeSpan_t413522987 *>(__this + 1) = _thisAdjusted.get_value_0();
}
// System.Boolean System.Nullable`1<System.TimeSpan>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m2797118855_gshared (Nullable_1_t497649510 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
extern "C"  bool Nullable_1_get_HasValue_m2797118855_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t497649510  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<TimeSpan_t413522987 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_get_HasValue_m2797118855(&_thisAdjusted, method);
	*reinterpret_cast<TimeSpan_t413522987 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.TimeSpan>::get_Value()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral78850559;
extern const uint32_t Nullable_1_get_Value_m3338249190_MetadataUsageId;
extern "C"  TimeSpan_t413522987  Nullable_1_get_Value_m3338249190_gshared (Nullable_1_t497649510 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_get_Value_m3338249190_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral78850559, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		TimeSpan_t413522987  L_2 = (TimeSpan_t413522987 )__this->get_value_0();
		return L_2;
	}
}
extern "C"  TimeSpan_t413522987  Nullable_1_get_Value_m3338249190_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t497649510  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<TimeSpan_t413522987 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	TimeSpan_t413522987  _returnValue = Nullable_1_get_Value_m3338249190(&_thisAdjusted, method);
	*reinterpret_cast<TimeSpan_t413522987 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<System.TimeSpan>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m2158814990_gshared (Nullable_1_t497649510 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = (bool)__this->get_has_value_1();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0010:
	{
		Il2CppObject * L_2 = ___other0;
		if (((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))
		{
			goto IL_001d;
		}
	}
	{
		return (bool)0;
	}

IL_001d:
	{
		Il2CppObject * L_3 = ___other0;
		void* L_4 = alloca(sizeof(Nullable_1_t497649510 ));
		UnBoxNullable(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), L_4);
		bool L_5 = ((  bool (*) (Il2CppObject *, Nullable_1_t497649510 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(il2cpp_codegen_fake_box((Nullable_1_t497649510 *)__this), (Nullable_1_t497649510 )((*(Nullable_1_t497649510 *)((Nullable_1_t497649510 *)L_4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_5;
	}
}
extern "C"  bool Nullable_1_Equals_m2158814990_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Nullable_1_t497649510  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<TimeSpan_t413522987 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m2158814990(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<TimeSpan_t413522987 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<System.TimeSpan>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m3609411697_gshared (Nullable_1_t497649510 * __this, Nullable_1_t497649510  ___other0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(&___other0)->get_has_value_1();
		bool L_1 = (bool)__this->get_has_value_1();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_has_value_1();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		return (bool)1;
	}

IL_0021:
	{
		TimeSpan_t413522987 * L_3 = (TimeSpan_t413522987 *)(&___other0)->get_address_of_value_0();
		TimeSpan_t413522987  L_4 = (TimeSpan_t413522987 )__this->get_value_0();
		TimeSpan_t413522987  L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		bool L_7 = TimeSpan_Equals_m2969422609((TimeSpan_t413522987 *)L_3, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
extern "C"  bool Nullable_1_Equals_m3609411697_AdjustorThunk (Il2CppObject * __this, Nullable_1_t497649510  ___other0, const MethodInfo* method)
{
	Nullable_1_t497649510  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<TimeSpan_t413522987 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m3609411697(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<TimeSpan_t413522987 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Int32 System.Nullable`1<System.TimeSpan>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m2957066482_gshared (Nullable_1_t497649510 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		TimeSpan_t413522987 * L_1 = (TimeSpan_t413522987 *)__this->get_address_of_value_0();
		int32_t L_2 = TimeSpan_GetHashCode_m3188156777((TimeSpan_t413522987 *)L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
extern "C"  int32_t Nullable_1_GetHashCode_m2957066482_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t497649510  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<TimeSpan_t413522987 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_GetHashCode_m2957066482(&_thisAdjusted, method);
	*reinterpret_cast<TimeSpan_t413522987 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.TimeSpan>::GetValueOrDefault()
extern Il2CppClass* TimeSpan_t413522987_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_GetValueOrDefault_m2343728705_MetadataUsageId;
extern "C"  TimeSpan_t413522987  Nullable_1_GetValueOrDefault_m2343728705_gshared (Nullable_1_t497649510 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_GetValueOrDefault_m2343728705_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	TimeSpan_t413522987  V_0;
	memset(&V_0, 0, sizeof(V_0));
	TimeSpan_t413522987  G_B3_0;
	memset(&G_B3_0, 0, sizeof(G_B3_0));
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		TimeSpan_t413522987  L_1 = (TimeSpan_t413522987 )__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_001f;
	}

IL_0016:
	{
		Initobj (TimeSpan_t413522987_il2cpp_TypeInfo_var, (&V_0));
		TimeSpan_t413522987  L_2 = V_0;
		G_B3_0 = L_2;
	}

IL_001f:
	{
		return G_B3_0;
	}
}
extern "C"  TimeSpan_t413522987  Nullable_1_GetValueOrDefault_m2343728705_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t497649510  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<TimeSpan_t413522987 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	TimeSpan_t413522987  _returnValue = Nullable_1_GetValueOrDefault_m2343728705(&_thisAdjusted, method);
	*reinterpret_cast<TimeSpan_t413522987 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.TimeSpan>::GetValueOrDefault(T)
extern "C"  TimeSpan_t413522987  Nullable_1_GetValueOrDefault_m3936114493_gshared (Nullable_1_t497649510 * __this, TimeSpan_t413522987  ___defaultValue0, const MethodInfo* method)
{
	TimeSpan_t413522987  G_B3_0;
	memset(&G_B3_0, 0, sizeof(G_B3_0));
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		TimeSpan_t413522987  L_1 = (TimeSpan_t413522987 )__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_0017;
	}

IL_0016:
	{
		TimeSpan_t413522987  L_2 = ___defaultValue0;
		G_B3_0 = L_2;
	}

IL_0017:
	{
		return G_B3_0;
	}
}
extern "C"  TimeSpan_t413522987  Nullable_1_GetValueOrDefault_m3936114493_AdjustorThunk (Il2CppObject * __this, TimeSpan_t413522987  ___defaultValue0, const MethodInfo* method)
{
	Nullable_1_t497649510  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<TimeSpan_t413522987 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	TimeSpan_t413522987  _returnValue = Nullable_1_GetValueOrDefault_m3936114493(&_thisAdjusted, ___defaultValue0, method);
	*reinterpret_cast<TimeSpan_t413522987 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.String System.Nullable`1<System.TimeSpan>::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_ToString_m3059865940_MetadataUsageId;
extern "C"  String_t* Nullable_1_ToString_m3059865940_gshared (Nullable_1_t497649510 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_ToString_m3059865940_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		TimeSpan_t413522987 * L_1 = (TimeSpan_t413522987 *)__this->get_address_of_value_0();
		String_t* L_2 = TimeSpan_ToString_m2803989647((TimeSpan_t413522987 *)L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_3;
	}
}
extern "C"  String_t* Nullable_1_ToString_m3059865940_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t497649510  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<TimeSpan_t413522987 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	String_t* _returnValue = Nullable_1_ToString_m3059865940(&_thisAdjusted, method);
	*reinterpret_cast<TimeSpan_t413522987 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Void System.Nullable`1<System.UInt16>::.ctor(T)
extern "C"  void Nullable_1__ctor_m2457572193_gshared (Nullable_1_t108794446 * __this, uint16_t ___value0, const MethodInfo* method)
{
	{
		__this->set_has_value_1((bool)1);
		uint16_t L_0 = ___value0;
		__this->set_value_0(L_0);
		return;
	}
}
extern "C"  void Nullable_1__ctor_m2457572193_AdjustorThunk (Il2CppObject * __this, uint16_t ___value0, const MethodInfo* method)
{
	Nullable_1_t108794446  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<uint16_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	Nullable_1__ctor_m2457572193(&_thisAdjusted, ___value0, method);
	*reinterpret_cast<uint16_t*>(__this + 1) = _thisAdjusted.get_value_0();
}
// System.Boolean System.Nullable`1<System.UInt16>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m775859528_gshared (Nullable_1_t108794446 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
extern "C"  bool Nullable_1_get_HasValue_m775859528_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t108794446  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<uint16_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_get_HasValue_m775859528(&_thisAdjusted, method);
	*reinterpret_cast<uint16_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.UInt16>::get_Value()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral78850559;
extern const uint32_t Nullable_1_get_Value_m3903505384_MetadataUsageId;
extern "C"  uint16_t Nullable_1_get_Value_m3903505384_gshared (Nullable_1_t108794446 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_get_Value_m3903505384_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral78850559, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		uint16_t L_2 = (uint16_t)__this->get_value_0();
		return L_2;
	}
}
extern "C"  uint16_t Nullable_1_get_Value_m3903505384_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t108794446  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<uint16_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	uint16_t _returnValue = Nullable_1_get_Value_m3903505384(&_thisAdjusted, method);
	*reinterpret_cast<uint16_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<System.UInt16>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m3946947190_gshared (Nullable_1_t108794446 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = (bool)__this->get_has_value_1();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0010:
	{
		Il2CppObject * L_2 = ___other0;
		if (((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))
		{
			goto IL_001d;
		}
	}
	{
		return (bool)0;
	}

IL_001d:
	{
		Il2CppObject * L_3 = ___other0;
		void* L_4 = alloca(sizeof(Nullable_1_t108794446 ));
		UnBoxNullable(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), L_4);
		bool L_5 = ((  bool (*) (Il2CppObject *, Nullable_1_t108794446 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(il2cpp_codegen_fake_box((Nullable_1_t108794446 *)__this), (Nullable_1_t108794446 )((*(Nullable_1_t108794446 *)((Nullable_1_t108794446 *)L_4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_5;
	}
}
extern "C"  bool Nullable_1_Equals_m3946947190_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Nullable_1_t108794446  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<uint16_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m3946947190(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<uint16_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<System.UInt16>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m3444596745_gshared (Nullable_1_t108794446 * __this, Nullable_1_t108794446  ___other0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(&___other0)->get_has_value_1();
		bool L_1 = (bool)__this->get_has_value_1();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_has_value_1();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		return (bool)1;
	}

IL_0021:
	{
		uint16_t* L_3 = (uint16_t*)(&___other0)->get_address_of_value_0();
		uint16_t L_4 = (uint16_t)__this->get_value_0();
		uint16_t L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		bool L_7 = UInt16_Equals_m857648105((uint16_t*)L_3, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
extern "C"  bool Nullable_1_Equals_m3444596745_AdjustorThunk (Il2CppObject * __this, Nullable_1_t108794446  ___other0, const MethodInfo* method)
{
	Nullable_1_t108794446  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<uint16_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m3444596745(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<uint16_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Int32 System.Nullable`1<System.UInt16>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m1873950170_gshared (Nullable_1_t108794446 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		uint16_t* L_1 = (uint16_t*)__this->get_address_of_value_0();
		int32_t L_2 = UInt16_GetHashCode_m592888001((uint16_t*)L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
extern "C"  int32_t Nullable_1_GetHashCode_m1873950170_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t108794446  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<uint16_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_GetHashCode_m1873950170(&_thisAdjusted, method);
	*reinterpret_cast<uint16_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.UInt16>::GetValueOrDefault()
extern Il2CppClass* UInt16_t24667923_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_GetValueOrDefault_m164067433_MetadataUsageId;
extern "C"  uint16_t Nullable_1_GetValueOrDefault_m164067433_gshared (Nullable_1_t108794446 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_GetValueOrDefault_m164067433_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint16_t V_0 = 0;
	uint16_t G_B3_0 = 0;
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		uint16_t L_1 = (uint16_t)__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_001f;
	}

IL_0016:
	{
		Initobj (UInt16_t24667923_il2cpp_TypeInfo_var, (&V_0));
		uint16_t L_2 = V_0;
		G_B3_0 = L_2;
	}

IL_001f:
	{
		return G_B3_0;
	}
}
extern "C"  uint16_t Nullable_1_GetValueOrDefault_m164067433_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t108794446  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<uint16_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	uint16_t _returnValue = Nullable_1_GetValueOrDefault_m164067433(&_thisAdjusted, method);
	*reinterpret_cast<uint16_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.UInt16>::GetValueOrDefault(T)
extern "C"  uint16_t Nullable_1_GetValueOrDefault_m791124501_gshared (Nullable_1_t108794446 * __this, uint16_t ___defaultValue0, const MethodInfo* method)
{
	uint16_t G_B3_0 = 0;
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		uint16_t L_1 = (uint16_t)__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_0017;
	}

IL_0016:
	{
		uint16_t L_2 = ___defaultValue0;
		G_B3_0 = L_2;
	}

IL_0017:
	{
		return G_B3_0;
	}
}
extern "C"  uint16_t Nullable_1_GetValueOrDefault_m791124501_AdjustorThunk (Il2CppObject * __this, uint16_t ___defaultValue0, const MethodInfo* method)
{
	Nullable_1_t108794446  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<uint16_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	uint16_t _returnValue = Nullable_1_GetValueOrDefault_m791124501(&_thisAdjusted, ___defaultValue0, method);
	*reinterpret_cast<uint16_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.String System.Nullable`1<System.UInt16>::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_ToString_m2325119788_MetadataUsageId;
extern "C"  String_t* Nullable_1_ToString_m2325119788_gshared (Nullable_1_t108794446 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_ToString_m2325119788_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		uint16_t* L_1 = (uint16_t*)__this->get_address_of_value_0();
		String_t* L_2 = UInt16_ToString_m741885559((uint16_t*)L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_3;
	}
}
extern "C"  String_t* Nullable_1_ToString_m2325119788_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t108794446  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<uint16_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	String_t* _returnValue = Nullable_1_ToString_m2325119788(&_thisAdjusted, method);
	*reinterpret_cast<uint16_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Void System.Nullable`1<System.UInt32>::.ctor(T)
extern "C"  void Nullable_1__ctor_m3881182141_gshared (Nullable_1_t108794504 * __this, uint32_t ___value0, const MethodInfo* method)
{
	{
		__this->set_has_value_1((bool)1);
		uint32_t L_0 = ___value0;
		__this->set_value_0(L_0);
		return;
	}
}
extern "C"  void Nullable_1__ctor_m3881182141_AdjustorThunk (Il2CppObject * __this, uint32_t ___value0, const MethodInfo* method)
{
	Nullable_1_t108794504  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<uint32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	Nullable_1__ctor_m3881182141(&_thisAdjusted, ___value0, method);
	*reinterpret_cast<uint32_t*>(__this + 1) = _thisAdjusted.get_value_0();
}
// System.Boolean System.Nullable`1<System.UInt32>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m3075205198_gshared (Nullable_1_t108794504 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
extern "C"  bool Nullable_1_get_HasValue_m3075205198_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t108794504  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<uint32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_get_HasValue_m3075205198(&_thisAdjusted, method);
	*reinterpret_cast<uint32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.UInt32>::get_Value()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral78850559;
extern const uint32_t Nullable_1_get_Value_m1147197090_MetadataUsageId;
extern "C"  uint32_t Nullable_1_get_Value_m1147197090_gshared (Nullable_1_t108794504 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_get_Value_m1147197090_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral78850559, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		uint32_t L_2 = (uint32_t)__this->get_value_0();
		return L_2;
	}
}
extern "C"  uint32_t Nullable_1_get_Value_m1147197090_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t108794504  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<uint32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	uint32_t _returnValue = Nullable_1_get_Value_m1147197090(&_thisAdjusted, method);
	*reinterpret_cast<uint32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<System.UInt32>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m3727889584_gshared (Nullable_1_t108794504 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = (bool)__this->get_has_value_1();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0010:
	{
		Il2CppObject * L_2 = ___other0;
		if (((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))
		{
			goto IL_001d;
		}
	}
	{
		return (bool)0;
	}

IL_001d:
	{
		Il2CppObject * L_3 = ___other0;
		void* L_4 = alloca(sizeof(Nullable_1_t108794504 ));
		UnBoxNullable(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), L_4);
		bool L_5 = ((  bool (*) (Il2CppObject *, Nullable_1_t108794504 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(il2cpp_codegen_fake_box((Nullable_1_t108794504 *)__this), (Nullable_1_t108794504 )((*(Nullable_1_t108794504 *)((Nullable_1_t108794504 *)L_4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_5;
	}
}
extern "C"  bool Nullable_1_Equals_m3727889584_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Nullable_1_t108794504  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<uint32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m3727889584(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<uint32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<System.UInt32>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m2822690959_gshared (Nullable_1_t108794504 * __this, Nullable_1_t108794504  ___other0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(&___other0)->get_has_value_1();
		bool L_1 = (bool)__this->get_has_value_1();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_has_value_1();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		return (bool)1;
	}

IL_0021:
	{
		uint32_t* L_3 = (uint32_t*)(&___other0)->get_address_of_value_0();
		uint32_t L_4 = (uint32_t)__this->get_value_0();
		uint32_t L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		bool L_7 = UInt32_Equals_m1266223727((uint32_t*)L_3, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
extern "C"  bool Nullable_1_Equals_m2822690959_AdjustorThunk (Il2CppObject * __this, Nullable_1_t108794504  ___other0, const MethodInfo* method)
{
	Nullable_1_t108794504  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<uint32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m2822690959(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<uint32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Int32 System.Nullable`1<System.UInt32>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m3056501268_gshared (Nullable_1_t108794504 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		uint32_t* L_1 = (uint32_t*)__this->get_address_of_value_0();
		int32_t L_2 = UInt32_GetHashCode_m1046676807((uint32_t*)L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
extern "C"  int32_t Nullable_1_GetHashCode_m3056501268_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t108794504  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<uint32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_GetHashCode_m3056501268(&_thisAdjusted, method);
	*reinterpret_cast<uint32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.UInt32>::GetValueOrDefault()
extern Il2CppClass* UInt32_t24667981_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_GetValueOrDefault_m3640930595_MetadataUsageId;
extern "C"  uint32_t Nullable_1_GetValueOrDefault_m3640930595_gshared (Nullable_1_t108794504 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_GetValueOrDefault_m3640930595_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	uint32_t G_B3_0 = 0;
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		uint32_t L_1 = (uint32_t)__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_001f;
	}

IL_0016:
	{
		Initobj (UInt32_t24667981_il2cpp_TypeInfo_var, (&V_0));
		uint32_t L_2 = V_0;
		G_B3_0 = L_2;
	}

IL_001f:
	{
		return G_B3_0;
	}
}
extern "C"  uint32_t Nullable_1_GetValueOrDefault_m3640930595_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t108794504  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<uint32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	uint32_t _returnValue = Nullable_1_GetValueOrDefault_m3640930595(&_thisAdjusted, method);
	*reinterpret_cast<uint32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.UInt32>::GetValueOrDefault(T)
extern "C"  uint32_t Nullable_1_GetValueOrDefault_m1199700123_gshared (Nullable_1_t108794504 * __this, uint32_t ___defaultValue0, const MethodInfo* method)
{
	uint32_t G_B3_0 = 0;
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		uint32_t L_1 = (uint32_t)__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_0017;
	}

IL_0016:
	{
		uint32_t L_2 = ___defaultValue0;
		G_B3_0 = L_2;
	}

IL_0017:
	{
		return G_B3_0;
	}
}
extern "C"  uint32_t Nullable_1_GetValueOrDefault_m1199700123_AdjustorThunk (Il2CppObject * __this, uint32_t ___defaultValue0, const MethodInfo* method)
{
	Nullable_1_t108794504  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<uint32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	uint32_t _returnValue = Nullable_1_GetValueOrDefault_m1199700123(&_thisAdjusted, ___defaultValue0, method);
	*reinterpret_cast<uint32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.String System.Nullable`1<System.UInt32>::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_ToString_m3067490610_MetadataUsageId;
extern "C"  String_t* Nullable_1_ToString_m3067490610_gshared (Nullable_1_t108794504 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_ToString_m3067490610_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		uint32_t* L_1 = (uint32_t*)__this->get_address_of_value_0();
		String_t* L_2 = UInt32_ToString_m904380337((uint32_t*)L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_3;
	}
}
extern "C"  String_t* Nullable_1_ToString_m3067490610_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t108794504  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<uint32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	String_t* _returnValue = Nullable_1_ToString_m3067490610(&_thisAdjusted, method);
	*reinterpret_cast<uint32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Void System.Nullable`1<System.UInt64>::.ctor(T)
extern "C"  void Nullable_1__ctor_m3259138558_gshared (Nullable_1_t108794599 * __this, uint64_t ___value0, const MethodInfo* method)
{
	{
		__this->set_has_value_1((bool)1);
		uint64_t L_0 = ___value0;
		__this->set_value_0(L_0);
		return;
	}
}
extern "C"  void Nullable_1__ctor_m3259138558_AdjustorThunk (Il2CppObject * __this, uint64_t ___value0, const MethodInfo* method)
{
	Nullable_1_t108794599  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<uint64_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	Nullable_1__ctor_m3259138558(&_thisAdjusted, ___value0, method);
	*reinterpret_cast<uint64_t*>(__this + 1) = _thisAdjusted.get_value_0();
}
// System.Boolean System.Nullable`1<System.UInt64>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m2842612175_gshared (Nullable_1_t108794599 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
extern "C"  bool Nullable_1_get_HasValue_m2842612175_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t108794599  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<uint64_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_get_HasValue_m2842612175(&_thisAdjusted, method);
	*reinterpret_cast<uint64_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.UInt64>::get_Value()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral78850559;
extern const uint32_t Nullable_1_get_Value_m2630698177_MetadataUsageId;
extern "C"  uint64_t Nullable_1_get_Value_m2630698177_gshared (Nullable_1_t108794599 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_get_Value_m2630698177_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral78850559, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		uint64_t L_2 = (uint64_t)__this->get_value_0();
		return L_2;
	}
}
extern "C"  uint64_t Nullable_1_get_Value_m2630698177_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t108794599  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<uint64_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	uint64_t _returnValue = Nullable_1_get_Value_m2630698177(&_thisAdjusted, method);
	*reinterpret_cast<uint64_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<System.UInt64>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m3961497615_gshared (Nullable_1_t108794599 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = (bool)__this->get_has_value_1();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0010:
	{
		Il2CppObject * L_2 = ___other0;
		if (((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))
		{
			goto IL_001d;
		}
	}
	{
		return (bool)0;
	}

IL_001d:
	{
		Il2CppObject * L_3 = ___other0;
		void* L_4 = alloca(sizeof(Nullable_1_t108794599 ));
		UnBoxNullable(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), L_4);
		bool L_5 = ((  bool (*) (Il2CppObject *, Nullable_1_t108794599 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(il2cpp_codegen_fake_box((Nullable_1_t108794599 *)__this), (Nullable_1_t108794599 )((*(Nullable_1_t108794599 *)((Nullable_1_t108794599 *)L_4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_5;
	}
}
extern "C"  bool Nullable_1_Equals_m3961497615_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Nullable_1_t108794599  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<uint64_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m3961497615(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<uint64_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<System.UInt64>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m841387088_gshared (Nullable_1_t108794599 * __this, Nullable_1_t108794599  ___other0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(&___other0)->get_has_value_1();
		bool L_1 = (bool)__this->get_has_value_1();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_has_value_1();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		return (bool)1;
	}

IL_0021:
	{
		uint64_t* L_3 = (uint64_t*)(&___other0)->get_address_of_value_0();
		uint64_t L_4 = (uint64_t)__this->get_value_0();
		uint64_t L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		bool L_7 = UInt64_Equals_m3490516784((uint64_t*)L_3, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
extern "C"  bool Nullable_1_Equals_m841387088_AdjustorThunk (Il2CppObject * __this, Nullable_1_t108794599  ___other0, const MethodInfo* method)
{
	Nullable_1_t108794599  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<uint64_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m841387088(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<uint64_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Int32 System.Nullable`1<System.UInt64>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m2771903603_gshared (Nullable_1_t108794599 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		uint64_t* L_1 = (uint64_t*)__this->get_address_of_value_0();
		int32_t L_2 = UInt64_GetHashCode_m4085537544((uint64_t*)L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
extern "C"  int32_t Nullable_1_GetHashCode_m2771903603_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t108794599  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<uint64_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_GetHashCode_m2771903603(&_thisAdjusted, method);
	*reinterpret_cast<uint64_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.UInt64>::GetValueOrDefault()
extern Il2CppClass* UInt64_t24668076_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_GetValueOrDefault_m1634472002_MetadataUsageId;
extern "C"  uint64_t Nullable_1_GetValueOrDefault_m1634472002_gshared (Nullable_1_t108794599 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_GetValueOrDefault_m1634472002_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint64_t V_0 = 0;
	uint64_t G_B3_0 = 0;
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		uint64_t L_1 = (uint64_t)__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_001f;
	}

IL_0016:
	{
		Initobj (UInt64_t24668076_il2cpp_TypeInfo_var, (&V_0));
		uint64_t L_2 = V_0;
		G_B3_0 = L_2;
	}

IL_001f:
	{
		return G_B3_0;
	}
}
extern "C"  uint64_t Nullable_1_GetValueOrDefault_m1634472002_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t108794599  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<uint64_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	uint64_t _returnValue = Nullable_1_GetValueOrDefault_m1634472002(&_thisAdjusted, method);
	*reinterpret_cast<uint64_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.UInt64>::GetValueOrDefault(T)
extern "C"  uint64_t Nullable_1_GetValueOrDefault_m3423993180_gshared (Nullable_1_t108794599 * __this, uint64_t ___defaultValue0, const MethodInfo* method)
{
	uint64_t G_B3_0 = 0;
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		uint64_t L_1 = (uint64_t)__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_0017;
	}

IL_0016:
	{
		uint64_t L_2 = ___defaultValue0;
		G_B3_0 = L_2;
	}

IL_0017:
	{
		return G_B3_0;
	}
}
extern "C"  uint64_t Nullable_1_GetValueOrDefault_m3423993180_AdjustorThunk (Il2CppObject * __this, uint64_t ___defaultValue0, const MethodInfo* method)
{
	Nullable_1_t108794599  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<uint64_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	uint64_t _returnValue = Nullable_1_GetValueOrDefault_m3423993180(&_thisAdjusted, ___defaultValue0, method);
	*reinterpret_cast<uint64_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.String System.Nullable`1<System.UInt64>::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_ToString_m2284061491_MetadataUsageId;
extern "C"  String_t* Nullable_1_ToString_m2284061491_gshared (Nullable_1_t108794599 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_ToString_m2284061491_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		uint64_t* L_1 = (uint64_t*)__this->get_address_of_value_0();
		String_t* L_2 = UInt64_ToString_m3095865744((uint64_t*)L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_3;
	}
}
extern "C"  String_t* Nullable_1_ToString_m2284061491_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t108794599  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<uint64_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	String_t* _returnValue = Nullable_1_ToString_m2284061491(&_thisAdjusted, method);
	*reinterpret_cast<uint64_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Void System.Nullable`1<UnityEngine.Vector3>::.ctor(T)
extern "C"  void Nullable_1__ctor_m2083710364_gshared (Nullable_1_t71225793 * __this, Vector3_t4282066566  ___value0, const MethodInfo* method)
{
	{
		__this->set_has_value_1((bool)1);
		Vector3_t4282066566  L_0 = ___value0;
		__this->set_value_0(L_0);
		return;
	}
}
extern "C"  void Nullable_1__ctor_m2083710364_AdjustorThunk (Il2CppObject * __this, Vector3_t4282066566  ___value0, const MethodInfo* method)
{
	Nullable_1_t71225793  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<Vector3_t4282066566 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	Nullable_1__ctor_m2083710364(&_thisAdjusted, ___value0, method);
	*reinterpret_cast<Vector3_t4282066566 *>(__this + 1) = _thisAdjusted.get_value_0();
}
// System.Boolean System.Nullable`1<UnityEngine.Vector3>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m2494347949_gshared (Nullable_1_t71225793 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
extern "C"  bool Nullable_1_get_HasValue_m2494347949_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t71225793  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<Vector3_t4282066566 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_get_HasValue_m2494347949(&_thisAdjusted, method);
	*reinterpret_cast<Vector3_t4282066566 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<UnityEngine.Vector3>::get_Value()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral78850559;
extern const uint32_t Nullable_1_get_Value_m1975946275_MetadataUsageId;
extern "C"  Vector3_t4282066566  Nullable_1_get_Value_m1975946275_gshared (Nullable_1_t71225793 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_get_Value_m1975946275_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral78850559, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		Vector3_t4282066566  L_2 = (Vector3_t4282066566 )__this->get_value_0();
		return L_2;
	}
}
extern "C"  Vector3_t4282066566  Nullable_1_get_Value_m1975946275_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t71225793  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<Vector3_t4282066566 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	Vector3_t4282066566  _returnValue = Nullable_1_get_Value_m1975946275(&_thisAdjusted, method);
	*reinterpret_cast<Vector3_t4282066566 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<UnityEngine.Vector3>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m3083210417_gshared (Nullable_1_t71225793 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = (bool)__this->get_has_value_1();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0010:
	{
		Il2CppObject * L_2 = ___other0;
		if (((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))
		{
			goto IL_001d;
		}
	}
	{
		return (bool)0;
	}

IL_001d:
	{
		Il2CppObject * L_3 = ___other0;
		void* L_4 = alloca(sizeof(Nullable_1_t71225793 ));
		UnBoxNullable(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), L_4);
		bool L_5 = ((  bool (*) (Il2CppObject *, Nullable_1_t71225793 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(il2cpp_codegen_fake_box((Nullable_1_t71225793 *)__this), (Nullable_1_t71225793 )((*(Nullable_1_t71225793 *)((Nullable_1_t71225793 *)L_4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_5;
	}
}
extern "C"  bool Nullable_1_Equals_m3083210417_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Nullable_1_t71225793  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<Vector3_t4282066566 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m3083210417(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<Vector3_t4282066566 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<UnityEngine.Vector3>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m2485111662_gshared (Nullable_1_t71225793 * __this, Nullable_1_t71225793  ___other0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(&___other0)->get_has_value_1();
		bool L_1 = (bool)__this->get_has_value_1();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_has_value_1();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		return (bool)1;
	}

IL_0021:
	{
		Vector3_t4282066566 * L_3 = (Vector3_t4282066566 *)(&___other0)->get_address_of_value_0();
		Vector3_t4282066566  L_4 = (Vector3_t4282066566 )__this->get_value_0();
		Vector3_t4282066566  L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		bool L_7 = Vector3_Equals_m3337192096((Vector3_t4282066566 *)L_3, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
extern "C"  bool Nullable_1_Equals_m2485111662_AdjustorThunk (Il2CppObject * __this, Nullable_1_t71225793  ___other0, const MethodInfo* method)
{
	Nullable_1_t71225793  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<Vector3_t4282066566 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m2485111662(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<Vector3_t4282066566 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Int32 System.Nullable`1<UnityEngine.Vector3>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m3406313621_gshared (Nullable_1_t71225793 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		Vector3_t4282066566 * L_1 = (Vector3_t4282066566 *)__this->get_address_of_value_0();
		int32_t L_2 = Vector3_GetHashCode_m3912867704((Vector3_t4282066566 *)L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
extern "C"  int32_t Nullable_1_GetHashCode_m3406313621_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t71225793  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<Vector3_t4282066566 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_GetHashCode_m3406313621(&_thisAdjusted, method);
	*reinterpret_cast<Vector3_t4282066566 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<UnityEngine.Vector3>::GetValueOrDefault()
extern Il2CppClass* Vector3_t4282066566_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_GetValueOrDefault_m4181369124_MetadataUsageId;
extern "C"  Vector3_t4282066566  Nullable_1_GetValueOrDefault_m4181369124_gshared (Nullable_1_t71225793 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_GetValueOrDefault_m4181369124_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t4282066566  G_B3_0;
	memset(&G_B3_0, 0, sizeof(G_B3_0));
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Vector3_t4282066566  L_1 = (Vector3_t4282066566 )__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_001f;
	}

IL_0016:
	{
		Initobj (Vector3_t4282066566_il2cpp_TypeInfo_var, (&V_0));
		Vector3_t4282066566  L_2 = V_0;
		G_B3_0 = L_2;
	}

IL_001f:
	{
		return G_B3_0;
	}
}
extern "C"  Vector3_t4282066566  Nullable_1_GetValueOrDefault_m4181369124_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t71225793  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<Vector3_t4282066566 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	Vector3_t4282066566  _returnValue = Nullable_1_GetValueOrDefault_m4181369124(&_thisAdjusted, method);
	*reinterpret_cast<Vector3_t4282066566 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<UnityEngine.Vector3>::GetValueOrDefault(T)
extern "C"  Vector3_t4282066566  Nullable_1_GetValueOrDefault_m773425338_gshared (Nullable_1_t71225793 * __this, Vector3_t4282066566  ___defaultValue0, const MethodInfo* method)
{
	Vector3_t4282066566  G_B3_0;
	memset(&G_B3_0, 0, sizeof(G_B3_0));
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Vector3_t4282066566  L_1 = (Vector3_t4282066566 )__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_0017;
	}

IL_0016:
	{
		Vector3_t4282066566  L_2 = ___defaultValue0;
		G_B3_0 = L_2;
	}

IL_0017:
	{
		return G_B3_0;
	}
}
extern "C"  Vector3_t4282066566  Nullable_1_GetValueOrDefault_m773425338_AdjustorThunk (Il2CppObject * __this, Vector3_t4282066566  ___defaultValue0, const MethodInfo* method)
{
	Nullable_1_t71225793  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<Vector3_t4282066566 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	Vector3_t4282066566  _returnValue = Nullable_1_GetValueOrDefault_m773425338(&_thisAdjusted, ___defaultValue0, method);
	*reinterpret_cast<Vector3_t4282066566 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.String System.Nullable`1<UnityEngine.Vector3>::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_ToString_m3285229841_MetadataUsageId;
extern "C"  String_t* Nullable_1_ToString_m3285229841_gshared (Nullable_1_t71225793 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_ToString_m3285229841_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		Vector3_t4282066566 * L_1 = (Vector3_t4282066566 *)__this->get_address_of_value_0();
		String_t* L_2 = Vector3_ToString_m3566373060((Vector3_t4282066566 *)L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_3;
	}
}
extern "C"  String_t* Nullable_1_ToString_m3285229841_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t71225793  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<Vector3_t4282066566 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	String_t* _returnValue = Nullable_1_ToString_m3285229841(&_thisAdjusted, method);
	*reinterpret_cast<Vector3_t4282066566 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Void System.Predicate`1<Core.RpsChoice>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m2704870132_gshared (Predicate_1_t3958821311 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<Core.RpsChoice>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m3704204046_gshared (Predicate_1_t3958821311 * __this, int32_t ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m3704204046((Predicate_1_t3958821311 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, int32_t ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<Core.RpsChoice>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* RpsChoice_t52797132_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m2932265373_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m2932265373_gshared (Predicate_1_t3958821311 * __this, int32_t ___obj0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m2932265373_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(RpsChoice_t52797132_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<Core.RpsChoice>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m413379974_gshared (Predicate_1_t3958821311 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<Core.RpsResult>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m111590904_gshared (Predicate_1_t90651707 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<Core.RpsResult>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m375536522_gshared (Predicate_1_t90651707 * __this, int32_t ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m375536522((Predicate_1_t90651707 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, int32_t ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<Core.RpsResult>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* RpsResult_t479594824_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m2787391001_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m2787391001_gshared (Predicate_1_t90651707 * __this, int32_t ___obj0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m2787391001_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(RpsResult_t479594824_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<Core.RpsResult>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m2949350026_gshared (Predicate_1_t90651707 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<Entity.Behavior.EBehaviorID>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m3153311678_gshared (Predicate_1_t849590019 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<Entity.Behavior.EBehaviorID>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m1158830920_gshared (Predicate_1_t849590019 * __this, uint8_t ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m1158830920((Predicate_1_t849590019 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, uint8_t ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, uint8_t ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<Entity.Behavior.EBehaviorID>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* EBehaviorID_t1238533136_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m3835250971_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m3835250971_gshared (Predicate_1_t849590019 * __this, uint8_t ___obj0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m3835250971_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(EBehaviorID_t1238533136_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<Entity.Behavior.EBehaviorID>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m2549433740_gshared (Predicate_1_t849590019 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<HatredCtrl/stValue>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m1751088633_gshared (Predicate_1_t3037002293 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<HatredCtrl/stValue>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m845256041_gshared (Predicate_1_t3037002293 * __this, stValue_t3425945410  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m845256041((Predicate_1_t3037002293 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, stValue_t3425945410  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, stValue_t3425945410  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<HatredCtrl/stValue>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* stValue_t3425945410_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m337311224_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m337311224_gshared (Predicate_1_t3037002293 * __this, stValue_t3425945410  ___obj0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m337311224_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(stValue_t3425945410_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<HatredCtrl/stValue>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m93018635_gshared (Predicate_1_t3037002293 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m1979413281_gshared (Predicate_1_t2245038093 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m2883462977_gshared (Predicate_1_t2245038093 * __this, ErrorInfo_t2633981210  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m2883462977((Predicate_1_t2245038093 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, ErrorInfo_t2633981210  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, ErrorInfo_t2633981210  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* ErrorInfo_t2633981210_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m2563609552_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m2563609552_gshared (Predicate_1_t2245038093 * __this, ErrorInfo_t2633981210  ___obj0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m2563609552_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(ErrorInfo_t2633981210_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m3508068147_gshared (Predicate_1_t2245038093 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<Newtonsoft.Json.Linq.JTokenType>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m3229495261_gshared (Predicate_1_t3527954444 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<Newtonsoft.Json.Linq.JTokenType>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m1463125321_gshared (Predicate_1_t3527954444 * __this, int32_t ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m1463125321((Predicate_1_t3527954444 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, int32_t ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<Newtonsoft.Json.Linq.JTokenType>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* JTokenType_t3916897561_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m612060444_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m612060444_gshared (Predicate_1_t3527954444 * __this, int32_t ___obj0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m612060444_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(JTokenType_t3916897561_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<Newtonsoft.Json.Linq.JTokenType>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m3235255851_gshared (Predicate_1_t3527954444 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<Newtonsoft.Json.Schema.JsonSchemaType>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m326331652_gshared (Predicate_1_t1726472704 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<Newtonsoft.Json.Schema.JsonSchemaType>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m1424652482_gshared (Predicate_1_t1726472704 * __this, int32_t ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m1424652482((Predicate_1_t1726472704 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, int32_t ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<Newtonsoft.Json.Schema.JsonSchemaType>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* JsonSchemaType_t2115415821_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m1830390293_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m1830390293_gshared (Predicate_1_t1726472704 * __this, int32_t ___obj0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m1830390293_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(JsonSchemaType_t2115415821_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<Newtonsoft.Json.Schema.JsonSchemaType>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m2700884050_gshared (Predicate_1_t1726472704 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<Pathfinding.AdvancedSmooth/Turn>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m3385244813_gshared (Predicate_1_t76252261 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<Pathfinding.AdvancedSmooth/Turn>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m4233073305_gshared (Predicate_1_t76252261 * __this, Turn_t465195378  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m4233073305((Predicate_1_t76252261 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, Turn_t465195378  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, Turn_t465195378  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<Pathfinding.AdvancedSmooth/Turn>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Turn_t465195378_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m1003604588_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m1003604588_gshared (Predicate_1_t76252261 * __this, Turn_t465195378  ___obj0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m1003604588_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Turn_t465195378_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<Pathfinding.AdvancedSmooth/Turn>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m71334619_gshared (Predicate_1_t76252261 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<Pathfinding.ClipperLib.IntPoint>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m575505298_gshared (Predicate_1_t2937183062 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<Pathfinding.ClipperLib.IntPoint>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m762640372_gshared (Predicate_1_t2937183062 * __this, IntPoint_t3326126179  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m762640372((Predicate_1_t2937183062 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, IntPoint_t3326126179  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, IntPoint_t3326126179  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<Pathfinding.ClipperLib.IntPoint>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* IntPoint_t3326126179_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m3520952519_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m3520952519_gshared (Predicate_1_t2937183062 * __this, IntPoint_t3326126179  ___obj0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m3520952519_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(IntPoint_t3326126179_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<Pathfinding.ClipperLib.IntPoint>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m6655072_gshared (Predicate_1_t2937183062 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<Pathfinding.Int2>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m707635534_gshared (Predicate_1_t1585102476 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<Pathfinding.Int2>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m208016372_gshared (Predicate_1_t1585102476 * __this, Int2_t1974045593  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m208016372((Predicate_1_t1585102476 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, Int2_t1974045593  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, Int2_t1974045593  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<Pathfinding.Int2>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Int2_t1974045593_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m1742824707_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m1742824707_gshared (Predicate_1_t1585102476 * __this, Int2_t1974045593  ___obj0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m1742824707_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Int2_t1974045593_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<Pathfinding.Int2>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m847114336_gshared (Predicate_1_t1585102476 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<Pathfinding.Int3>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m2340439533_gshared (Predicate_1_t1585102477 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<Pathfinding.Int3>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m4209580661_gshared (Predicate_1_t1585102477 * __this, Int3_t1974045594  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m4209580661((Predicate_1_t1585102477 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, Int3_t1974045594  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, Int3_t1974045594  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<Pathfinding.Int3>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Int3_t1974045594_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m4112258692_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m4112258692_gshared (Predicate_1_t1585102477 * __this, Int3_t1974045594  ___obj0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m4112258692_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Int3_t1974045594_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<Pathfinding.Int3>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m1872606335_gshared (Predicate_1_t1585102477 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<Pathfinding.IntRect>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m2182516054_gshared (Predicate_1_t2626115144 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<Pathfinding.IntRect>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m1388956848_gshared (Predicate_1_t2626115144 * __this, IntRect_t3015058261  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m1388956848((Predicate_1_t2626115144 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, IntRect_t3015058261  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, IntRect_t3015058261  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<Pathfinding.IntRect>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* IntRect_t3015058261_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m4033696899_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m4033696899_gshared (Predicate_1_t2626115144 * __this, IntRect_t3015058261  ___obj0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m4033696899_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(IntRect_t3015058261_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<Pathfinding.IntRect>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m3226617124_gshared (Predicate_1_t2626115144 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<Pathfinding.LocalAvoidance/IntersectionPair>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m425519152_gshared (Predicate_1_t2432376802 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<Pathfinding.LocalAvoidance/IntersectionPair>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m2988219030_gshared (Predicate_1_t2432376802 * __this, IntersectionPair_t2821319919  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m2988219030((Predicate_1_t2432376802 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, IntersectionPair_t2821319919  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, IntersectionPair_t2821319919  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<Pathfinding.LocalAvoidance/IntersectionPair>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* IntersectionPair_t2821319919_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m4071317097_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m4071317097_gshared (Predicate_1_t2432376802 * __this, IntersectionPair_t2821319919  ___obj0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m4071317097_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(IntersectionPair_t2821319919_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<Pathfinding.LocalAvoidance/IntersectionPair>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m2060927742_gshared (Predicate_1_t2432376802 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<Pathfinding.LocalAvoidance/VOLine>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m142599366_gshared (Predicate_1_t1640988684 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<Pathfinding.LocalAvoidance/VOLine>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m867756992_gshared (Predicate_1_t1640988684 * __this, VOLine_t2029931801  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m867756992((Predicate_1_t1640988684 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, VOLine_t2029931801  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, VOLine_t2029931801  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<Pathfinding.LocalAvoidance/VOLine>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* VOLine_t2029931801_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m4033296403_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m4033296403_gshared (Predicate_1_t1640988684 * __this, VOLine_t2029931801  ___obj0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m4033296403_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(VOLine_t2029931801_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<Pathfinding.LocalAvoidance/VOLine>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m3031969812_gshared (Predicate_1_t1640988684 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<Pathfinding.Voxels.ExtraMesh>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m422619221_gshared (Predicate_1_t3829086598 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<Pathfinding.Voxels.ExtraMesh>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m3095520781_gshared (Predicate_1_t3829086598 * __this, ExtraMesh_t4218029715  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m3095520781((Predicate_1_t3829086598 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, ExtraMesh_t4218029715  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, ExtraMesh_t4218029715  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<Pathfinding.Voxels.ExtraMesh>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* ExtraMesh_t4218029715_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m830078236_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m830078236_gshared (Predicate_1_t3829086598 * __this, ExtraMesh_t4218029715  ___obj0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m830078236_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(ExtraMesh_t4218029715_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<Pathfinding.Voxels.ExtraMesh>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m2338887655_gshared (Predicate_1_t3829086598 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<Pathfinding.Voxels.VoxelContour>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m2347830228_gshared (Predicate_1_t3208257899 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<Pathfinding.Voxels.VoxelContour>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m1349220722_gshared (Predicate_1_t3208257899 * __this, VoxelContour_t3597201016  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m1349220722((Predicate_1_t3208257899 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, VoxelContour_t3597201016  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, VoxelContour_t3597201016  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<Pathfinding.Voxels.VoxelContour>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* VoxelContour_t3597201016_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m861152325_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m861152325_gshared (Predicate_1_t3208257899 * __this, VoxelContour_t3597201016  ___obj0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m861152325_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(VoxelContour_t3597201016_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<Pathfinding.Voxels.VoxelContour>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m202081698_gshared (Predicate_1_t3208257899 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<PointCloudRegognizer/Point>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m3478604085_gshared (Predicate_1_t1449888633 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<PointCloudRegognizer/Point>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m1592909997_gshared (Predicate_1_t1449888633 * __this, Point_t1838831750  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m1592909997((Predicate_1_t1449888633 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, Point_t1838831750  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, Point_t1838831750  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<PointCloudRegognizer/Point>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Point_t1838831750_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m946605884_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m946605884_gshared (Predicate_1_t1449888633 * __this, Point_t1838831750  ___obj0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m946605884_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Point_t1838831750_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<PointCloudRegognizer/Point>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m1009615175_gshared (Predicate_1_t1449888633 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<PushType>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m1295400039_gshared (Predicate_1_t1451699335 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<PushType>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m2886466747_gshared (Predicate_1_t1451699335 * __this, int32_t ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m2886466747((Predicate_1_t1451699335 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, int32_t ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<PushType>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* PushType_t1840642452_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m296868042_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m296868042_gshared (Predicate_1_t1451699335 * __this, int32_t ___obj0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m296868042_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(PushType_t1840642452_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<PushType>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m4279048697_gshared (Predicate_1_t1451699335 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<System.Boolean>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m3215793458_gshared (Predicate_1_t87855601 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<System.Boolean>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m791068432_gshared (Predicate_1_t87855601 * __this, bool ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m791068432((Predicate_1_t87855601 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, bool ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, bool ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<System.Boolean>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Boolean_t476798718_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m1019231135_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m1019231135_gshared (Predicate_1_t87855601 * __this, bool ___obj0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m1019231135_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Boolean_t476798718_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<System.Boolean>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m1376161476_gshared (Predicate_1_t87855601 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<System.Byte>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m401407672_gshared (Predicate_1_t2473666543 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<System.Byte>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m986317582_gshared (Predicate_1_t2473666543 * __this, uint8_t ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m986317582((Predicate_1_t2473666543 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, uint8_t ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, uint8_t ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<System.Byte>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Byte_t2862609660_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m211908321_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m211908321_gshared (Predicate_1_t2473666543 * __this, uint8_t ___obj0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m211908321_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Byte_t2862609660_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<System.Byte>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m1925862790_gshared (Predicate_1_t2473666543 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m252712190_gshared (Predicate_1_t1555725860 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m684566468_gshared (Predicate_1_t1555725860 * __this, KeyValuePair_2_t1944668977  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m684566468((Predicate_1_t1555725860 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, KeyValuePair_2_t1944668977  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, KeyValuePair_2_t1944668977  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* KeyValuePair_2_t1944668977_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m2552303187_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m2552303187_gshared (Predicate_1_t1555725860 * __this, KeyValuePair_2_t1944668977  ___obj0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m2552303187_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(KeyValuePair_2_t1944668977_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m3276267152_gshared (Predicate_1_t1555725860 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<System.Int32>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m659588556_gshared (Predicate_1_t764895383 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<System.Int32>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m4051395766_gshared (Predicate_1_t764895383 * __this, int32_t ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m4051395766((Predicate_1_t764895383 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, int32_t ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<System.Int32>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m1598036677_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m1598036677_gshared (Predicate_1_t764895383 * __this, int32_t ___obj0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m1598036677_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<System.Int32>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m2016546014_gshared (Predicate_1_t764895383 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m231416842_gshared (Predicate_1_t3781873254 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<System.Object>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m1328901537_gshared (Predicate_1_t3781873254 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m1328901537((Predicate_1_t3781873254 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (void* __this, Il2CppObject * ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<System.Object>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m2038073176_gshared (Predicate_1_t3781873254 * __this, Il2CppObject * ___obj0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___obj0;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m3970497007_gshared (Predicate_1_t3781873254 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<System.Reflection.CustomAttributeNamedArgument>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m653858156_gshared (Predicate_1_t2670669872 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<System.Reflection.CustomAttributeNamedArgument>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m1065554326_gshared (Predicate_1_t2670669872 * __this, CustomAttributeNamedArgument_t3059612989  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m1065554326((Predicate_1_t2670669872 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, CustomAttributeNamedArgument_t3059612989  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, CustomAttributeNamedArgument_t3059612989  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<System.Reflection.CustomAttributeNamedArgument>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* CustomAttributeNamedArgument_t3059612989_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m2619512869_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m2619512869_gshared (Predicate_1_t2670669872 * __this, CustomAttributeNamedArgument_t3059612989  ___obj0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m2619512869_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(CustomAttributeNamedArgument_t3059612989_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<System.Reflection.CustomAttributeNamedArgument>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m2465278974_gshared (Predicate_1_t2670669872 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<System.Reflection.CustomAttributeTypedArgument>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m1933135835_gshared (Predicate_1_t2912350305 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<System.Reflection.CustomAttributeTypedArgument>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m3613331527_gshared (Predicate_1_t2912350305 * __this, CustomAttributeTypedArgument_t3301293422  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m3613331527((Predicate_1_t2912350305 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, CustomAttributeTypedArgument_t3301293422  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, CustomAttributeTypedArgument_t3301293422  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<System.Reflection.CustomAttributeTypedArgument>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* CustomAttributeTypedArgument_t3301293422_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m577130966_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m577130966_gshared (Predicate_1_t2912350305 * __this, CustomAttributeTypedArgument_t3301293422  ___obj0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m577130966_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(CustomAttributeTypedArgument_t3301293422_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<System.Reflection.CustomAttributeTypedArgument>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m3012949485_gshared (Predicate_1_t2912350305 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<System.Single>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m1094778552_gshared (Predicate_1_t3902975855 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<System.Single>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m1825106574_gshared (Predicate_1_t3902975855 * __this, float ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m1825106574((Predicate_1_t3902975855 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, float ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, float ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<System.Single>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Single_t4291918972_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m2957256161_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m2957256161_gshared (Predicate_1_t3902975855 * __this, float ___obj0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m2957256161_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Single_t4291918972_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<System.Single>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m4191100422_gshared (Predicate_1_t3902975855 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<System.UInt32>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m1727410823_gshared (Predicate_1_t3930692160 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<System.UInt32>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m2723750879_gshared (Predicate_1_t3930692160 * __this, uint32_t ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m2723750879((Predicate_1_t3930692160 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, uint32_t ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, uint32_t ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<System.UInt32>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* UInt32_t24667981_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m1041562162_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m1041562162_gshared (Predicate_1_t3930692160 * __this, uint32_t ___obj0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m1041562162_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(UInt32_t24667981_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<System.UInt32>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m891064661_gshared (Predicate_1_t3930692160 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<UnityEngine.Bounds>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m461440207_gshared (Predicate_1_t2322698732 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<UnityEngine.Bounds>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m3340040659_gshared (Predicate_1_t2322698732 * __this, Bounds_t2711641849  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m3340040659((Predicate_1_t2322698732 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, Bounds_t2711641849  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, Bounds_t2711641849  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<UnityEngine.Bounds>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Bounds_t2711641849_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m2230069346_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m2230069346_gshared (Predicate_1_t2322698732 * __this, Bounds_t2711641849  ___obj0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m2230069346_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Bounds_t2711641849_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<UnityEngine.Bounds>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m1593687009_gshared (Predicate_1_t2322698732 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<UnityEngine.Color>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m322232627_gshared (Predicate_1_t3805603788 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<UnityEngine.Color>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m2401458867_gshared (Predicate_1_t3805603788 * __this, Color_t4194546905  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m2401458867((Predicate_1_t3805603788 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, Color_t4194546905  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, Color_t4194546905  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<UnityEngine.Color>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Color_t4194546905_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m3019095046_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m3019095046_gshared (Predicate_1_t3805603788 * __this, Color_t4194546905  ___obj0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m3019095046_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Color_t4194546905_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<UnityEngine.Color>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m2796463873_gshared (Predicate_1_t3805603788 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<UnityEngine.Color32>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m1072563380_gshared (Predicate_1_t209910571 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<UnityEngine.Color32>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m3562076050_gshared (Predicate_1_t209910571 * __this, Color32_t598853688  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m3562076050((Predicate_1_t209910571 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, Color32_t598853688  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, Color32_t598853688  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<UnityEngine.Color32>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Color32_t598853688_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m2338878821_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m2338878821_gshared (Predicate_1_t209910571 * __this, Color32_t598853688  ___obj0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m2338878821_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Color32_t598853688_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<UnityEngine.Color32>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m4225244034_gshared (Predicate_1_t209910571 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<UnityEngine.EventSystems.RaycastResult>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m3721624866_gshared (Predicate_1_t3373718247 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<UnityEngine.EventSystems.RaycastResult>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m3513546528_gshared (Predicate_1_t3373718247 * __this, RaycastResult_t3762661364  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m3513546528((Predicate_1_t3373718247 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, RaycastResult_t3762661364  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, RaycastResult_t3762661364  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<UnityEngine.EventSystems.RaycastResult>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* RaycastResult_t3762661364_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m2814560687_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m2814560687_gshared (Predicate_1_t3373718247 * __this, RaycastResult_t3762661364  ___obj0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m2814560687_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(RaycastResult_t3762661364_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<UnityEngine.EventSystems.RaycastResult>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m3277815988_gshared (Predicate_1_t3373718247 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m3018602081_gshared (Predicate_1_t3733023121 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m1219278277_gshared (Predicate_1_t3733023121 * __this, MeshInstance_t4121966238  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m1219278277((Predicate_1_t3733023121 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, MeshInstance_t4121966238  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, MeshInstance_t4121966238  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* MeshInstance_t4121966238_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m2865927448_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m2865927448_gshared (Predicate_1_t3733023121 * __this, MeshInstance_t4121966238  ___obj0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m2865927448_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(MeshInstance_t4121966238_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m1803766575_gshared (Predicate_1_t3733023121 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m1674687157_gshared (Predicate_1_t1985896569 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m839194029_gshared (Predicate_1_t1985896569 * __this, SubMeshInstance_t2374839686  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m839194029((Predicate_1_t1985896569 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, SubMeshInstance_t2374839686  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, SubMeshInstance_t2374839686  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* SubMeshInstance_t2374839686_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m4212437692_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m4212437692_gshared (Predicate_1_t1985896569 * __this, SubMeshInstance_t2374839686  ___obj0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m4212437692_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(SubMeshInstance_t2374839686_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m3533532231_gshared (Predicate_1_t1985896569 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<UnityEngine.RaycastHit>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m1467106490_gshared (Predicate_1_t3614232609 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<UnityEngine.RaycastHit>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m1917472904_gshared (Predicate_1_t3614232609 * __this, RaycastHit_t4003175726  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m1917472904((Predicate_1_t3614232609 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, RaycastHit_t4003175726  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, RaycastHit_t4003175726  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<UnityEngine.RaycastHit>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* RaycastHit_t4003175726_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m1987073815_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m1987073815_gshared (Predicate_1_t3614232609 * __this, RaycastHit_t4003175726  ___obj0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m1987073815_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(RaycastHit_t4003175726_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<UnityEngine.RaycastHit>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m1076303948_gshared (Predicate_1_t3614232609 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m593948232_gshared (Predicate_1_t691654621 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m3531364666_gshared (Predicate_1_t691654621 * __this, ReflectionProbeBlendInfo_t1080597738  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m3531364666((Predicate_1_t691654621 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, ReflectionProbeBlendInfo_t1080597738  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, ReflectionProbeBlendInfo_t1080597738  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* ReflectionProbeBlendInfo_t1080597738_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m2225368009_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m2225368009_gshared (Predicate_1_t691654621 * __this, ReflectionProbeBlendInfo_t1080597738  ___obj0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m2225368009_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(ReflectionProbeBlendInfo_t1080597738_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m168153818_gshared (Predicate_1_t691654621 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<UnityEngine.UICharInfo>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m2347095596_gshared (Predicate_1_t3971831663 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<UnityEngine.UICharInfo>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m3556004566_gshared (Predicate_1_t3971831663 * __this, UICharInfo_t65807484  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m3556004566((Predicate_1_t3971831663 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, UICharInfo_t65807484  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, UICharInfo_t65807484  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<UnityEngine.UICharInfo>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* UICharInfo_t65807484_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m2134733669_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m2134733669_gshared (Predicate_1_t3971831663 * __this, UICharInfo_t65807484  ___obj0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m2134733669_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(UICharInfo_t65807484_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<UnityEngine.UICharInfo>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m1961964222_gshared (Predicate_1_t3971831663 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<UnityEngine.UILineInfo>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m44565326_gshared (Predicate_1_t3724932365 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<UnityEngine.UILineInfo>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m2836886388_gshared (Predicate_1_t3724932365 * __this, UILineInfo_t4113875482  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m2836886388((Predicate_1_t3724932365 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, UILineInfo_t4113875482  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, UILineInfo_t4113875482  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<UnityEngine.UILineInfo>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* UILineInfo_t4113875482_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m94051843_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m94051843_gshared (Predicate_1_t3724932365 * __this, UILineInfo_t4113875482  ___obj0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m94051843_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(UILineInfo_t4113875482_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<UnityEngine.UILineInfo>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m25903328_gshared (Predicate_1_t3724932365 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<UnityEngine.UIVertex>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m3519192684_gshared (Predicate_1_t3855122095 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<UnityEngine.UIVertex>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m3781638678_gshared (Predicate_1_t3855122095 * __this, UIVertex_t4244065212  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m3781638678((Predicate_1_t3855122095 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, UIVertex_t4244065212  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, UIVertex_t4244065212  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<UnityEngine.UIVertex>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* UIVertex_t4244065212_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m2249257509_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m2249257509_gshared (Predicate_1_t3855122095 * __this, UIVertex_t4244065212  ___obj0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m2249257509_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(UIVertex_t4244065212_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<UnityEngine.UIVertex>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m4019409790_gshared (Predicate_1_t3855122095 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<UnityEngine.Vector2>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m2975892103_gshared (Predicate_1_t3893123448 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<UnityEngine.Vector2>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m2217101919_gshared (Predicate_1_t3893123448 * __this, Vector2_t4282066565  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m2217101919((Predicate_1_t3893123448 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, Vector2_t4282066565  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, Vector2_t4282066565  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<UnityEngine.Vector2>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Vector2_t4282066565_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m3964024626_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m3964024626_gshared (Predicate_1_t3893123448 * __this, Vector2_t4282066565  ___obj0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m3964024626_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Vector2_t4282066565_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<UnityEngine.Vector2>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m2181069525_gshared (Predicate_1_t3893123448 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<UnityEngine.Vector3>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m313728806_gshared (Predicate_1_t3893123449 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<UnityEngine.Vector3>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m1923698912_gshared (Predicate_1_t3893123449 * __this, Vector3_t4282066566  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m1923698912((Predicate_1_t3893123449 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, Vector3_t4282066566  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, Vector3_t4282066566  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<UnityEngine.Vector3>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Vector3_t4282066566_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m2038491315_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m2038491315_gshared (Predicate_1_t3893123449 * __this, Vector3_t4282066566  ___obj0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m2038491315_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Vector3_t4282066566_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<UnityEngine.Vector3>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m3206561524_gshared (Predicate_1_t3893123449 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<UnityEngine.Vector4>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m1946532805_gshared (Predicate_1_t3893123450 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<UnityEngine.Vector4>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m1630295905_gshared (Predicate_1_t3893123450 * __this, Vector4_t4282066567  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m1630295905((Predicate_1_t3893123450 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, Vector4_t4282066567  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, Vector4_t4282066567  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<UnityEngine.Vector4>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Vector4_t4282066567_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m112958004_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m112958004_gshared (Predicate_1_t3893123450 * __this, Vector4_t4282066567  ___obj0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m112958004_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Vector4_t4282066567_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<UnityEngine.Vector4>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m4232053523_gshared (Predicate_1_t3893123450 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Reflection.MonoProperty/Getter`2<System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Getter_2__ctor_m4236926794_gshared (Getter_2_t2712548107 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// R System.Reflection.MonoProperty/Getter`2<System.Object,System.Object>::Invoke(T)
extern "C"  Il2CppObject * Getter_2_Invoke_m410564889_gshared (Getter_2_t2712548107 * __this, Il2CppObject * ____this0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Getter_2_Invoke_m410564889((Getter_2_t2712548107 *)__this->get_prev_9(),____this0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ____this0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),____this0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, Il2CppObject * ____this0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),____this0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(____this0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Reflection.MonoProperty/Getter`2<System.Object,System.Object>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Getter_2_BeginInvoke_m3146221447_gshared (Getter_2_t2712548107 * __this, Il2CppObject * ____this0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ____this0;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// R System.Reflection.MonoProperty/Getter`2<System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Getter_2_EndInvoke_m1749574747_gshared (Getter_2_t2712548107 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void System.Reflection.MonoProperty/StaticGetter`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void StaticGetter_1__ctor_m3357261135_gshared (StaticGetter_1_t3245381133 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// R System.Reflection.MonoProperty/StaticGetter`1<System.Object>::Invoke()
extern "C"  Il2CppObject * StaticGetter_1_Invoke_m3410367530_gshared (StaticGetter_1_t3245381133 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		StaticGetter_1_Invoke_m3410367530((StaticGetter_1_t3245381133 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Reflection.MonoProperty/StaticGetter`1<System.Object>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * StaticGetter_1_BeginInvoke_m3837643130_gshared (StaticGetter_1_t3245381133 * __this, AsyncCallback_t1369114871 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// R System.Reflection.MonoProperty/StaticGetter`1<System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * StaticGetter_1_EndInvoke_m3212189152_gshared (StaticGetter_1_t3245381133 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void System_Collections_Generic_List77Generated/<ListA1_ConvertAll_GetDelegate_member8_arg0>c__AnonStorey7B`2<System.Object,System.Object>::.ctor()
extern "C"  void U3CListA1_ConvertAll_GetDelegate_member8_arg0U3Ec__AnonStorey7B_2__ctor_m946481058_gshared (U3CListA1_ConvertAll_GetDelegate_member8_arg0U3Ec__AnonStorey7B_2_t1251072158 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// TOutput System_Collections_Generic_List77Generated/<ListA1_ConvertAll_GetDelegate_member8_arg0>c__AnonStorey7B`2<System.Object,System.Object>::<>m__9D(T)
extern Il2CppClass* JSMgr_t70890511_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern const uint32_t U3CListA1_ConvertAll_GetDelegate_member8_arg0U3Ec__AnonStorey7B_2_U3CU3Em__9D_m2343419978_MetadataUsageId;
extern "C"  Il2CppObject * U3CListA1_ConvertAll_GetDelegate_member8_arg0U3Ec__AnonStorey7B_2_U3CU3Em__9D_m2343419978_gshared (U3CListA1_ConvertAll_GetDelegate_member8_arg0U3Ec__AnonStorey7B_2_t1251072158 * __this, Il2CppObject * ___input0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CListA1_ConvertAll_GetDelegate_member8_arg0U3Ec__AnonStorey7B_2_U3CU3Em__9D_m2343419978_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(JSMgr_t70890511_il2cpp_TypeInfo_var);
		JSVCall_t3708497963 * L_0 = ((JSMgr_t70890511_StaticFields*)JSMgr_t70890511_il2cpp_TypeInfo_var->static_fields)->get_vCall_13();
		CSRepresentedObject_t3994124630 * L_1 = (CSRepresentedObject_t3994124630 *)__this->get_objFunction_0();
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get_jsObjID_2();
		ObjectU5BU5D_t1108656482* L_3 = (ObjectU5BU5D_t1108656482*)((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)1));
		Il2CppObject * L_4 = ___input0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 0);
		ArrayElementTypeCheck (L_3, L_4);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_4);
		NullCheck((JSVCall_t3708497963 *)L_0);
		JSVCall_CallJSFunctionValue_m3892116814((JSVCall_t3708497963 *)L_0, (int32_t)0, (int32_t)L_2, (ObjectU5BU5D_t1108656482*)L_3, /*hidden argument*/NULL);
		JSDataExchangeMgr_t3744712290 * L_5 = ((JSMgr_t70890511_StaticFields*)JSMgr_t70890511_il2cpp_TypeInfo_var->static_fields)->get_datax_14();
		NullCheck((JSDataExchangeMgr_t3744712290 *)L_5);
		Il2CppObject * L_6 = JSDataExchangeMgr_getWhatever_m4049807205((JSDataExchangeMgr_t3744712290 *)L_5, (int32_t)2, /*hidden argument*/NULL);
		return ((Il2CppObject *)Castclass(L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1)));
	}
}
// System.Void System_Collections_Generic_List77Generated/<ListA1_Exists_GetDelegate_member12_arg0>c__AnonStorey7D`1<System.Object>::.ctor()
extern "C"  void U3CListA1_Exists_GetDelegate_member12_arg0U3Ec__AnonStorey7D_1__ctor_m3933060306_gshared (U3CListA1_Exists_GetDelegate_member12_arg0U3Ec__AnonStorey7D_1_t3879607955 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean System_Collections_Generic_List77Generated/<ListA1_Exists_GetDelegate_member12_arg0>c__AnonStorey7D`1<System.Object>::<>m__A2(T)
extern Il2CppClass* JSMgr_t70890511_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* JSApi_t70879249_il2cpp_TypeInfo_var;
extern const uint32_t U3CListA1_Exists_GetDelegate_member12_arg0U3Ec__AnonStorey7D_1_U3CU3Em__A2_m2196246182_MetadataUsageId;
extern "C"  bool U3CListA1_Exists_GetDelegate_member12_arg0U3Ec__AnonStorey7D_1_U3CU3Em__A2_m2196246182_gshared (U3CListA1_Exists_GetDelegate_member12_arg0U3Ec__AnonStorey7D_1_t3879607955 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CListA1_Exists_GetDelegate_member12_arg0U3Ec__AnonStorey7D_1_U3CU3Em__A2_m2196246182_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(JSMgr_t70890511_il2cpp_TypeInfo_var);
		JSVCall_t3708497963 * L_0 = ((JSMgr_t70890511_StaticFields*)JSMgr_t70890511_il2cpp_TypeInfo_var->static_fields)->get_vCall_13();
		CSRepresentedObject_t3994124630 * L_1 = (CSRepresentedObject_t3994124630 *)__this->get_objFunction_0();
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get_jsObjID_2();
		ObjectU5BU5D_t1108656482* L_3 = (ObjectU5BU5D_t1108656482*)((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)1));
		Il2CppObject * L_4 = ___obj0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 0);
		ArrayElementTypeCheck (L_3, L_4);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_4);
		NullCheck((JSVCall_t3708497963 *)L_0);
		JSVCall_CallJSFunctionValue_m3892116814((JSVCall_t3708497963 *)L_0, (int32_t)0, (int32_t)L_2, (ObjectU5BU5D_t1108656482*)L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JSApi_t70879249_il2cpp_TypeInfo_var);
		bool L_5 = JSApi_getBooleanS_m3718804830(NULL /*static, unused*/, (int32_t)2, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Void System_Collections_Generic_List77Generated/<ListA1_Find_GetDelegate_member13_arg0>c__AnonStorey7F`1<System.Object>::.ctor()
extern "C"  void U3CListA1_Find_GetDelegate_member13_arg0U3Ec__AnonStorey7F_1__ctor_m1133187664_gshared (U3CListA1_Find_GetDelegate_member13_arg0U3Ec__AnonStorey7F_1_t2977357153 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean System_Collections_Generic_List77Generated/<ListA1_Find_GetDelegate_member13_arg0>c__AnonStorey7F`1<System.Object>::<>m__A4(T)
extern Il2CppClass* JSMgr_t70890511_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* JSApi_t70879249_il2cpp_TypeInfo_var;
extern const uint32_t U3CListA1_Find_GetDelegate_member13_arg0U3Ec__AnonStorey7F_1_U3CU3Em__A4_m1779230246_MetadataUsageId;
extern "C"  bool U3CListA1_Find_GetDelegate_member13_arg0U3Ec__AnonStorey7F_1_U3CU3Em__A4_m1779230246_gshared (U3CListA1_Find_GetDelegate_member13_arg0U3Ec__AnonStorey7F_1_t2977357153 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CListA1_Find_GetDelegate_member13_arg0U3Ec__AnonStorey7F_1_U3CU3Em__A4_m1779230246_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(JSMgr_t70890511_il2cpp_TypeInfo_var);
		JSVCall_t3708497963 * L_0 = ((JSMgr_t70890511_StaticFields*)JSMgr_t70890511_il2cpp_TypeInfo_var->static_fields)->get_vCall_13();
		CSRepresentedObject_t3994124630 * L_1 = (CSRepresentedObject_t3994124630 *)__this->get_objFunction_0();
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get_jsObjID_2();
		ObjectU5BU5D_t1108656482* L_3 = (ObjectU5BU5D_t1108656482*)((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)1));
		Il2CppObject * L_4 = ___obj0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 0);
		ArrayElementTypeCheck (L_3, L_4);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_4);
		NullCheck((JSVCall_t3708497963 *)L_0);
		JSVCall_CallJSFunctionValue_m3892116814((JSVCall_t3708497963 *)L_0, (int32_t)0, (int32_t)L_2, (ObjectU5BU5D_t1108656482*)L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JSApi_t70879249_il2cpp_TypeInfo_var);
		bool L_5 = JSApi_getBooleanS_m3718804830(NULL /*static, unused*/, (int32_t)2, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Void System_Collections_Generic_List77Generated/<ListA1_FindAll_GetDelegate_member14_arg0>c__AnonStorey81`1<System.Object>::.ctor()
extern "C"  void U3CListA1_FindAll_GetDelegate_member14_arg0U3Ec__AnonStorey81_1__ctor_m3056565102_gshared (U3CListA1_FindAll_GetDelegate_member14_arg0U3Ec__AnonStorey81_1_t3940615295 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean System_Collections_Generic_List77Generated/<ListA1_FindAll_GetDelegate_member14_arg0>c__AnonStorey81`1<System.Object>::<>m__A6(T)
extern Il2CppClass* JSMgr_t70890511_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* JSApi_t70879249_il2cpp_TypeInfo_var;
extern const uint32_t U3CListA1_FindAll_GetDelegate_member14_arg0U3Ec__AnonStorey81_1_U3CU3Em__A6_m3395208478_MetadataUsageId;
extern "C"  bool U3CListA1_FindAll_GetDelegate_member14_arg0U3Ec__AnonStorey81_1_U3CU3Em__A6_m3395208478_gshared (U3CListA1_FindAll_GetDelegate_member14_arg0U3Ec__AnonStorey81_1_t3940615295 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CListA1_FindAll_GetDelegate_member14_arg0U3Ec__AnonStorey81_1_U3CU3Em__A6_m3395208478_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(JSMgr_t70890511_il2cpp_TypeInfo_var);
		JSVCall_t3708497963 * L_0 = ((JSMgr_t70890511_StaticFields*)JSMgr_t70890511_il2cpp_TypeInfo_var->static_fields)->get_vCall_13();
		CSRepresentedObject_t3994124630 * L_1 = (CSRepresentedObject_t3994124630 *)__this->get_objFunction_0();
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get_jsObjID_2();
		ObjectU5BU5D_t1108656482* L_3 = (ObjectU5BU5D_t1108656482*)((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)1));
		Il2CppObject * L_4 = ___obj0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 0);
		ArrayElementTypeCheck (L_3, L_4);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_4);
		NullCheck((JSVCall_t3708497963 *)L_0);
		JSVCall_CallJSFunctionValue_m3892116814((JSVCall_t3708497963 *)L_0, (int32_t)0, (int32_t)L_2, (ObjectU5BU5D_t1108656482*)L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JSApi_t70879249_il2cpp_TypeInfo_var);
		bool L_5 = JSApi_getBooleanS_m3718804830(NULL /*static, unused*/, (int32_t)2, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Void System_Collections_Generic_List77Generated/<ListA1_FindIndex_GetDelegate_member15_arg2>c__AnonStorey83`1<System.Object>::.ctor()
extern "C"  void U3CListA1_FindIndex_GetDelegate_member15_arg2U3Ec__AnonStorey83_1__ctor_m2194871426_gshared (U3CListA1_FindIndex_GetDelegate_member15_arg2U3Ec__AnonStorey83_1_t3719523955 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean System_Collections_Generic_List77Generated/<ListA1_FindIndex_GetDelegate_member15_arg2>c__AnonStorey83`1<System.Object>::<>m__A8(T)
extern Il2CppClass* JSMgr_t70890511_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* JSApi_t70879249_il2cpp_TypeInfo_var;
extern const uint32_t U3CListA1_FindIndex_GetDelegate_member15_arg2U3Ec__AnonStorey83_1_U3CU3Em__A8_m2554841416_MetadataUsageId;
extern "C"  bool U3CListA1_FindIndex_GetDelegate_member15_arg2U3Ec__AnonStorey83_1_U3CU3Em__A8_m2554841416_gshared (U3CListA1_FindIndex_GetDelegate_member15_arg2U3Ec__AnonStorey83_1_t3719523955 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CListA1_FindIndex_GetDelegate_member15_arg2U3Ec__AnonStorey83_1_U3CU3Em__A8_m2554841416_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(JSMgr_t70890511_il2cpp_TypeInfo_var);
		JSVCall_t3708497963 * L_0 = ((JSMgr_t70890511_StaticFields*)JSMgr_t70890511_il2cpp_TypeInfo_var->static_fields)->get_vCall_13();
		CSRepresentedObject_t3994124630 * L_1 = (CSRepresentedObject_t3994124630 *)__this->get_objFunction_0();
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get_jsObjID_2();
		ObjectU5BU5D_t1108656482* L_3 = (ObjectU5BU5D_t1108656482*)((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)1));
		Il2CppObject * L_4 = ___obj0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 0);
		ArrayElementTypeCheck (L_3, L_4);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_4);
		NullCheck((JSVCall_t3708497963 *)L_0);
		JSVCall_CallJSFunctionValue_m3892116814((JSVCall_t3708497963 *)L_0, (int32_t)0, (int32_t)L_2, (ObjectU5BU5D_t1108656482*)L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JSApi_t70879249_il2cpp_TypeInfo_var);
		bool L_5 = JSApi_getBooleanS_m3718804830(NULL /*static, unused*/, (int32_t)2, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Void System_Collections_Generic_List77Generated/<ListA1_FindIndex_GetDelegate_member16_arg1>c__AnonStorey85`1<System.Object>::.ctor()
extern "C"  void U3CListA1_FindIndex_GetDelegate_member16_arg1U3Ec__AnonStorey85_1__ctor_m1241409826_gshared (U3CListA1_FindIndex_GetDelegate_member16_arg1U3Ec__AnonStorey85_1_t60889619 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean System_Collections_Generic_List77Generated/<ListA1_FindIndex_GetDelegate_member16_arg1>c__AnonStorey85`1<System.Object>::<>m__AA(T)
extern Il2CppClass* JSMgr_t70890511_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* JSApi_t70879249_il2cpp_TypeInfo_var;
extern const uint32_t U3CListA1_FindIndex_GetDelegate_member16_arg1U3Ec__AnonStorey85_1_U3CU3Em__AA_m599312383_MetadataUsageId;
extern "C"  bool U3CListA1_FindIndex_GetDelegate_member16_arg1U3Ec__AnonStorey85_1_U3CU3Em__AA_m599312383_gshared (U3CListA1_FindIndex_GetDelegate_member16_arg1U3Ec__AnonStorey85_1_t60889619 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CListA1_FindIndex_GetDelegate_member16_arg1U3Ec__AnonStorey85_1_U3CU3Em__AA_m599312383_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(JSMgr_t70890511_il2cpp_TypeInfo_var);
		JSVCall_t3708497963 * L_0 = ((JSMgr_t70890511_StaticFields*)JSMgr_t70890511_il2cpp_TypeInfo_var->static_fields)->get_vCall_13();
		CSRepresentedObject_t3994124630 * L_1 = (CSRepresentedObject_t3994124630 *)__this->get_objFunction_0();
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get_jsObjID_2();
		ObjectU5BU5D_t1108656482* L_3 = (ObjectU5BU5D_t1108656482*)((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)1));
		Il2CppObject * L_4 = ___obj0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 0);
		ArrayElementTypeCheck (L_3, L_4);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_4);
		NullCheck((JSVCall_t3708497963 *)L_0);
		JSVCall_CallJSFunctionValue_m3892116814((JSVCall_t3708497963 *)L_0, (int32_t)0, (int32_t)L_2, (ObjectU5BU5D_t1108656482*)L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JSApi_t70879249_il2cpp_TypeInfo_var);
		bool L_5 = JSApi_getBooleanS_m3718804830(NULL /*static, unused*/, (int32_t)2, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Void System_Collections_Generic_List77Generated/<ListA1_FindIndex_GetDelegate_member17_arg0>c__AnonStorey87`1<System.Object>::.ctor()
extern "C"  void U3CListA1_FindIndex_GetDelegate_member17_arg0U3Ec__AnonStorey87_1__ctor_m287948226_gshared (U3CListA1_FindIndex_GetDelegate_member17_arg0U3Ec__AnonStorey87_1_t697222579 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean System_Collections_Generic_List77Generated/<ListA1_FindIndex_GetDelegate_member17_arg0>c__AnonStorey87`1<System.Object>::<>m__AC(T)
extern Il2CppClass* JSMgr_t70890511_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* JSApi_t70879249_il2cpp_TypeInfo_var;
extern const uint32_t U3CListA1_FindIndex_GetDelegate_member17_arg0U3Ec__AnonStorey87_1_U3CU3Em__AC_m2938542109_MetadataUsageId;
extern "C"  bool U3CListA1_FindIndex_GetDelegate_member17_arg0U3Ec__AnonStorey87_1_U3CU3Em__AC_m2938542109_gshared (U3CListA1_FindIndex_GetDelegate_member17_arg0U3Ec__AnonStorey87_1_t697222579 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CListA1_FindIndex_GetDelegate_member17_arg0U3Ec__AnonStorey87_1_U3CU3Em__AC_m2938542109_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(JSMgr_t70890511_il2cpp_TypeInfo_var);
		JSVCall_t3708497963 * L_0 = ((JSMgr_t70890511_StaticFields*)JSMgr_t70890511_il2cpp_TypeInfo_var->static_fields)->get_vCall_13();
		CSRepresentedObject_t3994124630 * L_1 = (CSRepresentedObject_t3994124630 *)__this->get_objFunction_0();
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get_jsObjID_2();
		ObjectU5BU5D_t1108656482* L_3 = (ObjectU5BU5D_t1108656482*)((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)1));
		Il2CppObject * L_4 = ___obj0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 0);
		ArrayElementTypeCheck (L_3, L_4);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_4);
		NullCheck((JSVCall_t3708497963 *)L_0);
		JSVCall_CallJSFunctionValue_m3892116814((JSVCall_t3708497963 *)L_0, (int32_t)0, (int32_t)L_2, (ObjectU5BU5D_t1108656482*)L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JSApi_t70879249_il2cpp_TypeInfo_var);
		bool L_5 = JSApi_getBooleanS_m3718804830(NULL /*static, unused*/, (int32_t)2, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Void System_Collections_Generic_List77Generated/<ListA1_FindLast_GetDelegate_member18_arg0>c__AnonStorey89`1<System.Object>::.ctor()
extern "C"  void U3CListA1_FindLast_GetDelegate_member18_arg0U3Ec__AnonStorey89_1__ctor_m2724864499_gshared (U3CListA1_FindLast_GetDelegate_member18_arg0U3Ec__AnonStorey89_1_t3174686476 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean System_Collections_Generic_List77Generated/<ListA1_FindLast_GetDelegate_member18_arg0>c__AnonStorey89`1<System.Object>::<>m__AE(T)
extern Il2CppClass* JSMgr_t70890511_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* JSApi_t70879249_il2cpp_TypeInfo_var;
extern const uint32_t U3CListA1_FindLast_GetDelegate_member18_arg0U3Ec__AnonStorey89_1_U3CU3Em__AE_m763121650_MetadataUsageId;
extern "C"  bool U3CListA1_FindLast_GetDelegate_member18_arg0U3Ec__AnonStorey89_1_U3CU3Em__AE_m763121650_gshared (U3CListA1_FindLast_GetDelegate_member18_arg0U3Ec__AnonStorey89_1_t3174686476 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CListA1_FindLast_GetDelegate_member18_arg0U3Ec__AnonStorey89_1_U3CU3Em__AE_m763121650_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(JSMgr_t70890511_il2cpp_TypeInfo_var);
		JSVCall_t3708497963 * L_0 = ((JSMgr_t70890511_StaticFields*)JSMgr_t70890511_il2cpp_TypeInfo_var->static_fields)->get_vCall_13();
		CSRepresentedObject_t3994124630 * L_1 = (CSRepresentedObject_t3994124630 *)__this->get_objFunction_0();
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get_jsObjID_2();
		ObjectU5BU5D_t1108656482* L_3 = (ObjectU5BU5D_t1108656482*)((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)1));
		Il2CppObject * L_4 = ___obj0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 0);
		ArrayElementTypeCheck (L_3, L_4);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_4);
		NullCheck((JSVCall_t3708497963 *)L_0);
		JSVCall_CallJSFunctionValue_m3892116814((JSVCall_t3708497963 *)L_0, (int32_t)0, (int32_t)L_2, (ObjectU5BU5D_t1108656482*)L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JSApi_t70879249_il2cpp_TypeInfo_var);
		bool L_5 = JSApi_getBooleanS_m3718804830(NULL /*static, unused*/, (int32_t)2, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Void System_Collections_Generic_List77Generated/<ListA1_FindLastIndex_GetDelegate_member19_arg2>c__AnonStorey8B`1<System.Object>::.ctor()
extern "C"  void U3CListA1_FindLastIndex_GetDelegate_member19_arg2U3Ec__AnonStorey8B_1__ctor_m864804951_gshared (U3CListA1_FindLastIndex_GetDelegate_member19_arg2U3Ec__AnonStorey8B_1_t3672975744 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean System_Collections_Generic_List77Generated/<ListA1_FindLastIndex_GetDelegate_member19_arg2>c__AnonStorey8B`1<System.Object>::<>m__B0(T)
extern Il2CppClass* JSMgr_t70890511_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* JSApi_t70879249_il2cpp_TypeInfo_var;
extern const uint32_t U3CListA1_FindLastIndex_GetDelegate_member19_arg2U3Ec__AnonStorey8B_1_U3CU3Em__B0_m699080668_MetadataUsageId;
extern "C"  bool U3CListA1_FindLastIndex_GetDelegate_member19_arg2U3Ec__AnonStorey8B_1_U3CU3Em__B0_m699080668_gshared (U3CListA1_FindLastIndex_GetDelegate_member19_arg2U3Ec__AnonStorey8B_1_t3672975744 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CListA1_FindLastIndex_GetDelegate_member19_arg2U3Ec__AnonStorey8B_1_U3CU3Em__B0_m699080668_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(JSMgr_t70890511_il2cpp_TypeInfo_var);
		JSVCall_t3708497963 * L_0 = ((JSMgr_t70890511_StaticFields*)JSMgr_t70890511_il2cpp_TypeInfo_var->static_fields)->get_vCall_13();
		CSRepresentedObject_t3994124630 * L_1 = (CSRepresentedObject_t3994124630 *)__this->get_objFunction_0();
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get_jsObjID_2();
		ObjectU5BU5D_t1108656482* L_3 = (ObjectU5BU5D_t1108656482*)((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)1));
		Il2CppObject * L_4 = ___obj0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 0);
		ArrayElementTypeCheck (L_3, L_4);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_4);
		NullCheck((JSVCall_t3708497963 *)L_0);
		JSVCall_CallJSFunctionValue_m3892116814((JSVCall_t3708497963 *)L_0, (int32_t)0, (int32_t)L_2, (ObjectU5BU5D_t1108656482*)L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JSApi_t70879249_il2cpp_TypeInfo_var);
		bool L_5 = JSApi_getBooleanS_m3718804830(NULL /*static, unused*/, (int32_t)2, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Void System_Collections_Generic_List77Generated/<ListA1_FindLastIndex_GetDelegate_member20_arg1>c__AnonStorey8D`1<System.Object>::.ctor()
extern "C"  void U3CListA1_FindLastIndex_GetDelegate_member20_arg1U3Ec__AnonStorey8D_1__ctor_m3092675650_gshared (U3CListA1_FindLastIndex_GetDelegate_member20_arg1U3Ec__AnonStorey8D_1_t2423100467 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean System_Collections_Generic_List77Generated/<ListA1_FindLastIndex_GetDelegate_member20_arg1>c__AnonStorey8D`1<System.Object>::<>m__B2(T)
extern Il2CppClass* JSMgr_t70890511_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* JSApi_t70879249_il2cpp_TypeInfo_var;
extern const uint32_t U3CListA1_FindLastIndex_GetDelegate_member20_arg1U3Ec__AnonStorey8D_1_U3CU3Em__B2_m1065509071_MetadataUsageId;
extern "C"  bool U3CListA1_FindLastIndex_GetDelegate_member20_arg1U3Ec__AnonStorey8D_1_U3CU3Em__B2_m1065509071_gshared (U3CListA1_FindLastIndex_GetDelegate_member20_arg1U3Ec__AnonStorey8D_1_t2423100467 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CListA1_FindLastIndex_GetDelegate_member20_arg1U3Ec__AnonStorey8D_1_U3CU3Em__B2_m1065509071_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(JSMgr_t70890511_il2cpp_TypeInfo_var);
		JSVCall_t3708497963 * L_0 = ((JSMgr_t70890511_StaticFields*)JSMgr_t70890511_il2cpp_TypeInfo_var->static_fields)->get_vCall_13();
		CSRepresentedObject_t3994124630 * L_1 = (CSRepresentedObject_t3994124630 *)__this->get_objFunction_0();
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get_jsObjID_2();
		ObjectU5BU5D_t1108656482* L_3 = (ObjectU5BU5D_t1108656482*)((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)1));
		Il2CppObject * L_4 = ___obj0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 0);
		ArrayElementTypeCheck (L_3, L_4);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_4);
		NullCheck((JSVCall_t3708497963 *)L_0);
		JSVCall_CallJSFunctionValue_m3892116814((JSVCall_t3708497963 *)L_0, (int32_t)0, (int32_t)L_2, (ObjectU5BU5D_t1108656482*)L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JSApi_t70879249_il2cpp_TypeInfo_var);
		bool L_5 = JSApi_getBooleanS_m3718804830(NULL /*static, unused*/, (int32_t)2, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Void System_Collections_Generic_List77Generated/<ListA1_FindLastIndex_GetDelegate_member21_arg0>c__AnonStorey8F`1<System.Object>::.ctor()
extern "C"  void U3CListA1_FindLastIndex_GetDelegate_member21_arg0U3Ec__AnonStorey8F_1__ctor_m2139214050_gshared (U3CListA1_FindLastIndex_GetDelegate_member21_arg0U3Ec__AnonStorey8F_1_t3059433427 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean System_Collections_Generic_List77Generated/<ListA1_FindLastIndex_GetDelegate_member21_arg0>c__AnonStorey8F`1<System.Object>::<>m__B4(T)
extern Il2CppClass* JSMgr_t70890511_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* JSApi_t70879249_il2cpp_TypeInfo_var;
extern const uint32_t U3CListA1_FindLastIndex_GetDelegate_member21_arg0U3Ec__AnonStorey8F_1_U3CU3Em__B4_m3404738797_MetadataUsageId;
extern "C"  bool U3CListA1_FindLastIndex_GetDelegate_member21_arg0U3Ec__AnonStorey8F_1_U3CU3Em__B4_m3404738797_gshared (U3CListA1_FindLastIndex_GetDelegate_member21_arg0U3Ec__AnonStorey8F_1_t3059433427 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CListA1_FindLastIndex_GetDelegate_member21_arg0U3Ec__AnonStorey8F_1_U3CU3Em__B4_m3404738797_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(JSMgr_t70890511_il2cpp_TypeInfo_var);
		JSVCall_t3708497963 * L_0 = ((JSMgr_t70890511_StaticFields*)JSMgr_t70890511_il2cpp_TypeInfo_var->static_fields)->get_vCall_13();
		CSRepresentedObject_t3994124630 * L_1 = (CSRepresentedObject_t3994124630 *)__this->get_objFunction_0();
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get_jsObjID_2();
		ObjectU5BU5D_t1108656482* L_3 = (ObjectU5BU5D_t1108656482*)((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)1));
		Il2CppObject * L_4 = ___obj0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 0);
		ArrayElementTypeCheck (L_3, L_4);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_4);
		NullCheck((JSVCall_t3708497963 *)L_0);
		JSVCall_CallJSFunctionValue_m3892116814((JSVCall_t3708497963 *)L_0, (int32_t)0, (int32_t)L_2, (ObjectU5BU5D_t1108656482*)L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JSApi_t70879249_il2cpp_TypeInfo_var);
		bool L_5 = JSApi_getBooleanS_m3718804830(NULL /*static, unused*/, (int32_t)2, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Void System_Collections_Generic_List77Generated/<ListA1_ForEach_GetDelegate_member22_arg0>c__AnonStorey91`1<System.Object>::.ctor()
extern "C"  void U3CListA1_ForEach_GetDelegate_member22_arg0U3Ec__AnonStorey91_1__ctor_m3339397458_gshared (U3CListA1_ForEach_GetDelegate_member22_arg0U3Ec__AnonStorey91_1_t3044851907 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System_Collections_Generic_List77Generated/<ListA1_ForEach_GetDelegate_member22_arg0>c__AnonStorey91`1<System.Object>::<>m__B6(T)
extern Il2CppClass* JSMgr_t70890511_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern const uint32_t U3CListA1_ForEach_GetDelegate_member22_arg0U3Ec__AnonStorey91_1_U3CU3Em__B6_m2507836527_MetadataUsageId;
extern "C"  void U3CListA1_ForEach_GetDelegate_member22_arg0U3Ec__AnonStorey91_1_U3CU3Em__B6_m2507836527_gshared (U3CListA1_ForEach_GetDelegate_member22_arg0U3Ec__AnonStorey91_1_t3044851907 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CListA1_ForEach_GetDelegate_member22_arg0U3Ec__AnonStorey91_1_U3CU3Em__B6_m2507836527_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(JSMgr_t70890511_il2cpp_TypeInfo_var);
		JSVCall_t3708497963 * L_0 = ((JSMgr_t70890511_StaticFields*)JSMgr_t70890511_il2cpp_TypeInfo_var->static_fields)->get_vCall_13();
		CSRepresentedObject_t3994124630 * L_1 = (CSRepresentedObject_t3994124630 *)__this->get_objFunction_0();
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get_jsObjID_2();
		ObjectU5BU5D_t1108656482* L_3 = (ObjectU5BU5D_t1108656482*)((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)1));
		Il2CppObject * L_4 = ___obj0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 0);
		ArrayElementTypeCheck (L_3, L_4);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_4);
		NullCheck((JSVCall_t3708497963 *)L_0);
		JSVCall_CallJSFunctionValue_m3892116814((JSVCall_t3708497963 *)L_0, (int32_t)0, (int32_t)L_2, (ObjectU5BU5D_t1108656482*)L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System_Collections_Generic_List77Generated/<ListA1_RemoveAll_GetDelegate_member34_arg0>c__AnonStorey93`1<System.Object>::.ctor()
extern "C"  void U3CListA1_RemoveAll_GetDelegate_member34_arg0U3Ec__AnonStorey93_1__ctor_m4139072102_gshared (U3CListA1_RemoveAll_GetDelegate_member34_arg0U3Ec__AnonStorey93_1_t3917406903 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean System_Collections_Generic_List77Generated/<ListA1_RemoveAll_GetDelegate_member34_arg0>c__AnonStorey93`1<System.Object>::<>m__B8(T)
extern Il2CppClass* JSMgr_t70890511_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* JSApi_t70879249_il2cpp_TypeInfo_var;
extern const uint32_t U3CListA1_RemoveAll_GetDelegate_member34_arg0U3Ec__AnonStorey93_1_U3CU3Em__B8_m309149797_MetadataUsageId;
extern "C"  bool U3CListA1_RemoveAll_GetDelegate_member34_arg0U3Ec__AnonStorey93_1_U3CU3Em__B8_m309149797_gshared (U3CListA1_RemoveAll_GetDelegate_member34_arg0U3Ec__AnonStorey93_1_t3917406903 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CListA1_RemoveAll_GetDelegate_member34_arg0U3Ec__AnonStorey93_1_U3CU3Em__B8_m309149797_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(JSMgr_t70890511_il2cpp_TypeInfo_var);
		JSVCall_t3708497963 * L_0 = ((JSMgr_t70890511_StaticFields*)JSMgr_t70890511_il2cpp_TypeInfo_var->static_fields)->get_vCall_13();
		CSRepresentedObject_t3994124630 * L_1 = (CSRepresentedObject_t3994124630 *)__this->get_objFunction_0();
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get_jsObjID_2();
		ObjectU5BU5D_t1108656482* L_3 = (ObjectU5BU5D_t1108656482*)((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)1));
		Il2CppObject * L_4 = ___obj0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 0);
		ArrayElementTypeCheck (L_3, L_4);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_4);
		NullCheck((JSVCall_t3708497963 *)L_0);
		JSVCall_CallJSFunctionValue_m3892116814((JSVCall_t3708497963 *)L_0, (int32_t)0, (int32_t)L_2, (ObjectU5BU5D_t1108656482*)L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JSApi_t70879249_il2cpp_TypeInfo_var);
		bool L_5 = JSApi_getBooleanS_m3718804830(NULL /*static, unused*/, (int32_t)2, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Void System_Collections_Generic_List77Generated/<ListA1_Sort_GetDelegate_member40_arg0>c__AnonStorey95`1<System.Object>::.ctor()
extern "C"  void U3CListA1_Sort_GetDelegate_member40_arg0U3Ec__AnonStorey95_1__ctor_m2440284680_gshared (U3CListA1_Sort_GetDelegate_member40_arg0U3Ec__AnonStorey95_1_t3640470617 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 System_Collections_Generic_List77Generated/<ListA1_Sort_GetDelegate_member40_arg0>c__AnonStorey95`1<System.Object>::<>m__BA(T,T)
extern Il2CppClass* JSMgr_t70890511_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* JSApi_t70879249_il2cpp_TypeInfo_var;
extern const uint32_t U3CListA1_Sort_GetDelegate_member40_arg0U3Ec__AnonStorey95_1_U3CU3Em__BA_m3903861812_MetadataUsageId;
extern "C"  int32_t U3CListA1_Sort_GetDelegate_member40_arg0U3Ec__AnonStorey95_1_U3CU3Em__BA_m3903861812_gshared (U3CListA1_Sort_GetDelegate_member40_arg0U3Ec__AnonStorey95_1_t3640470617 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CListA1_Sort_GetDelegate_member40_arg0U3Ec__AnonStorey95_1_U3CU3Em__BA_m3903861812_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(JSMgr_t70890511_il2cpp_TypeInfo_var);
		JSVCall_t3708497963 * L_0 = ((JSMgr_t70890511_StaticFields*)JSMgr_t70890511_il2cpp_TypeInfo_var->static_fields)->get_vCall_13();
		CSRepresentedObject_t3994124630 * L_1 = (CSRepresentedObject_t3994124630 *)__this->get_objFunction_0();
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get_jsObjID_2();
		ObjectU5BU5D_t1108656482* L_3 = (ObjectU5BU5D_t1108656482*)((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)2));
		Il2CppObject * L_4 = ___x0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 0);
		ArrayElementTypeCheck (L_3, L_4);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_4);
		ObjectU5BU5D_t1108656482* L_5 = (ObjectU5BU5D_t1108656482*)L_3;
		Il2CppObject * L_6 = ___y1;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 1);
		ArrayElementTypeCheck (L_5, L_6);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_6);
		NullCheck((JSVCall_t3708497963 *)L_0);
		JSVCall_CallJSFunctionValue_m3892116814((JSVCall_t3708497963 *)L_0, (int32_t)0, (int32_t)L_2, (ObjectU5BU5D_t1108656482*)L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JSApi_t70879249_il2cpp_TypeInfo_var);
		int32_t L_7 = JSApi_getInt32_m2845440709(NULL /*static, unused*/, (int32_t)2, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Void System_Collections_Generic_List77Generated/<ListA1_TrueForAll_GetDelegate_member45_arg0>c__AnonStorey97`1<System.Object>::.ctor()
extern "C"  void U3CListA1_TrueForAll_GetDelegate_member45_arg0U3Ec__AnonStorey97_1__ctor_m1181171917_gshared (U3CListA1_TrueForAll_GetDelegate_member45_arg0U3Ec__AnonStorey97_1_t3358868182 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean System_Collections_Generic_List77Generated/<ListA1_TrueForAll_GetDelegate_member45_arg0>c__AnonStorey97`1<System.Object>::<>m__BC(T)
extern Il2CppClass* JSMgr_t70890511_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* JSApi_t70879249_il2cpp_TypeInfo_var;
extern const uint32_t U3CListA1_TrueForAll_GetDelegate_member45_arg0U3Ec__AnonStorey97_1_U3CU3Em__BC_m2034572443_MetadataUsageId;
extern "C"  bool U3CListA1_TrueForAll_GetDelegate_member45_arg0U3Ec__AnonStorey97_1_U3CU3Em__BC_m2034572443_gshared (U3CListA1_TrueForAll_GetDelegate_member45_arg0U3Ec__AnonStorey97_1_t3358868182 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CListA1_TrueForAll_GetDelegate_member45_arg0U3Ec__AnonStorey97_1_U3CU3Em__BC_m2034572443_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(JSMgr_t70890511_il2cpp_TypeInfo_var);
		JSVCall_t3708497963 * L_0 = ((JSMgr_t70890511_StaticFields*)JSMgr_t70890511_il2cpp_TypeInfo_var->static_fields)->get_vCall_13();
		CSRepresentedObject_t3994124630 * L_1 = (CSRepresentedObject_t3994124630 *)__this->get_objFunction_0();
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get_jsObjID_2();
		ObjectU5BU5D_t1108656482* L_3 = (ObjectU5BU5D_t1108656482*)((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)1));
		Il2CppObject * L_4 = ___obj0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 0);
		ArrayElementTypeCheck (L_3, L_4);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_4);
		NullCheck((JSVCall_t3708497963 *)L_0);
		JSVCall_CallJSFunctionValue_m3892116814((JSVCall_t3708497963 *)L_0, (int32_t)0, (int32_t)L_2, (ObjectU5BU5D_t1108656482*)L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JSApi_t70879249_il2cpp_TypeInfo_var);
		bool L_5 = JSApi_getBooleanS_m3718804830(NULL /*static, unused*/, (int32_t)2, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsDelegate`2<System.Object,System.Boolean>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAdsDelegate_2__ctor_m3333732870_gshared (UnityAdsDelegate_2_t92490124 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Advertisements.UnityAdsDelegate`2<System.Object,System.Boolean>::Invoke(T1,T2)
extern "C"  void UnityAdsDelegate_2_Invoke_m3219353189_gshared (UnityAdsDelegate_2_t92490124 * __this, Il2CppObject * ___p10, bool ___p21, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		UnityAdsDelegate_2_Invoke_m3219353189((UnityAdsDelegate_2_t92490124 *)__this->get_prev_9(),___p10, ___p21, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___p10, bool ___p21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___p10, ___p21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___p10, bool ___p21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___p10, ___p21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, bool ___p21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___p10, ___p21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.Advertisements.UnityAdsDelegate`2<System.Object,System.Boolean>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
extern Il2CppClass* Boolean_t476798718_il2cpp_TypeInfo_var;
extern const uint32_t UnityAdsDelegate_2_BeginInvoke_m904198668_MetadataUsageId;
extern "C"  Il2CppObject * UnityAdsDelegate_2_BeginInvoke_m904198668_gshared (UnityAdsDelegate_2_t92490124 * __this, Il2CppObject * ___p10, bool ___p21, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityAdsDelegate_2_BeginInvoke_m904198668_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___p10;
	__d_args[1] = Box(Boolean_t476798718_il2cpp_TypeInfo_var, &___p21);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void UnityEngine.Advertisements.UnityAdsDelegate`2<System.Object,System.Boolean>::EndInvoke(System.IAsyncResult)
extern "C"  void UnityAdsDelegate_2_EndInvoke_m399646486_gshared (UnityAdsDelegate_2_t92490124 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Advertisements.UnityAdsDelegate`2<System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAdsDelegate_2__ctor_m4033885901_gshared (UnityAdsDelegate_2_t3786507777 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Advertisements.UnityAdsDelegate`2<System.Object,System.Object>::Invoke(T1,T2)
extern "C"  void UnityAdsDelegate_2_Invoke_m1818993470_gshared (UnityAdsDelegate_2_t3786507777 * __this, Il2CppObject * ___p10, Il2CppObject * ___p21, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		UnityAdsDelegate_2_Invoke_m1818993470((UnityAdsDelegate_2_t3786507777 *)__this->get_prev_9(),___p10, ___p21, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___p10, Il2CppObject * ___p21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___p10, ___p21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___p10, Il2CppObject * ___p21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___p10, ___p21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___p21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___p10, ___p21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.Advertisements.UnityAdsDelegate`2<System.Object,System.Object>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * UnityAdsDelegate_2_BeginInvoke_m3096834653_gshared (UnityAdsDelegate_2_t3786507777 * __this, Il2CppObject * ___p10, Il2CppObject * ___p21, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___p10;
	__d_args[1] = ___p21;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void UnityEngine.Advertisements.UnityAdsDelegate`2<System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void UnityAdsDelegate_2_EndInvoke_m1306597213_gshared (UnityAdsDelegate_2_t3786507777 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Boolean>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern const uint32_t CachedInvokableCall_1__ctor_m562163564_MetadataUsageId;
extern "C"  void CachedInvokableCall_1__ctor_m562163564_gshared (CachedInvokableCall_1_t65035577 * __this, Object_t3071478659 * ___target0, MethodInfo_t * ___theFunction1, bool ___argument2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CachedInvokableCall_1__ctor_m562163564_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_m_Arg1_1(((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)1)));
		Object_t3071478659 * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		NullCheck((InvokableCall_1_t2626711275 *)__this);
		((  void (*) (InvokableCall_1_t2626711275 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((InvokableCall_1_t2626711275 *)__this, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		ObjectU5BU5D_t1108656482* L_2 = (ObjectU5BU5D_t1108656482*)__this->get_m_Arg1_1();
		bool L_3 = ___argument2;
		bool L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_4);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		ArrayElementTypeCheck (L_2, L_5);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_5);
		return;
	}
}
// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Boolean>::Invoke(System.Object[])
extern "C"  void CachedInvokableCall_1_Invoke_m3206745387_gshared (CachedInvokableCall_1_t65035577 * __this, ObjectU5BU5D_t1108656482* ___args0, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t1108656482* L_0 = (ObjectU5BU5D_t1108656482*)__this->get_m_Arg1_1();
		NullCheck((InvokableCall_1_t2626711275 *)__this);
		((  void (*) (InvokableCall_1_t2626711275 *, ObjectU5BU5D_t1108656482*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((InvokableCall_1_t2626711275 *)__this, (ObjectU5BU5D_t1108656482*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return;
	}
}
// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Int32>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern const uint32_t CachedInvokableCall_1__ctor_m1791898438_MetadataUsageId;
extern "C"  void CachedInvokableCall_1__ctor_m1791898438_gshared (CachedInvokableCall_1_t742075359 * __this, Object_t3071478659 * ___target0, MethodInfo_t * ___theFunction1, int32_t ___argument2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CachedInvokableCall_1__ctor_m1791898438_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_m_Arg1_1(((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)1)));
		Object_t3071478659 * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		NullCheck((InvokableCall_1_t3303751057 *)__this);
		((  void (*) (InvokableCall_1_t3303751057 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((InvokableCall_1_t3303751057 *)__this, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		ObjectU5BU5D_t1108656482* L_2 = (ObjectU5BU5D_t1108656482*)__this->get_m_Arg1_1();
		int32_t L_3 = ___argument2;
		int32_t L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_4);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		ArrayElementTypeCheck (L_2, L_5);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_5);
		return;
	}
}
// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Int32>::Invoke(System.Object[])
extern "C"  void CachedInvokableCall_1_Invoke_m3818847761_gshared (CachedInvokableCall_1_t742075359 * __this, ObjectU5BU5D_t1108656482* ___args0, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t1108656482* L_0 = (ObjectU5BU5D_t1108656482*)__this->get_m_Arg1_1();
		NullCheck((InvokableCall_1_t3303751057 *)__this);
		((  void (*) (InvokableCall_1_t3303751057 *, ObjectU5BU5D_t1108656482*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((InvokableCall_1_t3303751057 *)__this, (ObjectU5BU5D_t1108656482*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return;
	}
}
// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Object>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern const uint32_t CachedInvokableCall_1__ctor_m165005445_MetadataUsageId;
extern "C"  void CachedInvokableCall_1__ctor_m165005445_gshared (CachedInvokableCall_1_t3759053230 * __this, Object_t3071478659 * ___target0, MethodInfo_t * ___theFunction1, Il2CppObject * ___argument2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CachedInvokableCall_1__ctor_m165005445_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_m_Arg1_1(((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)1)));
		Object_t3071478659 * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		NullCheck((InvokableCall_1_t2025761632 *)__this);
		((  void (*) (InvokableCall_1_t2025761632 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((InvokableCall_1_t2025761632 *)__this, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		ObjectU5BU5D_t1108656482* L_2 = (ObjectU5BU5D_t1108656482*)__this->get_m_Arg1_1();
		Il2CppObject * L_3 = ___argument2;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		return;
	}
}
// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Object>::Invoke(System.Object[])
extern "C"  void CachedInvokableCall_1_Invoke_m3570141426_gshared (CachedInvokableCall_1_t3759053230 * __this, ObjectU5BU5D_t1108656482* ___args0, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t1108656482* L_0 = (ObjectU5BU5D_t1108656482*)__this->get_m_Arg1_1();
		NullCheck((InvokableCall_1_t2025761632 *)__this);
		((  void (*) (InvokableCall_1_t2025761632 *, ObjectU5BU5D_t1108656482*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((InvokableCall_1_t2025761632 *)__this, (ObjectU5BU5D_t1108656482*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return;
	}
}
// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Single>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern const uint32_t CachedInvokableCall_1__ctor_m3760156124_MetadataUsageId;
extern "C"  void CachedInvokableCall_1__ctor_m3760156124_gshared (CachedInvokableCall_1_t3880155831 * __this, Object_t3071478659 * ___target0, MethodInfo_t * ___theFunction1, float ___argument2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CachedInvokableCall_1__ctor_m3760156124_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_m_Arg1_1(((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)1)));
		Object_t3071478659 * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		NullCheck((InvokableCall_1_t2146864233 *)__this);
		((  void (*) (InvokableCall_1_t2146864233 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((InvokableCall_1_t2146864233 *)__this, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		ObjectU5BU5D_t1108656482* L_2 = (ObjectU5BU5D_t1108656482*)__this->get_m_Arg1_1();
		float L_3 = ___argument2;
		float L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_4);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		ArrayElementTypeCheck (L_2, L_5);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_5);
		return;
	}
}
// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Single>::Invoke(System.Object[])
extern "C"  void CachedInvokableCall_1_Invoke_m4174790843_gshared (CachedInvokableCall_1_t3880155831 * __this, ObjectU5BU5D_t1108656482* ___args0, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t1108656482* L_0 = (ObjectU5BU5D_t1108656482*)__this->get_m_Arg1_1();
		NullCheck((InvokableCall_1_t2146864233 *)__this);
		((  void (*) (InvokableCall_1_t2146864233 *, ObjectU5BU5D_t1108656482*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((InvokableCall_1_t2146864233 *)__this, (ObjectU5BU5D_t1108656482*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Boolean>::.ctor(System.Object,System.Reflection.MethodInfo)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t InvokableCall_1__ctor_m364542482_MetadataUsageId;
extern "C"  void InvokableCall_1__ctor_m364542482_gshared (InvokableCall_1_t2626711275 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_1__ctor_m364542482_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		NullCheck((BaseInvokableCall_t1559630662 *)__this);
		BaseInvokableCall__ctor_m1692884045((BaseInvokableCall_t1559630662 *)__this, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/NULL);
		UnityAction_1_t370978721 * L_2 = (UnityAction_1_t370978721 *)__this->get_Delegate_0();
		MethodInfo_t * L_3 = ___theFunction1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_4 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		Il2CppObject * L_5 = ___target0;
		Delegate_t3310234105 * L_6 = NetFxCoreExtensions_CreateDelegate_m3408605866(NULL /*static, unused*/, (MethodInfo_t *)L_3, (Type_t *)L_4, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		Delegate_t3310234105 * L_7 = Delegate_Combine_m1842362874(NULL /*static, unused*/, (Delegate_t3310234105 *)L_2, (Delegate_t3310234105 *)((UnityAction_1_t370978721 *)Castclass(L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), /*hidden argument*/NULL);
		__this->set_Delegate_0(((UnityAction_1_t370978721 *)Castclass(L_7, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Boolean>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
extern "C"  void InvokableCall_1__ctor_m1301209584_gshared (InvokableCall_1_t2626711275 * __this, UnityAction_1_t370978721 * ___action0, const MethodInfo* method)
{
	{
		NullCheck((BaseInvokableCall_t1559630662 *)__this);
		BaseInvokableCall__ctor_m2839815662((BaseInvokableCall_t1559630662 *)__this, /*hidden argument*/NULL);
		UnityAction_1_t370978721 * L_0 = (UnityAction_1_t370978721 *)__this->get_Delegate_0();
		UnityAction_1_t370978721 * L_1 = ___action0;
		Delegate_t3310234105 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, (Delegate_t3310234105 *)L_0, (Delegate_t3310234105 *)L_1, /*hidden argument*/NULL);
		__this->set_Delegate_0(((UnityAction_1_t370978721 *)Castclass(L_2, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Boolean>::Invoke(System.Object[])
extern Il2CppClass* ArgumentException_t928607144_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3093451866;
extern const uint32_t InvokableCall_1_Invoke_m4112204073_MetadataUsageId;
extern "C"  void InvokableCall_1_Invoke_m4112204073_gshared (InvokableCall_1_t2626711275 * __this, ObjectU5BU5D_t1108656482* ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_1_Invoke_m4112204073_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1108656482* L_0 = ___args0;
		NullCheck(L_0);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))) == ((int32_t)1)))
		{
			goto IL_0014;
		}
	}
	{
		ArgumentException_t928607144 * L_1 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_1, (String_t*)_stringLiteral3093451866, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0014:
	{
		ObjectU5BU5D_t1108656482* L_2 = ___args0;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		int32_t L_3 = 0;
		Il2CppObject * L_4 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		UnityAction_1_t370978721 * L_5 = (UnityAction_1_t370978721 *)__this->get_Delegate_0();
		bool L_6 = BaseInvokableCall_AllowInvoke_m2941661095(NULL /*static, unused*/, (Delegate_t3310234105 *)L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_003f;
		}
	}
	{
		UnityAction_1_t370978721 * L_7 = (UnityAction_1_t370978721 *)__this->get_Delegate_0();
		ObjectU5BU5D_t1108656482* L_8 = ___args0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 0);
		int32_t L_9 = 0;
		Il2CppObject * L_10 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		NullCheck((UnityAction_1_t370978721 *)L_7);
		((  void (*) (UnityAction_1_t370978721 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((UnityAction_1_t370978721 *)L_7, (bool)((*(bool*)((bool*)UnBox (L_10, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
	}

IL_003f:
	{
		return;
	}
}
// System.Boolean UnityEngine.Events.InvokableCall`1<System.Boolean>::Find(System.Object,System.Reflection.MethodInfo)
extern "C"  bool InvokableCall_1_Find_m560012207_gshared (InvokableCall_1_t2626711275 * __this, Il2CppObject * ___targetObj0, MethodInfo_t * ___method1, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		UnityAction_1_t370978721 * L_0 = (UnityAction_1_t370978721 *)__this->get_Delegate_0();
		NullCheck((Delegate_t3310234105 *)L_0);
		Il2CppObject * L_1 = Delegate_get_Target_m2860483769((Delegate_t3310234105 *)L_0, /*hidden argument*/NULL);
		Il2CppObject * L_2 = ___targetObj0;
		if ((!(((Il2CppObject*)(Il2CppObject *)L_1) == ((Il2CppObject*)(Il2CppObject *)L_2))))
		{
			goto IL_0021;
		}
	}
	{
		UnityAction_1_t370978721 * L_3 = (UnityAction_1_t370978721 *)__this->get_Delegate_0();
		MethodInfo_t * L_4 = NetFxCoreExtensions_GetMethodInfo_m1628849205(NULL /*static, unused*/, (Delegate_t3310234105 *)L_3, /*hidden argument*/NULL);
		MethodInfo_t * L_5 = ___method1;
		G_B3_0 = ((((Il2CppObject*)(MethodInfo_t *)L_4) == ((Il2CppObject*)(MethodInfo_t *)L_5))? 1 : 0);
		goto IL_0022;
	}

IL_0021:
	{
		G_B3_0 = 0;
	}

IL_0022:
	{
		return (bool)G_B3_0;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Int32>::.ctor(System.Object,System.Reflection.MethodInfo)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t InvokableCall_1__ctor_m629014264_MetadataUsageId;
extern "C"  void InvokableCall_1__ctor_m629014264_gshared (InvokableCall_1_t3303751057 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_1__ctor_m629014264_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		NullCheck((BaseInvokableCall_t1559630662 *)__this);
		BaseInvokableCall__ctor_m1692884045((BaseInvokableCall_t1559630662 *)__this, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/NULL);
		UnityAction_1_t1048018503 * L_2 = (UnityAction_1_t1048018503 *)__this->get_Delegate_0();
		MethodInfo_t * L_3 = ___theFunction1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_4 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		Il2CppObject * L_5 = ___target0;
		Delegate_t3310234105 * L_6 = NetFxCoreExtensions_CreateDelegate_m3408605866(NULL /*static, unused*/, (MethodInfo_t *)L_3, (Type_t *)L_4, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		Delegate_t3310234105 * L_7 = Delegate_Combine_m1842362874(NULL /*static, unused*/, (Delegate_t3310234105 *)L_2, (Delegate_t3310234105 *)((UnityAction_1_t1048018503 *)Castclass(L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), /*hidden argument*/NULL);
		__this->set_Delegate_0(((UnityAction_1_t1048018503 *)Castclass(L_7, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Int32>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
extern "C"  void InvokableCall_1__ctor_m1140427606_gshared (InvokableCall_1_t3303751057 * __this, UnityAction_1_t1048018503 * ___action0, const MethodInfo* method)
{
	{
		NullCheck((BaseInvokableCall_t1559630662 *)__this);
		BaseInvokableCall__ctor_m2839815662((BaseInvokableCall_t1559630662 *)__this, /*hidden argument*/NULL);
		UnityAction_1_t1048018503 * L_0 = (UnityAction_1_t1048018503 *)__this->get_Delegate_0();
		UnityAction_1_t1048018503 * L_1 = ___action0;
		Delegate_t3310234105 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, (Delegate_t3310234105 *)L_0, (Delegate_t3310234105 *)L_1, /*hidden argument*/NULL);
		__this->set_Delegate_0(((UnityAction_1_t1048018503 *)Castclass(L_2, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Int32>::Invoke(System.Object[])
extern Il2CppClass* ArgumentException_t928607144_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3093451866;
extern const uint32_t InvokableCall_1_Invoke_m1232083343_MetadataUsageId;
extern "C"  void InvokableCall_1_Invoke_m1232083343_gshared (InvokableCall_1_t3303751057 * __this, ObjectU5BU5D_t1108656482* ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_1_Invoke_m1232083343_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1108656482* L_0 = ___args0;
		NullCheck(L_0);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))) == ((int32_t)1)))
		{
			goto IL_0014;
		}
	}
	{
		ArgumentException_t928607144 * L_1 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_1, (String_t*)_stringLiteral3093451866, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0014:
	{
		ObjectU5BU5D_t1108656482* L_2 = ___args0;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		int32_t L_3 = 0;
		Il2CppObject * L_4 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		UnityAction_1_t1048018503 * L_5 = (UnityAction_1_t1048018503 *)__this->get_Delegate_0();
		bool L_6 = BaseInvokableCall_AllowInvoke_m2941661095(NULL /*static, unused*/, (Delegate_t3310234105 *)L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_003f;
		}
	}
	{
		UnityAction_1_t1048018503 * L_7 = (UnityAction_1_t1048018503 *)__this->get_Delegate_0();
		ObjectU5BU5D_t1108656482* L_8 = ___args0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 0);
		int32_t L_9 = 0;
		Il2CppObject * L_10 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		NullCheck((UnityAction_1_t1048018503 *)L_7);
		((  void (*) (UnityAction_1_t1048018503 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((UnityAction_1_t1048018503 *)L_7, (int32_t)((*(int32_t*)((int32_t*)UnBox (L_10, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
	}

IL_003f:
	{
		return;
	}
}
// System.Boolean UnityEngine.Events.InvokableCall`1<System.Int32>::Find(System.Object,System.Reflection.MethodInfo)
extern "C"  bool InvokableCall_1_Find_m3482592137_gshared (InvokableCall_1_t3303751057 * __this, Il2CppObject * ___targetObj0, MethodInfo_t * ___method1, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		UnityAction_1_t1048018503 * L_0 = (UnityAction_1_t1048018503 *)__this->get_Delegate_0();
		NullCheck((Delegate_t3310234105 *)L_0);
		Il2CppObject * L_1 = Delegate_get_Target_m2860483769((Delegate_t3310234105 *)L_0, /*hidden argument*/NULL);
		Il2CppObject * L_2 = ___targetObj0;
		if ((!(((Il2CppObject*)(Il2CppObject *)L_1) == ((Il2CppObject*)(Il2CppObject *)L_2))))
		{
			goto IL_0021;
		}
	}
	{
		UnityAction_1_t1048018503 * L_3 = (UnityAction_1_t1048018503 *)__this->get_Delegate_0();
		MethodInfo_t * L_4 = NetFxCoreExtensions_GetMethodInfo_m1628849205(NULL /*static, unused*/, (Delegate_t3310234105 *)L_3, /*hidden argument*/NULL);
		MethodInfo_t * L_5 = ___method1;
		G_B3_0 = ((((Il2CppObject*)(MethodInfo_t *)L_4) == ((Il2CppObject*)(MethodInfo_t *)L_5))? 1 : 0);
		goto IL_0022;
	}

IL_0021:
	{
		G_B3_0 = 0;
	}

IL_0022:
	{
		return (bool)G_B3_0;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Object>::.ctor(System.Object,System.Reflection.MethodInfo)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t InvokableCall_1__ctor_m3494792797_MetadataUsageId;
extern "C"  void InvokableCall_1__ctor_m3494792797_gshared (InvokableCall_1_t2025761632 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_1__ctor_m3494792797_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		NullCheck((BaseInvokableCall_t1559630662 *)__this);
		BaseInvokableCall__ctor_m1692884045((BaseInvokableCall_t1559630662 *)__this, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/NULL);
		UnityAction_1_t4064996374 * L_2 = (UnityAction_1_t4064996374 *)__this->get_Delegate_0();
		MethodInfo_t * L_3 = ___theFunction1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_4 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		Il2CppObject * L_5 = ___target0;
		Delegate_t3310234105 * L_6 = NetFxCoreExtensions_CreateDelegate_m3408605866(NULL /*static, unused*/, (MethodInfo_t *)L_3, (Type_t *)L_4, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		Delegate_t3310234105 * L_7 = Delegate_Combine_m1842362874(NULL /*static, unused*/, (Delegate_t3310234105 *)L_2, (Delegate_t3310234105 *)((UnityAction_1_t4064996374 *)Castclass(L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), /*hidden argument*/NULL);
		__this->set_Delegate_0(((UnityAction_1_t4064996374 *)Castclass(L_7, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Object>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
extern "C"  void InvokableCall_1__ctor_m2810088059_gshared (InvokableCall_1_t2025761632 * __this, UnityAction_1_t4064996374 * ___action0, const MethodInfo* method)
{
	{
		NullCheck((BaseInvokableCall_t1559630662 *)__this);
		BaseInvokableCall__ctor_m2839815662((BaseInvokableCall_t1559630662 *)__this, /*hidden argument*/NULL);
		UnityAction_1_t4064996374 * L_0 = (UnityAction_1_t4064996374 *)__this->get_Delegate_0();
		UnityAction_1_t4064996374 * L_1 = ___action0;
		Delegate_t3310234105 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, (Delegate_t3310234105 *)L_0, (Delegate_t3310234105 *)L_1, /*hidden argument*/NULL);
		__this->set_Delegate_0(((UnityAction_1_t4064996374 *)Castclass(L_2, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Object>::Invoke(System.Object[])
extern Il2CppClass* ArgumentException_t928607144_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3093451866;
extern const uint32_t InvokableCall_1_Invoke_m689855796_MetadataUsageId;
extern "C"  void InvokableCall_1_Invoke_m689855796_gshared (InvokableCall_1_t2025761632 * __this, ObjectU5BU5D_t1108656482* ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_1_Invoke_m689855796_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1108656482* L_0 = ___args0;
		NullCheck(L_0);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))) == ((int32_t)1)))
		{
			goto IL_0014;
		}
	}
	{
		ArgumentException_t928607144 * L_1 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_1, (String_t*)_stringLiteral3093451866, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0014:
	{
		ObjectU5BU5D_t1108656482* L_2 = ___args0;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		int32_t L_3 = 0;
		Il2CppObject * L_4 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		UnityAction_1_t4064996374 * L_5 = (UnityAction_1_t4064996374 *)__this->get_Delegate_0();
		bool L_6 = BaseInvokableCall_AllowInvoke_m2941661095(NULL /*static, unused*/, (Delegate_t3310234105 *)L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_003f;
		}
	}
	{
		UnityAction_1_t4064996374 * L_7 = (UnityAction_1_t4064996374 *)__this->get_Delegate_0();
		ObjectU5BU5D_t1108656482* L_8 = ___args0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 0);
		int32_t L_9 = 0;
		Il2CppObject * L_10 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		NullCheck((UnityAction_1_t4064996374 *)L_7);
		((  void (*) (UnityAction_1_t4064996374 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((UnityAction_1_t4064996374 *)L_7, (Il2CppObject *)((Il2CppObject *)Castclass(L_10, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
	}

IL_003f:
	{
		return;
	}
}
// System.Boolean UnityEngine.Events.InvokableCall`1<System.Object>::Find(System.Object,System.Reflection.MethodInfo)
extern "C"  bool InvokableCall_1_Find_m1349477752_gshared (InvokableCall_1_t2025761632 * __this, Il2CppObject * ___targetObj0, MethodInfo_t * ___method1, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		UnityAction_1_t4064996374 * L_0 = (UnityAction_1_t4064996374 *)__this->get_Delegate_0();
		NullCheck((Delegate_t3310234105 *)L_0);
		Il2CppObject * L_1 = Delegate_get_Target_m2860483769((Delegate_t3310234105 *)L_0, /*hidden argument*/NULL);
		Il2CppObject * L_2 = ___targetObj0;
		if ((!(((Il2CppObject*)(Il2CppObject *)L_1) == ((Il2CppObject*)(Il2CppObject *)L_2))))
		{
			goto IL_0021;
		}
	}
	{
		UnityAction_1_t4064996374 * L_3 = (UnityAction_1_t4064996374 *)__this->get_Delegate_0();
		MethodInfo_t * L_4 = NetFxCoreExtensions_GetMethodInfo_m1628849205(NULL /*static, unused*/, (Delegate_t3310234105 *)L_3, /*hidden argument*/NULL);
		MethodInfo_t * L_5 = ___method1;
		G_B3_0 = ((((Il2CppObject*)(MethodInfo_t *)L_4) == ((Il2CppObject*)(MethodInfo_t *)L_5))? 1 : 0);
		goto IL_0022;
	}

IL_0021:
	{
		G_B3_0 = 0;
	}

IL_0022:
	{
		return (bool)G_B3_0;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Single>::.ctor(System.Object,System.Reflection.MethodInfo)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t InvokableCall_1__ctor_m119008486_MetadataUsageId;
extern "C"  void InvokableCall_1__ctor_m119008486_gshared (InvokableCall_1_t2146864233 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_1__ctor_m119008486_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		NullCheck((BaseInvokableCall_t1559630662 *)__this);
		BaseInvokableCall__ctor_m1692884045((BaseInvokableCall_t1559630662 *)__this, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/NULL);
		UnityAction_1_t4186098975 * L_2 = (UnityAction_1_t4186098975 *)__this->get_Delegate_0();
		MethodInfo_t * L_3 = ___theFunction1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_4 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		Il2CppObject * L_5 = ___target0;
		Delegate_t3310234105 * L_6 = NetFxCoreExtensions_CreateDelegate_m3408605866(NULL /*static, unused*/, (MethodInfo_t *)L_3, (Type_t *)L_4, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		Delegate_t3310234105 * L_7 = Delegate_Combine_m1842362874(NULL /*static, unused*/, (Delegate_t3310234105 *)L_2, (Delegate_t3310234105 *)((UnityAction_1_t4186098975 *)Castclass(L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), /*hidden argument*/NULL);
		__this->set_Delegate_0(((UnityAction_1_t4186098975 *)Castclass(L_7, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Single>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
extern "C"  void InvokableCall_1__ctor_m593494980_gshared (InvokableCall_1_t2146864233 * __this, UnityAction_1_t4186098975 * ___action0, const MethodInfo* method)
{
	{
		NullCheck((BaseInvokableCall_t1559630662 *)__this);
		BaseInvokableCall__ctor_m2839815662((BaseInvokableCall_t1559630662 *)__this, /*hidden argument*/NULL);
		UnityAction_1_t4186098975 * L_0 = (UnityAction_1_t4186098975 *)__this->get_Delegate_0();
		UnityAction_1_t4186098975 * L_1 = ___action0;
		Delegate_t3310234105 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, (Delegate_t3310234105 *)L_0, (Delegate_t3310234105 *)L_1, /*hidden argument*/NULL);
		__this->set_Delegate_0(((UnityAction_1_t4186098975 *)Castclass(L_2, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Single>::Invoke(System.Object[])
extern Il2CppClass* ArgumentException_t928607144_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3093451866;
extern const uint32_t InvokableCall_1_Invoke_m1294505213_MetadataUsageId;
extern "C"  void InvokableCall_1_Invoke_m1294505213_gshared (InvokableCall_1_t2146864233 * __this, ObjectU5BU5D_t1108656482* ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_1_Invoke_m1294505213_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1108656482* L_0 = ___args0;
		NullCheck(L_0);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))) == ((int32_t)1)))
		{
			goto IL_0014;
		}
	}
	{
		ArgumentException_t928607144 * L_1 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_1, (String_t*)_stringLiteral3093451866, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0014:
	{
		ObjectU5BU5D_t1108656482* L_2 = ___args0;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		int32_t L_3 = 0;
		Il2CppObject * L_4 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		UnityAction_1_t4186098975 * L_5 = (UnityAction_1_t4186098975 *)__this->get_Delegate_0();
		bool L_6 = BaseInvokableCall_AllowInvoke_m2941661095(NULL /*static, unused*/, (Delegate_t3310234105 *)L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_003f;
		}
	}
	{
		UnityAction_1_t4186098975 * L_7 = (UnityAction_1_t4186098975 *)__this->get_Delegate_0();
		ObjectU5BU5D_t1108656482* L_8 = ___args0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 0);
		int32_t L_9 = 0;
		Il2CppObject * L_10 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		NullCheck((UnityAction_1_t4186098975 *)L_7);
		((  void (*) (UnityAction_1_t4186098975 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((UnityAction_1_t4186098975 *)L_7, (float)((*(float*)((float*)UnBox (L_10, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
	}

IL_003f:
	{
		return;
	}
}
// System.Boolean UnityEngine.Events.InvokableCall`1<System.Single>::Find(System.Object,System.Reflection.MethodInfo)
extern "C"  bool InvokableCall_1_Find_m270750159_gshared (InvokableCall_1_t2146864233 * __this, Il2CppObject * ___targetObj0, MethodInfo_t * ___method1, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		UnityAction_1_t4186098975 * L_0 = (UnityAction_1_t4186098975 *)__this->get_Delegate_0();
		NullCheck((Delegate_t3310234105 *)L_0);
		Il2CppObject * L_1 = Delegate_get_Target_m2860483769((Delegate_t3310234105 *)L_0, /*hidden argument*/NULL);
		Il2CppObject * L_2 = ___targetObj0;
		if ((!(((Il2CppObject*)(Il2CppObject *)L_1) == ((Il2CppObject*)(Il2CppObject *)L_2))))
		{
			goto IL_0021;
		}
	}
	{
		UnityAction_1_t4186098975 * L_3 = (UnityAction_1_t4186098975 *)__this->get_Delegate_0();
		MethodInfo_t * L_4 = NetFxCoreExtensions_GetMethodInfo_m1628849205(NULL /*static, unused*/, (Delegate_t3310234105 *)L_3, /*hidden argument*/NULL);
		MethodInfo_t * L_5 = ___method1;
		G_B3_0 = ((((Il2CppObject*)(MethodInfo_t *)L_4) == ((Il2CppObject*)(MethodInfo_t *)L_5))? 1 : 0);
		goto IL_0022;
	}

IL_0021:
	{
		G_B3_0 = 0;
	}

IL_0022:
	{
		return (bool)G_B3_0;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Color>::.ctor(System.Object,System.Reflection.MethodInfo)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t InvokableCall_1__ctor_m2623598219_MetadataUsageId;
extern "C"  void InvokableCall_1__ctor_m2623598219_gshared (InvokableCall_1_t2049492166 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_1__ctor_m2623598219_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		NullCheck((BaseInvokableCall_t1559630662 *)__this);
		BaseInvokableCall__ctor_m1692884045((BaseInvokableCall_t1559630662 *)__this, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/NULL);
		UnityAction_1_t4088726908 * L_2 = (UnityAction_1_t4088726908 *)__this->get_Delegate_0();
		MethodInfo_t * L_3 = ___theFunction1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_4 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		Il2CppObject * L_5 = ___target0;
		Delegate_t3310234105 * L_6 = NetFxCoreExtensions_CreateDelegate_m3408605866(NULL /*static, unused*/, (MethodInfo_t *)L_3, (Type_t *)L_4, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		Delegate_t3310234105 * L_7 = Delegate_Combine_m1842362874(NULL /*static, unused*/, (Delegate_t3310234105 *)L_2, (Delegate_t3310234105 *)((UnityAction_1_t4088726908 *)Castclass(L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), /*hidden argument*/NULL);
		__this->set_Delegate_0(((UnityAction_1_t4088726908 *)Castclass(L_7, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Color>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
extern "C"  void InvokableCall_1__ctor_m492856617_gshared (InvokableCall_1_t2049492166 * __this, UnityAction_1_t4088726908 * ___action0, const MethodInfo* method)
{
	{
		NullCheck((BaseInvokableCall_t1559630662 *)__this);
		BaseInvokableCall__ctor_m2839815662((BaseInvokableCall_t1559630662 *)__this, /*hidden argument*/NULL);
		UnityAction_1_t4088726908 * L_0 = (UnityAction_1_t4088726908 *)__this->get_Delegate_0();
		UnityAction_1_t4088726908 * L_1 = ___action0;
		Delegate_t3310234105 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, (Delegate_t3310234105 *)L_0, (Delegate_t3310234105 *)L_1, /*hidden argument*/NULL);
		__this->set_Delegate_0(((UnityAction_1_t4088726908 *)Castclass(L_2, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Color>::Invoke(System.Object[])
extern Il2CppClass* ArgumentException_t928607144_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3093451866;
extern const uint32_t InvokableCall_1_Invoke_m2460605154_MetadataUsageId;
extern "C"  void InvokableCall_1_Invoke_m2460605154_gshared (InvokableCall_1_t2049492166 * __this, ObjectU5BU5D_t1108656482* ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_1_Invoke_m2460605154_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1108656482* L_0 = ___args0;
		NullCheck(L_0);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))) == ((int32_t)1)))
		{
			goto IL_0014;
		}
	}
	{
		ArgumentException_t928607144 * L_1 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_1, (String_t*)_stringLiteral3093451866, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0014:
	{
		ObjectU5BU5D_t1108656482* L_2 = ___args0;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		int32_t L_3 = 0;
		Il2CppObject * L_4 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		UnityAction_1_t4088726908 * L_5 = (UnityAction_1_t4088726908 *)__this->get_Delegate_0();
		bool L_6 = BaseInvokableCall_AllowInvoke_m2941661095(NULL /*static, unused*/, (Delegate_t3310234105 *)L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_003f;
		}
	}
	{
		UnityAction_1_t4088726908 * L_7 = (UnityAction_1_t4088726908 *)__this->get_Delegate_0();
		ObjectU5BU5D_t1108656482* L_8 = ___args0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 0);
		int32_t L_9 = 0;
		Il2CppObject * L_10 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		NullCheck((UnityAction_1_t4088726908 *)L_7);
		((  void (*) (UnityAction_1_t4088726908 *, Color_t4194546905 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((UnityAction_1_t4088726908 *)L_7, (Color_t4194546905 )((*(Color_t4194546905 *)((Color_t4194546905 *)UnBox (L_10, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
	}

IL_003f:
	{
		return;
	}
}
// System.Boolean UnityEngine.Events.InvokableCall`1<UnityEngine.Color>::Find(System.Object,System.Reflection.MethodInfo)
extern "C"  bool InvokableCall_1_Find_m3902655114_gshared (InvokableCall_1_t2049492166 * __this, Il2CppObject * ___targetObj0, MethodInfo_t * ___method1, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		UnityAction_1_t4088726908 * L_0 = (UnityAction_1_t4088726908 *)__this->get_Delegate_0();
		NullCheck((Delegate_t3310234105 *)L_0);
		Il2CppObject * L_1 = Delegate_get_Target_m2860483769((Delegate_t3310234105 *)L_0, /*hidden argument*/NULL);
		Il2CppObject * L_2 = ___targetObj0;
		if ((!(((Il2CppObject*)(Il2CppObject *)L_1) == ((Il2CppObject*)(Il2CppObject *)L_2))))
		{
			goto IL_0021;
		}
	}
	{
		UnityAction_1_t4088726908 * L_3 = (UnityAction_1_t4088726908 *)__this->get_Delegate_0();
		MethodInfo_t * L_4 = NetFxCoreExtensions_GetMethodInfo_m1628849205(NULL /*static, unused*/, (Delegate_t3310234105 *)L_3, /*hidden argument*/NULL);
		MethodInfo_t * L_5 = ___method1;
		G_B3_0 = ((((Il2CppObject*)(MethodInfo_t *)L_4) == ((Il2CppObject*)(MethodInfo_t *)L_5))? 1 : 0);
		goto IL_0022;
	}

IL_0021:
	{
		G_B3_0 = 0;
	}

IL_0022:
	{
		return (bool)G_B3_0;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Vector2>::.ctor(System.Object,System.Reflection.MethodInfo)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t InvokableCall_1__ctor_m416642935_MetadataUsageId;
extern "C"  void InvokableCall_1__ctor_m416642935_gshared (InvokableCall_1_t2137011826 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_1__ctor_m416642935_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		NullCheck((BaseInvokableCall_t1559630662 *)__this);
		BaseInvokableCall__ctor_m1692884045((BaseInvokableCall_t1559630662 *)__this, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/NULL);
		UnityAction_1_t4176246568 * L_2 = (UnityAction_1_t4176246568 *)__this->get_Delegate_0();
		MethodInfo_t * L_3 = ___theFunction1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_4 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		Il2CppObject * L_5 = ___target0;
		Delegate_t3310234105 * L_6 = NetFxCoreExtensions_CreateDelegate_m3408605866(NULL /*static, unused*/, (MethodInfo_t *)L_3, (Type_t *)L_4, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		Delegate_t3310234105 * L_7 = Delegate_Combine_m1842362874(NULL /*static, unused*/, (Delegate_t3310234105 *)L_2, (Delegate_t3310234105 *)((UnityAction_1_t4176246568 *)Castclass(L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), /*hidden argument*/NULL);
		__this->set_Delegate_0(((UnityAction_1_t4176246568 *)Castclass(L_7, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Vector2>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
extern "C"  void InvokableCall_1__ctor_m175705877_gshared (InvokableCall_1_t2137011826 * __this, UnityAction_1_t4176246568 * ___action0, const MethodInfo* method)
{
	{
		NullCheck((BaseInvokableCall_t1559630662 *)__this);
		BaseInvokableCall__ctor_m2839815662((BaseInvokableCall_t1559630662 *)__this, /*hidden argument*/NULL);
		UnityAction_1_t4176246568 * L_0 = (UnityAction_1_t4176246568 *)__this->get_Delegate_0();
		UnityAction_1_t4176246568 * L_1 = ___action0;
		Delegate_t3310234105 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, (Delegate_t3310234105 *)L_0, (Delegate_t3310234105 *)L_1, /*hidden argument*/NULL);
		__this->set_Delegate_0(((UnityAction_1_t4176246568 *)Castclass(L_2, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Vector2>::Invoke(System.Object[])
extern Il2CppClass* ArgumentException_t928607144_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3093451866;
extern const uint32_t InvokableCall_1_Invoke_m638694094_MetadataUsageId;
extern "C"  void InvokableCall_1_Invoke_m638694094_gshared (InvokableCall_1_t2137011826 * __this, ObjectU5BU5D_t1108656482* ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_1_Invoke_m638694094_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1108656482* L_0 = ___args0;
		NullCheck(L_0);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))) == ((int32_t)1)))
		{
			goto IL_0014;
		}
	}
	{
		ArgumentException_t928607144 * L_1 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_1, (String_t*)_stringLiteral3093451866, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0014:
	{
		ObjectU5BU5D_t1108656482* L_2 = ___args0;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		int32_t L_3 = 0;
		Il2CppObject * L_4 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		UnityAction_1_t4176246568 * L_5 = (UnityAction_1_t4176246568 *)__this->get_Delegate_0();
		bool L_6 = BaseInvokableCall_AllowInvoke_m2941661095(NULL /*static, unused*/, (Delegate_t3310234105 *)L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_003f;
		}
	}
	{
		UnityAction_1_t4176246568 * L_7 = (UnityAction_1_t4176246568 *)__this->get_Delegate_0();
		ObjectU5BU5D_t1108656482* L_8 = ___args0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 0);
		int32_t L_9 = 0;
		Il2CppObject * L_10 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		NullCheck((UnityAction_1_t4176246568 *)L_7);
		((  void (*) (UnityAction_1_t4176246568 *, Vector2_t4282066565 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((UnityAction_1_t4176246568 *)L_7, (Vector2_t4282066565 )((*(Vector2_t4282066565 *)((Vector2_t4282066565 *)UnBox (L_10, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
	}

IL_003f:
	{
		return;
	}
}
// System.Boolean UnityEngine.Events.InvokableCall`1<UnityEngine.Vector2>::Find(System.Object,System.Reflection.MethodInfo)
extern "C"  bool InvokableCall_1_Find_m2276410782_gshared (InvokableCall_1_t2137011826 * __this, Il2CppObject * ___targetObj0, MethodInfo_t * ___method1, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		UnityAction_1_t4176246568 * L_0 = (UnityAction_1_t4176246568 *)__this->get_Delegate_0();
		NullCheck((Delegate_t3310234105 *)L_0);
		Il2CppObject * L_1 = Delegate_get_Target_m2860483769((Delegate_t3310234105 *)L_0, /*hidden argument*/NULL);
		Il2CppObject * L_2 = ___targetObj0;
		if ((!(((Il2CppObject*)(Il2CppObject *)L_1) == ((Il2CppObject*)(Il2CppObject *)L_2))))
		{
			goto IL_0021;
		}
	}
	{
		UnityAction_1_t4176246568 * L_3 = (UnityAction_1_t4176246568 *)__this->get_Delegate_0();
		MethodInfo_t * L_4 = NetFxCoreExtensions_GetMethodInfo_m1628849205(NULL /*static, unused*/, (Delegate_t3310234105 *)L_3, /*hidden argument*/NULL);
		MethodInfo_t * L_5 = ___method1;
		G_B3_0 = ((((Il2CppObject*)(MethodInfo_t *)L_4) == ((Il2CppObject*)(MethodInfo_t *)L_5))? 1 : 0);
		goto IL_0022;
	}

IL_0021:
	{
		G_B3_0 = 0;
	}

IL_0022:
	{
		return (bool)G_B3_0;
	}
}
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,System.Object>::.ctor(System.Object,System.Reflection.MethodInfo)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t InvokableCall_2__ctor_m3928141520_MetadataUsageId;
extern "C"  void InvokableCall_2__ctor_m3928141520_gshared (InvokableCall_2_t3350606262 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_2__ctor_m3928141520_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		NullCheck((BaseInvokableCall_t1559630662 *)__this);
		BaseInvokableCall__ctor_m1692884045((BaseInvokableCall_t1559630662 *)__this, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/NULL);
		MethodInfo_t * L_2 = ___theFunction1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		Il2CppObject * L_4 = ___target0;
		Delegate_t3310234105 * L_5 = NetFxCoreExtensions_CreateDelegate_m3408605866(NULL /*static, unused*/, (MethodInfo_t *)L_2, (Type_t *)L_3, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		__this->set_Delegate_0(((UnityAction_2_t2640361832 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,System.Object>::Invoke(System.Object[])
extern Il2CppClass* ArgumentException_t928607144_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3093451866;
extern const uint32_t InvokableCall_2_Invoke_m1749346151_MetadataUsageId;
extern "C"  void InvokableCall_2_Invoke_m1749346151_gshared (InvokableCall_2_t3350606262 * __this, ObjectU5BU5D_t1108656482* ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_2_Invoke_m1749346151_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1108656482* L_0 = ___args0;
		NullCheck(L_0);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))) == ((int32_t)2)))
		{
			goto IL_0014;
		}
	}
	{
		ArgumentException_t928607144 * L_1 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_1, (String_t*)_stringLiteral3093451866, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0014:
	{
		ObjectU5BU5D_t1108656482* L_2 = ___args0;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		int32_t L_3 = 0;
		Il2CppObject * L_4 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		ObjectU5BU5D_t1108656482* L_5 = ___args0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 1);
		int32_t L_6 = 1;
		Il2CppObject * L_7 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		UnityAction_2_t2640361832 * L_8 = (UnityAction_2_t2640361832 *)__this->get_Delegate_0();
		bool L_9 = BaseInvokableCall_AllowInvoke_m2941661095(NULL /*static, unused*/, (Delegate_t3310234105 *)L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_004f;
		}
	}
	{
		UnityAction_2_t2640361832 * L_10 = (UnityAction_2_t2640361832 *)__this->get_Delegate_0();
		ObjectU5BU5D_t1108656482* L_11 = ___args0;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 0);
		int32_t L_12 = 0;
		Il2CppObject * L_13 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		ObjectU5BU5D_t1108656482* L_14 = ___args0;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 1);
		int32_t L_15 = 1;
		Il2CppObject * L_16 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		NullCheck((UnityAction_2_t2640361832 *)L_10);
		((  void (*) (UnityAction_2_t2640361832 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((UnityAction_2_t2640361832 *)L_10, (Il2CppObject *)((Il2CppObject *)Castclass(L_13, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4))), (Il2CppObject *)((Il2CppObject *)Castclass(L_16, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
	}

IL_004f:
	{
		return;
	}
}
// System.Boolean UnityEngine.Events.InvokableCall`2<System.Object,System.Object>::Find(System.Object,System.Reflection.MethodInfo)
extern "C"  bool InvokableCall_2_Find_m3489973541_gshared (InvokableCall_2_t3350606262 * __this, Il2CppObject * ___targetObj0, MethodInfo_t * ___method1, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		UnityAction_2_t2640361832 * L_0 = (UnityAction_2_t2640361832 *)__this->get_Delegate_0();
		NullCheck((Delegate_t3310234105 *)L_0);
		Il2CppObject * L_1 = Delegate_get_Target_m2860483769((Delegate_t3310234105 *)L_0, /*hidden argument*/NULL);
		Il2CppObject * L_2 = ___targetObj0;
		if ((!(((Il2CppObject*)(Il2CppObject *)L_1) == ((Il2CppObject*)(Il2CppObject *)L_2))))
		{
			goto IL_0021;
		}
	}
	{
		UnityAction_2_t2640361832 * L_3 = (UnityAction_2_t2640361832 *)__this->get_Delegate_0();
		MethodInfo_t * L_4 = NetFxCoreExtensions_GetMethodInfo_m1628849205(NULL /*static, unused*/, (Delegate_t3310234105 *)L_3, /*hidden argument*/NULL);
		MethodInfo_t * L_5 = ___method1;
		G_B3_0 = ((((Il2CppObject*)(MethodInfo_t *)L_4) == ((Il2CppObject*)(MethodInfo_t *)L_5))? 1 : 0);
		goto IL_0022;
	}

IL_0021:
	{
		G_B3_0 = 0;
	}

IL_0022:
	{
		return (bool)G_B3_0;
	}
}
// System.Void UnityEngine.Events.InvokableCall`3<System.Object,System.Object,System.Object>::.ctor(System.Object,System.Reflection.MethodInfo)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t InvokableCall_3__ctor_m774681667_MetadataUsageId;
extern "C"  void InvokableCall_3__ctor_m774681667_gshared (InvokableCall_3_t3637709590 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_3__ctor_m774681667_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		NullCheck((BaseInvokableCall_t1559630662 *)__this);
		BaseInvokableCall__ctor_m1692884045((BaseInvokableCall_t1559630662 *)__this, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/NULL);
		MethodInfo_t * L_2 = ___theFunction1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		Il2CppObject * L_4 = ___target0;
		Delegate_t3310234105 * L_5 = NetFxCoreExtensions_CreateDelegate_m3408605866(NULL /*static, unused*/, (MethodInfo_t *)L_2, (Type_t *)L_3, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		__this->set_Delegate_0(((UnityAction_3_t2271724636 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`3<System.Object,System.Object,System.Object>::Invoke(System.Object[])
extern Il2CppClass* ArgumentException_t928607144_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3093451866;
extern const uint32_t InvokableCall_3_Invoke_m585455258_MetadataUsageId;
extern "C"  void InvokableCall_3_Invoke_m585455258_gshared (InvokableCall_3_t3637709590 * __this, ObjectU5BU5D_t1108656482* ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_3_Invoke_m585455258_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1108656482* L_0 = ___args0;
		NullCheck(L_0);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))) == ((int32_t)3)))
		{
			goto IL_0014;
		}
	}
	{
		ArgumentException_t928607144 * L_1 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_1, (String_t*)_stringLiteral3093451866, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0014:
	{
		ObjectU5BU5D_t1108656482* L_2 = ___args0;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		int32_t L_3 = 0;
		Il2CppObject * L_4 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		ObjectU5BU5D_t1108656482* L_5 = ___args0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 1);
		int32_t L_6 = 1;
		Il2CppObject * L_7 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		ObjectU5BU5D_t1108656482* L_8 = ___args0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 2);
		int32_t L_9 = 2;
		Il2CppObject * L_10 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		UnityAction_3_t2271724636 * L_11 = (UnityAction_3_t2271724636 *)__this->get_Delegate_0();
		bool L_12 = BaseInvokableCall_AllowInvoke_m2941661095(NULL /*static, unused*/, (Delegate_t3310234105 *)L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_005f;
		}
	}
	{
		UnityAction_3_t2271724636 * L_13 = (UnityAction_3_t2271724636 *)__this->get_Delegate_0();
		ObjectU5BU5D_t1108656482* L_14 = ___args0;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 0);
		int32_t L_15 = 0;
		Il2CppObject * L_16 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		ObjectU5BU5D_t1108656482* L_17 = ___args0;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, 1);
		int32_t L_18 = 1;
		Il2CppObject * L_19 = (L_17)->GetAt(static_cast<il2cpp_array_size_t>(L_18));
		ObjectU5BU5D_t1108656482* L_20 = ___args0;
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, 2);
		int32_t L_21 = 2;
		Il2CppObject * L_22 = (L_20)->GetAt(static_cast<il2cpp_array_size_t>(L_21));
		NullCheck((UnityAction_3_t2271724636 *)L_13);
		((  void (*) (UnityAction_3_t2271724636 *, Il2CppObject *, Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((UnityAction_3_t2271724636 *)L_13, (Il2CppObject *)((Il2CppObject *)Castclass(L_16, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5))), (Il2CppObject *)((Il2CppObject *)Castclass(L_19, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))), (Il2CppObject *)((Il2CppObject *)Castclass(L_22, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
	}

IL_005f:
	{
		return;
	}
}
// System.Boolean UnityEngine.Events.InvokableCall`3<System.Object,System.Object,System.Object>::Find(System.Object,System.Reflection.MethodInfo)
extern "C"  bool InvokableCall_3_Find_m4267409362_gshared (InvokableCall_3_t3637709590 * __this, Il2CppObject * ___targetObj0, MethodInfo_t * ___method1, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		UnityAction_3_t2271724636 * L_0 = (UnityAction_3_t2271724636 *)__this->get_Delegate_0();
		NullCheck((Delegate_t3310234105 *)L_0);
		Il2CppObject * L_1 = Delegate_get_Target_m2860483769((Delegate_t3310234105 *)L_0, /*hidden argument*/NULL);
		Il2CppObject * L_2 = ___targetObj0;
		if ((!(((Il2CppObject*)(Il2CppObject *)L_1) == ((Il2CppObject*)(Il2CppObject *)L_2))))
		{
			goto IL_0021;
		}
	}
	{
		UnityAction_3_t2271724636 * L_3 = (UnityAction_3_t2271724636 *)__this->get_Delegate_0();
		MethodInfo_t * L_4 = NetFxCoreExtensions_GetMethodInfo_m1628849205(NULL /*static, unused*/, (Delegate_t3310234105 *)L_3, /*hidden argument*/NULL);
		MethodInfo_t * L_5 = ___method1;
		G_B3_0 = ((((Il2CppObject*)(MethodInfo_t *)L_4) == ((Il2CppObject*)(MethodInfo_t *)L_5))? 1 : 0);
		goto IL_0022;
	}

IL_0021:
	{
		G_B3_0 = 0;
	}

IL_0022:
	{
		return (bool)G_B3_0;
	}
}
// System.Void UnityEngine.Events.InvokableCall`4<System.Object,System.Object,System.Object,System.Object>::.ctor(System.Object,System.Reflection.MethodInfo)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t InvokableCall_4__ctor_m4162891446_MetadataUsageId;
extern "C"  void InvokableCall_4__ctor_m4162891446_gshared (InvokableCall_4_t2281252720 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_4__ctor_m4162891446_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		NullCheck((BaseInvokableCall_t1559630662 *)__this);
		BaseInvokableCall__ctor_m1692884045((BaseInvokableCall_t1559630662 *)__this, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/NULL);
		MethodInfo_t * L_2 = ___theFunction1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		Il2CppObject * L_4 = ___target0;
		Delegate_t3310234105 * L_5 = NetFxCoreExtensions_CreateDelegate_m3408605866(NULL /*static, unused*/, (MethodInfo_t *)L_2, (Type_t *)L_3, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		__this->set_Delegate_0(((UnityAction_4_t2496524882 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`4<System.Object,System.Object,System.Object,System.Object>::Invoke(System.Object[])
extern Il2CppClass* ArgumentException_t928607144_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3093451866;
extern const uint32_t InvokableCall_4_Invoke_m2239232717_MetadataUsageId;
extern "C"  void InvokableCall_4_Invoke_m2239232717_gshared (InvokableCall_4_t2281252720 * __this, ObjectU5BU5D_t1108656482* ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_4_Invoke_m2239232717_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1108656482* L_0 = ___args0;
		NullCheck(L_0);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))) == ((int32_t)4)))
		{
			goto IL_0014;
		}
	}
	{
		ArgumentException_t928607144 * L_1 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_1, (String_t*)_stringLiteral3093451866, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0014:
	{
		ObjectU5BU5D_t1108656482* L_2 = ___args0;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		int32_t L_3 = 0;
		Il2CppObject * L_4 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		ObjectU5BU5D_t1108656482* L_5 = ___args0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 1);
		int32_t L_6 = 1;
		Il2CppObject * L_7 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		ObjectU5BU5D_t1108656482* L_8 = ___args0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 2);
		int32_t L_9 = 2;
		Il2CppObject * L_10 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		ObjectU5BU5D_t1108656482* L_11 = ___args0;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 3);
		int32_t L_12 = 3;
		Il2CppObject * L_13 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		UnityAction_4_t2496524882 * L_14 = (UnityAction_4_t2496524882 *)__this->get_Delegate_0();
		bool L_15 = BaseInvokableCall_AllowInvoke_m2941661095(NULL /*static, unused*/, (Delegate_t3310234105 *)L_14, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_006f;
		}
	}
	{
		UnityAction_4_t2496524882 * L_16 = (UnityAction_4_t2496524882 *)__this->get_Delegate_0();
		ObjectU5BU5D_t1108656482* L_17 = ___args0;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, 0);
		int32_t L_18 = 0;
		Il2CppObject * L_19 = (L_17)->GetAt(static_cast<il2cpp_array_size_t>(L_18));
		ObjectU5BU5D_t1108656482* L_20 = ___args0;
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, 1);
		int32_t L_21 = 1;
		Il2CppObject * L_22 = (L_20)->GetAt(static_cast<il2cpp_array_size_t>(L_21));
		ObjectU5BU5D_t1108656482* L_23 = ___args0;
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, 2);
		int32_t L_24 = 2;
		Il2CppObject * L_25 = (L_23)->GetAt(static_cast<il2cpp_array_size_t>(L_24));
		ObjectU5BU5D_t1108656482* L_26 = ___args0;
		NullCheck(L_26);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_26, 3);
		int32_t L_27 = 3;
		Il2CppObject * L_28 = (L_26)->GetAt(static_cast<il2cpp_array_size_t>(L_27));
		NullCheck((UnityAction_4_t2496524882 *)L_16);
		((  void (*) (UnityAction_4_t2496524882 *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((UnityAction_4_t2496524882 *)L_16, (Il2CppObject *)((Il2CppObject *)Castclass(L_19, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))), (Il2CppObject *)((Il2CppObject *)Castclass(L_22, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7))), (Il2CppObject *)((Il2CppObject *)Castclass(L_25, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))), (Il2CppObject *)((Il2CppObject *)Castclass(L_28, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 9))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
	}

IL_006f:
	{
		return;
	}
}
// System.Boolean UnityEngine.Events.InvokableCall`4<System.Object,System.Object,System.Object,System.Object>::Find(System.Object,System.Reflection.MethodInfo)
extern "C"  bool InvokableCall_4_Find_m2015184255_gshared (InvokableCall_4_t2281252720 * __this, Il2CppObject * ___targetObj0, MethodInfo_t * ___method1, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		UnityAction_4_t2496524882 * L_0 = (UnityAction_4_t2496524882 *)__this->get_Delegate_0();
		NullCheck((Delegate_t3310234105 *)L_0);
		Il2CppObject * L_1 = Delegate_get_Target_m2860483769((Delegate_t3310234105 *)L_0, /*hidden argument*/NULL);
		Il2CppObject * L_2 = ___targetObj0;
		if ((!(((Il2CppObject*)(Il2CppObject *)L_1) == ((Il2CppObject*)(Il2CppObject *)L_2))))
		{
			goto IL_0021;
		}
	}
	{
		UnityAction_4_t2496524882 * L_3 = (UnityAction_4_t2496524882 *)__this->get_Delegate_0();
		MethodInfo_t * L_4 = NetFxCoreExtensions_GetMethodInfo_m1628849205(NULL /*static, unused*/, (Delegate_t3310234105 *)L_3, /*hidden argument*/NULL);
		MethodInfo_t * L_5 = ___method1;
		G_B3_0 = ((((Il2CppObject*)(MethodInfo_t *)L_4) == ((Il2CppObject*)(MethodInfo_t *)L_5))? 1 : 0);
		goto IL_0022;
	}

IL_0021:
	{
		G_B3_0 = 0;
	}

IL_0022:
	{
		return (bool)G_B3_0;
	}
}
// System.Void UnityEngine.Events.UnityAction`1<System.Boolean>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction_1__ctor_m1354516801_gshared (UnityAction_1_t370978721 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Events.UnityAction`1<System.Boolean>::Invoke(T0)
extern "C"  void UnityAction_1_Invoke_m2333712627_gshared (UnityAction_1_t370978721 * __this, bool ___arg00, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		UnityAction_1_Invoke_m2333712627((UnityAction_1_t370978721 *)__this->get_prev_9(),___arg00, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, bool ___arg00, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg00,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, bool ___arg00, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg00,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.Events.UnityAction`1<System.Boolean>::BeginInvoke(T0,System.AsyncCallback,System.Object)
extern Il2CppClass* Boolean_t476798718_il2cpp_TypeInfo_var;
extern const uint32_t UnityAction_1_BeginInvoke_m987196326_MetadataUsageId;
extern "C"  Il2CppObject * UnityAction_1_BeginInvoke_m987196326_gshared (UnityAction_1_t370978721 * __this, bool ___arg00, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityAction_1_BeginInvoke_m987196326_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Boolean_t476798718_il2cpp_TypeInfo_var, &___arg00);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void UnityEngine.Events.UnityAction`1<System.Boolean>::EndInvoke(System.IAsyncResult)
extern "C"  void UnityAction_1_EndInvoke_m535265605_gshared (UnityAction_1_t370978721 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Events.UnityAction`1<System.Int32>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction_1__ctor_m3485915663_gshared (UnityAction_1_t1048018503 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Events.UnityAction`1<System.Int32>::Invoke(T0)
extern "C"  void UnityAction_1_Invoke_m698809421_gshared (UnityAction_1_t1048018503 * __this, int32_t ___arg00, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		UnityAction_1_Invoke_m698809421((UnityAction_1_t1048018503 *)__this->get_prev_9(),___arg00, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___arg00, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg00,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___arg00, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg00,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.Events.UnityAction`1<System.Int32>::BeginInvoke(T0,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern const uint32_t UnityAction_1_BeginInvoke_m3753424384_MetadataUsageId;
extern "C"  Il2CppObject * UnityAction_1_BeginInvoke_m3753424384_gshared (UnityAction_1_t1048018503 * __this, int32_t ___arg00, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityAction_1_BeginInvoke_m3753424384_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &___arg00);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void UnityEngine.Events.UnityAction`1<System.Int32>::EndInvoke(System.IAsyncResult)
extern "C"  void UnityAction_1_EndInvoke_m3271535519_gshared (UnityAction_1_t1048018503 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Events.UnityAction`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction_1__ctor_m2698834494_gshared (UnityAction_1_t4064996374 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Events.UnityAction`1<System.Object>::Invoke(T0)
extern "C"  void UnityAction_1_Invoke_m774762876_gshared (UnityAction_1_t4064996374 * __this, Il2CppObject * ___arg00, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		UnityAction_1_Invoke_m774762876((UnityAction_1_t4064996374 *)__this->get_prev_9(),___arg00, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg00, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg00,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg00, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg00,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___arg00,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.Events.UnityAction`1<System.Object>::BeginInvoke(T0,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * UnityAction_1_BeginInvoke_m1225830823_gshared (UnityAction_1_t4064996374 * __this, Il2CppObject * ___arg00, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___arg00;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void UnityEngine.Events.UnityAction`1<System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void UnityAction_1_EndInvoke_m4220465998_gshared (UnityAction_1_t4064996374 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Events.UnityAction`1<System.Single>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction_1__ctor_m480548041_gshared (UnityAction_1_t4186098975 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Events.UnityAction`1<System.Single>::Invoke(T0)
extern "C"  void UnityAction_1_Invoke_m3075983123_gshared (UnityAction_1_t4186098975 * __this, float ___arg00, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		UnityAction_1_Invoke_m3075983123((UnityAction_1_t4186098975 *)__this->get_prev_9(),___arg00, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, float ___arg00, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg00,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, float ___arg00, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg00,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.Events.UnityAction`1<System.Single>::BeginInvoke(T0,System.AsyncCallback,System.Object)
extern Il2CppClass* Single_t4291918972_il2cpp_TypeInfo_var;
extern const uint32_t UnityAction_1_BeginInvoke_m3950699582_MetadataUsageId;
extern "C"  Il2CppObject * UnityAction_1_BeginInvoke_m3950699582_gshared (UnityAction_1_t4186098975 * __this, float ___arg00, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityAction_1_BeginInvoke_m3950699582_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Single_t4291918972_il2cpp_TypeInfo_var, &___arg00);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void UnityEngine.Events.UnityAction`1<System.Single>::EndInvoke(System.IAsyncResult)
extern "C"  void UnityAction_1_EndInvoke_m146102117_gshared (UnityAction_1_t4186098975 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Color>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction_1__ctor_m3352102596_gshared (UnityAction_1_t4088726908 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Color>::Invoke(T0)
extern "C"  void UnityAction_1_Invoke_m3753038862_gshared (UnityAction_1_t4088726908 * __this, Color_t4194546905  ___arg00, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		UnityAction_1_Invoke_m3753038862((UnityAction_1_t4088726908 *)__this->get_prev_9(),___arg00, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Color_t4194546905  ___arg00, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg00,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Color_t4194546905  ___arg00, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg00,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.Events.UnityAction`1<UnityEngine.Color>::BeginInvoke(T0,System.AsyncCallback,System.Object)
extern Il2CppClass* Color_t4194546905_il2cpp_TypeInfo_var;
extern const uint32_t UnityAction_1_BeginInvoke_m3065986105_MetadataUsageId;
extern "C"  Il2CppObject * UnityAction_1_BeginInvoke_m3065986105_gshared (UnityAction_1_t4088726908 * __this, Color_t4194546905  ___arg00, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityAction_1_BeginInvoke_m3065986105_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Color_t4194546905_il2cpp_TypeInfo_var, &___arg00);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Color>::EndInvoke(System.IAsyncResult)
extern "C"  void UnityAction_1_EndInvoke_m4211808480_gshared (UnityAction_1_t4088726908 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Vector2>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction_1__ctor_m3402788068_gshared (UnityAction_1_t4176246568 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Vector2>::Invoke(T0)
extern "C"  void UnityAction_1_Invoke_m2612362786_gshared (UnityAction_1_t4176246568 * __this, Vector2_t4282066565  ___arg00, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		UnityAction_1_Invoke_m2612362786((UnityAction_1_t4176246568 *)__this->get_prev_9(),___arg00, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Vector2_t4282066565  ___arg00, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg00,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Vector2_t4282066565  ___arg00, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg00,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.Events.UnityAction`1<UnityEngine.Vector2>::BeginInvoke(T0,System.AsyncCallback,System.Object)
extern Il2CppClass* Vector2_t4282066565_il2cpp_TypeInfo_var;
extern const uint32_t UnityAction_1_BeginInvoke_m3621024589_MetadataUsageId;
extern "C"  Il2CppObject * UnityAction_1_BeginInvoke_m3621024589_gshared (UnityAction_1_t4176246568 * __this, Vector2_t4282066565  ___arg00, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityAction_1_BeginInvoke_m3621024589_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Vector2_t4282066565_il2cpp_TypeInfo_var, &___arg00);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Vector2>::EndInvoke(System.IAsyncResult)
extern "C"  void UnityAction_1_EndInvoke_m822604020_gshared (UnityAction_1_t4176246568 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Events.UnityAction`2<System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction_2__ctor_m1077712043_gshared (UnityAction_2_t2640361832 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Events.UnityAction`2<System.Object,System.Object>::Invoke(T0,T1)
extern "C"  void UnityAction_2_Invoke_m1848147712_gshared (UnityAction_2_t2640361832 * __this, Il2CppObject * ___arg00, Il2CppObject * ___arg11, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		UnityAction_2_Invoke_m1848147712((UnityAction_2_t2640361832 *)__this->get_prev_9(),___arg00, ___arg11, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg00, Il2CppObject * ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg00, Il2CppObject * ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.Events.UnityAction`2<System.Object,System.Object>::BeginInvoke(T0,T1,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * UnityAction_2_BeginInvoke_m1411513831_gshared (UnityAction_2_t2640361832 * __this, Il2CppObject * ___arg00, Il2CppObject * ___arg11, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___arg00;
	__d_args[1] = ___arg11;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void UnityEngine.Events.UnityAction`2<System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void UnityAction_2_EndInvoke_m2463306811_gshared (UnityAction_2_t2640361832 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Events.UnityAction`3<System.Object,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction_3__ctor_m2630406680_gshared (UnityAction_3_t2271724636 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Events.UnityAction`3<System.Object,System.Object,System.Object>::Invoke(T0,T1,T2)
extern "C"  void UnityAction_3_Invoke_m743272021_gshared (UnityAction_3_t2271724636 * __this, Il2CppObject * ___arg00, Il2CppObject * ___arg11, Il2CppObject * ___arg22, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		UnityAction_3_Invoke_m743272021((UnityAction_3_t2271724636 *)__this->get_prev_9(),___arg00, ___arg11, ___arg22, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg00, Il2CppObject * ___arg11, Il2CppObject * ___arg22, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg00, ___arg11, ___arg22,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg00, Il2CppObject * ___arg11, Il2CppObject * ___arg22, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg00, ___arg11, ___arg22,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg11, Il2CppObject * ___arg22, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___arg00, ___arg11, ___arg22,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.Events.UnityAction`3<System.Object,System.Object,System.Object>::BeginInvoke(T0,T1,T2,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * UnityAction_3_BeginInvoke_m3462825058_gshared (UnityAction_3_t2271724636 * __this, Il2CppObject * ___arg00, Il2CppObject * ___arg11, Il2CppObject * ___arg22, AsyncCallback_t1369114871 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method)
{
	void *__d_args[4] = {0};
	__d_args[0] = ___arg00;
	__d_args[1] = ___arg11;
	__d_args[2] = ___arg22;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback3, (Il2CppObject*)___object4);
}
// System.Void UnityEngine.Events.UnityAction`3<System.Object,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void UnityAction_3_EndInvoke_m2479427624_gshared (UnityAction_3_t2271724636 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Events.UnityAction`4<System.Object,System.Object,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction_4__ctor_m2430556805_gshared (UnityAction_4_t2496524882 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Events.UnityAction`4<System.Object,System.Object,System.Object,System.Object>::Invoke(T0,T1,T2,T3)
extern "C"  void UnityAction_4_Invoke_m295067525_gshared (UnityAction_4_t2496524882 * __this, Il2CppObject * ___arg00, Il2CppObject * ___arg11, Il2CppObject * ___arg22, Il2CppObject * ___arg33, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		UnityAction_4_Invoke_m295067525((UnityAction_4_t2496524882 *)__this->get_prev_9(),___arg00, ___arg11, ___arg22, ___arg33, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg00, Il2CppObject * ___arg11, Il2CppObject * ___arg22, Il2CppObject * ___arg33, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg00, ___arg11, ___arg22, ___arg33,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg00, Il2CppObject * ___arg11, Il2CppObject * ___arg22, Il2CppObject * ___arg33, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg00, ___arg11, ___arg22, ___arg33,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg11, Il2CppObject * ___arg22, Il2CppObject * ___arg33, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___arg00, ___arg11, ___arg22, ___arg33,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.Events.UnityAction`4<System.Object,System.Object,System.Object,System.Object>::BeginInvoke(T0,T1,T2,T3,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * UnityAction_4_BeginInvoke_m3784231950_gshared (UnityAction_4_t2496524882 * __this, Il2CppObject * ___arg00, Il2CppObject * ___arg11, Il2CppObject * ___arg22, Il2CppObject * ___arg33, AsyncCallback_t1369114871 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method)
{
	void *__d_args[5] = {0};
	__d_args[0] = ___arg00;
	__d_args[1] = ___arg11;
	__d_args[2] = ___arg22;
	__d_args[3] = ___arg33;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback4, (Il2CppObject*)___object5);
}
// System.Void UnityEngine.Events.UnityAction`4<System.Object,System.Object,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void UnityAction_4_EndInvoke_m2334578453_gshared (UnityAction_4_t2496524882 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Events.UnityEvent`1<System.Boolean>::.ctor()
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern const uint32_t UnityEvent_1__ctor_m1579102881_MetadataUsageId;
extern "C"  void UnityEvent_1__ctor_m1579102881_gshared (UnityEvent_1_t663396231 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_1__ctor_m1579102881_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_m_InvokeArray_4(((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)1)));
		NullCheck((UnityEventBase_t1020378628 *)__this);
		UnityEventBase__ctor_m199506446((UnityEventBase_t1020378628 *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<System.Boolean>::AddListener(UnityEngine.Events.UnityAction`1<T0>)
extern "C"  void UnityEvent_1_AddListener_m4118700355_gshared (UnityEvent_1_t663396231 * __this, UnityAction_1_t370978721 * ___call0, const MethodInfo* method)
{
	{
		UnityAction_1_t370978721 * L_0 = ___call0;
		BaseInvokableCall_t1559630662 * L_1 = ((  BaseInvokableCall_t1559630662 * (*) (Il2CppObject * /* static, unused */, UnityAction_1_t370978721 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, (UnityAction_1_t370978721 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		NullCheck((UnityEventBase_t1020378628 *)__this);
		UnityEventBase_AddCall_m1246572149((UnityEventBase_t1020378628 *)__this, (BaseInvokableCall_t1559630662 *)L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<System.Boolean>::RemoveListener(UnityEngine.Events.UnityAction`1<T0>)
extern "C"  void UnityEvent_1_RemoveListener_m3796628131_gshared (UnityEvent_1_t663396231 * __this, UnityAction_1_t370978721 * ___call0, const MethodInfo* method)
{
	{
		UnityAction_1_t370978721 * L_0 = ___call0;
		NullCheck((Delegate_t3310234105 *)L_0);
		Il2CppObject * L_1 = Delegate_get_Target_m2860483769((Delegate_t3310234105 *)L_0, /*hidden argument*/NULL);
		UnityAction_1_t370978721 * L_2 = ___call0;
		MethodInfo_t * L_3 = NetFxCoreExtensions_GetMethodInfo_m1628849205(NULL /*static, unused*/, (Delegate_t3310234105 *)L_2, /*hidden argument*/NULL);
		NullCheck((UnityEventBase_t1020378628 *)__this);
		UnityEventBase_RemoveListener_m276725997((UnityEventBase_t1020378628 *)__this, (Il2CppObject *)L_1, (MethodInfo_t *)L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`1<System.Boolean>::FindMethod_Impl(System.String,System.Object)
extern Il2CppClass* TypeU5BU5D_t3339007067_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t UnityEvent_1_FindMethod_Impl_m2991727964_MetadataUsageId;
extern "C"  MethodInfo_t * UnityEvent_1_FindMethod_Impl_m2991727964_gshared (UnityEvent_1_t663396231 * __this, String_t* ___name0, Il2CppObject * ___targetObj1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_1_FindMethod_Impl_m2991727964_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___targetObj1;
		String_t* L_1 = ___name0;
		TypeU5BU5D_t3339007067* L_2 = (TypeU5BU5D_t3339007067*)((TypeU5BU5D_t3339007067*)SZArrayNew(TypeU5BU5D_t3339007067_il2cpp_TypeInfo_var, (uint32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 2)), /*hidden argument*/NULL);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_3);
		MethodInfo_t * L_4 = UnityEventBase_GetValidMethodInfo_m2325301202(NULL /*static, unused*/, (Il2CppObject *)L_0, (String_t*)L_1, (TypeU5BU5D_t3339007067*)L_2, /*hidden argument*/NULL);
		return L_4;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1<System.Boolean>::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern "C"  BaseInvokableCall_t1559630662 * UnityEvent_1_GetDelegate_m183928870_gshared (UnityEvent_1_t663396231 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		InvokableCall_1_t2626711275 * L_2 = (InvokableCall_1_t2626711275 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3));
		((  void (*) (InvokableCall_1_t2626711275 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(L_2, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_2;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1<System.Boolean>::GetDelegate(UnityEngine.Events.UnityAction`1<T0>)
extern "C"  BaseInvokableCall_t1559630662 * UnityEvent_1_GetDelegate_m2888043331_gshared (Il2CppObject * __this /* static, unused */, UnityAction_1_t370978721 * ___action0, const MethodInfo* method)
{
	{
		UnityAction_1_t370978721 * L_0 = ___action0;
		InvokableCall_1_t2626711275 * L_1 = (InvokableCall_1_t2626711275 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (InvokableCall_1_t2626711275 *, UnityAction_1_t370978721 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_1, (UnityAction_1_t370978721 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<System.Boolean>::Invoke(T0)
extern "C"  void UnityEvent_1_Invoke_m4200629676_gshared (UnityEvent_1_t663396231 * __this, bool ___arg00, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t1108656482* L_0 = (ObjectU5BU5D_t1108656482*)__this->get_m_InvokeArray_4();
		bool L_1 = ___arg00;
		bool L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6), &L_2);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		ObjectU5BU5D_t1108656482* L_4 = (ObjectU5BU5D_t1108656482*)__this->get_m_InvokeArray_4();
		NullCheck((UnityEventBase_t1020378628 *)__this);
		UnityEventBase_Invoke_m3681078084((UnityEventBase_t1020378628 *)__this, (ObjectU5BU5D_t1108656482*)L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<System.Int32>::.ctor()
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern const uint32_t UnityEvent_1__ctor_m7858823_MetadataUsageId;
extern "C"  void UnityEvent_1__ctor_m7858823_gshared (UnityEvent_1_t1340436013 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_1__ctor_m7858823_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_m_InvokeArray_4(((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)1)));
		NullCheck((UnityEventBase_t1020378628 *)__this);
		UnityEventBase__ctor_m199506446((UnityEventBase_t1020378628 *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<System.Int32>::AddListener(UnityEngine.Events.UnityAction`1<T0>)
extern "C"  void UnityEvent_1_AddListener_m482909706_gshared (UnityEvent_1_t1340436013 * __this, UnityAction_1_t1048018503 * ___call0, const MethodInfo* method)
{
	{
		UnityAction_1_t1048018503 * L_0 = ___call0;
		BaseInvokableCall_t1559630662 * L_1 = ((  BaseInvokableCall_t1559630662 * (*) (Il2CppObject * /* static, unused */, UnityAction_1_t1048018503 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, (UnityAction_1_t1048018503 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		NullCheck((UnityEventBase_t1020378628 *)__this);
		UnityEventBase_AddCall_m1246572149((UnityEventBase_t1020378628 *)__this, (BaseInvokableCall_t1559630662 *)L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<System.Int32>::RemoveListener(UnityEngine.Events.UnityAction`1<T0>)
extern "C"  void UnityEvent_1_RemoveListener_m356723581_gshared (UnityEvent_1_t1340436013 * __this, UnityAction_1_t1048018503 * ___call0, const MethodInfo* method)
{
	{
		UnityAction_1_t1048018503 * L_0 = ___call0;
		NullCheck((Delegate_t3310234105 *)L_0);
		Il2CppObject * L_1 = Delegate_get_Target_m2860483769((Delegate_t3310234105 *)L_0, /*hidden argument*/NULL);
		UnityAction_1_t1048018503 * L_2 = ___call0;
		MethodInfo_t * L_3 = NetFxCoreExtensions_GetMethodInfo_m1628849205(NULL /*static, unused*/, (Delegate_t3310234105 *)L_2, /*hidden argument*/NULL);
		NullCheck((UnityEventBase_t1020378628 *)__this);
		UnityEventBase_RemoveListener_m276725997((UnityEventBase_t1020378628 *)__this, (Il2CppObject *)L_1, (MethodInfo_t *)L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`1<System.Int32>::FindMethod_Impl(System.String,System.Object)
extern Il2CppClass* TypeU5BU5D_t3339007067_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t UnityEvent_1_FindMethod_Impl_m2189312310_MetadataUsageId;
extern "C"  MethodInfo_t * UnityEvent_1_FindMethod_Impl_m2189312310_gshared (UnityEvent_1_t1340436013 * __this, String_t* ___name0, Il2CppObject * ___targetObj1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_1_FindMethod_Impl_m2189312310_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___targetObj1;
		String_t* L_1 = ___name0;
		TypeU5BU5D_t3339007067* L_2 = (TypeU5BU5D_t3339007067*)((TypeU5BU5D_t3339007067*)SZArrayNew(TypeU5BU5D_t3339007067_il2cpp_TypeInfo_var, (uint32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 2)), /*hidden argument*/NULL);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_3);
		MethodInfo_t * L_4 = UnityEventBase_GetValidMethodInfo_m2325301202(NULL /*static, unused*/, (Il2CppObject *)L_0, (String_t*)L_1, (TypeU5BU5D_t3339007067*)L_2, /*hidden argument*/NULL);
		return L_4;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1<System.Int32>::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern "C"  BaseInvokableCall_t1559630662 * UnityEvent_1_GetDelegate_m3062958924_gshared (UnityEvent_1_t1340436013 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		InvokableCall_1_t3303751057 * L_2 = (InvokableCall_1_t3303751057 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3));
		((  void (*) (InvokableCall_1_t3303751057 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(L_2, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_2;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1<System.Int32>::GetDelegate(UnityEngine.Events.UnityAction`1<T0>)
extern "C"  BaseInvokableCall_t1559630662 * UnityEvent_1_GetDelegate_m2987483881_gshared (Il2CppObject * __this /* static, unused */, UnityAction_1_t1048018503 * ___action0, const MethodInfo* method)
{
	{
		UnityAction_1_t1048018503 * L_0 = ___action0;
		InvokableCall_1_t3303751057 * L_1 = (InvokableCall_1_t3303751057 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (InvokableCall_1_t3303751057 *, UnityAction_1_t1048018503 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_1, (UnityAction_1_t1048018503 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<System.Int32>::Invoke(T0)
extern "C"  void UnityEvent_1_Invoke_m1822489606_gshared (UnityEvent_1_t1340436013 * __this, int32_t ___arg00, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t1108656482* L_0 = (ObjectU5BU5D_t1108656482*)__this->get_m_InvokeArray_4();
		int32_t L_1 = ___arg00;
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6), &L_2);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		ObjectU5BU5D_t1108656482* L_4 = (ObjectU5BU5D_t1108656482*)__this->get_m_InvokeArray_4();
		NullCheck((UnityEventBase_t1020378628 *)__this);
		UnityEventBase_Invoke_m3681078084((UnityEventBase_t1020378628 *)__this, (ObjectU5BU5D_t1108656482*)L_4, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
