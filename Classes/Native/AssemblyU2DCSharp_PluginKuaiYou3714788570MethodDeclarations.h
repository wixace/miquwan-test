﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PluginKuaiYou
struct PluginKuaiYou_t3714788570;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// System.String
struct String_t;
// CEvent.EventFunc`1<CEvent.ZEvent>
struct EventFunc_1_t1114914310;
// VersionInfo
struct VersionInfo_t2356638086;
// VersionMgr
struct VersionMgr_t1322950208;
// PluginsSdkMgr
struct PluginsSdkMgr_t3884624670;
// System.Object
struct Il2CppObject;
// System.Action
struct Action_t3771233898;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_VersionMgr1322950208.h"
#include "AssemblyU2DCSharp_PluginKuaiYou3714788570.h"
#include "AssemblyU2DCSharp_PluginsSdkMgr3884624670.h"
#include "mscorlib_System_Object4170816371.h"
#include "System_Core_System_Action3771233898.h"

// System.Void PluginKuaiYou::.ctor()
extern "C"  void PluginKuaiYou__ctor_m2544568785 (PluginKuaiYou_t3714788570 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginKuaiYou::Init()
extern "C"  void PluginKuaiYou_Init_m2250376803 (PluginKuaiYou_t3714788570 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginKuaiYou::ReqSDKHttpLogin()
extern "C"  Dictionary_2_t827649927 * PluginKuaiYou_ReqSDKHttpLogin_m3610776194 (PluginKuaiYou_t3714788570 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginKuaiYou::IsLoginSuccess(System.Boolean)
extern "C"  bool PluginKuaiYou_IsLoginSuccess_m1327607674 (PluginKuaiYou_t3714788570 * __this, bool ___isOpenUseLogin0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginKuaiYou::OpenUserLogin()
extern "C"  void PluginKuaiYou_OpenUserLogin_m1781123875 (PluginKuaiYou_t3714788570 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginKuaiYou::UserPay(CEvent.ZEvent)
extern "C"  void PluginKuaiYou_UserPay_m494439151 (PluginKuaiYou_t3714788570 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginKuaiYou::CreateRole(CEvent.ZEvent)
extern "C"  void PluginKuaiYou_CreateRole_m234059190 (PluginKuaiYou_t3714788570 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginKuaiYou::EnterGame(CEvent.ZEvent)
extern "C"  void PluginKuaiYou_EnterGame_m631200962 (PluginKuaiYou_t3714788570 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginKuaiYou::RoleUpgrade(CEvent.ZEvent)
extern "C"  void PluginKuaiYou_RoleUpgrade_m1329198822 (PluginKuaiYou_t3714788570 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginKuaiYou::OnLoginSuccess(System.String)
extern "C"  void PluginKuaiYou_OnLoginSuccess_m3001703958 (PluginKuaiYou_t3714788570 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginKuaiYou::OnLogoutSuccess(System.String)
extern "C"  void PluginKuaiYou_OnLogoutSuccess_m2862052153 (PluginKuaiYou_t3714788570 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginKuaiYou::OnLogout(System.String)
extern "C"  void PluginKuaiYou_OnLogout_m4181600806 (PluginKuaiYou_t3714788570 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginKuaiYou::initSdk()
extern "C"  void PluginKuaiYou_initSdk_m3163387417 (PluginKuaiYou_t3714788570 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginKuaiYou::login()
extern "C"  void PluginKuaiYou_login_m2066583608 (PluginKuaiYou_t3714788570 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginKuaiYou::logout()
extern "C"  void PluginKuaiYou_logout_m3940372413 (PluginKuaiYou_t3714788570 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginKuaiYou::updateRoleInfo(System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginKuaiYou_updateRoleInfo_m2660905742 (PluginKuaiYou_t3714788570 * __this, String_t* ___serverId0, String_t* ___serverName1, String_t* ___roleId2, String_t* ___roleName3, String_t* ___roleLv4, String_t* ___VipLevel5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginKuaiYou::pay(System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginKuaiYou_pay_m2608591627 (PluginKuaiYou_t3714788570 * __this, String_t* ___orderId0, String_t* ___productId1, String_t* ___amount2, String_t* ___serverId3, String_t* ___serverName4, String_t* ___roleName5, String_t* ___roleLv6, String_t* ___roleId7, String_t* ___extra8, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginKuaiYou::<OnLogoutSuccess>m__43B()
extern "C"  void PluginKuaiYou_U3COnLogoutSuccessU3Em__43B_m3365125093 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginKuaiYou::<OnLogout>m__43C()
extern "C"  void PluginKuaiYou_U3COnLogoutU3Em__43C_m898720035 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginKuaiYou::ilo_AddEventListener1(System.String,CEvent.EventFunc`1<CEvent.ZEvent>)
extern "C"  void PluginKuaiYou_ilo_AddEventListener1_m1889530115 (Il2CppObject * __this /* static, unused */, String_t* ___eventType0, EventFunc_1_t1114914310 * ___fun1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionInfo PluginKuaiYou::ilo_get_currentVS2(VersionMgr)
extern "C"  VersionInfo_t2356638086 * PluginKuaiYou_ilo_get_currentVS2_m814491582 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginKuaiYou::ilo_get_isSdkLogin3(VersionMgr)
extern "C"  bool PluginKuaiYou_ilo_get_isSdkLogin3_m1745565249 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionMgr PluginKuaiYou::ilo_get_Instance4()
extern "C"  VersionMgr_t1322950208 * PluginKuaiYou_ilo_get_Instance4_m1128296559 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginKuaiYou::ilo_OpenUserLogin5(PluginKuaiYou)
extern "C"  void PluginKuaiYou_ilo_OpenUserLogin5_m1531606577 (Il2CppObject * __this /* static, unused */, PluginKuaiYou_t3714788570 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginKuaiYou::ilo_updateRoleInfo6(PluginKuaiYou,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginKuaiYou_ilo_updateRoleInfo6_m3533275387 (Il2CppObject * __this /* static, unused */, PluginKuaiYou_t3714788570 * ____this0, String_t* ___serverId1, String_t* ___serverName2, String_t* ___roleId3, String_t* ___roleName4, String_t* ___roleLv5, String_t* ___VipLevel6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginKuaiYou::ilo_ReqSDKHttpLogin7(PluginsSdkMgr)
extern "C"  void PluginKuaiYou_ilo_ReqSDKHttpLogin7_m3177618588 (Il2CppObject * __this /* static, unused */, PluginsSdkMgr_t3884624670 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginKuaiYou::ilo_Log8(System.Object,System.Boolean)
extern "C"  void PluginKuaiYou_ilo_Log8_m3032210723 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginKuaiYou::ilo_Logout9(System.Action)
extern "C"  void PluginKuaiYou_ilo_Logout9_m732105618 (Il2CppObject * __this /* static, unused */, Action_t3771233898 * ___call0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginKuaiYou::ilo_logout10(PluginKuaiYou)
extern "C"  void PluginKuaiYou_ilo_logout10_m1681074153 (Il2CppObject * __this /* static, unused */, PluginKuaiYou_t3714788570 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PluginsSdkMgr PluginKuaiYou::ilo_get_PluginsSdkMgr11()
extern "C"  PluginsSdkMgr_t3884624670 * PluginKuaiYou_ilo_get_PluginsSdkMgr11_m541518150 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
