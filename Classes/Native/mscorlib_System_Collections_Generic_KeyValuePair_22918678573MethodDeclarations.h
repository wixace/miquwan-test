﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21944668977MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.WeakReference>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m2181231839(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t2918678573 *, String_t*, WeakReference_t2199479497 *, const MethodInfo*))KeyValuePair_2__ctor_m4168265535_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,System.WeakReference>::get_Key()
#define KeyValuePair_2_get_Key_m2474942505(__this, method) ((  String_t* (*) (KeyValuePair_2_t2918678573 *, const MethodInfo*))KeyValuePair_2_get_Key_m3256475977_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.WeakReference>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m431003114(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2918678573 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m1278074762_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,System.WeakReference>::get_Value()
#define KeyValuePair_2_get_Value_m335173161(__this, method) ((  WeakReference_t2199479497 * (*) (KeyValuePair_2_t2918678573 *, const MethodInfo*))KeyValuePair_2_get_Value_m3899079597_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.WeakReference>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m4137346282(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2918678573 *, WeakReference_t2199479497 *, const MethodInfo*))KeyValuePair_2_set_Value_m2954518154_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,System.WeakReference>::ToString()
#define KeyValuePair_2_ToString_m3361680824(__this, method) ((  String_t* (*) (KeyValuePair_2_t2918678573 *, const MethodInfo*))KeyValuePair_2_ToString_m1313859518_gshared)(__this, method)
