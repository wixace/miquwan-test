﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Set60Fps
struct Set60Fps_t1440280781;

#include "codegen/il2cpp-codegen.h"

// System.Void Set60Fps::.ctor()
extern "C"  void Set60Fps__ctor_m1048929390 (Set60Fps_t1440280781 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Set60Fps::Start()
extern "C"  void Set60Fps_Start_m4291034478 (Set60Fps_t1440280781 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
