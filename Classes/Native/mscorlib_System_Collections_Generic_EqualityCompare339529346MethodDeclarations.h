﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UIModelDisplayType>
struct DefaultComparer_t339529346;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_UIModelDisplayType1891752679.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<UIModelDisplayType>::.ctor()
extern "C"  void DefaultComparer__ctor_m1980002387_gshared (DefaultComparer_t339529346 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m1980002387(__this, method) ((  void (*) (DefaultComparer_t339529346 *, const MethodInfo*))DefaultComparer__ctor_m1980002387_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<UIModelDisplayType>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m1379600960_gshared (DefaultComparer_t339529346 * __this, int32_t ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m1379600960(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t339529346 *, int32_t, const MethodInfo*))DefaultComparer_GetHashCode_m1379600960_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<UIModelDisplayType>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m1301922408_gshared (DefaultComparer_t339529346 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m1301922408(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t339529346 *, int32_t, int32_t, const MethodInfo*))DefaultComparer_Equals_m1301922408_gshared)(__this, ___x0, ___y1, method)
