﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "AssemblyU2DCSharp_CsCfgBase69924517.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// superskillCfg
struct  superskillCfg_t3024131278  : public CsCfgBase_t69924517
{
public:
	// System.Int32 superskillCfg::id
	int32_t ___id_0;
	// System.String superskillCfg::name
	String_t* ___name_1;
	// System.String superskillCfg::icon
	String_t* ___icon_2;
	// System.String superskillCfg::btnicon
	String_t* ___btnicon_3;
	// System.String superskillCfg::flashicon
	String_t* ___flashicon_4;
	// System.String superskillCfg::rageres
	String_t* ___rageres_5;
	// System.String superskillCfg::btnres
	String_t* ___btnres_6;
	// System.Int32 superskillCfg::type
	int32_t ___type_7;
	// System.String superskillCfg::skillid
	String_t* ___skillid_8;
	// System.String superskillCfg::upnumber
	String_t* ___upnumber_9;
	// System.Single superskillCfg::endskilltime
	float ___endskilltime_10;
	// System.String superskillCfg::endskillid
	String_t* ___endskillid_11;
	// System.String superskillCfg::captainendskillid
	String_t* ___captainendskillid_12;
	// System.Boolean superskillCfg::ui_mask
	bool ___ui_mask_13;
	// System.String superskillCfg::ui_effectid
	String_t* ___ui_effectid_14;
	// System.Single superskillCfg::timeA
	float ___timeA_15;
	// System.Single superskillCfg::damageA
	float ___damageA_16;
	// System.Single superskillCfg::timeB
	float ___timeB_17;
	// System.Single superskillCfg::damageB
	float ___damageB_18;
	// System.String superskillCfg::skillsection
	String_t* ___skillsection_19;
	// System.String superskillCfg::skillreducetime
	String_t* ___skillreducetime_20;
	// System.String superskillCfg::skillgrade
	String_t* ___skillgrade_21;
	// System.Single superskillCfg::skillonetime
	float ___skillonetime_22;
	// System.Single superskillCfg::leasttime
	float ___leasttime_23;
	// System.Single superskillCfg::hidetime
	float ___hidetime_24;
	// System.String superskillCfg::superskill_prompt
	String_t* ___superskill_prompt_25;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(superskillCfg_t3024131278, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(superskillCfg_t3024131278, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier(&___name_1, value);
	}

	inline static int32_t get_offset_of_icon_2() { return static_cast<int32_t>(offsetof(superskillCfg_t3024131278, ___icon_2)); }
	inline String_t* get_icon_2() const { return ___icon_2; }
	inline String_t** get_address_of_icon_2() { return &___icon_2; }
	inline void set_icon_2(String_t* value)
	{
		___icon_2 = value;
		Il2CppCodeGenWriteBarrier(&___icon_2, value);
	}

	inline static int32_t get_offset_of_btnicon_3() { return static_cast<int32_t>(offsetof(superskillCfg_t3024131278, ___btnicon_3)); }
	inline String_t* get_btnicon_3() const { return ___btnicon_3; }
	inline String_t** get_address_of_btnicon_3() { return &___btnicon_3; }
	inline void set_btnicon_3(String_t* value)
	{
		___btnicon_3 = value;
		Il2CppCodeGenWriteBarrier(&___btnicon_3, value);
	}

	inline static int32_t get_offset_of_flashicon_4() { return static_cast<int32_t>(offsetof(superskillCfg_t3024131278, ___flashicon_4)); }
	inline String_t* get_flashicon_4() const { return ___flashicon_4; }
	inline String_t** get_address_of_flashicon_4() { return &___flashicon_4; }
	inline void set_flashicon_4(String_t* value)
	{
		___flashicon_4 = value;
		Il2CppCodeGenWriteBarrier(&___flashicon_4, value);
	}

	inline static int32_t get_offset_of_rageres_5() { return static_cast<int32_t>(offsetof(superskillCfg_t3024131278, ___rageres_5)); }
	inline String_t* get_rageres_5() const { return ___rageres_5; }
	inline String_t** get_address_of_rageres_5() { return &___rageres_5; }
	inline void set_rageres_5(String_t* value)
	{
		___rageres_5 = value;
		Il2CppCodeGenWriteBarrier(&___rageres_5, value);
	}

	inline static int32_t get_offset_of_btnres_6() { return static_cast<int32_t>(offsetof(superskillCfg_t3024131278, ___btnres_6)); }
	inline String_t* get_btnres_6() const { return ___btnres_6; }
	inline String_t** get_address_of_btnres_6() { return &___btnres_6; }
	inline void set_btnres_6(String_t* value)
	{
		___btnres_6 = value;
		Il2CppCodeGenWriteBarrier(&___btnres_6, value);
	}

	inline static int32_t get_offset_of_type_7() { return static_cast<int32_t>(offsetof(superskillCfg_t3024131278, ___type_7)); }
	inline int32_t get_type_7() const { return ___type_7; }
	inline int32_t* get_address_of_type_7() { return &___type_7; }
	inline void set_type_7(int32_t value)
	{
		___type_7 = value;
	}

	inline static int32_t get_offset_of_skillid_8() { return static_cast<int32_t>(offsetof(superskillCfg_t3024131278, ___skillid_8)); }
	inline String_t* get_skillid_8() const { return ___skillid_8; }
	inline String_t** get_address_of_skillid_8() { return &___skillid_8; }
	inline void set_skillid_8(String_t* value)
	{
		___skillid_8 = value;
		Il2CppCodeGenWriteBarrier(&___skillid_8, value);
	}

	inline static int32_t get_offset_of_upnumber_9() { return static_cast<int32_t>(offsetof(superskillCfg_t3024131278, ___upnumber_9)); }
	inline String_t* get_upnumber_9() const { return ___upnumber_9; }
	inline String_t** get_address_of_upnumber_9() { return &___upnumber_9; }
	inline void set_upnumber_9(String_t* value)
	{
		___upnumber_9 = value;
		Il2CppCodeGenWriteBarrier(&___upnumber_9, value);
	}

	inline static int32_t get_offset_of_endskilltime_10() { return static_cast<int32_t>(offsetof(superskillCfg_t3024131278, ___endskilltime_10)); }
	inline float get_endskilltime_10() const { return ___endskilltime_10; }
	inline float* get_address_of_endskilltime_10() { return &___endskilltime_10; }
	inline void set_endskilltime_10(float value)
	{
		___endskilltime_10 = value;
	}

	inline static int32_t get_offset_of_endskillid_11() { return static_cast<int32_t>(offsetof(superskillCfg_t3024131278, ___endskillid_11)); }
	inline String_t* get_endskillid_11() const { return ___endskillid_11; }
	inline String_t** get_address_of_endskillid_11() { return &___endskillid_11; }
	inline void set_endskillid_11(String_t* value)
	{
		___endskillid_11 = value;
		Il2CppCodeGenWriteBarrier(&___endskillid_11, value);
	}

	inline static int32_t get_offset_of_captainendskillid_12() { return static_cast<int32_t>(offsetof(superskillCfg_t3024131278, ___captainendskillid_12)); }
	inline String_t* get_captainendskillid_12() const { return ___captainendskillid_12; }
	inline String_t** get_address_of_captainendskillid_12() { return &___captainendskillid_12; }
	inline void set_captainendskillid_12(String_t* value)
	{
		___captainendskillid_12 = value;
		Il2CppCodeGenWriteBarrier(&___captainendskillid_12, value);
	}

	inline static int32_t get_offset_of_ui_mask_13() { return static_cast<int32_t>(offsetof(superskillCfg_t3024131278, ___ui_mask_13)); }
	inline bool get_ui_mask_13() const { return ___ui_mask_13; }
	inline bool* get_address_of_ui_mask_13() { return &___ui_mask_13; }
	inline void set_ui_mask_13(bool value)
	{
		___ui_mask_13 = value;
	}

	inline static int32_t get_offset_of_ui_effectid_14() { return static_cast<int32_t>(offsetof(superskillCfg_t3024131278, ___ui_effectid_14)); }
	inline String_t* get_ui_effectid_14() const { return ___ui_effectid_14; }
	inline String_t** get_address_of_ui_effectid_14() { return &___ui_effectid_14; }
	inline void set_ui_effectid_14(String_t* value)
	{
		___ui_effectid_14 = value;
		Il2CppCodeGenWriteBarrier(&___ui_effectid_14, value);
	}

	inline static int32_t get_offset_of_timeA_15() { return static_cast<int32_t>(offsetof(superskillCfg_t3024131278, ___timeA_15)); }
	inline float get_timeA_15() const { return ___timeA_15; }
	inline float* get_address_of_timeA_15() { return &___timeA_15; }
	inline void set_timeA_15(float value)
	{
		___timeA_15 = value;
	}

	inline static int32_t get_offset_of_damageA_16() { return static_cast<int32_t>(offsetof(superskillCfg_t3024131278, ___damageA_16)); }
	inline float get_damageA_16() const { return ___damageA_16; }
	inline float* get_address_of_damageA_16() { return &___damageA_16; }
	inline void set_damageA_16(float value)
	{
		___damageA_16 = value;
	}

	inline static int32_t get_offset_of_timeB_17() { return static_cast<int32_t>(offsetof(superskillCfg_t3024131278, ___timeB_17)); }
	inline float get_timeB_17() const { return ___timeB_17; }
	inline float* get_address_of_timeB_17() { return &___timeB_17; }
	inline void set_timeB_17(float value)
	{
		___timeB_17 = value;
	}

	inline static int32_t get_offset_of_damageB_18() { return static_cast<int32_t>(offsetof(superskillCfg_t3024131278, ___damageB_18)); }
	inline float get_damageB_18() const { return ___damageB_18; }
	inline float* get_address_of_damageB_18() { return &___damageB_18; }
	inline void set_damageB_18(float value)
	{
		___damageB_18 = value;
	}

	inline static int32_t get_offset_of_skillsection_19() { return static_cast<int32_t>(offsetof(superskillCfg_t3024131278, ___skillsection_19)); }
	inline String_t* get_skillsection_19() const { return ___skillsection_19; }
	inline String_t** get_address_of_skillsection_19() { return &___skillsection_19; }
	inline void set_skillsection_19(String_t* value)
	{
		___skillsection_19 = value;
		Il2CppCodeGenWriteBarrier(&___skillsection_19, value);
	}

	inline static int32_t get_offset_of_skillreducetime_20() { return static_cast<int32_t>(offsetof(superskillCfg_t3024131278, ___skillreducetime_20)); }
	inline String_t* get_skillreducetime_20() const { return ___skillreducetime_20; }
	inline String_t** get_address_of_skillreducetime_20() { return &___skillreducetime_20; }
	inline void set_skillreducetime_20(String_t* value)
	{
		___skillreducetime_20 = value;
		Il2CppCodeGenWriteBarrier(&___skillreducetime_20, value);
	}

	inline static int32_t get_offset_of_skillgrade_21() { return static_cast<int32_t>(offsetof(superskillCfg_t3024131278, ___skillgrade_21)); }
	inline String_t* get_skillgrade_21() const { return ___skillgrade_21; }
	inline String_t** get_address_of_skillgrade_21() { return &___skillgrade_21; }
	inline void set_skillgrade_21(String_t* value)
	{
		___skillgrade_21 = value;
		Il2CppCodeGenWriteBarrier(&___skillgrade_21, value);
	}

	inline static int32_t get_offset_of_skillonetime_22() { return static_cast<int32_t>(offsetof(superskillCfg_t3024131278, ___skillonetime_22)); }
	inline float get_skillonetime_22() const { return ___skillonetime_22; }
	inline float* get_address_of_skillonetime_22() { return &___skillonetime_22; }
	inline void set_skillonetime_22(float value)
	{
		___skillonetime_22 = value;
	}

	inline static int32_t get_offset_of_leasttime_23() { return static_cast<int32_t>(offsetof(superskillCfg_t3024131278, ___leasttime_23)); }
	inline float get_leasttime_23() const { return ___leasttime_23; }
	inline float* get_address_of_leasttime_23() { return &___leasttime_23; }
	inline void set_leasttime_23(float value)
	{
		___leasttime_23 = value;
	}

	inline static int32_t get_offset_of_hidetime_24() { return static_cast<int32_t>(offsetof(superskillCfg_t3024131278, ___hidetime_24)); }
	inline float get_hidetime_24() const { return ___hidetime_24; }
	inline float* get_address_of_hidetime_24() { return &___hidetime_24; }
	inline void set_hidetime_24(float value)
	{
		___hidetime_24 = value;
	}

	inline static int32_t get_offset_of_superskill_prompt_25() { return static_cast<int32_t>(offsetof(superskillCfg_t3024131278, ___superskill_prompt_25)); }
	inline String_t* get_superskill_prompt_25() const { return ___superskill_prompt_25; }
	inline String_t** get_address_of_superskill_prompt_25() { return &___superskill_prompt_25; }
	inline void set_superskill_prompt_25(String_t* value)
	{
		___superskill_prompt_25 = value;
		Il2CppCodeGenWriteBarrier(&___superskill_prompt_25, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
