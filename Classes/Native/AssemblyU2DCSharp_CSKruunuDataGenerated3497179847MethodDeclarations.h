﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CSKruunuDataGenerated
struct CSKruunuDataGenerated_t3497179847;
// JSVCall
struct JSVCall_t3708497963;
// CSAttrBuffData[]
struct CSAttrBuffDataU5BU5D_t2697436427;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"

// System.Void CSKruunuDataGenerated::.ctor()
extern "C"  void CSKruunuDataGenerated__ctor_m343699076 (CSKruunuDataGenerated_t3497179847 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CSKruunuDataGenerated::CSKruunuData_CSKruunuData1(JSVCall,System.Int32)
extern "C"  bool CSKruunuDataGenerated_CSKruunuData_CSKruunuData1_m557096430 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSKruunuDataGenerated::CSKruunuData_attrBuffs(JSVCall)
extern "C"  void CSKruunuDataGenerated_CSKruunuData_attrBuffs_m1010691343 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CSKruunuDataGenerated::CSKruunuData_GetBuffAttrDic(JSVCall,System.Int32)
extern "C"  bool CSKruunuDataGenerated_CSKruunuData_GetBuffAttrDic_m2899638369 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CSKruunuDataGenerated::CSKruunuData_GetPlusAttList(JSVCall,System.Int32)
extern "C"  bool CSKruunuDataGenerated_CSKruunuData_GetPlusAttList_m2708144396 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CSKruunuDataGenerated::CSKruunuData_LoadData__String(JSVCall,System.Int32)
extern "C"  bool CSKruunuDataGenerated_CSKruunuData_LoadData__String_m3762324382 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSKruunuDataGenerated::__Register()
extern "C"  void CSKruunuDataGenerated___Register_m1425637571 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CSAttrBuffData[] CSKruunuDataGenerated::<CSKruunuData_attrBuffs>m__27()
extern "C"  CSAttrBuffDataU5BU5D_t2697436427* CSKruunuDataGenerated_U3CCSKruunuData_attrBuffsU3Em__27_m2943618215 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CSKruunuDataGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t CSKruunuDataGenerated_ilo_getObject1_m1217134770 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSKruunuDataGenerated::ilo_addJSCSRel2(System.Int32,System.Object)
extern "C"  void CSKruunuDataGenerated_ilo_addJSCSRel2_m1019606977 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CSKruunuDataGenerated::ilo_setObject3(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t CSKruunuDataGenerated_ilo_setObject3_m2928582828 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String CSKruunuDataGenerated::ilo_getStringS4(System.Int32)
extern "C"  String_t* CSKruunuDataGenerated_ilo_getStringS4_m3344665131 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CSKruunuDataGenerated::ilo_getElement5(System.Int32,System.Int32)
extern "C"  int32_t CSKruunuDataGenerated_ilo_getElement5_m2349668090 (Il2CppObject * __this /* static, unused */, int32_t ___id0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object CSKruunuDataGenerated::ilo_getObject6(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * CSKruunuDataGenerated_ilo_getObject6_m759895718 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
