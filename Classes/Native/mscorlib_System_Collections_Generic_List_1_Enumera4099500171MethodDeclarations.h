﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<UnityEngine.Bounds>
struct List_1_t4079827401;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera4099500171.h"
#include "UnityEngine_UnityEngine_Bounds2711641849.h"

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Bounds>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m3309784255_gshared (Enumerator_t4099500171 * __this, List_1_t4079827401 * ___l0, const MethodInfo* method);
#define Enumerator__ctor_m3309784255(__this, ___l0, method) ((  void (*) (Enumerator_t4099500171 *, List_1_t4079827401 *, const MethodInfo*))Enumerator__ctor_m3309784255_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Bounds>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3815541811_gshared (Enumerator_t4099500171 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m3815541811(__this, method) ((  void (*) (Enumerator_t4099500171 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3815541811_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.Bounds>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1935244201_gshared (Enumerator_t4099500171 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1935244201(__this, method) ((  Il2CppObject * (*) (Enumerator_t4099500171 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1935244201_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Bounds>::Dispose()
extern "C"  void Enumerator_Dispose_m2051521124_gshared (Enumerator_t4099500171 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m2051521124(__this, method) ((  void (*) (Enumerator_t4099500171 *, const MethodInfo*))Enumerator_Dispose_m2051521124_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Bounds>::VerifyState()
extern "C"  void Enumerator_VerifyState_m2861541277_gshared (Enumerator_t4099500171 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m2861541277(__this, method) ((  void (*) (Enumerator_t4099500171 *, const MethodInfo*))Enumerator_VerifyState_m2861541277_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Bounds>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2118485987_gshared (Enumerator_t4099500171 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m2118485987(__this, method) ((  bool (*) (Enumerator_t4099500171 *, const MethodInfo*))Enumerator_MoveNext_m2118485987_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.Bounds>::get_Current()
extern "C"  Bounds_t2711641849  Enumerator_get_Current_m3450009334_gshared (Enumerator_t4099500171 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m3450009334(__this, method) ((  Bounds_t2711641849  (*) (Enumerator_t4099500171 *, const MethodInfo*))Enumerator_get_Current_m3450009334_gshared)(__this, method)
