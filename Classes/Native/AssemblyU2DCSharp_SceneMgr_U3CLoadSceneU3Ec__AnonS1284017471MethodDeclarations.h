﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SceneMgr/<LoadScene>c__AnonStorey159
struct U3CLoadSceneU3Ec__AnonStorey159_t1284017471;

#include "codegen/il2cpp-codegen.h"

// System.Void SceneMgr/<LoadScene>c__AnonStorey159::.ctor()
extern "C"  void U3CLoadSceneU3Ec__AnonStorey159__ctor_m3272217660 (U3CLoadSceneU3Ec__AnonStorey159_t1284017471 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SceneMgr/<LoadScene>c__AnonStorey159::<>m__3E3()
extern "C"  void U3CLoadSceneU3Ec__AnonStorey159_U3CU3Em__3E3_m2595837502 (U3CLoadSceneU3Ec__AnonStorey159_t1284017471 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
