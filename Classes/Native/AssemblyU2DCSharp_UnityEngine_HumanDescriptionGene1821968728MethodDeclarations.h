﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_HumanDescriptionGenerated
struct UnityEngine_HumanDescriptionGenerated_t1821968728;
// JSVCall
struct JSVCall_t3708497963;
// UnityEngine.HumanBone[]
struct HumanBoneU5BU5D_t838156158;
// UnityEngine.SkeletonBone[]
struct SkeletonBoneU5BU5D_t786140632;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void UnityEngine_HumanDescriptionGenerated::.ctor()
extern "C"  void UnityEngine_HumanDescriptionGenerated__ctor_m1949836051 (UnityEngine_HumanDescriptionGenerated_t1821968728 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_HumanDescriptionGenerated::.cctor()
extern "C"  void UnityEngine_HumanDescriptionGenerated__cctor_m4128246522 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_HumanDescriptionGenerated::HumanDescription_HumanDescription1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_HumanDescriptionGenerated_HumanDescription_HumanDescription1_m2737412831 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_HumanDescriptionGenerated::HumanDescription_human(JSVCall)
extern "C"  void UnityEngine_HumanDescriptionGenerated_HumanDescription_human_m2583192537 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_HumanDescriptionGenerated::HumanDescription_skeleton(JSVCall)
extern "C"  void UnityEngine_HumanDescriptionGenerated_HumanDescription_skeleton_m2469657337 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_HumanDescriptionGenerated::HumanDescription_upperArmTwist(JSVCall)
extern "C"  void UnityEngine_HumanDescriptionGenerated_HumanDescription_upperArmTwist_m3980893209 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_HumanDescriptionGenerated::HumanDescription_lowerArmTwist(JSVCall)
extern "C"  void UnityEngine_HumanDescriptionGenerated_HumanDescription_lowerArmTwist_m2070410138 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_HumanDescriptionGenerated::HumanDescription_upperLegTwist(JSVCall)
extern "C"  void UnityEngine_HumanDescriptionGenerated_HumanDescription_upperLegTwist_m3574031435 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_HumanDescriptionGenerated::HumanDescription_lowerLegTwist(JSVCall)
extern "C"  void UnityEngine_HumanDescriptionGenerated_HumanDescription_lowerLegTwist_m1663548364 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_HumanDescriptionGenerated::HumanDescription_armStretch(JSVCall)
extern "C"  void UnityEngine_HumanDescriptionGenerated_HumanDescription_armStretch_m3220920765 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_HumanDescriptionGenerated::HumanDescription_legStretch(JSVCall)
extern "C"  void UnityEngine_HumanDescriptionGenerated_HumanDescription_legStretch_m3068779887 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_HumanDescriptionGenerated::HumanDescription_feetSpacing(JSVCall)
extern "C"  void UnityEngine_HumanDescriptionGenerated_HumanDescription_feetSpacing_m1631510353 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_HumanDescriptionGenerated::HumanDescription_hasTranslationDoF(JSVCall)
extern "C"  void UnityEngine_HumanDescriptionGenerated_HumanDescription_hasTranslationDoF_m2341878018 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_HumanDescriptionGenerated::__Register()
extern "C"  void UnityEngine_HumanDescriptionGenerated___Register_m2926792084 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.HumanBone[] UnityEngine_HumanDescriptionGenerated::<HumanDescription_human>m__258()
extern "C"  HumanBoneU5BU5D_t838156158* UnityEngine_HumanDescriptionGenerated_U3CHumanDescription_humanU3Em__258_m4066248199 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.SkeletonBone[] UnityEngine_HumanDescriptionGenerated::<HumanDescription_skeleton>m__259()
extern "C"  SkeletonBoneU5BU5D_t786140632* UnityEngine_HumanDescriptionGenerated_U3CHumanDescription_skeletonU3Em__259_m872915114 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_HumanDescriptionGenerated::ilo_setObject1(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UnityEngine_HumanDescriptionGenerated_ilo_setObject1_m1105344251 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_HumanDescriptionGenerated::ilo_moveSaveID2Arr2(System.Int32)
extern "C"  void UnityEngine_HumanDescriptionGenerated_ilo_moveSaveID2Arr2_m261454249 (Il2CppObject * __this /* static, unused */, int32_t ___arrIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_HumanDescriptionGenerated::ilo_setArrayS3(System.Int32,System.Int32,System.Boolean)
extern "C"  void UnityEngine_HumanDescriptionGenerated_ilo_setArrayS3_m1249263570 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___count1, bool ___bClear2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_HumanDescriptionGenerated::ilo_changeJSObj4(System.Int32,System.Object)
extern "C"  void UnityEngine_HumanDescriptionGenerated_ilo_changeJSObj4_m3746443193 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObjNew1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_HumanDescriptionGenerated::ilo_setSingle5(System.Int32,System.Single)
extern "C"  void UnityEngine_HumanDescriptionGenerated_ilo_setSingle5_m2350175877 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_HumanDescriptionGenerated::ilo_getBooleanS6(System.Int32)
extern "C"  bool UnityEngine_HumanDescriptionGenerated_ilo_getBooleanS6_m447311542 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_HumanDescriptionGenerated::ilo_getElement7(System.Int32,System.Int32)
extern "C"  int32_t UnityEngine_HumanDescriptionGenerated_ilo_getElement7_m3827681993 (Il2CppObject * __this /* static, unused */, int32_t ___id0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_HumanDescriptionGenerated::ilo_getObject8(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_HumanDescriptionGenerated_ilo_getObject8_m1049473337 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
