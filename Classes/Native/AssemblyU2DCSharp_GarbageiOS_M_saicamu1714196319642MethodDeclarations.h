﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_saicamu171
struct M_saicamu171_t4196319642;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_saicamu1714196319642.h"

// System.Void GarbageiOS.M_saicamu171::.ctor()
extern "C"  void M_saicamu171__ctor_m632272857 (M_saicamu171_t4196319642 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_saicamu171::M_jepor0(System.String[],System.Int32)
extern "C"  void M_saicamu171_M_jepor0_m1266737292 (M_saicamu171_t4196319642 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_saicamu171::M_pooboo1(System.String[],System.Int32)
extern "C"  void M_saicamu171_M_pooboo1_m2321014555 (M_saicamu171_t4196319642 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_saicamu171::M_nedraRowmeme2(System.String[],System.Int32)
extern "C"  void M_saicamu171_M_nedraRowmeme2_m1598167136 (M_saicamu171_t4196319642 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_saicamu171::M_vowsa3(System.String[],System.Int32)
extern "C"  void M_saicamu171_M_vowsa3_m2578606683 (M_saicamu171_t4196319642 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_saicamu171::M_cecawe4(System.String[],System.Int32)
extern "C"  void M_saicamu171_M_cecawe4_m3433490114 (M_saicamu171_t4196319642 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_saicamu171::M_lawkifear5(System.String[],System.Int32)
extern "C"  void M_saicamu171_M_lawkifear5_m894554201 (M_saicamu171_t4196319642 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_saicamu171::M_birarBeawall6(System.String[],System.Int32)
extern "C"  void M_saicamu171_M_birarBeawall6_m3330944614 (M_saicamu171_t4196319642 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_saicamu171::M_noudrawYixear7(System.String[],System.Int32)
extern "C"  void M_saicamu171_M_noudrawYixear7_m3996038309 (M_saicamu171_t4196319642 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_saicamu171::M_ratrepasSertrall8(System.String[],System.Int32)
extern "C"  void M_saicamu171_M_ratrepasSertrall8_m304089415 (M_saicamu171_t4196319642 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_saicamu171::ilo_M_pooboo11(GarbageiOS.M_saicamu171,System.String[],System.Int32)
extern "C"  void M_saicamu171_ilo_M_pooboo11_m2285634047 (Il2CppObject * __this /* static, unused */, M_saicamu171_t4196319642 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_saicamu171::ilo_M_vowsa32(GarbageiOS.M_saicamu171,System.String[],System.Int32)
extern "C"  void M_saicamu171_ilo_M_vowsa32_m4175085370 (Il2CppObject * __this /* static, unused */, M_saicamu171_t4196319642 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_saicamu171::ilo_M_cecawe43(GarbageiOS.M_saicamu171,System.String[],System.Int32)
extern "C"  void M_saicamu171_ilo_M_cecawe43_m2776678074 (Il2CppObject * __this /* static, unused */, M_saicamu171_t4196319642 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_saicamu171::ilo_M_birarBeawall64(GarbageiOS.M_saicamu171,System.String[],System.Int32)
extern "C"  void M_saicamu171_ilo_M_birarBeawall64_m4249483927 (Il2CppObject * __this /* static, unused */, M_saicamu171_t4196319642 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
