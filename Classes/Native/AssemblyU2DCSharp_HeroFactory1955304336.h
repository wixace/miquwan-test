﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Hashtable
struct Hashtable_t1407064410;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HeroFactory
struct  HeroFactory_t1955304336  : public Il2CppObject
{
public:

public:
};

struct HeroFactory_t1955304336_StaticFields
{
public:
	// System.Collections.Hashtable HeroFactory::lookupType
	Hashtable_t1407064410 * ___lookupType_0;

public:
	inline static int32_t get_offset_of_lookupType_0() { return static_cast<int32_t>(offsetof(HeroFactory_t1955304336_StaticFields, ___lookupType_0)); }
	inline Hashtable_t1407064410 * get_lookupType_0() const { return ___lookupType_0; }
	inline Hashtable_t1407064410 ** get_address_of_lookupType_0() { return &___lookupType_0; }
	inline void set_lookupType_0(Hashtable_t1407064410 * value)
	{
		___lookupType_0 = value;
		Il2CppCodeGenWriteBarrier(&___lookupType_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
