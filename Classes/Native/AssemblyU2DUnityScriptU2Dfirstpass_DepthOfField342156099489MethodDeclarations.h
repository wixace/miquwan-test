﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DepthOfField34
struct DepthOfField34_t2156099489;
// UnityEngine.RenderTexture
struct RenderTexture_t1963041563;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture1963041563.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_DofBlurriness1555378096.h"

// System.Void DepthOfField34::.ctor()
extern "C"  void DepthOfField34__ctor_m3144651971 (DepthOfField34_t2156099489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DepthOfField34::.cctor()
extern "C"  void DepthOfField34__cctor_m2512834378 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DepthOfField34::CreateMaterials()
extern "C"  void DepthOfField34_CreateMaterials_m4196866161 (DepthOfField34_t2156099489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean DepthOfField34::CheckResources()
extern "C"  bool DepthOfField34_CheckResources_m2543719236 (DepthOfField34_t2156099489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DepthOfField34::OnDisable()
extern "C"  void DepthOfField34_OnDisable_m2856898474 (DepthOfField34_t2156099489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DepthOfField34::OnEnable()
extern "C"  void DepthOfField34_OnEnable_m2571786211 (DepthOfField34_t2156099489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single DepthOfField34::FocalDistance01(System.Single)
extern "C"  float DepthOfField34_FocalDistance01_m2973206811 (DepthOfField34_t2156099489 * __this, float ___worldDist0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 DepthOfField34::GetDividerBasedOnQuality()
extern "C"  int32_t DepthOfField34_GetDividerBasedOnQuality_m763614097 (DepthOfField34_t2156099489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 DepthOfField34::GetLowResolutionDividerBasedOnQuality(System.Int32)
extern "C"  int32_t DepthOfField34_GetLowResolutionDividerBasedOnQuality_m1635427470 (DepthOfField34_t2156099489 * __this, int32_t ___baseDivider0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DepthOfField34::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void DepthOfField34_OnRenderImage_m3298611643 (DepthOfField34_t2156099489 * __this, RenderTexture_t1963041563 * ___source0, RenderTexture_t1963041563 * ___destination1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DepthOfField34::Blur(UnityEngine.RenderTexture,UnityEngine.RenderTexture,DofBlurriness,System.Int32,System.Single)
extern "C"  void DepthOfField34_Blur_m3990620742 (DepthOfField34_t2156099489 * __this, RenderTexture_t1963041563 * ___from0, RenderTexture_t1963041563 * ___to1, int32_t ___iterations2, int32_t ___blurPass3, float ___spread4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DepthOfField34::BlurFg(UnityEngine.RenderTexture,UnityEngine.RenderTexture,DofBlurriness,System.Int32,System.Single)
extern "C"  void DepthOfField34_BlurFg_m767514023 (DepthOfField34_t2156099489 * __this, RenderTexture_t1963041563 * ___from0, RenderTexture_t1963041563 * ___to1, int32_t ___iterations2, int32_t ___blurPass3, float ___spread4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DepthOfField34::BlurHex(UnityEngine.RenderTexture,UnityEngine.RenderTexture,System.Int32,System.Single,UnityEngine.RenderTexture)
extern "C"  void DepthOfField34_BlurHex_m2599315629 (DepthOfField34_t2156099489 * __this, RenderTexture_t1963041563 * ___from0, RenderTexture_t1963041563 * ___to1, int32_t ___blurPass2, float ___spread3, RenderTexture_t1963041563 * ___tmp4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DepthOfField34::Downsample(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void DepthOfField34_Downsample_m1940966517 (DepthOfField34_t2156099489 * __this, RenderTexture_t1963041563 * ___from0, RenderTexture_t1963041563 * ___to1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DepthOfField34::AddBokeh(UnityEngine.RenderTexture,UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void DepthOfField34_AddBokeh_m903980319 (DepthOfField34_t2156099489 * __this, RenderTexture_t1963041563 * ___bokehInfo0, RenderTexture_t1963041563 * ___tempTex1, RenderTexture_t1963041563 * ___finalTarget2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DepthOfField34::ReleaseTextures()
extern "C"  void DepthOfField34_ReleaseTextures_m113527296 (DepthOfField34_t2156099489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DepthOfField34::AllocateTextures(System.Boolean,UnityEngine.RenderTexture,System.Int32,System.Int32)
extern "C"  void DepthOfField34_AllocateTextures_m2541777835 (DepthOfField34_t2156099489 * __this, bool ___blurForeground0, RenderTexture_t1963041563 * ___source1, int32_t ___divider2, int32_t ___lowTexDivider3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DepthOfField34::Main()
extern "C"  void DepthOfField34_Main_m2233692058 (DepthOfField34_t2156099489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
