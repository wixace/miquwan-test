﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2837902903.h"
#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DCSharp_PluginReYun_strutDict4055560227.h"

// System.Void System.Array/InternalEnumerator`1<PluginReYun/strutDict>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m478109032_gshared (InternalEnumerator_1_t2837902903 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m478109032(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t2837902903 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m478109032_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<PluginReYun/strutDict>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3457077048_gshared (InternalEnumerator_1_t2837902903 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3457077048(__this, method) ((  void (*) (InternalEnumerator_1_t2837902903 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3457077048_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<PluginReYun/strutDict>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2733648878_gshared (InternalEnumerator_1_t2837902903 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2733648878(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t2837902903 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2733648878_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<PluginReYun/strutDict>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1812400447_gshared (InternalEnumerator_1_t2837902903 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m1812400447(__this, method) ((  void (*) (InternalEnumerator_1_t2837902903 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1812400447_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<PluginReYun/strutDict>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2898605672_gshared (InternalEnumerator_1_t2837902903 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m2898605672(__this, method) ((  bool (*) (InternalEnumerator_1_t2837902903 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m2898605672_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<PluginReYun/strutDict>::get_Current()
extern "C"  strutDict_t4055560227  InternalEnumerator_1_get_Current_m1888178513_gshared (InternalEnumerator_1_t2837902903 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1888178513(__this, method) ((  strutDict_t4055560227  (*) (InternalEnumerator_1_t2837902903 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1888178513_gshared)(__this, method)
