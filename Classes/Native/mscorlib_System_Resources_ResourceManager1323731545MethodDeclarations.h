﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Resources.ResourceManager
struct ResourceManager_t1323731545;
// System.String
struct String_t;
// System.Reflection.Assembly
struct Assembly_t1418687608;
// System.Collections.Hashtable
struct Hashtable_t1407064410;
// System.Globalization.CultureInfo
struct CultureInfo_t1065375142;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Reflection_Assembly1418687608.h"

// System.Void System.Resources.ResourceManager::.ctor()
extern "C"  void ResourceManager__ctor_m1041657915 (ResourceManager_t1323731545 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Resources.ResourceManager::.ctor(System.String,System.Reflection.Assembly)
extern "C"  void ResourceManager__ctor_m2505354837 (ResourceManager_t1323731545 * __this, String_t* ___baseName0, Assembly_t1418687608 * ___assembly1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Resources.ResourceManager::.cctor()
extern "C"  void ResourceManager__cctor_m1744528082 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Hashtable System.Resources.ResourceManager::GetResourceSets(System.Reflection.Assembly,System.String)
extern "C"  Hashtable_t1407064410 * ResourceManager_GetResourceSets_m3015585469 (Il2CppObject * __this /* static, unused */, Assembly_t1418687608 * ___assembly0, String_t* ___basename1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Globalization.CultureInfo System.Resources.ResourceManager::GetNeutralResourcesLanguage(System.Reflection.Assembly)
extern "C"  CultureInfo_t1065375142 * ResourceManager_GetNeutralResourcesLanguage_m2512021728 (Il2CppObject * __this /* static, unused */, Assembly_t1418687608 * ___a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
