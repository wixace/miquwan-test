﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.GridGraph/TextureData
struct TextureData_t1394081396;
// Pathfinding.GridNode
struct GridNode_t3795753694;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_GridNode3795753694.h"
#include "AssemblyU2DCSharp_Pathfinding_GridGraph_TextureDat3235179551.h"

// System.Void Pathfinding.GridGraph/TextureData::.ctor()
extern "C"  void TextureData__ctor_m1766449527 (TextureData_t1394081396 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GridGraph/TextureData::Initialize()
extern "C"  void TextureData_Initialize_m3996632957 (TextureData_t1394081396 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GridGraph/TextureData::Apply(Pathfinding.GridNode,System.Int32,System.Int32)
extern "C"  void TextureData_Apply_m852378705 (TextureData_t1394081396 * __this, GridNode_t3795753694 * ___node0, int32_t ___x1, int32_t ___z2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GridGraph/TextureData::ApplyChannel(Pathfinding.GridNode,System.Int32,System.Int32,System.Int32,Pathfinding.GridGraph/TextureData/ChannelUse,System.Single)
extern "C"  void TextureData_ApplyChannel_m2863597861 (TextureData_t1394081396 * __this, GridNode_t3795753694 * ___node0, int32_t ___x1, int32_t ___z2, int32_t ___value3, int32_t ___channelUse4, float ___factor5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
