﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.GraphEditorBase
struct GraphEditorBase_t3362408622;

#include "codegen/il2cpp-codegen.h"

// System.Void Pathfinding.GraphEditorBase::.ctor()
extern "C"  void GraphEditorBase__ctor_m116047433 (GraphEditorBase_t3362408622 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
