﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<SoundTypeID,System.Object>
struct Dictionary_2_t2048431866;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3365755258.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21947212572.h"
#include "AssemblyU2DCSharp_SoundTypeID3626616740.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<SoundTypeID,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m1787693479_gshared (Enumerator_t3365755258 * __this, Dictionary_2_t2048431866 * ___dictionary0, const MethodInfo* method);
#define Enumerator__ctor_m1787693479(__this, ___dictionary0, method) ((  void (*) (Enumerator_t3365755258 *, Dictionary_2_t2048431866 *, const MethodInfo*))Enumerator__ctor_m1787693479_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<SoundTypeID,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3676151450_gshared (Enumerator_t3365755258 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m3676151450(__this, method) ((  Il2CppObject * (*) (Enumerator_t3365755258 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3676151450_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<SoundTypeID,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3540839598_gshared (Enumerator_t3365755258 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m3540839598(__this, method) ((  void (*) (Enumerator_t3365755258 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3540839598_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<SoundTypeID,System.Object>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t1751606614  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m4033043703_gshared (Enumerator_t3365755258 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m4033043703(__this, method) ((  DictionaryEntry_t1751606614  (*) (Enumerator_t3365755258 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m4033043703_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<SoundTypeID,System.Object>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m167641078_gshared (Enumerator_t3365755258 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m167641078(__this, method) ((  Il2CppObject * (*) (Enumerator_t3365755258 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m167641078_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<SoundTypeID,System.Object>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3237722376_gshared (Enumerator_t3365755258 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3237722376(__this, method) ((  Il2CppObject * (*) (Enumerator_t3365755258 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3237722376_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<SoundTypeID,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2997434906_gshared (Enumerator_t3365755258 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m2997434906(__this, method) ((  bool (*) (Enumerator_t3365755258 *, const MethodInfo*))Enumerator_MoveNext_m2997434906_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<SoundTypeID,System.Object>::get_Current()
extern "C"  KeyValuePair_2_t1947212572  Enumerator_get_Current_m1198975062_gshared (Enumerator_t3365755258 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m1198975062(__this, method) ((  KeyValuePair_2_t1947212572  (*) (Enumerator_t3365755258 *, const MethodInfo*))Enumerator_get_Current_m1198975062_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<SoundTypeID,System.Object>::get_CurrentKey()
extern "C"  int32_t Enumerator_get_CurrentKey_m3083891431_gshared (Enumerator_t3365755258 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m3083891431(__this, method) ((  int32_t (*) (Enumerator_t3365755258 *, const MethodInfo*))Enumerator_get_CurrentKey_m3083891431_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<SoundTypeID,System.Object>::get_CurrentValue()
extern "C"  Il2CppObject * Enumerator_get_CurrentValue_m1429984843_gshared (Enumerator_t3365755258 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m1429984843(__this, method) ((  Il2CppObject * (*) (Enumerator_t3365755258 *, const MethodInfo*))Enumerator_get_CurrentValue_m1429984843_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<SoundTypeID,System.Object>::Reset()
extern "C"  void Enumerator_Reset_m153295225_gshared (Enumerator_t3365755258 * __this, const MethodInfo* method);
#define Enumerator_Reset_m153295225(__this, method) ((  void (*) (Enumerator_t3365755258 *, const MethodInfo*))Enumerator_Reset_m153295225_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<SoundTypeID,System.Object>::VerifyState()
extern "C"  void Enumerator_VerifyState_m294759106_gshared (Enumerator_t3365755258 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m294759106(__this, method) ((  void (*) (Enumerator_t3365755258 *, const MethodInfo*))Enumerator_VerifyState_m294759106_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<SoundTypeID,System.Object>::VerifyCurrent()
extern "C"  void Enumerator_VerifyCurrent_m2540531114_gshared (Enumerator_t3365755258 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m2540531114(__this, method) ((  void (*) (Enumerator_t3365755258 *, const MethodInfo*))Enumerator_VerifyCurrent_m2540531114_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<SoundTypeID,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m3811252233_gshared (Enumerator_t3365755258 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m3811252233(__this, method) ((  void (*) (Enumerator_t3365755258 *, const MethodInfo*))Enumerator_Dispose_m3811252233_gshared)(__this, method)
