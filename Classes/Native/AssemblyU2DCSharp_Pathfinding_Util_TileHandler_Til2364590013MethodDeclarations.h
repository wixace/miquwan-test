﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.Util.TileHandler/TileType
struct TileType_t2364590013;
// Pathfinding.Int3[]
struct Int3U5BU5D_t516284607;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// UnityEngine.Mesh
struct Mesh_t4241756145;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_Int31974045594.h"
#include "UnityEngine_UnityEngine_Mesh4241756145.h"

// System.Void Pathfinding.Util.TileHandler/TileType::.ctor(Pathfinding.Int3[],System.Int32[],Pathfinding.Int3,Pathfinding.Int3,System.Int32,System.Int32)
extern "C"  void TileType__ctor_m3332670327 (TileType_t2364590013 * __this, Int3U5BU5D_t516284607* ___sourceVerts0, Int32U5BU5D_t3230847821* ___sourceTris1, Int3_t1974045594  ___tileSize2, Int3_t1974045594  ___centerOffset3, int32_t ___width4, int32_t ___depth5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Util.TileHandler/TileType::.ctor(UnityEngine.Mesh,Pathfinding.Int3,Pathfinding.Int3,System.Int32,System.Int32)
extern "C"  void TileType__ctor_m3538075274 (TileType_t2364590013 * __this, Mesh_t4241756145 * ___source0, Int3_t1974045594  ___tileSize1, Int3_t1974045594  ___centerOffset2, int32_t ___width3, int32_t ___depth4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Util.TileHandler/TileType::.cctor()
extern "C"  void TileType__cctor_m1351405343 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pathfinding.Util.TileHandler/TileType::get_Width()
extern "C"  int32_t TileType_get_Width_m4160652503 (TileType_t2364590013 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pathfinding.Util.TileHandler/TileType::get_Depth()
extern "C"  int32_t TileType_get_Depth_m79550100 (TileType_t2364590013 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Util.TileHandler/TileType::Load(Pathfinding.Int3[]&,System.Int32[]&,System.Int32,System.Int32)
extern "C"  void TileType_Load_m1408904823 (TileType_t2364590013 * __this, Int3U5BU5D_t516284607** ___verts0, Int32U5BU5D_t3230847821** ___tris1, int32_t ___rotation2, int32_t ___yoffset3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
