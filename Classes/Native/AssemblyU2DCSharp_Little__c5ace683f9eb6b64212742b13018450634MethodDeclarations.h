﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._c5ace683f9eb6b64212742b1a35758f5
struct _c5ace683f9eb6b64212742b1a35758f5_t3018450634;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._c5ace683f9eb6b64212742b1a35758f5::.ctor()
extern "C"  void _c5ace683f9eb6b64212742b1a35758f5__ctor_m462373539 (_c5ace683f9eb6b64212742b1a35758f5_t3018450634 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._c5ace683f9eb6b64212742b1a35758f5::_c5ace683f9eb6b64212742b1a35758f5m2(System.Int32)
extern "C"  int32_t _c5ace683f9eb6b64212742b1a35758f5__c5ace683f9eb6b64212742b1a35758f5m2_m1660439897 (_c5ace683f9eb6b64212742b1a35758f5_t3018450634 * __this, int32_t ____c5ace683f9eb6b64212742b1a35758f5a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._c5ace683f9eb6b64212742b1a35758f5::_c5ace683f9eb6b64212742b1a35758f5m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _c5ace683f9eb6b64212742b1a35758f5__c5ace683f9eb6b64212742b1a35758f5m_m2861764989 (_c5ace683f9eb6b64212742b1a35758f5_t3018450634 * __this, int32_t ____c5ace683f9eb6b64212742b1a35758f5a0, int32_t ____c5ace683f9eb6b64212742b1a35758f5441, int32_t ____c5ace683f9eb6b64212742b1a35758f5c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
