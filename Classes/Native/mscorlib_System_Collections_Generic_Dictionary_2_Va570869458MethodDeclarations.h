﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Va439486976MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.UInt16,System.String>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define ValueCollection__ctor_m1910996300(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t570869458 *, Dictionary_2_t1870263745 *, const MethodInfo*))ValueCollection__ctor_m4097393658_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.UInt16,System.String>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m2563499366(__this, ___item0, method) ((  void (*) (ValueCollection_t570869458 *, String_t*, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1525649656_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.UInt16,System.String>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m3693638959(__this, method) ((  void (*) (ValueCollection_t570869458 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m637843905_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.UInt16,System.String>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3061388928(__this, ___item0, method) ((  bool (*) (ValueCollection_t570869458 *, String_t*, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m2550912558_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.UInt16,System.String>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m3248869157(__this, ___item0, method) ((  bool (*) (ValueCollection_t570869458 *, String_t*, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m4137722451_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.UInt16,System.String>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m2412770109(__this, method) ((  Il2CppObject* (*) (ValueCollection_t570869458 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m4197418447_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.UInt16,System.String>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m4060981107(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t570869458 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m1121115525_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<System.UInt16,System.String>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m627437614(__this, method) ((  Il2CppObject * (*) (ValueCollection_t570869458 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m3885033152_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.UInt16,System.String>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1336564787(__this, method) ((  bool (*) (ValueCollection_t570869458 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m826088417_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.UInt16,System.String>::System.Collections.ICollection.get_IsSynchronized()
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m2739607187(__this, method) ((  bool (*) (ValueCollection_t570869458 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m1823304513_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<System.UInt16,System.String>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m1802626111(__this, method) ((  Il2CppObject * (*) (ValueCollection_t570869458 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m799331181_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.UInt16,System.String>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m1163415187(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t570869458 *, StringU5BU5D_t4054002952*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m3833289153_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.UInt16,System.String>::GetEnumerator()
#define ValueCollection_GetEnumerator_m569448630(__this, method) ((  Enumerator_t4097064449  (*) (ValueCollection_t570869458 *, const MethodInfo*))ValueCollection_GetEnumerator_m294595684_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<System.UInt16,System.String>::get_Count()
#define ValueCollection_get_Count_m2589170137(__this, method) ((  int32_t (*) (ValueCollection_t570869458 *, const MethodInfo*))ValueCollection_get_Count_m4252677255_gshared)(__this, method)
