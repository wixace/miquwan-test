﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Comparison`1<Pathfinding.IntRect>
struct Comparison_1_t1731419448;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "AssemblyU2DCSharp_Pathfinding_IntRect3015058261.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void System.Comparison`1<Pathfinding.IntRect>::.ctor(System.Object,System.IntPtr)
extern "C"  void Comparison_1__ctor_m3553316584_gshared (Comparison_1_t1731419448 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define Comparison_1__ctor_m3553316584(__this, ___object0, ___method1, method) ((  void (*) (Comparison_1_t1731419448 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Comparison_1__ctor_m3553316584_gshared)(__this, ___object0, ___method1, method)
// System.Int32 System.Comparison`1<Pathfinding.IntRect>::Invoke(T,T)
extern "C"  int32_t Comparison_1_Invoke_m3661433944_gshared (Comparison_1_t1731419448 * __this, IntRect_t3015058261  ___x0, IntRect_t3015058261  ___y1, const MethodInfo* method);
#define Comparison_1_Invoke_m3661433944(__this, ___x0, ___y1, method) ((  int32_t (*) (Comparison_1_t1731419448 *, IntRect_t3015058261 , IntRect_t3015058261 , const MethodInfo*))Comparison_1_Invoke_m3661433944_gshared)(__this, ___x0, ___y1, method)
// System.IAsyncResult System.Comparison`1<Pathfinding.IntRect>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Comparison_1_BeginInvoke_m934083537_gshared (Comparison_1_t1731419448 * __this, IntRect_t3015058261  ___x0, IntRect_t3015058261  ___y1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method);
#define Comparison_1_BeginInvoke_m934083537(__this, ___x0, ___y1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Comparison_1_t1731419448 *, IntRect_t3015058261 , IntRect_t3015058261 , AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))Comparison_1_BeginInvoke_m934083537_gshared)(__this, ___x0, ___y1, ___callback2, ___object3, method)
// System.Int32 System.Comparison`1<Pathfinding.IntRect>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t Comparison_1_EndInvoke_m2692240660_gshared (Comparison_1_t1731419448 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define Comparison_1_EndInvoke_m2692240660(__this, ___result0, method) ((  int32_t (*) (Comparison_1_t1731419448 *, Il2CppObject *, const MethodInfo*))Comparison_1_EndInvoke_m2692240660_gshared)(__this, ___result0, method)
