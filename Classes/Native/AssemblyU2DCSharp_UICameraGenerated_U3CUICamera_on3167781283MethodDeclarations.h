﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UICameraGenerated/<UICamera_onDragOut_GetDelegate_member51_arg0>c__AnonStoreyAC
struct U3CUICamera_onDragOut_GetDelegate_member51_arg0U3Ec__AnonStoreyAC_t3167781283;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"

// System.Void UICameraGenerated/<UICamera_onDragOut_GetDelegate_member51_arg0>c__AnonStoreyAC::.ctor()
extern "C"  void U3CUICamera_onDragOut_GetDelegate_member51_arg0U3Ec__AnonStoreyAC__ctor_m2024811816 (U3CUICamera_onDragOut_GetDelegate_member51_arg0U3Ec__AnonStoreyAC_t3167781283 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICameraGenerated/<UICamera_onDragOut_GetDelegate_member51_arg0>c__AnonStoreyAC::<>m__11C(UnityEngine.GameObject,UnityEngine.GameObject)
extern "C"  void U3CUICamera_onDragOut_GetDelegate_member51_arg0U3Ec__AnonStoreyAC_U3CU3Em__11C_m484041234 (U3CUICamera_onDragOut_GetDelegate_member51_arg0U3Ec__AnonStoreyAC_t3167781283 * __this, GameObject_t3674682005 * ___go0, GameObject_t3674682005 * ___obj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
