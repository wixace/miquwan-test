﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_borvaslawTaijee21
struct M_borvaslawTaijee21_t1400095756;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"

// System.Void GarbageiOS.M_borvaslawTaijee21::.ctor()
extern "C"  void M_borvaslawTaijee21__ctor_m2155713239 (M_borvaslawTaijee21_t1400095756 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_borvaslawTaijee21::M_gelxalllee0(System.String[],System.Int32)
extern "C"  void M_borvaslawTaijee21_M_gelxalllee0_m4235980213 (M_borvaslawTaijee21_t1400095756 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_borvaslawTaijee21::M_joumisnooSteesel1(System.String[],System.Int32)
extern "C"  void M_borvaslawTaijee21_M_joumisnooSteesel1_m258088441 (M_borvaslawTaijee21_t1400095756 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
