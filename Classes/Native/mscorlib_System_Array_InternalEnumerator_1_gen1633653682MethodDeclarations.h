﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1633653682.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22851311006.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<UIModelDisplayType,System.Int32>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m827603402_gshared (InternalEnumerator_1_t1633653682 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m827603402(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t1633653682 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m827603402_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<UIModelDisplayType,System.Int32>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2162721686_gshared (InternalEnumerator_1_t1633653682 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2162721686(__this, method) ((  void (*) (InternalEnumerator_1_t1633653682 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2162721686_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<UIModelDisplayType,System.Int32>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m559020930_gshared (InternalEnumerator_1_t1633653682 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m559020930(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t1633653682 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m559020930_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<UIModelDisplayType,System.Int32>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3452934177_gshared (InternalEnumerator_1_t1633653682 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m3452934177(__this, method) ((  void (*) (InternalEnumerator_1_t1633653682 *, const MethodInfo*))InternalEnumerator_1_Dispose_m3452934177_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<UIModelDisplayType,System.Int32>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m943211778_gshared (InternalEnumerator_1_t1633653682 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m943211778(__this, method) ((  bool (*) (InternalEnumerator_1_t1633653682 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m943211778_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<UIModelDisplayType,System.Int32>>::get_Current()
extern "C"  KeyValuePair_2_t2851311006  InternalEnumerator_1_get_Current_m2132924689_gshared (InternalEnumerator_1_t1633653682 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m2132924689(__this, method) ((  KeyValuePair_2_t2851311006  (*) (InternalEnumerator_1_t1633653682 *, const MethodInfo*))InternalEnumerator_1_get_Current_m2132924689_gshared)(__this, method)
