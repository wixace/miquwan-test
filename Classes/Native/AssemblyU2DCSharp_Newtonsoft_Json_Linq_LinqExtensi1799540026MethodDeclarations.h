﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Linq.LinqExtensions/<Values>c__Iterator1E`2<System.Object,System.Object>
struct U3CValuesU3Ec__Iterator1E_2_t1799540026;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t1787714124;

#include "codegen/il2cpp-codegen.h"

// System.Void Newtonsoft.Json.Linq.LinqExtensions/<Values>c__Iterator1E`2<System.Object,System.Object>::.ctor()
extern "C"  void U3CValuesU3Ec__Iterator1E_2__ctor_m364553862_gshared (U3CValuesU3Ec__Iterator1E_2_t1799540026 * __this, const MethodInfo* method);
#define U3CValuesU3Ec__Iterator1E_2__ctor_m364553862(__this, method) ((  void (*) (U3CValuesU3Ec__Iterator1E_2_t1799540026 *, const MethodInfo*))U3CValuesU3Ec__Iterator1E_2__ctor_m364553862_gshared)(__this, method)
// U Newtonsoft.Json.Linq.LinqExtensions/<Values>c__Iterator1E`2<System.Object,System.Object>::System.Collections.Generic.IEnumerator<U>.get_Current()
extern "C"  Il2CppObject * U3CValuesU3Ec__Iterator1E_2_System_Collections_Generic_IEnumeratorU3CUU3E_get_Current_m3168496399_gshared (U3CValuesU3Ec__Iterator1E_2_t1799540026 * __this, const MethodInfo* method);
#define U3CValuesU3Ec__Iterator1E_2_System_Collections_Generic_IEnumeratorU3CUU3E_get_Current_m3168496399(__this, method) ((  Il2CppObject * (*) (U3CValuesU3Ec__Iterator1E_2_t1799540026 *, const MethodInfo*))U3CValuesU3Ec__Iterator1E_2_System_Collections_Generic_IEnumeratorU3CUU3E_get_Current_m3168496399_gshared)(__this, method)
// System.Object Newtonsoft.Json.Linq.LinqExtensions/<Values>c__Iterator1E`2<System.Object,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CValuesU3Ec__Iterator1E_2_System_Collections_IEnumerator_get_Current_m2638088106_gshared (U3CValuesU3Ec__Iterator1E_2_t1799540026 * __this, const MethodInfo* method);
#define U3CValuesU3Ec__Iterator1E_2_System_Collections_IEnumerator_get_Current_m2638088106(__this, method) ((  Il2CppObject * (*) (U3CValuesU3Ec__Iterator1E_2_t1799540026 *, const MethodInfo*))U3CValuesU3Ec__Iterator1E_2_System_Collections_IEnumerator_get_Current_m2638088106_gshared)(__this, method)
// System.Collections.IEnumerator Newtonsoft.Json.Linq.LinqExtensions/<Values>c__Iterator1E`2<System.Object,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CValuesU3Ec__Iterator1E_2_System_Collections_IEnumerable_GetEnumerator_m2888959915_gshared (U3CValuesU3Ec__Iterator1E_2_t1799540026 * __this, const MethodInfo* method);
#define U3CValuesU3Ec__Iterator1E_2_System_Collections_IEnumerable_GetEnumerator_m2888959915(__this, method) ((  Il2CppObject * (*) (U3CValuesU3Ec__Iterator1E_2_t1799540026 *, const MethodInfo*))U3CValuesU3Ec__Iterator1E_2_System_Collections_IEnumerable_GetEnumerator_m2888959915_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<U> Newtonsoft.Json.Linq.LinqExtensions/<Values>c__Iterator1E`2<System.Object,System.Object>::System.Collections.Generic.IEnumerable<U>.GetEnumerator()
extern "C"  Il2CppObject* U3CValuesU3Ec__Iterator1E_2_System_Collections_Generic_IEnumerableU3CUU3E_GetEnumerator_m1779296022_gshared (U3CValuesU3Ec__Iterator1E_2_t1799540026 * __this, const MethodInfo* method);
#define U3CValuesU3Ec__Iterator1E_2_System_Collections_Generic_IEnumerableU3CUU3E_GetEnumerator_m1779296022(__this, method) ((  Il2CppObject* (*) (U3CValuesU3Ec__Iterator1E_2_t1799540026 *, const MethodInfo*))U3CValuesU3Ec__Iterator1E_2_System_Collections_Generic_IEnumerableU3CUU3E_GetEnumerator_m1779296022_gshared)(__this, method)
// System.Boolean Newtonsoft.Json.Linq.LinqExtensions/<Values>c__Iterator1E`2<System.Object,System.Object>::MoveNext()
extern "C"  bool U3CValuesU3Ec__Iterator1E_2_MoveNext_m201145750_gshared (U3CValuesU3Ec__Iterator1E_2_t1799540026 * __this, const MethodInfo* method);
#define U3CValuesU3Ec__Iterator1E_2_MoveNext_m201145750(__this, method) ((  bool (*) (U3CValuesU3Ec__Iterator1E_2_t1799540026 *, const MethodInfo*))U3CValuesU3Ec__Iterator1E_2_MoveNext_m201145750_gshared)(__this, method)
// System.Void Newtonsoft.Json.Linq.LinqExtensions/<Values>c__Iterator1E`2<System.Object,System.Object>::Dispose()
extern "C"  void U3CValuesU3Ec__Iterator1E_2_Dispose_m2342193475_gshared (U3CValuesU3Ec__Iterator1E_2_t1799540026 * __this, const MethodInfo* method);
#define U3CValuesU3Ec__Iterator1E_2_Dispose_m2342193475(__this, method) ((  void (*) (U3CValuesU3Ec__Iterator1E_2_t1799540026 *, const MethodInfo*))U3CValuesU3Ec__Iterator1E_2_Dispose_m2342193475_gshared)(__this, method)
// System.Void Newtonsoft.Json.Linq.LinqExtensions/<Values>c__Iterator1E`2<System.Object,System.Object>::Reset()
extern "C"  void U3CValuesU3Ec__Iterator1E_2_Reset_m2305954099_gshared (U3CValuesU3Ec__Iterator1E_2_t1799540026 * __this, const MethodInfo* method);
#define U3CValuesU3Ec__Iterator1E_2_Reset_m2305954099(__this, method) ((  void (*) (U3CValuesU3Ec__Iterator1E_2_t1799540026 *, const MethodInfo*))U3CValuesU3Ec__Iterator1E_2_Reset_m2305954099_gshared)(__this, method)
