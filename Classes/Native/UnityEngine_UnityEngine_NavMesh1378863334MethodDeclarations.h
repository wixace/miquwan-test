﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.NavMesh
struct NavMesh_t1378863334;
// UnityEngine.NavMeshPath
struct NavMeshPath_t384806059;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_NavMeshHit1624713351.h"
#include "UnityEngine_UnityEngine_NavMeshPath384806059.h"
#include "mscorlib_System_String7231557.h"
#include "UnityEngine_UnityEngine_NavMeshTriangulation2646108099.h"

// System.Void UnityEngine.NavMesh::.ctor()
extern "C"  void NavMesh__ctor_m1256906409 (NavMesh_t1378863334 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.NavMesh::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.NavMeshHit&,System.Int32)
extern "C"  bool NavMesh_Raycast_m2185189825 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___sourcePosition0, Vector3_t4282066566  ___targetPosition1, NavMeshHit_t1624713351 * ___hit2, int32_t ___areaMask3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.NavMesh::INTERNAL_CALL_Raycast(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.NavMeshHit&,System.Int32)
extern "C"  bool NavMesh_INTERNAL_CALL_Raycast_m197836392 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566 * ___sourcePosition0, Vector3_t4282066566 * ___targetPosition1, NavMeshHit_t1624713351 * ___hit2, int32_t ___areaMask3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.NavMesh::CalculatePath(UnityEngine.Vector3,UnityEngine.Vector3,System.Int32,UnityEngine.NavMeshPath)
extern "C"  bool NavMesh_CalculatePath_m544200337 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___sourcePosition0, Vector3_t4282066566  ___targetPosition1, int32_t ___areaMask2, NavMeshPath_t384806059 * ___path3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.NavMesh::CalculatePathInternal(UnityEngine.Vector3,UnityEngine.Vector3,System.Int32,UnityEngine.NavMeshPath)
extern "C"  bool NavMesh_CalculatePathInternal_m3391998894 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___sourcePosition0, Vector3_t4282066566  ___targetPosition1, int32_t ___areaMask2, NavMeshPath_t384806059 * ___path3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.NavMesh::INTERNAL_CALL_CalculatePathInternal(UnityEngine.Vector3&,UnityEngine.Vector3&,System.Int32,UnityEngine.NavMeshPath)
extern "C"  bool NavMesh_INTERNAL_CALL_CalculatePathInternal_m3311777493 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566 * ___sourcePosition0, Vector3_t4282066566 * ___targetPosition1, int32_t ___areaMask2, NavMeshPath_t384806059 * ___path3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.NavMesh::FindClosestEdge(UnityEngine.Vector3,UnityEngine.NavMeshHit&,System.Int32)
extern "C"  bool NavMesh_FindClosestEdge_m1073112264 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___sourcePosition0, NavMeshHit_t1624713351 * ___hit1, int32_t ___areaMask2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.NavMesh::INTERNAL_CALL_FindClosestEdge(UnityEngine.Vector3&,UnityEngine.NavMeshHit&,System.Int32)
extern "C"  bool NavMesh_INTERNAL_CALL_FindClosestEdge_m3372370873 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566 * ___sourcePosition0, NavMeshHit_t1624713351 * ___hit1, int32_t ___areaMask2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.NavMesh::SamplePosition(UnityEngine.Vector3,UnityEngine.NavMeshHit&,System.Single,System.Int32)
extern "C"  bool NavMesh_SamplePosition_m3613972513 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___sourcePosition0, NavMeshHit_t1624713351 * ___hit1, float ___maxDistance2, int32_t ___areaMask3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.NavMesh::INTERNAL_CALL_SamplePosition(UnityEngine.Vector3&,UnityEngine.NavMeshHit&,System.Single,System.Int32)
extern "C"  bool NavMesh_INTERNAL_CALL_SamplePosition_m3930132984 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566 * ___sourcePosition0, NavMeshHit_t1624713351 * ___hit1, float ___maxDistance2, int32_t ___areaMask3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.NavMesh::SetAreaCost(System.Int32,System.Single)
extern "C"  void NavMesh_SetAreaCost_m2203766137 (Il2CppObject * __this /* static, unused */, int32_t ___areaIndex0, float ___cost1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.NavMesh::GetAreaCost(System.Int32)
extern "C"  float NavMesh_GetAreaCost_m1934323604 (Il2CppObject * __this /* static, unused */, int32_t ___areaIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.NavMesh::GetAreaFromName(System.String)
extern "C"  int32_t NavMesh_GetAreaFromName_m488643363 (Il2CppObject * __this /* static, unused */, String_t* ___areaName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.NavMeshTriangulation UnityEngine.NavMesh::CalculateTriangulation()
extern "C"  NavMeshTriangulation_t2646108099  NavMesh_CalculateTriangulation_m2298527275 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine.NavMesh::TriangulateInternal()
extern "C"  Il2CppObject * NavMesh_TriangulateInternal_m3494293573 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.NavMesh::get_avoidancePredictionTime()
extern "C"  float NavMesh_get_avoidancePredictionTime_m1156188458 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.NavMesh::set_avoidancePredictionTime(System.Single)
extern "C"  void NavMesh_set_avoidancePredictionTime_m1612662465 (Il2CppObject * __this /* static, unused */, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.NavMesh::SetAvoidancePredictionTime(System.Single)
extern "C"  void NavMesh_SetAvoidancePredictionTime_m2131241266 (Il2CppObject * __this /* static, unused */, float ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.NavMesh::GetAvoidancePredictionTime()
extern "C"  float NavMesh_GetAvoidancePredictionTime_m3379302169 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.NavMesh::get_pathfindingIterationsPerFrame()
extern "C"  int32_t NavMesh_get_pathfindingIterationsPerFrame_m2174848136 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.NavMesh::set_pathfindingIterationsPerFrame(System.Int32)
extern "C"  void NavMesh_set_pathfindingIterationsPerFrame_m376705125 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.NavMesh::SetPathfindingIterationsPerFrame(System.Int32)
extern "C"  void NavMesh_SetPathfindingIterationsPerFrame_m1889275028 (Il2CppObject * __this /* static, unused */, int32_t ___iter0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.NavMesh::GetPathfindingIterationsPerFrame()
extern "C"  int32_t NavMesh_GetPathfindingIterationsPerFrame_m1337225679 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
