﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Int32>
struct Dictionary_2_t2731505821;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Va663339229.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Pathfinding.Int3,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m203045118_gshared (Enumerator_t663339229 * __this, Dictionary_2_t2731505821 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m203045118(__this, ___host0, method) ((  void (*) (Enumerator_t663339229 *, Dictionary_2_t2731505821 *, const MethodInfo*))Enumerator__ctor_m203045118_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Pathfinding.Int3,System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3122800803_gshared (Enumerator_t663339229 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m3122800803(__this, method) ((  Il2CppObject * (*) (Enumerator_t663339229 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3122800803_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Pathfinding.Int3,System.Int32>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2287465719_gshared (Enumerator_t663339229 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m2287465719(__this, method) ((  void (*) (Enumerator_t663339229 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2287465719_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Pathfinding.Int3,System.Int32>::Dispose()
extern "C"  void Enumerator_Dispose_m825121568_gshared (Enumerator_t663339229 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m825121568(__this, method) ((  void (*) (Enumerator_t663339229 *, const MethodInfo*))Enumerator_Dispose_m825121568_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Pathfinding.Int3,System.Int32>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3151515363_gshared (Enumerator_t663339229 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m3151515363(__this, method) ((  bool (*) (Enumerator_t663339229 *, const MethodInfo*))Enumerator_MoveNext_m3151515363_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Pathfinding.Int3,System.Int32>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m1349919167_gshared (Enumerator_t663339229 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m1349919167(__this, method) ((  int32_t (*) (Enumerator_t663339229 *, const MethodInfo*))Enumerator_get_Current_m1349919167_gshared)(__this, method)
