//
//  MiquwanAtMihua.m
//  Pengsongbei
//
//  Created by songbei on 2018/12/15.
//  Copyright © 2018年  All rights reserved.
//

#import "NewMiquwan.h"


@implementation NewMiquwan

static NewMiquwan *xam = nil;


-(id)init{
    self = [super init];
    return self;
}

+(NewMiquwan *)instance{
    if(xam == nil){
        xam = [[NewMiquwan alloc]init];
    }
    return xam;
}

//******************************************SDK函数******************************************
//SDK初始化 未用
-(void)MQWSDKInit:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions{
    [self addNoties];
}
- (void)addNoties {
    // SDK初始化结果通知
    [self addNotiWithName:MQWInitResultNotification sel:@selector(platformActiveHandler:)];
    // SDK注册成功通知
    [self addNotiWithName:MQWRegisterFinishedNotification sel:@selector(registSuccessHandler:)];
    // 登录成功
    [self addNotiWithName:MQWLoginFinishedNotification sel:@selector(loginSuccessHandler:)];
    // 注销成功
    [self addNotiWithName:MQWLogoutFinishedNotification sel:@selector(logoutSuccessHandler:)];
    // 内购失败
    [self addNotiWithName:IAPFailedNotification sel:@selector(payFailHandler:)];
    // 内购成功
    [self addNotiWithName:IAPFinishedNotification sel:@selector(paySuccessHandler:)];
    
}

- (void)addNotiWithName:(NSNotificationName)name sel:(SEL)sel {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:sel name:name object:nil];
}


#pragma mark ---- Handler

- (void)platformActiveHandler:(NSNotification *)noti {
    NSLog(@"初始化");
}



/// 新用户注册成功后无需在进行登录操作m，因此需要在这里进行新用户登录处理
- (void)registSuccessHandler:(NSNotification *)noti {
    NSLog(@"新用户信息：\n %@", noti.object);
}
- (void)loginSuccessHandler:(NSNotification *)noti {
    NSDictionary *info = noti.object;
    NSString *uid  = info[@"userId"];
    NSString *account  = info[@"userAccount"];
    NSString *token  = info[@"token"];
    NSLog(@"UID=%@\n\
          account=%@\n\
          token=%@", uid, account, token);
    SendMsg2Unity("OnLoginSuccess",[[NSString stringWithFormat:@"%@*%@", uid, token] UTF8String]);
}

- (void)logoutSuccessHandler:(NSNotification *)noti {
    [MQWPlatform loginPlatformNeedAutoLogin:NO];
}


- (void)payFailHandler:(NSNotification *)noti {
    NSError *error = noti.object;
    NSLog(@"支付失败：%@", error.localizedDescription);
    [MQWHud showAlertMsg:error.localizedDescription];
}

- (void)paySuccessHandler:(NSNotification *)noti {
    [MQWHud showInfo:@"交易成功"];
    //成功后服务器对接请详细参考《SDK服务器对接文档-v1.0.1.pdf》
}
//SDK初始化
-(void) MQWSIn:(NSString *)appId
{
    NSLog(@"appId:%@",appId);
    [MQWPlatform activePlatformWithAppId:@"1108" currencyISO:nil];
}

//角色信息上传
-(void) NewMQWSDKLoginRole:(NSString *)playerId
                playerName:(NSString *)playerName
               playerLevel:(NSString *)playerLevel
                    vipLv:(NSString *)vipLv
                  serverId:(NSString *)serverId
                serverName:(NSString *)serverName
                type:(NSString *)type
{
    NSLog(@"playerId:%@",playerId);
    NSLog(@"playerName:%@",playerName);
    NSLog(@"playerLevel:%@",playerLevel);
    NSLog(@"vipLv:%@",vipLv);
    NSLog(@"serverId:%@",serverId);
    NSLog(@"serverName:%@",serverName);
    NSLog(@"type:%@",type);
    MQWRole *role = [MQWRole createRoleWithServerId:serverId
                         serverName:serverName
                       gameRoleName:playerName
                         gameRoleID:playerId
                      gameRoleLevel:@([playerLevel integerValue])
                           vipLevel:@([vipLv integerValue])
                             source:type];
    [MQWPlatform uploadRole:role handler:^(BOOL flag, NSError * error) {
        if (error) {
            NSLog(@"uploadRole-ERROR:\n%@", error.localizedDescription);
            return ;
        }
    }];
}
//SDK登陆
-(void)MQWL {
    //YES-尝试自动登录,成功则不弹出登录界面，失败则弹出登录界面
    [MQWPlatform loginPlatformNeedAutoLogin:NO];

}
//SDK登出
-(void)MQWLogout {
    [MQWPlatform logoutPlatform];
}
//SDK支付
-(void)MQWP:(NSString *)productId
     productName:(NSString *)productName
          roleId:(NSString *)roleId
        roleName:(NSString *)roleName
       rolelevel:(NSString *)roleLevel
           price:(NSString *)Price
      extendInfo:(NSString *)extendInfo
        serverId:(NSString *)serverId
      serverName:(NSString *)serverName
            gold:(NSString *)gold
           vipLv:(NSString *)vipLv
         orderId:(NSString *)orderId{
    NSLog(@"开始打印支付信息-----------------");
    NSLog(@"productId:%@",productId);
    NSLog(@"productName:%@",productName);
    NSLog(@"Price:%@",Price);
    NSLog(@"serverId:%@",serverId);
    NSLog(@"outOrder:%@",orderId);
    NSLog(@"roleId:%@",roleId);
    NSLog(@"roleName:%@",roleName);
    NSLog(@"extendInfo:%@",extendInfo);
    NSLog(@"gold:%@",gold);
    NSLog(@"vipLv:%@",vipLv);
    NSLog(@"支付信息打印完成-----------------");
    MQWOrder *order = [MQWOrder createOrderWithGoodsID:productId
                                             goodsName:productName
                                             cpOrderID:orderId
                                                 count:@([gold integerValue])
                                                 money:@([Price integerValue])
                                          callBackInfo:extendInfo];
    MQWRole *role   = [MQWRole createRoleWithServerId:serverId
                                           serverName:serverName
                                         gameRoleName:roleName
                                           gameRoleID:roleId
                                            gameRoleLevel:@([roleLevel integerValue])
                                             vipLevel:@([vipLv integerValue])
                                               source:@""];
    [MQWHud show];
    [MQWPlatform purchaseOrder:order role:role];
    NSLog(@"order:%@",order);
    //NSLog(@"outOrder:%@",outOrder);
}
/*********************************start c fun for unity ***************************************************************************/
#if defined(__cplusplus)
extern "C"{
#endif
    //初始化
    void MQWIn(char *appId){
        [[NewMiquwan instance] MQWSIn:_CreateNSString(appId)];
    }
    //角色信息数据上传
    void MQWR(char *playerId,
                      char *playerName,
                      char *playerLevel,
                      char *vipLv,
                      char *serverId,
                      char *serverName,
                      char *type){
        [[NewMiquwan instance] NewMQWSDKLoginRole:_CreateNSString(playerId)
                                              playerName:_CreateNSString(playerName)
                                             playerLevel:_CreateNSString(playerLevel)
                                                  vipLv:_CreateNSString(vipLv)
                                                serverId:_CreateNSString(serverId)
                                              serverName:_CreateNSString(serverName)
                                                    type:_CreateNSString(type)];
        NSLog(@"C#调用角色信息数据上传");
    }
    //登陆
    void MQWL(){
        [[NewMiquwan instance] MQWL];
    }
    void logout(){
        [[NewMiquwan instance] MQWLogout];
    }
    
    //发起购买
    void MQWP(char *productId,
                   char *productName,
                   char *roleId,
                   char *roleName,
                   char *roleLevel,
                   char *Price,
                   char *extendInfo,
                   char *serverId,
                  char *serverName,
                  char *gold,
                  char *vipLv,
                  char *orderId){
        [[NewMiquwan instance] MQWP:_CreateNSString(productId)
                                    productName:_CreateNSString(productName)
                                         roleId:_CreateNSString(roleId)
                                       roleName:_CreateNSString(roleName)
                                      rolelevel:_CreateNSString(roleLevel)
                                          price:_CreateNSString(Price)
                                     extendInfo:_CreateNSString(extendInfo)
                                       serverId:_CreateNSString(serverId)
                                serverName:_CreateNSString(serverName)                                     gold:_CreateNSString(gold)
                                  vipLv:_CreateNSString(vipLv)
                                orderId:_CreateNSString(orderId)];
        NSLog(@"C#发起购买");
    }
    
#if defined(__cplusplus)
}
#endif
@end
