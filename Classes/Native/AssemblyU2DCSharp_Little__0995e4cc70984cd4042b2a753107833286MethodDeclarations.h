﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._0995e4cc70984cd4042b2a750db17f6e
struct _0995e4cc70984cd4042b2a750db17f6e_t3107833286;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._0995e4cc70984cd4042b2a750db17f6e::.ctor()
extern "C"  void _0995e4cc70984cd4042b2a750db17f6e__ctor_m2348842791 (_0995e4cc70984cd4042b2a750db17f6e_t3107833286 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._0995e4cc70984cd4042b2a750db17f6e::_0995e4cc70984cd4042b2a750db17f6em2(System.Int32)
extern "C"  int32_t _0995e4cc70984cd4042b2a750db17f6e__0995e4cc70984cd4042b2a750db17f6em2_m4142533593 (_0995e4cc70984cd4042b2a750db17f6e_t3107833286 * __this, int32_t ____0995e4cc70984cd4042b2a750db17f6ea0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._0995e4cc70984cd4042b2a750db17f6e::_0995e4cc70984cd4042b2a750db17f6em(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _0995e4cc70984cd4042b2a750db17f6e__0995e4cc70984cd4042b2a750db17f6em_m4028408573 (_0995e4cc70984cd4042b2a750db17f6e_t3107833286 * __this, int32_t ____0995e4cc70984cd4042b2a750db17f6ea0, int32_t ____0995e4cc70984cd4042b2a750db17f6e351, int32_t ____0995e4cc70984cd4042b2a750db17f6ec2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
