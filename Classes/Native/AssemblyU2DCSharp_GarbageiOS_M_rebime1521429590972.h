﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_rebime152
struct  M_rebime152_t1429590972  : public Il2CppObject
{
public:
	// System.String GarbageiOS.M_rebime152::_bowxa
	String_t* ____bowxa_0;
	// System.Int32 GarbageiOS.M_rebime152::_saqemror
	int32_t ____saqemror_1;
	// System.UInt32 GarbageiOS.M_rebime152::_hinasGeadreqai
	uint32_t ____hinasGeadreqai_2;
	// System.Boolean GarbageiOS.M_rebime152::_droorearla
	bool ____droorearla_3;
	// System.Boolean GarbageiOS.M_rebime152::_nerferfisStortawcis
	bool ____nerferfisStortawcis_4;
	// System.Int32 GarbageiOS.M_rebime152::_senece
	int32_t ____senece_5;
	// System.Single GarbageiOS.M_rebime152::_vemasci
	float ____vemasci_6;
	// System.Int32 GarbageiOS.M_rebime152::_kemdraReasou
	int32_t ____kemdraReasou_7;
	// System.String GarbageiOS.M_rebime152::_chorqar
	String_t* ____chorqar_8;
	// System.String GarbageiOS.M_rebime152::_doorowRermi
	String_t* ____doorowRermi_9;
	// System.Boolean GarbageiOS.M_rebime152::_sisrelSeltanoo
	bool ____sisrelSeltanoo_10;
	// System.UInt32 GarbageiOS.M_rebime152::_zicaSerjacem
	uint32_t ____zicaSerjacem_11;
	// System.Single GarbageiOS.M_rebime152::_berdramawLererepe
	float ____berdramawLererepe_12;
	// System.Boolean GarbageiOS.M_rebime152::_jorjelmis
	bool ____jorjelmis_13;
	// System.UInt32 GarbageiOS.M_rebime152::_rolaZercowti
	uint32_t ____rolaZercowti_14;
	// System.Boolean GarbageiOS.M_rebime152::_mooreekow
	bool ____mooreekow_15;
	// System.Single GarbageiOS.M_rebime152::_taiturmis
	float ____taiturmis_16;
	// System.UInt32 GarbageiOS.M_rebime152::_steacooFallnar
	uint32_t ____steacooFallnar_17;
	// System.String GarbageiOS.M_rebime152::_caixirlemChairlor
	String_t* ____caixirlemChairlor_18;

public:
	inline static int32_t get_offset_of__bowxa_0() { return static_cast<int32_t>(offsetof(M_rebime152_t1429590972, ____bowxa_0)); }
	inline String_t* get__bowxa_0() const { return ____bowxa_0; }
	inline String_t** get_address_of__bowxa_0() { return &____bowxa_0; }
	inline void set__bowxa_0(String_t* value)
	{
		____bowxa_0 = value;
		Il2CppCodeGenWriteBarrier(&____bowxa_0, value);
	}

	inline static int32_t get_offset_of__saqemror_1() { return static_cast<int32_t>(offsetof(M_rebime152_t1429590972, ____saqemror_1)); }
	inline int32_t get__saqemror_1() const { return ____saqemror_1; }
	inline int32_t* get_address_of__saqemror_1() { return &____saqemror_1; }
	inline void set__saqemror_1(int32_t value)
	{
		____saqemror_1 = value;
	}

	inline static int32_t get_offset_of__hinasGeadreqai_2() { return static_cast<int32_t>(offsetof(M_rebime152_t1429590972, ____hinasGeadreqai_2)); }
	inline uint32_t get__hinasGeadreqai_2() const { return ____hinasGeadreqai_2; }
	inline uint32_t* get_address_of__hinasGeadreqai_2() { return &____hinasGeadreqai_2; }
	inline void set__hinasGeadreqai_2(uint32_t value)
	{
		____hinasGeadreqai_2 = value;
	}

	inline static int32_t get_offset_of__droorearla_3() { return static_cast<int32_t>(offsetof(M_rebime152_t1429590972, ____droorearla_3)); }
	inline bool get__droorearla_3() const { return ____droorearla_3; }
	inline bool* get_address_of__droorearla_3() { return &____droorearla_3; }
	inline void set__droorearla_3(bool value)
	{
		____droorearla_3 = value;
	}

	inline static int32_t get_offset_of__nerferfisStortawcis_4() { return static_cast<int32_t>(offsetof(M_rebime152_t1429590972, ____nerferfisStortawcis_4)); }
	inline bool get__nerferfisStortawcis_4() const { return ____nerferfisStortawcis_4; }
	inline bool* get_address_of__nerferfisStortawcis_4() { return &____nerferfisStortawcis_4; }
	inline void set__nerferfisStortawcis_4(bool value)
	{
		____nerferfisStortawcis_4 = value;
	}

	inline static int32_t get_offset_of__senece_5() { return static_cast<int32_t>(offsetof(M_rebime152_t1429590972, ____senece_5)); }
	inline int32_t get__senece_5() const { return ____senece_5; }
	inline int32_t* get_address_of__senece_5() { return &____senece_5; }
	inline void set__senece_5(int32_t value)
	{
		____senece_5 = value;
	}

	inline static int32_t get_offset_of__vemasci_6() { return static_cast<int32_t>(offsetof(M_rebime152_t1429590972, ____vemasci_6)); }
	inline float get__vemasci_6() const { return ____vemasci_6; }
	inline float* get_address_of__vemasci_6() { return &____vemasci_6; }
	inline void set__vemasci_6(float value)
	{
		____vemasci_6 = value;
	}

	inline static int32_t get_offset_of__kemdraReasou_7() { return static_cast<int32_t>(offsetof(M_rebime152_t1429590972, ____kemdraReasou_7)); }
	inline int32_t get__kemdraReasou_7() const { return ____kemdraReasou_7; }
	inline int32_t* get_address_of__kemdraReasou_7() { return &____kemdraReasou_7; }
	inline void set__kemdraReasou_7(int32_t value)
	{
		____kemdraReasou_7 = value;
	}

	inline static int32_t get_offset_of__chorqar_8() { return static_cast<int32_t>(offsetof(M_rebime152_t1429590972, ____chorqar_8)); }
	inline String_t* get__chorqar_8() const { return ____chorqar_8; }
	inline String_t** get_address_of__chorqar_8() { return &____chorqar_8; }
	inline void set__chorqar_8(String_t* value)
	{
		____chorqar_8 = value;
		Il2CppCodeGenWriteBarrier(&____chorqar_8, value);
	}

	inline static int32_t get_offset_of__doorowRermi_9() { return static_cast<int32_t>(offsetof(M_rebime152_t1429590972, ____doorowRermi_9)); }
	inline String_t* get__doorowRermi_9() const { return ____doorowRermi_9; }
	inline String_t** get_address_of__doorowRermi_9() { return &____doorowRermi_9; }
	inline void set__doorowRermi_9(String_t* value)
	{
		____doorowRermi_9 = value;
		Il2CppCodeGenWriteBarrier(&____doorowRermi_9, value);
	}

	inline static int32_t get_offset_of__sisrelSeltanoo_10() { return static_cast<int32_t>(offsetof(M_rebime152_t1429590972, ____sisrelSeltanoo_10)); }
	inline bool get__sisrelSeltanoo_10() const { return ____sisrelSeltanoo_10; }
	inline bool* get_address_of__sisrelSeltanoo_10() { return &____sisrelSeltanoo_10; }
	inline void set__sisrelSeltanoo_10(bool value)
	{
		____sisrelSeltanoo_10 = value;
	}

	inline static int32_t get_offset_of__zicaSerjacem_11() { return static_cast<int32_t>(offsetof(M_rebime152_t1429590972, ____zicaSerjacem_11)); }
	inline uint32_t get__zicaSerjacem_11() const { return ____zicaSerjacem_11; }
	inline uint32_t* get_address_of__zicaSerjacem_11() { return &____zicaSerjacem_11; }
	inline void set__zicaSerjacem_11(uint32_t value)
	{
		____zicaSerjacem_11 = value;
	}

	inline static int32_t get_offset_of__berdramawLererepe_12() { return static_cast<int32_t>(offsetof(M_rebime152_t1429590972, ____berdramawLererepe_12)); }
	inline float get__berdramawLererepe_12() const { return ____berdramawLererepe_12; }
	inline float* get_address_of__berdramawLererepe_12() { return &____berdramawLererepe_12; }
	inline void set__berdramawLererepe_12(float value)
	{
		____berdramawLererepe_12 = value;
	}

	inline static int32_t get_offset_of__jorjelmis_13() { return static_cast<int32_t>(offsetof(M_rebime152_t1429590972, ____jorjelmis_13)); }
	inline bool get__jorjelmis_13() const { return ____jorjelmis_13; }
	inline bool* get_address_of__jorjelmis_13() { return &____jorjelmis_13; }
	inline void set__jorjelmis_13(bool value)
	{
		____jorjelmis_13 = value;
	}

	inline static int32_t get_offset_of__rolaZercowti_14() { return static_cast<int32_t>(offsetof(M_rebime152_t1429590972, ____rolaZercowti_14)); }
	inline uint32_t get__rolaZercowti_14() const { return ____rolaZercowti_14; }
	inline uint32_t* get_address_of__rolaZercowti_14() { return &____rolaZercowti_14; }
	inline void set__rolaZercowti_14(uint32_t value)
	{
		____rolaZercowti_14 = value;
	}

	inline static int32_t get_offset_of__mooreekow_15() { return static_cast<int32_t>(offsetof(M_rebime152_t1429590972, ____mooreekow_15)); }
	inline bool get__mooreekow_15() const { return ____mooreekow_15; }
	inline bool* get_address_of__mooreekow_15() { return &____mooreekow_15; }
	inline void set__mooreekow_15(bool value)
	{
		____mooreekow_15 = value;
	}

	inline static int32_t get_offset_of__taiturmis_16() { return static_cast<int32_t>(offsetof(M_rebime152_t1429590972, ____taiturmis_16)); }
	inline float get__taiturmis_16() const { return ____taiturmis_16; }
	inline float* get_address_of__taiturmis_16() { return &____taiturmis_16; }
	inline void set__taiturmis_16(float value)
	{
		____taiturmis_16 = value;
	}

	inline static int32_t get_offset_of__steacooFallnar_17() { return static_cast<int32_t>(offsetof(M_rebime152_t1429590972, ____steacooFallnar_17)); }
	inline uint32_t get__steacooFallnar_17() const { return ____steacooFallnar_17; }
	inline uint32_t* get_address_of__steacooFallnar_17() { return &____steacooFallnar_17; }
	inline void set__steacooFallnar_17(uint32_t value)
	{
		____steacooFallnar_17 = value;
	}

	inline static int32_t get_offset_of__caixirlemChairlor_18() { return static_cast<int32_t>(offsetof(M_rebime152_t1429590972, ____caixirlemChairlor_18)); }
	inline String_t* get__caixirlemChairlor_18() const { return ____caixirlemChairlor_18; }
	inline String_t** get_address_of__caixirlemChairlor_18() { return &____caixirlemChairlor_18; }
	inline void set__caixirlemChairlor_18(String_t* value)
	{
		____caixirlemChairlor_18 = value;
		Il2CppCodeGenWriteBarrier(&____caixirlemChairlor_18, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
