﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FS_ShadowSimple
struct FS_ShadowSimple_t4208748868;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t215400611;
// FS_ShadowManager
struct FS_ShadowManager_t363579995;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_FS_ShadowSimple4208748868.h"

// System.Void FS_ShadowSimple::.ctor()
extern "C"  void FS_ShadowSimple__ctor_m1370720935 (FS_ShadowSimple_t4208748868 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3[] FS_ShadowSimple::get_corners()
extern "C"  Vector3U5BU5D_t215400611* FS_ShadowSimple_get_corners_m3974142034 (FS_ShadowSimple_t4208748868 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color FS_ShadowSimple::get_color()
extern "C"  Color_t4194546905  FS_ShadowSimple_get_color_m4161819750 (FS_ShadowSimple_t4208748868 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 FS_ShadowSimple::get_normal()
extern "C"  Vector3_t4282066566  FS_ShadowSimple_get_normal_m3400890195 (FS_ShadowSimple_t4208748868 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FS_ShadowSimple::Awake()
extern "C"  void FS_ShadowSimple_Awake_m1608326154 (FS_ShadowSimple_t4208748868 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FS_ShadowSimple::CalculateShadowGeometry()
extern "C"  void FS_ShadowSimple_CalculateShadowGeometry_m2953716605 (FS_ShadowSimple_t4208748868 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FS_ShadowSimple::Update()
extern "C"  void FS_ShadowSimple_Update_m1269538118 (FS_ShadowSimple_t4208748868 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FS_ShadowSimple::OnDrawGizmos()
extern "C"  void FS_ShadowSimple_OnDrawGizmos_m4245392537 (FS_ShadowSimple_t4208748868 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FS_ShadowManager FS_ShadowSimple::ilo_Manager1()
extern "C"  FS_ShadowManager_t363579995 * FS_ShadowSimple_ilo_Manager1_m2718383186 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FS_ShadowSimple::ilo_CalculateShadowGeometry2(FS_ShadowSimple)
extern "C"  void FS_ShadowSimple_ilo_CalculateShadowGeometry2_m4064650564 (Il2CppObject * __this /* static, unused */, FS_ShadowSimple_t4208748868 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3[] FS_ShadowSimple::ilo_get_corners3(FS_ShadowSimple)
extern "C"  Vector3U5BU5D_t215400611* FS_ShadowSimple_ilo_get_corners3_m2614010296 (Il2CppObject * __this /* static, unused */, FS_ShadowSimple_t4208748868 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
