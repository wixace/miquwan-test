﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_sowheebasJairbivou29
struct  M_sowheebasJairbivou29_t2010660101  : public Il2CppObject
{
public:
	// System.Single GarbageiOS.M_sowheebasJairbivou29::_chuke
	float ____chuke_0;
	// System.Single GarbageiOS.M_sowheebasJairbivou29::_hootallni
	float ____hootallni_1;
	// System.Int32 GarbageiOS.M_sowheebasJairbivou29::_jiwhu
	int32_t ____jiwhu_2;
	// System.Boolean GarbageiOS.M_sowheebasJairbivou29::_dadawxir
	bool ____dadawxir_3;
	// System.String GarbageiOS.M_sowheebasJairbivou29::_semleemiKurcear
	String_t* ____semleemiKurcear_4;
	// System.Int32 GarbageiOS.M_sowheebasJairbivou29::_dearqeXetal
	int32_t ____dearqeXetal_5;
	// System.UInt32 GarbageiOS.M_sowheebasJairbivou29::_tresem
	uint32_t ____tresem_6;
	// System.Single GarbageiOS.M_sowheebasJairbivou29::_corwallwoKereraihe
	float ____corwallwoKereraihe_7;
	// System.Single GarbageiOS.M_sowheebasJairbivou29::_tadairKayere
	float ____tadairKayere_8;
	// System.Boolean GarbageiOS.M_sowheebasJairbivou29::_nujeama
	bool ____nujeama_9;
	// System.Int32 GarbageiOS.M_sowheebasJairbivou29::_tesaw
	int32_t ____tesaw_10;
	// System.Single GarbageiOS.M_sowheebasJairbivou29::_lemsou
	float ____lemsou_11;
	// System.Int32 GarbageiOS.M_sowheebasJairbivou29::_payteyur
	int32_t ____payteyur_12;
	// System.Single GarbageiOS.M_sowheebasJairbivou29::_semtereNerecha
	float ____semtereNerecha_13;
	// System.Single GarbageiOS.M_sowheebasJairbivou29::_lulowjear
	float ____lulowjear_14;
	// System.Single GarbageiOS.M_sowheebasJairbivou29::_finumouKaixa
	float ____finumouKaixa_15;
	// System.String GarbageiOS.M_sowheebasJairbivou29::_chemgenay
	String_t* ____chemgenay_16;
	// System.UInt32 GarbageiOS.M_sowheebasJairbivou29::_daqo
	uint32_t ____daqo_17;
	// System.String GarbageiOS.M_sowheebasJairbivou29::_miyel
	String_t* ____miyel_18;
	// System.Single GarbageiOS.M_sowheebasJairbivou29::_saircee
	float ____saircee_19;
	// System.String GarbageiOS.M_sowheebasJairbivou29::_meerawWhoukay
	String_t* ____meerawWhoukay_20;
	// System.String GarbageiOS.M_sowheebasJairbivou29::_qayvalDisawpear
	String_t* ____qayvalDisawpear_21;
	// System.Int32 GarbageiOS.M_sowheebasJairbivou29::_chalrawSaichelsur
	int32_t ____chalrawSaichelsur_22;
	// System.Boolean GarbageiOS.M_sowheebasJairbivou29::_jereqarho
	bool ____jereqarho_23;
	// System.String GarbageiOS.M_sowheebasJairbivou29::_loupai
	String_t* ____loupai_24;
	// System.Int32 GarbageiOS.M_sowheebasJairbivou29::_goujuwhuLissure
	int32_t ____goujuwhuLissure_25;
	// System.Int32 GarbageiOS.M_sowheebasJairbivou29::_yurlayKega
	int32_t ____yurlayKega_26;
	// System.Int32 GarbageiOS.M_sowheebasJairbivou29::_hideredeaDijourem
	int32_t ____hideredeaDijourem_27;
	// System.Int32 GarbageiOS.M_sowheebasJairbivou29::_sodrortouKayjea
	int32_t ____sodrortouKayjea_28;
	// System.UInt32 GarbageiOS.M_sowheebasJairbivou29::_paydrisHarloulou
	uint32_t ____paydrisHarloulou_29;
	// System.Single GarbageiOS.M_sowheebasJairbivou29::_chougas
	float ____chougas_30;
	// System.Single GarbageiOS.M_sowheebasJairbivou29::_rabarDichea
	float ____rabarDichea_31;
	// System.UInt32 GarbageiOS.M_sowheebasJairbivou29::_puwhoucoLeadosa
	uint32_t ____puwhoucoLeadosa_32;

public:
	inline static int32_t get_offset_of__chuke_0() { return static_cast<int32_t>(offsetof(M_sowheebasJairbivou29_t2010660101, ____chuke_0)); }
	inline float get__chuke_0() const { return ____chuke_0; }
	inline float* get_address_of__chuke_0() { return &____chuke_0; }
	inline void set__chuke_0(float value)
	{
		____chuke_0 = value;
	}

	inline static int32_t get_offset_of__hootallni_1() { return static_cast<int32_t>(offsetof(M_sowheebasJairbivou29_t2010660101, ____hootallni_1)); }
	inline float get__hootallni_1() const { return ____hootallni_1; }
	inline float* get_address_of__hootallni_1() { return &____hootallni_1; }
	inline void set__hootallni_1(float value)
	{
		____hootallni_1 = value;
	}

	inline static int32_t get_offset_of__jiwhu_2() { return static_cast<int32_t>(offsetof(M_sowheebasJairbivou29_t2010660101, ____jiwhu_2)); }
	inline int32_t get__jiwhu_2() const { return ____jiwhu_2; }
	inline int32_t* get_address_of__jiwhu_2() { return &____jiwhu_2; }
	inline void set__jiwhu_2(int32_t value)
	{
		____jiwhu_2 = value;
	}

	inline static int32_t get_offset_of__dadawxir_3() { return static_cast<int32_t>(offsetof(M_sowheebasJairbivou29_t2010660101, ____dadawxir_3)); }
	inline bool get__dadawxir_3() const { return ____dadawxir_3; }
	inline bool* get_address_of__dadawxir_3() { return &____dadawxir_3; }
	inline void set__dadawxir_3(bool value)
	{
		____dadawxir_3 = value;
	}

	inline static int32_t get_offset_of__semleemiKurcear_4() { return static_cast<int32_t>(offsetof(M_sowheebasJairbivou29_t2010660101, ____semleemiKurcear_4)); }
	inline String_t* get__semleemiKurcear_4() const { return ____semleemiKurcear_4; }
	inline String_t** get_address_of__semleemiKurcear_4() { return &____semleemiKurcear_4; }
	inline void set__semleemiKurcear_4(String_t* value)
	{
		____semleemiKurcear_4 = value;
		Il2CppCodeGenWriteBarrier(&____semleemiKurcear_4, value);
	}

	inline static int32_t get_offset_of__dearqeXetal_5() { return static_cast<int32_t>(offsetof(M_sowheebasJairbivou29_t2010660101, ____dearqeXetal_5)); }
	inline int32_t get__dearqeXetal_5() const { return ____dearqeXetal_5; }
	inline int32_t* get_address_of__dearqeXetal_5() { return &____dearqeXetal_5; }
	inline void set__dearqeXetal_5(int32_t value)
	{
		____dearqeXetal_5 = value;
	}

	inline static int32_t get_offset_of__tresem_6() { return static_cast<int32_t>(offsetof(M_sowheebasJairbivou29_t2010660101, ____tresem_6)); }
	inline uint32_t get__tresem_6() const { return ____tresem_6; }
	inline uint32_t* get_address_of__tresem_6() { return &____tresem_6; }
	inline void set__tresem_6(uint32_t value)
	{
		____tresem_6 = value;
	}

	inline static int32_t get_offset_of__corwallwoKereraihe_7() { return static_cast<int32_t>(offsetof(M_sowheebasJairbivou29_t2010660101, ____corwallwoKereraihe_7)); }
	inline float get__corwallwoKereraihe_7() const { return ____corwallwoKereraihe_7; }
	inline float* get_address_of__corwallwoKereraihe_7() { return &____corwallwoKereraihe_7; }
	inline void set__corwallwoKereraihe_7(float value)
	{
		____corwallwoKereraihe_7 = value;
	}

	inline static int32_t get_offset_of__tadairKayere_8() { return static_cast<int32_t>(offsetof(M_sowheebasJairbivou29_t2010660101, ____tadairKayere_8)); }
	inline float get__tadairKayere_8() const { return ____tadairKayere_8; }
	inline float* get_address_of__tadairKayere_8() { return &____tadairKayere_8; }
	inline void set__tadairKayere_8(float value)
	{
		____tadairKayere_8 = value;
	}

	inline static int32_t get_offset_of__nujeama_9() { return static_cast<int32_t>(offsetof(M_sowheebasJairbivou29_t2010660101, ____nujeama_9)); }
	inline bool get__nujeama_9() const { return ____nujeama_9; }
	inline bool* get_address_of__nujeama_9() { return &____nujeama_9; }
	inline void set__nujeama_9(bool value)
	{
		____nujeama_9 = value;
	}

	inline static int32_t get_offset_of__tesaw_10() { return static_cast<int32_t>(offsetof(M_sowheebasJairbivou29_t2010660101, ____tesaw_10)); }
	inline int32_t get__tesaw_10() const { return ____tesaw_10; }
	inline int32_t* get_address_of__tesaw_10() { return &____tesaw_10; }
	inline void set__tesaw_10(int32_t value)
	{
		____tesaw_10 = value;
	}

	inline static int32_t get_offset_of__lemsou_11() { return static_cast<int32_t>(offsetof(M_sowheebasJairbivou29_t2010660101, ____lemsou_11)); }
	inline float get__lemsou_11() const { return ____lemsou_11; }
	inline float* get_address_of__lemsou_11() { return &____lemsou_11; }
	inline void set__lemsou_11(float value)
	{
		____lemsou_11 = value;
	}

	inline static int32_t get_offset_of__payteyur_12() { return static_cast<int32_t>(offsetof(M_sowheebasJairbivou29_t2010660101, ____payteyur_12)); }
	inline int32_t get__payteyur_12() const { return ____payteyur_12; }
	inline int32_t* get_address_of__payteyur_12() { return &____payteyur_12; }
	inline void set__payteyur_12(int32_t value)
	{
		____payteyur_12 = value;
	}

	inline static int32_t get_offset_of__semtereNerecha_13() { return static_cast<int32_t>(offsetof(M_sowheebasJairbivou29_t2010660101, ____semtereNerecha_13)); }
	inline float get__semtereNerecha_13() const { return ____semtereNerecha_13; }
	inline float* get_address_of__semtereNerecha_13() { return &____semtereNerecha_13; }
	inline void set__semtereNerecha_13(float value)
	{
		____semtereNerecha_13 = value;
	}

	inline static int32_t get_offset_of__lulowjear_14() { return static_cast<int32_t>(offsetof(M_sowheebasJairbivou29_t2010660101, ____lulowjear_14)); }
	inline float get__lulowjear_14() const { return ____lulowjear_14; }
	inline float* get_address_of__lulowjear_14() { return &____lulowjear_14; }
	inline void set__lulowjear_14(float value)
	{
		____lulowjear_14 = value;
	}

	inline static int32_t get_offset_of__finumouKaixa_15() { return static_cast<int32_t>(offsetof(M_sowheebasJairbivou29_t2010660101, ____finumouKaixa_15)); }
	inline float get__finumouKaixa_15() const { return ____finumouKaixa_15; }
	inline float* get_address_of__finumouKaixa_15() { return &____finumouKaixa_15; }
	inline void set__finumouKaixa_15(float value)
	{
		____finumouKaixa_15 = value;
	}

	inline static int32_t get_offset_of__chemgenay_16() { return static_cast<int32_t>(offsetof(M_sowheebasJairbivou29_t2010660101, ____chemgenay_16)); }
	inline String_t* get__chemgenay_16() const { return ____chemgenay_16; }
	inline String_t** get_address_of__chemgenay_16() { return &____chemgenay_16; }
	inline void set__chemgenay_16(String_t* value)
	{
		____chemgenay_16 = value;
		Il2CppCodeGenWriteBarrier(&____chemgenay_16, value);
	}

	inline static int32_t get_offset_of__daqo_17() { return static_cast<int32_t>(offsetof(M_sowheebasJairbivou29_t2010660101, ____daqo_17)); }
	inline uint32_t get__daqo_17() const { return ____daqo_17; }
	inline uint32_t* get_address_of__daqo_17() { return &____daqo_17; }
	inline void set__daqo_17(uint32_t value)
	{
		____daqo_17 = value;
	}

	inline static int32_t get_offset_of__miyel_18() { return static_cast<int32_t>(offsetof(M_sowheebasJairbivou29_t2010660101, ____miyel_18)); }
	inline String_t* get__miyel_18() const { return ____miyel_18; }
	inline String_t** get_address_of__miyel_18() { return &____miyel_18; }
	inline void set__miyel_18(String_t* value)
	{
		____miyel_18 = value;
		Il2CppCodeGenWriteBarrier(&____miyel_18, value);
	}

	inline static int32_t get_offset_of__saircee_19() { return static_cast<int32_t>(offsetof(M_sowheebasJairbivou29_t2010660101, ____saircee_19)); }
	inline float get__saircee_19() const { return ____saircee_19; }
	inline float* get_address_of__saircee_19() { return &____saircee_19; }
	inline void set__saircee_19(float value)
	{
		____saircee_19 = value;
	}

	inline static int32_t get_offset_of__meerawWhoukay_20() { return static_cast<int32_t>(offsetof(M_sowheebasJairbivou29_t2010660101, ____meerawWhoukay_20)); }
	inline String_t* get__meerawWhoukay_20() const { return ____meerawWhoukay_20; }
	inline String_t** get_address_of__meerawWhoukay_20() { return &____meerawWhoukay_20; }
	inline void set__meerawWhoukay_20(String_t* value)
	{
		____meerawWhoukay_20 = value;
		Il2CppCodeGenWriteBarrier(&____meerawWhoukay_20, value);
	}

	inline static int32_t get_offset_of__qayvalDisawpear_21() { return static_cast<int32_t>(offsetof(M_sowheebasJairbivou29_t2010660101, ____qayvalDisawpear_21)); }
	inline String_t* get__qayvalDisawpear_21() const { return ____qayvalDisawpear_21; }
	inline String_t** get_address_of__qayvalDisawpear_21() { return &____qayvalDisawpear_21; }
	inline void set__qayvalDisawpear_21(String_t* value)
	{
		____qayvalDisawpear_21 = value;
		Il2CppCodeGenWriteBarrier(&____qayvalDisawpear_21, value);
	}

	inline static int32_t get_offset_of__chalrawSaichelsur_22() { return static_cast<int32_t>(offsetof(M_sowheebasJairbivou29_t2010660101, ____chalrawSaichelsur_22)); }
	inline int32_t get__chalrawSaichelsur_22() const { return ____chalrawSaichelsur_22; }
	inline int32_t* get_address_of__chalrawSaichelsur_22() { return &____chalrawSaichelsur_22; }
	inline void set__chalrawSaichelsur_22(int32_t value)
	{
		____chalrawSaichelsur_22 = value;
	}

	inline static int32_t get_offset_of__jereqarho_23() { return static_cast<int32_t>(offsetof(M_sowheebasJairbivou29_t2010660101, ____jereqarho_23)); }
	inline bool get__jereqarho_23() const { return ____jereqarho_23; }
	inline bool* get_address_of__jereqarho_23() { return &____jereqarho_23; }
	inline void set__jereqarho_23(bool value)
	{
		____jereqarho_23 = value;
	}

	inline static int32_t get_offset_of__loupai_24() { return static_cast<int32_t>(offsetof(M_sowheebasJairbivou29_t2010660101, ____loupai_24)); }
	inline String_t* get__loupai_24() const { return ____loupai_24; }
	inline String_t** get_address_of__loupai_24() { return &____loupai_24; }
	inline void set__loupai_24(String_t* value)
	{
		____loupai_24 = value;
		Il2CppCodeGenWriteBarrier(&____loupai_24, value);
	}

	inline static int32_t get_offset_of__goujuwhuLissure_25() { return static_cast<int32_t>(offsetof(M_sowheebasJairbivou29_t2010660101, ____goujuwhuLissure_25)); }
	inline int32_t get__goujuwhuLissure_25() const { return ____goujuwhuLissure_25; }
	inline int32_t* get_address_of__goujuwhuLissure_25() { return &____goujuwhuLissure_25; }
	inline void set__goujuwhuLissure_25(int32_t value)
	{
		____goujuwhuLissure_25 = value;
	}

	inline static int32_t get_offset_of__yurlayKega_26() { return static_cast<int32_t>(offsetof(M_sowheebasJairbivou29_t2010660101, ____yurlayKega_26)); }
	inline int32_t get__yurlayKega_26() const { return ____yurlayKega_26; }
	inline int32_t* get_address_of__yurlayKega_26() { return &____yurlayKega_26; }
	inline void set__yurlayKega_26(int32_t value)
	{
		____yurlayKega_26 = value;
	}

	inline static int32_t get_offset_of__hideredeaDijourem_27() { return static_cast<int32_t>(offsetof(M_sowheebasJairbivou29_t2010660101, ____hideredeaDijourem_27)); }
	inline int32_t get__hideredeaDijourem_27() const { return ____hideredeaDijourem_27; }
	inline int32_t* get_address_of__hideredeaDijourem_27() { return &____hideredeaDijourem_27; }
	inline void set__hideredeaDijourem_27(int32_t value)
	{
		____hideredeaDijourem_27 = value;
	}

	inline static int32_t get_offset_of__sodrortouKayjea_28() { return static_cast<int32_t>(offsetof(M_sowheebasJairbivou29_t2010660101, ____sodrortouKayjea_28)); }
	inline int32_t get__sodrortouKayjea_28() const { return ____sodrortouKayjea_28; }
	inline int32_t* get_address_of__sodrortouKayjea_28() { return &____sodrortouKayjea_28; }
	inline void set__sodrortouKayjea_28(int32_t value)
	{
		____sodrortouKayjea_28 = value;
	}

	inline static int32_t get_offset_of__paydrisHarloulou_29() { return static_cast<int32_t>(offsetof(M_sowheebasJairbivou29_t2010660101, ____paydrisHarloulou_29)); }
	inline uint32_t get__paydrisHarloulou_29() const { return ____paydrisHarloulou_29; }
	inline uint32_t* get_address_of__paydrisHarloulou_29() { return &____paydrisHarloulou_29; }
	inline void set__paydrisHarloulou_29(uint32_t value)
	{
		____paydrisHarloulou_29 = value;
	}

	inline static int32_t get_offset_of__chougas_30() { return static_cast<int32_t>(offsetof(M_sowheebasJairbivou29_t2010660101, ____chougas_30)); }
	inline float get__chougas_30() const { return ____chougas_30; }
	inline float* get_address_of__chougas_30() { return &____chougas_30; }
	inline void set__chougas_30(float value)
	{
		____chougas_30 = value;
	}

	inline static int32_t get_offset_of__rabarDichea_31() { return static_cast<int32_t>(offsetof(M_sowheebasJairbivou29_t2010660101, ____rabarDichea_31)); }
	inline float get__rabarDichea_31() const { return ____rabarDichea_31; }
	inline float* get_address_of__rabarDichea_31() { return &____rabarDichea_31; }
	inline void set__rabarDichea_31(float value)
	{
		____rabarDichea_31 = value;
	}

	inline static int32_t get_offset_of__puwhoucoLeadosa_32() { return static_cast<int32_t>(offsetof(M_sowheebasJairbivou29_t2010660101, ____puwhoucoLeadosa_32)); }
	inline uint32_t get__puwhoucoLeadosa_32() const { return ____puwhoucoLeadosa_32; }
	inline uint32_t* get_address_of__puwhoucoLeadosa_32() { return &____puwhoucoLeadosa_32; }
	inline void set__puwhoucoLeadosa_32(uint32_t value)
	{
		____puwhoucoLeadosa_32 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
