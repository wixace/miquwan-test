﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_AnimatorTransitionInfoGenerated
struct UnityEngine_AnimatorTransitionInfoGenerated_t1968877533;
// JSVCall
struct JSVCall_t3708497963;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"

// System.Void UnityEngine_AnimatorTransitionInfoGenerated::.ctor()
extern "C"  void UnityEngine_AnimatorTransitionInfoGenerated__ctor_m1443279918 (UnityEngine_AnimatorTransitionInfoGenerated_t1968877533 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimatorTransitionInfoGenerated::.cctor()
extern "C"  void UnityEngine_AnimatorTransitionInfoGenerated__cctor_m1309908287 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimatorTransitionInfoGenerated::AnimatorTransitionInfo_AnimatorTransitionInfo1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimatorTransitionInfoGenerated_AnimatorTransitionInfo_AnimatorTransitionInfo1_m1056492292 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimatorTransitionInfoGenerated::AnimatorTransitionInfo_fullPathHash(JSVCall)
extern "C"  void UnityEngine_AnimatorTransitionInfoGenerated_AnimatorTransitionInfo_fullPathHash_m2787919812 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimatorTransitionInfoGenerated::AnimatorTransitionInfo_nameHash(JSVCall)
extern "C"  void UnityEngine_AnimatorTransitionInfoGenerated_AnimatorTransitionInfo_nameHash_m1302387181 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimatorTransitionInfoGenerated::AnimatorTransitionInfo_userNameHash(JSVCall)
extern "C"  void UnityEngine_AnimatorTransitionInfoGenerated_AnimatorTransitionInfo_userNameHash_m3551829346 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimatorTransitionInfoGenerated::AnimatorTransitionInfo_normalizedTime(JSVCall)
extern "C"  void UnityEngine_AnimatorTransitionInfoGenerated_AnimatorTransitionInfo_normalizedTime_m4217173538 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimatorTransitionInfoGenerated::AnimatorTransitionInfo_anyState(JSVCall)
extern "C"  void UnityEngine_AnimatorTransitionInfoGenerated_AnimatorTransitionInfo_anyState_m3030852513 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimatorTransitionInfoGenerated::AnimatorTransitionInfo_IsName__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimatorTransitionInfoGenerated_AnimatorTransitionInfo_IsName__String_m1739942603 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimatorTransitionInfoGenerated::AnimatorTransitionInfo_IsUserName__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimatorTransitionInfoGenerated_AnimatorTransitionInfo_IsUserName__String_m1357406454 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimatorTransitionInfoGenerated::__Register()
extern "C"  void UnityEngine_AnimatorTransitionInfoGenerated___Register_m1512715609 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimatorTransitionInfoGenerated::ilo_setInt321(System.Int32,System.Int32)
extern "C"  void UnityEngine_AnimatorTransitionInfoGenerated_ilo_setInt321_m2416221544 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimatorTransitionInfoGenerated::ilo_setBooleanS2(System.Int32,System.Boolean)
extern "C"  void UnityEngine_AnimatorTransitionInfoGenerated_ilo_setBooleanS2_m3196596158 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
