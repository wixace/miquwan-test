﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Comparison`1<Newtonsoft.Json.Linq.JTokenType>
struct Comparison_1_t2633258748;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JTokenType3916897561.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void System.Comparison`1<Newtonsoft.Json.Linq.JTokenType>::.ctor(System.Object,System.IntPtr)
extern "C"  void Comparison_1__ctor_m4073924207_gshared (Comparison_1_t2633258748 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define Comparison_1__ctor_m4073924207(__this, ___object0, ___method1, method) ((  void (*) (Comparison_1_t2633258748 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Comparison_1__ctor_m4073924207_gshared)(__this, ___object0, ___method1, method)
// System.Int32 System.Comparison`1<Newtonsoft.Json.Linq.JTokenType>::Invoke(T,T)
extern "C"  int32_t Comparison_1_Invoke_m2887543473_gshared (Comparison_1_t2633258748 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method);
#define Comparison_1_Invoke_m2887543473(__this, ___x0, ___y1, method) ((  int32_t (*) (Comparison_1_t2633258748 *, int32_t, int32_t, const MethodInfo*))Comparison_1_Invoke_m2887543473_gshared)(__this, ___x0, ___y1, method)
// System.IAsyncResult System.Comparison`1<Newtonsoft.Json.Linq.JTokenType>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Comparison_1_BeginInvoke_m2715035946_gshared (Comparison_1_t2633258748 * __this, int32_t ___x0, int32_t ___y1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method);
#define Comparison_1_BeginInvoke_m2715035946(__this, ___x0, ___y1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Comparison_1_t2633258748 *, int32_t, int32_t, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))Comparison_1_BeginInvoke_m2715035946_gshared)(__this, ___x0, ___y1, ___callback2, ___object3, method)
// System.Int32 System.Comparison`1<Newtonsoft.Json.Linq.JTokenType>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t Comparison_1_EndInvoke_m3761487387_gshared (Comparison_1_t2633258748 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define Comparison_1_EndInvoke_m3761487387(__this, ___result0, method) ((  int32_t (*) (Comparison_1_t2633258748 *, Il2CppObject *, const MethodInfo*))Comparison_1_EndInvoke_m3761487387_gshared)(__this, ___result0, method)
