﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Func_1_gen1001010649MethodDeclarations.h"

// System.Void System.Func`1<System.Collections.Generic.Dictionary`2<System.String,System.String>>::.ctor(System.Object,System.IntPtr)
#define Func_1__ctor_m1057050994(__this, ___object0, ___method1, method) ((  void (*) (Func_1_t1952811501 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_1__ctor_m2351420511_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`1<System.Collections.Generic.Dictionary`2<System.String,System.String>>::Invoke()
#define Func_1_Invoke_m1665926390(__this, method) ((  Dictionary_2_t827649927 * (*) (Func_1_t1952811501 *, const MethodInfo*))Func_1_Invoke_m1970691095_gshared)(__this, method)
// System.IAsyncResult System.Func`1<System.Collections.Generic.Dictionary`2<System.String,System.String>>::BeginInvoke(System.AsyncCallback,System.Object)
#define Func_1_BeginInvoke_m399452163(__this, ___callback0, ___object1, method) ((  Il2CppObject * (*) (Func_1_t1952811501 *, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))Func_1_BeginInvoke_m4066620266_gshared)(__this, ___callback0, ___object1, method)
// TResult System.Func`1<System.Collections.Generic.Dictionary`2<System.String,System.String>>::EndInvoke(System.IAsyncResult)
#define Func_1_EndInvoke_m3957449436(__this, ___result0, method) ((  Dictionary_2_t827649927 * (*) (Func_1_t1952811501 *, Il2CppObject *, const MethodInfo*))Func_1_EndInvoke_m3356710033_gshared)(__this, ___result0, method)
