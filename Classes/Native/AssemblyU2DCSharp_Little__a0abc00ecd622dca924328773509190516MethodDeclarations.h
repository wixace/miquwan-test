﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._a0abc00ecd622dca924328774ffa94c9
struct _a0abc00ecd622dca924328774ffa94c9_t3509190516;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._a0abc00ecd622dca924328774ffa94c9::.ctor()
extern "C"  void _a0abc00ecd622dca924328774ffa94c9__ctor_m28264761 (_a0abc00ecd622dca924328774ffa94c9_t3509190516 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._a0abc00ecd622dca924328774ffa94c9::_a0abc00ecd622dca924328774ffa94c9m2(System.Int32)
extern "C"  int32_t _a0abc00ecd622dca924328774ffa94c9__a0abc00ecd622dca924328774ffa94c9m2_m2565160217 (_a0abc00ecd622dca924328774ffa94c9_t3509190516 * __this, int32_t ____a0abc00ecd622dca924328774ffa94c9a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._a0abc00ecd622dca924328774ffa94c9::_a0abc00ecd622dca924328774ffa94c9m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _a0abc00ecd622dca924328774ffa94c9__a0abc00ecd622dca924328774ffa94c9m_m4257175997 (_a0abc00ecd622dca924328774ffa94c9_t3509190516 * __this, int32_t ____a0abc00ecd622dca924328774ffa94c9a0, int32_t ____a0abc00ecd622dca924328774ffa94c9421, int32_t ____a0abc00ecd622dca924328774ffa94c9c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
