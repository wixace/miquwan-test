﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Pathfinding.NavMeshGraph
struct NavMeshGraph_t1979699444;
// Pathfinding.GridGraph
struct GridGraph_t2455707914;
// Pathfinding.PointGraph
struct PointGraph_t468341652;
// Pathfinding.RecastGraph
struct RecastGraph_t2197443166;
// System.Type[]
struct TypeU5BU5D_t3339007067;
// Pathfinding.NavGraph[]
struct NavGraphU5BU5D_t850130684;
// Pathfinding.UserConnection[]
struct UserConnectionU5BU5D_t328197926;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// UnityEngine.TextAsset
struct TextAsset_t3836129977;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.AstarData
struct  AstarData_t3283402719  : public Il2CppObject
{
public:
	// Pathfinding.NavMeshGraph Pathfinding.AstarData::navmesh
	NavMeshGraph_t1979699444 * ___navmesh_0;
	// Pathfinding.GridGraph Pathfinding.AstarData::gridGraph
	GridGraph_t2455707914 * ___gridGraph_1;
	// Pathfinding.PointGraph Pathfinding.AstarData::pointGraph
	PointGraph_t468341652 * ___pointGraph_2;
	// Pathfinding.RecastGraph Pathfinding.AstarData::recastGraph
	RecastGraph_t2197443166 * ___recastGraph_3;
	// System.Type[] Pathfinding.AstarData::graphTypes
	TypeU5BU5D_t3339007067* ___graphTypes_4;
	// Pathfinding.NavGraph[] Pathfinding.AstarData::graphs
	NavGraphU5BU5D_t850130684* ___graphs_5;
	// Pathfinding.UserConnection[] Pathfinding.AstarData::userConnections
	UserConnectionU5BU5D_t328197926* ___userConnections_6;
	// System.Boolean Pathfinding.AstarData::hasBeenReverted
	bool ___hasBeenReverted_7;
	// System.Byte[] Pathfinding.AstarData::data
	ByteU5BU5D_t4260760469* ___data_8;
	// System.UInt32 Pathfinding.AstarData::dataChecksum
	uint32_t ___dataChecksum_9;
	// System.Byte[] Pathfinding.AstarData::data_backup
	ByteU5BU5D_t4260760469* ___data_backup_10;
	// UnityEngine.TextAsset Pathfinding.AstarData::file_cachedStartup
	TextAsset_t3836129977 * ___file_cachedStartup_11;
	// System.Byte[] Pathfinding.AstarData::data_cachedStartup
	ByteU5BU5D_t4260760469* ___data_cachedStartup_12;
	// System.Boolean Pathfinding.AstarData::cacheStartup
	bool ___cacheStartup_13;

public:
	inline static int32_t get_offset_of_navmesh_0() { return static_cast<int32_t>(offsetof(AstarData_t3283402719, ___navmesh_0)); }
	inline NavMeshGraph_t1979699444 * get_navmesh_0() const { return ___navmesh_0; }
	inline NavMeshGraph_t1979699444 ** get_address_of_navmesh_0() { return &___navmesh_0; }
	inline void set_navmesh_0(NavMeshGraph_t1979699444 * value)
	{
		___navmesh_0 = value;
		Il2CppCodeGenWriteBarrier(&___navmesh_0, value);
	}

	inline static int32_t get_offset_of_gridGraph_1() { return static_cast<int32_t>(offsetof(AstarData_t3283402719, ___gridGraph_1)); }
	inline GridGraph_t2455707914 * get_gridGraph_1() const { return ___gridGraph_1; }
	inline GridGraph_t2455707914 ** get_address_of_gridGraph_1() { return &___gridGraph_1; }
	inline void set_gridGraph_1(GridGraph_t2455707914 * value)
	{
		___gridGraph_1 = value;
		Il2CppCodeGenWriteBarrier(&___gridGraph_1, value);
	}

	inline static int32_t get_offset_of_pointGraph_2() { return static_cast<int32_t>(offsetof(AstarData_t3283402719, ___pointGraph_2)); }
	inline PointGraph_t468341652 * get_pointGraph_2() const { return ___pointGraph_2; }
	inline PointGraph_t468341652 ** get_address_of_pointGraph_2() { return &___pointGraph_2; }
	inline void set_pointGraph_2(PointGraph_t468341652 * value)
	{
		___pointGraph_2 = value;
		Il2CppCodeGenWriteBarrier(&___pointGraph_2, value);
	}

	inline static int32_t get_offset_of_recastGraph_3() { return static_cast<int32_t>(offsetof(AstarData_t3283402719, ___recastGraph_3)); }
	inline RecastGraph_t2197443166 * get_recastGraph_3() const { return ___recastGraph_3; }
	inline RecastGraph_t2197443166 ** get_address_of_recastGraph_3() { return &___recastGraph_3; }
	inline void set_recastGraph_3(RecastGraph_t2197443166 * value)
	{
		___recastGraph_3 = value;
		Il2CppCodeGenWriteBarrier(&___recastGraph_3, value);
	}

	inline static int32_t get_offset_of_graphTypes_4() { return static_cast<int32_t>(offsetof(AstarData_t3283402719, ___graphTypes_4)); }
	inline TypeU5BU5D_t3339007067* get_graphTypes_4() const { return ___graphTypes_4; }
	inline TypeU5BU5D_t3339007067** get_address_of_graphTypes_4() { return &___graphTypes_4; }
	inline void set_graphTypes_4(TypeU5BU5D_t3339007067* value)
	{
		___graphTypes_4 = value;
		Il2CppCodeGenWriteBarrier(&___graphTypes_4, value);
	}

	inline static int32_t get_offset_of_graphs_5() { return static_cast<int32_t>(offsetof(AstarData_t3283402719, ___graphs_5)); }
	inline NavGraphU5BU5D_t850130684* get_graphs_5() const { return ___graphs_5; }
	inline NavGraphU5BU5D_t850130684** get_address_of_graphs_5() { return &___graphs_5; }
	inline void set_graphs_5(NavGraphU5BU5D_t850130684* value)
	{
		___graphs_5 = value;
		Il2CppCodeGenWriteBarrier(&___graphs_5, value);
	}

	inline static int32_t get_offset_of_userConnections_6() { return static_cast<int32_t>(offsetof(AstarData_t3283402719, ___userConnections_6)); }
	inline UserConnectionU5BU5D_t328197926* get_userConnections_6() const { return ___userConnections_6; }
	inline UserConnectionU5BU5D_t328197926** get_address_of_userConnections_6() { return &___userConnections_6; }
	inline void set_userConnections_6(UserConnectionU5BU5D_t328197926* value)
	{
		___userConnections_6 = value;
		Il2CppCodeGenWriteBarrier(&___userConnections_6, value);
	}

	inline static int32_t get_offset_of_hasBeenReverted_7() { return static_cast<int32_t>(offsetof(AstarData_t3283402719, ___hasBeenReverted_7)); }
	inline bool get_hasBeenReverted_7() const { return ___hasBeenReverted_7; }
	inline bool* get_address_of_hasBeenReverted_7() { return &___hasBeenReverted_7; }
	inline void set_hasBeenReverted_7(bool value)
	{
		___hasBeenReverted_7 = value;
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(AstarData_t3283402719, ___data_8)); }
	inline ByteU5BU5D_t4260760469* get_data_8() const { return ___data_8; }
	inline ByteU5BU5D_t4260760469** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(ByteU5BU5D_t4260760469* value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier(&___data_8, value);
	}

	inline static int32_t get_offset_of_dataChecksum_9() { return static_cast<int32_t>(offsetof(AstarData_t3283402719, ___dataChecksum_9)); }
	inline uint32_t get_dataChecksum_9() const { return ___dataChecksum_9; }
	inline uint32_t* get_address_of_dataChecksum_9() { return &___dataChecksum_9; }
	inline void set_dataChecksum_9(uint32_t value)
	{
		___dataChecksum_9 = value;
	}

	inline static int32_t get_offset_of_data_backup_10() { return static_cast<int32_t>(offsetof(AstarData_t3283402719, ___data_backup_10)); }
	inline ByteU5BU5D_t4260760469* get_data_backup_10() const { return ___data_backup_10; }
	inline ByteU5BU5D_t4260760469** get_address_of_data_backup_10() { return &___data_backup_10; }
	inline void set_data_backup_10(ByteU5BU5D_t4260760469* value)
	{
		___data_backup_10 = value;
		Il2CppCodeGenWriteBarrier(&___data_backup_10, value);
	}

	inline static int32_t get_offset_of_file_cachedStartup_11() { return static_cast<int32_t>(offsetof(AstarData_t3283402719, ___file_cachedStartup_11)); }
	inline TextAsset_t3836129977 * get_file_cachedStartup_11() const { return ___file_cachedStartup_11; }
	inline TextAsset_t3836129977 ** get_address_of_file_cachedStartup_11() { return &___file_cachedStartup_11; }
	inline void set_file_cachedStartup_11(TextAsset_t3836129977 * value)
	{
		___file_cachedStartup_11 = value;
		Il2CppCodeGenWriteBarrier(&___file_cachedStartup_11, value);
	}

	inline static int32_t get_offset_of_data_cachedStartup_12() { return static_cast<int32_t>(offsetof(AstarData_t3283402719, ___data_cachedStartup_12)); }
	inline ByteU5BU5D_t4260760469* get_data_cachedStartup_12() const { return ___data_cachedStartup_12; }
	inline ByteU5BU5D_t4260760469** get_address_of_data_cachedStartup_12() { return &___data_cachedStartup_12; }
	inline void set_data_cachedStartup_12(ByteU5BU5D_t4260760469* value)
	{
		___data_cachedStartup_12 = value;
		Il2CppCodeGenWriteBarrier(&___data_cachedStartup_12, value);
	}

	inline static int32_t get_offset_of_cacheStartup_13() { return static_cast<int32_t>(offsetof(AstarData_t3283402719, ___cacheStartup_13)); }
	inline bool get_cacheStartup_13() const { return ___cacheStartup_13; }
	inline bool* get_address_of_cacheStartup_13() { return &___cacheStartup_13; }
	inline void set_cacheStartup_13(bool value)
	{
		___cacheStartup_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
