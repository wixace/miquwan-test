﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Int32[]
struct Int32U5BU5D_t3230847821;
// Pathfinding.RVO.Simulator
struct Simulator_t2705969170;
// Pathfinding.RVO.RVONavmesh
struct RVONavmesh_t545242093;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.RVO.RVONavmesh/<AddGraphObstacles>c__AnonStorey123
struct  U3CAddGraphObstaclesU3Ec__AnonStorey123_t3755328625  : public Il2CppObject
{
public:
	// System.Int32[] Pathfinding.RVO.RVONavmesh/<AddGraphObstacles>c__AnonStorey123::uses
	Int32U5BU5D_t3230847821* ___uses_0;
	// Pathfinding.RVO.Simulator Pathfinding.RVO.RVONavmesh/<AddGraphObstacles>c__AnonStorey123::sim
	Simulator_t2705969170 * ___sim_1;
	// Pathfinding.RVO.RVONavmesh Pathfinding.RVO.RVONavmesh/<AddGraphObstacles>c__AnonStorey123::<>f__this
	RVONavmesh_t545242093 * ___U3CU3Ef__this_2;

public:
	inline static int32_t get_offset_of_uses_0() { return static_cast<int32_t>(offsetof(U3CAddGraphObstaclesU3Ec__AnonStorey123_t3755328625, ___uses_0)); }
	inline Int32U5BU5D_t3230847821* get_uses_0() const { return ___uses_0; }
	inline Int32U5BU5D_t3230847821** get_address_of_uses_0() { return &___uses_0; }
	inline void set_uses_0(Int32U5BU5D_t3230847821* value)
	{
		___uses_0 = value;
		Il2CppCodeGenWriteBarrier(&___uses_0, value);
	}

	inline static int32_t get_offset_of_sim_1() { return static_cast<int32_t>(offsetof(U3CAddGraphObstaclesU3Ec__AnonStorey123_t3755328625, ___sim_1)); }
	inline Simulator_t2705969170 * get_sim_1() const { return ___sim_1; }
	inline Simulator_t2705969170 ** get_address_of_sim_1() { return &___sim_1; }
	inline void set_sim_1(Simulator_t2705969170 * value)
	{
		___sim_1 = value;
		Il2CppCodeGenWriteBarrier(&___sim_1, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_2() { return static_cast<int32_t>(offsetof(U3CAddGraphObstaclesU3Ec__AnonStorey123_t3755328625, ___U3CU3Ef__this_2)); }
	inline RVONavmesh_t545242093 * get_U3CU3Ef__this_2() const { return ___U3CU3Ef__this_2; }
	inline RVONavmesh_t545242093 ** get_address_of_U3CU3Ef__this_2() { return &___U3CU3Ef__this_2; }
	inline void set_U3CU3Ef__this_2(RVONavmesh_t545242093 * value)
	{
		___U3CU3Ef__this_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
