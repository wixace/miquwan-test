﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>
struct Dictionary_2_t1674540875;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V3901341579.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<UIModelDisplayType,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m4263319080_gshared (Enumerator_t3901341579 * __this, Dictionary_2_t1674540875 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m4263319080(__this, ___host0, method) ((  void (*) (Enumerator_t3901341579 *, Dictionary_2_t1674540875 *, const MethodInfo*))Enumerator__ctor_m4263319080_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<UIModelDisplayType,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m298319619_gshared (Enumerator_t3901341579 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m298319619(__this, method) ((  Il2CppObject * (*) (Enumerator_t3901341579 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m298319619_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<UIModelDisplayType,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m444985741_gshared (Enumerator_t3901341579 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m444985741(__this, method) ((  void (*) (Enumerator_t3901341579 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m444985741_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<UIModelDisplayType,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m463222474_gshared (Enumerator_t3901341579 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m463222474(__this, method) ((  void (*) (Enumerator_t3901341579 *, const MethodInfo*))Enumerator_Dispose_m463222474_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<UIModelDisplayType,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2006073149_gshared (Enumerator_t3901341579 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m2006073149(__this, method) ((  bool (*) (Enumerator_t3901341579 *, const MethodInfo*))Enumerator_MoveNext_m2006073149_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<UIModelDisplayType,System.Object>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m2078755533_gshared (Enumerator_t3901341579 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m2078755533(__this, method) ((  Il2CppObject * (*) (Enumerator_t3901341579 *, const MethodInfo*))Enumerator_get_Current_m2078755533_gshared)(__this, method)
