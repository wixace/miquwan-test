﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EnemyDropItem
struct EnemyDropItem_t1468754474;
// CombatEntity
struct CombatEntity_t684137495;
// Mihua.Asset.ABLoadOperation.AssetOperation
struct AssetOperation_t778728221;
// CSDatacfgManager
struct CSDatacfgManager_t1565254243;
// Hero
struct Hero_t2245658;
// HeroMgr
struct HeroMgr_t2475965342;
// effectCfg
struct effectCfg_t2826279187;
// System.Action
struct Action_t3771233898;
// EnemyDropMgr
struct EnemyDropMgr_t2125592609;
// System.Collections.Generic.List`1<CombatEntity>
struct List_1_t2052323047;
// BuffCtrl
struct BuffCtrl_t2836564350;
// EffectMgr
struct EffectMgr_t535289511;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CombatEntity684137495.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_Mihua_Asset_ABLoadOperation_Asset778728221.h"
#include "AssemblyU2DCSharp_HeroMgr2475965342.h"
#include "AssemblyU2DCSharp_CSDatacfgManager1565254243.h"
#include "System_Core_System_Action3771233898.h"
#include "AssemblyU2DCSharp_EnemyDropItem1468754474.h"
#include "AssemblyU2DCSharp_BuffCtrl2836564350.h"
#include "AssemblyU2DCSharp_EffectMgr535289511.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"

// System.Void EnemyDropItem::.ctor()
extern "C"  void EnemyDropItem__ctor_m1483916929 (EnemyDropItem_t1468754474 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EnemyDropItem::set_IsCreatEffectFinish(System.Boolean)
extern "C"  void EnemyDropItem_set_IsCreatEffectFinish_m1525721244 (EnemyDropItem_t1468754474 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean EnemyDropItem::get_IsCreatEffectFinish()
extern "C"  bool EnemyDropItem_get_IsCreatEffectFinish_m3659183757 (EnemyDropItem_t1468754474 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EnemyDropItem::SetIndex(System.Int32)
extern "C"  void EnemyDropItem_SetIndex_m1533556740 (EnemyDropItem_t1468754474 * __this, int32_t ___dex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EnemyDropItem::Init(System.Int32,System.Single,CombatEntity,UnityEngine.Vector3)
extern "C"  void EnemyDropItem_Init_m1115264153 (EnemyDropItem_t1468754474 * __this, int32_t ___id0, float ___num1, CombatEntity_t684137495 * ___akill2, Vector3_t4282066566  ___targetPos3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EnemyDropItem::Creat3DEffect()
extern "C"  void EnemyDropItem_Creat3DEffect_m3016018378 (EnemyDropItem_t1468754474 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EnemyDropItem::Play()
extern "C"  void EnemyDropItem_Play_m2414485719 (EnemyDropItem_t1468754474 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EnemyDropItem::Update()
extern "C"  void EnemyDropItem_Update_m483646636 (EnemyDropItem_t1468754474 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EnemyDropItem::Clear()
extern "C"  void EnemyDropItem_Clear_m3185017516 (EnemyDropItem_t1468754474 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EnemyDropItem::AddValue()
extern "C"  void EnemyDropItem_AddValue_m3365094131 (EnemyDropItem_t1468754474 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EnemyDropItem::<Creat3DEffect>m__3CA(Mihua.Asset.ABLoadOperation.AssetOperation)
extern "C"  void EnemyDropItem_U3CCreat3DEffectU3Em__3CA_m400889945 (EnemyDropItem_t1468754474 * __this, AssetOperation_t778728221 * ___ao0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CSDatacfgManager EnemyDropItem::ilo_get_CfgDataMgr1()
extern "C"  CSDatacfgManager_t1565254243 * EnemyDropItem_ilo_get_CfgDataMgr1_m3054152580 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Hero EnemyDropItem::ilo_GetNearestHero2(HeroMgr,UnityEngine.Vector3)
extern "C"  Hero_t2245658 * EnemyDropItem_ilo_GetNearestHero2_m300456552 (Il2CppObject * __this /* static, unused */, HeroMgr_t2475965342 * ____this0, Vector3_t4282066566  ___pos1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// effectCfg EnemyDropItem::ilo_GeteffectCfg3(CSDatacfgManager,System.Int32)
extern "C"  effectCfg_t2826279187 * EnemyDropItem_ilo_GeteffectCfg3_m1139300280 (Il2CppObject * __this /* static, unused */, CSDatacfgManager_t1565254243 * ____this0, int32_t ___id1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 EnemyDropItem::ilo_DelayCall4(System.Single,System.Action)
extern "C"  uint32_t EnemyDropItem_ilo_DelayCall4_m1059913972 (Il2CppObject * __this /* static, unused */, float ___delaytime0, Action_t3771233898 * ___act1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 EnemyDropItem::ilo_WorldToNgui5(UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  EnemyDropItem_ilo_WorldToNgui5_m4169614530 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ____pos0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean EnemyDropItem::ilo_get_isDeath6(CombatEntity)
extern "C"  bool EnemyDropItem_ilo_get_isDeath6_m3155154336 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EnemyDropItem::ilo_Clear7(EnemyDropItem)
extern "C"  void EnemyDropItem_ilo_Clear7_m2928154472 (Il2CppObject * __this /* static, unused */, EnemyDropItem_t1468754474 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HeroMgr EnemyDropItem::ilo_get_instance8()
extern "C"  HeroMgr_t2475965342 * EnemyDropItem_ilo_get_instance8_m3674906171 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EnemyDropItem::ilo_AddValue9(EnemyDropItem)
extern "C"  void EnemyDropItem_ilo_AddValue9_m2774281907 (Il2CppObject * __this /* static, unused */, EnemyDropItem_t1468754474 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean EnemyDropItem::ilo_get_IsCreatEffectFinish10(EnemyDropItem)
extern "C"  bool EnemyDropItem_ilo_get_IsCreatEffectFinish10_m454126223 (Il2CppObject * __this /* static, unused */, EnemyDropItem_t1468754474 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// EnemyDropMgr EnemyDropItem::ilo_get_Instance11()
extern "C"  EnemyDropMgr_t2125592609 * EnemyDropItem_ilo_get_Instance11_m1492649496 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EnemyDropItem::ilo_AddHP12(CombatEntity,System.Single)
extern "C"  void EnemyDropItem_ilo_AddHP12_m2803629828 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___delta1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CombatEntity> EnemyDropItem::ilo_GetAllHero13(HeroMgr)
extern "C"  List_1_t2052323047 * EnemyDropItem_ilo_GetAllHero13_m251403489 (Il2CppObject * __this /* static, unused */, HeroMgr_t2475965342 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EnemyDropItem::ilo_AddBuff14(BuffCtrl,System.Int32)
extern "C"  void EnemyDropItem_ilo_AddBuff14_m3492553522 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, int32_t ___id1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EnemyDropItem::ilo_PlayEffect15(EffectMgr,System.Int32,CombatEntity)
extern "C"  void EnemyDropItem_ilo_PlayEffect15_m4101190532 (Il2CppObject * __this /* static, unused */, EffectMgr_t535289511 * ____this0, int32_t ___id1, CombatEntity_t684137495 * ___srcTf2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject EnemyDropItem::ilo_Instantiate16(UnityEngine.GameObject)
extern "C"  GameObject_t3674682005 * EnemyDropItem_ilo_Instantiate16_m108544968 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EnemyDropItem::ilo_ChangeParent17(UnityEngine.GameObject,UnityEngine.GameObject,System.Boolean)
extern "C"  void EnemyDropItem_ilo_ChangeParent17_m2708214829 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ____this0, GameObject_t3674682005 * ___parent1, bool ___resetMat2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
