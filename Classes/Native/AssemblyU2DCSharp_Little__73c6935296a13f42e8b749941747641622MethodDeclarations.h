﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._73c6935296a13f42e8b7499443bbd435
struct _73c6935296a13f42e8b7499443bbd435_t1747641622;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._73c6935296a13f42e8b7499443bbd435::.ctor()
extern "C"  void _73c6935296a13f42e8b7499443bbd435__ctor_m2472122839 (_73c6935296a13f42e8b7499443bbd435_t1747641622 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._73c6935296a13f42e8b7499443bbd435::_73c6935296a13f42e8b7499443bbd435m2(System.Int32)
extern "C"  int32_t _73c6935296a13f42e8b7499443bbd435__73c6935296a13f42e8b7499443bbd435m2_m4246948313 (_73c6935296a13f42e8b7499443bbd435_t1747641622 * __this, int32_t ____73c6935296a13f42e8b7499443bbd435a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._73c6935296a13f42e8b7499443bbd435::_73c6935296a13f42e8b7499443bbd435m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _73c6935296a13f42e8b7499443bbd435__73c6935296a13f42e8b7499443bbd435m_m3458540797 (_73c6935296a13f42e8b7499443bbd435_t1747641622 * __this, int32_t ____73c6935296a13f42e8b7499443bbd435a0, int32_t ____73c6935296a13f42e8b7499443bbd435221, int32_t ____73c6935296a13f42e8b7499443bbd435c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
