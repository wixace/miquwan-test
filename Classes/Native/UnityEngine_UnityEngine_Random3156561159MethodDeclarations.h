﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Random
struct Random_t3156561159;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"

// System.Void UnityEngine.Random::.ctor()
extern "C"  void Random__ctor_m1288200842 (Random_t3156561159 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Random::get_seed()
extern "C"  int32_t Random_get_seed_m1525577876 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Random::set_seed(System.Int32)
extern "C"  void Random_set_seed_m58891993 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Random::Range(System.Single,System.Single)
extern "C"  float Random_Range_m3362417303 (Il2CppObject * __this /* static, unused */, float ___min0, float ___max1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Random::Range(System.Int32,System.Int32)
extern "C"  int32_t Random_Range_m75452833 (Il2CppObject * __this /* static, unused */, int32_t ___min0, int32_t ___max1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Random::RandomRangeInt(System.Int32,System.Int32)
extern "C"  int32_t Random_RandomRangeInt_m1203631415 (Il2CppObject * __this /* static, unused */, int32_t ___min0, int32_t ___max1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Random::get_value()
extern "C"  float Random_get_value_m2402066692 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Random::get_insideUnitSphere()
extern "C"  Vector3_t4282066566  Random_get_insideUnitSphere_m1884270890 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Random::INTERNAL_get_insideUnitSphere(UnityEngine.Vector3&)
extern "C"  void Random_INTERNAL_get_insideUnitSphere_m1119768947 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Random::GetRandomUnitCircle(UnityEngine.Vector2&)
extern "C"  void Random_GetRandomUnitCircle_m3555525383 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565 * ___output0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Random::get_insideUnitCircle()
extern "C"  Vector2_t4282066565  Random_get_insideUnitCircle_m3455477774 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Random::get_onUnitSphere()
extern "C"  Vector3_t4282066566  Random_get_onUnitSphere_m1999405197 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Random::INTERNAL_get_onUnitSphere(UnityEngine.Vector3&)
extern "C"  void Random_INTERNAL_get_onUnitSphere_m1824529494 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Random::get_rotation()
extern "C"  Quaternion_t1553702882  Random_get_rotation_m390819995 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Random::INTERNAL_get_rotation(UnityEngine.Quaternion&)
extern "C"  void Random_INTERNAL_get_rotation_m757737008 (Il2CppObject * __this /* static, unused */, Quaternion_t1553702882 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Random::get_rotationUniform()
extern "C"  Quaternion_t1553702882  Random_get_rotationUniform_m231969979 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Random::INTERNAL_get_rotationUniform(UnityEngine.Quaternion&)
extern "C"  void Random_INTERNAL_get_rotationUniform_m4138882876 (Il2CppObject * __this /* static, unused */, Quaternion_t1553702882 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.Random::ColorHSV()
extern "C"  Color_t4194546905  Random_ColorHSV_m3233070633 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.Random::ColorHSV(System.Single,System.Single)
extern "C"  Color_t4194546905  Random_ColorHSV_m2622911879 (Il2CppObject * __this /* static, unused */, float ___hueMin0, float ___hueMax1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.Random::ColorHSV(System.Single,System.Single,System.Single,System.Single)
extern "C"  Color_t4194546905  Random_ColorHSV_m837293585 (Il2CppObject * __this /* static, unused */, float ___hueMin0, float ___hueMax1, float ___saturationMin2, float ___saturationMax3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.Random::ColorHSV(System.Single,System.Single,System.Single,System.Single,System.Single,System.Single)
extern "C"  Color_t4194546905  Random_ColorHSV_m3889882011 (Il2CppObject * __this /* static, unused */, float ___hueMin0, float ___hueMax1, float ___saturationMin2, float ___saturationMax3, float ___valueMin4, float ___valueMax5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.Random::ColorHSV(System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single)
extern "C"  Color_t4194546905  Random_ColorHSV_m1574526501 (Il2CppObject * __this /* static, unused */, float ___hueMin0, float ___hueMax1, float ___saturationMin2, float ___saturationMax3, float ___valueMin4, float ___valueMax5, float ___alphaMin6, float ___alphaMax7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
