﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_Events_UnityEventGenerated/<UnityEvent_RemoveListener_GetDelegate_member2_arg0>c__AnonStoreyE6
struct U3CUnityEvent_RemoveListener_GetDelegate_member2_arg0U3Ec__AnonStoreyE6_t2898994309;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine_Events_UnityEventGenerated/<UnityEvent_RemoveListener_GetDelegate_member2_arg0>c__AnonStoreyE6::.ctor()
extern "C"  void U3CUnityEvent_RemoveListener_GetDelegate_member2_arg0U3Ec__AnonStoreyE6__ctor_m31300022 (U3CUnityEvent_RemoveListener_GetDelegate_member2_arg0U3Ec__AnonStoreyE6_t2898994309 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Events_UnityEventGenerated/<UnityEvent_RemoveListener_GetDelegate_member2_arg0>c__AnonStoreyE6::<>m__1B3()
extern "C"  void U3CUnityEvent_RemoveListener_GetDelegate_member2_arg0U3Ec__AnonStoreyE6_U3CU3Em__1B3_m3281361509 (U3CUnityEvent_RemoveListener_GetDelegate_member2_arg0U3Ec__AnonStoreyE6_t2898994309 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
