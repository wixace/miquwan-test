﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Utilities.StringUtils/<Indent>c__AnonStorey149
struct U3CIndentU3Ec__AnonStorey149_t2737769005;
// System.IO.TextWriter
struct TextWriter_t2304124208;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IO_TextWriter2304124208.h"
#include "mscorlib_System_String7231557.h"

// System.Void Newtonsoft.Json.Utilities.StringUtils/<Indent>c__AnonStorey149::.ctor()
extern "C"  void U3CIndentU3Ec__AnonStorey149__ctor_m384390414 (U3CIndentU3Ec__AnonStorey149_t2737769005 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Utilities.StringUtils/<Indent>c__AnonStorey149::<>m__3B5(System.IO.TextWriter,System.String)
extern "C"  void U3CIndentU3Ec__AnonStorey149_U3CU3Em__3B5_m2798863716 (U3CIndentU3Ec__AnonStorey149_t2737769005 * __this, TextWriter_t2304124208 * ___tw0, String_t* ___line1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
