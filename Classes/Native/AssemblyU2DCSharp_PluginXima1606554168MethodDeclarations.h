﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PluginXima
struct PluginXima_t1606554168;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// Mihua.SDK.PayInfo
struct PayInfo_t1775308120;
// System.String
struct String_t;
// CEvent.EventFunc`1<CEvent.ZEvent>
struct EventFunc_1_t1114914310;
// VersionMgr
struct VersionMgr_t1322950208;
// ProductsCfgMgr
struct ProductsCfgMgr_t2493714872;
// System.Object
struct Il2CppObject;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// XiMaNewPay
struct XiMaNewPay_t2653418445;
// XiMaPay
struct XiMaPay_t3871054307;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PluginXima_PayType53038859.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "AssemblyU2DCSharp_Mihua_SDK_PayInfo1775308120.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_VersionMgr1322950208.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_XiMaNewPay2653418445.h"
#include "AssemblyU2DCSharp_XiMaPay3871054307.h"
#include "AssemblyU2DCSharp_PluginXima1606554168.h"

// System.Void PluginXima::.ctor()
extern "C"  void PluginXima__ctor_m1884194147 (PluginXima_t1606554168 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXima::.cctor()
extern "C"  void PluginXima__cctor_m2093347498 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXima::VoidFuction()
extern "C"  void PluginXima_VoidFuction_m3598689237 (PluginXima_t1606554168 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXima::Init()
extern "C"  void PluginXima_Init_m4030189713 (PluginXima_t1606554168 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginXima::HasPayType(PluginXima/PayType)
extern "C"  bool PluginXima_HasPayType_m506124690 (PluginXima_t1606554168 * __this, int32_t ___payType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginXima::ReqSDKHttpLogin()
extern "C"  Dictionary_2_t827649927 * PluginXima_ReqSDKHttpLogin_m4090694264 (PluginXima_t1606554168 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginXima::IsLoginSuccess(System.Boolean)
extern "C"  bool PluginXima_IsLoginSuccess_m2137601936 (PluginXima_t1606554168 * __this, bool ___isOpenUseLogin0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXima::OpenUserLogin()
extern "C"  void PluginXima_OpenUserLogin_m1613337781 (PluginXima_t1606554168 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXima::RoleEnterGame(CEvent.ZEvent)
extern "C"  void PluginXima_RoleEnterGame_m1125259782 (PluginXima_t1606554168 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXima::UserPay(CEvent.ZEvent)
extern "C"  void PluginXima_UserPay_m1161836829 (PluginXima_t1606554168 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXima::GoSelectPay(Mihua.SDK.PayInfo)
extern "C"  void PluginXima_GoSelectPay_m2642461057 (PluginXima_t1606554168 * __this, PayInfo_t1775308120 * ____payInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXima::OnSelectXimaPay(CEvent.ZEvent)
extern "C"  void PluginXima_OnSelectXimaPay_m1322315954 (PluginXima_t1606554168 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXima::OnLoginSuccess(System.String)
extern "C"  void PluginXima_OnLoginSuccess_m1144586280 (PluginXima_t1606554168 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXima::OnLoginFail(System.String)
extern "C"  void PluginXima_OnLoginFail_m4055409177 (PluginXima_t1606554168 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXima::OnPaySuccess(System.String)
extern "C"  void PluginXima_OnPaySuccess_m1378962535 (PluginXima_t1606554168 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXima::OnPayFail(System.String)
extern "C"  void PluginXima_OnPayFail_m288544314 (PluginXima_t1606554168 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXima::OnExitGameSuccess(System.String)
extern "C"  void PluginXima_OnExitGameSuccess_m1594430413 (PluginXima_t1606554168 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXima::OnLogoutSuccess(System.String)
extern "C"  void PluginXima_OnLogoutSuccess_m1125978983 (PluginXima_t1606554168 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXima::OnLogoutFail(System.String)
extern "C"  void PluginXima_OnLogoutFail_m976514874 (PluginXima_t1606554168 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXima::__IIap(System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginXima___IIap_m4009342482 (PluginXima_t1606554168 * __this, String_t* ___productId0, String_t* ___roleId1, String_t* ___serverId2, String_t* ___type3, String_t* ___amount4, String_t* ___orderId5, String_t* ___pcbUrl6, String_t* ___appKey7, String_t* ___eargs8, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXima::<OnLogoutSuccess>m__46C()
extern "C"  void PluginXima_U3COnLogoutSuccessU3Em__46C_m110860245 (PluginXima_t1606554168 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXima::ilo_AddEventListener1(System.String,CEvent.EventFunc`1<CEvent.ZEvent>)
extern "C"  void PluginXima_ilo_AddEventListener1_m3217902385 (Il2CppObject * __this /* static, unused */, String_t* ___eventType0, EventFunc_1_t1114914310 * ___fun1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionMgr PluginXima::ilo_get_Instance2()
extern "C"  VersionMgr_t1322950208 * PluginXima_ilo_get_Instance2_m1788456613 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginXima::ilo_get_isSdkLogin3(VersionMgr)
extern "C"  bool PluginXima_ilo_get_isSdkLogin3_m1085583595 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXima::ilo_DispatchEvent4(CEvent.ZEvent)
extern "C"  void PluginXima_ilo_DispatchEvent4_m2787740883 (Il2CppObject * __this /* static, unused */, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginXima::ilo_get_isHide20005(VersionMgr)
extern "C"  bool PluginXima_ilo_get_isHide20005_m1199946716 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProductsCfgMgr PluginXima::ilo_get_ProductsCfgMgr6()
extern "C"  ProductsCfgMgr_t2493714872 * PluginXima_ilo_get_ProductsCfgMgr6_m4263257214 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXima::ilo_Log7(System.Object,System.Boolean)
extern "C"  void PluginXima_ilo_Log7_m1292150544 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXima::ilo_CallJsFun8(System.String,System.Object[])
extern "C"  void PluginXima_ilo_CallJsFun8_m2678256954 (Il2CppObject * __this /* static, unused */, String_t* ___funName0, ObjectU5BU5D_t1108656482* ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXima::ilo_tiaoqian9(XiMaNewPay,Mihua.SDK.PayInfo)
extern "C"  void PluginXima_ilo_tiaoqian9_m2898929618 (Il2CppObject * __this /* static, unused */, XiMaNewPay_t2653418445 * ____this0, PayInfo_t1775308120 * ___payInfo1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXima::ilo_Tiaoqian10(XiMaPay,Mihua.SDK.PayInfo,System.String)
extern "C"  void PluginXima_ilo_Tiaoqian10_m1060165040 (Il2CppObject * __this /* static, unused */, XiMaPay_t3871054307 * ____this0, PayInfo_t1775308120 * ___payInfo1, String_t* ___payType2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXima::ilo_OpenUserLogin11(PluginXima)
extern "C"  void PluginXima_ilo_OpenUserLogin11_m3374714250 (Il2CppObject * __this /* static, unused */, PluginXima_t1606554168 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
