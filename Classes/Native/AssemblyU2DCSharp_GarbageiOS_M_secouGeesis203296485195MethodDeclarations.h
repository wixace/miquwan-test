﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_secouGeesis20
struct M_secouGeesis20_t3296485195;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"

// System.Void GarbageiOS.M_secouGeesis20::.ctor()
extern "C"  void M_secouGeesis20__ctor_m3601070712 (M_secouGeesis20_t3296485195 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_secouGeesis20::M_faqeVaver0(System.String[],System.Int32)
extern "C"  void M_secouGeesis20_M_faqeVaver0_m4061801052 (M_secouGeesis20_t3296485195 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
