﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BuglyAgent
struct BuglyAgent_t2905516196;
// BuglyAgent/LogCallbackDelegate
struct LogCallbackDelegate_t45039171;
// System.String
struct String_t;
// System.Func`1<System.Collections.Generic.Dictionary`2<System.String,System.String>>
struct Func_1_t1952811501;
// System.Exception
struct Exception_t3991598821;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// System.Object
struct Il2CppObject;
// System.UnhandledExceptionEventArgs
struct UnhandledExceptionEventArgs_t3134093121;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_BuglyAgent_LogCallback45039171.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Exception3991598821.h"
#include "AssemblyU2DCSharpU2Dfirstpass_LogSeverity591216193.h"
#include "UnityEngine_UnityEngine_LogType4286006228.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_UnhandledExceptionEventArgs3134093121.h"

// System.Void BuglyAgent::.ctor()
extern "C"  void BuglyAgent__ctor_m4019185195 (BuglyAgent_t2905516196 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuglyAgent::.cctor()
extern "C"  void BuglyAgent__cctor_m3853560546 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuglyAgent::add__LogCallbackEventHandler(BuglyAgent/LogCallbackDelegate)
extern "C"  void BuglyAgent_add__LogCallbackEventHandler_m3543983054 (Il2CppObject * __this /* static, unused */, LogCallbackDelegate_t45039171 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuglyAgent::remove__LogCallbackEventHandler(BuglyAgent/LogCallbackDelegate)
extern "C"  void BuglyAgent_remove__LogCallbackEventHandler_m1877121307 (Il2CppObject * __this /* static, unused */, LogCallbackDelegate_t45039171 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuglyAgent::ConfigCrashReporter(System.Int32,System.Int32)
extern "C"  void BuglyAgent_ConfigCrashReporter_m1021581495 (Il2CppObject * __this /* static, unused */, int32_t ___type0, int32_t ___logLevel1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuglyAgent::InitWithAppId(System.String)
extern "C"  void BuglyAgent_InitWithAppId_m2313387731 (Il2CppObject * __this /* static, unused */, String_t* ___appId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuglyAgent::EnableExceptionHandler()
extern "C"  void BuglyAgent_EnableExceptionHandler_m724280279 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuglyAgent::RegisterLogCallback(BuglyAgent/LogCallbackDelegate)
extern "C"  void BuglyAgent_RegisterLogCallback_m2466877548 (Il2CppObject * __this /* static, unused */, LogCallbackDelegate_t45039171 * ___handler0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuglyAgent::SetLogCallbackExtrasHandler(System.Func`1<System.Collections.Generic.Dictionary`2<System.String,System.String>>)
extern "C"  void BuglyAgent_SetLogCallbackExtrasHandler_m1328908226 (Il2CppObject * __this /* static, unused */, Func_1_t1952811501 * ___handler0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuglyAgent::ReportException(System.Exception,System.String)
extern "C"  void BuglyAgent_ReportException_m78035440 (Il2CppObject * __this /* static, unused */, Exception_t3991598821 * ___e0, String_t* ___message1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuglyAgent::ReportException(System.String,System.String,System.String)
extern "C"  void BuglyAgent_ReportException_m2758451190 (Il2CppObject * __this /* static, unused */, String_t* ___name0, String_t* ___message1, String_t* ___stackTrace2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuglyAgent::UnregisterLogCallback(BuglyAgent/LogCallbackDelegate)
extern "C"  void BuglyAgent_UnregisterLogCallback_m60396339 (Il2CppObject * __this /* static, unused */, LogCallbackDelegate_t45039171 * ___handler0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuglyAgent::SetUserId(System.String)
extern "C"  void BuglyAgent_SetUserId_m813488145 (Il2CppObject * __this /* static, unused */, String_t* ___userId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuglyAgent::SetScene(System.Int32)
extern "C"  void BuglyAgent_SetScene_m2773184852 (Il2CppObject * __this /* static, unused */, int32_t ___sceneId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuglyAgent::AddSceneData(System.String,System.String)
extern "C"  void BuglyAgent_AddSceneData_m2933483472 (Il2CppObject * __this /* static, unused */, String_t* ___key0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuglyAgent::ConfigDebugMode(System.Boolean)
extern "C"  void BuglyAgent_ConfigDebugMode_m3082024052 (Il2CppObject * __this /* static, unused */, bool ___enable0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuglyAgent::ConfigAutoQuitApplication(System.Boolean)
extern "C"  void BuglyAgent_ConfigAutoQuitApplication_m4019756080 (Il2CppObject * __this /* static, unused */, bool ___autoQuit0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuglyAgent::ConfigAutoReportLogLevel(LogSeverity)
extern "C"  void BuglyAgent_ConfigAutoReportLogLevel_m3131243571 (Il2CppObject * __this /* static, unused */, int32_t ___level0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuglyAgent::ConfigDefault(System.String,System.String,System.String,System.Int64)
extern "C"  void BuglyAgent_ConfigDefault_m4037068102 (Il2CppObject * __this /* static, unused */, String_t* ___channel0, String_t* ___version1, String_t* ___user2, int64_t ___delay3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuglyAgent::DebugLog(System.String,System.String,System.Object[])
extern "C"  void BuglyAgent_DebugLog_m609003136 (Il2CppObject * __this /* static, unused */, String_t* ___tag0, String_t* ___format1, ObjectU5BU5D_t1108656482* ___args2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuglyAgent::PrintLog(LogSeverity,System.String,System.Object[])
extern "C"  void BuglyAgent_PrintLog_m1023618409 (Il2CppObject * __this /* static, unused */, int32_t ___level0, String_t* ___format1, ObjectU5BU5D_t1108656482* ___args2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuglyAgent::ConfigCrashReporterType()
extern "C"  void BuglyAgent_ConfigCrashReporterType_m1930542889 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuglyAgent::ConfigDefaultBeforeInit(System.String,System.String,System.String,System.Int64)
extern "C"  void BuglyAgent_ConfigDefaultBeforeInit_m3838653653 (Il2CppObject * __this /* static, unused */, String_t* ___channel0, String_t* ___version1, String_t* ___user2, int64_t ___delay3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuglyAgent::EnableDebugMode(System.Boolean)
extern "C"  void BuglyAgent_EnableDebugMode_m2898701651 (Il2CppObject * __this /* static, unused */, bool ___enable0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuglyAgent::InitBuglyAgent(System.String)
extern "C"  void BuglyAgent_InitBuglyAgent_m1360743349 (Il2CppObject * __this /* static, unused */, String_t* ___appId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuglyAgent::SetUnityVersion()
extern "C"  void BuglyAgent_SetUnityVersion_m1918232878 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuglyAgent::SetUserInfo(System.String)
extern "C"  void BuglyAgent_SetUserInfo_m840563038 (Il2CppObject * __this /* static, unused */, String_t* ___userInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuglyAgent::ReportException(System.Int32,System.String,System.String,System.String,System.Boolean)
extern "C"  void BuglyAgent_ReportException_m769667476 (Il2CppObject * __this /* static, unused */, int32_t ___type0, String_t* ___name1, String_t* ___reason2, String_t* ___stackTrace3, bool ___quitProgram4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuglyAgent::SetCurrentScene(System.Int32)
extern "C"  void BuglyAgent_SetCurrentScene_m1379816559 (Il2CppObject * __this /* static, unused */, int32_t ___sceneId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuglyAgent::AddKeyAndValueInScene(System.String,System.String)
extern "C"  void BuglyAgent_AddKeyAndValueInScene_m1823870950 (Il2CppObject * __this /* static, unused */, String_t* ___key0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuglyAgent::AddExtraDataWithException(System.String,System.String)
extern "C"  void BuglyAgent_AddExtraDataWithException_m1806990949 (Il2CppObject * __this /* static, unused */, String_t* ___key0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuglyAgent::LogRecord(LogSeverity,System.String)
extern "C"  void BuglyAgent_LogRecord_m2405146447 (Il2CppObject * __this /* static, unused */, int32_t ___level0, String_t* ___message1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuglyAgent::LogSeverityToInt(LogSeverity)
extern "C"  int32_t BuglyAgent_LogSeverityToInt_m2971150991 (Il2CppObject * __this /* static, unused */, int32_t ___logLevel0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuglyAgent::_BuglyInit(System.String,System.Boolean,System.Int32)
extern "C"  void BuglyAgent__BuglyInit_m2210360241 (Il2CppObject * __this /* static, unused */, String_t* ___appId0, bool ___debug1, int32_t ___level2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuglyAgent::_BuglySetUserId(System.String)
extern "C"  void BuglyAgent__BuglySetUserId_m488354291 (Il2CppObject * __this /* static, unused */, String_t* ___userId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuglyAgent::_BuglySetTag(System.Int32)
extern "C"  void BuglyAgent__BuglySetTag_m1900775492 (Il2CppObject * __this /* static, unused */, int32_t ___tag0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuglyAgent::_BuglySetKeyValue(System.String,System.String)
extern "C"  void BuglyAgent__BuglySetKeyValue_m3618607715 (Il2CppObject * __this /* static, unused */, String_t* ___key0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuglyAgent::_BuglyReportException(System.Int32,System.String,System.String,System.String,System.String,System.Boolean)
extern "C"  void BuglyAgent__BuglyReportException_m987831418 (Il2CppObject * __this /* static, unused */, int32_t ___type0, String_t* ___name1, String_t* ___reason2, String_t* ___stackTrace3, String_t* ___extras4, bool ___quit5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuglyAgent::_BuglyDefaultConfig(System.String,System.String,System.String,System.String)
extern "C"  void BuglyAgent__BuglyDefaultConfig_m1986590636 (Il2CppObject * __this /* static, unused */, String_t* ___channel0, String_t* ___version1, String_t* ___user2, String_t* ___deviceId3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuglyAgent::_BuglyLogMessage(System.Int32,System.String,System.String)
extern "C"  void BuglyAgent__BuglyLogMessage_m58191687 (Il2CppObject * __this /* static, unused */, int32_t ___level0, String_t* ___tag1, String_t* ___log2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuglyAgent::_BuglyConfigCrashReporterType(System.Int32)
extern "C"  void BuglyAgent__BuglyConfigCrashReporterType_m3603612760 (Il2CppObject * __this /* static, unused */, int32_t ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuglyAgent::_BuglySetExtraConfig(System.String,System.String)
extern "C"  void BuglyAgent__BuglySetExtraConfig_m2740188051 (Il2CppObject * __this /* static, unused */, String_t* ___key0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String BuglyAgent::get_PluginVersion()
extern "C"  String_t* BuglyAgent_get_PluginVersion_m1696173506 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BuglyAgent::get_IsInitialized()
extern "C"  bool BuglyAgent_get_IsInitialized_m1689956366 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BuglyAgent::get_AutoQuitApplicationAfterReport()
extern "C"  bool BuglyAgent_get_AutoQuitApplicationAfterReport_m1259078300 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuglyAgent::_SetCrashReporterType(System.Int32)
extern "C"  void BuglyAgent__SetCrashReporterType_m3845894329 (Il2CppObject * __this /* static, unused */, int32_t ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuglyAgent::_SetCrashReporterLogLevel(System.Int32)
extern "C"  void BuglyAgent__SetCrashReporterLogLevel_m799001375 (Il2CppObject * __this /* static, unused */, int32_t ___logLevel0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuglyAgent::_RegisterExceptionHandler()
extern "C"  void BuglyAgent__RegisterExceptionHandler_m1437323366 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuglyAgent::_UnregisterExceptionHandler()
extern "C"  void BuglyAgent__UnregisterExceptionHandler_m963802111 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuglyAgent::_OnLogCallbackHandler(System.String,System.String,UnityEngine.LogType)
extern "C"  void BuglyAgent__OnLogCallbackHandler_m3249997691 (Il2CppObject * __this /* static, unused */, String_t* ___condition0, String_t* ___stackTrace1, int32_t ___type2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuglyAgent::_OnUncaughtExceptionHandler(System.Object,System.UnhandledExceptionEventArgs)
extern "C"  void BuglyAgent__OnUncaughtExceptionHandler_m228339741 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___sender0, UnhandledExceptionEventArgs_t3134093121 * ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuglyAgent::_HandleException(System.Exception,System.String,System.Boolean)
extern "C"  void BuglyAgent__HandleException_m1464489584 (Il2CppObject * __this /* static, unused */, Exception_t3991598821 * ___e0, String_t* ___message1, bool ___uncaught2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuglyAgent::_reportException(System.Boolean,System.String,System.String,System.String)
extern "C"  void BuglyAgent__reportException_m40291648 (Il2CppObject * __this /* static, unused */, bool ___uncaught0, String_t* ___name1, String_t* ___reason2, String_t* ___stackTrace3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuglyAgent::_HandleException(LogSeverity,System.String,System.String,System.String,System.Boolean)
extern "C"  void BuglyAgent__HandleException_m1838932569 (Il2CppObject * __this /* static, unused */, int32_t ___logLevel0, String_t* ___name1, String_t* ___message2, String_t* ___stackTrace3, bool ___uncaught4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
