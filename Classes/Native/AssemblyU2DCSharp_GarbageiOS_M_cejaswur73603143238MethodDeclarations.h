﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_cejaswur73
struct M_cejaswur73_t603143238;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_cejaswur73603143238.h"

// System.Void GarbageiOS.M_cejaswur73::.ctor()
extern "C"  void M_cejaswur73__ctor_m3952901805 (M_cejaswur73_t603143238 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_cejaswur73::M_nidoocou0(System.String[],System.Int32)
extern "C"  void M_cejaswur73_M_nidoocou0_m3869647904 (M_cejaswur73_t603143238 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_cejaswur73::M_temmoudarYallwhutir1(System.String[],System.Int32)
extern "C"  void M_cejaswur73_M_temmoudarYallwhutir1_m2674977682 (M_cejaswur73_t603143238 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_cejaswur73::ilo_M_nidoocou01(GarbageiOS.M_cejaswur73,System.String[],System.Int32)
extern "C"  void M_cejaswur73_ilo_M_nidoocou01_m1517276038 (Il2CppObject * __this /* static, unused */, M_cejaswur73_t603143238 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
