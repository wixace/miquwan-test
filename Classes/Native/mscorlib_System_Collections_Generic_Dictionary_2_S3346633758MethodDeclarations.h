﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<AIEnum.EAIEventtype,System.Object>
struct ShimEnumerator_t3346633758;
// System.Collections.Generic.Dictionary`2<AIEnum.EAIEventtype,System.Object>
struct Dictionary_2_t3630855731;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<AIEnum.EAIEventtype,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m1051670600_gshared (ShimEnumerator_t3346633758 * __this, Dictionary_2_t3630855731 * ___host0, const MethodInfo* method);
#define ShimEnumerator__ctor_m1051670600(__this, ___host0, method) ((  void (*) (ShimEnumerator_t3346633758 *, Dictionary_2_t3630855731 *, const MethodInfo*))ShimEnumerator__ctor_m1051670600_gshared)(__this, ___host0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<AIEnum.EAIEventtype,System.Object>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m3491586137_gshared (ShimEnumerator_t3346633758 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m3491586137(__this, method) ((  bool (*) (ShimEnumerator_t3346633758 *, const MethodInfo*))ShimEnumerator_MoveNext_m3491586137_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<AIEnum.EAIEventtype,System.Object>::get_Entry()
extern "C"  DictionaryEntry_t1751606614  ShimEnumerator_get_Entry_m1672122075_gshared (ShimEnumerator_t3346633758 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m1672122075(__this, method) ((  DictionaryEntry_t1751606614  (*) (ShimEnumerator_t3346633758 *, const MethodInfo*))ShimEnumerator_get_Entry_m1672122075_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<AIEnum.EAIEventtype,System.Object>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m3868635894_gshared (ShimEnumerator_t3346633758 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m3868635894(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t3346633758 *, const MethodInfo*))ShimEnumerator_get_Key_m3868635894_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<AIEnum.EAIEventtype,System.Object>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m3660819464_gshared (ShimEnumerator_t3346633758 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m3660819464(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t3346633758 *, const MethodInfo*))ShimEnumerator_get_Value_m3660819464_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<AIEnum.EAIEventtype,System.Object>::get_Current()
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m1893980304_gshared (ShimEnumerator_t3346633758 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m1893980304(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t3346633758 *, const MethodInfo*))ShimEnumerator_get_Current_m1893980304_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<AIEnum.EAIEventtype,System.Object>::Reset()
extern "C"  void ShimEnumerator_Reset_m1183835802_gshared (ShimEnumerator_t3346633758 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m1183835802(__this, method) ((  void (*) (ShimEnumerator_t3346633758 *, const MethodInfo*))ShimEnumerator_Reset_m1183835802_gshared)(__this, method)
