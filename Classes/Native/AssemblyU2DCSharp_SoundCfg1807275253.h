﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "AssemblyU2DCSharp_CsCfgBase69924517.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SoundCfg
struct  SoundCfg_t1807275253  : public CsCfgBase_t69924517
{
public:
	// System.Int32 SoundCfg::id
	int32_t ___id_0;
	// System.String SoundCfg::path
	String_t* ___path_1;
	// System.Boolean SoundCfg::loop
	bool ___loop_2;
	// System.Int32 SoundCfg::soundtype
	int32_t ___soundtype_3;
	// System.Int32 SoundCfg::playtype
	int32_t ___playtype_4;
	// System.Int32 SoundCfg::replacetype
	int32_t ___replacetype_5;
	// System.Single SoundCfg::volume
	float ___volume_6;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(SoundCfg_t1807275253, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_path_1() { return static_cast<int32_t>(offsetof(SoundCfg_t1807275253, ___path_1)); }
	inline String_t* get_path_1() const { return ___path_1; }
	inline String_t** get_address_of_path_1() { return &___path_1; }
	inline void set_path_1(String_t* value)
	{
		___path_1 = value;
		Il2CppCodeGenWriteBarrier(&___path_1, value);
	}

	inline static int32_t get_offset_of_loop_2() { return static_cast<int32_t>(offsetof(SoundCfg_t1807275253, ___loop_2)); }
	inline bool get_loop_2() const { return ___loop_2; }
	inline bool* get_address_of_loop_2() { return &___loop_2; }
	inline void set_loop_2(bool value)
	{
		___loop_2 = value;
	}

	inline static int32_t get_offset_of_soundtype_3() { return static_cast<int32_t>(offsetof(SoundCfg_t1807275253, ___soundtype_3)); }
	inline int32_t get_soundtype_3() const { return ___soundtype_3; }
	inline int32_t* get_address_of_soundtype_3() { return &___soundtype_3; }
	inline void set_soundtype_3(int32_t value)
	{
		___soundtype_3 = value;
	}

	inline static int32_t get_offset_of_playtype_4() { return static_cast<int32_t>(offsetof(SoundCfg_t1807275253, ___playtype_4)); }
	inline int32_t get_playtype_4() const { return ___playtype_4; }
	inline int32_t* get_address_of_playtype_4() { return &___playtype_4; }
	inline void set_playtype_4(int32_t value)
	{
		___playtype_4 = value;
	}

	inline static int32_t get_offset_of_replacetype_5() { return static_cast<int32_t>(offsetof(SoundCfg_t1807275253, ___replacetype_5)); }
	inline int32_t get_replacetype_5() const { return ___replacetype_5; }
	inline int32_t* get_address_of_replacetype_5() { return &___replacetype_5; }
	inline void set_replacetype_5(int32_t value)
	{
		___replacetype_5 = value;
	}

	inline static int32_t get_offset_of_volume_6() { return static_cast<int32_t>(offsetof(SoundCfg_t1807275253, ___volume_6)); }
	inline float get_volume_6() const { return ___volume_6; }
	inline float* get_address_of_volume_6() { return &___volume_6; }
	inline void set_volume_6(float value)
	{
		___volume_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
