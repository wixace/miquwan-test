﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._511c34ba2c0cd139eaa8d39245efaa8e
struct _511c34ba2c0cd139eaa8d39245efaa8e_t903363688;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._511c34ba2c0cd139eaa8d39245efaa8e::.ctor()
extern "C"  void _511c34ba2c0cd139eaa8d39245efaa8e__ctor_m222584517 (_511c34ba2c0cd139eaa8d39245efaa8e_t903363688 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._511c34ba2c0cd139eaa8d39245efaa8e::_511c34ba2c0cd139eaa8d39245efaa8em2(System.Int32)
extern "C"  int32_t _511c34ba2c0cd139eaa8d39245efaa8e__511c34ba2c0cd139eaa8d39245efaa8em2_m313948313 (_511c34ba2c0cd139eaa8d39245efaa8e_t903363688 * __this, int32_t ____511c34ba2c0cd139eaa8d39245efaa8ea0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._511c34ba2c0cd139eaa8d39245efaa8e::_511c34ba2c0cd139eaa8d39245efaa8em(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _511c34ba2c0cd139eaa8d39245efaa8e__511c34ba2c0cd139eaa8d39245efaa8em_m2212499005 (_511c34ba2c0cd139eaa8d39245efaa8e_t903363688 * __this, int32_t ____511c34ba2c0cd139eaa8d39245efaa8ea0, int32_t ____511c34ba2c0cd139eaa8d39245efaa8e281, int32_t ____511c34ba2c0cd139eaa8d39245efaa8ec2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
