﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FpsIndicator
struct  FpsIndicator_t1218517830  : public MonoBehaviour_t667441552
{
public:
	// System.Int32 FpsIndicator::frameCount
	int32_t ___frameCount_2;
	// System.Single FpsIndicator::dt
	float ___dt_3;
	// System.Single FpsIndicator::fps
	float ___fps_4;
	// System.Single FpsIndicator::updateRate
	float ___updateRate_5;
	// System.Single FpsIndicator::y
	float ___y_6;

public:
	inline static int32_t get_offset_of_frameCount_2() { return static_cast<int32_t>(offsetof(FpsIndicator_t1218517830, ___frameCount_2)); }
	inline int32_t get_frameCount_2() const { return ___frameCount_2; }
	inline int32_t* get_address_of_frameCount_2() { return &___frameCount_2; }
	inline void set_frameCount_2(int32_t value)
	{
		___frameCount_2 = value;
	}

	inline static int32_t get_offset_of_dt_3() { return static_cast<int32_t>(offsetof(FpsIndicator_t1218517830, ___dt_3)); }
	inline float get_dt_3() const { return ___dt_3; }
	inline float* get_address_of_dt_3() { return &___dt_3; }
	inline void set_dt_3(float value)
	{
		___dt_3 = value;
	}

	inline static int32_t get_offset_of_fps_4() { return static_cast<int32_t>(offsetof(FpsIndicator_t1218517830, ___fps_4)); }
	inline float get_fps_4() const { return ___fps_4; }
	inline float* get_address_of_fps_4() { return &___fps_4; }
	inline void set_fps_4(float value)
	{
		___fps_4 = value;
	}

	inline static int32_t get_offset_of_updateRate_5() { return static_cast<int32_t>(offsetof(FpsIndicator_t1218517830, ___updateRate_5)); }
	inline float get_updateRate_5() const { return ___updateRate_5; }
	inline float* get_address_of_updateRate_5() { return &___updateRate_5; }
	inline void set_updateRate_5(float value)
	{
		___updateRate_5 = value;
	}

	inline static int32_t get_offset_of_y_6() { return static_cast<int32_t>(offsetof(FpsIndicator_t1218517830, ___y_6)); }
	inline float get_y_6() const { return ___y_6; }
	inline float* get_address_of_y_6() { return &___y_6; }
	inline void set_y_6(float value)
	{
		___y_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
