﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIGet
struct UIGet_t80745506;

#include "codegen/il2cpp-codegen.h"

// System.Void UIGet::.ctor()
extern "C"  void UIGet__ctor_m3149740937 (UIGet_t80745506 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIGet::Start()
extern "C"  void UIGet_Start_m2096878729 (UIGet_t80745506 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIGet::<Start>m__48A()
extern "C"  void UIGet_U3CStartU3Em__48A_m1579953231 (UIGet_t80745506 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
