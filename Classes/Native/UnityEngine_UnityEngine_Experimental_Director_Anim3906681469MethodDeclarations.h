﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Experimental.Director.AnimatorControllerPlayable
struct AnimatorControllerPlayable_t3906681469;
// System.String
struct String_t;
// UnityEngine.AnimatorClipInfo[]
struct AnimatorClipInfoU5BU5D_t3117370260;
// UnityEngine.AnimatorControllerParameter[]
struct AnimatorControllerParameterU5BU5D_t1853597653;
// UnityEngine.AnimatorControllerParameter
struct AnimatorControllerParameter_t3339705788;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "UnityEngine_UnityEngine_AnimatorStateInfo323110318.h"
#include "UnityEngine_UnityEngine_AnimatorTransitionInfo2817229998.h"

// System.Single UnityEngine.Experimental.Director.AnimatorControllerPlayable::GetFloat(System.String)
extern "C"  float AnimatorControllerPlayable_GetFloat_m747363244 (AnimatorControllerPlayable_t3906681469 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Experimental.Director.AnimatorControllerPlayable::GetFloat(System.Int32)
extern "C"  float AnimatorControllerPlayable_GetFloat_m2503228263 (AnimatorControllerPlayable_t3906681469 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Experimental.Director.AnimatorControllerPlayable::SetFloat(System.String,System.Single)
extern "C"  void AnimatorControllerPlayable_SetFloat_m3624679977 (AnimatorControllerPlayable_t3906681469 * __this, String_t* ___name0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Experimental.Director.AnimatorControllerPlayable::SetFloat(System.Int32,System.Single)
extern "C"  void AnimatorControllerPlayable_SetFloat_m255402612 (AnimatorControllerPlayable_t3906681469 * __this, int32_t ___id0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Experimental.Director.AnimatorControllerPlayable::GetBool(System.String)
extern "C"  bool AnimatorControllerPlayable_GetBool_m4206709642 (AnimatorControllerPlayable_t3906681469 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Experimental.Director.AnimatorControllerPlayable::GetBool(System.Int32)
extern "C"  bool AnimatorControllerPlayable_GetBool_m1229346761 (AnimatorControllerPlayable_t3906681469 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Experimental.Director.AnimatorControllerPlayable::SetBool(System.String,System.Boolean)
extern "C"  void AnimatorControllerPlayable_SetBool_m4186410341 (AnimatorControllerPlayable_t3906681469 * __this, String_t* ___name0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Experimental.Director.AnimatorControllerPlayable::SetBool(System.Int32,System.Boolean)
extern "C"  void AnimatorControllerPlayable_SetBool_m2000218018 (AnimatorControllerPlayable_t3906681469 * __this, int32_t ___id0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Experimental.Director.AnimatorControllerPlayable::GetInteger(System.String)
extern "C"  int32_t AnimatorControllerPlayable_GetInteger_m752334550 (AnimatorControllerPlayable_t3906681469 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Experimental.Director.AnimatorControllerPlayable::GetInteger(System.Int32)
extern "C"  int32_t AnimatorControllerPlayable_GetInteger_m3888861949 (AnimatorControllerPlayable_t3906681469 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Experimental.Director.AnimatorControllerPlayable::SetInteger(System.String,System.Int32)
extern "C"  void AnimatorControllerPlayable_SetInteger_m1460443317 (AnimatorControllerPlayable_t3906681469 * __this, String_t* ___name0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Experimental.Director.AnimatorControllerPlayable::SetInteger(System.Int32,System.Int32)
extern "C"  void AnimatorControllerPlayable_SetInteger_m2661791622 (AnimatorControllerPlayable_t3906681469 * __this, int32_t ___id0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Experimental.Director.AnimatorControllerPlayable::SetTrigger(System.String)
extern "C"  void AnimatorControllerPlayable_SetTrigger_m2580020136 (AnimatorControllerPlayable_t3906681469 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Experimental.Director.AnimatorControllerPlayable::SetTrigger(System.Int32)
extern "C"  void AnimatorControllerPlayable_SetTrigger_m2285251563 (AnimatorControllerPlayable_t3906681469 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Experimental.Director.AnimatorControllerPlayable::ResetTrigger(System.String)
extern "C"  void AnimatorControllerPlayable_ResetTrigger_m678281621 (AnimatorControllerPlayable_t3906681469 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Experimental.Director.AnimatorControllerPlayable::ResetTrigger(System.Int32)
extern "C"  void AnimatorControllerPlayable_ResetTrigger_m838431838 (AnimatorControllerPlayable_t3906681469 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Experimental.Director.AnimatorControllerPlayable::IsParameterControlledByCurve(System.String)
extern "C"  bool AnimatorControllerPlayable_IsParameterControlledByCurve_m3668305165 (AnimatorControllerPlayable_t3906681469 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Experimental.Director.AnimatorControllerPlayable::IsParameterControlledByCurve(System.Int32)
extern "C"  bool AnimatorControllerPlayable_IsParameterControlledByCurve_m519242214 (AnimatorControllerPlayable_t3906681469 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Experimental.Director.AnimatorControllerPlayable::get_layerCount()
extern "C"  int32_t AnimatorControllerPlayable_get_layerCount_m2003233803 (AnimatorControllerPlayable_t3906681469 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Experimental.Director.AnimatorControllerPlayable::GetLayerName(System.Int32)
extern "C"  String_t* AnimatorControllerPlayable_GetLayerName_m4158759006 (AnimatorControllerPlayable_t3906681469 * __this, int32_t ___layerIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Experimental.Director.AnimatorControllerPlayable::GetLayerIndex(System.String)
extern "C"  int32_t AnimatorControllerPlayable_GetLayerIndex_m2379852397 (AnimatorControllerPlayable_t3906681469 * __this, String_t* ___layerName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Experimental.Director.AnimatorControllerPlayable::GetLayerWeight(System.Int32)
extern "C"  float AnimatorControllerPlayable_GetLayerWeight_m1653318324 (AnimatorControllerPlayable_t3906681469 * __this, int32_t ___layerIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Experimental.Director.AnimatorControllerPlayable::SetLayerWeight(System.Int32,System.Single)
extern "C"  void AnimatorControllerPlayable_SetLayerWeight_m4156707073 (AnimatorControllerPlayable_t3906681469 * __this, int32_t ___layerIndex0, float ___weight1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AnimatorStateInfo UnityEngine.Experimental.Director.AnimatorControllerPlayable::GetCurrentAnimatorStateInfo(System.Int32)
extern "C"  AnimatorStateInfo_t323110318  AnimatorControllerPlayable_GetCurrentAnimatorStateInfo_m480605298 (AnimatorControllerPlayable_t3906681469 * __this, int32_t ___layerIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AnimatorStateInfo UnityEngine.Experimental.Director.AnimatorControllerPlayable::GetNextAnimatorStateInfo(System.Int32)
extern "C"  AnimatorStateInfo_t323110318  AnimatorControllerPlayable_GetNextAnimatorStateInfo_m273644054 (AnimatorControllerPlayable_t3906681469 * __this, int32_t ___layerIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AnimatorTransitionInfo UnityEngine.Experimental.Director.AnimatorControllerPlayable::GetAnimatorTransitionInfo(System.Int32)
extern "C"  AnimatorTransitionInfo_t2817229998  AnimatorControllerPlayable_GetAnimatorTransitionInfo_m2238886913 (AnimatorControllerPlayable_t3906681469 * __this, int32_t ___layerIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AnimatorClipInfo[] UnityEngine.Experimental.Director.AnimatorControllerPlayable::GetCurrentAnimatorClipInfo(System.Int32)
extern "C"  AnimatorClipInfoU5BU5D_t3117370260* AnimatorControllerPlayable_GetCurrentAnimatorClipInfo_m2075319150 (AnimatorControllerPlayable_t3906681469 * __this, int32_t ___layerIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AnimatorClipInfo[] UnityEngine.Experimental.Director.AnimatorControllerPlayable::GetNextAnimatorClipInfo(System.Int32)
extern "C"  AnimatorClipInfoU5BU5D_t3117370260* AnimatorControllerPlayable_GetNextAnimatorClipInfo_m330906988 (AnimatorControllerPlayable_t3906681469 * __this, int32_t ___layerIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Experimental.Director.AnimatorControllerPlayable::IsInTransition(System.Int32)
extern "C"  bool AnimatorControllerPlayable_IsInTransition_m3586059519 (AnimatorControllerPlayable_t3906681469 * __this, int32_t ___layerIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Experimental.Director.AnimatorControllerPlayable::get_parameterCount()
extern "C"  int32_t AnimatorControllerPlayable_get_parameterCount_m266646707 (AnimatorControllerPlayable_t3906681469 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AnimatorControllerParameter[] UnityEngine.Experimental.Director.AnimatorControllerPlayable::get_parameters()
extern "C"  AnimatorControllerParameterU5BU5D_t1853597653* AnimatorControllerPlayable_get_parameters_m1287792089 (AnimatorControllerPlayable_t3906681469 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AnimatorControllerParameter UnityEngine.Experimental.Director.AnimatorControllerPlayable::GetParameter(System.Int32)
extern "C"  AnimatorControllerParameter_t3339705788 * AnimatorControllerPlayable_GetParameter_m656649068 (AnimatorControllerPlayable_t3906681469 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Experimental.Director.AnimatorControllerPlayable::StringToHash(System.String)
extern "C"  int32_t AnimatorControllerPlayable_StringToHash_m1050761412 (Il2CppObject * __this /* static, unused */, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Experimental.Director.AnimatorControllerPlayable::CrossFadeInFixedTime(System.String,System.Single,System.Int32,System.Single)
extern "C"  void AnimatorControllerPlayable_CrossFadeInFixedTime_m1594511961 (AnimatorControllerPlayable_t3906681469 * __this, String_t* ___stateName0, float ___transitionDuration1, int32_t ___layer2, float ___fixedTime3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Experimental.Director.AnimatorControllerPlayable::CrossFadeInFixedTime(System.Int32,System.Single,System.Int32,System.Single)
extern "C"  void AnimatorControllerPlayable_CrossFadeInFixedTime_m2308518178 (AnimatorControllerPlayable_t3906681469 * __this, int32_t ___stateNameHash0, float ___transitionDuration1, int32_t ___layer2, float ___fixedTime3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Experimental.Director.AnimatorControllerPlayable::CrossFade(System.String,System.Single,System.Int32,System.Single)
extern "C"  void AnimatorControllerPlayable_CrossFade_m43193999 (AnimatorControllerPlayable_t3906681469 * __this, String_t* ___stateName0, float ___transitionDuration1, int32_t ___layer2, float ___normalizedTime3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Experimental.Director.AnimatorControllerPlayable::CrossFade(System.Int32,System.Single,System.Int32,System.Single)
extern "C"  void AnimatorControllerPlayable_CrossFade_m3228306988 (AnimatorControllerPlayable_t3906681469 * __this, int32_t ___stateNameHash0, float ___transitionDuration1, int32_t ___layer2, float ___normalizedTime3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Experimental.Director.AnimatorControllerPlayable::PlayInFixedTime(System.String,System.Int32,System.Single)
extern "C"  void AnimatorControllerPlayable_PlayInFixedTime_m1703207904 (AnimatorControllerPlayable_t3906681469 * __this, String_t* ___stateName0, int32_t ___layer1, float ___fixedTime2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Experimental.Director.AnimatorControllerPlayable::PlayInFixedTime(System.Int32,System.Int32,System.Single)
extern "C"  void AnimatorControllerPlayable_PlayInFixedTime_m1791936037 (AnimatorControllerPlayable_t3906681469 * __this, int32_t ___stateNameHash0, int32_t ___layer1, float ___fixedTime2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Experimental.Director.AnimatorControllerPlayable::Play(System.String,System.Int32,System.Single)
extern "C"  void AnimatorControllerPlayable_Play_m249230290 (AnimatorControllerPlayable_t3906681469 * __this, String_t* ___stateName0, int32_t ___layer1, float ___normalizedTime2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Experimental.Director.AnimatorControllerPlayable::Play(System.Int32,System.Int32,System.Single)
extern "C"  void AnimatorControllerPlayable_Play_m3546148851 (AnimatorControllerPlayable_t3906681469 * __this, int32_t ___stateNameHash0, int32_t ___layer1, float ___normalizedTime2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Experimental.Director.AnimatorControllerPlayable::HasState(System.Int32,System.Int32)
extern "C"  bool AnimatorControllerPlayable_HasState_m316350501 (AnimatorControllerPlayable_t3906681469 * __this, int32_t ___layerIndex0, int32_t ___stateID1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Experimental.Director.AnimatorControllerPlayable::SetFloatString(System.String,System.Single)
extern "C"  void AnimatorControllerPlayable_SetFloatString_m1355051352 (AnimatorControllerPlayable_t3906681469 * __this, String_t* ___name0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Experimental.Director.AnimatorControllerPlayable::SetFloatID(System.Int32,System.Single)
extern "C"  void AnimatorControllerPlayable_SetFloatID_m2321295823 (AnimatorControllerPlayable_t3906681469 * __this, int32_t ___id0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Experimental.Director.AnimatorControllerPlayable::GetFloatString(System.String)
extern "C"  float AnimatorControllerPlayable_GetFloatString_m1293816603 (AnimatorControllerPlayable_t3906681469 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Experimental.Director.AnimatorControllerPlayable::GetFloatID(System.Int32)
extern "C"  float AnimatorControllerPlayable_GetFloatID_m2436917250 (AnimatorControllerPlayable_t3906681469 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Experimental.Director.AnimatorControllerPlayable::SetBoolString(System.String,System.Boolean)
extern "C"  void AnimatorControllerPlayable_SetBoolString_m1548094230 (AnimatorControllerPlayable_t3906681469 * __this, String_t* ___name0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Experimental.Director.AnimatorControllerPlayable::SetBoolID(System.Int32,System.Boolean)
extern "C"  void AnimatorControllerPlayable_SetBoolID_m2018485927 (AnimatorControllerPlayable_t3906681469 * __this, int32_t ___id0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Experimental.Director.AnimatorControllerPlayable::GetBoolString(System.String)
extern "C"  bool AnimatorControllerPlayable_GetBoolString_m2783421561 (AnimatorControllerPlayable_t3906681469 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Experimental.Director.AnimatorControllerPlayable::GetBoolID(System.Int32)
extern "C"  bool AnimatorControllerPlayable_GetBoolID_m2302473188 (AnimatorControllerPlayable_t3906681469 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Experimental.Director.AnimatorControllerPlayable::SetIntegerString(System.String,System.Int32)
extern "C"  void AnimatorControllerPlayable_SetIntegerString_m1907654182 (AnimatorControllerPlayable_t3906681469 * __this, String_t* ___name0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Experimental.Director.AnimatorControllerPlayable::SetIntegerID(System.Int32,System.Int32)
extern "C"  void AnimatorControllerPlayable_SetIntegerID_m3895544139 (AnimatorControllerPlayable_t3906681469 * __this, int32_t ___id0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Experimental.Director.AnimatorControllerPlayable::GetIntegerString(System.String)
extern "C"  int32_t AnimatorControllerPlayable_GetIntegerString_m1268737733 (AnimatorControllerPlayable_t3906681469 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Experimental.Director.AnimatorControllerPlayable::GetIntegerID(System.Int32)
extern "C"  int32_t AnimatorControllerPlayable_GetIntegerID_m2591027736 (AnimatorControllerPlayable_t3906681469 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Experimental.Director.AnimatorControllerPlayable::SetTriggerString(System.String)
extern "C"  void AnimatorControllerPlayable_SetTriggerString_m3509143063 (AnimatorControllerPlayable_t3906681469 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Experimental.Director.AnimatorControllerPlayable::SetTriggerID(System.Int32)
extern "C"  void AnimatorControllerPlayable_SetTriggerID_m3414706054 (AnimatorControllerPlayable_t3906681469 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Experimental.Director.AnimatorControllerPlayable::ResetTriggerString(System.String)
extern "C"  void AnimatorControllerPlayable_ResetTriggerString_m885794372 (AnimatorControllerPlayable_t3906681469 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Experimental.Director.AnimatorControllerPlayable::ResetTriggerID(System.Int32)
extern "C"  void AnimatorControllerPlayable_ResetTriggerID_m295386937 (AnimatorControllerPlayable_t3906681469 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Experimental.Director.AnimatorControllerPlayable::IsParameterControlledByCurveString(System.String)
extern "C"  bool AnimatorControllerPlayable_IsParameterControlledByCurveString_m2735329212 (AnimatorControllerPlayable_t3906681469 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Experimental.Director.AnimatorControllerPlayable::IsParameterControlledByCurveID(System.Int32)
extern "C"  bool AnimatorControllerPlayable_IsParameterControlledByCurveID_m2791803585 (AnimatorControllerPlayable_t3906681469 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
