﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.FloodPathTracer
struct FloodPathTracer_t1668942290;
// Pathfinding.FloodPath
struct FloodPath_t3766979749;
// OnPathDelegate
struct OnPathDelegate_t598607977;
// Pathfinding.GraphNode
struct GraphNode_t23612370;
// Pathfinding.Path
struct Path_t1974241691;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_Pathfinding_FloodPath3766979749.h"
#include "AssemblyU2DCSharp_OnPathDelegate598607977.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphNode23612370.h"
#include "AssemblyU2DCSharp_PathState1615290188.h"
#include "AssemblyU2DCSharp_Pathfinding_Path1974241691.h"

// System.Void Pathfinding.FloodPathTracer::.ctor(UnityEngine.Vector3,Pathfinding.FloodPath,OnPathDelegate)
extern "C"  void FloodPathTracer__ctor_m4281585878 (FloodPathTracer_t1668942290 * __this, Vector3_t4282066566  ___start0, FloodPath_t3766979749 * ___flood1, OnPathDelegate_t598607977 * ___callbackDelegate2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.FloodPathTracer::.ctor()
extern "C"  void FloodPathTracer__ctor_m626525605 (FloodPathTracer_t1668942290 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.FloodPathTracer Pathfinding.FloodPathTracer::Construct(UnityEngine.Vector3,Pathfinding.FloodPath,OnPathDelegate)
extern "C"  FloodPathTracer_t1668942290 * FloodPathTracer_Construct_m3652970968 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___start0, FloodPath_t3766979749 * ___flood1, OnPathDelegate_t598607977 * ___callback2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.FloodPathTracer::Setup(UnityEngine.Vector3,Pathfinding.FloodPath,OnPathDelegate)
extern "C"  void FloodPathTracer_Setup_m1578760241 (FloodPathTracer_t1668942290 * __this, Vector3_t4282066566  ___start0, FloodPath_t3766979749 * ___flood1, OnPathDelegate_t598607977 * ___callback2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.FloodPathTracer::Reset()
extern "C"  void FloodPathTracer_Reset_m2567925842 (FloodPathTracer_t1668942290 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.FloodPathTracer::Recycle()
extern "C"  void FloodPathTracer_Recycle_m1709317142 (FloodPathTracer_t1668942290 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.FloodPathTracer::Initialize()
extern "C"  void FloodPathTracer_Initialize_m1205800975 (FloodPathTracer_t1668942290 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.FloodPathTracer::CalculateStep(System.Int64)
extern "C"  void FloodPathTracer_CalculateStep_m2201444583 (FloodPathTracer_t1668942290 * __this, int64_t ___targetTick0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.FloodPathTracer::Trace(Pathfinding.GraphNode)
extern "C"  void FloodPathTracer_Trace_m2559425828 (FloodPathTracer_t1668942290 * __this, GraphNode_t23612370 * ___from0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PathState Pathfinding.FloodPathTracer::ilo_GetState1(Pathfinding.Path)
extern "C"  int32_t FloodPathTracer_ilo_GetState1_m902332286 (Il2CppObject * __this /* static, unused */, Path_t1974241691 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
