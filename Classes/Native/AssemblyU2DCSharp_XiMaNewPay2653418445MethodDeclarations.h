﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// XiMaNewPay
struct XiMaNewPay_t2653418445;
// System.String
struct String_t;
// UnityEngine.WWWForm
struct WWWForm_t461342257;
// Mihua.SDK.PayInfo
struct PayInfo_t1775308120;
// Newtonsoft.Json.Linq.JToken
struct JToken_t3412245951;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_Mihua_SDK_PayInfo1775308120.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JToken3412245951.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void XiMaNewPay::.ctor(System.String,System.String)
extern "C"  void XiMaNewPay__ctor_m3454208912 (XiMaNewPay_t2653418445 * __this, String_t* ___merchant_no0, String_t* ___return_url1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.WWWForm XiMaNewPay::BuildOrderParamWWWForm()
extern "C"  WWWForm_t461342257 * XiMaNewPay_BuildOrderParamWWWForm_m2080574547 (XiMaNewPay_t2653418445 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void XiMaNewPay::tiaoqian(Mihua.SDK.PayInfo)
extern "C"  void XiMaNewPay_tiaoqian_m339864200 (XiMaNewPay_t2653418445 * __this, PayInfo_t1775308120 * ___payInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void XiMaNewPay::SignCallBack(System.Boolean,System.String)
extern "C"  void XiMaNewPay_SignCallBack_m2856998219 (XiMaNewPay_t2653418445 * __this, bool ___success0, String_t* ___arg1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void XiMaNewPay::__IXmnp(System.String,System.String,System.String,System.String)
extern "C"  void XiMaNewPay___IXmnp_m3426220874 (XiMaNewPay_t2653418445 * __this, String_t* ___trade_no0, String_t* ___pay_url1, String_t* ___sign2, String_t* ___return_url3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken XiMaNewPay::ilo_get_Item1(Newtonsoft.Json.Linq.JToken,System.Object)
extern "C"  JToken_t3412245951 * XiMaNewPay_ilo_get_Item1_m3473977845 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ____this0, Il2CppObject * ___key1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
