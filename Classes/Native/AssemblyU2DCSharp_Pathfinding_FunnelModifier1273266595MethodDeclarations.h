﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.FunnelModifier
struct FunnelModifier_t1273266595;
// Pathfinding.Path
struct Path_t1974241691;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t1355284822;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_ModifierData4046421655.h"
#include "AssemblyU2DCSharp_Pathfinding_Path1974241691.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void Pathfinding.FunnelModifier::.ctor()
extern "C"  void FunnelModifier__ctor_m240682532 (FunnelModifier_t1273266595 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.ModifierData Pathfinding.FunnelModifier::get_input()
extern "C"  int32_t FunnelModifier_get_input_m3090105649 (FunnelModifier_t1273266595 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.ModifierData Pathfinding.FunnelModifier::get_output()
extern "C"  int32_t FunnelModifier_get_output_m903515036 (FunnelModifier_t1273266595 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.FunnelModifier::Apply(Pathfinding.Path,Pathfinding.ModifierData)
extern "C"  void FunnelModifier_Apply_m1674260986 (FunnelModifier_t1273266595 * __this, Path_t1974241691 * ___p0, int32_t ___source1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.FunnelModifier::RunFunnel(System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Collections.Generic.List`1<UnityEngine.Vector3>)
extern "C"  bool FunnelModifier_RunFunnel_m3608556172 (FunnelModifier_t1273266595 * __this, List_1_t1355284822 * ___left0, List_1_t1355284822 * ___right1, List_1_t1355284822 * ___funnelPath2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.FunnelModifier::ilo_Left1(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  bool FunnelModifier_ilo_Left1_m3771905242 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___a0, Vector3_t4282066566  ___b1, Vector3_t4282066566  ___p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.FunnelModifier::ilo_IsClockwise2(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  bool FunnelModifier_ilo_IsClockwise2_m773891086 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___a0, Vector3_t4282066566  ___b1, Vector3_t4282066566  ___c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.FunnelModifier::ilo_IsColinear3(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  bool FunnelModifier_ilo_IsColinear3_m4244754028 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___a0, Vector3_t4282066566  ___b1, Vector3_t4282066566  ___c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Pathfinding.FunnelModifier::ilo_TriangleArea24(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  float FunnelModifier_ilo_TriangleArea24_m3806373225 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___a0, Vector3_t4282066566  ___b1, Vector3_t4282066566  ___c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
