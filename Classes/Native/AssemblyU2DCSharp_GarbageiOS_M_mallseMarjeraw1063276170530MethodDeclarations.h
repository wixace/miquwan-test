﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_mallseMarjeraw106
struct M_mallseMarjeraw106_t3276170530;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"

// System.Void GarbageiOS.M_mallseMarjeraw106::.ctor()
extern "C"  void M_mallseMarjeraw106__ctor_m3948923521 (M_mallseMarjeraw106_t3276170530 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_mallseMarjeraw106::M_traikee0(System.String[],System.Int32)
extern "C"  void M_mallseMarjeraw106_M_traikee0_m2428206103 (M_mallseMarjeraw106_t3276170530 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
