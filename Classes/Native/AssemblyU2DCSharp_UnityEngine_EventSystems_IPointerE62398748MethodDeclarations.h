﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_EventSystems_IPointerEnterHandlerGenerated
struct UnityEngine_EventSystems_IPointerEnterHandlerGenerated_t62398748;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"

// System.Void UnityEngine_EventSystems_IPointerEnterHandlerGenerated::.ctor()
extern "C"  void UnityEngine_EventSystems_IPointerEnterHandlerGenerated__ctor_m3026924287 (UnityEngine_EventSystems_IPointerEnterHandlerGenerated_t62398748 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_EventSystems_IPointerEnterHandlerGenerated::IPointerEnterHandler_OnPointerEnter__PointerEventData(JSVCall,System.Int32)
extern "C"  bool UnityEngine_EventSystems_IPointerEnterHandlerGenerated_IPointerEnterHandler_OnPointerEnter__PointerEventData_m1881834227 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_IPointerEnterHandlerGenerated::__Register()
extern "C"  void UnityEngine_EventSystems_IPointerEnterHandlerGenerated___Register_m3247001896 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_EventSystems_IPointerEnterHandlerGenerated::ilo_getObject1(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_EventSystems_IPointerEnterHandlerGenerated_ilo_getObject1_m2062774864 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
