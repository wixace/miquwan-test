﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_raribel11
struct  M_raribel11_t58371779  : public Il2CppObject
{
public:
	// System.Int32 GarbageiOS.M_raribel11::_masea
	int32_t ____masea_0;
	// System.Int32 GarbageiOS.M_raribel11::_hastearkal
	int32_t ____hastearkal_1;
	// System.Int32 GarbageiOS.M_raribel11::_refurQallwhougaw
	int32_t ____refurQallwhougaw_2;
	// System.Single GarbageiOS.M_raribel11::_hadeHaypelcea
	float ____hadeHaypelcea_3;
	// System.String GarbageiOS.M_raribel11::_toheelaw
	String_t* ____toheelaw_4;
	// System.Int32 GarbageiOS.M_raribel11::_kasbemear
	int32_t ____kasbemear_5;
	// System.Single GarbageiOS.M_raribel11::_dornoSaybemdou
	float ____dornoSaybemdou_6;
	// System.Single GarbageiOS.M_raribel11::_jekeadeJaraw
	float ____jekeadeJaraw_7;
	// System.UInt32 GarbageiOS.M_raribel11::_kalsabo
	uint32_t ____kalsabo_8;
	// System.String GarbageiOS.M_raribel11::_neeyamai
	String_t* ____neeyamai_9;
	// System.Int32 GarbageiOS.M_raribel11::_tojisjouMosaper
	int32_t ____tojisjouMosaper_10;
	// System.String GarbageiOS.M_raribel11::_deebetou
	String_t* ____deebetou_11;
	// System.Int32 GarbageiOS.M_raribel11::_sowmalWhisgo
	int32_t ____sowmalWhisgo_12;
	// System.Single GarbageiOS.M_raribel11::_temor
	float ____temor_13;
	// System.Single GarbageiOS.M_raribel11::_wearpurCeegasror
	float ____wearpurCeegasror_14;
	// System.UInt32 GarbageiOS.M_raribel11::_chasdruMairga
	uint32_t ____chasdruMairga_15;
	// System.Boolean GarbageiOS.M_raribel11::_gatralbiRersas
	bool ____gatralbiRersas_16;
	// System.String GarbageiOS.M_raribel11::_girha
	String_t* ____girha_17;
	// System.Boolean GarbageiOS.M_raribel11::_dekena
	bool ____dekena_18;
	// System.UInt32 GarbageiOS.M_raribel11::_lirceava
	uint32_t ____lirceava_19;
	// System.Boolean GarbageiOS.M_raribel11::_yalljeljall
	bool ____yalljeljall_20;
	// System.String GarbageiOS.M_raribel11::_lelgelzel
	String_t* ____lelgelzel_21;
	// System.String GarbageiOS.M_raribel11::_cetasfor
	String_t* ____cetasfor_22;
	// System.Boolean GarbageiOS.M_raribel11::_loolouburStadiba
	bool ____loolouburStadiba_23;
	// System.Single GarbageiOS.M_raribel11::_perepowhawKarcir
	float ____perepowhawKarcir_24;
	// System.Single GarbageiOS.M_raribel11::_disjaimere
	float ____disjaimere_25;
	// System.Boolean GarbageiOS.M_raribel11::_keargepeFisjall
	bool ____keargepeFisjall_26;
	// System.UInt32 GarbageiOS.M_raribel11::_wheadrarPowrem
	uint32_t ____wheadrarPowrem_27;
	// System.Int32 GarbageiOS.M_raribel11::_cousair
	int32_t ____cousair_28;
	// System.String GarbageiOS.M_raribel11::_tereraJircar
	String_t* ____tereraJircar_29;
	// System.Boolean GarbageiOS.M_raribel11::_zele
	bool ____zele_30;
	// System.Int32 GarbageiOS.M_raribel11::_qavisouRawowsea
	int32_t ____qavisouRawowsea_31;
	// System.Int32 GarbageiOS.M_raribel11::_whervicur
	int32_t ____whervicur_32;
	// System.Int32 GarbageiOS.M_raribel11::_tisxe
	int32_t ____tisxe_33;
	// System.Single GarbageiOS.M_raribel11::_gallhe
	float ____gallhe_34;
	// System.Single GarbageiOS.M_raribel11::_gigalJisme
	float ____gigalJisme_35;
	// System.Single GarbageiOS.M_raribel11::_chacairsto
	float ____chacairsto_36;
	// System.Boolean GarbageiOS.M_raribel11::_drouboo
	bool ____drouboo_37;
	// System.Boolean GarbageiOS.M_raribel11::_murju
	bool ____murju_38;
	// System.Boolean GarbageiOS.M_raribel11::_biseSadouwhow
	bool ____biseSadouwhow_39;

public:
	inline static int32_t get_offset_of__masea_0() { return static_cast<int32_t>(offsetof(M_raribel11_t58371779, ____masea_0)); }
	inline int32_t get__masea_0() const { return ____masea_0; }
	inline int32_t* get_address_of__masea_0() { return &____masea_0; }
	inline void set__masea_0(int32_t value)
	{
		____masea_0 = value;
	}

	inline static int32_t get_offset_of__hastearkal_1() { return static_cast<int32_t>(offsetof(M_raribel11_t58371779, ____hastearkal_1)); }
	inline int32_t get__hastearkal_1() const { return ____hastearkal_1; }
	inline int32_t* get_address_of__hastearkal_1() { return &____hastearkal_1; }
	inline void set__hastearkal_1(int32_t value)
	{
		____hastearkal_1 = value;
	}

	inline static int32_t get_offset_of__refurQallwhougaw_2() { return static_cast<int32_t>(offsetof(M_raribel11_t58371779, ____refurQallwhougaw_2)); }
	inline int32_t get__refurQallwhougaw_2() const { return ____refurQallwhougaw_2; }
	inline int32_t* get_address_of__refurQallwhougaw_2() { return &____refurQallwhougaw_2; }
	inline void set__refurQallwhougaw_2(int32_t value)
	{
		____refurQallwhougaw_2 = value;
	}

	inline static int32_t get_offset_of__hadeHaypelcea_3() { return static_cast<int32_t>(offsetof(M_raribel11_t58371779, ____hadeHaypelcea_3)); }
	inline float get__hadeHaypelcea_3() const { return ____hadeHaypelcea_3; }
	inline float* get_address_of__hadeHaypelcea_3() { return &____hadeHaypelcea_3; }
	inline void set__hadeHaypelcea_3(float value)
	{
		____hadeHaypelcea_3 = value;
	}

	inline static int32_t get_offset_of__toheelaw_4() { return static_cast<int32_t>(offsetof(M_raribel11_t58371779, ____toheelaw_4)); }
	inline String_t* get__toheelaw_4() const { return ____toheelaw_4; }
	inline String_t** get_address_of__toheelaw_4() { return &____toheelaw_4; }
	inline void set__toheelaw_4(String_t* value)
	{
		____toheelaw_4 = value;
		Il2CppCodeGenWriteBarrier(&____toheelaw_4, value);
	}

	inline static int32_t get_offset_of__kasbemear_5() { return static_cast<int32_t>(offsetof(M_raribel11_t58371779, ____kasbemear_5)); }
	inline int32_t get__kasbemear_5() const { return ____kasbemear_5; }
	inline int32_t* get_address_of__kasbemear_5() { return &____kasbemear_5; }
	inline void set__kasbemear_5(int32_t value)
	{
		____kasbemear_5 = value;
	}

	inline static int32_t get_offset_of__dornoSaybemdou_6() { return static_cast<int32_t>(offsetof(M_raribel11_t58371779, ____dornoSaybemdou_6)); }
	inline float get__dornoSaybemdou_6() const { return ____dornoSaybemdou_6; }
	inline float* get_address_of__dornoSaybemdou_6() { return &____dornoSaybemdou_6; }
	inline void set__dornoSaybemdou_6(float value)
	{
		____dornoSaybemdou_6 = value;
	}

	inline static int32_t get_offset_of__jekeadeJaraw_7() { return static_cast<int32_t>(offsetof(M_raribel11_t58371779, ____jekeadeJaraw_7)); }
	inline float get__jekeadeJaraw_7() const { return ____jekeadeJaraw_7; }
	inline float* get_address_of__jekeadeJaraw_7() { return &____jekeadeJaraw_7; }
	inline void set__jekeadeJaraw_7(float value)
	{
		____jekeadeJaraw_7 = value;
	}

	inline static int32_t get_offset_of__kalsabo_8() { return static_cast<int32_t>(offsetof(M_raribel11_t58371779, ____kalsabo_8)); }
	inline uint32_t get__kalsabo_8() const { return ____kalsabo_8; }
	inline uint32_t* get_address_of__kalsabo_8() { return &____kalsabo_8; }
	inline void set__kalsabo_8(uint32_t value)
	{
		____kalsabo_8 = value;
	}

	inline static int32_t get_offset_of__neeyamai_9() { return static_cast<int32_t>(offsetof(M_raribel11_t58371779, ____neeyamai_9)); }
	inline String_t* get__neeyamai_9() const { return ____neeyamai_9; }
	inline String_t** get_address_of__neeyamai_9() { return &____neeyamai_9; }
	inline void set__neeyamai_9(String_t* value)
	{
		____neeyamai_9 = value;
		Il2CppCodeGenWriteBarrier(&____neeyamai_9, value);
	}

	inline static int32_t get_offset_of__tojisjouMosaper_10() { return static_cast<int32_t>(offsetof(M_raribel11_t58371779, ____tojisjouMosaper_10)); }
	inline int32_t get__tojisjouMosaper_10() const { return ____tojisjouMosaper_10; }
	inline int32_t* get_address_of__tojisjouMosaper_10() { return &____tojisjouMosaper_10; }
	inline void set__tojisjouMosaper_10(int32_t value)
	{
		____tojisjouMosaper_10 = value;
	}

	inline static int32_t get_offset_of__deebetou_11() { return static_cast<int32_t>(offsetof(M_raribel11_t58371779, ____deebetou_11)); }
	inline String_t* get__deebetou_11() const { return ____deebetou_11; }
	inline String_t** get_address_of__deebetou_11() { return &____deebetou_11; }
	inline void set__deebetou_11(String_t* value)
	{
		____deebetou_11 = value;
		Il2CppCodeGenWriteBarrier(&____deebetou_11, value);
	}

	inline static int32_t get_offset_of__sowmalWhisgo_12() { return static_cast<int32_t>(offsetof(M_raribel11_t58371779, ____sowmalWhisgo_12)); }
	inline int32_t get__sowmalWhisgo_12() const { return ____sowmalWhisgo_12; }
	inline int32_t* get_address_of__sowmalWhisgo_12() { return &____sowmalWhisgo_12; }
	inline void set__sowmalWhisgo_12(int32_t value)
	{
		____sowmalWhisgo_12 = value;
	}

	inline static int32_t get_offset_of__temor_13() { return static_cast<int32_t>(offsetof(M_raribel11_t58371779, ____temor_13)); }
	inline float get__temor_13() const { return ____temor_13; }
	inline float* get_address_of__temor_13() { return &____temor_13; }
	inline void set__temor_13(float value)
	{
		____temor_13 = value;
	}

	inline static int32_t get_offset_of__wearpurCeegasror_14() { return static_cast<int32_t>(offsetof(M_raribel11_t58371779, ____wearpurCeegasror_14)); }
	inline float get__wearpurCeegasror_14() const { return ____wearpurCeegasror_14; }
	inline float* get_address_of__wearpurCeegasror_14() { return &____wearpurCeegasror_14; }
	inline void set__wearpurCeegasror_14(float value)
	{
		____wearpurCeegasror_14 = value;
	}

	inline static int32_t get_offset_of__chasdruMairga_15() { return static_cast<int32_t>(offsetof(M_raribel11_t58371779, ____chasdruMairga_15)); }
	inline uint32_t get__chasdruMairga_15() const { return ____chasdruMairga_15; }
	inline uint32_t* get_address_of__chasdruMairga_15() { return &____chasdruMairga_15; }
	inline void set__chasdruMairga_15(uint32_t value)
	{
		____chasdruMairga_15 = value;
	}

	inline static int32_t get_offset_of__gatralbiRersas_16() { return static_cast<int32_t>(offsetof(M_raribel11_t58371779, ____gatralbiRersas_16)); }
	inline bool get__gatralbiRersas_16() const { return ____gatralbiRersas_16; }
	inline bool* get_address_of__gatralbiRersas_16() { return &____gatralbiRersas_16; }
	inline void set__gatralbiRersas_16(bool value)
	{
		____gatralbiRersas_16 = value;
	}

	inline static int32_t get_offset_of__girha_17() { return static_cast<int32_t>(offsetof(M_raribel11_t58371779, ____girha_17)); }
	inline String_t* get__girha_17() const { return ____girha_17; }
	inline String_t** get_address_of__girha_17() { return &____girha_17; }
	inline void set__girha_17(String_t* value)
	{
		____girha_17 = value;
		Il2CppCodeGenWriteBarrier(&____girha_17, value);
	}

	inline static int32_t get_offset_of__dekena_18() { return static_cast<int32_t>(offsetof(M_raribel11_t58371779, ____dekena_18)); }
	inline bool get__dekena_18() const { return ____dekena_18; }
	inline bool* get_address_of__dekena_18() { return &____dekena_18; }
	inline void set__dekena_18(bool value)
	{
		____dekena_18 = value;
	}

	inline static int32_t get_offset_of__lirceava_19() { return static_cast<int32_t>(offsetof(M_raribel11_t58371779, ____lirceava_19)); }
	inline uint32_t get__lirceava_19() const { return ____lirceava_19; }
	inline uint32_t* get_address_of__lirceava_19() { return &____lirceava_19; }
	inline void set__lirceava_19(uint32_t value)
	{
		____lirceava_19 = value;
	}

	inline static int32_t get_offset_of__yalljeljall_20() { return static_cast<int32_t>(offsetof(M_raribel11_t58371779, ____yalljeljall_20)); }
	inline bool get__yalljeljall_20() const { return ____yalljeljall_20; }
	inline bool* get_address_of__yalljeljall_20() { return &____yalljeljall_20; }
	inline void set__yalljeljall_20(bool value)
	{
		____yalljeljall_20 = value;
	}

	inline static int32_t get_offset_of__lelgelzel_21() { return static_cast<int32_t>(offsetof(M_raribel11_t58371779, ____lelgelzel_21)); }
	inline String_t* get__lelgelzel_21() const { return ____lelgelzel_21; }
	inline String_t** get_address_of__lelgelzel_21() { return &____lelgelzel_21; }
	inline void set__lelgelzel_21(String_t* value)
	{
		____lelgelzel_21 = value;
		Il2CppCodeGenWriteBarrier(&____lelgelzel_21, value);
	}

	inline static int32_t get_offset_of__cetasfor_22() { return static_cast<int32_t>(offsetof(M_raribel11_t58371779, ____cetasfor_22)); }
	inline String_t* get__cetasfor_22() const { return ____cetasfor_22; }
	inline String_t** get_address_of__cetasfor_22() { return &____cetasfor_22; }
	inline void set__cetasfor_22(String_t* value)
	{
		____cetasfor_22 = value;
		Il2CppCodeGenWriteBarrier(&____cetasfor_22, value);
	}

	inline static int32_t get_offset_of__loolouburStadiba_23() { return static_cast<int32_t>(offsetof(M_raribel11_t58371779, ____loolouburStadiba_23)); }
	inline bool get__loolouburStadiba_23() const { return ____loolouburStadiba_23; }
	inline bool* get_address_of__loolouburStadiba_23() { return &____loolouburStadiba_23; }
	inline void set__loolouburStadiba_23(bool value)
	{
		____loolouburStadiba_23 = value;
	}

	inline static int32_t get_offset_of__perepowhawKarcir_24() { return static_cast<int32_t>(offsetof(M_raribel11_t58371779, ____perepowhawKarcir_24)); }
	inline float get__perepowhawKarcir_24() const { return ____perepowhawKarcir_24; }
	inline float* get_address_of__perepowhawKarcir_24() { return &____perepowhawKarcir_24; }
	inline void set__perepowhawKarcir_24(float value)
	{
		____perepowhawKarcir_24 = value;
	}

	inline static int32_t get_offset_of__disjaimere_25() { return static_cast<int32_t>(offsetof(M_raribel11_t58371779, ____disjaimere_25)); }
	inline float get__disjaimere_25() const { return ____disjaimere_25; }
	inline float* get_address_of__disjaimere_25() { return &____disjaimere_25; }
	inline void set__disjaimere_25(float value)
	{
		____disjaimere_25 = value;
	}

	inline static int32_t get_offset_of__keargepeFisjall_26() { return static_cast<int32_t>(offsetof(M_raribel11_t58371779, ____keargepeFisjall_26)); }
	inline bool get__keargepeFisjall_26() const { return ____keargepeFisjall_26; }
	inline bool* get_address_of__keargepeFisjall_26() { return &____keargepeFisjall_26; }
	inline void set__keargepeFisjall_26(bool value)
	{
		____keargepeFisjall_26 = value;
	}

	inline static int32_t get_offset_of__wheadrarPowrem_27() { return static_cast<int32_t>(offsetof(M_raribel11_t58371779, ____wheadrarPowrem_27)); }
	inline uint32_t get__wheadrarPowrem_27() const { return ____wheadrarPowrem_27; }
	inline uint32_t* get_address_of__wheadrarPowrem_27() { return &____wheadrarPowrem_27; }
	inline void set__wheadrarPowrem_27(uint32_t value)
	{
		____wheadrarPowrem_27 = value;
	}

	inline static int32_t get_offset_of__cousair_28() { return static_cast<int32_t>(offsetof(M_raribel11_t58371779, ____cousair_28)); }
	inline int32_t get__cousair_28() const { return ____cousair_28; }
	inline int32_t* get_address_of__cousair_28() { return &____cousair_28; }
	inline void set__cousair_28(int32_t value)
	{
		____cousair_28 = value;
	}

	inline static int32_t get_offset_of__tereraJircar_29() { return static_cast<int32_t>(offsetof(M_raribel11_t58371779, ____tereraJircar_29)); }
	inline String_t* get__tereraJircar_29() const { return ____tereraJircar_29; }
	inline String_t** get_address_of__tereraJircar_29() { return &____tereraJircar_29; }
	inline void set__tereraJircar_29(String_t* value)
	{
		____tereraJircar_29 = value;
		Il2CppCodeGenWriteBarrier(&____tereraJircar_29, value);
	}

	inline static int32_t get_offset_of__zele_30() { return static_cast<int32_t>(offsetof(M_raribel11_t58371779, ____zele_30)); }
	inline bool get__zele_30() const { return ____zele_30; }
	inline bool* get_address_of__zele_30() { return &____zele_30; }
	inline void set__zele_30(bool value)
	{
		____zele_30 = value;
	}

	inline static int32_t get_offset_of__qavisouRawowsea_31() { return static_cast<int32_t>(offsetof(M_raribel11_t58371779, ____qavisouRawowsea_31)); }
	inline int32_t get__qavisouRawowsea_31() const { return ____qavisouRawowsea_31; }
	inline int32_t* get_address_of__qavisouRawowsea_31() { return &____qavisouRawowsea_31; }
	inline void set__qavisouRawowsea_31(int32_t value)
	{
		____qavisouRawowsea_31 = value;
	}

	inline static int32_t get_offset_of__whervicur_32() { return static_cast<int32_t>(offsetof(M_raribel11_t58371779, ____whervicur_32)); }
	inline int32_t get__whervicur_32() const { return ____whervicur_32; }
	inline int32_t* get_address_of__whervicur_32() { return &____whervicur_32; }
	inline void set__whervicur_32(int32_t value)
	{
		____whervicur_32 = value;
	}

	inline static int32_t get_offset_of__tisxe_33() { return static_cast<int32_t>(offsetof(M_raribel11_t58371779, ____tisxe_33)); }
	inline int32_t get__tisxe_33() const { return ____tisxe_33; }
	inline int32_t* get_address_of__tisxe_33() { return &____tisxe_33; }
	inline void set__tisxe_33(int32_t value)
	{
		____tisxe_33 = value;
	}

	inline static int32_t get_offset_of__gallhe_34() { return static_cast<int32_t>(offsetof(M_raribel11_t58371779, ____gallhe_34)); }
	inline float get__gallhe_34() const { return ____gallhe_34; }
	inline float* get_address_of__gallhe_34() { return &____gallhe_34; }
	inline void set__gallhe_34(float value)
	{
		____gallhe_34 = value;
	}

	inline static int32_t get_offset_of__gigalJisme_35() { return static_cast<int32_t>(offsetof(M_raribel11_t58371779, ____gigalJisme_35)); }
	inline float get__gigalJisme_35() const { return ____gigalJisme_35; }
	inline float* get_address_of__gigalJisme_35() { return &____gigalJisme_35; }
	inline void set__gigalJisme_35(float value)
	{
		____gigalJisme_35 = value;
	}

	inline static int32_t get_offset_of__chacairsto_36() { return static_cast<int32_t>(offsetof(M_raribel11_t58371779, ____chacairsto_36)); }
	inline float get__chacairsto_36() const { return ____chacairsto_36; }
	inline float* get_address_of__chacairsto_36() { return &____chacairsto_36; }
	inline void set__chacairsto_36(float value)
	{
		____chacairsto_36 = value;
	}

	inline static int32_t get_offset_of__drouboo_37() { return static_cast<int32_t>(offsetof(M_raribel11_t58371779, ____drouboo_37)); }
	inline bool get__drouboo_37() const { return ____drouboo_37; }
	inline bool* get_address_of__drouboo_37() { return &____drouboo_37; }
	inline void set__drouboo_37(bool value)
	{
		____drouboo_37 = value;
	}

	inline static int32_t get_offset_of__murju_38() { return static_cast<int32_t>(offsetof(M_raribel11_t58371779, ____murju_38)); }
	inline bool get__murju_38() const { return ____murju_38; }
	inline bool* get_address_of__murju_38() { return &____murju_38; }
	inline void set__murju_38(bool value)
	{
		____murju_38 = value;
	}

	inline static int32_t get_offset_of__biseSadouwhow_39() { return static_cast<int32_t>(offsetof(M_raribel11_t58371779, ____biseSadouwhow_39)); }
	inline bool get__biseSadouwhow_39() const { return ____biseSadouwhow_39; }
	inline bool* get_address_of__biseSadouwhow_39() { return &____biseSadouwhow_39; }
	inline void set__biseSadouwhow_39(bool value)
	{
		____biseSadouwhow_39 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
