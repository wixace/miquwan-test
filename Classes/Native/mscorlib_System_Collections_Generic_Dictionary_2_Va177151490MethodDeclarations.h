﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Int32>
struct Dictionary_2_t2245318082;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Va177151490.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Pathfinding.Int2,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m3389197215_gshared (Enumerator_t177151490 * __this, Dictionary_2_t2245318082 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m3389197215(__this, ___host0, method) ((  void (*) (Enumerator_t177151490 *, Dictionary_2_t2245318082 *, const MethodInfo*))Enumerator__ctor_m3389197215_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Pathfinding.Int2,System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m4289497122_gshared (Enumerator_t177151490 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m4289497122(__this, method) ((  Il2CppObject * (*) (Enumerator_t177151490 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m4289497122_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Pathfinding.Int2,System.Int32>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1594073014_gshared (Enumerator_t177151490 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1594073014(__this, method) ((  void (*) (Enumerator_t177151490 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1594073014_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Pathfinding.Int2,System.Int32>::Dispose()
extern "C"  void Enumerator_Dispose_m794101761_gshared (Enumerator_t177151490 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m794101761(__this, method) ((  void (*) (Enumerator_t177151490 *, const MethodInfo*))Enumerator_Dispose_m794101761_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Pathfinding.Int2,System.Int32>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2189901346_gshared (Enumerator_t177151490 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m2189901346(__this, method) ((  bool (*) (Enumerator_t177151490 *, const MethodInfo*))Enumerator_MoveNext_m2189901346_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Pathfinding.Int2,System.Int32>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m1338603040_gshared (Enumerator_t177151490 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m1338603040(__this, method) ((  int32_t (*) (Enumerator_t177151490 *, const MethodInfo*))Enumerator_get_Current_m1338603040_gshared)(__this, method)
