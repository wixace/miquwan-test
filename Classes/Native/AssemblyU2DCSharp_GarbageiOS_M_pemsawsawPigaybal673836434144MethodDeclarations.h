﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_pemsawsawPigaybal67
struct M_pemsawsawPigaybal67_t3836434144;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"

// System.Void GarbageiOS.M_pemsawsawPigaybal67::.ctor()
extern "C"  void M_pemsawsawPigaybal67__ctor_m1757890691 (M_pemsawsawPigaybal67_t3836434144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_pemsawsawPigaybal67::M_kugair0(System.String[],System.Int32)
extern "C"  void M_pemsawsawPigaybal67_M_kugair0_m274351529 (M_pemsawsawPigaybal67_t3836434144 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
