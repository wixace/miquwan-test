﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// MethodID
struct MethodID_t3916401116;
// JSDataExchangeMgr/DGetV`1<System.Object[]>
struct DGetV_1_t986307823;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EArrayGenerated
struct  EArrayGenerated_t2664647387  : public Il2CppObject
{
public:

public:
};

struct EArrayGenerated_t2664647387_StaticFields
{
public:
	// MethodID EArrayGenerated::methodID1
	MethodID_t3916401116 * ___methodID1_0;
	// MethodID EArrayGenerated::methodID2
	MethodID_t3916401116 * ___methodID2_1;
	// MethodID EArrayGenerated::methodID5
	MethodID_t3916401116 * ___methodID5_2;
	// MethodID EArrayGenerated::methodID7
	MethodID_t3916401116 * ___methodID7_3;
	// MethodID EArrayGenerated::methodID8
	MethodID_t3916401116 * ___methodID8_4;
	// MethodID EArrayGenerated::methodID11
	MethodID_t3916401116 * ___methodID11_5;
	// JSDataExchangeMgr/DGetV`1<System.Object[]> EArrayGenerated::<>f__am$cache6
	DGetV_1_t986307823 * ___U3CU3Ef__amU24cache6_6;
	// JSDataExchangeMgr/DGetV`1<System.Object[]> EArrayGenerated::<>f__am$cache7
	DGetV_1_t986307823 * ___U3CU3Ef__amU24cache7_7;
	// JSDataExchangeMgr/DGetV`1<System.Object[]> EArrayGenerated::<>f__am$cache8
	DGetV_1_t986307823 * ___U3CU3Ef__amU24cache8_8;
	// JSDataExchangeMgr/DGetV`1<System.Object[]> EArrayGenerated::<>f__am$cache9
	DGetV_1_t986307823 * ___U3CU3Ef__amU24cache9_9;
	// JSDataExchangeMgr/DGetV`1<System.Object[]> EArrayGenerated::<>f__am$cacheA
	DGetV_1_t986307823 * ___U3CU3Ef__amU24cacheA_10;
	// JSDataExchangeMgr/DGetV`1<System.Object[]> EArrayGenerated::<>f__am$cacheB
	DGetV_1_t986307823 * ___U3CU3Ef__amU24cacheB_11;

public:
	inline static int32_t get_offset_of_methodID1_0() { return static_cast<int32_t>(offsetof(EArrayGenerated_t2664647387_StaticFields, ___methodID1_0)); }
	inline MethodID_t3916401116 * get_methodID1_0() const { return ___methodID1_0; }
	inline MethodID_t3916401116 ** get_address_of_methodID1_0() { return &___methodID1_0; }
	inline void set_methodID1_0(MethodID_t3916401116 * value)
	{
		___methodID1_0 = value;
		Il2CppCodeGenWriteBarrier(&___methodID1_0, value);
	}

	inline static int32_t get_offset_of_methodID2_1() { return static_cast<int32_t>(offsetof(EArrayGenerated_t2664647387_StaticFields, ___methodID2_1)); }
	inline MethodID_t3916401116 * get_methodID2_1() const { return ___methodID2_1; }
	inline MethodID_t3916401116 ** get_address_of_methodID2_1() { return &___methodID2_1; }
	inline void set_methodID2_1(MethodID_t3916401116 * value)
	{
		___methodID2_1 = value;
		Il2CppCodeGenWriteBarrier(&___methodID2_1, value);
	}

	inline static int32_t get_offset_of_methodID5_2() { return static_cast<int32_t>(offsetof(EArrayGenerated_t2664647387_StaticFields, ___methodID5_2)); }
	inline MethodID_t3916401116 * get_methodID5_2() const { return ___methodID5_2; }
	inline MethodID_t3916401116 ** get_address_of_methodID5_2() { return &___methodID5_2; }
	inline void set_methodID5_2(MethodID_t3916401116 * value)
	{
		___methodID5_2 = value;
		Il2CppCodeGenWriteBarrier(&___methodID5_2, value);
	}

	inline static int32_t get_offset_of_methodID7_3() { return static_cast<int32_t>(offsetof(EArrayGenerated_t2664647387_StaticFields, ___methodID7_3)); }
	inline MethodID_t3916401116 * get_methodID7_3() const { return ___methodID7_3; }
	inline MethodID_t3916401116 ** get_address_of_methodID7_3() { return &___methodID7_3; }
	inline void set_methodID7_3(MethodID_t3916401116 * value)
	{
		___methodID7_3 = value;
		Il2CppCodeGenWriteBarrier(&___methodID7_3, value);
	}

	inline static int32_t get_offset_of_methodID8_4() { return static_cast<int32_t>(offsetof(EArrayGenerated_t2664647387_StaticFields, ___methodID8_4)); }
	inline MethodID_t3916401116 * get_methodID8_4() const { return ___methodID8_4; }
	inline MethodID_t3916401116 ** get_address_of_methodID8_4() { return &___methodID8_4; }
	inline void set_methodID8_4(MethodID_t3916401116 * value)
	{
		___methodID8_4 = value;
		Il2CppCodeGenWriteBarrier(&___methodID8_4, value);
	}

	inline static int32_t get_offset_of_methodID11_5() { return static_cast<int32_t>(offsetof(EArrayGenerated_t2664647387_StaticFields, ___methodID11_5)); }
	inline MethodID_t3916401116 * get_methodID11_5() const { return ___methodID11_5; }
	inline MethodID_t3916401116 ** get_address_of_methodID11_5() { return &___methodID11_5; }
	inline void set_methodID11_5(MethodID_t3916401116 * value)
	{
		___methodID11_5 = value;
		Il2CppCodeGenWriteBarrier(&___methodID11_5, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache6_6() { return static_cast<int32_t>(offsetof(EArrayGenerated_t2664647387_StaticFields, ___U3CU3Ef__amU24cache6_6)); }
	inline DGetV_1_t986307823 * get_U3CU3Ef__amU24cache6_6() const { return ___U3CU3Ef__amU24cache6_6; }
	inline DGetV_1_t986307823 ** get_address_of_U3CU3Ef__amU24cache6_6() { return &___U3CU3Ef__amU24cache6_6; }
	inline void set_U3CU3Ef__amU24cache6_6(DGetV_1_t986307823 * value)
	{
		___U3CU3Ef__amU24cache6_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache6_6, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache7_7() { return static_cast<int32_t>(offsetof(EArrayGenerated_t2664647387_StaticFields, ___U3CU3Ef__amU24cache7_7)); }
	inline DGetV_1_t986307823 * get_U3CU3Ef__amU24cache7_7() const { return ___U3CU3Ef__amU24cache7_7; }
	inline DGetV_1_t986307823 ** get_address_of_U3CU3Ef__amU24cache7_7() { return &___U3CU3Ef__amU24cache7_7; }
	inline void set_U3CU3Ef__amU24cache7_7(DGetV_1_t986307823 * value)
	{
		___U3CU3Ef__amU24cache7_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache7_7, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache8_8() { return static_cast<int32_t>(offsetof(EArrayGenerated_t2664647387_StaticFields, ___U3CU3Ef__amU24cache8_8)); }
	inline DGetV_1_t986307823 * get_U3CU3Ef__amU24cache8_8() const { return ___U3CU3Ef__amU24cache8_8; }
	inline DGetV_1_t986307823 ** get_address_of_U3CU3Ef__amU24cache8_8() { return &___U3CU3Ef__amU24cache8_8; }
	inline void set_U3CU3Ef__amU24cache8_8(DGetV_1_t986307823 * value)
	{
		___U3CU3Ef__amU24cache8_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache8_8, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache9_9() { return static_cast<int32_t>(offsetof(EArrayGenerated_t2664647387_StaticFields, ___U3CU3Ef__amU24cache9_9)); }
	inline DGetV_1_t986307823 * get_U3CU3Ef__amU24cache9_9() const { return ___U3CU3Ef__amU24cache9_9; }
	inline DGetV_1_t986307823 ** get_address_of_U3CU3Ef__amU24cache9_9() { return &___U3CU3Ef__amU24cache9_9; }
	inline void set_U3CU3Ef__amU24cache9_9(DGetV_1_t986307823 * value)
	{
		___U3CU3Ef__amU24cache9_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache9_9, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheA_10() { return static_cast<int32_t>(offsetof(EArrayGenerated_t2664647387_StaticFields, ___U3CU3Ef__amU24cacheA_10)); }
	inline DGetV_1_t986307823 * get_U3CU3Ef__amU24cacheA_10() const { return ___U3CU3Ef__amU24cacheA_10; }
	inline DGetV_1_t986307823 ** get_address_of_U3CU3Ef__amU24cacheA_10() { return &___U3CU3Ef__amU24cacheA_10; }
	inline void set_U3CU3Ef__amU24cacheA_10(DGetV_1_t986307823 * value)
	{
		___U3CU3Ef__amU24cacheA_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheA_10, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheB_11() { return static_cast<int32_t>(offsetof(EArrayGenerated_t2664647387_StaticFields, ___U3CU3Ef__amU24cacheB_11)); }
	inline DGetV_1_t986307823 * get_U3CU3Ef__amU24cacheB_11() const { return ___U3CU3Ef__amU24cacheB_11; }
	inline DGetV_1_t986307823 ** get_address_of_U3CU3Ef__amU24cacheB_11() { return &___U3CU3Ef__amU24cacheB_11; }
	inline void set_U3CU3Ef__amU24cacheB_11(DGetV_1_t986307823 * value)
	{
		___U3CU3Ef__amU24cacheB_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheB_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
