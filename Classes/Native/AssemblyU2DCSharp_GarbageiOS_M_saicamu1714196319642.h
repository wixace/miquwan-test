﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_saicamu171
struct  M_saicamu171_t4196319642  : public Il2CppObject
{
public:
	// System.Single GarbageiOS.M_saicamu171::_sobo
	float ____sobo_0;
	// System.Boolean GarbageiOS.M_saicamu171::_ceresur
	bool ____ceresur_1;
	// System.Boolean GarbageiOS.M_saicamu171::_hadajereSefem
	bool ____hadajereSefem_2;
	// System.Boolean GarbageiOS.M_saicamu171::_tahairnearJerpel
	bool ____tahairnearJerpel_3;
	// System.Single GarbageiOS.M_saicamu171::_werkojee
	float ____werkojee_4;
	// System.Int32 GarbageiOS.M_saicamu171::_jawbear
	int32_t ____jawbear_5;
	// System.String GarbageiOS.M_saicamu171::_cairsukisSasgiyow
	String_t* ____cairsukisSasgiyow_6;
	// System.Int32 GarbageiOS.M_saicamu171::_hislu
	int32_t ____hislu_7;
	// System.Boolean GarbageiOS.M_saicamu171::_heremefa
	bool ____heremefa_8;
	// System.String GarbageiOS.M_saicamu171::_callnemCeachapaw
	String_t* ____callnemCeachapaw_9;
	// System.Int32 GarbageiOS.M_saicamu171::_weadrertruBajel
	int32_t ____weadrertruBajel_10;
	// System.Int32 GarbageiOS.M_saicamu171::_tallwaymou
	int32_t ____tallwaymou_11;
	// System.String GarbageiOS.M_saicamu171::_kotooLairceade
	String_t* ____kotooLairceade_12;
	// System.Int32 GarbageiOS.M_saicamu171::_learvelem
	int32_t ____learvelem_13;
	// System.Single GarbageiOS.M_saicamu171::_secai
	float ____secai_14;
	// System.String GarbageiOS.M_saicamu171::_tokatawNatelfal
	String_t* ____tokatawNatelfal_15;
	// System.UInt32 GarbageiOS.M_saicamu171::_laihearsaiReresere
	uint32_t ____laihearsaiReresere_16;
	// System.Int32 GarbageiOS.M_saicamu171::_getrouQahurtor
	int32_t ____getrouQahurtor_17;
	// System.String GarbageiOS.M_saicamu171::_riwel
	String_t* ____riwel_18;
	// System.String GarbageiOS.M_saicamu171::_sijeaDerralwee
	String_t* ____sijeaDerralwee_19;
	// System.Boolean GarbageiOS.M_saicamu171::_qemowsaLerva
	bool ____qemowsaLerva_20;
	// System.Single GarbageiOS.M_saicamu171::_bearnadeFounallsow
	float ____bearnadeFounallsow_21;
	// System.Boolean GarbageiOS.M_saicamu171::_jarino
	bool ____jarino_22;
	// System.Boolean GarbageiOS.M_saicamu171::_seekoqir
	bool ____seekoqir_23;
	// System.UInt32 GarbageiOS.M_saicamu171::_hallyou
	uint32_t ____hallyou_24;
	// System.UInt32 GarbageiOS.M_saicamu171::_hascar
	uint32_t ____hascar_25;
	// System.String GarbageiOS.M_saicamu171::_kerday
	String_t* ____kerday_26;
	// System.Boolean GarbageiOS.M_saicamu171::_cayfearboGeazoobem
	bool ____cayfearboGeazoobem_27;
	// System.UInt32 GarbageiOS.M_saicamu171::_sounarHasi
	uint32_t ____sounarHasi_28;
	// System.String GarbageiOS.M_saicamu171::_bouwhili
	String_t* ____bouwhili_29;
	// System.String GarbageiOS.M_saicamu171::_harur
	String_t* ____harur_30;
	// System.Single GarbageiOS.M_saicamu171::_sowcabuCohow
	float ____sowcabuCohow_31;
	// System.String GarbageiOS.M_saicamu171::_dairlotee
	String_t* ____dairlotee_32;
	// System.UInt32 GarbageiOS.M_saicamu171::_corsursal
	uint32_t ____corsursal_33;
	// System.Boolean GarbageiOS.M_saicamu171::_xaiger
	bool ____xaiger_34;
	// System.String GarbageiOS.M_saicamu171::_rocamouPayme
	String_t* ____rocamouPayme_35;
	// System.Int32 GarbageiOS.M_saicamu171::_sawsem
	int32_t ____sawsem_36;
	// System.Boolean GarbageiOS.M_saicamu171::_tawrego
	bool ____tawrego_37;
	// System.Int32 GarbageiOS.M_saicamu171::_sarhasgall
	int32_t ____sarhasgall_38;
	// System.Int32 GarbageiOS.M_saicamu171::_xerevikaSapedral
	int32_t ____xerevikaSapedral_39;
	// System.UInt32 GarbageiOS.M_saicamu171::_sibisCeade
	uint32_t ____sibisCeade_40;
	// System.Single GarbageiOS.M_saicamu171::_huhowcee
	float ____huhowcee_41;
	// System.Boolean GarbageiOS.M_saicamu171::_kesay
	bool ____kesay_42;
	// System.UInt32 GarbageiOS.M_saicamu171::_caseGaceeris
	uint32_t ____caseGaceeris_43;
	// System.UInt32 GarbageiOS.M_saicamu171::_haiqireDurhasfi
	uint32_t ____haiqireDurhasfi_44;
	// System.UInt32 GarbageiOS.M_saicamu171::_hallziba
	uint32_t ____hallziba_45;

public:
	inline static int32_t get_offset_of__sobo_0() { return static_cast<int32_t>(offsetof(M_saicamu171_t4196319642, ____sobo_0)); }
	inline float get__sobo_0() const { return ____sobo_0; }
	inline float* get_address_of__sobo_0() { return &____sobo_0; }
	inline void set__sobo_0(float value)
	{
		____sobo_0 = value;
	}

	inline static int32_t get_offset_of__ceresur_1() { return static_cast<int32_t>(offsetof(M_saicamu171_t4196319642, ____ceresur_1)); }
	inline bool get__ceresur_1() const { return ____ceresur_1; }
	inline bool* get_address_of__ceresur_1() { return &____ceresur_1; }
	inline void set__ceresur_1(bool value)
	{
		____ceresur_1 = value;
	}

	inline static int32_t get_offset_of__hadajereSefem_2() { return static_cast<int32_t>(offsetof(M_saicamu171_t4196319642, ____hadajereSefem_2)); }
	inline bool get__hadajereSefem_2() const { return ____hadajereSefem_2; }
	inline bool* get_address_of__hadajereSefem_2() { return &____hadajereSefem_2; }
	inline void set__hadajereSefem_2(bool value)
	{
		____hadajereSefem_2 = value;
	}

	inline static int32_t get_offset_of__tahairnearJerpel_3() { return static_cast<int32_t>(offsetof(M_saicamu171_t4196319642, ____tahairnearJerpel_3)); }
	inline bool get__tahairnearJerpel_3() const { return ____tahairnearJerpel_3; }
	inline bool* get_address_of__tahairnearJerpel_3() { return &____tahairnearJerpel_3; }
	inline void set__tahairnearJerpel_3(bool value)
	{
		____tahairnearJerpel_3 = value;
	}

	inline static int32_t get_offset_of__werkojee_4() { return static_cast<int32_t>(offsetof(M_saicamu171_t4196319642, ____werkojee_4)); }
	inline float get__werkojee_4() const { return ____werkojee_4; }
	inline float* get_address_of__werkojee_4() { return &____werkojee_4; }
	inline void set__werkojee_4(float value)
	{
		____werkojee_4 = value;
	}

	inline static int32_t get_offset_of__jawbear_5() { return static_cast<int32_t>(offsetof(M_saicamu171_t4196319642, ____jawbear_5)); }
	inline int32_t get__jawbear_5() const { return ____jawbear_5; }
	inline int32_t* get_address_of__jawbear_5() { return &____jawbear_5; }
	inline void set__jawbear_5(int32_t value)
	{
		____jawbear_5 = value;
	}

	inline static int32_t get_offset_of__cairsukisSasgiyow_6() { return static_cast<int32_t>(offsetof(M_saicamu171_t4196319642, ____cairsukisSasgiyow_6)); }
	inline String_t* get__cairsukisSasgiyow_6() const { return ____cairsukisSasgiyow_6; }
	inline String_t** get_address_of__cairsukisSasgiyow_6() { return &____cairsukisSasgiyow_6; }
	inline void set__cairsukisSasgiyow_6(String_t* value)
	{
		____cairsukisSasgiyow_6 = value;
		Il2CppCodeGenWriteBarrier(&____cairsukisSasgiyow_6, value);
	}

	inline static int32_t get_offset_of__hislu_7() { return static_cast<int32_t>(offsetof(M_saicamu171_t4196319642, ____hislu_7)); }
	inline int32_t get__hislu_7() const { return ____hislu_7; }
	inline int32_t* get_address_of__hislu_7() { return &____hislu_7; }
	inline void set__hislu_7(int32_t value)
	{
		____hislu_7 = value;
	}

	inline static int32_t get_offset_of__heremefa_8() { return static_cast<int32_t>(offsetof(M_saicamu171_t4196319642, ____heremefa_8)); }
	inline bool get__heremefa_8() const { return ____heremefa_8; }
	inline bool* get_address_of__heremefa_8() { return &____heremefa_8; }
	inline void set__heremefa_8(bool value)
	{
		____heremefa_8 = value;
	}

	inline static int32_t get_offset_of__callnemCeachapaw_9() { return static_cast<int32_t>(offsetof(M_saicamu171_t4196319642, ____callnemCeachapaw_9)); }
	inline String_t* get__callnemCeachapaw_9() const { return ____callnemCeachapaw_9; }
	inline String_t** get_address_of__callnemCeachapaw_9() { return &____callnemCeachapaw_9; }
	inline void set__callnemCeachapaw_9(String_t* value)
	{
		____callnemCeachapaw_9 = value;
		Il2CppCodeGenWriteBarrier(&____callnemCeachapaw_9, value);
	}

	inline static int32_t get_offset_of__weadrertruBajel_10() { return static_cast<int32_t>(offsetof(M_saicamu171_t4196319642, ____weadrertruBajel_10)); }
	inline int32_t get__weadrertruBajel_10() const { return ____weadrertruBajel_10; }
	inline int32_t* get_address_of__weadrertruBajel_10() { return &____weadrertruBajel_10; }
	inline void set__weadrertruBajel_10(int32_t value)
	{
		____weadrertruBajel_10 = value;
	}

	inline static int32_t get_offset_of__tallwaymou_11() { return static_cast<int32_t>(offsetof(M_saicamu171_t4196319642, ____tallwaymou_11)); }
	inline int32_t get__tallwaymou_11() const { return ____tallwaymou_11; }
	inline int32_t* get_address_of__tallwaymou_11() { return &____tallwaymou_11; }
	inline void set__tallwaymou_11(int32_t value)
	{
		____tallwaymou_11 = value;
	}

	inline static int32_t get_offset_of__kotooLairceade_12() { return static_cast<int32_t>(offsetof(M_saicamu171_t4196319642, ____kotooLairceade_12)); }
	inline String_t* get__kotooLairceade_12() const { return ____kotooLairceade_12; }
	inline String_t** get_address_of__kotooLairceade_12() { return &____kotooLairceade_12; }
	inline void set__kotooLairceade_12(String_t* value)
	{
		____kotooLairceade_12 = value;
		Il2CppCodeGenWriteBarrier(&____kotooLairceade_12, value);
	}

	inline static int32_t get_offset_of__learvelem_13() { return static_cast<int32_t>(offsetof(M_saicamu171_t4196319642, ____learvelem_13)); }
	inline int32_t get__learvelem_13() const { return ____learvelem_13; }
	inline int32_t* get_address_of__learvelem_13() { return &____learvelem_13; }
	inline void set__learvelem_13(int32_t value)
	{
		____learvelem_13 = value;
	}

	inline static int32_t get_offset_of__secai_14() { return static_cast<int32_t>(offsetof(M_saicamu171_t4196319642, ____secai_14)); }
	inline float get__secai_14() const { return ____secai_14; }
	inline float* get_address_of__secai_14() { return &____secai_14; }
	inline void set__secai_14(float value)
	{
		____secai_14 = value;
	}

	inline static int32_t get_offset_of__tokatawNatelfal_15() { return static_cast<int32_t>(offsetof(M_saicamu171_t4196319642, ____tokatawNatelfal_15)); }
	inline String_t* get__tokatawNatelfal_15() const { return ____tokatawNatelfal_15; }
	inline String_t** get_address_of__tokatawNatelfal_15() { return &____tokatawNatelfal_15; }
	inline void set__tokatawNatelfal_15(String_t* value)
	{
		____tokatawNatelfal_15 = value;
		Il2CppCodeGenWriteBarrier(&____tokatawNatelfal_15, value);
	}

	inline static int32_t get_offset_of__laihearsaiReresere_16() { return static_cast<int32_t>(offsetof(M_saicamu171_t4196319642, ____laihearsaiReresere_16)); }
	inline uint32_t get__laihearsaiReresere_16() const { return ____laihearsaiReresere_16; }
	inline uint32_t* get_address_of__laihearsaiReresere_16() { return &____laihearsaiReresere_16; }
	inline void set__laihearsaiReresere_16(uint32_t value)
	{
		____laihearsaiReresere_16 = value;
	}

	inline static int32_t get_offset_of__getrouQahurtor_17() { return static_cast<int32_t>(offsetof(M_saicamu171_t4196319642, ____getrouQahurtor_17)); }
	inline int32_t get__getrouQahurtor_17() const { return ____getrouQahurtor_17; }
	inline int32_t* get_address_of__getrouQahurtor_17() { return &____getrouQahurtor_17; }
	inline void set__getrouQahurtor_17(int32_t value)
	{
		____getrouQahurtor_17 = value;
	}

	inline static int32_t get_offset_of__riwel_18() { return static_cast<int32_t>(offsetof(M_saicamu171_t4196319642, ____riwel_18)); }
	inline String_t* get__riwel_18() const { return ____riwel_18; }
	inline String_t** get_address_of__riwel_18() { return &____riwel_18; }
	inline void set__riwel_18(String_t* value)
	{
		____riwel_18 = value;
		Il2CppCodeGenWriteBarrier(&____riwel_18, value);
	}

	inline static int32_t get_offset_of__sijeaDerralwee_19() { return static_cast<int32_t>(offsetof(M_saicamu171_t4196319642, ____sijeaDerralwee_19)); }
	inline String_t* get__sijeaDerralwee_19() const { return ____sijeaDerralwee_19; }
	inline String_t** get_address_of__sijeaDerralwee_19() { return &____sijeaDerralwee_19; }
	inline void set__sijeaDerralwee_19(String_t* value)
	{
		____sijeaDerralwee_19 = value;
		Il2CppCodeGenWriteBarrier(&____sijeaDerralwee_19, value);
	}

	inline static int32_t get_offset_of__qemowsaLerva_20() { return static_cast<int32_t>(offsetof(M_saicamu171_t4196319642, ____qemowsaLerva_20)); }
	inline bool get__qemowsaLerva_20() const { return ____qemowsaLerva_20; }
	inline bool* get_address_of__qemowsaLerva_20() { return &____qemowsaLerva_20; }
	inline void set__qemowsaLerva_20(bool value)
	{
		____qemowsaLerva_20 = value;
	}

	inline static int32_t get_offset_of__bearnadeFounallsow_21() { return static_cast<int32_t>(offsetof(M_saicamu171_t4196319642, ____bearnadeFounallsow_21)); }
	inline float get__bearnadeFounallsow_21() const { return ____bearnadeFounallsow_21; }
	inline float* get_address_of__bearnadeFounallsow_21() { return &____bearnadeFounallsow_21; }
	inline void set__bearnadeFounallsow_21(float value)
	{
		____bearnadeFounallsow_21 = value;
	}

	inline static int32_t get_offset_of__jarino_22() { return static_cast<int32_t>(offsetof(M_saicamu171_t4196319642, ____jarino_22)); }
	inline bool get__jarino_22() const { return ____jarino_22; }
	inline bool* get_address_of__jarino_22() { return &____jarino_22; }
	inline void set__jarino_22(bool value)
	{
		____jarino_22 = value;
	}

	inline static int32_t get_offset_of__seekoqir_23() { return static_cast<int32_t>(offsetof(M_saicamu171_t4196319642, ____seekoqir_23)); }
	inline bool get__seekoqir_23() const { return ____seekoqir_23; }
	inline bool* get_address_of__seekoqir_23() { return &____seekoqir_23; }
	inline void set__seekoqir_23(bool value)
	{
		____seekoqir_23 = value;
	}

	inline static int32_t get_offset_of__hallyou_24() { return static_cast<int32_t>(offsetof(M_saicamu171_t4196319642, ____hallyou_24)); }
	inline uint32_t get__hallyou_24() const { return ____hallyou_24; }
	inline uint32_t* get_address_of__hallyou_24() { return &____hallyou_24; }
	inline void set__hallyou_24(uint32_t value)
	{
		____hallyou_24 = value;
	}

	inline static int32_t get_offset_of__hascar_25() { return static_cast<int32_t>(offsetof(M_saicamu171_t4196319642, ____hascar_25)); }
	inline uint32_t get__hascar_25() const { return ____hascar_25; }
	inline uint32_t* get_address_of__hascar_25() { return &____hascar_25; }
	inline void set__hascar_25(uint32_t value)
	{
		____hascar_25 = value;
	}

	inline static int32_t get_offset_of__kerday_26() { return static_cast<int32_t>(offsetof(M_saicamu171_t4196319642, ____kerday_26)); }
	inline String_t* get__kerday_26() const { return ____kerday_26; }
	inline String_t** get_address_of__kerday_26() { return &____kerday_26; }
	inline void set__kerday_26(String_t* value)
	{
		____kerday_26 = value;
		Il2CppCodeGenWriteBarrier(&____kerday_26, value);
	}

	inline static int32_t get_offset_of__cayfearboGeazoobem_27() { return static_cast<int32_t>(offsetof(M_saicamu171_t4196319642, ____cayfearboGeazoobem_27)); }
	inline bool get__cayfearboGeazoobem_27() const { return ____cayfearboGeazoobem_27; }
	inline bool* get_address_of__cayfearboGeazoobem_27() { return &____cayfearboGeazoobem_27; }
	inline void set__cayfearboGeazoobem_27(bool value)
	{
		____cayfearboGeazoobem_27 = value;
	}

	inline static int32_t get_offset_of__sounarHasi_28() { return static_cast<int32_t>(offsetof(M_saicamu171_t4196319642, ____sounarHasi_28)); }
	inline uint32_t get__sounarHasi_28() const { return ____sounarHasi_28; }
	inline uint32_t* get_address_of__sounarHasi_28() { return &____sounarHasi_28; }
	inline void set__sounarHasi_28(uint32_t value)
	{
		____sounarHasi_28 = value;
	}

	inline static int32_t get_offset_of__bouwhili_29() { return static_cast<int32_t>(offsetof(M_saicamu171_t4196319642, ____bouwhili_29)); }
	inline String_t* get__bouwhili_29() const { return ____bouwhili_29; }
	inline String_t** get_address_of__bouwhili_29() { return &____bouwhili_29; }
	inline void set__bouwhili_29(String_t* value)
	{
		____bouwhili_29 = value;
		Il2CppCodeGenWriteBarrier(&____bouwhili_29, value);
	}

	inline static int32_t get_offset_of__harur_30() { return static_cast<int32_t>(offsetof(M_saicamu171_t4196319642, ____harur_30)); }
	inline String_t* get__harur_30() const { return ____harur_30; }
	inline String_t** get_address_of__harur_30() { return &____harur_30; }
	inline void set__harur_30(String_t* value)
	{
		____harur_30 = value;
		Il2CppCodeGenWriteBarrier(&____harur_30, value);
	}

	inline static int32_t get_offset_of__sowcabuCohow_31() { return static_cast<int32_t>(offsetof(M_saicamu171_t4196319642, ____sowcabuCohow_31)); }
	inline float get__sowcabuCohow_31() const { return ____sowcabuCohow_31; }
	inline float* get_address_of__sowcabuCohow_31() { return &____sowcabuCohow_31; }
	inline void set__sowcabuCohow_31(float value)
	{
		____sowcabuCohow_31 = value;
	}

	inline static int32_t get_offset_of__dairlotee_32() { return static_cast<int32_t>(offsetof(M_saicamu171_t4196319642, ____dairlotee_32)); }
	inline String_t* get__dairlotee_32() const { return ____dairlotee_32; }
	inline String_t** get_address_of__dairlotee_32() { return &____dairlotee_32; }
	inline void set__dairlotee_32(String_t* value)
	{
		____dairlotee_32 = value;
		Il2CppCodeGenWriteBarrier(&____dairlotee_32, value);
	}

	inline static int32_t get_offset_of__corsursal_33() { return static_cast<int32_t>(offsetof(M_saicamu171_t4196319642, ____corsursal_33)); }
	inline uint32_t get__corsursal_33() const { return ____corsursal_33; }
	inline uint32_t* get_address_of__corsursal_33() { return &____corsursal_33; }
	inline void set__corsursal_33(uint32_t value)
	{
		____corsursal_33 = value;
	}

	inline static int32_t get_offset_of__xaiger_34() { return static_cast<int32_t>(offsetof(M_saicamu171_t4196319642, ____xaiger_34)); }
	inline bool get__xaiger_34() const { return ____xaiger_34; }
	inline bool* get_address_of__xaiger_34() { return &____xaiger_34; }
	inline void set__xaiger_34(bool value)
	{
		____xaiger_34 = value;
	}

	inline static int32_t get_offset_of__rocamouPayme_35() { return static_cast<int32_t>(offsetof(M_saicamu171_t4196319642, ____rocamouPayme_35)); }
	inline String_t* get__rocamouPayme_35() const { return ____rocamouPayme_35; }
	inline String_t** get_address_of__rocamouPayme_35() { return &____rocamouPayme_35; }
	inline void set__rocamouPayme_35(String_t* value)
	{
		____rocamouPayme_35 = value;
		Il2CppCodeGenWriteBarrier(&____rocamouPayme_35, value);
	}

	inline static int32_t get_offset_of__sawsem_36() { return static_cast<int32_t>(offsetof(M_saicamu171_t4196319642, ____sawsem_36)); }
	inline int32_t get__sawsem_36() const { return ____sawsem_36; }
	inline int32_t* get_address_of__sawsem_36() { return &____sawsem_36; }
	inline void set__sawsem_36(int32_t value)
	{
		____sawsem_36 = value;
	}

	inline static int32_t get_offset_of__tawrego_37() { return static_cast<int32_t>(offsetof(M_saicamu171_t4196319642, ____tawrego_37)); }
	inline bool get__tawrego_37() const { return ____tawrego_37; }
	inline bool* get_address_of__tawrego_37() { return &____tawrego_37; }
	inline void set__tawrego_37(bool value)
	{
		____tawrego_37 = value;
	}

	inline static int32_t get_offset_of__sarhasgall_38() { return static_cast<int32_t>(offsetof(M_saicamu171_t4196319642, ____sarhasgall_38)); }
	inline int32_t get__sarhasgall_38() const { return ____sarhasgall_38; }
	inline int32_t* get_address_of__sarhasgall_38() { return &____sarhasgall_38; }
	inline void set__sarhasgall_38(int32_t value)
	{
		____sarhasgall_38 = value;
	}

	inline static int32_t get_offset_of__xerevikaSapedral_39() { return static_cast<int32_t>(offsetof(M_saicamu171_t4196319642, ____xerevikaSapedral_39)); }
	inline int32_t get__xerevikaSapedral_39() const { return ____xerevikaSapedral_39; }
	inline int32_t* get_address_of__xerevikaSapedral_39() { return &____xerevikaSapedral_39; }
	inline void set__xerevikaSapedral_39(int32_t value)
	{
		____xerevikaSapedral_39 = value;
	}

	inline static int32_t get_offset_of__sibisCeade_40() { return static_cast<int32_t>(offsetof(M_saicamu171_t4196319642, ____sibisCeade_40)); }
	inline uint32_t get__sibisCeade_40() const { return ____sibisCeade_40; }
	inline uint32_t* get_address_of__sibisCeade_40() { return &____sibisCeade_40; }
	inline void set__sibisCeade_40(uint32_t value)
	{
		____sibisCeade_40 = value;
	}

	inline static int32_t get_offset_of__huhowcee_41() { return static_cast<int32_t>(offsetof(M_saicamu171_t4196319642, ____huhowcee_41)); }
	inline float get__huhowcee_41() const { return ____huhowcee_41; }
	inline float* get_address_of__huhowcee_41() { return &____huhowcee_41; }
	inline void set__huhowcee_41(float value)
	{
		____huhowcee_41 = value;
	}

	inline static int32_t get_offset_of__kesay_42() { return static_cast<int32_t>(offsetof(M_saicamu171_t4196319642, ____kesay_42)); }
	inline bool get__kesay_42() const { return ____kesay_42; }
	inline bool* get_address_of__kesay_42() { return &____kesay_42; }
	inline void set__kesay_42(bool value)
	{
		____kesay_42 = value;
	}

	inline static int32_t get_offset_of__caseGaceeris_43() { return static_cast<int32_t>(offsetof(M_saicamu171_t4196319642, ____caseGaceeris_43)); }
	inline uint32_t get__caseGaceeris_43() const { return ____caseGaceeris_43; }
	inline uint32_t* get_address_of__caseGaceeris_43() { return &____caseGaceeris_43; }
	inline void set__caseGaceeris_43(uint32_t value)
	{
		____caseGaceeris_43 = value;
	}

	inline static int32_t get_offset_of__haiqireDurhasfi_44() { return static_cast<int32_t>(offsetof(M_saicamu171_t4196319642, ____haiqireDurhasfi_44)); }
	inline uint32_t get__haiqireDurhasfi_44() const { return ____haiqireDurhasfi_44; }
	inline uint32_t* get_address_of__haiqireDurhasfi_44() { return &____haiqireDurhasfi_44; }
	inline void set__haiqireDurhasfi_44(uint32_t value)
	{
		____haiqireDurhasfi_44 = value;
	}

	inline static int32_t get_offset_of__hallziba_45() { return static_cast<int32_t>(offsetof(M_saicamu171_t4196319642, ____hallziba_45)); }
	inline uint32_t get__hallziba_45() const { return ____hallziba_45; }
	inline uint32_t* get_address_of__hallziba_45() { return &____hallziba_45; }
	inline void set__hallziba_45(uint32_t value)
	{
		____hallziba_45 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
