﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t1659122786;
// UnityEngine.Animation
struct Animation_t1724966010;
// Pathfinding.RichPath
struct RichPath_t1926198167;
// Seeker
struct Seeker_t2472610117;
// UnityEngine.CharacterController
struct CharacterController_t1618060635;
// Pathfinding.RVO.RVOController
struct RVOController_t1639282485;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t1355284822;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_LayerMask3236759763.h"
#include "AssemblyU2DCSharp_Pathfinding_RichFunnel_FunnelSimp683755508.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.RichAI
struct  RichAI_t1710845178  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.Transform Pathfinding.RichAI::target
	Transform_t1659122786 * ___target_2;
	// System.Boolean Pathfinding.RichAI::drawGizmos
	bool ___drawGizmos_3;
	// System.Boolean Pathfinding.RichAI::repeatedlySearchPaths
	bool ___repeatedlySearchPaths_4;
	// System.Single Pathfinding.RichAI::repathRate
	float ___repathRate_5;
	// System.Single Pathfinding.RichAI::maxSpeed
	float ___maxSpeed_6;
	// System.Single Pathfinding.RichAI::acceleration
	float ___acceleration_7;
	// System.Single Pathfinding.RichAI::slowdownTime
	float ___slowdownTime_8;
	// System.Single Pathfinding.RichAI::rotationSpeed
	float ___rotationSpeed_9;
	// System.Single Pathfinding.RichAI::endReachedDistance
	float ___endReachedDistance_10;
	// System.Single Pathfinding.RichAI::wallForce
	float ___wallForce_11;
	// System.Single Pathfinding.RichAI::wallDist
	float ___wallDist_12;
	// UnityEngine.Vector3 Pathfinding.RichAI::gravity
	Vector3_t4282066566  ___gravity_13;
	// System.Boolean Pathfinding.RichAI::raycastingForGroundPlacement
	bool ___raycastingForGroundPlacement_14;
	// UnityEngine.LayerMask Pathfinding.RichAI::groundMask
	LayerMask_t3236759763  ___groundMask_15;
	// System.Single Pathfinding.RichAI::centerOffset
	float ___centerOffset_16;
	// Pathfinding.RichFunnel/FunnelSimplification Pathfinding.RichAI::funnelSimplification
	int32_t ___funnelSimplification_17;
	// UnityEngine.Animation Pathfinding.RichAI::anim
	Animation_t1724966010 * ___anim_18;
	// System.Boolean Pathfinding.RichAI::preciseSlowdown
	bool ___preciseSlowdown_19;
	// System.Boolean Pathfinding.RichAI::slowWhenNotFacingTarget
	bool ___slowWhenNotFacingTarget_20;
	// UnityEngine.Vector3 Pathfinding.RichAI::velocity
	Vector3_t4282066566  ___velocity_21;
	// Pathfinding.RichPath Pathfinding.RichAI::rp
	RichPath_t1926198167 * ___rp_22;
	// Seeker Pathfinding.RichAI::seeker
	Seeker_t2472610117 * ___seeker_23;
	// UnityEngine.Transform Pathfinding.RichAI::tr
	Transform_t1659122786 * ___tr_24;
	// UnityEngine.CharacterController Pathfinding.RichAI::controller
	CharacterController_t1618060635 * ___controller_25;
	// Pathfinding.RVO.RVOController Pathfinding.RichAI::rvoController
	RVOController_t1639282485 * ___rvoController_26;
	// UnityEngine.Vector3 Pathfinding.RichAI::lastTargetPoint
	Vector3_t4282066566  ___lastTargetPoint_27;
	// UnityEngine.Vector3 Pathfinding.RichAI::currentTargetDirection
	Vector3_t4282066566  ___currentTargetDirection_28;
	// System.Boolean Pathfinding.RichAI::waitingForPathCalc
	bool ___waitingForPathCalc_29;
	// System.Boolean Pathfinding.RichAI::canSearchPath
	bool ___canSearchPath_30;
	// System.Boolean Pathfinding.RichAI::delayUpdatePath
	bool ___delayUpdatePath_31;
	// System.Boolean Pathfinding.RichAI::traversingSpecialPath
	bool ___traversingSpecialPath_32;
	// System.Boolean Pathfinding.RichAI::lastCorner
	bool ___lastCorner_33;
	// System.Single Pathfinding.RichAI::distanceToWaypoint
	float ___distanceToWaypoint_34;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> Pathfinding.RichAI::buffer
	List_1_t1355284822 * ___buffer_35;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> Pathfinding.RichAI::wallBuffer
	List_1_t1355284822 * ___wallBuffer_36;
	// System.Boolean Pathfinding.RichAI::startHasRun
	bool ___startHasRun_37;
	// System.Single Pathfinding.RichAI::lastRepath
	float ___lastRepath_38;

public:
	inline static int32_t get_offset_of_target_2() { return static_cast<int32_t>(offsetof(RichAI_t1710845178, ___target_2)); }
	inline Transform_t1659122786 * get_target_2() const { return ___target_2; }
	inline Transform_t1659122786 ** get_address_of_target_2() { return &___target_2; }
	inline void set_target_2(Transform_t1659122786 * value)
	{
		___target_2 = value;
		Il2CppCodeGenWriteBarrier(&___target_2, value);
	}

	inline static int32_t get_offset_of_drawGizmos_3() { return static_cast<int32_t>(offsetof(RichAI_t1710845178, ___drawGizmos_3)); }
	inline bool get_drawGizmos_3() const { return ___drawGizmos_3; }
	inline bool* get_address_of_drawGizmos_3() { return &___drawGizmos_3; }
	inline void set_drawGizmos_3(bool value)
	{
		___drawGizmos_3 = value;
	}

	inline static int32_t get_offset_of_repeatedlySearchPaths_4() { return static_cast<int32_t>(offsetof(RichAI_t1710845178, ___repeatedlySearchPaths_4)); }
	inline bool get_repeatedlySearchPaths_4() const { return ___repeatedlySearchPaths_4; }
	inline bool* get_address_of_repeatedlySearchPaths_4() { return &___repeatedlySearchPaths_4; }
	inline void set_repeatedlySearchPaths_4(bool value)
	{
		___repeatedlySearchPaths_4 = value;
	}

	inline static int32_t get_offset_of_repathRate_5() { return static_cast<int32_t>(offsetof(RichAI_t1710845178, ___repathRate_5)); }
	inline float get_repathRate_5() const { return ___repathRate_5; }
	inline float* get_address_of_repathRate_5() { return &___repathRate_5; }
	inline void set_repathRate_5(float value)
	{
		___repathRate_5 = value;
	}

	inline static int32_t get_offset_of_maxSpeed_6() { return static_cast<int32_t>(offsetof(RichAI_t1710845178, ___maxSpeed_6)); }
	inline float get_maxSpeed_6() const { return ___maxSpeed_6; }
	inline float* get_address_of_maxSpeed_6() { return &___maxSpeed_6; }
	inline void set_maxSpeed_6(float value)
	{
		___maxSpeed_6 = value;
	}

	inline static int32_t get_offset_of_acceleration_7() { return static_cast<int32_t>(offsetof(RichAI_t1710845178, ___acceleration_7)); }
	inline float get_acceleration_7() const { return ___acceleration_7; }
	inline float* get_address_of_acceleration_7() { return &___acceleration_7; }
	inline void set_acceleration_7(float value)
	{
		___acceleration_7 = value;
	}

	inline static int32_t get_offset_of_slowdownTime_8() { return static_cast<int32_t>(offsetof(RichAI_t1710845178, ___slowdownTime_8)); }
	inline float get_slowdownTime_8() const { return ___slowdownTime_8; }
	inline float* get_address_of_slowdownTime_8() { return &___slowdownTime_8; }
	inline void set_slowdownTime_8(float value)
	{
		___slowdownTime_8 = value;
	}

	inline static int32_t get_offset_of_rotationSpeed_9() { return static_cast<int32_t>(offsetof(RichAI_t1710845178, ___rotationSpeed_9)); }
	inline float get_rotationSpeed_9() const { return ___rotationSpeed_9; }
	inline float* get_address_of_rotationSpeed_9() { return &___rotationSpeed_9; }
	inline void set_rotationSpeed_9(float value)
	{
		___rotationSpeed_9 = value;
	}

	inline static int32_t get_offset_of_endReachedDistance_10() { return static_cast<int32_t>(offsetof(RichAI_t1710845178, ___endReachedDistance_10)); }
	inline float get_endReachedDistance_10() const { return ___endReachedDistance_10; }
	inline float* get_address_of_endReachedDistance_10() { return &___endReachedDistance_10; }
	inline void set_endReachedDistance_10(float value)
	{
		___endReachedDistance_10 = value;
	}

	inline static int32_t get_offset_of_wallForce_11() { return static_cast<int32_t>(offsetof(RichAI_t1710845178, ___wallForce_11)); }
	inline float get_wallForce_11() const { return ___wallForce_11; }
	inline float* get_address_of_wallForce_11() { return &___wallForce_11; }
	inline void set_wallForce_11(float value)
	{
		___wallForce_11 = value;
	}

	inline static int32_t get_offset_of_wallDist_12() { return static_cast<int32_t>(offsetof(RichAI_t1710845178, ___wallDist_12)); }
	inline float get_wallDist_12() const { return ___wallDist_12; }
	inline float* get_address_of_wallDist_12() { return &___wallDist_12; }
	inline void set_wallDist_12(float value)
	{
		___wallDist_12 = value;
	}

	inline static int32_t get_offset_of_gravity_13() { return static_cast<int32_t>(offsetof(RichAI_t1710845178, ___gravity_13)); }
	inline Vector3_t4282066566  get_gravity_13() const { return ___gravity_13; }
	inline Vector3_t4282066566 * get_address_of_gravity_13() { return &___gravity_13; }
	inline void set_gravity_13(Vector3_t4282066566  value)
	{
		___gravity_13 = value;
	}

	inline static int32_t get_offset_of_raycastingForGroundPlacement_14() { return static_cast<int32_t>(offsetof(RichAI_t1710845178, ___raycastingForGroundPlacement_14)); }
	inline bool get_raycastingForGroundPlacement_14() const { return ___raycastingForGroundPlacement_14; }
	inline bool* get_address_of_raycastingForGroundPlacement_14() { return &___raycastingForGroundPlacement_14; }
	inline void set_raycastingForGroundPlacement_14(bool value)
	{
		___raycastingForGroundPlacement_14 = value;
	}

	inline static int32_t get_offset_of_groundMask_15() { return static_cast<int32_t>(offsetof(RichAI_t1710845178, ___groundMask_15)); }
	inline LayerMask_t3236759763  get_groundMask_15() const { return ___groundMask_15; }
	inline LayerMask_t3236759763 * get_address_of_groundMask_15() { return &___groundMask_15; }
	inline void set_groundMask_15(LayerMask_t3236759763  value)
	{
		___groundMask_15 = value;
	}

	inline static int32_t get_offset_of_centerOffset_16() { return static_cast<int32_t>(offsetof(RichAI_t1710845178, ___centerOffset_16)); }
	inline float get_centerOffset_16() const { return ___centerOffset_16; }
	inline float* get_address_of_centerOffset_16() { return &___centerOffset_16; }
	inline void set_centerOffset_16(float value)
	{
		___centerOffset_16 = value;
	}

	inline static int32_t get_offset_of_funnelSimplification_17() { return static_cast<int32_t>(offsetof(RichAI_t1710845178, ___funnelSimplification_17)); }
	inline int32_t get_funnelSimplification_17() const { return ___funnelSimplification_17; }
	inline int32_t* get_address_of_funnelSimplification_17() { return &___funnelSimplification_17; }
	inline void set_funnelSimplification_17(int32_t value)
	{
		___funnelSimplification_17 = value;
	}

	inline static int32_t get_offset_of_anim_18() { return static_cast<int32_t>(offsetof(RichAI_t1710845178, ___anim_18)); }
	inline Animation_t1724966010 * get_anim_18() const { return ___anim_18; }
	inline Animation_t1724966010 ** get_address_of_anim_18() { return &___anim_18; }
	inline void set_anim_18(Animation_t1724966010 * value)
	{
		___anim_18 = value;
		Il2CppCodeGenWriteBarrier(&___anim_18, value);
	}

	inline static int32_t get_offset_of_preciseSlowdown_19() { return static_cast<int32_t>(offsetof(RichAI_t1710845178, ___preciseSlowdown_19)); }
	inline bool get_preciseSlowdown_19() const { return ___preciseSlowdown_19; }
	inline bool* get_address_of_preciseSlowdown_19() { return &___preciseSlowdown_19; }
	inline void set_preciseSlowdown_19(bool value)
	{
		___preciseSlowdown_19 = value;
	}

	inline static int32_t get_offset_of_slowWhenNotFacingTarget_20() { return static_cast<int32_t>(offsetof(RichAI_t1710845178, ___slowWhenNotFacingTarget_20)); }
	inline bool get_slowWhenNotFacingTarget_20() const { return ___slowWhenNotFacingTarget_20; }
	inline bool* get_address_of_slowWhenNotFacingTarget_20() { return &___slowWhenNotFacingTarget_20; }
	inline void set_slowWhenNotFacingTarget_20(bool value)
	{
		___slowWhenNotFacingTarget_20 = value;
	}

	inline static int32_t get_offset_of_velocity_21() { return static_cast<int32_t>(offsetof(RichAI_t1710845178, ___velocity_21)); }
	inline Vector3_t4282066566  get_velocity_21() const { return ___velocity_21; }
	inline Vector3_t4282066566 * get_address_of_velocity_21() { return &___velocity_21; }
	inline void set_velocity_21(Vector3_t4282066566  value)
	{
		___velocity_21 = value;
	}

	inline static int32_t get_offset_of_rp_22() { return static_cast<int32_t>(offsetof(RichAI_t1710845178, ___rp_22)); }
	inline RichPath_t1926198167 * get_rp_22() const { return ___rp_22; }
	inline RichPath_t1926198167 ** get_address_of_rp_22() { return &___rp_22; }
	inline void set_rp_22(RichPath_t1926198167 * value)
	{
		___rp_22 = value;
		Il2CppCodeGenWriteBarrier(&___rp_22, value);
	}

	inline static int32_t get_offset_of_seeker_23() { return static_cast<int32_t>(offsetof(RichAI_t1710845178, ___seeker_23)); }
	inline Seeker_t2472610117 * get_seeker_23() const { return ___seeker_23; }
	inline Seeker_t2472610117 ** get_address_of_seeker_23() { return &___seeker_23; }
	inline void set_seeker_23(Seeker_t2472610117 * value)
	{
		___seeker_23 = value;
		Il2CppCodeGenWriteBarrier(&___seeker_23, value);
	}

	inline static int32_t get_offset_of_tr_24() { return static_cast<int32_t>(offsetof(RichAI_t1710845178, ___tr_24)); }
	inline Transform_t1659122786 * get_tr_24() const { return ___tr_24; }
	inline Transform_t1659122786 ** get_address_of_tr_24() { return &___tr_24; }
	inline void set_tr_24(Transform_t1659122786 * value)
	{
		___tr_24 = value;
		Il2CppCodeGenWriteBarrier(&___tr_24, value);
	}

	inline static int32_t get_offset_of_controller_25() { return static_cast<int32_t>(offsetof(RichAI_t1710845178, ___controller_25)); }
	inline CharacterController_t1618060635 * get_controller_25() const { return ___controller_25; }
	inline CharacterController_t1618060635 ** get_address_of_controller_25() { return &___controller_25; }
	inline void set_controller_25(CharacterController_t1618060635 * value)
	{
		___controller_25 = value;
		Il2CppCodeGenWriteBarrier(&___controller_25, value);
	}

	inline static int32_t get_offset_of_rvoController_26() { return static_cast<int32_t>(offsetof(RichAI_t1710845178, ___rvoController_26)); }
	inline RVOController_t1639282485 * get_rvoController_26() const { return ___rvoController_26; }
	inline RVOController_t1639282485 ** get_address_of_rvoController_26() { return &___rvoController_26; }
	inline void set_rvoController_26(RVOController_t1639282485 * value)
	{
		___rvoController_26 = value;
		Il2CppCodeGenWriteBarrier(&___rvoController_26, value);
	}

	inline static int32_t get_offset_of_lastTargetPoint_27() { return static_cast<int32_t>(offsetof(RichAI_t1710845178, ___lastTargetPoint_27)); }
	inline Vector3_t4282066566  get_lastTargetPoint_27() const { return ___lastTargetPoint_27; }
	inline Vector3_t4282066566 * get_address_of_lastTargetPoint_27() { return &___lastTargetPoint_27; }
	inline void set_lastTargetPoint_27(Vector3_t4282066566  value)
	{
		___lastTargetPoint_27 = value;
	}

	inline static int32_t get_offset_of_currentTargetDirection_28() { return static_cast<int32_t>(offsetof(RichAI_t1710845178, ___currentTargetDirection_28)); }
	inline Vector3_t4282066566  get_currentTargetDirection_28() const { return ___currentTargetDirection_28; }
	inline Vector3_t4282066566 * get_address_of_currentTargetDirection_28() { return &___currentTargetDirection_28; }
	inline void set_currentTargetDirection_28(Vector3_t4282066566  value)
	{
		___currentTargetDirection_28 = value;
	}

	inline static int32_t get_offset_of_waitingForPathCalc_29() { return static_cast<int32_t>(offsetof(RichAI_t1710845178, ___waitingForPathCalc_29)); }
	inline bool get_waitingForPathCalc_29() const { return ___waitingForPathCalc_29; }
	inline bool* get_address_of_waitingForPathCalc_29() { return &___waitingForPathCalc_29; }
	inline void set_waitingForPathCalc_29(bool value)
	{
		___waitingForPathCalc_29 = value;
	}

	inline static int32_t get_offset_of_canSearchPath_30() { return static_cast<int32_t>(offsetof(RichAI_t1710845178, ___canSearchPath_30)); }
	inline bool get_canSearchPath_30() const { return ___canSearchPath_30; }
	inline bool* get_address_of_canSearchPath_30() { return &___canSearchPath_30; }
	inline void set_canSearchPath_30(bool value)
	{
		___canSearchPath_30 = value;
	}

	inline static int32_t get_offset_of_delayUpdatePath_31() { return static_cast<int32_t>(offsetof(RichAI_t1710845178, ___delayUpdatePath_31)); }
	inline bool get_delayUpdatePath_31() const { return ___delayUpdatePath_31; }
	inline bool* get_address_of_delayUpdatePath_31() { return &___delayUpdatePath_31; }
	inline void set_delayUpdatePath_31(bool value)
	{
		___delayUpdatePath_31 = value;
	}

	inline static int32_t get_offset_of_traversingSpecialPath_32() { return static_cast<int32_t>(offsetof(RichAI_t1710845178, ___traversingSpecialPath_32)); }
	inline bool get_traversingSpecialPath_32() const { return ___traversingSpecialPath_32; }
	inline bool* get_address_of_traversingSpecialPath_32() { return &___traversingSpecialPath_32; }
	inline void set_traversingSpecialPath_32(bool value)
	{
		___traversingSpecialPath_32 = value;
	}

	inline static int32_t get_offset_of_lastCorner_33() { return static_cast<int32_t>(offsetof(RichAI_t1710845178, ___lastCorner_33)); }
	inline bool get_lastCorner_33() const { return ___lastCorner_33; }
	inline bool* get_address_of_lastCorner_33() { return &___lastCorner_33; }
	inline void set_lastCorner_33(bool value)
	{
		___lastCorner_33 = value;
	}

	inline static int32_t get_offset_of_distanceToWaypoint_34() { return static_cast<int32_t>(offsetof(RichAI_t1710845178, ___distanceToWaypoint_34)); }
	inline float get_distanceToWaypoint_34() const { return ___distanceToWaypoint_34; }
	inline float* get_address_of_distanceToWaypoint_34() { return &___distanceToWaypoint_34; }
	inline void set_distanceToWaypoint_34(float value)
	{
		___distanceToWaypoint_34 = value;
	}

	inline static int32_t get_offset_of_buffer_35() { return static_cast<int32_t>(offsetof(RichAI_t1710845178, ___buffer_35)); }
	inline List_1_t1355284822 * get_buffer_35() const { return ___buffer_35; }
	inline List_1_t1355284822 ** get_address_of_buffer_35() { return &___buffer_35; }
	inline void set_buffer_35(List_1_t1355284822 * value)
	{
		___buffer_35 = value;
		Il2CppCodeGenWriteBarrier(&___buffer_35, value);
	}

	inline static int32_t get_offset_of_wallBuffer_36() { return static_cast<int32_t>(offsetof(RichAI_t1710845178, ___wallBuffer_36)); }
	inline List_1_t1355284822 * get_wallBuffer_36() const { return ___wallBuffer_36; }
	inline List_1_t1355284822 ** get_address_of_wallBuffer_36() { return &___wallBuffer_36; }
	inline void set_wallBuffer_36(List_1_t1355284822 * value)
	{
		___wallBuffer_36 = value;
		Il2CppCodeGenWriteBarrier(&___wallBuffer_36, value);
	}

	inline static int32_t get_offset_of_startHasRun_37() { return static_cast<int32_t>(offsetof(RichAI_t1710845178, ___startHasRun_37)); }
	inline bool get_startHasRun_37() const { return ___startHasRun_37; }
	inline bool* get_address_of_startHasRun_37() { return &___startHasRun_37; }
	inline void set_startHasRun_37(bool value)
	{
		___startHasRun_37 = value;
	}

	inline static int32_t get_offset_of_lastRepath_38() { return static_cast<int32_t>(offsetof(RichAI_t1710845178, ___lastRepath_38)); }
	inline float get_lastRepath_38() const { return ___lastRepath_38; }
	inline float* get_address_of_lastRepath_38() { return &___lastRepath_38; }
	inline void set_lastRepath_38(float value)
	{
		___lastRepath_38 = value;
	}
};

struct RichAI_t1710845178_StaticFields
{
public:
	// System.Single Pathfinding.RichAI::deltaTime
	float ___deltaTime_39;
	// UnityEngine.Color Pathfinding.RichAI::GizmoColorRaycast
	Color_t4194546905  ___GizmoColorRaycast_40;
	// UnityEngine.Color Pathfinding.RichAI::GizmoColorPath
	Color_t4194546905  ___GizmoColorPath_41;

public:
	inline static int32_t get_offset_of_deltaTime_39() { return static_cast<int32_t>(offsetof(RichAI_t1710845178_StaticFields, ___deltaTime_39)); }
	inline float get_deltaTime_39() const { return ___deltaTime_39; }
	inline float* get_address_of_deltaTime_39() { return &___deltaTime_39; }
	inline void set_deltaTime_39(float value)
	{
		___deltaTime_39 = value;
	}

	inline static int32_t get_offset_of_GizmoColorRaycast_40() { return static_cast<int32_t>(offsetof(RichAI_t1710845178_StaticFields, ___GizmoColorRaycast_40)); }
	inline Color_t4194546905  get_GizmoColorRaycast_40() const { return ___GizmoColorRaycast_40; }
	inline Color_t4194546905 * get_address_of_GizmoColorRaycast_40() { return &___GizmoColorRaycast_40; }
	inline void set_GizmoColorRaycast_40(Color_t4194546905  value)
	{
		___GizmoColorRaycast_40 = value;
	}

	inline static int32_t get_offset_of_GizmoColorPath_41() { return static_cast<int32_t>(offsetof(RichAI_t1710845178_StaticFields, ___GizmoColorPath_41)); }
	inline Color_t4194546905  get_GizmoColorPath_41() const { return ___GizmoColorPath_41; }
	inline Color_t4194546905 * get_address_of_GizmoColorPath_41() { return &___GizmoColorPath_41; }
	inline void set_GizmoColorPath_41(Color_t4194546905  value)
	{
		___GizmoColorPath_41 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
