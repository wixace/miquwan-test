﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ReplayUnitState
struct ReplayUnitState_t3104690470;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// System.String[]
struct StringU5BU5D_t4054002952;
// System.String
struct String_t;
// CombatEntity
struct CombatEntity_t684137495;
// ReplayBase
struct ReplayBase_t779703160;
// System.Object
struct Il2CppObject;
// FightCtrl
struct FightCtrl_t648967803;
// CSDatacfgManager
struct CSDatacfgManager_t1565254243;
// skillCfg
struct skillCfg_t2142425171;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_ReplayBase779703160.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_CombatEntity684137495.h"
#include "AssemblyU2DCSharp_CSDatacfgManager1565254243.h"

// System.Void ReplayUnitState::.ctor()
extern "C"  void ReplayUnitState__ctor_m805036293 (ReplayUnitState_t3104690470 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReplayUnitState::.ctor(System.Object[])
extern "C"  void ReplayUnitState__ctor_m2373240845 (ReplayUnitState_t3104690470 * __this, ObjectU5BU5D_t1108656482* ___datas0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] ReplayUnitState::ParserJsonStr(System.String)
extern "C"  StringU5BU5D_t4054002952* ReplayUnitState_ParserJsonStr_m4235674836 (ReplayUnitState_t3104690470 * __this, String_t* ___json0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ReplayUnitState::GetJsonStr()
extern "C"  String_t* ReplayUnitState_GetJsonStr_m1625915663 (ReplayUnitState_t3104690470 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReplayUnitState::Execute()
extern "C"  void ReplayUnitState_Execute_m3397153752 (ReplayUnitState_t3104690470 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CombatEntity ReplayUnitState::ilo_get_Entity1(ReplayBase)
extern "C"  CombatEntity_t684137495 * ReplayUnitState_ilo_get_Entity1_m2279700579 (Il2CppObject * __this /* static, unused */, ReplayBase_t779703160 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReplayUnitState::ilo_Log2(System.Object,System.Boolean)
extern "C"  void ReplayUnitState_ilo_Log2_m3295178217 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FightCtrl ReplayUnitState::ilo_get_fightCtrl3(CombatEntity)
extern "C"  FightCtrl_t648967803 * ReplayUnitState_ilo_get_fightCtrl3_m3982685188 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CSDatacfgManager ReplayUnitState::ilo_get_CfgDataMgr4()
extern "C"  CSDatacfgManager_t1565254243 * ReplayUnitState_ilo_get_CfgDataMgr4_m2478363595 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// skillCfg ReplayUnitState::ilo_GetskillCfg5(CSDatacfgManager,System.Int32)
extern "C"  skillCfg_t2142425171 * ReplayUnitState_ilo_GetskillCfg5_m1274906438 (Il2CppObject * __this /* static, unused */, CSDatacfgManager_t1565254243 * ____this0, int32_t ___id1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
