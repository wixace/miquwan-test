﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_rihoPole230
struct  M_rihoPole230_t2695664953  : public Il2CppObject
{
public:
	// System.String GarbageiOS.M_rihoPole230::_nisgeSefobu
	String_t* ____nisgeSefobu_0;
	// System.UInt32 GarbageiOS.M_rihoPole230::_ballna
	uint32_t ____ballna_1;
	// System.String GarbageiOS.M_rihoPole230::_vasawCearir
	String_t* ____vasawCearir_2;
	// System.UInt32 GarbageiOS.M_rihoPole230::_teyouSehi
	uint32_t ____teyouSehi_3;
	// System.Boolean GarbageiOS.M_rihoPole230::_titereFidrea
	bool ____titereFidrea_4;
	// System.Int32 GarbageiOS.M_rihoPole230::_purteartre
	int32_t ____purteartre_5;
	// System.Single GarbageiOS.M_rihoPole230::_jaycouTaisai
	float ____jaycouTaisai_6;
	// System.UInt32 GarbageiOS.M_rihoPole230::_repa
	uint32_t ____repa_7;
	// System.Boolean GarbageiOS.M_rihoPole230::_sairfarSarfastrou
	bool ____sairfarSarfastrou_8;
	// System.String GarbageiOS.M_rihoPole230::_marjaJepu
	String_t* ____marjaJepu_9;
	// System.String GarbageiOS.M_rihoPole230::_powgarxelQelpare
	String_t* ____powgarxelQelpare_10;
	// System.String GarbageiOS.M_rihoPole230::_sakorxirJurjay
	String_t* ____sakorxirJurjay_11;
	// System.UInt32 GarbageiOS.M_rihoPole230::_trajupeChawallla
	uint32_t ____trajupeChawallla_12;
	// System.Single GarbageiOS.M_rihoPole230::_vajunel
	float ____vajunel_13;
	// System.String GarbageiOS.M_rihoPole230::_jedrenur
	String_t* ____jedrenur_14;
	// System.Boolean GarbageiOS.M_rihoPole230::_wairmeNusi
	bool ____wairmeNusi_15;
	// System.Int32 GarbageiOS.M_rihoPole230::_coumay
	int32_t ____coumay_16;
	// System.Single GarbageiOS.M_rihoPole230::_teaxaiLoupe
	float ____teaxaiLoupe_17;
	// System.Boolean GarbageiOS.M_rihoPole230::_pijowBargilu
	bool ____pijowBargilu_18;
	// System.String GarbageiOS.M_rihoPole230::_hawemXawtor
	String_t* ____hawemXawtor_19;
	// System.UInt32 GarbageiOS.M_rihoPole230::_tartis
	uint32_t ____tartis_20;
	// System.Single GarbageiOS.M_rihoPole230::_merewemrorNoogisri
	float ____merewemrorNoogisri_21;
	// System.Boolean GarbageiOS.M_rihoPole230::_nastasaLehear
	bool ____nastasaLehear_22;
	// System.Int32 GarbageiOS.M_rihoPole230::_rarecas
	int32_t ____rarecas_23;
	// System.String GarbageiOS.M_rihoPole230::_trawxea
	String_t* ____trawxea_24;
	// System.Int32 GarbageiOS.M_rihoPole230::_munaMerestocas
	int32_t ____munaMerestocas_25;
	// System.Boolean GarbageiOS.M_rihoPole230::_fumurcallGemmai
	bool ____fumurcallGemmai_26;
	// System.String GarbageiOS.M_rihoPole230::_pikusereStihahall
	String_t* ____pikusereStihahall_27;
	// System.UInt32 GarbageiOS.M_rihoPole230::_tikoHaywhou
	uint32_t ____tikoHaywhou_28;
	// System.Boolean GarbageiOS.M_rihoPole230::_saybowPirboupou
	bool ____saybowPirboupou_29;
	// System.Single GarbageiOS.M_rihoPole230::_falmeHoqe
	float ____falmeHoqe_30;
	// System.UInt32 GarbageiOS.M_rihoPole230::_hajor
	uint32_t ____hajor_31;
	// System.UInt32 GarbageiOS.M_rihoPole230::_daxiwo
	uint32_t ____daxiwo_32;
	// System.Int32 GarbageiOS.M_rihoPole230::_fawsearFisrereto
	int32_t ____fawsearFisrereto_33;
	// System.Boolean GarbageiOS.M_rihoPole230::_lorchainisChibear
	bool ____lorchainisChibear_34;
	// System.UInt32 GarbageiOS.M_rihoPole230::_dasmearjeTekasvi
	uint32_t ____dasmearjeTekasvi_35;
	// System.Single GarbageiOS.M_rihoPole230::_gorwaiYejewir
	float ____gorwaiYejewir_36;
	// System.String GarbageiOS.M_rihoPole230::_tributrairZadairsal
	String_t* ____tributrairZadairsal_37;
	// System.String GarbageiOS.M_rihoPole230::_mirmowZibouwear
	String_t* ____mirmowZibouwear_38;
	// System.String GarbageiOS.M_rihoPole230::_talljowSembo
	String_t* ____talljowSembo_39;
	// System.UInt32 GarbageiOS.M_rihoPole230::_wamaBawweapear
	uint32_t ____wamaBawweapear_40;
	// System.Boolean GarbageiOS.M_rihoPole230::_jerechaNaremlair
	bool ____jerechaNaremlair_41;
	// System.UInt32 GarbageiOS.M_rihoPole230::_kezaymiNebairdray
	uint32_t ____kezaymiNebairdray_42;
	// System.String GarbageiOS.M_rihoPole230::_dearcowgair
	String_t* ____dearcowgair_43;
	// System.Int32 GarbageiOS.M_rihoPole230::_hitehisRiber
	int32_t ____hitehisRiber_44;
	// System.Single GarbageiOS.M_rihoPole230::_stoohay
	float ____stoohay_45;
	// System.Single GarbageiOS.M_rihoPole230::_seewhi
	float ____seewhi_46;
	// System.Int32 GarbageiOS.M_rihoPole230::_xelbemSawge
	int32_t ____xelbemSawge_47;
	// System.Int32 GarbageiOS.M_rihoPole230::_wemetu
	int32_t ____wemetu_48;
	// System.Boolean GarbageiOS.M_rihoPole230::_jasmisNowtrisga
	bool ____jasmisNowtrisga_49;
	// System.String GarbageiOS.M_rihoPole230::_girera
	String_t* ____girera_50;
	// System.Single GarbageiOS.M_rihoPole230::_mujow
	float ____mujow_51;
	// System.Int32 GarbageiOS.M_rihoPole230::_sutrerMorer
	int32_t ____sutrerMorer_52;
	// System.Single GarbageiOS.M_rihoPole230::_mererair
	float ____mererair_53;
	// System.Single GarbageiOS.M_rihoPole230::_kodor
	float ____kodor_54;
	// System.UInt32 GarbageiOS.M_rihoPole230::_ballrarwe
	uint32_t ____ballrarwe_55;
	// System.Int32 GarbageiOS.M_rihoPole230::_corviNejocer
	int32_t ____corviNejocer_56;
	// System.UInt32 GarbageiOS.M_rihoPole230::_bapar
	uint32_t ____bapar_57;
	// System.String GarbageiOS.M_rihoPole230::_yoobegow
	String_t* ____yoobegow_58;
	// System.String GarbageiOS.M_rihoPole230::_whijaynuJoomemke
	String_t* ____whijaynuJoomemke_59;
	// System.UInt32 GarbageiOS.M_rihoPole230::_delowper
	uint32_t ____delowper_60;
	// System.UInt32 GarbageiOS.M_rihoPole230::_lecas
	uint32_t ____lecas_61;
	// System.Boolean GarbageiOS.M_rihoPole230::_draynaJeqairwo
	bool ____draynaJeqairwo_62;
	// System.String GarbageiOS.M_rihoPole230::_jebouZasur
	String_t* ____jebouZasur_63;
	// System.String GarbageiOS.M_rihoPole230::_soutowtoo
	String_t* ____soutowtoo_64;
	// System.Int32 GarbageiOS.M_rihoPole230::_callsou
	int32_t ____callsou_65;
	// System.UInt32 GarbageiOS.M_rihoPole230::_chaleNohelti
	uint32_t ____chaleNohelti_66;
	// System.String GarbageiOS.M_rihoPole230::_gibeRecall
	String_t* ____gibeRecall_67;
	// System.Single GarbageiOS.M_rihoPole230::_jetraymallDelfou
	float ____jetraymallDelfou_68;
	// System.String GarbageiOS.M_rihoPole230::_nalerebis
	String_t* ____nalerebis_69;
	// System.Boolean GarbageiOS.M_rihoPole230::_tayjiMaje
	bool ____tayjiMaje_70;
	// System.String GarbageiOS.M_rihoPole230::_kowsesou
	String_t* ____kowsesou_71;
	// System.Single GarbageiOS.M_rihoPole230::_sowricear
	float ____sowricear_72;
	// System.Single GarbageiOS.M_rihoPole230::_virjou
	float ____virjou_73;
	// System.Single GarbageiOS.M_rihoPole230::_pairdereSaceral
	float ____pairdereSaceral_74;
	// System.String GarbageiOS.M_rihoPole230::_masbur
	String_t* ____masbur_75;
	// System.UInt32 GarbageiOS.M_rihoPole230::_calllerki
	uint32_t ____calllerki_76;
	// System.Boolean GarbageiOS.M_rihoPole230::_nuroufaKelwheatur
	bool ____nuroufaKelwheatur_77;
	// System.Int32 GarbageiOS.M_rihoPole230::_nisbea
	int32_t ____nisbea_78;
	// System.Single GarbageiOS.M_rihoPole230::_cayloo
	float ____cayloo_79;
	// System.Int32 GarbageiOS.M_rihoPole230::_pousas
	int32_t ____pousas_80;
	// System.Boolean GarbageiOS.M_rihoPole230::_yajearjarRayzouso
	bool ____yajearjarRayzouso_81;
	// System.UInt32 GarbageiOS.M_rihoPole230::_nayta
	uint32_t ____nayta_82;
	// System.Int32 GarbageiOS.M_rihoPole230::_saykavi
	int32_t ____saykavi_83;

public:
	inline static int32_t get_offset_of__nisgeSefobu_0() { return static_cast<int32_t>(offsetof(M_rihoPole230_t2695664953, ____nisgeSefobu_0)); }
	inline String_t* get__nisgeSefobu_0() const { return ____nisgeSefobu_0; }
	inline String_t** get_address_of__nisgeSefobu_0() { return &____nisgeSefobu_0; }
	inline void set__nisgeSefobu_0(String_t* value)
	{
		____nisgeSefobu_0 = value;
		Il2CppCodeGenWriteBarrier(&____nisgeSefobu_0, value);
	}

	inline static int32_t get_offset_of__ballna_1() { return static_cast<int32_t>(offsetof(M_rihoPole230_t2695664953, ____ballna_1)); }
	inline uint32_t get__ballna_1() const { return ____ballna_1; }
	inline uint32_t* get_address_of__ballna_1() { return &____ballna_1; }
	inline void set__ballna_1(uint32_t value)
	{
		____ballna_1 = value;
	}

	inline static int32_t get_offset_of__vasawCearir_2() { return static_cast<int32_t>(offsetof(M_rihoPole230_t2695664953, ____vasawCearir_2)); }
	inline String_t* get__vasawCearir_2() const { return ____vasawCearir_2; }
	inline String_t** get_address_of__vasawCearir_2() { return &____vasawCearir_2; }
	inline void set__vasawCearir_2(String_t* value)
	{
		____vasawCearir_2 = value;
		Il2CppCodeGenWriteBarrier(&____vasawCearir_2, value);
	}

	inline static int32_t get_offset_of__teyouSehi_3() { return static_cast<int32_t>(offsetof(M_rihoPole230_t2695664953, ____teyouSehi_3)); }
	inline uint32_t get__teyouSehi_3() const { return ____teyouSehi_3; }
	inline uint32_t* get_address_of__teyouSehi_3() { return &____teyouSehi_3; }
	inline void set__teyouSehi_3(uint32_t value)
	{
		____teyouSehi_3 = value;
	}

	inline static int32_t get_offset_of__titereFidrea_4() { return static_cast<int32_t>(offsetof(M_rihoPole230_t2695664953, ____titereFidrea_4)); }
	inline bool get__titereFidrea_4() const { return ____titereFidrea_4; }
	inline bool* get_address_of__titereFidrea_4() { return &____titereFidrea_4; }
	inline void set__titereFidrea_4(bool value)
	{
		____titereFidrea_4 = value;
	}

	inline static int32_t get_offset_of__purteartre_5() { return static_cast<int32_t>(offsetof(M_rihoPole230_t2695664953, ____purteartre_5)); }
	inline int32_t get__purteartre_5() const { return ____purteartre_5; }
	inline int32_t* get_address_of__purteartre_5() { return &____purteartre_5; }
	inline void set__purteartre_5(int32_t value)
	{
		____purteartre_5 = value;
	}

	inline static int32_t get_offset_of__jaycouTaisai_6() { return static_cast<int32_t>(offsetof(M_rihoPole230_t2695664953, ____jaycouTaisai_6)); }
	inline float get__jaycouTaisai_6() const { return ____jaycouTaisai_6; }
	inline float* get_address_of__jaycouTaisai_6() { return &____jaycouTaisai_6; }
	inline void set__jaycouTaisai_6(float value)
	{
		____jaycouTaisai_6 = value;
	}

	inline static int32_t get_offset_of__repa_7() { return static_cast<int32_t>(offsetof(M_rihoPole230_t2695664953, ____repa_7)); }
	inline uint32_t get__repa_7() const { return ____repa_7; }
	inline uint32_t* get_address_of__repa_7() { return &____repa_7; }
	inline void set__repa_7(uint32_t value)
	{
		____repa_7 = value;
	}

	inline static int32_t get_offset_of__sairfarSarfastrou_8() { return static_cast<int32_t>(offsetof(M_rihoPole230_t2695664953, ____sairfarSarfastrou_8)); }
	inline bool get__sairfarSarfastrou_8() const { return ____sairfarSarfastrou_8; }
	inline bool* get_address_of__sairfarSarfastrou_8() { return &____sairfarSarfastrou_8; }
	inline void set__sairfarSarfastrou_8(bool value)
	{
		____sairfarSarfastrou_8 = value;
	}

	inline static int32_t get_offset_of__marjaJepu_9() { return static_cast<int32_t>(offsetof(M_rihoPole230_t2695664953, ____marjaJepu_9)); }
	inline String_t* get__marjaJepu_9() const { return ____marjaJepu_9; }
	inline String_t** get_address_of__marjaJepu_9() { return &____marjaJepu_9; }
	inline void set__marjaJepu_9(String_t* value)
	{
		____marjaJepu_9 = value;
		Il2CppCodeGenWriteBarrier(&____marjaJepu_9, value);
	}

	inline static int32_t get_offset_of__powgarxelQelpare_10() { return static_cast<int32_t>(offsetof(M_rihoPole230_t2695664953, ____powgarxelQelpare_10)); }
	inline String_t* get__powgarxelQelpare_10() const { return ____powgarxelQelpare_10; }
	inline String_t** get_address_of__powgarxelQelpare_10() { return &____powgarxelQelpare_10; }
	inline void set__powgarxelQelpare_10(String_t* value)
	{
		____powgarxelQelpare_10 = value;
		Il2CppCodeGenWriteBarrier(&____powgarxelQelpare_10, value);
	}

	inline static int32_t get_offset_of__sakorxirJurjay_11() { return static_cast<int32_t>(offsetof(M_rihoPole230_t2695664953, ____sakorxirJurjay_11)); }
	inline String_t* get__sakorxirJurjay_11() const { return ____sakorxirJurjay_11; }
	inline String_t** get_address_of__sakorxirJurjay_11() { return &____sakorxirJurjay_11; }
	inline void set__sakorxirJurjay_11(String_t* value)
	{
		____sakorxirJurjay_11 = value;
		Il2CppCodeGenWriteBarrier(&____sakorxirJurjay_11, value);
	}

	inline static int32_t get_offset_of__trajupeChawallla_12() { return static_cast<int32_t>(offsetof(M_rihoPole230_t2695664953, ____trajupeChawallla_12)); }
	inline uint32_t get__trajupeChawallla_12() const { return ____trajupeChawallla_12; }
	inline uint32_t* get_address_of__trajupeChawallla_12() { return &____trajupeChawallla_12; }
	inline void set__trajupeChawallla_12(uint32_t value)
	{
		____trajupeChawallla_12 = value;
	}

	inline static int32_t get_offset_of__vajunel_13() { return static_cast<int32_t>(offsetof(M_rihoPole230_t2695664953, ____vajunel_13)); }
	inline float get__vajunel_13() const { return ____vajunel_13; }
	inline float* get_address_of__vajunel_13() { return &____vajunel_13; }
	inline void set__vajunel_13(float value)
	{
		____vajunel_13 = value;
	}

	inline static int32_t get_offset_of__jedrenur_14() { return static_cast<int32_t>(offsetof(M_rihoPole230_t2695664953, ____jedrenur_14)); }
	inline String_t* get__jedrenur_14() const { return ____jedrenur_14; }
	inline String_t** get_address_of__jedrenur_14() { return &____jedrenur_14; }
	inline void set__jedrenur_14(String_t* value)
	{
		____jedrenur_14 = value;
		Il2CppCodeGenWriteBarrier(&____jedrenur_14, value);
	}

	inline static int32_t get_offset_of__wairmeNusi_15() { return static_cast<int32_t>(offsetof(M_rihoPole230_t2695664953, ____wairmeNusi_15)); }
	inline bool get__wairmeNusi_15() const { return ____wairmeNusi_15; }
	inline bool* get_address_of__wairmeNusi_15() { return &____wairmeNusi_15; }
	inline void set__wairmeNusi_15(bool value)
	{
		____wairmeNusi_15 = value;
	}

	inline static int32_t get_offset_of__coumay_16() { return static_cast<int32_t>(offsetof(M_rihoPole230_t2695664953, ____coumay_16)); }
	inline int32_t get__coumay_16() const { return ____coumay_16; }
	inline int32_t* get_address_of__coumay_16() { return &____coumay_16; }
	inline void set__coumay_16(int32_t value)
	{
		____coumay_16 = value;
	}

	inline static int32_t get_offset_of__teaxaiLoupe_17() { return static_cast<int32_t>(offsetof(M_rihoPole230_t2695664953, ____teaxaiLoupe_17)); }
	inline float get__teaxaiLoupe_17() const { return ____teaxaiLoupe_17; }
	inline float* get_address_of__teaxaiLoupe_17() { return &____teaxaiLoupe_17; }
	inline void set__teaxaiLoupe_17(float value)
	{
		____teaxaiLoupe_17 = value;
	}

	inline static int32_t get_offset_of__pijowBargilu_18() { return static_cast<int32_t>(offsetof(M_rihoPole230_t2695664953, ____pijowBargilu_18)); }
	inline bool get__pijowBargilu_18() const { return ____pijowBargilu_18; }
	inline bool* get_address_of__pijowBargilu_18() { return &____pijowBargilu_18; }
	inline void set__pijowBargilu_18(bool value)
	{
		____pijowBargilu_18 = value;
	}

	inline static int32_t get_offset_of__hawemXawtor_19() { return static_cast<int32_t>(offsetof(M_rihoPole230_t2695664953, ____hawemXawtor_19)); }
	inline String_t* get__hawemXawtor_19() const { return ____hawemXawtor_19; }
	inline String_t** get_address_of__hawemXawtor_19() { return &____hawemXawtor_19; }
	inline void set__hawemXawtor_19(String_t* value)
	{
		____hawemXawtor_19 = value;
		Il2CppCodeGenWriteBarrier(&____hawemXawtor_19, value);
	}

	inline static int32_t get_offset_of__tartis_20() { return static_cast<int32_t>(offsetof(M_rihoPole230_t2695664953, ____tartis_20)); }
	inline uint32_t get__tartis_20() const { return ____tartis_20; }
	inline uint32_t* get_address_of__tartis_20() { return &____tartis_20; }
	inline void set__tartis_20(uint32_t value)
	{
		____tartis_20 = value;
	}

	inline static int32_t get_offset_of__merewemrorNoogisri_21() { return static_cast<int32_t>(offsetof(M_rihoPole230_t2695664953, ____merewemrorNoogisri_21)); }
	inline float get__merewemrorNoogisri_21() const { return ____merewemrorNoogisri_21; }
	inline float* get_address_of__merewemrorNoogisri_21() { return &____merewemrorNoogisri_21; }
	inline void set__merewemrorNoogisri_21(float value)
	{
		____merewemrorNoogisri_21 = value;
	}

	inline static int32_t get_offset_of__nastasaLehear_22() { return static_cast<int32_t>(offsetof(M_rihoPole230_t2695664953, ____nastasaLehear_22)); }
	inline bool get__nastasaLehear_22() const { return ____nastasaLehear_22; }
	inline bool* get_address_of__nastasaLehear_22() { return &____nastasaLehear_22; }
	inline void set__nastasaLehear_22(bool value)
	{
		____nastasaLehear_22 = value;
	}

	inline static int32_t get_offset_of__rarecas_23() { return static_cast<int32_t>(offsetof(M_rihoPole230_t2695664953, ____rarecas_23)); }
	inline int32_t get__rarecas_23() const { return ____rarecas_23; }
	inline int32_t* get_address_of__rarecas_23() { return &____rarecas_23; }
	inline void set__rarecas_23(int32_t value)
	{
		____rarecas_23 = value;
	}

	inline static int32_t get_offset_of__trawxea_24() { return static_cast<int32_t>(offsetof(M_rihoPole230_t2695664953, ____trawxea_24)); }
	inline String_t* get__trawxea_24() const { return ____trawxea_24; }
	inline String_t** get_address_of__trawxea_24() { return &____trawxea_24; }
	inline void set__trawxea_24(String_t* value)
	{
		____trawxea_24 = value;
		Il2CppCodeGenWriteBarrier(&____trawxea_24, value);
	}

	inline static int32_t get_offset_of__munaMerestocas_25() { return static_cast<int32_t>(offsetof(M_rihoPole230_t2695664953, ____munaMerestocas_25)); }
	inline int32_t get__munaMerestocas_25() const { return ____munaMerestocas_25; }
	inline int32_t* get_address_of__munaMerestocas_25() { return &____munaMerestocas_25; }
	inline void set__munaMerestocas_25(int32_t value)
	{
		____munaMerestocas_25 = value;
	}

	inline static int32_t get_offset_of__fumurcallGemmai_26() { return static_cast<int32_t>(offsetof(M_rihoPole230_t2695664953, ____fumurcallGemmai_26)); }
	inline bool get__fumurcallGemmai_26() const { return ____fumurcallGemmai_26; }
	inline bool* get_address_of__fumurcallGemmai_26() { return &____fumurcallGemmai_26; }
	inline void set__fumurcallGemmai_26(bool value)
	{
		____fumurcallGemmai_26 = value;
	}

	inline static int32_t get_offset_of__pikusereStihahall_27() { return static_cast<int32_t>(offsetof(M_rihoPole230_t2695664953, ____pikusereStihahall_27)); }
	inline String_t* get__pikusereStihahall_27() const { return ____pikusereStihahall_27; }
	inline String_t** get_address_of__pikusereStihahall_27() { return &____pikusereStihahall_27; }
	inline void set__pikusereStihahall_27(String_t* value)
	{
		____pikusereStihahall_27 = value;
		Il2CppCodeGenWriteBarrier(&____pikusereStihahall_27, value);
	}

	inline static int32_t get_offset_of__tikoHaywhou_28() { return static_cast<int32_t>(offsetof(M_rihoPole230_t2695664953, ____tikoHaywhou_28)); }
	inline uint32_t get__tikoHaywhou_28() const { return ____tikoHaywhou_28; }
	inline uint32_t* get_address_of__tikoHaywhou_28() { return &____tikoHaywhou_28; }
	inline void set__tikoHaywhou_28(uint32_t value)
	{
		____tikoHaywhou_28 = value;
	}

	inline static int32_t get_offset_of__saybowPirboupou_29() { return static_cast<int32_t>(offsetof(M_rihoPole230_t2695664953, ____saybowPirboupou_29)); }
	inline bool get__saybowPirboupou_29() const { return ____saybowPirboupou_29; }
	inline bool* get_address_of__saybowPirboupou_29() { return &____saybowPirboupou_29; }
	inline void set__saybowPirboupou_29(bool value)
	{
		____saybowPirboupou_29 = value;
	}

	inline static int32_t get_offset_of__falmeHoqe_30() { return static_cast<int32_t>(offsetof(M_rihoPole230_t2695664953, ____falmeHoqe_30)); }
	inline float get__falmeHoqe_30() const { return ____falmeHoqe_30; }
	inline float* get_address_of__falmeHoqe_30() { return &____falmeHoqe_30; }
	inline void set__falmeHoqe_30(float value)
	{
		____falmeHoqe_30 = value;
	}

	inline static int32_t get_offset_of__hajor_31() { return static_cast<int32_t>(offsetof(M_rihoPole230_t2695664953, ____hajor_31)); }
	inline uint32_t get__hajor_31() const { return ____hajor_31; }
	inline uint32_t* get_address_of__hajor_31() { return &____hajor_31; }
	inline void set__hajor_31(uint32_t value)
	{
		____hajor_31 = value;
	}

	inline static int32_t get_offset_of__daxiwo_32() { return static_cast<int32_t>(offsetof(M_rihoPole230_t2695664953, ____daxiwo_32)); }
	inline uint32_t get__daxiwo_32() const { return ____daxiwo_32; }
	inline uint32_t* get_address_of__daxiwo_32() { return &____daxiwo_32; }
	inline void set__daxiwo_32(uint32_t value)
	{
		____daxiwo_32 = value;
	}

	inline static int32_t get_offset_of__fawsearFisrereto_33() { return static_cast<int32_t>(offsetof(M_rihoPole230_t2695664953, ____fawsearFisrereto_33)); }
	inline int32_t get__fawsearFisrereto_33() const { return ____fawsearFisrereto_33; }
	inline int32_t* get_address_of__fawsearFisrereto_33() { return &____fawsearFisrereto_33; }
	inline void set__fawsearFisrereto_33(int32_t value)
	{
		____fawsearFisrereto_33 = value;
	}

	inline static int32_t get_offset_of__lorchainisChibear_34() { return static_cast<int32_t>(offsetof(M_rihoPole230_t2695664953, ____lorchainisChibear_34)); }
	inline bool get__lorchainisChibear_34() const { return ____lorchainisChibear_34; }
	inline bool* get_address_of__lorchainisChibear_34() { return &____lorchainisChibear_34; }
	inline void set__lorchainisChibear_34(bool value)
	{
		____lorchainisChibear_34 = value;
	}

	inline static int32_t get_offset_of__dasmearjeTekasvi_35() { return static_cast<int32_t>(offsetof(M_rihoPole230_t2695664953, ____dasmearjeTekasvi_35)); }
	inline uint32_t get__dasmearjeTekasvi_35() const { return ____dasmearjeTekasvi_35; }
	inline uint32_t* get_address_of__dasmearjeTekasvi_35() { return &____dasmearjeTekasvi_35; }
	inline void set__dasmearjeTekasvi_35(uint32_t value)
	{
		____dasmearjeTekasvi_35 = value;
	}

	inline static int32_t get_offset_of__gorwaiYejewir_36() { return static_cast<int32_t>(offsetof(M_rihoPole230_t2695664953, ____gorwaiYejewir_36)); }
	inline float get__gorwaiYejewir_36() const { return ____gorwaiYejewir_36; }
	inline float* get_address_of__gorwaiYejewir_36() { return &____gorwaiYejewir_36; }
	inline void set__gorwaiYejewir_36(float value)
	{
		____gorwaiYejewir_36 = value;
	}

	inline static int32_t get_offset_of__tributrairZadairsal_37() { return static_cast<int32_t>(offsetof(M_rihoPole230_t2695664953, ____tributrairZadairsal_37)); }
	inline String_t* get__tributrairZadairsal_37() const { return ____tributrairZadairsal_37; }
	inline String_t** get_address_of__tributrairZadairsal_37() { return &____tributrairZadairsal_37; }
	inline void set__tributrairZadairsal_37(String_t* value)
	{
		____tributrairZadairsal_37 = value;
		Il2CppCodeGenWriteBarrier(&____tributrairZadairsal_37, value);
	}

	inline static int32_t get_offset_of__mirmowZibouwear_38() { return static_cast<int32_t>(offsetof(M_rihoPole230_t2695664953, ____mirmowZibouwear_38)); }
	inline String_t* get__mirmowZibouwear_38() const { return ____mirmowZibouwear_38; }
	inline String_t** get_address_of__mirmowZibouwear_38() { return &____mirmowZibouwear_38; }
	inline void set__mirmowZibouwear_38(String_t* value)
	{
		____mirmowZibouwear_38 = value;
		Il2CppCodeGenWriteBarrier(&____mirmowZibouwear_38, value);
	}

	inline static int32_t get_offset_of__talljowSembo_39() { return static_cast<int32_t>(offsetof(M_rihoPole230_t2695664953, ____talljowSembo_39)); }
	inline String_t* get__talljowSembo_39() const { return ____talljowSembo_39; }
	inline String_t** get_address_of__talljowSembo_39() { return &____talljowSembo_39; }
	inline void set__talljowSembo_39(String_t* value)
	{
		____talljowSembo_39 = value;
		Il2CppCodeGenWriteBarrier(&____talljowSembo_39, value);
	}

	inline static int32_t get_offset_of__wamaBawweapear_40() { return static_cast<int32_t>(offsetof(M_rihoPole230_t2695664953, ____wamaBawweapear_40)); }
	inline uint32_t get__wamaBawweapear_40() const { return ____wamaBawweapear_40; }
	inline uint32_t* get_address_of__wamaBawweapear_40() { return &____wamaBawweapear_40; }
	inline void set__wamaBawweapear_40(uint32_t value)
	{
		____wamaBawweapear_40 = value;
	}

	inline static int32_t get_offset_of__jerechaNaremlair_41() { return static_cast<int32_t>(offsetof(M_rihoPole230_t2695664953, ____jerechaNaremlair_41)); }
	inline bool get__jerechaNaremlair_41() const { return ____jerechaNaremlair_41; }
	inline bool* get_address_of__jerechaNaremlair_41() { return &____jerechaNaremlair_41; }
	inline void set__jerechaNaremlair_41(bool value)
	{
		____jerechaNaremlair_41 = value;
	}

	inline static int32_t get_offset_of__kezaymiNebairdray_42() { return static_cast<int32_t>(offsetof(M_rihoPole230_t2695664953, ____kezaymiNebairdray_42)); }
	inline uint32_t get__kezaymiNebairdray_42() const { return ____kezaymiNebairdray_42; }
	inline uint32_t* get_address_of__kezaymiNebairdray_42() { return &____kezaymiNebairdray_42; }
	inline void set__kezaymiNebairdray_42(uint32_t value)
	{
		____kezaymiNebairdray_42 = value;
	}

	inline static int32_t get_offset_of__dearcowgair_43() { return static_cast<int32_t>(offsetof(M_rihoPole230_t2695664953, ____dearcowgair_43)); }
	inline String_t* get__dearcowgair_43() const { return ____dearcowgair_43; }
	inline String_t** get_address_of__dearcowgair_43() { return &____dearcowgair_43; }
	inline void set__dearcowgair_43(String_t* value)
	{
		____dearcowgair_43 = value;
		Il2CppCodeGenWriteBarrier(&____dearcowgair_43, value);
	}

	inline static int32_t get_offset_of__hitehisRiber_44() { return static_cast<int32_t>(offsetof(M_rihoPole230_t2695664953, ____hitehisRiber_44)); }
	inline int32_t get__hitehisRiber_44() const { return ____hitehisRiber_44; }
	inline int32_t* get_address_of__hitehisRiber_44() { return &____hitehisRiber_44; }
	inline void set__hitehisRiber_44(int32_t value)
	{
		____hitehisRiber_44 = value;
	}

	inline static int32_t get_offset_of__stoohay_45() { return static_cast<int32_t>(offsetof(M_rihoPole230_t2695664953, ____stoohay_45)); }
	inline float get__stoohay_45() const { return ____stoohay_45; }
	inline float* get_address_of__stoohay_45() { return &____stoohay_45; }
	inline void set__stoohay_45(float value)
	{
		____stoohay_45 = value;
	}

	inline static int32_t get_offset_of__seewhi_46() { return static_cast<int32_t>(offsetof(M_rihoPole230_t2695664953, ____seewhi_46)); }
	inline float get__seewhi_46() const { return ____seewhi_46; }
	inline float* get_address_of__seewhi_46() { return &____seewhi_46; }
	inline void set__seewhi_46(float value)
	{
		____seewhi_46 = value;
	}

	inline static int32_t get_offset_of__xelbemSawge_47() { return static_cast<int32_t>(offsetof(M_rihoPole230_t2695664953, ____xelbemSawge_47)); }
	inline int32_t get__xelbemSawge_47() const { return ____xelbemSawge_47; }
	inline int32_t* get_address_of__xelbemSawge_47() { return &____xelbemSawge_47; }
	inline void set__xelbemSawge_47(int32_t value)
	{
		____xelbemSawge_47 = value;
	}

	inline static int32_t get_offset_of__wemetu_48() { return static_cast<int32_t>(offsetof(M_rihoPole230_t2695664953, ____wemetu_48)); }
	inline int32_t get__wemetu_48() const { return ____wemetu_48; }
	inline int32_t* get_address_of__wemetu_48() { return &____wemetu_48; }
	inline void set__wemetu_48(int32_t value)
	{
		____wemetu_48 = value;
	}

	inline static int32_t get_offset_of__jasmisNowtrisga_49() { return static_cast<int32_t>(offsetof(M_rihoPole230_t2695664953, ____jasmisNowtrisga_49)); }
	inline bool get__jasmisNowtrisga_49() const { return ____jasmisNowtrisga_49; }
	inline bool* get_address_of__jasmisNowtrisga_49() { return &____jasmisNowtrisga_49; }
	inline void set__jasmisNowtrisga_49(bool value)
	{
		____jasmisNowtrisga_49 = value;
	}

	inline static int32_t get_offset_of__girera_50() { return static_cast<int32_t>(offsetof(M_rihoPole230_t2695664953, ____girera_50)); }
	inline String_t* get__girera_50() const { return ____girera_50; }
	inline String_t** get_address_of__girera_50() { return &____girera_50; }
	inline void set__girera_50(String_t* value)
	{
		____girera_50 = value;
		Il2CppCodeGenWriteBarrier(&____girera_50, value);
	}

	inline static int32_t get_offset_of__mujow_51() { return static_cast<int32_t>(offsetof(M_rihoPole230_t2695664953, ____mujow_51)); }
	inline float get__mujow_51() const { return ____mujow_51; }
	inline float* get_address_of__mujow_51() { return &____mujow_51; }
	inline void set__mujow_51(float value)
	{
		____mujow_51 = value;
	}

	inline static int32_t get_offset_of__sutrerMorer_52() { return static_cast<int32_t>(offsetof(M_rihoPole230_t2695664953, ____sutrerMorer_52)); }
	inline int32_t get__sutrerMorer_52() const { return ____sutrerMorer_52; }
	inline int32_t* get_address_of__sutrerMorer_52() { return &____sutrerMorer_52; }
	inline void set__sutrerMorer_52(int32_t value)
	{
		____sutrerMorer_52 = value;
	}

	inline static int32_t get_offset_of__mererair_53() { return static_cast<int32_t>(offsetof(M_rihoPole230_t2695664953, ____mererair_53)); }
	inline float get__mererair_53() const { return ____mererair_53; }
	inline float* get_address_of__mererair_53() { return &____mererair_53; }
	inline void set__mererair_53(float value)
	{
		____mererair_53 = value;
	}

	inline static int32_t get_offset_of__kodor_54() { return static_cast<int32_t>(offsetof(M_rihoPole230_t2695664953, ____kodor_54)); }
	inline float get__kodor_54() const { return ____kodor_54; }
	inline float* get_address_of__kodor_54() { return &____kodor_54; }
	inline void set__kodor_54(float value)
	{
		____kodor_54 = value;
	}

	inline static int32_t get_offset_of__ballrarwe_55() { return static_cast<int32_t>(offsetof(M_rihoPole230_t2695664953, ____ballrarwe_55)); }
	inline uint32_t get__ballrarwe_55() const { return ____ballrarwe_55; }
	inline uint32_t* get_address_of__ballrarwe_55() { return &____ballrarwe_55; }
	inline void set__ballrarwe_55(uint32_t value)
	{
		____ballrarwe_55 = value;
	}

	inline static int32_t get_offset_of__corviNejocer_56() { return static_cast<int32_t>(offsetof(M_rihoPole230_t2695664953, ____corviNejocer_56)); }
	inline int32_t get__corviNejocer_56() const { return ____corviNejocer_56; }
	inline int32_t* get_address_of__corviNejocer_56() { return &____corviNejocer_56; }
	inline void set__corviNejocer_56(int32_t value)
	{
		____corviNejocer_56 = value;
	}

	inline static int32_t get_offset_of__bapar_57() { return static_cast<int32_t>(offsetof(M_rihoPole230_t2695664953, ____bapar_57)); }
	inline uint32_t get__bapar_57() const { return ____bapar_57; }
	inline uint32_t* get_address_of__bapar_57() { return &____bapar_57; }
	inline void set__bapar_57(uint32_t value)
	{
		____bapar_57 = value;
	}

	inline static int32_t get_offset_of__yoobegow_58() { return static_cast<int32_t>(offsetof(M_rihoPole230_t2695664953, ____yoobegow_58)); }
	inline String_t* get__yoobegow_58() const { return ____yoobegow_58; }
	inline String_t** get_address_of__yoobegow_58() { return &____yoobegow_58; }
	inline void set__yoobegow_58(String_t* value)
	{
		____yoobegow_58 = value;
		Il2CppCodeGenWriteBarrier(&____yoobegow_58, value);
	}

	inline static int32_t get_offset_of__whijaynuJoomemke_59() { return static_cast<int32_t>(offsetof(M_rihoPole230_t2695664953, ____whijaynuJoomemke_59)); }
	inline String_t* get__whijaynuJoomemke_59() const { return ____whijaynuJoomemke_59; }
	inline String_t** get_address_of__whijaynuJoomemke_59() { return &____whijaynuJoomemke_59; }
	inline void set__whijaynuJoomemke_59(String_t* value)
	{
		____whijaynuJoomemke_59 = value;
		Il2CppCodeGenWriteBarrier(&____whijaynuJoomemke_59, value);
	}

	inline static int32_t get_offset_of__delowper_60() { return static_cast<int32_t>(offsetof(M_rihoPole230_t2695664953, ____delowper_60)); }
	inline uint32_t get__delowper_60() const { return ____delowper_60; }
	inline uint32_t* get_address_of__delowper_60() { return &____delowper_60; }
	inline void set__delowper_60(uint32_t value)
	{
		____delowper_60 = value;
	}

	inline static int32_t get_offset_of__lecas_61() { return static_cast<int32_t>(offsetof(M_rihoPole230_t2695664953, ____lecas_61)); }
	inline uint32_t get__lecas_61() const { return ____lecas_61; }
	inline uint32_t* get_address_of__lecas_61() { return &____lecas_61; }
	inline void set__lecas_61(uint32_t value)
	{
		____lecas_61 = value;
	}

	inline static int32_t get_offset_of__draynaJeqairwo_62() { return static_cast<int32_t>(offsetof(M_rihoPole230_t2695664953, ____draynaJeqairwo_62)); }
	inline bool get__draynaJeqairwo_62() const { return ____draynaJeqairwo_62; }
	inline bool* get_address_of__draynaJeqairwo_62() { return &____draynaJeqairwo_62; }
	inline void set__draynaJeqairwo_62(bool value)
	{
		____draynaJeqairwo_62 = value;
	}

	inline static int32_t get_offset_of__jebouZasur_63() { return static_cast<int32_t>(offsetof(M_rihoPole230_t2695664953, ____jebouZasur_63)); }
	inline String_t* get__jebouZasur_63() const { return ____jebouZasur_63; }
	inline String_t** get_address_of__jebouZasur_63() { return &____jebouZasur_63; }
	inline void set__jebouZasur_63(String_t* value)
	{
		____jebouZasur_63 = value;
		Il2CppCodeGenWriteBarrier(&____jebouZasur_63, value);
	}

	inline static int32_t get_offset_of__soutowtoo_64() { return static_cast<int32_t>(offsetof(M_rihoPole230_t2695664953, ____soutowtoo_64)); }
	inline String_t* get__soutowtoo_64() const { return ____soutowtoo_64; }
	inline String_t** get_address_of__soutowtoo_64() { return &____soutowtoo_64; }
	inline void set__soutowtoo_64(String_t* value)
	{
		____soutowtoo_64 = value;
		Il2CppCodeGenWriteBarrier(&____soutowtoo_64, value);
	}

	inline static int32_t get_offset_of__callsou_65() { return static_cast<int32_t>(offsetof(M_rihoPole230_t2695664953, ____callsou_65)); }
	inline int32_t get__callsou_65() const { return ____callsou_65; }
	inline int32_t* get_address_of__callsou_65() { return &____callsou_65; }
	inline void set__callsou_65(int32_t value)
	{
		____callsou_65 = value;
	}

	inline static int32_t get_offset_of__chaleNohelti_66() { return static_cast<int32_t>(offsetof(M_rihoPole230_t2695664953, ____chaleNohelti_66)); }
	inline uint32_t get__chaleNohelti_66() const { return ____chaleNohelti_66; }
	inline uint32_t* get_address_of__chaleNohelti_66() { return &____chaleNohelti_66; }
	inline void set__chaleNohelti_66(uint32_t value)
	{
		____chaleNohelti_66 = value;
	}

	inline static int32_t get_offset_of__gibeRecall_67() { return static_cast<int32_t>(offsetof(M_rihoPole230_t2695664953, ____gibeRecall_67)); }
	inline String_t* get__gibeRecall_67() const { return ____gibeRecall_67; }
	inline String_t** get_address_of__gibeRecall_67() { return &____gibeRecall_67; }
	inline void set__gibeRecall_67(String_t* value)
	{
		____gibeRecall_67 = value;
		Il2CppCodeGenWriteBarrier(&____gibeRecall_67, value);
	}

	inline static int32_t get_offset_of__jetraymallDelfou_68() { return static_cast<int32_t>(offsetof(M_rihoPole230_t2695664953, ____jetraymallDelfou_68)); }
	inline float get__jetraymallDelfou_68() const { return ____jetraymallDelfou_68; }
	inline float* get_address_of__jetraymallDelfou_68() { return &____jetraymallDelfou_68; }
	inline void set__jetraymallDelfou_68(float value)
	{
		____jetraymallDelfou_68 = value;
	}

	inline static int32_t get_offset_of__nalerebis_69() { return static_cast<int32_t>(offsetof(M_rihoPole230_t2695664953, ____nalerebis_69)); }
	inline String_t* get__nalerebis_69() const { return ____nalerebis_69; }
	inline String_t** get_address_of__nalerebis_69() { return &____nalerebis_69; }
	inline void set__nalerebis_69(String_t* value)
	{
		____nalerebis_69 = value;
		Il2CppCodeGenWriteBarrier(&____nalerebis_69, value);
	}

	inline static int32_t get_offset_of__tayjiMaje_70() { return static_cast<int32_t>(offsetof(M_rihoPole230_t2695664953, ____tayjiMaje_70)); }
	inline bool get__tayjiMaje_70() const { return ____tayjiMaje_70; }
	inline bool* get_address_of__tayjiMaje_70() { return &____tayjiMaje_70; }
	inline void set__tayjiMaje_70(bool value)
	{
		____tayjiMaje_70 = value;
	}

	inline static int32_t get_offset_of__kowsesou_71() { return static_cast<int32_t>(offsetof(M_rihoPole230_t2695664953, ____kowsesou_71)); }
	inline String_t* get__kowsesou_71() const { return ____kowsesou_71; }
	inline String_t** get_address_of__kowsesou_71() { return &____kowsesou_71; }
	inline void set__kowsesou_71(String_t* value)
	{
		____kowsesou_71 = value;
		Il2CppCodeGenWriteBarrier(&____kowsesou_71, value);
	}

	inline static int32_t get_offset_of__sowricear_72() { return static_cast<int32_t>(offsetof(M_rihoPole230_t2695664953, ____sowricear_72)); }
	inline float get__sowricear_72() const { return ____sowricear_72; }
	inline float* get_address_of__sowricear_72() { return &____sowricear_72; }
	inline void set__sowricear_72(float value)
	{
		____sowricear_72 = value;
	}

	inline static int32_t get_offset_of__virjou_73() { return static_cast<int32_t>(offsetof(M_rihoPole230_t2695664953, ____virjou_73)); }
	inline float get__virjou_73() const { return ____virjou_73; }
	inline float* get_address_of__virjou_73() { return &____virjou_73; }
	inline void set__virjou_73(float value)
	{
		____virjou_73 = value;
	}

	inline static int32_t get_offset_of__pairdereSaceral_74() { return static_cast<int32_t>(offsetof(M_rihoPole230_t2695664953, ____pairdereSaceral_74)); }
	inline float get__pairdereSaceral_74() const { return ____pairdereSaceral_74; }
	inline float* get_address_of__pairdereSaceral_74() { return &____pairdereSaceral_74; }
	inline void set__pairdereSaceral_74(float value)
	{
		____pairdereSaceral_74 = value;
	}

	inline static int32_t get_offset_of__masbur_75() { return static_cast<int32_t>(offsetof(M_rihoPole230_t2695664953, ____masbur_75)); }
	inline String_t* get__masbur_75() const { return ____masbur_75; }
	inline String_t** get_address_of__masbur_75() { return &____masbur_75; }
	inline void set__masbur_75(String_t* value)
	{
		____masbur_75 = value;
		Il2CppCodeGenWriteBarrier(&____masbur_75, value);
	}

	inline static int32_t get_offset_of__calllerki_76() { return static_cast<int32_t>(offsetof(M_rihoPole230_t2695664953, ____calllerki_76)); }
	inline uint32_t get__calllerki_76() const { return ____calllerki_76; }
	inline uint32_t* get_address_of__calllerki_76() { return &____calllerki_76; }
	inline void set__calllerki_76(uint32_t value)
	{
		____calllerki_76 = value;
	}

	inline static int32_t get_offset_of__nuroufaKelwheatur_77() { return static_cast<int32_t>(offsetof(M_rihoPole230_t2695664953, ____nuroufaKelwheatur_77)); }
	inline bool get__nuroufaKelwheatur_77() const { return ____nuroufaKelwheatur_77; }
	inline bool* get_address_of__nuroufaKelwheatur_77() { return &____nuroufaKelwheatur_77; }
	inline void set__nuroufaKelwheatur_77(bool value)
	{
		____nuroufaKelwheatur_77 = value;
	}

	inline static int32_t get_offset_of__nisbea_78() { return static_cast<int32_t>(offsetof(M_rihoPole230_t2695664953, ____nisbea_78)); }
	inline int32_t get__nisbea_78() const { return ____nisbea_78; }
	inline int32_t* get_address_of__nisbea_78() { return &____nisbea_78; }
	inline void set__nisbea_78(int32_t value)
	{
		____nisbea_78 = value;
	}

	inline static int32_t get_offset_of__cayloo_79() { return static_cast<int32_t>(offsetof(M_rihoPole230_t2695664953, ____cayloo_79)); }
	inline float get__cayloo_79() const { return ____cayloo_79; }
	inline float* get_address_of__cayloo_79() { return &____cayloo_79; }
	inline void set__cayloo_79(float value)
	{
		____cayloo_79 = value;
	}

	inline static int32_t get_offset_of__pousas_80() { return static_cast<int32_t>(offsetof(M_rihoPole230_t2695664953, ____pousas_80)); }
	inline int32_t get__pousas_80() const { return ____pousas_80; }
	inline int32_t* get_address_of__pousas_80() { return &____pousas_80; }
	inline void set__pousas_80(int32_t value)
	{
		____pousas_80 = value;
	}

	inline static int32_t get_offset_of__yajearjarRayzouso_81() { return static_cast<int32_t>(offsetof(M_rihoPole230_t2695664953, ____yajearjarRayzouso_81)); }
	inline bool get__yajearjarRayzouso_81() const { return ____yajearjarRayzouso_81; }
	inline bool* get_address_of__yajearjarRayzouso_81() { return &____yajearjarRayzouso_81; }
	inline void set__yajearjarRayzouso_81(bool value)
	{
		____yajearjarRayzouso_81 = value;
	}

	inline static int32_t get_offset_of__nayta_82() { return static_cast<int32_t>(offsetof(M_rihoPole230_t2695664953, ____nayta_82)); }
	inline uint32_t get__nayta_82() const { return ____nayta_82; }
	inline uint32_t* get_address_of__nayta_82() { return &____nayta_82; }
	inline void set__nayta_82(uint32_t value)
	{
		____nayta_82 = value;
	}

	inline static int32_t get_offset_of__saykavi_83() { return static_cast<int32_t>(offsetof(M_rihoPole230_t2695664953, ____saykavi_83)); }
	inline int32_t get__saykavi_83() const { return ____saykavi_83; }
	inline int32_t* get_address_of__saykavi_83() { return &____saykavi_83; }
	inline void set__saykavi_83(int32_t value)
	{
		____saykavi_83 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
