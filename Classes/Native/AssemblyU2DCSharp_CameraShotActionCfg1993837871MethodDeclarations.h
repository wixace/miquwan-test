﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraShotActionCfg
struct CameraShotActionCfg_t1993837871;
// ProtoBuf.IExtension
struct IExtension_t1606339106;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void CameraShotActionCfg::.ctor()
extern "C"  void CameraShotActionCfg__ctor_m972090140 (CameraShotActionCfg_t1993837871 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.IExtension CameraShotActionCfg::ProtoBuf.IExtensible.GetExtensionObject(System.Boolean)
extern "C"  Il2CppObject * CameraShotActionCfg_ProtoBuf_IExtensible_GetExtensionObject_m3872870304 (CameraShotActionCfg_t1993837871 * __this, bool ___createIfMissing0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CameraShotActionCfg::get_id()
extern "C"  int32_t CameraShotActionCfg_get_id_m693157662 (CameraShotActionCfg_t1993837871 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotActionCfg::set_id(System.Int32)
extern "C"  void CameraShotActionCfg_set_id_m1085586993 (CameraShotActionCfg_t1993837871 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CameraShotActionCfg::get_heroOrNpcId()
extern "C"  int32_t CameraShotActionCfg_get_heroOrNpcId_m2454679070 (CameraShotActionCfg_t1993837871 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotActionCfg::set_heroOrNpcId(System.Int32)
extern "C"  void CameraShotActionCfg_set_heroOrNpcId_m4117715437 (CameraShotActionCfg_t1993837871 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CameraShotActionCfg::get_isHero()
extern "C"  int32_t CameraShotActionCfg_get_isHero_m3659237127 (CameraShotActionCfg_t1993837871 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotActionCfg::set_isHero(System.Int32)
extern "C"  void CameraShotActionCfg_set_isHero_m3221798810 (CameraShotActionCfg_t1993837871 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String CameraShotActionCfg::get_actionName()
extern "C"  String_t* CameraShotActionCfg_get_actionName_m2936363375 (CameraShotActionCfg_t1993837871 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotActionCfg::set_actionName(System.String)
extern "C"  void CameraShotActionCfg_set_actionName_m1866841244 (CameraShotActionCfg_t1993837871 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CameraShotActionCfg::get_actionTime()
extern "C"  float CameraShotActionCfg_get_actionTime_m1758489640 (CameraShotActionCfg_t1993837871 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotActionCfg::set_actionTime(System.Single)
extern "C"  void CameraShotActionCfg_set_actionTime_m1389656771 (CameraShotActionCfg_t1993837871 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CameraShotActionCfg::get_isLoop()
extern "C"  int32_t CameraShotActionCfg_get_isLoop_m3782900529 (CameraShotActionCfg_t1993837871 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotActionCfg::set_isLoop(System.Int32)
extern "C"  void CameraShotActionCfg_set_isLoop_m2920858820 (CameraShotActionCfg_t1993837871 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.IExtension CameraShotActionCfg::ilo_GetExtensionObject1(ProtoBuf.IExtension&,System.Boolean)
extern "C"  Il2CppObject * CameraShotActionCfg_ilo_GetExtensionObject1_m2916441952 (Il2CppObject * __this /* static, unused */, Il2CppObject ** ___extensionObject0, bool ___createIfMissing1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
