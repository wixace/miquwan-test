﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._acd985249d98f31768c2074d66f94909
struct _acd985249d98f31768c2074d66f94909_t3362093557;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__acd985249d98f31768c2074d3362093557.h"

// System.Void Little._acd985249d98f31768c2074d66f94909::.ctor()
extern "C"  void _acd985249d98f31768c2074d66f94909__ctor_m364842968 (_acd985249d98f31768c2074d66f94909_t3362093557 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._acd985249d98f31768c2074d66f94909::_acd985249d98f31768c2074d66f94909m2(System.Int32)
extern "C"  int32_t _acd985249d98f31768c2074d66f94909__acd985249d98f31768c2074d66f94909m2_m3827410297 (_acd985249d98f31768c2074d66f94909_t3362093557 * __this, int32_t ____acd985249d98f31768c2074d66f94909a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._acd985249d98f31768c2074d66f94909::_acd985249d98f31768c2074d66f94909m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _acd985249d98f31768c2074d66f94909__acd985249d98f31768c2074d66f94909m_m2303139165 (_acd985249d98f31768c2074d66f94909_t3362093557 * __this, int32_t ____acd985249d98f31768c2074d66f94909a0, int32_t ____acd985249d98f31768c2074d66f94909171, int32_t ____acd985249d98f31768c2074d66f94909c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._acd985249d98f31768c2074d66f94909::ilo__acd985249d98f31768c2074d66f94909m21(Little._acd985249d98f31768c2074d66f94909,System.Int32)
extern "C"  int32_t _acd985249d98f31768c2074d66f94909_ilo__acd985249d98f31768c2074d66f94909m21_m2980429148 (Il2CppObject * __this /* static, unused */, _acd985249d98f31768c2074d66f94909_t3362093557 * ____this0, int32_t ____acd985249d98f31768c2074d66f94909a1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
