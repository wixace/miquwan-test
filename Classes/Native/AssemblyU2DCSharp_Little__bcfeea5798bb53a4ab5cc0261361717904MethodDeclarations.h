﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._bcfeea5798bb53a4ab5cc0264fe1ab8b
struct _bcfeea5798bb53a4ab5cc0264fe1ab8b_t1361717904;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__bcfeea5798bb53a4ab5cc0261361717904.h"

// System.Void Little._bcfeea5798bb53a4ab5cc0264fe1ab8b::.ctor()
extern "C"  void _bcfeea5798bb53a4ab5cc0264fe1ab8b__ctor_m2411191709 (_bcfeea5798bb53a4ab5cc0264fe1ab8b_t1361717904 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._bcfeea5798bb53a4ab5cc0264fe1ab8b::_bcfeea5798bb53a4ab5cc0264fe1ab8bm2(System.Int32)
extern "C"  int32_t _bcfeea5798bb53a4ab5cc0264fe1ab8b__bcfeea5798bb53a4ab5cc0264fe1ab8bm2_m1499629465 (_bcfeea5798bb53a4ab5cc0264fe1ab8b_t1361717904 * __this, int32_t ____bcfeea5798bb53a4ab5cc0264fe1ab8ba0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._bcfeea5798bb53a4ab5cc0264fe1ab8b::_bcfeea5798bb53a4ab5cc0264fe1ab8bm(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _bcfeea5798bb53a4ab5cc0264fe1ab8b__bcfeea5798bb53a4ab5cc0264fe1ab8bm_m2459098941 (_bcfeea5798bb53a4ab5cc0264fe1ab8b_t1361717904 * __this, int32_t ____bcfeea5798bb53a4ab5cc0264fe1ab8ba0, int32_t ____bcfeea5798bb53a4ab5cc0264fe1ab8b921, int32_t ____bcfeea5798bb53a4ab5cc0264fe1ab8bc2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._bcfeea5798bb53a4ab5cc0264fe1ab8b::ilo__bcfeea5798bb53a4ab5cc0264fe1ab8bm21(Little._bcfeea5798bb53a4ab5cc0264fe1ab8b,System.Int32)
extern "C"  int32_t _bcfeea5798bb53a4ab5cc0264fe1ab8b_ilo__bcfeea5798bb53a4ab5cc0264fe1ab8bm21_m413127255 (Il2CppObject * __this /* static, unused */, _bcfeea5798bb53a4ab5cc0264fe1ab8b_t1361717904 * ____this0, int32_t ____bcfeea5798bb53a4ab5cc0264fe1ab8ba1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
