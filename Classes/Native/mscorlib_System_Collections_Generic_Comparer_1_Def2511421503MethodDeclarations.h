﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.RaycastHit>
struct DefaultComparer_t2511421503;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RaycastHit4003175726.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.RaycastHit>::.ctor()
extern "C"  void DefaultComparer__ctor_m1164510631_gshared (DefaultComparer_t2511421503 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m1164510631(__this, method) ((  void (*) (DefaultComparer_t2511421503 *, const MethodInfo*))DefaultComparer__ctor_m1164510631_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.RaycastHit>::Compare(T,T)
extern "C"  int32_t DefaultComparer_Compare_m2611526704_gshared (DefaultComparer_t2511421503 * __this, RaycastHit_t4003175726  ___x0, RaycastHit_t4003175726  ___y1, const MethodInfo* method);
#define DefaultComparer_Compare_m2611526704(__this, ___x0, ___y1, method) ((  int32_t (*) (DefaultComparer_t2511421503 *, RaycastHit_t4003175726 , RaycastHit_t4003175726 , const MethodInfo*))DefaultComparer_Compare_m2611526704_gshared)(__this, ___x0, ___y1, method)
