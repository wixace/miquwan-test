﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.ParticleSystemRenderer
struct ParticleSystemRenderer_t1034709628;
// UnityEngine.Mesh
struct Mesh_t4241756145;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_ParticleSystemRenderMode536392466.h"
#include "UnityEngine_UnityEngine_ParticleSystemRenderSpace1011934321.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_ParticleSystemSortMode3191197210.h"
#include "UnityEngine_UnityEngine_Mesh4241756145.h"

// System.Void UnityEngine.ParticleSystemRenderer::.ctor()
extern "C"  void ParticleSystemRenderer__ctor_m4007229109 (ParticleSystemRenderer_t1034709628 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ParticleSystemRenderMode UnityEngine.ParticleSystemRenderer::get_renderMode()
extern "C"  int32_t ParticleSystemRenderer_get_renderMode_m2571005499 (ParticleSystemRenderer_t1034709628 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystemRenderer::set_renderMode(UnityEngine.ParticleSystemRenderMode)
extern "C"  void ParticleSystemRenderer_set_renderMode_m51581184 (ParticleSystemRenderer_t1034709628 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.ParticleSystemRenderer::get_lengthScale()
extern "C"  float ParticleSystemRenderer_get_lengthScale_m1058390466 (ParticleSystemRenderer_t1034709628 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystemRenderer::set_lengthScale(System.Single)
extern "C"  void ParticleSystemRenderer_set_lengthScale_m2846211729 (ParticleSystemRenderer_t1034709628 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.ParticleSystemRenderer::get_velocityScale()
extern "C"  float ParticleSystemRenderer_get_velocityScale_m3674206379 (ParticleSystemRenderer_t1034709628 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystemRenderer::set_velocityScale(System.Single)
extern "C"  void ParticleSystemRenderer_set_velocityScale_m777754696 (ParticleSystemRenderer_t1034709628 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.ParticleSystemRenderer::get_cameraVelocityScale()
extern "C"  float ParticleSystemRenderer_get_cameraVelocityScale_m2115208454 (ParticleSystemRenderer_t1034709628 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystemRenderer::set_cameraVelocityScale(System.Single)
extern "C"  void ParticleSystemRenderer_set_cameraVelocityScale_m1230012877 (ParticleSystemRenderer_t1034709628 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.ParticleSystemRenderer::get_normalDirection()
extern "C"  float ParticleSystemRenderer_get_normalDirection_m50468502 (ParticleSystemRenderer_t1034709628 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystemRenderer::set_normalDirection(System.Single)
extern "C"  void ParticleSystemRenderer_set_normalDirection_m3599035965 (ParticleSystemRenderer_t1034709628 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ParticleSystemRenderSpace UnityEngine.ParticleSystemRenderer::get_alignment()
extern "C"  int32_t ParticleSystemRenderer_get_alignment_m1274463710 (ParticleSystemRenderer_t1034709628 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystemRenderer::set_alignment(UnityEngine.ParticleSystemRenderSpace)
extern "C"  void ParticleSystemRenderer_set_alignment_m3738230773 (ParticleSystemRenderer_t1034709628 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.ParticleSystemRenderer::get_pivot()
extern "C"  Vector3_t4282066566  ParticleSystemRenderer_get_pivot_m2237027602 (ParticleSystemRenderer_t1034709628 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystemRenderer::set_pivot(UnityEngine.Vector3)
extern "C"  void ParticleSystemRenderer_set_pivot_m2573242625 (ParticleSystemRenderer_t1034709628 * __this, Vector3_t4282066566  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystemRenderer::INTERNAL_get_pivot(UnityEngine.Vector3&)
extern "C"  void ParticleSystemRenderer_INTERNAL_get_pivot_m1712537785 (ParticleSystemRenderer_t1034709628 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystemRenderer::INTERNAL_set_pivot(UnityEngine.Vector3&)
extern "C"  void ParticleSystemRenderer_INTERNAL_set_pivot_m1627169733 (ParticleSystemRenderer_t1034709628 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ParticleSystemSortMode UnityEngine.ParticleSystemRenderer::get_sortMode()
extern "C"  int32_t ParticleSystemRenderer_get_sortMode_m4216839163 (ParticleSystemRenderer_t1034709628 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystemRenderer::set_sortMode(UnityEngine.ParticleSystemSortMode)
extern "C"  void ParticleSystemRenderer_set_sortMode_m280971456 (ParticleSystemRenderer_t1034709628 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.ParticleSystemRenderer::get_sortingFudge()
extern "C"  float ParticleSystemRenderer_get_sortingFudge_m198994707 (ParticleSystemRenderer_t1034709628 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystemRenderer::set_sortingFudge(System.Single)
extern "C"  void ParticleSystemRenderer_set_sortingFudge_m4230006544 (ParticleSystemRenderer_t1034709628 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.ParticleSystemRenderer::get_minParticleSize()
extern "C"  float ParticleSystemRenderer_get_minParticleSize_m3708947223 (ParticleSystemRenderer_t1034709628 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystemRenderer::set_minParticleSize(System.Single)
extern "C"  void ParticleSystemRenderer_set_minParticleSize_m1103369052 (ParticleSystemRenderer_t1034709628 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.ParticleSystemRenderer::get_maxParticleSize()
extern "C"  float ParticleSystemRenderer_get_maxParticleSize_m1393902249 (ParticleSystemRenderer_t1034709628 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystemRenderer::set_maxParticleSize(System.Single)
extern "C"  void ParticleSystemRenderer_set_maxParticleSize_m1601124618 (ParticleSystemRenderer_t1034709628 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Mesh UnityEngine.ParticleSystemRenderer::get_mesh()
extern "C"  Mesh_t4241756145 * ParticleSystemRenderer_get_mesh_m2245291984 (ParticleSystemRenderer_t1034709628 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystemRenderer::set_mesh(UnityEngine.Mesh)
extern "C"  void ParticleSystemRenderer_set_mesh_m2214948565 (ParticleSystemRenderer_t1034709628 * __this, Mesh_t4241756145 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
