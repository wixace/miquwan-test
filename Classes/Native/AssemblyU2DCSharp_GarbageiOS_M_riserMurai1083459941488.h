﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_riserMurai108
struct  M_riserMurai108_t3459941488  : public Il2CppObject
{
public:
	// System.String GarbageiOS.M_riserMurai108::_foupisso
	String_t* ____foupisso_0;
	// System.Boolean GarbageiOS.M_riserMurai108::_droler
	bool ____droler_1;
	// System.UInt32 GarbageiOS.M_riserMurai108::_cafaLowfekou
	uint32_t ____cafaLowfekou_2;
	// System.Int32 GarbageiOS.M_riserMurai108::_trairfirce
	int32_t ____trairfirce_3;
	// System.Single GarbageiOS.M_riserMurai108::_neardir
	float ____neardir_4;
	// System.Int32 GarbageiOS.M_riserMurai108::_kairberveTaper
	int32_t ____kairberveTaper_5;
	// System.String GarbageiOS.M_riserMurai108::_xidelow
	String_t* ____xidelow_6;
	// System.UInt32 GarbageiOS.M_riserMurai108::_rallxerSomawdal
	uint32_t ____rallxerSomawdal_7;
	// System.Boolean GarbageiOS.M_riserMurai108::_rerrestee
	bool ____rerrestee_8;
	// System.Boolean GarbageiOS.M_riserMurai108::_subasDeni
	bool ____subasDeni_9;
	// System.Int32 GarbageiOS.M_riserMurai108::_whorrasas
	int32_t ____whorrasas_10;
	// System.UInt32 GarbageiOS.M_riserMurai108::_rorjiqorReseacai
	uint32_t ____rorjiqorReseacai_11;
	// System.Int32 GarbageiOS.M_riserMurai108::_douzowsiJawtror
	int32_t ____douzowsiJawtror_12;
	// System.UInt32 GarbageiOS.M_riserMurai108::_neka
	uint32_t ____neka_13;
	// System.UInt32 GarbageiOS.M_riserMurai108::_nearitrow
	uint32_t ____nearitrow_14;
	// System.Int32 GarbageiOS.M_riserMurai108::_whebasci
	int32_t ____whebasci_15;

public:
	inline static int32_t get_offset_of__foupisso_0() { return static_cast<int32_t>(offsetof(M_riserMurai108_t3459941488, ____foupisso_0)); }
	inline String_t* get__foupisso_0() const { return ____foupisso_0; }
	inline String_t** get_address_of__foupisso_0() { return &____foupisso_0; }
	inline void set__foupisso_0(String_t* value)
	{
		____foupisso_0 = value;
		Il2CppCodeGenWriteBarrier(&____foupisso_0, value);
	}

	inline static int32_t get_offset_of__droler_1() { return static_cast<int32_t>(offsetof(M_riserMurai108_t3459941488, ____droler_1)); }
	inline bool get__droler_1() const { return ____droler_1; }
	inline bool* get_address_of__droler_1() { return &____droler_1; }
	inline void set__droler_1(bool value)
	{
		____droler_1 = value;
	}

	inline static int32_t get_offset_of__cafaLowfekou_2() { return static_cast<int32_t>(offsetof(M_riserMurai108_t3459941488, ____cafaLowfekou_2)); }
	inline uint32_t get__cafaLowfekou_2() const { return ____cafaLowfekou_2; }
	inline uint32_t* get_address_of__cafaLowfekou_2() { return &____cafaLowfekou_2; }
	inline void set__cafaLowfekou_2(uint32_t value)
	{
		____cafaLowfekou_2 = value;
	}

	inline static int32_t get_offset_of__trairfirce_3() { return static_cast<int32_t>(offsetof(M_riserMurai108_t3459941488, ____trairfirce_3)); }
	inline int32_t get__trairfirce_3() const { return ____trairfirce_3; }
	inline int32_t* get_address_of__trairfirce_3() { return &____trairfirce_3; }
	inline void set__trairfirce_3(int32_t value)
	{
		____trairfirce_3 = value;
	}

	inline static int32_t get_offset_of__neardir_4() { return static_cast<int32_t>(offsetof(M_riserMurai108_t3459941488, ____neardir_4)); }
	inline float get__neardir_4() const { return ____neardir_4; }
	inline float* get_address_of__neardir_4() { return &____neardir_4; }
	inline void set__neardir_4(float value)
	{
		____neardir_4 = value;
	}

	inline static int32_t get_offset_of__kairberveTaper_5() { return static_cast<int32_t>(offsetof(M_riserMurai108_t3459941488, ____kairberveTaper_5)); }
	inline int32_t get__kairberveTaper_5() const { return ____kairberveTaper_5; }
	inline int32_t* get_address_of__kairberveTaper_5() { return &____kairberveTaper_5; }
	inline void set__kairberveTaper_5(int32_t value)
	{
		____kairberveTaper_5 = value;
	}

	inline static int32_t get_offset_of__xidelow_6() { return static_cast<int32_t>(offsetof(M_riserMurai108_t3459941488, ____xidelow_6)); }
	inline String_t* get__xidelow_6() const { return ____xidelow_6; }
	inline String_t** get_address_of__xidelow_6() { return &____xidelow_6; }
	inline void set__xidelow_6(String_t* value)
	{
		____xidelow_6 = value;
		Il2CppCodeGenWriteBarrier(&____xidelow_6, value);
	}

	inline static int32_t get_offset_of__rallxerSomawdal_7() { return static_cast<int32_t>(offsetof(M_riserMurai108_t3459941488, ____rallxerSomawdal_7)); }
	inline uint32_t get__rallxerSomawdal_7() const { return ____rallxerSomawdal_7; }
	inline uint32_t* get_address_of__rallxerSomawdal_7() { return &____rallxerSomawdal_7; }
	inline void set__rallxerSomawdal_7(uint32_t value)
	{
		____rallxerSomawdal_7 = value;
	}

	inline static int32_t get_offset_of__rerrestee_8() { return static_cast<int32_t>(offsetof(M_riserMurai108_t3459941488, ____rerrestee_8)); }
	inline bool get__rerrestee_8() const { return ____rerrestee_8; }
	inline bool* get_address_of__rerrestee_8() { return &____rerrestee_8; }
	inline void set__rerrestee_8(bool value)
	{
		____rerrestee_8 = value;
	}

	inline static int32_t get_offset_of__subasDeni_9() { return static_cast<int32_t>(offsetof(M_riserMurai108_t3459941488, ____subasDeni_9)); }
	inline bool get__subasDeni_9() const { return ____subasDeni_9; }
	inline bool* get_address_of__subasDeni_9() { return &____subasDeni_9; }
	inline void set__subasDeni_9(bool value)
	{
		____subasDeni_9 = value;
	}

	inline static int32_t get_offset_of__whorrasas_10() { return static_cast<int32_t>(offsetof(M_riserMurai108_t3459941488, ____whorrasas_10)); }
	inline int32_t get__whorrasas_10() const { return ____whorrasas_10; }
	inline int32_t* get_address_of__whorrasas_10() { return &____whorrasas_10; }
	inline void set__whorrasas_10(int32_t value)
	{
		____whorrasas_10 = value;
	}

	inline static int32_t get_offset_of__rorjiqorReseacai_11() { return static_cast<int32_t>(offsetof(M_riserMurai108_t3459941488, ____rorjiqorReseacai_11)); }
	inline uint32_t get__rorjiqorReseacai_11() const { return ____rorjiqorReseacai_11; }
	inline uint32_t* get_address_of__rorjiqorReseacai_11() { return &____rorjiqorReseacai_11; }
	inline void set__rorjiqorReseacai_11(uint32_t value)
	{
		____rorjiqorReseacai_11 = value;
	}

	inline static int32_t get_offset_of__douzowsiJawtror_12() { return static_cast<int32_t>(offsetof(M_riserMurai108_t3459941488, ____douzowsiJawtror_12)); }
	inline int32_t get__douzowsiJawtror_12() const { return ____douzowsiJawtror_12; }
	inline int32_t* get_address_of__douzowsiJawtror_12() { return &____douzowsiJawtror_12; }
	inline void set__douzowsiJawtror_12(int32_t value)
	{
		____douzowsiJawtror_12 = value;
	}

	inline static int32_t get_offset_of__neka_13() { return static_cast<int32_t>(offsetof(M_riserMurai108_t3459941488, ____neka_13)); }
	inline uint32_t get__neka_13() const { return ____neka_13; }
	inline uint32_t* get_address_of__neka_13() { return &____neka_13; }
	inline void set__neka_13(uint32_t value)
	{
		____neka_13 = value;
	}

	inline static int32_t get_offset_of__nearitrow_14() { return static_cast<int32_t>(offsetof(M_riserMurai108_t3459941488, ____nearitrow_14)); }
	inline uint32_t get__nearitrow_14() const { return ____nearitrow_14; }
	inline uint32_t* get_address_of__nearitrow_14() { return &____nearitrow_14; }
	inline void set__nearitrow_14(uint32_t value)
	{
		____nearitrow_14 = value;
	}

	inline static int32_t get_offset_of__whebasci_15() { return static_cast<int32_t>(offsetof(M_riserMurai108_t3459941488, ____whebasci_15)); }
	inline int32_t get__whebasci_15() const { return ____whebasci_15; }
	inline int32_t* get_address_of__whebasci_15() { return &____whebasci_15; }
	inline void set__whebasci_15(int32_t value)
	{
		____whebasci_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
