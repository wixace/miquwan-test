﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._73db4f97c2fd7e8b51e95f30f3bf841d
struct _73db4f97c2fd7e8b51e95f30f3bf841d_t1627998150;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._73db4f97c2fd7e8b51e95f30f3bf841d::.ctor()
extern "C"  void _73db4f97c2fd7e8b51e95f30f3bf841d__ctor_m3288808743 (_73db4f97c2fd7e8b51e95f30f3bf841d_t1627998150 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._73db4f97c2fd7e8b51e95f30f3bf841d::_73db4f97c2fd7e8b51e95f30f3bf841dm2(System.Int32)
extern "C"  int32_t _73db4f97c2fd7e8b51e95f30f3bf841d__73db4f97c2fd7e8b51e95f30f3bf841dm2_m1277873113 (_73db4f97c2fd7e8b51e95f30f3bf841d_t1627998150 * __this, int32_t ____73db4f97c2fd7e8b51e95f30f3bf841da0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._73db4f97c2fd7e8b51e95f30f3bf841d::_73db4f97c2fd7e8b51e95f30f3bf841dm(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _73db4f97c2fd7e8b51e95f30f3bf841d__73db4f97c2fd7e8b51e95f30f3bf841dm_m20177661 (_73db4f97c2fd7e8b51e95f30f3bf841d_t1627998150 * __this, int32_t ____73db4f97c2fd7e8b51e95f30f3bf841da0, int32_t ____73db4f97c2fd7e8b51e95f30f3bf841d441, int32_t ____73db4f97c2fd7e8b51e95f30f3bf841dc2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
