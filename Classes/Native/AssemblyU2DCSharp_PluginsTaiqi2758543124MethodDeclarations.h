﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PluginsTaiqi
struct PluginsTaiqi_t2758543124;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// System.String
struct String_t;
// CEvent.EventFunc`1<CEvent.ZEvent>
struct EventFunc_1_t1114914310;
// VersionMgr
struct VersionMgr_t1322950208;
// PluginsSdkMgr
struct PluginsSdkMgr_t3884624670;
// PluginsTaiqi/LoginResult
struct LoginResult_t2998189643;
// System.Object
struct Il2CppObject;
// FloatTextMgr
struct FloatTextMgr_t630384591;
// System.Action
struct Action_t3771233898;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_VersionMgr1322950208.h"
#include "AssemblyU2DCSharp_PluginsTaiqi_LoginResult2998189643.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_PluginsTaiqi2758543124.h"
#include "AssemblyU2DCSharp_PluginsSdkMgr3884624670.h"
#include "System_Core_System_Action3771233898.h"

// System.Void PluginsTaiqi::.ctor()
extern "C"  void PluginsTaiqi__ctor_m883991047 (PluginsTaiqi_t2758543124 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsTaiqi::Init()
extern "C"  void PluginsTaiqi_Init_m4136472429 (PluginsTaiqi_t2758543124 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginsTaiqi::ReqSDKHttpLogin()
extern "C"  Dictionary_2_t827649927 * PluginsTaiqi_ReqSDKHttpLogin_m2012200348 (PluginsTaiqi_t2758543124 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginsTaiqi::IsLoginSuccess(System.Boolean)
extern "C"  bool PluginsTaiqi_IsLoginSuccess_m3624568172 (PluginsTaiqi_t2758543124 * __this, bool ___isOpenUseLogin0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsTaiqi::OpenUserLogin()
extern "C"  void PluginsTaiqi_OpenUserLogin_m445042009 (PluginsTaiqi_t2758543124 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsTaiqi::RoleEnterGame(CEvent.ZEvent)
extern "C"  void PluginsTaiqi_RoleEnterGame_m1040518114 (PluginsTaiqi_t2758543124 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsTaiqi::UserPay(CEvent.ZEvent)
extern "C"  void PluginsTaiqi_UserPay_m1716830201 (PluginsTaiqi_t2758543124 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsTaiqi::RecRoleUpLv(CEvent.ZEvent)
extern "C"  void PluginsTaiqi_RecRoleUpLv_m2026944843 (PluginsTaiqi_t2758543124 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsTaiqi::RecGotoGame(CEvent.ZEvent)
extern "C"  void PluginsTaiqi_RecGotoGame_m1629583665 (PluginsTaiqi_t2758543124 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsTaiqi::RecCreateRole(CEvent.ZEvent)
extern "C"  void PluginsTaiqi_RecCreateRole_m1439191284 (PluginsTaiqi_t2758543124 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsTaiqi::SumitChannelData(System.Object[])
extern "C"  void PluginsTaiqi_SumitChannelData_m1633006046 (PluginsTaiqi_t2758543124 * __this, ObjectU5BU5D_t1108656482* ___datas0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsTaiqi::OnInitSuccess(System.String)
extern "C"  void PluginsTaiqi_OnInitSuccess_m3680387081 (PluginsTaiqi_t2758543124 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsTaiqi::OnInitFail(System.String)
extern "C"  void PluginsTaiqi_OnInitFail_m575231448 (PluginsTaiqi_t2758543124 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsTaiqi::OnLoginSuccess(System.String)
extern "C"  void PluginsTaiqi_OnLoginSuccess_m2812561868 (PluginsTaiqi_t2758543124 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsTaiqi::OnLoginFail(System.String)
extern "C"  void PluginsTaiqi_OnLoginFail_m3577109237 (PluginsTaiqi_t2758543124 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsTaiqi::OnUserSwitchSuccess(System.String)
extern "C"  void PluginsTaiqi_OnUserSwitchSuccess_m1346269272 (PluginsTaiqi_t2758543124 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsTaiqi::OnUserSwitchFail(System.String)
extern "C"  void PluginsTaiqi_OnUserSwitchFail_m2738135017 (PluginsTaiqi_t2758543124 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsTaiqi::OnPaySuccess(System.String)
extern "C"  void PluginsTaiqi_OnPaySuccess_m3731533579 (PluginsTaiqi_t2758543124 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsTaiqi::OnPayFail(System.String)
extern "C"  void PluginsTaiqi_OnPayFail_m1061230102 (PluginsTaiqi_t2758543124 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsTaiqi::OnPayCancel(System.String)
extern "C"  void PluginsTaiqi_OnPayCancel_m4078735994 (PluginsTaiqi_t2758543124 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsTaiqi::OnExitGameSuccess(System.String)
extern "C"  void PluginsTaiqi_OnExitGameSuccess_m3778525097 (PluginsTaiqi_t2758543124 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsTaiqi::OnExitGameFail(System.String)
extern "C"  void PluginsTaiqi_OnExitGameFail_m3414950456 (PluginsTaiqi_t2758543124 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsTaiqi::OnLogoutSuccess(System.String)
extern "C"  void PluginsTaiqi_OnLogoutSuccess_m1293614659 (PluginsTaiqi_t2758543124 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsTaiqi::OnLogoutFail(System.String)
extern "C"  void PluginsTaiqi_OnLogoutFail_m3329085918 (PluginsTaiqi_t2758543124 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsTaiqi::setLogOut(System.String)
extern "C"  void PluginsTaiqi_setLogOut_m1860472817 (PluginsTaiqi_t2758543124 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsTaiqi::setLogIn(System.String)
extern "C"  void PluginsTaiqi_setLogIn_m4011541182 (PluginsTaiqi_t2758543124 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsTaiqi::<OnLogoutSuccess>m__486()
extern "C"  void PluginsTaiqi_U3COnLogoutSuccessU3Em__486_m2589330282 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsTaiqi::<setLogOut>m__487()
extern "C"  void PluginsTaiqi_U3CsetLogOutU3Em__487_m3959012953 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsTaiqi::ilo_AddEventListener1(System.String,CEvent.EventFunc`1<CEvent.ZEvent>)
extern "C"  void PluginsTaiqi_ilo_AddEventListener1_m2310941197 (Il2CppObject * __this /* static, unused */, String_t* ___eventType0, EventFunc_1_t1114914310 * ___fun1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginsTaiqi::ilo_get_isSdkLogin2(VersionMgr)
extern "C"  bool PluginsTaiqi_ilo_get_isSdkLogin2_m230299662 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PluginsSdkMgr PluginsTaiqi::ilo_get_PluginsSdkMgr3()
extern "C"  PluginsSdkMgr_t3884624670 * PluginsTaiqi_ilo_get_PluginsSdkMgr3_m1434090755 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PluginsTaiqi::ilo_getChannelAction4(PluginsTaiqi/LoginResult)
extern "C"  String_t* PluginsTaiqi_ilo_getChannelAction4_m3575321333 (Il2CppObject * __this /* static, unused */, LoginResult_t2998189643 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionMgr PluginsTaiqi::ilo_get_Instance5()
extern "C"  VersionMgr_t1322950208 * PluginsTaiqi_ilo_get_Instance5_m3977954252 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PluginsTaiqi::ilo_getToken6(PluginsTaiqi/LoginResult)
extern "C"  String_t* PluginsTaiqi_ilo_getToken6_m2955832215 (Il2CppObject * __this /* static, unused */, LoginResult_t2998189643 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsTaiqi::ilo_Log7(System.Object,System.Boolean)
extern "C"  void PluginsTaiqi_ilo_Log7_m38270444 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsTaiqi::ilo_SumitChannelData8(PluginsTaiqi,System.Object[])
extern "C"  void PluginsTaiqi_ilo_SumitChannelData8_m3015631115 (Il2CppObject * __this /* static, unused */, PluginsTaiqi_t2758543124 * ____this0, ObjectU5BU5D_t1108656482* ___datas1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsTaiqi::ilo_ReqSDKHttpLogin9(PluginsSdkMgr)
extern "C"  void PluginsTaiqi_ilo_ReqSDKHttpLogin9_m982918032 (Il2CppObject * __this /* static, unused */, PluginsSdkMgr_t3884624670 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsTaiqi::ilo_Clear10(PluginsTaiqi/LoginResult)
extern "C"  void PluginsTaiqi_ilo_Clear10_m4031255955 (Il2CppObject * __this /* static, unused */, LoginResult_t2998189643 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FloatTextMgr PluginsTaiqi::ilo_get_FloatTextMgr11()
extern "C"  FloatTextMgr_t630384591 * PluginsTaiqi_ilo_get_FloatTextMgr11_m3797552118 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsTaiqi::ilo_Logout12(System.Action)
extern "C"  void PluginsTaiqi_ilo_Logout12_m2709995042 (Il2CppObject * __this /* static, unused */, Action_t3771233898 * ___call0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
