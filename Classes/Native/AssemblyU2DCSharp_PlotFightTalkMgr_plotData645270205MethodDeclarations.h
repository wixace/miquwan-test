﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlotFightTalkMgr/plotData
struct plotData_t645270205;
// StoryCfg
struct StoryCfg_t1782371151;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_StoryCfg1782371151.h"
#include "AssemblyU2DCSharp_PlotFightTalkMgr_TermType1548552312.h"

// System.Void PlotFightTalkMgr/plotData::.ctor(StoryCfg)
extern "C"  void plotData__ctor_m2553585887 (plotData_t645270205 * __this, StoryCfg_t1782371151 * ___cfg0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlotFightTalkMgr/plotData::get_isTerm()
extern "C"  bool plotData_get_isTerm_m67713313 (plotData_t645270205 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlotFightTalkMgr/plotData::get_isDeath()
extern "C"  bool plotData_get_isDeath_m768525889 (plotData_t645270205 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlotFightTalkMgr/plotData::set_curCount(System.Int32)
extern "C"  void plotData_set_curCount_m231291987 (plotData_t645270205 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PlotFightTalkMgr/plotData::get_curCount()
extern "C"  int32_t plotData_get_curCount_m4104850496 (plotData_t645270205 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlotFightTalkMgr/plotData::Bind(PlotFightTalkMgr/TermType)
extern "C"  void plotData_Bind_m526471399 (plotData_t645270205 * __this, int32_t ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlotFightTalkMgr/plotData::UnBind(PlotFightTalkMgr/TermType)
extern "C"  void plotData_UnBind_m2394077262 (plotData_t645270205 * __this, int32_t ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
