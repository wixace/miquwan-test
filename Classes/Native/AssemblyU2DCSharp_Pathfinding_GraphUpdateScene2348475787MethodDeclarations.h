﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.GraphUpdateScene
struct GraphUpdateScene_t2348475787;
// AstarPath
struct AstarPath_t4090270936;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t215400611;
// Pathfinding.GraphUpdateShape
struct GraphUpdateShape_t2348620960;
// Pathfinding.GraphUpdateObject
struct GraphUpdateObject_t430843704;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_AstarPath4090270936.h"
#include "UnityEngine_UnityEngine_Bounds2711641849.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphUpdateScene2348475787.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphUpdateShape2348620960.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphUpdateObject430843704.h"

// System.Void Pathfinding.GraphUpdateScene::.ctor()
extern "C"  void GraphUpdateScene__ctor_m2086166844 (GraphUpdateScene_t2348475787 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GraphUpdateScene::Start()
extern "C"  void GraphUpdateScene_Start_m1033304636 (GraphUpdateScene_t2348475787 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GraphUpdateScene::OnPostScan()
extern "C"  void GraphUpdateScene_OnPostScan_m3658234500 (GraphUpdateScene_t2348475787 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GraphUpdateScene::InvertSettings()
extern "C"  void GraphUpdateScene_InvertSettings_m2682298273 (GraphUpdateScene_t2348475787 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GraphUpdateScene::RecalcConvex()
extern "C"  void GraphUpdateScene_RecalcConvex_m3313136087 (GraphUpdateScene_t2348475787 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GraphUpdateScene::ToggleUseWorldSpace()
extern "C"  void GraphUpdateScene_ToggleUseWorldSpace_m944169825 (GraphUpdateScene_t2348475787 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GraphUpdateScene::LockToY()
extern "C"  void GraphUpdateScene_LockToY_m2058685293 (GraphUpdateScene_t2348475787 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GraphUpdateScene::Apply(AstarPath)
extern "C"  void GraphUpdateScene_Apply_m2458997522 (GraphUpdateScene_t2348475787 * __this, AstarPath_t4090270936 * ___active0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Bounds Pathfinding.GraphUpdateScene::GetBounds()
extern "C"  Bounds_t2711641849  GraphUpdateScene_GetBounds_m2446071444 (GraphUpdateScene_t2348475787 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GraphUpdateScene::Apply()
extern "C"  void GraphUpdateScene_Apply_m2137269832 (GraphUpdateScene_t2348475787 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GraphUpdateScene::OnDrawGizmos()
extern "C"  void GraphUpdateScene_OnDrawGizmos_m58266724 (GraphUpdateScene_t2348475787 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GraphUpdateScene::OnDrawGizmosSelected()
extern "C"  void GraphUpdateScene_OnDrawGizmosSelected_m3885405535 (GraphUpdateScene_t2348475787 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GraphUpdateScene::OnDrawGizmos(System.Boolean)
extern "C"  void GraphUpdateScene_OnDrawGizmos_m1008880347 (GraphUpdateScene_t2348475787 * __this, bool ___selected0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GraphUpdateScene::ilo_Apply1(Pathfinding.GraphUpdateScene)
extern "C"  void GraphUpdateScene_ilo_Apply1_m2167513695 (Il2CppObject * __this /* static, unused */, GraphUpdateScene_t2348475787 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3[] Pathfinding.GraphUpdateScene::ilo_ConvexHull2(UnityEngine.Vector3[])
extern "C"  Vector3U5BU5D_t215400611* GraphUpdateScene_ilo_ConvexHull2_m1580576650 (Il2CppObject * __this /* static, unused */, Vector3U5BU5D_t215400611* ___points0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GraphUpdateScene::ilo_set_convex3(Pathfinding.GraphUpdateShape,System.Boolean)
extern "C"  void GraphUpdateScene_ilo_set_convex3_m2032014875 (Il2CppObject * __this /* static, unused */, GraphUpdateShape_t2348620960 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GraphUpdateScene::ilo_UpdateGraphs4(AstarPath,Pathfinding.GraphUpdateObject)
extern "C"  void GraphUpdateScene_ilo_UpdateGraphs4_m3772192525 (Il2CppObject * __this /* static, unused */, AstarPath_t4090270936 * ____this0, GraphUpdateObject_t430843704 * ___ob1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GraphUpdateScene::ilo_OnDrawGizmos5(Pathfinding.GraphUpdateScene,System.Boolean)
extern "C"  void GraphUpdateScene_ilo_OnDrawGizmos5_m4016935004 (Il2CppObject * __this /* static, unused */, GraphUpdateScene_t2348475787 * ____this0, bool ___selected1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GraphUpdateScene::ilo_RecalcConvex6(Pathfinding.GraphUpdateScene)
extern "C"  void GraphUpdateScene_ilo_RecalcConvex6_m576988335 (Il2CppObject * __this /* static, unused */, GraphUpdateScene_t2348475787 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
