﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// CameraAniCfgMgr
struct CameraAniCfgMgr_t11764299;
// System.Collections.Generic.Dictionary`2<System.Int32,CameraAniMap>
struct Dictionary_2_t3001919244;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraAniCfgMgr
struct  CameraAniCfgMgr_t11764299  : public Il2CppObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.Int32,CameraAniMap> CameraAniCfgMgr::cameraAniMaps
	Dictionary_2_t3001919244 * ___cameraAniMaps_1;

public:
	inline static int32_t get_offset_of_cameraAniMaps_1() { return static_cast<int32_t>(offsetof(CameraAniCfgMgr_t11764299, ___cameraAniMaps_1)); }
	inline Dictionary_2_t3001919244 * get_cameraAniMaps_1() const { return ___cameraAniMaps_1; }
	inline Dictionary_2_t3001919244 ** get_address_of_cameraAniMaps_1() { return &___cameraAniMaps_1; }
	inline void set_cameraAniMaps_1(Dictionary_2_t3001919244 * value)
	{
		___cameraAniMaps_1 = value;
		Il2CppCodeGenWriteBarrier(&___cameraAniMaps_1, value);
	}
};

struct CameraAniCfgMgr_t11764299_StaticFields
{
public:
	// CameraAniCfgMgr CameraAniCfgMgr::_inst
	CameraAniCfgMgr_t11764299 * ____inst_0;
	// System.Boolean CameraAniCfgMgr::<inited>k__BackingField
	bool ___U3CinitedU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of__inst_0() { return static_cast<int32_t>(offsetof(CameraAniCfgMgr_t11764299_StaticFields, ____inst_0)); }
	inline CameraAniCfgMgr_t11764299 * get__inst_0() const { return ____inst_0; }
	inline CameraAniCfgMgr_t11764299 ** get_address_of__inst_0() { return &____inst_0; }
	inline void set__inst_0(CameraAniCfgMgr_t11764299 * value)
	{
		____inst_0 = value;
		Il2CppCodeGenWriteBarrier(&____inst_0, value);
	}

	inline static int32_t get_offset_of_U3CinitedU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(CameraAniCfgMgr_t11764299_StaticFields, ___U3CinitedU3Ek__BackingField_2)); }
	inline bool get_U3CinitedU3Ek__BackingField_2() const { return ___U3CinitedU3Ek__BackingField_2; }
	inline bool* get_address_of_U3CinitedU3Ek__BackingField_2() { return &___U3CinitedU3Ek__BackingField_2; }
	inline void set_U3CinitedU3Ek__BackingField_2(bool value)
	{
		___U3CinitedU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
