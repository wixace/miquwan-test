﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.PathNode
struct PathNode_t417131581;

#include "codegen/il2cpp-codegen.h"

// System.Void Pathfinding.PathNode::.ctor()
extern "C"  void PathNode__ctor_m3841284810 (PathNode_t417131581 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Pathfinding.PathNode::get_cost()
extern "C"  uint32_t PathNode_get_cost_m1807819339 (PathNode_t417131581 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.PathNode::set_cost(System.UInt32)
extern "C"  void PathNode_set_cost_m2003495894 (PathNode_t417131581 * __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.PathNode::get_flag1()
extern "C"  bool PathNode_get_flag1_m3772773744 (PathNode_t417131581 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.PathNode::set_flag1(System.Boolean)
extern "C"  void PathNode_set_flag1_m3939622951 (PathNode_t417131581 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.PathNode::get_flag2()
extern "C"  bool PathNode_get_flag2_m3772774705 (PathNode_t417131581 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.PathNode::set_flag2(System.Boolean)
extern "C"  void PathNode_set_flag2_m997965352 (PathNode_t417131581 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Pathfinding.PathNode::get_G()
extern "C"  uint32_t PathNode_get_G_m3569247211 (PathNode_t417131581 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.PathNode::set_G(System.UInt32)
extern "C"  void PathNode_set_G_m3581728008 (PathNode_t417131581 * __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Pathfinding.PathNode::get_H()
extern "C"  uint32_t PathNode_get_H_m3569248172 (PathNode_t417131581 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.PathNode::set_H(System.UInt32)
extern "C"  void PathNode_set_H_m3071193831 (PathNode_t417131581 * __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Pathfinding.PathNode::get_F()
extern "C"  uint32_t PathNode_get_F_m3569246250 (PathNode_t417131581 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
