﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mihua_Asset_ABLoadOperation_AssetOperationLoadGenerated
struct Mihua_Asset_ABLoadOperation_AssetOperationLoadGenerated_t3133937437;
// Mihua.Asset.OnLoadAsset
struct OnLoadAsset_t4181543125;
// CSRepresentedObject
struct CSRepresentedObject_t3994124630;
// JSVCall
struct JSVCall_t3708497963;
// System.Delegate
struct Delegate_t3310234105;
// System.Object
struct Il2CppObject;
// Mihua.Asset.ABLoadOperation.AssetOperationLoad
struct AssetOperationLoad_t3754788771;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CSRepresentedObject3994124630.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Delegate3310234105.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_Mihua_Asset_ABLoadOperation_Asse3754788771.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"

// System.Void Mihua_Asset_ABLoadOperation_AssetOperationLoadGenerated::.ctor()
extern "C"  void Mihua_Asset_ABLoadOperation_AssetOperationLoadGenerated__ctor_m3169343726 (Mihua_Asset_ABLoadOperation_AssetOperationLoadGenerated_t3133937437 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mihua.Asset.OnLoadAsset Mihua_Asset_ABLoadOperation_AssetOperationLoadGenerated::AssetOperationLoad__ctor_GetDelegate_member0_arg1(CSRepresentedObject)
extern "C"  OnLoadAsset_t4181543125 * Mihua_Asset_ABLoadOperation_AssetOperationLoadGenerated_AssetOperationLoad__ctor_GetDelegate_member0_arg1_m1354507646 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mihua_Asset_ABLoadOperation_AssetOperationLoadGenerated::AssetOperationLoad_AssetOperationLoad1(JSVCall,System.Int32)
extern "C"  bool Mihua_Asset_ABLoadOperation_AssetOperationLoadGenerated_AssetOperationLoad_AssetOperationLoad1_m2552834468 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua_Asset_ABLoadOperation_AssetOperationLoadGenerated::AssetOperationLoad_progress(JSVCall)
extern "C"  void Mihua_Asset_ABLoadOperation_AssetOperationLoadGenerated_AssetOperationLoad_progress_m3900993260 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mihua_Asset_ABLoadOperation_AssetOperationLoadGenerated::AssetOperationLoad_CacheAsset(JSVCall,System.Int32)
extern "C"  bool Mihua_Asset_ABLoadOperation_AssetOperationLoadGenerated_AssetOperationLoad_CacheAsset_m2191791776 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mihua_Asset_ABLoadOperation_AssetOperationLoadGenerated::AssetOperationLoad_Clear(JSVCall,System.Int32)
extern "C"  bool Mihua_Asset_ABLoadOperation_AssetOperationLoadGenerated_AssetOperationLoad_Clear_m4009274237 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mihua_Asset_ABLoadOperation_AssetOperationLoadGenerated::AssetOperationLoad_ClearOnLoad(JSVCall,System.Int32)
extern "C"  bool Mihua_Asset_ABLoadOperation_AssetOperationLoadGenerated_AssetOperationLoad_ClearOnLoad_m4031231042 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mihua_Asset_ABLoadOperation_AssetOperationLoadGenerated::AssetOperationLoad_GetAsset(JSVCall,System.Int32)
extern "C"  bool Mihua_Asset_ABLoadOperation_AssetOperationLoadGenerated_AssetOperationLoad_GetAsset_m2504646284 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mihua_Asset_ABLoadOperation_AssetOperationLoadGenerated::AssetOperationLoad_Init(JSVCall,System.Int32)
extern "C"  bool Mihua_Asset_ABLoadOperation_AssetOperationLoadGenerated_AssetOperationLoad_Init_m998544994 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mihua_Asset_ABLoadOperation_AssetOperationLoadGenerated::AssetOperationLoad_IsDone(JSVCall,System.Int32)
extern "C"  bool Mihua_Asset_ABLoadOperation_AssetOperationLoadGenerated_AssetOperationLoad_IsDone_m1236414430 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua_Asset_ABLoadOperation_AssetOperationLoadGenerated::__Register()
extern "C"  void Mihua_Asset_ABLoadOperation_AssetOperationLoadGenerated___Register_m978421913 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mihua.Asset.OnLoadAsset Mihua_Asset_ABLoadOperation_AssetOperationLoadGenerated::<AssetOperationLoad_AssetOperationLoad1>m__79()
extern "C"  OnLoadAsset_t4181543125 * Mihua_Asset_ABLoadOperation_AssetOperationLoadGenerated_U3CAssetOperationLoad_AssetOperationLoad1U3Em__79_m1473868871 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua_Asset_ABLoadOperation_AssetOperationLoadGenerated::ilo_addJSFunCSDelegateRel1(System.Int32,System.Delegate)
extern "C"  void Mihua_Asset_ABLoadOperation_AssetOperationLoadGenerated_ilo_addJSFunCSDelegateRel1_m4286645156 (Il2CppObject * __this /* static, unused */, int32_t ___funID0, Delegate_t3310234105 * ___del1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mihua_Asset_ABLoadOperation_AssetOperationLoadGenerated::ilo_getObject2(System.Int32)
extern "C"  int32_t Mihua_Asset_ABLoadOperation_AssetOperationLoadGenerated_ilo_getObject2_m3605183049 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua_Asset_ABLoadOperation_AssetOperationLoadGenerated::ilo_addJSCSRel3(System.Int32,System.Object)
extern "C"  void Mihua_Asset_ABLoadOperation_AssetOperationLoadGenerated_ilo_addJSCSRel3_m4291975596 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Mihua_Asset_ABLoadOperation_AssetOperationLoadGenerated::ilo_get_progress4(Mihua.Asset.ABLoadOperation.AssetOperationLoad)
extern "C"  float Mihua_Asset_ABLoadOperation_AssetOperationLoadGenerated_ilo_get_progress4_m1298475332 (Il2CppObject * __this /* static, unused */, AssetOperationLoad_t3754788771 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua_Asset_ABLoadOperation_AssetOperationLoadGenerated::ilo_setSingle5(System.Int32,System.Single)
extern "C"  void Mihua_Asset_ABLoadOperation_AssetOperationLoadGenerated_ilo_setSingle5_m2384837322 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mihua_Asset_ABLoadOperation_AssetOperationLoadGenerated::ilo_setObject6(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t Mihua_Asset_ABLoadOperation_AssetOperationLoadGenerated_ilo_setObject6_m1895140037 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mihua_Asset_ABLoadOperation_AssetOperationLoadGenerated::ilo_isFunctionS7(System.Int32)
extern "C"  bool Mihua_Asset_ABLoadOperation_AssetOperationLoadGenerated_ilo_isFunctionS7_m598209772 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CSRepresentedObject Mihua_Asset_ABLoadOperation_AssetOperationLoadGenerated::ilo_getFunctionS8(System.Int32)
extern "C"  CSRepresentedObject_t3994124630 * Mihua_Asset_ABLoadOperation_AssetOperationLoadGenerated_ilo_getFunctionS8_m1967585466 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
