﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SevenZip.LzmaAlone
struct LzmaAlone_t3726148603;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t4054002952;
// SevenZip.CommandLineParser.SwitchResult
struct SwitchResult_t80294439;
// SevenZip.CommandLineParser.Parser
struct Parser_t3511445845;
// SevenZip.Compression.LZMA.Encoder
struct Encoder_t1693961342;
// SevenZip.CoderPropID[]
struct CoderPropIDU5BU5D_t3009125352;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// System.IO.Stream
struct Stream_t1561764144;
// SevenZip.Compression.LZMA.Decoder
struct Decoder_t548795302;
// System.Byte[]
struct ByteU5BU5D_t4260760469;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_SevenZip_CommandLineParser_Parse3511445845.h"
#include "AssemblyU2DCSharp_SevenZip_Compression_LZMA_Encode1693961342.h"
#include "mscorlib_System_IO_Stream1561764144.h"
#include "AssemblyU2DCSharp_SevenZip_Compression_LZMA_Decoder548795302.h"

// System.Void SevenZip.LzmaAlone::.ctor()
extern "C"  void LzmaAlone__ctor_m3001439548 (LzmaAlone_t3726148603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.LzmaAlone::PrintHelp()
extern "C"  void LzmaAlone_PrintHelp_m1959039464 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SevenZip.LzmaAlone::GetNumber(System.String,System.Int32&)
extern "C"  bool LzmaAlone_GetNumber_m4168734482 (Il2CppObject * __this /* static, unused */, String_t* ___s0, int32_t* ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SevenZip.LzmaAlone::IncorrectCommand()
extern "C"  int32_t LzmaAlone_IncorrectCommand_m719618044 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SevenZip.LzmaAlone::Main2(System.String[])
extern "C"  int32_t LzmaAlone_Main2_m2803955739 (Il2CppObject * __this /* static, unused */, StringU5BU5D_t4054002952* ___args0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.LzmaAlone::ilo_PrintHelp1()
extern "C"  void LzmaAlone_ilo_PrintHelp1_m419367710 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SevenZip.LzmaAlone::ilo_IncorrectCommand2()
extern "C"  int32_t LzmaAlone_ilo_IncorrectCommand2_m685780613 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SevenZip.LzmaAlone::ilo_GetNumber3(System.String,System.Int32&)
extern "C"  bool LzmaAlone_ilo_GetNumber3_m1323114612 (Il2CppObject * __this /* static, unused */, String_t* ___s0, int32_t* ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SevenZip.CommandLineParser.SwitchResult SevenZip.LzmaAlone::ilo_get_Item4(SevenZip.CommandLineParser.Parser,System.Int32)
extern "C"  SwitchResult_t80294439 * LzmaAlone_ilo_get_Item4_m3646594357 (Il2CppObject * __this /* static, unused */, Parser_t3511445845 * ____this0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.LzmaAlone::ilo_SetCoderProperties5(SevenZip.Compression.LZMA.Encoder,SevenZip.CoderPropID[],System.Object[])
extern "C"  void LzmaAlone_ilo_SetCoderProperties5_m1841059437 (Il2CppObject * __this /* static, unused */, Encoder_t1693961342 * ____this0, CoderPropIDU5BU5D_t3009125352* ___propIDs1, ObjectU5BU5D_t1108656482* ___properties2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.LzmaAlone::ilo_WriteCoderProperties6(SevenZip.Compression.LZMA.Encoder,System.IO.Stream)
extern "C"  void LzmaAlone_ilo_WriteCoderProperties6_m2053828289 (Il2CppObject * __this /* static, unused */, Encoder_t1693961342 * ____this0, Stream_t1561764144 * ___outStream1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.LzmaAlone::ilo_SetDecoderProperties7(SevenZip.Compression.LZMA.Decoder,System.Byte[])
extern "C"  void LzmaAlone_ilo_SetDecoderProperties7_m1136228818 (Il2CppObject * __this /* static, unused */, Decoder_t548795302 * ____this0, ByteU5BU5D_t4260760469* ___properties1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
