﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_MonoBehaviourEx1076555597.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DelayDestroy
struct  DelayDestroy_t3754862871  : public MonoBehaviourEx_t1076555597
{
public:
	// System.Single DelayDestroy::delay
	float ___delay_3;
	// System.Single DelayDestroy::endTime
	float ___endTime_4;

public:
	inline static int32_t get_offset_of_delay_3() { return static_cast<int32_t>(offsetof(DelayDestroy_t3754862871, ___delay_3)); }
	inline float get_delay_3() const { return ___delay_3; }
	inline float* get_address_of_delay_3() { return &___delay_3; }
	inline void set_delay_3(float value)
	{
		___delay_3 = value;
	}

	inline static int32_t get_offset_of_endTime_4() { return static_cast<int32_t>(offsetof(DelayDestroy_t3754862871, ___endTime_4)); }
	inline float get_endTime_4() const { return ___endTime_4; }
	inline float* get_address_of_endTime_4() { return &___endTime_4; }
	inline void set_endTime_4(float value)
	{
		___endTime_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
