﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TexturesStaticLoad
struct TexturesStaticLoad_t2473754988;
// UnityEngine.Texture
struct Texture_t2526458961;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_TexturesStaticLoad2473754988.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void TexturesStaticLoad::.ctor()
extern "C"  void TexturesStaticLoad__ctor_m368072879 (TexturesStaticLoad_t2473754988 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 TexturesStaticLoad::get_Length()
extern "C"  int32_t TexturesStaticLoad_get_Length_m3641573874 (TexturesStaticLoad_t2473754988 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture TexturesStaticLoad::GetTexture(System.String)
extern "C"  Texture_t2526458961 * TexturesStaticLoad_GetTexture_m1695569209 (TexturesStaticLoad_t2473754988 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture TexturesStaticLoad::GetTexture(System.Int32)
extern "C"  Texture_t2526458961 * TexturesStaticLoad_GetTexture_m178510906 (TexturesStaticLoad_t2473754988 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture TexturesStaticLoad::GetRandomTexture()
extern "C"  Texture_t2526458961 * TexturesStaticLoad_GetRandomTexture_m3949110566 (TexturesStaticLoad_t2473754988 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TexturesStaticLoad::AddTextures(TexturesStaticLoad)
extern "C"  void TexturesStaticLoad_AddTextures_m867550170 (TexturesStaticLoad_t2473754988 * __this, TexturesStaticLoad_t2473754988 * ___texs0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TexturesStaticLoad::Init()
extern "C"  void TexturesStaticLoad_Init_m1487430597 (TexturesStaticLoad_t2473754988 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TexturesStaticLoad::ilo_LogError1(System.Object,System.Boolean)
extern "C"  void TexturesStaticLoad_ilo_LogError1_m838318320 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TexturesStaticLoad::ilo_Init2(TexturesStaticLoad)
extern "C"  void TexturesStaticLoad_ilo_Init2_m1188379696 (Il2CppObject * __this /* static, unused */, TexturesStaticLoad_t2473754988 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 TexturesStaticLoad::ilo_get_Length3(TexturesStaticLoad)
extern "C"  int32_t TexturesStaticLoad_ilo_get_Length3_m3275998116 (Il2CppObject * __this /* static, unused */, TexturesStaticLoad_t2473754988 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
