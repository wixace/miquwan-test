﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PluginTT
struct PluginTT_t2499992851;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// System.String
struct String_t;
// UnityEngine.WWWForm
struct WWWForm_t461342257;
// Mihua.SDK.PayInfo
struct PayInfo_t1775308120;
// CEvent.EventFunc`1<CEvent.ZEvent>
struct EventFunc_1_t1114914310;
// VersionMgr
struct VersionMgr_t1322950208;
// FloatTextMgr
struct FloatTextMgr_t630384591;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// System.Object
struct Il2CppObject;
// PluginsSdkMgr
struct PluginsSdkMgr_t3884624670;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_Mihua_SDK_PayInfo1775308120.h"
#include "AssemblyU2DCSharp_VersionMgr1322950208.h"
#include "AssemblyU2DCSharp_FloatTextMgr630384591.h"
#include "AssemblyU2DCSharp_FLOAT_TEXT_ID2199345770.h"
#include "AssemblyU2DCSharp_PluginTT2499992851.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_PluginsSdkMgr3884624670.h"

// System.Void PluginTT::.ctor()
extern "C"  void PluginTT__ctor_m2111432424 (PluginTT_t2499992851 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginTT::Init()
extern "C"  void PluginTT_Init_m4037519980 (PluginTT_t2499992851 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginTT::ReqSDKHttpLogin()
extern "C"  Dictionary_2_t827649927 * PluginTT_ReqSDKHttpLogin_m2538608189 (PluginTT_t2499992851 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginTT::IsLoginSuccess(System.Boolean)
extern "C"  bool PluginTT_IsLoginSuccess_m2471480683 (PluginTT_t2499992851 * __this, bool ___isOpenUseLogin0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginTT::OpenUserLogin()
extern "C"  void PluginTT_OpenUserLogin_m2823921978 (PluginTT_t2499992851 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginTT::UserPay(CEvent.ZEvent)
extern "C"  void PluginTT_UserPay_m2251226360 (PluginTT_t2499992851 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginTT::SignCallBack(System.Boolean,System.String)
extern "C"  void PluginTT_SignCallBack_m749340561 (PluginTT_t2499992851 * __this, bool ___success0, String_t* ___arg1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.WWWForm PluginTT::BuildOrderParam2WWWForm(Mihua.SDK.PayInfo)
extern "C"  WWWForm_t461342257 * PluginTT_BuildOrderParam2WWWForm_m118048761 (PluginTT_t2499992851 * __this, PayInfo_t1775308120 * ___info0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginTT::CreateRole(CEvent.ZEvent)
extern "C"  void PluginTT_CreateRole_m2505300749 (PluginTT_t2499992851 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginTT::EnterGame(CEvent.ZEvent)
extern "C"  void PluginTT_EnterGame_m981561483 (PluginTT_t2499992851 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginTT::RoleUpgrade(CEvent.ZEvent)
extern "C"  void PluginTT_RoleUpgrade_m3018210415 (PluginTT_t2499992851 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginTT::OnLoginSuccess(System.String)
extern "C"  void PluginTT_OnLoginSuccess_m509231085 (PluginTT_t2499992851 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginTT::OnLogoutSuccess(System.String)
extern "C"  void PluginTT_OnLogoutSuccess_m2904804418 (PluginTT_t2499992851 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginTT::OnLogout(System.String)
extern "C"  void PluginTT_OnLogout_m2807429437 (PluginTT_t2499992851 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginTT::initSdk()
extern "C"  void PluginTT_initSdk_m3531172208 (PluginTT_t2499992851 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginTT::login()
extern "C"  void PluginTT_login_m1633447247 (PluginTT_t2499992851 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginTT::logout()
extern "C"  void PluginTT_logout_m3398047110 (PluginTT_t2499992851 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginTT::updateRoleInfo(System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginTT_updateRoleInfo_m2540205841 (PluginTT_t2499992851 * __this, String_t* ___dataType0, String_t* ___serverID1, String_t* ___serverName2, String_t* ___roleID3, String_t* ___roleName4, String_t* ___roleLevel5, String_t* ___roleVip6, String_t* ___roleBalence7, String_t* ___partyName8, String_t* ___rolelevelCtime9, String_t* ___rolelevelMtime10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginTT::pay(System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginTT_pay_m3627828492 (PluginTT_t2499992851 * __this, String_t* ___gameOrderId0, String_t* ___serverID1, String_t* ___key_serverName2, String_t* ___productId3, String_t* ___productName4, String_t* ___key_productdesc5, String_t* ___key_ext6, String_t* ___key_productPrice7, String_t* ___key_roleID8, String_t* ___key_roleName9, String_t* ___key_currencyName10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginTT::<OnLogoutSuccess>m__457()
extern "C"  void PluginTT_U3COnLogoutSuccessU3Em__457_m1341448367 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginTT::<OnLogout>m__458()
extern "C"  void PluginTT_U3COnLogoutU3Em__458_m1398603551 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginTT::ilo_AddEventListener1(System.String,CEvent.EventFunc`1<CEvent.ZEvent>)
extern "C"  void PluginTT_ilo_AddEventListener1_m2545995020 (Il2CppObject * __this /* static, unused */, String_t* ___eventType0, EventFunc_1_t1114914310 * ___fun1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionMgr PluginTT::ilo_get_Instance2()
extern "C"  VersionMgr_t1322950208 * PluginTT_ilo_get_Instance2_m2631753066 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginTT::ilo_get_isSdkLogin3(VersionMgr)
extern "C"  bool PluginTT_ilo_get_isSdkLogin3_m2845890160 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginTT::ilo_FloatText4(FloatTextMgr,FLOAT_TEXT_ID,System.Int32,System.Object[])
extern "C"  void PluginTT_ilo_FloatText4_m3286776598 (Il2CppObject * __this /* static, unused */, FloatTextMgr_t630384591 * ____this0, int32_t ___textId1, int32_t ___offsetY2, ObjectU5BU5D_t1108656482* ___args3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginTT::ilo_pay5(PluginTT,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginTT_ilo_pay5_m1011596701 (Il2CppObject * __this /* static, unused */, PluginTT_t2499992851 * ____this0, String_t* ___gameOrderId1, String_t* ___serverID2, String_t* ___key_serverName3, String_t* ___productId4, String_t* ___productName5, String_t* ___key_productdesc6, String_t* ___key_ext7, String_t* ___key_productPrice8, String_t* ___key_roleID9, String_t* ___key_roleName10, String_t* ___key_currencyName11, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginTT::ilo_Log6(System.Object,System.Boolean)
extern "C"  void PluginTT_ilo_Log6_m1114999978 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginTT::ilo_LogError7(System.Object,System.Boolean)
extern "C"  void PluginTT_ilo_LogError7_m2391085103 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PluginsSdkMgr PluginTT::ilo_get_PluginsSdkMgr8()
extern "C"  PluginsSdkMgr_t3884624670 * PluginTT_ilo_get_PluginsSdkMgr8_m1780788103 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginTT::ilo_ReqSDKHttpLogin9(PluginsSdkMgr)
extern "C"  void PluginTT_ilo_ReqSDKHttpLogin9_m606682353 (Il2CppObject * __this /* static, unused */, PluginsSdkMgr_t3884624670 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
