﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_EventSystems_IEndDragHandlerGenerated
struct UnityEngine_EventSystems_IEndDragHandlerGenerated_t1241680824;
// JSVCall
struct JSVCall_t3708497963;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"

// System.Void UnityEngine_EventSystems_IEndDragHandlerGenerated::.ctor()
extern "C"  void UnityEngine_EventSystems_IEndDragHandlerGenerated__ctor_m1664692403 (UnityEngine_EventSystems_IEndDragHandlerGenerated_t1241680824 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_EventSystems_IEndDragHandlerGenerated::IEndDragHandler_OnEndDrag__PointerEventData(JSVCall,System.Int32)
extern "C"  bool UnityEngine_EventSystems_IEndDragHandlerGenerated_IEndDragHandler_OnEndDrag__PointerEventData_m3916623249 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_IEndDragHandlerGenerated::__Register()
extern "C"  void UnityEngine_EventSystems_IEndDragHandlerGenerated___Register_m941212660 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
