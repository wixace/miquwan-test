﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SevenZip.LzmaBench
struct LzmaBench_t3726862288;
// SevenZip.Compression.LZMA.Encoder
struct Encoder_t1693961342;
// SevenZip.CoderPropID[]
struct CoderPropIDU5BU5D_t3009125352;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// SevenZip.LzmaBench/CBenchRandomGenerator
struct CBenchRandomGenerator_t1052694568;
// SevenZip.CRC
struct CRC_t1616380534;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// SevenZip.LzmaBench/CProgressInfo
struct CProgressInfo_t3639622563;
// System.IO.Stream
struct Stream_t1561764144;
// SevenZip.ICodeProgress
struct ICodeProgress_t453587813;
// SevenZip.Compression.LZMA.Decoder
struct Decoder_t548795302;
// SevenZip.LzmaBench/CrcOutStream
struct CrcOutStream_t4130964789;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SevenZip_Compression_LZMA_Encode1693961342.h"
#include "AssemblyU2DCSharp_SevenZip_LzmaBench_CBenchRandomG1052694568.h"
#include "AssemblyU2DCSharp_SevenZip_CRC1616380534.h"
#include "AssemblyU2DCSharp_SevenZip_LzmaBench_CProgressInfo3639622563.h"
#include "mscorlib_System_IO_Stream1561764144.h"
#include "AssemblyU2DCSharp_SevenZip_Compression_LZMA_Decoder548795302.h"
#include "AssemblyU2DCSharp_SevenZip_LzmaBench_CrcOutStream4130964789.h"

// System.Void SevenZip.LzmaBench::.ctor()
extern "C"  void LzmaBench__ctor_m2122707207 (LzmaBench_t3726862288 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 SevenZip.LzmaBench::GetLogSize(System.UInt32)
extern "C"  uint32_t LzmaBench_GetLogSize_m792900723 (Il2CppObject * __this /* static, unused */, uint32_t ___size0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 SevenZip.LzmaBench::MyMultDiv64(System.UInt64,System.UInt64)
extern "C"  uint64_t LzmaBench_MyMultDiv64_m2064734132 (Il2CppObject * __this /* static, unused */, uint64_t ___value0, uint64_t ___elapsedTime1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 SevenZip.LzmaBench::GetCompressRating(System.UInt32,System.UInt64,System.UInt64)
extern "C"  uint64_t LzmaBench_GetCompressRating_m2574253574 (Il2CppObject * __this /* static, unused */, uint32_t ___dictionarySize0, uint64_t ___elapsedTime1, uint64_t ___size2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 SevenZip.LzmaBench::GetDecompressRating(System.UInt64,System.UInt64,System.UInt64)
extern "C"  uint64_t LzmaBench_GetDecompressRating_m2140553254 (Il2CppObject * __this /* static, unused */, uint64_t ___elapsedTime0, uint64_t ___outSize1, uint64_t ___inSize2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 SevenZip.LzmaBench::GetTotalRating(System.UInt32,System.UInt64,System.UInt64,System.UInt64,System.UInt64,System.UInt64)
extern "C"  uint64_t LzmaBench_GetTotalRating_m1477869567 (Il2CppObject * __this /* static, unused */, uint32_t ___dictionarySize0, uint64_t ___elapsedTimeEn1, uint64_t ___sizeEn2, uint64_t ___elapsedTimeDe3, uint64_t ___inSizeDe4, uint64_t ___outSizeDe5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.LzmaBench::PrintValue(System.UInt64)
extern "C"  void LzmaBench_PrintValue_m360463866 (Il2CppObject * __this /* static, unused */, uint64_t ___v0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.LzmaBench::PrintRating(System.UInt64)
extern "C"  void LzmaBench_PrintRating_m3819950476 (Il2CppObject * __this /* static, unused */, uint64_t ___rating0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.LzmaBench::PrintResults(System.UInt32,System.UInt64,System.UInt64,System.Boolean,System.UInt64)
extern "C"  void LzmaBench_PrintResults_m1283096948 (Il2CppObject * __this /* static, unused */, uint32_t ___dictionarySize0, uint64_t ___elapsedTime1, uint64_t ___size2, bool ___decompressMode3, uint64_t ___secondSize4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SevenZip.LzmaBench::LzmaBenchmark(System.Int32,System.UInt32)
extern "C"  int32_t LzmaBench_LzmaBenchmark_m4033123671 (Il2CppObject * __this /* static, unused */, int32_t ___numIterations0, uint32_t ___dictionarySize1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 SevenZip.LzmaBench::ilo_GetLogSize1(System.UInt32)
extern "C"  uint32_t LzmaBench_ilo_GetLogSize1_m4068940449 (Il2CppObject * __this /* static, unused */, uint32_t ___size0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 SevenZip.LzmaBench::ilo_MyMultDiv642(System.UInt64,System.UInt64)
extern "C"  uint64_t LzmaBench_ilo_MyMultDiv642_m309231557 (Il2CppObject * __this /* static, unused */, uint64_t ___value0, uint64_t ___elapsedTime1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.LzmaBench::ilo_PrintValue3(System.UInt64)
extern "C"  void LzmaBench_ilo_PrintValue3_m33360410 (Il2CppObject * __this /* static, unused */, uint64_t ___v0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.LzmaBench::ilo_SetCoderProperties4(SevenZip.Compression.LZMA.Encoder,SevenZip.CoderPropID[],System.Object[])
extern "C"  void LzmaBench_ilo_SetCoderProperties4_m427063223 (Il2CppObject * __this /* static, unused */, Encoder_t1693961342 * ____this0, CoderPropIDU5BU5D_t3009125352* ___propIDs1, ObjectU5BU5D_t1108656482* ___properties2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.LzmaBench::ilo_Set5(SevenZip.LzmaBench/CBenchRandomGenerator,System.UInt32)
extern "C"  void LzmaBench_ilo_Set5_m2365240943 (Il2CppObject * __this /* static, unused */, CBenchRandomGenerator_t1052694568 * ____this0, uint32_t ___bufferSize1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.LzmaBench::ilo_Update6(SevenZip.CRC,System.Byte[],System.UInt32,System.UInt32)
extern "C"  void LzmaBench_ilo_Update6_m64065232 (Il2CppObject * __this /* static, unused */, CRC_t1616380534 * ____this0, ByteU5BU5D_t4260760469* ___data1, uint32_t ___offset2, uint32_t ___size3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.LzmaBench::ilo_Init7(SevenZip.LzmaBench/CProgressInfo)
extern "C"  void LzmaBench_ilo_Init7_m3154407094 (Il2CppObject * __this /* static, unused */, CProgressInfo_t3639622563 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.LzmaBench::ilo_Code8(SevenZip.Compression.LZMA.Encoder,System.IO.Stream,System.IO.Stream,System.Int64,System.Int64,SevenZip.ICodeProgress)
extern "C"  void LzmaBench_ilo_Code8_m1454272634 (Il2CppObject * __this /* static, unused */, Encoder_t1693961342 * ____this0, Stream_t1561764144 * ___inStream1, Stream_t1561764144 * ___outStream2, int64_t ___inSize3, int64_t ___outSize4, Il2CppObject * ___progress5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.LzmaBench::ilo_SetDecoderProperties9(SevenZip.Compression.LZMA.Decoder,System.Byte[])
extern "C"  void LzmaBench_ilo_SetDecoderProperties9_m3725748837 (Il2CppObject * __this /* static, unused */, Decoder_t548795302 * ____this0, ByteU5BU5D_t4260760469* ___properties1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 SevenZip.LzmaBench::ilo_GetDigest10(SevenZip.LzmaBench/CrcOutStream)
extern "C"  uint32_t LzmaBench_ilo_GetDigest10_m2996720685 (Il2CppObject * __this /* static, unused */, CrcOutStream_t4130964789 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
