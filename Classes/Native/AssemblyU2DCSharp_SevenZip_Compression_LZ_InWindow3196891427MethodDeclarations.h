﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SevenZip.Compression.LZ.InWindow
struct InWindow_t3196891427;
// System.IO.Stream
struct Stream_t1561764144;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IO_Stream1561764144.h"
#include "AssemblyU2DCSharp_SevenZip_Compression_LZ_InWindow3196891427.h"

// System.Void SevenZip.Compression.LZ.InWindow::.ctor()
extern "C"  void InWindow__ctor_m3980785320 (InWindow_t3196891427 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.LZ.InWindow::MoveBlock()
extern "C"  void InWindow_MoveBlock_m2019681666 (InWindow_t3196891427 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.LZ.InWindow::ReadBlock()
extern "C"  void InWindow_ReadBlock_m1149022237 (InWindow_t3196891427 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.LZ.InWindow::Free()
extern "C"  void InWindow_Free_m1521642760 (InWindow_t3196891427 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.LZ.InWindow::Create(System.UInt32,System.UInt32,System.UInt32)
extern "C"  void InWindow_Create_m3845233034 (InWindow_t3196891427 * __this, uint32_t ___keepSizeBefore0, uint32_t ___keepSizeAfter1, uint32_t ___keepSizeReserv2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.LZ.InWindow::SetStream(System.IO.Stream)
extern "C"  void InWindow_SetStream_m2968597983 (InWindow_t3196891427 * __this, Stream_t1561764144 * ___stream0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.LZ.InWindow::ReleaseStream()
extern "C"  void InWindow_ReleaseStream_m1784597357 (InWindow_t3196891427 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.LZ.InWindow::Init()
extern "C"  void InWindow_Init_m1603969708 (InWindow_t3196891427 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.LZ.InWindow::MovePos()
extern "C"  void InWindow_MovePos_m3715676809 (InWindow_t3196891427 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte SevenZip.Compression.LZ.InWindow::GetIndexByte(System.Int32)
extern "C"  uint8_t InWindow_GetIndexByte_m4086920765 (InWindow_t3196891427 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 SevenZip.Compression.LZ.InWindow::GetMatchLen(System.Int32,System.UInt32,System.UInt32)
extern "C"  uint32_t InWindow_GetMatchLen_m1664488138 (InWindow_t3196891427 * __this, int32_t ___index0, uint32_t ___distance1, uint32_t ___limit2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 SevenZip.Compression.LZ.InWindow::GetNumAvailableBytes()
extern "C"  uint32_t InWindow_GetNumAvailableBytes_m315852233 (InWindow_t3196891427 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.LZ.InWindow::ReduceOffsets(System.Int32)
extern "C"  void InWindow_ReduceOffsets_m2845677457 (InWindow_t3196891427 * __this, int32_t ___subValue0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.LZ.InWindow::ilo_Free1(SevenZip.Compression.LZ.InWindow)
extern "C"  void InWindow_ilo_Free1_m4251815621 (Il2CppObject * __this /* static, unused */, InWindow_t3196891427 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
