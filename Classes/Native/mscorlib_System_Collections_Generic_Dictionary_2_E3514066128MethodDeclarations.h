﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1190435706MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.WeakReference>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m916198466(__this, ___dictionary0, method) ((  void (*) (Enumerator_t3514066128 *, Dictionary_2_t2196742736 *, const MethodInfo*))Enumerator__ctor_m2377115088_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.WeakReference>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m601585247(__this, method) ((  Il2CppObject * (*) (Enumerator_t3514066128 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1037642267_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.WeakReference>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m223811379(__this, method) ((  void (*) (Enumerator_t3514066128 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2809374949_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.WeakReference>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2035716540(__this, method) ((  DictionaryEntry_t1751606614  (*) (Enumerator_t3514066128 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2434214620_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.WeakReference>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m568995835(__this, method) ((  Il2CppObject * (*) (Enumerator_t3514066128 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3735627447_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.WeakReference>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2392587213(__this, method) ((  Il2CppObject * (*) (Enumerator_t3514066128 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m393753481_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.WeakReference>::MoveNext()
#define Enumerator_MoveNext_m2579557654(__this, method) ((  bool (*) (Enumerator_t3514066128 *, const MethodInfo*))Enumerator_MoveNext_m1213995029_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.WeakReference>::get_Current()
#define Enumerator_get_Current_m1178090505(__this, method) ((  KeyValuePair_2_t2095523442  (*) (Enumerator_t3514066128 *, const MethodInfo*))Enumerator_get_Current_m1399860359_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.WeakReference>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m3322253100(__this, method) ((  int32_t (*) (Enumerator_t3514066128 *, const MethodInfo*))Enumerator_get_CurrentKey_m1767398110_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.WeakReference>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m1776304976(__this, method) ((  WeakReference_t2199479497 * (*) (Enumerator_t3514066128 *, const MethodInfo*))Enumerator_get_CurrentValue_m3384846750_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.WeakReference>::Reset()
#define Enumerator_Reset_m1076194196(__this, method) ((  void (*) (Enumerator_t3514066128 *, const MethodInfo*))Enumerator_Reset_m1080084514_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.WeakReference>::VerifyState()
#define Enumerator_VerifyState_m3528899741(__this, method) ((  void (*) (Enumerator_t3514066128 *, const MethodInfo*))Enumerator_VerifyState_m2404513451_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.WeakReference>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m993359045(__this, method) ((  void (*) (Enumerator_t3514066128 *, const MethodInfo*))Enumerator_VerifyCurrent_m2789892947_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.WeakReference>::Dispose()
#define Enumerator_Dispose_m1658933092(__this, method) ((  void (*) (Enumerator_t3514066128 *, const MethodInfo*))Enumerator_Dispose_m1102561394_gshared)(__this, method)
