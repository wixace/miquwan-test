﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIPlayTween
struct UIPlayTween_t533751651;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// UIToggle
struct UIToggle_t688812808;
// EventDelegate
struct EventDelegate_t4004424223;
// System.Collections.Generic.List`1<EventDelegate>
struct List_1_t1077642479;
// EventDelegate/Callback
struct Callback_t1094463061;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "AssemblyU2DCSharp_UIPlayTween533751651.h"
#include "AssemblyU2DCSharp_UIToggle688812808.h"
#include "AssemblyU2DCSharp_EventDelegate_Callback1094463061.h"

// System.Void UIPlayTween::.ctor()
extern "C"  void UIPlayTween__ctor_m38122344 (UIPlayTween_t533751651 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPlayTween::Awake()
extern "C"  void UIPlayTween_Awake_m275727563 (UIPlayTween_t533751651 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPlayTween::Start()
extern "C"  void UIPlayTween_Start_m3280227432 (UIPlayTween_t533751651 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPlayTween::OnEnable()
extern "C"  void UIPlayTween_OnEnable_m3902962462 (UIPlayTween_t533751651 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPlayTween::OnDisable()
extern "C"  void UIPlayTween_OnDisable_m1173689295 (UIPlayTween_t533751651 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPlayTween::OnDragOver()
extern "C"  void UIPlayTween_OnDragOver_m2127001091 (UIPlayTween_t533751651 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPlayTween::OnHover(System.Boolean)
extern "C"  void UIPlayTween_OnHover_m1150128858 (UIPlayTween_t533751651 * __this, bool ___isOver0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPlayTween::OnDragOut()
extern "C"  void UIPlayTween_OnDragOut_m1038426593 (UIPlayTween_t533751651 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPlayTween::OnPress(System.Boolean)
extern "C"  void UIPlayTween_OnPress_m3364782049 (UIPlayTween_t533751651 * __this, bool ___isPressed0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPlayTween::OnClick()
extern "C"  void UIPlayTween_OnClick_m4120039407 (UIPlayTween_t533751651 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPlayTween::OnDoubleClick()
extern "C"  void UIPlayTween_OnDoubleClick_m193170014 (UIPlayTween_t533751651 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPlayTween::OnSelect(System.Boolean)
extern "C"  void UIPlayTween_OnSelect_m855794062 (UIPlayTween_t533751651 * __this, bool ___isSelected0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPlayTween::OnToggle()
extern "C"  void UIPlayTween_OnToggle_m1044242607 (UIPlayTween_t533751651 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPlayTween::Update()
extern "C"  void UIPlayTween_Update_m2908654757 (UIPlayTween_t533751651 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPlayTween::Play(System.Boolean)
extern "C"  void UIPlayTween_Play_m4123825287 (UIPlayTween_t533751651 * __this, bool ___forward0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPlayTween::OnFinished()
extern "C"  void UIPlayTween_OnFinished_m500639693 (UIPlayTween_t533751651 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPlayTween::OnDestory()
extern "C"  void UIPlayTween_OnDestory_m3348540155 (UIPlayTween_t533751651 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIPlayTween::ilo_IsHighlighted1(UnityEngine.GameObject)
extern "C"  bool UIPlayTween_ilo_IsHighlighted1_m1646957403 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPlayTween::ilo_OnHover2(UIPlayTween,System.Boolean)
extern "C"  void UIPlayTween_ilo_OnHover2_m2515077618 (Il2CppObject * __this /* static, unused */, UIPlayTween_t533751651 * ____this0, bool ___isOver1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPlayTween::ilo_Play3(UIPlayTween,System.Boolean)
extern "C"  void UIPlayTween_ilo_Play3_m4085333088 (Il2CppObject * __this /* static, unused */, UIPlayTween_t533751651 * ____this0, bool ___forward1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIPlayTween::ilo_get_value4(UIToggle)
extern "C"  bool UIPlayTween_ilo_get_value4_m2102148447 (Il2CppObject * __this /* static, unused */, UIToggle_t688812808 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIPlayTween::ilo_GetActive5(UnityEngine.GameObject)
extern "C"  bool UIPlayTween_ilo_GetActive5_m2019379852 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPlayTween::ilo_SetActive6(UnityEngine.GameObject,System.Boolean)
extern "C"  void UIPlayTween_ilo_SetActive6_m1144182440 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___go0, bool ___state1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// EventDelegate UIPlayTween::ilo_Add7(System.Collections.Generic.List`1<EventDelegate>,EventDelegate/Callback,System.Boolean)
extern "C"  EventDelegate_t4004424223 * UIPlayTween_ilo_Add7_m2910676896 (Il2CppObject * __this /* static, unused */, List_1_t1077642479 * ___list0, Callback_t1094463061 * ___callback1, bool ___oneShot2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
