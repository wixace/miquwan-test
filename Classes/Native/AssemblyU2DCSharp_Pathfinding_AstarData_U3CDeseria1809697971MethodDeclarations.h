﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.AstarData/<DeserializeGraphsPart>c__AnonStorey102
struct U3CDeserializeGraphsPartU3Ec__AnonStorey102_t1809697971;
// Pathfinding.GraphNode
struct GraphNode_t23612370;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphNode23612370.h"

// System.Void Pathfinding.AstarData/<DeserializeGraphsPart>c__AnonStorey102::.ctor()
extern "C"  void U3CDeserializeGraphsPartU3Ec__AnonStorey102__ctor_m228572696 (U3CDeserializeGraphsPartU3Ec__AnonStorey102_t1809697971 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.AstarData/<DeserializeGraphsPart>c__AnonStorey102::<>m__32F(Pathfinding.GraphNode)
extern "C"  bool U3CDeserializeGraphsPartU3Ec__AnonStorey102_U3CU3Em__32F_m105136248 (U3CDeserializeGraphsPartU3Ec__AnonStorey102_t1809697971 * __this, GraphNode_t23612370 * ___node0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
