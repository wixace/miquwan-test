﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_SpriteRendererGenerated
struct UnityEngine_SpriteRendererGenerated_t4063888735;
// JSVCall
struct JSVCall_t3708497963;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void UnityEngine_SpriteRendererGenerated::.ctor()
extern "C"  void UnityEngine_SpriteRendererGenerated__ctor_m2369247468 (UnityEngine_SpriteRendererGenerated_t4063888735 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_SpriteRendererGenerated::SpriteRenderer_SpriteRenderer1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_SpriteRendererGenerated_SpriteRenderer_SpriteRenderer1_m1307781062 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SpriteRendererGenerated::SpriteRenderer_sprite(JSVCall)
extern "C"  void UnityEngine_SpriteRendererGenerated_SpriteRenderer_sprite_m2032090529 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SpriteRendererGenerated::SpriteRenderer_color(JSVCall)
extern "C"  void UnityEngine_SpriteRendererGenerated_SpriteRenderer_color_m836317251 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SpriteRendererGenerated::SpriteRenderer_flipX(JSVCall)
extern "C"  void UnityEngine_SpriteRendererGenerated_SpriteRenderer_flipX_m3125002011 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SpriteRendererGenerated::SpriteRenderer_flipY(JSVCall)
extern "C"  void UnityEngine_SpriteRendererGenerated_SpriteRenderer_flipY_m2928488506 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SpriteRendererGenerated::__Register()
extern "C"  void UnityEngine_SpriteRendererGenerated___Register_m2890422107 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_SpriteRendererGenerated::ilo_setObject1(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UnityEngine_SpriteRendererGenerated_ilo_setObject1_m3804961602 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SpriteRendererGenerated::ilo_setBooleanS2(System.Int32,System.Boolean)
extern "C"  void UnityEngine_SpriteRendererGenerated_ilo_setBooleanS2_m1517671932 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_SpriteRendererGenerated::ilo_getBooleanS3(System.Int32)
extern "C"  bool UnityEngine_SpriteRendererGenerated_ilo_getBooleanS3_m2460743930 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
