﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PluginYouDong
struct PluginYouDong_t3100552560;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// System.String
struct String_t;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// CEvent.EventFunc`1<CEvent.ZEvent>
struct EventFunc_1_t1114914310;
// VersionMgr
struct VersionMgr_t1322950208;
// PluginsSdkMgr
struct PluginsSdkMgr_t3884624670;
// Mihua.SDK.PayInfo
struct PayInfo_t1775308120;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// Mihua.SDK.EnterGameInfo
struct EnterGameInfo_t1897532954;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "AssemblyU2DCSharp_PluginYouDong3100552560.h"

// System.Void PluginYouDong::.ctor()
extern "C"  void PluginYouDong__ctor_m3074413051 (PluginYouDong_t3100552560 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYouDong::Init()
extern "C"  void PluginYouDong_Init_m1713279225 (PluginYouDong_t3100552560 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginYouDong::ReqSDKHttpLogin()
extern "C"  Dictionary_2_t827649927 * PluginYouDong_ReqSDKHttpLogin_m2801282604 (PluginYouDong_t3100552560 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginYouDong::IsLoginSuccess(System.Boolean)
extern "C"  bool PluginYouDong_IsLoginSuccess_m2028820496 (PluginYouDong_t3100552560 * __this, bool ___isOpenUseLogin0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYouDong::OpenUserLogin()
extern "C"  void PluginYouDong_OpenUserLogin_m2142292301 (PluginYouDong_t3100552560 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYouDong::OnLoginSuccess(System.String)
extern "C"  void PluginYouDong_OnLoginSuccess_m391924416 (PluginYouDong_t3100552560 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYouDong::OnLogoutSuccess(System.String)
extern "C"  void PluginYouDong_OnLogoutSuccess_m3563264975 (PluginYouDong_t3100552560 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYouDong::ResLogoutSuccess(System.String)
extern "C"  void PluginYouDong_ResLogoutSuccess_m4041704000 (PluginYouDong_t3100552560 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYouDong::UserPay(CEvent.ZEvent)
extern "C"  void PluginYouDong_UserPay_m1521035653 (PluginYouDong_t3100552560 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYouDong::OnPaySuccess(System.String)
extern "C"  void PluginYouDong_OnPaySuccess_m2589351167 (PluginYouDong_t3100552560 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYouDong::OnPayFail(System.String)
extern "C"  void PluginYouDong_OnPayFail_m1881230498 (PluginYouDong_t3100552560 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYouDong::OnEnterGame(CEvent.ZEvent)
extern "C"  void PluginYouDong_OnEnterGame_m3672135895 (PluginYouDong_t3100552560 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYouDong::OnCreatRole(CEvent.ZEvent)
extern "C"  void PluginYouDong_OnCreatRole_m206584450 (PluginYouDong_t3100552560 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYouDong::OnLevelUp(CEvent.ZEvent)
extern "C"  void PluginYouDong_OnLevelUp_m4167587746 (PluginYouDong_t3100552560 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYouDong::ClollectData(System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginYouDong_ClollectData_m3315076575 (PluginYouDong_t3100552560 * __this, String_t* ___type0, String_t* ___serverid1, String_t* ___servername2, String_t* ___roleid3, String_t* ___rolename4, String_t* ___rolelevel5, String_t* ___roleGold6, String_t* ___VIPLv7, String_t* ___GameName8, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYouDong::switchAcc()
extern "C"  void PluginYouDong_switchAcc_m3285178534 (PluginYouDong_t3100552560 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYouDong::login()
extern "C"  void PluginYouDong_login_m2596427874 (PluginYouDong_t3100552560 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYouDong::goZF(System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginYouDong_goZF_m1689240737 (PluginYouDong_t3100552560 * __this, String_t* ___amount0, String_t* ___orderId1, String_t* ___extra2, String_t* ___tempId3, String_t* ___serverId4, String_t* ___serverName5, String_t* ___roleId6, String_t* ___roleName7, String_t* ___setProductNumber8, String_t* ___setProductName9, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYouDong::coloectData(System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginYouDong_coloectData_m710246944 (PluginYouDong_t3100552560 * __this, String_t* ___serverId0, String_t* ___servername1, String_t* ___roleid2, String_t* ___rolename3, String_t* ___rolelevel4, String_t* ___ctime5, String_t* ___type6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYouDong::<ResLogoutSuccess>m__475()
extern "C"  void PluginYouDong_U3CResLogoutSuccessU3Em__475_m2843810711 (PluginYouDong_t3100552560 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYouDong::ilo_AddEventListener1(System.String,CEvent.EventFunc`1<CEvent.ZEvent>)
extern "C"  void PluginYouDong_ilo_AddEventListener1_m897371033 (Il2CppObject * __this /* static, unused */, String_t* ___eventType0, EventFunc_1_t1114914310 * ___fun1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionMgr PluginYouDong::ilo_get_Instance2()
extern "C"  VersionMgr_t1322950208 * PluginYouDong_ilo_get_Instance2_m594035223 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYouDong::ilo_OpenUserLogin3(PluginYouDong)
extern "C"  void PluginYouDong_ilo_OpenUserLogin3_m4096447559 (Il2CppObject * __this /* static, unused */, PluginYouDong_t3100552560 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PluginsSdkMgr PluginYouDong::ilo_get_PluginsSdkMgr4()
extern "C"  PluginsSdkMgr_t3884624670 * PluginYouDong_ilo_get_PluginsSdkMgr4_m3999029894 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mihua.SDK.PayInfo PluginYouDong::ilo_Parse5(System.Object[])
extern "C"  PayInfo_t1775308120 * PluginYouDong_ilo_Parse5_m4057957179 (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t1108656482* ___payOjs0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYouDong::ilo_DispatchEvent6(CEvent.ZEvent)
extern "C"  void PluginYouDong_ilo_DispatchEvent6_m236554025 (Il2CppObject * __this /* static, unused */, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mihua.SDK.EnterGameInfo PluginYouDong::ilo_Parse7(System.Object[])
extern "C"  EnterGameInfo_t1897532954 * PluginYouDong_ilo_Parse7_m4050989367 (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t1108656482* ___crtRoleOjs0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYouDong::ilo_ClollectData8(PluginYouDong,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginYouDong_ilo_ClollectData8_m2898409050 (Il2CppObject * __this /* static, unused */, PluginYouDong_t3100552560 * ____this0, String_t* ___type1, String_t* ___serverid2, String_t* ___servername3, String_t* ___roleid4, String_t* ___rolename5, String_t* ___rolelevel6, String_t* ___roleGold7, String_t* ___VIPLv8, String_t* ___GameName9, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYouDong::ilo_coloectData9(PluginYouDong,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginYouDong_ilo_coloectData9_m3977412450 (Il2CppObject * __this /* static, unused */, PluginYouDong_t3100552560 * ____this0, String_t* ___serverId1, String_t* ___servername2, String_t* ___roleid3, String_t* ___rolename4, String_t* ___rolelevel5, String_t* ___ctime6, String_t* ___type7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
