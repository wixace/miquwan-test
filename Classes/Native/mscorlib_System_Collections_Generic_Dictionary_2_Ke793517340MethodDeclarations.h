﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K1499758504MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.UInt32,System.Collections.IEnumerator>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define KeyCollection__ctor_m1834716241(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t793517340 *, Dictionary_2_t3461725185 *, const MethodInfo*))KeyCollection__ctor_m4137106658_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.UInt32,System.Collections.IEnumerator>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1334156901(__this, ___item0, method) ((  void (*) (KeyCollection_t793517340 *, uint32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3142588212_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.UInt32,System.Collections.IEnumerator>::System.Collections.Generic.ICollection<TKey>.Clear()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m2767853852(__this, method) ((  void (*) (KeyCollection_t793517340 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m660240811_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.UInt32,System.Collections.IEnumerator>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1171399177(__this, ___item0, method) ((  bool (*) (KeyCollection_t793517340 *, uint32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m3107174486_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.UInt32,System.Collections.IEnumerator>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m690061742(__this, ___item0, method) ((  bool (*) (KeyCollection_t793517340 *, uint32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2292074299_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.UInt32,System.Collections.IEnumerator>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m3738248622(__this, method) ((  Il2CppObject* (*) (KeyCollection_t793517340 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m3531386983_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.UInt32,System.Collections.IEnumerator>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define KeyCollection_System_Collections_ICollection_CopyTo_m2689807246(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t793517340 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m4032050589_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.UInt32,System.Collections.IEnumerator>::System.Collections.IEnumerable.GetEnumerator()
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m131642141(__this, method) ((  Il2CppObject * (*) (KeyCollection_t793517340 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m2531852888_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.UInt32,System.Collections.IEnumerator>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m3314236202(__this, method) ((  bool (*) (KeyCollection_t793517340 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m3873468983_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.UInt32,System.Collections.IEnumerator>::System.Collections.ICollection.get_IsSynchronized()
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m2728087644(__this, method) ((  bool (*) (KeyCollection_t793517340 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m4199187369_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.UInt32,System.Collections.IEnumerator>::System.Collections.ICollection.get_SyncRoot()
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m1941227982(__this, method) ((  Il2CppObject * (*) (KeyCollection_t793517340 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m3281150229_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.UInt32,System.Collections.IEnumerator>::CopyTo(TKey[],System.Int32)
#define KeyCollection_CopyTo_m709261062(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t793517340 *, UInt32U5BU5D_t3230734560*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m2183179159_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.UInt32,System.Collections.IEnumerator>::GetEnumerator()
#define KeyCollection_GetEnumerator_m2725533459(__this, method) ((  Enumerator_t4076661239  (*) (KeyCollection_t793517340 *, const MethodInfo*))KeyCollection_GetEnumerator_m4267000826_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.UInt32,System.Collections.IEnumerator>::get_Count()
#define KeyCollection_get_Count_m65340310(__this, method) ((  int32_t (*) (KeyCollection_t793517340 *, const MethodInfo*))KeyCollection_get_Count_m168011375_gshared)(__this, method)
