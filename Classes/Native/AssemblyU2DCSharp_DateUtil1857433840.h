﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_DateTime4283661327.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DateUtil
struct  DateUtil_t1857433840  : public Il2CppObject
{
public:

public:
};

struct DateUtil_t1857433840_StaticFields
{
public:
	// System.Double DateUtil::SecondsOf1970
	double ___SecondsOf1970_1;
	// System.DateTime DateUtil::inputDate
	DateTime_t4283661327  ___inputDate_2;

public:
	inline static int32_t get_offset_of_SecondsOf1970_1() { return static_cast<int32_t>(offsetof(DateUtil_t1857433840_StaticFields, ___SecondsOf1970_1)); }
	inline double get_SecondsOf1970_1() const { return ___SecondsOf1970_1; }
	inline double* get_address_of_SecondsOf1970_1() { return &___SecondsOf1970_1; }
	inline void set_SecondsOf1970_1(double value)
	{
		___SecondsOf1970_1 = value;
	}

	inline static int32_t get_offset_of_inputDate_2() { return static_cast<int32_t>(offsetof(DateUtil_t1857433840_StaticFields, ___inputDate_2)); }
	inline DateTime_t4283661327  get_inputDate_2() const { return ___inputDate_2; }
	inline DateTime_t4283661327 * get_address_of_inputDate_2() { return &___inputDate_2; }
	inline void set_inputDate_2(DateTime_t4283661327  value)
	{
		___inputDate_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
