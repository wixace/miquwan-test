﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OnScanStatus
struct OnScanStatus_t2412749870;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "AssemblyU2DCSharp_Pathfinding_Progress2476786339.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void OnScanStatus::.ctor(System.Object,System.IntPtr)
extern "C"  void OnScanStatus__ctor_m3853977045 (OnScanStatus_t2412749870 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnScanStatus::Invoke(Pathfinding.Progress)
extern "C"  void OnScanStatus_Invoke_m1982171384 (OnScanStatus_t2412749870 * __this, Progress_t2476786339  ___progress0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult OnScanStatus::BeginInvoke(Pathfinding.Progress,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnScanStatus_BeginInvoke_m760531201 (OnScanStatus_t2412749870 * __this, Progress_t2476786339  ___progress0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnScanStatus::EndInvoke(System.IAsyncResult)
extern "C"  void OnScanStatus_EndInvoke_m1857596005 (OnScanStatus_t2412749870 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
