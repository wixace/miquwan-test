﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pomelo.DotNetClient.StateObject
struct StateObject_t4115849974;

#include "codegen/il2cpp-codegen.h"

// System.Void Pomelo.DotNetClient.StateObject::.ctor()
extern "C"  void StateObject__ctor_m3553409814 (StateObject_t4115849974 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
