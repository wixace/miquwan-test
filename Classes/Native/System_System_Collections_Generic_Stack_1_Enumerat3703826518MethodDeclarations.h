﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_Stack_1_Enumerat2532196025MethodDeclarations.h"

// System.Void System.Collections.Generic.Stack`1/Enumerator<Pathfinding.Poly2Tri.Polygon>::.ctor(System.Collections.Generic.Stack`1<T>)
#define Enumerator__ctor_m2595791676(__this, ___t0, method) ((  void (*) (Enumerator_t3703826518 *, Stack_1_t4146040492 *, const MethodInfo*))Enumerator__ctor_m1003414509_gshared)(__this, ___t0, method)
// System.Void System.Collections.Generic.Stack`1/Enumerator<Pathfinding.Poly2Tri.Polygon>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2044354688(__this, method) ((  void (*) (Enumerator_t3703826518 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3054975473_gshared)(__this, method)
// System.Object System.Collections.Generic.Stack`1/Enumerator<Pathfinding.Poly2Tri.Polygon>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2761283884(__this, method) ((  Il2CppObject * (*) (Enumerator_t3703826518 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2470832103_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1/Enumerator<Pathfinding.Poly2Tri.Polygon>::Dispose()
#define Enumerator_Dispose_m1262187255(__this, method) ((  void (*) (Enumerator_t3703826518 *, const MethodInfo*))Enumerator_Dispose_m1634653158_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Stack`1/Enumerator<Pathfinding.Poly2Tri.Polygon>::MoveNext()
#define Enumerator_MoveNext_m3170731096(__this, method) ((  bool (*) (Enumerator_t3703826518 *, const MethodInfo*))Enumerator_MoveNext_m3012756789_gshared)(__this, method)
// T System.Collections.Generic.Stack`1/Enumerator<Pathfinding.Poly2Tri.Polygon>::get_Current()
#define Enumerator_get_Current_m1282493479(__this, method) ((  Polygon_t1047479568 * (*) (Enumerator_t3703826518 *, const MethodInfo*))Enumerator_get_Current_m2483819640_gshared)(__this, method)
