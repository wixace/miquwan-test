﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GenericTypeCache
struct GenericTypeCache_t3056017521;
// GenericTypeCache/TypeMembers
struct TypeMembers_t2369396673;
// System.Type
struct Type_t;
// System.String
struct String_t;
// System.Reflection.ParameterInfo[]
struct ParameterInfoU5BU5D_t2015293532;
// System.String[]
struct StringU5BU5D_t4054002952;
// TypeFlag[]
struct TypeFlagU5BU5D_t693965763;
// System.Reflection.ConstructorInfo
struct ConstructorInfo_t4136801618;
// ConstructorID
struct ConstructorID_t3348888181;
// System.Reflection.FieldInfo
struct FieldInfo_t;
// FieldID
struct FieldID_t803400565;
// System.Reflection.PropertyInfo
struct PropertyInfo_t;
// PropertyID
struct PropertyID_t1067426256;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// MethodID
struct MethodID_t3916401116;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Type2863145774.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_TypeFlag3682875878.h"
#include "AssemblyU2DCSharp_ConstructorID3348888181.h"
#include "AssemblyU2DCSharp_FieldID803400565.h"
#include "AssemblyU2DCSharp_PropertyID1067426256.h"
#include "AssemblyU2DCSharp_MethodID3916401116.h"

// System.Void GenericTypeCache::.ctor()
extern "C"  void GenericTypeCache__ctor_m815592778 (GenericTypeCache_t3056017521 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GenericTypeCache::.cctor()
extern "C"  void GenericTypeCache__cctor_m3326443427 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GenericTypeCache/TypeMembers GenericTypeCache::getMembers(System.Type)
extern "C"  TypeMembers_t2369396673 * GenericTypeCache_getMembers_m3813761600 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GenericTypeCache::matchReturnType(System.Type,System.String,TypeFlag)
extern "C"  bool GenericTypeCache_matchReturnType_m1939435598 (Il2CppObject * __this /* static, unused */, Type_t * ___targetType0, String_t* ___typeName1, int32_t ___flag2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GenericTypeCache::matchParameters(System.Reflection.ParameterInfo[],System.String[],TypeFlag[])
extern "C"  bool GenericTypeCache_matchParameters_m1626498986 (Il2CppObject * __this /* static, unused */, ParameterInfoU5BU5D_t2015293532* ___pi0, StringU5BU5D_t4054002952* ___paramTypeNames1, TypeFlagU5BU5D_t693965763* ___typeFlags2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.ConstructorInfo GenericTypeCache::getConstructor(System.Type,ConstructorID)
extern "C"  ConstructorInfo_t4136801618 * GenericTypeCache_getConstructor_m822570819 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, ConstructorID_t3348888181 * ___id1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.FieldInfo GenericTypeCache::getField(System.Type,FieldID)
extern "C"  FieldInfo_t * GenericTypeCache_getField_m4050478275 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, FieldID_t803400565 * ___id1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.PropertyInfo GenericTypeCache::getProperty(System.Type,PropertyID)
extern "C"  PropertyInfo_t * GenericTypeCache_getProperty_m2769033530 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, PropertyID_t1067426256 * ___id1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo GenericTypeCache::getMethod(System.Type,MethodID)
extern "C"  MethodInfo_t * GenericTypeCache_getMethod_m683703430 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, MethodID_t3916401116 * ___id1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GenericTypeCache::ilo_matchParameters1(System.Reflection.ParameterInfo[],System.String[],TypeFlag[])
extern "C"  bool GenericTypeCache_ilo_matchParameters1_m1721253386 (Il2CppObject * __this /* static, unused */, ParameterInfoU5BU5D_t2015293532* ___pi0, StringU5BU5D_t4054002952* ___paramTypeNames1, TypeFlagU5BU5D_t693965763* ___typeFlags2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GenericTypeCache::ilo_matchReturnType2(System.Type,System.String,TypeFlag)
extern "C"  bool GenericTypeCache_ilo_matchReturnType2_m3581802735 (Il2CppObject * __this /* static, unused */, Type_t * ___targetType0, String_t* ___typeName1, int32_t ___flag2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
