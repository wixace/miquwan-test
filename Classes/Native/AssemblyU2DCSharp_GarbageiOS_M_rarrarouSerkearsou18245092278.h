﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_rarrarouSerkearsou181
struct  M_rarrarouSerkearsou181_t245092278  : public Il2CppObject
{
public:
	// System.UInt32 GarbageiOS.M_rarrarouSerkearsou181::_louteaWoonow
	uint32_t ____louteaWoonow_0;
	// System.Single GarbageiOS.M_rarrarouSerkearsou181::_weesairgiBijou
	float ____weesairgiBijou_1;
	// System.Int32 GarbageiOS.M_rarrarouSerkearsou181::_fakurrayDallce
	int32_t ____fakurrayDallce_2;
	// System.UInt32 GarbageiOS.M_rarrarouSerkearsou181::_waycay
	uint32_t ____waycay_3;
	// System.UInt32 GarbageiOS.M_rarrarouSerkearsou181::_niswhoo
	uint32_t ____niswhoo_4;
	// System.Int32 GarbageiOS.M_rarrarouSerkearsou181::_furlempouMearkorru
	int32_t ____furlempouMearkorru_5;
	// System.Single GarbageiOS.M_rarrarouSerkearsou181::_horto
	float ____horto_6;
	// System.Single GarbageiOS.M_rarrarouSerkearsou181::_salldiJouraijea
	float ____salldiJouraijea_7;
	// System.Boolean GarbageiOS.M_rarrarouSerkearsou181::_rowmuHairke
	bool ____rowmuHairke_8;
	// System.UInt32 GarbageiOS.M_rarrarouSerkearsou181::_heljalay
	uint32_t ____heljalay_9;
	// System.Single GarbageiOS.M_rarrarouSerkearsou181::_whalcetoo
	float ____whalcetoo_10;
	// System.String GarbageiOS.M_rarrarouSerkearsou181::_nuwisooJeane
	String_t* ____nuwisooJeane_11;
	// System.String GarbageiOS.M_rarrarouSerkearsou181::_relsurmouYahujou
	String_t* ____relsurmouYahujou_12;
	// System.Boolean GarbageiOS.M_rarrarouSerkearsou181::_wharair
	bool ____wharair_13;
	// System.Single GarbageiOS.M_rarrarouSerkearsou181::_dedeKokerechor
	float ____dedeKokerechor_14;
	// System.Boolean GarbageiOS.M_rarrarouSerkearsou181::_kelooboCowcallyu
	bool ____kelooboCowcallyu_15;
	// System.String GarbageiOS.M_rarrarouSerkearsou181::_limirSelkelmair
	String_t* ____limirSelkelmair_16;
	// System.Boolean GarbageiOS.M_rarrarouSerkearsou181::_saswer
	bool ____saswer_17;

public:
	inline static int32_t get_offset_of__louteaWoonow_0() { return static_cast<int32_t>(offsetof(M_rarrarouSerkearsou181_t245092278, ____louteaWoonow_0)); }
	inline uint32_t get__louteaWoonow_0() const { return ____louteaWoonow_0; }
	inline uint32_t* get_address_of__louteaWoonow_0() { return &____louteaWoonow_0; }
	inline void set__louteaWoonow_0(uint32_t value)
	{
		____louteaWoonow_0 = value;
	}

	inline static int32_t get_offset_of__weesairgiBijou_1() { return static_cast<int32_t>(offsetof(M_rarrarouSerkearsou181_t245092278, ____weesairgiBijou_1)); }
	inline float get__weesairgiBijou_1() const { return ____weesairgiBijou_1; }
	inline float* get_address_of__weesairgiBijou_1() { return &____weesairgiBijou_1; }
	inline void set__weesairgiBijou_1(float value)
	{
		____weesairgiBijou_1 = value;
	}

	inline static int32_t get_offset_of__fakurrayDallce_2() { return static_cast<int32_t>(offsetof(M_rarrarouSerkearsou181_t245092278, ____fakurrayDallce_2)); }
	inline int32_t get__fakurrayDallce_2() const { return ____fakurrayDallce_2; }
	inline int32_t* get_address_of__fakurrayDallce_2() { return &____fakurrayDallce_2; }
	inline void set__fakurrayDallce_2(int32_t value)
	{
		____fakurrayDallce_2 = value;
	}

	inline static int32_t get_offset_of__waycay_3() { return static_cast<int32_t>(offsetof(M_rarrarouSerkearsou181_t245092278, ____waycay_3)); }
	inline uint32_t get__waycay_3() const { return ____waycay_3; }
	inline uint32_t* get_address_of__waycay_3() { return &____waycay_3; }
	inline void set__waycay_3(uint32_t value)
	{
		____waycay_3 = value;
	}

	inline static int32_t get_offset_of__niswhoo_4() { return static_cast<int32_t>(offsetof(M_rarrarouSerkearsou181_t245092278, ____niswhoo_4)); }
	inline uint32_t get__niswhoo_4() const { return ____niswhoo_4; }
	inline uint32_t* get_address_of__niswhoo_4() { return &____niswhoo_4; }
	inline void set__niswhoo_4(uint32_t value)
	{
		____niswhoo_4 = value;
	}

	inline static int32_t get_offset_of__furlempouMearkorru_5() { return static_cast<int32_t>(offsetof(M_rarrarouSerkearsou181_t245092278, ____furlempouMearkorru_5)); }
	inline int32_t get__furlempouMearkorru_5() const { return ____furlempouMearkorru_5; }
	inline int32_t* get_address_of__furlempouMearkorru_5() { return &____furlempouMearkorru_5; }
	inline void set__furlempouMearkorru_5(int32_t value)
	{
		____furlempouMearkorru_5 = value;
	}

	inline static int32_t get_offset_of__horto_6() { return static_cast<int32_t>(offsetof(M_rarrarouSerkearsou181_t245092278, ____horto_6)); }
	inline float get__horto_6() const { return ____horto_6; }
	inline float* get_address_of__horto_6() { return &____horto_6; }
	inline void set__horto_6(float value)
	{
		____horto_6 = value;
	}

	inline static int32_t get_offset_of__salldiJouraijea_7() { return static_cast<int32_t>(offsetof(M_rarrarouSerkearsou181_t245092278, ____salldiJouraijea_7)); }
	inline float get__salldiJouraijea_7() const { return ____salldiJouraijea_7; }
	inline float* get_address_of__salldiJouraijea_7() { return &____salldiJouraijea_7; }
	inline void set__salldiJouraijea_7(float value)
	{
		____salldiJouraijea_7 = value;
	}

	inline static int32_t get_offset_of__rowmuHairke_8() { return static_cast<int32_t>(offsetof(M_rarrarouSerkearsou181_t245092278, ____rowmuHairke_8)); }
	inline bool get__rowmuHairke_8() const { return ____rowmuHairke_8; }
	inline bool* get_address_of__rowmuHairke_8() { return &____rowmuHairke_8; }
	inline void set__rowmuHairke_8(bool value)
	{
		____rowmuHairke_8 = value;
	}

	inline static int32_t get_offset_of__heljalay_9() { return static_cast<int32_t>(offsetof(M_rarrarouSerkearsou181_t245092278, ____heljalay_9)); }
	inline uint32_t get__heljalay_9() const { return ____heljalay_9; }
	inline uint32_t* get_address_of__heljalay_9() { return &____heljalay_9; }
	inline void set__heljalay_9(uint32_t value)
	{
		____heljalay_9 = value;
	}

	inline static int32_t get_offset_of__whalcetoo_10() { return static_cast<int32_t>(offsetof(M_rarrarouSerkearsou181_t245092278, ____whalcetoo_10)); }
	inline float get__whalcetoo_10() const { return ____whalcetoo_10; }
	inline float* get_address_of__whalcetoo_10() { return &____whalcetoo_10; }
	inline void set__whalcetoo_10(float value)
	{
		____whalcetoo_10 = value;
	}

	inline static int32_t get_offset_of__nuwisooJeane_11() { return static_cast<int32_t>(offsetof(M_rarrarouSerkearsou181_t245092278, ____nuwisooJeane_11)); }
	inline String_t* get__nuwisooJeane_11() const { return ____nuwisooJeane_11; }
	inline String_t** get_address_of__nuwisooJeane_11() { return &____nuwisooJeane_11; }
	inline void set__nuwisooJeane_11(String_t* value)
	{
		____nuwisooJeane_11 = value;
		Il2CppCodeGenWriteBarrier(&____nuwisooJeane_11, value);
	}

	inline static int32_t get_offset_of__relsurmouYahujou_12() { return static_cast<int32_t>(offsetof(M_rarrarouSerkearsou181_t245092278, ____relsurmouYahujou_12)); }
	inline String_t* get__relsurmouYahujou_12() const { return ____relsurmouYahujou_12; }
	inline String_t** get_address_of__relsurmouYahujou_12() { return &____relsurmouYahujou_12; }
	inline void set__relsurmouYahujou_12(String_t* value)
	{
		____relsurmouYahujou_12 = value;
		Il2CppCodeGenWriteBarrier(&____relsurmouYahujou_12, value);
	}

	inline static int32_t get_offset_of__wharair_13() { return static_cast<int32_t>(offsetof(M_rarrarouSerkearsou181_t245092278, ____wharair_13)); }
	inline bool get__wharair_13() const { return ____wharair_13; }
	inline bool* get_address_of__wharair_13() { return &____wharair_13; }
	inline void set__wharair_13(bool value)
	{
		____wharair_13 = value;
	}

	inline static int32_t get_offset_of__dedeKokerechor_14() { return static_cast<int32_t>(offsetof(M_rarrarouSerkearsou181_t245092278, ____dedeKokerechor_14)); }
	inline float get__dedeKokerechor_14() const { return ____dedeKokerechor_14; }
	inline float* get_address_of__dedeKokerechor_14() { return &____dedeKokerechor_14; }
	inline void set__dedeKokerechor_14(float value)
	{
		____dedeKokerechor_14 = value;
	}

	inline static int32_t get_offset_of__kelooboCowcallyu_15() { return static_cast<int32_t>(offsetof(M_rarrarouSerkearsou181_t245092278, ____kelooboCowcallyu_15)); }
	inline bool get__kelooboCowcallyu_15() const { return ____kelooboCowcallyu_15; }
	inline bool* get_address_of__kelooboCowcallyu_15() { return &____kelooboCowcallyu_15; }
	inline void set__kelooboCowcallyu_15(bool value)
	{
		____kelooboCowcallyu_15 = value;
	}

	inline static int32_t get_offset_of__limirSelkelmair_16() { return static_cast<int32_t>(offsetof(M_rarrarouSerkearsou181_t245092278, ____limirSelkelmair_16)); }
	inline String_t* get__limirSelkelmair_16() const { return ____limirSelkelmair_16; }
	inline String_t** get_address_of__limirSelkelmair_16() { return &____limirSelkelmair_16; }
	inline void set__limirSelkelmair_16(String_t* value)
	{
		____limirSelkelmair_16 = value;
		Il2CppCodeGenWriteBarrier(&____limirSelkelmair_16, value);
	}

	inline static int32_t get_offset_of__saswer_17() { return static_cast<int32_t>(offsetof(M_rarrarouSerkearsou181_t245092278, ____saswer_17)); }
	inline bool get__saswer_17() const { return ____saswer_17; }
	inline bool* get_address_of__saswer_17() { return &____saswer_17; }
	inline void set__saswer_17(bool value)
	{
		____saswer_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
