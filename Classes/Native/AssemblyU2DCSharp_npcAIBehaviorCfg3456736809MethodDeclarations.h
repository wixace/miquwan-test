﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// npcAIBehaviorCfg
struct npcAIBehaviorCfg_t3456736809;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;

#include "codegen/il2cpp-codegen.h"

// System.Void npcAIBehaviorCfg::.ctor()
extern "C"  void npcAIBehaviorCfg__ctor_m1207985810 (npcAIBehaviorCfg_t3456736809 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void npcAIBehaviorCfg::Init(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C"  void npcAIBehaviorCfg_Init_m1900326835 (npcAIBehaviorCfg_t3456736809 * __this, Dictionary_2_t827649927 * ____info0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
