﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En498386530MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,ProductsCfgMgr/ProductInfo>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m2367465361(__this, ___dictionary0, method) ((  void (*) (Enumerator_t3443733000 *, Dictionary_2_t2126409608 *, const MethodInfo*))Enumerator__ctor_m2045912099_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,ProductsCfgMgr/ProductInfo>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m4195610042(__this, method) ((  Il2CppObject * (*) (Enumerator_t3443733000 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m4277870440_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,ProductsCfgMgr/ProductInfo>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m167784580(__this, method) ((  void (*) (Enumerator_t3443733000 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3521232050_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,ProductsCfgMgr/ProductInfo>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2364110843(__this, method) ((  DictionaryEntry_t1751606614  (*) (Enumerator_t3443733000 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2593332137_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,ProductsCfgMgr/ProductInfo>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1099200278(__this, method) ((  Il2CppObject * (*) (Enumerator_t3443733000 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3271503428_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,ProductsCfgMgr/ProductInfo>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m817948712(__this, method) ((  Il2CppObject * (*) (Enumerator_t3443733000 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1047170006_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,ProductsCfgMgr/ProductInfo>::MoveNext()
#define Enumerator_MoveNext_m3587565748(__this, method) ((  bool (*) (Enumerator_t3443733000 *, const MethodInfo*))Enumerator_MoveNext_m923395554_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,ProductsCfgMgr/ProductInfo>::get_Current()
#define Enumerator_get_Current_m4178626824(__this, method) ((  KeyValuePair_2_t2025190314  (*) (Enumerator_t3443733000 *, const MethodInfo*))Enumerator_get_Current_m2585040154_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,ProductsCfgMgr/ProductInfo>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m2672096189(__this, method) ((  String_t* (*) (Enumerator_t3443733000 *, const MethodInfo*))Enumerator_get_CurrentKey_m405132907_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,ProductsCfgMgr/ProductInfo>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m2666430013(__this, method) ((  ProductInfo_t1305991238  (*) (Enumerator_t3443733000 *, const MethodInfo*))Enumerator_get_CurrentValue_m1663135083_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,ProductsCfgMgr/ProductInfo>::Reset()
#define Enumerator_Reset_m509507683(__this, method) ((  void (*) (Enumerator_t3443733000 *, const MethodInfo*))Enumerator_Reset_m2431203829_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,ProductsCfgMgr/ProductInfo>::VerifyState()
#define Enumerator_VerifyState_m4072714796(__this, method) ((  void (*) (Enumerator_t3443733000 *, const MethodInfo*))Enumerator_VerifyState_m2479128126_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,ProductsCfgMgr/ProductInfo>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m3908584084(__this, method) ((  void (*) (Enumerator_t3443733000 *, const MethodInfo*))Enumerator_VerifyCurrent_m1480151590_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,ProductsCfgMgr/ProductInfo>::Dispose()
#define Enumerator_Dispose_m2534040691(__this, method) ((  void (*) (Enumerator_t3443733000 *, const MethodInfo*))Enumerator_Dispose_m2448099717_gshared)(__this, method)
