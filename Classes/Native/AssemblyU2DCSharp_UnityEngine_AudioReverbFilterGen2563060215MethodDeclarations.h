﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_AudioReverbFilterGenerated
struct UnityEngine_AudioReverbFilterGenerated_t2563060215;
// JSVCall
struct JSVCall_t3708497963;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"

// System.Void UnityEngine_AudioReverbFilterGenerated::.ctor()
extern "C"  void UnityEngine_AudioReverbFilterGenerated__ctor_m1148131460 (UnityEngine_AudioReverbFilterGenerated_t2563060215 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AudioReverbFilterGenerated::AudioReverbFilter_AudioReverbFilter1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AudioReverbFilterGenerated_AudioReverbFilter_AudioReverbFilter1_m1967116724 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioReverbFilterGenerated::AudioReverbFilter_reverbPreset(JSVCall)
extern "C"  void UnityEngine_AudioReverbFilterGenerated_AudioReverbFilter_reverbPreset_m3375934935 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioReverbFilterGenerated::AudioReverbFilter_dryLevel(JSVCall)
extern "C"  void UnityEngine_AudioReverbFilterGenerated_AudioReverbFilter_dryLevel_m775703183 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioReverbFilterGenerated::AudioReverbFilter_room(JSVCall)
extern "C"  void UnityEngine_AudioReverbFilterGenerated_AudioReverbFilter_room_m2307980685 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioReverbFilterGenerated::AudioReverbFilter_roomHF(JSVCall)
extern "C"  void UnityEngine_AudioReverbFilterGenerated_AudioReverbFilter_roomHF_m2818854031 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioReverbFilterGenerated::AudioReverbFilter_roomRolloff(JSVCall)
extern "C"  void UnityEngine_AudioReverbFilterGenerated_AudioReverbFilter_roomRolloff_m566396973 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioReverbFilterGenerated::AudioReverbFilter_decayTime(JSVCall)
extern "C"  void UnityEngine_AudioReverbFilterGenerated_AudioReverbFilter_decayTime_m2252856957 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioReverbFilterGenerated::AudioReverbFilter_decayHFRatio(JSVCall)
extern "C"  void UnityEngine_AudioReverbFilterGenerated_AudioReverbFilter_decayHFRatio_m742442677 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioReverbFilterGenerated::AudioReverbFilter_reflectionsLevel(JSVCall)
extern "C"  void UnityEngine_AudioReverbFilterGenerated_AudioReverbFilter_reflectionsLevel_m94010828 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioReverbFilterGenerated::AudioReverbFilter_reflectionsDelay(JSVCall)
extern "C"  void UnityEngine_AudioReverbFilterGenerated_AudioReverbFilter_reflectionsDelay_m3185815213 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioReverbFilterGenerated::AudioReverbFilter_reverbLevel(JSVCall)
extern "C"  void UnityEngine_AudioReverbFilterGenerated_AudioReverbFilter_reverbLevel_m792814642 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioReverbFilterGenerated::AudioReverbFilter_reverbDelay(JSVCall)
extern "C"  void UnityEngine_AudioReverbFilterGenerated_AudioReverbFilter_reverbDelay_m3884619027 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioReverbFilterGenerated::AudioReverbFilter_diffusion(JSVCall)
extern "C"  void UnityEngine_AudioReverbFilterGenerated_AudioReverbFilter_diffusion_m1236469279 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioReverbFilterGenerated::AudioReverbFilter_density(JSVCall)
extern "C"  void UnityEngine_AudioReverbFilterGenerated_AudioReverbFilter_density_m1735715804 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioReverbFilterGenerated::AudioReverbFilter_hfReference(JSVCall)
extern "C"  void UnityEngine_AudioReverbFilterGenerated_AudioReverbFilter_hfReference_m946124663 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioReverbFilterGenerated::AudioReverbFilter_roomLF(JSVCall)
extern "C"  void UnityEngine_AudioReverbFilterGenerated_AudioReverbFilter_roomLF_m4220983187 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioReverbFilterGenerated::AudioReverbFilter_lfReference(JSVCall)
extern "C"  void UnityEngine_AudioReverbFilterGenerated_AudioReverbFilter_lfReference_m3605795059 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioReverbFilterGenerated::__Register()
extern "C"  void UnityEngine_AudioReverbFilterGenerated___Register_m1679890115 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_AudioReverbFilterGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UnityEngine_AudioReverbFilterGenerated_ilo_getObject1_m307856014 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine_AudioReverbFilterGenerated::ilo_getSingle2(System.Int32)
extern "C"  float UnityEngine_AudioReverbFilterGenerated_ilo_getSingle2_m406795364 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioReverbFilterGenerated::ilo_setSingle3(System.Int32,System.Single)
extern "C"  void UnityEngine_AudioReverbFilterGenerated_ilo_setSingle3_m509205106 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
