﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_GUILayoutOptionGenerated
struct UnityEngine_GUILayoutOptionGenerated_t3146637053;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine_GUILayoutOptionGenerated::.ctor()
extern "C"  void UnityEngine_GUILayoutOptionGenerated__ctor_m4109490238 (UnityEngine_GUILayoutOptionGenerated_t3146637053 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUILayoutOptionGenerated::__Register()
extern "C"  void UnityEngine_GUILayoutOptionGenerated___Register_m3756202825 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
