﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._c7ddaba247e5c2461d57b7dbc6269161
struct _c7ddaba247e5c2461d57b7dbc6269161_t3415036309;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._c7ddaba247e5c2461d57b7dbc6269161::.ctor()
extern "C"  void _c7ddaba247e5c2461d57b7dbc6269161__ctor_m174050360 (_c7ddaba247e5c2461d57b7dbc6269161_t3415036309 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._c7ddaba247e5c2461d57b7dbc6269161::_c7ddaba247e5c2461d57b7dbc6269161m2(System.Int32)
extern "C"  int32_t _c7ddaba247e5c2461d57b7dbc6269161__c7ddaba247e5c2461d57b7dbc6269161m2_m4284842361 (_c7ddaba247e5c2461d57b7dbc6269161_t3415036309 * __this, int32_t ____c7ddaba247e5c2461d57b7dbc6269161a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._c7ddaba247e5c2461d57b7dbc6269161::_c7ddaba247e5c2461d57b7dbc6269161m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _c7ddaba247e5c2461d57b7dbc6269161__c7ddaba247e5c2461d57b7dbc6269161m_m1389543773 (_c7ddaba247e5c2461d57b7dbc6269161_t3415036309 * __this, int32_t ____c7ddaba247e5c2461d57b7dbc6269161a0, int32_t ____c7ddaba247e5c2461d57b7dbc6269161551, int32_t ____c7ddaba247e5c2461d57b7dbc6269161c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
