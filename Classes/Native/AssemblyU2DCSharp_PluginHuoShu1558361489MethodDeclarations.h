﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PluginHuoShu
struct PluginHuoShu_t1558361489;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// UnityEngine.WWWForm
struct WWWForm_t461342257;
// Mihua.SDK.PayInfo
struct PayInfo_t1775308120;
// System.String
struct String_t;
// CEvent.EventFunc`1<CEvent.ZEvent>
struct EventFunc_1_t1114914310;
// VersionMgr
struct VersionMgr_t1322950208;
// Mihua.SDK.CreatRoleInfo
struct CreatRoleInfo_t2858846543;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// Mihua.SDK.LvUpInfo
struct LvUpInfo_t411687177;
// PluginsSdkMgr
struct PluginsSdkMgr_t3884624670;
// System.Action
struct Action_t3771233898;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "AssemblyU2DCSharp_Mihua_SDK_PayInfo1775308120.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_PluginHuoShu1558361489.h"
#include "AssemblyU2DCSharp_PluginsSdkMgr3884624670.h"
#include "System_Core_System_Action3771233898.h"

// System.Void PluginHuoShu::.ctor()
extern "C"  void PluginHuoShu__ctor_m3552178218 (PluginHuoShu_t1558361489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginHuoShu::Init()
extern "C"  void PluginHuoShu_Init_m3252711658 (PluginHuoShu_t1558361489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginHuoShu::ReqSDKHttpLogin()
extern "C"  Dictionary_2_t827649927 * PluginHuoShu_ReqSDKHttpLogin_m2842950655 (PluginHuoShu_t1558361489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginHuoShu::IsLoginSuccess(System.Boolean)
extern "C"  bool PluginHuoShu_IsLoginSuccess_m1285554153 (PluginHuoShu_t1558361489 * __this, bool ___isOpenUseLogin0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginHuoShu::OpenUserLogin()
extern "C"  void PluginHuoShu_OpenUserLogin_m3216853116 (PluginHuoShu_t1558361489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginHuoShu::UserPay(CEvent.ZEvent)
extern "C"  void PluginHuoShu_UserPay_m4239853430 (PluginHuoShu_t1558361489 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.WWWForm PluginHuoShu::BuildOrderParam2WWWForm(Mihua.SDK.PayInfo)
extern "C"  WWWForm_t461342257 * PluginHuoShu_BuildOrderParam2WWWForm_m1658501111 (PluginHuoShu_t1558361489 * __this, PayInfo_t1775308120 * ___info0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginHuoShu::CreateRole(CEvent.ZEvent)
extern "C"  void PluginHuoShu_CreateRole_m915462095 (PluginHuoShu_t1558361489 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginHuoShu::EnterGame(CEvent.ZEvent)
extern "C"  void PluginHuoShu_EnterGame_m791729033 (PluginHuoShu_t1558361489 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginHuoShu::RoleUpgrade(CEvent.ZEvent)
extern "C"  void PluginHuoShu_RoleUpgrade_m977852397 (PluginHuoShu_t1558361489 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginHuoShu::OnLoginSuccess(System.String)
extern "C"  void PluginHuoShu_OnLoginSuccess_m2875657135 (PluginHuoShu_t1558361489 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginHuoShu::OnLogoutSuccess(System.String)
extern "C"  void PluginHuoShu_OnLogoutSuccess_m3249567936 (PluginHuoShu_t1558361489 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginHuoShu::OnLogout(System.String)
extern "C"  void PluginHuoShu_OnLogout_m30359167 (PluginHuoShu_t1558361489 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginHuoShu::SwitchAccount(System.String)
extern "C"  void PluginHuoShu_SwitchAccount_m3948572737 (PluginHuoShu_t1558361489 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginHuoShu::initSdk()
extern "C"  void PluginHuoShu_initSdk_m813443634 (PluginHuoShu_t1558361489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginHuoShu::login()
extern "C"  void PluginHuoShu_login_m3074193041 (PluginHuoShu_t1558361489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginHuoShu::logout()
extern "C"  void PluginHuoShu_logout_m816526468 (PluginHuoShu_t1558361489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginHuoShu::updateRoleInfo(System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginHuoShu_updateRoleInfo_m52006867 (PluginHuoShu_t1558361489 * __this, String_t* ___dataType0, String_t* ___serverID1, String_t* ___serverName2, String_t* ___roleID3, String_t* ___roleName4, String_t* ___roleLevel5, String_t* ___roleVip6, String_t* ___roleBalence7, String_t* ___partyName8, String_t* ___rolelevelCtime9, String_t* ___rolelevelMtime10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginHuoShu::pay(System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginHuoShu_pay_m2382565770 (PluginHuoShu_t1558361489 * __this, String_t* ___gameOrderId0, String_t* ___serverID1, String_t* ___key_serverName2, String_t* ___productId3, String_t* ___productName4, String_t* ___key_productdesc5, String_t* ___key_ext6, String_t* ___key_productPrice7, String_t* ___key_roleID8, String_t* ___key_roleName9, String_t* ___key_currencyName10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginHuoShu::<OnLogoutSuccess>m__42C()
extern "C"  void PluginHuoShu_U3COnLogoutSuccessU3Em__42C_m4046499168 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginHuoShu::<OnLogout>m__42D()
extern "C"  void PluginHuoShu_U3COnLogoutU3Em__42D_m3424176268 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginHuoShu::<SwitchAccount>m__42E()
extern "C"  void PluginHuoShu_U3CSwitchAccountU3Em__42E_m1107112867 (PluginHuoShu_t1558361489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginHuoShu::ilo_AddEventListener1(System.String,CEvent.EventFunc`1<CEvent.ZEvent>)
extern "C"  void PluginHuoShu_ilo_AddEventListener1_m67543946 (Il2CppObject * __this /* static, unused */, String_t* ___eventType0, EventFunc_1_t1114914310 * ___fun1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionMgr PluginHuoShu::ilo_get_Instance2()
extern "C"  VersionMgr_t1322950208 * PluginHuoShu_ilo_get_Instance2_m3465079340 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginHuoShu::ilo_initSdk3(PluginHuoShu)
extern "C"  void PluginHuoShu_ilo_initSdk3_m1007590501 (Il2CppObject * __this /* static, unused */, PluginHuoShu_t1558361489 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mihua.SDK.CreatRoleInfo PluginHuoShu::ilo_Parse4(System.Object[])
extern "C"  CreatRoleInfo_t2858846543 * PluginHuoShu_ilo_Parse4_m3470337236 (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t1108656482* ___crtRoleOjs0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginHuoShu::ilo_updateRoleInfo5(PluginHuoShu,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginHuoShu_ilo_updateRoleInfo5_m2298550304 (Il2CppObject * __this /* static, unused */, PluginHuoShu_t1558361489 * ____this0, String_t* ___dataType1, String_t* ___serverID2, String_t* ___serverName3, String_t* ___roleID4, String_t* ___roleName5, String_t* ___roleLevel6, String_t* ___roleVip7, String_t* ___roleBalence8, String_t* ___partyName9, String_t* ___rolelevelCtime10, String_t* ___rolelevelMtime11, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mihua.SDK.LvUpInfo PluginHuoShu::ilo_Parse6(System.Object[])
extern "C"  LvUpInfo_t411687177 * PluginHuoShu_ilo_Parse6_m1132679202 (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t1108656482* ___crtRoleOjs0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginHuoShu::ilo_ReqSDKHttpLogin7(PluginsSdkMgr)
extern "C"  void PluginHuoShu_ilo_ReqSDKHttpLogin7_m2626089845 (Il2CppObject * __this /* static, unused */, PluginsSdkMgr_t3884624670 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginHuoShu::ilo_logout8(PluginHuoShu)
extern "C"  void PluginHuoShu_ilo_logout8_m3952426322 (Il2CppObject * __this /* static, unused */, PluginHuoShu_t1558361489 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginHuoShu::ilo_Logout9(System.Action)
extern "C"  void PluginHuoShu_ilo_Logout9_m380759193 (Il2CppObject * __this /* static, unused */, Action_t3771233898 * ___call0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PluginsSdkMgr PluginHuoShu::ilo_get_PluginsSdkMgr10()
extern "C"  PluginsSdkMgr_t3884624670 * PluginHuoShu_ilo_get_PluginsSdkMgr10_m2964423700 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
