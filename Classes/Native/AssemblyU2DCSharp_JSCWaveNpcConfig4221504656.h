﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<JSCLevelMonsterConfig>
struct List_1_t3292265250;
// ProtoBuf.IExtension
struct IExtension_t1606339106;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JSCWaveNpcConfig
struct  JSCWaveNpcConfig_t4221504656  : public Il2CppObject
{
public:
	// System.Single JSCWaveNpcConfig::_delay
	float ____delay_0;
	// System.Single JSCWaveNpcConfig::_distance
	float ____distance_1;
	// System.Collections.Generic.List`1<JSCLevelMonsterConfig> JSCWaveNpcConfig::_wave
	List_1_t3292265250 * ____wave_2;
	// ProtoBuf.IExtension JSCWaveNpcConfig::extensionObject
	Il2CppObject * ___extensionObject_3;

public:
	inline static int32_t get_offset_of__delay_0() { return static_cast<int32_t>(offsetof(JSCWaveNpcConfig_t4221504656, ____delay_0)); }
	inline float get__delay_0() const { return ____delay_0; }
	inline float* get_address_of__delay_0() { return &____delay_0; }
	inline void set__delay_0(float value)
	{
		____delay_0 = value;
	}

	inline static int32_t get_offset_of__distance_1() { return static_cast<int32_t>(offsetof(JSCWaveNpcConfig_t4221504656, ____distance_1)); }
	inline float get__distance_1() const { return ____distance_1; }
	inline float* get_address_of__distance_1() { return &____distance_1; }
	inline void set__distance_1(float value)
	{
		____distance_1 = value;
	}

	inline static int32_t get_offset_of__wave_2() { return static_cast<int32_t>(offsetof(JSCWaveNpcConfig_t4221504656, ____wave_2)); }
	inline List_1_t3292265250 * get__wave_2() const { return ____wave_2; }
	inline List_1_t3292265250 ** get_address_of__wave_2() { return &____wave_2; }
	inline void set__wave_2(List_1_t3292265250 * value)
	{
		____wave_2 = value;
		Il2CppCodeGenWriteBarrier(&____wave_2, value);
	}

	inline static int32_t get_offset_of_extensionObject_3() { return static_cast<int32_t>(offsetof(JSCWaveNpcConfig_t4221504656, ___extensionObject_3)); }
	inline Il2CppObject * get_extensionObject_3() const { return ___extensionObject_3; }
	inline Il2CppObject ** get_address_of_extensionObject_3() { return &___extensionObject_3; }
	inline void set_extensionObject_3(Il2CppObject * value)
	{
		___extensionObject_3 = value;
		Il2CppCodeGenWriteBarrier(&___extensionObject_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
