﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<Pathfinding.Voxels.ExtraMesh>
struct List_1_t1291247971;
// System.Collections.Generic.IEnumerable`1<Pathfinding.Voxels.ExtraMesh>
struct IEnumerable_1_t3223975376;
// System.Collections.Generic.IEnumerator`1<Pathfinding.Voxels.ExtraMesh>
struct IEnumerator_1_t1834927468;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.ICollection`1<Pathfinding.Voxels.ExtraMesh>
struct ICollection_1_t817652406;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Voxels.ExtraMesh>
struct ReadOnlyCollection_1_t1480139955;
// Pathfinding.Voxels.ExtraMesh[]
struct ExtraMeshU5BU5D_t2875893186;
// System.Predicate`1<Pathfinding.Voxels.ExtraMesh>
struct Predicate_1_t3829086598;
// System.Collections.Generic.IComparer`1<Pathfinding.Voxels.ExtraMesh>
struct IComparer_1_t2498076461;
// System.Comparison`1<Pathfinding.Voxels.ExtraMesh>
struct Comparison_1_t2934390902;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_Pathfinding_Voxels_ExtraMesh4218029715.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1310920741.h"

// System.Void System.Collections.Generic.List`1<Pathfinding.Voxels.ExtraMesh>::.ctor()
extern "C"  void List_1__ctor_m4244377361_gshared (List_1_t1291247971 * __this, const MethodInfo* method);
#define List_1__ctor_m4244377361(__this, method) ((  void (*) (List_1_t1291247971 *, const MethodInfo*))List_1__ctor_m4244377361_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Voxels.ExtraMesh>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1__ctor_m1185089189_gshared (List_1_t1291247971 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1__ctor_m1185089189(__this, ___collection0, method) ((  void (*) (List_1_t1291247971 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m1185089189_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Voxels.ExtraMesh>::.ctor(System.Int32)
extern "C"  void List_1__ctor_m584370155_gshared (List_1_t1291247971 * __this, int32_t ___capacity0, const MethodInfo* method);
#define List_1__ctor_m584370155(__this, ___capacity0, method) ((  void (*) (List_1_t1291247971 *, int32_t, const MethodInfo*))List_1__ctor_m584370155_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Voxels.ExtraMesh>::.cctor()
extern "C"  void List_1__cctor_m2272154963_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m2272154963(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m2272154963_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Pathfinding.Voxels.ExtraMesh>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3129787684_gshared (List_1_t1291247971 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3129787684(__this, method) ((  Il2CppObject* (*) (List_1_t1291247971 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3129787684_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Voxels.ExtraMesh>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m2152103146_gshared (List_1_t1291247971 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m2152103146(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t1291247971 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m2152103146_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<Pathfinding.Voxels.ExtraMesh>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m441215097_gshared (List_1_t1291247971 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m441215097(__this, method) ((  Il2CppObject * (*) (List_1_t1291247971 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m441215097_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Pathfinding.Voxels.ExtraMesh>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_Add_m1767254884_gshared (List_1_t1291247971 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m1767254884(__this, ___item0, method) ((  int32_t (*) (List_1_t1291247971 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m1767254884_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<Pathfinding.Voxels.ExtraMesh>::System.Collections.IList.Contains(System.Object)
extern "C"  bool List_1_System_Collections_IList_Contains_m3675352924_gshared (List_1_t1291247971 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m3675352924(__this, ___item0, method) ((  bool (*) (List_1_t1291247971 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m3675352924_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<Pathfinding.Voxels.ExtraMesh>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m3775532988_gshared (List_1_t1291247971 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m3775532988(__this, ___item0, method) ((  int32_t (*) (List_1_t1291247971 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m3775532988_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Voxels.ExtraMesh>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_Insert_m1050451247_gshared (List_1_t1291247971 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m1050451247(__this, ___index0, ___item1, method) ((  void (*) (List_1_t1291247971 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m1050451247_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Voxels.ExtraMesh>::System.Collections.IList.Remove(System.Object)
extern "C"  void List_1_System_Collections_IList_Remove_m548686745_gshared (List_1_t1291247971 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m548686745(__this, ___item0, method) ((  void (*) (List_1_t1291247971 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m548686745_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<Pathfinding.Voxels.ExtraMesh>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1160698973_gshared (List_1_t1291247971 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1160698973(__this, method) ((  bool (*) (List_1_t1291247971 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1160698973_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Pathfinding.Voxels.ExtraMesh>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool List_1_System_Collections_ICollection_get_IsSynchronized_m832658048_gshared (List_1_t1291247971 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m832658048(__this, method) ((  bool (*) (List_1_t1291247971 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m832658048_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Pathfinding.Voxels.ExtraMesh>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m992615538_gshared (List_1_t1291247971 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m992615538(__this, method) ((  Il2CppObject * (*) (List_1_t1291247971 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m992615538_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Pathfinding.Voxels.ExtraMesh>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool List_1_System_Collections_IList_get_IsFixedSize_m2705773515_gshared (List_1_t1291247971 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m2705773515(__this, method) ((  bool (*) (List_1_t1291247971 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m2705773515_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Pathfinding.Voxels.ExtraMesh>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_IList_get_IsReadOnly_m679839182_gshared (List_1_t1291247971 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m679839182(__this, method) ((  bool (*) (List_1_t1291247971 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m679839182_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Pathfinding.Voxels.ExtraMesh>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m1486403961_gshared (List_1_t1291247971 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m1486403961(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t1291247971 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m1486403961_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Voxels.ExtraMesh>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_set_Item_m780286278_gshared (List_1_t1291247971 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m780286278(__this, ___index0, ___value1, method) ((  void (*) (List_1_t1291247971 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m780286278_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Voxels.ExtraMesh>::Add(T)
extern "C"  void List_1_Add_m3938738177_gshared (List_1_t1291247971 * __this, ExtraMesh_t4218029715  ___item0, const MethodInfo* method);
#define List_1_Add_m3938738177(__this, ___item0, method) ((  void (*) (List_1_t1291247971 *, ExtraMesh_t4218029715 , const MethodInfo*))List_1_Add_m3938738177_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Voxels.ExtraMesh>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m1738646880_gshared (List_1_t1291247971 * __this, int32_t ___newCount0, const MethodInfo* method);
#define List_1_GrowIfNeeded_m1738646880(__this, ___newCount0, method) ((  void (*) (List_1_t1291247971 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m1738646880_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Voxels.ExtraMesh>::CheckRange(System.Int32,System.Int32)
extern "C"  void List_1_CheckRange_m1818059399_gshared (List_1_t1291247971 * __this, int32_t ___idx0, int32_t ___count1, const MethodInfo* method);
#define List_1_CheckRange_m1818059399(__this, ___idx0, ___count1, method) ((  void (*) (List_1_t1291247971 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m1818059399_gshared)(__this, ___idx0, ___count1, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Voxels.ExtraMesh>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m228883550_gshared (List_1_t1291247971 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddCollection_m228883550(__this, ___collection0, method) ((  void (*) (List_1_t1291247971 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m228883550_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Voxels.ExtraMesh>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddEnumerable_m3784823070_gshared (List_1_t1291247971 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method);
#define List_1_AddEnumerable_m3784823070(__this, ___enumerable0, method) ((  void (*) (List_1_t1291247971 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m3784823070_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Voxels.ExtraMesh>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m116810425_gshared (List_1_t1291247971 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddRange_m116810425(__this, ___collection0, method) ((  void (*) (List_1_t1291247971 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m116810425_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<Pathfinding.Voxels.ExtraMesh>::AsReadOnly()
extern "C"  ReadOnlyCollection_1_t1480139955 * List_1_AsReadOnly_m3778837804_gshared (List_1_t1291247971 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m3778837804(__this, method) ((  ReadOnlyCollection_1_t1480139955 * (*) (List_1_t1291247971 *, const MethodInfo*))List_1_AsReadOnly_m3778837804_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Pathfinding.Voxels.ExtraMesh>::BinarySearch(T)
extern "C"  int32_t List_1_BinarySearch_m3595167143_gshared (List_1_t1291247971 * __this, ExtraMesh_t4218029715  ___item0, const MethodInfo* method);
#define List_1_BinarySearch_m3595167143(__this, ___item0, method) ((  int32_t (*) (List_1_t1291247971 *, ExtraMesh_t4218029715 , const MethodInfo*))List_1_BinarySearch_m3595167143_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Voxels.ExtraMesh>::Clear()
extern "C"  void List_1_Clear_m3868157381_gshared (List_1_t1291247971 * __this, const MethodInfo* method);
#define List_1_Clear_m3868157381(__this, method) ((  void (*) (List_1_t1291247971 *, const MethodInfo*))List_1_Clear_m3868157381_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Pathfinding.Voxels.ExtraMesh>::Contains(T)
extern "C"  bool List_1_Contains_m1081914423_gshared (List_1_t1291247971 * __this, ExtraMesh_t4218029715  ___item0, const MethodInfo* method);
#define List_1_Contains_m1081914423(__this, ___item0, method) ((  bool (*) (List_1_t1291247971 *, ExtraMesh_t4218029715 , const MethodInfo*))List_1_Contains_m1081914423_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Voxels.ExtraMesh>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m1478616149_gshared (List_1_t1291247971 * __this, ExtraMeshU5BU5D_t2875893186* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_CopyTo_m1478616149(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t1291247971 *, ExtraMeshU5BU5D_t2875893186*, int32_t, const MethodInfo*))List_1_CopyTo_m1478616149_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<Pathfinding.Voxels.ExtraMesh>::Find(System.Predicate`1<T>)
extern "C"  ExtraMesh_t4218029715  List_1_Find_m1675885073_gshared (List_1_t1291247971 * __this, Predicate_1_t3829086598 * ___match0, const MethodInfo* method);
#define List_1_Find_m1675885073(__this, ___match0, method) ((  ExtraMesh_t4218029715  (*) (List_1_t1291247971 *, Predicate_1_t3829086598 *, const MethodInfo*))List_1_Find_m1675885073_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Voxels.ExtraMesh>::CheckMatch(System.Predicate`1<T>)
extern "C"  void List_1_CheckMatch_m811813422_gshared (Il2CppObject * __this /* static, unused */, Predicate_1_t3829086598 * ___match0, const MethodInfo* method);
#define List_1_CheckMatch_m811813422(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t3829086598 *, const MethodInfo*))List_1_CheckMatch_m811813422_gshared)(__this /* static, unused */, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<Pathfinding.Voxels.ExtraMesh>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C"  int32_t List_1_GetIndex_m961152523_gshared (List_1_t1291247971 * __this, int32_t ___startIndex0, int32_t ___count1, Predicate_1_t3829086598 * ___match2, const MethodInfo* method);
#define List_1_GetIndex_m961152523(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t1291247971 *, int32_t, int32_t, Predicate_1_t3829086598 *, const MethodInfo*))List_1_GetIndex_m961152523_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Pathfinding.Voxels.ExtraMesh>::GetEnumerator()
extern "C"  Enumerator_t1310920741  List_1_GetEnumerator_m2631827956_gshared (List_1_t1291247971 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m2631827956(__this, method) ((  Enumerator_t1310920741  (*) (List_1_t1291247971 *, const MethodInfo*))List_1_GetEnumerator_m2631827956_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Pathfinding.Voxels.ExtraMesh>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m3502189281_gshared (List_1_t1291247971 * __this, ExtraMesh_t4218029715  ___item0, const MethodInfo* method);
#define List_1_IndexOf_m3502189281(__this, ___item0, method) ((  int32_t (*) (List_1_t1291247971 *, ExtraMesh_t4218029715 , const MethodInfo*))List_1_IndexOf_m3502189281_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Voxels.ExtraMesh>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m1091474860_gshared (List_1_t1291247971 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method);
#define List_1_Shift_m1091474860(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t1291247971 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m1091474860_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Voxels.ExtraMesh>::CheckIndex(System.Int32)
extern "C"  void List_1_CheckIndex_m1224983333_gshared (List_1_t1291247971 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_CheckIndex_m1224983333(__this, ___index0, method) ((  void (*) (List_1_t1291247971 *, int32_t, const MethodInfo*))List_1_CheckIndex_m1224983333_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Voxels.ExtraMesh>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m2888240140_gshared (List_1_t1291247971 * __this, int32_t ___index0, ExtraMesh_t4218029715  ___item1, const MethodInfo* method);
#define List_1_Insert_m2888240140(__this, ___index0, ___item1, method) ((  void (*) (List_1_t1291247971 *, int32_t, ExtraMesh_t4218029715 , const MethodInfo*))List_1_Insert_m2888240140_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Voxels.ExtraMesh>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_CheckCollection_m1440427905_gshared (List_1_t1291247971 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_CheckCollection_m1440427905(__this, ___collection0, method) ((  void (*) (List_1_t1291247971 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m1440427905_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<Pathfinding.Voxels.ExtraMesh>::Remove(T)
extern "C"  bool List_1_Remove_m518985202_gshared (List_1_t1291247971 * __this, ExtraMesh_t4218029715  ___item0, const MethodInfo* method);
#define List_1_Remove_m518985202(__this, ___item0, method) ((  bool (*) (List_1_t1291247971 *, ExtraMesh_t4218029715 , const MethodInfo*))List_1_Remove_m518985202_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<Pathfinding.Voxels.ExtraMesh>::RemoveAll(System.Predicate`1<T>)
extern "C"  int32_t List_1_RemoveAll_m3176539108_gshared (List_1_t1291247971 * __this, Predicate_1_t3829086598 * ___match0, const MethodInfo* method);
#define List_1_RemoveAll_m3176539108(__this, ___match0, method) ((  int32_t (*) (List_1_t1291247971 *, Predicate_1_t3829086598 *, const MethodInfo*))List_1_RemoveAll_m3176539108_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Voxels.ExtraMesh>::RemoveAt(System.Int32)
extern "C"  void List_1_RemoveAt_m762093010_gshared (List_1_t1291247971 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_RemoveAt_m762093010(__this, ___index0, method) ((  void (*) (List_1_t1291247971 *, int32_t, const MethodInfo*))List_1_RemoveAt_m762093010_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Voxels.ExtraMesh>::RemoveRange(System.Int32,System.Int32)
extern "C"  void List_1_RemoveRange_m260478453_gshared (List_1_t1291247971 * __this, int32_t ___index0, int32_t ___count1, const MethodInfo* method);
#define List_1_RemoveRange_m260478453(__this, ___index0, ___count1, method) ((  void (*) (List_1_t1291247971 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m260478453_gshared)(__this, ___index0, ___count1, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Voxels.ExtraMesh>::Reverse()
extern "C"  void List_1_Reverse_m3815219034_gshared (List_1_t1291247971 * __this, const MethodInfo* method);
#define List_1_Reverse_m3815219034(__this, method) ((  void (*) (List_1_t1291247971 *, const MethodInfo*))List_1_Reverse_m3815219034_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Voxels.ExtraMesh>::Sort()
extern "C"  void List_1_Sort_m3772608136_gshared (List_1_t1291247971 * __this, const MethodInfo* method);
#define List_1_Sort_m3772608136(__this, method) ((  void (*) (List_1_t1291247971 *, const MethodInfo*))List_1_Sort_m3772608136_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Voxels.ExtraMesh>::Sort(System.Collections.Generic.IComparer`1<T>)
extern "C"  void List_1_Sort_m4127182940_gshared (List_1_t1291247971 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define List_1_Sort_m4127182940(__this, ___comparer0, method) ((  void (*) (List_1_t1291247971 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m4127182940_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Voxels.ExtraMesh>::Sort(System.Comparison`1<T>)
extern "C"  void List_1_Sort_m2429969627_gshared (List_1_t1291247971 * __this, Comparison_1_t2934390902 * ___comparison0, const MethodInfo* method);
#define List_1_Sort_m2429969627(__this, ___comparison0, method) ((  void (*) (List_1_t1291247971 *, Comparison_1_t2934390902 *, const MethodInfo*))List_1_Sort_m2429969627_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<Pathfinding.Voxels.ExtraMesh>::ToArray()
extern "C"  ExtraMeshU5BU5D_t2875893186* List_1_ToArray_m3520740723_gshared (List_1_t1291247971 * __this, const MethodInfo* method);
#define List_1_ToArray_m3520740723(__this, method) ((  ExtraMeshU5BU5D_t2875893186* (*) (List_1_t1291247971 *, const MethodInfo*))List_1_ToArray_m3520740723_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Voxels.ExtraMesh>::TrimExcess()
extern "C"  void List_1_TrimExcess_m4145121313_gshared (List_1_t1291247971 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m4145121313(__this, method) ((  void (*) (List_1_t1291247971 *, const MethodInfo*))List_1_TrimExcess_m4145121313_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Pathfinding.Voxels.ExtraMesh>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m2510080657_gshared (List_1_t1291247971 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m2510080657(__this, method) ((  int32_t (*) (List_1_t1291247971 *, const MethodInfo*))List_1_get_Capacity_m2510080657_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Voxels.ExtraMesh>::set_Capacity(System.Int32)
extern "C"  void List_1_set_Capacity_m3243802994_gshared (List_1_t1291247971 * __this, int32_t ___value0, const MethodInfo* method);
#define List_1_set_Capacity_m3243802994(__this, ___value0, method) ((  void (*) (List_1_t1291247971 *, int32_t, const MethodInfo*))List_1_set_Capacity_m3243802994_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<Pathfinding.Voxels.ExtraMesh>::get_Count()
extern "C"  int32_t List_1_get_Count_m2677601187_gshared (List_1_t1291247971 * __this, const MethodInfo* method);
#define List_1_get_Count_m2677601187(__this, method) ((  int32_t (*) (List_1_t1291247971 *, const MethodInfo*))List_1_get_Count_m2677601187_gshared)(__this, method)
// T System.Collections.Generic.List`1<Pathfinding.Voxels.ExtraMesh>::get_Item(System.Int32)
extern "C"  ExtraMesh_t4218029715  List_1_get_Item_m2186166098_gshared (List_1_t1291247971 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_get_Item_m2186166098(__this, ___index0, method) ((  ExtraMesh_t4218029715  (*) (List_1_t1291247971 *, int32_t, const MethodInfo*))List_1_get_Item_m2186166098_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Voxels.ExtraMesh>::set_Item(System.Int32,T)
extern "C"  void List_1_set_Item_m2859497955_gshared (List_1_t1291247971 * __this, int32_t ___index0, ExtraMesh_t4218029715  ___value1, const MethodInfo* method);
#define List_1_set_Item_m2859497955(__this, ___index0, ___value1, method) ((  void (*) (List_1_t1291247971 *, int32_t, ExtraMesh_t4218029715 , const MethodInfo*))List_1_set_Item_m2859497955_gshared)(__this, ___index0, ___value1, method)
