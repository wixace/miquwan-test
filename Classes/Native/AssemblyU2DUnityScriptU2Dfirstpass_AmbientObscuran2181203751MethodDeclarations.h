﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AmbientObscurance
struct AmbientObscurance_t2181203751;
// UnityEngine.RenderTexture
struct RenderTexture_t1963041563;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture1963041563.h"

// System.Void AmbientObscurance::.ctor()
extern "C"  void AmbientObscurance__ctor_m3363609307 (AmbientObscurance_t2181203751 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AmbientObscurance::CheckResources()
extern "C"  bool AmbientObscurance_CheckResources_m385541536 (AmbientObscurance_t2181203751 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AmbientObscurance::OnDisable()
extern "C"  void AmbientObscurance_OnDisable_m3199535554 (AmbientObscurance_t2181203751 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AmbientObscurance::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void AmbientObscurance_OnRenderImage_m2458852515 (AmbientObscurance_t2181203751 * __this, RenderTexture_t1963041563 * ___source0, RenderTexture_t1963041563 * ___destination1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AmbientObscurance::Main()
extern "C"  void AmbientObscurance_Main_m2379302530 (AmbientObscurance_t2181203751 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
