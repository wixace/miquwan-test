﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t1659122786;
// UISprite
struct UISprite_t661437049;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ProgressBar
struct  ProgressBar_t2799378054  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.Transform ProgressBar::targetTf
	Transform_t1659122786 * ___targetTf_2;
	// UISprite ProgressBar::pb_sp
	UISprite_t661437049 * ___pb_sp_4;
	// System.Boolean ProgressBar::hide
	bool ___hide_5;
	// System.Boolean ProgressBar::showBreak
	bool ___showBreak_6;
	// System.Int32 ProgressBar::instanceID
	int32_t ___instanceID_7;
	// UnityEngine.GameObject ProgressBar::breakGO
	GameObject_t3674682005 * ___breakGO_8;

public:
	inline static int32_t get_offset_of_targetTf_2() { return static_cast<int32_t>(offsetof(ProgressBar_t2799378054, ___targetTf_2)); }
	inline Transform_t1659122786 * get_targetTf_2() const { return ___targetTf_2; }
	inline Transform_t1659122786 ** get_address_of_targetTf_2() { return &___targetTf_2; }
	inline void set_targetTf_2(Transform_t1659122786 * value)
	{
		___targetTf_2 = value;
		Il2CppCodeGenWriteBarrier(&___targetTf_2, value);
	}

	inline static int32_t get_offset_of_pb_sp_4() { return static_cast<int32_t>(offsetof(ProgressBar_t2799378054, ___pb_sp_4)); }
	inline UISprite_t661437049 * get_pb_sp_4() const { return ___pb_sp_4; }
	inline UISprite_t661437049 ** get_address_of_pb_sp_4() { return &___pb_sp_4; }
	inline void set_pb_sp_4(UISprite_t661437049 * value)
	{
		___pb_sp_4 = value;
		Il2CppCodeGenWriteBarrier(&___pb_sp_4, value);
	}

	inline static int32_t get_offset_of_hide_5() { return static_cast<int32_t>(offsetof(ProgressBar_t2799378054, ___hide_5)); }
	inline bool get_hide_5() const { return ___hide_5; }
	inline bool* get_address_of_hide_5() { return &___hide_5; }
	inline void set_hide_5(bool value)
	{
		___hide_5 = value;
	}

	inline static int32_t get_offset_of_showBreak_6() { return static_cast<int32_t>(offsetof(ProgressBar_t2799378054, ___showBreak_6)); }
	inline bool get_showBreak_6() const { return ___showBreak_6; }
	inline bool* get_address_of_showBreak_6() { return &___showBreak_6; }
	inline void set_showBreak_6(bool value)
	{
		___showBreak_6 = value;
	}

	inline static int32_t get_offset_of_instanceID_7() { return static_cast<int32_t>(offsetof(ProgressBar_t2799378054, ___instanceID_7)); }
	inline int32_t get_instanceID_7() const { return ___instanceID_7; }
	inline int32_t* get_address_of_instanceID_7() { return &___instanceID_7; }
	inline void set_instanceID_7(int32_t value)
	{
		___instanceID_7 = value;
	}

	inline static int32_t get_offset_of_breakGO_8() { return static_cast<int32_t>(offsetof(ProgressBar_t2799378054, ___breakGO_8)); }
	inline GameObject_t3674682005 * get_breakGO_8() const { return ___breakGO_8; }
	inline GameObject_t3674682005 ** get_address_of_breakGO_8() { return &___breakGO_8; }
	inline void set_breakGO_8(GameObject_t3674682005 * value)
	{
		___breakGO_8 = value;
		Il2CppCodeGenWriteBarrier(&___breakGO_8, value);
	}
};

struct ProgressBar_t2799378054_StaticFields
{
public:
	// UnityEngine.Vector3 ProgressBar::offset
	Vector3_t4282066566  ___offset_3;

public:
	inline static int32_t get_offset_of_offset_3() { return static_cast<int32_t>(offsetof(ProgressBar_t2799378054_StaticFields, ___offset_3)); }
	inline Vector3_t4282066566  get_offset_3() const { return ___offset_3; }
	inline Vector3_t4282066566 * get_address_of_offset_3() { return &___offset_3; }
	inline void set_offset_3(Vector3_t4282066566  value)
	{
		___offset_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
