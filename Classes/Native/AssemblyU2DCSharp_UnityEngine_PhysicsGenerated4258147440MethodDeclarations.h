﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_PhysicsGenerated
struct UnityEngine_PhysicsGenerated_t4258147440;
// JSVCall
struct JSVCall_t3708497963;
// UnityEngine.RaycastHit[]
struct RaycastHitU5BU5D_t528650843;
// UnityEngine.Collider[]
struct ColliderU5BU5D_t2697150633;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void UnityEngine_PhysicsGenerated::.ctor()
extern "C"  void UnityEngine_PhysicsGenerated__ctor_m1025872939 (UnityEngine_PhysicsGenerated_t4258147440 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_Physics1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_Physics1_m4123536607 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_PhysicsGenerated::Physics_IgnoreRaycastLayer(JSVCall)
extern "C"  void UnityEngine_PhysicsGenerated_Physics_IgnoreRaycastLayer_m2108251548 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_PhysicsGenerated::Physics_DefaultRaycastLayers(JSVCall)
extern "C"  void UnityEngine_PhysicsGenerated_Physics_DefaultRaycastLayers_m1306807980 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_PhysicsGenerated::Physics_AllLayers(JSVCall)
extern "C"  void UnityEngine_PhysicsGenerated_Physics_AllLayers_m2387964531 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_PhysicsGenerated::Physics_gravity(JSVCall)
extern "C"  void UnityEngine_PhysicsGenerated_Physics_gravity_m2109770408 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_PhysicsGenerated::Physics_defaultContactOffset(JSVCall)
extern "C"  void UnityEngine_PhysicsGenerated_Physics_defaultContactOffset_m2209865732 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_PhysicsGenerated::Physics_bounceThreshold(JSVCall)
extern "C"  void UnityEngine_PhysicsGenerated_Physics_bounceThreshold_m552770003 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_PhysicsGenerated::Physics_solverIterationCount(JSVCall)
extern "C"  void UnityEngine_PhysicsGenerated_Physics_solverIterationCount_m1656326737 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_PhysicsGenerated::Physics_sleepThreshold(JSVCall)
extern "C"  void UnityEngine_PhysicsGenerated_Physics_sleepThreshold_m1922791170 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_PhysicsGenerated::Physics_queriesHitTriggers(JSVCall)
extern "C"  void UnityEngine_PhysicsGenerated_Physics_queriesHitTriggers_m4019849646 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_BoxCast__Vector3__Vector3__Vector3__RaycastHit__Quaternion__Single__Int32__QueryTriggerInteraction(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_BoxCast__Vector3__Vector3__Vector3__RaycastHit__Quaternion__Single__Int32__QueryTriggerInteraction_m2074017439 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_BoxCast__Vector3__Vector3__Vector3__RaycastHit__Quaternion__Single__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_BoxCast__Vector3__Vector3__Vector3__RaycastHit__Quaternion__Single__Int32_m1125088421 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_BoxCast__Vector3__Vector3__Vector3__Quaternion__Single__Int32__QueryTriggerInteraction(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_BoxCast__Vector3__Vector3__Vector3__Quaternion__Single__Int32__QueryTriggerInteraction_m1294827957 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_BoxCast__Vector3__Vector3__Vector3__RaycastHit__Quaternion__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_BoxCast__Vector3__Vector3__Vector3__RaycastHit__Quaternion__Single_m1472286795 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_BoxCast__Vector3__Vector3__Vector3__Quaternion__Single__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_BoxCast__Vector3__Vector3__Vector3__Quaternion__Single__Int32_m1949080783 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_BoxCast__Vector3__Vector3__Vector3__Quaternion__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_BoxCast__Vector3__Vector3__Vector3__Quaternion__Single_m1760668513 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_BoxCast__Vector3__Vector3__Vector3__RaycastHit__Quaternion(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_BoxCast__Vector3__Vector3__Vector3__RaycastHit__Quaternion_m964768771 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_BoxCast__Vector3__Vector3__Vector3__Quaternion(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_BoxCast__Vector3__Vector3__Vector3__Quaternion_m3442541849 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_BoxCast__Vector3__Vector3__Vector3__RaycastHit(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_BoxCast__Vector3__Vector3__Vector3__RaycastHit_m1360970085 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_BoxCast__Vector3__Vector3__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_BoxCast__Vector3__Vector3__Vector3_m3185509243 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_BoxCastAll__Vector3__Vector3__Vector3__Quaternion__Single__Int32__QueryTriggerInteraction(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_BoxCastAll__Vector3__Vector3__Vector3__Quaternion__Single__Int32__QueryTriggerInteraction_m3950331488 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_BoxCastAll__Vector3__Vector3__Vector3__Quaternion__Single__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_BoxCastAll__Vector3__Vector3__Vector3__Quaternion__Single__Int32_m2876401092 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_BoxCastAll__Vector3__Vector3__Vector3__Quaternion__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_BoxCastAll__Vector3__Vector3__Vector3__Quaternion__Single_m2027871244 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_BoxCastAll__Vector3__Vector3__Vector3__Quaternion(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_BoxCastAll__Vector3__Vector3__Vector3__Quaternion_m1956843716 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_BoxCastAll__Vector3__Vector3__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_BoxCastAll__Vector3__Vector3__Vector3_m2795304870 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_BoxCastNonAlloc__Vector3__Vector3__Vector3__RaycastHit_Array__Quaternion__Single__Int32__QueryTriggerInteraction(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_BoxCastNonAlloc__Vector3__Vector3__Vector3__RaycastHit_Array__Quaternion__Single__Int32__QueryTriggerInteraction_m1160096977 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_BoxCastNonAlloc__Vector3__Vector3__Vector3__RaycastHit_Array__Quaternion__Single__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_BoxCastNonAlloc__Vector3__Vector3__Vector3__RaycastHit_Array__Quaternion__Single__Int32_m3809315379 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_BoxCastNonAlloc__Vector3__Vector3__Vector3__RaycastHit_Array__Quaternion__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_BoxCastNonAlloc__Vector3__Vector3__Vector3__RaycastHit_Array__Quaternion__Single_m3934078589 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_BoxCastNonAlloc__Vector3__Vector3__Vector3__RaycastHit_Array__Quaternion(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_BoxCastNonAlloc__Vector3__Vector3__Vector3__RaycastHit_Array__Quaternion_m497515573 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_BoxCastNonAlloc__Vector3__Vector3__Vector3__RaycastHit_Array(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_BoxCastNonAlloc__Vector3__Vector3__Vector3__RaycastHit_Array_m3554296983 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_CapsuleCast__Vector3__Vector3__Single__Vector3__RaycastHit__Single__Int32__QueryTriggerInteraction(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_CapsuleCast__Vector3__Vector3__Single__Vector3__RaycastHit__Single__Int32__QueryTriggerInteraction_m3671994519 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_CapsuleCast__Vector3__Vector3__Single__Vector3__RaycastHit__Single__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_CapsuleCast__Vector3__Vector3__Single__Vector3__RaycastHit__Single__Int32_m717800365 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_CapsuleCast__Vector3__Vector3__Single__Vector3__Single__Int32__QueryTriggerInteraction(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_CapsuleCast__Vector3__Vector3__Single__Vector3__Single__Int32__QueryTriggerInteraction_m1074320813 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_CapsuleCast__Vector3__Vector3__Single__Vector3__Single__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_CapsuleCast__Vector3__Vector3__Single__Vector3__Single__Int32_m112689623 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_CapsuleCast__Vector3__Vector3__Single__Vector3__RaycastHit__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_CapsuleCast__Vector3__Vector3__Single__Vector3__RaycastHit__Single_m3464913475 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_CapsuleCast__Vector3__Vector3__Single__Vector3__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_CapsuleCast__Vector3__Vector3__Single__Vector3__Single_m1549459289 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_CapsuleCast__Vector3__Vector3__Single__Vector3__RaycastHit(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_CapsuleCast__Vector3__Vector3__Single__Vector3__RaycastHit_m2694036987 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_CapsuleCast__Vector3__Vector3__Single__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_CapsuleCast__Vector3__Vector3__Single__Vector3_m3760828689 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_CapsuleCastAll__Vector3__Vector3__Single__Vector3__Single__Int32__QueryTriggerInteraction(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_CapsuleCastAll__Vector3__Vector3__Single__Vector3__Single__Int32__QueryTriggerInteraction_m2436576604 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_CapsuleCastAll__Vector3__Vector3__Single__Vector3__Single__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_CapsuleCastAll__Vector3__Vector3__Single__Vector3__Single__Int32_m3584291656 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_CapsuleCastAll__Vector3__Vector3__Single__Vector3__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_CapsuleCastAll__Vector3__Vector3__Single__Vector3__Single_m2839526152 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_CapsuleCastAll__Vector3__Vector3__Single__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_CapsuleCastAll__Vector3__Vector3__Single__Vector3_m333491136 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_CapsuleCastNonAlloc__Vector3__Vector3__Single__Vector3__RaycastHit_Array__Single__Int32__QueryTriggerInteraction(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_CapsuleCastNonAlloc__Vector3__Vector3__Single__Vector3__RaycastHit_Array__Single__Int32__QueryTriggerInteraction_m298130249 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_CapsuleCastNonAlloc__Vector3__Vector3__Single__Vector3__RaycastHit_Array__Single__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_CapsuleCastNonAlloc__Vector3__Vector3__Single__Vector3__RaycastHit_Array__Single__Int32_m607093947 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_CapsuleCastNonAlloc__Vector3__Vector3__Single__Vector3__RaycastHit_Array__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_CapsuleCastNonAlloc__Vector3__Vector3__Single__Vector3__RaycastHit_Array__Single_m2595525877 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_CapsuleCastNonAlloc__Vector3__Vector3__Single__Vector3__RaycastHit_Array(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_CapsuleCastNonAlloc__Vector3__Vector3__Single__Vector3__RaycastHit_Array_m1886438061 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_CheckBox__Vector3__Vector3__Quaternion__Int32__QueryTriggerInteraction(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_CheckBox__Vector3__Vector3__Quaternion__Int32__QueryTriggerInteraction_m1864831914 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_CheckBox__Vector3__Vector3__Quaternion__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_CheckBox__Vector3__Vector3__Quaternion__Int32_m3582085434 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_CheckBox__Vector3__Vector3__Quaternion(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_CheckBox__Vector3__Vector3__Quaternion_m3880276310 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_CheckBox__Vector3__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_CheckBox__Vector3__Vector3_m1195443000 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_CheckCapsule__Vector3__Vector3__Single__Int32__QueryTriggerInteraction(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_CheckCapsule__Vector3__Vector3__Single__Int32__QueryTriggerInteraction_m3144116022 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_CheckCapsule__Vector3__Vector3__Single__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_CheckCapsule__Vector3__Vector3__Single__Int32_m543944750 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_CheckCapsule__Vector3__Vector3__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_CheckCapsule__Vector3__Vector3__Single_m2153370850 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_CheckSphere__Vector3__Single__Int32__QueryTriggerInteraction(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_CheckSphere__Vector3__Single__Int32__QueryTriggerInteraction_m2419972844 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_CheckSphere__Vector3__Single__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_CheckSphere__Vector3__Single__Int32_m57977784 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_CheckSphere__Vector3__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_CheckSphere__Vector3__Single_m3552354456 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_GetIgnoreLayerCollision__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_GetIgnoreLayerCollision__Int32__Int32_m3502260886 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_IgnoreCollision__Collider__Collider__Boolean(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_IgnoreCollision__Collider__Collider__Boolean_m2697209941 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_IgnoreCollision__Collider__Collider(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_IgnoreCollision__Collider__Collider_m1116727413 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_IgnoreLayerCollision__Int32__Int32__Boolean(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_IgnoreLayerCollision__Int32__Int32__Boolean_m3892781954 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_IgnoreLayerCollision__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_IgnoreLayerCollision__Int32__Int32_m3475388072 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_Linecast__Vector3__Vector3__RaycastHit__Int32__QueryTriggerInteraction(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_Linecast__Vector3__Vector3__RaycastHit__Int32__QueryTriggerInteraction_m3616744838 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_Linecast__Vector3__Vector3__Int32__QueryTriggerInteraction(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_Linecast__Vector3__Vector3__Int32__QueryTriggerInteraction_m1726094108 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_Linecast__Vector3__Vector3__RaycastHit__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_Linecast__Vector3__Vector3__RaycastHit__Int32_m3054974942 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_Linecast__Vector3__Vector3__RaycastHit(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_Linecast__Vector3__Vector3__RaycastHit_m479384370 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_Linecast__Vector3__Vector3__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_Linecast__Vector3__Vector3__Int32_m2455508360 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_Linecast__Vector3__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_Linecast__Vector3__Vector3_m367894728 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_OverlapBox__Vector3__Vector3__Quaternion__Int32__QueryTriggerInteraction(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_OverlapBox__Vector3__Vector3__Quaternion__Int32__QueryTriggerInteraction_m2876712075 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_OverlapBox__Vector3__Vector3__Quaternion__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_OverlapBox__Vector3__Vector3__Quaternion__Int32_m2620417337 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_OverlapBox__Vector3__Vector3__Quaternion(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_OverlapBox__Vector3__Vector3__Quaternion_m2907448887 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_OverlapBox__Vector3__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_OverlapBox__Vector3__Vector3_m1908171161 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_OverlapBoxNonAlloc__Vector3__Vector3__Collider_Array__Quaternion__Int32__QueryTriggerInteraction(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_OverlapBoxNonAlloc__Vector3__Vector3__Collider_Array__Quaternion__Int32__QueryTriggerInteraction_m1165905857 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_OverlapBoxNonAlloc__Vector3__Vector3__Collider_Array__Quaternion__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_OverlapBoxNonAlloc__Vector3__Vector3__Collider_Array__Quaternion__Int32_m624228675 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_OverlapBoxNonAlloc__Vector3__Vector3__Collider_Array__Quaternion(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_OverlapBoxNonAlloc__Vector3__Vector3__Collider_Array__Quaternion_m3667765613 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_OverlapBoxNonAlloc__Vector3__Vector3__Collider_Array(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_OverlapBoxNonAlloc__Vector3__Vector3__Collider_Array_m3900736975 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_OverlapSphere__Vector3__Single__Int32__QueryTriggerInteraction(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_OverlapSphere__Vector3__Single__Int32__QueryTriggerInteraction_m3934413069 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_OverlapSphere__Vector3__Single__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_OverlapSphere__Vector3__Single__Int32_m4206291575 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_OverlapSphere__Vector3__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_OverlapSphere__Vector3__Single_m1289349817 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_OverlapSphereNonAlloc__Vector3__Single__Collider_Array__Int32__QueryTriggerInteraction(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_OverlapSphereNonAlloc__Vector3__Single__Collider_Array__Int32__QueryTriggerInteraction_m2974063411 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_OverlapSphereNonAlloc__Vector3__Single__Collider_Array__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_OverlapSphereNonAlloc__Vector3__Single__Collider_Array__Int32_m3682533777 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_OverlapSphereNonAlloc__Vector3__Single__Collider_Array(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_OverlapSphereNonAlloc__Vector3__Single__Collider_Array_m1532902623 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_Raycast__Vector3__Vector3__RaycastHit__Single__Int32__QueryTriggerInteraction(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_Raycast__Vector3__Vector3__RaycastHit__Single__Int32__QueryTriggerInteraction_m1054465916 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_Raycast__Vector3__Vector3__Single__Int32__QueryTriggerInteraction(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_Raycast__Vector3__Vector3__Single__Int32__QueryTriggerInteraction_m34599954 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_Raycast__Vector3__Vector3__RaycastHit__Single__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_Raycast__Vector3__Vector3__RaycastHit__Single__Int32_m1651975464 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_Raycast__Ray__RaycastHit__Single__Int32__QueryTriggerInteraction(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_Raycast__Ray__RaycastHit__Single__Int32__QueryTriggerInteraction_m1849894716 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_Raycast__Ray__RaycastHit__Single__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_Raycast__Ray__RaycastHit__Single__Int32_m4015170920 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_Raycast__Ray__Single__Int32__QueryTriggerInteraction(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_Raycast__Ray__Single__Int32__QueryTriggerInteraction_m3445021650 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_Raycast__Vector3__Vector3__RaycastHit__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_Raycast__Vector3__Vector3__RaycastHit__Single_m856614184 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_Raycast__Vector3__Vector3__Single__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_Raycast__Vector3__Vector3__Single__Int32_m1168638930 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_Raycast__Vector3__Vector3__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_Raycast__Vector3__Vector3__Single_m2358038974 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_Raycast__Ray__Single__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_Raycast__Ray__Single__Int32_m563209234 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_Raycast__Vector3__Vector3__RaycastHit(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_Raycast__Vector3__Vector3__RaycastHit_m2819356128 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_Raycast__Ray__RaycastHit__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_Raycast__Ray__RaycastHit__Single_m193408232 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_Raycast__Ray__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_Raycast__Ray__Single_m3747789182 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_Raycast__Ray__RaycastHit(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_Raycast__Ray__RaycastHit_m1244134816 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_Raycast__Vector3__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_Raycast__Vector3__Vector3_m2104678518 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_Raycast__Ray(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_Raycast__Ray_m562855990 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_RaycastAll__Vector3__Vector3__Single__Int32__QueryTriggerInteraction(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_RaycastAll__Vector3__Vector3__Single__Int32__QueryTriggerInteraction_m1091878057 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_RaycastAll__Vector3__Vector3__Single__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_RaycastAll__Vector3__Vector3__Single__Int32_m2140936027 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_RaycastAll__Ray__Single__Int32__QueryTriggerInteraction(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_RaycastAll__Ray__Single__Int32__QueryTriggerInteraction_m384100827 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_RaycastAll__Ray__Single__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_RaycastAll__Ray__Single__Int32_m456639977 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_RaycastAll__Vector3__Vector3__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_RaycastAll__Vector3__Vector3__Single_m3185686101 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_RaycastAll__Vector3__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_RaycastAll__Vector3__Vector3_m1376076813 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_RaycastAll__Ray__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_RaycastAll__Ray__Single_m2862820231 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_RaycastAll__Ray(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_RaycastAll__Ray_m3508538175 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_RaycastNonAlloc__Vector3__Vector3__RaycastHit_Array__Single__Int32__QueryTriggerInteraction(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_RaycastNonAlloc__Vector3__Vector3__RaycastHit_Array__Single__Int32__QueryTriggerInteraction_m4191482238 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_RaycastNonAlloc__Vector3__Vector3__RaycastHit_Array__Single__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_RaycastNonAlloc__Vector3__Vector3__RaycastHit_Array__Single__Int32_m2477040870 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_RaycastNonAlloc__Ray__RaycastHit_Array__Single__Int32__QueryTriggerInteraction(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_RaycastNonAlloc__Ray__RaycastHit_Array__Single__Int32__QueryTriggerInteraction_m1191473454 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_RaycastNonAlloc__Vector3__Vector3__RaycastHit_Array__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_RaycastNonAlloc__Vector3__Vector3__RaycastHit_Array__Single_m3074588458 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_RaycastNonAlloc__Ray__RaycastHit_Array__Single__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_RaycastNonAlloc__Ray__RaycastHit_Array__Single__Int32_m2913230646 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_RaycastNonAlloc__Ray__RaycastHit_Array__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_RaycastNonAlloc__Ray__RaycastHit_Array__Single_m1356644058 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_RaycastNonAlloc__Vector3__Vector3__RaycastHit_Array(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_RaycastNonAlloc__Vector3__Vector3__RaycastHit_Array_m1211281890 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_RaycastNonAlloc__Ray__RaycastHit_Array(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_RaycastNonAlloc__Ray__RaycastHit_Array_m3139412370 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_SphereCast__Vector3__Single__Vector3__RaycastHit__Single__Int32__QueryTriggerInteraction(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_SphereCast__Vector3__Single__Vector3__RaycastHit__Single__Int32__QueryTriggerInteraction_m3204192383 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_SphereCast__Ray__Single__RaycastHit__Single__Int32__QueryTriggerInteraction(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_SphereCast__Ray__Single__RaycastHit__Single__Int32__QueryTriggerInteraction_m3035637593 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_SphereCast__Vector3__Single__Vector3__RaycastHit__Single__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_SphereCast__Vector3__Single__Vector3__RaycastHit__Single__Int32_m959186629 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_SphereCast__Ray__Single__Single__Int32__QueryTriggerInteraction(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_SphereCast__Ray__Single__Single__Int32__QueryTriggerInteraction_m1314217839 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_SphereCast__Ray__Single__RaycastHit__Single__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_SphereCast__Ray__Single__RaycastHit__Single__Int32_m4241852075 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_SphereCast__Vector3__Single__Vector3__RaycastHit__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_SphereCast__Vector3__Single__Vector3__RaycastHit__Single_m2130569771 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_SphereCast__Ray__Single__Single__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_SphereCast__Ray__Single__Single__Int32_m528634325 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_SphereCast__Ray__Single__RaycastHit__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_SphereCast__Ray__Single__RaycastHit__Single_m2425610501 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_SphereCast__Vector3__Single__Vector3__RaycastHit(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_SphereCast__Vector3__Single__Vector3__RaycastHit_m1885711843 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_SphereCast__Ray__Single__RaycastHit(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_SphereCast__Ray__Single__RaycastHit_m2116361917 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_SphereCast__Ray__Single__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_SphereCast__Ray__Single__Single_m162459931 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_SphereCast__Ray__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_SphereCast__Ray__Single_m4131693779 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_SphereCastAll__Vector3__Single__Vector3__Single__Int32__QueryTriggerInteraction(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_SphereCastAll__Vector3__Single__Vector3__Single__Int32__QueryTriggerInteraction_m2338292662 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_SphereCastAll__Ray__Single__Single__Int32__QueryTriggerInteraction(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_SphereCastAll__Ray__Single__Single__Int32__QueryTriggerInteraction_m126636974 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_SphereCastAll__Vector3__Single__Vector3__Single__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_SphereCastAll__Vector3__Single__Vector3__Single__Int32_m2401289134 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_SphereCastAll__Ray__Single__Single__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_SphereCastAll__Ray__Single__Single__Int32_m2315693238 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_SphereCastAll__Vector3__Single__Vector3__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_SphereCastAll__Vector3__Single__Vector3__Single_m1944318306 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_SphereCastAll__Vector3__Single__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_SphereCastAll__Vector3__Single__Vector3_m2124799002 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_SphereCastAll__Ray__Single__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_SphereCastAll__Ray__Single__Single_m3046023514 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_SphereCastAll__Ray__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_SphereCastAll__Ray__Single_m2266760210 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_SphereCastNonAlloc__Vector3__Single__Vector3__RaycastHit_Array__Single__Int32__QueryTriggerInteraction(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_SphereCastNonAlloc__Vector3__Single__Vector3__RaycastHit_Array__Single__Int32__QueryTriggerInteraction_m3491158337 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_SphereCastNonAlloc__Vector3__Single__Vector3__RaycastHit_Array__Single__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_SphereCastNonAlloc__Vector3__Single__Vector3__RaycastHit_Array__Single__Int32_m1441841091 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_SphereCastNonAlloc__Ray__Single__RaycastHit_Array__Single__Int32__QueryTriggerInteraction(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_SphereCastNonAlloc__Ray__Single__RaycastHit_Array__Single__Int32__QueryTriggerInteraction_m150686347 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_SphereCastNonAlloc__Vector3__Single__Vector3__RaycastHit_Array__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_SphereCastNonAlloc__Vector3__Single__Vector3__RaycastHit_Array__Single_m4256969453 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_SphereCastNonAlloc__Ray__Single__RaycastHit_Array__Single__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_SphereCastNonAlloc__Ray__Single__RaycastHit_Array__Single__Int32_m3116203833 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_SphereCastNonAlloc__Ray__Single__RaycastHit_Array__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_SphereCastNonAlloc__Ray__Single__RaycastHit_Array__Single_m1961380919 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_SphereCastNonAlloc__Vector3__Single__Vector3__RaycastHit_Array(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_SphereCastNonAlloc__Vector3__Single__Vector3__RaycastHit_Array_m3306281125 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::Physics_SphereCastNonAlloc__Ray__Single__RaycastHit_Array(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_Physics_SphereCastNonAlloc__Ray__Single__RaycastHit_Array_m2586369007 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_PhysicsGenerated::__Register()
extern "C"  void UnityEngine_PhysicsGenerated___Register_m1849215868 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit[] UnityEngine_PhysicsGenerated::<Physics_BoxCastNonAlloc__Vector3__Vector3__Vector3__RaycastHit_Array__Quaternion__Single__Int32__QueryTriggerInteraction>m__2A9()
extern "C"  RaycastHitU5BU5D_t528650843* UnityEngine_PhysicsGenerated_U3CPhysics_BoxCastNonAlloc__Vector3__Vector3__Vector3__RaycastHit_Array__Quaternion__Single__Int32__QueryTriggerInteractionU3Em__2A9_m599793566 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit[] UnityEngine_PhysicsGenerated::<Physics_BoxCastNonAlloc__Vector3__Vector3__Vector3__RaycastHit_Array__Quaternion__Single__Int32>m__2AA()
extern "C"  RaycastHitU5BU5D_t528650843* UnityEngine_PhysicsGenerated_U3CPhysics_BoxCastNonAlloc__Vector3__Vector3__Vector3__RaycastHit_Array__Quaternion__Single__Int32U3Em__2AA_m152634176 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit[] UnityEngine_PhysicsGenerated::<Physics_BoxCastNonAlloc__Vector3__Vector3__Vector3__RaycastHit_Array__Quaternion__Single>m__2AB()
extern "C"  RaycastHitU5BU5D_t528650843* UnityEngine_PhysicsGenerated_U3CPhysics_BoxCastNonAlloc__Vector3__Vector3__Vector3__RaycastHit_Array__Quaternion__SingleU3Em__2AB_m2924734523 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit[] UnityEngine_PhysicsGenerated::<Physics_BoxCastNonAlloc__Vector3__Vector3__Vector3__RaycastHit_Array__Quaternion>m__2AC()
extern "C"  RaycastHitU5BU5D_t528650843* UnityEngine_PhysicsGenerated_U3CPhysics_BoxCastNonAlloc__Vector3__Vector3__Vector3__RaycastHit_Array__QuaternionU3Em__2AC_m1706667332 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit[] UnityEngine_PhysicsGenerated::<Physics_BoxCastNonAlloc__Vector3__Vector3__Vector3__RaycastHit_Array>m__2AD()
extern "C"  RaycastHitU5BU5D_t528650843* UnityEngine_PhysicsGenerated_U3CPhysics_BoxCastNonAlloc__Vector3__Vector3__Vector3__RaycastHit_ArrayU3Em__2AD_m58066019 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit[] UnityEngine_PhysicsGenerated::<Physics_CapsuleCastNonAlloc__Vector3__Vector3__Single__Vector3__RaycastHit_Array__Single__Int32__QueryTriggerInteraction>m__2AE()
extern "C"  RaycastHitU5BU5D_t528650843* UnityEngine_PhysicsGenerated_U3CPhysics_CapsuleCastNonAlloc__Vector3__Vector3__Single__Vector3__RaycastHit_Array__Single__Int32__QueryTriggerInteractionU3Em__2AE_m1700182834 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit[] UnityEngine_PhysicsGenerated::<Physics_CapsuleCastNonAlloc__Vector3__Vector3__Single__Vector3__RaycastHit_Array__Single__Int32>m__2AF()
extern "C"  RaycastHitU5BU5D_t528650843* UnityEngine_PhysicsGenerated_U3CPhysics_CapsuleCastNonAlloc__Vector3__Vector3__Single__Vector3__RaycastHit_Array__Single__Int32U3Em__2AF_m3467288189 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit[] UnityEngine_PhysicsGenerated::<Physics_CapsuleCastNonAlloc__Vector3__Vector3__Single__Vector3__RaycastHit_Array__Single>m__2B0()
extern "C"  RaycastHitU5BU5D_t528650843* UnityEngine_PhysicsGenerated_U3CPhysics_CapsuleCastNonAlloc__Vector3__Vector3__Single__Vector3__RaycastHit_Array__SingleU3Em__2B0_m3003557776 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit[] UnityEngine_PhysicsGenerated::<Physics_CapsuleCastNonAlloc__Vector3__Vector3__Single__Vector3__RaycastHit_Array>m__2B1()
extern "C"  RaycastHitU5BU5D_t528650843* UnityEngine_PhysicsGenerated_U3CPhysics_CapsuleCastNonAlloc__Vector3__Vector3__Single__Vector3__RaycastHit_ArrayU3Em__2B1_m705131673 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider[] UnityEngine_PhysicsGenerated::<Physics_OverlapBoxNonAlloc__Vector3__Vector3__Collider_Array__Quaternion__Int32__QueryTriggerInteraction>m__2B2()
extern "C"  ColliderU5BU5D_t2697150633* UnityEngine_PhysicsGenerated_U3CPhysics_OverlapBoxNonAlloc__Vector3__Vector3__Collider_Array__Quaternion__Int32__QueryTriggerInteractionU3Em__2B2_m2837866620 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider[] UnityEngine_PhysicsGenerated::<Physics_OverlapBoxNonAlloc__Vector3__Vector3__Collider_Array__Quaternion__Int32>m__2B3()
extern "C"  ColliderU5BU5D_t2697150633* UnityEngine_PhysicsGenerated_U3CPhysics_OverlapBoxNonAlloc__Vector3__Vector3__Collider_Array__Quaternion__Int32U3Em__2B3_m1750905227 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider[] UnityEngine_PhysicsGenerated::<Physics_OverlapBoxNonAlloc__Vector3__Vector3__Collider_Array__Quaternion>m__2B4()
extern "C"  ColliderU5BU5D_t2697150633* UnityEngine_PhysicsGenerated_U3CPhysics_OverlapBoxNonAlloc__Vector3__Vector3__Collider_Array__QuaternionU3Em__2B4_m4202082002 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider[] UnityEngine_PhysicsGenerated::<Physics_OverlapBoxNonAlloc__Vector3__Vector3__Collider_Array>m__2B5()
extern "C"  ColliderU5BU5D_t2697150633* UnityEngine_PhysicsGenerated_U3CPhysics_OverlapBoxNonAlloc__Vector3__Vector3__Collider_ArrayU3Em__2B5_m2030097137 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider[] UnityEngine_PhysicsGenerated::<Physics_OverlapSphereNonAlloc__Vector3__Single__Collider_Array__Int32__QueryTriggerInteraction>m__2B6()
extern "C"  ColliderU5BU5D_t2697150633* UnityEngine_PhysicsGenerated_U3CPhysics_OverlapSphereNonAlloc__Vector3__Single__Collider_Array__Int32__QueryTriggerInteractionU3Em__2B6_m1962209486 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider[] UnityEngine_PhysicsGenerated::<Physics_OverlapSphereNonAlloc__Vector3__Single__Collider_Array__Int32>m__2B7()
extern "C"  ColliderU5BU5D_t2697150633* UnityEngine_PhysicsGenerated_U3CPhysics_OverlapSphereNonAlloc__Vector3__Single__Collider_Array__Int32U3Em__2B7_m1268259713 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider[] UnityEngine_PhysicsGenerated::<Physics_OverlapSphereNonAlloc__Vector3__Single__Collider_Array>m__2B8()
extern "C"  ColliderU5BU5D_t2697150633* UnityEngine_PhysicsGenerated_U3CPhysics_OverlapSphereNonAlloc__Vector3__Single__Collider_ArrayU3Em__2B8_m1066266404 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit[] UnityEngine_PhysicsGenerated::<Physics_RaycastNonAlloc__Vector3__Vector3__RaycastHit_Array__Single__Int32__QueryTriggerInteraction>m__2B9()
extern "C"  RaycastHitU5BU5D_t528650843* UnityEngine_PhysicsGenerated_U3CPhysics_RaycastNonAlloc__Vector3__Vector3__RaycastHit_Array__Single__Int32__QueryTriggerInteractionU3Em__2B9_m4077110636 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit[] UnityEngine_PhysicsGenerated::<Physics_RaycastNonAlloc__Vector3__Vector3__RaycastHit_Array__Single__Int32>m__2BA()
extern "C"  RaycastHitU5BU5D_t528650843* UnityEngine_PhysicsGenerated_U3CPhysics_RaycastNonAlloc__Vector3__Vector3__RaycastHit_Array__Single__Int32U3Em__2BA_m3236077392 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit[] UnityEngine_PhysicsGenerated::<Physics_RaycastNonAlloc__Ray__RaycastHit_Array__Single__Int32__QueryTriggerInteraction>m__2BB()
extern "C"  RaycastHitU5BU5D_t528650843* UnityEngine_PhysicsGenerated_U3CPhysics_RaycastNonAlloc__Ray__RaycastHit_Array__Single__Int32__QueryTriggerInteractionU3Em__2BB_m675786441 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit[] UnityEngine_PhysicsGenerated::<Physics_RaycastNonAlloc__Vector3__Vector3__RaycastHit_Array__Single>m__2BC()
extern "C"  RaycastHitU5BU5D_t528650843* UnityEngine_PhysicsGenerated_U3CPhysics_RaycastNonAlloc__Vector3__Vector3__RaycastHit_Array__SingleU3Em__2BC_m3540061642 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit[] UnityEngine_PhysicsGenerated::<Physics_RaycastNonAlloc__Ray__RaycastHit_Array__Single__Int32>m__2BD()
extern "C"  RaycastHitU5BU5D_t528650843* UnityEngine_PhysicsGenerated_U3CPhysics_RaycastNonAlloc__Ray__RaycastHit_Array__Single__Int32U3Em__2BD_m563192959 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit[] UnityEngine_PhysicsGenerated::<Physics_RaycastNonAlloc__Ray__RaycastHit_Array__Single>m__2BE()
extern "C"  RaycastHitU5BU5D_t528650843* UnityEngine_PhysicsGenerated_U3CPhysics_RaycastNonAlloc__Ray__RaycastHit_Array__SingleU3Em__2BE_m1101540576 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit[] UnityEngine_PhysicsGenerated::<Physics_RaycastNonAlloc__Vector3__Vector3__RaycastHit_Array>m__2BF()
extern "C"  RaycastHitU5BU5D_t528650843* UnityEngine_PhysicsGenerated_U3CPhysics_RaycastNonAlloc__Vector3__Vector3__RaycastHit_ArrayU3Em__2BF_m3758537045 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit[] UnityEngine_PhysicsGenerated::<Physics_RaycastNonAlloc__Ray__RaycastHit_Array>m__2C0()
extern "C"  RaycastHitU5BU5D_t528650843* UnityEngine_PhysicsGenerated_U3CPhysics_RaycastNonAlloc__Ray__RaycastHit_ArrayU3Em__2C0_m2251867826 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit[] UnityEngine_PhysicsGenerated::<Physics_SphereCastNonAlloc__Vector3__Single__Vector3__RaycastHit_Array__Single__Int32__QueryTriggerInteraction>m__2C1()
extern "C"  RaycastHitU5BU5D_t528650843* UnityEngine_PhysicsGenerated_U3CPhysics_SphereCastNonAlloc__Vector3__Single__Vector3__RaycastHit_Array__Single__Int32__QueryTriggerInteractionU3Em__2C1_m4179800676 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit[] UnityEngine_PhysicsGenerated::<Physics_SphereCastNonAlloc__Vector3__Single__Vector3__RaycastHit_Array__Single__Int32>m__2C2()
extern "C"  RaycastHitU5BU5D_t528650843* UnityEngine_PhysicsGenerated_U3CPhysics_SphereCastNonAlloc__Vector3__Single__Vector3__RaycastHit_Array__Single__Int32U3Em__2C2_m2345499039 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit[] UnityEngine_PhysicsGenerated::<Physics_SphereCastNonAlloc__Ray__Single__RaycastHit_Array__Single__Int32__QueryTriggerInteraction>m__2C3()
extern "C"  RaycastHitU5BU5D_t528650843* UnityEngine_PhysicsGenerated_U3CPhysics_SphereCastNonAlloc__Ray__Single__RaycastHit_Array__Single__Int32__QueryTriggerInteractionU3Em__2C3_m1088405656 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit[] UnityEngine_PhysicsGenerated::<Physics_SphereCastNonAlloc__Vector3__Single__Vector3__RaycastHit_Array__Single>m__2C4()
extern "C"  RaycastHitU5BU5D_t528650843* UnityEngine_PhysicsGenerated_U3CPhysics_SphereCastNonAlloc__Vector3__Single__Vector3__RaycastHit_Array__SingleU3Em__2C4_m2153423995 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit[] UnityEngine_PhysicsGenerated::<Physics_SphereCastNonAlloc__Ray__Single__RaycastHit_Array__Single__Int32>m__2C5()
extern "C"  RaycastHitU5BU5D_t528650843* UnityEngine_PhysicsGenerated_U3CPhysics_SphereCastNonAlloc__Ray__Single__RaycastHit_Array__Single__Int32U3Em__2C5_m1528211952 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit[] UnityEngine_PhysicsGenerated::<Physics_SphereCastNonAlloc__Ray__Single__RaycastHit_Array__Single>m__2C6()
extern "C"  RaycastHitU5BU5D_t528650843* UnityEngine_PhysicsGenerated_U3CPhysics_SphereCastNonAlloc__Ray__Single__RaycastHit_Array__SingleU3Em__2C6_m1557068463 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit[] UnityEngine_PhysicsGenerated::<Physics_SphereCastNonAlloc__Vector3__Single__Vector3__RaycastHit_Array>m__2C7()
extern "C"  RaycastHitU5BU5D_t528650843* UnityEngine_PhysicsGenerated_U3CPhysics_SphereCastNonAlloc__Vector3__Single__Vector3__RaycastHit_ArrayU3Em__2C7_m3490808070 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit[] UnityEngine_PhysicsGenerated::<Physics_SphereCastNonAlloc__Ray__Single__RaycastHit_Array>m__2C8()
extern "C"  RaycastHitU5BU5D_t528650843* UnityEngine_PhysicsGenerated_U3CPhysics_SphereCastNonAlloc__Ray__Single__RaycastHit_ArrayU3Em__2C8_m1980802425 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_PhysicsGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UnityEngine_PhysicsGenerated_ilo_getObject1_m2292743879 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_PhysicsGenerated::ilo_setInt322(System.Int32,System.Int32)
extern "C"  void UnityEngine_PhysicsGenerated_ilo_setInt322_m2310240362 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_PhysicsGenerated::ilo_setVector3S3(System.Int32,UnityEngine.Vector3)
extern "C"  void UnityEngine_PhysicsGenerated_ilo_setVector3S3_m3222344894 (Il2CppObject * __this /* static, unused */, int32_t ___e0, Vector3_t4282066566  ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine_PhysicsGenerated::ilo_getVector3S4(System.Int32)
extern "C"  Vector3_t4282066566  UnityEngine_PhysicsGenerated_ilo_getVector3S4_m117385714 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_PhysicsGenerated::ilo_setSingle5(System.Int32,System.Single)
extern "C"  void UnityEngine_PhysicsGenerated_ilo_setSingle5_m3252096621 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicsGenerated::ilo_getBooleanS6(System.Int32)
extern "C"  bool UnityEngine_PhysicsGenerated_ilo_getBooleanS6_m2800306566 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_PhysicsGenerated::ilo_getObject7(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_PhysicsGenerated_ilo_getObject7_m1671399402 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine_PhysicsGenerated::ilo_getSingle8(System.Int32)
extern "C"  float UnityEngine_PhysicsGenerated_ilo_getSingle8_m3584598947 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_PhysicsGenerated::ilo_getInt329(System.Int32)
extern "C"  int32_t UnityEngine_PhysicsGenerated_ilo_getInt329_m2421582842 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_PhysicsGenerated::ilo_setBooleanS10(System.Int32,System.Boolean)
extern "C"  void UnityEngine_PhysicsGenerated_ilo_setBooleanS10_m2629572202 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_PhysicsGenerated::ilo_incArgIndex11()
extern "C"  int32_t UnityEngine_PhysicsGenerated_ilo_incArgIndex11_m3899699650 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_PhysicsGenerated::ilo_setObject12(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UnityEngine_PhysicsGenerated_ilo_setObject12_m1392651071 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_PhysicsGenerated::ilo_moveSaveID2Arr13(System.Int32)
extern "C"  void UnityEngine_PhysicsGenerated_ilo_moveSaveID2Arr13_m1833242583 (Il2CppObject * __this /* static, unused */, int32_t ___arrIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_PhysicsGenerated::ilo_setArrayS14(System.Int32,System.Int32,System.Boolean)
extern "C"  void UnityEngine_PhysicsGenerated_ilo_setArrayS14_m3466653228 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___count1, bool ___bClear2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_PhysicsGenerated::ilo_getEnum15(System.Int32)
extern "C"  int32_t UnityEngine_PhysicsGenerated_ilo_getEnum15_m3017652724 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_PhysicsGenerated::ilo_setArgIndex16(System.Int32)
extern "C"  void UnityEngine_PhysicsGenerated_ilo_setArgIndex16_m2377287466 (Il2CppObject * __this /* static, unused */, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_PhysicsGenerated::ilo_getElement17(System.Int32,System.Int32)
extern "C"  int32_t UnityEngine_PhysicsGenerated_ilo_getElement17_m444078816 (Il2CppObject * __this /* static, unused */, int32_t ___id0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_PhysicsGenerated::ilo_getArrayLength18(System.Int32)
extern "C"  int32_t UnityEngine_PhysicsGenerated_ilo_getArrayLength18_m2584647099 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
