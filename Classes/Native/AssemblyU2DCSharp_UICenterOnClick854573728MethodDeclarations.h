﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UICenterOnClick
struct UICenterOnClick_t854573728;
// UnityEngine.Transform
struct Transform_t1659122786;
// UIRect
struct UIRect_t2503437976;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_UIRect2503437976.h"

// System.Void UICenterOnClick::.ctor()
extern "C"  void UICenterOnClick__ctor_m3518560459 (UICenterOnClick_t854573728 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICenterOnClick::OnClick()
extern "C"  void UICenterOnClick_OnClick_m3041544338 (UICenterOnClick_t854573728 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UICenterOnClick::ilo_get_cachedTransform1(UIRect)
extern "C"  Transform_t1659122786 * UICenterOnClick_ilo_get_cachedTransform1_m4239425862 (Il2CppObject * __this /* static, unused */, UIRect_t2503437976 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UICenterOnClick::ilo_get_cachedGameObject2(UIRect)
extern "C"  GameObject_t3674682005 * UICenterOnClick_ilo_get_cachedGameObject2_m3820148713 (Il2CppObject * __this /* static, unused */, UIRect_t2503437976 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
