﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.WheelJoint2D
struct WheelJoint2D_t2492372869;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_JointSuspension2D939016335.h"
#include "UnityEngine_UnityEngine_JointMotor2D682576033.h"
#include "UnityEngine_UnityEngine_WheelJoint2D2492372869.h"

// System.Void UnityEngine.WheelJoint2D::.ctor()
extern "C"  void WheelJoint2D__ctor_m4092272908 (WheelJoint2D_t2492372869 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.JointSuspension2D UnityEngine.WheelJoint2D::get_suspension()
extern "C"  JointSuspension2D_t939016335  WheelJoint2D_get_suspension_m754877071 (WheelJoint2D_t2492372869 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WheelJoint2D::set_suspension(UnityEngine.JointSuspension2D)
extern "C"  void WheelJoint2D_set_suspension_m2148642238 (WheelJoint2D_t2492372869 * __this, JointSuspension2D_t939016335  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WheelJoint2D::INTERNAL_get_suspension(UnityEngine.JointSuspension2D&)
extern "C"  void WheelJoint2D_INTERNAL_get_suspension_m1586745550 (WheelJoint2D_t2492372869 * __this, JointSuspension2D_t939016335 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WheelJoint2D::INTERNAL_set_suspension(UnityEngine.JointSuspension2D&)
extern "C"  void WheelJoint2D_INTERNAL_set_suspension_m271479874 (WheelJoint2D_t2492372869 * __this, JointSuspension2D_t939016335 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.WheelJoint2D::get_useMotor()
extern "C"  bool WheelJoint2D_get_useMotor_m2297864373 (WheelJoint2D_t2492372869 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WheelJoint2D::set_useMotor(System.Boolean)
extern "C"  void WheelJoint2D_set_useMotor_m1321090938 (WheelJoint2D_t2492372869 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.JointMotor2D UnityEngine.WheelJoint2D::get_motor()
extern "C"  JointMotor2D_t682576033  WheelJoint2D_get_motor_m1572121627 (WheelJoint2D_t2492372869 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WheelJoint2D::set_motor(UnityEngine.JointMotor2D)
extern "C"  void WheelJoint2D_set_motor_m755531086 (WheelJoint2D_t2492372869 * __this, JointMotor2D_t682576033  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WheelJoint2D::INTERNAL_get_motor(UnityEngine.JointMotor2D&)
extern "C"  void WheelJoint2D_INTERNAL_get_motor_m1026798398 (WheelJoint2D_t2492372869 * __this, JointMotor2D_t682576033 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WheelJoint2D::INTERNAL_set_motor(UnityEngine.JointMotor2D&)
extern "C"  void WheelJoint2D_INTERNAL_set_motor_m2955564978 (WheelJoint2D_t2492372869 * __this, JointMotor2D_t682576033 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.WheelJoint2D::get_jointTranslation()
extern "C"  float WheelJoint2D_get_jointTranslation_m3851739444 (WheelJoint2D_t2492372869 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.WheelJoint2D::get_jointSpeed()
extern "C"  float WheelJoint2D_get_jointSpeed_m1907350218 (WheelJoint2D_t2492372869 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.WheelJoint2D::GetMotorTorque(System.Single)
extern "C"  float WheelJoint2D_GetMotorTorque_m1789641054 (WheelJoint2D_t2492372869 * __this, float ___timeStep0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.WheelJoint2D::INTERNAL_CALL_GetMotorTorque(UnityEngine.WheelJoint2D,System.Single)
extern "C"  float WheelJoint2D_INTERNAL_CALL_GetMotorTorque_m2528075417 (Il2CppObject * __this /* static, unused */, WheelJoint2D_t2492372869 * ___self0, float ___timeStep1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
