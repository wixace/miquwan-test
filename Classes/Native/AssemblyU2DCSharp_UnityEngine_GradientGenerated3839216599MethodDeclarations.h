﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_GradientGenerated
struct UnityEngine_GradientGenerated_t3839216599;
// JSVCall
struct JSVCall_t3708497963;
// UnityEngine.GradientColorKey[]
struct GradientColorKeyU5BU5D_t2977343473;
// UnityEngine.GradientAlphaKey[]
struct GradientAlphaKeyU5BU5D_t1918196120;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"

// System.Void UnityEngine_GradientGenerated::.ctor()
extern "C"  void UnityEngine_GradientGenerated__ctor_m4146603892 (UnityEngine_GradientGenerated_t3839216599 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GradientGenerated::Gradient_Gradient1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GradientGenerated_Gradient_Gradient1_m1102426494 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GradientGenerated::Gradient_colorKeys(JSVCall)
extern "C"  void UnityEngine_GradientGenerated_Gradient_colorKeys_m2917401071 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GradientGenerated::Gradient_alphaKeys(JSVCall)
extern "C"  void UnityEngine_GradientGenerated_Gradient_alphaKeys_m1146081236 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GradientGenerated::Gradient_Evaluate__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GradientGenerated_Gradient_Evaluate__Single_m3489073286 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GradientGenerated::Gradient_SetKeys__GradientColorKey_Array__GradientAlphaKey_Array(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GradientGenerated_Gradient_SetKeys__GradientColorKey_Array__GradientAlphaKey_Array_m2403079140 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GradientGenerated::__Register()
extern "C"  void UnityEngine_GradientGenerated___Register_m4201373139 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GradientColorKey[] UnityEngine_GradientGenerated::<Gradient_colorKeys>m__252()
extern "C"  GradientColorKeyU5BU5D_t2977343473* UnityEngine_GradientGenerated_U3CGradient_colorKeysU3Em__252_m1349910996 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GradientAlphaKey[] UnityEngine_GradientGenerated::<Gradient_alphaKeys>m__253()
extern "C"  GradientAlphaKeyU5BU5D_t1918196120* UnityEngine_GradientGenerated_U3CGradient_alphaKeysU3Em__253_m57796831 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GradientColorKey[] UnityEngine_GradientGenerated::<Gradient_SetKeys__GradientColorKey_Array__GradientAlphaKey_Array>m__254()
extern "C"  GradientColorKeyU5BU5D_t2977343473* UnityEngine_GradientGenerated_U3CGradient_SetKeys__GradientColorKey_Array__GradientAlphaKey_ArrayU3Em__254_m3450450694 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GradientAlphaKey[] UnityEngine_GradientGenerated::<Gradient_SetKeys__GradientColorKey_Array__GradientAlphaKey_Array>m__255()
extern "C"  GradientAlphaKeyU5BU5D_t1918196120* UnityEngine_GradientGenerated_U3CGradient_SetKeys__GradientColorKey_Array__GradientAlphaKey_ArrayU3Em__255_m3431867244 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GradientGenerated::ilo_addJSCSRel1(System.Int32,System.Object)
extern "C"  void UnityEngine_GradientGenerated_ilo_addJSCSRel1_m1241257008 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_GradientGenerated::ilo_setObject2(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UnityEngine_GradientGenerated_ilo_setObject2_m867771771 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GradientGenerated::ilo_setArrayS3(System.Int32,System.Int32,System.Boolean)
extern "C"  void UnityEngine_GradientGenerated_ilo_setArrayS3_m4251364753 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___count1, bool ___bClear2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_GradientGenerated::ilo_getObject4(System.Int32)
extern "C"  int32_t UnityEngine_GradientGenerated_ilo_getObject4_m3115526789 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_GradientGenerated::ilo_getElement5(System.Int32,System.Int32)
extern "C"  int32_t UnityEngine_GradientGenerated_ilo_getElement5_m618751498 (Il2CppObject * __this /* static, unused */, int32_t ___id0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_GradientGenerated::ilo_getObject6(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_GradientGenerated_ilo_getObject6_m3654849718 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_GradientGenerated::ilo_getArrayLength7(System.Int32)
extern "C"  int32_t UnityEngine_GradientGenerated_ilo_getArrayLength7_m1143801196 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
