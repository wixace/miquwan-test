﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>
struct List_1_t1195184494;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1214857264.h"
#include "UnityEngine_UnityEngine_MeshSubsetCombineUtility_M4121966238.h"

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m901610501_gshared (Enumerator_t1214857264 * __this, List_1_t1195184494 * ___l0, const MethodInfo* method);
#define Enumerator__ctor_m901610501(__this, ___l0, method) ((  void (*) (Enumerator_t1214857264 *, List_1_t1195184494 *, const MethodInfo*))Enumerator__ctor_m901610501_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2155571501_gshared (Enumerator_t1214857264 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m2155571501(__this, method) ((  void (*) (Enumerator_t1214857264 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2155571501_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1956010777_gshared (Enumerator_t1214857264 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1956010777(__this, method) ((  Il2CppObject * (*) (Enumerator_t1214857264 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1956010777_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::Dispose()
extern "C"  void Enumerator_Dispose_m3381762858_gshared (Enumerator_t1214857264 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m3381762858(__this, method) ((  void (*) (Enumerator_t1214857264 *, const MethodInfo*))Enumerator_Dispose_m3381762858_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::VerifyState()
extern "C"  void Enumerator_VerifyState_m2362422627_gshared (Enumerator_t1214857264 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m2362422627(__this, method) ((  void (*) (Enumerator_t1214857264 *, const MethodInfo*))Enumerator_VerifyState_m2362422627_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2077689753_gshared (Enumerator_t1214857264 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m2077689753(__this, method) ((  bool (*) (Enumerator_t1214857264 *, const MethodInfo*))Enumerator_MoveNext_m2077689753_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::get_Current()
extern "C"  MeshInstance_t4121966238  Enumerator_get_Current_m3853416922_gshared (Enumerator_t1214857264 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m3853416922(__this, method) ((  MeshInstance_t4121966238  (*) (Enumerator_t1214857264 *, const MethodInfo*))Enumerator_get_Current_m3853416922_gshared)(__this, method)
