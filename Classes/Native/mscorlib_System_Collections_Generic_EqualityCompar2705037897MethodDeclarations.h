﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1<Pathfinding.Voxels.VoxelContour>
struct EqualityComparer_1_t2705037897;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System.Collections.Generic.EqualityComparer`1<Pathfinding.Voxels.VoxelContour>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m2963149482_gshared (EqualityComparer_1_t2705037897 * __this, const MethodInfo* method);
#define EqualityComparer_1__ctor_m2963149482(__this, method) ((  void (*) (EqualityComparer_1_t2705037897 *, const MethodInfo*))EqualityComparer_1__ctor_m2963149482_gshared)(__this, method)
// System.Void System.Collections.Generic.EqualityComparer`1<Pathfinding.Voxels.VoxelContour>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m1181224515_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1__cctor_m1181224515(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EqualityComparer_1__cctor_m1181224515_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1<Pathfinding.Voxels.VoxelContour>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m1476471939_gshared (EqualityComparer_1_t2705037897 * __this, Il2CppObject * ___obj0, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m1476471939(__this, ___obj0, method) ((  int32_t (*) (EqualityComparer_1_t2705037897 *, Il2CppObject *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m1476471939_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1<Pathfinding.Voxels.VoxelContour>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m849225319_gshared (EqualityComparer_1_t2705037897 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m849225319(__this, ___x0, ___y1, method) ((  bool (*) (EqualityComparer_1_t2705037897 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m849225319_gshared)(__this, ___x0, ___y1, method)
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<Pathfinding.Voxels.VoxelContour>::get_Default()
extern "C"  EqualityComparer_1_t2705037897 * EqualityComparer_1_get_Default_m4098080468_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1_get_Default_m4098080468(__this /* static, unused */, method) ((  EqualityComparer_1_t2705037897 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EqualityComparer_1_get_Default_m4098080468_gshared)(__this /* static, unused */, method)
