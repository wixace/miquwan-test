﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PluginsTaiqi/LoginResult
struct  LoginResult_t2998189643  : public Il2CppObject
{
public:
	// System.String PluginsTaiqi/LoginResult::token
	String_t* ___token_0;
	// System.String PluginsTaiqi/LoginResult::channelId
	String_t* ___channelId_1;
	// System.String PluginsTaiqi/LoginResult::userId
	String_t* ___userId_2;
	// System.String PluginsTaiqi/LoginResult::channelAction
	String_t* ___channelAction_3;
	// System.String PluginsTaiqi/LoginResult::extInfo
	String_t* ___extInfo_4;

public:
	inline static int32_t get_offset_of_token_0() { return static_cast<int32_t>(offsetof(LoginResult_t2998189643, ___token_0)); }
	inline String_t* get_token_0() const { return ___token_0; }
	inline String_t** get_address_of_token_0() { return &___token_0; }
	inline void set_token_0(String_t* value)
	{
		___token_0 = value;
		Il2CppCodeGenWriteBarrier(&___token_0, value);
	}

	inline static int32_t get_offset_of_channelId_1() { return static_cast<int32_t>(offsetof(LoginResult_t2998189643, ___channelId_1)); }
	inline String_t* get_channelId_1() const { return ___channelId_1; }
	inline String_t** get_address_of_channelId_1() { return &___channelId_1; }
	inline void set_channelId_1(String_t* value)
	{
		___channelId_1 = value;
		Il2CppCodeGenWriteBarrier(&___channelId_1, value);
	}

	inline static int32_t get_offset_of_userId_2() { return static_cast<int32_t>(offsetof(LoginResult_t2998189643, ___userId_2)); }
	inline String_t* get_userId_2() const { return ___userId_2; }
	inline String_t** get_address_of_userId_2() { return &___userId_2; }
	inline void set_userId_2(String_t* value)
	{
		___userId_2 = value;
		Il2CppCodeGenWriteBarrier(&___userId_2, value);
	}

	inline static int32_t get_offset_of_channelAction_3() { return static_cast<int32_t>(offsetof(LoginResult_t2998189643, ___channelAction_3)); }
	inline String_t* get_channelAction_3() const { return ___channelAction_3; }
	inline String_t** get_address_of_channelAction_3() { return &___channelAction_3; }
	inline void set_channelAction_3(String_t* value)
	{
		___channelAction_3 = value;
		Il2CppCodeGenWriteBarrier(&___channelAction_3, value);
	}

	inline static int32_t get_offset_of_extInfo_4() { return static_cast<int32_t>(offsetof(LoginResult_t2998189643, ___extInfo_4)); }
	inline String_t* get_extInfo_4() const { return ___extInfo_4; }
	inline String_t** get_address_of_extInfo_4() { return &___extInfo_4; }
	inline void set_extInfo_4(String_t* value)
	{
		___extInfo_4 = value;
		Il2CppCodeGenWriteBarrier(&___extInfo_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
