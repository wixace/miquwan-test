﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIToggledObjects
struct UIToggledObjects_t793825048;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// EventDelegate
struct EventDelegate_t4004424223;
// System.Collections.Generic.List`1<EventDelegate>
struct List_1_t1077642479;
// EventDelegate/Callback
struct Callback_t1094463061;
// UIToggle
struct UIToggle_t688812808;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "AssemblyU2DCSharp_EventDelegate_Callback1094463061.h"
#include "AssemblyU2DCSharp_UIToggle688812808.h"
#include "AssemblyU2DCSharp_UIToggledObjects793825048.h"

// System.Void UIToggledObjects::.ctor()
extern "C"  void UIToggledObjects__ctor_m1012912771 (UIToggledObjects_t793825048 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIToggledObjects::Awake()
extern "C"  void UIToggledObjects_Awake_m1250517990 (UIToggledObjects_t793825048 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIToggledObjects::Toggle()
extern "C"  void UIToggledObjects_Toggle_m523263157 (UIToggledObjects_t793825048 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIToggledObjects::Set(UnityEngine.GameObject,System.Boolean)
extern "C"  void UIToggledObjects_Set_m1453031266 (UIToggledObjects_t793825048 * __this, GameObject_t3674682005 * ___go0, bool ___state1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// EventDelegate UIToggledObjects::ilo_Add1(System.Collections.Generic.List`1<EventDelegate>,EventDelegate/Callback)
extern "C"  EventDelegate_t4004424223 * UIToggledObjects_ilo_Add1_m3875803702 (Il2CppObject * __this /* static, unused */, List_1_t1077642479 * ___list0, Callback_t1094463061 * ___callback1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIToggledObjects::ilo_get_value2(UIToggle)
extern "C"  bool UIToggledObjects_ilo_get_value2_m1094110730 (Il2CppObject * __this /* static, unused */, UIToggle_t688812808 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIToggledObjects::ilo_Set3(UIToggledObjects,UnityEngine.GameObject,System.Boolean)
extern "C"  void UIToggledObjects_ilo_Set3_m1537459340 (Il2CppObject * __this /* static, unused */, UIToggledObjects_t793825048 * ____this0, GameObject_t3674682005 * ___go1, bool ___state2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIToggledObjects::ilo_SetActive4(UnityEngine.GameObject,System.Boolean)
extern "C"  void UIToggledObjects_ilo_SetActive4_m1628757189 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___go0, bool ___state1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
