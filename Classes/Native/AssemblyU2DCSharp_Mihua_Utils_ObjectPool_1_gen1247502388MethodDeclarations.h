﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Mihua_Utils_ObjectPool_1_gen2200870896MethodDeclarations.h"

// System.Void Mihua.Utils.ObjectPool`1<Mihua.Utils.WaitForSeconds>::.cctor()
#define ObjectPool_1__cctor_m4193642357(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ObjectPool_1__cctor_m2632395647_gshared)(__this /* static, unused */, method)
// System.Void Mihua.Utils.ObjectPool`1<Mihua.Utils.WaitForSeconds>::Recycle(T)
#define ObjectPool_1_Recycle_m3401767317(__this /* static, unused */, ___obj0, method) ((  void (*) (Il2CppObject * /* static, unused */, WaitForSeconds_t3217447863 *, const MethodInfo*))ObjectPool_1_Recycle_m1987265311_gshared)(__this /* static, unused */, ___obj0, method)
// System.Int32 Mihua.Utils.ObjectPool`1<Mihua.Utils.WaitForSeconds>::get_maxCount()
#define ObjectPool_1_get_maxCount_m1024403150(__this /* static, unused */, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ObjectPool_1_get_maxCount_m713746044_gshared)(__this /* static, unused */, method)
// System.Void Mihua.Utils.ObjectPool`1<Mihua.Utils.WaitForSeconds>::set_maxCount(System.Int32)
#define ObjectPool_1_set_maxCount_m3852543621(__this /* static, unused */, ___value0, method) ((  void (*) (Il2CppObject * /* static, unused */, int32_t, const MethodInfo*))ObjectPool_1_set_maxCount_m1492204559_gshared)(__this /* static, unused */, ___value0, method)
// System.Int32 Mihua.Utils.ObjectPool`1<Mihua.Utils.WaitForSeconds>::GetTotalCreated()
#define ObjectPool_1_GetTotalCreated_m1061072706(__this /* static, unused */, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ObjectPool_1_GetTotalCreated_m1929750740_gshared)(__this /* static, unused */, method)
// System.Int32 Mihua.Utils.ObjectPool`1<Mihua.Utils.WaitForSeconds>::GetSize()
#define ObjectPool_1_GetSize_m1364265471(__this /* static, unused */, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ObjectPool_1_GetSize_m2045490577_gshared)(__this /* static, unused */, method)
// T Mihua.Utils.ObjectPool`1<Mihua.Utils.WaitForSeconds>::GetObj()
#define ObjectPool_1_GetObj_m698170636(__this /* static, unused */, method) ((  WaitForSeconds_t3217447863 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ObjectPool_1_GetObj_m2492700728_gshared)(__this /* static, unused */, method)
// System.Void Mihua.Utils.ObjectPool`1<Mihua.Utils.WaitForSeconds>::Cleanup()
#define ObjectPool_1_Cleanup_m1587768506(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ObjectPool_1_Cleanup_m433760752_gshared)(__this /* static, unused */, method)
