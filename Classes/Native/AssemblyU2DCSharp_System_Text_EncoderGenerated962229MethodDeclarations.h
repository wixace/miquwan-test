﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System_Text_EncoderGenerated
struct System_Text_EncoderGenerated_t962229;
// JSVCall
struct JSVCall_t3708497963;
// System.Char[]
struct CharU5BU5D_t3324145743;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System_Text_EncoderGenerated::.ctor()
extern "C"  void System_Text_EncoderGenerated__ctor_m3861041030 (System_Text_EncoderGenerated_t962229 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Text_EncoderGenerated::Encoder_Fallback(JSVCall)
extern "C"  void System_Text_EncoderGenerated_Encoder_Fallback_m1030091076 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Text_EncoderGenerated::Encoder_FallbackBuffer(JSVCall)
extern "C"  void System_Text_EncoderGenerated_Encoder_FallbackBuffer_m4253128292 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_EncoderGenerated::Encoder_Convert__Char_Array__Int32__Int32__Byte_Array__Int32__Int32__Boolean__Int32__Int32__Boolean(JSVCall,System.Int32)
extern "C"  bool System_Text_EncoderGenerated_Encoder_Convert__Char_Array__Int32__Int32__Byte_Array__Int32__Int32__Boolean__Int32__Int32__Boolean_m913425602 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_EncoderGenerated::Encoder_GetByteCount__Char_Array__Int32__Int32__Boolean(JSVCall,System.Int32)
extern "C"  bool System_Text_EncoderGenerated_Encoder_GetByteCount__Char_Array__Int32__Int32__Boolean_m2264113412 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_EncoderGenerated::Encoder_GetBytes__Char_Array__Int32__Int32__Byte_Array__Int32__Boolean(JSVCall,System.Int32)
extern "C"  bool System_Text_EncoderGenerated_Encoder_GetBytes__Char_Array__Int32__Int32__Byte_Array__Int32__Boolean_m3553537670 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_EncoderGenerated::Encoder_Reset(JSVCall,System.Int32)
extern "C"  bool System_Text_EncoderGenerated_Encoder_Reset_m2131008844 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Text_EncoderGenerated::__Register()
extern "C"  void System_Text_EncoderGenerated___Register_m613511425 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char[] System_Text_EncoderGenerated::<Encoder_Convert__Char_Array__Int32__Int32__Byte_Array__Int32__Int32__Boolean__Int32__Int32__Boolean>m__CF()
extern "C"  CharU5BU5D_t3324145743* System_Text_EncoderGenerated_U3CEncoder_Convert__Char_Array__Int32__Int32__Byte_Array__Int32__Int32__Boolean__Int32__Int32__BooleanU3Em__CF_m3637084114 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System_Text_EncoderGenerated::<Encoder_Convert__Char_Array__Int32__Int32__Byte_Array__Int32__Int32__Boolean__Int32__Int32__Boolean>m__D0()
extern "C"  ByteU5BU5D_t4260760469* System_Text_EncoderGenerated_U3CEncoder_Convert__Char_Array__Int32__Int32__Byte_Array__Int32__Int32__Boolean__Int32__Int32__BooleanU3Em__D0_m244204841 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char[] System_Text_EncoderGenerated::<Encoder_GetByteCount__Char_Array__Int32__Int32__Boolean>m__D1()
extern "C"  CharU5BU5D_t3324145743* System_Text_EncoderGenerated_U3CEncoder_GetByteCount__Char_Array__Int32__Int32__BooleanU3Em__D1_m2729003550 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char[] System_Text_EncoderGenerated::<Encoder_GetBytes__Char_Array__Int32__Int32__Byte_Array__Int32__Boolean>m__D2()
extern "C"  CharU5BU5D_t3324145743* System_Text_EncoderGenerated_U3CEncoder_GetBytes__Char_Array__Int32__Int32__Byte_Array__Int32__BooleanU3Em__D2_m1777418437 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System_Text_EncoderGenerated::<Encoder_GetBytes__Char_Array__Int32__Int32__Byte_Array__Int32__Boolean>m__D3()
extern "C"  ByteU5BU5D_t4260760469* System_Text_EncoderGenerated_U3CEncoder_GetBytes__Char_Array__Int32__Int32__Byte_Array__Int32__BooleanU3Em__D3_m361338424 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System_Text_EncoderGenerated::ilo_setObject1(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t System_Text_EncoderGenerated_ilo_setObject1_m772335044 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System_Text_EncoderGenerated::ilo_getObject2(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * System_Text_EncoderGenerated_ilo_getObject2_m821289002 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Text_EncoderGenerated::ilo_setArgIndex3(System.Int32)
extern "C"  void System_Text_EncoderGenerated_ilo_setArgIndex3_m915294903 (Il2CppObject * __this /* static, unused */, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System_Text_EncoderGenerated::ilo_getInt324(System.Int32)
extern "C"  int32_t System_Text_EncoderGenerated_ilo_getInt324_m3531495568 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System_Text_EncoderGenerated::ilo_getArrayLength5(System.Int32)
extern "C"  int32_t System_Text_EncoderGenerated_ilo_getArrayLength5_m2527406240 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System_Text_EncoderGenerated::ilo_getObject6(System.Int32)
extern "C"  int32_t System_Text_EncoderGenerated_ilo_getObject6_m3030420881 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System_Text_EncoderGenerated::ilo_getElement7(System.Int32,System.Int32)
extern "C"  int32_t System_Text_EncoderGenerated_ilo_getElement7_m2624637394 (Il2CppObject * __this /* static, unused */, int32_t ___id0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte System_Text_EncoderGenerated::ilo_getByte8(System.Int32)
extern "C"  uint8_t System_Text_EncoderGenerated_ilo_getByte8_m750308904 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
