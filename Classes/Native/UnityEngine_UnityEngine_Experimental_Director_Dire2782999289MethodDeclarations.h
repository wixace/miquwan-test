﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Experimental.Director.DirectorPlayer
struct DirectorPlayer_t2782999289;
// UnityEngine.Experimental.Director.Playable
struct Playable_t70832698;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Playab70832698.h"
#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Dire3328563876.h"

// System.Void UnityEngine.Experimental.Director.DirectorPlayer::.ctor()
extern "C"  void DirectorPlayer__ctor_m200266596 (DirectorPlayer_t2782999289 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Experimental.Director.DirectorPlayer::Play(UnityEngine.Experimental.Director.Playable,System.Object)
extern "C"  void DirectorPlayer_Play_m2985921889 (DirectorPlayer_t2782999289 * __this, Playable_t70832698 * ___playable0, Il2CppObject * ___customData1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Experimental.Director.DirectorPlayer::Play(UnityEngine.Experimental.Director.Playable)
extern "C"  void DirectorPlayer_Play_m3996708819 (DirectorPlayer_t2782999289 * __this, Playable_t70832698 * ___playable0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Experimental.Director.DirectorPlayer::PlayInternal(UnityEngine.Experimental.Director.Playable,System.Object)
extern "C"  void DirectorPlayer_PlayInternal_m1023915774 (DirectorPlayer_t2782999289 * __this, Playable_t70832698 * ___playable0, Il2CppObject * ___customData1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Experimental.Director.DirectorPlayer::Stop()
extern "C"  void DirectorPlayer_Stop_m2743856354 (DirectorPlayer_t2782999289 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Experimental.Director.DirectorPlayer::SetTime(System.Double)
extern "C"  void DirectorPlayer_SetTime_m2260783121 (DirectorPlayer_t2782999289 * __this, double ___time0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double UnityEngine.Experimental.Director.DirectorPlayer::GetTime()
extern "C"  double DirectorPlayer_GetTime_m980118594 (DirectorPlayer_t2782999289 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Experimental.Director.DirectorPlayer::SetTimeUpdateMode(UnityEngine.Experimental.Director.DirectorUpdateMode)
extern "C"  void DirectorPlayer_SetTimeUpdateMode_m2470533842 (DirectorPlayer_t2782999289 * __this, int32_t ___mode0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Experimental.Director.DirectorUpdateMode UnityEngine.Experimental.Director.DirectorPlayer::GetTimeUpdateMode()
extern "C"  int32_t DirectorPlayer_GetTimeUpdateMode_m2215678445 (DirectorPlayer_t2782999289 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
