﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_saraiSeecigea35
struct M_saraiSeecigea35_t3093906936;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_saraiSeecigea353093906936.h"

// System.Void GarbageiOS.M_saraiSeecigea35::.ctor()
extern "C"  void M_saraiSeecigea35__ctor_m3380846699 (M_saraiSeecigea35_t3093906936 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_saraiSeecigea35::M_sesu0(System.String[],System.Int32)
extern "C"  void M_saraiSeecigea35_M_sesu0_m3904966122 (M_saraiSeecigea35_t3093906936 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_saraiSeecigea35::M_fonere1(System.String[],System.Int32)
extern "C"  void M_saraiSeecigea35_M_fonere1_m489815692 (M_saraiSeecigea35_t3093906936 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_saraiSeecigea35::M_jocalllearNisi2(System.String[],System.Int32)
extern "C"  void M_saraiSeecigea35_M_jocalllearNisi2_m1682413538 (M_saraiSeecigea35_t3093906936 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_saraiSeecigea35::M_drouchorRarwhem3(System.String[],System.Int32)
extern "C"  void M_saraiSeecigea35_M_drouchorRarwhem3_m900832245 (M_saraiSeecigea35_t3093906936 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_saraiSeecigea35::M_norsaVisairse4(System.String[],System.Int32)
extern "C"  void M_saraiSeecigea35_M_norsaVisairse4_m3455644827 (M_saraiSeecigea35_t3093906936 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_saraiSeecigea35::M_melkarme5(System.String[],System.Int32)
extern "C"  void M_saraiSeecigea35_M_melkarme5_m758389699 (M_saraiSeecigea35_t3093906936 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_saraiSeecigea35::ilo_M_sesu01(GarbageiOS.M_saraiSeecigea35,System.String[],System.Int32)
extern "C"  void M_saraiSeecigea35_ilo_M_sesu01_m3405675866 (Il2CppObject * __this /* static, unused */, M_saraiSeecigea35_t3093906936 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_saraiSeecigea35::ilo_M_norsaVisairse42(GarbageiOS.M_saraiSeecigea35,System.String[],System.Int32)
extern "C"  void M_saraiSeecigea35_ilo_M_norsaVisairse42_m657536112 (Il2CppObject * __this /* static, unused */, M_saraiSeecigea35_t3093906936 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
