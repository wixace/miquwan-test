﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.String
struct String_t;
// UIEffect
struct UIEffect_t251031877;

#include "AssemblyU2DCSharp_FloatTextUnit2362298029.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Float_skill_uieffect
struct  Float_skill_uieffect_t2262955958  : public FloatTextUnit_t2362298029
{
public:
	// UnityEngine.GameObject Float_skill_uieffect::effGO
	GameObject_t3674682005 * ___effGO_3;
	// System.String Float_skill_uieffect::effGOname
	String_t* ___effGOname_4;
	// UIEffect Float_skill_uieffect::effectInit
	UIEffect_t251031877 * ___effectInit_5;

public:
	inline static int32_t get_offset_of_effGO_3() { return static_cast<int32_t>(offsetof(Float_skill_uieffect_t2262955958, ___effGO_3)); }
	inline GameObject_t3674682005 * get_effGO_3() const { return ___effGO_3; }
	inline GameObject_t3674682005 ** get_address_of_effGO_3() { return &___effGO_3; }
	inline void set_effGO_3(GameObject_t3674682005 * value)
	{
		___effGO_3 = value;
		Il2CppCodeGenWriteBarrier(&___effGO_3, value);
	}

	inline static int32_t get_offset_of_effGOname_4() { return static_cast<int32_t>(offsetof(Float_skill_uieffect_t2262955958, ___effGOname_4)); }
	inline String_t* get_effGOname_4() const { return ___effGOname_4; }
	inline String_t** get_address_of_effGOname_4() { return &___effGOname_4; }
	inline void set_effGOname_4(String_t* value)
	{
		___effGOname_4 = value;
		Il2CppCodeGenWriteBarrier(&___effGOname_4, value);
	}

	inline static int32_t get_offset_of_effectInit_5() { return static_cast<int32_t>(offsetof(Float_skill_uieffect_t2262955958, ___effectInit_5)); }
	inline UIEffect_t251031877 * get_effectInit_5() const { return ___effectInit_5; }
	inline UIEffect_t251031877 ** get_address_of_effectInit_5() { return &___effectInit_5; }
	inline void set_effectInit_5(UIEffect_t251031877 * value)
	{
		___effectInit_5 = value;
		Il2CppCodeGenWriteBarrier(&___effectInit_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
