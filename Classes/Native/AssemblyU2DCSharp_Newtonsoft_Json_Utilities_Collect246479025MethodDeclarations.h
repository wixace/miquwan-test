﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ICollection
struct ICollection_t2643922881;
// System.Collections.IList
struct IList_t1751339649;
// System.Collections.IEnumerable
struct IEnumerable_t3464557803;
// System.Type
struct Type_t;
// System.Collections.IDictionary
struct IDictionary_t537317817;
// Newtonsoft.Json.Utilities.IWrappedCollection
struct IWrappedCollection_t3896884266;
// System.Object
struct Il2CppObject;
// Newtonsoft.Json.Utilities.IWrappedList
struct IWrappedList_t1831965162;
// Newtonsoft.Json.Utilities.IWrappedDictionary
struct IWrappedDictionary_t1790279202;
// System.Action`2<System.Collections.IList,System.Boolean>
struct Action_2_t3066507156;
// System.Array
struct Il2CppArray;
// System.String
struct String_t;
// System.IFormatProvider
struct IFormatProvider_t192740775;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// System.Collections.Generic.IList`1<System.Type>
struct IList_1_t1262825681;
// System.Func`3<System.Type,System.Collections.Generic.IList`1<System.Object>,System.Object>
struct Func_3_t1067326363;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Type2863145774.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_String7231557.h"

// System.Boolean Newtonsoft.Json.Utilities.CollectionUtils::IsNullOrEmpty(System.Collections.ICollection)
extern "C"  bool CollectionUtils_IsNullOrEmpty_m3143636493 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___collection0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Utilities.CollectionUtils::AddRange(System.Collections.IList,System.Collections.IEnumerable)
extern "C"  void CollectionUtils_AddRange_m2854786381 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___initial0, Il2CppObject * ___collection1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IList Newtonsoft.Json.Utilities.CollectionUtils::CreateGenericList(System.Type)
extern "C"  Il2CppObject * CollectionUtils_CreateGenericList_m326994755 (Il2CppObject * __this /* static, unused */, Type_t * ___listType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IDictionary Newtonsoft.Json.Utilities.CollectionUtils::CreateGenericDictionary(System.Type,System.Type)
extern "C"  Il2CppObject * CollectionUtils_CreateGenericDictionary_m1909638454 (Il2CppObject * __this /* static, unused */, Type_t * ___keyType0, Type_t * ___valueType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Utilities.CollectionUtils::IsListType(System.Type)
extern "C"  bool CollectionUtils_IsListType_m3964571032 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Utilities.CollectionUtils::IsCollectionType(System.Type)
extern "C"  bool CollectionUtils_IsCollectionType_m957523352 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Utilities.CollectionUtils::IsDictionaryType(System.Type)
extern "C"  bool CollectionUtils_IsDictionaryType_m1800409760 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Utilities.IWrappedCollection Newtonsoft.Json.Utilities.CollectionUtils::CreateCollectionWrapper(System.Object)
extern "C"  Il2CppObject * CollectionUtils_CreateCollectionWrapper_m1013539234 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___list0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Utilities.IWrappedList Newtonsoft.Json.Utilities.CollectionUtils::CreateListWrapper(System.Object)
extern "C"  Il2CppObject * CollectionUtils_CreateListWrapper_m2713470114 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___list0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Utilities.IWrappedDictionary Newtonsoft.Json.Utilities.CollectionUtils::CreateDictionaryWrapper(System.Object)
extern "C"  Il2CppObject * CollectionUtils_CreateDictionaryWrapper_m33770898 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___dictionary0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Utilities.CollectionUtils::CreateAndPopulateList(System.Type,System.Action`2<System.Collections.IList,System.Boolean>)
extern "C"  Il2CppObject * CollectionUtils_CreateAndPopulateList_m3338216615 (Il2CppObject * __this /* static, unused */, Type_t * ___listType0, Action_2_t3066507156 * ___populateList1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Array Newtonsoft.Json.Utilities.CollectionUtils::ToArray(System.Array,System.Type)
extern "C"  Il2CppArray * CollectionUtils_ToArray_m2477951209 (Il2CppObject * __this /* static, unused */, Il2CppArray * ___initial0, Type_t * ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Utilities.CollectionUtils::ilo_FormatWith1(System.String,System.IFormatProvider,System.Object[])
extern "C"  String_t* CollectionUtils_ilo_FormatWith1_m3005439217 (Il2CppObject * __this /* static, unused */, String_t* ___format0, Il2CppObject * ___provider1, ObjectU5BU5D_t1108656482* ___args2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Utilities.CollectionUtils::ilo_ArgumentNotNull2(System.Object,System.String)
extern "C"  void CollectionUtils_ilo_ArgumentNotNull2_m2611745589 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, String_t* ___parameterName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Utilities.CollectionUtils::ilo_CreateGeneric3(System.Type,System.Type,System.Object[])
extern "C"  Il2CppObject * CollectionUtils_ilo_CreateGeneric3_m140319631 (Il2CppObject * __this /* static, unused */, Type_t * ___genericTypeDefinition0, Type_t * ___innerType1, ObjectU5BU5D_t1108656482* ___args2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Utilities.CollectionUtils::ilo_ImplementsGenericDefinition4(System.Type,System.Type,System.Type&)
extern "C"  bool CollectionUtils_ilo_ImplementsGenericDefinition4_m2906123129 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, Type_t * ___genericInterfaceDefinition1, Type_t ** ___implementingType2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Newtonsoft.Json.Utilities.CollectionUtils::ilo_GetCollectionItemType5(System.Type)
extern "C"  Type_t * CollectionUtils_ilo_GetCollectionItemType5_m3718415929 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Utilities.CollectionUtils::ilo_CreateGeneric6(System.Type,System.Collections.Generic.IList`1<System.Type>,System.Func`3<System.Type,System.Collections.Generic.IList`1<System.Object>,System.Object>,System.Object[])
extern "C"  Il2CppObject * CollectionUtils_ilo_CreateGeneric6_m2142065471 (Il2CppObject * __this /* static, unused */, Type_t * ___genericTypeDefinition0, Il2CppObject* ___innerTypes1, Func_3_t1067326363 * ___instanceCreator2, ObjectU5BU5D_t1108656482* ___args3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IList Newtonsoft.Json.Utilities.CollectionUtils::ilo_CreateGenericList7(System.Type)
extern "C"  Il2CppObject * CollectionUtils_ilo_CreateGenericList7_m116834403 (Il2CppObject * __this /* static, unused */, Type_t * ___listType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Utilities.CollectionUtils::ilo_IsInstantiatableType8(System.Type)
extern "C"  bool CollectionUtils_ilo_IsInstantiatableType8_m1021451690 (Il2CppObject * __this /* static, unused */, Type_t * ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Utilities.IWrappedCollection Newtonsoft.Json.Utilities.CollectionUtils::ilo_CreateCollectionWrapper9(System.Object)
extern "C"  Il2CppObject * CollectionUtils_ilo_CreateCollectionWrapper9_m552653240 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___list0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Utilities.CollectionUtils::ilo_get_UnderlyingCollection10(Newtonsoft.Json.Utilities.IWrappedCollection)
extern "C"  Il2CppObject * CollectionUtils_ilo_get_UnderlyingCollection10_m2571901889 (Il2CppObject * __this /* static, unused */, Il2CppObject * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
