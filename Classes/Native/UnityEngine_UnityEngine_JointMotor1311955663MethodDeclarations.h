﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.JointMotor
struct JointMotor_t1311955663;
struct JointMotor_t1311955663_marshaled_pinvoke;
struct JointMotor_t1311955663_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_JointMotor1311955663.h"

// System.Single UnityEngine.JointMotor::get_targetVelocity()
extern "C"  float JointMotor_get_targetVelocity_m608251589 (JointMotor_t1311955663 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.JointMotor::set_targetVelocity(System.Single)
extern "C"  void JointMotor_set_targetVelocity_m347537566 (JointMotor_t1311955663 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.JointMotor::get_force()
extern "C"  float JointMotor_get_force_m2904619190 (JointMotor_t1311955663 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.JointMotor::set_force(System.Single)
extern "C"  void JointMotor_set_force_m2587553821 (JointMotor_t1311955663 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.JointMotor::get_freeSpin()
extern "C"  bool JointMotor_get_freeSpin_m1684419295 (JointMotor_t1311955663 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.JointMotor::set_freeSpin(System.Boolean)
extern "C"  void JointMotor_set_freeSpin_m3620298404 (JointMotor_t1311955663 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct JointMotor_t1311955663;
struct JointMotor_t1311955663_marshaled_pinvoke;

extern "C" void JointMotor_t1311955663_marshal_pinvoke(const JointMotor_t1311955663& unmarshaled, JointMotor_t1311955663_marshaled_pinvoke& marshaled);
extern "C" void JointMotor_t1311955663_marshal_pinvoke_back(const JointMotor_t1311955663_marshaled_pinvoke& marshaled, JointMotor_t1311955663& unmarshaled);
extern "C" void JointMotor_t1311955663_marshal_pinvoke_cleanup(JointMotor_t1311955663_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct JointMotor_t1311955663;
struct JointMotor_t1311955663_marshaled_com;

extern "C" void JointMotor_t1311955663_marshal_com(const JointMotor_t1311955663& unmarshaled, JointMotor_t1311955663_marshaled_com& marshaled);
extern "C" void JointMotor_t1311955663_marshal_com_back(const JointMotor_t1311955663_marshaled_com& marshaled, JointMotor_t1311955663& unmarshaled);
extern "C" void JointMotor_t1311955663_marshal_com_cleanup(JointMotor_t1311955663_marshaled_com& marshaled);
