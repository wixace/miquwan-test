﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Material[]
struct MaterialU5BU5D_t170856778;
// UnityEngine.SkinnedMeshRenderer
struct SkinnedMeshRenderer_t3986041494;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HeorSetMaterial
struct  HeorSetMaterial_t636587977  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.Material[] HeorSetMaterial::NormalMat
	MaterialU5BU5D_t170856778* ___NormalMat_2;
	// UnityEngine.Material[] HeorSetMaterial::GaoguangMat
	MaterialU5BU5D_t170856778* ___GaoguangMat_3;
	// UnityEngine.SkinnedMeshRenderer HeorSetMaterial::CurRender
	SkinnedMeshRenderer_t3986041494 * ___CurRender_4;

public:
	inline static int32_t get_offset_of_NormalMat_2() { return static_cast<int32_t>(offsetof(HeorSetMaterial_t636587977, ___NormalMat_2)); }
	inline MaterialU5BU5D_t170856778* get_NormalMat_2() const { return ___NormalMat_2; }
	inline MaterialU5BU5D_t170856778** get_address_of_NormalMat_2() { return &___NormalMat_2; }
	inline void set_NormalMat_2(MaterialU5BU5D_t170856778* value)
	{
		___NormalMat_2 = value;
		Il2CppCodeGenWriteBarrier(&___NormalMat_2, value);
	}

	inline static int32_t get_offset_of_GaoguangMat_3() { return static_cast<int32_t>(offsetof(HeorSetMaterial_t636587977, ___GaoguangMat_3)); }
	inline MaterialU5BU5D_t170856778* get_GaoguangMat_3() const { return ___GaoguangMat_3; }
	inline MaterialU5BU5D_t170856778** get_address_of_GaoguangMat_3() { return &___GaoguangMat_3; }
	inline void set_GaoguangMat_3(MaterialU5BU5D_t170856778* value)
	{
		___GaoguangMat_3 = value;
		Il2CppCodeGenWriteBarrier(&___GaoguangMat_3, value);
	}

	inline static int32_t get_offset_of_CurRender_4() { return static_cast<int32_t>(offsetof(HeorSetMaterial_t636587977, ___CurRender_4)); }
	inline SkinnedMeshRenderer_t3986041494 * get_CurRender_4() const { return ___CurRender_4; }
	inline SkinnedMeshRenderer_t3986041494 ** get_address_of_CurRender_4() { return &___CurRender_4; }
	inline void set_CurRender_4(SkinnedMeshRenderer_t3986041494 * value)
	{
		___CurRender_4 = value;
		Il2CppCodeGenWriteBarrier(&___CurRender_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
