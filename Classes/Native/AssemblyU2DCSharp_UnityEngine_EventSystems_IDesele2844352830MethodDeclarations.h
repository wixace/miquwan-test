﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_EventSystems_IDeselectHandlerGenerated
struct UnityEngine_EventSystems_IDeselectHandlerGenerated_t2844352830;
// JSVCall
struct JSVCall_t3708497963;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"

// System.Void UnityEngine_EventSystems_IDeselectHandlerGenerated::.ctor()
extern "C"  void UnityEngine_EventSystems_IDeselectHandlerGenerated__ctor_m381469981 (UnityEngine_EventSystems_IDeselectHandlerGenerated_t2844352830 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_EventSystems_IDeselectHandlerGenerated::IDeselectHandler_OnDeselect__BaseEventData(JSVCall,System.Int32)
extern "C"  bool UnityEngine_EventSystems_IDeselectHandlerGenerated_IDeselectHandler_OnDeselect__BaseEventData_m1971031719 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_IDeselectHandlerGenerated::__Register()
extern "C"  void UnityEngine_EventSystems_IDeselectHandlerGenerated___Register_m2157208010 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
