﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_lechijorTeteahi82
struct  M_lechijorTeteahi82_t3106795064  : public Il2CppObject
{
public:
	// System.UInt32 GarbageiOS.M_lechijorTeteahi82::_drerpedoo
	uint32_t ____drerpedoo_0;
	// System.String GarbageiOS.M_lechijorTeteahi82::_nenalldi
	String_t* ____nenalldi_1;
	// System.Boolean GarbageiOS.M_lechijorTeteahi82::_boceaZalljamo
	bool ____boceaZalljamo_2;
	// System.String GarbageiOS.M_lechijorTeteahi82::_nadrarouRalmuli
	String_t* ____nadrarouRalmuli_3;
	// System.String GarbageiOS.M_lechijorTeteahi82::_tawxaw
	String_t* ____tawxaw_4;
	// System.Single GarbageiOS.M_lechijorTeteahi82::_rowpaspalKayqome
	float ____rowpaspalKayqome_5;
	// System.Int32 GarbageiOS.M_lechijorTeteahi82::_nawchowHowsallay
	int32_t ____nawchowHowsallay_6;
	// System.Single GarbageiOS.M_lechijorTeteahi82::_zemxurjem
	float ____zemxurjem_7;
	// System.Single GarbageiOS.M_lechijorTeteahi82::_stulePisroo
	float ____stulePisroo_8;
	// System.Int32 GarbageiOS.M_lechijorTeteahi82::_coujawwer
	int32_t ____coujawwer_9;
	// System.Boolean GarbageiOS.M_lechijorTeteahi82::_moodallTafearche
	bool ____moodallTafearche_10;
	// System.UInt32 GarbageiOS.M_lechijorTeteahi82::_xouraroTrubanall
	uint32_t ____xouraroTrubanall_11;
	// System.Boolean GarbageiOS.M_lechijorTeteahi82::_jouda
	bool ____jouda_12;
	// System.Single GarbageiOS.M_lechijorTeteahi82::_hurgaFounis
	float ____hurgaFounis_13;
	// System.String GarbageiOS.M_lechijorTeteahi82::_belyegem
	String_t* ____belyegem_14;
	// System.Boolean GarbageiOS.M_lechijorTeteahi82::_doujallfuSakalfai
	bool ____doujallfuSakalfai_15;
	// System.Boolean GarbageiOS.M_lechijorTeteahi82::_gemluralTabedem
	bool ____gemluralTabedem_16;
	// System.Single GarbageiOS.M_lechijorTeteahi82::_mairdral
	float ____mairdral_17;
	// System.Single GarbageiOS.M_lechijorTeteahi82::_sereto
	float ____sereto_18;
	// System.Int32 GarbageiOS.M_lechijorTeteahi82::_ronallDrelba
	int32_t ____ronallDrelba_19;
	// System.Boolean GarbageiOS.M_lechijorTeteahi82::_valjeebowDalmem
	bool ____valjeebowDalmem_20;
	// System.UInt32 GarbageiOS.M_lechijorTeteahi82::_xasnawJourasgee
	uint32_t ____xasnawJourasgee_21;
	// System.Boolean GarbageiOS.M_lechijorTeteahi82::_kowfirroDootrudas
	bool ____kowfirroDootrudas_22;
	// System.Int32 GarbageiOS.M_lechijorTeteahi82::_hafoubo
	int32_t ____hafoubo_23;

public:
	inline static int32_t get_offset_of__drerpedoo_0() { return static_cast<int32_t>(offsetof(M_lechijorTeteahi82_t3106795064, ____drerpedoo_0)); }
	inline uint32_t get__drerpedoo_0() const { return ____drerpedoo_0; }
	inline uint32_t* get_address_of__drerpedoo_0() { return &____drerpedoo_0; }
	inline void set__drerpedoo_0(uint32_t value)
	{
		____drerpedoo_0 = value;
	}

	inline static int32_t get_offset_of__nenalldi_1() { return static_cast<int32_t>(offsetof(M_lechijorTeteahi82_t3106795064, ____nenalldi_1)); }
	inline String_t* get__nenalldi_1() const { return ____nenalldi_1; }
	inline String_t** get_address_of__nenalldi_1() { return &____nenalldi_1; }
	inline void set__nenalldi_1(String_t* value)
	{
		____nenalldi_1 = value;
		Il2CppCodeGenWriteBarrier(&____nenalldi_1, value);
	}

	inline static int32_t get_offset_of__boceaZalljamo_2() { return static_cast<int32_t>(offsetof(M_lechijorTeteahi82_t3106795064, ____boceaZalljamo_2)); }
	inline bool get__boceaZalljamo_2() const { return ____boceaZalljamo_2; }
	inline bool* get_address_of__boceaZalljamo_2() { return &____boceaZalljamo_2; }
	inline void set__boceaZalljamo_2(bool value)
	{
		____boceaZalljamo_2 = value;
	}

	inline static int32_t get_offset_of__nadrarouRalmuli_3() { return static_cast<int32_t>(offsetof(M_lechijorTeteahi82_t3106795064, ____nadrarouRalmuli_3)); }
	inline String_t* get__nadrarouRalmuli_3() const { return ____nadrarouRalmuli_3; }
	inline String_t** get_address_of__nadrarouRalmuli_3() { return &____nadrarouRalmuli_3; }
	inline void set__nadrarouRalmuli_3(String_t* value)
	{
		____nadrarouRalmuli_3 = value;
		Il2CppCodeGenWriteBarrier(&____nadrarouRalmuli_3, value);
	}

	inline static int32_t get_offset_of__tawxaw_4() { return static_cast<int32_t>(offsetof(M_lechijorTeteahi82_t3106795064, ____tawxaw_4)); }
	inline String_t* get__tawxaw_4() const { return ____tawxaw_4; }
	inline String_t** get_address_of__tawxaw_4() { return &____tawxaw_4; }
	inline void set__tawxaw_4(String_t* value)
	{
		____tawxaw_4 = value;
		Il2CppCodeGenWriteBarrier(&____tawxaw_4, value);
	}

	inline static int32_t get_offset_of__rowpaspalKayqome_5() { return static_cast<int32_t>(offsetof(M_lechijorTeteahi82_t3106795064, ____rowpaspalKayqome_5)); }
	inline float get__rowpaspalKayqome_5() const { return ____rowpaspalKayqome_5; }
	inline float* get_address_of__rowpaspalKayqome_5() { return &____rowpaspalKayqome_5; }
	inline void set__rowpaspalKayqome_5(float value)
	{
		____rowpaspalKayqome_5 = value;
	}

	inline static int32_t get_offset_of__nawchowHowsallay_6() { return static_cast<int32_t>(offsetof(M_lechijorTeteahi82_t3106795064, ____nawchowHowsallay_6)); }
	inline int32_t get__nawchowHowsallay_6() const { return ____nawchowHowsallay_6; }
	inline int32_t* get_address_of__nawchowHowsallay_6() { return &____nawchowHowsallay_6; }
	inline void set__nawchowHowsallay_6(int32_t value)
	{
		____nawchowHowsallay_6 = value;
	}

	inline static int32_t get_offset_of__zemxurjem_7() { return static_cast<int32_t>(offsetof(M_lechijorTeteahi82_t3106795064, ____zemxurjem_7)); }
	inline float get__zemxurjem_7() const { return ____zemxurjem_7; }
	inline float* get_address_of__zemxurjem_7() { return &____zemxurjem_7; }
	inline void set__zemxurjem_7(float value)
	{
		____zemxurjem_7 = value;
	}

	inline static int32_t get_offset_of__stulePisroo_8() { return static_cast<int32_t>(offsetof(M_lechijorTeteahi82_t3106795064, ____stulePisroo_8)); }
	inline float get__stulePisroo_8() const { return ____stulePisroo_8; }
	inline float* get_address_of__stulePisroo_8() { return &____stulePisroo_8; }
	inline void set__stulePisroo_8(float value)
	{
		____stulePisroo_8 = value;
	}

	inline static int32_t get_offset_of__coujawwer_9() { return static_cast<int32_t>(offsetof(M_lechijorTeteahi82_t3106795064, ____coujawwer_9)); }
	inline int32_t get__coujawwer_9() const { return ____coujawwer_9; }
	inline int32_t* get_address_of__coujawwer_9() { return &____coujawwer_9; }
	inline void set__coujawwer_9(int32_t value)
	{
		____coujawwer_9 = value;
	}

	inline static int32_t get_offset_of__moodallTafearche_10() { return static_cast<int32_t>(offsetof(M_lechijorTeteahi82_t3106795064, ____moodallTafearche_10)); }
	inline bool get__moodallTafearche_10() const { return ____moodallTafearche_10; }
	inline bool* get_address_of__moodallTafearche_10() { return &____moodallTafearche_10; }
	inline void set__moodallTafearche_10(bool value)
	{
		____moodallTafearche_10 = value;
	}

	inline static int32_t get_offset_of__xouraroTrubanall_11() { return static_cast<int32_t>(offsetof(M_lechijorTeteahi82_t3106795064, ____xouraroTrubanall_11)); }
	inline uint32_t get__xouraroTrubanall_11() const { return ____xouraroTrubanall_11; }
	inline uint32_t* get_address_of__xouraroTrubanall_11() { return &____xouraroTrubanall_11; }
	inline void set__xouraroTrubanall_11(uint32_t value)
	{
		____xouraroTrubanall_11 = value;
	}

	inline static int32_t get_offset_of__jouda_12() { return static_cast<int32_t>(offsetof(M_lechijorTeteahi82_t3106795064, ____jouda_12)); }
	inline bool get__jouda_12() const { return ____jouda_12; }
	inline bool* get_address_of__jouda_12() { return &____jouda_12; }
	inline void set__jouda_12(bool value)
	{
		____jouda_12 = value;
	}

	inline static int32_t get_offset_of__hurgaFounis_13() { return static_cast<int32_t>(offsetof(M_lechijorTeteahi82_t3106795064, ____hurgaFounis_13)); }
	inline float get__hurgaFounis_13() const { return ____hurgaFounis_13; }
	inline float* get_address_of__hurgaFounis_13() { return &____hurgaFounis_13; }
	inline void set__hurgaFounis_13(float value)
	{
		____hurgaFounis_13 = value;
	}

	inline static int32_t get_offset_of__belyegem_14() { return static_cast<int32_t>(offsetof(M_lechijorTeteahi82_t3106795064, ____belyegem_14)); }
	inline String_t* get__belyegem_14() const { return ____belyegem_14; }
	inline String_t** get_address_of__belyegem_14() { return &____belyegem_14; }
	inline void set__belyegem_14(String_t* value)
	{
		____belyegem_14 = value;
		Il2CppCodeGenWriteBarrier(&____belyegem_14, value);
	}

	inline static int32_t get_offset_of__doujallfuSakalfai_15() { return static_cast<int32_t>(offsetof(M_lechijorTeteahi82_t3106795064, ____doujallfuSakalfai_15)); }
	inline bool get__doujallfuSakalfai_15() const { return ____doujallfuSakalfai_15; }
	inline bool* get_address_of__doujallfuSakalfai_15() { return &____doujallfuSakalfai_15; }
	inline void set__doujallfuSakalfai_15(bool value)
	{
		____doujallfuSakalfai_15 = value;
	}

	inline static int32_t get_offset_of__gemluralTabedem_16() { return static_cast<int32_t>(offsetof(M_lechijorTeteahi82_t3106795064, ____gemluralTabedem_16)); }
	inline bool get__gemluralTabedem_16() const { return ____gemluralTabedem_16; }
	inline bool* get_address_of__gemluralTabedem_16() { return &____gemluralTabedem_16; }
	inline void set__gemluralTabedem_16(bool value)
	{
		____gemluralTabedem_16 = value;
	}

	inline static int32_t get_offset_of__mairdral_17() { return static_cast<int32_t>(offsetof(M_lechijorTeteahi82_t3106795064, ____mairdral_17)); }
	inline float get__mairdral_17() const { return ____mairdral_17; }
	inline float* get_address_of__mairdral_17() { return &____mairdral_17; }
	inline void set__mairdral_17(float value)
	{
		____mairdral_17 = value;
	}

	inline static int32_t get_offset_of__sereto_18() { return static_cast<int32_t>(offsetof(M_lechijorTeteahi82_t3106795064, ____sereto_18)); }
	inline float get__sereto_18() const { return ____sereto_18; }
	inline float* get_address_of__sereto_18() { return &____sereto_18; }
	inline void set__sereto_18(float value)
	{
		____sereto_18 = value;
	}

	inline static int32_t get_offset_of__ronallDrelba_19() { return static_cast<int32_t>(offsetof(M_lechijorTeteahi82_t3106795064, ____ronallDrelba_19)); }
	inline int32_t get__ronallDrelba_19() const { return ____ronallDrelba_19; }
	inline int32_t* get_address_of__ronallDrelba_19() { return &____ronallDrelba_19; }
	inline void set__ronallDrelba_19(int32_t value)
	{
		____ronallDrelba_19 = value;
	}

	inline static int32_t get_offset_of__valjeebowDalmem_20() { return static_cast<int32_t>(offsetof(M_lechijorTeteahi82_t3106795064, ____valjeebowDalmem_20)); }
	inline bool get__valjeebowDalmem_20() const { return ____valjeebowDalmem_20; }
	inline bool* get_address_of__valjeebowDalmem_20() { return &____valjeebowDalmem_20; }
	inline void set__valjeebowDalmem_20(bool value)
	{
		____valjeebowDalmem_20 = value;
	}

	inline static int32_t get_offset_of__xasnawJourasgee_21() { return static_cast<int32_t>(offsetof(M_lechijorTeteahi82_t3106795064, ____xasnawJourasgee_21)); }
	inline uint32_t get__xasnawJourasgee_21() const { return ____xasnawJourasgee_21; }
	inline uint32_t* get_address_of__xasnawJourasgee_21() { return &____xasnawJourasgee_21; }
	inline void set__xasnawJourasgee_21(uint32_t value)
	{
		____xasnawJourasgee_21 = value;
	}

	inline static int32_t get_offset_of__kowfirroDootrudas_22() { return static_cast<int32_t>(offsetof(M_lechijorTeteahi82_t3106795064, ____kowfirroDootrudas_22)); }
	inline bool get__kowfirroDootrudas_22() const { return ____kowfirroDootrudas_22; }
	inline bool* get_address_of__kowfirroDootrudas_22() { return &____kowfirroDootrudas_22; }
	inline void set__kowfirroDootrudas_22(bool value)
	{
		____kowfirroDootrudas_22 = value;
	}

	inline static int32_t get_offset_of__hafoubo_23() { return static_cast<int32_t>(offsetof(M_lechijorTeteahi82_t3106795064, ____hafoubo_23)); }
	inline int32_t get__hafoubo_23() const { return ____hafoubo_23; }
	inline int32_t* get_address_of__hafoubo_23() { return &____hafoubo_23; }
	inline void set__hafoubo_23(int32_t value)
	{
		____hafoubo_23 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
