﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_zaheechem114
struct  M_zaheechem114_t3290481366  : public Il2CppObject
{
public:
	// System.UInt32 GarbageiOS.M_zaheechem114::_nurouRayleepar
	uint32_t ____nurouRayleepar_0;
	// System.Single GarbageiOS.M_zaheechem114::_fapairjeJasburwar
	float ____fapairjeJasburwar_1;
	// System.Boolean GarbageiOS.M_zaheechem114::_rarjowpiXaybili
	bool ____rarjowpiXaybili_2;
	// System.Int32 GarbageiOS.M_zaheechem114::_ceepellow
	int32_t ____ceepellow_3;
	// System.String GarbageiOS.M_zaheechem114::_gisalCejonou
	String_t* ____gisalCejonou_4;
	// System.Boolean GarbageiOS.M_zaheechem114::_karsarfor
	bool ____karsarfor_5;
	// System.Boolean GarbageiOS.M_zaheechem114::_calvati
	bool ____calvati_6;
	// System.String GarbageiOS.M_zaheechem114::_peahall
	String_t* ____peahall_7;
	// System.UInt32 GarbageiOS.M_zaheechem114::_dapeaBirdere
	uint32_t ____dapeaBirdere_8;
	// System.String GarbageiOS.M_zaheechem114::_dretow
	String_t* ____dretow_9;
	// System.UInt32 GarbageiOS.M_zaheechem114::_satrearhowJuhode
	uint32_t ____satrearhowJuhode_10;
	// System.Int32 GarbageiOS.M_zaheechem114::_dorlutraCowkall
	int32_t ____dorlutraCowkall_11;
	// System.Single GarbageiOS.M_zaheechem114::_keehi
	float ____keehi_12;
	// System.String GarbageiOS.M_zaheechem114::_gokirda
	String_t* ____gokirda_13;
	// System.Single GarbageiOS.M_zaheechem114::_howhiFajijal
	float ____howhiFajijal_14;
	// System.Boolean GarbageiOS.M_zaheechem114::_fissarmas
	bool ____fissarmas_15;
	// System.String GarbageiOS.M_zaheechem114::_herejou
	String_t* ____herejou_16;
	// System.String GarbageiOS.M_zaheechem114::_kaikaidou
	String_t* ____kaikaidou_17;
	// System.Boolean GarbageiOS.M_zaheechem114::_murrouyou
	bool ____murrouyou_18;
	// System.String GarbageiOS.M_zaheechem114::_drearleehuKejay
	String_t* ____drearleehuKejay_19;
	// System.Single GarbageiOS.M_zaheechem114::_cikairjo
	float ____cikairjo_20;
	// System.Single GarbageiOS.M_zaheechem114::_pairdertrouHogai
	float ____pairdertrouHogai_21;
	// System.Single GarbageiOS.M_zaheechem114::_reequrRalre
	float ____reequrRalre_22;
	// System.Single GarbageiOS.M_zaheechem114::_liwhaihouCusoujea
	float ____liwhaihouCusoujea_23;
	// System.Int32 GarbageiOS.M_zaheechem114::_sordrair
	int32_t ____sordrair_24;
	// System.String GarbageiOS.M_zaheechem114::_stawmurzi
	String_t* ____stawmurzi_25;
	// System.UInt32 GarbageiOS.M_zaheechem114::_laqaWusall
	uint32_t ____laqaWusall_26;
	// System.Boolean GarbageiOS.M_zaheechem114::_rersefere
	bool ____rersefere_27;
	// System.UInt32 GarbageiOS.M_zaheechem114::_jirpisjai
	uint32_t ____jirpisjai_28;
	// System.UInt32 GarbageiOS.M_zaheechem114::_dutigereBilas
	uint32_t ____dutigereBilas_29;
	// System.Int32 GarbageiOS.M_zaheechem114::_joodaysti
	int32_t ____joodaysti_30;
	// System.Boolean GarbageiOS.M_zaheechem114::_drervo
	bool ____drervo_31;
	// System.Boolean GarbageiOS.M_zaheechem114::_qirdreCerbi
	bool ____qirdreCerbi_32;
	// System.String GarbageiOS.M_zaheechem114::_jiwawmou
	String_t* ____jiwawmou_33;
	// System.UInt32 GarbageiOS.M_zaheechem114::_sesereMearjitai
	uint32_t ____sesereMearjitai_34;
	// System.UInt32 GarbageiOS.M_zaheechem114::_neyeRigar
	uint32_t ____neyeRigar_35;
	// System.Single GarbageiOS.M_zaheechem114::_cufowbairHayralaw
	float ____cufowbairHayralaw_36;
	// System.Single GarbageiOS.M_zaheechem114::_soonelKesall
	float ____soonelKesall_37;
	// System.Single GarbageiOS.M_zaheechem114::_drihaicur
	float ____drihaicur_38;
	// System.Single GarbageiOS.M_zaheechem114::_leduje
	float ____leduje_39;
	// System.Single GarbageiOS.M_zaheechem114::_moqedrirToucoufow
	float ____moqedrirToucoufow_40;
	// System.Int32 GarbageiOS.M_zaheechem114::_neweadrereMawtror
	int32_t ____neweadrereMawtror_41;
	// System.String GarbageiOS.M_zaheechem114::_cistiwhoCegemnea
	String_t* ____cistiwhoCegemnea_42;
	// System.UInt32 GarbageiOS.M_zaheechem114::_deecarisMawstejar
	uint32_t ____deecarisMawstejar_43;
	// System.Single GarbageiOS.M_zaheechem114::_zerebur
	float ____zerebur_44;
	// System.String GarbageiOS.M_zaheechem114::_geforsouSelhee
	String_t* ____geforsouSelhee_45;
	// System.Single GarbageiOS.M_zaheechem114::_qijall
	float ____qijall_46;
	// System.UInt32 GarbageiOS.M_zaheechem114::_surwunallJaigowtas
	uint32_t ____surwunallJaigowtas_47;
	// System.Single GarbageiOS.M_zaheechem114::_nowsarpel
	float ____nowsarpel_48;
	// System.Single GarbageiOS.M_zaheechem114::_celsa
	float ____celsa_49;
	// System.Single GarbageiOS.M_zaheechem114::_jijajur
	float ____jijajur_50;

public:
	inline static int32_t get_offset_of__nurouRayleepar_0() { return static_cast<int32_t>(offsetof(M_zaheechem114_t3290481366, ____nurouRayleepar_0)); }
	inline uint32_t get__nurouRayleepar_0() const { return ____nurouRayleepar_0; }
	inline uint32_t* get_address_of__nurouRayleepar_0() { return &____nurouRayleepar_0; }
	inline void set__nurouRayleepar_0(uint32_t value)
	{
		____nurouRayleepar_0 = value;
	}

	inline static int32_t get_offset_of__fapairjeJasburwar_1() { return static_cast<int32_t>(offsetof(M_zaheechem114_t3290481366, ____fapairjeJasburwar_1)); }
	inline float get__fapairjeJasburwar_1() const { return ____fapairjeJasburwar_1; }
	inline float* get_address_of__fapairjeJasburwar_1() { return &____fapairjeJasburwar_1; }
	inline void set__fapairjeJasburwar_1(float value)
	{
		____fapairjeJasburwar_1 = value;
	}

	inline static int32_t get_offset_of__rarjowpiXaybili_2() { return static_cast<int32_t>(offsetof(M_zaheechem114_t3290481366, ____rarjowpiXaybili_2)); }
	inline bool get__rarjowpiXaybili_2() const { return ____rarjowpiXaybili_2; }
	inline bool* get_address_of__rarjowpiXaybili_2() { return &____rarjowpiXaybili_2; }
	inline void set__rarjowpiXaybili_2(bool value)
	{
		____rarjowpiXaybili_2 = value;
	}

	inline static int32_t get_offset_of__ceepellow_3() { return static_cast<int32_t>(offsetof(M_zaheechem114_t3290481366, ____ceepellow_3)); }
	inline int32_t get__ceepellow_3() const { return ____ceepellow_3; }
	inline int32_t* get_address_of__ceepellow_3() { return &____ceepellow_3; }
	inline void set__ceepellow_3(int32_t value)
	{
		____ceepellow_3 = value;
	}

	inline static int32_t get_offset_of__gisalCejonou_4() { return static_cast<int32_t>(offsetof(M_zaheechem114_t3290481366, ____gisalCejonou_4)); }
	inline String_t* get__gisalCejonou_4() const { return ____gisalCejonou_4; }
	inline String_t** get_address_of__gisalCejonou_4() { return &____gisalCejonou_4; }
	inline void set__gisalCejonou_4(String_t* value)
	{
		____gisalCejonou_4 = value;
		Il2CppCodeGenWriteBarrier(&____gisalCejonou_4, value);
	}

	inline static int32_t get_offset_of__karsarfor_5() { return static_cast<int32_t>(offsetof(M_zaheechem114_t3290481366, ____karsarfor_5)); }
	inline bool get__karsarfor_5() const { return ____karsarfor_5; }
	inline bool* get_address_of__karsarfor_5() { return &____karsarfor_5; }
	inline void set__karsarfor_5(bool value)
	{
		____karsarfor_5 = value;
	}

	inline static int32_t get_offset_of__calvati_6() { return static_cast<int32_t>(offsetof(M_zaheechem114_t3290481366, ____calvati_6)); }
	inline bool get__calvati_6() const { return ____calvati_6; }
	inline bool* get_address_of__calvati_6() { return &____calvati_6; }
	inline void set__calvati_6(bool value)
	{
		____calvati_6 = value;
	}

	inline static int32_t get_offset_of__peahall_7() { return static_cast<int32_t>(offsetof(M_zaheechem114_t3290481366, ____peahall_7)); }
	inline String_t* get__peahall_7() const { return ____peahall_7; }
	inline String_t** get_address_of__peahall_7() { return &____peahall_7; }
	inline void set__peahall_7(String_t* value)
	{
		____peahall_7 = value;
		Il2CppCodeGenWriteBarrier(&____peahall_7, value);
	}

	inline static int32_t get_offset_of__dapeaBirdere_8() { return static_cast<int32_t>(offsetof(M_zaheechem114_t3290481366, ____dapeaBirdere_8)); }
	inline uint32_t get__dapeaBirdere_8() const { return ____dapeaBirdere_8; }
	inline uint32_t* get_address_of__dapeaBirdere_8() { return &____dapeaBirdere_8; }
	inline void set__dapeaBirdere_8(uint32_t value)
	{
		____dapeaBirdere_8 = value;
	}

	inline static int32_t get_offset_of__dretow_9() { return static_cast<int32_t>(offsetof(M_zaheechem114_t3290481366, ____dretow_9)); }
	inline String_t* get__dretow_9() const { return ____dretow_9; }
	inline String_t** get_address_of__dretow_9() { return &____dretow_9; }
	inline void set__dretow_9(String_t* value)
	{
		____dretow_9 = value;
		Il2CppCodeGenWriteBarrier(&____dretow_9, value);
	}

	inline static int32_t get_offset_of__satrearhowJuhode_10() { return static_cast<int32_t>(offsetof(M_zaheechem114_t3290481366, ____satrearhowJuhode_10)); }
	inline uint32_t get__satrearhowJuhode_10() const { return ____satrearhowJuhode_10; }
	inline uint32_t* get_address_of__satrearhowJuhode_10() { return &____satrearhowJuhode_10; }
	inline void set__satrearhowJuhode_10(uint32_t value)
	{
		____satrearhowJuhode_10 = value;
	}

	inline static int32_t get_offset_of__dorlutraCowkall_11() { return static_cast<int32_t>(offsetof(M_zaheechem114_t3290481366, ____dorlutraCowkall_11)); }
	inline int32_t get__dorlutraCowkall_11() const { return ____dorlutraCowkall_11; }
	inline int32_t* get_address_of__dorlutraCowkall_11() { return &____dorlutraCowkall_11; }
	inline void set__dorlutraCowkall_11(int32_t value)
	{
		____dorlutraCowkall_11 = value;
	}

	inline static int32_t get_offset_of__keehi_12() { return static_cast<int32_t>(offsetof(M_zaheechem114_t3290481366, ____keehi_12)); }
	inline float get__keehi_12() const { return ____keehi_12; }
	inline float* get_address_of__keehi_12() { return &____keehi_12; }
	inline void set__keehi_12(float value)
	{
		____keehi_12 = value;
	}

	inline static int32_t get_offset_of__gokirda_13() { return static_cast<int32_t>(offsetof(M_zaheechem114_t3290481366, ____gokirda_13)); }
	inline String_t* get__gokirda_13() const { return ____gokirda_13; }
	inline String_t** get_address_of__gokirda_13() { return &____gokirda_13; }
	inline void set__gokirda_13(String_t* value)
	{
		____gokirda_13 = value;
		Il2CppCodeGenWriteBarrier(&____gokirda_13, value);
	}

	inline static int32_t get_offset_of__howhiFajijal_14() { return static_cast<int32_t>(offsetof(M_zaheechem114_t3290481366, ____howhiFajijal_14)); }
	inline float get__howhiFajijal_14() const { return ____howhiFajijal_14; }
	inline float* get_address_of__howhiFajijal_14() { return &____howhiFajijal_14; }
	inline void set__howhiFajijal_14(float value)
	{
		____howhiFajijal_14 = value;
	}

	inline static int32_t get_offset_of__fissarmas_15() { return static_cast<int32_t>(offsetof(M_zaheechem114_t3290481366, ____fissarmas_15)); }
	inline bool get__fissarmas_15() const { return ____fissarmas_15; }
	inline bool* get_address_of__fissarmas_15() { return &____fissarmas_15; }
	inline void set__fissarmas_15(bool value)
	{
		____fissarmas_15 = value;
	}

	inline static int32_t get_offset_of__herejou_16() { return static_cast<int32_t>(offsetof(M_zaheechem114_t3290481366, ____herejou_16)); }
	inline String_t* get__herejou_16() const { return ____herejou_16; }
	inline String_t** get_address_of__herejou_16() { return &____herejou_16; }
	inline void set__herejou_16(String_t* value)
	{
		____herejou_16 = value;
		Il2CppCodeGenWriteBarrier(&____herejou_16, value);
	}

	inline static int32_t get_offset_of__kaikaidou_17() { return static_cast<int32_t>(offsetof(M_zaheechem114_t3290481366, ____kaikaidou_17)); }
	inline String_t* get__kaikaidou_17() const { return ____kaikaidou_17; }
	inline String_t** get_address_of__kaikaidou_17() { return &____kaikaidou_17; }
	inline void set__kaikaidou_17(String_t* value)
	{
		____kaikaidou_17 = value;
		Il2CppCodeGenWriteBarrier(&____kaikaidou_17, value);
	}

	inline static int32_t get_offset_of__murrouyou_18() { return static_cast<int32_t>(offsetof(M_zaheechem114_t3290481366, ____murrouyou_18)); }
	inline bool get__murrouyou_18() const { return ____murrouyou_18; }
	inline bool* get_address_of__murrouyou_18() { return &____murrouyou_18; }
	inline void set__murrouyou_18(bool value)
	{
		____murrouyou_18 = value;
	}

	inline static int32_t get_offset_of__drearleehuKejay_19() { return static_cast<int32_t>(offsetof(M_zaheechem114_t3290481366, ____drearleehuKejay_19)); }
	inline String_t* get__drearleehuKejay_19() const { return ____drearleehuKejay_19; }
	inline String_t** get_address_of__drearleehuKejay_19() { return &____drearleehuKejay_19; }
	inline void set__drearleehuKejay_19(String_t* value)
	{
		____drearleehuKejay_19 = value;
		Il2CppCodeGenWriteBarrier(&____drearleehuKejay_19, value);
	}

	inline static int32_t get_offset_of__cikairjo_20() { return static_cast<int32_t>(offsetof(M_zaheechem114_t3290481366, ____cikairjo_20)); }
	inline float get__cikairjo_20() const { return ____cikairjo_20; }
	inline float* get_address_of__cikairjo_20() { return &____cikairjo_20; }
	inline void set__cikairjo_20(float value)
	{
		____cikairjo_20 = value;
	}

	inline static int32_t get_offset_of__pairdertrouHogai_21() { return static_cast<int32_t>(offsetof(M_zaheechem114_t3290481366, ____pairdertrouHogai_21)); }
	inline float get__pairdertrouHogai_21() const { return ____pairdertrouHogai_21; }
	inline float* get_address_of__pairdertrouHogai_21() { return &____pairdertrouHogai_21; }
	inline void set__pairdertrouHogai_21(float value)
	{
		____pairdertrouHogai_21 = value;
	}

	inline static int32_t get_offset_of__reequrRalre_22() { return static_cast<int32_t>(offsetof(M_zaheechem114_t3290481366, ____reequrRalre_22)); }
	inline float get__reequrRalre_22() const { return ____reequrRalre_22; }
	inline float* get_address_of__reequrRalre_22() { return &____reequrRalre_22; }
	inline void set__reequrRalre_22(float value)
	{
		____reequrRalre_22 = value;
	}

	inline static int32_t get_offset_of__liwhaihouCusoujea_23() { return static_cast<int32_t>(offsetof(M_zaheechem114_t3290481366, ____liwhaihouCusoujea_23)); }
	inline float get__liwhaihouCusoujea_23() const { return ____liwhaihouCusoujea_23; }
	inline float* get_address_of__liwhaihouCusoujea_23() { return &____liwhaihouCusoujea_23; }
	inline void set__liwhaihouCusoujea_23(float value)
	{
		____liwhaihouCusoujea_23 = value;
	}

	inline static int32_t get_offset_of__sordrair_24() { return static_cast<int32_t>(offsetof(M_zaheechem114_t3290481366, ____sordrair_24)); }
	inline int32_t get__sordrair_24() const { return ____sordrair_24; }
	inline int32_t* get_address_of__sordrair_24() { return &____sordrair_24; }
	inline void set__sordrair_24(int32_t value)
	{
		____sordrair_24 = value;
	}

	inline static int32_t get_offset_of__stawmurzi_25() { return static_cast<int32_t>(offsetof(M_zaheechem114_t3290481366, ____stawmurzi_25)); }
	inline String_t* get__stawmurzi_25() const { return ____stawmurzi_25; }
	inline String_t** get_address_of__stawmurzi_25() { return &____stawmurzi_25; }
	inline void set__stawmurzi_25(String_t* value)
	{
		____stawmurzi_25 = value;
		Il2CppCodeGenWriteBarrier(&____stawmurzi_25, value);
	}

	inline static int32_t get_offset_of__laqaWusall_26() { return static_cast<int32_t>(offsetof(M_zaheechem114_t3290481366, ____laqaWusall_26)); }
	inline uint32_t get__laqaWusall_26() const { return ____laqaWusall_26; }
	inline uint32_t* get_address_of__laqaWusall_26() { return &____laqaWusall_26; }
	inline void set__laqaWusall_26(uint32_t value)
	{
		____laqaWusall_26 = value;
	}

	inline static int32_t get_offset_of__rersefere_27() { return static_cast<int32_t>(offsetof(M_zaheechem114_t3290481366, ____rersefere_27)); }
	inline bool get__rersefere_27() const { return ____rersefere_27; }
	inline bool* get_address_of__rersefere_27() { return &____rersefere_27; }
	inline void set__rersefere_27(bool value)
	{
		____rersefere_27 = value;
	}

	inline static int32_t get_offset_of__jirpisjai_28() { return static_cast<int32_t>(offsetof(M_zaheechem114_t3290481366, ____jirpisjai_28)); }
	inline uint32_t get__jirpisjai_28() const { return ____jirpisjai_28; }
	inline uint32_t* get_address_of__jirpisjai_28() { return &____jirpisjai_28; }
	inline void set__jirpisjai_28(uint32_t value)
	{
		____jirpisjai_28 = value;
	}

	inline static int32_t get_offset_of__dutigereBilas_29() { return static_cast<int32_t>(offsetof(M_zaheechem114_t3290481366, ____dutigereBilas_29)); }
	inline uint32_t get__dutigereBilas_29() const { return ____dutigereBilas_29; }
	inline uint32_t* get_address_of__dutigereBilas_29() { return &____dutigereBilas_29; }
	inline void set__dutigereBilas_29(uint32_t value)
	{
		____dutigereBilas_29 = value;
	}

	inline static int32_t get_offset_of__joodaysti_30() { return static_cast<int32_t>(offsetof(M_zaheechem114_t3290481366, ____joodaysti_30)); }
	inline int32_t get__joodaysti_30() const { return ____joodaysti_30; }
	inline int32_t* get_address_of__joodaysti_30() { return &____joodaysti_30; }
	inline void set__joodaysti_30(int32_t value)
	{
		____joodaysti_30 = value;
	}

	inline static int32_t get_offset_of__drervo_31() { return static_cast<int32_t>(offsetof(M_zaheechem114_t3290481366, ____drervo_31)); }
	inline bool get__drervo_31() const { return ____drervo_31; }
	inline bool* get_address_of__drervo_31() { return &____drervo_31; }
	inline void set__drervo_31(bool value)
	{
		____drervo_31 = value;
	}

	inline static int32_t get_offset_of__qirdreCerbi_32() { return static_cast<int32_t>(offsetof(M_zaheechem114_t3290481366, ____qirdreCerbi_32)); }
	inline bool get__qirdreCerbi_32() const { return ____qirdreCerbi_32; }
	inline bool* get_address_of__qirdreCerbi_32() { return &____qirdreCerbi_32; }
	inline void set__qirdreCerbi_32(bool value)
	{
		____qirdreCerbi_32 = value;
	}

	inline static int32_t get_offset_of__jiwawmou_33() { return static_cast<int32_t>(offsetof(M_zaheechem114_t3290481366, ____jiwawmou_33)); }
	inline String_t* get__jiwawmou_33() const { return ____jiwawmou_33; }
	inline String_t** get_address_of__jiwawmou_33() { return &____jiwawmou_33; }
	inline void set__jiwawmou_33(String_t* value)
	{
		____jiwawmou_33 = value;
		Il2CppCodeGenWriteBarrier(&____jiwawmou_33, value);
	}

	inline static int32_t get_offset_of__sesereMearjitai_34() { return static_cast<int32_t>(offsetof(M_zaheechem114_t3290481366, ____sesereMearjitai_34)); }
	inline uint32_t get__sesereMearjitai_34() const { return ____sesereMearjitai_34; }
	inline uint32_t* get_address_of__sesereMearjitai_34() { return &____sesereMearjitai_34; }
	inline void set__sesereMearjitai_34(uint32_t value)
	{
		____sesereMearjitai_34 = value;
	}

	inline static int32_t get_offset_of__neyeRigar_35() { return static_cast<int32_t>(offsetof(M_zaheechem114_t3290481366, ____neyeRigar_35)); }
	inline uint32_t get__neyeRigar_35() const { return ____neyeRigar_35; }
	inline uint32_t* get_address_of__neyeRigar_35() { return &____neyeRigar_35; }
	inline void set__neyeRigar_35(uint32_t value)
	{
		____neyeRigar_35 = value;
	}

	inline static int32_t get_offset_of__cufowbairHayralaw_36() { return static_cast<int32_t>(offsetof(M_zaheechem114_t3290481366, ____cufowbairHayralaw_36)); }
	inline float get__cufowbairHayralaw_36() const { return ____cufowbairHayralaw_36; }
	inline float* get_address_of__cufowbairHayralaw_36() { return &____cufowbairHayralaw_36; }
	inline void set__cufowbairHayralaw_36(float value)
	{
		____cufowbairHayralaw_36 = value;
	}

	inline static int32_t get_offset_of__soonelKesall_37() { return static_cast<int32_t>(offsetof(M_zaheechem114_t3290481366, ____soonelKesall_37)); }
	inline float get__soonelKesall_37() const { return ____soonelKesall_37; }
	inline float* get_address_of__soonelKesall_37() { return &____soonelKesall_37; }
	inline void set__soonelKesall_37(float value)
	{
		____soonelKesall_37 = value;
	}

	inline static int32_t get_offset_of__drihaicur_38() { return static_cast<int32_t>(offsetof(M_zaheechem114_t3290481366, ____drihaicur_38)); }
	inline float get__drihaicur_38() const { return ____drihaicur_38; }
	inline float* get_address_of__drihaicur_38() { return &____drihaicur_38; }
	inline void set__drihaicur_38(float value)
	{
		____drihaicur_38 = value;
	}

	inline static int32_t get_offset_of__leduje_39() { return static_cast<int32_t>(offsetof(M_zaheechem114_t3290481366, ____leduje_39)); }
	inline float get__leduje_39() const { return ____leduje_39; }
	inline float* get_address_of__leduje_39() { return &____leduje_39; }
	inline void set__leduje_39(float value)
	{
		____leduje_39 = value;
	}

	inline static int32_t get_offset_of__moqedrirToucoufow_40() { return static_cast<int32_t>(offsetof(M_zaheechem114_t3290481366, ____moqedrirToucoufow_40)); }
	inline float get__moqedrirToucoufow_40() const { return ____moqedrirToucoufow_40; }
	inline float* get_address_of__moqedrirToucoufow_40() { return &____moqedrirToucoufow_40; }
	inline void set__moqedrirToucoufow_40(float value)
	{
		____moqedrirToucoufow_40 = value;
	}

	inline static int32_t get_offset_of__neweadrereMawtror_41() { return static_cast<int32_t>(offsetof(M_zaheechem114_t3290481366, ____neweadrereMawtror_41)); }
	inline int32_t get__neweadrereMawtror_41() const { return ____neweadrereMawtror_41; }
	inline int32_t* get_address_of__neweadrereMawtror_41() { return &____neweadrereMawtror_41; }
	inline void set__neweadrereMawtror_41(int32_t value)
	{
		____neweadrereMawtror_41 = value;
	}

	inline static int32_t get_offset_of__cistiwhoCegemnea_42() { return static_cast<int32_t>(offsetof(M_zaheechem114_t3290481366, ____cistiwhoCegemnea_42)); }
	inline String_t* get__cistiwhoCegemnea_42() const { return ____cistiwhoCegemnea_42; }
	inline String_t** get_address_of__cistiwhoCegemnea_42() { return &____cistiwhoCegemnea_42; }
	inline void set__cistiwhoCegemnea_42(String_t* value)
	{
		____cistiwhoCegemnea_42 = value;
		Il2CppCodeGenWriteBarrier(&____cistiwhoCegemnea_42, value);
	}

	inline static int32_t get_offset_of__deecarisMawstejar_43() { return static_cast<int32_t>(offsetof(M_zaheechem114_t3290481366, ____deecarisMawstejar_43)); }
	inline uint32_t get__deecarisMawstejar_43() const { return ____deecarisMawstejar_43; }
	inline uint32_t* get_address_of__deecarisMawstejar_43() { return &____deecarisMawstejar_43; }
	inline void set__deecarisMawstejar_43(uint32_t value)
	{
		____deecarisMawstejar_43 = value;
	}

	inline static int32_t get_offset_of__zerebur_44() { return static_cast<int32_t>(offsetof(M_zaheechem114_t3290481366, ____zerebur_44)); }
	inline float get__zerebur_44() const { return ____zerebur_44; }
	inline float* get_address_of__zerebur_44() { return &____zerebur_44; }
	inline void set__zerebur_44(float value)
	{
		____zerebur_44 = value;
	}

	inline static int32_t get_offset_of__geforsouSelhee_45() { return static_cast<int32_t>(offsetof(M_zaheechem114_t3290481366, ____geforsouSelhee_45)); }
	inline String_t* get__geforsouSelhee_45() const { return ____geforsouSelhee_45; }
	inline String_t** get_address_of__geforsouSelhee_45() { return &____geforsouSelhee_45; }
	inline void set__geforsouSelhee_45(String_t* value)
	{
		____geforsouSelhee_45 = value;
		Il2CppCodeGenWriteBarrier(&____geforsouSelhee_45, value);
	}

	inline static int32_t get_offset_of__qijall_46() { return static_cast<int32_t>(offsetof(M_zaheechem114_t3290481366, ____qijall_46)); }
	inline float get__qijall_46() const { return ____qijall_46; }
	inline float* get_address_of__qijall_46() { return &____qijall_46; }
	inline void set__qijall_46(float value)
	{
		____qijall_46 = value;
	}

	inline static int32_t get_offset_of__surwunallJaigowtas_47() { return static_cast<int32_t>(offsetof(M_zaheechem114_t3290481366, ____surwunallJaigowtas_47)); }
	inline uint32_t get__surwunallJaigowtas_47() const { return ____surwunallJaigowtas_47; }
	inline uint32_t* get_address_of__surwunallJaigowtas_47() { return &____surwunallJaigowtas_47; }
	inline void set__surwunallJaigowtas_47(uint32_t value)
	{
		____surwunallJaigowtas_47 = value;
	}

	inline static int32_t get_offset_of__nowsarpel_48() { return static_cast<int32_t>(offsetof(M_zaheechem114_t3290481366, ____nowsarpel_48)); }
	inline float get__nowsarpel_48() const { return ____nowsarpel_48; }
	inline float* get_address_of__nowsarpel_48() { return &____nowsarpel_48; }
	inline void set__nowsarpel_48(float value)
	{
		____nowsarpel_48 = value;
	}

	inline static int32_t get_offset_of__celsa_49() { return static_cast<int32_t>(offsetof(M_zaheechem114_t3290481366, ____celsa_49)); }
	inline float get__celsa_49() const { return ____celsa_49; }
	inline float* get_address_of__celsa_49() { return &____celsa_49; }
	inline void set__celsa_49(float value)
	{
		____celsa_49 = value;
	}

	inline static int32_t get_offset_of__jijajur_50() { return static_cast<int32_t>(offsetof(M_zaheechem114_t3290481366, ____jijajur_50)); }
	inline float get__jijajur_50() const { return ____jijajur_50; }
	inline float* get_address_of__jijajur_50() { return &____jijajur_50; }
	inline void set__jijajur_50(float value)
	{
		____jijajur_50 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
