﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "AssemblyU2DCSharp_CsCfgBase69924517.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChannelConfigCfg
struct  ChannelConfigCfg_t4272979999  : public CsCfgBase_t69924517
{
public:
	// System.Int32 ChannelConfigCfg::id
	int32_t ___id_0;
	// System.String ChannelConfigCfg::key
	String_t* ___key_1;
	// System.String ChannelConfigCfg::value
	String_t* ___value_2;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(ChannelConfigCfg_t4272979999, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_key_1() { return static_cast<int32_t>(offsetof(ChannelConfigCfg_t4272979999, ___key_1)); }
	inline String_t* get_key_1() const { return ___key_1; }
	inline String_t** get_address_of_key_1() { return &___key_1; }
	inline void set_key_1(String_t* value)
	{
		___key_1 = value;
		Il2CppCodeGenWriteBarrier(&___key_1, value);
	}

	inline static int32_t get_offset_of_value_2() { return static_cast<int32_t>(offsetof(ChannelConfigCfg_t4272979999, ___value_2)); }
	inline String_t* get_value_2() const { return ___value_2; }
	inline String_t** get_address_of_value_2() { return &___value_2; }
	inline void set_value_2(String_t* value)
	{
		___value_2 = value;
		Il2CppCodeGenWriteBarrier(&___value_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
