﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<Pathfinding.Int3>
struct List_1_t3342231146;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3361903916.h"
#include "AssemblyU2DCSharp_Pathfinding_Int31974045594.h"

// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.Int3>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m2015394401_gshared (Enumerator_t3361903916 * __this, List_1_t3342231146 * ___l0, const MethodInfo* method);
#define Enumerator__ctor_m2015394401(__this, ___l0, method) ((  void (*) (Enumerator_t3361903916 *, List_1_t3342231146 *, const MethodInfo*))Enumerator__ctor_m2015394401_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.Int3>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3335643473_gshared (Enumerator_t3361903916 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m3335643473(__this, method) ((  void (*) (Enumerator_t3361903916 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3335643473_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Pathfinding.Int3>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2033249159_gshared (Enumerator_t3361903916 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m2033249159(__this, method) ((  Il2CppObject * (*) (Enumerator_t3361903916 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2033249159_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.Int3>::Dispose()
extern "C"  void Enumerator_Dispose_m1126494342_gshared (Enumerator_t3361903916 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1126494342(__this, method) ((  void (*) (Enumerator_t3361903916 *, const MethodInfo*))Enumerator_Dispose_m1126494342_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.Int3>::VerifyState()
extern "C"  void Enumerator_VerifyState_m3082878143_gshared (Enumerator_t3361903916 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m3082878143(__this, method) ((  void (*) (Enumerator_t3361903916 *, const MethodInfo*))Enumerator_VerifyState_m3082878143_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Pathfinding.Int3>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m4038733441_gshared (Enumerator_t3361903916 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m4038733441(__this, method) ((  bool (*) (Enumerator_t3361903916 *, const MethodInfo*))Enumerator_MoveNext_m4038733441_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Pathfinding.Int3>::get_Current()
extern "C"  Int3_t1974045594  Enumerator_get_Current_m385527896_gshared (Enumerator_t3361903916 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m385527896(__this, method) ((  Int3_t1974045594  (*) (Enumerator_t3361903916 *, const MethodInfo*))Enumerator_get_Current_m385527896_gshared)(__this, method)
