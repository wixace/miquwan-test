﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Serialization.JsonSerializerProxy
struct JsonSerializerProxy_t3893567258;
// Newtonsoft.Json.Serialization.JsonSerializerInternalReader
struct JsonSerializerInternalReader_t3659144454;
// Newtonsoft.Json.Serialization.JsonSerializerInternalWriter
struct JsonSerializerInternalWriter_t3814549686;
// System.EventHandler`1<Newtonsoft.Json.Serialization.ErrorEventArgs>
struct EventHandler_1_t937589677;
// Newtonsoft.Json.Serialization.IReferenceResolver
struct IReferenceResolver_t425424564;
// Newtonsoft.Json.JsonConverterCollection
struct JsonConverterCollection_t530165700;
// Newtonsoft.Json.Serialization.IContractResolver
struct IContractResolver_t507353639;
// System.Runtime.Serialization.SerializationBinder
struct SerializationBinder_t2137423328;
// Newtonsoft.Json.Serialization.JsonSerializerInternalBase
struct JsonSerializerInternalBase_t2068678036;
// System.Object
struct Il2CppObject;
// Newtonsoft.Json.JsonReader
struct JsonReader_t816925123;
// System.Type
struct Type_t;
// Newtonsoft.Json.JsonWriter
struct JsonWriter_t972330355;
// Newtonsoft.Json.JsonSerializer
struct JsonSerializer_t251850770;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_Js3659144454.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_Js3814549686.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_DefaultValueHand1569448045.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_MissingMemberHan2077487315.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_NullValueHandlin2754652381.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_ObjectCreationHand56081595.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_ReferenceLoopHan2761661122.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_PreserveReferenc4230591217.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_TypeNameHandling2359325474.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_F3005881063.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_ConstructorHandl2475221485.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2137423328.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon2761351129.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonReader816925123.h"
#include "mscorlib_System_Type2863145774.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonWriter972330355.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonSerializer251850770.h"

// System.Void Newtonsoft.Json.Serialization.JsonSerializerProxy::.ctor(Newtonsoft.Json.Serialization.JsonSerializerInternalReader)
extern "C"  void JsonSerializerProxy__ctor_m3169172285 (JsonSerializerProxy_t3893567258 * __this, JsonSerializerInternalReader_t3659144454 * ___serializerReader0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerProxy::.ctor(Newtonsoft.Json.Serialization.JsonSerializerInternalWriter)
extern "C"  void JsonSerializerProxy__ctor_m3691767181 (JsonSerializerProxy_t3893567258 * __this, JsonSerializerInternalWriter_t3814549686 * ___serializerWriter0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerProxy::add_Error(System.EventHandler`1<Newtonsoft.Json.Serialization.ErrorEventArgs>)
extern "C"  void JsonSerializerProxy_add_Error_m2367046112 (JsonSerializerProxy_t3893567258 * __this, EventHandler_1_t937589677 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerProxy::remove_Error(System.EventHandler`1<Newtonsoft.Json.Serialization.ErrorEventArgs>)
extern "C"  void JsonSerializerProxy_remove_Error_m2336192779 (JsonSerializerProxy_t3893567258 * __this, EventHandler_1_t937589677 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Serialization.IReferenceResolver Newtonsoft.Json.Serialization.JsonSerializerProxy::get_ReferenceResolver()
extern "C"  Il2CppObject * JsonSerializerProxy_get_ReferenceResolver_m1919141042 (JsonSerializerProxy_t3893567258 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerProxy::set_ReferenceResolver(Newtonsoft.Json.Serialization.IReferenceResolver)
extern "C"  void JsonSerializerProxy_set_ReferenceResolver_m568119425 (JsonSerializerProxy_t3893567258 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.JsonConverterCollection Newtonsoft.Json.Serialization.JsonSerializerProxy::get_Converters()
extern "C"  JsonConverterCollection_t530165700 * JsonSerializerProxy_get_Converters_m1538445604 (JsonSerializerProxy_t3893567258 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.DefaultValueHandling Newtonsoft.Json.Serialization.JsonSerializerProxy::get_DefaultValueHandling()
extern "C"  int32_t JsonSerializerProxy_get_DefaultValueHandling_m698533253 (JsonSerializerProxy_t3893567258 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerProxy::set_DefaultValueHandling(Newtonsoft.Json.DefaultValueHandling)
extern "C"  void JsonSerializerProxy_set_DefaultValueHandling_m1050488002 (JsonSerializerProxy_t3893567258 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Serialization.IContractResolver Newtonsoft.Json.Serialization.JsonSerializerProxy::get_ContractResolver()
extern "C"  Il2CppObject * JsonSerializerProxy_get_ContractResolver_m2285065430 (JsonSerializerProxy_t3893567258 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerProxy::set_ContractResolver(Newtonsoft.Json.Serialization.IContractResolver)
extern "C"  void JsonSerializerProxy_set_ContractResolver_m2896214741 (JsonSerializerProxy_t3893567258 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.MissingMemberHandling Newtonsoft.Json.Serialization.JsonSerializerProxy::get_MissingMemberHandling()
extern "C"  int32_t JsonSerializerProxy_get_MissingMemberHandling_m2039952935 (JsonSerializerProxy_t3893567258 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerProxy::set_MissingMemberHandling(Newtonsoft.Json.MissingMemberHandling)
extern "C"  void JsonSerializerProxy_set_MissingMemberHandling_m736504754 (JsonSerializerProxy_t3893567258 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.NullValueHandling Newtonsoft.Json.Serialization.JsonSerializerProxy::get_NullValueHandling()
extern "C"  int32_t JsonSerializerProxy_get_NullValueHandling_m2454761959 (JsonSerializerProxy_t3893567258 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerProxy::set_NullValueHandling(Newtonsoft.Json.NullValueHandling)
extern "C"  void JsonSerializerProxy_set_NullValueHandling_m4222680478 (JsonSerializerProxy_t3893567258 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.ObjectCreationHandling Newtonsoft.Json.Serialization.JsonSerializerProxy::get_ObjectCreationHandling()
extern "C"  int32_t JsonSerializerProxy_get_ObjectCreationHandling_m953222497 (JsonSerializerProxy_t3893567258 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerProxy::set_ObjectCreationHandling(Newtonsoft.Json.ObjectCreationHandling)
extern "C"  void JsonSerializerProxy_set_ObjectCreationHandling_m2050598786 (JsonSerializerProxy_t3893567258 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.ReferenceLoopHandling Newtonsoft.Json.Serialization.JsonSerializerProxy::get_ReferenceLoopHandling()
extern "C"  int32_t JsonSerializerProxy_get_ReferenceLoopHandling_m2079421063 (JsonSerializerProxy_t3893567258 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerProxy::set_ReferenceLoopHandling(Newtonsoft.Json.ReferenceLoopHandling)
extern "C"  void JsonSerializerProxy_set_ReferenceLoopHandling_m1248057044 (JsonSerializerProxy_t3893567258 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.PreserveReferencesHandling Newtonsoft.Json.Serialization.JsonSerializerProxy::get_PreserveReferencesHandling()
extern "C"  int32_t JsonSerializerProxy_get_PreserveReferencesHandling_m716172237 (JsonSerializerProxy_t3893567258 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerProxy::set_PreserveReferencesHandling(Newtonsoft.Json.PreserveReferencesHandling)
extern "C"  void JsonSerializerProxy_set_PreserveReferencesHandling_m3206769730 (JsonSerializerProxy_t3893567258 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.TypeNameHandling Newtonsoft.Json.Serialization.JsonSerializerProxy::get_TypeNameHandling()
extern "C"  int32_t JsonSerializerProxy_get_TypeNameHandling_m404192751 (JsonSerializerProxy_t3893567258 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerProxy::set_TypeNameHandling(Newtonsoft.Json.TypeNameHandling)
extern "C"  void JsonSerializerProxy_set_TypeNameHandling_m1770915874 (JsonSerializerProxy_t3893567258 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Serialization.Formatters.FormatterAssemblyStyle Newtonsoft.Json.Serialization.JsonSerializerProxy::get_TypeNameAssemblyFormat()
extern "C"  int32_t JsonSerializerProxy_get_TypeNameAssemblyFormat_m1754984955 (JsonSerializerProxy_t3893567258 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerProxy::set_TypeNameAssemblyFormat(System.Runtime.Serialization.Formatters.FormatterAssemblyStyle)
extern "C"  void JsonSerializerProxy_set_TypeNameAssemblyFormat_m1345238918 (JsonSerializerProxy_t3893567258 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.ConstructorHandling Newtonsoft.Json.Serialization.JsonSerializerProxy::get_ConstructorHandling()
extern "C"  int32_t JsonSerializerProxy_get_ConstructorHandling_m1957515815 (JsonSerializerProxy_t3893567258 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerProxy::set_ConstructorHandling(Newtonsoft.Json.ConstructorHandling)
extern "C"  void JsonSerializerProxy_set_ConstructorHandling_m1248140158 (JsonSerializerProxy_t3893567258 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Serialization.SerializationBinder Newtonsoft.Json.Serialization.JsonSerializerProxy::get_Binder()
extern "C"  SerializationBinder_t2137423328 * JsonSerializerProxy_get_Binder_m3810755095 (JsonSerializerProxy_t3893567258 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerProxy::set_Binder(System.Runtime.Serialization.SerializationBinder)
extern "C"  void JsonSerializerProxy_set_Binder_m2865085690 (JsonSerializerProxy_t3893567258 * __this, SerializationBinder_t2137423328 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Serialization.StreamingContext Newtonsoft.Json.Serialization.JsonSerializerProxy::get_Context()
extern "C"  StreamingContext_t2761351129  JsonSerializerProxy_get_Context_m677295297 (JsonSerializerProxy_t3893567258 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerProxy::set_Context(System.Runtime.Serialization.StreamingContext)
extern "C"  void JsonSerializerProxy_set_Context_m1577261112 (JsonSerializerProxy_t3893567258 * __this, StreamingContext_t2761351129  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Serialization.JsonSerializerInternalBase Newtonsoft.Json.Serialization.JsonSerializerProxy::GetInternalSerializer()
extern "C"  JsonSerializerInternalBase_t2068678036 * JsonSerializerProxy_GetInternalSerializer_m1725235183 (JsonSerializerProxy_t3893567258 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Serialization.JsonSerializerProxy::DeserializeInternal(Newtonsoft.Json.JsonReader,System.Type)
extern "C"  Il2CppObject * JsonSerializerProxy_DeserializeInternal_m2481065516 (JsonSerializerProxy_t3893567258 * __this, JsonReader_t816925123 * ___reader0, Type_t * ___objectType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerProxy::PopulateInternal(Newtonsoft.Json.JsonReader,System.Object)
extern "C"  void JsonSerializerProxy_PopulateInternal_m1987257295 (JsonSerializerProxy_t3893567258 * __this, JsonReader_t816925123 * ___reader0, Il2CppObject * ___target1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerProxy::SerializeInternal(Newtonsoft.Json.JsonWriter,System.Object)
extern "C"  void JsonSerializerProxy_SerializeInternal_m1033600515 (JsonSerializerProxy_t3893567258 * __this, JsonWriter_t972330355 * ___jsonWriter0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerProxy::ilo_add_Error1(Newtonsoft.Json.JsonSerializer,System.EventHandler`1<Newtonsoft.Json.Serialization.ErrorEventArgs>)
extern "C"  void JsonSerializerProxy_ilo_add_Error1_m3005859537 (Il2CppObject * __this /* static, unused */, JsonSerializer_t251850770 * ____this0, EventHandler_1_t937589677 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerProxy::ilo_remove_Error2(Newtonsoft.Json.JsonSerializer,System.EventHandler`1<Newtonsoft.Json.Serialization.ErrorEventArgs>)
extern "C"  void JsonSerializerProxy_ilo_remove_Error2_m341369783 (Il2CppObject * __this /* static, unused */, JsonSerializer_t251850770 * ____this0, EventHandler_1_t937589677 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerProxy::ilo_set_ReferenceResolver3(Newtonsoft.Json.JsonSerializer,Newtonsoft.Json.Serialization.IReferenceResolver)
extern "C"  void JsonSerializerProxy_ilo_set_ReferenceResolver3_m775063534 (Il2CppObject * __this /* static, unused */, JsonSerializer_t251850770 * ____this0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.JsonConverterCollection Newtonsoft.Json.Serialization.JsonSerializerProxy::ilo_get_Converters4(Newtonsoft.Json.JsonSerializer)
extern "C"  JsonConverterCollection_t530165700 * JsonSerializerProxy_ilo_get_Converters4_m183377936 (Il2CppObject * __this /* static, unused */, JsonSerializer_t251850770 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.DefaultValueHandling Newtonsoft.Json.Serialization.JsonSerializerProxy::ilo_get_DefaultValueHandling5(Newtonsoft.Json.JsonSerializer)
extern "C"  int32_t JsonSerializerProxy_ilo_get_DefaultValueHandling5_m158899920 (Il2CppObject * __this /* static, unused */, JsonSerializer_t251850770 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.NullValueHandling Newtonsoft.Json.Serialization.JsonSerializerProxy::ilo_get_NullValueHandling6(Newtonsoft.Json.JsonSerializer)
extern "C"  int32_t JsonSerializerProxy_ilo_get_NullValueHandling6_m3061677045 (Il2CppObject * __this /* static, unused */, JsonSerializer_t251850770 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerProxy::ilo_set_NullValueHandling7(Newtonsoft.Json.JsonSerializer,Newtonsoft.Json.NullValueHandling)
extern "C"  void JsonSerializerProxy_ilo_set_NullValueHandling7_m640049365 (Il2CppObject * __this /* static, unused */, JsonSerializer_t251850770 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerProxy::ilo_set_ObjectCreationHandling8(Newtonsoft.Json.JsonSerializer,Newtonsoft.Json.ObjectCreationHandling)
extern "C"  void JsonSerializerProxy_ilo_set_ObjectCreationHandling8_m1715198288 (Il2CppObject * __this /* static, unused */, JsonSerializer_t251850770 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.ReferenceLoopHandling Newtonsoft.Json.Serialization.JsonSerializerProxy::ilo_get_ReferenceLoopHandling9(Newtonsoft.Json.JsonSerializer)
extern "C"  int32_t JsonSerializerProxy_ilo_get_ReferenceLoopHandling9_m1748619096 (Il2CppObject * __this /* static, unused */, JsonSerializer_t251850770 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerProxy::ilo_set_TypeNameAssemblyFormat10(Newtonsoft.Json.JsonSerializer,System.Runtime.Serialization.Formatters.FormatterAssemblyStyle)
extern "C"  void JsonSerializerProxy_ilo_set_TypeNameAssemblyFormat10_m1815335383 (Il2CppObject * __this /* static, unused */, JsonSerializer_t251850770 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.ConstructorHandling Newtonsoft.Json.Serialization.JsonSerializerProxy::ilo_get_ConstructorHandling11(Newtonsoft.Json.JsonSerializer)
extern "C"  int32_t JsonSerializerProxy_ilo_get_ConstructorHandling11_m359195621 (Il2CppObject * __this /* static, unused */, JsonSerializer_t251850770 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Serialization.JsonSerializerProxy::ilo_Deserialize12(Newtonsoft.Json.JsonSerializer,Newtonsoft.Json.JsonReader,System.Type)
extern "C"  Il2CppObject * JsonSerializerProxy_ilo_Deserialize12_m2979049150 (Il2CppObject * __this /* static, unused */, JsonSerializer_t251850770 * ____this0, JsonReader_t816925123 * ___reader1, Type_t * ___objectType2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerProxy::ilo_Serialize13(Newtonsoft.Json.Serialization.JsonSerializerInternalWriter,Newtonsoft.Json.JsonWriter,System.Object)
extern "C"  void JsonSerializerProxy_ilo_Serialize13_m2835966506 (Il2CppObject * __this /* static, unused */, JsonSerializerInternalWriter_t3814549686 * ____this0, JsonWriter_t972330355 * ___jsonWriter1, Il2CppObject * ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerProxy::ilo_Serialize14(Newtonsoft.Json.JsonSerializer,Newtonsoft.Json.JsonWriter,System.Object)
extern "C"  void JsonSerializerProxy_ilo_Serialize14_m3448130491 (Il2CppObject * __this /* static, unused */, JsonSerializer_t251850770 * ____this0, JsonWriter_t972330355 * ___jsonWriter1, Il2CppObject * ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
