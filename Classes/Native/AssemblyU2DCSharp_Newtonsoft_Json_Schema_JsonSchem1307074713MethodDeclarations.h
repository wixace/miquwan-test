﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Schema.JsonSchemaResolver/<GetSchema>c__AnonStorey12F
struct U3CGetSchemaU3Ec__AnonStorey12F_t1307074713;
// Newtonsoft.Json.Schema.JsonSchema
struct JsonSchema_t460567603;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Schema_JsonSchema460567603.h"

// System.Void Newtonsoft.Json.Schema.JsonSchemaResolver/<GetSchema>c__AnonStorey12F::.ctor()
extern "C"  void U3CGetSchemaU3Ec__AnonStorey12F__ctor_m81970418 (U3CGetSchemaU3Ec__AnonStorey12F_t1307074713 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Schema.JsonSchemaResolver/<GetSchema>c__AnonStorey12F::<>m__37E(Newtonsoft.Json.Schema.JsonSchema)
extern "C"  bool U3CGetSchemaU3Ec__AnonStorey12F_U3CU3Em__37E_m2791990173 (U3CGetSchemaU3Ec__AnonStorey12F_t1307074713 * __this, JsonSchema_t460567603 * ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
