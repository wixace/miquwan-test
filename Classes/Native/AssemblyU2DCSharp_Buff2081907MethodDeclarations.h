﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Buff
struct Buff_t2081907;
// buffCfg
struct buffCfg_t227963665;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_buffCfg227963665.h"
#include "AssemblyU2DCSharp_BuffEnum_EBuffFunType4189275319.h"

// System.Void Buff::.ctor()
extern "C"  void Buff__ctor_m2816087944 (Buff_t2081907 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Buff::get_srcAtkPower()
extern "C"  float Buff_get_srcAtkPower_m1286942242 (Buff_t2081907 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Buff::get_srcCureP()
extern "C"  float Buff_get_srcCureP_m70462072 (Buff_t2081907 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Buff::Setup(buffCfg,System.Int32,System.Single,System.Single)
extern "C"  void Buff_Setup_m3611632867 (Buff_t2081907 * __this, buffCfg_t227963665 * ___buffDB0, int32_t ___awakenLv1, float ___timeBonus2, float ___timeBonusPer3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Buff::HasFun(BuffEnum.EBuffFunType)
extern "C"  bool Buff_HasFun_m531592866 (Buff_t2081907 * __this, int32_t ___funType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Buff::Reset()
extern "C"  void Buff_Reset_m462520885 (Buff_t2081907 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Buff::Cleanup()
extern "C"  void Buff_Cleanup_m3099878666 (Buff_t2081907 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
