﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._8c0a8d06add0702174c1414cf2bba00e
struct _8c0a8d06add0702174c1414cf2bba00e_t2489032853;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._8c0a8d06add0702174c1414cf2bba00e::.ctor()
extern "C"  void _8c0a8d06add0702174c1414cf2bba00e__ctor_m4080688440 (_8c0a8d06add0702174c1414cf2bba00e_t2489032853 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._8c0a8d06add0702174c1414cf2bba00e::_8c0a8d06add0702174c1414cf2bba00em2(System.Int32)
extern "C"  int32_t _8c0a8d06add0702174c1414cf2bba00e__8c0a8d06add0702174c1414cf2bba00em2_m533406073 (_8c0a8d06add0702174c1414cf2bba00e_t2489032853 * __this, int32_t ____8c0a8d06add0702174c1414cf2bba00ea0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._8c0a8d06add0702174c1414cf2bba00e::_8c0a8d06add0702174c1414cf2bba00em(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _8c0a8d06add0702174c1414cf2bba00e__8c0a8d06add0702174c1414cf2bba00em_m3523289437 (_8c0a8d06add0702174c1414cf2bba00e_t2489032853 * __this, int32_t ____8c0a8d06add0702174c1414cf2bba00ea0, int32_t ____8c0a8d06add0702174c1414cf2bba00e831, int32_t ____8c0a8d06add0702174c1414cf2bba00ec2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
