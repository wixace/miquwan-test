﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>
struct Dictionary_2_t509053110;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K1123989164.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m2856760165_gshared (Enumerator_t1123989164 * __this, Dictionary_2_t509053110 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m2856760165(__this, ___host0, method) ((  void (*) (Enumerator_t1123989164 *, Dictionary_2_t509053110 *, const MethodInfo*))Enumerator__ctor_m2856760165_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2686119590_gshared (Enumerator_t1123989164 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m2686119590(__this, method) ((  Il2CppObject * (*) (Enumerator_t1123989164 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2686119590_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3346524208_gshared (Enumerator_t1123989164 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m3346524208(__this, method) ((  void (*) (Enumerator_t1123989164 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3346524208_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::Dispose()
extern "C"  void Enumerator_Dispose_m2259275079_gshared (Enumerator_t1123989164 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m2259275079(__this, method) ((  void (*) (Enumerator_t1123989164 *, const MethodInfo*))Enumerator_Dispose_m2259275079_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1112345824_gshared (Enumerator_t1123989164 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m1112345824(__this, method) ((  bool (*) (Enumerator_t1123989164 *, const MethodInfo*))Enumerator_MoveNext_m1112345824_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m3545032888_gshared (Enumerator_t1123989164 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m3545032888(__this, method) ((  Il2CppObject * (*) (Enumerator_t1123989164 *, const MethodInfo*))Enumerator_get_Current_m3545032888_gshared)(__this, method)
