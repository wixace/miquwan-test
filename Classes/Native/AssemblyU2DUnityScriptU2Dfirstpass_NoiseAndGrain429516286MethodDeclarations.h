﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// NoiseAndGrain
struct NoiseAndGrain_t429516286;
// UnityEngine.RenderTexture
struct RenderTexture_t1963041563;
// UnityEngine.Material
struct Material_t3870600107;
// UnityEngine.Texture2D
struct Texture2D_t3884108195;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture1963041563.h"
#include "UnityEngine_UnityEngine_Material3870600107.h"
#include "UnityEngine_UnityEngine_Texture2D3884108195.h"

// System.Void NoiseAndGrain::.ctor()
extern "C"  void NoiseAndGrain__ctor_m568501604 (NoiseAndGrain_t429516286 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NoiseAndGrain::.cctor()
extern "C"  void NoiseAndGrain__cctor_m4256551625 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NoiseAndGrain::CheckResources()
extern "C"  bool NoiseAndGrain_CheckResources_m1557307959 (NoiseAndGrain_t429516286 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NoiseAndGrain::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void NoiseAndGrain_OnRenderImage_m4284655738 (NoiseAndGrain_t429516286 * __this, RenderTexture_t1963041563 * ___source0, RenderTexture_t1963041563 * ___destination1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NoiseAndGrain::DrawNoiseQuadGrid(UnityEngine.RenderTexture,UnityEngine.RenderTexture,UnityEngine.Material,UnityEngine.Texture2D,System.Int32)
extern "C"  void NoiseAndGrain_DrawNoiseQuadGrid_m937261686 (Il2CppObject * __this /* static, unused */, RenderTexture_t1963041563 * ___source0, RenderTexture_t1963041563 * ___dest1, Material_t3870600107 * ___fxMaterial2, Texture2D_t3884108195 * ___noise3, int32_t ___passNr4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NoiseAndGrain::Main()
extern "C"  void NoiseAndGrain_Main_m1873495769 (NoiseAndGrain_t429516286 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
