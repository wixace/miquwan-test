﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PuzzleManager
struct PuzzleManager_t2329339247;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// InputItem
struct InputItem_t3710611933;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PuzzleManager2329339247.h"
#include "AssemblyU2DCSharp_InputItem3710611933.h"

// System.Void PuzzleManager::.ctor()
extern "C"  void PuzzleManager__ctor_m3400345820 (PuzzleManager_t2329339247 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PuzzleManager::Init()
extern "C"  void PuzzleManager_Init_m338319864 (PuzzleManager_t2329339247 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PuzzleManager::Start()
extern "C"  void PuzzleManager_Start_m2347483612 (PuzzleManager_t2329339247 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PuzzleManager::Open(System.Int32)
extern "C"  void PuzzleManager_Open_m1933307651 (PuzzleManager_t2329339247 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] PuzzleManager::GetRandomNum(System.Int32[])
extern "C"  Int32U5BU5D_t3230847821* PuzzleManager_GetRandomNum_m624030264 (PuzzleManager_t2329339247 * __this, Int32U5BU5D_t3230847821* ___num0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PuzzleManager::Caculate(System.Int32)
extern "C"  void PuzzleManager_Caculate_m1638089775 (PuzzleManager_t2329339247 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] PuzzleManager::ilo_GetRandomNum1(PuzzleManager,System.Int32[])
extern "C"  Int32U5BU5D_t3230847821* PuzzleManager_ilo_GetRandomNum1_m925070665 (Il2CppObject * __this /* static, unused */, PuzzleManager_t2329339247 * ____this0, Int32U5BU5D_t3230847821* ___num1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PuzzleManager::ilo_SetBasicPoint2(InputItem,System.Int32)
extern "C"  void PuzzleManager_ilo_SetBasicPoint2_m1593996363 (Il2CppObject * __this /* static, unused */, InputItem_t3710611933 * ____this0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PuzzleManager::ilo_Init3(InputItem)
extern "C"  void PuzzleManager_ilo_Init3_m3308663531 (Il2CppObject * __this /* static, unused */, InputItem_t3710611933 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PuzzleManager::ilo_AddTicket4(System.Int32)
extern "C"  void PuzzleManager_ilo_AddTicket4_m650078515 (Il2CppObject * __this /* static, unused */, int32_t ___v0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PuzzleManager::ilo_Caculate5(PuzzleManager,System.Int32)
extern "C"  void PuzzleManager_ilo_Caculate5_m3936062522 (Il2CppObject * __this /* static, unused */, PuzzleManager_t2329339247 * ____this0, int32_t ___id1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PuzzleManager::ilo_Show6(InputItem)
extern "C"  void PuzzleManager_ilo_Show6_m2811567477 (Il2CppObject * __this /* static, unused */, InputItem_t3710611933 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PuzzleManager::ilo_get_IsOpened7(InputItem)
extern "C"  bool PuzzleManager_ilo_get_IsOpened7_m2089688351 (Il2CppObject * __this /* static, unused */, InputItem_t3710611933 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
