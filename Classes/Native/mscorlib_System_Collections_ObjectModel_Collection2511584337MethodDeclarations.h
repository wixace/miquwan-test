﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<Pathfinding.ClipperLib.IntPoint>
struct Collection_1_t2511584337;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// Pathfinding.ClipperLib.IntPoint[]
struct IntPointU5BU5D_t3364663986;
// System.Collections.Generic.IEnumerator`1<Pathfinding.ClipperLib.IntPoint>
struct IEnumerator_1_t943023932;
// System.Collections.Generic.IList`1<Pathfinding.ClipperLib.IntPoint>
struct IList_1_t1725806086;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Object4170816371.h"
#include "Pathfinding_ClipperLib_Pathfinding_ClipperLib_IntP3326126179.h"

// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.ClipperLib.IntPoint>::.ctor()
extern "C"  void Collection_1__ctor_m3597922000_gshared (Collection_1_t2511584337 * __this, const MethodInfo* method);
#define Collection_1__ctor_m3597922000(__this, method) ((  void (*) (Collection_1_t2511584337 *, const MethodInfo*))Collection_1__ctor_m3597922000_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Pathfinding.ClipperLib.IntPoint>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1868512427_gshared (Collection_1_t2511584337 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1868512427(__this, method) ((  bool (*) (Collection_1_t2511584337 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1868512427_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.ClipperLib.IntPoint>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Collection_1_System_Collections_ICollection_CopyTo_m73119860_gshared (Collection_1_t2511584337 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m73119860(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t2511584337 *, Il2CppArray *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m73119860_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<Pathfinding.ClipperLib.IntPoint>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Collection_1_System_Collections_IEnumerable_GetEnumerator_m3286558063_gshared (Collection_1_t2511584337 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m3286558063(__this, method) ((  Il2CppObject * (*) (Collection_1_t2511584337 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m3286558063_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Pathfinding.ClipperLib.IntPoint>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_Add_m109762274_gshared (Collection_1_t2511584337 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m109762274(__this, ___value0, method) ((  int32_t (*) (Collection_1_t2511584337 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m109762274_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Pathfinding.ClipperLib.IntPoint>::System.Collections.IList.Contains(System.Object)
extern "C"  bool Collection_1_System_Collections_IList_Contains_m3416340778_gshared (Collection_1_t2511584337 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m3416340778(__this, ___value0, method) ((  bool (*) (Collection_1_t2511584337 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m3416340778_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Pathfinding.ClipperLib.IntPoint>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_IndexOf_m887147578_gshared (Collection_1_t2511584337 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m887147578(__this, ___value0, method) ((  int32_t (*) (Collection_1_t2511584337 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m887147578_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.ClipperLib.IntPoint>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_Insert_m1302254437_gshared (Collection_1_t2511584337 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m1302254437(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t2511584337 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m1302254437_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.ClipperLib.IntPoint>::System.Collections.IList.Remove(System.Object)
extern "C"  void Collection_1_System_Collections_IList_Remove_m1300772259_gshared (Collection_1_t2511584337 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m1300772259(__this, ___value0, method) ((  void (*) (Collection_1_t2511584337 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m1300772259_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Pathfinding.ClipperLib.IntPoint>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m2658047474_gshared (Collection_1_t2511584337 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m2658047474(__this, method) ((  bool (*) (Collection_1_t2511584337 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m2658047474_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<Pathfinding.ClipperLib.IntPoint>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Collection_1_System_Collections_ICollection_get_SyncRoot_m3923317726_gshared (Collection_1_t2511584337 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m3923317726(__this, method) ((  Il2CppObject * (*) (Collection_1_t2511584337 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m3923317726_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Pathfinding.ClipperLib.IntPoint>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool Collection_1_System_Collections_IList_get_IsFixedSize_m78483481_gshared (Collection_1_t2511584337 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m78483481(__this, method) ((  bool (*) (Collection_1_t2511584337 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m78483481_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Pathfinding.ClipperLib.IntPoint>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_IList_get_IsReadOnly_m2119108544_gshared (Collection_1_t2511584337 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m2119108544(__this, method) ((  bool (*) (Collection_1_t2511584337 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m2119108544_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<Pathfinding.ClipperLib.IntPoint>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * Collection_1_System_Collections_IList_get_Item_m392660453_gshared (Collection_1_t2511584337 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m392660453(__this, ___index0, method) ((  Il2CppObject * (*) (Collection_1_t2511584337 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m392660453_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.ClipperLib.IntPoint>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_set_Item_m2244983292_gshared (Collection_1_t2511584337 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m2244983292(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t2511584337 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m2244983292_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.ClipperLib.IntPoint>::Add(T)
extern "C"  void Collection_1_Add_m3154245551_gshared (Collection_1_t2511584337 * __this, IntPoint_t3326126179  ___item0, const MethodInfo* method);
#define Collection_1_Add_m3154245551(__this, ___item0, method) ((  void (*) (Collection_1_t2511584337 *, IntPoint_t3326126179 , const MethodInfo*))Collection_1_Add_m3154245551_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.ClipperLib.IntPoint>::Clear()
extern "C"  void Collection_1_Clear_m1004055291_gshared (Collection_1_t2511584337 * __this, const MethodInfo* method);
#define Collection_1_Clear_m1004055291(__this, method) ((  void (*) (Collection_1_t2511584337 *, const MethodInfo*))Collection_1_Clear_m1004055291_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.ClipperLib.IntPoint>::ClearItems()
extern "C"  void Collection_1_ClearItems_m2260572903_gshared (Collection_1_t2511584337 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m2260572903(__this, method) ((  void (*) (Collection_1_t2511584337 *, const MethodInfo*))Collection_1_ClearItems_m2260572903_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Pathfinding.ClipperLib.IntPoint>::Contains(T)
extern "C"  bool Collection_1_Contains_m426756521_gshared (Collection_1_t2511584337 * __this, IntPoint_t3326126179  ___item0, const MethodInfo* method);
#define Collection_1_Contains_m426756521(__this, ___item0, method) ((  bool (*) (Collection_1_t2511584337 *, IntPoint_t3326126179 , const MethodInfo*))Collection_1_Contains_m426756521_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.ClipperLib.IntPoint>::CopyTo(T[],System.Int32)
extern "C"  void Collection_1_CopyTo_m3870355935_gshared (Collection_1_t2511584337 * __this, IntPointU5BU5D_t3364663986* ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_CopyTo_m3870355935(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t2511584337 *, IntPointU5BU5D_t3364663986*, int32_t, const MethodInfo*))Collection_1_CopyTo_m3870355935_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<Pathfinding.ClipperLib.IntPoint>::GetEnumerator()
extern "C"  Il2CppObject* Collection_1_GetEnumerator_m1272125068_gshared (Collection_1_t2511584337 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m1272125068(__this, method) ((  Il2CppObject* (*) (Collection_1_t2511584337 *, const MethodInfo*))Collection_1_GetEnumerator_m1272125068_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Pathfinding.ClipperLib.IntPoint>::IndexOf(T)
extern "C"  int32_t Collection_1_IndexOf_m33267619_gshared (Collection_1_t2511584337 * __this, IntPoint_t3326126179  ___item0, const MethodInfo* method);
#define Collection_1_IndexOf_m33267619(__this, ___item0, method) ((  int32_t (*) (Collection_1_t2511584337 *, IntPoint_t3326126179 , const MethodInfo*))Collection_1_IndexOf_m33267619_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.ClipperLib.IntPoint>::Insert(System.Int32,T)
extern "C"  void Collection_1_Insert_m3780113430_gshared (Collection_1_t2511584337 * __this, int32_t ___index0, IntPoint_t3326126179  ___item1, const MethodInfo* method);
#define Collection_1_Insert_m3780113430(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t2511584337 *, int32_t, IntPoint_t3326126179 , const MethodInfo*))Collection_1_Insert_m3780113430_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.ClipperLib.IntPoint>::InsertItem(System.Int32,T)
extern "C"  void Collection_1_InsertItem_m1931683657_gshared (Collection_1_t2511584337 * __this, int32_t ___index0, IntPoint_t3326126179  ___item1, const MethodInfo* method);
#define Collection_1_InsertItem_m1931683657(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t2511584337 *, int32_t, IntPoint_t3326126179 , const MethodInfo*))Collection_1_InsertItem_m1931683657_gshared)(__this, ___index0, ___item1, method)
// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.Collection`1<Pathfinding.ClipperLib.IntPoint>::get_Items()
extern "C"  Il2CppObject* Collection_1_get_Items_m4247251727_gshared (Collection_1_t2511584337 * __this, const MethodInfo* method);
#define Collection_1_get_Items_m4247251727(__this, method) ((  Il2CppObject* (*) (Collection_1_t2511584337 *, const MethodInfo*))Collection_1_get_Items_m4247251727_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Pathfinding.ClipperLib.IntPoint>::Remove(T)
extern "C"  bool Collection_1_Remove_m1345118180_gshared (Collection_1_t2511584337 * __this, IntPoint_t3326126179  ___item0, const MethodInfo* method);
#define Collection_1_Remove_m1345118180(__this, ___item0, method) ((  bool (*) (Collection_1_t2511584337 *, IntPoint_t3326126179 , const MethodInfo*))Collection_1_Remove_m1345118180_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.ClipperLib.IntPoint>::RemoveAt(System.Int32)
extern "C"  void Collection_1_RemoveAt_m1653966300_gshared (Collection_1_t2511584337 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveAt_m1653966300(__this, ___index0, method) ((  void (*) (Collection_1_t2511584337 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m1653966300_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.ClipperLib.IntPoint>::RemoveItem(System.Int32)
extern "C"  void Collection_1_RemoveItem_m3223339772_gshared (Collection_1_t2511584337 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveItem_m3223339772(__this, ___index0, method) ((  void (*) (Collection_1_t2511584337 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m3223339772_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Pathfinding.ClipperLib.IntPoint>::get_Count()
extern "C"  int32_t Collection_1_get_Count_m848603704_gshared (Collection_1_t2511584337 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m848603704(__this, method) ((  int32_t (*) (Collection_1_t2511584337 *, const MethodInfo*))Collection_1_get_Count_m848603704_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<Pathfinding.ClipperLib.IntPoint>::get_Item(System.Int32)
extern "C"  IntPoint_t3326126179  Collection_1_get_Item_m4218866400_gshared (Collection_1_t2511584337 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_get_Item_m4218866400(__this, ___index0, method) ((  IntPoint_t3326126179  (*) (Collection_1_t2511584337 *, int32_t, const MethodInfo*))Collection_1_get_Item_m4218866400_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.ClipperLib.IntPoint>::set_Item(System.Int32,T)
extern "C"  void Collection_1_set_Item_m956270445_gshared (Collection_1_t2511584337 * __this, int32_t ___index0, IntPoint_t3326126179  ___value1, const MethodInfo* method);
#define Collection_1_set_Item_m956270445(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t2511584337 *, int32_t, IntPoint_t3326126179 , const MethodInfo*))Collection_1_set_Item_m956270445_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.ClipperLib.IntPoint>::SetItem(System.Int32,T)
extern "C"  void Collection_1_SetItem_m2535989100_gshared (Collection_1_t2511584337 * __this, int32_t ___index0, IntPoint_t3326126179  ___item1, const MethodInfo* method);
#define Collection_1_SetItem_m2535989100(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t2511584337 *, int32_t, IntPoint_t3326126179 , const MethodInfo*))Collection_1_SetItem_m2535989100_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Pathfinding.ClipperLib.IntPoint>::IsValidItem(System.Object)
extern "C"  bool Collection_1_IsValidItem_m510722403_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_IsValidItem_m510722403(__this /* static, unused */, ___item0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_IsValidItem_m510722403_gshared)(__this /* static, unused */, ___item0, method)
// T System.Collections.ObjectModel.Collection`1<Pathfinding.ClipperLib.IntPoint>::ConvertItem(System.Object)
extern "C"  IntPoint_t3326126179  Collection_1_ConvertItem_m3465546175_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_ConvertItem_m3465546175(__this /* static, unused */, ___item0, method) ((  IntPoint_t3326126179  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_ConvertItem_m3465546175_gshared)(__this /* static, unused */, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.ClipperLib.IntPoint>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C"  void Collection_1_CheckWritable_m3439429407_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_CheckWritable_m3439429407(__this /* static, unused */, ___list0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_CheckWritable_m3439429407_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Pathfinding.ClipperLib.IntPoint>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsSynchronized_m3690134401_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsSynchronized_m3690134401(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsSynchronized_m3690134401_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Pathfinding.ClipperLib.IntPoint>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsFixedSize_m1859379902_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsFixedSize_m1859379902(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsFixedSize_m1859379902_gshared)(__this /* static, unused */, ___list0, method)
