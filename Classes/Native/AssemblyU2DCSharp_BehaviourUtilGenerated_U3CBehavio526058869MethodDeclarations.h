﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BehaviourUtilGenerated/<BehaviourUtil_DelayCall_GetDelegate_member3_arg1>c__AnonStorey47`2<System.Object,System.Object>
struct U3CBehaviourUtil_DelayCall_GetDelegate_member3_arg1U3Ec__AnonStorey47_2_t526058869;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void BehaviourUtilGenerated/<BehaviourUtil_DelayCall_GetDelegate_member3_arg1>c__AnonStorey47`2<System.Object,System.Object>::.ctor()
extern "C"  void U3CBehaviourUtil_DelayCall_GetDelegate_member3_arg1U3Ec__AnonStorey47_2__ctor_m3874591903_gshared (U3CBehaviourUtil_DelayCall_GetDelegate_member3_arg1U3Ec__AnonStorey47_2_t526058869 * __this, const MethodInfo* method);
#define U3CBehaviourUtil_DelayCall_GetDelegate_member3_arg1U3Ec__AnonStorey47_2__ctor_m3874591903(__this, method) ((  void (*) (U3CBehaviourUtil_DelayCall_GetDelegate_member3_arg1U3Ec__AnonStorey47_2_t526058869 *, const MethodInfo*))U3CBehaviourUtil_DelayCall_GetDelegate_member3_arg1U3Ec__AnonStorey47_2__ctor_m3874591903_gshared)(__this, method)
// System.Void BehaviourUtilGenerated/<BehaviourUtil_DelayCall_GetDelegate_member3_arg1>c__AnonStorey47`2<System.Object,System.Object>::<>m__6(T1,T2)
extern "C"  void U3CBehaviourUtil_DelayCall_GetDelegate_member3_arg1U3Ec__AnonStorey47_2_U3CU3Em__6_m3314266645_gshared (U3CBehaviourUtil_DelayCall_GetDelegate_member3_arg1U3Ec__AnonStorey47_2_t526058869 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, const MethodInfo* method);
#define U3CBehaviourUtil_DelayCall_GetDelegate_member3_arg1U3Ec__AnonStorey47_2_U3CU3Em__6_m3314266645(__this, ___arg10, ___arg21, method) ((  void (*) (U3CBehaviourUtil_DelayCall_GetDelegate_member3_arg1U3Ec__AnonStorey47_2_t526058869 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))U3CBehaviourUtil_DelayCall_GetDelegate_member3_arg1U3Ec__AnonStorey47_2_U3CU3Em__6_m3314266645_gshared)(__this, ___arg10, ___arg21, method)
