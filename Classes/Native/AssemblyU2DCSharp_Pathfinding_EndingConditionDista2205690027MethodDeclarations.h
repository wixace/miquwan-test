﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.EndingConditionDistance
struct EndingConditionDistance_t2205690027;
// Pathfinding.Path
struct Path_t1974241691;
// Pathfinding.PathNode
struct PathNode_t417131581;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_Path1974241691.h"
#include "AssemblyU2DCSharp_Pathfinding_PathNode417131581.h"

// System.Void Pathfinding.EndingConditionDistance::.ctor(Pathfinding.Path,System.Int32)
extern "C"  void EndingConditionDistance__ctor_m3066815866 (EndingConditionDistance_t2205690027 * __this, Path_t1974241691 * ___p0, int32_t ___maxGScore1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.EndingConditionDistance::TargetFound(Pathfinding.PathNode)
extern "C"  bool EndingConditionDistance_TargetFound_m3775711070 (EndingConditionDistance_t2205690027 * __this, PathNode_t417131581 * ___node0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
