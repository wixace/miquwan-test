﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Pathfinding.PathHandler
struct PathHandler_t918952263;
// OnPathDelegate
struct OnPathDelegate_t598607977;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// Pathfinding.GraphNode[]
struct GraphNodeU5BU5D_t927449255;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t215400611;
// System.Collections.Generic.List`1<Pathfinding.GraphNode>
struct List_1_t1391797922;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t1355284822;
// Pathfinding.PathNode
struct PathNode_t417131581;
// Pathfinding.NNConstraint
struct NNConstraint_t758567699;
// Pathfinding.Path
struct Path_t1974241691;
// Pathfinding.GraphNode
struct GraphNode_t23612370;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t1244034627;

#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_PathState1615290188.h"
#include "AssemblyU2DCSharp_PathCompleteState1625108115.h"
#include "mscorlib_System_DateTime4283661327.h"
#include "AssemblyU2DCSharp_Heuristic2765431530.h"
#include "AssemblyU2DCSharp_Pathfinding_Int31974045594.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.Path
struct  Path_t1974241691  : public Il2CppObject
{
public:
	// Pathfinding.PathHandler Pathfinding.Path::pathHandler
	PathHandler_t918952263 * ___pathHandler_0;
	// OnPathDelegate Pathfinding.Path::callback
	OnPathDelegate_t598607977 * ___callback_1;
	// OnPathDelegate Pathfinding.Path::immediateCallback
	OnPathDelegate_t598607977 * ___immediateCallback_2;
	// PathState Pathfinding.Path::state
	int32_t ___state_3;
	// System.Object Pathfinding.Path::stateLock
	Il2CppObject * ___stateLock_4;
	// PathCompleteState Pathfinding.Path::pathCompleteState
	int32_t ___pathCompleteState_5;
	// System.String Pathfinding.Path::_errorLog
	String_t* ____errorLog_6;
	// Pathfinding.GraphNode[] Pathfinding.Path::_path
	GraphNodeU5BU5D_t927449255* ____path_7;
	// UnityEngine.Vector3[] Pathfinding.Path::_vectorPath
	Vector3U5BU5D_t215400611* ____vectorPath_8;
	// System.Collections.Generic.List`1<Pathfinding.GraphNode> Pathfinding.Path::path
	List_1_t1391797922 * ___path_9;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> Pathfinding.Path::vectorPath
	List_1_t1355284822 * ___vectorPath_10;
	// System.Single Pathfinding.Path::maxFrameTime
	float ___maxFrameTime_11;
	// Pathfinding.PathNode Pathfinding.Path::currentR
	PathNode_t417131581 * ___currentR_12;
	// System.Single Pathfinding.Path::duration
	float ___duration_13;
	// System.Int32 Pathfinding.Path::searchIterations
	int32_t ___searchIterations_14;
	// System.Int32 Pathfinding.Path::searchedNodes
	int32_t ___searchedNodes_15;
	// System.DateTime Pathfinding.Path::callTime
	DateTime_t4283661327  ___callTime_16;
	// System.Boolean Pathfinding.Path::recycled
	bool ___recycled_17;
	// System.Boolean Pathfinding.Path::hasBeenReset
	bool ___hasBeenReset_18;
	// Pathfinding.NNConstraint Pathfinding.Path::nnConstraint
	NNConstraint_t758567699 * ___nnConstraint_19;
	// Pathfinding.Path Pathfinding.Path::next
	Path_t1974241691 * ___next_20;
	// System.Int32 Pathfinding.Path::radius
	int32_t ___radius_21;
	// System.Int32 Pathfinding.Path::walkabilityMask
	int32_t ___walkabilityMask_22;
	// System.Int32 Pathfinding.Path::height
	int32_t ___height_23;
	// System.Int32 Pathfinding.Path::turnRadius
	int32_t ___turnRadius_24;
	// System.Int32 Pathfinding.Path::speed
	int32_t ___speed_25;
	// Heuristic Pathfinding.Path::heuristic
	int32_t ___heuristic_26;
	// System.Single Pathfinding.Path::heuristicScale
	float ___heuristicScale_27;
	// System.UInt16 Pathfinding.Path::pathID
	uint16_t ___pathID_28;
	// Pathfinding.GraphNode Pathfinding.Path::hTargetNode
	GraphNode_t23612370 * ___hTargetNode_29;
	// Pathfinding.Int3 Pathfinding.Path::hTarget
	Int3_t1974045594  ___hTarget_30;
	// System.Int32 Pathfinding.Path::enabledTags
	int32_t ___enabledTags_31;
	// System.Int32[] Pathfinding.Path::internalTagPenalties
	Int32U5BU5D_t3230847821* ___internalTagPenalties_33;
	// System.Int32[] Pathfinding.Path::manualTagPenalties
	Int32U5BU5D_t3230847821* ___manualTagPenalties_34;
	// System.Collections.Generic.List`1<System.Object> Pathfinding.Path::claimed
	List_1_t1244034627 * ___claimed_35;
	// System.Boolean Pathfinding.Path::releasedNotSilent
	bool ___releasedNotSilent_36;
	// System.UInt32 Pathfinding.Path::<id>k__BackingField
	uint32_t ___U3CidU3Ek__BackingField_37;

public:
	inline static int32_t get_offset_of_pathHandler_0() { return static_cast<int32_t>(offsetof(Path_t1974241691, ___pathHandler_0)); }
	inline PathHandler_t918952263 * get_pathHandler_0() const { return ___pathHandler_0; }
	inline PathHandler_t918952263 ** get_address_of_pathHandler_0() { return &___pathHandler_0; }
	inline void set_pathHandler_0(PathHandler_t918952263 * value)
	{
		___pathHandler_0 = value;
		Il2CppCodeGenWriteBarrier(&___pathHandler_0, value);
	}

	inline static int32_t get_offset_of_callback_1() { return static_cast<int32_t>(offsetof(Path_t1974241691, ___callback_1)); }
	inline OnPathDelegate_t598607977 * get_callback_1() const { return ___callback_1; }
	inline OnPathDelegate_t598607977 ** get_address_of_callback_1() { return &___callback_1; }
	inline void set_callback_1(OnPathDelegate_t598607977 * value)
	{
		___callback_1 = value;
		Il2CppCodeGenWriteBarrier(&___callback_1, value);
	}

	inline static int32_t get_offset_of_immediateCallback_2() { return static_cast<int32_t>(offsetof(Path_t1974241691, ___immediateCallback_2)); }
	inline OnPathDelegate_t598607977 * get_immediateCallback_2() const { return ___immediateCallback_2; }
	inline OnPathDelegate_t598607977 ** get_address_of_immediateCallback_2() { return &___immediateCallback_2; }
	inline void set_immediateCallback_2(OnPathDelegate_t598607977 * value)
	{
		___immediateCallback_2 = value;
		Il2CppCodeGenWriteBarrier(&___immediateCallback_2, value);
	}

	inline static int32_t get_offset_of_state_3() { return static_cast<int32_t>(offsetof(Path_t1974241691, ___state_3)); }
	inline int32_t get_state_3() const { return ___state_3; }
	inline int32_t* get_address_of_state_3() { return &___state_3; }
	inline void set_state_3(int32_t value)
	{
		___state_3 = value;
	}

	inline static int32_t get_offset_of_stateLock_4() { return static_cast<int32_t>(offsetof(Path_t1974241691, ___stateLock_4)); }
	inline Il2CppObject * get_stateLock_4() const { return ___stateLock_4; }
	inline Il2CppObject ** get_address_of_stateLock_4() { return &___stateLock_4; }
	inline void set_stateLock_4(Il2CppObject * value)
	{
		___stateLock_4 = value;
		Il2CppCodeGenWriteBarrier(&___stateLock_4, value);
	}

	inline static int32_t get_offset_of_pathCompleteState_5() { return static_cast<int32_t>(offsetof(Path_t1974241691, ___pathCompleteState_5)); }
	inline int32_t get_pathCompleteState_5() const { return ___pathCompleteState_5; }
	inline int32_t* get_address_of_pathCompleteState_5() { return &___pathCompleteState_5; }
	inline void set_pathCompleteState_5(int32_t value)
	{
		___pathCompleteState_5 = value;
	}

	inline static int32_t get_offset_of__errorLog_6() { return static_cast<int32_t>(offsetof(Path_t1974241691, ____errorLog_6)); }
	inline String_t* get__errorLog_6() const { return ____errorLog_6; }
	inline String_t** get_address_of__errorLog_6() { return &____errorLog_6; }
	inline void set__errorLog_6(String_t* value)
	{
		____errorLog_6 = value;
		Il2CppCodeGenWriteBarrier(&____errorLog_6, value);
	}

	inline static int32_t get_offset_of__path_7() { return static_cast<int32_t>(offsetof(Path_t1974241691, ____path_7)); }
	inline GraphNodeU5BU5D_t927449255* get__path_7() const { return ____path_7; }
	inline GraphNodeU5BU5D_t927449255** get_address_of__path_7() { return &____path_7; }
	inline void set__path_7(GraphNodeU5BU5D_t927449255* value)
	{
		____path_7 = value;
		Il2CppCodeGenWriteBarrier(&____path_7, value);
	}

	inline static int32_t get_offset_of__vectorPath_8() { return static_cast<int32_t>(offsetof(Path_t1974241691, ____vectorPath_8)); }
	inline Vector3U5BU5D_t215400611* get__vectorPath_8() const { return ____vectorPath_8; }
	inline Vector3U5BU5D_t215400611** get_address_of__vectorPath_8() { return &____vectorPath_8; }
	inline void set__vectorPath_8(Vector3U5BU5D_t215400611* value)
	{
		____vectorPath_8 = value;
		Il2CppCodeGenWriteBarrier(&____vectorPath_8, value);
	}

	inline static int32_t get_offset_of_path_9() { return static_cast<int32_t>(offsetof(Path_t1974241691, ___path_9)); }
	inline List_1_t1391797922 * get_path_9() const { return ___path_9; }
	inline List_1_t1391797922 ** get_address_of_path_9() { return &___path_9; }
	inline void set_path_9(List_1_t1391797922 * value)
	{
		___path_9 = value;
		Il2CppCodeGenWriteBarrier(&___path_9, value);
	}

	inline static int32_t get_offset_of_vectorPath_10() { return static_cast<int32_t>(offsetof(Path_t1974241691, ___vectorPath_10)); }
	inline List_1_t1355284822 * get_vectorPath_10() const { return ___vectorPath_10; }
	inline List_1_t1355284822 ** get_address_of_vectorPath_10() { return &___vectorPath_10; }
	inline void set_vectorPath_10(List_1_t1355284822 * value)
	{
		___vectorPath_10 = value;
		Il2CppCodeGenWriteBarrier(&___vectorPath_10, value);
	}

	inline static int32_t get_offset_of_maxFrameTime_11() { return static_cast<int32_t>(offsetof(Path_t1974241691, ___maxFrameTime_11)); }
	inline float get_maxFrameTime_11() const { return ___maxFrameTime_11; }
	inline float* get_address_of_maxFrameTime_11() { return &___maxFrameTime_11; }
	inline void set_maxFrameTime_11(float value)
	{
		___maxFrameTime_11 = value;
	}

	inline static int32_t get_offset_of_currentR_12() { return static_cast<int32_t>(offsetof(Path_t1974241691, ___currentR_12)); }
	inline PathNode_t417131581 * get_currentR_12() const { return ___currentR_12; }
	inline PathNode_t417131581 ** get_address_of_currentR_12() { return &___currentR_12; }
	inline void set_currentR_12(PathNode_t417131581 * value)
	{
		___currentR_12 = value;
		Il2CppCodeGenWriteBarrier(&___currentR_12, value);
	}

	inline static int32_t get_offset_of_duration_13() { return static_cast<int32_t>(offsetof(Path_t1974241691, ___duration_13)); }
	inline float get_duration_13() const { return ___duration_13; }
	inline float* get_address_of_duration_13() { return &___duration_13; }
	inline void set_duration_13(float value)
	{
		___duration_13 = value;
	}

	inline static int32_t get_offset_of_searchIterations_14() { return static_cast<int32_t>(offsetof(Path_t1974241691, ___searchIterations_14)); }
	inline int32_t get_searchIterations_14() const { return ___searchIterations_14; }
	inline int32_t* get_address_of_searchIterations_14() { return &___searchIterations_14; }
	inline void set_searchIterations_14(int32_t value)
	{
		___searchIterations_14 = value;
	}

	inline static int32_t get_offset_of_searchedNodes_15() { return static_cast<int32_t>(offsetof(Path_t1974241691, ___searchedNodes_15)); }
	inline int32_t get_searchedNodes_15() const { return ___searchedNodes_15; }
	inline int32_t* get_address_of_searchedNodes_15() { return &___searchedNodes_15; }
	inline void set_searchedNodes_15(int32_t value)
	{
		___searchedNodes_15 = value;
	}

	inline static int32_t get_offset_of_callTime_16() { return static_cast<int32_t>(offsetof(Path_t1974241691, ___callTime_16)); }
	inline DateTime_t4283661327  get_callTime_16() const { return ___callTime_16; }
	inline DateTime_t4283661327 * get_address_of_callTime_16() { return &___callTime_16; }
	inline void set_callTime_16(DateTime_t4283661327  value)
	{
		___callTime_16 = value;
	}

	inline static int32_t get_offset_of_recycled_17() { return static_cast<int32_t>(offsetof(Path_t1974241691, ___recycled_17)); }
	inline bool get_recycled_17() const { return ___recycled_17; }
	inline bool* get_address_of_recycled_17() { return &___recycled_17; }
	inline void set_recycled_17(bool value)
	{
		___recycled_17 = value;
	}

	inline static int32_t get_offset_of_hasBeenReset_18() { return static_cast<int32_t>(offsetof(Path_t1974241691, ___hasBeenReset_18)); }
	inline bool get_hasBeenReset_18() const { return ___hasBeenReset_18; }
	inline bool* get_address_of_hasBeenReset_18() { return &___hasBeenReset_18; }
	inline void set_hasBeenReset_18(bool value)
	{
		___hasBeenReset_18 = value;
	}

	inline static int32_t get_offset_of_nnConstraint_19() { return static_cast<int32_t>(offsetof(Path_t1974241691, ___nnConstraint_19)); }
	inline NNConstraint_t758567699 * get_nnConstraint_19() const { return ___nnConstraint_19; }
	inline NNConstraint_t758567699 ** get_address_of_nnConstraint_19() { return &___nnConstraint_19; }
	inline void set_nnConstraint_19(NNConstraint_t758567699 * value)
	{
		___nnConstraint_19 = value;
		Il2CppCodeGenWriteBarrier(&___nnConstraint_19, value);
	}

	inline static int32_t get_offset_of_next_20() { return static_cast<int32_t>(offsetof(Path_t1974241691, ___next_20)); }
	inline Path_t1974241691 * get_next_20() const { return ___next_20; }
	inline Path_t1974241691 ** get_address_of_next_20() { return &___next_20; }
	inline void set_next_20(Path_t1974241691 * value)
	{
		___next_20 = value;
		Il2CppCodeGenWriteBarrier(&___next_20, value);
	}

	inline static int32_t get_offset_of_radius_21() { return static_cast<int32_t>(offsetof(Path_t1974241691, ___radius_21)); }
	inline int32_t get_radius_21() const { return ___radius_21; }
	inline int32_t* get_address_of_radius_21() { return &___radius_21; }
	inline void set_radius_21(int32_t value)
	{
		___radius_21 = value;
	}

	inline static int32_t get_offset_of_walkabilityMask_22() { return static_cast<int32_t>(offsetof(Path_t1974241691, ___walkabilityMask_22)); }
	inline int32_t get_walkabilityMask_22() const { return ___walkabilityMask_22; }
	inline int32_t* get_address_of_walkabilityMask_22() { return &___walkabilityMask_22; }
	inline void set_walkabilityMask_22(int32_t value)
	{
		___walkabilityMask_22 = value;
	}

	inline static int32_t get_offset_of_height_23() { return static_cast<int32_t>(offsetof(Path_t1974241691, ___height_23)); }
	inline int32_t get_height_23() const { return ___height_23; }
	inline int32_t* get_address_of_height_23() { return &___height_23; }
	inline void set_height_23(int32_t value)
	{
		___height_23 = value;
	}

	inline static int32_t get_offset_of_turnRadius_24() { return static_cast<int32_t>(offsetof(Path_t1974241691, ___turnRadius_24)); }
	inline int32_t get_turnRadius_24() const { return ___turnRadius_24; }
	inline int32_t* get_address_of_turnRadius_24() { return &___turnRadius_24; }
	inline void set_turnRadius_24(int32_t value)
	{
		___turnRadius_24 = value;
	}

	inline static int32_t get_offset_of_speed_25() { return static_cast<int32_t>(offsetof(Path_t1974241691, ___speed_25)); }
	inline int32_t get_speed_25() const { return ___speed_25; }
	inline int32_t* get_address_of_speed_25() { return &___speed_25; }
	inline void set_speed_25(int32_t value)
	{
		___speed_25 = value;
	}

	inline static int32_t get_offset_of_heuristic_26() { return static_cast<int32_t>(offsetof(Path_t1974241691, ___heuristic_26)); }
	inline int32_t get_heuristic_26() const { return ___heuristic_26; }
	inline int32_t* get_address_of_heuristic_26() { return &___heuristic_26; }
	inline void set_heuristic_26(int32_t value)
	{
		___heuristic_26 = value;
	}

	inline static int32_t get_offset_of_heuristicScale_27() { return static_cast<int32_t>(offsetof(Path_t1974241691, ___heuristicScale_27)); }
	inline float get_heuristicScale_27() const { return ___heuristicScale_27; }
	inline float* get_address_of_heuristicScale_27() { return &___heuristicScale_27; }
	inline void set_heuristicScale_27(float value)
	{
		___heuristicScale_27 = value;
	}

	inline static int32_t get_offset_of_pathID_28() { return static_cast<int32_t>(offsetof(Path_t1974241691, ___pathID_28)); }
	inline uint16_t get_pathID_28() const { return ___pathID_28; }
	inline uint16_t* get_address_of_pathID_28() { return &___pathID_28; }
	inline void set_pathID_28(uint16_t value)
	{
		___pathID_28 = value;
	}

	inline static int32_t get_offset_of_hTargetNode_29() { return static_cast<int32_t>(offsetof(Path_t1974241691, ___hTargetNode_29)); }
	inline GraphNode_t23612370 * get_hTargetNode_29() const { return ___hTargetNode_29; }
	inline GraphNode_t23612370 ** get_address_of_hTargetNode_29() { return &___hTargetNode_29; }
	inline void set_hTargetNode_29(GraphNode_t23612370 * value)
	{
		___hTargetNode_29 = value;
		Il2CppCodeGenWriteBarrier(&___hTargetNode_29, value);
	}

	inline static int32_t get_offset_of_hTarget_30() { return static_cast<int32_t>(offsetof(Path_t1974241691, ___hTarget_30)); }
	inline Int3_t1974045594  get_hTarget_30() const { return ___hTarget_30; }
	inline Int3_t1974045594 * get_address_of_hTarget_30() { return &___hTarget_30; }
	inline void set_hTarget_30(Int3_t1974045594  value)
	{
		___hTarget_30 = value;
	}

	inline static int32_t get_offset_of_enabledTags_31() { return static_cast<int32_t>(offsetof(Path_t1974241691, ___enabledTags_31)); }
	inline int32_t get_enabledTags_31() const { return ___enabledTags_31; }
	inline int32_t* get_address_of_enabledTags_31() { return &___enabledTags_31; }
	inline void set_enabledTags_31(int32_t value)
	{
		___enabledTags_31 = value;
	}

	inline static int32_t get_offset_of_internalTagPenalties_33() { return static_cast<int32_t>(offsetof(Path_t1974241691, ___internalTagPenalties_33)); }
	inline Int32U5BU5D_t3230847821* get_internalTagPenalties_33() const { return ___internalTagPenalties_33; }
	inline Int32U5BU5D_t3230847821** get_address_of_internalTagPenalties_33() { return &___internalTagPenalties_33; }
	inline void set_internalTagPenalties_33(Int32U5BU5D_t3230847821* value)
	{
		___internalTagPenalties_33 = value;
		Il2CppCodeGenWriteBarrier(&___internalTagPenalties_33, value);
	}

	inline static int32_t get_offset_of_manualTagPenalties_34() { return static_cast<int32_t>(offsetof(Path_t1974241691, ___manualTagPenalties_34)); }
	inline Int32U5BU5D_t3230847821* get_manualTagPenalties_34() const { return ___manualTagPenalties_34; }
	inline Int32U5BU5D_t3230847821** get_address_of_manualTagPenalties_34() { return &___manualTagPenalties_34; }
	inline void set_manualTagPenalties_34(Int32U5BU5D_t3230847821* value)
	{
		___manualTagPenalties_34 = value;
		Il2CppCodeGenWriteBarrier(&___manualTagPenalties_34, value);
	}

	inline static int32_t get_offset_of_claimed_35() { return static_cast<int32_t>(offsetof(Path_t1974241691, ___claimed_35)); }
	inline List_1_t1244034627 * get_claimed_35() const { return ___claimed_35; }
	inline List_1_t1244034627 ** get_address_of_claimed_35() { return &___claimed_35; }
	inline void set_claimed_35(List_1_t1244034627 * value)
	{
		___claimed_35 = value;
		Il2CppCodeGenWriteBarrier(&___claimed_35, value);
	}

	inline static int32_t get_offset_of_releasedNotSilent_36() { return static_cast<int32_t>(offsetof(Path_t1974241691, ___releasedNotSilent_36)); }
	inline bool get_releasedNotSilent_36() const { return ___releasedNotSilent_36; }
	inline bool* get_address_of_releasedNotSilent_36() { return &___releasedNotSilent_36; }
	inline void set_releasedNotSilent_36(bool value)
	{
		___releasedNotSilent_36 = value;
	}

	inline static int32_t get_offset_of_U3CidU3Ek__BackingField_37() { return static_cast<int32_t>(offsetof(Path_t1974241691, ___U3CidU3Ek__BackingField_37)); }
	inline uint32_t get_U3CidU3Ek__BackingField_37() const { return ___U3CidU3Ek__BackingField_37; }
	inline uint32_t* get_address_of_U3CidU3Ek__BackingField_37() { return &___U3CidU3Ek__BackingField_37; }
	inline void set_U3CidU3Ek__BackingField_37(uint32_t value)
	{
		___U3CidU3Ek__BackingField_37 = value;
	}
};

struct Path_t1974241691_StaticFields
{
public:
	// System.Int32[] Pathfinding.Path::ZeroTagPenalties
	Int32U5BU5D_t3230847821* ___ZeroTagPenalties_32;

public:
	inline static int32_t get_offset_of_ZeroTagPenalties_32() { return static_cast<int32_t>(offsetof(Path_t1974241691_StaticFields, ___ZeroTagPenalties_32)); }
	inline Int32U5BU5D_t3230847821* get_ZeroTagPenalties_32() const { return ___ZeroTagPenalties_32; }
	inline Int32U5BU5D_t3230847821** get_address_of_ZeroTagPenalties_32() { return &___ZeroTagPenalties_32; }
	inline void set_ZeroTagPenalties_32(Int32U5BU5D_t3230847821* value)
	{
		___ZeroTagPenalties_32 = value;
		Il2CppCodeGenWriteBarrier(&___ZeroTagPenalties_32, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
