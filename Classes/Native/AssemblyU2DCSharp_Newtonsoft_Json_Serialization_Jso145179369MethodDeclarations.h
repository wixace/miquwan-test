﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Serialization.JsonArrayContract
struct JsonArrayContract_t145179369;
// System.Type
struct Type_t;
// Newtonsoft.Json.Utilities.IWrappedCollection
struct IWrappedCollection_t3896884266;
// System.Object
struct Il2CppObject;
// System.Type[]
struct TypeU5BU5D_t3339007067;
// Newtonsoft.Json.Utilities.ReflectionDelegateFactory
struct ReflectionDelegateFactory_t1590616920;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Type2863145774.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_Jso145179369.h"

// System.Void Newtonsoft.Json.Serialization.JsonArrayContract::.ctor(System.Type)
extern "C"  void JsonArrayContract__ctor_m929367774 (JsonArrayContract_t145179369 * __this, Type_t * ___underlyingType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Newtonsoft.Json.Serialization.JsonArrayContract::get_CollectionItemType()
extern "C"  Type_t * JsonArrayContract_get_CollectionItemType_m1741250691 (JsonArrayContract_t145179369 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonArrayContract::set_CollectionItemType(System.Type)
extern "C"  void JsonArrayContract_set_CollectionItemType_m2219582984 (JsonArrayContract_t145179369 * __this, Type_t * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Utilities.IWrappedCollection Newtonsoft.Json.Serialization.JsonArrayContract::CreateWrapper(System.Object)
extern "C"  Il2CppObject * JsonArrayContract_CreateWrapper_m1607256922 (JsonArrayContract_t145179369 * __this, Il2CppObject * ___list0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonArrayContract::EnsureGenericWrapperCreator()
extern "C"  void JsonArrayContract_EnsureGenericWrapperCreator_m649127051 (JsonArrayContract_t145179369 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.JsonArrayContract::IsTypeGenericCollectionInterface(System.Type)
extern "C"  bool JsonArrayContract_IsTypeGenericCollectionInterface_m518944988 (JsonArrayContract_t145179369 * __this, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Newtonsoft.Json.Serialization.JsonArrayContract::ilo_get_CollectionItemType1(Newtonsoft.Json.Serialization.JsonArrayContract)
extern "C"  Type_t * JsonArrayContract_ilo_get_CollectionItemType1_m3359856197 (Il2CppObject * __this /* static, unused */, JsonArrayContract_t145179369 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Newtonsoft.Json.Serialization.JsonArrayContract::ilo_MakeGenericType2(System.Type,System.Type[])
extern "C"  Type_t * JsonArrayContract_ilo_MakeGenericType2_m3402805561 (Il2CppObject * __this /* static, unused */, Type_t * ___genericTypeDefinition0, TypeU5BU5D_t3339007067* ___innerTypes1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.JsonArrayContract::ilo_InheritsGenericDefinition3(System.Type,System.Type)
extern "C"  bool JsonArrayContract_ilo_InheritsGenericDefinition3_m961933763 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, Type_t * ___genericClassDefinition1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Utilities.ReflectionDelegateFactory Newtonsoft.Json.Serialization.JsonArrayContract::ilo_get_ReflectionDelegateFactory4()
extern "C"  ReflectionDelegateFactory_t1590616920 * JsonArrayContract_ilo_get_ReflectionDelegateFactory4_m2522336365 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
