﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UITextList
struct UITextList_t736798239;
// System.String
struct String_t;
// UnityEngine.Object
struct Object_t3071478659;
// UILabel
struct UILabel_t291504320;
// EventDelegate
struct EventDelegate_t4004424223;
// System.Collections.Generic.List`1<EventDelegate>
struct List_1_t1077642479;
// EventDelegate/Callback
struct Callback_t1094463061;
// UIWidget
struct UIWidget_t769069560;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_UILabel291504320.h"
#include "AssemblyU2DCSharp_UITextList736798239.h"
#include "AssemblyU2DCSharp_EventDelegate_Callback1094463061.h"
#include "AssemblyU2DCSharp_UILabel_Overflow229724305.h"
#include "AssemblyU2DCSharp_UIWidget769069560.h"

// System.Void UITextList::.ctor()
extern "C"  void UITextList__ctor_m1801479516 (UITextList_t736798239 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UITextList::get_isValid()
extern "C"  bool UITextList_get_isValid_m4155293295 (UITextList_t736798239 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UITextList::get_scrollValue()
extern "C"  float UITextList_get_scrollValue_m578722441 (UITextList_t736798239 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITextList::set_scrollValue(System.Single)
extern "C"  void UITextList_set_scrollValue_m4133436266 (UITextList_t736798239 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UITextList::get_lineHeight()
extern "C"  float UITextList_get_lineHeight_m2659975192 (UITextList_t736798239 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UITextList::get_scrollHeight()
extern "C"  int32_t UITextList_get_scrollHeight_m685030579 (UITextList_t736798239 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITextList::Clear()
extern "C"  void UITextList_Clear_m3502580103 (UITextList_t736798239 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITextList::Start()
extern "C"  void UITextList_Start_m748617308 (UITextList_t736798239 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITextList::Update()
extern "C"  void UITextList_Update_m1738152241 (UITextList_t736798239 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITextList::OnScroll(System.Single)
extern "C"  void UITextList_OnScroll_m1316852407 (UITextList_t736798239 * __this, float ___val0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITextList::OnDrag(UnityEngine.Vector2)
extern "C"  void UITextList_OnDrag_m2302371391 (UITextList_t736798239 * __this, Vector2_t4282066565  ___delta0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITextList::OnScrollBar()
extern "C"  void UITextList_OnScrollBar_m233289953 (UITextList_t736798239 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITextList::Add(System.String)
extern "C"  void UITextList_Add_m1668405703 (UITextList_t736798239 * __this, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITextList::Add(System.String,System.Boolean)
extern "C"  void UITextList_Add_m891605718 (UITextList_t736798239 * __this, String_t* ___text0, bool ___updateVisible1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITextList::Rebuild()
extern "C"  void UITextList_Rebuild_m267146645 (UITextList_t736798239 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITextList::UpdateVisibleText()
extern "C"  void UITextList_UpdateVisibleText_m1816301488 (UITextList_t736798239 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object UITextList::ilo_get_ambigiousFont1(UILabel)
extern "C"  Object_t3071478659 * UITextList_ilo_get_ambigiousFont1_m2496894461 (Il2CppObject * __this /* static, unused */, UILabel_t291504320 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UITextList::ilo_get_isValid2(UITextList)
extern "C"  bool UITextList_ilo_get_isValid2_m440799097 (Il2CppObject * __this /* static, unused */, UITextList_t736798239 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITextList::ilo_UpdateVisibleText3(UITextList)
extern "C"  void UITextList_ilo_UpdateVisibleText3_m1801439705 (Il2CppObject * __this /* static, unused */, UITextList_t736798239 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UITextList::ilo_get_fontSize4(UILabel)
extern "C"  int32_t UITextList_ilo_get_fontSize4_m3416873374 (Il2CppObject * __this /* static, unused */, UILabel_t291504320 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// EventDelegate UITextList::ilo_Add5(System.Collections.Generic.List`1<EventDelegate>,EventDelegate/Callback)
extern "C"  EventDelegate_t4004424223 * UITextList_ilo_Add5_m2390976331 (Il2CppObject * __this /* static, unused */, List_1_t1077642479 * ___list0, Callback_t1094463061 * ___callback1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITextList::ilo_set_overflowMethod6(UILabel,UILabel/Overflow)
extern "C"  void UITextList_ilo_set_overflowMethod6_m3918562144 (Il2CppObject * __this /* static, unused */, UILabel_t291504320 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITextList::ilo_set_scrollValue7(UITextList,System.Single)
extern "C"  void UITextList_ilo_set_scrollValue7_m3871602097 (Il2CppObject * __this /* static, unused */, UITextList_t736798239 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UITextList::ilo_get_width8(UIWidget)
extern "C"  int32_t UITextList_ilo_get_width8_m1927989708 (Il2CppObject * __this /* static, unused */, UIWidget_t769069560 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UITextList::ilo_get_scrollHeight9(UITextList)
extern "C"  int32_t UITextList_ilo_get_scrollHeight9_m925606326 (Il2CppObject * __this /* static, unused */, UITextList_t736798239 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UITextList::ilo_get_lineHeight10(UITextList)
extern "C"  float UITextList_ilo_get_lineHeight10_m4088665899 (Il2CppObject * __this /* static, unused */, UITextList_t736798239 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITextList::ilo_Rebuild11(UITextList)
extern "C"  void UITextList_ilo_Rebuild11_m3988052131 (Il2CppObject * __this /* static, unused */, UITextList_t736798239 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITextList::ilo_set_text12(UILabel,System.String)
extern "C"  void UITextList_ilo_set_text12_m3932866344 (Il2CppObject * __this /* static, unused */, UILabel_t291504320 * ____this0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
