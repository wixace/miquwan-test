﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Byte[]
struct ByteU5BU5D_t4260760469;

#include "codegen/il2cpp-codegen.h"

// System.Void Pathfinding.Ionic.Zlib.Adler::.cctor()
extern "C"  void Adler__cctor_m2876908366 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Pathfinding.Ionic.Zlib.Adler::Adler32(System.UInt32,System.Byte[],System.Int32,System.Int32)
extern "C"  uint32_t Adler_Adler32_m1181431462 (Il2CppObject * __this /* static, unused */, uint32_t ___adler0, ByteU5BU5D_t4260760469* ___buf1, int32_t ___index2, int32_t ___len3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
