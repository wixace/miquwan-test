﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CSGameDataMgr
struct CSGameDataMgr_t2623305516;

#include "codegen/il2cpp-codegen.h"

// System.Void CSGameDataMgr::.ctor()
extern "C"  void CSGameDataMgr__ctor_m2450734015 (CSGameDataMgr_t2623305516 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
