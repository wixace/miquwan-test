﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_voboWata162
struct  M_voboWata162_t2402556464  : public Il2CppObject
{
public:
	// System.UInt32 GarbageiOS.M_voboWata162::_habe
	uint32_t ____habe_0;
	// System.Boolean GarbageiOS.M_voboWata162::_dooqife
	bool ____dooqife_1;
	// System.UInt32 GarbageiOS.M_voboWata162::_fosorle
	uint32_t ____fosorle_2;
	// System.UInt32 GarbageiOS.M_voboWata162::_dootairGayra
	uint32_t ____dootairGayra_3;
	// System.Int32 GarbageiOS.M_voboWata162::_lacunu
	int32_t ____lacunu_4;
	// System.Boolean GarbageiOS.M_voboWata162::_werecuDrouwer
	bool ____werecuDrouwer_5;
	// System.Int32 GarbageiOS.M_voboWata162::_whicas
	int32_t ____whicas_6;
	// System.UInt32 GarbageiOS.M_voboWata162::_lurfassairTurnela
	uint32_t ____lurfassairTurnela_7;
	// System.Int32 GarbageiOS.M_voboWata162::_couboosa
	int32_t ____couboosa_8;
	// System.Int32 GarbageiOS.M_voboWata162::_kaitanai
	int32_t ____kaitanai_9;
	// System.Int32 GarbageiOS.M_voboWata162::_ceanel
	int32_t ____ceanel_10;
	// System.String GarbageiOS.M_voboWata162::_yalldelsu
	String_t* ____yalldelsu_11;
	// System.Int32 GarbageiOS.M_voboWata162::_resoofi
	int32_t ____resoofi_12;
	// System.Single GarbageiOS.M_voboWata162::_wermourel
	float ____wermourel_13;
	// System.UInt32 GarbageiOS.M_voboWata162::_vemeMure
	uint32_t ____vemeMure_14;
	// System.UInt32 GarbageiOS.M_voboWata162::_pudouRaras
	uint32_t ____pudouRaras_15;
	// System.UInt32 GarbageiOS.M_voboWata162::_kafarturHowdra
	uint32_t ____kafarturHowdra_16;
	// System.Int32 GarbageiOS.M_voboWata162::_betrirray
	int32_t ____betrirray_17;
	// System.Boolean GarbageiOS.M_voboWata162::_demaweeFuwoura
	bool ____demaweeFuwoura_18;
	// System.String GarbageiOS.M_voboWata162::_nistoHairnoo
	String_t* ____nistoHairnoo_19;
	// System.UInt32 GarbageiOS.M_voboWata162::_demnair
	uint32_t ____demnair_20;
	// System.UInt32 GarbageiOS.M_voboWata162::_pucascoPoosorou
	uint32_t ____pucascoPoosorou_21;

public:
	inline static int32_t get_offset_of__habe_0() { return static_cast<int32_t>(offsetof(M_voboWata162_t2402556464, ____habe_0)); }
	inline uint32_t get__habe_0() const { return ____habe_0; }
	inline uint32_t* get_address_of__habe_0() { return &____habe_0; }
	inline void set__habe_0(uint32_t value)
	{
		____habe_0 = value;
	}

	inline static int32_t get_offset_of__dooqife_1() { return static_cast<int32_t>(offsetof(M_voboWata162_t2402556464, ____dooqife_1)); }
	inline bool get__dooqife_1() const { return ____dooqife_1; }
	inline bool* get_address_of__dooqife_1() { return &____dooqife_1; }
	inline void set__dooqife_1(bool value)
	{
		____dooqife_1 = value;
	}

	inline static int32_t get_offset_of__fosorle_2() { return static_cast<int32_t>(offsetof(M_voboWata162_t2402556464, ____fosorle_2)); }
	inline uint32_t get__fosorle_2() const { return ____fosorle_2; }
	inline uint32_t* get_address_of__fosorle_2() { return &____fosorle_2; }
	inline void set__fosorle_2(uint32_t value)
	{
		____fosorle_2 = value;
	}

	inline static int32_t get_offset_of__dootairGayra_3() { return static_cast<int32_t>(offsetof(M_voboWata162_t2402556464, ____dootairGayra_3)); }
	inline uint32_t get__dootairGayra_3() const { return ____dootairGayra_3; }
	inline uint32_t* get_address_of__dootairGayra_3() { return &____dootairGayra_3; }
	inline void set__dootairGayra_3(uint32_t value)
	{
		____dootairGayra_3 = value;
	}

	inline static int32_t get_offset_of__lacunu_4() { return static_cast<int32_t>(offsetof(M_voboWata162_t2402556464, ____lacunu_4)); }
	inline int32_t get__lacunu_4() const { return ____lacunu_4; }
	inline int32_t* get_address_of__lacunu_4() { return &____lacunu_4; }
	inline void set__lacunu_4(int32_t value)
	{
		____lacunu_4 = value;
	}

	inline static int32_t get_offset_of__werecuDrouwer_5() { return static_cast<int32_t>(offsetof(M_voboWata162_t2402556464, ____werecuDrouwer_5)); }
	inline bool get__werecuDrouwer_5() const { return ____werecuDrouwer_5; }
	inline bool* get_address_of__werecuDrouwer_5() { return &____werecuDrouwer_5; }
	inline void set__werecuDrouwer_5(bool value)
	{
		____werecuDrouwer_5 = value;
	}

	inline static int32_t get_offset_of__whicas_6() { return static_cast<int32_t>(offsetof(M_voboWata162_t2402556464, ____whicas_6)); }
	inline int32_t get__whicas_6() const { return ____whicas_6; }
	inline int32_t* get_address_of__whicas_6() { return &____whicas_6; }
	inline void set__whicas_6(int32_t value)
	{
		____whicas_6 = value;
	}

	inline static int32_t get_offset_of__lurfassairTurnela_7() { return static_cast<int32_t>(offsetof(M_voboWata162_t2402556464, ____lurfassairTurnela_7)); }
	inline uint32_t get__lurfassairTurnela_7() const { return ____lurfassairTurnela_7; }
	inline uint32_t* get_address_of__lurfassairTurnela_7() { return &____lurfassairTurnela_7; }
	inline void set__lurfassairTurnela_7(uint32_t value)
	{
		____lurfassairTurnela_7 = value;
	}

	inline static int32_t get_offset_of__couboosa_8() { return static_cast<int32_t>(offsetof(M_voboWata162_t2402556464, ____couboosa_8)); }
	inline int32_t get__couboosa_8() const { return ____couboosa_8; }
	inline int32_t* get_address_of__couboosa_8() { return &____couboosa_8; }
	inline void set__couboosa_8(int32_t value)
	{
		____couboosa_8 = value;
	}

	inline static int32_t get_offset_of__kaitanai_9() { return static_cast<int32_t>(offsetof(M_voboWata162_t2402556464, ____kaitanai_9)); }
	inline int32_t get__kaitanai_9() const { return ____kaitanai_9; }
	inline int32_t* get_address_of__kaitanai_9() { return &____kaitanai_9; }
	inline void set__kaitanai_9(int32_t value)
	{
		____kaitanai_9 = value;
	}

	inline static int32_t get_offset_of__ceanel_10() { return static_cast<int32_t>(offsetof(M_voboWata162_t2402556464, ____ceanel_10)); }
	inline int32_t get__ceanel_10() const { return ____ceanel_10; }
	inline int32_t* get_address_of__ceanel_10() { return &____ceanel_10; }
	inline void set__ceanel_10(int32_t value)
	{
		____ceanel_10 = value;
	}

	inline static int32_t get_offset_of__yalldelsu_11() { return static_cast<int32_t>(offsetof(M_voboWata162_t2402556464, ____yalldelsu_11)); }
	inline String_t* get__yalldelsu_11() const { return ____yalldelsu_11; }
	inline String_t** get_address_of__yalldelsu_11() { return &____yalldelsu_11; }
	inline void set__yalldelsu_11(String_t* value)
	{
		____yalldelsu_11 = value;
		Il2CppCodeGenWriteBarrier(&____yalldelsu_11, value);
	}

	inline static int32_t get_offset_of__resoofi_12() { return static_cast<int32_t>(offsetof(M_voboWata162_t2402556464, ____resoofi_12)); }
	inline int32_t get__resoofi_12() const { return ____resoofi_12; }
	inline int32_t* get_address_of__resoofi_12() { return &____resoofi_12; }
	inline void set__resoofi_12(int32_t value)
	{
		____resoofi_12 = value;
	}

	inline static int32_t get_offset_of__wermourel_13() { return static_cast<int32_t>(offsetof(M_voboWata162_t2402556464, ____wermourel_13)); }
	inline float get__wermourel_13() const { return ____wermourel_13; }
	inline float* get_address_of__wermourel_13() { return &____wermourel_13; }
	inline void set__wermourel_13(float value)
	{
		____wermourel_13 = value;
	}

	inline static int32_t get_offset_of__vemeMure_14() { return static_cast<int32_t>(offsetof(M_voboWata162_t2402556464, ____vemeMure_14)); }
	inline uint32_t get__vemeMure_14() const { return ____vemeMure_14; }
	inline uint32_t* get_address_of__vemeMure_14() { return &____vemeMure_14; }
	inline void set__vemeMure_14(uint32_t value)
	{
		____vemeMure_14 = value;
	}

	inline static int32_t get_offset_of__pudouRaras_15() { return static_cast<int32_t>(offsetof(M_voboWata162_t2402556464, ____pudouRaras_15)); }
	inline uint32_t get__pudouRaras_15() const { return ____pudouRaras_15; }
	inline uint32_t* get_address_of__pudouRaras_15() { return &____pudouRaras_15; }
	inline void set__pudouRaras_15(uint32_t value)
	{
		____pudouRaras_15 = value;
	}

	inline static int32_t get_offset_of__kafarturHowdra_16() { return static_cast<int32_t>(offsetof(M_voboWata162_t2402556464, ____kafarturHowdra_16)); }
	inline uint32_t get__kafarturHowdra_16() const { return ____kafarturHowdra_16; }
	inline uint32_t* get_address_of__kafarturHowdra_16() { return &____kafarturHowdra_16; }
	inline void set__kafarturHowdra_16(uint32_t value)
	{
		____kafarturHowdra_16 = value;
	}

	inline static int32_t get_offset_of__betrirray_17() { return static_cast<int32_t>(offsetof(M_voboWata162_t2402556464, ____betrirray_17)); }
	inline int32_t get__betrirray_17() const { return ____betrirray_17; }
	inline int32_t* get_address_of__betrirray_17() { return &____betrirray_17; }
	inline void set__betrirray_17(int32_t value)
	{
		____betrirray_17 = value;
	}

	inline static int32_t get_offset_of__demaweeFuwoura_18() { return static_cast<int32_t>(offsetof(M_voboWata162_t2402556464, ____demaweeFuwoura_18)); }
	inline bool get__demaweeFuwoura_18() const { return ____demaweeFuwoura_18; }
	inline bool* get_address_of__demaweeFuwoura_18() { return &____demaweeFuwoura_18; }
	inline void set__demaweeFuwoura_18(bool value)
	{
		____demaweeFuwoura_18 = value;
	}

	inline static int32_t get_offset_of__nistoHairnoo_19() { return static_cast<int32_t>(offsetof(M_voboWata162_t2402556464, ____nistoHairnoo_19)); }
	inline String_t* get__nistoHairnoo_19() const { return ____nistoHairnoo_19; }
	inline String_t** get_address_of__nistoHairnoo_19() { return &____nistoHairnoo_19; }
	inline void set__nistoHairnoo_19(String_t* value)
	{
		____nistoHairnoo_19 = value;
		Il2CppCodeGenWriteBarrier(&____nistoHairnoo_19, value);
	}

	inline static int32_t get_offset_of__demnair_20() { return static_cast<int32_t>(offsetof(M_voboWata162_t2402556464, ____demnair_20)); }
	inline uint32_t get__demnair_20() const { return ____demnair_20; }
	inline uint32_t* get_address_of__demnair_20() { return &____demnair_20; }
	inline void set__demnair_20(uint32_t value)
	{
		____demnair_20 = value;
	}

	inline static int32_t get_offset_of__pucascoPoosorou_21() { return static_cast<int32_t>(offsetof(M_voboWata162_t2402556464, ____pucascoPoosorou_21)); }
	inline uint32_t get__pucascoPoosorou_21() const { return ____pucascoPoosorou_21; }
	inline uint32_t* get_address_of__pucascoPoosorou_21() { return &____pucascoPoosorou_21; }
	inline void set__pucascoPoosorou_21(uint32_t value)
	{
		____pucascoPoosorou_21 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
