﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_GUIStyleGenerated
struct UnityEngine_GUIStyleGenerated_t1248754833;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"

// System.Void UnityEngine_GUIStyleGenerated::.ctor()
extern "C"  void UnityEngine_GUIStyleGenerated__ctor_m1471260666 (UnityEngine_GUIStyleGenerated_t1248754833 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIStyleGenerated::GUIStyle_GUIStyle1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIStyleGenerated_GUIStyle_GUIStyle1_m2412763896 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIStyleGenerated::GUIStyle_GUIStyle2(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIStyleGenerated_GUIStyle_GUIStyle2_m3657528377 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUIStyleGenerated::GUIStyle_normal(JSVCall)
extern "C"  void UnityEngine_GUIStyleGenerated_GUIStyle_normal_m3813024831 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUIStyleGenerated::GUIStyle_hover(JSVCall)
extern "C"  void UnityEngine_GUIStyleGenerated_GUIStyle_hover_m1262219274 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUIStyleGenerated::GUIStyle_active(JSVCall)
extern "C"  void UnityEngine_GUIStyleGenerated_GUIStyle_active_m62674176 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUIStyleGenerated::GUIStyle_onNormal(JSVCall)
extern "C"  void UnityEngine_GUIStyleGenerated_GUIStyle_onNormal_m2762245216 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUIStyleGenerated::GUIStyle_onHover(JSVCall)
extern "C"  void UnityEngine_GUIStyleGenerated_GUIStyle_onHover_m812681161 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUIStyleGenerated::GUIStyle_onActive(JSVCall)
extern "C"  void UnityEngine_GUIStyleGenerated_GUIStyle_onActive_m3306861857 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUIStyleGenerated::GUIStyle_focused(JSVCall)
extern "C"  void UnityEngine_GUIStyleGenerated_GUIStyle_focused_m2458415151 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUIStyleGenerated::GUIStyle_onFocused(JSVCall)
extern "C"  void UnityEngine_GUIStyleGenerated_GUIStyle_onFocused_m4243985454 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUIStyleGenerated::GUIStyle_border(JSVCall)
extern "C"  void UnityEngine_GUIStyleGenerated_GUIStyle_border_m1963575962 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUIStyleGenerated::GUIStyle_margin(JSVCall)
extern "C"  void UnityEngine_GUIStyleGenerated_GUIStyle_margin_m2430094168 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUIStyleGenerated::GUIStyle_padding(JSVCall)
extern "C"  void UnityEngine_GUIStyleGenerated_GUIStyle_padding_m1331416277 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUIStyleGenerated::GUIStyle_overflow(JSVCall)
extern "C"  void UnityEngine_GUIStyleGenerated_GUIStyle_overflow_m3612806020 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUIStyleGenerated::GUIStyle_font(JSVCall)
extern "C"  void UnityEngine_GUIStyleGenerated_GUIStyle_font_m33593815 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUIStyleGenerated::GUIStyle_lineHeight(JSVCall)
extern "C"  void UnityEngine_GUIStyleGenerated_GUIStyle_lineHeight_m1417468267 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUIStyleGenerated::GUIStyle_none(JSVCall)
extern "C"  void UnityEngine_GUIStyleGenerated_GUIStyle_none_m634072334 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUIStyleGenerated::GUIStyle_isHeightDependantOnWidth(JSVCall)
extern "C"  void UnityEngine_GUIStyleGenerated_GUIStyle_isHeightDependantOnWidth_m1546599977 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUIStyleGenerated::GUIStyle_name(JSVCall)
extern "C"  void UnityEngine_GUIStyleGenerated_GUIStyle_name_m623865627 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUIStyleGenerated::GUIStyle_imagePosition(JSVCall)
extern "C"  void UnityEngine_GUIStyleGenerated_GUIStyle_imagePosition_m3480064674 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUIStyleGenerated::GUIStyle_alignment(JSVCall)
extern "C"  void UnityEngine_GUIStyleGenerated_GUIStyle_alignment_m4179084099 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUIStyleGenerated::GUIStyle_wordWrap(JSVCall)
extern "C"  void UnityEngine_GUIStyleGenerated_GUIStyle_wordWrap_m3383028050 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUIStyleGenerated::GUIStyle_clipping(JSVCall)
extern "C"  void UnityEngine_GUIStyleGenerated_GUIStyle_clipping_m1139645124 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUIStyleGenerated::GUIStyle_contentOffset(JSVCall)
extern "C"  void UnityEngine_GUIStyleGenerated_GUIStyle_contentOffset_m3023620314 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUIStyleGenerated::GUIStyle_fixedWidth(JSVCall)
extern "C"  void UnityEngine_GUIStyleGenerated_GUIStyle_fixedWidth_m2469525204 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUIStyleGenerated::GUIStyle_fixedHeight(JSVCall)
extern "C"  void UnityEngine_GUIStyleGenerated_GUIStyle_fixedHeight_m4196280939 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUIStyleGenerated::GUIStyle_stretchWidth(JSVCall)
extern "C"  void UnityEngine_GUIStyleGenerated_GUIStyle_stretchWidth_m151254181 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUIStyleGenerated::GUIStyle_stretchHeight(JSVCall)
extern "C"  void UnityEngine_GUIStyleGenerated_GUIStyle_stretchHeight_m1049355962 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUIStyleGenerated::GUIStyle_fontSize(JSVCall)
extern "C"  void UnityEngine_GUIStyleGenerated_GUIStyle_fontSize_m816761430 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUIStyleGenerated::GUIStyle_fontStyle(JSVCall)
extern "C"  void UnityEngine_GUIStyleGenerated_GUIStyle_fontStyle_m2363969892 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUIStyleGenerated::GUIStyle_richText(JSVCall)
extern "C"  void UnityEngine_GUIStyleGenerated_GUIStyle_richText_m849848125 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIStyleGenerated::GUIStyle_CalcHeight__GUIContent__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIStyleGenerated_GUIStyle_CalcHeight__GUIContent__Single_m3548341127 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIStyleGenerated::GUIStyle_CalcMinMaxWidth__GUIContent__Single__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIStyleGenerated_GUIStyle_CalcMinMaxWidth__GUIContent__Single__Single_m94384426 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIStyleGenerated::GUIStyle_CalcScreenSize__Vector2(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIStyleGenerated_GUIStyle_CalcScreenSize__Vector2_m1136541066 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIStyleGenerated::GUIStyle_CalcSize__GUIContent(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIStyleGenerated_GUIStyle_CalcSize__GUIContent_m31025625 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIStyleGenerated::GUIStyle_Draw__Rect__String__Boolean__Boolean__Boolean__Boolean(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIStyleGenerated_GUIStyle_Draw__Rect__String__Boolean__Boolean__Boolean__Boolean_m4109209374 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIStyleGenerated::GUIStyle_Draw__Rect__Texture__Boolean__Boolean__Boolean__Boolean(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIStyleGenerated_GUIStyle_Draw__Rect__Texture__Boolean__Boolean__Boolean__Boolean_m1750557872 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIStyleGenerated::GUIStyle_Draw__Rect__GUIContent__Boolean__Boolean__Boolean__Boolean(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIStyleGenerated_GUIStyle_Draw__Rect__GUIContent__Boolean__Boolean__Boolean__Boolean_m3732604299 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIStyleGenerated::GUIStyle_Draw__Rect__Boolean__Boolean__Boolean__Boolean(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIStyleGenerated_GUIStyle_Draw__Rect__Boolean__Boolean__Boolean__Boolean_m1512178253 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIStyleGenerated::GUIStyle_Draw__Rect__GUIContent__Int32__Boolean(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIStyleGenerated_GUIStyle_Draw__Rect__GUIContent__Int32__Boolean_m2705103461 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIStyleGenerated::GUIStyle_Draw__Rect__GUIContent__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIStyleGenerated_GUIStyle_Draw__Rect__GUIContent__Int32_m2570839141 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIStyleGenerated::GUIStyle_DrawCursor__Rect__GUIContent__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIStyleGenerated_GUIStyle_DrawCursor__Rect__GUIContent__Int32__Int32_m2272026689 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIStyleGenerated::GUIStyle_DrawWithTextSelection__Rect__GUIContent__Int32__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIStyleGenerated_GUIStyle_DrawWithTextSelection__Rect__GUIContent__Int32__Int32__Int32_m1708083292 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIStyleGenerated::GUIStyle_GetCursorPixelPosition__Rect__GUIContent__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIStyleGenerated_GUIStyle_GetCursorPixelPosition__Rect__GUIContent__Int32_m3754572262 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIStyleGenerated::GUIStyle_GetCursorStringIndex__Rect__GUIContent__Vector2(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIStyleGenerated_GUIStyle_GetCursorStringIndex__Rect__GUIContent__Vector2_m2374792437 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIStyleGenerated::GUIStyle_ToString(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIStyleGenerated_GUIStyle_ToString_m3432087185 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIStyleGenerated::GUIStyle_op_Implicit__String_to_GUIStyle(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIStyleGenerated_GUIStyle_op_Implicit__String_to_GUIStyle_m1940037612 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUIStyleGenerated::__Register()
extern "C"  void UnityEngine_GUIStyleGenerated___Register_m3631989005 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_GUIStyleGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UnityEngine_GUIStyleGenerated_ilo_getObject1_m1921456892 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_GUIStyleGenerated::ilo_getObject2(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_GUIStyleGenerated_ilo_getObject2_m4226562412 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUIStyleGenerated::ilo_addJSCSRel3(System.Int32,System.Object)
extern "C"  void UnityEngine_GUIStyleGenerated_ilo_addJSCSRel3_m1614680760 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_GUIStyleGenerated::ilo_setObject4(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UnityEngine_GUIStyleGenerated_ilo_setObject4_m3950524727 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUIStyleGenerated::ilo_setBooleanS5(System.Int32,System.Boolean)
extern "C"  void UnityEngine_GUIStyleGenerated_ilo_setBooleanS5_m1244654951 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_GUIStyleGenerated::ilo_getEnum6(System.Int32)
extern "C"  int32_t UnityEngine_GUIStyleGenerated_ilo_getEnum6_m1318098495 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine_GUIStyleGenerated::ilo_getVector2S7(System.Int32)
extern "C"  Vector2_t4282066565  UnityEngine_GUIStyleGenerated_ilo_getVector2S7_m2225701904 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine_GUIStyleGenerated::ilo_getSingle8(System.Int32)
extern "C"  float UnityEngine_GUIStyleGenerated_ilo_getSingle8_m441091356 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_GUIStyleGenerated::ilo_getInt329(System.Int32)
extern "C"  int32_t UnityEngine_GUIStyleGenerated_ilo_getInt329_m885585189 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUIStyleGenerated::ilo_setSingle10(System.Int32,System.Single)
extern "C"  void UnityEngine_GUIStyleGenerated_ilo_setSingle10_m890823364 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUIStyleGenerated::ilo_setArgIndex11(System.Int32)
extern "C"  void UnityEngine_GUIStyleGenerated_ilo_setArgIndex11_m2075839156 (Il2CppObject * __this /* static, unused */, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUIStyleGenerated::ilo_setVector2S12(System.Int32,UnityEngine.Vector2)
extern "C"  void UnityEngine_GUIStyleGenerated_ilo_setVector2S12_m1404938417 (Il2CppObject * __this /* static, unused */, int32_t ___e0, Vector2_t4282066565  ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine_GUIStyleGenerated::ilo_getStringS13(System.Int32)
extern "C"  String_t* UnityEngine_GUIStyleGenerated_ilo_getStringS13_m367274169 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIStyleGenerated::ilo_getBooleanS14(System.Int32)
extern "C"  bool UnityEngine_GUIStyleGenerated_ilo_getBooleanS14_m11993902 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
