﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Bson.BsonBinaryWriter
struct BsonBinaryWriter_t2157484308;
// System.IO.Stream
struct Stream_t1561764144;
// Newtonsoft.Json.Bson.BsonToken
struct BsonToken_t455725415;
// System.String
struct String_t;
// System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.Bson.BsonProperty>
struct IEnumerator_1_t1910719086;
// Newtonsoft.Json.Bson.BsonObject
struct BsonObject_t2743735103;
// Newtonsoft.Json.Bson.BsonProperty
struct BsonProperty_t4293821333;
// System.Object
struct Il2CppObject;
// Newtonsoft.Json.Bson.BsonValue
struct BsonValue_t457156831;
// Newtonsoft.Json.Bson.BsonString
struct BsonString_t2875117585;
// Newtonsoft.Json.Bson.BsonRegex
struct BsonRegex_t453576629;
// System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.Bson.BsonToken>
struct IEnumerator_1_t2367590464;
// Newtonsoft.Json.Bson.BsonArray
struct BsonArray_t438274503;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IO_Stream1561764144.h"
#include "mscorlib_System_DateTimeKind1472618179.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Bson_BsonToken455725415.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Nullable_1_gen1237965023.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Bson_BsonBinaryW2157484308.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Bson_BsonObject2743735103.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Bson_BsonPropert4293821333.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Bson_BsonValue457156831.h"
#include "mscorlib_System_DateTime4283661327.h"
#include "mscorlib_System_TimeSpan413522987.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Bson_BsonRegex453576629.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Bson_BsonString2875117585.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Bson_BsonType2455132538.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Bson_BsonArray438274503.h"

// System.Void Newtonsoft.Json.Bson.BsonBinaryWriter::.ctor(System.IO.Stream)
extern "C"  void BsonBinaryWriter__ctor_m2590765781 (BsonBinaryWriter_t2157484308 * __this, Stream_t1561764144 * ___stream0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Bson.BsonBinaryWriter::.cctor()
extern "C"  void BsonBinaryWriter__cctor_m3574131503 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTimeKind Newtonsoft.Json.Bson.BsonBinaryWriter::get_DateTimeKindHandling()
extern "C"  int32_t BsonBinaryWriter_get_DateTimeKindHandling_m2296583198 (BsonBinaryWriter_t2157484308 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Bson.BsonBinaryWriter::set_DateTimeKindHandling(System.DateTimeKind)
extern "C"  void BsonBinaryWriter_set_DateTimeKindHandling_m3005845517 (BsonBinaryWriter_t2157484308 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Bson.BsonBinaryWriter::Flush()
extern "C"  void BsonBinaryWriter_Flush_m3124287328 (BsonBinaryWriter_t2157484308 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Bson.BsonBinaryWriter::Close()
extern "C"  void BsonBinaryWriter_Close_m456232276 (BsonBinaryWriter_t2157484308 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Bson.BsonBinaryWriter::WriteToken(Newtonsoft.Json.Bson.BsonToken)
extern "C"  void BsonBinaryWriter_WriteToken_m3710807552 (BsonBinaryWriter_t2157484308 * __this, BsonToken_t455725415 * ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Bson.BsonBinaryWriter::WriteTokenInternal(Newtonsoft.Json.Bson.BsonToken)
extern "C"  void BsonBinaryWriter_WriteTokenInternal_m3348649437 (BsonBinaryWriter_t2157484308 * __this, BsonToken_t455725415 * ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Bson.BsonBinaryWriter::WriteString(System.String,System.Int32,System.Nullable`1<System.Int32>)
extern "C"  void BsonBinaryWriter_WriteString_m1462600969 (BsonBinaryWriter_t2157484308 * __this, String_t* ___s0, int32_t ___byteCount1, Nullable_1_t1237965023  ___calculatedlengthPrefix2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Newtonsoft.Json.Bson.BsonBinaryWriter::CalculateSize(System.Int32)
extern "C"  int32_t BsonBinaryWriter_CalculateSize_m3040964738 (BsonBinaryWriter_t2157484308 * __this, int32_t ___stringByteCount0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Newtonsoft.Json.Bson.BsonBinaryWriter::CalculateSizeWithLength(System.Int32,System.Boolean)
extern "C"  int32_t BsonBinaryWriter_CalculateSizeWithLength_m3963235023 (BsonBinaryWriter_t2157484308 * __this, int32_t ___stringByteCount0, bool ___includeSize1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Newtonsoft.Json.Bson.BsonBinaryWriter::CalculateSize(Newtonsoft.Json.Bson.BsonToken)
extern "C"  int32_t BsonBinaryWriter_CalculateSize_m3429806737 (BsonBinaryWriter_t2157484308 * __this, BsonToken_t455725415 * ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Bson.BsonBinaryWriter::ilo_WriteTokenInternal1(Newtonsoft.Json.Bson.BsonBinaryWriter,Newtonsoft.Json.Bson.BsonToken)
extern "C"  void BsonBinaryWriter_ilo_WriteTokenInternal1_m2151449508 (Il2CppObject * __this /* static, unused */, BsonBinaryWriter_t2157484308 * ____this0, BsonToken_t455725415 * ___t1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.Bson.BsonProperty> Newtonsoft.Json.Bson.BsonBinaryWriter::ilo_GetEnumerator2(Newtonsoft.Json.Bson.BsonObject)
extern "C"  Il2CppObject* BsonBinaryWriter_ilo_GetEnumerator2_m1304314111 (Il2CppObject * __this /* static, unused */, BsonObject_t2743735103 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Bson.BsonToken Newtonsoft.Json.Bson.BsonBinaryWriter::ilo_get_Value3(Newtonsoft.Json.Bson.BsonProperty)
extern "C"  BsonToken_t455725415 * BsonBinaryWriter_ilo_get_Value3_m3406909213 (Il2CppObject * __this /* static, unused */, BsonProperty_t4293821333 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Newtonsoft.Json.Bson.BsonBinaryWriter::ilo_get_CalculatedSize4(Newtonsoft.Json.Bson.BsonToken)
extern "C"  int32_t BsonBinaryWriter_ilo_get_CalculatedSize4_m1463235587 (Il2CppObject * __this /* static, unused */, BsonToken_t455725415 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Newtonsoft.Json.Bson.BsonBinaryWriter::ilo_IntLength5(System.Int32)
extern "C"  int32_t BsonBinaryWriter_ilo_IntLength5_m2325065436 (Il2CppObject * __this /* static, unused */, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Bson.BsonBinaryWriter::ilo_get_Value6(Newtonsoft.Json.Bson.BsonValue)
extern "C"  Il2CppObject * BsonBinaryWriter_ilo_get_Value6_m2657308794 (Il2CppObject * __this /* static, unused */, BsonValue_t457156831 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTimeKind Newtonsoft.Json.Bson.BsonBinaryWriter::ilo_get_DateTimeKindHandling7(Newtonsoft.Json.Bson.BsonBinaryWriter)
extern "C"  int32_t BsonBinaryWriter_ilo_get_DateTimeKindHandling7_m780872605 (Il2CppObject * __this /* static, unused */, BsonBinaryWriter_t2157484308 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 Newtonsoft.Json.Bson.BsonBinaryWriter::ilo_ConvertDateTimeToJavaScriptTicks8(System.DateTime,System.TimeSpan)
extern "C"  int64_t BsonBinaryWriter_ilo_ConvertDateTimeToJavaScriptTicks8_m293268702 (Il2CppObject * __this /* static, unused */, DateTime_t4283661327  ___dateTime0, TimeSpan_t413522987  ___offset1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Bson.BsonString Newtonsoft.Json.Bson.BsonBinaryWriter::ilo_get_Pattern9(Newtonsoft.Json.Bson.BsonRegex)
extern "C"  BsonString_t2875117585 * BsonBinaryWriter_ilo_get_Pattern9_m763236052 (Il2CppObject * __this /* static, unused */, BsonRegex_t453576629 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Bson.BsonBinaryWriter::ilo_WriteString10(Newtonsoft.Json.Bson.BsonBinaryWriter,System.String,System.Int32,System.Nullable`1<System.Int32>)
extern "C"  void BsonBinaryWriter_ilo_WriteString10_m2963855606 (Il2CppObject * __this /* static, unused */, BsonBinaryWriter_t2157484308 * ____this0, String_t* ___s1, int32_t ___byteCount2, Nullable_1_t1237965023  ___calculatedlengthPrefix3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Bson.BsonString Newtonsoft.Json.Bson.BsonBinaryWriter::ilo_get_Options11(Newtonsoft.Json.Bson.BsonRegex)
extern "C"  BsonString_t2875117585 * BsonBinaryWriter_ilo_get_Options11_m2004809913 (Il2CppObject * __this /* static, unused */, BsonRegex_t453576629 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Newtonsoft.Json.Bson.BsonBinaryWriter::ilo_get_ByteCount12(Newtonsoft.Json.Bson.BsonString)
extern "C"  int32_t BsonBinaryWriter_ilo_get_ByteCount12_m2432076210 (Il2CppObject * __this /* static, unused */, BsonString_t2875117585 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Bson.BsonType Newtonsoft.Json.Bson.BsonBinaryWriter::ilo_get_Type13(Newtonsoft.Json.Bson.BsonToken)
extern "C"  int8_t BsonBinaryWriter_ilo_get_Type13_m4190750590 (Il2CppObject * __this /* static, unused */, BsonToken_t455725415 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Bson.BsonString Newtonsoft.Json.Bson.BsonBinaryWriter::ilo_get_Name14(Newtonsoft.Json.Bson.BsonProperty)
extern "C"  BsonString_t2875117585 * BsonBinaryWriter_ilo_get_Name14_m942984765 (Il2CppObject * __this /* static, unused */, BsonProperty_t4293821333 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Newtonsoft.Json.Bson.BsonBinaryWriter::ilo_CalculateSize15(Newtonsoft.Json.Bson.BsonBinaryWriter,Newtonsoft.Json.Bson.BsonToken)
extern "C"  int32_t BsonBinaryWriter_ilo_CalculateSize15_m2285155587 (Il2CppObject * __this /* static, unused */, BsonBinaryWriter_t2157484308 * ____this0, BsonToken_t455725415 * ___t1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.Bson.BsonToken> Newtonsoft.Json.Bson.BsonBinaryWriter::ilo_GetEnumerator16(Newtonsoft.Json.Bson.BsonArray)
extern "C"  Il2CppObject* BsonBinaryWriter_ilo_GetEnumerator16_m1583517876 (Il2CppObject * __this /* static, unused */, BsonArray_t438274503 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Bson.BsonBinaryWriter::ilo_set_CalculatedSize17(Newtonsoft.Json.Bson.BsonToken,System.Int32)
extern "C"  void BsonBinaryWriter_ilo_set_CalculatedSize17_m4054043292 (Il2CppObject * __this /* static, unused */, BsonToken_t455725415 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Bson.BsonBinaryWriter::ilo_get_IncludeLength18(Newtonsoft.Json.Bson.BsonString)
extern "C"  bool BsonBinaryWriter_ilo_get_IncludeLength18_m425228639 (Il2CppObject * __this /* static, unused */, BsonString_t2875117585 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
