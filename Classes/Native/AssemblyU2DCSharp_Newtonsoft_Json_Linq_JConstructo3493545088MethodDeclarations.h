﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Linq.JConstructor
struct JConstructor_t3493545088;
// System.String
struct String_t;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.IList`1<Newtonsoft.Json.Linq.JToken>
struct IList_1_t1811925858;
// Newtonsoft.Json.Linq.JToken
struct JToken_t3412245951;
// Newtonsoft.Json.JsonWriter
struct JsonWriter_t972330355;
// Newtonsoft.Json.JsonConverter[]
struct JsonConverterU5BU5D_t638349667;
// Newtonsoft.Json.JsonReader
struct JsonReader_t816925123;
// Newtonsoft.Json.Linq.JContainer
struct JContainer_t3364442311;
// Newtonsoft.Json.IJsonLineInfo
struct IJsonLineInfo_t1008519681;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JConstructo3493545088.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JTokenType3916897561.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JToken3412245951.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonWriter972330355.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonReader816925123.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JContainer3364442311.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonToken4173078175.h"

// System.Void Newtonsoft.Json.Linq.JConstructor::.ctor()
extern "C"  void JConstructor__ctor_m3946107906 (JConstructor_t3493545088 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JConstructor::.ctor(Newtonsoft.Json.Linq.JConstructor)
extern "C"  void JConstructor__ctor_m1250314503 (JConstructor_t3493545088 * __this, JConstructor_t3493545088 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JConstructor::.ctor(System.String,System.Object[])
extern "C"  void JConstructor__ctor_m1890317484 (JConstructor_t3493545088 * __this, String_t* ___name0, ObjectU5BU5D_t1108656482* ___content1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JConstructor::.ctor(System.String,System.Object)
extern "C"  void JConstructor__ctor_m1628780814 (JConstructor_t3493545088 * __this, String_t* ___name0, Il2CppObject * ___content1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JConstructor::.ctor(System.String)
extern "C"  void JConstructor__ctor_m778100288 (JConstructor_t3493545088 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IList`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JConstructor::get_ChildrenTokens()
extern "C"  Il2CppObject* JConstructor_get_ChildrenTokens_m2882883162 (JConstructor_t3493545088 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Linq.JConstructor::get_Name()
extern "C"  String_t* JConstructor_get_Name_m3112763411 (JConstructor_t3493545088 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JConstructor::set_Name(System.String)
extern "C"  void JConstructor_set_Name_m873303160 (JConstructor_t3493545088 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JTokenType Newtonsoft.Json.Linq.JConstructor::get_Type()
extern "C"  int32_t JConstructor_get_Type_m3771393460 (JConstructor_t3493545088 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Linq.JConstructor::DeepEquals(Newtonsoft.Json.Linq.JToken)
extern "C"  bool JConstructor_DeepEquals_m5817969 (JConstructor_t3493545088 * __this, JToken_t3412245951 * ___node0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JConstructor::CloneToken()
extern "C"  JToken_t3412245951 * JConstructor_CloneToken_m428555955 (JConstructor_t3493545088 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JConstructor::WriteTo(Newtonsoft.Json.JsonWriter,Newtonsoft.Json.JsonConverter[])
extern "C"  void JConstructor_WriteTo_m1472933265 (JConstructor_t3493545088 * __this, JsonWriter_t972330355 * ___writer0, JsonConverterU5BU5D_t638349667* ___converters1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JConstructor::get_Item(System.Object)
extern "C"  JToken_t3412245951 * JConstructor_get_Item_m2959641729 (JConstructor_t3493545088 * __this, Il2CppObject * ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JConstructor::set_Item(System.Object,Newtonsoft.Json.Linq.JToken)
extern "C"  void JConstructor_set_Item_m2849370214 (JConstructor_t3493545088 * __this, Il2CppObject * ___key0, JToken_t3412245951 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Newtonsoft.Json.Linq.JConstructor::GetDeepHashCode()
extern "C"  int32_t JConstructor_GetDeepHashCode_m1003107787 (JConstructor_t3493545088 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JConstructor Newtonsoft.Json.Linq.JConstructor::Load(Newtonsoft.Json.JsonReader)
extern "C"  JConstructor_t3493545088 * JConstructor_Load_m679228766 (Il2CppObject * __this /* static, unused */, JsonReader_t816925123 * ___reader0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JConstructor::ilo_GetItem1(Newtonsoft.Json.Linq.JContainer,System.Int32)
extern "C"  JToken_t3412245951 * JConstructor_ilo_GetItem1_m2972102983 (Il2CppObject * __this /* static, unused */, JContainer_t3364442311 * ____this0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Linq.JConstructor::ilo_ToString2(System.Object)
extern "C"  String_t* JConstructor_ilo_ToString2_m4093390366 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JConstructor::ilo_SetItem3(Newtonsoft.Json.Linq.JContainer,System.Int32,Newtonsoft.Json.Linq.JToken)
extern "C"  void JConstructor_ilo_SetItem3_m1900772620 (Il2CppObject * __this /* static, unused */, JContainer_t3364442311 * ____this0, int32_t ___index1, JToken_t3412245951 * ___item2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.JsonToken Newtonsoft.Json.Linq.JConstructor::ilo_get_TokenType4(Newtonsoft.Json.JsonReader)
extern "C"  int32_t JConstructor_ilo_get_TokenType4_m4095617448 (Il2CppObject * __this /* static, unused */, JsonReader_t816925123 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Linq.JConstructor::ilo_get_Value5(Newtonsoft.Json.JsonReader)
extern "C"  Il2CppObject * JConstructor_ilo_get_Value5_m2149895469 (Il2CppObject * __this /* static, unused */, JsonReader_t816925123 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JConstructor::ilo_SetLineInfo6(Newtonsoft.Json.Linq.JToken,Newtonsoft.Json.IJsonLineInfo)
extern "C"  void JConstructor_ilo_SetLineInfo6_m1411388529 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ____this0, Il2CppObject * ___lineInfo1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
