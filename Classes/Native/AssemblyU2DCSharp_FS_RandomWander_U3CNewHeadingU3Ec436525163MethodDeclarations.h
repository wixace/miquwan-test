﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FS_RandomWander/<NewHeading>c__Iterator18
struct U3CNewHeadingU3Ec__Iterator18_t436525163;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void FS_RandomWander/<NewHeading>c__Iterator18::.ctor()
extern "C"  void U3CNewHeadingU3Ec__Iterator18__ctor_m3222136160 (U3CNewHeadingU3Ec__Iterator18_t436525163 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object FS_RandomWander/<NewHeading>c__Iterator18::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CNewHeadingU3Ec__Iterator18_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2982256690 (U3CNewHeadingU3Ec__Iterator18_t436525163 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object FS_RandomWander/<NewHeading>c__Iterator18::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CNewHeadingU3Ec__Iterator18_System_Collections_IEnumerator_get_Current_m3872278470 (U3CNewHeadingU3Ec__Iterator18_t436525163 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FS_RandomWander/<NewHeading>c__Iterator18::MoveNext()
extern "C"  bool U3CNewHeadingU3Ec__Iterator18_MoveNext_m1778658708 (U3CNewHeadingU3Ec__Iterator18_t436525163 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FS_RandomWander/<NewHeading>c__Iterator18::Dispose()
extern "C"  void U3CNewHeadingU3Ec__Iterator18_Dispose_m3994679709 (U3CNewHeadingU3Ec__Iterator18_t436525163 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FS_RandomWander/<NewHeading>c__Iterator18::Reset()
extern "C"  void U3CNewHeadingU3Ec__Iterator18_Reset_m868569101 (U3CNewHeadingU3Ec__Iterator18_t436525163 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
