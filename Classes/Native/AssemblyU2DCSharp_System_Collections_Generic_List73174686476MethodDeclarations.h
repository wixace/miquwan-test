﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System_Collections_Generic_List77Generated/<ListA1_FindLast_GetDelegate_member18_arg0>c__AnonStorey89`1<System.Object>
struct U3CListA1_FindLast_GetDelegate_member18_arg0U3Ec__AnonStorey89_1_t3174686476;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System_Collections_Generic_List77Generated/<ListA1_FindLast_GetDelegate_member18_arg0>c__AnonStorey89`1<System.Object>::.ctor()
extern "C"  void U3CListA1_FindLast_GetDelegate_member18_arg0U3Ec__AnonStorey89_1__ctor_m2724864499_gshared (U3CListA1_FindLast_GetDelegate_member18_arg0U3Ec__AnonStorey89_1_t3174686476 * __this, const MethodInfo* method);
#define U3CListA1_FindLast_GetDelegate_member18_arg0U3Ec__AnonStorey89_1__ctor_m2724864499(__this, method) ((  void (*) (U3CListA1_FindLast_GetDelegate_member18_arg0U3Ec__AnonStorey89_1_t3174686476 *, const MethodInfo*))U3CListA1_FindLast_GetDelegate_member18_arg0U3Ec__AnonStorey89_1__ctor_m2724864499_gshared)(__this, method)
// System.Boolean System_Collections_Generic_List77Generated/<ListA1_FindLast_GetDelegate_member18_arg0>c__AnonStorey89`1<System.Object>::<>m__AE(T)
extern "C"  bool U3CListA1_FindLast_GetDelegate_member18_arg0U3Ec__AnonStorey89_1_U3CU3Em__AE_m763121650_gshared (U3CListA1_FindLast_GetDelegate_member18_arg0U3Ec__AnonStorey89_1_t3174686476 * __this, Il2CppObject * ___obj0, const MethodInfo* method);
#define U3CListA1_FindLast_GetDelegate_member18_arg0U3Ec__AnonStorey89_1_U3CU3Em__AE_m763121650(__this, ___obj0, method) ((  bool (*) (U3CListA1_FindLast_GetDelegate_member18_arg0U3Ec__AnonStorey89_1_t3174686476 *, Il2CppObject *, const MethodInfo*))U3CListA1_FindLast_GetDelegate_member18_arg0U3Ec__AnonStorey89_1_U3CU3Em__AE_m763121650_gshared)(__this, ___obj0, method)
