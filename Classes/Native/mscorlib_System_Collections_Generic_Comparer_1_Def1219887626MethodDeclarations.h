﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Bounds>
struct DefaultComparer_t1219887626;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Bounds2711641849.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Bounds>::.ctor()
extern "C"  void DefaultComparer__ctor_m3527831090_gshared (DefaultComparer_t1219887626 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m3527831090(__this, method) ((  void (*) (DefaultComparer_t1219887626 *, const MethodInfo*))DefaultComparer__ctor_m3527831090_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Bounds>::Compare(T,T)
extern "C"  int32_t DefaultComparer_Compare_m3564029061_gshared (DefaultComparer_t1219887626 * __this, Bounds_t2711641849  ___x0, Bounds_t2711641849  ___y1, const MethodInfo* method);
#define DefaultComparer_Compare_m3564029061(__this, ___x0, ___y1, method) ((  int32_t (*) (DefaultComparer_t1219887626 *, Bounds_t2711641849 , Bounds_t2711641849 , const MethodInfo*))DefaultComparer_Compare_m3564029061_gshared)(__this, ___x0, ___y1, method)
