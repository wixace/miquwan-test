﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1244034627MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.Texture>::.ctor()
#define List_1__ctor_m277519267(__this, method) ((  void (*) (List_1_t3894644513 *, const MethodInfo*))List_1__ctor_m1239231859_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Texture>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m769016133(__this, ___collection0, method) ((  void (*) (List_1_t3894644513 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m1160795371_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Texture>::.ctor(System.Int32)
#define List_1__ctor_m96887115(__this, ___capacity0, method) ((  void (*) (List_1_t3894644513 *, int32_t, const MethodInfo*))List_1__ctor_m3643386469_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Texture>::.cctor()
#define List_1__cctor_m4286502899(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m3826137881_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.Texture>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2409717452(__this, method) ((  Il2CppObject* (*) (List_1_t3894644513 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2808422246_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Texture>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m829333386(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t3894644513 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m4034025648_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.Texture>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m2014534277(__this, method) ((  Il2CppObject * (*) (List_1_t3894644513 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m1841330603_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Texture>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m3457388812(__this, ___item0, method) ((  int32_t (*) (List_1_t3894644513 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m3794749222_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Texture>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m2113378752(__this, ___item0, method) ((  bool (*) (List_1_t3894644513 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m2659633254_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Texture>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m936141156(__this, ___item0, method) ((  int32_t (*) (List_1_t3894644513 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m3431692926_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Texture>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m307320463(__this, ___index0, ___item1, method) ((  void (*) (List_1_t3894644513 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m2067529129_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Texture>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m720229945(__this, ___item0, method) ((  void (*) (List_1_t3894644513 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m1644145887_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Texture>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2940099009(__this, method) ((  bool (*) (List_1_t3894644513 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1299706087_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Texture>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m4030752156(__this, method) ((  bool (*) (List_1_t3894644513 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m3867536694_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.Texture>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m3010144968(__this, method) ((  Il2CppObject * (*) (List_1_t3894644513 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m4244374434_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Texture>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m1294309679(__this, method) ((  bool (*) (List_1_t3894644513 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m432946261_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Texture>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m80118762(__this, method) ((  bool (*) (List_1_t3894644513 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m2961826820_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.Texture>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m3301935695(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t3894644513 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m3985478825_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Texture>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m3891141286(__this, ___index0, ___value1, method) ((  void (*) (List_1_t3894644513 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m3234554688_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Texture>::Add(T)
#define List_1_Add_m4266847379(__this, ___item0, method) ((  void (*) (List_1_t3894644513 *, Texture_t2526458961 *, const MethodInfo*))List_1_Add_m933592675_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Texture>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m198361088(__this, ___newCount0, method) ((  void (*) (List_1_t3894644513 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m4122600870_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Texture>::CheckRange(System.Int32,System.Int32)
#define List_1_CheckRange_m3378471911(__this, ___idx0, ___count1, method) ((  void (*) (List_1_t3894644513 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m590371457_gshared)(__this, ___idx0, ___count1, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Texture>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m1036195582(__this, ___collection0, method) ((  void (*) (List_1_t3894644513 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m2478449828_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Texture>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m297167806(__this, ___enumerable0, method) ((  void (*) (List_1_t3894644513 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m1739422052_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Texture>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m160015385(__this, ___collection0, method) ((  void (*) (List_1_t3894644513 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m2229151411_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.Texture>::AsReadOnly()
#define List_1_AsReadOnly_m1564371056(__this, method) ((  ReadOnlyCollection_1_t4083536497 * (*) (List_1_t3894644513 *, const MethodInfo*))List_1_AsReadOnly_m769820182_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Texture>::BinarySearch(T)
#define List_1_BinarySearch_m3346847567(__this, ___item0, method) ((  int32_t (*) (List_1_t3894644513 *, Texture_t2526458961 *, const MethodInfo*))List_1_BinarySearch_m2761349225_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Texture>::Clear()
#define List_1_Clear_m2132021029(__this, method) ((  void (*) (List_1_t3894644513 *, const MethodInfo*))List_1_Clear_m2940332446_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Texture>::Contains(T)
#define List_1_Contains_m804916051(__this, ___item0, method) ((  bool (*) (List_1_t3894644513 *, Texture_t2526458961 *, const MethodInfo*))List_1_Contains_m4186092781_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Texture>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m1561929461(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t3894644513 *, TextureU5BU5D_t606176076*, int32_t, const MethodInfo*))List_1_CopyTo_m3988356635_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<UnityEngine.Texture>::Find(System.Predicate`1<T>)
#define List_1_Find_m810988563(__this, ___match0, method) ((  Texture_t2526458961 * (*) (List_1_t3894644513 *, Predicate_1_t2137515844 *, const MethodInfo*))List_1_Find_m3379773421_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Texture>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m1851587470(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t2137515844 *, const MethodInfo*))List_1_CheckMatch_m3390394152_gshared)(__this /* static, unused */, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Texture>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m2553308595(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t3894644513 *, int32_t, int32_t, Predicate_1_t2137515844 *, const MethodInfo*))List_1_GetIndex_m4275988045_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.Texture>::GetEnumerator()
#define List_1_GetEnumerator_m1850810128(__this, method) ((  Enumerator_t3914317283  (*) (List_1_t3894644513 *, const MethodInfo*))List_1_GetEnumerator_m1919240000_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Texture>::IndexOf(T)
#define List_1_IndexOf_m185978937(__this, ___item0, method) ((  int32_t (*) (List_1_t3894644513 *, Texture_t2526458961 *, const MethodInfo*))List_1_IndexOf_m2776798407_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Texture>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m2926131276(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t3894644513 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m3807054194_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Texture>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m1308296645(__this, ___index0, method) ((  void (*) (List_1_t3894644513 *, int32_t, const MethodInfo*))List_1_CheckIndex_m3734723819_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Texture>::Insert(System.Int32,T)
#define List_1_Insert_m1565423276(__this, ___index0, ___item1, method) ((  void (*) (List_1_t3894644513 *, int32_t, Texture_t2526458961 *, const MethodInfo*))List_1_Insert_m3427163986_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Texture>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m4173177377(__this, ___collection0, method) ((  void (*) (List_1_t3894644513 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m2905071175_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Texture>::Remove(T)
#define List_1_Remove_m80708622(__this, ___item0, method) ((  bool (*) (List_1_t3894644513 *, Texture_t2526458961 *, const MethodInfo*))List_1_Remove_m2747911208_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Texture>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m1811920700(__this, ___match0, method) ((  int32_t (*) (List_1_t3894644513 *, Predicate_1_t2137515844 *, const MethodInfo*))List_1_RemoveAll_m2933443938_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Texture>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m3734243442(__this, ___index0, method) ((  void (*) (List_1_t3894644513 *, int32_t, const MethodInfo*))List_1_RemoveAt_m2208877785_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Texture>::RemoveRange(System.Int32,System.Int32)
#define List_1_RemoveRange_m1388626069(__this, ___index0, ___count1, method) ((  void (*) (List_1_t3894644513 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m856857915_gshared)(__this, ___index0, ___count1, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Texture>::Reverse()
#define List_1_Reverse_m1835495610(__this, method) ((  void (*) (List_1_t3894644513 *, const MethodInfo*))List_1_Reverse_m449081940_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Texture>::Sort()
#define List_1_Sort_m2054035752(__this, method) ((  void (*) (List_1_t3894644513 *, const MethodInfo*))List_1_Sort_m1168641486_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Texture>::Sort(System.Collections.Generic.IComparer`1<T>)
#define List_1_Sort_m855087548(__this, ___comparer0, method) ((  void (*) (List_1_t3894644513 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m3726677974_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Texture>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m3979040635(__this, ___comparison0, method) ((  void (*) (List_1_t3894644513 *, Comparison_1_t1242820148 *, const MethodInfo*))List_1_Sort_m4192185249_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<UnityEngine.Texture>::ToArray()
#define List_1_ToArray_m2197801427(__this, method) ((  TextureU5BU5D_t606176076* (*) (List_1_t3894644513 *, const MethodInfo*))List_1_ToArray_m1449333879_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Texture>::TrimExcess()
#define List_1_TrimExcess_m400538305(__this, method) ((  void (*) (List_1_t3894644513 *, const MethodInfo*))List_1_TrimExcess_m2451380967_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Texture>::get_Capacity()
#define List_1_get_Capacity_m3471901673(__this, method) ((  int32_t (*) (List_1_t3894644513 *, const MethodInfo*))List_1_get_Capacity_m543520655_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Texture>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m1703517202(__this, ___value0, method) ((  void (*) (List_1_t3894644513 *, int32_t, const MethodInfo*))List_1_set_Capacity_m1332789688_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Texture>::get_Count()
#define List_1_get_Count_m1287687266(__this, method) ((  int32_t (*) (List_1_t3894644513 *, const MethodInfo*))List_1_get_Count_m3549598589_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.Texture>::get_Item(System.Int32)
#define List_1_get_Item_m2719106742(__this, ___index0, method) ((  Texture_t2526458961 * (*) (List_1_t3894644513 *, int32_t, const MethodInfo*))List_1_get_Item_m404118264_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Texture>::set_Item(System.Int32,T)
#define List_1_set_Item_m2942811267(__this, ___index0, ___value1, method) ((  void (*) (List_1_t3894644513 *, int32_t, Texture_t2526458961 *, const MethodInfo*))List_1_set_Item_m1074271145_gshared)(__this, ___index0, ___value1, method)
