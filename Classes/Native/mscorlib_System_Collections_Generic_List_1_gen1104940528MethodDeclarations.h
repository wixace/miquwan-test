﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1244034627MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1<AIEventInfo>::.ctor()
#define List_1__ctor_m229475775(__this, method) ((  void (*) (List_1_t1104940528 *, const MethodInfo*))List_1__ctor_m1239231859_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<AIEventInfo>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m180221609(__this, ___collection0, method) ((  void (*) (List_1_t1104940528 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m1160795371_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<AIEventInfo>::.ctor(System.Int32)
#define List_1__ctor_m4254648423(__this, ___capacity0, method) ((  void (*) (List_1_t1104940528 *, int32_t, const MethodInfo*))List_1__ctor_m3643386469_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<AIEventInfo>::.cctor()
#define List_1__cctor_m61196887(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m3826137881_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<AIEventInfo>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m802043112(__this, method) ((  Il2CppObject* (*) (List_1_t1104940528 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2808422246_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<AIEventInfo>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m703272430(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t1104940528 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m4034025648_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<AIEventInfo>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m962189289(__this, method) ((  Il2CppObject * (*) (List_1_t1104940528 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m1841330603_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<AIEventInfo>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m1260390440(__this, ___item0, method) ((  int32_t (*) (List_1_t1104940528 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m3794749222_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<AIEventInfo>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m2659416612(__this, ___item0, method) ((  bool (*) (List_1_t1104940528 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m2659633254_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<AIEventInfo>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m3713002112(__this, ___item0, method) ((  int32_t (*) (List_1_t1104940528 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m3431692926_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<AIEventInfo>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m586510763(__this, ___index0, ___item1, method) ((  void (*) (List_1_t1104940528 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m2067529129_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<AIEventInfo>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m1353702813(__this, ___item0, method) ((  void (*) (List_1_t1104940528 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m1644145887_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<AIEventInfo>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m600313125(__this, method) ((  bool (*) (List_1_t1104940528 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1299706087_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<AIEventInfo>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m1708522168(__this, method) ((  bool (*) (List_1_t1104940528 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m3867536694_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<AIEventInfo>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m1189839332(__this, method) ((  Il2CppObject * (*) (List_1_t1104940528 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m4244374434_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<AIEventInfo>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m984257683(__this, method) ((  bool (*) (List_1_t1104940528 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m432946261_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<AIEventInfo>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m1455590406(__this, method) ((  bool (*) (List_1_t1104940528 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m2961826820_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<AIEventInfo>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m2039908971(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t1104940528 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m3985478825_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<AIEventInfo>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m1610079938(__this, ___index0, ___value1, method) ((  void (*) (List_1_t1104940528 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m3234554688_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<AIEventInfo>::Add(T)
#define List_1_Add_m4218803887(__this, ___item0, method) ((  void (*) (List_1_t1104940528 *, AIEventInfo_t4031722272 *, const MethodInfo*))List_1_Add_m933592675_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<AIEventInfo>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m4184826212(__this, ___newCount0, method) ((  void (*) (List_1_t1104940528 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m4122600870_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<AIEventInfo>::CheckRange(System.Int32,System.Int32)
#define List_1_CheckRange_m1017691139(__this, ___idx0, ___count1, method) ((  void (*) (List_1_t1104940528 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m590371457_gshared)(__this, ___idx0, ___count1, method)
// System.Void System.Collections.Generic.List`1<AIEventInfo>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m2430675042(__this, ___collection0, method) ((  void (*) (List_1_t1104940528 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m2478449828_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<AIEventInfo>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m1691647266(__this, ___enumerable0, method) ((  void (*) (List_1_t1104940528 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m1739422052_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<AIEventInfo>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m28787765(__this, ___collection0, method) ((  void (*) (List_1_t1104940528 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m2229151411_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<AIEventInfo>::AsReadOnly()
#define List_1_AsReadOnly_m1776425684(__this, method) ((  ReadOnlyCollection_1_t1293832512 * (*) (List_1_t1104940528 *, const MethodInfo*))List_1_AsReadOnly_m769820182_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<AIEventInfo>::BinarySearch(T)
#define List_1_BinarySearch_m2341195883(__this, ___item0, method) ((  int32_t (*) (List_1_t1104940528 *, AIEventInfo_t4031722272 *, const MethodInfo*))List_1_BinarySearch_m2761349225_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<AIEventInfo>::Clear()
#define List_1_Clear_m4073930817(__this, method) ((  void (*) (List_1_t1104940528 *, const MethodInfo*))List_1_Clear_m2940332446_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<AIEventInfo>::Contains(T)
#define List_1_Contains_m3879726191(__this, ___item0, method) ((  bool (*) (List_1_t1104940528 *, AIEventInfo_t4031722272 *, const MethodInfo*))List_1_Contains_m4186092781_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<AIEventInfo>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m1802948953(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t1104940528 *, AIEventInfoU5BU5D_t526056545*, int32_t, const MethodInfo*))List_1_CopyTo_m3988356635_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<AIEventInfo>::Find(System.Predicate`1<T>)
#define List_1_Find_m1801433135(__this, ___match0, method) ((  AIEventInfo_t4031722272 * (*) (List_1_t1104940528 *, Predicate_1_t3642779155 *, const MethodInfo*))List_1_Find_m3379773421_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<AIEventInfo>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m1785643434(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t3642779155 *, const MethodInfo*))List_1_CheckMatch_m3390394152_gshared)(__this /* static, unused */, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<AIEventInfo>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m580926415(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t1104940528 *, int32_t, int32_t, Predicate_1_t3642779155 *, const MethodInfo*))List_1_GetIndex_m4275988045_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<AIEventInfo>::GetEnumerator()
#define List_1_GetEnumerator_m262345260(__this, method) ((  Enumerator_t1124613298  (*) (List_1_t1104940528 *, const MethodInfo*))List_1_GetEnumerator_m1919240000_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<AIEventInfo>::IndexOf(T)
#define List_1_IndexOf_m3378419101(__this, ___item0, method) ((  int32_t (*) (List_1_t1104940528 *, AIEventInfo_t4031722272 *, const MethodInfo*))List_1_IndexOf_m2776798407_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<AIEventInfo>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m1815625392(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t1104940528 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m3807054194_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<AIEventInfo>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m1549316137(__this, ___index0, method) ((  void (*) (List_1_t1104940528 *, int32_t, const MethodInfo*))List_1_CheckIndex_m3734723819_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<AIEventInfo>::Insert(System.Int32,T)
#define List_1_Insert_m2870700560(__this, ___index0, ___item1, method) ((  void (*) (List_1_t1104940528 *, int32_t, AIEventInfo_t4031722272 *, const MethodInfo*))List_1_Insert_m3427163986_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<AIEventInfo>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m4238142085(__this, ___collection0, method) ((  void (*) (List_1_t1104940528 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m2905071175_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<AIEventInfo>::Remove(T)
#define List_1_Remove_m1058208810(__this, ___item0, method) ((  bool (*) (List_1_t1104940528 *, AIEventInfo_t4031722272 *, const MethodInfo*))List_1_Remove_m2747911208_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<AIEventInfo>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m3935738272(__this, ___match0, method) ((  int32_t (*) (List_1_t1104940528 *, Predicate_1_t3642779155 *, const MethodInfo*))List_1_RemoveAll_m2933443938_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<AIEventInfo>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m744553430(__this, ___index0, method) ((  void (*) (List_1_t1104940528 *, int32_t, const MethodInfo*))List_1_RemoveAt_m2208877785_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<AIEventInfo>::RemoveRange(System.Int32,System.Int32)
#define List_1_RemoveRange_m1218866169(__this, ___index0, ___count1, method) ((  void (*) (List_1_t1104940528 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m856857915_gshared)(__this, ___index0, ___count1, method)
// System.Void System.Collections.Generic.List`1<AIEventInfo>::Reverse()
#define List_1_Reverse_m3994995414(__this, method) ((  void (*) (List_1_t1104940528 *, const MethodInfo*))List_1_Reverse_m449081940_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<AIEventInfo>::Sort()
#define List_1_Sort_m2947961996(__this, method) ((  void (*) (List_1_t1104940528 *, const MethodInfo*))List_1_Sort_m1168641486_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<AIEventInfo>::Sort(System.Collections.Generic.IComparer`1<T>)
#define List_1_Sort_m3312877272(__this, ___comparer0, method) ((  void (*) (List_1_t1104940528 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m3726677974_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<AIEventInfo>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m3861196767(__this, ___comparison0, method) ((  void (*) (List_1_t1104940528 *, Comparison_1_t2748083459 *, const MethodInfo*))List_1_Sort_m4192185249_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<AIEventInfo>::ToArray()
#define List_1_ToArray_m3618545813(__this, method) ((  AIEventInfoU5BU5D_t526056545* (*) (List_1_t1104940528 *, const MethodInfo*))List_1_ToArray_m1449333879_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<AIEventInfo>::TrimExcess()
#define List_1_TrimExcess_m4039039781(__this, method) ((  void (*) (List_1_t1104940528 *, const MethodInfo*))List_1_TrimExcess_m2451380967_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<AIEventInfo>::get_Capacity()
#define List_1_get_Capacity_m114325325(__this, method) ((  int32_t (*) (List_1_t1104940528 *, const MethodInfo*))List_1_get_Capacity_m543520655_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<AIEventInfo>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m1395015030(__this, ___value0, method) ((  void (*) (List_1_t1104940528 *, int32_t, const MethodInfo*))List_1_set_Capacity_m1332789688_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<AIEventInfo>::get_Count()
#define List_1_get_Count_m4226618229(__this, method) ((  int32_t (*) (List_1_t1104940528 *, const MethodInfo*))List_1_get_Count_m3549598589_gshared)(__this, method)
// T System.Collections.Generic.List`1<AIEventInfo>::get_Item(System.Int32)
#define List_1_get_Item_m2622889088(__this, ___index0, method) ((  AIEventInfo_t4031722272 * (*) (List_1_t1104940528 *, int32_t, const MethodInfo*))List_1_get_Item_m404118264_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<AIEventInfo>::set_Item(System.Int32,T)
#define List_1_set_Item_m3183830759(__this, ___index0, ___value1, method) ((  void (*) (List_1_t1104940528 *, int32_t, AIEventInfo_t4031722272 *, const MethodInfo*))List_1_set_Item_m1074271145_gshared)(__this, ___index0, ___value1, method)
