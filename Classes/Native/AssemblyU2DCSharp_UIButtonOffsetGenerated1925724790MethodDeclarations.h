﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIButtonOffsetGenerated
struct UIButtonOffsetGenerated_t1925724790;
// JSVCall
struct JSVCall_t3708497963;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void UIButtonOffsetGenerated::.ctor()
extern "C"  void UIButtonOffsetGenerated__ctor_m566331829 (UIButtonOffsetGenerated_t1925724790 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIButtonOffsetGenerated::UIButtonOffset_UIButtonOffset1(JSVCall,System.Int32)
extern "C"  bool UIButtonOffsetGenerated_UIButtonOffset_UIButtonOffset1_m2967071677 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonOffsetGenerated::UIButtonOffset_tweenTarget(JSVCall)
extern "C"  void UIButtonOffsetGenerated_UIButtonOffset_tweenTarget_m3778705794 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonOffsetGenerated::UIButtonOffset_hover(JSVCall)
extern "C"  void UIButtonOffsetGenerated_UIButtonOffset_hover_m2274510370 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonOffsetGenerated::UIButtonOffset_pressed(JSVCall)
extern "C"  void UIButtonOffsetGenerated_UIButtonOffset_pressed_m3600551932 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonOffsetGenerated::UIButtonOffset_duration(JSVCall)
extern "C"  void UIButtonOffsetGenerated_UIButtonOffset_duration_m4017088794 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonOffsetGenerated::__Register()
extern "C"  void UIButtonOffsetGenerated___Register_m826981426 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIButtonOffsetGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UIButtonOffsetGenerated_ilo_getObject1_m4251416993 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UIButtonOffsetGenerated::ilo_getVector3S2(System.Int32)
extern "C"  Vector3_t4282066566  UIButtonOffsetGenerated_ilo_getVector3S2_m2873015154 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonOffsetGenerated::ilo_setSingle3(System.Int32,System.Single)
extern "C"  void UIButtonOffsetGenerated_ilo_setSingle3_m1472435105 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
