﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraShotGameSpeedCfg
struct CameraShotGameSpeedCfg_t308624942;
// ProtoBuf.IExtension
struct IExtension_t1606339106;

#include "codegen/il2cpp-codegen.h"

// System.Void CameraShotGameSpeedCfg::.ctor()
extern "C"  void CameraShotGameSpeedCfg__ctor_m483330605 (CameraShotGameSpeedCfg_t308624942 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.IExtension CameraShotGameSpeedCfg::ProtoBuf.IExtensible.GetExtensionObject(System.Boolean)
extern "C"  Il2CppObject * CameraShotGameSpeedCfg_ProtoBuf_IExtensible_GetExtensionObject_m1921170813 (CameraShotGameSpeedCfg_t308624942 * __this, bool ___createIfMissing0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CameraShotGameSpeedCfg::get_id()
extern "C"  int32_t CameraShotGameSpeedCfg_get_id_m602172297 (CameraShotGameSpeedCfg_t308624942 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotGameSpeedCfg::set_id(System.Int32)
extern "C"  void CameraShotGameSpeedCfg_set_id_m1371235008 (CameraShotGameSpeedCfg_t308624942 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CameraShotGameSpeedCfg::get_speed()
extern "C"  float CameraShotGameSpeedCfg_get_speed_m4155796669 (CameraShotGameSpeedCfg_t308624942 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotGameSpeedCfg::set_speed(System.Single)
extern "C"  void CameraShotGameSpeedCfg_set_speed_m4005088694 (CameraShotGameSpeedCfg_t308624942 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
