﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Linq.JToken/<AfterSelf>c__Iterator1A
struct U3CAfterSelfU3Ec__Iterator1A_t406957142;
// Newtonsoft.Json.Linq.JToken
struct JToken_t3412245951;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.Linq.JToken>
struct IEnumerator_1_t1029143704;

#include "codegen/il2cpp-codegen.h"

// System.Void Newtonsoft.Json.Linq.JToken/<AfterSelf>c__Iterator1A::.ctor()
extern "C"  void U3CAfterSelfU3Ec__Iterator1A__ctor_m2972045061 (U3CAfterSelfU3Ec__Iterator1A_t406957142 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken/<AfterSelf>c__Iterator1A::System.Collections.Generic.IEnumerator<Newtonsoft.Json.Linq.JToken>.get_Current()
extern "C"  JToken_t3412245951 * U3CAfterSelfU3Ec__Iterator1A_System_Collections_Generic_IEnumeratorU3CNewtonsoft_Json_Linq_JTokenU3E_get_Current_m1482121326 (U3CAfterSelfU3Ec__Iterator1A_t406957142 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Linq.JToken/<AfterSelf>c__Iterator1A::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CAfterSelfU3Ec__Iterator1A_System_Collections_IEnumerator_get_Current_m1883990283 (U3CAfterSelfU3Ec__Iterator1A_t406957142 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Newtonsoft.Json.Linq.JToken/<AfterSelf>c__Iterator1A::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CAfterSelfU3Ec__Iterator1A_System_Collections_IEnumerable_GetEnumerator_m3590709836 (U3CAfterSelfU3Ec__Iterator1A_t406957142 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JToken/<AfterSelf>c__Iterator1A::System.Collections.Generic.IEnumerable<Newtonsoft.Json.Linq.JToken>.GetEnumerator()
extern "C"  Il2CppObject* U3CAfterSelfU3Ec__Iterator1A_System_Collections_Generic_IEnumerableU3CNewtonsoft_Json_Linq_JTokenU3E_GetEnumerator_m4291353007 (U3CAfterSelfU3Ec__Iterator1A_t406957142 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Linq.JToken/<AfterSelf>c__Iterator1A::MoveNext()
extern "C"  bool U3CAfterSelfU3Ec__Iterator1A_MoveNext_m464067255 (U3CAfterSelfU3Ec__Iterator1A_t406957142 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JToken/<AfterSelf>c__Iterator1A::Dispose()
extern "C"  void U3CAfterSelfU3Ec__Iterator1A_Dispose_m4175302146 (U3CAfterSelfU3Ec__Iterator1A_t406957142 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JToken/<AfterSelf>c__Iterator1A::Reset()
extern "C"  void U3CAfterSelfU3Ec__Iterator1A_Reset_m618478002 (U3CAfterSelfU3Ec__Iterator1A_t406957142 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
