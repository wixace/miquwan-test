﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Loader_PathUtilGenerated
struct Loader_PathUtilGenerated_t1232460380;
// JSVCall
struct JSVCall_t3708497963;
// System.String[]
struct StringU5BU5D_t4054002952;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_String7231557.h"

// System.Void Loader_PathUtilGenerated::.ctor()
extern "C"  void Loader_PathUtilGenerated__ctor_m1613077439 (Loader_PathUtilGenerated_t1232460380 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Loader_PathUtilGenerated::PathUtil_PATH_VS_NUM(JSVCall)
extern "C"  void Loader_PathUtilGenerated_PathUtil_PATH_VS_NUM_m3856122204 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Loader_PathUtilGenerated::PathUtil_PATH_VS_CONFIG(JSVCall)
extern "C"  void Loader_PathUtilGenerated_PathUtil_PATH_VS_CONFIG_m3667938952 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Loader_PathUtilGenerated::PathUtil_PATH_ABDEPENDS(JSVCall)
extern "C"  void Loader_PathUtilGenerated_PathUtil_PATH_ABDEPENDS_m2424241062 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Loader_PathUtilGenerated::PathUtil_PATH_CONFIG(JSVCall)
extern "C"  void Loader_PathUtilGenerated_PathUtil_PATH_CONFIG_m3932559038 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Loader_PathUtilGenerated::PathUtil_PATH_SHADER(JSVCall)
extern "C"  void Loader_PathUtilGenerated_PathUtil_PATH_SHADER_m1519823227 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Loader_PathUtilGenerated::PathUtil_PATH_JAVASCRIPT(JSVCall)
extern "C"  void Loader_PathUtilGenerated_PathUtil_PATH_JAVASCRIPT_m2882460659 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Loader_PathUtilGenerated::PathUtil_PATH_RESNAME(JSVCall)
extern "C"  void Loader_PathUtilGenerated_PathUtil_PATH_RESNAME_m150006753 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Loader_PathUtilGenerated::PathUtil_PATH_CRC(JSVCall)
extern "C"  void Loader_PathUtilGenerated_PathUtil_PATH_CRC_m1262827096 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Loader_PathUtilGenerated::PathUtil_PATH_BULLETIN(JSVCall)
extern "C"  void Loader_PathUtilGenerated_PathUtil_PATH_BULLETIN_m2573115257 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Loader_PathUtilGenerated::PathUtil_persistentDataPath(JSVCall)
extern "C"  void Loader_PathUtilGenerated_PathUtil_persistentDataPath_m4203242700 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Loader_PathUtilGenerated::PathUtil_SaySoundDataPath(JSVCall)
extern "C"  void Loader_PathUtilGenerated_PathUtil_SaySoundDataPath_m2327542271 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Loader_PathUtilGenerated::PathUtil_streamingDataPath(JSVCall)
extern "C"  void Loader_PathUtilGenerated_PathUtil_streamingDataPath_m1668177545 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Loader_PathUtilGenerated::PathUtil_LOGIN_URL(JSVCall)
extern "C"  void Loader_PathUtilGenerated_PathUtil_LOGIN_URL_m3276624289 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Loader_PathUtilGenerated::PathUtil_HELP_URL(JSVCall)
extern "C"  void Loader_PathUtilGenerated_PathUtil_HELP_URL_m2908967809 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Loader_PathUtilGenerated::PathUtil_ALLVS_URL(JSVCall)
extern "C"  void Loader_PathUtilGenerated_PathUtil_ALLVS_URL_m3227364108 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Loader_PathUtilGenerated::PathUtil_SERVER_URL(JSVCall)
extern "C"  void Loader_PathUtilGenerated_PathUtil_SERVER_URL_m1258106815 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Loader_PathUtilGenerated::PathUtil_CheckPath__String(JSVCall,System.Int32)
extern "C"  bool Loader_PathUtilGenerated_PathUtil_CheckPath__String_m1706740143 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Loader_PathUtilGenerated::PathUtil_GetFilePathWithoutExtension__String(JSVCall,System.Int32)
extern "C"  bool Loader_PathUtilGenerated_PathUtil_GetFilePathWithoutExtension__String_m1563525200 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Loader_PathUtilGenerated::PathUtil_GetPersistentDataPath(JSVCall,System.Int32)
extern "C"  bool Loader_PathUtilGenerated_PathUtil_GetPersistentDataPath_m1308224365 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Loader_PathUtilGenerated::PathUtil_Init(JSVCall,System.Int32)
extern "C"  bool Loader_PathUtilGenerated_PathUtil_Init_m914766689 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Loader_PathUtilGenerated::PathUtil_JoinPath__String__String(JSVCall,System.Int32)
extern "C"  bool Loader_PathUtilGenerated_PathUtil_JoinPath__String__String_m3080749346 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Loader_PathUtilGenerated::PathUtil_JoinPath__String_Array(JSVCall,System.Int32)
extern "C"  bool Loader_PathUtilGenerated_PathUtil_JoinPath__String_Array_m2241589643 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Loader_PathUtilGenerated::PathUtil_PersistentDataPath__String(JSVCall,System.Int32)
extern "C"  bool Loader_PathUtilGenerated_PathUtil_PersistentDataPath__String_m987977672 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Loader_PathUtilGenerated::PathUtil_ServerPath__String(JSVCall,System.Int32)
extern "C"  bool Loader_PathUtilGenerated_PathUtil_ServerPath__String_m3626983338 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Loader_PathUtilGenerated::PathUtil_StreamingAssetsPath__String(JSVCall,System.Int32)
extern "C"  bool Loader_PathUtilGenerated_PathUtil_StreamingAssetsPath__String_m2408250348 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Loader_PathUtilGenerated::__Register()
extern "C"  void Loader_PathUtilGenerated___Register_m431120488 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] Loader_PathUtilGenerated::<PathUtil_JoinPath__String_Array>m__6B()
extern "C"  StringU5BU5D_t4054002952* Loader_PathUtilGenerated_U3CPathUtil_JoinPath__String_ArrayU3Em__6B_m3463745367 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Loader_PathUtilGenerated::ilo_setStringS1(System.Int32,System.String)
extern "C"  void Loader_PathUtilGenerated_ilo_setStringS1_m2531474184 (Il2CppObject * __this /* static, unused */, int32_t ___e0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Loader_PathUtilGenerated::ilo_getStringS2(System.Int32)
extern "C"  String_t* Loader_PathUtilGenerated_ilo_getStringS2_m197644414 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Loader_PathUtilGenerated::ilo_GetPersistentDataPath3()
extern "C"  String_t* Loader_PathUtilGenerated_ilo_GetPersistentDataPath3_m2126048018 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Loader_PathUtilGenerated::ilo_Init4()
extern "C"  void Loader_PathUtilGenerated_ilo_Init4_m3734239598 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Loader_PathUtilGenerated::ilo_getArrayLength5(System.Int32)
extern "C"  int32_t Loader_PathUtilGenerated_ilo_getArrayLength5_m1853069465 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
