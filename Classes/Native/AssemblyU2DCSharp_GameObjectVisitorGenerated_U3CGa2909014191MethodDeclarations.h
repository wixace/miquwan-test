﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GameObjectVisitorGenerated/<GameObjectVisitor_FindChildrenInCondition_GetDelegate_member3_arg2>c__AnonStorey64
struct U3CGameObjectVisitor_FindChildrenInCondition_GetDelegate_member3_arg2U3Ec__AnonStorey64_t2909014191;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"

// System.Void GameObjectVisitorGenerated/<GameObjectVisitor_FindChildrenInCondition_GetDelegate_member3_arg2>c__AnonStorey64::.ctor()
extern "C"  void U3CGameObjectVisitor_FindChildrenInCondition_GetDelegate_member3_arg2U3Ec__AnonStorey64__ctor_m2985031884 (U3CGameObjectVisitor_FindChildrenInCondition_GetDelegate_member3_arg2U3Ec__AnonStorey64_t2909014191 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameObjectVisitorGenerated/<GameObjectVisitor_FindChildrenInCondition_GetDelegate_member3_arg2>c__AnonStorey64::<>m__5A(UnityEngine.GameObject)
extern "C"  bool U3CGameObjectVisitor_FindChildrenInCondition_GetDelegate_member3_arg2U3Ec__AnonStorey64_U3CU3Em__5A_m2606094277 (U3CGameObjectVisitor_FindChildrenInCondition_GetDelegate_member3_arg2U3Ec__AnonStorey64_t2909014191 * __this, GameObject_t3674682005 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
