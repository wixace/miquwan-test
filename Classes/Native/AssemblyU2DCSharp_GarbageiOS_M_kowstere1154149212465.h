﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_kowstere115
struct  M_kowstere115_t4149212465  : public Il2CppObject
{
public:
	// System.Single GarbageiOS.M_kowstere115::_mamowse
	float ____mamowse_0;
	// System.Int32 GarbageiOS.M_kowstere115::_murjerJearno
	int32_t ____murjerJearno_1;
	// System.Int32 GarbageiOS.M_kowstere115::_daheStosallma
	int32_t ____daheStosallma_2;
	// System.Boolean GarbageiOS.M_kowstere115::_jeqowwayPutrenou
	bool ____jeqowwayPutrenou_3;
	// System.Boolean GarbageiOS.M_kowstere115::_hairsooPearsas
	bool ____hairsooPearsas_4;
	// System.Boolean GarbageiOS.M_kowstere115::_sureresa
	bool ____sureresa_5;
	// System.Single GarbageiOS.M_kowstere115::_malea
	float ____malea_6;
	// System.Int32 GarbageiOS.M_kowstere115::_nairvustouRudawpel
	int32_t ____nairvustouRudawpel_7;
	// System.Single GarbageiOS.M_kowstere115::_jidawxo
	float ____jidawxo_8;
	// System.Boolean GarbageiOS.M_kowstere115::_nernalka
	bool ____nernalka_9;
	// System.UInt32 GarbageiOS.M_kowstere115::_simu
	uint32_t ____simu_10;
	// System.Single GarbageiOS.M_kowstere115::_rearrorRefownu
	float ____rearrorRefownu_11;
	// System.String GarbageiOS.M_kowstere115::_teqemtriZairaller
	String_t* ____teqemtriZairaller_12;
	// System.Single GarbageiOS.M_kowstere115::_qorhirDowne
	float ____qorhirDowne_13;
	// System.Single GarbageiOS.M_kowstere115::_heesehooWacule
	float ____heesehooWacule_14;
	// System.Int32 GarbageiOS.M_kowstere115::_bouseeQecoo
	int32_t ____bouseeQecoo_15;
	// System.Int32 GarbageiOS.M_kowstere115::_komaldruMedibu
	int32_t ____komaldruMedibu_16;
	// System.Boolean GarbageiOS.M_kowstere115::_bator
	bool ____bator_17;
	// System.Boolean GarbageiOS.M_kowstere115::_cusowsemPorpe
	bool ____cusowsemPorpe_18;
	// System.Int32 GarbageiOS.M_kowstere115::_seheSorzou
	int32_t ____seheSorzou_19;
	// System.Int32 GarbageiOS.M_kowstere115::_desaipai
	int32_t ____desaipai_20;
	// System.Single GarbageiOS.M_kowstere115::_sexi
	float ____sexi_21;
	// System.String GarbageiOS.M_kowstere115::_sateWipemoo
	String_t* ____sateWipemoo_22;
	// System.UInt32 GarbageiOS.M_kowstere115::_fakemGeldow
	uint32_t ____fakemGeldow_23;
	// System.UInt32 GarbageiOS.M_kowstere115::_dubi
	uint32_t ____dubi_24;
	// System.Int32 GarbageiOS.M_kowstere115::_ninuMasu
	int32_t ____ninuMasu_25;
	// System.Int32 GarbageiOS.M_kowstere115::_payposaw
	int32_t ____payposaw_26;
	// System.Int32 GarbageiOS.M_kowstere115::_stirdiSofi
	int32_t ____stirdiSofi_27;
	// System.Int32 GarbageiOS.M_kowstere115::_xaileseRalheardar
	int32_t ____xaileseRalheardar_28;
	// System.String GarbageiOS.M_kowstere115::_hersoudror
	String_t* ____hersoudror_29;
	// System.String GarbageiOS.M_kowstere115::_pearbeeLaidri
	String_t* ____pearbeeLaidri_30;
	// System.Boolean GarbageiOS.M_kowstere115::_taswousoChucal
	bool ____taswousoChucal_31;
	// System.Boolean GarbageiOS.M_kowstere115::_mairwaNaswereba
	bool ____mairwaNaswereba_32;
	// System.Int32 GarbageiOS.M_kowstere115::_jassaldiTairjato
	int32_t ____jassaldiTairjato_33;
	// System.UInt32 GarbageiOS.M_kowstere115::_zoucearWema
	uint32_t ____zoucearWema_34;
	// System.String GarbageiOS.M_kowstere115::_wownou
	String_t* ____wownou_35;
	// System.Boolean GarbageiOS.M_kowstere115::_nevubuDeararha
	bool ____nevubuDeararha_36;
	// System.UInt32 GarbageiOS.M_kowstere115::_seremas
	uint32_t ____seremas_37;
	// System.Boolean GarbageiOS.M_kowstere115::_runawhowNerrai
	bool ____runawhowNerrai_38;
	// System.Int32 GarbageiOS.M_kowstere115::_pairlairSorvemdu
	int32_t ____pairlairSorvemdu_39;
	// System.Int32 GarbageiOS.M_kowstere115::_selmaifem
	int32_t ____selmaifem_40;
	// System.Boolean GarbageiOS.M_kowstere115::_gogarvouCehi
	bool ____gogarvouCehi_41;
	// System.Boolean GarbageiOS.M_kowstere115::_kelstayDemow
	bool ____kelstayDemow_42;
	// System.UInt32 GarbageiOS.M_kowstere115::_rala
	uint32_t ____rala_43;
	// System.String GarbageiOS.M_kowstere115::_camouheBorwhowhe
	String_t* ____camouheBorwhowhe_44;
	// System.Int32 GarbageiOS.M_kowstere115::_palmall
	int32_t ____palmall_45;
	// System.Boolean GarbageiOS.M_kowstere115::_sebair
	bool ____sebair_46;
	// System.Int32 GarbageiOS.M_kowstere115::_boojereciCairbou
	int32_t ____boojereciCairbou_47;
	// System.String GarbageiOS.M_kowstere115::_xirasbou
	String_t* ____xirasbou_48;
	// System.Boolean GarbageiOS.M_kowstere115::_sechowsu
	bool ____sechowsu_49;
	// System.UInt32 GarbageiOS.M_kowstere115::_xawqarke
	uint32_t ____xawqarke_50;
	// System.Boolean GarbageiOS.M_kowstere115::_dralor
	bool ____dralor_51;
	// System.String GarbageiOS.M_kowstere115::_temay
	String_t* ____temay_52;
	// System.Single GarbageiOS.M_kowstere115::_bearreremayChujanel
	float ____bearreremayChujanel_53;
	// System.Int32 GarbageiOS.M_kowstere115::_houchayjaDece
	int32_t ____houchayjaDece_54;
	// System.UInt32 GarbageiOS.M_kowstere115::_mouqelsuValurya
	uint32_t ____mouqelsuValurya_55;
	// System.Int32 GarbageiOS.M_kowstere115::_doonuNotrurtrear
	int32_t ____doonuNotrurtrear_56;
	// System.Single GarbageiOS.M_kowstere115::_rasdrerWhowrarda
	float ____rasdrerWhowrarda_57;
	// System.String GarbageiOS.M_kowstere115::_torcadeMartefou
	String_t* ____torcadeMartefou_58;
	// System.UInt32 GarbageiOS.M_kowstere115::_gaceDaycall
	uint32_t ____gaceDaycall_59;
	// System.Single GarbageiOS.M_kowstere115::_joobeKernowsoo
	float ____joobeKernowsoo_60;
	// System.Boolean GarbageiOS.M_kowstere115::_rayrar
	bool ____rayrar_61;
	// System.String GarbageiOS.M_kowstere115::_tereto
	String_t* ____tereto_62;
	// System.Boolean GarbageiOS.M_kowstere115::_qelsal
	bool ____qelsal_63;
	// System.String GarbageiOS.M_kowstere115::_kalchaldear
	String_t* ____kalchaldear_64;
	// System.String GarbageiOS.M_kowstere115::_raroworWissor
	String_t* ____raroworWissor_65;
	// System.Single GarbageiOS.M_kowstere115::_lewhis
	float ____lewhis_66;
	// System.UInt32 GarbageiOS.M_kowstere115::_garormouPallear
	uint32_t ____garormouPallear_67;
	// System.Boolean GarbageiOS.M_kowstere115::_gechayTallxoohi
	bool ____gechayTallxoohi_68;
	// System.String GarbageiOS.M_kowstere115::_zaile
	String_t* ____zaile_69;
	// System.Int32 GarbageiOS.M_kowstere115::_hoochici
	int32_t ____hoochici_70;
	// System.Single GarbageiOS.M_kowstere115::_nearnooPelmoyair
	float ____nearnooPelmoyair_71;
	// System.Int32 GarbageiOS.M_kowstere115::_louwaltrir
	int32_t ____louwaltrir_72;
	// System.Single GarbageiOS.M_kowstere115::_pouhawwaTisgir
	float ____pouhawwaTisgir_73;
	// System.Single GarbageiOS.M_kowstere115::_rirras
	float ____rirras_74;
	// System.Boolean GarbageiOS.M_kowstere115::_tisday
	bool ____tisday_75;
	// System.Int32 GarbageiOS.M_kowstere115::_bisnuBerwhe
	int32_t ____bisnuBerwhe_76;
	// System.Int32 GarbageiOS.M_kowstere115::_beecawsu
	int32_t ____beecawsu_77;
	// System.String GarbageiOS.M_kowstere115::_jilaw
	String_t* ____jilaw_78;
	// System.Int32 GarbageiOS.M_kowstere115::_roumi
	int32_t ____roumi_79;
	// System.Boolean GarbageiOS.M_kowstere115::_narlobear
	bool ____narlobear_80;
	// System.Int32 GarbageiOS.M_kowstere115::_whibay
	int32_t ____whibay_81;
	// System.Int32 GarbageiOS.M_kowstere115::_semruliReedaycou
	int32_t ____semruliReedaycou_82;
	// System.String GarbageiOS.M_kowstere115::_tearhebu
	String_t* ____tearhebu_83;
	// System.Boolean GarbageiOS.M_kowstere115::_nejeSalcirro
	bool ____nejeSalcirro_84;

public:
	inline static int32_t get_offset_of__mamowse_0() { return static_cast<int32_t>(offsetof(M_kowstere115_t4149212465, ____mamowse_0)); }
	inline float get__mamowse_0() const { return ____mamowse_0; }
	inline float* get_address_of__mamowse_0() { return &____mamowse_0; }
	inline void set__mamowse_0(float value)
	{
		____mamowse_0 = value;
	}

	inline static int32_t get_offset_of__murjerJearno_1() { return static_cast<int32_t>(offsetof(M_kowstere115_t4149212465, ____murjerJearno_1)); }
	inline int32_t get__murjerJearno_1() const { return ____murjerJearno_1; }
	inline int32_t* get_address_of__murjerJearno_1() { return &____murjerJearno_1; }
	inline void set__murjerJearno_1(int32_t value)
	{
		____murjerJearno_1 = value;
	}

	inline static int32_t get_offset_of__daheStosallma_2() { return static_cast<int32_t>(offsetof(M_kowstere115_t4149212465, ____daheStosallma_2)); }
	inline int32_t get__daheStosallma_2() const { return ____daheStosallma_2; }
	inline int32_t* get_address_of__daheStosallma_2() { return &____daheStosallma_2; }
	inline void set__daheStosallma_2(int32_t value)
	{
		____daheStosallma_2 = value;
	}

	inline static int32_t get_offset_of__jeqowwayPutrenou_3() { return static_cast<int32_t>(offsetof(M_kowstere115_t4149212465, ____jeqowwayPutrenou_3)); }
	inline bool get__jeqowwayPutrenou_3() const { return ____jeqowwayPutrenou_3; }
	inline bool* get_address_of__jeqowwayPutrenou_3() { return &____jeqowwayPutrenou_3; }
	inline void set__jeqowwayPutrenou_3(bool value)
	{
		____jeqowwayPutrenou_3 = value;
	}

	inline static int32_t get_offset_of__hairsooPearsas_4() { return static_cast<int32_t>(offsetof(M_kowstere115_t4149212465, ____hairsooPearsas_4)); }
	inline bool get__hairsooPearsas_4() const { return ____hairsooPearsas_4; }
	inline bool* get_address_of__hairsooPearsas_4() { return &____hairsooPearsas_4; }
	inline void set__hairsooPearsas_4(bool value)
	{
		____hairsooPearsas_4 = value;
	}

	inline static int32_t get_offset_of__sureresa_5() { return static_cast<int32_t>(offsetof(M_kowstere115_t4149212465, ____sureresa_5)); }
	inline bool get__sureresa_5() const { return ____sureresa_5; }
	inline bool* get_address_of__sureresa_5() { return &____sureresa_5; }
	inline void set__sureresa_5(bool value)
	{
		____sureresa_5 = value;
	}

	inline static int32_t get_offset_of__malea_6() { return static_cast<int32_t>(offsetof(M_kowstere115_t4149212465, ____malea_6)); }
	inline float get__malea_6() const { return ____malea_6; }
	inline float* get_address_of__malea_6() { return &____malea_6; }
	inline void set__malea_6(float value)
	{
		____malea_6 = value;
	}

	inline static int32_t get_offset_of__nairvustouRudawpel_7() { return static_cast<int32_t>(offsetof(M_kowstere115_t4149212465, ____nairvustouRudawpel_7)); }
	inline int32_t get__nairvustouRudawpel_7() const { return ____nairvustouRudawpel_7; }
	inline int32_t* get_address_of__nairvustouRudawpel_7() { return &____nairvustouRudawpel_7; }
	inline void set__nairvustouRudawpel_7(int32_t value)
	{
		____nairvustouRudawpel_7 = value;
	}

	inline static int32_t get_offset_of__jidawxo_8() { return static_cast<int32_t>(offsetof(M_kowstere115_t4149212465, ____jidawxo_8)); }
	inline float get__jidawxo_8() const { return ____jidawxo_8; }
	inline float* get_address_of__jidawxo_8() { return &____jidawxo_8; }
	inline void set__jidawxo_8(float value)
	{
		____jidawxo_8 = value;
	}

	inline static int32_t get_offset_of__nernalka_9() { return static_cast<int32_t>(offsetof(M_kowstere115_t4149212465, ____nernalka_9)); }
	inline bool get__nernalka_9() const { return ____nernalka_9; }
	inline bool* get_address_of__nernalka_9() { return &____nernalka_9; }
	inline void set__nernalka_9(bool value)
	{
		____nernalka_9 = value;
	}

	inline static int32_t get_offset_of__simu_10() { return static_cast<int32_t>(offsetof(M_kowstere115_t4149212465, ____simu_10)); }
	inline uint32_t get__simu_10() const { return ____simu_10; }
	inline uint32_t* get_address_of__simu_10() { return &____simu_10; }
	inline void set__simu_10(uint32_t value)
	{
		____simu_10 = value;
	}

	inline static int32_t get_offset_of__rearrorRefownu_11() { return static_cast<int32_t>(offsetof(M_kowstere115_t4149212465, ____rearrorRefownu_11)); }
	inline float get__rearrorRefownu_11() const { return ____rearrorRefownu_11; }
	inline float* get_address_of__rearrorRefownu_11() { return &____rearrorRefownu_11; }
	inline void set__rearrorRefownu_11(float value)
	{
		____rearrorRefownu_11 = value;
	}

	inline static int32_t get_offset_of__teqemtriZairaller_12() { return static_cast<int32_t>(offsetof(M_kowstere115_t4149212465, ____teqemtriZairaller_12)); }
	inline String_t* get__teqemtriZairaller_12() const { return ____teqemtriZairaller_12; }
	inline String_t** get_address_of__teqemtriZairaller_12() { return &____teqemtriZairaller_12; }
	inline void set__teqemtriZairaller_12(String_t* value)
	{
		____teqemtriZairaller_12 = value;
		Il2CppCodeGenWriteBarrier(&____teqemtriZairaller_12, value);
	}

	inline static int32_t get_offset_of__qorhirDowne_13() { return static_cast<int32_t>(offsetof(M_kowstere115_t4149212465, ____qorhirDowne_13)); }
	inline float get__qorhirDowne_13() const { return ____qorhirDowne_13; }
	inline float* get_address_of__qorhirDowne_13() { return &____qorhirDowne_13; }
	inline void set__qorhirDowne_13(float value)
	{
		____qorhirDowne_13 = value;
	}

	inline static int32_t get_offset_of__heesehooWacule_14() { return static_cast<int32_t>(offsetof(M_kowstere115_t4149212465, ____heesehooWacule_14)); }
	inline float get__heesehooWacule_14() const { return ____heesehooWacule_14; }
	inline float* get_address_of__heesehooWacule_14() { return &____heesehooWacule_14; }
	inline void set__heesehooWacule_14(float value)
	{
		____heesehooWacule_14 = value;
	}

	inline static int32_t get_offset_of__bouseeQecoo_15() { return static_cast<int32_t>(offsetof(M_kowstere115_t4149212465, ____bouseeQecoo_15)); }
	inline int32_t get__bouseeQecoo_15() const { return ____bouseeQecoo_15; }
	inline int32_t* get_address_of__bouseeQecoo_15() { return &____bouseeQecoo_15; }
	inline void set__bouseeQecoo_15(int32_t value)
	{
		____bouseeQecoo_15 = value;
	}

	inline static int32_t get_offset_of__komaldruMedibu_16() { return static_cast<int32_t>(offsetof(M_kowstere115_t4149212465, ____komaldruMedibu_16)); }
	inline int32_t get__komaldruMedibu_16() const { return ____komaldruMedibu_16; }
	inline int32_t* get_address_of__komaldruMedibu_16() { return &____komaldruMedibu_16; }
	inline void set__komaldruMedibu_16(int32_t value)
	{
		____komaldruMedibu_16 = value;
	}

	inline static int32_t get_offset_of__bator_17() { return static_cast<int32_t>(offsetof(M_kowstere115_t4149212465, ____bator_17)); }
	inline bool get__bator_17() const { return ____bator_17; }
	inline bool* get_address_of__bator_17() { return &____bator_17; }
	inline void set__bator_17(bool value)
	{
		____bator_17 = value;
	}

	inline static int32_t get_offset_of__cusowsemPorpe_18() { return static_cast<int32_t>(offsetof(M_kowstere115_t4149212465, ____cusowsemPorpe_18)); }
	inline bool get__cusowsemPorpe_18() const { return ____cusowsemPorpe_18; }
	inline bool* get_address_of__cusowsemPorpe_18() { return &____cusowsemPorpe_18; }
	inline void set__cusowsemPorpe_18(bool value)
	{
		____cusowsemPorpe_18 = value;
	}

	inline static int32_t get_offset_of__seheSorzou_19() { return static_cast<int32_t>(offsetof(M_kowstere115_t4149212465, ____seheSorzou_19)); }
	inline int32_t get__seheSorzou_19() const { return ____seheSorzou_19; }
	inline int32_t* get_address_of__seheSorzou_19() { return &____seheSorzou_19; }
	inline void set__seheSorzou_19(int32_t value)
	{
		____seheSorzou_19 = value;
	}

	inline static int32_t get_offset_of__desaipai_20() { return static_cast<int32_t>(offsetof(M_kowstere115_t4149212465, ____desaipai_20)); }
	inline int32_t get__desaipai_20() const { return ____desaipai_20; }
	inline int32_t* get_address_of__desaipai_20() { return &____desaipai_20; }
	inline void set__desaipai_20(int32_t value)
	{
		____desaipai_20 = value;
	}

	inline static int32_t get_offset_of__sexi_21() { return static_cast<int32_t>(offsetof(M_kowstere115_t4149212465, ____sexi_21)); }
	inline float get__sexi_21() const { return ____sexi_21; }
	inline float* get_address_of__sexi_21() { return &____sexi_21; }
	inline void set__sexi_21(float value)
	{
		____sexi_21 = value;
	}

	inline static int32_t get_offset_of__sateWipemoo_22() { return static_cast<int32_t>(offsetof(M_kowstere115_t4149212465, ____sateWipemoo_22)); }
	inline String_t* get__sateWipemoo_22() const { return ____sateWipemoo_22; }
	inline String_t** get_address_of__sateWipemoo_22() { return &____sateWipemoo_22; }
	inline void set__sateWipemoo_22(String_t* value)
	{
		____sateWipemoo_22 = value;
		Il2CppCodeGenWriteBarrier(&____sateWipemoo_22, value);
	}

	inline static int32_t get_offset_of__fakemGeldow_23() { return static_cast<int32_t>(offsetof(M_kowstere115_t4149212465, ____fakemGeldow_23)); }
	inline uint32_t get__fakemGeldow_23() const { return ____fakemGeldow_23; }
	inline uint32_t* get_address_of__fakemGeldow_23() { return &____fakemGeldow_23; }
	inline void set__fakemGeldow_23(uint32_t value)
	{
		____fakemGeldow_23 = value;
	}

	inline static int32_t get_offset_of__dubi_24() { return static_cast<int32_t>(offsetof(M_kowstere115_t4149212465, ____dubi_24)); }
	inline uint32_t get__dubi_24() const { return ____dubi_24; }
	inline uint32_t* get_address_of__dubi_24() { return &____dubi_24; }
	inline void set__dubi_24(uint32_t value)
	{
		____dubi_24 = value;
	}

	inline static int32_t get_offset_of__ninuMasu_25() { return static_cast<int32_t>(offsetof(M_kowstere115_t4149212465, ____ninuMasu_25)); }
	inline int32_t get__ninuMasu_25() const { return ____ninuMasu_25; }
	inline int32_t* get_address_of__ninuMasu_25() { return &____ninuMasu_25; }
	inline void set__ninuMasu_25(int32_t value)
	{
		____ninuMasu_25 = value;
	}

	inline static int32_t get_offset_of__payposaw_26() { return static_cast<int32_t>(offsetof(M_kowstere115_t4149212465, ____payposaw_26)); }
	inline int32_t get__payposaw_26() const { return ____payposaw_26; }
	inline int32_t* get_address_of__payposaw_26() { return &____payposaw_26; }
	inline void set__payposaw_26(int32_t value)
	{
		____payposaw_26 = value;
	}

	inline static int32_t get_offset_of__stirdiSofi_27() { return static_cast<int32_t>(offsetof(M_kowstere115_t4149212465, ____stirdiSofi_27)); }
	inline int32_t get__stirdiSofi_27() const { return ____stirdiSofi_27; }
	inline int32_t* get_address_of__stirdiSofi_27() { return &____stirdiSofi_27; }
	inline void set__stirdiSofi_27(int32_t value)
	{
		____stirdiSofi_27 = value;
	}

	inline static int32_t get_offset_of__xaileseRalheardar_28() { return static_cast<int32_t>(offsetof(M_kowstere115_t4149212465, ____xaileseRalheardar_28)); }
	inline int32_t get__xaileseRalheardar_28() const { return ____xaileseRalheardar_28; }
	inline int32_t* get_address_of__xaileseRalheardar_28() { return &____xaileseRalheardar_28; }
	inline void set__xaileseRalheardar_28(int32_t value)
	{
		____xaileseRalheardar_28 = value;
	}

	inline static int32_t get_offset_of__hersoudror_29() { return static_cast<int32_t>(offsetof(M_kowstere115_t4149212465, ____hersoudror_29)); }
	inline String_t* get__hersoudror_29() const { return ____hersoudror_29; }
	inline String_t** get_address_of__hersoudror_29() { return &____hersoudror_29; }
	inline void set__hersoudror_29(String_t* value)
	{
		____hersoudror_29 = value;
		Il2CppCodeGenWriteBarrier(&____hersoudror_29, value);
	}

	inline static int32_t get_offset_of__pearbeeLaidri_30() { return static_cast<int32_t>(offsetof(M_kowstere115_t4149212465, ____pearbeeLaidri_30)); }
	inline String_t* get__pearbeeLaidri_30() const { return ____pearbeeLaidri_30; }
	inline String_t** get_address_of__pearbeeLaidri_30() { return &____pearbeeLaidri_30; }
	inline void set__pearbeeLaidri_30(String_t* value)
	{
		____pearbeeLaidri_30 = value;
		Il2CppCodeGenWriteBarrier(&____pearbeeLaidri_30, value);
	}

	inline static int32_t get_offset_of__taswousoChucal_31() { return static_cast<int32_t>(offsetof(M_kowstere115_t4149212465, ____taswousoChucal_31)); }
	inline bool get__taswousoChucal_31() const { return ____taswousoChucal_31; }
	inline bool* get_address_of__taswousoChucal_31() { return &____taswousoChucal_31; }
	inline void set__taswousoChucal_31(bool value)
	{
		____taswousoChucal_31 = value;
	}

	inline static int32_t get_offset_of__mairwaNaswereba_32() { return static_cast<int32_t>(offsetof(M_kowstere115_t4149212465, ____mairwaNaswereba_32)); }
	inline bool get__mairwaNaswereba_32() const { return ____mairwaNaswereba_32; }
	inline bool* get_address_of__mairwaNaswereba_32() { return &____mairwaNaswereba_32; }
	inline void set__mairwaNaswereba_32(bool value)
	{
		____mairwaNaswereba_32 = value;
	}

	inline static int32_t get_offset_of__jassaldiTairjato_33() { return static_cast<int32_t>(offsetof(M_kowstere115_t4149212465, ____jassaldiTairjato_33)); }
	inline int32_t get__jassaldiTairjato_33() const { return ____jassaldiTairjato_33; }
	inline int32_t* get_address_of__jassaldiTairjato_33() { return &____jassaldiTairjato_33; }
	inline void set__jassaldiTairjato_33(int32_t value)
	{
		____jassaldiTairjato_33 = value;
	}

	inline static int32_t get_offset_of__zoucearWema_34() { return static_cast<int32_t>(offsetof(M_kowstere115_t4149212465, ____zoucearWema_34)); }
	inline uint32_t get__zoucearWema_34() const { return ____zoucearWema_34; }
	inline uint32_t* get_address_of__zoucearWema_34() { return &____zoucearWema_34; }
	inline void set__zoucearWema_34(uint32_t value)
	{
		____zoucearWema_34 = value;
	}

	inline static int32_t get_offset_of__wownou_35() { return static_cast<int32_t>(offsetof(M_kowstere115_t4149212465, ____wownou_35)); }
	inline String_t* get__wownou_35() const { return ____wownou_35; }
	inline String_t** get_address_of__wownou_35() { return &____wownou_35; }
	inline void set__wownou_35(String_t* value)
	{
		____wownou_35 = value;
		Il2CppCodeGenWriteBarrier(&____wownou_35, value);
	}

	inline static int32_t get_offset_of__nevubuDeararha_36() { return static_cast<int32_t>(offsetof(M_kowstere115_t4149212465, ____nevubuDeararha_36)); }
	inline bool get__nevubuDeararha_36() const { return ____nevubuDeararha_36; }
	inline bool* get_address_of__nevubuDeararha_36() { return &____nevubuDeararha_36; }
	inline void set__nevubuDeararha_36(bool value)
	{
		____nevubuDeararha_36 = value;
	}

	inline static int32_t get_offset_of__seremas_37() { return static_cast<int32_t>(offsetof(M_kowstere115_t4149212465, ____seremas_37)); }
	inline uint32_t get__seremas_37() const { return ____seremas_37; }
	inline uint32_t* get_address_of__seremas_37() { return &____seremas_37; }
	inline void set__seremas_37(uint32_t value)
	{
		____seremas_37 = value;
	}

	inline static int32_t get_offset_of__runawhowNerrai_38() { return static_cast<int32_t>(offsetof(M_kowstere115_t4149212465, ____runawhowNerrai_38)); }
	inline bool get__runawhowNerrai_38() const { return ____runawhowNerrai_38; }
	inline bool* get_address_of__runawhowNerrai_38() { return &____runawhowNerrai_38; }
	inline void set__runawhowNerrai_38(bool value)
	{
		____runawhowNerrai_38 = value;
	}

	inline static int32_t get_offset_of__pairlairSorvemdu_39() { return static_cast<int32_t>(offsetof(M_kowstere115_t4149212465, ____pairlairSorvemdu_39)); }
	inline int32_t get__pairlairSorvemdu_39() const { return ____pairlairSorvemdu_39; }
	inline int32_t* get_address_of__pairlairSorvemdu_39() { return &____pairlairSorvemdu_39; }
	inline void set__pairlairSorvemdu_39(int32_t value)
	{
		____pairlairSorvemdu_39 = value;
	}

	inline static int32_t get_offset_of__selmaifem_40() { return static_cast<int32_t>(offsetof(M_kowstere115_t4149212465, ____selmaifem_40)); }
	inline int32_t get__selmaifem_40() const { return ____selmaifem_40; }
	inline int32_t* get_address_of__selmaifem_40() { return &____selmaifem_40; }
	inline void set__selmaifem_40(int32_t value)
	{
		____selmaifem_40 = value;
	}

	inline static int32_t get_offset_of__gogarvouCehi_41() { return static_cast<int32_t>(offsetof(M_kowstere115_t4149212465, ____gogarvouCehi_41)); }
	inline bool get__gogarvouCehi_41() const { return ____gogarvouCehi_41; }
	inline bool* get_address_of__gogarvouCehi_41() { return &____gogarvouCehi_41; }
	inline void set__gogarvouCehi_41(bool value)
	{
		____gogarvouCehi_41 = value;
	}

	inline static int32_t get_offset_of__kelstayDemow_42() { return static_cast<int32_t>(offsetof(M_kowstere115_t4149212465, ____kelstayDemow_42)); }
	inline bool get__kelstayDemow_42() const { return ____kelstayDemow_42; }
	inline bool* get_address_of__kelstayDemow_42() { return &____kelstayDemow_42; }
	inline void set__kelstayDemow_42(bool value)
	{
		____kelstayDemow_42 = value;
	}

	inline static int32_t get_offset_of__rala_43() { return static_cast<int32_t>(offsetof(M_kowstere115_t4149212465, ____rala_43)); }
	inline uint32_t get__rala_43() const { return ____rala_43; }
	inline uint32_t* get_address_of__rala_43() { return &____rala_43; }
	inline void set__rala_43(uint32_t value)
	{
		____rala_43 = value;
	}

	inline static int32_t get_offset_of__camouheBorwhowhe_44() { return static_cast<int32_t>(offsetof(M_kowstere115_t4149212465, ____camouheBorwhowhe_44)); }
	inline String_t* get__camouheBorwhowhe_44() const { return ____camouheBorwhowhe_44; }
	inline String_t** get_address_of__camouheBorwhowhe_44() { return &____camouheBorwhowhe_44; }
	inline void set__camouheBorwhowhe_44(String_t* value)
	{
		____camouheBorwhowhe_44 = value;
		Il2CppCodeGenWriteBarrier(&____camouheBorwhowhe_44, value);
	}

	inline static int32_t get_offset_of__palmall_45() { return static_cast<int32_t>(offsetof(M_kowstere115_t4149212465, ____palmall_45)); }
	inline int32_t get__palmall_45() const { return ____palmall_45; }
	inline int32_t* get_address_of__palmall_45() { return &____palmall_45; }
	inline void set__palmall_45(int32_t value)
	{
		____palmall_45 = value;
	}

	inline static int32_t get_offset_of__sebair_46() { return static_cast<int32_t>(offsetof(M_kowstere115_t4149212465, ____sebair_46)); }
	inline bool get__sebair_46() const { return ____sebair_46; }
	inline bool* get_address_of__sebair_46() { return &____sebair_46; }
	inline void set__sebair_46(bool value)
	{
		____sebair_46 = value;
	}

	inline static int32_t get_offset_of__boojereciCairbou_47() { return static_cast<int32_t>(offsetof(M_kowstere115_t4149212465, ____boojereciCairbou_47)); }
	inline int32_t get__boojereciCairbou_47() const { return ____boojereciCairbou_47; }
	inline int32_t* get_address_of__boojereciCairbou_47() { return &____boojereciCairbou_47; }
	inline void set__boojereciCairbou_47(int32_t value)
	{
		____boojereciCairbou_47 = value;
	}

	inline static int32_t get_offset_of__xirasbou_48() { return static_cast<int32_t>(offsetof(M_kowstere115_t4149212465, ____xirasbou_48)); }
	inline String_t* get__xirasbou_48() const { return ____xirasbou_48; }
	inline String_t** get_address_of__xirasbou_48() { return &____xirasbou_48; }
	inline void set__xirasbou_48(String_t* value)
	{
		____xirasbou_48 = value;
		Il2CppCodeGenWriteBarrier(&____xirasbou_48, value);
	}

	inline static int32_t get_offset_of__sechowsu_49() { return static_cast<int32_t>(offsetof(M_kowstere115_t4149212465, ____sechowsu_49)); }
	inline bool get__sechowsu_49() const { return ____sechowsu_49; }
	inline bool* get_address_of__sechowsu_49() { return &____sechowsu_49; }
	inline void set__sechowsu_49(bool value)
	{
		____sechowsu_49 = value;
	}

	inline static int32_t get_offset_of__xawqarke_50() { return static_cast<int32_t>(offsetof(M_kowstere115_t4149212465, ____xawqarke_50)); }
	inline uint32_t get__xawqarke_50() const { return ____xawqarke_50; }
	inline uint32_t* get_address_of__xawqarke_50() { return &____xawqarke_50; }
	inline void set__xawqarke_50(uint32_t value)
	{
		____xawqarke_50 = value;
	}

	inline static int32_t get_offset_of__dralor_51() { return static_cast<int32_t>(offsetof(M_kowstere115_t4149212465, ____dralor_51)); }
	inline bool get__dralor_51() const { return ____dralor_51; }
	inline bool* get_address_of__dralor_51() { return &____dralor_51; }
	inline void set__dralor_51(bool value)
	{
		____dralor_51 = value;
	}

	inline static int32_t get_offset_of__temay_52() { return static_cast<int32_t>(offsetof(M_kowstere115_t4149212465, ____temay_52)); }
	inline String_t* get__temay_52() const { return ____temay_52; }
	inline String_t** get_address_of__temay_52() { return &____temay_52; }
	inline void set__temay_52(String_t* value)
	{
		____temay_52 = value;
		Il2CppCodeGenWriteBarrier(&____temay_52, value);
	}

	inline static int32_t get_offset_of__bearreremayChujanel_53() { return static_cast<int32_t>(offsetof(M_kowstere115_t4149212465, ____bearreremayChujanel_53)); }
	inline float get__bearreremayChujanel_53() const { return ____bearreremayChujanel_53; }
	inline float* get_address_of__bearreremayChujanel_53() { return &____bearreremayChujanel_53; }
	inline void set__bearreremayChujanel_53(float value)
	{
		____bearreremayChujanel_53 = value;
	}

	inline static int32_t get_offset_of__houchayjaDece_54() { return static_cast<int32_t>(offsetof(M_kowstere115_t4149212465, ____houchayjaDece_54)); }
	inline int32_t get__houchayjaDece_54() const { return ____houchayjaDece_54; }
	inline int32_t* get_address_of__houchayjaDece_54() { return &____houchayjaDece_54; }
	inline void set__houchayjaDece_54(int32_t value)
	{
		____houchayjaDece_54 = value;
	}

	inline static int32_t get_offset_of__mouqelsuValurya_55() { return static_cast<int32_t>(offsetof(M_kowstere115_t4149212465, ____mouqelsuValurya_55)); }
	inline uint32_t get__mouqelsuValurya_55() const { return ____mouqelsuValurya_55; }
	inline uint32_t* get_address_of__mouqelsuValurya_55() { return &____mouqelsuValurya_55; }
	inline void set__mouqelsuValurya_55(uint32_t value)
	{
		____mouqelsuValurya_55 = value;
	}

	inline static int32_t get_offset_of__doonuNotrurtrear_56() { return static_cast<int32_t>(offsetof(M_kowstere115_t4149212465, ____doonuNotrurtrear_56)); }
	inline int32_t get__doonuNotrurtrear_56() const { return ____doonuNotrurtrear_56; }
	inline int32_t* get_address_of__doonuNotrurtrear_56() { return &____doonuNotrurtrear_56; }
	inline void set__doonuNotrurtrear_56(int32_t value)
	{
		____doonuNotrurtrear_56 = value;
	}

	inline static int32_t get_offset_of__rasdrerWhowrarda_57() { return static_cast<int32_t>(offsetof(M_kowstere115_t4149212465, ____rasdrerWhowrarda_57)); }
	inline float get__rasdrerWhowrarda_57() const { return ____rasdrerWhowrarda_57; }
	inline float* get_address_of__rasdrerWhowrarda_57() { return &____rasdrerWhowrarda_57; }
	inline void set__rasdrerWhowrarda_57(float value)
	{
		____rasdrerWhowrarda_57 = value;
	}

	inline static int32_t get_offset_of__torcadeMartefou_58() { return static_cast<int32_t>(offsetof(M_kowstere115_t4149212465, ____torcadeMartefou_58)); }
	inline String_t* get__torcadeMartefou_58() const { return ____torcadeMartefou_58; }
	inline String_t** get_address_of__torcadeMartefou_58() { return &____torcadeMartefou_58; }
	inline void set__torcadeMartefou_58(String_t* value)
	{
		____torcadeMartefou_58 = value;
		Il2CppCodeGenWriteBarrier(&____torcadeMartefou_58, value);
	}

	inline static int32_t get_offset_of__gaceDaycall_59() { return static_cast<int32_t>(offsetof(M_kowstere115_t4149212465, ____gaceDaycall_59)); }
	inline uint32_t get__gaceDaycall_59() const { return ____gaceDaycall_59; }
	inline uint32_t* get_address_of__gaceDaycall_59() { return &____gaceDaycall_59; }
	inline void set__gaceDaycall_59(uint32_t value)
	{
		____gaceDaycall_59 = value;
	}

	inline static int32_t get_offset_of__joobeKernowsoo_60() { return static_cast<int32_t>(offsetof(M_kowstere115_t4149212465, ____joobeKernowsoo_60)); }
	inline float get__joobeKernowsoo_60() const { return ____joobeKernowsoo_60; }
	inline float* get_address_of__joobeKernowsoo_60() { return &____joobeKernowsoo_60; }
	inline void set__joobeKernowsoo_60(float value)
	{
		____joobeKernowsoo_60 = value;
	}

	inline static int32_t get_offset_of__rayrar_61() { return static_cast<int32_t>(offsetof(M_kowstere115_t4149212465, ____rayrar_61)); }
	inline bool get__rayrar_61() const { return ____rayrar_61; }
	inline bool* get_address_of__rayrar_61() { return &____rayrar_61; }
	inline void set__rayrar_61(bool value)
	{
		____rayrar_61 = value;
	}

	inline static int32_t get_offset_of__tereto_62() { return static_cast<int32_t>(offsetof(M_kowstere115_t4149212465, ____tereto_62)); }
	inline String_t* get__tereto_62() const { return ____tereto_62; }
	inline String_t** get_address_of__tereto_62() { return &____tereto_62; }
	inline void set__tereto_62(String_t* value)
	{
		____tereto_62 = value;
		Il2CppCodeGenWriteBarrier(&____tereto_62, value);
	}

	inline static int32_t get_offset_of__qelsal_63() { return static_cast<int32_t>(offsetof(M_kowstere115_t4149212465, ____qelsal_63)); }
	inline bool get__qelsal_63() const { return ____qelsal_63; }
	inline bool* get_address_of__qelsal_63() { return &____qelsal_63; }
	inline void set__qelsal_63(bool value)
	{
		____qelsal_63 = value;
	}

	inline static int32_t get_offset_of__kalchaldear_64() { return static_cast<int32_t>(offsetof(M_kowstere115_t4149212465, ____kalchaldear_64)); }
	inline String_t* get__kalchaldear_64() const { return ____kalchaldear_64; }
	inline String_t** get_address_of__kalchaldear_64() { return &____kalchaldear_64; }
	inline void set__kalchaldear_64(String_t* value)
	{
		____kalchaldear_64 = value;
		Il2CppCodeGenWriteBarrier(&____kalchaldear_64, value);
	}

	inline static int32_t get_offset_of__raroworWissor_65() { return static_cast<int32_t>(offsetof(M_kowstere115_t4149212465, ____raroworWissor_65)); }
	inline String_t* get__raroworWissor_65() const { return ____raroworWissor_65; }
	inline String_t** get_address_of__raroworWissor_65() { return &____raroworWissor_65; }
	inline void set__raroworWissor_65(String_t* value)
	{
		____raroworWissor_65 = value;
		Il2CppCodeGenWriteBarrier(&____raroworWissor_65, value);
	}

	inline static int32_t get_offset_of__lewhis_66() { return static_cast<int32_t>(offsetof(M_kowstere115_t4149212465, ____lewhis_66)); }
	inline float get__lewhis_66() const { return ____lewhis_66; }
	inline float* get_address_of__lewhis_66() { return &____lewhis_66; }
	inline void set__lewhis_66(float value)
	{
		____lewhis_66 = value;
	}

	inline static int32_t get_offset_of__garormouPallear_67() { return static_cast<int32_t>(offsetof(M_kowstere115_t4149212465, ____garormouPallear_67)); }
	inline uint32_t get__garormouPallear_67() const { return ____garormouPallear_67; }
	inline uint32_t* get_address_of__garormouPallear_67() { return &____garormouPallear_67; }
	inline void set__garormouPallear_67(uint32_t value)
	{
		____garormouPallear_67 = value;
	}

	inline static int32_t get_offset_of__gechayTallxoohi_68() { return static_cast<int32_t>(offsetof(M_kowstere115_t4149212465, ____gechayTallxoohi_68)); }
	inline bool get__gechayTallxoohi_68() const { return ____gechayTallxoohi_68; }
	inline bool* get_address_of__gechayTallxoohi_68() { return &____gechayTallxoohi_68; }
	inline void set__gechayTallxoohi_68(bool value)
	{
		____gechayTallxoohi_68 = value;
	}

	inline static int32_t get_offset_of__zaile_69() { return static_cast<int32_t>(offsetof(M_kowstere115_t4149212465, ____zaile_69)); }
	inline String_t* get__zaile_69() const { return ____zaile_69; }
	inline String_t** get_address_of__zaile_69() { return &____zaile_69; }
	inline void set__zaile_69(String_t* value)
	{
		____zaile_69 = value;
		Il2CppCodeGenWriteBarrier(&____zaile_69, value);
	}

	inline static int32_t get_offset_of__hoochici_70() { return static_cast<int32_t>(offsetof(M_kowstere115_t4149212465, ____hoochici_70)); }
	inline int32_t get__hoochici_70() const { return ____hoochici_70; }
	inline int32_t* get_address_of__hoochici_70() { return &____hoochici_70; }
	inline void set__hoochici_70(int32_t value)
	{
		____hoochici_70 = value;
	}

	inline static int32_t get_offset_of__nearnooPelmoyair_71() { return static_cast<int32_t>(offsetof(M_kowstere115_t4149212465, ____nearnooPelmoyair_71)); }
	inline float get__nearnooPelmoyair_71() const { return ____nearnooPelmoyair_71; }
	inline float* get_address_of__nearnooPelmoyair_71() { return &____nearnooPelmoyair_71; }
	inline void set__nearnooPelmoyair_71(float value)
	{
		____nearnooPelmoyair_71 = value;
	}

	inline static int32_t get_offset_of__louwaltrir_72() { return static_cast<int32_t>(offsetof(M_kowstere115_t4149212465, ____louwaltrir_72)); }
	inline int32_t get__louwaltrir_72() const { return ____louwaltrir_72; }
	inline int32_t* get_address_of__louwaltrir_72() { return &____louwaltrir_72; }
	inline void set__louwaltrir_72(int32_t value)
	{
		____louwaltrir_72 = value;
	}

	inline static int32_t get_offset_of__pouhawwaTisgir_73() { return static_cast<int32_t>(offsetof(M_kowstere115_t4149212465, ____pouhawwaTisgir_73)); }
	inline float get__pouhawwaTisgir_73() const { return ____pouhawwaTisgir_73; }
	inline float* get_address_of__pouhawwaTisgir_73() { return &____pouhawwaTisgir_73; }
	inline void set__pouhawwaTisgir_73(float value)
	{
		____pouhawwaTisgir_73 = value;
	}

	inline static int32_t get_offset_of__rirras_74() { return static_cast<int32_t>(offsetof(M_kowstere115_t4149212465, ____rirras_74)); }
	inline float get__rirras_74() const { return ____rirras_74; }
	inline float* get_address_of__rirras_74() { return &____rirras_74; }
	inline void set__rirras_74(float value)
	{
		____rirras_74 = value;
	}

	inline static int32_t get_offset_of__tisday_75() { return static_cast<int32_t>(offsetof(M_kowstere115_t4149212465, ____tisday_75)); }
	inline bool get__tisday_75() const { return ____tisday_75; }
	inline bool* get_address_of__tisday_75() { return &____tisday_75; }
	inline void set__tisday_75(bool value)
	{
		____tisday_75 = value;
	}

	inline static int32_t get_offset_of__bisnuBerwhe_76() { return static_cast<int32_t>(offsetof(M_kowstere115_t4149212465, ____bisnuBerwhe_76)); }
	inline int32_t get__bisnuBerwhe_76() const { return ____bisnuBerwhe_76; }
	inline int32_t* get_address_of__bisnuBerwhe_76() { return &____bisnuBerwhe_76; }
	inline void set__bisnuBerwhe_76(int32_t value)
	{
		____bisnuBerwhe_76 = value;
	}

	inline static int32_t get_offset_of__beecawsu_77() { return static_cast<int32_t>(offsetof(M_kowstere115_t4149212465, ____beecawsu_77)); }
	inline int32_t get__beecawsu_77() const { return ____beecawsu_77; }
	inline int32_t* get_address_of__beecawsu_77() { return &____beecawsu_77; }
	inline void set__beecawsu_77(int32_t value)
	{
		____beecawsu_77 = value;
	}

	inline static int32_t get_offset_of__jilaw_78() { return static_cast<int32_t>(offsetof(M_kowstere115_t4149212465, ____jilaw_78)); }
	inline String_t* get__jilaw_78() const { return ____jilaw_78; }
	inline String_t** get_address_of__jilaw_78() { return &____jilaw_78; }
	inline void set__jilaw_78(String_t* value)
	{
		____jilaw_78 = value;
		Il2CppCodeGenWriteBarrier(&____jilaw_78, value);
	}

	inline static int32_t get_offset_of__roumi_79() { return static_cast<int32_t>(offsetof(M_kowstere115_t4149212465, ____roumi_79)); }
	inline int32_t get__roumi_79() const { return ____roumi_79; }
	inline int32_t* get_address_of__roumi_79() { return &____roumi_79; }
	inline void set__roumi_79(int32_t value)
	{
		____roumi_79 = value;
	}

	inline static int32_t get_offset_of__narlobear_80() { return static_cast<int32_t>(offsetof(M_kowstere115_t4149212465, ____narlobear_80)); }
	inline bool get__narlobear_80() const { return ____narlobear_80; }
	inline bool* get_address_of__narlobear_80() { return &____narlobear_80; }
	inline void set__narlobear_80(bool value)
	{
		____narlobear_80 = value;
	}

	inline static int32_t get_offset_of__whibay_81() { return static_cast<int32_t>(offsetof(M_kowstere115_t4149212465, ____whibay_81)); }
	inline int32_t get__whibay_81() const { return ____whibay_81; }
	inline int32_t* get_address_of__whibay_81() { return &____whibay_81; }
	inline void set__whibay_81(int32_t value)
	{
		____whibay_81 = value;
	}

	inline static int32_t get_offset_of__semruliReedaycou_82() { return static_cast<int32_t>(offsetof(M_kowstere115_t4149212465, ____semruliReedaycou_82)); }
	inline int32_t get__semruliReedaycou_82() const { return ____semruliReedaycou_82; }
	inline int32_t* get_address_of__semruliReedaycou_82() { return &____semruliReedaycou_82; }
	inline void set__semruliReedaycou_82(int32_t value)
	{
		____semruliReedaycou_82 = value;
	}

	inline static int32_t get_offset_of__tearhebu_83() { return static_cast<int32_t>(offsetof(M_kowstere115_t4149212465, ____tearhebu_83)); }
	inline String_t* get__tearhebu_83() const { return ____tearhebu_83; }
	inline String_t** get_address_of__tearhebu_83() { return &____tearhebu_83; }
	inline void set__tearhebu_83(String_t* value)
	{
		____tearhebu_83 = value;
		Il2CppCodeGenWriteBarrier(&____tearhebu_83, value);
	}

	inline static int32_t get_offset_of__nejeSalcirro_84() { return static_cast<int32_t>(offsetof(M_kowstere115_t4149212465, ____nejeSalcirro_84)); }
	inline bool get__nejeSalcirro_84() const { return ____nejeSalcirro_84; }
	inline bool* get_address_of__nejeSalcirro_84() { return &____nejeSalcirro_84; }
	inline void set__nejeSalcirro_84(bool value)
	{
		____nejeSalcirro_84 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
