﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HeroMgrGenerated/<HeroMgr_LeaveBattleReq_GetDelegate_member49_arg0>c__AnonStorey68
struct U3CHeroMgr_LeaveBattleReq_GetDelegate_member49_arg0U3Ec__AnonStorey68_t2915351760;

#include "codegen/il2cpp-codegen.h"

// System.Void HeroMgrGenerated/<HeroMgr_LeaveBattleReq_GetDelegate_member49_arg0>c__AnonStorey68::.ctor()
extern "C"  void U3CHeroMgr_LeaveBattleReq_GetDelegate_member49_arg0U3Ec__AnonStorey68__ctor_m2211704267 (U3CHeroMgr_LeaveBattleReq_GetDelegate_member49_arg0U3Ec__AnonStorey68_t2915351760 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroMgrGenerated/<HeroMgr_LeaveBattleReq_GetDelegate_member49_arg0>c__AnonStorey68::<>m__64()
extern "C"  void U3CHeroMgr_LeaveBattleReq_GetDelegate_member49_arg0U3Ec__AnonStorey68_U3CU3Em__64_m1747181842 (U3CHeroMgr_LeaveBattleReq_GetDelegate_member49_arg0U3Ec__AnonStorey68_t2915351760 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
