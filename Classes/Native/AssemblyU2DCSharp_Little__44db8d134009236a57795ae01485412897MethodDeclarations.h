﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._44db8d134009236a57795ae0586741e4
struct _44db8d134009236a57795ae0586741e4_t1485412897;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__44db8d134009236a57795ae01485412897.h"

// System.Void Little._44db8d134009236a57795ae0586741e4::.ctor()
extern "C"  void _44db8d134009236a57795ae0586741e4__ctor_m2564710700 (_44db8d134009236a57795ae0586741e4_t1485412897 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._44db8d134009236a57795ae0586741e4::_44db8d134009236a57795ae0586741e4m2(System.Int32)
extern "C"  int32_t _44db8d134009236a57795ae0586741e4__44db8d134009236a57795ae0586741e4m2_m3743045113 (_44db8d134009236a57795ae0586741e4_t1485412897 * __this, int32_t ____44db8d134009236a57795ae0586741e4a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._44db8d134009236a57795ae0586741e4::_44db8d134009236a57795ae0586741e4m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _44db8d134009236a57795ae0586741e4__44db8d134009236a57795ae0586741e4m_m2870869213 (_44db8d134009236a57795ae0586741e4_t1485412897 * __this, int32_t ____44db8d134009236a57795ae0586741e4a0, int32_t ____44db8d134009236a57795ae0586741e4341, int32_t ____44db8d134009236a57795ae0586741e4c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._44db8d134009236a57795ae0586741e4::ilo__44db8d134009236a57795ae0586741e4m21(Little._44db8d134009236a57795ae0586741e4,System.Int32)
extern "C"  int32_t _44db8d134009236a57795ae0586741e4_ilo__44db8d134009236a57795ae0586741e4m21_m1338271496 (Il2CppObject * __this /* static, unused */, _44db8d134009236a57795ae0586741e4_t1485412897 * ____this0, int32_t ____44db8d134009236a57795ae0586741e4a1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
