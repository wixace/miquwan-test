﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_baydee91
struct M_baydee91_t1061612434;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"

// System.Void GarbageiOS.M_baydee91::.ctor()
extern "C"  void M_baydee91__ctor_m623107809 (M_baydee91_t1061612434 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_baydee91::M_qaldriwhisMubelwair0(System.String[],System.Int32)
extern "C"  void M_baydee91_M_qaldriwhisMubelwair0_m3413298306 (M_baydee91_t1061612434 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
