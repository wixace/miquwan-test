﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Pathfinding.EuclideanEmbedding/<RecalculateCosts>c__AnonStorey120
struct U3CRecalculateCostsU3Ec__AnonStorey120_t3302164579;
// Pathfinding.EuclideanEmbedding/<RecalculateCosts>c__AnonStorey120/<RecalculateCosts>c__AnonStorey11E
struct U3CRecalculateCostsU3Ec__AnonStorey11E_t853919929;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.EuclideanEmbedding/<RecalculateCosts>c__AnonStorey120/<RecalculateCosts>c__AnonStorey11E/<RecalculateCosts>c__AnonStorey11F
struct  U3CRecalculateCostsU3Ec__AnonStorey11F_t1930950820  : public Il2CppObject
{
public:
	// System.UInt32 Pathfinding.EuclideanEmbedding/<RecalculateCosts>c__AnonStorey120/<RecalculateCosts>c__AnonStorey11E/<RecalculateCosts>c__AnonStorey11F::costOffset
	uint32_t ___costOffset_0;
	// Pathfinding.EuclideanEmbedding/<RecalculateCosts>c__AnonStorey120 Pathfinding.EuclideanEmbedding/<RecalculateCosts>c__AnonStorey120/<RecalculateCosts>c__AnonStorey11E/<RecalculateCosts>c__AnonStorey11F::<>f__ref$288
	U3CRecalculateCostsU3Ec__AnonStorey120_t3302164579 * ___U3CU3Ef__refU24288_1;
	// Pathfinding.EuclideanEmbedding/<RecalculateCosts>c__AnonStorey120/<RecalculateCosts>c__AnonStorey11E Pathfinding.EuclideanEmbedding/<RecalculateCosts>c__AnonStorey120/<RecalculateCosts>c__AnonStorey11E/<RecalculateCosts>c__AnonStorey11F::<>f__ref$286
	U3CRecalculateCostsU3Ec__AnonStorey11E_t853919929 * ___U3CU3Ef__refU24286_2;

public:
	inline static int32_t get_offset_of_costOffset_0() { return static_cast<int32_t>(offsetof(U3CRecalculateCostsU3Ec__AnonStorey11F_t1930950820, ___costOffset_0)); }
	inline uint32_t get_costOffset_0() const { return ___costOffset_0; }
	inline uint32_t* get_address_of_costOffset_0() { return &___costOffset_0; }
	inline void set_costOffset_0(uint32_t value)
	{
		___costOffset_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU24288_1() { return static_cast<int32_t>(offsetof(U3CRecalculateCostsU3Ec__AnonStorey11F_t1930950820, ___U3CU3Ef__refU24288_1)); }
	inline U3CRecalculateCostsU3Ec__AnonStorey120_t3302164579 * get_U3CU3Ef__refU24288_1() const { return ___U3CU3Ef__refU24288_1; }
	inline U3CRecalculateCostsU3Ec__AnonStorey120_t3302164579 ** get_address_of_U3CU3Ef__refU24288_1() { return &___U3CU3Ef__refU24288_1; }
	inline void set_U3CU3Ef__refU24288_1(U3CRecalculateCostsU3Ec__AnonStorey120_t3302164579 * value)
	{
		___U3CU3Ef__refU24288_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU24288_1, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU24286_2() { return static_cast<int32_t>(offsetof(U3CRecalculateCostsU3Ec__AnonStorey11F_t1930950820, ___U3CU3Ef__refU24286_2)); }
	inline U3CRecalculateCostsU3Ec__AnonStorey11E_t853919929 * get_U3CU3Ef__refU24286_2() const { return ___U3CU3Ef__refU24286_2; }
	inline U3CRecalculateCostsU3Ec__AnonStorey11E_t853919929 ** get_address_of_U3CU3Ef__refU24286_2() { return &___U3CU3Ef__refU24286_2; }
	inline void set_U3CU3Ef__refU24286_2(U3CRecalculateCostsU3Ec__AnonStorey11E_t853919929 * value)
	{
		___U3CU3Ef__refU24286_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU24286_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
