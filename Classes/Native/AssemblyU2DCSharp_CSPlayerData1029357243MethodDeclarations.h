﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CSPlayerData
struct CSPlayerData_t1029357243;
// System.Collections.Generic.List`1<CSHeroUnit>
struct List_1_t837576702;
// System.String
struct String_t;
// System.Collections.Generic.List`1<CSPlusAtt>
struct List_1_t341533415;
// CSPlusAtt
struct CSPlusAtt_t3268315159;
// CSHeroUnit
struct CSHeroUnit_t3764358446;
// ReplayMgr
struct ReplayMgr_t1549183121;
// System.Object
struct Il2CppObject;
// Newtonsoft.Json.Linq.JToken
struct JToken_t3412245951;
// Newtonsoft.Json.Linq.JObject
struct JObject_t1798544199;
// Newtonsoft.Json.Linq.JContainer
struct JContainer_t3364442311;
// Newtonsoft.Json.Linq.JArray
struct JArray_t3394795039;
// GameMgr
struct GameMgr_t1469029542;
// HeroEmbattleCfg
struct HeroEmbattleCfg_t4134124650;
// CSDatacfgManager
struct CSDatacfgManager_t1565254243;
// Formula/PlusAtt
struct PlusAtt_t2698014398;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_CSHeroUnit3764358446.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JObject1798544199.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JContainer3364442311.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JArray3394795039.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_GameMgr1469029542.h"
#include "AssemblyU2DCSharp_CSPlayerData1029357243.h"
#include "AssemblyU2DCSharp_CSDatacfgManager1565254243.h"

// System.Void CSPlayerData::.ctor()
extern "C"  void CSPlayerData__ctor_m2739443264 (CSPlayerData_t1029357243 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CSPlayerData::IsPVPRobat()
extern "C"  bool CSPlayerData_IsPVPRobat_m1086862192 (CSPlayerData_t1029357243 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CSHeroUnit> CSPlayerData::GetMonsterUnits()
extern "C"  List_1_t837576702 * CSPlayerData_GetMonsterUnits_m3541904168 (CSPlayerData_t1029357243 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSPlayerData::LoadData(System.String)
extern "C"  void CSPlayerData_LoadData_m3652240462 (CSPlayerData_t1029357243 * __this, String_t* ___jsonStr0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSPlayerData::UnLoadData()
extern "C"  void CSPlayerData_UnLoadData_m2816956909 (CSPlayerData_t1029357243 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CSPlusAtt> CSPlayerData::GetLeaderPlusAtt()
extern "C"  List_1_t341533415 * CSPlayerData_GetLeaderPlusAtt_m3398289732 (CSPlayerData_t1029357243 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CSPlusAtt> CSPlayerData::GetReinPlusAtts()
extern "C"  List_1_t341533415 * CSPlayerData_GetReinPlusAtts_m866482784 (CSPlayerData_t1029357243 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CSPlusAtt CSPlayerData::GetLearerAttUnit(System.String)
extern "C"  CSPlusAtt_t3268315159 * CSPlayerData_GetLearerAttUnit_m3051896298 (CSPlayerData_t1029357243 * __this, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CSPlusAtt CSPlayerData::GetReinAttUnit(System.String)
extern "C"  CSPlusAtt_t3268315159 * CSPlayerData_GetReinAttUnit_m1515788779 (CSPlayerData_t1029357243 * __this, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CSPlayerData::<GetMonsterUnits>m__3D7(CSHeroUnit,CSHeroUnit)
extern "C"  int32_t CSPlayerData_U3CGetMonsterUnitsU3Em__3D7_m1231628814 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ___hu10, CSHeroUnit_t3764358446 * ___hu21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ReplayMgr CSPlayerData::ilo_get_Instance1()
extern "C"  ReplayMgr_t1549183121 * CSPlayerData_ilo_get_Instance1_m4210625916 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object CSPlayerData::ilo_DeserializeObject2(System.String)
extern "C"  Il2CppObject * CSPlayerData_ilo_DeserializeObject2_m1611911106 (Il2CppObject * __this /* static, unused */, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken CSPlayerData::ilo_get_Item3(Newtonsoft.Json.Linq.JObject,System.String)
extern "C"  JToken_t3412245951 * CSPlayerData_ilo_get_Item3_m3188360867 (Il2CppObject * __this /* static, unused */, JObject_t1798544199 * ____this0, String_t* ___propertyName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CSPlayerData::ilo_get_Count4(Newtonsoft.Json.Linq.JContainer)
extern "C"  int32_t CSPlayerData_ilo_get_Count4_m1674735663 (Il2CppObject * __this /* static, unused */, JContainer_t3364442311 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken CSPlayerData::ilo_get_Item5(Newtonsoft.Json.Linq.JArray,System.Int32)
extern "C"  JToken_t3412245951 * CSPlayerData_ilo_get_Item5_m1337134534 (Il2CppObject * __this /* static, unused */, JArray_t3394795039 * ____this0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSPlayerData::ilo_Log6(System.Object,System.Boolean)
extern "C"  void CSPlayerData_ilo_Log6_m3146183250 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CSPlayerData::ilo_get_IsLeaderPlusAtt7(GameMgr)
extern "C"  bool CSPlayerData_ilo_get_IsLeaderPlusAtt7_m2021106517 (Il2CppObject * __this /* static, unused */, GameMgr_t1469029542 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CSHeroUnit> CSPlayerData::ilo_GetMonsterUnits8(CSPlayerData)
extern "C"  List_1_t837576702 * CSPlayerData_ilo_GetMonsterUnits8_m4186503690 (Il2CppObject * __this /* static, unused */, CSPlayerData_t1029357243 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CSPlayerData::ilo_get_IsReinPlusAtt9(GameMgr)
extern "C"  bool CSPlayerData_ilo_get_IsReinPlusAtt9_m239033508 (Il2CppObject * __this /* static, unused */, GameMgr_t1469029542 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 CSPlayerData::ilo_get_hp10(CSHeroUnit)
extern "C"  uint32_t CSPlayerData_ilo_get_hp10_m792770996 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 CSPlayerData::ilo_get_phy_def11(CSHeroUnit)
extern "C"  uint32_t CSPlayerData_ilo_get_phy_def11_m3580738176 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 CSPlayerData::ilo_get_mag_def12(CSHeroUnit)
extern "C"  uint32_t CSPlayerData_ilo_get_mag_def12_m3535676499 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HeroEmbattleCfg CSPlayerData::ilo_GetHeroEmbattleCfg13(CSDatacfgManager,System.Int32)
extern "C"  HeroEmbattleCfg_t4134124650 * CSPlayerData_ilo_GetHeroEmbattleCfg13_m2901355108 (Il2CppObject * __this /* static, unused */, CSDatacfgManager_t1565254243 * ____this0, int32_t ___id1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Formula/PlusAtt CSPlayerData::ilo_ComputReinPlusAtt14(System.UInt32,System.UInt32,System.UInt32,System.UInt32,System.Int32)
extern "C"  PlusAtt_t2698014398 * CSPlayerData_ilo_ComputReinPlusAtt14_m1879996827 (Il2CppObject * __this /* static, unused */, uint32_t ___bsHp0, uint32_t ___bsAttack1, uint32_t ___bsPhy_def2, uint32_t ___bsMag_def3, int32_t ___ratio4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 CSPlayerData::ilo_get_location15(CSHeroUnit)
extern "C"  uint32_t CSPlayerData_ilo_get_location15_m3796285094 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
