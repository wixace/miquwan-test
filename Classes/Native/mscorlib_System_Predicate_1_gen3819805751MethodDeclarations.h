﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Predicate_1_gen3781873254MethodDeclarations.h"

// System.Void System.Predicate`1<FS_ShadowSimple>::.ctor(System.Object,System.IntPtr)
#define Predicate_1__ctor_m480199675(__this, ___object0, ___method1, method) ((  void (*) (Predicate_1_t3819805751 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Predicate_1__ctor_m231416842_gshared)(__this, ___object0, ___method1, method)
// System.Boolean System.Predicate`1<FS_ShadowSimple>::Invoke(T)
#define Predicate_1_Invoke_m528318315(__this, ___obj0, method) ((  bool (*) (Predicate_1_t3819805751 *, FS_ShadowSimple_t4208748868 *, const MethodInfo*))Predicate_1_Invoke_m1328901537_gshared)(__this, ___obj0, method)
// System.IAsyncResult System.Predicate`1<FS_ShadowSimple>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Predicate_1_BeginInvoke_m1671905598(__this, ___obj0, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Predicate_1_t3819805751 *, FS_ShadowSimple_t4208748868 *, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))Predicate_1_BeginInvoke_m2038073176_gshared)(__this, ___obj0, ___callback1, ___object2, method)
// System.Boolean System.Predicate`1<FS_ShadowSimple>::EndInvoke(System.IAsyncResult)
#define Predicate_1_EndInvoke_m821110089(__this, ___result0, method) ((  bool (*) (Predicate_1_t3819805751 *, Il2CppObject *, const MethodInfo*))Predicate_1_EndInvoke_m3970497007_gshared)(__this, ___result0, method)
