﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// Mihua.SDK.PayInfo
struct PayInfo_t1775308120;
// System.Action
struct Action_t3771233898;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PluginAiLeXinYou
struct  PluginAiLeXinYou_t3790076438  : public MonoBehaviour_t667441552
{
public:
	// System.String PluginAiLeXinYou::userId
	String_t* ___userId_2;
	// System.String PluginAiLeXinYou::uname
	String_t* ___uname_3;
	// System.String PluginAiLeXinYou::configId
	String_t* ___configId_4;
	// System.String PluginAiLeXinYou::appId
	String_t* ___appId_5;
	// System.String PluginAiLeXinYou::gid
	String_t* ___gid_6;
	// System.String PluginAiLeXinYou::sid
	String_t* ___sid_7;
	// Mihua.SDK.PayInfo PluginAiLeXinYou::iapInfo
	PayInfo_t1775308120 * ___iapInfo_8;

public:
	inline static int32_t get_offset_of_userId_2() { return static_cast<int32_t>(offsetof(PluginAiLeXinYou_t3790076438, ___userId_2)); }
	inline String_t* get_userId_2() const { return ___userId_2; }
	inline String_t** get_address_of_userId_2() { return &___userId_2; }
	inline void set_userId_2(String_t* value)
	{
		___userId_2 = value;
		Il2CppCodeGenWriteBarrier(&___userId_2, value);
	}

	inline static int32_t get_offset_of_uname_3() { return static_cast<int32_t>(offsetof(PluginAiLeXinYou_t3790076438, ___uname_3)); }
	inline String_t* get_uname_3() const { return ___uname_3; }
	inline String_t** get_address_of_uname_3() { return &___uname_3; }
	inline void set_uname_3(String_t* value)
	{
		___uname_3 = value;
		Il2CppCodeGenWriteBarrier(&___uname_3, value);
	}

	inline static int32_t get_offset_of_configId_4() { return static_cast<int32_t>(offsetof(PluginAiLeXinYou_t3790076438, ___configId_4)); }
	inline String_t* get_configId_4() const { return ___configId_4; }
	inline String_t** get_address_of_configId_4() { return &___configId_4; }
	inline void set_configId_4(String_t* value)
	{
		___configId_4 = value;
		Il2CppCodeGenWriteBarrier(&___configId_4, value);
	}

	inline static int32_t get_offset_of_appId_5() { return static_cast<int32_t>(offsetof(PluginAiLeXinYou_t3790076438, ___appId_5)); }
	inline String_t* get_appId_5() const { return ___appId_5; }
	inline String_t** get_address_of_appId_5() { return &___appId_5; }
	inline void set_appId_5(String_t* value)
	{
		___appId_5 = value;
		Il2CppCodeGenWriteBarrier(&___appId_5, value);
	}

	inline static int32_t get_offset_of_gid_6() { return static_cast<int32_t>(offsetof(PluginAiLeXinYou_t3790076438, ___gid_6)); }
	inline String_t* get_gid_6() const { return ___gid_6; }
	inline String_t** get_address_of_gid_6() { return &___gid_6; }
	inline void set_gid_6(String_t* value)
	{
		___gid_6 = value;
		Il2CppCodeGenWriteBarrier(&___gid_6, value);
	}

	inline static int32_t get_offset_of_sid_7() { return static_cast<int32_t>(offsetof(PluginAiLeXinYou_t3790076438, ___sid_7)); }
	inline String_t* get_sid_7() const { return ___sid_7; }
	inline String_t** get_address_of_sid_7() { return &___sid_7; }
	inline void set_sid_7(String_t* value)
	{
		___sid_7 = value;
		Il2CppCodeGenWriteBarrier(&___sid_7, value);
	}

	inline static int32_t get_offset_of_iapInfo_8() { return static_cast<int32_t>(offsetof(PluginAiLeXinYou_t3790076438, ___iapInfo_8)); }
	inline PayInfo_t1775308120 * get_iapInfo_8() const { return ___iapInfo_8; }
	inline PayInfo_t1775308120 ** get_address_of_iapInfo_8() { return &___iapInfo_8; }
	inline void set_iapInfo_8(PayInfo_t1775308120 * value)
	{
		___iapInfo_8 = value;
		Il2CppCodeGenWriteBarrier(&___iapInfo_8, value);
	}
};

struct PluginAiLeXinYou_t3790076438_StaticFields
{
public:
	// System.Action PluginAiLeXinYou::<>f__am$cache7
	Action_t3771233898 * ___U3CU3Ef__amU24cache7_9;
	// System.Action PluginAiLeXinYou::<>f__am$cache8
	Action_t3771233898 * ___U3CU3Ef__amU24cache8_10;
	// System.Action PluginAiLeXinYou::<>f__am$cache9
	Action_t3771233898 * ___U3CU3Ef__amU24cache9_11;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache7_9() { return static_cast<int32_t>(offsetof(PluginAiLeXinYou_t3790076438_StaticFields, ___U3CU3Ef__amU24cache7_9)); }
	inline Action_t3771233898 * get_U3CU3Ef__amU24cache7_9() const { return ___U3CU3Ef__amU24cache7_9; }
	inline Action_t3771233898 ** get_address_of_U3CU3Ef__amU24cache7_9() { return &___U3CU3Ef__amU24cache7_9; }
	inline void set_U3CU3Ef__amU24cache7_9(Action_t3771233898 * value)
	{
		___U3CU3Ef__amU24cache7_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache7_9, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache8_10() { return static_cast<int32_t>(offsetof(PluginAiLeXinYou_t3790076438_StaticFields, ___U3CU3Ef__amU24cache8_10)); }
	inline Action_t3771233898 * get_U3CU3Ef__amU24cache8_10() const { return ___U3CU3Ef__amU24cache8_10; }
	inline Action_t3771233898 ** get_address_of_U3CU3Ef__amU24cache8_10() { return &___U3CU3Ef__amU24cache8_10; }
	inline void set_U3CU3Ef__amU24cache8_10(Action_t3771233898 * value)
	{
		___U3CU3Ef__amU24cache8_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache8_10, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache9_11() { return static_cast<int32_t>(offsetof(PluginAiLeXinYou_t3790076438_StaticFields, ___U3CU3Ef__amU24cache9_11)); }
	inline Action_t3771233898 * get_U3CU3Ef__amU24cache9_11() const { return ___U3CU3Ef__amU24cache9_11; }
	inline Action_t3771233898 ** get_address_of_U3CU3Ef__amU24cache9_11() { return &___U3CU3Ef__amU24cache9_11; }
	inline void set_U3CU3Ef__amU24cache9_11(Action_t3771233898 * value)
	{
		___U3CU3Ef__amU24cache9_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache9_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
