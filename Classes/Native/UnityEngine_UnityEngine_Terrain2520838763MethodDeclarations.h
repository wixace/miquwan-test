﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Terrain
struct Terrain_t2520838763;
// UnityEngine.TerrainData
struct TerrainData_t865146677;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_Terrain2520838763.h"

// UnityEngine.TerrainData UnityEngine.Terrain::get_terrainData()
extern "C"  TerrainData_t865146677 * Terrain_get_terrainData_m315613379 (Terrain_t2520838763 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Terrain::GetPosition()
extern "C"  Vector3_t4282066566  Terrain_GetPosition_m1664151035 (Terrain_t2520838763 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Terrain::INTERNAL_CALL_GetPosition(UnityEngine.Terrain,UnityEngine.Vector3&)
extern "C"  void Terrain_INTERNAL_CALL_GetPosition_m1261296487 (Il2CppObject * __this /* static, unused */, Terrain_t2520838763 * ___self0, Vector3_t4282066566 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
