﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.Serialization.JsonFx.JsonSpecifiedPropertyAttribute
struct JsonSpecifiedPropertyAttribute_t1712644481;
// System.String
struct String_t;
// System.Reflection.MemberInfo
struct MemberInfo_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_MemberInfo3995515898.h"

// System.String Pathfinding.Serialization.JsonFx.JsonSpecifiedPropertyAttribute::get_SpecifiedProperty()
extern "C"  String_t* JsonSpecifiedPropertyAttribute_get_SpecifiedProperty_m3397186727 (JsonSpecifiedPropertyAttribute_t1712644481 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Pathfinding.Serialization.JsonFx.JsonSpecifiedPropertyAttribute::GetJsonSpecifiedProperty(System.Reflection.MemberInfo)
extern "C"  String_t* JsonSpecifiedPropertyAttribute_GetJsonSpecifiedProperty_m1760565138 (Il2CppObject * __this /* static, unused */, MemberInfo_t * ___memberInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
