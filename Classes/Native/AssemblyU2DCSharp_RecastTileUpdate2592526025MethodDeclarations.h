﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// RecastTileUpdate
struct RecastTileUpdate_t2592526025;
// System.Action`1<UnityEngine.Bounds>
struct Action_1_t3107457985;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_RecastTileUpdate2592526025.h"

// System.Void RecastTileUpdate::.ctor()
extern "C"  void RecastTileUpdate__ctor_m2873092594 (RecastTileUpdate_t2592526025 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RecastTileUpdate::add_OnNeedUpdates(System.Action`1<UnityEngine.Bounds>)
extern "C"  void RecastTileUpdate_add_OnNeedUpdates_m1619939247 (Il2CppObject * __this /* static, unused */, Action_1_t3107457985 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RecastTileUpdate::remove_OnNeedUpdates(System.Action`1<UnityEngine.Bounds>)
extern "C"  void RecastTileUpdate_remove_OnNeedUpdates_m2604217706 (Il2CppObject * __this /* static, unused */, Action_1_t3107457985 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RecastTileUpdate::Start()
extern "C"  void RecastTileUpdate_Start_m1820230386 (RecastTileUpdate_t2592526025 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RecastTileUpdate::OnDestroy()
extern "C"  void RecastTileUpdate_OnDestroy_m1682404843 (RecastTileUpdate_t2592526025 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RecastTileUpdate::ScheduleUpdate()
extern "C"  void RecastTileUpdate_ScheduleUpdate_m563033618 (RecastTileUpdate_t2592526025 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RecastTileUpdate::ilo_ScheduleUpdate1(RecastTileUpdate)
extern "C"  void RecastTileUpdate_ilo_ScheduleUpdate1_m3317443845 (Il2CppObject * __this /* static, unused */, RecastTileUpdate_t2592526025 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
