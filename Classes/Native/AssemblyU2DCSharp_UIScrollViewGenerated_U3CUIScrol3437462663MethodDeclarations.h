﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIScrollViewGenerated/<UIScrollView_onDragFinished_GetDelegate_member17_arg0>c__AnonStoreyCC
struct U3CUIScrollView_onDragFinished_GetDelegate_member17_arg0U3Ec__AnonStoreyCC_t3437462663;

#include "codegen/il2cpp-codegen.h"

// System.Void UIScrollViewGenerated/<UIScrollView_onDragFinished_GetDelegate_member17_arg0>c__AnonStoreyCC::.ctor()
extern "C"  void U3CUIScrollView_onDragFinished_GetDelegate_member17_arg0U3Ec__AnonStoreyCC__ctor_m3259999732 (U3CUIScrollView_onDragFinished_GetDelegate_member17_arg0U3Ec__AnonStoreyCC_t3437462663 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIScrollViewGenerated/<UIScrollView_onDragFinished_GetDelegate_member17_arg0>c__AnonStoreyCC::<>m__15D()
extern "C"  void U3CUIScrollView_onDragFinished_GetDelegate_member17_arg0U3Ec__AnonStoreyCC_U3CU3Em__15D_m3681457253 (U3CUIScrollView_onDragFinished_GetDelegate_member17_arg0U3Ec__AnonStoreyCC_t3437462663 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
