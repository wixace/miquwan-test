﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PluginReYun
struct PluginReYun_t2552860172;
// System.String
struct String_t;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// PluginReYun/strutDict[]
struct strutDictU5BU5D_t1829737458;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// ChannelConfigCfg
struct ChannelConfigCfg_t4272979999;
// CSDatacfgManager
struct CSDatacfgManager_t1565254243;
// VersionMgr
struct VersionMgr_t1322950208;
// CEvent.EventFunc`1<CEvent.ZEvent>
struct EventFunc_1_t1114914310;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_Gender2129321697.h"
#include "AssemblyU2DCSharp_QuestStatus2006567764.h"
#include "AssemblyU2DCSharp_CSDatacfgManager1565254243.h"
#include "AssemblyU2DCSharp_PluginReYun2552860172.h"
#include "AssemblyU2DCSharp_VersionMgr1322950208.h"

// System.Void PluginReYun::.ctor()
extern "C"  void PluginReYun__ctor_m156433119 (PluginReYun_t2552860172 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PluginReYun::get_APPKEY()
extern "C"  String_t* PluginReYun_get_APPKEY_m2510709385 (PluginReYun_t2552860172 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PluginReYun::get_TRACK_APPKEY()
extern "C"  String_t* PluginReYun_get_TRACK_APPKEY_m757154717 (PluginReYun_t2552860172 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PluginReYun::get_TRACKINGIO_APPKEY()
extern "C"  String_t* PluginReYun_get_TRACKINGIO_APPKEY_m1240887127 (PluginReYun_t2552860172 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PluginReYun::get_channelId()
extern "C"  String_t* PluginReYun_get_channelId_m941845077 (PluginReYun_t2552860172 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginReYun::Init()
extern "C"  void PluginReYun_Init_m2588982165 (PluginReYun_t2552860172 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginReYun::OnDestory()
extern "C"  void PluginReYun_OnDestory_m1865768690 (PluginReYun_t2552860172 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginReYun::CreatRole(CEvent.ZEvent)
extern "C"  void PluginReYun_CreatRole_m2653014815 (PluginReYun_t2552860172 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginReYun::initWithAppId(System.String,System.String)
extern "C"  void PluginReYun_initWithAppId_m4028591227 (Il2CppObject * __this /* static, unused */, String_t* ___appId0, String_t* ___channelId1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginReYun::setNRegisterWithAccountID(System.String,Gender,System.String,System.String,System.String,System.String)
extern "C"  void PluginReYun_setNRegisterWithAccountID_m1402858819 (Il2CppObject * __this /* static, unused */, String_t* ___account0, int32_t ___genders1, String_t* ___age2, String_t* ___serverId3, String_t* ___accountType4, String_t* ___rolename5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginReYun::setLoginWithAccountID(System.String,Gender,System.String,System.String,System.Int32,System.String)
extern "C"  void PluginReYun_setLoginWithAccountID_m1544067808 (Il2CppObject * __this /* static, unused */, String_t* ___account0, int32_t ___genders1, String_t* ___age2, String_t* ___serverId3, int32_t ___level4, String_t* ___rolename5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginReYun::setEconomy(System.String,System.Int32,System.Single)
extern "C"  void PluginReYun_setEconomy_m280506907 (Il2CppObject * __this /* static, unused */, String_t* ___itemName0, int32_t ___itemAmount1, float ___itemTotalPrice2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginReYun::setNQuest(System.String,QuestStatus,System.String)
extern "C"  void PluginReYun_setNQuest_m3650006403 (Il2CppObject * __this /* static, unused */, String_t* ___questId0, int32_t ___questStatu1, String_t* ___questType2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginReYun::setNEvent(System.String,System.Int32,PluginReYun/strutDict[])
extern "C"  void PluginReYun_setNEvent_m3365273287 (Il2CppObject * __this /* static, unused */, String_t* ___eventName0, int32_t ___number1, strutDictU5BU5D_t1829737458* ___dict2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginReYun::setNewEvent(System.String,System.String)
extern "C"  void PluginReYun_setNewEvent_m3879428261 (Il2CppObject * __this /* static, unused */, String_t* ___eventName0, String_t* ___json1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PluginReYun::getDeviceId()
extern "C"  String_t* PluginReYun_getDeviceId_m4120897863 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginReYun::setGamePrintLog(System.Boolean)
extern "C"  void PluginReYun_setGamePrintLog_m1854432959 (Il2CppObject * __this /* static, unused */, bool ___print0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginReYun::initTrackWithappKey(System.String,System.String)
extern "C"  void PluginReYun_initTrackWithappKey_m3849230754 (Il2CppObject * __this /* static, unused */, String_t* ___appKey0, String_t* ___channelId1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginReYun::setTrackRegisterWithAccountID(System.String)
extern "C"  void PluginReYun_setTrackRegisterWithAccountID_m1674227151 (Il2CppObject * __this /* static, unused */, String_t* ___account0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginReYun::setTrackLoginWithAccountID(System.String)
extern "C"  void PluginReYun_setTrackLoginWithAccountID_m2977350811 (Il2CppObject * __this /* static, unused */, String_t* ___account0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginReYun::setTrackEvent(System.String)
extern "C"  void PluginReYun_setTrackEvent_m2261604308 (Il2CppObject * __this /* static, unused */, String_t* ___eventName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PluginReYun::getTrackDeviceId()
extern "C"  String_t* PluginReYun_getTrackDeviceId_m2772541736 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginReYun::setTrackPrintLog(System.Boolean)
extern "C"  void PluginReYun_setTrackPrintLog_m977793948 (Il2CppObject * __this /* static, unused */, bool ___print0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginReYun::initTrackIOWithappKey(System.String,System.String)
extern "C"  void PluginReYun_initTrackIOWithappKey_m3596376956 (Il2CppObject * __this /* static, unused */, String_t* ___appKey0, String_t* ___channelId1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginReYun::setTrackIORegisterWithAccountID(System.String)
extern "C"  void PluginReYun_setTrackIORegisterWithAccountID_m2919978165 (Il2CppObject * __this /* static, unused */, String_t* ___account0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginReYun::setTrackIOLoginWithAccountID(System.String)
extern "C"  void PluginReYun_setTrackIOLoginWithAccountID_m154400629 (Il2CppObject * __this /* static, unused */, String_t* ___account0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginReYun::setTrackIOEvent(System.String)
extern "C"  void PluginReYun_setTrackIOEvent_m2568260282 (Il2CppObject * __this /* static, unused */, String_t* ___eventName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PluginReYun::getTrackIODeviceId()
extern "C"  String_t* PluginReYun_getTrackIODeviceId_m1687174478 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginReYun::setTrackIOPrintLog(System.Boolean)
extern "C"  void PluginReYun_setTrackIOPrintLog_m2655994754 (Il2CppObject * __this /* static, unused */, bool ___print0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginReYun::Game_Init(System.String,System.String)
extern "C"  void PluginReYun_Game_Init_m609685348 (PluginReYun_t2552860172 * __this, String_t* ___appId0, String_t* ___channelId1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginReYun::Game_Register(System.String,Gender,System.String,System.String,System.String,System.String)
extern "C"  void PluginReYun_Game_Register_m2802594208 (PluginReYun_t2552860172 * __this, String_t* ___account0, int32_t ___gender1, String_t* ___age2, String_t* ___serverId3, String_t* ___accountType4, String_t* ___rolename5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginReYun::Game_Login(System.String,Gender,System.String,System.String,System.Int32,System.String)
extern "C"  void PluginReYun_Game_Login_m3200248919 (PluginReYun_t2552860172 * __this, String_t* ___account0, int32_t ___genders1, String_t* ___age2, String_t* ___serverId3, int32_t ___level4, String_t* ___rolename5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginReYun::Game_SetEconomy(System.String,System.Int32,System.Single)
extern "C"  void PluginReYun_Game_SetEconomy_m2503579136 (PluginReYun_t2552860172 * __this, String_t* ___itemName0, int32_t ___itemAmount1, float ___itemTotalPrice2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginReYun::Game_Quest(System.String,QuestStatus,System.String)
extern "C"  void PluginReYun_Game_Quest_m2564342748 (PluginReYun_t2552860172 * __this, String_t* ___questId0, int32_t ___questStatu1, String_t* ___questType2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginReYun::Game_SetEvent(System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C"  void PluginReYun_Game_SetEvent_m3557399127 (PluginReYun_t2552860172 * __this, String_t* ___eventName0, Dictionary_2_t827649927 * ___dict1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PluginReYun::Game_getDeviceId()
extern "C"  String_t* PluginReYun_Game_getDeviceId_m3153897020 (PluginReYun_t2552860172 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PluginReYun::GetDeviceId()
extern "C"  String_t* PluginReYun_GetDeviceId_m624892199 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginReYun::Game_setPrintLog(System.Boolean)
extern "C"  void PluginReYun_Game_setPrintLog_m159666248 (PluginReYun_t2552860172 * __this, bool ___print0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginReYun::Track_Init(System.String,System.String)
extern "C"  void PluginReYun_Track_Init_m1283343349 (PluginReYun_t2552860172 * __this, String_t* ___appKey0, String_t* ___channelId1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginReYun::Track_Register(System.String)
extern "C"  void PluginReYun_Track_Register_m2936532358 (PluginReYun_t2552860172 * __this, String_t* ___account0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginReYun::Track_Login(System.String)
extern "C"  void PluginReYun_Track_Login_m2933312304 (PluginReYun_t2552860172 * __this, String_t* ___account0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginReYun::Track_SetEvent(System.String)
extern "C"  void PluginReYun_Track_SetEvent_m2278246449 (PluginReYun_t2552860172 * __this, String_t* ___eventName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PluginReYun::Track_getDeviceId()
extern "C"  String_t* PluginReYun_Track_getDeviceId_m1603647987 (PluginReYun_t2552860172 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginReYun::Track_setPrintLog(System.Boolean)
extern "C"  void PluginReYun_Track_setPrintLog_m3658384793 (PluginReYun_t2552860172 * __this, bool ___print0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginReYun::TrackingIO_Init(System.String,System.String)
extern "C"  void PluginReYun_TrackingIO_Init_m3211303887 (PluginReYun_t2552860172 * __this, String_t* ___appKey0, String_t* ___channelId1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginReYun::TrackingIO_Register(System.String)
extern "C"  void PluginReYun_TrackingIO_Register_m4025183712 (PluginReYun_t2552860172 * __this, String_t* ___account0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginReYun::TrackingIO_Login(System.String)
extern "C"  void PluginReYun_TrackingIO_Login_m712554774 (PluginReYun_t2552860172 * __this, String_t* ___account0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginReYun::TrackingIO_SetEvent(System.String)
extern "C"  void PluginReYun_TrackingIO_SetEvent_m3366897803 (PluginReYun_t2552860172 * __this, String_t* ___eventName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PluginReYun::TrackingIO_getDeviceId()
extern "C"  String_t* PluginReYun_TrackingIO_getDeviceId_m3324544103 (PluginReYun_t2552860172 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginReYun::TrackingIO_setPrintLog(System.Boolean)
extern "C"  void PluginReYun_TrackingIO_setPrintLog_m36063475 (PluginReYun_t2552860172 * __this, bool ___print0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ChannelConfigCfg PluginReYun::ilo_GetChannelConfigCfg1(CSDatacfgManager,System.Int32)
extern "C"  ChannelConfigCfg_t4272979999 * PluginReYun_ilo_GetChannelConfigCfg1_m4251153956 (Il2CppObject * __this /* static, unused */, CSDatacfgManager_t1565254243 * ____this0, int32_t ___id1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionMgr PluginReYun::ilo_get_Instance2()
extern "C"  VersionMgr_t1322950208 * PluginReYun_ilo_get_Instance2_m3893465147 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginReYun::ilo_AddEventListener3(System.String,CEvent.EventFunc`1<CEvent.ZEvent>)
extern "C"  void PluginReYun_ilo_AddEventListener3_m2271175283 (Il2CppObject * __this /* static, unused */, String_t* ___eventType0, EventFunc_1_t1114914310 * ___fun1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PluginReYun::ilo_get_APPKEY4(PluginReYun)
extern "C"  String_t* PluginReYun_ilo_get_APPKEY4_m3123722828 (Il2CppObject * __this /* static, unused */, PluginReYun_t2552860172 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PluginReYun::ilo_get_channelId5(PluginReYun)
extern "C"  String_t* PluginReYun_ilo_get_channelId5_m729948017 (Il2CppObject * __this /* static, unused */, PluginReYun_t2552860172 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginReYun::ilo_Game_Init6(PluginReYun,System.String,System.String)
extern "C"  void PluginReYun_ilo_Game_Init6_m2557831501 (Il2CppObject * __this /* static, unused */, PluginReYun_t2552860172 * ____this0, String_t* ___appId1, String_t* ___channelId2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PluginReYun::ilo_get_TRACK_APPKEY7(PluginReYun)
extern "C"  String_t* PluginReYun_ilo_get_TRACK_APPKEY7_m494266365 (Il2CppObject * __this /* static, unused */, PluginReYun_t2552860172 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginReYun::ilo_initWithAppId8(System.String,System.String)
extern "C"  void PluginReYun_ilo_initWithAppId8_m3401537588 (Il2CppObject * __this /* static, unused */, String_t* ___appId0, String_t* ___channelId1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginReYun::ilo_get_isSdkLogin9(VersionMgr)
extern "C"  bool PluginReYun_ilo_get_isSdkLogin9_m730849621 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginReYun::ilo_setNQuest10(System.String,QuestStatus,System.String)
extern "C"  void PluginReYun_ilo_setNQuest10_m2120938359 (Il2CppObject * __this /* static, unused */, String_t* ___questId0, int32_t ___questStatu1, String_t* ___questType2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginReYun::ilo_setNewEvent11(System.String,System.String)
extern "C"  void PluginReYun_ilo_setNewEvent11_m1860348152 (Il2CppObject * __this /* static, unused */, String_t* ___eventName0, String_t* ___json1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginReYun::ilo_initTrackWithappKey12(System.String,System.String)
extern "C"  void PluginReYun_ilo_initTrackWithappKey12_m1379540820 (Il2CppObject * __this /* static, unused */, String_t* ___appKey0, String_t* ___channelId1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginReYun::ilo_setTrackRegisterWithAccountID13(System.String)
extern "C"  void PluginReYun_ilo_setTrackRegisterWithAccountID13_m2095888096 (Il2CppObject * __this /* static, unused */, String_t* ___account0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginReYun::ilo_setTrackLoginWithAccountID14(System.String)
extern "C"  void PluginReYun_ilo_setTrackLoginWithAccountID14_m1964502917 (Il2CppObject * __this /* static, unused */, String_t* ___account0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginReYun::ilo_setTrackPrintLog15(System.Boolean)
extern "C"  void PluginReYun_ilo_setTrackPrintLog15_m124962867 (Il2CppObject * __this /* static, unused */, bool ___print0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginReYun::ilo_initTrackIOWithappKey16(System.String,System.String)
extern "C"  void PluginReYun_ilo_initTrackIOWithappKey16_m2553868394 (Il2CppObject * __this /* static, unused */, String_t* ___appKey0, String_t* ___channelId1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginReYun::ilo_setTrackIORegisterWithAccountID17(System.String)
extern "C"  void PluginReYun_ilo_setTrackIORegisterWithAccountID17_m3628581634 (Il2CppObject * __this /* static, unused */, String_t* ___account0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginReYun::ilo_setTrackIOLoginWithAccountID18(System.String)
extern "C"  void PluginReYun_ilo_setTrackIOLoginWithAccountID18_m3928392731 (Il2CppObject * __this /* static, unused */, String_t* ___account0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
