﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AssetDownMgr
struct AssetDownMgr_t4130275622;
// System.String
struct String_t;
// System.Collections.Generic.List`1<FileInfoRes>
struct List_1_t2585245350;
// AssetDownMgr/SetBytes
struct SetBytes_t2856136722;
// System.Action
struct Action_t3771233898;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// FileInfoRes
struct FileInfoRes_t1217059798;
// Mihua.Net.UrlLoader
struct UrlLoader_t2490729496;
// System.Object
struct Il2CppObject;
// LoadingBoardMgr
struct LoadingBoardMgr_t303632014;
// Mihua.Assets.UnzipAssetMgr
struct UnzipAssetMgr_t1276665662;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_AssetDownMgr_SetBytes2856136722.h"
#include "System_Core_System_Action3771233898.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_FileInfoRes1217059798.h"
#include "AssemblyU2DCSharp_Mihua_Net_UrlLoader2490729496.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_LoadingBoardMgr303632014.h"
#include "AssemblyU2DCSharp_AssetDownMgr4130275622.h"
#include "AssemblyU2DCSharp_Mihua_Assets_UnzipAssetMgr1276665662.h"

// System.Void AssetDownMgr::.ctor()
extern "C"  void AssetDownMgr__ctor_m2731596853 (AssetDownMgr_t4130275622 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AssetDownMgr::IZUpdate.ZUpdate()
extern "C"  void AssetDownMgr_IZUpdate_ZUpdate_m3287468734 (AssetDownMgr_t4130275622 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// AssetDownMgr AssetDownMgr::get_Instance()
extern "C"  AssetDownMgr_t4130275622 * AssetDownMgr_get_Instance_m496262564 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 AssetDownMgr::get_GetMinutes()
extern "C"  int64_t AssetDownMgr_get_GetMinutes_m215852784 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single AssetDownMgr::get_Progress()
extern "C"  float AssetDownMgr_get_Progress_m33737393 (AssetDownMgr_t4130275622 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String AssetDownMgr::get_ProgressMsg()
extern "C"  String_t* AssetDownMgr_get_ProgressMsg_m4058084411 (AssetDownMgr_t4130275622 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single AssetDownMgr::get_WriteProgress()
extern "C"  float AssetDownMgr_get_WriteProgress_m3418062922 (AssetDownMgr_t4130275622 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AssetDownMgr::Init()
extern "C"  void AssetDownMgr_Init_m2810599295 (AssetDownMgr_t4130275622 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AssetDownMgr::AddUpdateData(System.Collections.Generic.List`1<FileInfoRes>,AssetDownMgr/SetBytes,System.Action)
extern "C"  void AssetDownMgr_AddUpdateData_m264120466 (AssetDownMgr_t4130275622 * __this, List_1_t2585245350 * ___updateList0, SetBytes_t2856136722 * ___downFinishCall1, Action_t3771233898 * ___writeDownFinishCall2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AssetDownMgr::AddUpdateData(System.String,AssetDownMgr/SetBytes,System.Boolean)
extern "C"  void AssetDownMgr_AddUpdateData_m2131171976 (AssetDownMgr_t4130275622 * __this, String_t* ___path0, SetBytes_t2856136722 * ___downFinishCall1, bool ___isInsert2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AssetDownMgr::StartDown()
extern "C"  void AssetDownMgr_StartDown_m1813097335 (AssetDownMgr_t4130275622 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AssetDownMgr::Clear()
extern "C"  void AssetDownMgr_Clear_m137730144 (AssetDownMgr_t4130275622 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AssetDownMgr::DownNext()
extern "C"  bool AssetDownMgr_DownNext_m1809632152 (AssetDownMgr_t4130275622 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AssetDownMgr::SaveBytes(System.Byte[],FileInfoRes)
extern "C"  void AssetDownMgr_SaveBytes_m313744958 (AssetDownMgr_t4130275622 * __this, ByteU5BU5D_t4260760469* ___bytes0, FileInfoRes_t1217059798 * ___assetData1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AssetDownMgr::OnUnzip(System.Byte[],System.String)
extern "C"  void AssetDownMgr_OnUnzip_m4030191849 (AssetDownMgr_t4130275622 * __this, ByteU5BU5D_t4260760469* ___bytes0, String_t* ___assetName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AssetDownMgr::SaveBytesToFile(System.Byte[],System.String)
extern "C"  void AssetDownMgr_SaveBytesToFile_m1746655885 (AssetDownMgr_t4130275622 * __this, ByteU5BU5D_t4260760469* ___bytes0, String_t* ___assetName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AssetDownMgr::<ZUpdate>m__3F6()
extern "C"  void AssetDownMgr_U3CZUpdateU3Em__3F6_m1283999104 (AssetDownMgr_t4130275622 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String AssetDownMgr::ilo_get_error1(Mihua.Net.UrlLoader)
extern "C"  String_t* AssetDownMgr_ilo_get_error1_m3624758912 (Il2CppObject * __this /* static, unused */, UrlLoader_t2490729496 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AssetDownMgr::ilo_Log2(System.Object,System.Boolean)
extern "C"  void AssetDownMgr_ilo_Log2_m3468692665 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// LoadingBoardMgr AssetDownMgr::ilo_get_Instance3()
extern "C"  LoadingBoardMgr_t303632014 * AssetDownMgr_ilo_get_Instance3_m2089431664 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AssetDownMgr::ilo_OpenDialog4(LoadingBoardMgr,System.Action)
extern "C"  void AssetDownMgr_ilo_OpenDialog4_m1909140569 (Il2CppObject * __this /* static, unused */, LoadingBoardMgr_t303632014 * ____this0, Action_t3771233898 * ___call1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String AssetDownMgr::ilo_get_url5(Mihua.Net.UrlLoader)
extern "C"  String_t* AssetDownMgr_ilo_get_url5_m3861054947 (Il2CppObject * __this /* static, unused */, UrlLoader_t2490729496 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AssetDownMgr::ilo_SaveBytes6(AssetDownMgr,System.Byte[],FileInfoRes)
extern "C"  void AssetDownMgr_ilo_SaveBytes6_m2194346461 (Il2CppObject * __this /* static, unused */, AssetDownMgr_t4130275622 * ____this0, ByteU5BU5D_t4260760469* ___bytes1, FileInfoRes_t1217059798 * ___assetData2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AssetDownMgr::ilo_Dispose7(Mihua.Net.UrlLoader)
extern "C"  void AssetDownMgr_ilo_Dispose7_m1414346717 (Il2CppObject * __this /* static, unused */, UrlLoader_t2490729496 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 AssetDownMgr::ilo_get_GetMinutes8()
extern "C"  int64_t AssetDownMgr_ilo_get_GetMinutes8_m2924799959 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 AssetDownMgr::ilo_get_UnZipFileCount9(Mihua.Assets.UnzipAssetMgr)
extern "C"  int32_t AssetDownMgr_ilo_get_UnZipFileCount9_m1192522652 (Il2CppObject * __this /* static, unused */, UnzipAssetMgr_t1276665662 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String AssetDownMgr::ilo_GetCRC3210(System.String)
extern "C"  String_t* AssetDownMgr_ilo_GetCRC3210_m1106067905 (Il2CppObject * __this /* static, unused */, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
