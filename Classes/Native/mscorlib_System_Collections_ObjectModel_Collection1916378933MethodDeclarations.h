﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_ObjectModel_Collection3356274529MethodDeclarations.h"

// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.Attachment>::.ctor()
#define Collection_1__ctor_m1221214599(__this, method) ((  void (*) (Collection_1_t1916378933 *, const MethodInfo*))Collection_1__ctor_m1690372513_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Net.Mail.Attachment>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2755899494(__this, method) ((  bool (*) (Collection_1_t1916378933 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1624016570_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.Attachment>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Collection_1_System_Collections_ICollection_CopyTo_m941786163(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t1916378933 *, Il2CppArray *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m1285013187_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<System.Net.Mail.Attachment>::System.Collections.IEnumerable.GetEnumerator()
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m2022938114(__this, method) ((  Il2CppObject * (*) (Collection_1_t1916378933 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m2828471038_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Net.Mail.Attachment>::System.Collections.IList.Add(System.Object)
#define Collection_1_System_Collections_IList_Add_m886320251(__this, ___value0, method) ((  int32_t (*) (Collection_1_t1916378933 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m1708617267_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Net.Mail.Attachment>::System.Collections.IList.Contains(System.Object)
#define Collection_1_System_Collections_IList_Contains_m1819609765(__this, ___value0, method) ((  bool (*) (Collection_1_t1916378933 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m504494585_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Net.Mail.Attachment>::System.Collections.IList.IndexOf(System.Object)
#define Collection_1_System_Collections_IList_IndexOf_m3437473107(__this, ___value0, method) ((  int32_t (*) (Collection_1_t1916378933 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m1652511499_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.Attachment>::System.Collections.IList.Insert(System.Int32,System.Object)
#define Collection_1_System_Collections_IList_Insert_m1353280582(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t1916378933 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m2187188150_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.Attachment>::System.Collections.IList.Remove(System.Object)
#define Collection_1_System_Collections_IList_Remove_m439468834(__this, ___value0, method) ((  void (*) (Collection_1_t1916378933 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m2625864114_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Net.Mail.Attachment>::System.Collections.ICollection.get_IsSynchronized()
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m1207242391(__this, method) ((  bool (*) (Collection_1_t1916378933 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m1302589123_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<System.Net.Mail.Attachment>::System.Collections.ICollection.get_SyncRoot()
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m783059657(__this, method) ((  Il2CppObject * (*) (Collection_1_t1916378933 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m1873829551_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Net.Mail.Attachment>::System.Collections.IList.get_IsFixedSize()
#define Collection_1_System_Collections_IList_get_IsFixedSize_m4281260500(__this, method) ((  bool (*) (Collection_1_t1916378933 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m2813777960_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Net.Mail.Attachment>::System.Collections.IList.get_IsReadOnly()
#define Collection_1_System_Collections_IList_get_IsReadOnly_m3085965989(__this, method) ((  bool (*) (Collection_1_t1916378933 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m1376059857_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<System.Net.Mail.Attachment>::System.Collections.IList.get_Item(System.Int32)
#define Collection_1_System_Collections_IList_get_Item_m1966665232(__this, ___index0, method) ((  Il2CppObject * (*) (Collection_1_t1916378933 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m2224513142_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.Attachment>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define Collection_1_System_Collections_IList_set_Item_m4036468381(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t1916378933 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m2262756877_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.Attachment>::Add(T)
#define Collection_1_Add_m2288194968(__this, ___item0, method) ((  void (*) (Collection_1_t1916378933 *, Attachment_t2730920775 *, const MethodInfo*))Collection_1_Add_m321765054_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.Attachment>::Clear()
#define Collection_1_Clear_m4187193948(__this, method) ((  void (*) (Collection_1_t1916378933 *, const MethodInfo*))Collection_1_Clear_m3391473100_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.Attachment>::ClearItems()
#define Collection_1_ClearItems_m1070686864(__this, method) ((  void (*) (Collection_1_t1916378933 *, const MethodInfo*))Collection_1_ClearItems_m2738199222_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Net.Mail.Attachment>::Contains(T)
#define Collection_1_Contains_m3931081550(__this, ___item0, method) ((  bool (*) (Collection_1_t1916378933 *, Attachment_t2730920775 *, const MethodInfo*))Collection_1_Contains_m1050871674_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.Attachment>::CopyTo(T[],System.Int32)
#define Collection_1_CopyTo_m342206622(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t1916378933 *, AttachmentU5BU5D_t2874282942*, int32_t, const MethodInfo*))Collection_1_CopyTo_m1746187054_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<System.Net.Mail.Attachment>::GetEnumerator()
#define Collection_1_GetEnumerator_m2729897692(__this, method) ((  Il2CppObject* (*) (Collection_1_t1916378933 *, const MethodInfo*))Collection_1_GetEnumerator_m625631581_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Net.Mail.Attachment>::IndexOf(T)
#define Collection_1_IndexOf_m3682307818(__this, ___item0, method) ((  int32_t (*) (Collection_1_t1916378933 *, Attachment_t2730920775 *, const MethodInfo*))Collection_1_IndexOf_m3101447730_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.Attachment>::Insert(System.Int32,T)
#define Collection_1_Insert_m1636685040(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t1916378933 *, int32_t, Attachment_t2730920775 *, const MethodInfo*))Collection_1_Insert_m1208073509_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.Attachment>::InsertItem(System.Int32,T)
#define Collection_1_InsertItem_m3700544669(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t1916378933 *, int32_t, Attachment_t2730920775 *, const MethodInfo*))Collection_1_InsertItem_m714854616_gshared)(__this, ___index0, ___item1, method)
// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.Collection`1<System.Net.Mail.Attachment>::get_Items()
#define Collection_1_get_Items_m2773637148(__this, method) ((  Il2CppObject* (*) (Collection_1_t1916378933 *, const MethodInfo*))Collection_1_get_Items_m1477178336_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Net.Mail.Attachment>::Remove(T)
#define Collection_1_Remove_m3164108476(__this, ___item0, method) ((  bool (*) (Collection_1_t1916378933 *, Attachment_t2730920775 *, const MethodInfo*))Collection_1_Remove_m2181520885_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.Attachment>::RemoveAt(System.Int32)
#define Collection_1_RemoveAt_m859234395(__this, ___index0, method) ((  void (*) (Collection_1_t1916378933 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m3376893675_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.Attachment>::RemoveItem(System.Int32)
#define Collection_1_RemoveItem_m3275805989(__this, ___index0, method) ((  void (*) (Collection_1_t1916378933 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m1099170891_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Net.Mail.Attachment>::get_Count()
#define Collection_1_get_Count_m4096577083(__this, method) ((  int32_t (*) (Collection_1_t1916378933 *, const MethodInfo*))Collection_1_get_Count_m1472906633_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<System.Net.Mail.Attachment>::get_Item(System.Int32)
#define Collection_1_get_Item_m4220694650(__this, ___index0, method) ((  Attachment_t2730920775 * (*) (Collection_1_t1916378933 *, int32_t, const MethodInfo*))Collection_1_get_Item_m2356360623_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.Attachment>::set_Item(System.Int32,T)
#define Collection_1_set_Item_m1723088428(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t1916378933 *, int32_t, Attachment_t2730920775 *, const MethodInfo*))Collection_1_set_Item_m3127068860_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.Attachment>::SetItem(System.Int32,T)
#define Collection_1_SetItem_m1358668236(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t1916378933 *, int32_t, Attachment_t2730920775 *, const MethodInfo*))Collection_1_SetItem_m112162877_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Net.Mail.Attachment>::IsValidItem(System.Object)
#define Collection_1_IsValidItem_m4009435422(__this /* static, unused */, ___item0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_IsValidItem_m1993492338_gshared)(__this /* static, unused */, ___item0, method)
// T System.Collections.ObjectModel.Collection`1<System.Net.Mail.Attachment>::ConvertItem(System.Object)
#define Collection_1_ConvertItem_m3018133984(__this /* static, unused */, ___item0, method) ((  Attachment_t2730920775 * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_ConvertItem_m1655469326_gshared)(__this /* static, unused */, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.Attachment>::CheckWritable(System.Collections.Generic.IList`1<T>)
#define Collection_1_CheckWritable_m3284449630(__this /* static, unused */, ___list0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_CheckWritable_m651250670_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Net.Mail.Attachment>::IsSynchronized(System.Collections.Generic.IList`1<T>)
#define Collection_1_IsSynchronized_m1035853542(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsSynchronized_m2469749778_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Net.Mail.Attachment>::IsFixedSize(System.Collections.Generic.IList`1<T>)
#define Collection_1_IsFixedSize_m704201081(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsFixedSize_m3893865421_gshared)(__this /* static, unused */, ___list0, method)
