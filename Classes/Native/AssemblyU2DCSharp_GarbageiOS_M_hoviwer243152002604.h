﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_hoviwer24
struct  M_hoviwer24_t3152002604  : public Il2CppObject
{
public:
	// System.String GarbageiOS.M_hoviwer24::_jatawCowkar
	String_t* ____jatawCowkar_0;
	// System.Boolean GarbageiOS.M_hoviwer24::_hoobelSepe
	bool ____hoobelSepe_1;
	// System.String GarbageiOS.M_hoviwer24::_mouledear
	String_t* ____mouledear_2;
	// System.UInt32 GarbageiOS.M_hoviwer24::_fajawMarwofa
	uint32_t ____fajawMarwofa_3;
	// System.Single GarbageiOS.M_hoviwer24::_tirhouPoochi
	float ____tirhouPoochi_4;
	// System.Single GarbageiOS.M_hoviwer24::_mufeeHerkitou
	float ____mufeeHerkitou_5;
	// System.Boolean GarbageiOS.M_hoviwer24::_cearstiJese
	bool ____cearstiJese_6;
	// System.Int32 GarbageiOS.M_hoviwer24::_segearMemeaba
	int32_t ____segearMemeaba_7;
	// System.Single GarbageiOS.M_hoviwer24::_marearRoomu
	float ____marearRoomu_8;
	// System.String GarbageiOS.M_hoviwer24::_purperesteCunune
	String_t* ____purperesteCunune_9;
	// System.Boolean GarbageiOS.M_hoviwer24::_rairmeaTouber
	bool ____rairmeaTouber_10;
	// System.Int32 GarbageiOS.M_hoviwer24::_jerpi
	int32_t ____jerpi_11;
	// System.String GarbageiOS.M_hoviwer24::_dawsouteKojur
	String_t* ____dawsouteKojur_12;
	// System.Single GarbageiOS.M_hoviwer24::_nishasasDeldal
	float ____nishasasDeldal_13;
	// System.Boolean GarbageiOS.M_hoviwer24::_tawriJorhormoo
	bool ____tawriJorhormoo_14;
	// System.Single GarbageiOS.M_hoviwer24::_noceebouStetaycee
	float ____noceebouStetaycee_15;
	// System.UInt32 GarbageiOS.M_hoviwer24::_sallbireJiso
	uint32_t ____sallbireJiso_16;
	// System.UInt32 GarbageiOS.M_hoviwer24::_yare
	uint32_t ____yare_17;
	// System.Single GarbageiOS.M_hoviwer24::_nuje
	float ____nuje_18;
	// System.String GarbageiOS.M_hoviwer24::_palzemsi
	String_t* ____palzemsi_19;
	// System.Boolean GarbageiOS.M_hoviwer24::_kerpiferGeejou
	bool ____kerpiferGeejou_20;
	// System.String GarbageiOS.M_hoviwer24::_tirsevi
	String_t* ____tirsevi_21;
	// System.UInt32 GarbageiOS.M_hoviwer24::_salgas
	uint32_t ____salgas_22;
	// System.Boolean GarbageiOS.M_hoviwer24::_jerefisurNearhaja
	bool ____jerefisurNearhaja_23;
	// System.UInt32 GarbageiOS.M_hoviwer24::_xooha
	uint32_t ____xooha_24;
	// System.Single GarbageiOS.M_hoviwer24::_goochenow
	float ____goochenow_25;
	// System.Int32 GarbageiOS.M_hoviwer24::_netelvisSawjeeqa
	int32_t ____netelvisSawjeeqa_26;
	// System.Single GarbageiOS.M_hoviwer24::_leefatisGavairrem
	float ____leefatisGavairrem_27;
	// System.Int32 GarbageiOS.M_hoviwer24::_nirnaKisci
	int32_t ____nirnaKisci_28;
	// System.Int32 GarbageiOS.M_hoviwer24::_laneredre
	int32_t ____laneredre_29;
	// System.Single GarbageiOS.M_hoviwer24::_mowheKateera
	float ____mowheKateera_30;
	// System.Single GarbageiOS.M_hoviwer24::_jarmaswhemWowem
	float ____jarmaswhemWowem_31;
	// System.UInt32 GarbageiOS.M_hoviwer24::_benosemCurcaymu
	uint32_t ____benosemCurcaymu_32;
	// System.Single GarbageiOS.M_hoviwer24::_wasbear
	float ____wasbear_33;
	// System.Single GarbageiOS.M_hoviwer24::_yagooCelmulor
	float ____yagooCelmulor_34;
	// System.UInt32 GarbageiOS.M_hoviwer24::_berxirDorpe
	uint32_t ____berxirDorpe_35;
	// System.UInt32 GarbageiOS.M_hoviwer24::_masra
	uint32_t ____masra_36;

public:
	inline static int32_t get_offset_of__jatawCowkar_0() { return static_cast<int32_t>(offsetof(M_hoviwer24_t3152002604, ____jatawCowkar_0)); }
	inline String_t* get__jatawCowkar_0() const { return ____jatawCowkar_0; }
	inline String_t** get_address_of__jatawCowkar_0() { return &____jatawCowkar_0; }
	inline void set__jatawCowkar_0(String_t* value)
	{
		____jatawCowkar_0 = value;
		Il2CppCodeGenWriteBarrier(&____jatawCowkar_0, value);
	}

	inline static int32_t get_offset_of__hoobelSepe_1() { return static_cast<int32_t>(offsetof(M_hoviwer24_t3152002604, ____hoobelSepe_1)); }
	inline bool get__hoobelSepe_1() const { return ____hoobelSepe_1; }
	inline bool* get_address_of__hoobelSepe_1() { return &____hoobelSepe_1; }
	inline void set__hoobelSepe_1(bool value)
	{
		____hoobelSepe_1 = value;
	}

	inline static int32_t get_offset_of__mouledear_2() { return static_cast<int32_t>(offsetof(M_hoviwer24_t3152002604, ____mouledear_2)); }
	inline String_t* get__mouledear_2() const { return ____mouledear_2; }
	inline String_t** get_address_of__mouledear_2() { return &____mouledear_2; }
	inline void set__mouledear_2(String_t* value)
	{
		____mouledear_2 = value;
		Il2CppCodeGenWriteBarrier(&____mouledear_2, value);
	}

	inline static int32_t get_offset_of__fajawMarwofa_3() { return static_cast<int32_t>(offsetof(M_hoviwer24_t3152002604, ____fajawMarwofa_3)); }
	inline uint32_t get__fajawMarwofa_3() const { return ____fajawMarwofa_3; }
	inline uint32_t* get_address_of__fajawMarwofa_3() { return &____fajawMarwofa_3; }
	inline void set__fajawMarwofa_3(uint32_t value)
	{
		____fajawMarwofa_3 = value;
	}

	inline static int32_t get_offset_of__tirhouPoochi_4() { return static_cast<int32_t>(offsetof(M_hoviwer24_t3152002604, ____tirhouPoochi_4)); }
	inline float get__tirhouPoochi_4() const { return ____tirhouPoochi_4; }
	inline float* get_address_of__tirhouPoochi_4() { return &____tirhouPoochi_4; }
	inline void set__tirhouPoochi_4(float value)
	{
		____tirhouPoochi_4 = value;
	}

	inline static int32_t get_offset_of__mufeeHerkitou_5() { return static_cast<int32_t>(offsetof(M_hoviwer24_t3152002604, ____mufeeHerkitou_5)); }
	inline float get__mufeeHerkitou_5() const { return ____mufeeHerkitou_5; }
	inline float* get_address_of__mufeeHerkitou_5() { return &____mufeeHerkitou_5; }
	inline void set__mufeeHerkitou_5(float value)
	{
		____mufeeHerkitou_5 = value;
	}

	inline static int32_t get_offset_of__cearstiJese_6() { return static_cast<int32_t>(offsetof(M_hoviwer24_t3152002604, ____cearstiJese_6)); }
	inline bool get__cearstiJese_6() const { return ____cearstiJese_6; }
	inline bool* get_address_of__cearstiJese_6() { return &____cearstiJese_6; }
	inline void set__cearstiJese_6(bool value)
	{
		____cearstiJese_6 = value;
	}

	inline static int32_t get_offset_of__segearMemeaba_7() { return static_cast<int32_t>(offsetof(M_hoviwer24_t3152002604, ____segearMemeaba_7)); }
	inline int32_t get__segearMemeaba_7() const { return ____segearMemeaba_7; }
	inline int32_t* get_address_of__segearMemeaba_7() { return &____segearMemeaba_7; }
	inline void set__segearMemeaba_7(int32_t value)
	{
		____segearMemeaba_7 = value;
	}

	inline static int32_t get_offset_of__marearRoomu_8() { return static_cast<int32_t>(offsetof(M_hoviwer24_t3152002604, ____marearRoomu_8)); }
	inline float get__marearRoomu_8() const { return ____marearRoomu_8; }
	inline float* get_address_of__marearRoomu_8() { return &____marearRoomu_8; }
	inline void set__marearRoomu_8(float value)
	{
		____marearRoomu_8 = value;
	}

	inline static int32_t get_offset_of__purperesteCunune_9() { return static_cast<int32_t>(offsetof(M_hoviwer24_t3152002604, ____purperesteCunune_9)); }
	inline String_t* get__purperesteCunune_9() const { return ____purperesteCunune_9; }
	inline String_t** get_address_of__purperesteCunune_9() { return &____purperesteCunune_9; }
	inline void set__purperesteCunune_9(String_t* value)
	{
		____purperesteCunune_9 = value;
		Il2CppCodeGenWriteBarrier(&____purperesteCunune_9, value);
	}

	inline static int32_t get_offset_of__rairmeaTouber_10() { return static_cast<int32_t>(offsetof(M_hoviwer24_t3152002604, ____rairmeaTouber_10)); }
	inline bool get__rairmeaTouber_10() const { return ____rairmeaTouber_10; }
	inline bool* get_address_of__rairmeaTouber_10() { return &____rairmeaTouber_10; }
	inline void set__rairmeaTouber_10(bool value)
	{
		____rairmeaTouber_10 = value;
	}

	inline static int32_t get_offset_of__jerpi_11() { return static_cast<int32_t>(offsetof(M_hoviwer24_t3152002604, ____jerpi_11)); }
	inline int32_t get__jerpi_11() const { return ____jerpi_11; }
	inline int32_t* get_address_of__jerpi_11() { return &____jerpi_11; }
	inline void set__jerpi_11(int32_t value)
	{
		____jerpi_11 = value;
	}

	inline static int32_t get_offset_of__dawsouteKojur_12() { return static_cast<int32_t>(offsetof(M_hoviwer24_t3152002604, ____dawsouteKojur_12)); }
	inline String_t* get__dawsouteKojur_12() const { return ____dawsouteKojur_12; }
	inline String_t** get_address_of__dawsouteKojur_12() { return &____dawsouteKojur_12; }
	inline void set__dawsouteKojur_12(String_t* value)
	{
		____dawsouteKojur_12 = value;
		Il2CppCodeGenWriteBarrier(&____dawsouteKojur_12, value);
	}

	inline static int32_t get_offset_of__nishasasDeldal_13() { return static_cast<int32_t>(offsetof(M_hoviwer24_t3152002604, ____nishasasDeldal_13)); }
	inline float get__nishasasDeldal_13() const { return ____nishasasDeldal_13; }
	inline float* get_address_of__nishasasDeldal_13() { return &____nishasasDeldal_13; }
	inline void set__nishasasDeldal_13(float value)
	{
		____nishasasDeldal_13 = value;
	}

	inline static int32_t get_offset_of__tawriJorhormoo_14() { return static_cast<int32_t>(offsetof(M_hoviwer24_t3152002604, ____tawriJorhormoo_14)); }
	inline bool get__tawriJorhormoo_14() const { return ____tawriJorhormoo_14; }
	inline bool* get_address_of__tawriJorhormoo_14() { return &____tawriJorhormoo_14; }
	inline void set__tawriJorhormoo_14(bool value)
	{
		____tawriJorhormoo_14 = value;
	}

	inline static int32_t get_offset_of__noceebouStetaycee_15() { return static_cast<int32_t>(offsetof(M_hoviwer24_t3152002604, ____noceebouStetaycee_15)); }
	inline float get__noceebouStetaycee_15() const { return ____noceebouStetaycee_15; }
	inline float* get_address_of__noceebouStetaycee_15() { return &____noceebouStetaycee_15; }
	inline void set__noceebouStetaycee_15(float value)
	{
		____noceebouStetaycee_15 = value;
	}

	inline static int32_t get_offset_of__sallbireJiso_16() { return static_cast<int32_t>(offsetof(M_hoviwer24_t3152002604, ____sallbireJiso_16)); }
	inline uint32_t get__sallbireJiso_16() const { return ____sallbireJiso_16; }
	inline uint32_t* get_address_of__sallbireJiso_16() { return &____sallbireJiso_16; }
	inline void set__sallbireJiso_16(uint32_t value)
	{
		____sallbireJiso_16 = value;
	}

	inline static int32_t get_offset_of__yare_17() { return static_cast<int32_t>(offsetof(M_hoviwer24_t3152002604, ____yare_17)); }
	inline uint32_t get__yare_17() const { return ____yare_17; }
	inline uint32_t* get_address_of__yare_17() { return &____yare_17; }
	inline void set__yare_17(uint32_t value)
	{
		____yare_17 = value;
	}

	inline static int32_t get_offset_of__nuje_18() { return static_cast<int32_t>(offsetof(M_hoviwer24_t3152002604, ____nuje_18)); }
	inline float get__nuje_18() const { return ____nuje_18; }
	inline float* get_address_of__nuje_18() { return &____nuje_18; }
	inline void set__nuje_18(float value)
	{
		____nuje_18 = value;
	}

	inline static int32_t get_offset_of__palzemsi_19() { return static_cast<int32_t>(offsetof(M_hoviwer24_t3152002604, ____palzemsi_19)); }
	inline String_t* get__palzemsi_19() const { return ____palzemsi_19; }
	inline String_t** get_address_of__palzemsi_19() { return &____palzemsi_19; }
	inline void set__palzemsi_19(String_t* value)
	{
		____palzemsi_19 = value;
		Il2CppCodeGenWriteBarrier(&____palzemsi_19, value);
	}

	inline static int32_t get_offset_of__kerpiferGeejou_20() { return static_cast<int32_t>(offsetof(M_hoviwer24_t3152002604, ____kerpiferGeejou_20)); }
	inline bool get__kerpiferGeejou_20() const { return ____kerpiferGeejou_20; }
	inline bool* get_address_of__kerpiferGeejou_20() { return &____kerpiferGeejou_20; }
	inline void set__kerpiferGeejou_20(bool value)
	{
		____kerpiferGeejou_20 = value;
	}

	inline static int32_t get_offset_of__tirsevi_21() { return static_cast<int32_t>(offsetof(M_hoviwer24_t3152002604, ____tirsevi_21)); }
	inline String_t* get__tirsevi_21() const { return ____tirsevi_21; }
	inline String_t** get_address_of__tirsevi_21() { return &____tirsevi_21; }
	inline void set__tirsevi_21(String_t* value)
	{
		____tirsevi_21 = value;
		Il2CppCodeGenWriteBarrier(&____tirsevi_21, value);
	}

	inline static int32_t get_offset_of__salgas_22() { return static_cast<int32_t>(offsetof(M_hoviwer24_t3152002604, ____salgas_22)); }
	inline uint32_t get__salgas_22() const { return ____salgas_22; }
	inline uint32_t* get_address_of__salgas_22() { return &____salgas_22; }
	inline void set__salgas_22(uint32_t value)
	{
		____salgas_22 = value;
	}

	inline static int32_t get_offset_of__jerefisurNearhaja_23() { return static_cast<int32_t>(offsetof(M_hoviwer24_t3152002604, ____jerefisurNearhaja_23)); }
	inline bool get__jerefisurNearhaja_23() const { return ____jerefisurNearhaja_23; }
	inline bool* get_address_of__jerefisurNearhaja_23() { return &____jerefisurNearhaja_23; }
	inline void set__jerefisurNearhaja_23(bool value)
	{
		____jerefisurNearhaja_23 = value;
	}

	inline static int32_t get_offset_of__xooha_24() { return static_cast<int32_t>(offsetof(M_hoviwer24_t3152002604, ____xooha_24)); }
	inline uint32_t get__xooha_24() const { return ____xooha_24; }
	inline uint32_t* get_address_of__xooha_24() { return &____xooha_24; }
	inline void set__xooha_24(uint32_t value)
	{
		____xooha_24 = value;
	}

	inline static int32_t get_offset_of__goochenow_25() { return static_cast<int32_t>(offsetof(M_hoviwer24_t3152002604, ____goochenow_25)); }
	inline float get__goochenow_25() const { return ____goochenow_25; }
	inline float* get_address_of__goochenow_25() { return &____goochenow_25; }
	inline void set__goochenow_25(float value)
	{
		____goochenow_25 = value;
	}

	inline static int32_t get_offset_of__netelvisSawjeeqa_26() { return static_cast<int32_t>(offsetof(M_hoviwer24_t3152002604, ____netelvisSawjeeqa_26)); }
	inline int32_t get__netelvisSawjeeqa_26() const { return ____netelvisSawjeeqa_26; }
	inline int32_t* get_address_of__netelvisSawjeeqa_26() { return &____netelvisSawjeeqa_26; }
	inline void set__netelvisSawjeeqa_26(int32_t value)
	{
		____netelvisSawjeeqa_26 = value;
	}

	inline static int32_t get_offset_of__leefatisGavairrem_27() { return static_cast<int32_t>(offsetof(M_hoviwer24_t3152002604, ____leefatisGavairrem_27)); }
	inline float get__leefatisGavairrem_27() const { return ____leefatisGavairrem_27; }
	inline float* get_address_of__leefatisGavairrem_27() { return &____leefatisGavairrem_27; }
	inline void set__leefatisGavairrem_27(float value)
	{
		____leefatisGavairrem_27 = value;
	}

	inline static int32_t get_offset_of__nirnaKisci_28() { return static_cast<int32_t>(offsetof(M_hoviwer24_t3152002604, ____nirnaKisci_28)); }
	inline int32_t get__nirnaKisci_28() const { return ____nirnaKisci_28; }
	inline int32_t* get_address_of__nirnaKisci_28() { return &____nirnaKisci_28; }
	inline void set__nirnaKisci_28(int32_t value)
	{
		____nirnaKisci_28 = value;
	}

	inline static int32_t get_offset_of__laneredre_29() { return static_cast<int32_t>(offsetof(M_hoviwer24_t3152002604, ____laneredre_29)); }
	inline int32_t get__laneredre_29() const { return ____laneredre_29; }
	inline int32_t* get_address_of__laneredre_29() { return &____laneredre_29; }
	inline void set__laneredre_29(int32_t value)
	{
		____laneredre_29 = value;
	}

	inline static int32_t get_offset_of__mowheKateera_30() { return static_cast<int32_t>(offsetof(M_hoviwer24_t3152002604, ____mowheKateera_30)); }
	inline float get__mowheKateera_30() const { return ____mowheKateera_30; }
	inline float* get_address_of__mowheKateera_30() { return &____mowheKateera_30; }
	inline void set__mowheKateera_30(float value)
	{
		____mowheKateera_30 = value;
	}

	inline static int32_t get_offset_of__jarmaswhemWowem_31() { return static_cast<int32_t>(offsetof(M_hoviwer24_t3152002604, ____jarmaswhemWowem_31)); }
	inline float get__jarmaswhemWowem_31() const { return ____jarmaswhemWowem_31; }
	inline float* get_address_of__jarmaswhemWowem_31() { return &____jarmaswhemWowem_31; }
	inline void set__jarmaswhemWowem_31(float value)
	{
		____jarmaswhemWowem_31 = value;
	}

	inline static int32_t get_offset_of__benosemCurcaymu_32() { return static_cast<int32_t>(offsetof(M_hoviwer24_t3152002604, ____benosemCurcaymu_32)); }
	inline uint32_t get__benosemCurcaymu_32() const { return ____benosemCurcaymu_32; }
	inline uint32_t* get_address_of__benosemCurcaymu_32() { return &____benosemCurcaymu_32; }
	inline void set__benosemCurcaymu_32(uint32_t value)
	{
		____benosemCurcaymu_32 = value;
	}

	inline static int32_t get_offset_of__wasbear_33() { return static_cast<int32_t>(offsetof(M_hoviwer24_t3152002604, ____wasbear_33)); }
	inline float get__wasbear_33() const { return ____wasbear_33; }
	inline float* get_address_of__wasbear_33() { return &____wasbear_33; }
	inline void set__wasbear_33(float value)
	{
		____wasbear_33 = value;
	}

	inline static int32_t get_offset_of__yagooCelmulor_34() { return static_cast<int32_t>(offsetof(M_hoviwer24_t3152002604, ____yagooCelmulor_34)); }
	inline float get__yagooCelmulor_34() const { return ____yagooCelmulor_34; }
	inline float* get_address_of__yagooCelmulor_34() { return &____yagooCelmulor_34; }
	inline void set__yagooCelmulor_34(float value)
	{
		____yagooCelmulor_34 = value;
	}

	inline static int32_t get_offset_of__berxirDorpe_35() { return static_cast<int32_t>(offsetof(M_hoviwer24_t3152002604, ____berxirDorpe_35)); }
	inline uint32_t get__berxirDorpe_35() const { return ____berxirDorpe_35; }
	inline uint32_t* get_address_of__berxirDorpe_35() { return &____berxirDorpe_35; }
	inline void set__berxirDorpe_35(uint32_t value)
	{
		____berxirDorpe_35 = value;
	}

	inline static int32_t get_offset_of__masra_36() { return static_cast<int32_t>(offsetof(M_hoviwer24_t3152002604, ____masra_36)); }
	inline uint32_t get__masra_36() const { return ____masra_36; }
	inline uint32_t* get_address_of__masra_36() { return &____masra_36; }
	inline void set__masra_36(uint32_t value)
	{
		____masra_36 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
