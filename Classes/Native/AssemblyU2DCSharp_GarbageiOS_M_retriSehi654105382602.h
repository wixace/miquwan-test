﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_retriSehi65
struct  M_retriSehi65_t4105382602  : public Il2CppObject
{
public:
	// System.Boolean GarbageiOS.M_retriSehi65::_reqemYupawne
	bool ____reqemYupawne_0;
	// System.String GarbageiOS.M_retriSehi65::_nairle
	String_t* ____nairle_1;
	// System.UInt32 GarbageiOS.M_retriSehi65::_kougowtral
	uint32_t ____kougowtral_2;
	// System.Int32 GarbageiOS.M_retriSehi65::_soolaJartrayfou
	int32_t ____soolaJartrayfou_3;
	// System.Int32 GarbageiOS.M_retriSehi65::_drayfaixallGihas
	int32_t ____drayfaixallGihas_4;
	// System.Boolean GarbageiOS.M_retriSehi65::_nahipaiVeju
	bool ____nahipaiVeju_5;
	// System.Boolean GarbageiOS.M_retriSehi65::_drerecee
	bool ____drerecee_6;
	// System.Boolean GarbageiOS.M_retriSehi65::_camo
	bool ____camo_7;
	// System.String GarbageiOS.M_retriSehi65::_temal
	String_t* ____temal_8;
	// System.Single GarbageiOS.M_retriSehi65::_bipuceaTrojur
	float ____bipuceaTrojur_9;
	// System.UInt32 GarbageiOS.M_retriSehi65::_siworla
	uint32_t ____siworla_10;
	// System.UInt32 GarbageiOS.M_retriSehi65::_fadeje
	uint32_t ____fadeje_11;
	// System.Single GarbageiOS.M_retriSehi65::_jelchalldearTrowtirboo
	float ____jelchalldearTrowtirboo_12;
	// System.Int32 GarbageiOS.M_retriSehi65::_heecheGoumoci
	int32_t ____heecheGoumoci_13;
	// System.Int32 GarbageiOS.M_retriSehi65::_drileepawNemwar
	int32_t ____drileepawNemwar_14;
	// System.Single GarbageiOS.M_retriSehi65::_jalljaijal
	float ____jalljaijal_15;
	// System.Single GarbageiOS.M_retriSehi65::_zowla
	float ____zowla_16;
	// System.UInt32 GarbageiOS.M_retriSehi65::_daskouwhow
	uint32_t ____daskouwhow_17;
	// System.Int32 GarbageiOS.M_retriSehi65::_taysasSemem
	int32_t ____taysasSemem_18;
	// System.Single GarbageiOS.M_retriSehi65::_murwowjaHarhibea
	float ____murwowjaHarhibea_19;
	// System.String GarbageiOS.M_retriSehi65::_qalhear
	String_t* ____qalhear_20;
	// System.Boolean GarbageiOS.M_retriSehi65::_whuguKelea
	bool ____whuguKelea_21;
	// System.Boolean GarbageiOS.M_retriSehi65::_galgaycu
	bool ____galgaycu_22;
	// System.String GarbageiOS.M_retriSehi65::_nassasZeagar
	String_t* ____nassasZeagar_23;
	// System.UInt32 GarbageiOS.M_retriSehi65::_lowsiDamadai
	uint32_t ____lowsiDamadai_24;
	// System.UInt32 GarbageiOS.M_retriSehi65::_hallcuriGimou
	uint32_t ____hallcuriGimou_25;
	// System.UInt32 GarbageiOS.M_retriSehi65::_talllorCheacearni
	uint32_t ____talllorCheacearni_26;
	// System.String GarbageiOS.M_retriSehi65::_marwirte
	String_t* ____marwirte_27;
	// System.UInt32 GarbageiOS.M_retriSehi65::_vaynerwa
	uint32_t ____vaynerwa_28;

public:
	inline static int32_t get_offset_of__reqemYupawne_0() { return static_cast<int32_t>(offsetof(M_retriSehi65_t4105382602, ____reqemYupawne_0)); }
	inline bool get__reqemYupawne_0() const { return ____reqemYupawne_0; }
	inline bool* get_address_of__reqemYupawne_0() { return &____reqemYupawne_0; }
	inline void set__reqemYupawne_0(bool value)
	{
		____reqemYupawne_0 = value;
	}

	inline static int32_t get_offset_of__nairle_1() { return static_cast<int32_t>(offsetof(M_retriSehi65_t4105382602, ____nairle_1)); }
	inline String_t* get__nairle_1() const { return ____nairle_1; }
	inline String_t** get_address_of__nairle_1() { return &____nairle_1; }
	inline void set__nairle_1(String_t* value)
	{
		____nairle_1 = value;
		Il2CppCodeGenWriteBarrier(&____nairle_1, value);
	}

	inline static int32_t get_offset_of__kougowtral_2() { return static_cast<int32_t>(offsetof(M_retriSehi65_t4105382602, ____kougowtral_2)); }
	inline uint32_t get__kougowtral_2() const { return ____kougowtral_2; }
	inline uint32_t* get_address_of__kougowtral_2() { return &____kougowtral_2; }
	inline void set__kougowtral_2(uint32_t value)
	{
		____kougowtral_2 = value;
	}

	inline static int32_t get_offset_of__soolaJartrayfou_3() { return static_cast<int32_t>(offsetof(M_retriSehi65_t4105382602, ____soolaJartrayfou_3)); }
	inline int32_t get__soolaJartrayfou_3() const { return ____soolaJartrayfou_3; }
	inline int32_t* get_address_of__soolaJartrayfou_3() { return &____soolaJartrayfou_3; }
	inline void set__soolaJartrayfou_3(int32_t value)
	{
		____soolaJartrayfou_3 = value;
	}

	inline static int32_t get_offset_of__drayfaixallGihas_4() { return static_cast<int32_t>(offsetof(M_retriSehi65_t4105382602, ____drayfaixallGihas_4)); }
	inline int32_t get__drayfaixallGihas_4() const { return ____drayfaixallGihas_4; }
	inline int32_t* get_address_of__drayfaixallGihas_4() { return &____drayfaixallGihas_4; }
	inline void set__drayfaixallGihas_4(int32_t value)
	{
		____drayfaixallGihas_4 = value;
	}

	inline static int32_t get_offset_of__nahipaiVeju_5() { return static_cast<int32_t>(offsetof(M_retriSehi65_t4105382602, ____nahipaiVeju_5)); }
	inline bool get__nahipaiVeju_5() const { return ____nahipaiVeju_5; }
	inline bool* get_address_of__nahipaiVeju_5() { return &____nahipaiVeju_5; }
	inline void set__nahipaiVeju_5(bool value)
	{
		____nahipaiVeju_5 = value;
	}

	inline static int32_t get_offset_of__drerecee_6() { return static_cast<int32_t>(offsetof(M_retriSehi65_t4105382602, ____drerecee_6)); }
	inline bool get__drerecee_6() const { return ____drerecee_6; }
	inline bool* get_address_of__drerecee_6() { return &____drerecee_6; }
	inline void set__drerecee_6(bool value)
	{
		____drerecee_6 = value;
	}

	inline static int32_t get_offset_of__camo_7() { return static_cast<int32_t>(offsetof(M_retriSehi65_t4105382602, ____camo_7)); }
	inline bool get__camo_7() const { return ____camo_7; }
	inline bool* get_address_of__camo_7() { return &____camo_7; }
	inline void set__camo_7(bool value)
	{
		____camo_7 = value;
	}

	inline static int32_t get_offset_of__temal_8() { return static_cast<int32_t>(offsetof(M_retriSehi65_t4105382602, ____temal_8)); }
	inline String_t* get__temal_8() const { return ____temal_8; }
	inline String_t** get_address_of__temal_8() { return &____temal_8; }
	inline void set__temal_8(String_t* value)
	{
		____temal_8 = value;
		Il2CppCodeGenWriteBarrier(&____temal_8, value);
	}

	inline static int32_t get_offset_of__bipuceaTrojur_9() { return static_cast<int32_t>(offsetof(M_retriSehi65_t4105382602, ____bipuceaTrojur_9)); }
	inline float get__bipuceaTrojur_9() const { return ____bipuceaTrojur_9; }
	inline float* get_address_of__bipuceaTrojur_9() { return &____bipuceaTrojur_9; }
	inline void set__bipuceaTrojur_9(float value)
	{
		____bipuceaTrojur_9 = value;
	}

	inline static int32_t get_offset_of__siworla_10() { return static_cast<int32_t>(offsetof(M_retriSehi65_t4105382602, ____siworla_10)); }
	inline uint32_t get__siworla_10() const { return ____siworla_10; }
	inline uint32_t* get_address_of__siworla_10() { return &____siworla_10; }
	inline void set__siworla_10(uint32_t value)
	{
		____siworla_10 = value;
	}

	inline static int32_t get_offset_of__fadeje_11() { return static_cast<int32_t>(offsetof(M_retriSehi65_t4105382602, ____fadeje_11)); }
	inline uint32_t get__fadeje_11() const { return ____fadeje_11; }
	inline uint32_t* get_address_of__fadeje_11() { return &____fadeje_11; }
	inline void set__fadeje_11(uint32_t value)
	{
		____fadeje_11 = value;
	}

	inline static int32_t get_offset_of__jelchalldearTrowtirboo_12() { return static_cast<int32_t>(offsetof(M_retriSehi65_t4105382602, ____jelchalldearTrowtirboo_12)); }
	inline float get__jelchalldearTrowtirboo_12() const { return ____jelchalldearTrowtirboo_12; }
	inline float* get_address_of__jelchalldearTrowtirboo_12() { return &____jelchalldearTrowtirboo_12; }
	inline void set__jelchalldearTrowtirboo_12(float value)
	{
		____jelchalldearTrowtirboo_12 = value;
	}

	inline static int32_t get_offset_of__heecheGoumoci_13() { return static_cast<int32_t>(offsetof(M_retriSehi65_t4105382602, ____heecheGoumoci_13)); }
	inline int32_t get__heecheGoumoci_13() const { return ____heecheGoumoci_13; }
	inline int32_t* get_address_of__heecheGoumoci_13() { return &____heecheGoumoci_13; }
	inline void set__heecheGoumoci_13(int32_t value)
	{
		____heecheGoumoci_13 = value;
	}

	inline static int32_t get_offset_of__drileepawNemwar_14() { return static_cast<int32_t>(offsetof(M_retriSehi65_t4105382602, ____drileepawNemwar_14)); }
	inline int32_t get__drileepawNemwar_14() const { return ____drileepawNemwar_14; }
	inline int32_t* get_address_of__drileepawNemwar_14() { return &____drileepawNemwar_14; }
	inline void set__drileepawNemwar_14(int32_t value)
	{
		____drileepawNemwar_14 = value;
	}

	inline static int32_t get_offset_of__jalljaijal_15() { return static_cast<int32_t>(offsetof(M_retriSehi65_t4105382602, ____jalljaijal_15)); }
	inline float get__jalljaijal_15() const { return ____jalljaijal_15; }
	inline float* get_address_of__jalljaijal_15() { return &____jalljaijal_15; }
	inline void set__jalljaijal_15(float value)
	{
		____jalljaijal_15 = value;
	}

	inline static int32_t get_offset_of__zowla_16() { return static_cast<int32_t>(offsetof(M_retriSehi65_t4105382602, ____zowla_16)); }
	inline float get__zowla_16() const { return ____zowla_16; }
	inline float* get_address_of__zowla_16() { return &____zowla_16; }
	inline void set__zowla_16(float value)
	{
		____zowla_16 = value;
	}

	inline static int32_t get_offset_of__daskouwhow_17() { return static_cast<int32_t>(offsetof(M_retriSehi65_t4105382602, ____daskouwhow_17)); }
	inline uint32_t get__daskouwhow_17() const { return ____daskouwhow_17; }
	inline uint32_t* get_address_of__daskouwhow_17() { return &____daskouwhow_17; }
	inline void set__daskouwhow_17(uint32_t value)
	{
		____daskouwhow_17 = value;
	}

	inline static int32_t get_offset_of__taysasSemem_18() { return static_cast<int32_t>(offsetof(M_retriSehi65_t4105382602, ____taysasSemem_18)); }
	inline int32_t get__taysasSemem_18() const { return ____taysasSemem_18; }
	inline int32_t* get_address_of__taysasSemem_18() { return &____taysasSemem_18; }
	inline void set__taysasSemem_18(int32_t value)
	{
		____taysasSemem_18 = value;
	}

	inline static int32_t get_offset_of__murwowjaHarhibea_19() { return static_cast<int32_t>(offsetof(M_retriSehi65_t4105382602, ____murwowjaHarhibea_19)); }
	inline float get__murwowjaHarhibea_19() const { return ____murwowjaHarhibea_19; }
	inline float* get_address_of__murwowjaHarhibea_19() { return &____murwowjaHarhibea_19; }
	inline void set__murwowjaHarhibea_19(float value)
	{
		____murwowjaHarhibea_19 = value;
	}

	inline static int32_t get_offset_of__qalhear_20() { return static_cast<int32_t>(offsetof(M_retriSehi65_t4105382602, ____qalhear_20)); }
	inline String_t* get__qalhear_20() const { return ____qalhear_20; }
	inline String_t** get_address_of__qalhear_20() { return &____qalhear_20; }
	inline void set__qalhear_20(String_t* value)
	{
		____qalhear_20 = value;
		Il2CppCodeGenWriteBarrier(&____qalhear_20, value);
	}

	inline static int32_t get_offset_of__whuguKelea_21() { return static_cast<int32_t>(offsetof(M_retriSehi65_t4105382602, ____whuguKelea_21)); }
	inline bool get__whuguKelea_21() const { return ____whuguKelea_21; }
	inline bool* get_address_of__whuguKelea_21() { return &____whuguKelea_21; }
	inline void set__whuguKelea_21(bool value)
	{
		____whuguKelea_21 = value;
	}

	inline static int32_t get_offset_of__galgaycu_22() { return static_cast<int32_t>(offsetof(M_retriSehi65_t4105382602, ____galgaycu_22)); }
	inline bool get__galgaycu_22() const { return ____galgaycu_22; }
	inline bool* get_address_of__galgaycu_22() { return &____galgaycu_22; }
	inline void set__galgaycu_22(bool value)
	{
		____galgaycu_22 = value;
	}

	inline static int32_t get_offset_of__nassasZeagar_23() { return static_cast<int32_t>(offsetof(M_retriSehi65_t4105382602, ____nassasZeagar_23)); }
	inline String_t* get__nassasZeagar_23() const { return ____nassasZeagar_23; }
	inline String_t** get_address_of__nassasZeagar_23() { return &____nassasZeagar_23; }
	inline void set__nassasZeagar_23(String_t* value)
	{
		____nassasZeagar_23 = value;
		Il2CppCodeGenWriteBarrier(&____nassasZeagar_23, value);
	}

	inline static int32_t get_offset_of__lowsiDamadai_24() { return static_cast<int32_t>(offsetof(M_retriSehi65_t4105382602, ____lowsiDamadai_24)); }
	inline uint32_t get__lowsiDamadai_24() const { return ____lowsiDamadai_24; }
	inline uint32_t* get_address_of__lowsiDamadai_24() { return &____lowsiDamadai_24; }
	inline void set__lowsiDamadai_24(uint32_t value)
	{
		____lowsiDamadai_24 = value;
	}

	inline static int32_t get_offset_of__hallcuriGimou_25() { return static_cast<int32_t>(offsetof(M_retriSehi65_t4105382602, ____hallcuriGimou_25)); }
	inline uint32_t get__hallcuriGimou_25() const { return ____hallcuriGimou_25; }
	inline uint32_t* get_address_of__hallcuriGimou_25() { return &____hallcuriGimou_25; }
	inline void set__hallcuriGimou_25(uint32_t value)
	{
		____hallcuriGimou_25 = value;
	}

	inline static int32_t get_offset_of__talllorCheacearni_26() { return static_cast<int32_t>(offsetof(M_retriSehi65_t4105382602, ____talllorCheacearni_26)); }
	inline uint32_t get__talllorCheacearni_26() const { return ____talllorCheacearni_26; }
	inline uint32_t* get_address_of__talllorCheacearni_26() { return &____talllorCheacearni_26; }
	inline void set__talllorCheacearni_26(uint32_t value)
	{
		____talllorCheacearni_26 = value;
	}

	inline static int32_t get_offset_of__marwirte_27() { return static_cast<int32_t>(offsetof(M_retriSehi65_t4105382602, ____marwirte_27)); }
	inline String_t* get__marwirte_27() const { return ____marwirte_27; }
	inline String_t** get_address_of__marwirte_27() { return &____marwirte_27; }
	inline void set__marwirte_27(String_t* value)
	{
		____marwirte_27 = value;
		Il2CppCodeGenWriteBarrier(&____marwirte_27, value);
	}

	inline static int32_t get_offset_of__vaynerwa_28() { return static_cast<int32_t>(offsetof(M_retriSehi65_t4105382602, ____vaynerwa_28)); }
	inline uint32_t get__vaynerwa_28() const { return ____vaynerwa_28; }
	inline uint32_t* get_address_of__vaynerwa_28() { return &____vaynerwa_28; }
	inline void set__vaynerwa_28(uint32_t value)
	{
		____vaynerwa_28 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
