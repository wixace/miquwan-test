﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;
// System.Text.Encoding
struct Encoding_t2012439129;
// System.IO.StreamWriter
struct StreamWriter_t2705123075;
// System.IO.FileStream
struct FileStream_t2141505868;
// System.IO.StreamReader
struct StreamReader_t2549717843;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Text_Encoding2012439129.h"
#include "mscorlib_System_IO_FileAttributes2368997539.h"
#include "mscorlib_System_DateTime4283661327.h"
#include "mscorlib_System_IO_FileMode3233790127.h"
#include "mscorlib_System_IO_FileAccess1610034992.h"
#include "mscorlib_System_IO_FileShare783541953.h"
#include "mscorlib_System_IO_StreamReader2549717843.h"
#include "mscorlib_System_IO_StreamWriter2705123075.h"

// System.Void System.IO.File::AppendAllText(System.String,System.String)
extern "C"  void File_AppendAllText_m3910657151 (Il2CppObject * __this /* static, unused */, String_t* ___path0, String_t* ___contents1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.File::AppendAllText(System.String,System.String,System.Text.Encoding)
extern "C"  void File_AppendAllText_m4019716082 (Il2CppObject * __this /* static, unused */, String_t* ___path0, String_t* ___contents1, Encoding_t2012439129 * ___encoding2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.StreamWriter System.IO.File::AppendText(System.String)
extern "C"  StreamWriter_t2705123075 * File_AppendText_m388272437 (Il2CppObject * __this /* static, unused */, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.File::Copy(System.String,System.String)
extern "C"  void File_Copy_m4182716978 (Il2CppObject * __this /* static, unused */, String_t* ___sourceFileName0, String_t* ___destFileName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.File::Copy(System.String,System.String,System.Boolean)
extern "C"  void File_Copy_m4125374219 (Il2CppObject * __this /* static, unused */, String_t* ___sourceFileName0, String_t* ___destFileName1, bool ___overwrite2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.FileStream System.IO.File::Create(System.String)
extern "C"  FileStream_t2141505868 * File_Create_m3497726217 (Il2CppObject * __this /* static, unused */, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.FileStream System.IO.File::Create(System.String,System.Int32)
extern "C"  FileStream_t2141505868 * File_Create_m2847392366 (Il2CppObject * __this /* static, unused */, String_t* ___path0, int32_t ___bufferSize1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.StreamWriter System.IO.File::CreateText(System.String)
extern "C"  StreamWriter_t2705123075 * File_CreateText_m8871955 (Il2CppObject * __this /* static, unused */, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.File::Delete(System.String)
extern "C"  void File_Delete_m760984832 (Il2CppObject * __this /* static, unused */, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.IO.File::Exists(System.String)
extern "C"  bool File_Exists_m1326262381 (Il2CppObject * __this /* static, unused */, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.FileAttributes System.IO.File::GetAttributes(System.String)
extern "C"  int32_t File_GetAttributes_m1646191705 (Il2CppObject * __this /* static, unused */, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.IO.File::GetCreationTime(System.String)
extern "C"  DateTime_t4283661327  File_GetCreationTime_m4247736334 (Il2CppObject * __this /* static, unused */, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.IO.File::GetCreationTimeUtc(System.String)
extern "C"  DateTime_t4283661327  File_GetCreationTimeUtc_m311377424 (Il2CppObject * __this /* static, unused */, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.IO.File::GetLastAccessTime(System.String)
extern "C"  DateTime_t4283661327  File_GetLastAccessTime_m731398451 (Il2CppObject * __this /* static, unused */, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.IO.File::GetLastAccessTimeUtc(System.String)
extern "C"  DateTime_t4283661327  File_GetLastAccessTimeUtc_m3636821707 (Il2CppObject * __this /* static, unused */, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.IO.File::GetLastWriteTime(System.String)
extern "C"  DateTime_t4283661327  File_GetLastWriteTime_m3623250546 (Il2CppObject * __this /* static, unused */, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.IO.File::GetLastWriteTimeUtc(System.String)
extern "C"  DateTime_t4283661327  File_GetLastWriteTimeUtc_m2053593388 (Il2CppObject * __this /* static, unused */, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.File::Move(System.String,System.String)
extern "C"  void File_Move_m1404293974 (Il2CppObject * __this /* static, unused */, String_t* ___sourceFileName0, String_t* ___destFileName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.FileStream System.IO.File::Open(System.String,System.IO.FileMode)
extern "C"  FileStream_t2141505868 * File_Open_m1918038371 (Il2CppObject * __this /* static, unused */, String_t* ___path0, int32_t ___mode1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.FileStream System.IO.File::Open(System.String,System.IO.FileMode,System.IO.FileAccess)
extern "C"  FileStream_t2141505868 * File_Open_m271323418 (Il2CppObject * __this /* static, unused */, String_t* ___path0, int32_t ___mode1, int32_t ___access2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.FileStream System.IO.File::Open(System.String,System.IO.FileMode,System.IO.FileAccess,System.IO.FileShare)
extern "C"  FileStream_t2141505868 * File_Open_m3723338220 (Il2CppObject * __this /* static, unused */, String_t* ___path0, int32_t ___mode1, int32_t ___access2, int32_t ___share3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.FileStream System.IO.File::OpenRead(System.String)
extern "C"  FileStream_t2141505868 * File_OpenRead_m3104031109 (Il2CppObject * __this /* static, unused */, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.StreamReader System.IO.File::OpenText(System.String)
extern "C"  StreamReader_t2549717843 * File_OpenText_m396847893 (Il2CppObject * __this /* static, unused */, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.FileStream System.IO.File::OpenWrite(System.String)
extern "C"  FileStream_t2141505868 * File_OpenWrite_m23896808 (Il2CppObject * __this /* static, unused */, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.File::Replace(System.String,System.String,System.String)
extern "C"  void File_Replace_m4265820731 (Il2CppObject * __this /* static, unused */, String_t* ___sourceFileName0, String_t* ___destinationFileName1, String_t* ___destinationBackupFileName2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.File::Replace(System.String,System.String,System.String,System.Boolean)
extern "C"  void File_Replace_m4134466530 (Il2CppObject * __this /* static, unused */, String_t* ___sourceFileName0, String_t* ___destinationFileName1, String_t* ___destinationBackupFileName2, bool ___ignoreMetadataErrors3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.File::SetAttributes(System.String,System.IO.FileAttributes)
extern "C"  void File_SetAttributes_m2359825740 (Il2CppObject * __this /* static, unused */, String_t* ___path0, int32_t ___fileAttributes1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.File::SetCreationTime(System.String,System.DateTime)
extern "C"  void File_SetCreationTime_m3400682971 (Il2CppObject * __this /* static, unused */, String_t* ___path0, DateTime_t4283661327  ___creationTime1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.File::SetCreationTimeUtc(System.String,System.DateTime)
extern "C"  void File_SetCreationTimeUtc_m866354247 (Il2CppObject * __this /* static, unused */, String_t* ___path0, DateTime_t4283661327  ___creationTimeUtc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.File::SetLastAccessTime(System.String,System.DateTime)
extern "C"  void File_SetLastAccessTime_m40471104 (Il2CppObject * __this /* static, unused */, String_t* ___path0, DateTime_t4283661327  ___lastAccessTime1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.File::SetLastAccessTimeUtc(System.String,System.DateTime)
extern "C"  void File_SetLastAccessTimeUtc_m3892359618 (Il2CppObject * __this /* static, unused */, String_t* ___path0, DateTime_t4283661327  ___lastAccessTimeUtc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.File::SetLastWriteTime(System.String,System.DateTime)
extern "C"  void File_SetLastWriteTime_m3432397289 (Il2CppObject * __this /* static, unused */, String_t* ___path0, DateTime_t4283661327  ___lastWriteTime1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.File::SetLastWriteTimeUtc(System.String,System.DateTime)
extern "C"  void File_SetLastWriteTimeUtc_m774796665 (Il2CppObject * __this /* static, unused */, String_t* ___path0, DateTime_t4283661327  ___lastWriteTimeUtc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.File::CheckPathExceptions(System.String)
extern "C"  void File_CheckPathExceptions_m1390322214 (Il2CppObject * __this /* static, unused */, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.IO.File::ReadAllBytes(System.String)
extern "C"  ByteU5BU5D_t4260760469* File_ReadAllBytes_m621899937 (Il2CppObject * __this /* static, unused */, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] System.IO.File::ReadAllLines(System.String)
extern "C"  StringU5BU5D_t4054002952* File_ReadAllLines_m173915158 (Il2CppObject * __this /* static, unused */, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] System.IO.File::ReadAllLines(System.String,System.Text.Encoding)
extern "C"  StringU5BU5D_t4054002952* File_ReadAllLines_m1678744763 (Il2CppObject * __this /* static, unused */, String_t* ___path0, Encoding_t2012439129 * ___encoding1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] System.IO.File::ReadAllLines(System.IO.StreamReader)
extern "C"  StringU5BU5D_t4054002952* File_ReadAllLines_m3534810368 (Il2CppObject * __this /* static, unused */, StreamReader_t2549717843 * ___reader0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.IO.File::ReadAllText(System.String)
extern "C"  String_t* File_ReadAllText_m3411439778 (Il2CppObject * __this /* static, unused */, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.IO.File::ReadAllText(System.String,System.Text.Encoding)
extern "C"  String_t* File_ReadAllText_m108099503 (Il2CppObject * __this /* static, unused */, String_t* ___path0, Encoding_t2012439129 * ___encoding1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.File::WriteAllBytes(System.String,System.Byte[])
extern "C"  void File_WriteAllBytes_m2419938065 (Il2CppObject * __this /* static, unused */, String_t* ___path0, ByteU5BU5D_t4260760469* ___bytes1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.File::WriteAllLines(System.String,System.String[])
extern "C"  void File_WriteAllLines_m2476731860 (Il2CppObject * __this /* static, unused */, String_t* ___path0, StringU5BU5D_t4054002952* ___contents1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.File::WriteAllLines(System.String,System.String[],System.Text.Encoding)
extern "C"  void File_WriteAllLines_m134647229 (Il2CppObject * __this /* static, unused */, String_t* ___path0, StringU5BU5D_t4054002952* ___contents1, Encoding_t2012439129 * ___encoding2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.File::WriteAllLines(System.IO.StreamWriter,System.String[])
extern "C"  void File_WriteAllLines_m2174077638 (Il2CppObject * __this /* static, unused */, StreamWriter_t2705123075 * ___writer0, StringU5BU5D_t4054002952* ___contents1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.File::WriteAllText(System.String,System.String)
extern "C"  void File_WriteAllText_m4080768088 (Il2CppObject * __this /* static, unused */, String_t* ___path0, String_t* ___contents1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.File::WriteAllText(System.String,System.String,System.Text.Encoding)
extern "C"  void File_WriteAllText_m457367993 (Il2CppObject * __this /* static, unused */, String_t* ___path0, String_t* ___contents1, Encoding_t2012439129 * ___encoding2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.IO.File::get_DefaultLocalFileTime()
extern "C"  DateTime_t4283661327  File_get_DefaultLocalFileTime_m2149273196 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.File::Encrypt(System.String)
extern "C"  void File_Encrypt_m3148137842 (Il2CppObject * __this /* static, unused */, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.File::Decrypt(System.String)
extern "C"  void File_Decrypt_m3284035914 (Il2CppObject * __this /* static, unused */, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
