﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Appegg/HotfixConfig
struct HotfixConfig_t2954663925;

#include "codegen/il2cpp-codegen.h"

// System.Void Appegg/HotfixConfig::.ctor()
extern "C"  void HotfixConfig__ctor_m1837408022 (HotfixConfig_t2954663925 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
