﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_Pathfinding_AdvancedSmooth_TurnC3543216936.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.AdvancedSmooth/ConstantTurn
struct  ConstantTurn_t1658810934  : public TurnConstructor_t3543216936
{
public:
	// UnityEngine.Vector3 Pathfinding.AdvancedSmooth/ConstantTurn::circleCenter
	Vector3_t4282066566  ___circleCenter_12;
	// System.Double Pathfinding.AdvancedSmooth/ConstantTurn::gamma1
	double ___gamma1_13;
	// System.Double Pathfinding.AdvancedSmooth/ConstantTurn::gamma2
	double ___gamma2_14;
	// System.Boolean Pathfinding.AdvancedSmooth/ConstantTurn::clockwise
	bool ___clockwise_15;

public:
	inline static int32_t get_offset_of_circleCenter_12() { return static_cast<int32_t>(offsetof(ConstantTurn_t1658810934, ___circleCenter_12)); }
	inline Vector3_t4282066566  get_circleCenter_12() const { return ___circleCenter_12; }
	inline Vector3_t4282066566 * get_address_of_circleCenter_12() { return &___circleCenter_12; }
	inline void set_circleCenter_12(Vector3_t4282066566  value)
	{
		___circleCenter_12 = value;
	}

	inline static int32_t get_offset_of_gamma1_13() { return static_cast<int32_t>(offsetof(ConstantTurn_t1658810934, ___gamma1_13)); }
	inline double get_gamma1_13() const { return ___gamma1_13; }
	inline double* get_address_of_gamma1_13() { return &___gamma1_13; }
	inline void set_gamma1_13(double value)
	{
		___gamma1_13 = value;
	}

	inline static int32_t get_offset_of_gamma2_14() { return static_cast<int32_t>(offsetof(ConstantTurn_t1658810934, ___gamma2_14)); }
	inline double get_gamma2_14() const { return ___gamma2_14; }
	inline double* get_address_of_gamma2_14() { return &___gamma2_14; }
	inline void set_gamma2_14(double value)
	{
		___gamma2_14 = value;
	}

	inline static int32_t get_offset_of_clockwise_15() { return static_cast<int32_t>(offsetof(ConstantTurn_t1658810934, ___clockwise_15)); }
	inline bool get_clockwise_15() const { return ___clockwise_15; }
	inline bool* get_address_of_clockwise_15() { return &___clockwise_15; }
	inline void set_clockwise_15(bool value)
	{
		___clockwise_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
