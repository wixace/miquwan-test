﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mihua_Asset_ABLoadOperation_AssetOperationGenerated
struct Mihua_Asset_ABLoadOperation_AssetOperationGenerated_t2981471075;
// JSVCall
struct JSVCall_t3708497963;
// UnityEngine.Object
struct Object_t3071478659;
// Mihua.Asset.ABLoadOperation.AssetOperation
struct AssetOperation_t778728221;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_Mihua_Asset_ABLoadOperation_Asset778728221.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void Mihua_Asset_ABLoadOperation_AssetOperationGenerated::.ctor()
extern "C"  void Mihua_Asset_ABLoadOperation_AssetOperationGenerated__ctor_m307294056 (Mihua_Asset_ABLoadOperation_AssetOperationGenerated_t2981471075 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua_Asset_ABLoadOperation_AssetOperationGenerated::AssetOperation_isAsync(JSVCall)
extern "C"  void Mihua_Asset_ABLoadOperation_AssetOperationGenerated_AssetOperation_isAsync_m1987441089 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua_Asset_ABLoadOperation_AssetOperationGenerated::AssetOperation_isInit(JSVCall)
extern "C"  void Mihua_Asset_ABLoadOperation_AssetOperationGenerated_AssetOperation_isInit_m3659660383 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mihua_Asset_ABLoadOperation_AssetOperationGenerated::AssetOperation_GetAsset(JSVCall,System.Int32)
extern "C"  bool Mihua_Asset_ABLoadOperation_AssetOperationGenerated_AssetOperation_GetAsset_m968982860 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mihua_Asset_ABLoadOperation_AssetOperationGenerated::AssetOperation_Init(JSVCall,System.Int32)
extern "C"  bool Mihua_Asset_ABLoadOperation_AssetOperationGenerated_AssetOperation_Init_m750491938 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua_Asset_ABLoadOperation_AssetOperationGenerated::__Register()
extern "C"  void Mihua_Asset_ABLoadOperation_AssetOperationGenerated___Register_m3439717983 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua_Asset_ABLoadOperation_AssetOperationGenerated::ilo_setBooleanS1(System.Int32,System.Boolean)
extern "C"  void Mihua_Asset_ABLoadOperation_AssetOperationGenerated_ilo_setBooleanS1_m3050931161 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object Mihua_Asset_ABLoadOperation_AssetOperationGenerated::ilo_GetAsset2(Mihua.Asset.ABLoadOperation.AssetOperation)
extern "C"  Object_t3071478659 * Mihua_Asset_ABLoadOperation_AssetOperationGenerated_ilo_GetAsset2_m3450239257 (Il2CppObject * __this /* static, unused */, AssetOperation_t778728221 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mihua_Asset_ABLoadOperation_AssetOperationGenerated::ilo_setObject3(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t Mihua_Asset_ABLoadOperation_AssetOperationGenerated_ilo_setObject3_m4247241416 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua_Asset_ABLoadOperation_AssetOperationGenerated::ilo_Init4(Mihua.Asset.ABLoadOperation.AssetOperation)
extern "C"  void Mihua_Asset_ABLoadOperation_AssetOperationGenerated_ilo_Init4_m2173262142 (Il2CppObject * __this /* static, unused */, AssetOperation_t778728221 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
