﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_siscewisKassarqel79
struct  M_siscewisKassarqel79_t189522833  : public Il2CppObject
{
public:
	// System.Int32 GarbageiOS.M_siscewisKassarqel79::_jorcursarNismujee
	int32_t ____jorcursarNismujee_0;
	// System.Boolean GarbageiOS.M_siscewisKassarqel79::_xisurpelWurdaime
	bool ____xisurpelWurdaime_1;
	// System.Boolean GarbageiOS.M_siscewisKassarqel79::_hairedear
	bool ____hairedear_2;
	// System.Single GarbageiOS.M_siscewisKassarqel79::_yirhu
	float ____yirhu_3;
	// System.Int32 GarbageiOS.M_siscewisKassarqel79::_walnoutriBarcee
	int32_t ____walnoutriBarcee_4;
	// System.String GarbageiOS.M_siscewisKassarqel79::_mawbi
	String_t* ____mawbi_5;
	// System.UInt32 GarbageiOS.M_siscewisKassarqel79::_cicecaNelsta
	uint32_t ____cicecaNelsta_6;
	// System.Single GarbageiOS.M_siscewisKassarqel79::_rase
	float ____rase_7;
	// System.Single GarbageiOS.M_siscewisKassarqel79::_reguNeresal
	float ____reguNeresal_8;
	// System.Boolean GarbageiOS.M_siscewisKassarqel79::_melmea
	bool ____melmea_9;
	// System.Boolean GarbageiOS.M_siscewisKassarqel79::_vallraymal
	bool ____vallraymal_10;
	// System.Int32 GarbageiOS.M_siscewisKassarqel79::_hasvefou
	int32_t ____hasvefou_11;
	// System.String GarbageiOS.M_siscewisKassarqel79::_cejelRearjehou
	String_t* ____cejelRearjehou_12;
	// System.Int32 GarbageiOS.M_siscewisKassarqel79::_pebajer
	int32_t ____pebajer_13;
	// System.UInt32 GarbageiOS.M_siscewisKassarqel79::_gakiBomojur
	uint32_t ____gakiBomojur_14;
	// System.Boolean GarbageiOS.M_siscewisKassarqel79::_vowkunor
	bool ____vowkunor_15;
	// System.String GarbageiOS.M_siscewisKassarqel79::_ceqearrooJafujur
	String_t* ____ceqearrooJafujur_16;
	// System.UInt32 GarbageiOS.M_siscewisKassarqel79::_lifeeNitra
	uint32_t ____lifeeNitra_17;
	// System.Single GarbageiOS.M_siscewisKassarqel79::_pesenir
	float ____pesenir_18;
	// System.String GarbageiOS.M_siscewisKassarqel79::_jawjesalFerepi
	String_t* ____jawjesalFerepi_19;
	// System.Int32 GarbageiOS.M_siscewisKassarqel79::_mefee
	int32_t ____mefee_20;
	// System.Single GarbageiOS.M_siscewisKassarqel79::_pelstu
	float ____pelstu_21;
	// System.UInt32 GarbageiOS.M_siscewisKassarqel79::_foocaNeagewa
	uint32_t ____foocaNeagewa_22;

public:
	inline static int32_t get_offset_of__jorcursarNismujee_0() { return static_cast<int32_t>(offsetof(M_siscewisKassarqel79_t189522833, ____jorcursarNismujee_0)); }
	inline int32_t get__jorcursarNismujee_0() const { return ____jorcursarNismujee_0; }
	inline int32_t* get_address_of__jorcursarNismujee_0() { return &____jorcursarNismujee_0; }
	inline void set__jorcursarNismujee_0(int32_t value)
	{
		____jorcursarNismujee_0 = value;
	}

	inline static int32_t get_offset_of__xisurpelWurdaime_1() { return static_cast<int32_t>(offsetof(M_siscewisKassarqel79_t189522833, ____xisurpelWurdaime_1)); }
	inline bool get__xisurpelWurdaime_1() const { return ____xisurpelWurdaime_1; }
	inline bool* get_address_of__xisurpelWurdaime_1() { return &____xisurpelWurdaime_1; }
	inline void set__xisurpelWurdaime_1(bool value)
	{
		____xisurpelWurdaime_1 = value;
	}

	inline static int32_t get_offset_of__hairedear_2() { return static_cast<int32_t>(offsetof(M_siscewisKassarqel79_t189522833, ____hairedear_2)); }
	inline bool get__hairedear_2() const { return ____hairedear_2; }
	inline bool* get_address_of__hairedear_2() { return &____hairedear_2; }
	inline void set__hairedear_2(bool value)
	{
		____hairedear_2 = value;
	}

	inline static int32_t get_offset_of__yirhu_3() { return static_cast<int32_t>(offsetof(M_siscewisKassarqel79_t189522833, ____yirhu_3)); }
	inline float get__yirhu_3() const { return ____yirhu_3; }
	inline float* get_address_of__yirhu_3() { return &____yirhu_3; }
	inline void set__yirhu_3(float value)
	{
		____yirhu_3 = value;
	}

	inline static int32_t get_offset_of__walnoutriBarcee_4() { return static_cast<int32_t>(offsetof(M_siscewisKassarqel79_t189522833, ____walnoutriBarcee_4)); }
	inline int32_t get__walnoutriBarcee_4() const { return ____walnoutriBarcee_4; }
	inline int32_t* get_address_of__walnoutriBarcee_4() { return &____walnoutriBarcee_4; }
	inline void set__walnoutriBarcee_4(int32_t value)
	{
		____walnoutriBarcee_4 = value;
	}

	inline static int32_t get_offset_of__mawbi_5() { return static_cast<int32_t>(offsetof(M_siscewisKassarqel79_t189522833, ____mawbi_5)); }
	inline String_t* get__mawbi_5() const { return ____mawbi_5; }
	inline String_t** get_address_of__mawbi_5() { return &____mawbi_5; }
	inline void set__mawbi_5(String_t* value)
	{
		____mawbi_5 = value;
		Il2CppCodeGenWriteBarrier(&____mawbi_5, value);
	}

	inline static int32_t get_offset_of__cicecaNelsta_6() { return static_cast<int32_t>(offsetof(M_siscewisKassarqel79_t189522833, ____cicecaNelsta_6)); }
	inline uint32_t get__cicecaNelsta_6() const { return ____cicecaNelsta_6; }
	inline uint32_t* get_address_of__cicecaNelsta_6() { return &____cicecaNelsta_6; }
	inline void set__cicecaNelsta_6(uint32_t value)
	{
		____cicecaNelsta_6 = value;
	}

	inline static int32_t get_offset_of__rase_7() { return static_cast<int32_t>(offsetof(M_siscewisKassarqel79_t189522833, ____rase_7)); }
	inline float get__rase_7() const { return ____rase_7; }
	inline float* get_address_of__rase_7() { return &____rase_7; }
	inline void set__rase_7(float value)
	{
		____rase_7 = value;
	}

	inline static int32_t get_offset_of__reguNeresal_8() { return static_cast<int32_t>(offsetof(M_siscewisKassarqel79_t189522833, ____reguNeresal_8)); }
	inline float get__reguNeresal_8() const { return ____reguNeresal_8; }
	inline float* get_address_of__reguNeresal_8() { return &____reguNeresal_8; }
	inline void set__reguNeresal_8(float value)
	{
		____reguNeresal_8 = value;
	}

	inline static int32_t get_offset_of__melmea_9() { return static_cast<int32_t>(offsetof(M_siscewisKassarqel79_t189522833, ____melmea_9)); }
	inline bool get__melmea_9() const { return ____melmea_9; }
	inline bool* get_address_of__melmea_9() { return &____melmea_9; }
	inline void set__melmea_9(bool value)
	{
		____melmea_9 = value;
	}

	inline static int32_t get_offset_of__vallraymal_10() { return static_cast<int32_t>(offsetof(M_siscewisKassarqel79_t189522833, ____vallraymal_10)); }
	inline bool get__vallraymal_10() const { return ____vallraymal_10; }
	inline bool* get_address_of__vallraymal_10() { return &____vallraymal_10; }
	inline void set__vallraymal_10(bool value)
	{
		____vallraymal_10 = value;
	}

	inline static int32_t get_offset_of__hasvefou_11() { return static_cast<int32_t>(offsetof(M_siscewisKassarqel79_t189522833, ____hasvefou_11)); }
	inline int32_t get__hasvefou_11() const { return ____hasvefou_11; }
	inline int32_t* get_address_of__hasvefou_11() { return &____hasvefou_11; }
	inline void set__hasvefou_11(int32_t value)
	{
		____hasvefou_11 = value;
	}

	inline static int32_t get_offset_of__cejelRearjehou_12() { return static_cast<int32_t>(offsetof(M_siscewisKassarqel79_t189522833, ____cejelRearjehou_12)); }
	inline String_t* get__cejelRearjehou_12() const { return ____cejelRearjehou_12; }
	inline String_t** get_address_of__cejelRearjehou_12() { return &____cejelRearjehou_12; }
	inline void set__cejelRearjehou_12(String_t* value)
	{
		____cejelRearjehou_12 = value;
		Il2CppCodeGenWriteBarrier(&____cejelRearjehou_12, value);
	}

	inline static int32_t get_offset_of__pebajer_13() { return static_cast<int32_t>(offsetof(M_siscewisKassarqel79_t189522833, ____pebajer_13)); }
	inline int32_t get__pebajer_13() const { return ____pebajer_13; }
	inline int32_t* get_address_of__pebajer_13() { return &____pebajer_13; }
	inline void set__pebajer_13(int32_t value)
	{
		____pebajer_13 = value;
	}

	inline static int32_t get_offset_of__gakiBomojur_14() { return static_cast<int32_t>(offsetof(M_siscewisKassarqel79_t189522833, ____gakiBomojur_14)); }
	inline uint32_t get__gakiBomojur_14() const { return ____gakiBomojur_14; }
	inline uint32_t* get_address_of__gakiBomojur_14() { return &____gakiBomojur_14; }
	inline void set__gakiBomojur_14(uint32_t value)
	{
		____gakiBomojur_14 = value;
	}

	inline static int32_t get_offset_of__vowkunor_15() { return static_cast<int32_t>(offsetof(M_siscewisKassarqel79_t189522833, ____vowkunor_15)); }
	inline bool get__vowkunor_15() const { return ____vowkunor_15; }
	inline bool* get_address_of__vowkunor_15() { return &____vowkunor_15; }
	inline void set__vowkunor_15(bool value)
	{
		____vowkunor_15 = value;
	}

	inline static int32_t get_offset_of__ceqearrooJafujur_16() { return static_cast<int32_t>(offsetof(M_siscewisKassarqel79_t189522833, ____ceqearrooJafujur_16)); }
	inline String_t* get__ceqearrooJafujur_16() const { return ____ceqearrooJafujur_16; }
	inline String_t** get_address_of__ceqearrooJafujur_16() { return &____ceqearrooJafujur_16; }
	inline void set__ceqearrooJafujur_16(String_t* value)
	{
		____ceqearrooJafujur_16 = value;
		Il2CppCodeGenWriteBarrier(&____ceqearrooJafujur_16, value);
	}

	inline static int32_t get_offset_of__lifeeNitra_17() { return static_cast<int32_t>(offsetof(M_siscewisKassarqel79_t189522833, ____lifeeNitra_17)); }
	inline uint32_t get__lifeeNitra_17() const { return ____lifeeNitra_17; }
	inline uint32_t* get_address_of__lifeeNitra_17() { return &____lifeeNitra_17; }
	inline void set__lifeeNitra_17(uint32_t value)
	{
		____lifeeNitra_17 = value;
	}

	inline static int32_t get_offset_of__pesenir_18() { return static_cast<int32_t>(offsetof(M_siscewisKassarqel79_t189522833, ____pesenir_18)); }
	inline float get__pesenir_18() const { return ____pesenir_18; }
	inline float* get_address_of__pesenir_18() { return &____pesenir_18; }
	inline void set__pesenir_18(float value)
	{
		____pesenir_18 = value;
	}

	inline static int32_t get_offset_of__jawjesalFerepi_19() { return static_cast<int32_t>(offsetof(M_siscewisKassarqel79_t189522833, ____jawjesalFerepi_19)); }
	inline String_t* get__jawjesalFerepi_19() const { return ____jawjesalFerepi_19; }
	inline String_t** get_address_of__jawjesalFerepi_19() { return &____jawjesalFerepi_19; }
	inline void set__jawjesalFerepi_19(String_t* value)
	{
		____jawjesalFerepi_19 = value;
		Il2CppCodeGenWriteBarrier(&____jawjesalFerepi_19, value);
	}

	inline static int32_t get_offset_of__mefee_20() { return static_cast<int32_t>(offsetof(M_siscewisKassarqel79_t189522833, ____mefee_20)); }
	inline int32_t get__mefee_20() const { return ____mefee_20; }
	inline int32_t* get_address_of__mefee_20() { return &____mefee_20; }
	inline void set__mefee_20(int32_t value)
	{
		____mefee_20 = value;
	}

	inline static int32_t get_offset_of__pelstu_21() { return static_cast<int32_t>(offsetof(M_siscewisKassarqel79_t189522833, ____pelstu_21)); }
	inline float get__pelstu_21() const { return ____pelstu_21; }
	inline float* get_address_of__pelstu_21() { return &____pelstu_21; }
	inline void set__pelstu_21(float value)
	{
		____pelstu_21 = value;
	}

	inline static int32_t get_offset_of__foocaNeagewa_22() { return static_cast<int32_t>(offsetof(M_siscewisKassarqel79_t189522833, ____foocaNeagewa_22)); }
	inline uint32_t get__foocaNeagewa_22() const { return ____foocaNeagewa_22; }
	inline uint32_t* get_address_of__foocaNeagewa_22() { return &____foocaNeagewa_22; }
	inline void set__foocaNeagewa_22(uint32_t value)
	{
		____foocaNeagewa_22 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
