﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CsCfgBase
struct CsCfgBase_t69924517;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;

#include "codegen/il2cpp-codegen.h"

// System.Void CsCfgBase::.ctor()
extern "C"  void CsCfgBase__ctor_m2386217062 (CsCfgBase_t69924517 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CsCfgBase::Init(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C"  void CsCfgBase_Init_m3498028383 (CsCfgBase_t69924517 * __this, Dictionary_2_t827649927 * ___info0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
