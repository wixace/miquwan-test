﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen4083548623.h"
#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DCSharp_AnimationRunner_AniType1006238651.h"

// System.Void System.Array/InternalEnumerator`1<AnimationRunner/AniType>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m15960192_gshared (InternalEnumerator_1_t4083548623 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m15960192(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t4083548623 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m15960192_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<AnimationRunner/AniType>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1934155040_gshared (InternalEnumerator_1_t4083548623 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1934155040(__this, method) ((  void (*) (InternalEnumerator_1_t4083548623 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1934155040_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<AnimationRunner/AniType>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m554652310_gshared (InternalEnumerator_1_t4083548623 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m554652310(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t4083548623 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m554652310_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<AnimationRunner/AniType>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1535750743_gshared (InternalEnumerator_1_t4083548623 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m1535750743(__this, method) ((  void (*) (InternalEnumerator_1_t4083548623 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1535750743_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<AnimationRunner/AniType>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m194000848_gshared (InternalEnumerator_1_t4083548623 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m194000848(__this, method) ((  bool (*) (InternalEnumerator_1_t4083548623 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m194000848_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<AnimationRunner/AniType>::get_Current()
extern "C"  int32_t InternalEnumerator_1_get_Current_m3171752489_gshared (InternalEnumerator_1_t4083548623 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m3171752489(__this, method) ((  int32_t (*) (InternalEnumerator_1_t4083548623 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3171752489_gshared)(__this, method)
