﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Physics
struct Physics_t3358180733;
// UnityEngine.RaycastHit[]
struct RaycastHitU5BU5D_t528650843;
// UnityEngine.Collider[]
struct ColliderU5BU5D_t2697150633;
// UnityEngine.Collider
struct Collider_t2939674232;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_QueryTriggerInteraction577895768.h"
#include "UnityEngine_UnityEngine_RaycastHit4003175726.h"
#include "UnityEngine_UnityEngine_Ray3134616544.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882.h"
#include "UnityEngine_UnityEngine_Collider2939674232.h"

// System.Void UnityEngine.Physics::.ctor()
extern "C"  void Physics__ctor_m3753778162 (Physics_t3358180733 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Physics::get_gravity()
extern "C"  Vector3_t4282066566  Physics_get_gravity_m2907531023 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Physics::set_gravity(UnityEngine.Vector3)
extern "C"  void Physics_set_gravity_m2814881048 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Physics::INTERNAL_get_gravity(UnityEngine.Vector3&)
extern "C"  void Physics_INTERNAL_get_gravity_m3320492712 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Physics::INTERNAL_set_gravity(UnityEngine.Vector3&)
extern "C"  void Physics_INTERNAL_set_gravity_m2886173364 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Physics::get_defaultContactOffset()
extern "C"  float Physics_get_defaultContactOffset_m730014337 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Physics::set_defaultContactOffset(System.Single)
extern "C"  void Physics_set_defaultContactOffset_m104029898 (Il2CppObject * __this /* static, unused */, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Physics::get_bounceThreshold()
extern "C"  float Physics_get_bounceThreshold_m1786943574 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Physics::set_bounceThreshold(System.Single)
extern "C"  void Physics_set_bounceThreshold_m578797333 (Il2CppObject * __this /* static, unused */, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Physics::get_solverIterationCount()
extern "C"  int32_t Physics_get_solverIterationCount_m1587919072 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Physics::set_solverIterationCount(System.Int32)
extern "C"  void Physics_set_solverIterationCount_m781940261 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Physics::get_sleepThreshold()
extern "C"  float Physics_get_sleepThreshold_m730551235 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Physics::set_sleepThreshold(System.Single)
extern "C"  void Physics_set_sleepThreshold_m61163720 (Il2CppObject * __this /* static, unused */, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics::get_queriesHitTriggers()
extern "C"  bool Physics_get_queriesHitTriggers_m730106365 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Physics::set_queriesHitTriggers(System.Boolean)
extern "C"  void Physics_set_queriesHitTriggers_m2342344398 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32)
extern "C"  bool Physics_Raycast_m2062764485 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___origin0, Vector3_t4282066566  ___direction1, float ___maxDistance2, int32_t ___layerMask3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern "C"  bool Physics_Raycast_m3288557650 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___origin0, Vector3_t4282066566  ___direction1, float ___maxDistance2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  bool Physics_Raycast_m910897197 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___origin0, Vector3_t4282066566  ___direction1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  bool Physics_Raycast_m2714144326 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___origin0, Vector3_t4282066566  ___direction1, float ___maxDistance2, int32_t ___layerMask3, int32_t ___queryTriggerInteraction4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32)
extern "C"  bool Physics_Raycast_m267364350 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___origin0, Vector3_t4282066566  ___direction1, RaycastHit_t4003175726 * ___hitInfo2, float ___maxDistance3, int32_t ___layerMask4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single)
extern "C"  bool Physics_Raycast_m395141497 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___origin0, Vector3_t4282066566  ___direction1, RaycastHit_t4003175726 * ___hitInfo2, float ___maxDistance3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&)
extern "C"  bool Physics_Raycast_m2482317716 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___origin0, Vector3_t4282066566  ___direction1, RaycastHit_t4003175726 * ___hitInfo2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  bool Physics_Raycast_m1758069759 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___origin0, Vector3_t4282066566  ___direction1, RaycastHit_t4003175726 * ___hitInfo2, float ___maxDistance3, int32_t ___layerMask4, int32_t ___queryTriggerInteraction5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,System.Single,System.Int32)
extern "C"  bool Physics_Raycast_m1027100562 (Il2CppObject * __this /* static, unused */, Ray_t3134616544  ___ray0, float ___maxDistance1, int32_t ___layerMask2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,System.Single)
extern "C"  bool Physics_Raycast_m921172325 (Il2CppObject * __this /* static, unused */, Ray_t3134616544  ___ray0, float ___maxDistance1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray)
extern "C"  bool Physics_Raycast_m4265135744 (Il2CppObject * __this /* static, unused */, Ray_t3134616544  ___ray0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  bool Physics_Raycast_m1779528083 (Il2CppObject * __this /* static, unused */, Ray_t3134616544  ___ray0, float ___maxDistance1, int32_t ___layerMask2, int32_t ___queryTriggerInteraction3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,UnityEngine.RaycastHit&,System.Single,System.Int32)
extern "C"  bool Physics_Raycast_m1600345803 (Il2CppObject * __this /* static, unused */, Ray_t3134616544  ___ray0, RaycastHit_t4003175726 * ___hitInfo1, float ___maxDistance2, int32_t ___layerMask3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,UnityEngine.RaycastHit&,System.Single)
extern "C"  bool Physics_Raycast_m1235528076 (Il2CppObject * __this /* static, unused */, Ray_t3134616544  ___ray0, RaycastHit_t4003175726 * ___hitInfo1, float ___maxDistance2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,UnityEngine.RaycastHit&)
extern "C"  bool Physics_Raycast_m1343340263 (Il2CppObject * __this /* static, unused */, Ray_t3134616544  ___ray0, RaycastHit_t4003175726 * ___hitInfo1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  bool Physics_Raycast_m165875788 (Il2CppObject * __this /* static, unused */, Ray_t3134616544  ___ray0, RaycastHit_t4003175726 * ___hitInfo1, float ___maxDistance2, int32_t ___layerMask3, int32_t ___queryTriggerInteraction4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Ray,System.Single,System.Int32)
extern "C"  RaycastHitU5BU5D_t528650843* Physics_RaycastAll_m1771931441 (Il2CppObject * __this /* static, unused */, Ray_t3134616544  ___ray0, float ___maxDistance1, int32_t ___layerMask2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Ray,System.Single)
extern "C"  RaycastHitU5BU5D_t528650843* Physics_RaycastAll_m2128989030 (Il2CppObject * __this /* static, unused */, Ray_t3134616544  ___ray0, float ___maxDistance1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Ray)
extern "C"  RaycastHitU5BU5D_t528650843* Physics_RaycastAll_m1690814017 (Il2CppObject * __this /* static, unused */, Ray_t3134616544  ___ray0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Ray,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  RaycastHitU5BU5D_t528650843* Physics_RaycastAll_m1269007794 (Il2CppObject * __this /* static, unused */, Ray_t3134616544  ___ray0, float ___maxDistance1, int32_t ___layerMask2, int32_t ___queryTriggerInteraction3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  RaycastHitU5BU5D_t528650843* Physics_RaycastAll_m892728677 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___origin0, Vector3_t4282066566  ___direction1, float ___maxDistance2, int32_t ___layermask3, int32_t ___queryTriggerInteraction4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32)
extern "C"  RaycastHitU5BU5D_t528650843* Physics_RaycastAll_m2195936356 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___origin0, Vector3_t4282066566  ___direction1, float ___maxDistance2, int32_t ___layermask3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern "C"  RaycastHitU5BU5D_t528650843* Physics_RaycastAll_m1213957971 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___origin0, Vector3_t4282066566  ___direction1, float ___maxDistance2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  RaycastHitU5BU5D_t528650843* Physics_RaycastAll_m2509190382 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___origin0, Vector3_t4282066566  ___direction1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit[] UnityEngine.Physics::INTERNAL_CALL_RaycastAll(UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  RaycastHitU5BU5D_t528650843* Physics_INTERNAL_CALL_RaycastAll_m2642095530 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566 * ___origin0, Vector3_t4282066566 * ___direction1, float ___maxDistance2, int32_t ___layermask3, int32_t ___queryTriggerInteraction4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Physics::RaycastNonAlloc(UnityEngine.Ray,UnityEngine.RaycastHit[],System.Single,System.Int32)
extern "C"  int32_t Physics_RaycastNonAlloc_m3375503085 (Il2CppObject * __this /* static, unused */, Ray_t3134616544  ___ray0, RaycastHitU5BU5D_t528650843* ___results1, float ___maxDistance2, int32_t ___layerMask3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Physics::RaycastNonAlloc(UnityEngine.Ray,UnityEngine.RaycastHit[],System.Single)
extern "C"  int32_t Physics_RaycastNonAlloc_m1828084266 (Il2CppObject * __this /* static, unused */, Ray_t3134616544  ___ray0, RaycastHitU5BU5D_t528650843* ___results1, float ___maxDistance2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Physics::RaycastNonAlloc(UnityEngine.Ray,UnityEngine.RaycastHit[])
extern "C"  int32_t Physics_RaycastNonAlloc_m1668004869 (Il2CppObject * __this /* static, unused */, Ray_t3134616544  ___ray0, RaycastHitU5BU5D_t528650843* ___results1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Physics::RaycastNonAlloc(UnityEngine.Ray,UnityEngine.RaycastHit[],System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  int32_t Physics_RaycastNonAlloc_m2677794670 (Il2CppObject * __this /* static, unused */, Ray_t3134616544  ___ray0, RaycastHitU5BU5D_t528650843* ___results1, float ___maxDistance2, int32_t ___layerMask3, int32_t ___queryTriggerInteraction4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Physics::RaycastNonAlloc(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit[],System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  int32_t Physics_RaycastNonAlloc_m2638442523 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___origin0, Vector3_t4282066566  ___direction1, RaycastHitU5BU5D_t528650843* ___results2, float ___maxDistance3, int32_t ___layermask4, int32_t ___queryTriggerInteraction5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Physics::RaycastNonAlloc(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit[],System.Single,System.Int32)
extern "C"  int32_t Physics_RaycastNonAlloc_m4234867226 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___origin0, Vector3_t4282066566  ___direction1, RaycastHitU5BU5D_t528650843* ___results2, float ___maxDistance3, int32_t ___layermask4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Physics::RaycastNonAlloc(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit[],System.Single)
extern "C"  int32_t Physics_RaycastNonAlloc_m2826426845 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___origin0, Vector3_t4282066566  ___direction1, RaycastHitU5BU5D_t528650843* ___results2, float ___maxDistance3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Physics::RaycastNonAlloc(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit[])
extern "C"  int32_t Physics_RaycastNonAlloc_m4111884536 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___origin0, Vector3_t4282066566  ___direction1, RaycastHitU5BU5D_t528650843* ___results2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Physics::INTERNAL_CALL_RaycastNonAlloc(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.RaycastHit[],System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  int32_t Physics_INTERNAL_CALL_RaycastNonAlloc_m3837704468 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566 * ___origin0, Vector3_t4282066566 * ___direction1, RaycastHitU5BU5D_t528650843* ___results2, float ___maxDistance3, int32_t ___layermask4, int32_t ___queryTriggerInteraction5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics::Linecast(UnityEngine.Vector3,UnityEngine.Vector3,System.Int32)
extern "C"  bool Physics_Linecast_m3546841930 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___start0, Vector3_t4282066566  ___end1, int32_t ___layerMask2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics::Linecast(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  bool Physics_Linecast_m3503692461 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___start0, Vector3_t4282066566  ___end1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics::Linecast(UnityEngine.Vector3,UnityEngine.Vector3,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  bool Physics_Linecast_m939796811 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___start0, Vector3_t4282066566  ___end1, int32_t ___layerMask2, int32_t ___queryTriggerInteraction3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics::Linecast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Int32)
extern "C"  bool Physics_Linecast_m2331953475 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___start0, Vector3_t4282066566  ___end1, RaycastHit_t4003175726 * ___hitInfo2, int32_t ___layerMask3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics::Linecast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&)
extern "C"  bool Physics_Linecast_m4010316820 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___start0, Vector3_t4282066566  ___end1, RaycastHit_t4003175726 * ___hitInfo2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics::Linecast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  bool Physics_Linecast_m3067651780 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___start0, Vector3_t4282066566  ___end1, RaycastHit_t4003175726 * ___hitInfo2, int32_t ___layerMask3, int32_t ___queryTriggerInteraction4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider[] UnityEngine.Physics::OverlapSphere(UnityEngine.Vector3,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  ColliderU5BU5D_t2697150633* Physics_OverlapSphere_m2490747040 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___position0, float ___radius1, int32_t ___layerMask2, int32_t ___queryTriggerInteraction3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider[] UnityEngine.Physics::OverlapSphere(UnityEngine.Vector3,System.Single,System.Int32)
extern "C"  ColliderU5BU5D_t2697150633* Physics_OverlapSphere_m1847961887 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___position0, float ___radius1, int32_t ___layerMask2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider[] UnityEngine.Physics::OverlapSphere(UnityEngine.Vector3,System.Single)
extern "C"  ColliderU5BU5D_t2697150633* Physics_OverlapSphere_m359079608 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___position0, float ___radius1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider[] UnityEngine.Physics::INTERNAL_CALL_OverlapSphere(UnityEngine.Vector3&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  ColliderU5BU5D_t2697150633* Physics_INTERNAL_CALL_OverlapSphere_m4255329177 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566 * ___position0, float ___radius1, int32_t ___layerMask2, int32_t ___queryTriggerInteraction3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Physics::OverlapSphereNonAlloc(UnityEngine.Vector3,System.Single,UnityEngine.Collider[],System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  int32_t Physics_OverlapSphereNonAlloc_m2811209809 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___position0, float ___radius1, ColliderU5BU5D_t2697150633* ___results2, int32_t ___layerMask3, int32_t ___queryTriggerInteraction4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Physics::OverlapSphereNonAlloc(UnityEngine.Vector3,System.Single,UnityEngine.Collider[],System.Int32)
extern "C"  int32_t Physics_OverlapSphereNonAlloc_m2910981456 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___position0, float ___radius1, ColliderU5BU5D_t2697150633* ___results2, int32_t ___layerMask3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Physics::OverlapSphereNonAlloc(UnityEngine.Vector3,System.Single,UnityEngine.Collider[])
extern "C"  int32_t Physics_OverlapSphereNonAlloc_m3659972327 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___position0, float ___radius1, ColliderU5BU5D_t2697150633* ___results2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Physics::INTERNAL_CALL_OverlapSphereNonAlloc(UnityEngine.Vector3&,System.Single,UnityEngine.Collider[],System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  int32_t Physics_INTERNAL_CALL_OverlapSphereNonAlloc_m499466116 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566 * ___position0, float ___radius1, ColliderU5BU5D_t2697150633* ___results2, int32_t ___layerMask3, int32_t ___queryTriggerInteraction4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics::CapsuleCast(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,UnityEngine.Vector3,System.Single,System.Int32)
extern "C"  bool Physics_CapsuleCast_m846093808 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___point10, Vector3_t4282066566  ___point21, float ___radius2, Vector3_t4282066566  ___direction3, float ___maxDistance4, int32_t ___layerMask5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics::CapsuleCast(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,UnityEngine.Vector3,System.Single)
extern "C"  bool Physics_CapsuleCast_m2489576007 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___point10, Vector3_t4282066566  ___point21, float ___radius2, Vector3_t4282066566  ___direction3, float ___maxDistance4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics::CapsuleCast(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,UnityEngine.Vector3)
extern "C"  bool Physics_CapsuleCast_m4101837538 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___point10, Vector3_t4282066566  ___point21, float ___radius2, Vector3_t4282066566  ___direction3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics::CapsuleCast(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,UnityEngine.Vector3,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  bool Physics_CapsuleCast_m3782748401 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___point10, Vector3_t4282066566  ___point21, float ___radius2, Vector3_t4282066566  ___direction3, float ___maxDistance4, int32_t ___layerMask5, int32_t ___queryTriggerInteraction6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics::CapsuleCast(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32)
extern "C"  bool Physics_CapsuleCast_m3230378793 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___point10, Vector3_t4282066566  ___point21, float ___radius2, Vector3_t4282066566  ___direction3, RaycastHit_t4003175726 * ___hitInfo4, float ___maxDistance5, int32_t ___layerMask6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics::CapsuleCast(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single)
extern "C"  bool Physics_CapsuleCast_m2700383342 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___point10, Vector3_t4282066566  ___point21, float ___radius2, Vector3_t4282066566  ___direction3, RaycastHit_t4003175726 * ___hitInfo4, float ___maxDistance5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics::CapsuleCast(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,UnityEngine.Vector3,UnityEngine.RaycastHit&)
extern "C"  bool Physics_CapsuleCast_m692550985 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___point10, Vector3_t4282066566  ___point21, float ___radius2, Vector3_t4282066566  ___direction3, RaycastHit_t4003175726 * ___hitInfo4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics::CapsuleCast(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  bool Physics_CapsuleCast_m1074859434 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___point10, Vector3_t4282066566  ___point21, float ___radius2, Vector3_t4282066566  ___direction3, RaycastHit_t4003175726 * ___hitInfo4, float ___maxDistance5, int32_t ___layerMask6, int32_t ___queryTriggerInteraction7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics::SphereCast(UnityEngine.Vector3,System.Single,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32)
extern "C"  bool Physics_SphereCast_m3031395826 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___origin0, float ___radius1, Vector3_t4282066566  ___direction2, RaycastHit_t4003175726 * ___hitInfo3, float ___maxDistance4, int32_t ___layerMask5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics::SphereCast(UnityEngine.Vector3,System.Single,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single)
extern "C"  bool Physics_SphereCast_m2556362501 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___origin0, float ___radius1, Vector3_t4282066566  ___direction2, RaycastHit_t4003175726 * ___hitInfo3, float ___maxDistance4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics::SphereCast(UnityEngine.Vector3,System.Single,UnityEngine.Vector3,UnityEngine.RaycastHit&)
extern "C"  bool Physics_SphereCast_m338963488 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___origin0, float ___radius1, Vector3_t4282066566  ___direction2, RaycastHit_t4003175726 * ___hitInfo3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics::SphereCast(UnityEngine.Vector3,System.Single,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  bool Physics_SphereCast_m3149979635 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___origin0, float ___radius1, Vector3_t4282066566  ___direction2, RaycastHit_t4003175726 * ___hitInfo3, float ___maxDistance4, int32_t ___layerMask5, int32_t ___queryTriggerInteraction6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics::SphereCast(UnityEngine.Ray,System.Single,System.Single,System.Int32)
extern "C"  bool Physics_SphereCast_m490533510 (Il2CppObject * __this /* static, unused */, Ray_t3134616544  ___ray0, float ___radius1, float ___maxDistance2, int32_t ___layerMask3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics::SphereCast(UnityEngine.Ray,System.Single,System.Single)
extern "C"  bool Physics_SphereCast_m483498481 (Il2CppObject * __this /* static, unused */, Ray_t3134616544  ___ray0, float ___radius1, float ___maxDistance2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics::SphereCast(UnityEngine.Ray,System.Single)
extern "C"  bool Physics_SphereCast_m3129021452 (Il2CppObject * __this /* static, unused */, Ray_t3134616544  ___ray0, float ___radius1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics::SphereCast(UnityEngine.Ray,System.Single,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  bool Physics_SphereCast_m2279778951 (Il2CppObject * __this /* static, unused */, Ray_t3134616544  ___ray0, float ___radius1, float ___maxDistance2, int32_t ___layerMask3, int32_t ___queryTriggerInteraction4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics::SphereCast(UnityEngine.Ray,System.Single,UnityEngine.RaycastHit&,System.Single,System.Int32)
extern "C"  bool Physics_SphereCast_m36396479 (Il2CppObject * __this /* static, unused */, Ray_t3134616544  ___ray0, float ___radius1, RaycastHit_t4003175726 * ___hitInfo2, float ___maxDistance3, int32_t ___layerMask4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics::SphereCast(UnityEngine.Ray,System.Single,UnityEngine.RaycastHit&,System.Single)
extern "C"  bool Physics_SphereCast_m3206047256 (Il2CppObject * __this /* static, unused */, Ray_t3134616544  ___ray0, float ___radius1, RaycastHit_t4003175726 * ___hitInfo2, float ___maxDistance3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics::SphereCast(UnityEngine.Ray,System.Single,UnityEngine.RaycastHit&)
extern "C"  bool Physics_SphereCast_m59056243 (Il2CppObject * __this /* static, unused */, Ray_t3134616544  ___ray0, float ___radius1, RaycastHit_t4003175726 * ___hitInfo2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics::SphereCast(UnityEngine.Ray,System.Single,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  bool Physics_SphereCast_m2476059968 (Il2CppObject * __this /* static, unused */, Ray_t3134616544  ___ray0, float ___radius1, RaycastHit_t4003175726 * ___hitInfo2, float ___maxDistance3, int32_t ___layerMask4, int32_t ___queryTriggerInteraction5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit[] UnityEngine.Physics::CapsuleCastAll(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,UnityEngine.Vector3,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  RaycastHitU5BU5D_t528650843* Physics_CapsuleCastAll_m389433258 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___point10, Vector3_t4282066566  ___point21, float ___radius2, Vector3_t4282066566  ___direction3, float ___maxDistance4, int32_t ___layermask5, int32_t ___queryTriggerInteraction6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit[] UnityEngine.Physics::CapsuleCastAll(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,UnityEngine.Vector3,System.Single,System.Int32)
extern "C"  RaycastHitU5BU5D_t528650843* Physics_CapsuleCastAll_m2797987113 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___point10, Vector3_t4282066566  ___point21, float ___radius2, Vector3_t4282066566  ___direction3, float ___maxDistance4, int32_t ___layermask5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit[] UnityEngine.Physics::CapsuleCastAll(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,UnityEngine.Vector3,System.Single)
extern "C"  RaycastHitU5BU5D_t528650843* Physics_CapsuleCastAll_m3332643438 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___point10, Vector3_t4282066566  ___point21, float ___radius2, Vector3_t4282066566  ___direction3, float ___maxDistance4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit[] UnityEngine.Physics::CapsuleCastAll(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,UnityEngine.Vector3)
extern "C"  RaycastHitU5BU5D_t528650843* Physics_CapsuleCastAll_m1241088841 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___point10, Vector3_t4282066566  ___point21, float ___radius2, Vector3_t4282066566  ___direction3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit[] UnityEngine.Physics::INTERNAL_CALL_CapsuleCastAll(UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,UnityEngine.Vector3&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  RaycastHitU5BU5D_t528650843* Physics_INTERNAL_CALL_CapsuleCastAll_m2975798891 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566 * ___point10, Vector3_t4282066566 * ___point21, float ___radius2, Vector3_t4282066566 * ___direction3, float ___maxDistance4, int32_t ___layermask5, int32_t ___queryTriggerInteraction6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Physics::CapsuleCastNonAlloc(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,UnityEngine.Vector3,UnityEngine.RaycastHit[],System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  int32_t Physics_CapsuleCastNonAlloc_m3159262288 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___point10, Vector3_t4282066566  ___point21, float ___radius2, Vector3_t4282066566  ___direction3, RaycastHitU5BU5D_t528650843* ___results4, float ___maxDistance5, int32_t ___layermask6, int32_t ___queryTriggerInteraction7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Physics::CapsuleCastNonAlloc(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,UnityEngine.Vector3,UnityEngine.RaycastHit[],System.Single,System.Int32)
extern "C"  int32_t Physics_CapsuleCastNonAlloc_m2169142479 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___point10, Vector3_t4282066566  ___point21, float ___radius2, Vector3_t4282066566  ___direction3, RaycastHitU5BU5D_t528650843* ___results4, float ___maxDistance5, int32_t ___layermask6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Physics::CapsuleCastNonAlloc(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,UnityEngine.Vector3,UnityEngine.RaycastHit[],System.Single)
extern "C"  int32_t Physics_CapsuleCastNonAlloc_m1988365576 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___point10, Vector3_t4282066566  ___point21, float ___radius2, Vector3_t4282066566  ___direction3, RaycastHitU5BU5D_t528650843* ___results4, float ___maxDistance5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Physics::CapsuleCastNonAlloc(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,UnityEngine.Vector3,UnityEngine.RaycastHit[])
extern "C"  int32_t Physics_CapsuleCastNonAlloc_m3753290595 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___point10, Vector3_t4282066566  ___point21, float ___radius2, Vector3_t4282066566  ___direction3, RaycastHitU5BU5D_t528650843* ___results4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Physics::INTERNAL_CALL_CapsuleCastNonAlloc(UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,UnityEngine.Vector3&,UnityEngine.RaycastHit[],System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  int32_t Physics_INTERNAL_CALL_CapsuleCastNonAlloc_m4232874563 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566 * ___point10, Vector3_t4282066566 * ___point21, float ___radius2, Vector3_t4282066566 * ___direction3, RaycastHitU5BU5D_t528650843* ___results4, float ___maxDistance5, int32_t ___layermask6, int32_t ___queryTriggerInteraction7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit[] UnityEngine.Physics::SphereCastAll(UnityEngine.Vector3,System.Single,UnityEngine.Vector3,System.Single,System.Int32)
extern "C"  RaycastHitU5BU5D_t528650843* Physics_SphereCastAll_m1037167634 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___origin0, float ___radius1, Vector3_t4282066566  ___direction2, float ___maxDistance3, int32_t ___layerMask4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit[] UnityEngine.Physics::SphereCastAll(UnityEngine.Vector3,System.Single,UnityEngine.Vector3,System.Single)
extern "C"  RaycastHitU5BU5D_t528650843* Physics_SphereCastAll_m1584008421 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___origin0, float ___radius1, Vector3_t4282066566  ___direction2, float ___maxDistance3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit[] UnityEngine.Physics::SphereCastAll(UnityEngine.Vector3,System.Single,UnityEngine.Vector3)
extern "C"  RaycastHitU5BU5D_t528650843* Physics_SphereCastAll_m2774204928 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___origin0, float ___radius1, Vector3_t4282066566  ___direction2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit[] UnityEngine.Physics::SphereCastAll(UnityEngine.Vector3,System.Single,UnityEngine.Vector3,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  RaycastHitU5BU5D_t528650843* Physics_SphereCastAll_m3942043155 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___origin0, float ___radius1, Vector3_t4282066566  ___direction2, float ___maxDistance3, int32_t ___layerMask4, int32_t ___queryTriggerInteraction5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit[] UnityEngine.Physics::SphereCastAll(UnityEngine.Ray,System.Single,System.Single,System.Int32)
extern "C"  RaycastHitU5BU5D_t528650843* Physics_SphereCastAll_m3267915231 (Il2CppObject * __this /* static, unused */, Ray_t3134616544  ___ray0, float ___radius1, float ___maxDistance2, int32_t ___layerMask3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit[] UnityEngine.Physics::SphereCastAll(UnityEngine.Ray,System.Single,System.Single)
extern "C"  RaycastHitU5BU5D_t528650843* Physics_SphereCastAll_m2682495480 (Il2CppObject * __this /* static, unused */, Ray_t3134616544  ___ray0, float ___radius1, float ___maxDistance2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit[] UnityEngine.Physics::SphereCastAll(UnityEngine.Ray,System.Single)
extern "C"  RaycastHitU5BU5D_t528650843* Physics_SphereCastAll_m2603918419 (Il2CppObject * __this /* static, unused */, Ray_t3134616544  ___ray0, float ___radius1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit[] UnityEngine.Physics::SphereCastAll(UnityEngine.Ray,System.Single,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  RaycastHitU5BU5D_t528650843* Physics_SphereCastAll_m4226100576 (Il2CppObject * __this /* static, unused */, Ray_t3134616544  ___ray0, float ___radius1, float ___maxDistance2, int32_t ___layerMask3, int32_t ___queryTriggerInteraction4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Physics::SphereCastNonAlloc(UnityEngine.Vector3,System.Single,UnityEngine.Vector3,UnityEngine.RaycastHit[],System.Single,System.Int32)
extern "C"  int32_t Physics_SphereCastNonAlloc_m1664790298 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___origin0, float ___radius1, Vector3_t4282066566  ___direction2, RaycastHitU5BU5D_t528650843* ___results3, float ___maxDistance4, int32_t ___layerMask5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Physics::SphereCastNonAlloc(UnityEngine.Vector3,System.Single,UnityEngine.Vector3,UnityEngine.RaycastHit[],System.Single)
extern "C"  int32_t Physics_SphereCastNonAlloc_m2581134557 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___origin0, float ___radius1, Vector3_t4282066566  ___direction2, RaycastHitU5BU5D_t528650843* ___results3, float ___maxDistance4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Physics::SphereCastNonAlloc(UnityEngine.Vector3,System.Single,UnityEngine.Vector3,UnityEngine.RaycastHit[])
extern "C"  int32_t Physics_SphereCastNonAlloc_m903922680 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___origin0, float ___radius1, Vector3_t4282066566  ___direction2, RaycastHitU5BU5D_t528650843* ___results3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Physics::SphereCastNonAlloc(UnityEngine.Vector3,System.Single,UnityEngine.Vector3,UnityEngine.RaycastHit[],System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  int32_t Physics_SphereCastNonAlloc_m4021857563 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___origin0, float ___radius1, Vector3_t4282066566  ___direction2, RaycastHitU5BU5D_t528650843* ___results3, float ___maxDistance4, int32_t ___layerMask5, int32_t ___queryTriggerInteraction6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Physics::SphereCastNonAlloc(UnityEngine.Ray,System.Single,UnityEngine.RaycastHit[],System.Single,System.Int32)
extern "C"  int32_t Physics_SphereCastNonAlloc_m3535720173 (Il2CppObject * __this /* static, unused */, Ray_t3134616544  ___ray0, float ___radius1, RaycastHitU5BU5D_t528650843* ___results2, float ___maxDistance3, int32_t ___layerMask4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Physics::SphereCastNonAlloc(UnityEngine.Ray,System.Single,UnityEngine.RaycastHit[],System.Single)
extern "C"  int32_t Physics_SphereCastNonAlloc_m1159635498 (Il2CppObject * __this /* static, unused */, Ray_t3134616544  ___ray0, float ___radius1, RaycastHitU5BU5D_t528650843* ___results2, float ___maxDistance3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Physics::SphereCastNonAlloc(UnityEngine.Ray,System.Single,UnityEngine.RaycastHit[])
extern "C"  int32_t Physics_SphereCastNonAlloc_m1479672837 (Il2CppObject * __this /* static, unused */, Ray_t3134616544  ___ray0, float ___radius1, RaycastHitU5BU5D_t528650843* ___results2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Physics::SphereCastNonAlloc(UnityEngine.Ray,System.Single,UnityEngine.RaycastHit[],System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  int32_t Physics_SphereCastNonAlloc_m1932304238 (Il2CppObject * __this /* static, unused */, Ray_t3134616544  ___ray0, float ___radius1, RaycastHitU5BU5D_t528650843* ___results2, float ___maxDistance3, int32_t ___layerMask4, int32_t ___queryTriggerInteraction5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics::CheckSphere(UnityEngine.Vector3,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  bool Physics_CheckSphere_m851365189 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___position0, float ___radius1, int32_t ___layerMask2, int32_t ___queryTriggerInteraction3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics::CheckSphere(UnityEngine.Vector3,System.Single,System.Int32)
extern "C"  bool Physics_CheckSphere_m963288132 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___position0, float ___radius1, int32_t ___layerMask2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics::CheckSphere(UnityEngine.Vector3,System.Single)
extern "C"  bool Physics_CheckSphere_m1950151539 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___position0, float ___radius1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics::INTERNAL_CALL_CheckSphere(UnityEngine.Vector3&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  bool Physics_INTERNAL_CALL_CheckSphere_m403414612 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566 * ___position0, float ___radius1, int32_t ___layerMask2, int32_t ___queryTriggerInteraction3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics::CheckCapsule(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  bool Physics_CheckCapsule_m2675767064 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___start0, Vector3_t4282066566  ___end1, float ___radius2, int32_t ___layermask3, int32_t ___queryTriggerInteraction4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics::CheckCapsule(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32)
extern "C"  bool Physics_CheckCapsule_m1287566231 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___start0, Vector3_t4282066566  ___end1, float ___radius2, int32_t ___layermask3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics::CheckCapsule(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern "C"  bool Physics_CheckCapsule_m60191040 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___start0, Vector3_t4282066566  ___end1, float ___radius2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics::INTERNAL_CALL_CheckCapsule(UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  bool Physics_INTERNAL_CALL_CheckCapsule_m2301698141 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566 * ___start0, Vector3_t4282066566 * ___end1, float ___radius2, int32_t ___layermask3, int32_t ___queryTriggerInteraction4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics::CheckBox(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Quaternion,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  bool Physics_CheckBox_m3510496740 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___center0, Vector3_t4282066566  ___halfExtents1, Quaternion_t1553702882  ___orientation2, int32_t ___layermask3, int32_t ___queryTriggerInteraction4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics::CheckBox(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Quaternion,System.Int32)
extern "C"  bool Physics_CheckBox_m2760450147 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___center0, Vector3_t4282066566  ___halfExtents1, Quaternion_t1553702882  ___orientation2, int32_t ___layermask3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics::CheckBox(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C"  bool Physics_CheckBox_m3151964404 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___center0, Vector3_t4282066566  ___halfExtents1, Quaternion_t1553702882  ___orientation2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics::CheckBox(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  bool Physics_CheckBox_m3387885117 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___center0, Vector3_t4282066566  ___halfExtents1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics::INTERNAL_CALL_CheckBox(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Quaternion&,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  bool Physics_INTERNAL_CALL_CheckBox_m3915629829 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566 * ___center0, Vector3_t4282066566 * ___halfExtents1, Quaternion_t1553702882 * ___orientation2, int32_t ___layermask3, int32_t ___queryTriggerInteraction4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider[] UnityEngine.Physics::OverlapBox(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Quaternion,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  ColliderU5BU5D_t2697150633* Physics_OverlapBox_m3065126271 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___center0, Vector3_t4282066566  ___halfExtents1, Quaternion_t1553702882  ___orientation2, int32_t ___layerMask3, int32_t ___queryTriggerInteraction4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider[] UnityEngine.Physics::OverlapBox(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Quaternion,System.Int32)
extern "C"  ColliderU5BU5D_t2697150633* Physics_OverlapBox_m3936780670 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___center0, Vector3_t4282066566  ___halfExtents1, Quaternion_t1553702882  ___orientation2, int32_t ___layerMask3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider[] UnityEngine.Physics::OverlapBox(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C"  ColliderU5BU5D_t2697150633* Physics_OverlapBox_m3979548665 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___center0, Vector3_t4282066566  ___halfExtents1, Quaternion_t1553702882  ___orientation2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider[] UnityEngine.Physics::OverlapBox(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  ColliderU5BU5D_t2697150633* Physics_OverlapBox_m3023067352 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___center0, Vector3_t4282066566  ___halfExtents1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider[] UnityEngine.Physics::INTERNAL_CALL_OverlapBox(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Quaternion&,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  ColliderU5BU5D_t2697150633* Physics_INTERNAL_CALL_OverlapBox_m970877130 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566 * ___center0, Vector3_t4282066566 * ___halfExtents1, Quaternion_t1553702882 * ___orientation2, int32_t ___layerMask3, int32_t ___queryTriggerInteraction4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Physics::OverlapBoxNonAlloc(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Collider[],UnityEngine.Quaternion,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  int32_t Physics_OverlapBoxNonAlloc_m2078322354 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___center0, Vector3_t4282066566  ___halfExtents1, ColliderU5BU5D_t2697150633* ___results2, Quaternion_t1553702882  ___orientation3, int32_t ___layerMask4, int32_t ___queryTriggerInteraction5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Physics::OverlapBoxNonAlloc(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Collider[],UnityEngine.Quaternion,System.Int32)
extern "C"  int32_t Physics_OverlapBoxNonAlloc_m3652923441 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___center0, Vector3_t4282066566  ___halfExtents1, ColliderU5BU5D_t2697150633* ___results2, Quaternion_t1553702882  ___orientation3, int32_t ___layerMask4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Physics::OverlapBoxNonAlloc(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Collider[],UnityEngine.Quaternion)
extern "C"  int32_t Physics_OverlapBoxNonAlloc_m1120502374 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___center0, Vector3_t4282066566  ___halfExtents1, ColliderU5BU5D_t2697150633* ___results2, Quaternion_t1553702882  ___orientation3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Physics::OverlapBoxNonAlloc(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Collider[])
extern "C"  int32_t Physics_OverlapBoxNonAlloc_m1386997003 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___center0, Vector3_t4282066566  ___halfExtents1, ColliderU5BU5D_t2697150633* ___results2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Physics::INTERNAL_CALL_OverlapBoxNonAlloc(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Collider[],UnityEngine.Quaternion&,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  int32_t Physics_INTERNAL_CALL_OverlapBoxNonAlloc_m739016525 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566 * ___center0, Vector3_t4282066566 * ___halfExtents1, ColliderU5BU5D_t2697150633* ___results2, Quaternion_t1553702882 * ___orientation3, int32_t ___layerMask4, int32_t ___queryTriggerInteraction5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit[] UnityEngine.Physics::BoxCastAll(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Quaternion,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  RaycastHitU5BU5D_t528650843* Physics_BoxCastAll_m92199268 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___center0, Vector3_t4282066566  ___halfExtents1, Vector3_t4282066566  ___direction2, Quaternion_t1553702882  ___orientation3, float ___maxDistance4, int32_t ___layermask5, int32_t ___queryTriggerInteraction6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit[] UnityEngine.Physics::BoxCastAll(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Quaternion,System.Single,System.Int32)
extern "C"  RaycastHitU5BU5D_t528650843* Physics_BoxCastAll_m2996423651 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___center0, Vector3_t4282066566  ___halfExtents1, Vector3_t4282066566  ___direction2, Quaternion_t1553702882  ___orientation3, float ___maxDistance4, int32_t ___layermask5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit[] UnityEngine.Physics::BoxCastAll(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Quaternion,System.Single)
extern "C"  RaycastHitU5BU5D_t528650843* Physics_BoxCastAll_m2526924148 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___center0, Vector3_t4282066566  ___halfExtents1, Vector3_t4282066566  ___direction2, Quaternion_t1553702882  ___orientation3, float ___maxDistance4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit[] UnityEngine.Physics::BoxCastAll(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C"  RaycastHitU5BU5D_t528650843* Physics_BoxCastAll_m3820116175 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___center0, Vector3_t4282066566  ___halfExtents1, Vector3_t4282066566  ___direction2, Quaternion_t1553702882  ___orientation3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit[] UnityEngine.Physics::BoxCastAll(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  RaycastHitU5BU5D_t528650843* Physics_BoxCastAll_m2057818306 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___center0, Vector3_t4282066566  ___halfExtents1, Vector3_t4282066566  ___direction2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit[] UnityEngine.Physics::INTERNAL_CALL_BoxCastAll(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Quaternion&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  RaycastHitU5BU5D_t528650843* Physics_INTERNAL_CALL_BoxCastAll_m3003580329 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566 * ___center0, Vector3_t4282066566 * ___halfExtents1, Vector3_t4282066566 * ___direction2, Quaternion_t1553702882 * ___orientation3, float ___maxDistance4, int32_t ___layermask5, int32_t ___queryTriggerInteraction6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Physics::BoxCastNonAlloc(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit[],UnityEngine.Quaternion,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  int32_t Physics_BoxCastNonAlloc_m74520936 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___center0, Vector3_t4282066566  ___halfExtents1, Vector3_t4282066566  ___direction2, RaycastHitU5BU5D_t528650843* ___results3, Quaternion_t1553702882  ___orientation4, float ___maxDistance5, int32_t ___layermask6, int32_t ___queryTriggerInteraction7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Physics::BoxCastNonAlloc(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit[],UnityEngine.Quaternion,System.Single,System.Int32)
extern "C"  int32_t Physics_BoxCastNonAlloc_m2994585063 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___center0, Vector3_t4282066566  ___halfExtents1, Vector3_t4282066566  ___direction2, RaycastHitU5BU5D_t528650843* ___results3, Quaternion_t1553702882  ___orientation4, float ___maxDistance5, int32_t ___layermask6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Physics::BoxCastNonAlloc(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit[],UnityEngine.Quaternion,System.Single)
extern "C"  int32_t Physics_BoxCastNonAlloc_m1856300272 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___center0, Vector3_t4282066566  ___halfExtents1, Vector3_t4282066566  ___direction2, RaycastHitU5BU5D_t528650843* ___results3, Quaternion_t1553702882  ___orientation4, float ___maxDistance5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Physics::BoxCastNonAlloc(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit[],UnityEngine.Quaternion)
extern "C"  int32_t Physics_BoxCastNonAlloc_m2806643019 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___center0, Vector3_t4282066566  ___halfExtents1, Vector3_t4282066566  ___direction2, RaycastHitU5BU5D_t528650843* ___results3, Quaternion_t1553702882  ___orientation4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Physics::BoxCastNonAlloc(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit[])
extern "C"  int32_t Physics_BoxCastNonAlloc_m3414428614 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___center0, Vector3_t4282066566  ___halfExtents1, Vector3_t4282066566  ___direction2, RaycastHitU5BU5D_t528650843* ___results3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Physics::INTERNAL_CALL_BoxCastNonAlloc(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.RaycastHit[],UnityEngine.Quaternion&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  int32_t Physics_INTERNAL_CALL_BoxCastNonAlloc_m2254187 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566 * ___center0, Vector3_t4282066566 * ___halfExtents1, Vector3_t4282066566 * ___direction2, RaycastHitU5BU5D_t528650843* ___results3, Quaternion_t1553702882 * ___orientation4, float ___maxDistance5, int32_t ___layermask6, int32_t ___queryTriggerInteraction7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics::Internal_BoxCast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  bool Physics_Internal_BoxCast_m824011744 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___center0, Vector3_t4282066566  ___halfExtents1, Quaternion_t1553702882  ___orientation2, Vector3_t4282066566  ___direction3, RaycastHit_t4003175726 * ___hitInfo4, float ___maxDistance5, int32_t ___layermask6, int32_t ___queryTriggerInteraction7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics::INTERNAL_CALL_Internal_BoxCast(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Quaternion&,UnityEngine.Vector3&,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  bool Physics_INTERNAL_CALL_Internal_BoxCast_m2643512385 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566 * ___center0, Vector3_t4282066566 * ___halfExtents1, Quaternion_t1553702882 * ___orientation2, Vector3_t4282066566 * ___direction3, RaycastHit_t4003175726 * ___hitInfo4, float ___maxDistance5, int32_t ___layermask6, int32_t ___queryTriggerInteraction7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics::BoxCast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Quaternion,System.Single,System.Int32)
extern "C"  bool Physics_BoxCast_m2873646464 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___center0, Vector3_t4282066566  ___halfExtents1, Vector3_t4282066566  ___direction2, Quaternion_t1553702882  ___orientation3, float ___maxDistance4, int32_t ___layerMask5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics::BoxCast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Quaternion,System.Single)
extern "C"  bool Physics_BoxCast_m1769070775 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___center0, Vector3_t4282066566  ___halfExtents1, Vector3_t4282066566  ___direction2, Quaternion_t1553702882  ___orientation3, float ___maxDistance4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics::BoxCast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C"  bool Physics_BoxCast_m2831813970 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___center0, Vector3_t4282066566  ___halfExtents1, Vector3_t4282066566  ___direction2, Quaternion_t1553702882  ___orientation3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics::BoxCast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  bool Physics_BoxCast_m861549471 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___center0, Vector3_t4282066566  ___halfExtents1, Vector3_t4282066566  ___direction2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics::BoxCast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Quaternion,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  bool Physics_BoxCast_m3689259137 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___center0, Vector3_t4282066566  ___halfExtents1, Vector3_t4282066566  ___direction2, Quaternion_t1553702882  ___orientation3, float ___maxDistance4, int32_t ___layerMask5, int32_t ___queryTriggerInteraction6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics::BoxCast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,UnityEngine.Quaternion,System.Single,System.Int32)
extern "C"  bool Physics_BoxCast_m90036519 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___center0, Vector3_t4282066566  ___halfExtents1, Vector3_t4282066566  ___direction2, RaycastHit_t4003175726 * ___hitInfo3, Quaternion_t1553702882  ___orientation4, float ___maxDistance5, int32_t ___layerMask6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics::BoxCast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,UnityEngine.Quaternion,System.Single)
extern "C"  bool Physics_BoxCast_m3782836656 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___center0, Vector3_t4282066566  ___halfExtents1, Vector3_t4282066566  ___direction2, RaycastHit_t4003175726 * ___hitInfo3, Quaternion_t1553702882  ___orientation4, float ___maxDistance5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics::BoxCast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,UnityEngine.Quaternion)
extern "C"  bool Physics_BoxCast_m3780896267 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___center0, Vector3_t4282066566  ___halfExtents1, Vector3_t4282066566  ___direction2, RaycastHit_t4003175726 * ___hitInfo3, Quaternion_t1553702882  ___orientation4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics::BoxCast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&)
extern "C"  bool Physics_BoxCast_m2188211974 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___center0, Vector3_t4282066566  ___halfExtents1, Vector3_t4282066566  ___direction2, RaycastHit_t4003175726 * ___hitInfo3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics::BoxCast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,UnityEngine.Quaternion,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  bool Physics_BoxCast_m76846248 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___center0, Vector3_t4282066566  ___halfExtents1, Vector3_t4282066566  ___direction2, RaycastHit_t4003175726 * ___hitInfo3, Quaternion_t1553702882  ___orientation4, float ___maxDistance5, int32_t ___layerMask6, int32_t ___queryTriggerInteraction7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Physics::IgnoreCollision(UnityEngine.Collider,UnityEngine.Collider,System.Boolean)
extern "C"  void Physics_IgnoreCollision_m944332263 (Il2CppObject * __this /* static, unused */, Collider_t2939674232 * ___collider10, Collider_t2939674232 * ___collider21, bool ___ignore2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Physics::IgnoreCollision(UnityEngine.Collider,UnityEngine.Collider)
extern "C"  void Physics_IgnoreCollision_m2033495766 (Il2CppObject * __this /* static, unused */, Collider_t2939674232 * ___collider10, Collider_t2939674232 * ___collider21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Physics::IgnoreLayerCollision(System.Int32,System.Int32,System.Boolean)
extern "C"  void Physics_IgnoreLayerCollision_m149878332 (Il2CppObject * __this /* static, unused */, int32_t ___layer10, int32_t ___layer21, bool ___ignore2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Physics::IgnoreLayerCollision(System.Int32,System.Int32)
extern "C"  void Physics_IgnoreLayerCollision_m1800279841 (Il2CppObject * __this /* static, unused */, int32_t ___layer10, int32_t ___layer21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics::GetIgnoreLayerCollision(System.Int32,System.Int32)
extern "C"  bool Physics_GetIgnoreLayerCollision_m1628027591 (Il2CppObject * __this /* static, unused */, int32_t ___layer10, int32_t ___layer21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics::Internal_Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  bool Physics_Internal_Raycast_m3365413907 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___origin0, Vector3_t4282066566  ___direction1, RaycastHit_t4003175726 * ___hitInfo2, float ___maxDistance3, int32_t ___layermask4, int32_t ___queryTriggerInteraction5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics::INTERNAL_CALL_Internal_Raycast(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  bool Physics_INTERNAL_CALL_Internal_Raycast_m1291554392 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566 * ___origin0, Vector3_t4282066566 * ___direction1, RaycastHit_t4003175726 * ___hitInfo2, float ___maxDistance3, int32_t ___layermask4, int32_t ___queryTriggerInteraction5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics::Internal_CapsuleCast(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  bool Physics_Internal_CapsuleCast_m677019326 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___point10, Vector3_t4282066566  ___point21, float ___radius2, Vector3_t4282066566  ___direction3, RaycastHit_t4003175726 * ___hitInfo4, float ___maxDistance5, int32_t ___layermask6, int32_t ___queryTriggerInteraction7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics::INTERNAL_CALL_Internal_CapsuleCast(UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,UnityEngine.Vector3&,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  bool Physics_INTERNAL_CALL_Internal_CapsuleCast_m1067850665 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566 * ___point10, Vector3_t4282066566 * ___point21, float ___radius2, Vector3_t4282066566 * ___direction3, RaycastHit_t4003175726 * ___hitInfo4, float ___maxDistance5, int32_t ___layermask6, int32_t ___queryTriggerInteraction7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics::Internal_RaycastTest(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  bool Physics_Internal_RaycastTest_m4007079948 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___origin0, Vector3_t4282066566  ___direction1, float ___maxDistance2, int32_t ___layermask3, int32_t ___queryTriggerInteraction4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics::INTERNAL_CALL_Internal_RaycastTest(UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  bool Physics_INTERNAL_CALL_Internal_RaycastTest_m2935176529 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566 * ___origin0, Vector3_t4282066566 * ___direction1, float ___maxDistance2, int32_t ___layermask3, int32_t ___queryTriggerInteraction4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
