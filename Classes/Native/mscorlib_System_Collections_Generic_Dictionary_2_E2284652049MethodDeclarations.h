﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Object>
struct Dictionary_2_t967328657;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2284652049.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_866109363.h"
#include "AssemblyU2DCSharp_Pathfinding_Int21974045593.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.Int2,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m2839623054_gshared (Enumerator_t2284652049 * __this, Dictionary_2_t967328657 * ___dictionary0, const MethodInfo* method);
#define Enumerator__ctor_m2839623054(__this, ___dictionary0, method) ((  void (*) (Enumerator_t2284652049 *, Dictionary_2_t967328657 *, const MethodInfo*))Enumerator__ctor_m2839623054_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.Int2,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m4140471197_gshared (Enumerator_t2284652049 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m4140471197(__this, method) ((  Il2CppObject * (*) (Enumerator_t2284652049 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m4140471197_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.Int2,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3207216743_gshared (Enumerator_t2284652049 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m3207216743(__this, method) ((  void (*) (Enumerator_t2284652049 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3207216743_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.Int2,System.Object>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t1751606614  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1172003166_gshared (Enumerator_t2284652049 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1172003166(__this, method) ((  DictionaryEntry_t1751606614  (*) (Enumerator_t2284652049 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1172003166_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.Int2,System.Object>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2853299897_gshared (Enumerator_t2284652049 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2853299897(__this, method) ((  Il2CppObject * (*) (Enumerator_t2284652049 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2853299897_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.Int2,System.Object>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2880502539_gshared (Enumerator_t2284652049 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2880502539(__this, method) ((  Il2CppObject * (*) (Enumerator_t2284652049 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2880502539_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.Int2,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m14324631_gshared (Enumerator_t2284652049 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m14324631(__this, method) ((  bool (*) (Enumerator_t2284652049 *, const MethodInfo*))Enumerator_MoveNext_m14324631_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.Int2,System.Object>::get_Current()
extern "C"  KeyValuePair_2_t866109363  Enumerator_get_Current_m2428217029_gshared (Enumerator_t2284652049 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m2428217029(__this, method) ((  KeyValuePair_2_t866109363  (*) (Enumerator_t2284652049 *, const MethodInfo*))Enumerator_get_Current_m2428217029_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.Int2,System.Object>::get_CurrentKey()
extern "C"  Int2_t1974045593  Enumerator_get_CurrentKey_m1838538208_gshared (Enumerator_t2284652049 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m1838538208(__this, method) ((  Int2_t1974045593  (*) (Enumerator_t2284652049 *, const MethodInfo*))Enumerator_get_CurrentKey_m1838538208_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.Int2,System.Object>::get_CurrentValue()
extern "C"  Il2CppObject * Enumerator_get_CurrentValue_m231599392_gshared (Enumerator_t2284652049 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m231599392(__this, method) ((  Il2CppObject * (*) (Enumerator_t2284652049 *, const MethodInfo*))Enumerator_get_CurrentValue_m231599392_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.Int2,System.Object>::Reset()
extern "C"  void Enumerator_Reset_m1210538720_gshared (Enumerator_t2284652049 * __this, const MethodInfo* method);
#define Enumerator_Reset_m1210538720(__this, method) ((  void (*) (Enumerator_t2284652049 *, const MethodInfo*))Enumerator_Reset_m1210538720_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.Int2,System.Object>::VerifyState()
extern "C"  void Enumerator_VerifyState_m3141731049_gshared (Enumerator_t2284652049 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m3141731049(__this, method) ((  void (*) (Enumerator_t2284652049 *, const MethodInfo*))Enumerator_VerifyState_m3141731049_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.Int2,System.Object>::VerifyCurrent()
extern "C"  void Enumerator_VerifyCurrent_m2586400785_gshared (Enumerator_t2284652049 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m2586400785(__this, method) ((  void (*) (Enumerator_t2284652049 *, const MethodInfo*))Enumerator_VerifyCurrent_m2586400785_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.Int2,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m1915001776_gshared (Enumerator_t2284652049 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1915001776(__this, method) ((  void (*) (Enumerator_t2284652049 *, const MethodInfo*))Enumerator_Dispose_m1915001776_gshared)(__this, method)
