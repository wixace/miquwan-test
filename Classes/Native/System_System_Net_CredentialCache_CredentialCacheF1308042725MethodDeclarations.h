﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.CredentialCache/CredentialCacheForHostKey
struct CredentialCacheForHostKey_t1308042725;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"

// System.String System.Net.CredentialCache/CredentialCacheForHostKey::get_Host()
extern "C"  String_t* CredentialCacheForHostKey_get_Host_m3132187553 (CredentialCacheForHostKey_t1308042725 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Net.CredentialCache/CredentialCacheForHostKey::get_Port()
extern "C"  int32_t CredentialCacheForHostKey_get_Port_m3942163975 (CredentialCacheForHostKey_t1308042725 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Net.CredentialCache/CredentialCacheForHostKey::get_AuthType()
extern "C"  String_t* CredentialCacheForHostKey_get_AuthType_m2122616955 (CredentialCacheForHostKey_t1308042725 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Net.CredentialCache/CredentialCacheForHostKey::GetHashCode()
extern "C"  int32_t CredentialCacheForHostKey_GetHashCode_m838856982 (CredentialCacheForHostKey_t1308042725 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.CredentialCache/CredentialCacheForHostKey::Equals(System.Object)
extern "C"  bool CredentialCacheForHostKey_Equals_m175862578 (CredentialCacheForHostKey_t1308042725 * __this, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Net.CredentialCache/CredentialCacheForHostKey::ToString()
extern "C"  String_t* CredentialCacheForHostKey_ToString_m2652565084 (CredentialCacheForHostKey_t1308042725 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
