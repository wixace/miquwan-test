﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// Mihua.SDK.PayInfo
struct PayInfo_t1775308120;
// System.Action
struct Action_t3771233898;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PluginAiLe
struct  PluginAiLe_t1605867956  : public MonoBehaviour_t667441552
{
public:
	// System.String PluginAiLe::token
	String_t* ___token_2;
	// System.String PluginAiLe::userid
	String_t* ___userid_3;
	// System.Boolean PluginAiLe::isOut
	bool ___isOut_4;
	// System.String PluginAiLe::configId
	String_t* ___configId_5;
	// Mihua.SDK.PayInfo PluginAiLe::iapInfo
	PayInfo_t1775308120 * ___iapInfo_6;

public:
	inline static int32_t get_offset_of_token_2() { return static_cast<int32_t>(offsetof(PluginAiLe_t1605867956, ___token_2)); }
	inline String_t* get_token_2() const { return ___token_2; }
	inline String_t** get_address_of_token_2() { return &___token_2; }
	inline void set_token_2(String_t* value)
	{
		___token_2 = value;
		Il2CppCodeGenWriteBarrier(&___token_2, value);
	}

	inline static int32_t get_offset_of_userid_3() { return static_cast<int32_t>(offsetof(PluginAiLe_t1605867956, ___userid_3)); }
	inline String_t* get_userid_3() const { return ___userid_3; }
	inline String_t** get_address_of_userid_3() { return &___userid_3; }
	inline void set_userid_3(String_t* value)
	{
		___userid_3 = value;
		Il2CppCodeGenWriteBarrier(&___userid_3, value);
	}

	inline static int32_t get_offset_of_isOut_4() { return static_cast<int32_t>(offsetof(PluginAiLe_t1605867956, ___isOut_4)); }
	inline bool get_isOut_4() const { return ___isOut_4; }
	inline bool* get_address_of_isOut_4() { return &___isOut_4; }
	inline void set_isOut_4(bool value)
	{
		___isOut_4 = value;
	}

	inline static int32_t get_offset_of_configId_5() { return static_cast<int32_t>(offsetof(PluginAiLe_t1605867956, ___configId_5)); }
	inline String_t* get_configId_5() const { return ___configId_5; }
	inline String_t** get_address_of_configId_5() { return &___configId_5; }
	inline void set_configId_5(String_t* value)
	{
		___configId_5 = value;
		Il2CppCodeGenWriteBarrier(&___configId_5, value);
	}

	inline static int32_t get_offset_of_iapInfo_6() { return static_cast<int32_t>(offsetof(PluginAiLe_t1605867956, ___iapInfo_6)); }
	inline PayInfo_t1775308120 * get_iapInfo_6() const { return ___iapInfo_6; }
	inline PayInfo_t1775308120 ** get_address_of_iapInfo_6() { return &___iapInfo_6; }
	inline void set_iapInfo_6(PayInfo_t1775308120 * value)
	{
		___iapInfo_6 = value;
		Il2CppCodeGenWriteBarrier(&___iapInfo_6, value);
	}
};

struct PluginAiLe_t1605867956_StaticFields
{
public:
	// System.Action PluginAiLe::<>f__am$cache5
	Action_t3771233898 * ___U3CU3Ef__amU24cache5_7;
	// System.Action PluginAiLe::<>f__am$cache6
	Action_t3771233898 * ___U3CU3Ef__amU24cache6_8;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache5_7() { return static_cast<int32_t>(offsetof(PluginAiLe_t1605867956_StaticFields, ___U3CU3Ef__amU24cache5_7)); }
	inline Action_t3771233898 * get_U3CU3Ef__amU24cache5_7() const { return ___U3CU3Ef__amU24cache5_7; }
	inline Action_t3771233898 ** get_address_of_U3CU3Ef__amU24cache5_7() { return &___U3CU3Ef__amU24cache5_7; }
	inline void set_U3CU3Ef__amU24cache5_7(Action_t3771233898 * value)
	{
		___U3CU3Ef__amU24cache5_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache5_7, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache6_8() { return static_cast<int32_t>(offsetof(PluginAiLe_t1605867956_StaticFields, ___U3CU3Ef__amU24cache6_8)); }
	inline Action_t3771233898 * get_U3CU3Ef__amU24cache6_8() const { return ___U3CU3Ef__amU24cache6_8; }
	inline Action_t3771233898 ** get_address_of_U3CU3Ef__amU24cache6_8() { return &___U3CU3Ef__amU24cache6_8; }
	inline void set_U3CU3Ef__amU24cache6_8(Action_t3771233898 * value)
	{
		___U3CU3Ef__amU24cache6_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache6_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
