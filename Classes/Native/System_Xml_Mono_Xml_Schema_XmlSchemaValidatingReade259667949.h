﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Xml.XmlReaderSettings
struct XmlReaderSettings_t4229224207;
// Mono.Xml.Schema.XmlSchemaValidatingReader
struct XmlSchemaValidatingReader_t2564576372;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XmlSchemaValidatingReader/<XmlSchemaValidatingReader>c__AnonStorey4
struct  U3CXmlSchemaValidatingReaderU3Ec__AnonStorey4_t259667949  : public Il2CppObject
{
public:
	// System.Xml.XmlReaderSettings Mono.Xml.Schema.XmlSchemaValidatingReader/<XmlSchemaValidatingReader>c__AnonStorey4::settings
	XmlReaderSettings_t4229224207 * ___settings_0;
	// Mono.Xml.Schema.XmlSchemaValidatingReader Mono.Xml.Schema.XmlSchemaValidatingReader/<XmlSchemaValidatingReader>c__AnonStorey4::<>f__this
	XmlSchemaValidatingReader_t2564576372 * ___U3CU3Ef__this_1;

public:
	inline static int32_t get_offset_of_settings_0() { return static_cast<int32_t>(offsetof(U3CXmlSchemaValidatingReaderU3Ec__AnonStorey4_t259667949, ___settings_0)); }
	inline XmlReaderSettings_t4229224207 * get_settings_0() const { return ___settings_0; }
	inline XmlReaderSettings_t4229224207 ** get_address_of_settings_0() { return &___settings_0; }
	inline void set_settings_0(XmlReaderSettings_t4229224207 * value)
	{
		___settings_0 = value;
		Il2CppCodeGenWriteBarrier(&___settings_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_1() { return static_cast<int32_t>(offsetof(U3CXmlSchemaValidatingReaderU3Ec__AnonStorey4_t259667949, ___U3CU3Ef__this_1)); }
	inline XmlSchemaValidatingReader_t2564576372 * get_U3CU3Ef__this_1() const { return ___U3CU3Ef__this_1; }
	inline XmlSchemaValidatingReader_t2564576372 ** get_address_of_U3CU3Ef__this_1() { return &___U3CU3Ef__this_1; }
	inline void set_U3CU3Ef__this_1(XmlSchemaValidatingReader_t2564576372 * value)
	{
		___U3CU3Ef__this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
