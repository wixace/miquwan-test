﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Flare
struct Flare_t4197217604;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.Flare::.ctor()
extern "C"  void Flare__ctor_m3576453067 (Flare_t4197217604 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
