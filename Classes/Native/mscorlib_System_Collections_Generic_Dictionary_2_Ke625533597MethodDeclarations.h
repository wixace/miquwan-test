﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<AnimationRunner/AniType,System.Object>
struct Dictionary_2_t10597543;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Ke625533597.h"
#include "AssemblyU2DCSharp_AnimationRunner_AniType1006238651.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<AnimationRunner/AniType,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m422223888_gshared (Enumerator_t625533597 * __this, Dictionary_2_t10597543 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m422223888(__this, ___host0, method) ((  void (*) (Enumerator_t625533597 *, Dictionary_2_t10597543 *, const MethodInfo*))Enumerator__ctor_m422223888_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<AnimationRunner/AniType,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m226304913_gshared (Enumerator_t625533597 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m226304913(__this, method) ((  Il2CppObject * (*) (Enumerator_t625533597 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m226304913_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<AnimationRunner/AniType,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2100364965_gshared (Enumerator_t625533597 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m2100364965(__this, method) ((  void (*) (Enumerator_t625533597 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2100364965_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<AnimationRunner/AniType,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m3576793778_gshared (Enumerator_t625533597 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m3576793778(__this, method) ((  void (*) (Enumerator_t625533597 *, const MethodInfo*))Enumerator_Dispose_m3576793778_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<AnimationRunner/AniType,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3626789649_gshared (Enumerator_t625533597 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m3626789649(__this, method) ((  bool (*) (Enumerator_t625533597 *, const MethodInfo*))Enumerator_MoveNext_m3626789649_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<AnimationRunner/AniType,System.Object>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m3208405283_gshared (Enumerator_t625533597 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m3208405283(__this, method) ((  int32_t (*) (Enumerator_t625533597 *, const MethodInfo*))Enumerator_get_Current_m3208405283_gshared)(__this, method)
