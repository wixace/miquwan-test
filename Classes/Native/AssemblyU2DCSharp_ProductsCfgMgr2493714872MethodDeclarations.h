﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ProductsCfgMgr
struct ProductsCfgMgr_t2493714872;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// System.Object
struct Il2CppObject;
// System.Text.Encoding
struct Encoding_t2012439129;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Text_Encoding2012439129.h"

// System.Void ProductsCfgMgr::.ctor()
extern "C"  void ProductsCfgMgr__ctor_m3458148835 (ProductsCfgMgr_t2493714872 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProductsCfgMgr::OnLoginServer(CEvent.ZEvent)
extern "C"  void ProductsCfgMgr_OnLoginServer_m1265794765 (ProductsCfgMgr_t2493714872 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProductsCfgMgr::Init(System.String)
extern "C"  void ProductsCfgMgr_Init_m2977490577 (ProductsCfgMgr_t2493714872 * __this, String_t* ___cfgInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ProductsCfgMgr::GetProductID(System.String)
extern "C"  String_t* ProductsCfgMgr_GetProductID_m949426570 (ProductsCfgMgr_t2493714872 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProductsCfgMgr::<OnLoginServer>m__3E2(System.String,System.Byte[])
extern "C"  void ProductsCfgMgr_U3COnLoginServerU3Em__3E2_m3208111652 (ProductsCfgMgr_t2493714872 * __this, String_t* ___name0, ByteU5BU5D_t4260760469* ___abBytes1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProductsCfgMgr::ilo_LogError1(System.Object,System.Boolean)
extern "C"  void ProductsCfgMgr_ilo_LogError1_m3974692900 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ProductsCfgMgr::ilo_BytesToString2(System.Byte[],System.Text.Encoding)
extern "C"  String_t* ProductsCfgMgr_ilo_BytesToString2_m1105230490 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___bytes0, Encoding_t2012439129 * ___encoding1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
