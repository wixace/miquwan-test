﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V2868572062MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.UInt32,CSSkillData>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define ValueCollection__ctor_m1612223777(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t3425011510 *, Dictionary_2_t429438501 *, const MethodInfo*))ValueCollection__ctor_m587328820_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.UInt32,CSSkillData>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m752746225(__this, ___item0, method) ((  void (*) (ValueCollection_t3425011510 *, CSSkillData_t432288523 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1412421758_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.UInt32,CSSkillData>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m4240641082(__this, method) ((  void (*) (ValueCollection_t3425011510 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m2867967047_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.UInt32,CSSkillData>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1821160277(__this, ___item0, method) ((  bool (*) (ValueCollection_t3425011510 *, CSSkillData_t432288523 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1938379368_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.UInt32,CSSkillData>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m3761544506(__this, ___item0, method) ((  bool (*) (ValueCollection_t3425011510 *, CSSkillData_t432288523 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m2514740493_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.UInt32,CSSkillData>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m2283032840(__this, method) ((  Il2CppObject* (*) (ValueCollection_t3425011510 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1129618005_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.UInt32,CSSkillData>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m3849006270(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t3425011510 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m3607423115_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<System.UInt32,CSSkillData>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1065496505(__this, method) ((  Il2CppObject * (*) (ValueCollection_t3425011510 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m2307639110_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.UInt32,CSSkillData>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m96336136(__this, method) ((  bool (*) (ValueCollection_t3425011510 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m213555227_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.UInt32,CSSkillData>::System.Collections.ICollection.get_IsSynchronized()
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m954960616(__this, method) ((  bool (*) (ValueCollection_t3425011510 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m422047355_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<System.UInt32,CSSkillData>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m878543764(__this, method) ((  Il2CppObject * (*) (ValueCollection_t3425011510 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m1579731495_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.UInt32,CSSkillData>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m3350878504(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t3425011510 *, CSSkillDataU5BU5D_t1071816810*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m2183660667_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.UInt32,CSSkillData>::GetEnumerator()
#define ValueCollection_GetEnumerator_m1781306187(__this, method) ((  Enumerator_t2656239205  (*) (ValueCollection_t3425011510 *, const MethodInfo*))ValueCollection_GetEnumerator_m2490352798_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<System.UInt32,CSSkillData>::get_Count()
#define ValueCollection_get_Count_m2853443758(__this, method) ((  int32_t (*) (ValueCollection_t3425011510 *, const MethodInfo*))ValueCollection_get_Count_m1502902721_gshared)(__this, method)
