﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FS_AddSome
struct FS_AddSome_t2308363171;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;

#include "codegen/il2cpp-codegen.h"

// System.Void FS_AddSome::.ctor()
extern "C"  void FS_AddSome__ctor_m2381655128 (FS_AddSome_t2308363171 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FS_AddSome::Start()
extern "C"  void FS_AddSome_Start_m1328792920 (FS_AddSome_t2308363171 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator FS_AddSome::MakeSomeNewOnes()
extern "C"  Il2CppObject * FS_AddSome_MakeSomeNewOnes_m142115161 (FS_AddSome_t2308363171 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
