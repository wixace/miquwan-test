﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1797400937.h"
#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DCSharp_Pathfinding_IntRect3015058261.h"

// System.Void System.Array/InternalEnumerator`1<Pathfinding.IntRect>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m745607502_gshared (InternalEnumerator_1_t1797400937 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m745607502(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t1797400937 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m745607502_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<Pathfinding.IntRect>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1925764498_gshared (InternalEnumerator_1_t1797400937 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1925764498(__this, method) ((  void (*) (InternalEnumerator_1_t1797400937 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1925764498_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<Pathfinding.IntRect>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3508973576_gshared (InternalEnumerator_1_t1797400937 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3508973576(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t1797400937 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3508973576_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Pathfinding.IntRect>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2074559653_gshared (InternalEnumerator_1_t1797400937 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m2074559653(__this, method) ((  void (*) (InternalEnumerator_1_t1797400937 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2074559653_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Pathfinding.IntRect>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2813853762_gshared (InternalEnumerator_1_t1797400937 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m2813853762(__this, method) ((  bool (*) (InternalEnumerator_1_t1797400937 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m2813853762_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Pathfinding.IntRect>::get_Current()
extern "C"  IntRect_t3015058261  InternalEnumerator_1_get_Current_m388107767_gshared (InternalEnumerator_1_t1797400937 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m388107767(__this, method) ((  IntRect_t3015058261  (*) (InternalEnumerator_1_t1797400937 *, const MethodInfo*))InternalEnumerator_1_get_Current_m388107767_gshared)(__this, method)
