﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PluginDiYibo
struct  PluginDiYibo_t1432128181  : public MonoBehaviour_t667441552
{
public:
	// System.String PluginDiYibo::user_id
	String_t* ___user_id_2;
	// System.String PluginDiYibo::account
	String_t* ___account_3;
	// System.Boolean PluginDiYibo::isOut
	bool ___isOut_4;
	// System.String PluginDiYibo::userId
	String_t* ___userId_5;
	// System.String PluginDiYibo::sessiond
	String_t* ___sessiond_6;
	// System.String[] PluginDiYibo::sdkInfo
	StringU5BU5D_t4054002952* ___sdkInfo_7;
	// System.Boolean PluginDiYibo::isOut_ios
	bool ___isOut_ios_8;

public:
	inline static int32_t get_offset_of_user_id_2() { return static_cast<int32_t>(offsetof(PluginDiYibo_t1432128181, ___user_id_2)); }
	inline String_t* get_user_id_2() const { return ___user_id_2; }
	inline String_t** get_address_of_user_id_2() { return &___user_id_2; }
	inline void set_user_id_2(String_t* value)
	{
		___user_id_2 = value;
		Il2CppCodeGenWriteBarrier(&___user_id_2, value);
	}

	inline static int32_t get_offset_of_account_3() { return static_cast<int32_t>(offsetof(PluginDiYibo_t1432128181, ___account_3)); }
	inline String_t* get_account_3() const { return ___account_3; }
	inline String_t** get_address_of_account_3() { return &___account_3; }
	inline void set_account_3(String_t* value)
	{
		___account_3 = value;
		Il2CppCodeGenWriteBarrier(&___account_3, value);
	}

	inline static int32_t get_offset_of_isOut_4() { return static_cast<int32_t>(offsetof(PluginDiYibo_t1432128181, ___isOut_4)); }
	inline bool get_isOut_4() const { return ___isOut_4; }
	inline bool* get_address_of_isOut_4() { return &___isOut_4; }
	inline void set_isOut_4(bool value)
	{
		___isOut_4 = value;
	}

	inline static int32_t get_offset_of_userId_5() { return static_cast<int32_t>(offsetof(PluginDiYibo_t1432128181, ___userId_5)); }
	inline String_t* get_userId_5() const { return ___userId_5; }
	inline String_t** get_address_of_userId_5() { return &___userId_5; }
	inline void set_userId_5(String_t* value)
	{
		___userId_5 = value;
		Il2CppCodeGenWriteBarrier(&___userId_5, value);
	}

	inline static int32_t get_offset_of_sessiond_6() { return static_cast<int32_t>(offsetof(PluginDiYibo_t1432128181, ___sessiond_6)); }
	inline String_t* get_sessiond_6() const { return ___sessiond_6; }
	inline String_t** get_address_of_sessiond_6() { return &___sessiond_6; }
	inline void set_sessiond_6(String_t* value)
	{
		___sessiond_6 = value;
		Il2CppCodeGenWriteBarrier(&___sessiond_6, value);
	}

	inline static int32_t get_offset_of_sdkInfo_7() { return static_cast<int32_t>(offsetof(PluginDiYibo_t1432128181, ___sdkInfo_7)); }
	inline StringU5BU5D_t4054002952* get_sdkInfo_7() const { return ___sdkInfo_7; }
	inline StringU5BU5D_t4054002952** get_address_of_sdkInfo_7() { return &___sdkInfo_7; }
	inline void set_sdkInfo_7(StringU5BU5D_t4054002952* value)
	{
		___sdkInfo_7 = value;
		Il2CppCodeGenWriteBarrier(&___sdkInfo_7, value);
	}

	inline static int32_t get_offset_of_isOut_ios_8() { return static_cast<int32_t>(offsetof(PluginDiYibo_t1432128181, ___isOut_ios_8)); }
	inline bool get_isOut_ios_8() const { return ___isOut_ios_8; }
	inline bool* get_address_of_isOut_ios_8() { return &___isOut_ios_8; }
	inline void set_isOut_ios_8(bool value)
	{
		___isOut_ios_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
