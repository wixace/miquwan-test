﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Collections_Generic_EqualityCompar1223252702.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<Newtonsoft.Json.Schema.JsonSchemaType>
struct  DefaultComparer_t563192488  : public EqualityComparer_1_t1223252702
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
