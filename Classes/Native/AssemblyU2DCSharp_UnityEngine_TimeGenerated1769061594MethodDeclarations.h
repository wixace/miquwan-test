﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_TimeGenerated
struct UnityEngine_TimeGenerated_t1769061594;
// JSVCall
struct JSVCall_t3708497963;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"

// System.Void UnityEngine_TimeGenerated::.ctor()
extern "C"  void UnityEngine_TimeGenerated__ctor_m3082837457 (UnityEngine_TimeGenerated_t1769061594 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TimeGenerated::Time_Time1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TimeGenerated_Time_Time1_m1604202785 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TimeGenerated::Time_time(JSVCall)
extern "C"  void UnityEngine_TimeGenerated_Time_time_m3534638265 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TimeGenerated::Time_timeSinceLevelLoad(JSVCall)
extern "C"  void UnityEngine_TimeGenerated_Time_timeSinceLevelLoad_m2234445033 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TimeGenerated::Time_deltaTime(JSVCall)
extern "C"  void UnityEngine_TimeGenerated_Time_deltaTime_m3668865313 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TimeGenerated::Time_fixedTime(JSVCall)
extern "C"  void UnityEngine_TimeGenerated_Time_fixedTime_m2601400037 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TimeGenerated::Time_unscaledTime(JSVCall)
extern "C"  void UnityEngine_TimeGenerated_Time_unscaledTime_m1487744838 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TimeGenerated::Time_unscaledDeltaTime(JSVCall)
extern "C"  void UnityEngine_TimeGenerated_Time_unscaledDeltaTime_m4215807668 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TimeGenerated::Time_fixedDeltaTime(JSVCall)
extern "C"  void UnityEngine_TimeGenerated_Time_fixedDeltaTime_m247857781 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TimeGenerated::Time_maximumDeltaTime(JSVCall)
extern "C"  void UnityEngine_TimeGenerated_Time_maximumDeltaTime_m453486113 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TimeGenerated::Time_smoothDeltaTime(JSVCall)
extern "C"  void UnityEngine_TimeGenerated_Time_smoothDeltaTime_m3370376047 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TimeGenerated::Time_timeScale(JSVCall)
extern "C"  void UnityEngine_TimeGenerated_Time_timeScale_m3510971465 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TimeGenerated::Time_frameCount(JSVCall)
extern "C"  void UnityEngine_TimeGenerated_Time_frameCount_m2513052548 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TimeGenerated::Time_renderedFrameCount(JSVCall)
extern "C"  void UnityEngine_TimeGenerated_Time_renderedFrameCount_m2194136207 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TimeGenerated::Time_realtimeSinceStartup(JSVCall)
extern "C"  void UnityEngine_TimeGenerated_Time_realtimeSinceStartup_m2179762616 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TimeGenerated::Time_captureFramerate(JSVCall)
extern "C"  void UnityEngine_TimeGenerated_Time_captureFramerate_m1928017311 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TimeGenerated::__Register()
extern "C"  void UnityEngine_TimeGenerated___Register_m2637699478 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TimeGenerated::ilo_setSingle1(System.Int32,System.Single)
extern "C"  void UnityEngine_TimeGenerated_ilo_setSingle1_m1342087427 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine_TimeGenerated::ilo_getSingle2(System.Int32)
extern "C"  float UnityEngine_TimeGenerated_ilo_getSingle2_m3668977695 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TimeGenerated::ilo_setInt323(System.Int32,System.Int32)
extern "C"  void UnityEngine_TimeGenerated_ilo_setInt323_m3468690147 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
