﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_TextGenerationSettingsGenerated
struct UnityEngine_TextGenerationSettingsGenerated_t3804395167;
// JSVCall
struct JSVCall_t3708497963;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"

// System.Void UnityEngine_TextGenerationSettingsGenerated::.ctor()
extern "C"  void UnityEngine_TextGenerationSettingsGenerated__ctor_m4188366764 (UnityEngine_TextGenerationSettingsGenerated_t3804395167 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TextGenerationSettingsGenerated::.cctor()
extern "C"  void UnityEngine_TextGenerationSettingsGenerated__cctor_m508254593 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TextGenerationSettingsGenerated::TextGenerationSettings_TextGenerationSettings1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TextGenerationSettingsGenerated_TextGenerationSettings_TextGenerationSettings1_m2503237126 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TextGenerationSettingsGenerated::TextGenerationSettings_font(JSVCall)
extern "C"  void UnityEngine_TextGenerationSettingsGenerated_TextGenerationSettings_font_m1481064535 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TextGenerationSettingsGenerated::TextGenerationSettings_color(JSVCall)
extern "C"  void UnityEngine_TextGenerationSettingsGenerated_TextGenerationSettings_color_m3631460163 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TextGenerationSettingsGenerated::TextGenerationSettings_fontSize(JSVCall)
extern "C"  void UnityEngine_TextGenerationSettingsGenerated_TextGenerationSettings_fontSize_m507392214 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TextGenerationSettingsGenerated::TextGenerationSettings_lineSpacing(JSVCall)
extern "C"  void UnityEngine_TextGenerationSettingsGenerated_TextGenerationSettings_lineSpacing_m4076057207 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TextGenerationSettingsGenerated::TextGenerationSettings_richText(JSVCall)
extern "C"  void UnityEngine_TextGenerationSettingsGenerated_TextGenerationSettings_richText_m540478909 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TextGenerationSettingsGenerated::TextGenerationSettings_scaleFactor(JSVCall)
extern "C"  void UnityEngine_TextGenerationSettingsGenerated_TextGenerationSettings_scaleFactor_m915386413 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TextGenerationSettingsGenerated::TextGenerationSettings_fontStyle(JSVCall)
extern "C"  void UnityEngine_TextGenerationSettingsGenerated_TextGenerationSettings_fontStyle_m1363458788 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TextGenerationSettingsGenerated::TextGenerationSettings_textAnchor(JSVCall)
extern "C"  void UnityEngine_TextGenerationSettingsGenerated_TextGenerationSettings_textAnchor_m3944775620 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TextGenerationSettingsGenerated::TextGenerationSettings_alignByGeometry(JSVCall)
extern "C"  void UnityEngine_TextGenerationSettingsGenerated_TextGenerationSettings_alignByGeometry_m167834680 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TextGenerationSettingsGenerated::TextGenerationSettings_resizeTextForBestFit(JSVCall)
extern "C"  void UnityEngine_TextGenerationSettingsGenerated_TextGenerationSettings_resizeTextForBestFit_m4201181185 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TextGenerationSettingsGenerated::TextGenerationSettings_resizeTextMinSize(JSVCall)
extern "C"  void UnityEngine_TextGenerationSettingsGenerated_TextGenerationSettings_resizeTextMinSize_m3260943700 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TextGenerationSettingsGenerated::TextGenerationSettings_resizeTextMaxSize(JSVCall)
extern "C"  void UnityEngine_TextGenerationSettingsGenerated_TextGenerationSettings_resizeTextMaxSize_m3324812162 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TextGenerationSettingsGenerated::TextGenerationSettings_updateBounds(JSVCall)
extern "C"  void UnityEngine_TextGenerationSettingsGenerated_TextGenerationSettings_updateBounds_m2849774760 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TextGenerationSettingsGenerated::TextGenerationSettings_verticalOverflow(JSVCall)
extern "C"  void UnityEngine_TextGenerationSettingsGenerated_TextGenerationSettings_verticalOverflow_m3201115438 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TextGenerationSettingsGenerated::TextGenerationSettings_horizontalOverflow(JSVCall)
extern "C"  void UnityEngine_TextGenerationSettingsGenerated_TextGenerationSettings_horizontalOverflow_m3257639488 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TextGenerationSettingsGenerated::TextGenerationSettings_generationExtents(JSVCall)
extern "C"  void UnityEngine_TextGenerationSettingsGenerated_TextGenerationSettings_generationExtents_m3712319509 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TextGenerationSettingsGenerated::TextGenerationSettings_pivot(JSVCall)
extern "C"  void UnityEngine_TextGenerationSettingsGenerated_TextGenerationSettings_pivot_m4012383556 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TextGenerationSettingsGenerated::TextGenerationSettings_generateOutOfBounds(JSVCall)
extern "C"  void UnityEngine_TextGenerationSettingsGenerated_TextGenerationSettings_generateOutOfBounds_m3390810305 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TextGenerationSettingsGenerated::TextGenerationSettings_Equals__TextGenerationSettings(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TextGenerationSettingsGenerated_TextGenerationSettings_Equals__TextGenerationSettings_m557624972 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TextGenerationSettingsGenerated::__Register()
extern "C"  void UnityEngine_TextGenerationSettingsGenerated___Register_m2479227035 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_TextGenerationSettingsGenerated::ilo_setObject1(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UnityEngine_TextGenerationSettingsGenerated_ilo_setObject1_m1219981954 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_TextGenerationSettingsGenerated::ilo_getObject2(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_TextGenerationSettingsGenerated_ilo_getObject2_m419872314 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TextGenerationSettingsGenerated::ilo_changeJSObj3(System.Int32,System.Object)
extern "C"  void UnityEngine_TextGenerationSettingsGenerated_ilo_changeJSObj3_m480977471 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObjNew1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TextGenerationSettingsGenerated::ilo_setEnum4(System.Int32,System.Int32)
extern "C"  void UnityEngine_TextGenerationSettingsGenerated_ilo_setEnum4_m471128938 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_TextGenerationSettingsGenerated::ilo_getEnum5(System.Int32)
extern "C"  int32_t UnityEngine_TextGenerationSettingsGenerated_ilo_getEnum5_m2964077452 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TextGenerationSettingsGenerated::ilo_setBooleanS6(System.Int32,System.Boolean)
extern "C"  void UnityEngine_TextGenerationSettingsGenerated_ilo_setBooleanS6_m1505035064 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TextGenerationSettingsGenerated::ilo_setInt327(System.Int32,System.Int32)
extern "C"  void UnityEngine_TextGenerationSettingsGenerated_ilo_setInt327_m1221656036 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_TextGenerationSettingsGenerated::ilo_getInt328(System.Int32)
extern "C"  int32_t UnityEngine_TextGenerationSettingsGenerated_ilo_getInt328_m2648206614 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TextGenerationSettingsGenerated::ilo_setVector2S9(System.Int32,UnityEngine.Vector2)
extern "C"  void UnityEngine_TextGenerationSettingsGenerated_ilo_setVector2S9_m865674627 (Il2CppObject * __this /* static, unused */, int32_t ___e0, Vector2_t4282066565  ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine_TextGenerationSettingsGenerated::ilo_getVector2S10(System.Int32)
extern "C"  Vector2_t4282066565  UnityEngine_TextGenerationSettingsGenerated_ilo_getVector2S10_m211328188 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
