﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType1744280289.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>{8E8FA28D-216A-43EC-8DCB-2258D1F7BF00}/__StaticArrayInitTypeSize=12
struct  __StaticArrayInitTypeSizeU3D12_t894410181 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D12_t894410181__padding[12];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for marshalling of: <PrivateImplementationDetails>{8E8FA28D-216A-43EC-8DCB-2258D1F7BF00}/__StaticArrayInitTypeSize=12
struct __StaticArrayInitTypeSizeU3D12_t894410181_marshaled_pinvoke
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D12_t894410181__padding[12];
	};
};
// Native definition for marshalling of: <PrivateImplementationDetails>{8E8FA28D-216A-43EC-8DCB-2258D1F7BF00}/__StaticArrayInitTypeSize=12
struct __StaticArrayInitTypeSizeU3D12_t894410181_marshaled_com
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D12_t894410181__padding[12];
	};
};
