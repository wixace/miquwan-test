﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ProtoBuf.ProtoEnumAttribute
struct ProtoEnumAttribute_t1733368543;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void ProtoBuf.ProtoEnumAttribute::.ctor()
extern "C"  void ProtoEnumAttribute__ctor_m625770293 (ProtoEnumAttribute_t1733368543 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ProtoBuf.ProtoEnumAttribute::get_Value()
extern "C"  int32_t ProtoEnumAttribute_get_Value_m2764296361 (ProtoEnumAttribute_t1733368543 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoEnumAttribute::set_Value(System.Int32)
extern "C"  void ProtoEnumAttribute_set_Value_m429926520 (ProtoEnumAttribute_t1733368543 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.ProtoEnumAttribute::HasValue()
extern "C"  bool ProtoEnumAttribute_HasValue_m849508466 (ProtoEnumAttribute_t1733368543 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ProtoBuf.ProtoEnumAttribute::get_Name()
extern "C"  String_t* ProtoEnumAttribute_get_Name_m1208006080 (ProtoEnumAttribute_t1733368543 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoEnumAttribute::set_Name(System.String)
extern "C"  void ProtoEnumAttribute_set_Name_m3913707947 (ProtoEnumAttribute_t1733368543 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
