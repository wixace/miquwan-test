﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TwirlEffect
struct TwirlEffect_t1211262097;
// UnityEngine.RenderTexture
struct RenderTexture_t1963041563;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture1963041563.h"

// System.Void TwirlEffect::.ctor()
extern "C"  void TwirlEffect__ctor_m1380508358 (TwirlEffect_t1211262097 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TwirlEffect::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void TwirlEffect_OnRenderImage_m203826648 (TwirlEffect_t1211262097 * __this, RenderTexture_t1963041563 * ___source0, RenderTexture_t1963041563 * ___destination1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
