﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PluginXiaomi
struct  PluginXiaomi_t2004955630  : public MonoBehaviour_t667441552
{
public:
	// System.String PluginXiaomi::uid
	String_t* ___uid_2;
	// System.String PluginXiaomi::session
	String_t* ___session_3;

public:
	inline static int32_t get_offset_of_uid_2() { return static_cast<int32_t>(offsetof(PluginXiaomi_t2004955630, ___uid_2)); }
	inline String_t* get_uid_2() const { return ___uid_2; }
	inline String_t** get_address_of_uid_2() { return &___uid_2; }
	inline void set_uid_2(String_t* value)
	{
		___uid_2 = value;
		Il2CppCodeGenWriteBarrier(&___uid_2, value);
	}

	inline static int32_t get_offset_of_session_3() { return static_cast<int32_t>(offsetof(PluginXiaomi_t2004955630, ___session_3)); }
	inline String_t* get_session_3() const { return ___session_3; }
	inline String_t** get_address_of_session_3() { return &___session_3; }
	inline void set_session_3(String_t* value)
	{
		___session_3 = value;
		Il2CppCodeGenWriteBarrier(&___session_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
