﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIGridGenerated/<UIGrid_onCustomSort_GetDelegate_member10_arg0>c__AnonStoreyC5
struct U3CUIGrid_onCustomSort_GetDelegate_member10_arg0U3Ec__AnonStoreyC5_t713359177;
// UnityEngine.Transform
struct Transform_t1659122786;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Transform1659122786.h"

// System.Void UIGridGenerated/<UIGrid_onCustomSort_GetDelegate_member10_arg0>c__AnonStoreyC5::.ctor()
extern "C"  void U3CUIGrid_onCustomSort_GetDelegate_member10_arg0U3Ec__AnonStoreyC5__ctor_m4001640306 (U3CUIGrid_onCustomSort_GetDelegate_member10_arg0U3Ec__AnonStoreyC5_t713359177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIGridGenerated/<UIGrid_onCustomSort_GetDelegate_member10_arg0>c__AnonStoreyC5::<>m__14E(UnityEngine.Transform,UnityEngine.Transform)
extern "C"  int32_t U3CUIGrid_onCustomSort_GetDelegate_member10_arg0U3Ec__AnonStoreyC5_U3CU3Em__14E_m2087775101 (U3CUIGrid_onCustomSort_GetDelegate_member10_arg0U3Ec__AnonStoreyC5_t713359177 * __this, Transform_t1659122786 * ___x0, Transform_t1659122786 * ___y1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
