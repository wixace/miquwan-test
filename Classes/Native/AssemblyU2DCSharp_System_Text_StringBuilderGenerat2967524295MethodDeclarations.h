﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System_Text_StringBuilderGenerated
struct System_Text_StringBuilderGenerated_t2967524295;
// JSVCall
struct JSVCall_t3708497963;
// System.Char[]
struct CharU5BU5D_t3324145743;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"

// System.Void System_Text_StringBuilderGenerated::.ctor()
extern "C"  void System_Text_StringBuilderGenerated__ctor_m3935718580 (System_Text_StringBuilderGenerated_t2967524295 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_StringBuilderGenerated::StringBuilder_StringBuilder1(JSVCall,System.Int32)
extern "C"  bool System_Text_StringBuilderGenerated_StringBuilder_StringBuilder1_m3056867376 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_StringBuilderGenerated::StringBuilder_StringBuilder2(JSVCall,System.Int32)
extern "C"  bool System_Text_StringBuilderGenerated_StringBuilder_StringBuilder2_m6664561 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_StringBuilderGenerated::StringBuilder_StringBuilder3(JSVCall,System.Int32)
extern "C"  bool System_Text_StringBuilderGenerated_StringBuilder_StringBuilder3_m1251429042 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_StringBuilderGenerated::StringBuilder_StringBuilder4(JSVCall,System.Int32)
extern "C"  bool System_Text_StringBuilderGenerated_StringBuilder_StringBuilder4_m2496193523 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_StringBuilderGenerated::StringBuilder_StringBuilder5(JSVCall,System.Int32)
extern "C"  bool System_Text_StringBuilderGenerated_StringBuilder_StringBuilder5_m3740958004 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_StringBuilderGenerated::StringBuilder_StringBuilder6(JSVCall,System.Int32)
extern "C"  bool System_Text_StringBuilderGenerated_StringBuilder_StringBuilder6_m690755189 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Text_StringBuilderGenerated::StringBuilder_MaxCapacity(JSVCall)
extern "C"  void System_Text_StringBuilderGenerated_StringBuilder_MaxCapacity_m1420005516 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Text_StringBuilderGenerated::StringBuilder_Capacity(JSVCall)
extern "C"  void System_Text_StringBuilderGenerated_StringBuilder_Capacity_m3501220104 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Text_StringBuilderGenerated::StringBuilder_Length(JSVCall)
extern "C"  void System_Text_StringBuilderGenerated_StringBuilder_Length_m2878536540 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Text_StringBuilderGenerated::StringBuilder_Chars_Int32(JSVCall)
extern "C"  void System_Text_StringBuilderGenerated_StringBuilder_Chars_Int32_m3418788606 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_StringBuilderGenerated::StringBuilder_Append__Char_Array__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool System_Text_StringBuilderGenerated_StringBuilder_Append__Char_Array__Int32__Int32_m618285491 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_StringBuilderGenerated::StringBuilder_Append__String__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool System_Text_StringBuilderGenerated_StringBuilder_Append__String__Int32__Int32_m2457359924 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_StringBuilderGenerated::StringBuilder_Append__Char__Int32(JSVCall,System.Int32)
extern "C"  bool System_Text_StringBuilderGenerated_StringBuilder_Append__Char__Int32_m1413307383 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_StringBuilderGenerated::StringBuilder_Append__Int64(JSVCall,System.Int32)
extern "C"  bool System_Text_StringBuilderGenerated_StringBuilder_Append__Int64_m680706348 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_StringBuilderGenerated::StringBuilder_Append__Object(JSVCall,System.Int32)
extern "C"  bool System_Text_StringBuilderGenerated_StringBuilder_Append__Object_m3293472738 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_StringBuilderGenerated::StringBuilder_Append__Int32(JSVCall,System.Int32)
extern "C"  bool System_Text_StringBuilderGenerated_StringBuilder_Append__Int32_m2687164941 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_StringBuilderGenerated::StringBuilder_Append__Double(JSVCall,System.Int32)
extern "C"  bool System_Text_StringBuilderGenerated_StringBuilder_Append__Double_m1599907572 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_StringBuilderGenerated::StringBuilder_Append__Int16(JSVCall,System.Int32)
extern "C"  bool System_Text_StringBuilderGenerated_StringBuilder_Append__Int16_m3505269075 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_StringBuilderGenerated::StringBuilder_Append__SByte(JSVCall,System.Int32)
extern "C"  bool System_Text_StringBuilderGenerated_StringBuilder_Append__SByte_m3845361626 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_StringBuilderGenerated::StringBuilder_Append__UInt64(JSVCall,System.Int32)
extern "C"  bool System_Text_StringBuilderGenerated_StringBuilder_Append__UInt64_m3185005403 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_StringBuilderGenerated::StringBuilder_Append__Char(JSVCall,System.Int32)
extern "C"  bool System_Text_StringBuilderGenerated_StringBuilder_Append__Char_m2097686841 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_StringBuilderGenerated::StringBuilder_Append__UInt32(JSVCall,System.Int32)
extern "C"  bool System_Text_StringBuilderGenerated_StringBuilder_Append__UInt32_m896496700 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_StringBuilderGenerated::StringBuilder_Append__Single(JSVCall,System.Int32)
extern "C"  bool System_Text_StringBuilderGenerated_StringBuilder_Append__Single_m2880826411 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_StringBuilderGenerated::StringBuilder_Append__UInt16(JSVCall,System.Int32)
extern "C"  bool System_Text_StringBuilderGenerated_StringBuilder_Append__UInt16_m1714600834 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_StringBuilderGenerated::StringBuilder_Append__String(JSVCall,System.Int32)
extern "C"  bool System_Text_StringBuilderGenerated_StringBuilder_Append__String_m3193822004 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_StringBuilderGenerated::StringBuilder_Append__Char_Array(JSVCall,System.Int32)
extern "C"  bool System_Text_StringBuilderGenerated_StringBuilder_Append__Char_Array_m2647542003 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_StringBuilderGenerated::StringBuilder_Append__Boolean(JSVCall,System.Int32)
extern "C"  bool System_Text_StringBuilderGenerated_StringBuilder_Append__Boolean_m410180775 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_StringBuilderGenerated::StringBuilder_Append__Decimal(JSVCall,System.Int32)
extern "C"  bool System_Text_StringBuilderGenerated_StringBuilder_Append__Decimal_m3668889360 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_StringBuilderGenerated::StringBuilder_Append__Byte(JSVCall,System.Int32)
extern "C"  bool System_Text_StringBuilderGenerated_StringBuilder_Append__Byte_m838649195 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_StringBuilderGenerated::StringBuilder_AppendFormat__String__Object__Object__Object(JSVCall,System.Int32)
extern "C"  bool System_Text_StringBuilderGenerated_StringBuilder_AppendFormat__String__Object__Object__Object_m1599781512 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_StringBuilderGenerated::StringBuilder_AppendFormat__IFormatProvider__String__Object_Array(JSVCall,System.Int32)
extern "C"  bool System_Text_StringBuilderGenerated_StringBuilder_AppendFormat__IFormatProvider__String__Object_Array_m1810788643 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_StringBuilderGenerated::StringBuilder_AppendFormat__String__Object__Object(JSVCall,System.Int32)
extern "C"  bool System_Text_StringBuilderGenerated_StringBuilder_AppendFormat__String__Object__Object_m1120143497 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_StringBuilderGenerated::StringBuilder_AppendFormat__String__Object(JSVCall,System.Int32)
extern "C"  bool System_Text_StringBuilderGenerated_StringBuilder_AppendFormat__String__Object_m406316938 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_StringBuilderGenerated::StringBuilder_AppendFormat__String__Object_Array(JSVCall,System.Int32)
extern "C"  bool System_Text_StringBuilderGenerated_StringBuilder_AppendFormat__String__Object_Array_m1989166724 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_StringBuilderGenerated::StringBuilder_AppendLine__String(JSVCall,System.Int32)
extern "C"  bool System_Text_StringBuilderGenerated_StringBuilder_AppendLine__String_m2654464840 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_StringBuilderGenerated::StringBuilder_AppendLine(JSVCall,System.Int32)
extern "C"  bool System_Text_StringBuilderGenerated_StringBuilder_AppendLine_m308614135 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_StringBuilderGenerated::StringBuilder_CopyTo__Int32__Char_Array__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool System_Text_StringBuilderGenerated_StringBuilder_CopyTo__Int32__Char_Array__Int32__Int32_m1095869671 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_StringBuilderGenerated::StringBuilder_EnsureCapacity__Int32(JSVCall,System.Int32)
extern "C"  bool System_Text_StringBuilderGenerated_StringBuilder_EnsureCapacity__Int32_m2410748623 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_StringBuilderGenerated::StringBuilder_Equals__StringBuilder(JSVCall,System.Int32)
extern "C"  bool System_Text_StringBuilderGenerated_StringBuilder_Equals__StringBuilder_m805791044 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_StringBuilderGenerated::StringBuilder_Insert__Int32__Char_Array__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool System_Text_StringBuilderGenerated_StringBuilder_Insert__Int32__Char_Array__Int32__Int32_m924049630 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_StringBuilderGenerated::StringBuilder_Insert__Int32__String__Int32(JSVCall,System.Int32)
extern "C"  bool System_Text_StringBuilderGenerated_StringBuilder_Insert__Int32__String__Int32_m680705873 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_StringBuilderGenerated::StringBuilder_Insert__Int32__Int64(JSVCall,System.Int32)
extern "C"  bool System_Text_StringBuilderGenerated_StringBuilder_Insert__Int32__Int64_m352812001 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_StringBuilderGenerated::StringBuilder_Insert__Int32__Object(JSVCall,System.Int32)
extern "C"  bool System_Text_StringBuilderGenerated_StringBuilder_Insert__Int32__Object_m1718682573 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_StringBuilderGenerated::StringBuilder_Insert__Int32__Double(JSVCall,System.Int32)
extern "C"  bool System_Text_StringBuilderGenerated_StringBuilder_Insert__Int32__Double_m25117407 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_StringBuilderGenerated::StringBuilder_Insert__Int32__Int16(JSVCall,System.Int32)
extern "C"  bool System_Text_StringBuilderGenerated_StringBuilder_Insert__Int32__Int16_m3177374728 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_StringBuilderGenerated::StringBuilder_Insert__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool System_Text_StringBuilderGenerated_StringBuilder_Insert__Int32__Int32_m2359270594 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_StringBuilderGenerated::StringBuilder_Insert__Int32__UInt16(JSVCall,System.Int32)
extern "C"  bool System_Text_StringBuilderGenerated_StringBuilder_Insert__Int32__UInt16_m139810669 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_StringBuilderGenerated::StringBuilder_Insert__Int32__UInt32(JSVCall,System.Int32)
extern "C"  bool System_Text_StringBuilderGenerated_StringBuilder_Insert__Int32__UInt32_m3616673831 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_StringBuilderGenerated::StringBuilder_Insert__Int32__Single(JSVCall,System.Int32)
extern "C"  bool System_Text_StringBuilderGenerated_StringBuilder_Insert__Int32__Single_m1306036246 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_StringBuilderGenerated::StringBuilder_Insert__Int32__SByte(JSVCall,System.Int32)
extern "C"  bool System_Text_StringBuilderGenerated_StringBuilder_Insert__Int32__SByte_m3517467279 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_StringBuilderGenerated::StringBuilder_Insert__Int32__UInt64(JSVCall,System.Int32)
extern "C"  bool System_Text_StringBuilderGenerated_StringBuilder_Insert__Int32__UInt64_m1610215238 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_StringBuilderGenerated::StringBuilder_Insert__Int32__Boolean(JSVCall,System.Int32)
extern "C"  bool System_Text_StringBuilderGenerated_StringBuilder_Insert__Int32__Boolean_m3131293212 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_StringBuilderGenerated::StringBuilder_Insert__Int32__String(JSVCall,System.Int32)
extern "C"  bool System_Text_StringBuilderGenerated_StringBuilder_Insert__Int32__String_m1619031839 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_StringBuilderGenerated::StringBuilder_Insert__Int32__Char_Array(JSVCall,System.Int32)
extern "C"  bool System_Text_StringBuilderGenerated_StringBuilder_Insert__Int32__Char_Array_m4095407966 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_StringBuilderGenerated::StringBuilder_Insert__Int32__Decimal(JSVCall,System.Int32)
extern "C"  bool System_Text_StringBuilderGenerated_StringBuilder_Insert__Int32__Decimal_m2095034501 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_StringBuilderGenerated::StringBuilder_Insert__Int32__Char(JSVCall,System.Int32)
extern "C"  bool System_Text_StringBuilderGenerated_StringBuilder_Insert__Int32__Char_m2087109604 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_StringBuilderGenerated::StringBuilder_Insert__Int32__Byte(JSVCall,System.Int32)
extern "C"  bool System_Text_StringBuilderGenerated_StringBuilder_Insert__Int32__Byte_m828071958 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_StringBuilderGenerated::StringBuilder_Remove__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool System_Text_StringBuilderGenerated_StringBuilder_Remove__Int32__Int32_m190318861 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_StringBuilderGenerated::StringBuilder_Replace__Char__Char__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool System_Text_StringBuilderGenerated_StringBuilder_Replace__Char__Char__Int32__Int32_m1632671225 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_StringBuilderGenerated::StringBuilder_Replace__String__String__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool System_Text_StringBuilderGenerated_StringBuilder_Replace__String__String__Int32__Int32_m1112573423 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_StringBuilderGenerated::StringBuilder_Replace__String__String(JSVCall,System.Int32)
extern "C"  bool System_Text_StringBuilderGenerated_StringBuilder_Replace__String__String_m1134583343 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_StringBuilderGenerated::StringBuilder_Replace__Char__Char(JSVCall,System.Int32)
extern "C"  bool System_Text_StringBuilderGenerated_StringBuilder_Replace__Char__Char_m2004674489 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_StringBuilderGenerated::StringBuilder_ToString__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool System_Text_StringBuilderGenerated_StringBuilder_ToString__Int32__Int32_m1301946613 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_StringBuilderGenerated::StringBuilder_ToString(JSVCall,System.Int32)
extern "C"  bool System_Text_StringBuilderGenerated_StringBuilder_ToString_m3172932533 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Text_StringBuilderGenerated::__Register()
extern "C"  void System_Text_StringBuilderGenerated___Register_m2353201299 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char[] System_Text_StringBuilderGenerated::<StringBuilder_Append__Char_Array__Int32__Int32>m__E5()
extern "C"  CharU5BU5D_t3324145743* System_Text_StringBuilderGenerated_U3CStringBuilder_Append__Char_Array__Int32__Int32U3Em__E5_m767135856 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char[] System_Text_StringBuilderGenerated::<StringBuilder_Append__Char_Array>m__E6()
extern "C"  CharU5BU5D_t3324145743* System_Text_StringBuilderGenerated_U3CStringBuilder_Append__Char_ArrayU3Em__E6_m3722021873 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] System_Text_StringBuilderGenerated::<StringBuilder_AppendFormat__IFormatProvider__String__Object_Array>m__E7()
extern "C"  ObjectU5BU5D_t1108656482* System_Text_StringBuilderGenerated_U3CStringBuilder_AppendFormat__IFormatProvider__String__Object_ArrayU3Em__E7_m2942947453 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] System_Text_StringBuilderGenerated::<StringBuilder_AppendFormat__String__Object_Array>m__E8()
extern "C"  ObjectU5BU5D_t1108656482* System_Text_StringBuilderGenerated_U3CStringBuilder_AppendFormat__String__Object_ArrayU3Em__E8_m4287455661 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char[] System_Text_StringBuilderGenerated::<StringBuilder_CopyTo__Int32__Char_Array__Int32__Int32>m__E9()
extern "C"  CharU5BU5D_t3324145743* System_Text_StringBuilderGenerated_U3CStringBuilder_CopyTo__Int32__Char_Array__Int32__Int32U3Em__E9_m2217277836 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char[] System_Text_StringBuilderGenerated::<StringBuilder_Insert__Int32__Char_Array__Int32__Int32>m__EA()
extern "C"  CharU5BU5D_t3324145743* System_Text_StringBuilderGenerated_U3CStringBuilder_Insert__Int32__Char_Array__Int32__Int32U3Em__EA_m597524427 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char[] System_Text_StringBuilderGenerated::<StringBuilder_Insert__Int32__Char_Array>m__EB()
extern "C"  CharU5BU5D_t3324145743* System_Text_StringBuilderGenerated_U3CStringBuilder_Insert__Int32__Char_ArrayU3Em__EB_m3401785740 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System_Text_StringBuilderGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t System_Text_StringBuilderGenerated_ilo_getObject1_m1576108894 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System_Text_StringBuilderGenerated::ilo_getStringS2(System.Int32)
extern "C"  String_t* System_Text_StringBuilderGenerated_ilo_getStringS2_m906949491 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System_Text_StringBuilderGenerated::ilo_getInt323(System.Int32)
extern "C"  int32_t System_Text_StringBuilderGenerated_ilo_getInt323_m419068669 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_StringBuilderGenerated::ilo_attachFinalizerObject4(System.Int32)
extern "C"  bool System_Text_StringBuilderGenerated_ilo_attachFinalizerObject4_m1671891822 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Text_StringBuilderGenerated::ilo_addJSCSRel5(System.Int32,System.Object)
extern "C"  void System_Text_StringBuilderGenerated_ilo_addJSCSRel5_m1865427828 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System_Text_StringBuilderGenerated::ilo_setObject6(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t System_Text_StringBuilderGenerated_ilo_setObject6_m1791813147 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System_Text_StringBuilderGenerated::ilo_getWhatever7(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * System_Text_StringBuilderGenerated_ilo_getWhatever7_m3627368702 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double System_Text_StringBuilderGenerated::ilo_getDouble8(System.Int32)
extern "C"  double System_Text_StringBuilderGenerated_ilo_getDouble8_m428271784 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int16 System_Text_StringBuilderGenerated::ilo_getChar9(System.Int32)
extern "C"  int16_t System_Text_StringBuilderGenerated_ilo_getChar9_m3897096809 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System_Text_StringBuilderGenerated::ilo_getObject10(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * System_Text_StringBuilderGenerated_ilo_getObject10_m1854137637 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System_Text_StringBuilderGenerated::ilo_getInt6411(System.Int32)
extern "C"  int64_t System_Text_StringBuilderGenerated_ilo_getInt6411_m1863830682 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt16 System_Text_StringBuilderGenerated::ilo_getUInt1612(System.Int32)
extern "C"  uint16_t System_Text_StringBuilderGenerated_ilo_getUInt1612_m562975217 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 System_Text_StringBuilderGenerated::ilo_getUInt3213(System.Int32)
extern "C"  uint32_t System_Text_StringBuilderGenerated_ilo_getUInt3213_m2344358438 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single System_Text_StringBuilderGenerated::ilo_getSingle14(System.Int32)
extern "C"  float System_Text_StringBuilderGenerated_ilo_getSingle14_m2350637317 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 System_Text_StringBuilderGenerated::ilo_getUInt6415(System.Int32)
extern "C"  uint64_t System_Text_StringBuilderGenerated_ilo_getUInt6415_m3142137254 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_StringBuilderGenerated::ilo_getBooleanS16(System.Int32)
extern "C"  bool System_Text_StringBuilderGenerated_ilo_getBooleanS16_m3197421442 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System_Text_StringBuilderGenerated::ilo_getElement17(System.Int32,System.Int32)
extern "C"  int32_t System_Text_StringBuilderGenerated_ilo_getElement17_m1049654633 (Il2CppObject * __this /* static, unused */, int32_t ___id0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
