﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UICameraGenerated/<UICamera_onKey_GetDelegate_member54_arg0>c__AnonStoreyAF
struct U3CUICamera_onKey_GetDelegate_member54_arg0U3Ec__AnonStoreyAF_t1090294568;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "UnityEngine_UnityEngine_KeyCode3128317986.h"

// System.Void UICameraGenerated/<UICamera_onKey_GetDelegate_member54_arg0>c__AnonStoreyAF::.ctor()
extern "C"  void U3CUICamera_onKey_GetDelegate_member54_arg0U3Ec__AnonStoreyAF__ctor_m974304579 (U3CUICamera_onKey_GetDelegate_member54_arg0U3Ec__AnonStoreyAF_t1090294568 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICameraGenerated/<UICamera_onKey_GetDelegate_member54_arg0>c__AnonStoreyAF::<>m__122(UnityEngine.GameObject,UnityEngine.KeyCode)
extern "C"  void U3CUICamera_onKey_GetDelegate_member54_arg0U3Ec__AnonStoreyAF_U3CU3Em__122_m1355046966 (U3CUICamera_onKey_GetDelegate_member54_arg0U3Ec__AnonStoreyAF_t1090294568 * __this, GameObject_t3674682005 * ___go0, int32_t ___key1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
