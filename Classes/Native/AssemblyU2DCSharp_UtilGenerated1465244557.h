﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// JSDataExchangeMgr/DGetV`1<System.Object[]>
struct DGetV_1_t986307823;
// JSDataExchangeMgr/DGetV`1<System.Char[]>
struct DGetV_1_t3201797084;
// JSDataExchangeMgr/DGetV`1<System.Byte[]>
struct DGetV_1_t4138411810;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UtilGenerated
struct  UtilGenerated_t1465244557  : public Il2CppObject
{
public:

public:
};

struct UtilGenerated_t1465244557_StaticFields
{
public:
	// JSDataExchangeMgr/DGetV`1<System.Object[]> UtilGenerated::<>f__am$cache0
	DGetV_1_t986307823 * ___U3CU3Ef__amU24cache0_0;
	// JSDataExchangeMgr/DGetV`1<System.Object[]> UtilGenerated::<>f__am$cache1
	DGetV_1_t986307823 * ___U3CU3Ef__amU24cache1_1;
	// JSDataExchangeMgr/DGetV`1<System.Char[]> UtilGenerated::<>f__am$cache2
	DGetV_1_t3201797084 * ___U3CU3Ef__amU24cache2_2;
	// JSDataExchangeMgr/DGetV`1<System.Byte[]> UtilGenerated::<>f__am$cache3
	DGetV_1_t4138411810 * ___U3CU3Ef__amU24cache3_3;
	// JSDataExchangeMgr/DGetV`1<System.Byte[]> UtilGenerated::<>f__am$cache4
	DGetV_1_t4138411810 * ___U3CU3Ef__amU24cache4_4;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_0() { return static_cast<int32_t>(offsetof(UtilGenerated_t1465244557_StaticFields, ___U3CU3Ef__amU24cache0_0)); }
	inline DGetV_1_t986307823 * get_U3CU3Ef__amU24cache0_0() const { return ___U3CU3Ef__amU24cache0_0; }
	inline DGetV_1_t986307823 ** get_address_of_U3CU3Ef__amU24cache0_0() { return &___U3CU3Ef__amU24cache0_0; }
	inline void set_U3CU3Ef__amU24cache0_0(DGetV_1_t986307823 * value)
	{
		___U3CU3Ef__amU24cache0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_1() { return static_cast<int32_t>(offsetof(UtilGenerated_t1465244557_StaticFields, ___U3CU3Ef__amU24cache1_1)); }
	inline DGetV_1_t986307823 * get_U3CU3Ef__amU24cache1_1() const { return ___U3CU3Ef__amU24cache1_1; }
	inline DGetV_1_t986307823 ** get_address_of_U3CU3Ef__amU24cache1_1() { return &___U3CU3Ef__amU24cache1_1; }
	inline void set_U3CU3Ef__amU24cache1_1(DGetV_1_t986307823 * value)
	{
		___U3CU3Ef__amU24cache1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1_1, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_2() { return static_cast<int32_t>(offsetof(UtilGenerated_t1465244557_StaticFields, ___U3CU3Ef__amU24cache2_2)); }
	inline DGetV_1_t3201797084 * get_U3CU3Ef__amU24cache2_2() const { return ___U3CU3Ef__amU24cache2_2; }
	inline DGetV_1_t3201797084 ** get_address_of_U3CU3Ef__amU24cache2_2() { return &___U3CU3Ef__amU24cache2_2; }
	inline void set_U3CU3Ef__amU24cache2_2(DGetV_1_t3201797084 * value)
	{
		___U3CU3Ef__amU24cache2_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache2_2, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_3() { return static_cast<int32_t>(offsetof(UtilGenerated_t1465244557_StaticFields, ___U3CU3Ef__amU24cache3_3)); }
	inline DGetV_1_t4138411810 * get_U3CU3Ef__amU24cache3_3() const { return ___U3CU3Ef__amU24cache3_3; }
	inline DGetV_1_t4138411810 ** get_address_of_U3CU3Ef__amU24cache3_3() { return &___U3CU3Ef__amU24cache3_3; }
	inline void set_U3CU3Ef__amU24cache3_3(DGetV_1_t4138411810 * value)
	{
		___U3CU3Ef__amU24cache3_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache3_3, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache4_4() { return static_cast<int32_t>(offsetof(UtilGenerated_t1465244557_StaticFields, ___U3CU3Ef__amU24cache4_4)); }
	inline DGetV_1_t4138411810 * get_U3CU3Ef__amU24cache4_4() const { return ___U3CU3Ef__amU24cache4_4; }
	inline DGetV_1_t4138411810 ** get_address_of_U3CU3Ef__amU24cache4_4() { return &___U3CU3Ef__amU24cache4_4; }
	inline void set_U3CU3Ef__amU24cache4_4(DGetV_1_t4138411810 * value)
	{
		___U3CU3Ef__amU24cache4_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache4_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
