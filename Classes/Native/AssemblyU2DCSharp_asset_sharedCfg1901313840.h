﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "AssemblyU2DCSharp_CsCfgBase69924517.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// asset_sharedCfg
struct  asset_sharedCfg_t1901313840  : public CsCfgBase_t69924517
{
public:
	// System.Int32 asset_sharedCfg::id
	int32_t ___id_0;
	// System.Int32 asset_sharedCfg::type
	int32_t ___type_1;
	// System.String asset_sharedCfg::asset
	String_t* ___asset_2;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(asset_sharedCfg_t1901313840, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_type_1() { return static_cast<int32_t>(offsetof(asset_sharedCfg_t1901313840, ___type_1)); }
	inline int32_t get_type_1() const { return ___type_1; }
	inline int32_t* get_address_of_type_1() { return &___type_1; }
	inline void set_type_1(int32_t value)
	{
		___type_1 = value;
	}

	inline static int32_t get_offset_of_asset_2() { return static_cast<int32_t>(offsetof(asset_sharedCfg_t1901313840, ___asset_2)); }
	inline String_t* get_asset_2() const { return ___asset_2; }
	inline String_t** get_address_of_asset_2() { return &___asset_2; }
	inline void set_asset_2(String_t* value)
	{
		___asset_2 = value;
		Il2CppCodeGenWriteBarrier(&___asset_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
