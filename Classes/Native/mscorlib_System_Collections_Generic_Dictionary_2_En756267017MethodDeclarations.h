﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3363211663MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,Scripts.JSBindingClasses.JsRepresentClass>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m1334771884(__this, ___dictionary0, method) ((  void (*) (Enumerator_t756267017 *, Dictionary_2_t3733910921 *, const MethodInfo*))Enumerator__ctor_m3920831137_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,Scripts.JSBindingClasses.JsRepresentClass>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m402313461(__this, method) ((  Il2CppObject * (*) (Enumerator_t756267017 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3262087712_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,Scripts.JSBindingClasses.JsRepresentClass>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m595721353(__this, method) ((  void (*) (Enumerator_t756267017 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2959141748_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,Scripts.JSBindingClasses.JsRepresentClass>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3336060882(__this, method) ((  DictionaryEntry_t1751606614  (*) (Enumerator_t756267017 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2279524093_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,Scripts.JSBindingClasses.JsRepresentClass>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m847561233(__this, method) ((  Il2CppObject * (*) (Enumerator_t756267017 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1201448700_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,Scripts.JSBindingClasses.JsRepresentClass>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3805962339(__this, method) ((  Il2CppObject * (*) (Enumerator_t756267017 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m294434446_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,Scripts.JSBindingClasses.JsRepresentClass>::MoveNext()
#define Enumerator_MoveNext_m1612402677(__this, method) ((  bool (*) (Enumerator_t756267017 *, const MethodInfo*))Enumerator_MoveNext_m217327200_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,Scripts.JSBindingClasses.JsRepresentClass>::get_Current()
#define Enumerator_get_Current_m1196816155(__this, method) ((  KeyValuePair_2_t3632691627  (*) (Enumerator_t756267017 *, const MethodInfo*))Enumerator_get_Current_m4240003024_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,Scripts.JSBindingClasses.JsRepresentClass>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m1365428482(__this, method) ((  String_t* (*) (Enumerator_t756267017 *, const MethodInfo*))Enumerator_get_CurrentKey_m3062159917_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,Scripts.JSBindingClasses.JsRepresentClass>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m3465266726(__this, method) ((  JsRepresentClass_t2913492551 * (*) (Enumerator_t756267017 *, const MethodInfo*))Enumerator_get_CurrentValue_m592783249_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,Scripts.JSBindingClasses.JsRepresentClass>::Reset()
#define Enumerator_Reset_m2667010302(__this, method) ((  void (*) (Enumerator_t756267017 *, const MethodInfo*))Enumerator_Reset_m3001375603_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,Scripts.JSBindingClasses.JsRepresentClass>::VerifyState()
#define Enumerator_VerifyState_m479687303(__this, method) ((  void (*) (Enumerator_t756267017 *, const MethodInfo*))Enumerator_VerifyState_m4290054460_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,Scripts.JSBindingClasses.JsRepresentClass>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m4162869295(__this, method) ((  void (*) (Enumerator_t756267017 *, const MethodInfo*))Enumerator_VerifyCurrent_m2318603684_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,Scripts.JSBindingClasses.JsRepresentClass>::Dispose()
#define Enumerator_Dispose_m1424853582(__this, method) ((  void (*) (Enumerator_t756267017 *, const MethodInfo*))Enumerator_Dispose_m627360643_gshared)(__this, method)
