﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ProtoBuf.Serializers.DefaultValueDecorator
struct DefaultValueDecorator_t2303838757;
// ProtoBuf.Meta.TypeModel
struct TypeModel_t2730011105;
// System.Object
struct Il2CppObject;
// ProtoBuf.Serializers.IProtoSerializer
struct IProtoSerializer_t3033312651;
// System.Type
struct Type_t;
// ProtoBuf.ProtoWriter
struct ProtoWriter_t4117914721;
// ProtoBuf.ProtoReader
struct ProtoReader_t3962509489;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ProtoBuf_Meta_TypeModel2730011105.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_ProtoBuf_ProtoWriter4117914721.h"
#include "AssemblyU2DCSharp_ProtoBuf_ProtoReader3962509489.h"

// System.Void ProtoBuf.Serializers.DefaultValueDecorator::.ctor(ProtoBuf.Meta.TypeModel,System.Object,ProtoBuf.Serializers.IProtoSerializer)
extern "C"  void DefaultValueDecorator__ctor_m134836450 (DefaultValueDecorator_t2303838757 * __this, TypeModel_t2730011105 * ___model0, Il2CppObject * ___defaultValue1, Il2CppObject * ___tail2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type ProtoBuf.Serializers.DefaultValueDecorator::get_ExpectedType()
extern "C"  Type_t * DefaultValueDecorator_get_ExpectedType_m1691912313 (DefaultValueDecorator_t2303838757 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Serializers.DefaultValueDecorator::get_RequiresOldValue()
extern "C"  bool DefaultValueDecorator_get_RequiresOldValue_m2589006937 (DefaultValueDecorator_t2303838757 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Serializers.DefaultValueDecorator::get_ReturnsValue()
extern "C"  bool DefaultValueDecorator_get_ReturnsValue_m2838700911 (DefaultValueDecorator_t2303838757 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Serializers.DefaultValueDecorator::Write(System.Object,ProtoBuf.ProtoWriter)
extern "C"  void DefaultValueDecorator_Write_m2251374839 (DefaultValueDecorator_t2303838757 * __this, Il2CppObject * ___value0, ProtoWriter_t4117914721 * ___dest1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProtoBuf.Serializers.DefaultValueDecorator::Read(System.Object,ProtoBuf.ProtoReader)
extern "C"  Il2CppObject * DefaultValueDecorator_Read_m2225008825 (DefaultValueDecorator_t2303838757 * __this, Il2CppObject * ___value0, ProtoReader_t3962509489 * ___source1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type ProtoBuf.Serializers.DefaultValueDecorator::ilo_get_ExpectedType1(ProtoBuf.Serializers.IProtoSerializer)
extern "C"  Type_t * DefaultValueDecorator_ilo_get_ExpectedType1_m2680104554 (Il2CppObject * __this /* static, unused */, Il2CppObject * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Serializers.DefaultValueDecorator::ilo_Write2(ProtoBuf.Serializers.IProtoSerializer,System.Object,ProtoBuf.ProtoWriter)
extern "C"  void DefaultValueDecorator_ilo_Write2_m1830173977 (Il2CppObject * __this /* static, unused */, Il2CppObject * ____this0, Il2CppObject * ___value1, ProtoWriter_t4117914721 * ___dest2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProtoBuf.Serializers.DefaultValueDecorator::ilo_Read3(ProtoBuf.Serializers.IProtoSerializer,System.Object,ProtoBuf.ProtoReader)
extern "C"  Il2CppObject * DefaultValueDecorator_ilo_Read3_m3353556466 (Il2CppObject * __this /* static, unused */, Il2CppObject * ____this0, Il2CppObject * ___value1, ProtoReader_t3962509489 * ___source2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
