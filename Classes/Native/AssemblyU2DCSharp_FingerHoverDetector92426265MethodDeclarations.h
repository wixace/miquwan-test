﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FingerHoverDetector
struct FingerHoverDetector_t92426265;
// FingerEventDetector`1/FingerEventHandler<FingerHoverEvent>
struct FingerEventHandler_t80399884;
// FingerHoverEvent
struct FingerHoverEvent_t1767068455;
// FingerGestures/Finger
struct Finger_t182428197;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// FingerEventDetector
struct FingerEventDetector_t1271053175;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_FingerHoverEvent1767068455.h"
#include "AssemblyU2DCSharp_FingerHoverPhase1776806408.h"
#include "AssemblyU2DCSharp_FingerGestures_Finger182428197.h"
#include "AssemblyU2DCSharp_FingerEventDetector1271053175.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "AssemblyU2DCSharp_FingerHoverDetector92426265.h"
#include "AssemblyU2DCSharp_ScreenRaycastData1110901127.h"

// System.Void FingerHoverDetector::.ctor()
extern "C"  void FingerHoverDetector__ctor_m2103562098 (FingerHoverDetector_t92426265 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FingerHoverDetector::add_OnFingerHover(FingerEventDetector`1/FingerEventHandler<FingerHoverEvent>)
extern "C"  void FingerHoverDetector_add_OnFingerHover_m2525599723 (FingerHoverDetector_t92426265 * __this, FingerEventHandler_t80399884 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FingerHoverDetector::remove_OnFingerHover(FingerEventDetector`1/FingerEventHandler<FingerHoverEvent>)
extern "C"  void FingerHoverDetector_remove_OnFingerHover_m777057488 (FingerHoverDetector_t92426265 * __this, FingerEventHandler_t80399884 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FingerHoverDetector::Start()
extern "C"  void FingerHoverDetector_Start_m1050699890 (FingerHoverDetector_t92426265 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FingerHoverDetector::FireEvent(FingerHoverEvent,FingerHoverPhase)
extern "C"  bool FingerHoverDetector_FireEvent_m4213308189 (FingerHoverDetector_t92426265 * __this, FingerHoverEvent_t1767068455 * ___e0, int32_t ___phase1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FingerHoverDetector::ProcessFinger(FingerGestures/Finger)
extern "C"  void FingerHoverDetector_ProcessFinger_m375248357 (FingerHoverDetector_t92426265 * __this, Finger_t182428197 * ___finger0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FingerHoverDetector::ilo_get_IsDown1(FingerGestures/Finger)
extern "C"  bool FingerHoverDetector_ilo_get_IsDown1_m2205770048 (Il2CppObject * __this /* static, unused */, Finger_t182428197 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject FingerHoverDetector::ilo_PickObject2(FingerEventDetector,UnityEngine.Vector2)
extern "C"  GameObject_t3674682005 * FingerHoverDetector_ilo_PickObject2_m2285803883 (Il2CppObject * __this /* static, unused */, FingerEventDetector_t1271053175 * ____this0, Vector2_t4282066565  ___screenPos1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FingerHoverDetector::ilo_FireEvent3(FingerHoverDetector,FingerHoverEvent,FingerHoverPhase)
extern "C"  bool FingerHoverDetector_ilo_FireEvent3_m2750783000 (Il2CppObject * __this /* static, unused */, FingerHoverDetector_t92426265 * ____this0, FingerHoverEvent_t1767068455 * ___e1, int32_t ___phase2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ScreenRaycastData FingerHoverDetector::ilo_get_Raycast4(FingerEventDetector)
extern "C"  ScreenRaycastData_t1110901127  FingerHoverDetector_ilo_get_Raycast4_m3006894510 (Il2CppObject * __this /* static, unused */, FingerEventDetector_t1271053175 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
