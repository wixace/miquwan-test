﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mihua_Asset_AssetCollectGenerated
struct Mihua_Asset_AssetCollectGenerated_t3233660991;
// JSVCall
struct JSVCall_t3708497963;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_String7231557.h"

// System.Void Mihua_Asset_AssetCollectGenerated::.ctor()
extern "C"  void Mihua_Asset_AssetCollectGenerated__ctor_m2580149260 (Mihua_Asset_AssetCollectGenerated_t3233660991 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua_Asset_AssetCollectGenerated::AssetCollect_uiNeedList(JSVCall)
extern "C"  void Mihua_Asset_AssetCollectGenerated_AssetCollect_uiNeedList_m2724830672 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mihua_Asset_AssetCollectGenerated::AssetCollect_AddAssets__String(JSVCall,System.Int32)
extern "C"  bool Mihua_Asset_AssetCollectGenerated_AssetCollect_AddAssets__String_m786549988 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mihua_Asset_AssetCollectGenerated::AssetCollect_AddAssetsByJArray__String(JSVCall,System.Int32)
extern "C"  bool Mihua_Asset_AssetCollectGenerated_AssetCollect_AddAssetsByJArray__String_m1369466826 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mihua_Asset_AssetCollectGenerated::AssetCollect_AddUIAssets__String(JSVCall,System.Int32)
extern "C"  bool Mihua_Asset_AssetCollectGenerated_AssetCollect_AddUIAssets__String_m652612024 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mihua_Asset_AssetCollectGenerated::AssetCollect_AllAssets__checkpointCfg(JSVCall,System.Int32)
extern "C"  bool Mihua_Asset_AssetCollectGenerated_AssetCollect_AllAssets__checkpointCfg_m2454094891 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mihua_Asset_AssetCollectGenerated::AssetCollect_NpcAseetsCs__Int32(JSVCall,System.Int32)
extern "C"  bool Mihua_Asset_AssetCollectGenerated_AssetCollect_NpcAseetsCs__Int32_m2305547647 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua_Asset_AssetCollectGenerated::__Register()
extern "C"  void Mihua_Asset_AssetCollectGenerated___Register_m4100599867 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mihua_Asset_AssetCollectGenerated::ilo_getStringS1(System.Int32)
extern "C"  String_t* Mihua_Asset_AssetCollectGenerated_ilo_getStringS1_m959356528 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua_Asset_AssetCollectGenerated::ilo_AddAssets2(System.String)
extern "C"  void Mihua_Asset_AssetCollectGenerated_ilo_AddAssets2_m4148178249 (Il2CppObject * __this /* static, unused */, String_t* ___assetPath0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua_Asset_AssetCollectGenerated::ilo_AddAssetsByJArray3(System.String)
extern "C"  void Mihua_Asset_AssetCollectGenerated_ilo_AddAssetsByJArray3_m2416539790 (Il2CppObject * __this /* static, unused */, String_t* ___arr0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mihua_Asset_AssetCollectGenerated::ilo_getInt324(System.Int32)
extern "C"  int32_t Mihua_Asset_AssetCollectGenerated_ilo_getInt324_m2114091506 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
