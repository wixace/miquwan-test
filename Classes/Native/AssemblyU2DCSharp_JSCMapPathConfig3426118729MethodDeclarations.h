﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// JSCMapPathConfig
struct JSCMapPathConfig_t3426118729;
// ProtoBuf.IExtension
struct IExtension_t1606339106;
// System.String
struct String_t;
// System.Collections.Generic.List`1<JSCMapPathPointInfoConfig>
struct List_1_t4071866601;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void JSCMapPathConfig::.ctor()
extern "C"  void JSCMapPathConfig__ctor_m495549554 (JSCMapPathConfig_t3426118729 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.IExtension JSCMapPathConfig::ProtoBuf.IExtensible.GetExtensionObject(System.Boolean)
extern "C"  Il2CppObject * JSCMapPathConfig_ProtoBuf_IExtensible_GetExtensionObject_m1621007938 (JSCMapPathConfig_t3426118729 * __this, bool ___createIfMissing0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 JSCMapPathConfig::get_id()
extern "C"  int32_t JSCMapPathConfig_get_id_m2526831716 (JSCMapPathConfig_t3426118729 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSCMapPathConfig::set_id(System.Int32)
extern "C"  void JSCMapPathConfig_set_id_m1943111835 (JSCMapPathConfig_t3426118729 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String JSCMapPathConfig::get_sceneName()
extern "C"  String_t* JSCMapPathConfig_get_sceneName_m905612539 (JSCMapPathConfig_t3426118729 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSCMapPathConfig::set_sceneName(System.String)
extern "C"  void JSCMapPathConfig_set_sceneName_m2593662328 (JSCMapPathConfig_t3426118729 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String JSCMapPathConfig::get_viewName()
extern "C"  String_t* JSCMapPathConfig_get_viewName_m4116196078 (JSCMapPathConfig_t3426118729 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSCMapPathConfig::set_viewName(System.String)
extern "C"  void JSCMapPathConfig_set_viewName_m3801287971 (JSCMapPathConfig_t3426118729 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<JSCMapPathPointInfoConfig> JSCMapPathConfig::get_pathPoints()
extern "C"  List_1_t4071866601 * JSCMapPathConfig_get_pathPoints_m2921600765 (JSCMapPathConfig_t3426118729 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
