﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ProtoBuf.Meta.ValueMember/Comparer
struct Comparer_t2869992983;
// System.Object
struct Il2CppObject;
// ProtoBuf.Meta.ValueMember
struct ValueMember_t110398141;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_ProtoBuf_Meta_ValueMember110398141.h"

// System.Void ProtoBuf.Meta.ValueMember/Comparer::.ctor()
extern "C"  void Comparer__ctor_m1065163876 (Comparer_t2869992983 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.ValueMember/Comparer::.cctor()
extern "C"  void Comparer__cctor_m2473212873 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ProtoBuf.Meta.ValueMember/Comparer::Compare(System.Object,System.Object)
extern "C"  int32_t Comparer_Compare_m2932975657 (Comparer_t2869992983 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ProtoBuf.Meta.ValueMember/Comparer::Compare(ProtoBuf.Meta.ValueMember,ProtoBuf.Meta.ValueMember)
extern "C"  int32_t Comparer_Compare_m1936907611 (Comparer_t2869992983 * __this, ValueMember_t110398141 * ___x0, ValueMember_t110398141 * ___y1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
