﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ProtoBuf.Serializers.DoubleSerializer
struct DoubleSerializer_t2835027933;
// ProtoBuf.Meta.TypeModel
struct TypeModel_t2730011105;
// System.Type
struct Type_t;
// System.Object
struct Il2CppObject;
// ProtoBuf.ProtoReader
struct ProtoReader_t3962509489;
// ProtoBuf.ProtoWriter
struct ProtoWriter_t4117914721;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ProtoBuf_Meta_TypeModel2730011105.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_ProtoBuf_ProtoReader3962509489.h"
#include "AssemblyU2DCSharp_ProtoBuf_ProtoWriter4117914721.h"

// System.Void ProtoBuf.Serializers.DoubleSerializer::.ctor(ProtoBuf.Meta.TypeModel)
extern "C"  void DoubleSerializer__ctor_m4174508753 (DoubleSerializer_t2835027933 * __this, TypeModel_t2730011105 * ___model0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Serializers.DoubleSerializer::.cctor()
extern "C"  void DoubleSerializer__cctor_m124982549 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Serializers.DoubleSerializer::ProtoBuf.Serializers.IProtoSerializer.get_RequiresOldValue()
extern "C"  bool DoubleSerializer_ProtoBuf_Serializers_IProtoSerializer_get_RequiresOldValue_m2614204358 (DoubleSerializer_t2835027933 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Serializers.DoubleSerializer::ProtoBuf.Serializers.IProtoSerializer.get_ReturnsValue()
extern "C"  bool DoubleSerializer_ProtoBuf_Serializers_IProtoSerializer_get_ReturnsValue_m4276903260 (DoubleSerializer_t2835027933 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type ProtoBuf.Serializers.DoubleSerializer::get_ExpectedType()
extern "C"  Type_t * DoubleSerializer_get_ExpectedType_m1697469421 (DoubleSerializer_t2835027933 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProtoBuf.Serializers.DoubleSerializer::Read(System.Object,ProtoBuf.ProtoReader)
extern "C"  Il2CppObject * DoubleSerializer_Read_m1754555415 (DoubleSerializer_t2835027933 * __this, Il2CppObject * ___value0, ProtoReader_t3962509489 * ___source1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Serializers.DoubleSerializer::Write(System.Object,ProtoBuf.ProtoWriter)
extern "C"  void DoubleSerializer_Write_m2056801551 (DoubleSerializer_t2835027933 * __this, Il2CppObject * ___value0, ProtoWriter_t4117914721 * ___dest1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
