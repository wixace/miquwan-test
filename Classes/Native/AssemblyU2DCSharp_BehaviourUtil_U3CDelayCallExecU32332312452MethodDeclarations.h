﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BehaviourUtil/<DelayCallExec>c__Iterator42`4<System.Int32,System.Object,UnityEngine.Vector3,System.Object>
struct U3CDelayCallExecU3Ec__Iterator42_4_t2332312452;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void BehaviourUtil/<DelayCallExec>c__Iterator42`4<System.Int32,System.Object,UnityEngine.Vector3,System.Object>::.ctor()
extern "C"  void U3CDelayCallExecU3Ec__Iterator42_4__ctor_m3865019310_gshared (U3CDelayCallExecU3Ec__Iterator42_4_t2332312452 * __this, const MethodInfo* method);
#define U3CDelayCallExecU3Ec__Iterator42_4__ctor_m3865019310(__this, method) ((  void (*) (U3CDelayCallExecU3Ec__Iterator42_4_t2332312452 *, const MethodInfo*))U3CDelayCallExecU3Ec__Iterator42_4__ctor_m3865019310_gshared)(__this, method)
// System.Object BehaviourUtil/<DelayCallExec>c__Iterator42`4<System.Int32,System.Object,UnityEngine.Vector3,System.Object>::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDelayCallExecU3Ec__Iterator42_4_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3153473710_gshared (U3CDelayCallExecU3Ec__Iterator42_4_t2332312452 * __this, const MethodInfo* method);
#define U3CDelayCallExecU3Ec__Iterator42_4_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3153473710(__this, method) ((  Il2CppObject * (*) (U3CDelayCallExecU3Ec__Iterator42_4_t2332312452 *, const MethodInfo*))U3CDelayCallExecU3Ec__Iterator42_4_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3153473710_gshared)(__this, method)
// System.Object BehaviourUtil/<DelayCallExec>c__Iterator42`4<System.Int32,System.Object,UnityEngine.Vector3,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDelayCallExecU3Ec__Iterator42_4_System_Collections_IEnumerator_get_Current_m701190210_gshared (U3CDelayCallExecU3Ec__Iterator42_4_t2332312452 * __this, const MethodInfo* method);
#define U3CDelayCallExecU3Ec__Iterator42_4_System_Collections_IEnumerator_get_Current_m701190210(__this, method) ((  Il2CppObject * (*) (U3CDelayCallExecU3Ec__Iterator42_4_t2332312452 *, const MethodInfo*))U3CDelayCallExecU3Ec__Iterator42_4_System_Collections_IEnumerator_get_Current_m701190210_gshared)(__this, method)
// System.Boolean BehaviourUtil/<DelayCallExec>c__Iterator42`4<System.Int32,System.Object,UnityEngine.Vector3,System.Object>::MoveNext()
extern "C"  bool U3CDelayCallExecU3Ec__Iterator42_4_MoveNext_m3316984430_gshared (U3CDelayCallExecU3Ec__Iterator42_4_t2332312452 * __this, const MethodInfo* method);
#define U3CDelayCallExecU3Ec__Iterator42_4_MoveNext_m3316984430(__this, method) ((  bool (*) (U3CDelayCallExecU3Ec__Iterator42_4_t2332312452 *, const MethodInfo*))U3CDelayCallExecU3Ec__Iterator42_4_MoveNext_m3316984430_gshared)(__this, method)
// System.Void BehaviourUtil/<DelayCallExec>c__Iterator42`4<System.Int32,System.Object,UnityEngine.Vector3,System.Object>::Dispose()
extern "C"  void U3CDelayCallExecU3Ec__Iterator42_4_Dispose_m3330096235_gshared (U3CDelayCallExecU3Ec__Iterator42_4_t2332312452 * __this, const MethodInfo* method);
#define U3CDelayCallExecU3Ec__Iterator42_4_Dispose_m3330096235(__this, method) ((  void (*) (U3CDelayCallExecU3Ec__Iterator42_4_t2332312452 *, const MethodInfo*))U3CDelayCallExecU3Ec__Iterator42_4_Dispose_m3330096235_gshared)(__this, method)
// System.Void BehaviourUtil/<DelayCallExec>c__Iterator42`4<System.Int32,System.Object,UnityEngine.Vector3,System.Object>::Reset()
extern "C"  void U3CDelayCallExecU3Ec__Iterator42_4_Reset_m1511452251_gshared (U3CDelayCallExecU3Ec__Iterator42_4_t2332312452 * __this, const MethodInfo* method);
#define U3CDelayCallExecU3Ec__Iterator42_4_Reset_m1511452251(__this, method) ((  void (*) (U3CDelayCallExecU3Ec__Iterator42_4_t2332312452 *, const MethodInfo*))U3CDelayCallExecU3Ec__Iterator42_4_Reset_m1511452251_gshared)(__this, method)
