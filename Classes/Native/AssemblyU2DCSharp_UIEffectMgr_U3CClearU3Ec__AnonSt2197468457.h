﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UIEffectMgr
struct UIEffectMgr_t952662675;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIEffectMgr/<Clear>c__AnonStorey15C
struct  U3CClearU3Ec__AnonStorey15C_t2197468457  : public Il2CppObject
{
public:
	// System.Int32 UIEffectMgr/<Clear>c__AnonStorey15C::id
	int32_t ___id_0;
	// UIEffectMgr UIEffectMgr/<Clear>c__AnonStorey15C::<>f__this
	UIEffectMgr_t952662675 * ___U3CU3Ef__this_1;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(U3CClearU3Ec__AnonStorey15C_t2197468457, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_1() { return static_cast<int32_t>(offsetof(U3CClearU3Ec__AnonStorey15C_t2197468457, ___U3CU3Ef__this_1)); }
	inline UIEffectMgr_t952662675 * get_U3CU3Ef__this_1() const { return ___U3CU3Ef__this_1; }
	inline UIEffectMgr_t952662675 ** get_address_of_U3CU3Ef__this_1() { return &___U3CU3Ef__this_1; }
	inline void set_U3CU3Ef__this_1(UIEffectMgr_t952662675 * value)
	{
		___U3CU3Ef__this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
