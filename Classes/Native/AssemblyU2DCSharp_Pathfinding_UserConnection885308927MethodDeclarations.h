﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.UserConnection
struct UserConnection_t885308927;

#include "codegen/il2cpp-codegen.h"

// System.Void Pathfinding.UserConnection::.ctor()
extern "C"  void UserConnection__ctor_m576745800 (UserConnection_t885308927 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
