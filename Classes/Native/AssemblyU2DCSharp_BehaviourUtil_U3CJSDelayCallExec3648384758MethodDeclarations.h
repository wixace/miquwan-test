﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BehaviourUtil/<JSDelayCallExec>c__Iterator39
struct U3CJSDelayCallExecU3Ec__Iterator39_t3648384758;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void BehaviourUtil/<JSDelayCallExec>c__Iterator39::.ctor()
extern "C"  void U3CJSDelayCallExecU3Ec__Iterator39__ctor_m4089583717 (U3CJSDelayCallExecU3Ec__Iterator39_t3648384758 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object BehaviourUtil/<JSDelayCallExec>c__Iterator39::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CJSDelayCallExecU3Ec__Iterator39_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3318722327 (U3CJSDelayCallExecU3Ec__Iterator39_t3648384758 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object BehaviourUtil/<JSDelayCallExec>c__Iterator39::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CJSDelayCallExecU3Ec__Iterator39_System_Collections_IEnumerator_get_Current_m2262352043 (U3CJSDelayCallExecU3Ec__Iterator39_t3648384758 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BehaviourUtil/<JSDelayCallExec>c__Iterator39::MoveNext()
extern "C"  bool U3CJSDelayCallExecU3Ec__Iterator39_MoveNext_m1177274711 (U3CJSDelayCallExecU3Ec__Iterator39_t3648384758 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BehaviourUtil/<JSDelayCallExec>c__Iterator39::Dispose()
extern "C"  void U3CJSDelayCallExecU3Ec__Iterator39_Dispose_m93159266 (U3CJSDelayCallExecU3Ec__Iterator39_t3648384758 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BehaviourUtil/<JSDelayCallExec>c__Iterator39::Reset()
extern "C"  void U3CJSDelayCallExecU3Ec__Iterator39_Reset_m1736016658 (U3CJSDelayCallExecU3Ec__Iterator39_t3648384758 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
