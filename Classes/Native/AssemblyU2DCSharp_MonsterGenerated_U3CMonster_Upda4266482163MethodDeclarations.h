﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MonsterGenerated/<Monster_UpdateTargetPos_GetDelegate_member25_arg1>c__AnonStorey74
struct U3CMonster_UpdateTargetPos_GetDelegate_member25_arg1U3Ec__AnonStorey74_t4266482163;

#include "codegen/il2cpp-codegen.h"

// System.Void MonsterGenerated/<Monster_UpdateTargetPos_GetDelegate_member25_arg1>c__AnonStorey74::.ctor()
extern "C"  void U3CMonster_UpdateTargetPos_GetDelegate_member25_arg1U3Ec__AnonStorey74__ctor_m1756363480 (U3CMonster_UpdateTargetPos_GetDelegate_member25_arg1U3Ec__AnonStorey74_t4266482163 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MonsterGenerated/<Monster_UpdateTargetPos_GetDelegate_member25_arg1>c__AnonStorey74::<>m__83()
extern "C"  void U3CMonster_UpdateTargetPos_GetDelegate_member25_arg1U3Ec__AnonStorey74_U3CU3Em__83_m2251408348 (U3CMonster_UpdateTargetPos_GetDelegate_member25_arg1U3Ec__AnonStorey74_t4266482163 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
