﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_trearvade130
struct M_trearvade130_t1924839578;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_trearvade1301924839578.h"

// System.Void GarbageiOS.M_trearvade130::.ctor()
extern "C"  void M_trearvade130__ctor_m1845462745 (M_trearvade130_t1924839578 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_trearvade130::M_sacuCure0(System.String[],System.Int32)
extern "C"  void M_trearvade130_M_sacuCure0_m2705955431 (M_trearvade130_t1924839578 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_trearvade130::M_tawaSayda1(System.String[],System.Int32)
extern "C"  void M_trearvade130_M_tawaSayda1_m1192850932 (M_trearvade130_t1924839578 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_trearvade130::M_barbeanairHasnaker2(System.String[],System.Int32)
extern "C"  void M_trearvade130_M_barbeanairHasnaker2_m2240046876 (M_trearvade130_t1924839578 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_trearvade130::M_lanokeHakecu3(System.String[],System.Int32)
extern "C"  void M_trearvade130_M_lanokeHakecu3_m1141372666 (M_trearvade130_t1924839578 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_trearvade130::M_matirWezitrea4(System.String[],System.Int32)
extern "C"  void M_trearvade130_M_matirWezitrea4_m2675120008 (M_trearvade130_t1924839578 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_trearvade130::M_qarsouyem5(System.String[],System.Int32)
extern "C"  void M_trearvade130_M_qarsouyem5_m2961601599 (M_trearvade130_t1924839578 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_trearvade130::M_nekall6(System.String[],System.Int32)
extern "C"  void M_trearvade130_M_nekall6_m332727781 (M_trearvade130_t1924839578 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_trearvade130::ilo_M_tawaSayda11(GarbageiOS.M_trearvade130,System.String[],System.Int32)
extern "C"  void M_trearvade130_ilo_M_tawaSayda11_m3944738432 (Il2CppObject * __this /* static, unused */, M_trearvade130_t1924839578 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_trearvade130::ilo_M_qarsouyem52(GarbageiOS.M_trearvade130,System.String[],System.Int32)
extern "C"  void M_trearvade130_ilo_M_qarsouyem52_m374694486 (Il2CppObject * __this /* static, unused */, M_trearvade130_t1924839578 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
