﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Utilities.ReflectionDelegateFactory
struct ReflectionDelegateFactory_t1590616920;
// System.String
struct String_t;
// System.IFormatProvider
struct IFormatProvider_t192740775;
// System.Object[]
struct ObjectU5BU5D_t1108656482;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void Newtonsoft.Json.Utilities.ReflectionDelegateFactory::.ctor()
extern "C"  void ReflectionDelegateFactory__ctor_m1324390442 (ReflectionDelegateFactory_t1590616920 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Utilities.ReflectionDelegateFactory::ilo_FormatWith1(System.String,System.IFormatProvider,System.Object[])
extern "C"  String_t* ReflectionDelegateFactory_ilo_FormatWith1_m364977834 (Il2CppObject * __this /* static, unused */, String_t* ___format0, Il2CppObject * ___provider1, ObjectU5BU5D_t1108656482* ___args2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
