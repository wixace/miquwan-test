﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.InternalEmitParticleArguments
struct InternalEmitParticleArguments_t1613430166;
struct InternalEmitParticleArguments_t1613430166_marshaled_pinvoke;
struct InternalEmitParticleArguments_t1613430166_marshaled_com;

#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct InternalEmitParticleArguments_t1613430166;
struct InternalEmitParticleArguments_t1613430166_marshaled_pinvoke;

extern "C" void InternalEmitParticleArguments_t1613430166_marshal_pinvoke(const InternalEmitParticleArguments_t1613430166& unmarshaled, InternalEmitParticleArguments_t1613430166_marshaled_pinvoke& marshaled);
extern "C" void InternalEmitParticleArguments_t1613430166_marshal_pinvoke_back(const InternalEmitParticleArguments_t1613430166_marshaled_pinvoke& marshaled, InternalEmitParticleArguments_t1613430166& unmarshaled);
extern "C" void InternalEmitParticleArguments_t1613430166_marshal_pinvoke_cleanup(InternalEmitParticleArguments_t1613430166_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct InternalEmitParticleArguments_t1613430166;
struct InternalEmitParticleArguments_t1613430166_marshaled_com;

extern "C" void InternalEmitParticleArguments_t1613430166_marshal_com(const InternalEmitParticleArguments_t1613430166& unmarshaled, InternalEmitParticleArguments_t1613430166_marshaled_com& marshaled);
extern "C" void InternalEmitParticleArguments_t1613430166_marshal_com_back(const InternalEmitParticleArguments_t1613430166_marshaled_com& marshaled, InternalEmitParticleArguments_t1613430166& unmarshaled);
extern "C" void InternalEmitParticleArguments_t1613430166_marshal_com_cleanup(InternalEmitParticleArguments_t1613430166_marshaled_com& marshaled);
