﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.Poly2Tri.DelaunayTriangle
struct DelaunayTriangle_t2835103587;
// Pathfinding.Poly2Tri.TriangulationPoint
struct TriangulationPoint_t3810082933;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "Pathfinding_Poly2Tri_Pathfinding_Poly2Tri_Triangul3810082933.h"
#include "Pathfinding_Poly2Tri_Pathfinding_Poly2Tri_Delaunay2835103587.h"

// System.Void Pathfinding.Poly2Tri.DelaunayTriangle::.ctor(Pathfinding.Poly2Tri.TriangulationPoint,Pathfinding.Poly2Tri.TriangulationPoint,Pathfinding.Poly2Tri.TriangulationPoint)
extern "C"  void DelaunayTriangle__ctor_m1610534382 (DelaunayTriangle_t2835103587 * __this, TriangulationPoint_t3810082933 * ___p10, TriangulationPoint_t3810082933 * ___p21, TriangulationPoint_t3810082933 * ___p32, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.Poly2Tri.DelaunayTriangle::get_IsInterior()
extern "C"  bool DelaunayTriangle_get_IsInterior_m1627348341 (DelaunayTriangle_t2835103587 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Poly2Tri.DelaunayTriangle::set_IsInterior(System.Boolean)
extern "C"  void DelaunayTriangle_set_IsInterior_m1507960310 (DelaunayTriangle_t2835103587 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pathfinding.Poly2Tri.DelaunayTriangle::IndexOf(Pathfinding.Poly2Tri.TriangulationPoint)
extern "C"  int32_t DelaunayTriangle_IndexOf_m1914770527 (DelaunayTriangle_t2835103587 * __this, TriangulationPoint_t3810082933 * ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pathfinding.Poly2Tri.DelaunayTriangle::IndexCCWFrom(Pathfinding.Poly2Tri.TriangulationPoint)
extern "C"  int32_t DelaunayTriangle_IndexCCWFrom_m3252439787 (DelaunayTriangle_t2835103587 * __this, TriangulationPoint_t3810082933 * ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.Poly2Tri.DelaunayTriangle::Contains(Pathfinding.Poly2Tri.TriangulationPoint)
extern "C"  bool DelaunayTriangle_Contains_m3545485537 (DelaunayTriangle_t2835103587 * __this, TriangulationPoint_t3810082933 * ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Poly2Tri.DelaunayTriangle::MarkNeighbor(Pathfinding.Poly2Tri.TriangulationPoint,Pathfinding.Poly2Tri.TriangulationPoint,Pathfinding.Poly2Tri.DelaunayTriangle)
extern "C"  void DelaunayTriangle_MarkNeighbor_m2976269813 (DelaunayTriangle_t2835103587 * __this, TriangulationPoint_t3810082933 * ___p10, TriangulationPoint_t3810082933 * ___p21, DelaunayTriangle_t2835103587 * ___t2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Poly2Tri.DelaunayTriangle::MarkNeighbor(Pathfinding.Poly2Tri.DelaunayTriangle)
extern "C"  void DelaunayTriangle_MarkNeighbor_m119719133 (DelaunayTriangle_t2835103587 * __this, DelaunayTriangle_t2835103587 * ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Poly2Tri.TriangulationPoint Pathfinding.Poly2Tri.DelaunayTriangle::OppositePoint(Pathfinding.Poly2Tri.DelaunayTriangle,Pathfinding.Poly2Tri.TriangulationPoint)
extern "C"  TriangulationPoint_t3810082933 * DelaunayTriangle_OppositePoint_m3734492474 (DelaunayTriangle_t2835103587 * __this, DelaunayTriangle_t2835103587 * ___t0, TriangulationPoint_t3810082933 * ___p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Poly2Tri.DelaunayTriangle Pathfinding.Poly2Tri.DelaunayTriangle::NeighborCWFrom(Pathfinding.Poly2Tri.TriangulationPoint)
extern "C"  DelaunayTriangle_t2835103587 * DelaunayTriangle_NeighborCWFrom_m3856958495 (DelaunayTriangle_t2835103587 * __this, TriangulationPoint_t3810082933 * ___point0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Poly2Tri.DelaunayTriangle Pathfinding.Poly2Tri.DelaunayTriangle::NeighborCCWFrom(Pathfinding.Poly2Tri.TriangulationPoint)
extern "C"  DelaunayTriangle_t2835103587 * DelaunayTriangle_NeighborCCWFrom_m3747172644 (DelaunayTriangle_t2835103587 * __this, TriangulationPoint_t3810082933 * ___point0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Poly2Tri.DelaunayTriangle Pathfinding.Poly2Tri.DelaunayTriangle::NeighborAcrossFrom(Pathfinding.Poly2Tri.TriangulationPoint)
extern "C"  DelaunayTriangle_t2835103587 * DelaunayTriangle_NeighborAcrossFrom_m3663882196 (DelaunayTriangle_t2835103587 * __this, TriangulationPoint_t3810082933 * ___point0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Poly2Tri.TriangulationPoint Pathfinding.Poly2Tri.DelaunayTriangle::PointCCWFrom(Pathfinding.Poly2Tri.TriangulationPoint)
extern "C"  TriangulationPoint_t3810082933 * DelaunayTriangle_PointCCWFrom_m667919724 (DelaunayTriangle_t2835103587 * __this, TriangulationPoint_t3810082933 * ___point0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Poly2Tri.TriangulationPoint Pathfinding.Poly2Tri.DelaunayTriangle::PointCWFrom(Pathfinding.Poly2Tri.TriangulationPoint)
extern "C"  TriangulationPoint_t3810082933 * DelaunayTriangle_PointCWFrom_m3064891095 (DelaunayTriangle_t2835103587 * __this, TriangulationPoint_t3810082933 * ___point0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Poly2Tri.DelaunayTriangle::RotateCW()
extern "C"  void DelaunayTriangle_RotateCW_m1049320471 (DelaunayTriangle_t2835103587 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Poly2Tri.DelaunayTriangle::Legalize(Pathfinding.Poly2Tri.TriangulationPoint,Pathfinding.Poly2Tri.TriangulationPoint)
extern "C"  void DelaunayTriangle_Legalize_m2367693307 (DelaunayTriangle_t2835103587 * __this, TriangulationPoint_t3810082933 * ___oPoint0, TriangulationPoint_t3810082933 * ___nPoint1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Pathfinding.Poly2Tri.DelaunayTriangle::ToString()
extern "C"  String_t* DelaunayTriangle_ToString_m3784777777 (DelaunayTriangle_t2835103587 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Poly2Tri.DelaunayTriangle::MarkConstrainedEdge(System.Int32)
extern "C"  void DelaunayTriangle_MarkConstrainedEdge_m3767794513 (DelaunayTriangle_t2835103587 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Poly2Tri.DelaunayTriangle::MarkConstrainedEdge(Pathfinding.Poly2Tri.TriangulationPoint,Pathfinding.Poly2Tri.TriangulationPoint)
extern "C"  void DelaunayTriangle_MarkConstrainedEdge_m3313274974 (DelaunayTriangle_t2835103587 * __this, TriangulationPoint_t3810082933 * ___p0, TriangulationPoint_t3810082933 * ___q1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pathfinding.Poly2Tri.DelaunayTriangle::EdgeIndex(Pathfinding.Poly2Tri.TriangulationPoint,Pathfinding.Poly2Tri.TriangulationPoint)
extern "C"  int32_t DelaunayTriangle_EdgeIndex_m78242015 (DelaunayTriangle_t2835103587 * __this, TriangulationPoint_t3810082933 * ___p10, TriangulationPoint_t3810082933 * ___p21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.Poly2Tri.DelaunayTriangle::GetConstrainedEdgeCCW(Pathfinding.Poly2Tri.TriangulationPoint)
extern "C"  bool DelaunayTriangle_GetConstrainedEdgeCCW_m1246493448 (DelaunayTriangle_t2835103587 * __this, TriangulationPoint_t3810082933 * ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.Poly2Tri.DelaunayTriangle::GetConstrainedEdgeCW(Pathfinding.Poly2Tri.TriangulationPoint)
extern "C"  bool DelaunayTriangle_GetConstrainedEdgeCW_m1169250959 (DelaunayTriangle_t2835103587 * __this, TriangulationPoint_t3810082933 * ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Poly2Tri.DelaunayTriangle::SetConstrainedEdgeCCW(Pathfinding.Poly2Tri.TriangulationPoint,System.Boolean)
extern "C"  void DelaunayTriangle_SetConstrainedEdgeCCW_m3838082667 (DelaunayTriangle_t2835103587 * __this, TriangulationPoint_t3810082933 * ___p0, bool ___ce1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Poly2Tri.DelaunayTriangle::SetConstrainedEdgeCW(Pathfinding.Poly2Tri.TriangulationPoint,System.Boolean)
extern "C"  void DelaunayTriangle_SetConstrainedEdgeCW_m3217709912 (DelaunayTriangle_t2835103587 * __this, TriangulationPoint_t3810082933 * ___p0, bool ___ce1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.Poly2Tri.DelaunayTriangle::GetDelaunayEdgeCCW(Pathfinding.Poly2Tri.TriangulationPoint)
extern "C"  bool DelaunayTriangle_GetDelaunayEdgeCCW_m1588591715 (DelaunayTriangle_t2835103587 * __this, TriangulationPoint_t3810082933 * ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.Poly2Tri.DelaunayTriangle::GetDelaunayEdgeCW(Pathfinding.Poly2Tri.TriangulationPoint)
extern "C"  bool DelaunayTriangle_GetDelaunayEdgeCW_m2288665044 (DelaunayTriangle_t2835103587 * __this, TriangulationPoint_t3810082933 * ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Poly2Tri.DelaunayTriangle::SetDelaunayEdgeCCW(Pathfinding.Poly2Tri.TriangulationPoint,System.Boolean)
extern "C"  void DelaunayTriangle_SetDelaunayEdgeCCW_m1549623684 (DelaunayTriangle_t2835103587 * __this, TriangulationPoint_t3810082933 * ___p0, bool ___ce1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Poly2Tri.DelaunayTriangle::SetDelaunayEdgeCW(Pathfinding.Poly2Tri.TriangulationPoint,System.Boolean)
extern "C"  void DelaunayTriangle_SetDelaunayEdgeCW_m3698077983 (DelaunayTriangle_t2835103587 * __this, TriangulationPoint_t3810082933 * ___p0, bool ___ce1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
