﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._9fa0d3aba4ec4a380a50209ee71e7919
struct _9fa0d3aba4ec4a380a50209ee71e7919_t2230771598;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__9fa0d3aba4ec4a380a50209e2230771598.h"

// System.Void Little._9fa0d3aba4ec4a380a50209ee71e7919::.ctor()
extern "C"  void _9fa0d3aba4ec4a380a50209ee71e7919__ctor_m3855869535 (_9fa0d3aba4ec4a380a50209ee71e7919_t2230771598 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._9fa0d3aba4ec4a380a50209ee71e7919::_9fa0d3aba4ec4a380a50209ee71e7919m2(System.Int32)
extern "C"  int32_t _9fa0d3aba4ec4a380a50209ee71e7919__9fa0d3aba4ec4a380a50209ee71e7919m2_m2818143961 (_9fa0d3aba4ec4a380a50209ee71e7919_t2230771598 * __this, int32_t ____9fa0d3aba4ec4a380a50209ee71e7919a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._9fa0d3aba4ec4a380a50209ee71e7919::_9fa0d3aba4ec4a380a50209ee71e7919m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _9fa0d3aba4ec4a380a50209ee71e7919__9fa0d3aba4ec4a380a50209ee71e7919m_m330012669 (_9fa0d3aba4ec4a380a50209ee71e7919_t2230771598 * __this, int32_t ____9fa0d3aba4ec4a380a50209ee71e7919a0, int32_t ____9fa0d3aba4ec4a380a50209ee71e791941, int32_t ____9fa0d3aba4ec4a380a50209ee71e7919c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._9fa0d3aba4ec4a380a50209ee71e7919::ilo__9fa0d3aba4ec4a380a50209ee71e7919m21(Little._9fa0d3aba4ec4a380a50209ee71e7919,System.Int32)
extern "C"  int32_t _9fa0d3aba4ec4a380a50209ee71e7919_ilo__9fa0d3aba4ec4a380a50209ee71e7919m21_m3810599189 (Il2CppObject * __this /* static, unused */, _9fa0d3aba4ec4a380a50209ee71e7919_t2230771598 * ____this0, int32_t ____9fa0d3aba4ec4a380a50209ee71e7919a1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
