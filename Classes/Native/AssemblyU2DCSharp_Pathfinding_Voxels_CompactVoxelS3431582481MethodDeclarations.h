﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.Voxels.CompactVoxelSpan
struct CompactVoxelSpan_t3431582481;
struct CompactVoxelSpan_t3431582481_marshaled_pinvoke;
struct CompactVoxelSpan_t3431582481_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_Voxels_CompactVoxelS3431582481.h"

// System.Void Pathfinding.Voxels.CompactVoxelSpan::.ctor(System.UInt16,System.UInt32)
extern "C"  void CompactVoxelSpan__ctor_m146688833 (CompactVoxelSpan_t3431582481 * __this, uint16_t ___bottom0, uint32_t ___height1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Voxels.CompactVoxelSpan::SetConnection(System.Int32,System.UInt32)
extern "C"  void CompactVoxelSpan_SetConnection_m3876106506 (CompactVoxelSpan_t3431582481 * __this, int32_t ___dir0, uint32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pathfinding.Voxels.CompactVoxelSpan::GetConnection(System.Int32)
extern "C"  int32_t CompactVoxelSpan_GetConnection_m4043157080 (CompactVoxelSpan_t3431582481 * __this, int32_t ___dir0, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct CompactVoxelSpan_t3431582481;
struct CompactVoxelSpan_t3431582481_marshaled_pinvoke;

extern "C" void CompactVoxelSpan_t3431582481_marshal_pinvoke(const CompactVoxelSpan_t3431582481& unmarshaled, CompactVoxelSpan_t3431582481_marshaled_pinvoke& marshaled);
extern "C" void CompactVoxelSpan_t3431582481_marshal_pinvoke_back(const CompactVoxelSpan_t3431582481_marshaled_pinvoke& marshaled, CompactVoxelSpan_t3431582481& unmarshaled);
extern "C" void CompactVoxelSpan_t3431582481_marshal_pinvoke_cleanup(CompactVoxelSpan_t3431582481_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct CompactVoxelSpan_t3431582481;
struct CompactVoxelSpan_t3431582481_marshaled_com;

extern "C" void CompactVoxelSpan_t3431582481_marshal_com(const CompactVoxelSpan_t3431582481& unmarshaled, CompactVoxelSpan_t3431582481_marshaled_com& marshaled);
extern "C" void CompactVoxelSpan_t3431582481_marshal_com_back(const CompactVoxelSpan_t3431582481_marshaled_com& marshaled, CompactVoxelSpan_t3431582481& unmarshaled);
extern "C" void CompactVoxelSpan_t3431582481_marshal_com_cleanup(CompactVoxelSpan_t3431582481_marshaled_com& marshaled);
