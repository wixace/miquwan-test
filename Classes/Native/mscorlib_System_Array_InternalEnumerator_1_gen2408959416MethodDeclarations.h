﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2408959416.h"
#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DCSharp_SoundTypeID3626616740.h"

// System.Void System.Array/InternalEnumerator`1<SoundTypeID>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2569512617_gshared (InternalEnumerator_1_t2408959416 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m2569512617(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t2408959416 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m2569512617_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<SoundTypeID>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1990153303_gshared (InternalEnumerator_1_t2408959416 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1990153303(__this, method) ((  void (*) (InternalEnumerator_1_t2408959416 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1990153303_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<SoundTypeID>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2348289037_gshared (InternalEnumerator_1_t2408959416 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2348289037(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t2408959416 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2348289037_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<SoundTypeID>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1084014528_gshared (InternalEnumerator_1_t2408959416 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m1084014528(__this, method) ((  void (*) (InternalEnumerator_1_t2408959416 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1084014528_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<SoundTypeID>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m4184443527_gshared (InternalEnumerator_1_t2408959416 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m4184443527(__this, method) ((  bool (*) (InternalEnumerator_1_t2408959416 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m4184443527_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<SoundTypeID>::get_Current()
extern "C"  int32_t InternalEnumerator_1_get_Current_m1767486610_gshared (InternalEnumerator_1_t2408959416 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1767486610(__this, method) ((  int32_t (*) (InternalEnumerator_1_t2408959416 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1767486610_gshared)(__this, method)
