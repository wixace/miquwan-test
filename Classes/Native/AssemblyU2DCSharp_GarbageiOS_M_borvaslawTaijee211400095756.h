﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_borvaslawTaijee21
struct  M_borvaslawTaijee21_t1400095756  : public Il2CppObject
{
public:
	// System.Single GarbageiOS.M_borvaslawTaijee21::_diday
	float ____diday_0;
	// System.Boolean GarbageiOS.M_borvaslawTaijee21::_chacenaGouxa
	bool ____chacenaGouxa_1;
	// System.String GarbageiOS.M_borvaslawTaijee21::_meaxifea
	String_t* ____meaxifea_2;
	// System.Single GarbageiOS.M_borvaslawTaijee21::_mocaSisre
	float ____mocaSisre_3;
	// System.Single GarbageiOS.M_borvaslawTaijee21::_readreereTairmor
	float ____readreereTairmor_4;
	// System.UInt32 GarbageiOS.M_borvaslawTaijee21::_jowjehayGidoki
	uint32_t ____jowjehayGidoki_5;
	// System.Single GarbageiOS.M_borvaslawTaijee21::_romeka
	float ____romeka_6;
	// System.String GarbageiOS.M_borvaslawTaijee21::_kairrowtall
	String_t* ____kairrowtall_7;
	// System.UInt32 GarbageiOS.M_borvaslawTaijee21::_mubaikoMonouhur
	uint32_t ____mubaikoMonouhur_8;
	// System.Single GarbageiOS.M_borvaslawTaijee21::_vateremi
	float ____vateremi_9;

public:
	inline static int32_t get_offset_of__diday_0() { return static_cast<int32_t>(offsetof(M_borvaslawTaijee21_t1400095756, ____diday_0)); }
	inline float get__diday_0() const { return ____diday_0; }
	inline float* get_address_of__diday_0() { return &____diday_0; }
	inline void set__diday_0(float value)
	{
		____diday_0 = value;
	}

	inline static int32_t get_offset_of__chacenaGouxa_1() { return static_cast<int32_t>(offsetof(M_borvaslawTaijee21_t1400095756, ____chacenaGouxa_1)); }
	inline bool get__chacenaGouxa_1() const { return ____chacenaGouxa_1; }
	inline bool* get_address_of__chacenaGouxa_1() { return &____chacenaGouxa_1; }
	inline void set__chacenaGouxa_1(bool value)
	{
		____chacenaGouxa_1 = value;
	}

	inline static int32_t get_offset_of__meaxifea_2() { return static_cast<int32_t>(offsetof(M_borvaslawTaijee21_t1400095756, ____meaxifea_2)); }
	inline String_t* get__meaxifea_2() const { return ____meaxifea_2; }
	inline String_t** get_address_of__meaxifea_2() { return &____meaxifea_2; }
	inline void set__meaxifea_2(String_t* value)
	{
		____meaxifea_2 = value;
		Il2CppCodeGenWriteBarrier(&____meaxifea_2, value);
	}

	inline static int32_t get_offset_of__mocaSisre_3() { return static_cast<int32_t>(offsetof(M_borvaslawTaijee21_t1400095756, ____mocaSisre_3)); }
	inline float get__mocaSisre_3() const { return ____mocaSisre_3; }
	inline float* get_address_of__mocaSisre_3() { return &____mocaSisre_3; }
	inline void set__mocaSisre_3(float value)
	{
		____mocaSisre_3 = value;
	}

	inline static int32_t get_offset_of__readreereTairmor_4() { return static_cast<int32_t>(offsetof(M_borvaslawTaijee21_t1400095756, ____readreereTairmor_4)); }
	inline float get__readreereTairmor_4() const { return ____readreereTairmor_4; }
	inline float* get_address_of__readreereTairmor_4() { return &____readreereTairmor_4; }
	inline void set__readreereTairmor_4(float value)
	{
		____readreereTairmor_4 = value;
	}

	inline static int32_t get_offset_of__jowjehayGidoki_5() { return static_cast<int32_t>(offsetof(M_borvaslawTaijee21_t1400095756, ____jowjehayGidoki_5)); }
	inline uint32_t get__jowjehayGidoki_5() const { return ____jowjehayGidoki_5; }
	inline uint32_t* get_address_of__jowjehayGidoki_5() { return &____jowjehayGidoki_5; }
	inline void set__jowjehayGidoki_5(uint32_t value)
	{
		____jowjehayGidoki_5 = value;
	}

	inline static int32_t get_offset_of__romeka_6() { return static_cast<int32_t>(offsetof(M_borvaslawTaijee21_t1400095756, ____romeka_6)); }
	inline float get__romeka_6() const { return ____romeka_6; }
	inline float* get_address_of__romeka_6() { return &____romeka_6; }
	inline void set__romeka_6(float value)
	{
		____romeka_6 = value;
	}

	inline static int32_t get_offset_of__kairrowtall_7() { return static_cast<int32_t>(offsetof(M_borvaslawTaijee21_t1400095756, ____kairrowtall_7)); }
	inline String_t* get__kairrowtall_7() const { return ____kairrowtall_7; }
	inline String_t** get_address_of__kairrowtall_7() { return &____kairrowtall_7; }
	inline void set__kairrowtall_7(String_t* value)
	{
		____kairrowtall_7 = value;
		Il2CppCodeGenWriteBarrier(&____kairrowtall_7, value);
	}

	inline static int32_t get_offset_of__mubaikoMonouhur_8() { return static_cast<int32_t>(offsetof(M_borvaslawTaijee21_t1400095756, ____mubaikoMonouhur_8)); }
	inline uint32_t get__mubaikoMonouhur_8() const { return ____mubaikoMonouhur_8; }
	inline uint32_t* get_address_of__mubaikoMonouhur_8() { return &____mubaikoMonouhur_8; }
	inline void set__mubaikoMonouhur_8(uint32_t value)
	{
		____mubaikoMonouhur_8 = value;
	}

	inline static int32_t get_offset_of__vateremi_9() { return static_cast<int32_t>(offsetof(M_borvaslawTaijee21_t1400095756, ____vateremi_9)); }
	inline float get__vateremi_9() const { return ____vateremi_9; }
	inline float* get_address_of__vateremi_9() { return &____vateremi_9; }
	inline void set__vateremi_9(float value)
	{
		____vateremi_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
