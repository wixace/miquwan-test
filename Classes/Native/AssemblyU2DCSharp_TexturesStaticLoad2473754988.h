﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Texture[]
struct TextureU5BU5D_t606176076;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TexturesStaticLoad
struct  TexturesStaticLoad_t2473754988  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.Texture[] TexturesStaticLoad::textureArr
	TextureU5BU5D_t606176076* ___textureArr_2;
	// System.Boolean TexturesStaticLoad::isInit
	bool ___isInit_3;

public:
	inline static int32_t get_offset_of_textureArr_2() { return static_cast<int32_t>(offsetof(TexturesStaticLoad_t2473754988, ___textureArr_2)); }
	inline TextureU5BU5D_t606176076* get_textureArr_2() const { return ___textureArr_2; }
	inline TextureU5BU5D_t606176076** get_address_of_textureArr_2() { return &___textureArr_2; }
	inline void set_textureArr_2(TextureU5BU5D_t606176076* value)
	{
		___textureArr_2 = value;
		Il2CppCodeGenWriteBarrier(&___textureArr_2, value);
	}

	inline static int32_t get_offset_of_isInit_3() { return static_cast<int32_t>(offsetof(TexturesStaticLoad_t2473754988, ___isInit_3)); }
	inline bool get_isInit_3() const { return ___isInit_3; }
	inline bool* get_address_of_isInit_3() { return &___isInit_3; }
	inline void set_isInit_3(bool value)
	{
		___isInit_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
