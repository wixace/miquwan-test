﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Reflection.Emit.ILGenerator
struct ILGenerator_t1499877190;
// System.Type
struct Type_t;
// System.Reflection.MethodInfo
struct MethodInfo_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_Emit_ILGenerator1499877190.h"
#include "mscorlib_System_Type2863145774.h"
#include "mscorlib_System_Reflection_MethodInfo318736065.h"

// System.Void Newtonsoft.Json.Utilities.ILGeneratorExtensions::PushInstance(System.Reflection.Emit.ILGenerator,System.Type)
extern "C"  void ILGeneratorExtensions_PushInstance_m376087245 (Il2CppObject * __this /* static, unused */, ILGenerator_t1499877190 * ___generator0, Type_t * ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Utilities.ILGeneratorExtensions::BoxIfNeeded(System.Reflection.Emit.ILGenerator,System.Type)
extern "C"  void ILGeneratorExtensions_BoxIfNeeded_m3029022773 (Il2CppObject * __this /* static, unused */, ILGenerator_t1499877190 * ___generator0, Type_t * ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Utilities.ILGeneratorExtensions::UnboxIfNeeded(System.Reflection.Emit.ILGenerator,System.Type)
extern "C"  void ILGeneratorExtensions_UnboxIfNeeded_m2693780156 (Il2CppObject * __this /* static, unused */, ILGenerator_t1499877190 * ___generator0, Type_t * ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Utilities.ILGeneratorExtensions::CallMethod(System.Reflection.Emit.ILGenerator,System.Reflection.MethodInfo)
extern "C"  void ILGeneratorExtensions_CallMethod_m1181791945 (Il2CppObject * __this /* static, unused */, ILGenerator_t1499877190 * ___generator0, MethodInfo_t * ___methodInfo1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Utilities.ILGeneratorExtensions::Return(System.Reflection.Emit.ILGenerator)
extern "C"  void ILGeneratorExtensions_Return_m1166568379 (Il2CppObject * __this /* static, unused */, ILGenerator_t1499877190 * ___generator0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
