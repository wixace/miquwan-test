﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.NNConstraint
struct  NNConstraint_t758567699  : public Il2CppObject
{
public:
	// System.Int32 Pathfinding.NNConstraint::graphMask
	int32_t ___graphMask_0;
	// System.Boolean Pathfinding.NNConstraint::constrainArea
	bool ___constrainArea_1;
	// System.Int32 Pathfinding.NNConstraint::area
	int32_t ___area_2;
	// System.Boolean Pathfinding.NNConstraint::constrainWalkability
	bool ___constrainWalkability_3;
	// System.Boolean Pathfinding.NNConstraint::walkable
	bool ___walkable_4;
	// System.Boolean Pathfinding.NNConstraint::distanceXZ
	bool ___distanceXZ_5;
	// System.Boolean Pathfinding.NNConstraint::constrainTags
	bool ___constrainTags_6;
	// System.Int32 Pathfinding.NNConstraint::tags
	int32_t ___tags_7;
	// System.Boolean Pathfinding.NNConstraint::constrainDistance
	bool ___constrainDistance_8;

public:
	inline static int32_t get_offset_of_graphMask_0() { return static_cast<int32_t>(offsetof(NNConstraint_t758567699, ___graphMask_0)); }
	inline int32_t get_graphMask_0() const { return ___graphMask_0; }
	inline int32_t* get_address_of_graphMask_0() { return &___graphMask_0; }
	inline void set_graphMask_0(int32_t value)
	{
		___graphMask_0 = value;
	}

	inline static int32_t get_offset_of_constrainArea_1() { return static_cast<int32_t>(offsetof(NNConstraint_t758567699, ___constrainArea_1)); }
	inline bool get_constrainArea_1() const { return ___constrainArea_1; }
	inline bool* get_address_of_constrainArea_1() { return &___constrainArea_1; }
	inline void set_constrainArea_1(bool value)
	{
		___constrainArea_1 = value;
	}

	inline static int32_t get_offset_of_area_2() { return static_cast<int32_t>(offsetof(NNConstraint_t758567699, ___area_2)); }
	inline int32_t get_area_2() const { return ___area_2; }
	inline int32_t* get_address_of_area_2() { return &___area_2; }
	inline void set_area_2(int32_t value)
	{
		___area_2 = value;
	}

	inline static int32_t get_offset_of_constrainWalkability_3() { return static_cast<int32_t>(offsetof(NNConstraint_t758567699, ___constrainWalkability_3)); }
	inline bool get_constrainWalkability_3() const { return ___constrainWalkability_3; }
	inline bool* get_address_of_constrainWalkability_3() { return &___constrainWalkability_3; }
	inline void set_constrainWalkability_3(bool value)
	{
		___constrainWalkability_3 = value;
	}

	inline static int32_t get_offset_of_walkable_4() { return static_cast<int32_t>(offsetof(NNConstraint_t758567699, ___walkable_4)); }
	inline bool get_walkable_4() const { return ___walkable_4; }
	inline bool* get_address_of_walkable_4() { return &___walkable_4; }
	inline void set_walkable_4(bool value)
	{
		___walkable_4 = value;
	}

	inline static int32_t get_offset_of_distanceXZ_5() { return static_cast<int32_t>(offsetof(NNConstraint_t758567699, ___distanceXZ_5)); }
	inline bool get_distanceXZ_5() const { return ___distanceXZ_5; }
	inline bool* get_address_of_distanceXZ_5() { return &___distanceXZ_5; }
	inline void set_distanceXZ_5(bool value)
	{
		___distanceXZ_5 = value;
	}

	inline static int32_t get_offset_of_constrainTags_6() { return static_cast<int32_t>(offsetof(NNConstraint_t758567699, ___constrainTags_6)); }
	inline bool get_constrainTags_6() const { return ___constrainTags_6; }
	inline bool* get_address_of_constrainTags_6() { return &___constrainTags_6; }
	inline void set_constrainTags_6(bool value)
	{
		___constrainTags_6 = value;
	}

	inline static int32_t get_offset_of_tags_7() { return static_cast<int32_t>(offsetof(NNConstraint_t758567699, ___tags_7)); }
	inline int32_t get_tags_7() const { return ___tags_7; }
	inline int32_t* get_address_of_tags_7() { return &___tags_7; }
	inline void set_tags_7(int32_t value)
	{
		___tags_7 = value;
	}

	inline static int32_t get_offset_of_constrainDistance_8() { return static_cast<int32_t>(offsetof(NNConstraint_t758567699, ___constrainDistance_8)); }
	inline bool get_constrainDistance_8() const { return ___constrainDistance_8; }
	inline bool* get_address_of_constrainDistance_8() { return &___constrainDistance_8; }
	inline void set_constrainDistance_8(bool value)
	{
		___constrainDistance_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
