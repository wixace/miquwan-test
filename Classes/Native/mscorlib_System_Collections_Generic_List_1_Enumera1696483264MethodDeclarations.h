﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1263707397MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<CameraShotGameSpeedCfg>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m3352900033(__this, ___l0, method) ((  void (*) (Enumerator_t1696483264 *, List_1_t1676810494 *, const MethodInfo*))Enumerator__ctor_m1029849669_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<CameraShotGameSpeedCfg>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m194776561(__this, method) ((  void (*) (Enumerator_t1696483264 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m771996397_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<CameraShotGameSpeedCfg>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1025135719(__this, method) ((  Il2CppObject * (*) (Enumerator_t1696483264 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<CameraShotGameSpeedCfg>::Dispose()
#define Enumerator_Dispose_m58294246(__this, method) ((  void (*) (Enumerator_t1696483264 *, const MethodInfo*))Enumerator_Dispose_m2904289642_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<CameraShotGameSpeedCfg>::VerifyState()
#define Enumerator_VerifyState_m310303775(__this, method) ((  void (*) (Enumerator_t1696483264 *, const MethodInfo*))Enumerator_VerifyState_m1522854819_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<CameraShotGameSpeedCfg>::MoveNext()
#define Enumerator_MoveNext_m2392498593(__this, method) ((  bool (*) (Enumerator_t1696483264 *, const MethodInfo*))Enumerator_MoveNext_m4284703760_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<CameraShotGameSpeedCfg>::get_Current()
#define Enumerator_get_Current_m731484920(__this, method) ((  CameraShotGameSpeedCfg_t308624942 * (*) (Enumerator_t1696483264 *, const MethodInfo*))Enumerator_get_Current_m396252160_gshared)(__this, method)
