﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_ristoMallstowlall199
struct M_ristoMallstowlall199_t2093772344;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"

// System.Void GarbageiOS.M_ristoMallstowlall199::.ctor()
extern "C"  void M_ristoMallstowlall199__ctor_m1608899323 (M_ristoMallstowlall199_t2093772344 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_ristoMallstowlall199::M_sajuDrayeachor0(System.String[],System.Int32)
extern "C"  void M_ristoMallstowlall199_M_sajuDrayeachor0_m4002943467 (M_ristoMallstowlall199_t2093772344 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_ristoMallstowlall199::M_mirhorJatea1(System.String[],System.Int32)
extern "C"  void M_ristoMallstowlall199_M_mirhorJatea1_m3910262111 (M_ristoMallstowlall199_t2093772344 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_ristoMallstowlall199::M_beca2(System.String[],System.Int32)
extern "C"  void M_ristoMallstowlall199_M_beca2_m4229858223 (M_ristoMallstowlall199_t2093772344 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
