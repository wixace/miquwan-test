﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Converters.JavaScriptDateTimeConverter
struct JavaScriptDateTimeConverter_t81069962;
// Newtonsoft.Json.JsonWriter
struct JsonWriter_t972330355;
// System.Object
struct Il2CppObject;
// Newtonsoft.Json.JsonSerializer
struct JsonSerializer_t251850770;
// Newtonsoft.Json.JsonReader
struct JsonReader_t816925123;
// System.Type
struct Type_t;
// System.String
struct String_t;
// System.IFormatProvider
struct IFormatProvider_t192740775;
// System.Object[]
struct ObjectU5BU5D_t1108656482;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonWriter972330355.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonSerializer251850770.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonReader816925123.h"
#include "mscorlib_System_Type2863145774.h"
#include "mscorlib_System_DateTime4283661327.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonToken4173078175.h"
#include "mscorlib_System_String7231557.h"

// System.Void Newtonsoft.Json.Converters.JavaScriptDateTimeConverter::.ctor()
extern "C"  void JavaScriptDateTimeConverter__ctor_m3840933257 (JavaScriptDateTimeConverter_t81069962 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Converters.JavaScriptDateTimeConverter::WriteJson(Newtonsoft.Json.JsonWriter,System.Object,Newtonsoft.Json.JsonSerializer)
extern "C"  void JavaScriptDateTimeConverter_WriteJson_m3093094955 (JavaScriptDateTimeConverter_t81069962 * __this, JsonWriter_t972330355 * ___writer0, Il2CppObject * ___value1, JsonSerializer_t251850770 * ___serializer2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Converters.JavaScriptDateTimeConverter::ReadJson(Newtonsoft.Json.JsonReader,System.Type,System.Object,Newtonsoft.Json.JsonSerializer)
extern "C"  Il2CppObject * JavaScriptDateTimeConverter_ReadJson_m3197732168 (JavaScriptDateTimeConverter_t81069962 * __this, JsonReader_t816925123 * ___reader0, Type_t * ___objectType1, Il2CppObject * ___existingValue2, JsonSerializer_t251850770 * ___serializer3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 Newtonsoft.Json.Converters.JavaScriptDateTimeConverter::ilo_ConvertDateTimeToJavaScriptTicks1(System.DateTime)
extern "C"  int64_t JavaScriptDateTimeConverter_ilo_ConvertDateTimeToJavaScriptTicks1_m1849168770 (Il2CppObject * __this /* static, unused */, DateTime_t4283661327  ___dateTime0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Converters.JavaScriptDateTimeConverter::ilo_WriteValue2(Newtonsoft.Json.JsonWriter,System.Int64)
extern "C"  void JavaScriptDateTimeConverter_ilo_WriteValue2_m878241268 (Il2CppObject * __this /* static, unused */, JsonWriter_t972330355 * ____this0, int64_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Converters.JavaScriptDateTimeConverter::ilo_IsNullableType3(System.Type)
extern "C"  bool JavaScriptDateTimeConverter_ilo_IsNullableType3_m684394955 (Il2CppObject * __this /* static, unused */, Type_t * ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.JsonToken Newtonsoft.Json.Converters.JavaScriptDateTimeConverter::ilo_get_TokenType4(Newtonsoft.Json.JsonReader)
extern "C"  int32_t JavaScriptDateTimeConverter_ilo_get_TokenType4_m55176143 (Il2CppObject * __this /* static, unused */, JsonReader_t816925123 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Converters.JavaScriptDateTimeConverter::ilo_FormatWith5(System.String,System.IFormatProvider,System.Object[])
extern "C"  String_t* JavaScriptDateTimeConverter_ilo_FormatWith5_m1372253479 (Il2CppObject * __this /* static, unused */, String_t* ___format0, Il2CppObject * ___provider1, ObjectU5BU5D_t1108656482* ___args2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime Newtonsoft.Json.Converters.JavaScriptDateTimeConverter::ilo_ConvertJavaScriptTicksToDateTime6(System.Int64)
extern "C"  DateTime_t4283661327  JavaScriptDateTimeConverter_ilo_ConvertJavaScriptTicksToDateTime6_m622411159 (Il2CppObject * __this /* static, unused */, int64_t ___javaScriptTicks0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
