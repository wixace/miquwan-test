﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object[]
struct ObjectU5BU5D_t1108656482;
// FloatTextMgr
struct FloatTextMgr_t630384591;

#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_FLOAT_TEXT_ID2199345770.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FloatTextMgr/<FloatText>c__AnonStorey158
struct  U3CFloatTextU3Ec__AnonStorey158_t3834694782  : public Il2CppObject
{
public:
	// FLOAT_TEXT_ID FloatTextMgr/<FloatText>c__AnonStorey158::textId
	int32_t ___textId_0;
	// UnityEngine.Vector3 FloatTextMgr/<FloatText>c__AnonStorey158::offsetPos
	Vector3_t4282066566  ___offsetPos_1;
	// System.Object[] FloatTextMgr/<FloatText>c__AnonStorey158::args
	ObjectU5BU5D_t1108656482* ___args_2;
	// FloatTextMgr FloatTextMgr/<FloatText>c__AnonStorey158::<>f__this
	FloatTextMgr_t630384591 * ___U3CU3Ef__this_3;

public:
	inline static int32_t get_offset_of_textId_0() { return static_cast<int32_t>(offsetof(U3CFloatTextU3Ec__AnonStorey158_t3834694782, ___textId_0)); }
	inline int32_t get_textId_0() const { return ___textId_0; }
	inline int32_t* get_address_of_textId_0() { return &___textId_0; }
	inline void set_textId_0(int32_t value)
	{
		___textId_0 = value;
	}

	inline static int32_t get_offset_of_offsetPos_1() { return static_cast<int32_t>(offsetof(U3CFloatTextU3Ec__AnonStorey158_t3834694782, ___offsetPos_1)); }
	inline Vector3_t4282066566  get_offsetPos_1() const { return ___offsetPos_1; }
	inline Vector3_t4282066566 * get_address_of_offsetPos_1() { return &___offsetPos_1; }
	inline void set_offsetPos_1(Vector3_t4282066566  value)
	{
		___offsetPos_1 = value;
	}

	inline static int32_t get_offset_of_args_2() { return static_cast<int32_t>(offsetof(U3CFloatTextU3Ec__AnonStorey158_t3834694782, ___args_2)); }
	inline ObjectU5BU5D_t1108656482* get_args_2() const { return ___args_2; }
	inline ObjectU5BU5D_t1108656482** get_address_of_args_2() { return &___args_2; }
	inline void set_args_2(ObjectU5BU5D_t1108656482* value)
	{
		___args_2 = value;
		Il2CppCodeGenWriteBarrier(&___args_2, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_3() { return static_cast<int32_t>(offsetof(U3CFloatTextU3Ec__AnonStorey158_t3834694782, ___U3CU3Ef__this_3)); }
	inline FloatTextMgr_t630384591 * get_U3CU3Ef__this_3() const { return ___U3CU3Ef__this_3; }
	inline FloatTextMgr_t630384591 ** get_address_of_U3CU3Ef__this_3() { return &___U3CU3Ef__this_3; }
	inline void set_U3CU3Ef__this_3(FloatTextMgr_t630384591 * value)
	{
		___U3CU3Ef__this_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
