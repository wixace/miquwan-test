﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Pathfinding.LinkedLevelNode
struct LinkedLevelNode_t936048751;

#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_RaycastHit4003175726.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.LinkedLevelNode
struct  LinkedLevelNode_t936048751  : public Il2CppObject
{
public:
	// UnityEngine.Vector3 Pathfinding.LinkedLevelNode::position
	Vector3_t4282066566  ___position_0;
	// System.Boolean Pathfinding.LinkedLevelNode::walkable
	bool ___walkable_1;
	// UnityEngine.RaycastHit Pathfinding.LinkedLevelNode::hit
	RaycastHit_t4003175726  ___hit_2;
	// System.Single Pathfinding.LinkedLevelNode::height
	float ___height_3;
	// Pathfinding.LinkedLevelNode Pathfinding.LinkedLevelNode::next
	LinkedLevelNode_t936048751 * ___next_4;

public:
	inline static int32_t get_offset_of_position_0() { return static_cast<int32_t>(offsetof(LinkedLevelNode_t936048751, ___position_0)); }
	inline Vector3_t4282066566  get_position_0() const { return ___position_0; }
	inline Vector3_t4282066566 * get_address_of_position_0() { return &___position_0; }
	inline void set_position_0(Vector3_t4282066566  value)
	{
		___position_0 = value;
	}

	inline static int32_t get_offset_of_walkable_1() { return static_cast<int32_t>(offsetof(LinkedLevelNode_t936048751, ___walkable_1)); }
	inline bool get_walkable_1() const { return ___walkable_1; }
	inline bool* get_address_of_walkable_1() { return &___walkable_1; }
	inline void set_walkable_1(bool value)
	{
		___walkable_1 = value;
	}

	inline static int32_t get_offset_of_hit_2() { return static_cast<int32_t>(offsetof(LinkedLevelNode_t936048751, ___hit_2)); }
	inline RaycastHit_t4003175726  get_hit_2() const { return ___hit_2; }
	inline RaycastHit_t4003175726 * get_address_of_hit_2() { return &___hit_2; }
	inline void set_hit_2(RaycastHit_t4003175726  value)
	{
		___hit_2 = value;
	}

	inline static int32_t get_offset_of_height_3() { return static_cast<int32_t>(offsetof(LinkedLevelNode_t936048751, ___height_3)); }
	inline float get_height_3() const { return ___height_3; }
	inline float* get_address_of_height_3() { return &___height_3; }
	inline void set_height_3(float value)
	{
		___height_3 = value;
	}

	inline static int32_t get_offset_of_next_4() { return static_cast<int32_t>(offsetof(LinkedLevelNode_t936048751, ___next_4)); }
	inline LinkedLevelNode_t936048751 * get_next_4() const { return ___next_4; }
	inline LinkedLevelNode_t936048751 ** get_address_of_next_4() { return &___next_4; }
	inline void set_next_4(LinkedLevelNode_t936048751 * value)
	{
		___next_4 = value;
		Il2CppCodeGenWriteBarrier(&___next_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
