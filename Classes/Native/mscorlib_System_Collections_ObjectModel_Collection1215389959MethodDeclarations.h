﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<Pathfinding.LocalAvoidance/VOLine>
struct Collection_1_t1215389959;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// Pathfinding.LocalAvoidance/VOLine[]
struct VOLineU5BU5D_t1958725220;
// System.Collections.Generic.IEnumerator`1<Pathfinding.LocalAvoidance/VOLine>
struct IEnumerator_1_t3941796850;
// System.Collections.Generic.IList`1<Pathfinding.LocalAvoidance/VOLine>
struct IList_1_t429611708;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_Pathfinding_LocalAvoidance_VOLin2029931801.h"

// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.LocalAvoidance/VOLine>::.ctor()
extern "C"  void Collection_1__ctor_m4240920092_gshared (Collection_1_t1215389959 * __this, const MethodInfo* method);
#define Collection_1__ctor_m4240920092(__this, method) ((  void (*) (Collection_1_t1215389959 *, const MethodInfo*))Collection_1__ctor_m4240920092_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Pathfinding.LocalAvoidance/VOLine>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m556800351_gshared (Collection_1_t1215389959 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m556800351(__this, method) ((  bool (*) (Collection_1_t1215389959 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m556800351_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.LocalAvoidance/VOLine>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Collection_1_System_Collections_ICollection_CopyTo_m4016051880_gshared (Collection_1_t1215389959 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m4016051880(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t1215389959 *, Il2CppArray *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m4016051880_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<Pathfinding.LocalAvoidance/VOLine>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Collection_1_System_Collections_IEnumerable_GetEnumerator_m1891080995_gshared (Collection_1_t1215389959 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m1891080995(__this, method) ((  Il2CppObject * (*) (Collection_1_t1215389959 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m1891080995_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Pathfinding.LocalAvoidance/VOLine>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_Add_m1590179630_gshared (Collection_1_t1215389959 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m1590179630(__this, ___value0, method) ((  int32_t (*) (Collection_1_t1215389959 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m1590179630_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Pathfinding.LocalAvoidance/VOLine>::System.Collections.IList.Contains(System.Object)
extern "C"  bool Collection_1_System_Collections_IList_Contains_m289973982_gshared (Collection_1_t1215389959 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m289973982(__this, ___value0, method) ((  bool (*) (Collection_1_t1215389959 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m289973982_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Pathfinding.LocalAvoidance/VOLine>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_IndexOf_m1939678854_gshared (Collection_1_t1215389959 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m1939678854(__this, ___value0, method) ((  int32_t (*) (Collection_1_t1215389959 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m1939678854_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.LocalAvoidance/VOLine>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_Insert_m3049088177_gshared (Collection_1_t1215389959 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m3049088177(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t1215389959 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m3049088177_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.LocalAvoidance/VOLine>::System.Collections.IList.Remove(System.Object)
extern "C"  void Collection_1_System_Collections_IList_Remove_m2031843543_gshared (Collection_1_t1215389959 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m2031843543(__this, ___value0, method) ((  void (*) (Collection_1_t1215389959 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m2031843543_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Pathfinding.LocalAvoidance/VOLine>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m1430641598_gshared (Collection_1_t1215389959 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m1430641598(__this, method) ((  bool (*) (Collection_1_t1215389959 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m1430641598_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<Pathfinding.LocalAvoidance/VOLine>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Collection_1_System_Collections_ICollection_get_SyncRoot_m4250479466_gshared (Collection_1_t1215389959 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m4250479466(__this, method) ((  Il2CppObject * (*) (Collection_1_t1215389959 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m4250479466_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Pathfinding.LocalAvoidance/VOLine>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool Collection_1_System_Collections_IList_get_IsFixedSize_m3239466189_gshared (Collection_1_t1215389959 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m3239466189(__this, method) ((  bool (*) (Collection_1_t1215389959 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m3239466189_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Pathfinding.LocalAvoidance/VOLine>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_IList_get_IsReadOnly_m2082528396_gshared (Collection_1_t1215389959 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m2082528396(__this, method) ((  bool (*) (Collection_1_t1215389959 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m2082528396_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<Pathfinding.LocalAvoidance/VOLine>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * Collection_1_System_Collections_IList_get_Item_m1262479985_gshared (Collection_1_t1215389959 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m1262479985(__this, ___index0, method) ((  Il2CppObject * (*) (Collection_1_t1215389959 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m1262479985_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.LocalAvoidance/VOLine>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_set_Item_m1619994696_gshared (Collection_1_t1215389959 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m1619994696(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t1215389959 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m1619994696_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.LocalAvoidance/VOLine>::Add(T)
extern "C"  void Collection_1_Add_m1096777443_gshared (Collection_1_t1215389959 * __this, VOLine_t2029931801  ___item0, const MethodInfo* method);
#define Collection_1_Add_m1096777443(__this, ___item0, method) ((  void (*) (Collection_1_t1215389959 *, VOLine_t2029931801 , const MethodInfo*))Collection_1_Add_m1096777443_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.LocalAvoidance/VOLine>::Clear()
extern "C"  void Collection_1_Clear_m1647053383_gshared (Collection_1_t1215389959 * __this, const MethodInfo* method);
#define Collection_1_Clear_m1647053383(__this, method) ((  void (*) (Collection_1_t1215389959 *, const MethodInfo*))Collection_1_Clear_m1647053383_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.LocalAvoidance/VOLine>::ClearItems()
extern "C"  void Collection_1_ClearItems_m4200459035_gshared (Collection_1_t1215389959 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m4200459035(__this, method) ((  void (*) (Collection_1_t1215389959 *, const MethodInfo*))Collection_1_ClearItems_m4200459035_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Pathfinding.LocalAvoidance/VOLine>::Contains(T)
extern "C"  bool Collection_1_Contains_m1920584565_gshared (Collection_1_t1215389959 * __this, VOLine_t2029931801  ___item0, const MethodInfo* method);
#define Collection_1_Contains_m1920584565(__this, ___item0, method) ((  bool (*) (Collection_1_t1215389959 *, VOLine_t2029931801 , const MethodInfo*))Collection_1_Contains_m1920584565_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.LocalAvoidance/VOLine>::CopyTo(T[],System.Int32)
extern "C"  void Collection_1_CopyTo_m1324782611_gshared (Collection_1_t1215389959 * __this, VOLineU5BU5D_t1958725220* ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_CopyTo_m1324782611(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t1215389959 *, VOLineU5BU5D_t1958725220*, int32_t, const MethodInfo*))Collection_1_CopyTo_m1324782611_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<Pathfinding.LocalAvoidance/VOLine>::GetEnumerator()
extern "C"  Il2CppObject* Collection_1_GetEnumerator_m2376360152_gshared (Collection_1_t1215389959 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m2376360152(__this, method) ((  Il2CppObject* (*) (Collection_1_t1215389959 *, const MethodInfo*))Collection_1_GetEnumerator_m2376360152_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Pathfinding.LocalAvoidance/VOLine>::IndexOf(T)
extern "C"  int32_t Collection_1_IndexOf_m1647367639_gshared (Collection_1_t1215389959 * __this, VOLine_t2029931801  ___item0, const MethodInfo* method);
#define Collection_1_IndexOf_m1647367639(__this, ___item0, method) ((  int32_t (*) (Collection_1_t1215389959 *, VOLine_t2029931801 , const MethodInfo*))Collection_1_IndexOf_m1647367639_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.LocalAvoidance/VOLine>::Insert(System.Int32,T)
extern "C"  void Collection_1_Insert_m54563658_gshared (Collection_1_t1215389959 * __this, int32_t ___index0, VOLine_t2029931801  ___item1, const MethodInfo* method);
#define Collection_1_Insert_m54563658(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t1215389959 *, int32_t, VOLine_t2029931801 , const MethodInfo*))Collection_1_Insert_m54563658_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.LocalAvoidance/VOLine>::InsertItem(System.Int32,T)
extern "C"  void Collection_1_InsertItem_m3767078013_gshared (Collection_1_t1215389959 * __this, int32_t ___index0, VOLine_t2029931801  ___item1, const MethodInfo* method);
#define Collection_1_InsertItem_m3767078013(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t1215389959 *, int32_t, VOLine_t2029931801 , const MethodInfo*))Collection_1_InsertItem_m3767078013_gshared)(__this, ___index0, ___item1, method)
// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.Collection`1<Pathfinding.LocalAvoidance/VOLine>::get_Items()
extern "C"  Il2CppObject* Collection_1_get_Items_m1610512859_gshared (Collection_1_t1215389959 * __this, const MethodInfo* method);
#define Collection_1_get_Items_m1610512859(__this, method) ((  Il2CppObject* (*) (Collection_1_t1215389959 *, const MethodInfo*))Collection_1_get_Items_m1610512859_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Pathfinding.LocalAvoidance/VOLine>::Remove(T)
extern "C"  bool Collection_1_Remove_m2267342000_gshared (Collection_1_t1215389959 * __this, VOLine_t2029931801  ___item0, const MethodInfo* method);
#define Collection_1_Remove_m2267342000(__this, ___item0, method) ((  bool (*) (Collection_1_t1215389959 *, VOLine_t2029931801 , const MethodInfo*))Collection_1_Remove_m2267342000_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.LocalAvoidance/VOLine>::RemoveAt(System.Int32)
extern "C"  void Collection_1_RemoveAt_m2223383824_gshared (Collection_1_t1215389959 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveAt_m2223383824(__this, ___index0, method) ((  void (*) (Collection_1_t1215389959 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m2223383824_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.LocalAvoidance/VOLine>::RemoveItem(System.Int32)
extern "C"  void Collection_1_RemoveItem_m677766448_gshared (Collection_1_t1215389959 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveItem_m677766448(__this, ___index0, method) ((  void (*) (Collection_1_t1215389959 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m677766448_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Pathfinding.LocalAvoidance/VOLine>::get_Count()
extern "C"  int32_t Collection_1_get_Count_m3641064068_gshared (Collection_1_t1215389959 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m3641064068(__this, method) ((  int32_t (*) (Collection_1_t1215389959 *, const MethodInfo*))Collection_1_get_Count_m3641064068_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<Pathfinding.LocalAvoidance/VOLine>::get_Item(System.Int32)
extern "C"  VOLine_t2029931801  Collection_1_get_Item_m135960148_gshared (Collection_1_t1215389959 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_get_Item_m135960148(__this, ___index0, method) ((  VOLine_t2029931801  (*) (Collection_1_t1215389959 *, int32_t, const MethodInfo*))Collection_1_get_Item_m135960148_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.LocalAvoidance/VOLine>::set_Item(System.Int32,T)
extern "C"  void Collection_1_set_Item_m2705664417_gshared (Collection_1_t1215389959 * __this, int32_t ___index0, VOLine_t2029931801  ___value1, const MethodInfo* method);
#define Collection_1_set_Item_m2705664417(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t1215389959 *, int32_t, VOLine_t2029931801 , const MethodInfo*))Collection_1_set_Item_m2705664417_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.LocalAvoidance/VOLine>::SetItem(System.Int32,T)
extern "C"  void Collection_1_SetItem_m3008063160_gshared (Collection_1_t1215389959 * __this, int32_t ___index0, VOLine_t2029931801  ___item1, const MethodInfo* method);
#define Collection_1_SetItem_m3008063160(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t1215389959 *, int32_t, VOLine_t2029931801 , const MethodInfo*))Collection_1_SetItem_m3008063160_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Pathfinding.LocalAvoidance/VOLine>::IsValidItem(System.Object)
extern "C"  bool Collection_1_IsValidItem_m2722213399_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_IsValidItem_m2722213399(__this /* static, unused */, ___item0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_IsValidItem_m2722213399_gshared)(__this /* static, unused */, ___item0, method)
// T System.Collections.ObjectModel.Collection`1<Pathfinding.LocalAvoidance/VOLine>::ConvertItem(System.Object)
extern "C"  VOLine_t2029931801  Collection_1_ConvertItem_m79231795_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_ConvertItem_m79231795(__this /* static, unused */, ___item0, method) ((  VOLine_t2029931801  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_ConvertItem_m79231795_gshared)(__this /* static, unused */, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.LocalAvoidance/VOLine>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C"  void Collection_1_CheckWritable_m3197801299_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_CheckWritable_m3197801299(__this /* static, unused */, ___list0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_CheckWritable_m3197801299_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Pathfinding.LocalAvoidance/VOLine>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsSynchronized_m974126669_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsSynchronized_m974126669(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsSynchronized_m974126669_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Pathfinding.LocalAvoidance/VOLine>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsFixedSize_m3897996146_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsFixedSize_m3897996146(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsFixedSize_m3897996146_gshared)(__this /* static, unused */, ___list0, method)
