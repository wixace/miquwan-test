﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._062a0caa4415b99c05be83da849e837d
struct _062a0caa4415b99c05be83da849e837d_t505588726;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._062a0caa4415b99c05be83da849e837d::.ctor()
extern "C"  void _062a0caa4415b99c05be83da849e837d__ctor_m822316791 (_062a0caa4415b99c05be83da849e837d_t505588726 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._062a0caa4415b99c05be83da849e837d::_062a0caa4415b99c05be83da849e837dm2(System.Int32)
extern "C"  int32_t _062a0caa4415b99c05be83da849e837d__062a0caa4415b99c05be83da849e837dm2_m2634472921 (_062a0caa4415b99c05be83da849e837d_t505588726 * __this, int32_t ____062a0caa4415b99c05be83da849e837da0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._062a0caa4415b99c05be83da849e837d::_062a0caa4415b99c05be83da849e837dm(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _062a0caa4415b99c05be83da849e837d__062a0caa4415b99c05be83da849e837dm_m1739591933 (_062a0caa4415b99c05be83da849e837d_t505588726 * __this, int32_t ____062a0caa4415b99c05be83da849e837da0, int32_t ____062a0caa4415b99c05be83da849e837d11, int32_t ____062a0caa4415b99c05be83da849e837dc2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
