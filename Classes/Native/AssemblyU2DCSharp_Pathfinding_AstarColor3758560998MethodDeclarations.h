﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.AstarColor
struct AstarColor_t3758560998;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"

// System.Void Pathfinding.AstarColor::.ctor()
extern "C"  void AstarColor__ctor_m1709832257 (AstarColor_t3758560998 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.AstarColor::.cctor()
extern "C"  void AstarColor__cctor_m983096204 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color Pathfinding.AstarColor::GetAreaColor(System.UInt32)
extern "C"  Color_t4194546905  AstarColor_GetAreaColor_m2827445040 (Il2CppObject * __this /* static, unused */, uint32_t ___area0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.AstarColor::OnEnable()
extern "C"  void AstarColor_OnEnable_m1372216229 (AstarColor_t3758560998 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
