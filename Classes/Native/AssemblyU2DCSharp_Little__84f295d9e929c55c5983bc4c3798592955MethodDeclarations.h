﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._84f295d9e929c55c5983bc4cd9659d44
struct _84f295d9e929c55c5983bc4cd9659d44_t3798592955;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._84f295d9e929c55c5983bc4cd9659d44::.ctor()
extern "C"  void _84f295d9e929c55c5983bc4cd9659d44__ctor_m4007230162 (_84f295d9e929c55c5983bc4cd9659d44_t3798592955 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._84f295d9e929c55c5983bc4cd9659d44::_84f295d9e929c55c5983bc4cd9659d44m2(System.Int32)
extern "C"  int32_t _84f295d9e929c55c5983bc4cd9659d44__84f295d9e929c55c5983bc4cd9659d44m2_m4204650425 (_84f295d9e929c55c5983bc4cd9659d44_t3798592955 * __this, int32_t ____84f295d9e929c55c5983bc4cd9659d44a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._84f295d9e929c55c5983bc4cd9659d44::_84f295d9e929c55c5983bc4cd9659d44m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _84f295d9e929c55c5983bc4cd9659d44__84f295d9e929c55c5983bc4cd9659d44m_m695528221 (_84f295d9e929c55c5983bc4cd9659d44_t3798592955 * __this, int32_t ____84f295d9e929c55c5983bc4cd9659d44a0, int32_t ____84f295d9e929c55c5983bc4cd9659d4411, int32_t ____84f295d9e929c55c5983bc4cd9659d44c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
