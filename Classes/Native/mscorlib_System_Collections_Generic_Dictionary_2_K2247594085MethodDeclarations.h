﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/KeyCollection<PushType,System.Object>
struct KeyCollection_t2247594085;
// System.Collections.Generic.Dictionary`2<PushType,System.Object>
struct Dictionary_2_t620834634;
// System.Collections.Generic.IEnumerator`1<PushType>
struct IEnumerator_1_t3752507501;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// PushType[]
struct PushTypeU5BU5D_t3978570141;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PushType1840642452.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K1235770688.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<PushType,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void KeyCollection__ctor_m1606937486_gshared (KeyCollection_t2247594085 * __this, Dictionary_2_t620834634 * ___dictionary0, const MethodInfo* method);
#define KeyCollection__ctor_m1606937486(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t2247594085 *, Dictionary_2_t620834634 *, const MethodInfo*))KeyCollection__ctor_m1606937486_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<PushType,System.Object>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3112168712_gshared (KeyCollection_t2247594085 * __this, int32_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3112168712(__this, ___item0, method) ((  void (*) (KeyCollection_t2247594085 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3112168712_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<PushType,System.Object>::System.Collections.Generic.ICollection<TKey>.Clear()
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1567470719_gshared (KeyCollection_t2247594085 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1567470719(__this, method) ((  void (*) (KeyCollection_t2247594085 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1567470719_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<PushType,System.Object>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m680636102_gshared (KeyCollection_t2247594085 * __this, int32_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m680636102(__this, ___item0, method) ((  bool (*) (KeyCollection_t2247594085 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m680636102_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<PushType,System.Object>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1726421419_gshared (KeyCollection_t2247594085 * __this, int32_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1726421419(__this, ___item0, method) ((  bool (*) (KeyCollection_t2247594085 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1726421419_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<PushType,System.Object>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m2086956113_gshared (KeyCollection_t2247594085 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m2086956113(__this, method) ((  Il2CppObject* (*) (KeyCollection_t2247594085 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m2086956113_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<PushType,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m3588346481_gshared (KeyCollection_t2247594085 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_CopyTo_m3588346481(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t2247594085 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m3588346481_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<PushType,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1807839296_gshared (KeyCollection_t2247594085 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1807839296(__this, method) ((  Il2CppObject * (*) (KeyCollection_t2247594085 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1807839296_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<PushType,System.Object>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m4137323687_gshared (KeyCollection_t2247594085 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m4137323687(__this, method) ((  bool (*) (KeyCollection_t2247594085 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m4137323687_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<PushType,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m712895001_gshared (KeyCollection_t2247594085 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m712895001(__this, method) ((  bool (*) (KeyCollection_t2247594085 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m712895001_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<PushType,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m2112109963_gshared (KeyCollection_t2247594085 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m2112109963(__this, method) ((  Il2CppObject * (*) (KeyCollection_t2247594085 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m2112109963_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<PushType,System.Object>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m2136410691_gshared (KeyCollection_t2247594085 * __this, PushTypeU5BU5D_t3978570141* ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_CopyTo_m2136410691(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t2247594085 *, PushTypeU5BU5D_t3978570141*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m2136410691_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<PushType,System.Object>::GetEnumerator()
extern "C"  Enumerator_t1235770688  KeyCollection_GetEnumerator_m1549819792_gshared (KeyCollection_t2247594085 * __this, const MethodInfo* method);
#define KeyCollection_GetEnumerator_m1549819792(__this, method) ((  Enumerator_t1235770688  (*) (KeyCollection_t2247594085 *, const MethodInfo*))KeyCollection_GetEnumerator_m1549819792_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<PushType,System.Object>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m440118739_gshared (KeyCollection_t2247594085 * __this, const MethodInfo* method);
#define KeyCollection_get_Count_m440118739(__this, method) ((  int32_t (*) (KeyCollection_t2247594085 *, const MethodInfo*))KeyCollection_get_Count_m440118739_gshared)(__this, method)
