﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TweenWidth
struct TweenWidth_t2940542523;
// UIWidget
struct UIWidget_t769069560;
// UITweener
struct UITweener_t105489188;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_UIWidget769069560.h"
#include "AssemblyU2DCSharp_UITweener105489188.h"
#include "AssemblyU2DCSharp_TweenWidth2940542523.h"

// System.Void TweenWidth::.ctor()
extern "C"  void TweenWidth__ctor_m844451008 (TweenWidth_t2940542523 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIWidget TweenWidth::get_cachedWidget()
extern "C"  UIWidget_t769069560 * TweenWidth_get_cachedWidget_m3407009656 (TweenWidth_t2940542523 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 TweenWidth::get_width()
extern "C"  int32_t TweenWidth_get_width_m2404980237 (TweenWidth_t2940542523 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenWidth::set_width(System.Int32)
extern "C"  void TweenWidth_set_width_m937532344 (TweenWidth_t2940542523 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 TweenWidth::get_value()
extern "C"  int32_t TweenWidth_get_value_m1295858424 (TweenWidth_t2940542523 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenWidth::set_value(System.Int32)
extern "C"  void TweenWidth_set_value_m2666969123 (TweenWidth_t2940542523 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenWidth::OnUpdate(System.Single,System.Boolean)
extern "C"  void TweenWidth_OnUpdate_m1897199870 (TweenWidth_t2940542523 * __this, float ___factor0, bool ___isFinished1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TweenWidth TweenWidth::Begin(UIWidget,System.Single,System.Int32)
extern "C"  TweenWidth_t2940542523 * TweenWidth_Begin_m960642181 (Il2CppObject * __this /* static, unused */, UIWidget_t769069560 * ___widget0, float ___duration1, int32_t ___width2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenWidth::SetStartToCurrentValue()
extern "C"  void TweenWidth_SetStartToCurrentValue_m550397911 (TweenWidth_t2940542523 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenWidth::SetEndToCurrentValue()
extern "C"  void TweenWidth_SetEndToCurrentValue_m2740881040 (TweenWidth_t2940542523 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenWidth::SetCurrentValueToStart()
extern "C"  void TweenWidth_SetCurrentValueToStart_m4091068113 (TweenWidth_t2940542523 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenWidth::SetCurrentValueToEnd()
extern "C"  void TweenWidth_SetCurrentValueToEnd_m1452600458 (TweenWidth_t2940542523 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 TweenWidth::ilo_get_width1(UIWidget)
extern "C"  int32_t TweenWidth_ilo_get_width1_m1279013793 (Il2CppObject * __this /* static, unused */, UIWidget_t769069560 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenWidth::ilo_Sample2(UITweener,System.Single,System.Boolean)
extern "C"  void TweenWidth_ilo_Sample2_m2303732733 (Il2CppObject * __this /* static, unused */, UITweener_t105489188 * ____this0, float ___factor1, bool ___isFinished2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 TweenWidth::ilo_get_value3(TweenWidth)
extern "C"  int32_t TweenWidth_ilo_get_value3_m1147013877 (Il2CppObject * __this /* static, unused */, TweenWidth_t2940542523 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
