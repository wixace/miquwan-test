﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SevenZip.LzmaBench/CRandomGenerator
struct  CRandomGenerator_t1521078568  : public Il2CppObject
{
public:
	// System.UInt32 SevenZip.LzmaBench/CRandomGenerator::A1
	uint32_t ___A1_0;
	// System.UInt32 SevenZip.LzmaBench/CRandomGenerator::A2
	uint32_t ___A2_1;

public:
	inline static int32_t get_offset_of_A1_0() { return static_cast<int32_t>(offsetof(CRandomGenerator_t1521078568, ___A1_0)); }
	inline uint32_t get_A1_0() const { return ___A1_0; }
	inline uint32_t* get_address_of_A1_0() { return &___A1_0; }
	inline void set_A1_0(uint32_t value)
	{
		___A1_0 = value;
	}

	inline static int32_t get_offset_of_A2_1() { return static_cast<int32_t>(offsetof(CRandomGenerator_t1521078568, ___A2_1)); }
	inline uint32_t get_A2_1() const { return ___A2_1; }
	inline uint32_t* get_address_of_A2_1() { return &___A2_1; }
	inline void set_A2_1(uint32_t value)
	{
		___A2_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
