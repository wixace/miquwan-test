﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CEvent.ZEventCenter
struct ZEventCenter_t1058262265;
// System.String
struct String_t;
// CEvent.EventFunc`1<CEvent.ZEvent>
struct EventFunc_1_t1114914310;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void CEvent.ZEventCenter::.ctor()
extern "C"  void ZEventCenter__ctor_m1716378015 (ZEventCenter_t1058262265 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CEvent.ZEventCenter::.cctor()
extern "C"  void ZEventCenter__cctor_m1186014702 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CEvent.ZEventCenter::AddEventListener(System.String,CEvent.EventFunc`1<CEvent.ZEvent>)
extern "C"  void ZEventCenter_AddEventListener_m1738427985 (Il2CppObject * __this /* static, unused */, String_t* ___eventType0, EventFunc_1_t1114914310 * ___fun1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CEvent.ZEventCenter::AddEventListener(System.String,CEvent.EventFunc`1<CEvent.ZEvent>,System.Boolean)
extern "C"  void ZEventCenter_AddEventListener_m2225305868 (Il2CppObject * __this /* static, unused */, String_t* ___eventType0, EventFunc_1_t1114914310 * ___fun1, bool ___isInsert2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CEvent.ZEventCenter::DispatchEvent(CEvent.ZEvent)
extern "C"  void ZEventCenter_DispatchEvent_m474491550 (Il2CppObject * __this /* static, unused */, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CEvent.ZEventCenter::RemoveEventListener(System.String,CEvent.EventFunc`1<CEvent.ZEvent>)
extern "C"  void ZEventCenter_RemoveEventListener_m1396628476 (Il2CppObject * __this /* static, unused */, String_t* ___eventType0, EventFunc_1_t1114914310 * ___func1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CEvent.ZEventCenter::ilo_AddEventListener1(System.String,CEvent.EventFunc`1<CEvent.ZEvent>,System.Boolean)
extern "C"  void ZEventCenter_ilo_AddEventListener1_m2890955624 (Il2CppObject * __this /* static, unused */, String_t* ___eventType0, EventFunc_1_t1114914310 * ___fun1, bool ___isInsert2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CEvent.ZEventCenter::ilo_LogError2(System.Object,System.Boolean)
extern "C"  void ZEventCenter_ilo_LogError2_m3285469729 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
