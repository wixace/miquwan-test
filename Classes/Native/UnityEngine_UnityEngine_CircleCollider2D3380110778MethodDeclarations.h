﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.CircleCollider2D
struct CircleCollider2D_t3380110778;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.CircleCollider2D::.ctor()
extern "C"  void CircleCollider2D__ctor_m2219259383 (CircleCollider2D_t3380110778 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.CircleCollider2D::get_radius()
extern "C"  float CircleCollider2D_get_radius_m1584608820 (CircleCollider2D_t3380110778 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.CircleCollider2D::set_radius(System.Single)
extern "C"  void CircleCollider2D_set_radius_m1280410767 (CircleCollider2D_t3380110778 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
