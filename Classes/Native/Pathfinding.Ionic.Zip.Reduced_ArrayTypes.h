﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


// Pathfinding.Ionic.Zip.ZipEntry
struct ZipEntry_t2786874973;
// Pathfinding.Ionic.Zlib.DeflateManager/Config
struct Config_t4262987426;
// Pathfinding.Ionic.Zlib.WorkItem
struct WorkItem_t4048658636;

#include "mscorlib_System_Array1146569071.h"
#include "Pathfinding_Ionic_Zip_Reduced_Pathfinding_Ionic_Zi2786874973.h"
#include "Pathfinding_Ionic_Zip_Reduced_Pathfinding_Ionic_Zl4262987426.h"
#include "Pathfinding_Ionic_Zip_Reduced_Pathfinding_Ionic_Zl4048658636.h"

#pragma once
// Pathfinding.Ionic.Zip.ZipEntry[]
struct ZipEntryU5BU5D_t2344008592  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ZipEntry_t2786874973 * m_Items[1];

public:
	inline ZipEntry_t2786874973 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ZipEntry_t2786874973 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ZipEntry_t2786874973 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Pathfinding.Ionic.Zlib.DeflateManager/Config[]
struct ConfigU5BU5D_t3304337559  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Config_t4262987426 * m_Items[1];

public:
	inline Config_t4262987426 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Config_t4262987426 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Config_t4262987426 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Pathfinding.Ionic.Zlib.WorkItem[]
struct WorkItemU5BU5D_t1875748485  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) WorkItem_t4048658636 * m_Items[1];

public:
	inline WorkItem_t4048658636 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline WorkItem_t4048658636 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, WorkItem_t4048658636 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
