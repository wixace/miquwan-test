﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_serecherRirsisdou147
struct  M_serecherRirsisdou147_t1306728851  : public Il2CppObject
{
public:
	// System.Boolean GarbageiOS.M_serecherRirsisdou147::_hohowxow
	bool ____hohowxow_0;
	// System.UInt32 GarbageiOS.M_serecherRirsisdou147::_ferstowLougejal
	uint32_t ____ferstowLougejal_1;
	// System.Single GarbageiOS.M_serecherRirsisdou147::_kawepi
	float ____kawepi_2;
	// System.String GarbageiOS.M_serecherRirsisdou147::_muwaDurnoosay
	String_t* ____muwaDurnoosay_3;
	// System.UInt32 GarbageiOS.M_serecherRirsisdou147::_searkowNamosor
	uint32_t ____searkowNamosor_4;
	// System.String GarbageiOS.M_serecherRirsisdou147::_toomasSarke
	String_t* ____toomasSarke_5;
	// System.String GarbageiOS.M_serecherRirsisdou147::_stouseKura
	String_t* ____stouseKura_6;
	// System.Boolean GarbageiOS.M_serecherRirsisdou147::_weldrou
	bool ____weldrou_7;
	// System.Single GarbageiOS.M_serecherRirsisdou147::_drakafa
	float ____drakafa_8;
	// System.Boolean GarbageiOS.M_serecherRirsisdou147::_chisne
	bool ____chisne_9;
	// System.Single GarbageiOS.M_serecherRirsisdou147::_dermiHalpai
	float ____dermiHalpai_10;
	// System.Boolean GarbageiOS.M_serecherRirsisdou147::_tairrar
	bool ____tairrar_11;
	// System.Boolean GarbageiOS.M_serecherRirsisdou147::_drisweStastowsi
	bool ____drisweStastowsi_12;
	// System.String GarbageiOS.M_serecherRirsisdou147::_firdomouCoustay
	String_t* ____firdomouCoustay_13;
	// System.UInt32 GarbageiOS.M_serecherRirsisdou147::_gowrelCelta
	uint32_t ____gowrelCelta_14;
	// System.Int32 GarbageiOS.M_serecherRirsisdou147::_tawhas
	int32_t ____tawhas_15;
	// System.Single GarbageiOS.M_serecherRirsisdou147::_kege
	float ____kege_16;
	// System.UInt32 GarbageiOS.M_serecherRirsisdou147::_letedi
	uint32_t ____letedi_17;
	// System.Boolean GarbageiOS.M_serecherRirsisdou147::_nairjoupaw
	bool ____nairjoupaw_18;
	// System.Int32 GarbageiOS.M_serecherRirsisdou147::_rernixaRermem
	int32_t ____rernixaRermem_19;
	// System.UInt32 GarbageiOS.M_serecherRirsisdou147::_gasza
	uint32_t ____gasza_20;
	// System.Single GarbageiOS.M_serecherRirsisdou147::_jalhischemMirzoudou
	float ____jalhischemMirzoudou_21;
	// System.Boolean GarbageiOS.M_serecherRirsisdou147::_saho
	bool ____saho_22;
	// System.Int32 GarbageiOS.M_serecherRirsisdou147::_karlirja
	int32_t ____karlirja_23;
	// System.Boolean GarbageiOS.M_serecherRirsisdou147::_nemniRoreeral
	bool ____nemniRoreeral_24;
	// System.UInt32 GarbageiOS.M_serecherRirsisdou147::_lelopallLeltearstou
	uint32_t ____lelopallLeltearstou_25;
	// System.Boolean GarbageiOS.M_serecherRirsisdou147::_jerepallPiyerece
	bool ____jerepallPiyerece_26;
	// System.Single GarbageiOS.M_serecherRirsisdou147::_fallwheborSoosasyo
	float ____fallwheborSoosasyo_27;
	// System.UInt32 GarbageiOS.M_serecherRirsisdou147::_caxa
	uint32_t ____caxa_28;
	// System.Int32 GarbageiOS.M_serecherRirsisdou147::_saycharwearJaibutaw
	int32_t ____saycharwearJaibutaw_29;
	// System.Single GarbageiOS.M_serecherRirsisdou147::_jeabalBupo
	float ____jeabalBupo_30;
	// System.String GarbageiOS.M_serecherRirsisdou147::_stalerwheRousocas
	String_t* ____stalerwheRousocas_31;
	// System.Single GarbageiOS.M_serecherRirsisdou147::_serzofallLori
	float ____serzofallLori_32;
	// System.UInt32 GarbageiOS.M_serecherRirsisdou147::_ralnairnoFearyichoo
	uint32_t ____ralnairnoFearyichoo_33;
	// System.UInt32 GarbageiOS.M_serecherRirsisdou147::_helgormou
	uint32_t ____helgormou_34;
	// System.Single GarbageiOS.M_serecherRirsisdou147::_sairkur
	float ____sairkur_35;
	// System.Boolean GarbageiOS.M_serecherRirsisdou147::_pouchere
	bool ____pouchere_36;
	// System.Boolean GarbageiOS.M_serecherRirsisdou147::_yerarSacearnee
	bool ____yerarSacearnee_37;
	// System.UInt32 GarbageiOS.M_serecherRirsisdou147::_trasdrallMearserlur
	uint32_t ____trasdrallMearserlur_38;
	// System.Boolean GarbageiOS.M_serecherRirsisdou147::_hiwairbaTowa
	bool ____hiwairbaTowa_39;
	// System.Single GarbageiOS.M_serecherRirsisdou147::_pirbeFoubere
	float ____pirbeFoubere_40;
	// System.UInt32 GarbageiOS.M_serecherRirsisdou147::_wallnibow
	uint32_t ____wallnibow_41;
	// System.String GarbageiOS.M_serecherRirsisdou147::_belouwai
	String_t* ____belouwai_42;
	// System.Boolean GarbageiOS.M_serecherRirsisdou147::_sipawu
	bool ____sipawu_43;
	// System.UInt32 GarbageiOS.M_serecherRirsisdou147::_zallis
	uint32_t ____zallis_44;
	// System.Single GarbageiOS.M_serecherRirsisdou147::_ceabiBirraiwel
	float ____ceabiBirraiwel_45;
	// System.UInt32 GarbageiOS.M_serecherRirsisdou147::_nayhalRishurnaw
	uint32_t ____nayhalRishurnaw_46;
	// System.Boolean GarbageiOS.M_serecherRirsisdou147::_retrair
	bool ____retrair_47;
	// System.Int32 GarbageiOS.M_serecherRirsisdou147::_yeameRaynur
	int32_t ____yeameRaynur_48;
	// System.UInt32 GarbageiOS.M_serecherRirsisdou147::_ceedrairNarsu
	uint32_t ____ceedrairNarsu_49;
	// System.Boolean GarbageiOS.M_serecherRirsisdou147::_jostelrer
	bool ____jostelrer_50;
	// System.UInt32 GarbageiOS.M_serecherRirsisdou147::_chirpouchouJamasta
	uint32_t ____chirpouchouJamasta_51;
	// System.Int32 GarbageiOS.M_serecherRirsisdou147::_youceaRujowke
	int32_t ____youceaRujowke_52;

public:
	inline static int32_t get_offset_of__hohowxow_0() { return static_cast<int32_t>(offsetof(M_serecherRirsisdou147_t1306728851, ____hohowxow_0)); }
	inline bool get__hohowxow_0() const { return ____hohowxow_0; }
	inline bool* get_address_of__hohowxow_0() { return &____hohowxow_0; }
	inline void set__hohowxow_0(bool value)
	{
		____hohowxow_0 = value;
	}

	inline static int32_t get_offset_of__ferstowLougejal_1() { return static_cast<int32_t>(offsetof(M_serecherRirsisdou147_t1306728851, ____ferstowLougejal_1)); }
	inline uint32_t get__ferstowLougejal_1() const { return ____ferstowLougejal_1; }
	inline uint32_t* get_address_of__ferstowLougejal_1() { return &____ferstowLougejal_1; }
	inline void set__ferstowLougejal_1(uint32_t value)
	{
		____ferstowLougejal_1 = value;
	}

	inline static int32_t get_offset_of__kawepi_2() { return static_cast<int32_t>(offsetof(M_serecherRirsisdou147_t1306728851, ____kawepi_2)); }
	inline float get__kawepi_2() const { return ____kawepi_2; }
	inline float* get_address_of__kawepi_2() { return &____kawepi_2; }
	inline void set__kawepi_2(float value)
	{
		____kawepi_2 = value;
	}

	inline static int32_t get_offset_of__muwaDurnoosay_3() { return static_cast<int32_t>(offsetof(M_serecherRirsisdou147_t1306728851, ____muwaDurnoosay_3)); }
	inline String_t* get__muwaDurnoosay_3() const { return ____muwaDurnoosay_3; }
	inline String_t** get_address_of__muwaDurnoosay_3() { return &____muwaDurnoosay_3; }
	inline void set__muwaDurnoosay_3(String_t* value)
	{
		____muwaDurnoosay_3 = value;
		Il2CppCodeGenWriteBarrier(&____muwaDurnoosay_3, value);
	}

	inline static int32_t get_offset_of__searkowNamosor_4() { return static_cast<int32_t>(offsetof(M_serecherRirsisdou147_t1306728851, ____searkowNamosor_4)); }
	inline uint32_t get__searkowNamosor_4() const { return ____searkowNamosor_4; }
	inline uint32_t* get_address_of__searkowNamosor_4() { return &____searkowNamosor_4; }
	inline void set__searkowNamosor_4(uint32_t value)
	{
		____searkowNamosor_4 = value;
	}

	inline static int32_t get_offset_of__toomasSarke_5() { return static_cast<int32_t>(offsetof(M_serecherRirsisdou147_t1306728851, ____toomasSarke_5)); }
	inline String_t* get__toomasSarke_5() const { return ____toomasSarke_5; }
	inline String_t** get_address_of__toomasSarke_5() { return &____toomasSarke_5; }
	inline void set__toomasSarke_5(String_t* value)
	{
		____toomasSarke_5 = value;
		Il2CppCodeGenWriteBarrier(&____toomasSarke_5, value);
	}

	inline static int32_t get_offset_of__stouseKura_6() { return static_cast<int32_t>(offsetof(M_serecherRirsisdou147_t1306728851, ____stouseKura_6)); }
	inline String_t* get__stouseKura_6() const { return ____stouseKura_6; }
	inline String_t** get_address_of__stouseKura_6() { return &____stouseKura_6; }
	inline void set__stouseKura_6(String_t* value)
	{
		____stouseKura_6 = value;
		Il2CppCodeGenWriteBarrier(&____stouseKura_6, value);
	}

	inline static int32_t get_offset_of__weldrou_7() { return static_cast<int32_t>(offsetof(M_serecherRirsisdou147_t1306728851, ____weldrou_7)); }
	inline bool get__weldrou_7() const { return ____weldrou_7; }
	inline bool* get_address_of__weldrou_7() { return &____weldrou_7; }
	inline void set__weldrou_7(bool value)
	{
		____weldrou_7 = value;
	}

	inline static int32_t get_offset_of__drakafa_8() { return static_cast<int32_t>(offsetof(M_serecherRirsisdou147_t1306728851, ____drakafa_8)); }
	inline float get__drakafa_8() const { return ____drakafa_8; }
	inline float* get_address_of__drakafa_8() { return &____drakafa_8; }
	inline void set__drakafa_8(float value)
	{
		____drakafa_8 = value;
	}

	inline static int32_t get_offset_of__chisne_9() { return static_cast<int32_t>(offsetof(M_serecherRirsisdou147_t1306728851, ____chisne_9)); }
	inline bool get__chisne_9() const { return ____chisne_9; }
	inline bool* get_address_of__chisne_9() { return &____chisne_9; }
	inline void set__chisne_9(bool value)
	{
		____chisne_9 = value;
	}

	inline static int32_t get_offset_of__dermiHalpai_10() { return static_cast<int32_t>(offsetof(M_serecherRirsisdou147_t1306728851, ____dermiHalpai_10)); }
	inline float get__dermiHalpai_10() const { return ____dermiHalpai_10; }
	inline float* get_address_of__dermiHalpai_10() { return &____dermiHalpai_10; }
	inline void set__dermiHalpai_10(float value)
	{
		____dermiHalpai_10 = value;
	}

	inline static int32_t get_offset_of__tairrar_11() { return static_cast<int32_t>(offsetof(M_serecherRirsisdou147_t1306728851, ____tairrar_11)); }
	inline bool get__tairrar_11() const { return ____tairrar_11; }
	inline bool* get_address_of__tairrar_11() { return &____tairrar_11; }
	inline void set__tairrar_11(bool value)
	{
		____tairrar_11 = value;
	}

	inline static int32_t get_offset_of__drisweStastowsi_12() { return static_cast<int32_t>(offsetof(M_serecherRirsisdou147_t1306728851, ____drisweStastowsi_12)); }
	inline bool get__drisweStastowsi_12() const { return ____drisweStastowsi_12; }
	inline bool* get_address_of__drisweStastowsi_12() { return &____drisweStastowsi_12; }
	inline void set__drisweStastowsi_12(bool value)
	{
		____drisweStastowsi_12 = value;
	}

	inline static int32_t get_offset_of__firdomouCoustay_13() { return static_cast<int32_t>(offsetof(M_serecherRirsisdou147_t1306728851, ____firdomouCoustay_13)); }
	inline String_t* get__firdomouCoustay_13() const { return ____firdomouCoustay_13; }
	inline String_t** get_address_of__firdomouCoustay_13() { return &____firdomouCoustay_13; }
	inline void set__firdomouCoustay_13(String_t* value)
	{
		____firdomouCoustay_13 = value;
		Il2CppCodeGenWriteBarrier(&____firdomouCoustay_13, value);
	}

	inline static int32_t get_offset_of__gowrelCelta_14() { return static_cast<int32_t>(offsetof(M_serecherRirsisdou147_t1306728851, ____gowrelCelta_14)); }
	inline uint32_t get__gowrelCelta_14() const { return ____gowrelCelta_14; }
	inline uint32_t* get_address_of__gowrelCelta_14() { return &____gowrelCelta_14; }
	inline void set__gowrelCelta_14(uint32_t value)
	{
		____gowrelCelta_14 = value;
	}

	inline static int32_t get_offset_of__tawhas_15() { return static_cast<int32_t>(offsetof(M_serecherRirsisdou147_t1306728851, ____tawhas_15)); }
	inline int32_t get__tawhas_15() const { return ____tawhas_15; }
	inline int32_t* get_address_of__tawhas_15() { return &____tawhas_15; }
	inline void set__tawhas_15(int32_t value)
	{
		____tawhas_15 = value;
	}

	inline static int32_t get_offset_of__kege_16() { return static_cast<int32_t>(offsetof(M_serecherRirsisdou147_t1306728851, ____kege_16)); }
	inline float get__kege_16() const { return ____kege_16; }
	inline float* get_address_of__kege_16() { return &____kege_16; }
	inline void set__kege_16(float value)
	{
		____kege_16 = value;
	}

	inline static int32_t get_offset_of__letedi_17() { return static_cast<int32_t>(offsetof(M_serecherRirsisdou147_t1306728851, ____letedi_17)); }
	inline uint32_t get__letedi_17() const { return ____letedi_17; }
	inline uint32_t* get_address_of__letedi_17() { return &____letedi_17; }
	inline void set__letedi_17(uint32_t value)
	{
		____letedi_17 = value;
	}

	inline static int32_t get_offset_of__nairjoupaw_18() { return static_cast<int32_t>(offsetof(M_serecherRirsisdou147_t1306728851, ____nairjoupaw_18)); }
	inline bool get__nairjoupaw_18() const { return ____nairjoupaw_18; }
	inline bool* get_address_of__nairjoupaw_18() { return &____nairjoupaw_18; }
	inline void set__nairjoupaw_18(bool value)
	{
		____nairjoupaw_18 = value;
	}

	inline static int32_t get_offset_of__rernixaRermem_19() { return static_cast<int32_t>(offsetof(M_serecherRirsisdou147_t1306728851, ____rernixaRermem_19)); }
	inline int32_t get__rernixaRermem_19() const { return ____rernixaRermem_19; }
	inline int32_t* get_address_of__rernixaRermem_19() { return &____rernixaRermem_19; }
	inline void set__rernixaRermem_19(int32_t value)
	{
		____rernixaRermem_19 = value;
	}

	inline static int32_t get_offset_of__gasza_20() { return static_cast<int32_t>(offsetof(M_serecherRirsisdou147_t1306728851, ____gasza_20)); }
	inline uint32_t get__gasza_20() const { return ____gasza_20; }
	inline uint32_t* get_address_of__gasza_20() { return &____gasza_20; }
	inline void set__gasza_20(uint32_t value)
	{
		____gasza_20 = value;
	}

	inline static int32_t get_offset_of__jalhischemMirzoudou_21() { return static_cast<int32_t>(offsetof(M_serecherRirsisdou147_t1306728851, ____jalhischemMirzoudou_21)); }
	inline float get__jalhischemMirzoudou_21() const { return ____jalhischemMirzoudou_21; }
	inline float* get_address_of__jalhischemMirzoudou_21() { return &____jalhischemMirzoudou_21; }
	inline void set__jalhischemMirzoudou_21(float value)
	{
		____jalhischemMirzoudou_21 = value;
	}

	inline static int32_t get_offset_of__saho_22() { return static_cast<int32_t>(offsetof(M_serecherRirsisdou147_t1306728851, ____saho_22)); }
	inline bool get__saho_22() const { return ____saho_22; }
	inline bool* get_address_of__saho_22() { return &____saho_22; }
	inline void set__saho_22(bool value)
	{
		____saho_22 = value;
	}

	inline static int32_t get_offset_of__karlirja_23() { return static_cast<int32_t>(offsetof(M_serecherRirsisdou147_t1306728851, ____karlirja_23)); }
	inline int32_t get__karlirja_23() const { return ____karlirja_23; }
	inline int32_t* get_address_of__karlirja_23() { return &____karlirja_23; }
	inline void set__karlirja_23(int32_t value)
	{
		____karlirja_23 = value;
	}

	inline static int32_t get_offset_of__nemniRoreeral_24() { return static_cast<int32_t>(offsetof(M_serecherRirsisdou147_t1306728851, ____nemniRoreeral_24)); }
	inline bool get__nemniRoreeral_24() const { return ____nemniRoreeral_24; }
	inline bool* get_address_of__nemniRoreeral_24() { return &____nemniRoreeral_24; }
	inline void set__nemniRoreeral_24(bool value)
	{
		____nemniRoreeral_24 = value;
	}

	inline static int32_t get_offset_of__lelopallLeltearstou_25() { return static_cast<int32_t>(offsetof(M_serecherRirsisdou147_t1306728851, ____lelopallLeltearstou_25)); }
	inline uint32_t get__lelopallLeltearstou_25() const { return ____lelopallLeltearstou_25; }
	inline uint32_t* get_address_of__lelopallLeltearstou_25() { return &____lelopallLeltearstou_25; }
	inline void set__lelopallLeltearstou_25(uint32_t value)
	{
		____lelopallLeltearstou_25 = value;
	}

	inline static int32_t get_offset_of__jerepallPiyerece_26() { return static_cast<int32_t>(offsetof(M_serecherRirsisdou147_t1306728851, ____jerepallPiyerece_26)); }
	inline bool get__jerepallPiyerece_26() const { return ____jerepallPiyerece_26; }
	inline bool* get_address_of__jerepallPiyerece_26() { return &____jerepallPiyerece_26; }
	inline void set__jerepallPiyerece_26(bool value)
	{
		____jerepallPiyerece_26 = value;
	}

	inline static int32_t get_offset_of__fallwheborSoosasyo_27() { return static_cast<int32_t>(offsetof(M_serecherRirsisdou147_t1306728851, ____fallwheborSoosasyo_27)); }
	inline float get__fallwheborSoosasyo_27() const { return ____fallwheborSoosasyo_27; }
	inline float* get_address_of__fallwheborSoosasyo_27() { return &____fallwheborSoosasyo_27; }
	inline void set__fallwheborSoosasyo_27(float value)
	{
		____fallwheborSoosasyo_27 = value;
	}

	inline static int32_t get_offset_of__caxa_28() { return static_cast<int32_t>(offsetof(M_serecherRirsisdou147_t1306728851, ____caxa_28)); }
	inline uint32_t get__caxa_28() const { return ____caxa_28; }
	inline uint32_t* get_address_of__caxa_28() { return &____caxa_28; }
	inline void set__caxa_28(uint32_t value)
	{
		____caxa_28 = value;
	}

	inline static int32_t get_offset_of__saycharwearJaibutaw_29() { return static_cast<int32_t>(offsetof(M_serecherRirsisdou147_t1306728851, ____saycharwearJaibutaw_29)); }
	inline int32_t get__saycharwearJaibutaw_29() const { return ____saycharwearJaibutaw_29; }
	inline int32_t* get_address_of__saycharwearJaibutaw_29() { return &____saycharwearJaibutaw_29; }
	inline void set__saycharwearJaibutaw_29(int32_t value)
	{
		____saycharwearJaibutaw_29 = value;
	}

	inline static int32_t get_offset_of__jeabalBupo_30() { return static_cast<int32_t>(offsetof(M_serecherRirsisdou147_t1306728851, ____jeabalBupo_30)); }
	inline float get__jeabalBupo_30() const { return ____jeabalBupo_30; }
	inline float* get_address_of__jeabalBupo_30() { return &____jeabalBupo_30; }
	inline void set__jeabalBupo_30(float value)
	{
		____jeabalBupo_30 = value;
	}

	inline static int32_t get_offset_of__stalerwheRousocas_31() { return static_cast<int32_t>(offsetof(M_serecherRirsisdou147_t1306728851, ____stalerwheRousocas_31)); }
	inline String_t* get__stalerwheRousocas_31() const { return ____stalerwheRousocas_31; }
	inline String_t** get_address_of__stalerwheRousocas_31() { return &____stalerwheRousocas_31; }
	inline void set__stalerwheRousocas_31(String_t* value)
	{
		____stalerwheRousocas_31 = value;
		Il2CppCodeGenWriteBarrier(&____stalerwheRousocas_31, value);
	}

	inline static int32_t get_offset_of__serzofallLori_32() { return static_cast<int32_t>(offsetof(M_serecherRirsisdou147_t1306728851, ____serzofallLori_32)); }
	inline float get__serzofallLori_32() const { return ____serzofallLori_32; }
	inline float* get_address_of__serzofallLori_32() { return &____serzofallLori_32; }
	inline void set__serzofallLori_32(float value)
	{
		____serzofallLori_32 = value;
	}

	inline static int32_t get_offset_of__ralnairnoFearyichoo_33() { return static_cast<int32_t>(offsetof(M_serecherRirsisdou147_t1306728851, ____ralnairnoFearyichoo_33)); }
	inline uint32_t get__ralnairnoFearyichoo_33() const { return ____ralnairnoFearyichoo_33; }
	inline uint32_t* get_address_of__ralnairnoFearyichoo_33() { return &____ralnairnoFearyichoo_33; }
	inline void set__ralnairnoFearyichoo_33(uint32_t value)
	{
		____ralnairnoFearyichoo_33 = value;
	}

	inline static int32_t get_offset_of__helgormou_34() { return static_cast<int32_t>(offsetof(M_serecherRirsisdou147_t1306728851, ____helgormou_34)); }
	inline uint32_t get__helgormou_34() const { return ____helgormou_34; }
	inline uint32_t* get_address_of__helgormou_34() { return &____helgormou_34; }
	inline void set__helgormou_34(uint32_t value)
	{
		____helgormou_34 = value;
	}

	inline static int32_t get_offset_of__sairkur_35() { return static_cast<int32_t>(offsetof(M_serecherRirsisdou147_t1306728851, ____sairkur_35)); }
	inline float get__sairkur_35() const { return ____sairkur_35; }
	inline float* get_address_of__sairkur_35() { return &____sairkur_35; }
	inline void set__sairkur_35(float value)
	{
		____sairkur_35 = value;
	}

	inline static int32_t get_offset_of__pouchere_36() { return static_cast<int32_t>(offsetof(M_serecherRirsisdou147_t1306728851, ____pouchere_36)); }
	inline bool get__pouchere_36() const { return ____pouchere_36; }
	inline bool* get_address_of__pouchere_36() { return &____pouchere_36; }
	inline void set__pouchere_36(bool value)
	{
		____pouchere_36 = value;
	}

	inline static int32_t get_offset_of__yerarSacearnee_37() { return static_cast<int32_t>(offsetof(M_serecherRirsisdou147_t1306728851, ____yerarSacearnee_37)); }
	inline bool get__yerarSacearnee_37() const { return ____yerarSacearnee_37; }
	inline bool* get_address_of__yerarSacearnee_37() { return &____yerarSacearnee_37; }
	inline void set__yerarSacearnee_37(bool value)
	{
		____yerarSacearnee_37 = value;
	}

	inline static int32_t get_offset_of__trasdrallMearserlur_38() { return static_cast<int32_t>(offsetof(M_serecherRirsisdou147_t1306728851, ____trasdrallMearserlur_38)); }
	inline uint32_t get__trasdrallMearserlur_38() const { return ____trasdrallMearserlur_38; }
	inline uint32_t* get_address_of__trasdrallMearserlur_38() { return &____trasdrallMearserlur_38; }
	inline void set__trasdrallMearserlur_38(uint32_t value)
	{
		____trasdrallMearserlur_38 = value;
	}

	inline static int32_t get_offset_of__hiwairbaTowa_39() { return static_cast<int32_t>(offsetof(M_serecherRirsisdou147_t1306728851, ____hiwairbaTowa_39)); }
	inline bool get__hiwairbaTowa_39() const { return ____hiwairbaTowa_39; }
	inline bool* get_address_of__hiwairbaTowa_39() { return &____hiwairbaTowa_39; }
	inline void set__hiwairbaTowa_39(bool value)
	{
		____hiwairbaTowa_39 = value;
	}

	inline static int32_t get_offset_of__pirbeFoubere_40() { return static_cast<int32_t>(offsetof(M_serecherRirsisdou147_t1306728851, ____pirbeFoubere_40)); }
	inline float get__pirbeFoubere_40() const { return ____pirbeFoubere_40; }
	inline float* get_address_of__pirbeFoubere_40() { return &____pirbeFoubere_40; }
	inline void set__pirbeFoubere_40(float value)
	{
		____pirbeFoubere_40 = value;
	}

	inline static int32_t get_offset_of__wallnibow_41() { return static_cast<int32_t>(offsetof(M_serecherRirsisdou147_t1306728851, ____wallnibow_41)); }
	inline uint32_t get__wallnibow_41() const { return ____wallnibow_41; }
	inline uint32_t* get_address_of__wallnibow_41() { return &____wallnibow_41; }
	inline void set__wallnibow_41(uint32_t value)
	{
		____wallnibow_41 = value;
	}

	inline static int32_t get_offset_of__belouwai_42() { return static_cast<int32_t>(offsetof(M_serecherRirsisdou147_t1306728851, ____belouwai_42)); }
	inline String_t* get__belouwai_42() const { return ____belouwai_42; }
	inline String_t** get_address_of__belouwai_42() { return &____belouwai_42; }
	inline void set__belouwai_42(String_t* value)
	{
		____belouwai_42 = value;
		Il2CppCodeGenWriteBarrier(&____belouwai_42, value);
	}

	inline static int32_t get_offset_of__sipawu_43() { return static_cast<int32_t>(offsetof(M_serecherRirsisdou147_t1306728851, ____sipawu_43)); }
	inline bool get__sipawu_43() const { return ____sipawu_43; }
	inline bool* get_address_of__sipawu_43() { return &____sipawu_43; }
	inline void set__sipawu_43(bool value)
	{
		____sipawu_43 = value;
	}

	inline static int32_t get_offset_of__zallis_44() { return static_cast<int32_t>(offsetof(M_serecherRirsisdou147_t1306728851, ____zallis_44)); }
	inline uint32_t get__zallis_44() const { return ____zallis_44; }
	inline uint32_t* get_address_of__zallis_44() { return &____zallis_44; }
	inline void set__zallis_44(uint32_t value)
	{
		____zallis_44 = value;
	}

	inline static int32_t get_offset_of__ceabiBirraiwel_45() { return static_cast<int32_t>(offsetof(M_serecherRirsisdou147_t1306728851, ____ceabiBirraiwel_45)); }
	inline float get__ceabiBirraiwel_45() const { return ____ceabiBirraiwel_45; }
	inline float* get_address_of__ceabiBirraiwel_45() { return &____ceabiBirraiwel_45; }
	inline void set__ceabiBirraiwel_45(float value)
	{
		____ceabiBirraiwel_45 = value;
	}

	inline static int32_t get_offset_of__nayhalRishurnaw_46() { return static_cast<int32_t>(offsetof(M_serecherRirsisdou147_t1306728851, ____nayhalRishurnaw_46)); }
	inline uint32_t get__nayhalRishurnaw_46() const { return ____nayhalRishurnaw_46; }
	inline uint32_t* get_address_of__nayhalRishurnaw_46() { return &____nayhalRishurnaw_46; }
	inline void set__nayhalRishurnaw_46(uint32_t value)
	{
		____nayhalRishurnaw_46 = value;
	}

	inline static int32_t get_offset_of__retrair_47() { return static_cast<int32_t>(offsetof(M_serecherRirsisdou147_t1306728851, ____retrair_47)); }
	inline bool get__retrair_47() const { return ____retrair_47; }
	inline bool* get_address_of__retrair_47() { return &____retrair_47; }
	inline void set__retrair_47(bool value)
	{
		____retrair_47 = value;
	}

	inline static int32_t get_offset_of__yeameRaynur_48() { return static_cast<int32_t>(offsetof(M_serecherRirsisdou147_t1306728851, ____yeameRaynur_48)); }
	inline int32_t get__yeameRaynur_48() const { return ____yeameRaynur_48; }
	inline int32_t* get_address_of__yeameRaynur_48() { return &____yeameRaynur_48; }
	inline void set__yeameRaynur_48(int32_t value)
	{
		____yeameRaynur_48 = value;
	}

	inline static int32_t get_offset_of__ceedrairNarsu_49() { return static_cast<int32_t>(offsetof(M_serecherRirsisdou147_t1306728851, ____ceedrairNarsu_49)); }
	inline uint32_t get__ceedrairNarsu_49() const { return ____ceedrairNarsu_49; }
	inline uint32_t* get_address_of__ceedrairNarsu_49() { return &____ceedrairNarsu_49; }
	inline void set__ceedrairNarsu_49(uint32_t value)
	{
		____ceedrairNarsu_49 = value;
	}

	inline static int32_t get_offset_of__jostelrer_50() { return static_cast<int32_t>(offsetof(M_serecherRirsisdou147_t1306728851, ____jostelrer_50)); }
	inline bool get__jostelrer_50() const { return ____jostelrer_50; }
	inline bool* get_address_of__jostelrer_50() { return &____jostelrer_50; }
	inline void set__jostelrer_50(bool value)
	{
		____jostelrer_50 = value;
	}

	inline static int32_t get_offset_of__chirpouchouJamasta_51() { return static_cast<int32_t>(offsetof(M_serecherRirsisdou147_t1306728851, ____chirpouchouJamasta_51)); }
	inline uint32_t get__chirpouchouJamasta_51() const { return ____chirpouchouJamasta_51; }
	inline uint32_t* get_address_of__chirpouchouJamasta_51() { return &____chirpouchouJamasta_51; }
	inline void set__chirpouchouJamasta_51(uint32_t value)
	{
		____chirpouchouJamasta_51 = value;
	}

	inline static int32_t get_offset_of__youceaRujowke_52() { return static_cast<int32_t>(offsetof(M_serecherRirsisdou147_t1306728851, ____youceaRujowke_52)); }
	inline int32_t get__youceaRujowke_52() const { return ____youceaRujowke_52; }
	inline int32_t* get_address_of__youceaRujowke_52() { return &____youceaRujowke_52; }
	inline void set__youceaRujowke_52(int32_t value)
	{
		____youceaRujowke_52 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
