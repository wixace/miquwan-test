﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_fowjouporTereqal25
struct M_fowjouporTereqal25_t119778618;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"

// System.Void GarbageiOS.M_fowjouporTereqal25::.ctor()
extern "C"  void M_fowjouporTereqal25__ctor_m3585636409 (M_fowjouporTereqal25_t119778618 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_fowjouporTereqal25::M_jokoujaRupeexu0(System.String[],System.Int32)
extern "C"  void M_fowjouporTereqal25_M_jokoujaRupeexu0_m1770660421 (M_fowjouporTereqal25_t119778618 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_fowjouporTereqal25::M_tistalljou1(System.String[],System.Int32)
extern "C"  void M_fowjouporTereqal25_M_tistalljou1_m3851952872 (M_fowjouporTereqal25_t119778618 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
