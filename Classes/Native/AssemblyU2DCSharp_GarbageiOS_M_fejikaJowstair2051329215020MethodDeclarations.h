﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_fejikaJowstair205
struct M_fejikaJowstair205_t1329215020;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"

// System.Void GarbageiOS.M_fejikaJowstair205::.ctor()
extern "C"  void M_fejikaJowstair205__ctor_m2701493431 (M_fejikaJowstair205_t1329215020 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_fejikaJowstair205::M_nirnawor0(System.String[],System.Int32)
extern "C"  void M_fejikaJowstair205_M_nirnawor0_m1204000410 (M_fejikaJowstair205_t1329215020 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
