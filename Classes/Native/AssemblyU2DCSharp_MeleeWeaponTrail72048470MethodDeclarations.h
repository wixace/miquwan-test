﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MeleeWeaponTrail
struct MeleeWeaponTrail_t72048470;
// System.Collections.Generic.List`1<MeleeWeaponTrail/Point>
struct List_1_t1002179655;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Vector3>
struct IEnumerable_1_t3288012227;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t215400611;

#include "codegen/il2cpp-codegen.h"

// System.Void MeleeWeaponTrail::.ctor()
extern "C"  void MeleeWeaponTrail__ctor_m3658524677 (MeleeWeaponTrail_t72048470 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MeleeWeaponTrail::set_Emit(System.Boolean)
extern "C"  void MeleeWeaponTrail_set_Emit_m3769454182 (MeleeWeaponTrail_t72048470 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MeleeWeaponTrail::set_Use(System.Boolean)
extern "C"  void MeleeWeaponTrail_set_Use_m821294340 (MeleeWeaponTrail_t72048470 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MeleeWeaponTrail::Start()
extern "C"  void MeleeWeaponTrail_Start_m2605662469 (MeleeWeaponTrail_t72048470 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MeleeWeaponTrail::OnDisable()
extern "C"  void MeleeWeaponTrail_OnDisable_m2680844780 (MeleeWeaponTrail_t72048470 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MeleeWeaponTrail::OnEnable()
extern "C"  void MeleeWeaponTrail_OnEnable_m4090127713 (MeleeWeaponTrail_t72048470 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MeleeWeaponTrail::Update()
extern "C"  void MeleeWeaponTrail_Update_m3471977384 (MeleeWeaponTrail_t72048470 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MeleeWeaponTrail::RemoveOldPoints(System.Collections.Generic.List`1<MeleeWeaponTrail/Point>)
extern "C"  void MeleeWeaponTrail_RemoveOldPoints_m4291957984 (MeleeWeaponTrail_t72048470 * __this, List_1_t1002179655 * ___pointList0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Vector3> MeleeWeaponTrail::ilo_NewCatmullRom1(UnityEngine.Vector3[],System.Int32,System.Boolean)
extern "C"  Il2CppObject* MeleeWeaponTrail_ilo_NewCatmullRom1_m782400999 (Il2CppObject * __this /* static, unused */, Vector3U5BU5D_t215400611* ___points0, int32_t ___slices1, bool ___loop2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
