﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3363211663MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<CEvent.EventFunc`1<System.Object>>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m1012771459(__this, ___dictionary0, method) ((  void (*) (Enumerator_t858672199 *, Dictionary_2_t3836316103 *, const MethodInfo*))Enumerator__ctor_m3920831137_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<CEvent.EventFunc`1<System.Object>>>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1267438280(__this, method) ((  Il2CppObject * (*) (Enumerator_t858672199 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3262087712_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<CEvent.EventFunc`1<System.Object>>>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m52623954(__this, method) ((  void (*) (Enumerator_t858672199 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2959141748_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<CEvent.EventFunc`1<System.Object>>>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2259269769(__this, method) ((  DictionaryEntry_t1751606614  (*) (Enumerator_t858672199 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2279524093_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<CEvent.EventFunc`1<System.Object>>>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2793642404(__this, method) ((  Il2CppObject * (*) (Enumerator_t858672199 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1201448700_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<CEvent.EventFunc`1<System.Object>>>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1384226614(__this, method) ((  Il2CppObject * (*) (Enumerator_t858672199 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m294434446_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<CEvent.EventFunc`1<System.Object>>>::MoveNext()
#define Enumerator_MoveNext_m1530294530(__this, method) ((  bool (*) (Enumerator_t858672199 *, const MethodInfo*))Enumerator_MoveNext_m217327200_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<CEvent.EventFunc`1<System.Object>>>::get_Current()
#define Enumerator_get_Current_m3256182394(__this, method) ((  KeyValuePair_2_t3735096809  (*) (Enumerator_t858672199 *, const MethodInfo*))Enumerator_get_Current_m4240003024_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<CEvent.EventFunc`1<System.Object>>>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m1830861835(__this, method) ((  String_t* (*) (Enumerator_t858672199 *, const MethodInfo*))Enumerator_get_CurrentKey_m3062159917_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<CEvent.EventFunc`1<System.Object>>>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m4139422091(__this, method) ((  List_1_t3015897733 * (*) (Enumerator_t858672199 *, const MethodInfo*))Enumerator_get_CurrentValue_m592783249_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<CEvent.EventFunc`1<System.Object>>>::Reset()
#define Enumerator_Reset_m608251989(__this, method) ((  void (*) (Enumerator_t858672199 *, const MethodInfo*))Enumerator_Reset_m3001375603_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<CEvent.EventFunc`1<System.Object>>>::VerifyState()
#define Enumerator_VerifyState_m488778910(__this, method) ((  void (*) (Enumerator_t858672199 *, const MethodInfo*))Enumerator_VerifyState_m4290054460_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<CEvent.EventFunc`1<System.Object>>>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m15001734(__this, method) ((  void (*) (Enumerator_t858672199 *, const MethodInfo*))Enumerator_VerifyCurrent_m2318603684_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<CEvent.EventFunc`1<System.Object>>>::Dispose()
#define Enumerator_Dispose_m2938038245(__this, method) ((  void (*) (Enumerator_t858672199 *, const MethodInfo*))Enumerator_Dispose_m627360643_gshared)(__this, method)
