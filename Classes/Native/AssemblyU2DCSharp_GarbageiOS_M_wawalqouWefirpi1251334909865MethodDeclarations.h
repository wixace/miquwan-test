﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_wawalqouWefirpi125
struct M_wawalqouWefirpi125_t1334909865;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"

// System.Void GarbageiOS.M_wawalqouWefirpi125::.ctor()
extern "C"  void M_wawalqouWefirpi125__ctor_m845076138 (M_wawalqouWefirpi125_t1334909865 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_wawalqouWefirpi125::M_lervearjoCawgar0(System.String[],System.Int32)
extern "C"  void M_wawalqouWefirpi125_M_lervearjoCawgar0_m3017682614 (M_wawalqouWefirpi125_t1334909865 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_wawalqouWefirpi125::M_dajaw1(System.String[],System.Int32)
extern "C"  void M_wawalqouWefirpi125_M_dajaw1_m1480785201 (M_wawalqouWefirpi125_t1334909865 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
