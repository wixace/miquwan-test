﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// effectCfg
struct effectCfg_t2826279187;
// CombatEntity
struct CombatEntity_t684137495;
// EffectCtrl
struct EffectCtrl_t3708787644;
// EffectMgr
struct EffectMgr_t535289511;

#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_EntityEnum_UnitTag299076696.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EffectMgr/<PlayShoot>c__AnonStorey157
struct  U3CPlayShootU3Ec__AnonStorey157_t788163571  : public Il2CppObject
{
public:
	// System.Int32 EffectMgr/<PlayShoot>c__AnonStorey157::id
	int32_t ___id_0;
	// effectCfg EffectMgr/<PlayShoot>c__AnonStorey157::effectCfg
	effectCfg_t2826279187 * ___effectCfg_1;
	// CombatEntity EffectMgr/<PlayShoot>c__AnonStorey157::srcEntity
	CombatEntity_t684137495 * ___srcEntity_2;
	// EffectCtrl EffectMgr/<PlayShoot>c__AnonStorey157::effectObj
	EffectCtrl_t3708787644 * ___effectObj_3;
	// CombatEntity EffectMgr/<PlayShoot>c__AnonStorey157::targetTf
	CombatEntity_t684137495 * ___targetTf_4;
	// System.Single EffectMgr/<PlayShoot>c__AnonStorey157::speed
	float ___speed_5;
	// System.Boolean EffectMgr/<PlayShoot>c__AnonStorey157::isatk
	bool ___isatk_6;
	// UnityEngine.Vector3 EffectMgr/<PlayShoot>c__AnonStorey157::targetPosition
	Vector3_t4282066566  ___targetPosition_7;
	// EntityEnum.UnitTag EffectMgr/<PlayShoot>c__AnonStorey157::tag
	int32_t ___tag_8;
	// System.Int32 EffectMgr/<PlayShoot>c__AnonStorey157::skillID
	int32_t ___skillID_9;
	// EffectMgr EffectMgr/<PlayShoot>c__AnonStorey157::<>f__this
	EffectMgr_t535289511 * ___U3CU3Ef__this_10;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(U3CPlayShootU3Ec__AnonStorey157_t788163571, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_effectCfg_1() { return static_cast<int32_t>(offsetof(U3CPlayShootU3Ec__AnonStorey157_t788163571, ___effectCfg_1)); }
	inline effectCfg_t2826279187 * get_effectCfg_1() const { return ___effectCfg_1; }
	inline effectCfg_t2826279187 ** get_address_of_effectCfg_1() { return &___effectCfg_1; }
	inline void set_effectCfg_1(effectCfg_t2826279187 * value)
	{
		___effectCfg_1 = value;
		Il2CppCodeGenWriteBarrier(&___effectCfg_1, value);
	}

	inline static int32_t get_offset_of_srcEntity_2() { return static_cast<int32_t>(offsetof(U3CPlayShootU3Ec__AnonStorey157_t788163571, ___srcEntity_2)); }
	inline CombatEntity_t684137495 * get_srcEntity_2() const { return ___srcEntity_2; }
	inline CombatEntity_t684137495 ** get_address_of_srcEntity_2() { return &___srcEntity_2; }
	inline void set_srcEntity_2(CombatEntity_t684137495 * value)
	{
		___srcEntity_2 = value;
		Il2CppCodeGenWriteBarrier(&___srcEntity_2, value);
	}

	inline static int32_t get_offset_of_effectObj_3() { return static_cast<int32_t>(offsetof(U3CPlayShootU3Ec__AnonStorey157_t788163571, ___effectObj_3)); }
	inline EffectCtrl_t3708787644 * get_effectObj_3() const { return ___effectObj_3; }
	inline EffectCtrl_t3708787644 ** get_address_of_effectObj_3() { return &___effectObj_3; }
	inline void set_effectObj_3(EffectCtrl_t3708787644 * value)
	{
		___effectObj_3 = value;
		Il2CppCodeGenWriteBarrier(&___effectObj_3, value);
	}

	inline static int32_t get_offset_of_targetTf_4() { return static_cast<int32_t>(offsetof(U3CPlayShootU3Ec__AnonStorey157_t788163571, ___targetTf_4)); }
	inline CombatEntity_t684137495 * get_targetTf_4() const { return ___targetTf_4; }
	inline CombatEntity_t684137495 ** get_address_of_targetTf_4() { return &___targetTf_4; }
	inline void set_targetTf_4(CombatEntity_t684137495 * value)
	{
		___targetTf_4 = value;
		Il2CppCodeGenWriteBarrier(&___targetTf_4, value);
	}

	inline static int32_t get_offset_of_speed_5() { return static_cast<int32_t>(offsetof(U3CPlayShootU3Ec__AnonStorey157_t788163571, ___speed_5)); }
	inline float get_speed_5() const { return ___speed_5; }
	inline float* get_address_of_speed_5() { return &___speed_5; }
	inline void set_speed_5(float value)
	{
		___speed_5 = value;
	}

	inline static int32_t get_offset_of_isatk_6() { return static_cast<int32_t>(offsetof(U3CPlayShootU3Ec__AnonStorey157_t788163571, ___isatk_6)); }
	inline bool get_isatk_6() const { return ___isatk_6; }
	inline bool* get_address_of_isatk_6() { return &___isatk_6; }
	inline void set_isatk_6(bool value)
	{
		___isatk_6 = value;
	}

	inline static int32_t get_offset_of_targetPosition_7() { return static_cast<int32_t>(offsetof(U3CPlayShootU3Ec__AnonStorey157_t788163571, ___targetPosition_7)); }
	inline Vector3_t4282066566  get_targetPosition_7() const { return ___targetPosition_7; }
	inline Vector3_t4282066566 * get_address_of_targetPosition_7() { return &___targetPosition_7; }
	inline void set_targetPosition_7(Vector3_t4282066566  value)
	{
		___targetPosition_7 = value;
	}

	inline static int32_t get_offset_of_tag_8() { return static_cast<int32_t>(offsetof(U3CPlayShootU3Ec__AnonStorey157_t788163571, ___tag_8)); }
	inline int32_t get_tag_8() const { return ___tag_8; }
	inline int32_t* get_address_of_tag_8() { return &___tag_8; }
	inline void set_tag_8(int32_t value)
	{
		___tag_8 = value;
	}

	inline static int32_t get_offset_of_skillID_9() { return static_cast<int32_t>(offsetof(U3CPlayShootU3Ec__AnonStorey157_t788163571, ___skillID_9)); }
	inline int32_t get_skillID_9() const { return ___skillID_9; }
	inline int32_t* get_address_of_skillID_9() { return &___skillID_9; }
	inline void set_skillID_9(int32_t value)
	{
		___skillID_9 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_10() { return static_cast<int32_t>(offsetof(U3CPlayShootU3Ec__AnonStorey157_t788163571, ___U3CU3Ef__this_10)); }
	inline EffectMgr_t535289511 * get_U3CU3Ef__this_10() const { return ___U3CU3Ef__this_10; }
	inline EffectMgr_t535289511 ** get_address_of_U3CU3Ef__this_10() { return &___U3CU3Ef__this_10; }
	inline void set_U3CU3Ef__this_10(EffectMgr_t535289511 * value)
	{
		___U3CU3Ef__this_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
