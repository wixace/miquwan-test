﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<Pathfinding.Poly2Tri.TriangulationPoint>
struct List_1_t883301189;
// System.Collections.Generic.List`1<Pathfinding.Poly2Tri.Polygon>
struct List_1_t2415665120;
// System.Collections.Generic.List`1<Pathfinding.Poly2Tri.DelaunayTriangle>
struct List_1_t4203289139;
// Pathfinding.Poly2Tri.PolygonPoint
struct PolygonPoint_t2222827754;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.Poly2Tri.Polygon
struct  Polygon_t1047479568  : public Il2CppObject
{
public:
	// System.Collections.Generic.List`1<Pathfinding.Poly2Tri.TriangulationPoint> Pathfinding.Poly2Tri.Polygon::_points
	List_1_t883301189 * ____points_0;
	// System.Collections.Generic.List`1<Pathfinding.Poly2Tri.TriangulationPoint> Pathfinding.Poly2Tri.Polygon::_steinerPoints
	List_1_t883301189 * ____steinerPoints_1;
	// System.Collections.Generic.List`1<Pathfinding.Poly2Tri.Polygon> Pathfinding.Poly2Tri.Polygon::_holes
	List_1_t2415665120 * ____holes_2;
	// System.Collections.Generic.List`1<Pathfinding.Poly2Tri.DelaunayTriangle> Pathfinding.Poly2Tri.Polygon::_triangles
	List_1_t4203289139 * ____triangles_3;
	// Pathfinding.Poly2Tri.PolygonPoint Pathfinding.Poly2Tri.Polygon::_last
	PolygonPoint_t2222827754 * ____last_4;

public:
	inline static int32_t get_offset_of__points_0() { return static_cast<int32_t>(offsetof(Polygon_t1047479568, ____points_0)); }
	inline List_1_t883301189 * get__points_0() const { return ____points_0; }
	inline List_1_t883301189 ** get_address_of__points_0() { return &____points_0; }
	inline void set__points_0(List_1_t883301189 * value)
	{
		____points_0 = value;
		Il2CppCodeGenWriteBarrier(&____points_0, value);
	}

	inline static int32_t get_offset_of__steinerPoints_1() { return static_cast<int32_t>(offsetof(Polygon_t1047479568, ____steinerPoints_1)); }
	inline List_1_t883301189 * get__steinerPoints_1() const { return ____steinerPoints_1; }
	inline List_1_t883301189 ** get_address_of__steinerPoints_1() { return &____steinerPoints_1; }
	inline void set__steinerPoints_1(List_1_t883301189 * value)
	{
		____steinerPoints_1 = value;
		Il2CppCodeGenWriteBarrier(&____steinerPoints_1, value);
	}

	inline static int32_t get_offset_of__holes_2() { return static_cast<int32_t>(offsetof(Polygon_t1047479568, ____holes_2)); }
	inline List_1_t2415665120 * get__holes_2() const { return ____holes_2; }
	inline List_1_t2415665120 ** get_address_of__holes_2() { return &____holes_2; }
	inline void set__holes_2(List_1_t2415665120 * value)
	{
		____holes_2 = value;
		Il2CppCodeGenWriteBarrier(&____holes_2, value);
	}

	inline static int32_t get_offset_of__triangles_3() { return static_cast<int32_t>(offsetof(Polygon_t1047479568, ____triangles_3)); }
	inline List_1_t4203289139 * get__triangles_3() const { return ____triangles_3; }
	inline List_1_t4203289139 ** get_address_of__triangles_3() { return &____triangles_3; }
	inline void set__triangles_3(List_1_t4203289139 * value)
	{
		____triangles_3 = value;
		Il2CppCodeGenWriteBarrier(&____triangles_3, value);
	}

	inline static int32_t get_offset_of__last_4() { return static_cast<int32_t>(offsetof(Polygon_t1047479568, ____last_4)); }
	inline PolygonPoint_t2222827754 * get__last_4() const { return ____last_4; }
	inline PolygonPoint_t2222827754 ** get_address_of__last_4() { return &____last_4; }
	inline void set__last_4(PolygonPoint_t2222827754 * value)
	{
		____last_4 = value;
		Il2CppCodeGenWriteBarrier(&____last_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
