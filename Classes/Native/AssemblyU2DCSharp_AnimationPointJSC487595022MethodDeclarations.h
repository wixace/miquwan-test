﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AnimationPointJSC
struct AnimationPointJSC_t487595022;
// ProtoBuf.IExtension
struct IExtension_t1606339106;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_AnimationPointJSC487595022.h"

// System.Void AnimationPointJSC::.ctor()
extern "C"  void AnimationPointJSC__ctor_m2435268893 (AnimationPointJSC_t487595022 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.IExtension AnimationPointJSC::ProtoBuf.IExtensible.GetExtensionObject(System.Boolean)
extern "C"  Il2CppObject * AnimationPointJSC_ProtoBuf_IExtensible_GetExtensionObject_m3512276513 (AnimationPointJSC_t487595022 * __this, bool ___createIfMissing0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 AnimationPointJSC::get_Pos()
extern "C"  Vector3_t4282066566  AnimationPointJSC_get_Pos_m2207682912 (AnimationPointJSC_t487595022 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 AnimationPointJSC::get_Rot()
extern "C"  Vector3_t4282066566  AnimationPointJSC_get_Rot_m2209530915 (AnimationPointJSC_t487595022 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 AnimationPointJSC::get_VibrationV3()
extern "C"  Vector3_t4282066566  AnimationPointJSC_get_VibrationV3_m69278203 (AnimationPointJSC_t487595022 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String AnimationPointJSC::get_aniType()
extern "C"  String_t* AnimationPointJSC_get_aniType_m548839691 (AnimationPointJSC_t487595022 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimationPointJSC::set_aniType(System.String)
extern "C"  void AnimationPointJSC_set_aniType_m3613870734 (AnimationPointJSC_t487595022 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single AnimationPointJSC::get_aniTime()
extern "C"  float AnimationPointJSC_get_aniTime_m1916291719 (AnimationPointJSC_t487595022 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimationPointJSC::set_aniTime(System.Single)
extern "C"  void AnimationPointJSC_set_aniTime_m2303521092 (AnimationPointJSC_t487595022 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single AnimationPointJSC::get_px()
extern "C"  float AnimationPointJSC_get_px_m871917036 (AnimationPointJSC_t487595022 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimationPointJSC::set_px(System.Single)
extern "C"  void AnimationPointJSC_set_px_m1114291327 (AnimationPointJSC_t487595022 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single AnimationPointJSC::get_py()
extern "C"  float AnimationPointJSC_get_py_m871917997 (AnimationPointJSC_t487595022 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimationPointJSC::set_py(System.Single)
extern "C"  void AnimationPointJSC_set_py_m603757150 (AnimationPointJSC_t487595022 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single AnimationPointJSC::get_pz()
extern "C"  float AnimationPointJSC_get_pz_m871918958 (AnimationPointJSC_t487595022 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimationPointJSC::set_pz(System.Single)
extern "C"  void AnimationPointJSC_set_pz_m93222973 (AnimationPointJSC_t487595022 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single AnimationPointJSC::get_rx()
extern "C"  float AnimationPointJSC_get_rx_m871976618 (AnimationPointJSC_t487595022 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimationPointJSC::set_rx(System.Single)
extern "C"  void AnimationPointJSC_set_rx_m3820910721 (AnimationPointJSC_t487595022 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single AnimationPointJSC::get_ry()
extern "C"  float AnimationPointJSC_get_ry_m871977579 (AnimationPointJSC_t487595022 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimationPointJSC::set_ry(System.Single)
extern "C"  void AnimationPointJSC_set_ry_m3310376544 (AnimationPointJSC_t487595022 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single AnimationPointJSC::get_rz()
extern "C"  float AnimationPointJSC_get_rz_m871978540 (AnimationPointJSC_t487595022 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimationPointJSC::set_rz(System.Single)
extern "C"  void AnimationPointJSC_set_rz_m2799842367 (AnimationPointJSC_t487595022 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single AnimationPointJSC::get_delay()
extern "C"  float AnimationPointJSC_get_delay_m1073702401 (AnimationPointJSC_t487595022 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimationPointJSC::set_delay(System.Single)
extern "C"  void AnimationPointJSC_set_delay_m4183194634 (AnimationPointJSC_t487595022 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single AnimationPointJSC::get_time()
extern "C"  float AnimationPointJSC_get_time_m496427185 (AnimationPointJSC_t487595022 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimationPointJSC::set_time(System.Single)
extern "C"  void AnimationPointJSC_set_time_m608917978 (AnimationPointJSC_t487595022 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String AnimationPointJSC::get_fun()
extern "C"  String_t* AnimationPointJSC_get_fun_m500095764 (AnimationPointJSC_t487595022 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimationPointJSC::set_fun(System.String)
extern "C"  void AnimationPointJSC_set_fun_m422402725 (AnimationPointJSC_t487595022 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single AnimationPointJSC::get_vx()
extern "C"  float AnimationPointJSC_get_vx_m872095782 (AnimationPointJSC_t487595022 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimationPointJSC::set_vx(System.Single)
extern "C"  void AnimationPointJSC_set_vx_m644214917 (AnimationPointJSC_t487595022 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single AnimationPointJSC::get_vy()
extern "C"  float AnimationPointJSC_get_vy_m872096743 (AnimationPointJSC_t487595022 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimationPointJSC::set_vy(System.Single)
extern "C"  void AnimationPointJSC_set_vy_m133680740 (AnimationPointJSC_t487595022 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single AnimationPointJSC::get_vz()
extern "C"  float AnimationPointJSC_get_vz_m872097704 (AnimationPointJSC_t487595022 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimationPointJSC::set_vz(System.Single)
extern "C"  void AnimationPointJSC_set_vz_m3918113859 (AnimationPointJSC_t487595022 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single AnimationPointJSC::get_vibrationTime()
extern "C"  float AnimationPointJSC_get_vibrationTime_m2959765757 (AnimationPointJSC_t487595022 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimationPointJSC::set_vibrationTime(System.Single)
extern "C"  void AnimationPointJSC_set_vibrationTime_m815491726 (AnimationPointJSC_t487595022 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single AnimationPointJSC::get_vibrationDelay()
extern "C"  float AnimationPointJSC_get_vibrationDelay_m127786805 (AnimationPointJSC_t487595022 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimationPointJSC::set_vibrationDelay(System.Single)
extern "C"  void AnimationPointJSC_set_vibrationDelay_m1997046230 (AnimationPointJSC_t487595022 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single AnimationPointJSC::ilo_get_ry1(AnimationPointJSC)
extern "C"  float AnimationPointJSC_ilo_get_ry1_m1068782223 (Il2CppObject * __this /* static, unused */, AnimationPointJSC_t487595022 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single AnimationPointJSC::ilo_get_rz2(AnimationPointJSC)
extern "C"  float AnimationPointJSC_ilo_get_rz2_m871308911 (Il2CppObject * __this /* static, unused */, AnimationPointJSC_t487595022 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single AnimationPointJSC::ilo_get_vy3(AnimationPointJSC)
extern "C"  float AnimationPointJSC_ilo_get_vy3_m688843209 (Il2CppObject * __this /* static, unused */, AnimationPointJSC_t487595022 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
