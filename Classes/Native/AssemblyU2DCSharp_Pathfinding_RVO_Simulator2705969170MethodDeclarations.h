﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.RVO.Simulator
struct Simulator_t2705969170;
// Pathfinding.RVO.RVOQuadtree
struct RVOQuadtree_t4291712126;
// System.Collections.Generic.List`1<Pathfinding.RVO.Sampled.Agent>
struct List_1_t2658239795;
// System.Collections.Generic.List`1<Pathfinding.RVO.ObstacleVertex>
struct List_1_t1243525355;
// Pathfinding.RVO.IAgent
struct IAgent_t2802767396;
// Pathfinding.RVO.ObstacleVertex
struct ObstacleVertex_t4170307099;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t215400611;
// Pathfinding.RVO.Simulator/Worker
struct Worker_t3927454006;
// Pathfinding.RVO.Sampled.Agent
struct Agent_t1290054243;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_Pathfinding_RVO_ObstacleVertex4170307099.h"
#include "UnityEngine_UnityEngine_Matrix4x41651859333.h"
#include "AssemblyU2DCSharp_Pathfinding_RVO_RVOLayer1386980398.h"
#include "AssemblyU2DCSharp_Pathfinding_RVO_Simulator2705969170.h"
#include "AssemblyU2DCSharp_Pathfinding_RVO_Simulator_Worker3927454006.h"
#include "AssemblyU2DCSharp_Pathfinding_RVO_RVOQuadtree4291712126.h"
#include "AssemblyU2DCSharp_Pathfinding_RVO_Sampled_Agent1290054243.h"

// System.Void Pathfinding.RVO.Simulator::.ctor(System.Int32,System.Boolean)
extern "C"  void Simulator__ctor_m4071300536 (Simulator_t2705969170 * __this, int32_t ___workers0, bool ___doubleBuffering1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.RVO.RVOQuadtree Pathfinding.RVO.Simulator::get_Quadtree()
extern "C"  RVOQuadtree_t4291712126 * Simulator_get_Quadtree_m323660942 (Simulator_t2705969170 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Pathfinding.RVO.Simulator::get_DeltaTime()
extern "C"  float Simulator_get_DeltaTime_m3706154938 (Simulator_t2705969170 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Pathfinding.RVO.Simulator::get_PrevDeltaTime()
extern "C"  float Simulator_get_PrevDeltaTime_m3662830983 (Simulator_t2705969170 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.RVO.Simulator::get_Multithreading()
extern "C"  bool Simulator_get_Multithreading_m91164164 (Simulator_t2705969170 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Pathfinding.RVO.Simulator::get_DesiredDeltaTime()
extern "C"  float Simulator_get_DesiredDeltaTime_m3571489688 (Simulator_t2705969170 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RVO.Simulator::set_DesiredDeltaTime(System.Single)
extern "C"  void Simulator_set_DesiredDeltaTime_m2550364755 (Simulator_t2705969170 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Pathfinding.RVO.Simulator::get_WallThickness()
extern "C"  float Simulator_get_WallThickness_m1305064095 (Simulator_t2705969170 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RVO.Simulator::set_WallThickness(System.Single)
extern "C"  void Simulator_set_WallThickness_m2707444780 (Simulator_t2705969170 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.RVO.Simulator::get_Interpolation()
extern "C"  bool Simulator_get_Interpolation_m2024042721 (Simulator_t2705969170 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RVO.Simulator::set_Interpolation(System.Boolean)
extern "C"  void Simulator_set_Interpolation_m4032729072 (Simulator_t2705969170 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.RVO.Simulator::get_Oversampling()
extern "C"  bool Simulator_get_Oversampling_m1660409920 (Simulator_t2705969170 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RVO.Simulator::set_Oversampling(System.Boolean)
extern "C"  void Simulator_set_Oversampling_m1886767967 (Simulator_t2705969170 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<Pathfinding.RVO.Sampled.Agent> Pathfinding.RVO.Simulator::GetAgents()
extern "C"  List_1_t2658239795 * Simulator_GetAgents_m947919067 (Simulator_t2705969170 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<Pathfinding.RVO.ObstacleVertex> Pathfinding.RVO.Simulator::GetObstacles()
extern "C"  List_1_t1243525355 * Simulator_GetObstacles_m3609151051 (Simulator_t2705969170 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RVO.Simulator::ClearAgents()
extern "C"  void Simulator_ClearAgents_m2093091885 (Simulator_t2705969170 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RVO.Simulator::OnDestroy()
extern "C"  void Simulator_OnDestroy_m339516109 (Simulator_t2705969170 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RVO.Simulator::Finalize()
extern "C"  void Simulator_Finalize_m3776401422 (Simulator_t2705969170 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.RVO.IAgent Pathfinding.RVO.Simulator::AddAgent(Pathfinding.RVO.IAgent)
extern "C"  Il2CppObject * Simulator_AddAgent_m875004567 (Simulator_t2705969170 * __this, Il2CppObject * ___agent0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.RVO.IAgent Pathfinding.RVO.Simulator::AddAgent(UnityEngine.Vector3)
extern "C"  Il2CppObject * Simulator_AddAgent_m2717026713 (Simulator_t2705969170 * __this, Vector3_t4282066566  ___position0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RVO.Simulator::RemoveAgent(Pathfinding.RVO.IAgent)
extern "C"  void Simulator_RemoveAgent_m3223689450 (Simulator_t2705969170 * __this, Il2CppObject * ___agent0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.RVO.ObstacleVertex Pathfinding.RVO.Simulator::AddObstacle(Pathfinding.RVO.ObstacleVertex)
extern "C"  ObstacleVertex_t4170307099 * Simulator_AddObstacle_m4113052415 (Simulator_t2705969170 * __this, ObstacleVertex_t4170307099 * ___v0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.RVO.ObstacleVertex Pathfinding.RVO.Simulator::AddObstacle(UnityEngine.Vector3[],System.Single)
extern "C"  ObstacleVertex_t4170307099 * Simulator_AddObstacle_m3594894333 (Simulator_t2705969170 * __this, Vector3U5BU5D_t215400611* ___vertices0, float ___height1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.RVO.ObstacleVertex Pathfinding.RVO.Simulator::AddObstacle(UnityEngine.Vector3[],System.Single,UnityEngine.Matrix4x4,Pathfinding.RVO.RVOLayer)
extern "C"  ObstacleVertex_t4170307099 * Simulator_AddObstacle_m4214334850 (Simulator_t2705969170 * __this, Vector3U5BU5D_t215400611* ___vertices0, float ___height1, Matrix4x4_t1651859333  ___matrix2, int32_t ___layer3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.RVO.ObstacleVertex Pathfinding.RVO.Simulator::AddObstacle(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern "C"  ObstacleVertex_t4170307099 * Simulator_AddObstacle_m1795063602 (Simulator_t2705969170 * __this, Vector3_t4282066566  ___a0, Vector3_t4282066566  ___b1, float ___height2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RVO.Simulator::UpdateObstacle(Pathfinding.RVO.ObstacleVertex,UnityEngine.Vector3[],UnityEngine.Matrix4x4)
extern "C"  void Simulator_UpdateObstacle_m2092144589 (Simulator_t2705969170 * __this, ObstacleVertex_t4170307099 * ___obstacle0, Vector3U5BU5D_t215400611* ___vertices1, Matrix4x4_t1651859333  ___matrix2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RVO.Simulator::ScheduleCleanObstacles()
extern "C"  void Simulator_ScheduleCleanObstacles_m3761862306 (Simulator_t2705969170 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RVO.Simulator::CleanObstacles()
extern "C"  void Simulator_CleanObstacles_m5197035 (Simulator_t2705969170 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RVO.Simulator::RemoveObstacle(Pathfinding.RVO.ObstacleVertex)
extern "C"  void Simulator_RemoveObstacle_m64826275 (Simulator_t2705969170 * __this, ObstacleVertex_t4170307099 * ___v0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RVO.Simulator::UpdateObstacles()
extern "C"  void Simulator_UpdateObstacles_m2233188845 (Simulator_t2705969170 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RVO.Simulator::BuildQuadtree()
extern "C"  void Simulator_BuildQuadtree_m2238544165 (Simulator_t2705969170 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RVO.Simulator::Update()
extern "C"  void Simulator_Update_m470351289 (Simulator_t2705969170 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.RVO.Simulator::ilo_get_Multithreading1(Pathfinding.RVO.Simulator)
extern "C"  bool Simulator_ilo_get_Multithreading1_m145472927 (Il2CppObject * __this /* static, unused */, Simulator_t2705969170 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RVO.Simulator::ilo_WaitOne2(Pathfinding.RVO.Simulator/Worker)
extern "C"  void Simulator_ilo_WaitOne2_m21038414 (Il2CppObject * __this /* static, unused */, Worker_t3927454006 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RVO.Simulator::ilo_UpdateObstacles3(Pathfinding.RVO.Simulator)
extern "C"  void Simulator_ilo_UpdateObstacles3_m3800692480 (Il2CppObject * __this /* static, unused */, Simulator_t2705969170 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.RVO.ObstacleVertex Pathfinding.RVO.Simulator::ilo_AddObstacle4(Pathfinding.RVO.Simulator,UnityEngine.Vector3[],System.Single,UnityEngine.Matrix4x4,Pathfinding.RVO.RVOLayer)
extern "C"  ObstacleVertex_t4170307099 * Simulator_ilo_AddObstacle4_m2696704696 (Il2CppObject * __this /* static, unused */, Simulator_t2705969170 * ____this0, Vector3U5BU5D_t215400611* ___vertices1, float ___height2, Matrix4x4_t1651859333  ___matrix3, int32_t ___layer4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RVO.Simulator::ilo_Insert5(Pathfinding.RVO.RVOQuadtree,Pathfinding.RVO.Sampled.Agent)
extern "C"  void Simulator_ilo_Insert5_m2268998940 (Il2CppObject * __this /* static, unused */, RVOQuadtree_t4291712126 * ____this0, Agent_t1290054243 * ___agent1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RVO.Simulator::ilo_BuildQuadtree6(Pathfinding.RVO.Simulator)
extern "C"  void Simulator_ilo_BuildQuadtree6_m1184015765 (Il2CppObject * __this /* static, unused */, Simulator_t2705969170 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RVO.Simulator::ilo_Execute7(Pathfinding.RVO.Simulator/Worker,System.Int32)
extern "C"  void Simulator_ilo_Execute7_m3889724008 (Il2CppObject * __this /* static, unused */, Worker_t3927454006 * ____this0, int32_t ___task1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.RVO.Simulator::ilo_get_Interpolation8(Pathfinding.RVO.Simulator)
extern "C"  bool Simulator_ilo_get_Interpolation8_m537632911 (Il2CppObject * __this /* static, unused */, Simulator_t2705969170 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RVO.Simulator::ilo_CalculateNeighbours9(Pathfinding.RVO.Sampled.Agent)
extern "C"  void Simulator_ilo_CalculateNeighbours9_m3758191214 (Il2CppObject * __this /* static, unused */, Agent_t1290054243 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RVO.Simulator::ilo_Interpolate10(Pathfinding.RVO.Sampled.Agent,System.Single)
extern "C"  void Simulator_ilo_Interpolate10_m1244812180 (Il2CppObject * __this /* static, unused */, Agent_t1290054243 * ____this0, float ___t1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Pathfinding.RVO.Simulator::ilo_get_DeltaTime11(Pathfinding.RVO.Simulator)
extern "C"  float Simulator_ilo_get_DeltaTime11_m614147636 (Il2CppObject * __this /* static, unused */, Simulator_t2705969170 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
