﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GlobalGOMgrGenerated
struct GlobalGOMgrGenerated_t1200049634;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// GlobalGOMgr
struct GlobalGOMgr_t803081773;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "AssemblyU2DCSharp_GlobalGOMgr803081773.h"
#include "mscorlib_System_String7231557.h"

// System.Void GlobalGOMgrGenerated::.ctor()
extern "C"  void GlobalGOMgrGenerated__ctor_m392006393 (GlobalGOMgrGenerated_t1200049634 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GlobalGOMgrGenerated::GlobalGOMgr_GlobalGOMgr1(JSVCall,System.Int32)
extern "C"  bool GlobalGOMgrGenerated_GlobalGOMgr_GlobalGOMgr1_m4093538757 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlobalGOMgrGenerated::GlobalGOMgr_separator(JSVCall)
extern "C"  void GlobalGOMgrGenerated_GlobalGOMgr_separator_m3410645917 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlobalGOMgrGenerated::GlobalGOMgr_uiroot(JSVCall)
extern "C"  void GlobalGOMgrGenerated_GlobalGOMgr_uiroot_m2356138740 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlobalGOMgrGenerated::GlobalGOMgr_uiCamera(JSVCall)
extern "C"  void GlobalGOMgrGenerated_GlobalGOMgr_uiCamera_m1978621937 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlobalGOMgrGenerated::GlobalGOMgr_uiNguiCamera(JSVCall)
extern "C"  void GlobalGOMgrGenerated_GlobalGOMgr_uiNguiCamera_m2239828516 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlobalGOMgrGenerated::GlobalGOMgr_uiparent(JSVCall)
extern "C"  void GlobalGOMgrGenerated_GlobalGOMgr_uiparent_m3086010412 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlobalGOMgrGenerated::GlobalGOMgr_uiBy3dCamera(JSVCall)
extern "C"  void GlobalGOMgrGenerated_GlobalGOMgr_uiBy3dCamera_m2149700009 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlobalGOMgrGenerated::GlobalGOMgr_maskGO(JSVCall)
extern "C"  void GlobalGOMgrGenerated_GlobalGOMgr_maskGO_m3960036694 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlobalGOMgrGenerated::GlobalGOMgr_uiby3d(JSVCall)
extern "C"  void GlobalGOMgrGenerated_GlobalGOMgr_uiby3d_m3338594670 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlobalGOMgrGenerated::GlobalGOMgr_herosGO(JSVCall)
extern "C"  void GlobalGOMgrGenerated_GlobalGOMgr_herosGO_m2005162657 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlobalGOMgrGenerated::GlobalGOMgr_SceneCamera(JSVCall)
extern "C"  void GlobalGOMgrGenerated_GlobalGOMgr_SceneCamera_m2821750353 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlobalGOMgrGenerated::GlobalGOMgr_PlayerCamera(JSVCall)
extern "C"  void GlobalGOMgrGenerated_GlobalGOMgr_PlayerCamera_m1672818148 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlobalGOMgrGenerated::GlobalGOMgr_IsClick2D(JSVCall)
extern "C"  void GlobalGOMgrGenerated_GlobalGOMgr_IsClick2D_m1907960242 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlobalGOMgrGenerated::GlobalGOMgr_IsClick3D(JSVCall)
extern "C"  void GlobalGOMgrGenerated_GlobalGOMgr_IsClick3D_m111008883 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GlobalGOMgrGenerated::GlobalGOMgr_ClearLock(JSVCall,System.Int32)
extern "C"  bool GlobalGOMgrGenerated_GlobalGOMgr_ClearLock_m1163195897 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GlobalGOMgrGenerated::GlobalGOMgr_ForeUnLock2DTouchEvent(JSVCall,System.Int32)
extern "C"  bool GlobalGOMgrGenerated_GlobalGOMgr_ForeUnLock2DTouchEvent_m3235097422 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GlobalGOMgrGenerated::GlobalGOMgr_Init(JSVCall,System.Int32)
extern "C"  bool GlobalGOMgrGenerated_GlobalGOMgr_Init_m1511764561 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GlobalGOMgrGenerated::GlobalGOMgr_Lock2DTouchEvent__String(JSVCall,System.Int32)
extern "C"  bool GlobalGOMgrGenerated_GlobalGOMgr_Lock2DTouchEvent__String_m4030496714 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GlobalGOMgrGenerated::GlobalGOMgr_Lock3DTouchEvent__String(JSVCall,System.Int32)
extern "C"  bool GlobalGOMgrGenerated_GlobalGOMgr_Lock3DTouchEvent__String_m4154569961 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GlobalGOMgrGenerated::GlobalGOMgr_PlayOverEffect(JSVCall,System.Int32)
extern "C"  bool GlobalGOMgrGenerated_GlobalGOMgr_PlayOverEffect_m322899546 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GlobalGOMgrGenerated::GlobalGOMgr_Reduction2DTouchEvent(JSVCall,System.Int32)
extern "C"  bool GlobalGOMgrGenerated_GlobalGOMgr_Reduction2DTouchEvent_m240538465 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GlobalGOMgrGenerated::GlobalGOMgr_Reset(JSVCall,System.Int32)
extern "C"  bool GlobalGOMgrGenerated_GlobalGOMgr_Reset_m2971756272 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GlobalGOMgrGenerated::GlobalGOMgr_SceneLoadCompleteInit__Int32(JSVCall,System.Int32)
extern "C"  bool GlobalGOMgrGenerated_GlobalGOMgr_SceneLoadCompleteInit__Int32_m1688266900 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GlobalGOMgrGenerated::GlobalGOMgr_UnLock2DTouchEvent__String(JSVCall,System.Int32)
extern "C"  bool GlobalGOMgrGenerated_GlobalGOMgr_UnLock2DTouchEvent__String_m1791617731 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GlobalGOMgrGenerated::GlobalGOMgr_UnLock2DTouchEvent__Object(JSVCall,System.Int32)
extern "C"  bool GlobalGOMgrGenerated_GlobalGOMgr_UnLock2DTouchEvent__Object_m1891268465 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GlobalGOMgrGenerated::GlobalGOMgr_UnLock3DTouchEvent__String(JSVCall,System.Int32)
extern "C"  bool GlobalGOMgrGenerated_GlobalGOMgr_UnLock3DTouchEvent__String_m1915690978 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GlobalGOMgrGenerated::GlobalGOMgr_UnLock3DTouchEvent__Object(JSVCall,System.Int32)
extern "C"  bool GlobalGOMgrGenerated_GlobalGOMgr_UnLock3DTouchEvent__Object_m2015341712 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlobalGOMgrGenerated::__Register()
extern "C"  void GlobalGOMgrGenerated___Register_m2249254254 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 GlobalGOMgrGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t GlobalGOMgrGenerated_ilo_getObject1_m3610174137 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlobalGOMgrGenerated::ilo_addJSCSRel2(System.Int32,System.Object)
extern "C"  void GlobalGOMgrGenerated_ilo_addJSCSRel2_m2046606774 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlobalGOMgrGenerated::ilo_setChar3(System.Int32,System.Char)
extern "C"  void GlobalGOMgrGenerated_ilo_setChar3_m4144629889 (Il2CppObject * __this /* static, unused */, int32_t ___e0, Il2CppChar ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 GlobalGOMgrGenerated::ilo_setObject4(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t GlobalGOMgrGenerated_ilo_setObject4_m2408172788 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GlobalGOMgrGenerated::ilo_getObject5(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * GlobalGOMgrGenerated_ilo_getObject5_m3467522650 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GlobalGOMgrGenerated::ilo_get_IsClick2D6(GlobalGOMgr)
extern "C"  bool GlobalGOMgrGenerated_ilo_get_IsClick2D6_m11769732 (Il2CppObject * __this /* static, unused */, GlobalGOMgr_t803081773 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GlobalGOMgrGenerated::ilo_getStringS7(System.Int32)
extern "C"  String_t* GlobalGOMgrGenerated_ilo_getStringS7_m1308299773 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlobalGOMgrGenerated::ilo_Lock2DTouchEvent8(GlobalGOMgr,System.String)
extern "C"  void GlobalGOMgrGenerated_ilo_Lock2DTouchEvent8_m3041107037 (Il2CppObject * __this /* static, unused */, GlobalGOMgr_t803081773 * ____this0, String_t* ___key1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GlobalGOMgrGenerated::ilo_getWhatever9(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * GlobalGOMgrGenerated_ilo_getWhatever9_m256892187 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
