﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.GameObject>
struct Dictionary_2_t3671945244;
// UnityEngine.Camera
struct Camera_t2727095145;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIModelDisplay
struct  UIModelDisplay_t1730520589  : public Il2CppObject
{
public:
	// UnityEngine.GameObject UIModelDisplay::root
	GameObject_t3674682005 * ___root_0;
	// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.GameObject> UIModelDisplay::modelList
	Dictionary_2_t3671945244 * ___modelList_1;
	// UnityEngine.Camera UIModelDisplay::camera
	Camera_t2727095145 * ___camera_2;
	// UnityEngine.GameObject UIModelDisplay::light
	GameObject_t3674682005 * ___light_3;
	// System.Int32 UIModelDisplay::go_layer
	int32_t ___go_layer_4;
	// System.Int32 UIModelDisplay::camera_depth
	int32_t ___camera_depth_5;
	// System.Int32 UIModelDisplay::attach
	int32_t ___attach_6;
	// System.Boolean UIModelDisplay::<LightState>k__BackingField
	bool ___U3CLightStateU3Ek__BackingField_7;

public:
	inline static int32_t get_offset_of_root_0() { return static_cast<int32_t>(offsetof(UIModelDisplay_t1730520589, ___root_0)); }
	inline GameObject_t3674682005 * get_root_0() const { return ___root_0; }
	inline GameObject_t3674682005 ** get_address_of_root_0() { return &___root_0; }
	inline void set_root_0(GameObject_t3674682005 * value)
	{
		___root_0 = value;
		Il2CppCodeGenWriteBarrier(&___root_0, value);
	}

	inline static int32_t get_offset_of_modelList_1() { return static_cast<int32_t>(offsetof(UIModelDisplay_t1730520589, ___modelList_1)); }
	inline Dictionary_2_t3671945244 * get_modelList_1() const { return ___modelList_1; }
	inline Dictionary_2_t3671945244 ** get_address_of_modelList_1() { return &___modelList_1; }
	inline void set_modelList_1(Dictionary_2_t3671945244 * value)
	{
		___modelList_1 = value;
		Il2CppCodeGenWriteBarrier(&___modelList_1, value);
	}

	inline static int32_t get_offset_of_camera_2() { return static_cast<int32_t>(offsetof(UIModelDisplay_t1730520589, ___camera_2)); }
	inline Camera_t2727095145 * get_camera_2() const { return ___camera_2; }
	inline Camera_t2727095145 ** get_address_of_camera_2() { return &___camera_2; }
	inline void set_camera_2(Camera_t2727095145 * value)
	{
		___camera_2 = value;
		Il2CppCodeGenWriteBarrier(&___camera_2, value);
	}

	inline static int32_t get_offset_of_light_3() { return static_cast<int32_t>(offsetof(UIModelDisplay_t1730520589, ___light_3)); }
	inline GameObject_t3674682005 * get_light_3() const { return ___light_3; }
	inline GameObject_t3674682005 ** get_address_of_light_3() { return &___light_3; }
	inline void set_light_3(GameObject_t3674682005 * value)
	{
		___light_3 = value;
		Il2CppCodeGenWriteBarrier(&___light_3, value);
	}

	inline static int32_t get_offset_of_go_layer_4() { return static_cast<int32_t>(offsetof(UIModelDisplay_t1730520589, ___go_layer_4)); }
	inline int32_t get_go_layer_4() const { return ___go_layer_4; }
	inline int32_t* get_address_of_go_layer_4() { return &___go_layer_4; }
	inline void set_go_layer_4(int32_t value)
	{
		___go_layer_4 = value;
	}

	inline static int32_t get_offset_of_camera_depth_5() { return static_cast<int32_t>(offsetof(UIModelDisplay_t1730520589, ___camera_depth_5)); }
	inline int32_t get_camera_depth_5() const { return ___camera_depth_5; }
	inline int32_t* get_address_of_camera_depth_5() { return &___camera_depth_5; }
	inline void set_camera_depth_5(int32_t value)
	{
		___camera_depth_5 = value;
	}

	inline static int32_t get_offset_of_attach_6() { return static_cast<int32_t>(offsetof(UIModelDisplay_t1730520589, ___attach_6)); }
	inline int32_t get_attach_6() const { return ___attach_6; }
	inline int32_t* get_address_of_attach_6() { return &___attach_6; }
	inline void set_attach_6(int32_t value)
	{
		___attach_6 = value;
	}

	inline static int32_t get_offset_of_U3CLightStateU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(UIModelDisplay_t1730520589, ___U3CLightStateU3Ek__BackingField_7)); }
	inline bool get_U3CLightStateU3Ek__BackingField_7() const { return ___U3CLightStateU3Ek__BackingField_7; }
	inline bool* get_address_of_U3CLightStateU3Ek__BackingField_7() { return &___U3CLightStateU3Ek__BackingField_7; }
	inline void set_U3CLightStateU3Ek__BackingField_7(bool value)
	{
		___U3CLightStateU3Ek__BackingField_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
