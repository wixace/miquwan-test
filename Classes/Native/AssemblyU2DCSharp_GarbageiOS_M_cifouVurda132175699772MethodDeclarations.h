﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_cifouVurda13
struct M_cifouVurda13_t2175699772;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"

// System.Void GarbageiOS.M_cifouVurda13::.ctor()
extern "C"  void M_cifouVurda13__ctor_m1437698679 (M_cifouVurda13_t2175699772 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_cifouVurda13::M_mearmear0(System.String[],System.Int32)
extern "C"  void M_cifouVurda13_M_mearmear0_m141263704 (M_cifouVurda13_t2175699772 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
