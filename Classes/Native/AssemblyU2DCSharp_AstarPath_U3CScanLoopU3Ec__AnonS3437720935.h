﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// AstarPath/<ScanLoop>c__AnonStorey106
struct U3CScanLoopU3Ec__AnonStorey106_t3437720936;
// AstarPath/<ScanLoop>c__AnonStorey107
struct U3CScanLoopU3Ec__AnonStorey107_t3437720937;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AstarPath/<ScanLoop>c__AnonStorey105
struct  U3CScanLoopU3Ec__AnonStorey105_t3437720935  : public Il2CppObject
{
public:
	// System.Single AstarPath/<ScanLoop>c__AnonStorey105::minp
	float ___minp_0;
	// System.Single AstarPath/<ScanLoop>c__AnonStorey105::maxp
	float ___maxp_1;
	// AstarPath/<ScanLoop>c__AnonStorey106 AstarPath/<ScanLoop>c__AnonStorey105::<>f__ref$262
	U3CScanLoopU3Ec__AnonStorey106_t3437720936 * ___U3CU3Ef__refU24262_2;
	// AstarPath/<ScanLoop>c__AnonStorey107 AstarPath/<ScanLoop>c__AnonStorey105::<>f__ref$263
	U3CScanLoopU3Ec__AnonStorey107_t3437720937 * ___U3CU3Ef__refU24263_3;

public:
	inline static int32_t get_offset_of_minp_0() { return static_cast<int32_t>(offsetof(U3CScanLoopU3Ec__AnonStorey105_t3437720935, ___minp_0)); }
	inline float get_minp_0() const { return ___minp_0; }
	inline float* get_address_of_minp_0() { return &___minp_0; }
	inline void set_minp_0(float value)
	{
		___minp_0 = value;
	}

	inline static int32_t get_offset_of_maxp_1() { return static_cast<int32_t>(offsetof(U3CScanLoopU3Ec__AnonStorey105_t3437720935, ___maxp_1)); }
	inline float get_maxp_1() const { return ___maxp_1; }
	inline float* get_address_of_maxp_1() { return &___maxp_1; }
	inline void set_maxp_1(float value)
	{
		___maxp_1 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU24262_2() { return static_cast<int32_t>(offsetof(U3CScanLoopU3Ec__AnonStorey105_t3437720935, ___U3CU3Ef__refU24262_2)); }
	inline U3CScanLoopU3Ec__AnonStorey106_t3437720936 * get_U3CU3Ef__refU24262_2() const { return ___U3CU3Ef__refU24262_2; }
	inline U3CScanLoopU3Ec__AnonStorey106_t3437720936 ** get_address_of_U3CU3Ef__refU24262_2() { return &___U3CU3Ef__refU24262_2; }
	inline void set_U3CU3Ef__refU24262_2(U3CScanLoopU3Ec__AnonStorey106_t3437720936 * value)
	{
		___U3CU3Ef__refU24262_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU24262_2, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU24263_3() { return static_cast<int32_t>(offsetof(U3CScanLoopU3Ec__AnonStorey105_t3437720935, ___U3CU3Ef__refU24263_3)); }
	inline U3CScanLoopU3Ec__AnonStorey107_t3437720937 * get_U3CU3Ef__refU24263_3() const { return ___U3CU3Ef__refU24263_3; }
	inline U3CScanLoopU3Ec__AnonStorey107_t3437720937 ** get_address_of_U3CU3Ef__refU24263_3() { return &___U3CU3Ef__refU24263_3; }
	inline void set_U3CU3Ef__refU24263_3(U3CScanLoopU3Ec__AnonStorey107_t3437720937 * value)
	{
		___U3CU3Ef__refU24263_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU24263_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
