﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_nouho55
struct M_nouho55_t344361979;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_nouho55344361979.h"

// System.Void GarbageiOS.M_nouho55::.ctor()
extern "C"  void M_nouho55__ctor_m3008955848 (M_nouho55_t344361979 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_nouho55::M_baicharTearce0(System.String[],System.Int32)
extern "C"  void M_nouho55_M_baicharTearce0_m1673199793 (M_nouho55_t344361979 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_nouho55::M_derjuJiji1(System.String[],System.Int32)
extern "C"  void M_nouho55_M_derjuJiji1_m835940220 (M_nouho55_t344361979 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_nouho55::M_dicou2(System.String[],System.Int32)
extern "C"  void M_nouho55_M_dicou2_m2057739283 (M_nouho55_t344361979 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_nouho55::ilo_M_baicharTearce01(GarbageiOS.M_nouho55,System.String[],System.Int32)
extern "C"  void M_nouho55_ilo_M_baicharTearce01_m779231978 (Il2CppObject * __this /* static, unused */, M_nouho55_t344361979 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_nouho55::ilo_M_derjuJiji12(GarbageiOS.M_nouho55,System.String[],System.Int32)
extern "C"  void M_nouho55_ilo_M_derjuJiji12_m2646000468 (Il2CppObject * __this /* static, unused */, M_nouho55_t344361979 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
