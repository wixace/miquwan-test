﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Gyroscope
struct Gyroscope_t932197883;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882.h"

// System.Void UnityEngine.Gyroscope::.ctor(System.Int32)
extern "C"  void Gyroscope__ctor_m2665436357 (Gyroscope_t932197883 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Gyroscope::rotationRate_Internal(System.Int32)
extern "C"  Vector3_t4282066566  Gyroscope_rotationRate_Internal_m598485243 (Il2CppObject * __this /* static, unused */, int32_t ___idx0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gyroscope::INTERNAL_CALL_rotationRate_Internal(System.Int32,UnityEngine.Vector3&)
extern "C"  void Gyroscope_INTERNAL_CALL_rotationRate_Internal_m3505756855 (Il2CppObject * __this /* static, unused */, int32_t ___idx0, Vector3_t4282066566 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Gyroscope::rotationRateUnbiased_Internal(System.Int32)
extern "C"  Vector3_t4282066566  Gyroscope_rotationRateUnbiased_Internal_m3841099722 (Il2CppObject * __this /* static, unused */, int32_t ___idx0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gyroscope::INTERNAL_CALL_rotationRateUnbiased_Internal(System.Int32,UnityEngine.Vector3&)
extern "C"  void Gyroscope_INTERNAL_CALL_rotationRateUnbiased_Internal_m1444452936 (Il2CppObject * __this /* static, unused */, int32_t ___idx0, Vector3_t4282066566 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Gyroscope::gravity_Internal(System.Int32)
extern "C"  Vector3_t4282066566  Gyroscope_gravity_Internal_m1194710773 (Il2CppObject * __this /* static, unused */, int32_t ___idx0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gyroscope::INTERNAL_CALL_gravity_Internal(System.Int32,UnityEngine.Vector3&)
extern "C"  void Gyroscope_INTERNAL_CALL_gravity_Internal_m4139439339 (Il2CppObject * __this /* static, unused */, int32_t ___idx0, Vector3_t4282066566 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Gyroscope::userAcceleration_Internal(System.Int32)
extern "C"  Vector3_t4282066566  Gyroscope_userAcceleration_Internal_m3877852782 (Il2CppObject * __this /* static, unused */, int32_t ___idx0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gyroscope::INTERNAL_CALL_userAcceleration_Internal(System.Int32,UnityEngine.Vector3&)
extern "C"  void Gyroscope_INTERNAL_CALL_userAcceleration_Internal_m1589943972 (Il2CppObject * __this /* static, unused */, int32_t ___idx0, Vector3_t4282066566 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Gyroscope::attitude_Internal(System.Int32)
extern "C"  Quaternion_t1553702882  Gyroscope_attitude_Internal_m315730319 (Il2CppObject * __this /* static, unused */, int32_t ___idx0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gyroscope::INTERNAL_CALL_attitude_Internal(System.Int32,UnityEngine.Quaternion&)
extern "C"  void Gyroscope_INTERNAL_CALL_attitude_Internal_m2063094673 (Il2CppObject * __this /* static, unused */, int32_t ___idx0, Quaternion_t1553702882 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Gyroscope::getEnabled_Internal(System.Int32)
extern "C"  bool Gyroscope_getEnabled_Internal_m1272567802 (Il2CppObject * __this /* static, unused */, int32_t ___idx0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gyroscope::setEnabled_Internal(System.Int32,System.Boolean)
extern "C"  void Gyroscope_setEnabled_Internal_m1708627037 (Il2CppObject * __this /* static, unused */, int32_t ___idx0, bool ___enabled1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Gyroscope::getUpdateInterval_Internal(System.Int32)
extern "C"  float Gyroscope_getUpdateInterval_Internal_m1100291533 (Il2CppObject * __this /* static, unused */, int32_t ___idx0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gyroscope::setUpdateInterval_Internal(System.Int32,System.Single)
extern "C"  void Gyroscope_setUpdateInterval_Internal_m4238484722 (Il2CppObject * __this /* static, unused */, int32_t ___idx0, float ___interval1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Gyroscope::get_rotationRate()
extern "C"  Vector3_t4282066566  Gyroscope_get_rotationRate_m371559293 (Gyroscope_t932197883 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Gyroscope::get_rotationRateUnbiased()
extern "C"  Vector3_t4282066566  Gyroscope_get_rotationRateUnbiased_m3101364302 (Gyroscope_t932197883 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Gyroscope::get_gravity()
extern "C"  Vector3_t4282066566  Gyroscope_get_gravity_m2609559697 (Gyroscope_t932197883 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Gyroscope::get_userAcceleration()
extern "C"  Vector3_t4282066566  Gyroscope_get_userAcceleration_m1302564778 (Gyroscope_t932197883 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Gyroscope::get_attitude()
extern "C"  Quaternion_t1553702882  Gyroscope_get_attitude_m822764265 (Gyroscope_t932197883 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Gyroscope::get_enabled()
extern "C"  bool Gyroscope_get_enabled_m1656767280 (Gyroscope_t932197883 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gyroscope::set_enabled(System.Boolean)
extern "C"  void Gyroscope_set_enabled_m4165844045 (Gyroscope_t932197883 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Gyroscope::get_updateInterval()
extern "C"  float Gyroscope_get_updateInterval_m662477659 (Gyroscope_t932197883 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gyroscope::set_updateInterval(System.Single)
extern "C"  void Gyroscope_set_updateInterval_m2062641968 (Gyroscope_t932197883 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
