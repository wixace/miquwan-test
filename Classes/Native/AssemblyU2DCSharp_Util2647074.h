﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t1974256870;
// System.Action
struct Action_t3771233898;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Util
struct  Util_t2647074  : public Il2CppObject
{
public:

public:
};

struct Util_t2647074_StaticFields
{
public:
	// System.Boolean Util::panelUp
	bool ___panelUp_0;
	// System.Boolean Util::panelDown
	bool ___panelDown_1;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Util::<>f__switch$map1E
	Dictionary_2_t1974256870 * ___U3CU3Ef__switchU24map1E_2;
	// System.Action Util::<>f__am$cache3
	Action_t3771233898 * ___U3CU3Ef__amU24cache3_3;
	// System.Action Util::<>f__am$cache4
	Action_t3771233898 * ___U3CU3Ef__amU24cache4_4;

public:
	inline static int32_t get_offset_of_panelUp_0() { return static_cast<int32_t>(offsetof(Util_t2647074_StaticFields, ___panelUp_0)); }
	inline bool get_panelUp_0() const { return ___panelUp_0; }
	inline bool* get_address_of_panelUp_0() { return &___panelUp_0; }
	inline void set_panelUp_0(bool value)
	{
		___panelUp_0 = value;
	}

	inline static int32_t get_offset_of_panelDown_1() { return static_cast<int32_t>(offsetof(Util_t2647074_StaticFields, ___panelDown_1)); }
	inline bool get_panelDown_1() const { return ___panelDown_1; }
	inline bool* get_address_of_panelDown_1() { return &___panelDown_1; }
	inline void set_panelDown_1(bool value)
	{
		___panelDown_1 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map1E_2() { return static_cast<int32_t>(offsetof(Util_t2647074_StaticFields, ___U3CU3Ef__switchU24map1E_2)); }
	inline Dictionary_2_t1974256870 * get_U3CU3Ef__switchU24map1E_2() const { return ___U3CU3Ef__switchU24map1E_2; }
	inline Dictionary_2_t1974256870 ** get_address_of_U3CU3Ef__switchU24map1E_2() { return &___U3CU3Ef__switchU24map1E_2; }
	inline void set_U3CU3Ef__switchU24map1E_2(Dictionary_2_t1974256870 * value)
	{
		___U3CU3Ef__switchU24map1E_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map1E_2, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_3() { return static_cast<int32_t>(offsetof(Util_t2647074_StaticFields, ___U3CU3Ef__amU24cache3_3)); }
	inline Action_t3771233898 * get_U3CU3Ef__amU24cache3_3() const { return ___U3CU3Ef__amU24cache3_3; }
	inline Action_t3771233898 ** get_address_of_U3CU3Ef__amU24cache3_3() { return &___U3CU3Ef__amU24cache3_3; }
	inline void set_U3CU3Ef__amU24cache3_3(Action_t3771233898 * value)
	{
		___U3CU3Ef__amU24cache3_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache3_3, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache4_4() { return static_cast<int32_t>(offsetof(Util_t2647074_StaticFields, ___U3CU3Ef__amU24cache4_4)); }
	inline Action_t3771233898 * get_U3CU3Ef__amU24cache4_4() const { return ___U3CU3Ef__amU24cache4_4; }
	inline Action_t3771233898 ** get_address_of_U3CU3Ef__amU24cache4_4() { return &___U3CU3Ef__amU24cache4_4; }
	inline void set_U3CU3Ef__amU24cache4_4(Action_t3771233898 * value)
	{
		___U3CU3Ef__amU24cache4_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache4_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
