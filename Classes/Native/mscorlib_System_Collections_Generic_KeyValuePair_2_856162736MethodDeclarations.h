﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21352297102MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<Pathfinding.Int3,UnityEngine.GameObject>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m10574889(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t856162736 *, Int3_t1974045594 , GameObject_t3674682005 *, const MethodInfo*))KeyValuePair_2__ctor_m1445654159_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<Pathfinding.Int3,UnityEngine.GameObject>::get_Key()
#define KeyValuePair_2_get_Key_m3994387743(__this, method) ((  Int3_t1974045594  (*) (KeyValuePair_2_t856162736 *, const MethodInfo*))KeyValuePair_2_get_Key_m680403065_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Pathfinding.Int3,UnityEngine.GameObject>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m2358069344(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t856162736 *, Int3_t1974045594 , const MethodInfo*))KeyValuePair_2_set_Key_m444550714_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<Pathfinding.Int3,UnityEngine.GameObject>::get_Value()
#define KeyValuePair_2_get_Value_m3096491779(__this, method) ((  GameObject_t3674682005 * (*) (KeyValuePair_2_t856162736 *, const MethodInfo*))KeyValuePair_2_get_Value_m861079929_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Pathfinding.Int3,UnityEngine.GameObject>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m1850567776(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t856162736 *, GameObject_t3674682005 *, const MethodInfo*))KeyValuePair_2_set_Value_m95745338_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<Pathfinding.Int3,UnityEngine.GameObject>::ToString()
#define KeyValuePair_2_ToString_m1199538728(__this, method) ((  String_t* (*) (KeyValuePair_2_t856162736 *, const MethodInfo*))KeyValuePair_2_ToString_m3407486440_gshared)(__this, method)
