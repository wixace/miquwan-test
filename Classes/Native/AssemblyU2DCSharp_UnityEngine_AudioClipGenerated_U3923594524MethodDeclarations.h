﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_AudioClipGenerated/<AudioClip_Create_GetDelegate_member5_arg5>c__AnonStoreyDD
struct U3CAudioClip_Create_GetDelegate_member5_arg5U3Ec__AnonStoreyDD_t3923594524;
// System.Single[]
struct SingleU5BU5D_t2316563989;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine_AudioClipGenerated/<AudioClip_Create_GetDelegate_member5_arg5>c__AnonStoreyDD::.ctor()
extern "C"  void U3CAudioClip_Create_GetDelegate_member5_arg5U3Ec__AnonStoreyDD__ctor_m334149583 (U3CAudioClip_Create_GetDelegate_member5_arg5U3Ec__AnonStoreyDD_t3923594524 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioClipGenerated/<AudioClip_Create_GetDelegate_member5_arg5>c__AnonStoreyDD::<>m__18A(System.Single[])
extern "C"  void U3CAudioClip_Create_GetDelegate_member5_arg5U3Ec__AnonStoreyDD_U3CU3Em__18A_m2175889157 (U3CAudioClip_Create_GetDelegate_member5_arg5U3Ec__AnonStoreyDD_t3923594524 * __this, SingleU5BU5D_t2316563989* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
