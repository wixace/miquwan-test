﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_pemsawsawPigaybal67
struct  M_pemsawsawPigaybal67_t3836434144  : public Il2CppObject
{
public:
	// System.UInt32 GarbageiOS.M_pemsawsawPigaybal67::_nerenu
	uint32_t ____nerenu_0;
	// System.Single GarbageiOS.M_pemsawsawPigaybal67::_releder
	float ____releder_1;
	// System.UInt32 GarbageiOS.M_pemsawsawPigaybal67::_guroSoloopa
	uint32_t ____guroSoloopa_2;
	// System.Int32 GarbageiOS.M_pemsawsawPigaybal67::_rerutasBelwou
	int32_t ____rerutasBelwou_3;
	// System.Int32 GarbageiOS.M_pemsawsawPigaybal67::_baslawRojear
	int32_t ____baslawRojear_4;
	// System.UInt32 GarbageiOS.M_pemsawsawPigaybal67::_feenairFeqall
	uint32_t ____feenairFeqall_5;
	// System.Single GarbageiOS.M_pemsawsawPigaybal67::_whaipal
	float ____whaipal_6;
	// System.Int32 GarbageiOS.M_pemsawsawPigaybal67::_hecor
	int32_t ____hecor_7;
	// System.String GarbageiOS.M_pemsawsawPigaybal67::_vipo
	String_t* ____vipo_8;
	// System.UInt32 GarbageiOS.M_pemsawsawPigaybal67::_fowlallbou
	uint32_t ____fowlallbou_9;

public:
	inline static int32_t get_offset_of__nerenu_0() { return static_cast<int32_t>(offsetof(M_pemsawsawPigaybal67_t3836434144, ____nerenu_0)); }
	inline uint32_t get__nerenu_0() const { return ____nerenu_0; }
	inline uint32_t* get_address_of__nerenu_0() { return &____nerenu_0; }
	inline void set__nerenu_0(uint32_t value)
	{
		____nerenu_0 = value;
	}

	inline static int32_t get_offset_of__releder_1() { return static_cast<int32_t>(offsetof(M_pemsawsawPigaybal67_t3836434144, ____releder_1)); }
	inline float get__releder_1() const { return ____releder_1; }
	inline float* get_address_of__releder_1() { return &____releder_1; }
	inline void set__releder_1(float value)
	{
		____releder_1 = value;
	}

	inline static int32_t get_offset_of__guroSoloopa_2() { return static_cast<int32_t>(offsetof(M_pemsawsawPigaybal67_t3836434144, ____guroSoloopa_2)); }
	inline uint32_t get__guroSoloopa_2() const { return ____guroSoloopa_2; }
	inline uint32_t* get_address_of__guroSoloopa_2() { return &____guroSoloopa_2; }
	inline void set__guroSoloopa_2(uint32_t value)
	{
		____guroSoloopa_2 = value;
	}

	inline static int32_t get_offset_of__rerutasBelwou_3() { return static_cast<int32_t>(offsetof(M_pemsawsawPigaybal67_t3836434144, ____rerutasBelwou_3)); }
	inline int32_t get__rerutasBelwou_3() const { return ____rerutasBelwou_3; }
	inline int32_t* get_address_of__rerutasBelwou_3() { return &____rerutasBelwou_3; }
	inline void set__rerutasBelwou_3(int32_t value)
	{
		____rerutasBelwou_3 = value;
	}

	inline static int32_t get_offset_of__baslawRojear_4() { return static_cast<int32_t>(offsetof(M_pemsawsawPigaybal67_t3836434144, ____baslawRojear_4)); }
	inline int32_t get__baslawRojear_4() const { return ____baslawRojear_4; }
	inline int32_t* get_address_of__baslawRojear_4() { return &____baslawRojear_4; }
	inline void set__baslawRojear_4(int32_t value)
	{
		____baslawRojear_4 = value;
	}

	inline static int32_t get_offset_of__feenairFeqall_5() { return static_cast<int32_t>(offsetof(M_pemsawsawPigaybal67_t3836434144, ____feenairFeqall_5)); }
	inline uint32_t get__feenairFeqall_5() const { return ____feenairFeqall_5; }
	inline uint32_t* get_address_of__feenairFeqall_5() { return &____feenairFeqall_5; }
	inline void set__feenairFeqall_5(uint32_t value)
	{
		____feenairFeqall_5 = value;
	}

	inline static int32_t get_offset_of__whaipal_6() { return static_cast<int32_t>(offsetof(M_pemsawsawPigaybal67_t3836434144, ____whaipal_6)); }
	inline float get__whaipal_6() const { return ____whaipal_6; }
	inline float* get_address_of__whaipal_6() { return &____whaipal_6; }
	inline void set__whaipal_6(float value)
	{
		____whaipal_6 = value;
	}

	inline static int32_t get_offset_of__hecor_7() { return static_cast<int32_t>(offsetof(M_pemsawsawPigaybal67_t3836434144, ____hecor_7)); }
	inline int32_t get__hecor_7() const { return ____hecor_7; }
	inline int32_t* get_address_of__hecor_7() { return &____hecor_7; }
	inline void set__hecor_7(int32_t value)
	{
		____hecor_7 = value;
	}

	inline static int32_t get_offset_of__vipo_8() { return static_cast<int32_t>(offsetof(M_pemsawsawPigaybal67_t3836434144, ____vipo_8)); }
	inline String_t* get__vipo_8() const { return ____vipo_8; }
	inline String_t** get_address_of__vipo_8() { return &____vipo_8; }
	inline void set__vipo_8(String_t* value)
	{
		____vipo_8 = value;
		Il2CppCodeGenWriteBarrier(&____vipo_8, value);
	}

	inline static int32_t get_offset_of__fowlallbou_9() { return static_cast<int32_t>(offsetof(M_pemsawsawPigaybal67_t3836434144, ____fowlallbou_9)); }
	inline uint32_t get__fowlallbou_9() const { return ____fowlallbou_9; }
	inline uint32_t* get_address_of__fowlallbou_9() { return &____fowlallbou_9; }
	inline void set__fowlallbou_9(uint32_t value)
	{
		____fowlallbou_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
