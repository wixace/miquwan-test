﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Pathfinding.RVO.Simulator/Worker[]
struct WorkerU5BU5D_t2291287347;
// System.Collections.Generic.List`1<Pathfinding.RVO.Sampled.Agent>
struct List_1_t2658239795;
// System.Collections.Generic.List`1<Pathfinding.RVO.ObstacleVertex>
struct List_1_t1243525355;
// Pathfinding.RVO.RVOQuadtree
struct RVOQuadtree_t4291712126;
// Pathfinding.RVO.Simulator/WorkerContext
struct WorkerContext_t2850943897;

#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_Pathfinding_RVO_Simulator_Samplin259839888.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.RVO.Simulator
struct  Simulator_t2705969170  : public Il2CppObject
{
public:
	// System.Boolean Pathfinding.RVO.Simulator::doubleBuffering
	bool ___doubleBuffering_0;
	// System.Single Pathfinding.RVO.Simulator::desiredDeltaTime
	float ___desiredDeltaTime_1;
	// System.Boolean Pathfinding.RVO.Simulator::interpolation
	bool ___interpolation_2;
	// Pathfinding.RVO.Simulator/Worker[] Pathfinding.RVO.Simulator::workers
	WorkerU5BU5D_t2291287347* ___workers_3;
	// System.Collections.Generic.List`1<Pathfinding.RVO.Sampled.Agent> Pathfinding.RVO.Simulator::agents
	List_1_t2658239795 * ___agents_4;
	// System.Collections.Generic.List`1<Pathfinding.RVO.ObstacleVertex> Pathfinding.RVO.Simulator::obstacles
	List_1_t1243525355 * ___obstacles_5;
	// Pathfinding.RVO.Simulator/SamplingAlgorithm Pathfinding.RVO.Simulator::algorithm
	int32_t ___algorithm_6;
	// Pathfinding.RVO.RVOQuadtree Pathfinding.RVO.Simulator::quadtree
	RVOQuadtree_t4291712126 * ___quadtree_7;
	// System.Single Pathfinding.RVO.Simulator::qualityCutoff
	float ___qualityCutoff_8;
	// System.Single Pathfinding.RVO.Simulator::stepScale
	float ___stepScale_9;
	// System.Single Pathfinding.RVO.Simulator::deltaTime
	float ___deltaTime_10;
	// System.Single Pathfinding.RVO.Simulator::prevDeltaTime
	float ___prevDeltaTime_11;
	// System.Single Pathfinding.RVO.Simulator::lastStep
	float ___lastStep_12;
	// System.Single Pathfinding.RVO.Simulator::lastStepInterpolationReference
	float ___lastStepInterpolationReference_13;
	// System.Boolean Pathfinding.RVO.Simulator::doUpdateObstacles
	bool ___doUpdateObstacles_14;
	// System.Boolean Pathfinding.RVO.Simulator::doCleanObstacles
	bool ___doCleanObstacles_15;
	// System.Boolean Pathfinding.RVO.Simulator::oversampling
	bool ___oversampling_16;
	// System.Single Pathfinding.RVO.Simulator::wallThickness
	float ___wallThickness_17;
	// Pathfinding.RVO.Simulator/WorkerContext Pathfinding.RVO.Simulator::coroutineWorkerContext
	WorkerContext_t2850943897 * ___coroutineWorkerContext_18;

public:
	inline static int32_t get_offset_of_doubleBuffering_0() { return static_cast<int32_t>(offsetof(Simulator_t2705969170, ___doubleBuffering_0)); }
	inline bool get_doubleBuffering_0() const { return ___doubleBuffering_0; }
	inline bool* get_address_of_doubleBuffering_0() { return &___doubleBuffering_0; }
	inline void set_doubleBuffering_0(bool value)
	{
		___doubleBuffering_0 = value;
	}

	inline static int32_t get_offset_of_desiredDeltaTime_1() { return static_cast<int32_t>(offsetof(Simulator_t2705969170, ___desiredDeltaTime_1)); }
	inline float get_desiredDeltaTime_1() const { return ___desiredDeltaTime_1; }
	inline float* get_address_of_desiredDeltaTime_1() { return &___desiredDeltaTime_1; }
	inline void set_desiredDeltaTime_1(float value)
	{
		___desiredDeltaTime_1 = value;
	}

	inline static int32_t get_offset_of_interpolation_2() { return static_cast<int32_t>(offsetof(Simulator_t2705969170, ___interpolation_2)); }
	inline bool get_interpolation_2() const { return ___interpolation_2; }
	inline bool* get_address_of_interpolation_2() { return &___interpolation_2; }
	inline void set_interpolation_2(bool value)
	{
		___interpolation_2 = value;
	}

	inline static int32_t get_offset_of_workers_3() { return static_cast<int32_t>(offsetof(Simulator_t2705969170, ___workers_3)); }
	inline WorkerU5BU5D_t2291287347* get_workers_3() const { return ___workers_3; }
	inline WorkerU5BU5D_t2291287347** get_address_of_workers_3() { return &___workers_3; }
	inline void set_workers_3(WorkerU5BU5D_t2291287347* value)
	{
		___workers_3 = value;
		Il2CppCodeGenWriteBarrier(&___workers_3, value);
	}

	inline static int32_t get_offset_of_agents_4() { return static_cast<int32_t>(offsetof(Simulator_t2705969170, ___agents_4)); }
	inline List_1_t2658239795 * get_agents_4() const { return ___agents_4; }
	inline List_1_t2658239795 ** get_address_of_agents_4() { return &___agents_4; }
	inline void set_agents_4(List_1_t2658239795 * value)
	{
		___agents_4 = value;
		Il2CppCodeGenWriteBarrier(&___agents_4, value);
	}

	inline static int32_t get_offset_of_obstacles_5() { return static_cast<int32_t>(offsetof(Simulator_t2705969170, ___obstacles_5)); }
	inline List_1_t1243525355 * get_obstacles_5() const { return ___obstacles_5; }
	inline List_1_t1243525355 ** get_address_of_obstacles_5() { return &___obstacles_5; }
	inline void set_obstacles_5(List_1_t1243525355 * value)
	{
		___obstacles_5 = value;
		Il2CppCodeGenWriteBarrier(&___obstacles_5, value);
	}

	inline static int32_t get_offset_of_algorithm_6() { return static_cast<int32_t>(offsetof(Simulator_t2705969170, ___algorithm_6)); }
	inline int32_t get_algorithm_6() const { return ___algorithm_6; }
	inline int32_t* get_address_of_algorithm_6() { return &___algorithm_6; }
	inline void set_algorithm_6(int32_t value)
	{
		___algorithm_6 = value;
	}

	inline static int32_t get_offset_of_quadtree_7() { return static_cast<int32_t>(offsetof(Simulator_t2705969170, ___quadtree_7)); }
	inline RVOQuadtree_t4291712126 * get_quadtree_7() const { return ___quadtree_7; }
	inline RVOQuadtree_t4291712126 ** get_address_of_quadtree_7() { return &___quadtree_7; }
	inline void set_quadtree_7(RVOQuadtree_t4291712126 * value)
	{
		___quadtree_7 = value;
		Il2CppCodeGenWriteBarrier(&___quadtree_7, value);
	}

	inline static int32_t get_offset_of_qualityCutoff_8() { return static_cast<int32_t>(offsetof(Simulator_t2705969170, ___qualityCutoff_8)); }
	inline float get_qualityCutoff_8() const { return ___qualityCutoff_8; }
	inline float* get_address_of_qualityCutoff_8() { return &___qualityCutoff_8; }
	inline void set_qualityCutoff_8(float value)
	{
		___qualityCutoff_8 = value;
	}

	inline static int32_t get_offset_of_stepScale_9() { return static_cast<int32_t>(offsetof(Simulator_t2705969170, ___stepScale_9)); }
	inline float get_stepScale_9() const { return ___stepScale_9; }
	inline float* get_address_of_stepScale_9() { return &___stepScale_9; }
	inline void set_stepScale_9(float value)
	{
		___stepScale_9 = value;
	}

	inline static int32_t get_offset_of_deltaTime_10() { return static_cast<int32_t>(offsetof(Simulator_t2705969170, ___deltaTime_10)); }
	inline float get_deltaTime_10() const { return ___deltaTime_10; }
	inline float* get_address_of_deltaTime_10() { return &___deltaTime_10; }
	inline void set_deltaTime_10(float value)
	{
		___deltaTime_10 = value;
	}

	inline static int32_t get_offset_of_prevDeltaTime_11() { return static_cast<int32_t>(offsetof(Simulator_t2705969170, ___prevDeltaTime_11)); }
	inline float get_prevDeltaTime_11() const { return ___prevDeltaTime_11; }
	inline float* get_address_of_prevDeltaTime_11() { return &___prevDeltaTime_11; }
	inline void set_prevDeltaTime_11(float value)
	{
		___prevDeltaTime_11 = value;
	}

	inline static int32_t get_offset_of_lastStep_12() { return static_cast<int32_t>(offsetof(Simulator_t2705969170, ___lastStep_12)); }
	inline float get_lastStep_12() const { return ___lastStep_12; }
	inline float* get_address_of_lastStep_12() { return &___lastStep_12; }
	inline void set_lastStep_12(float value)
	{
		___lastStep_12 = value;
	}

	inline static int32_t get_offset_of_lastStepInterpolationReference_13() { return static_cast<int32_t>(offsetof(Simulator_t2705969170, ___lastStepInterpolationReference_13)); }
	inline float get_lastStepInterpolationReference_13() const { return ___lastStepInterpolationReference_13; }
	inline float* get_address_of_lastStepInterpolationReference_13() { return &___lastStepInterpolationReference_13; }
	inline void set_lastStepInterpolationReference_13(float value)
	{
		___lastStepInterpolationReference_13 = value;
	}

	inline static int32_t get_offset_of_doUpdateObstacles_14() { return static_cast<int32_t>(offsetof(Simulator_t2705969170, ___doUpdateObstacles_14)); }
	inline bool get_doUpdateObstacles_14() const { return ___doUpdateObstacles_14; }
	inline bool* get_address_of_doUpdateObstacles_14() { return &___doUpdateObstacles_14; }
	inline void set_doUpdateObstacles_14(bool value)
	{
		___doUpdateObstacles_14 = value;
	}

	inline static int32_t get_offset_of_doCleanObstacles_15() { return static_cast<int32_t>(offsetof(Simulator_t2705969170, ___doCleanObstacles_15)); }
	inline bool get_doCleanObstacles_15() const { return ___doCleanObstacles_15; }
	inline bool* get_address_of_doCleanObstacles_15() { return &___doCleanObstacles_15; }
	inline void set_doCleanObstacles_15(bool value)
	{
		___doCleanObstacles_15 = value;
	}

	inline static int32_t get_offset_of_oversampling_16() { return static_cast<int32_t>(offsetof(Simulator_t2705969170, ___oversampling_16)); }
	inline bool get_oversampling_16() const { return ___oversampling_16; }
	inline bool* get_address_of_oversampling_16() { return &___oversampling_16; }
	inline void set_oversampling_16(bool value)
	{
		___oversampling_16 = value;
	}

	inline static int32_t get_offset_of_wallThickness_17() { return static_cast<int32_t>(offsetof(Simulator_t2705969170, ___wallThickness_17)); }
	inline float get_wallThickness_17() const { return ___wallThickness_17; }
	inline float* get_address_of_wallThickness_17() { return &___wallThickness_17; }
	inline void set_wallThickness_17(float value)
	{
		___wallThickness_17 = value;
	}

	inline static int32_t get_offset_of_coroutineWorkerContext_18() { return static_cast<int32_t>(offsetof(Simulator_t2705969170, ___coroutineWorkerContext_18)); }
	inline WorkerContext_t2850943897 * get_coroutineWorkerContext_18() const { return ___coroutineWorkerContext_18; }
	inline WorkerContext_t2850943897 ** get_address_of_coroutineWorkerContext_18() { return &___coroutineWorkerContext_18; }
	inline void set_coroutineWorkerContext_18(WorkerContext_t2850943897 * value)
	{
		___coroutineWorkerContext_18 = value;
		Il2CppCodeGenWriteBarrier(&___coroutineWorkerContext_18, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
