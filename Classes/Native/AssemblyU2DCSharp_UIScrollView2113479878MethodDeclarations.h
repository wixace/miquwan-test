﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIScrollView
struct UIScrollView_t2113479878;
// UIPanel
struct UIPanel_t295209936;
// UIProgressBar
struct UIProgressBar_t168062834;
// EventDelegate
struct EventDelegate_t4004424223;
// System.Collections.Generic.List`1<EventDelegate>
struct List_1_t1077642479;
// EventDelegate/Callback
struct Callback_t1094463061;
// UnityEngine.Behaviour
struct Behaviour_t200106419;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// UIScrollView/OnDragNotification
struct OnDragNotification_t2323474503;
// UICenterOnChild
struct UICenterOnChild_t854454836;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Bounds2711641849.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_UIProgressBar168062834.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "AssemblyU2DCSharp_UIPanel295209936.h"
#include "AssemblyU2DCSharp_UIScrollView2113479878.h"
#include "AssemblyU2DCSharp_UIDrawCall_Clipping3937989211.h"
#include "AssemblyU2DCSharp_UIWidget_Pivot240933195.h"
#include "AssemblyU2DCSharp_EventDelegate_Callback1094463061.h"
#include "UnityEngine_UnityEngine_Vector44282066567.h"
#include "UnityEngine_UnityEngine_Behaviour200106419.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "AssemblyU2DCSharp_UIScrollView_OnDragNotification2323474503.h"
#include "AssemblyU2DCSharp_UICenterOnChild854454836.h"

// System.Void UIScrollView::.ctor()
extern "C"  void UIScrollView__ctor_m4137995413 (UIScrollView_t2113479878 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIScrollView::.cctor()
extern "C"  void UIScrollView__cctor_m3241710008 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIPanel UIScrollView::get_panel()
extern "C"  UIPanel_t295209936 * UIScrollView_get_panel_m4014558219 (UIScrollView_t2113479878 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIScrollView::get_isDragging()
extern "C"  bool UIScrollView_get_isDragging_m1479979205 (UIScrollView_t2113479878 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Bounds UIScrollView::get_bounds()
extern "C"  Bounds_t2711641849  UIScrollView_get_bounds_m1028289982 (UIScrollView_t2113479878 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIScrollView::get_canMoveHorizontally()
extern "C"  bool UIScrollView_get_canMoveHorizontally_m2065803912 (UIScrollView_t2113479878 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIScrollView::get_canMoveVertically()
extern "C"  bool UIScrollView_get_canMoveVertically_m4075477594 (UIScrollView_t2113479878 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIScrollView::get_shouldMoveHorizontally()
extern "C"  bool UIScrollView_get_shouldMoveHorizontally_m2072661921 (UIScrollView_t2113479878 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIScrollView::get_shouldMoveVertically()
extern "C"  bool UIScrollView_get_shouldMoveVertically_m3275485619 (UIScrollView_t2113479878 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIScrollView::get_shouldMove()
extern "C"  bool UIScrollView_get_shouldMove_m2959813008 (UIScrollView_t2113479878 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UIScrollView::get_currentMomentum()
extern "C"  Vector3_t4282066566  UIScrollView_get_currentMomentum_m3889642209 (UIScrollView_t2113479878 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIScrollView::set_currentMomentum(UnityEngine.Vector3)
extern "C"  void UIScrollView_set_currentMomentum_m3393819602 (UIScrollView_t2113479878 * __this, Vector3_t4282066566  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIScrollView::Awake()
extern "C"  void UIScrollView_Awake_m80633336 (UIScrollView_t2113479878 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIScrollView::OnEnable()
extern "C"  void UIScrollView_OnEnable_m2941597393 (UIScrollView_t2113479878 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIScrollView::Start()
extern "C"  void UIScrollView_Start_m3085133205 (UIScrollView_t2113479878 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIScrollView::CheckScrollbars()
extern "C"  void UIScrollView_CheckScrollbars_m1897109544 (UIScrollView_t2113479878 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIScrollView::OnDisable()
extern "C"  void UIScrollView_OnDisable_m1436143228 (UIScrollView_t2113479878 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIScrollView::RestrictWithinBounds(System.Boolean)
extern "C"  bool UIScrollView_RestrictWithinBounds_m2752728790 (UIScrollView_t2113479878 * __this, bool ___instant0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIScrollView::RestrictWithinBounds(System.Boolean,System.Boolean,System.Boolean)
extern "C"  bool UIScrollView_RestrictWithinBounds_m1872177846 (UIScrollView_t2113479878 * __this, bool ___instant0, bool ___horizontal1, bool ___vertical2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIScrollView::DisableSpring()
extern "C"  void UIScrollView_DisableSpring_m2168533384 (UIScrollView_t2113479878 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIScrollView::UpdateScrollbars()
extern "C"  void UIScrollView_UpdateScrollbars_m2694261829 (UIScrollView_t2113479878 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIScrollView::UpdateScrollbars(System.Boolean)
extern "C"  void UIScrollView_UpdateScrollbars_m1025803516 (UIScrollView_t2113479878 * __this, bool ___recalculateBounds0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIScrollView::UpdateScrollbars(UIProgressBar,System.Single,System.Single,System.Single,System.Single,System.Boolean)
extern "C"  void UIScrollView_UpdateScrollbars_m3866199342 (UIScrollView_t2113479878 * __this, UIProgressBar_t168062834 * ___slider0, float ___contentMin1, float ___contentMax2, float ___contentSize3, float ___viewSize4, bool ___inverted5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIScrollView::SetDragAmount(System.Single,System.Single,System.Boolean)
extern "C"  void UIScrollView_SetDragAmount_m1468547022 (UIScrollView_t2113479878 * __this, float ___x0, float ___y1, bool ___updateScrollbars2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIScrollView::InvalidateBounds()
extern "C"  void UIScrollView_InvalidateBounds_m2073271519 (UIScrollView_t2113479878 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIScrollView::ResetPosition()
extern "C"  void UIScrollView_ResetPosition_m445461835 (UIScrollView_t2113479878 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIScrollView::ResetPositionDown()
extern "C"  void UIScrollView_ResetPositionDown_m786788493 (UIScrollView_t2113479878 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIScrollView::UpdatePosition()
extern "C"  void UIScrollView_UpdatePosition_m3998703905 (UIScrollView_t2113479878 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIScrollView::OnScrollBar()
extern "C"  void UIScrollView_OnScrollBar_m2135298394 (UIScrollView_t2113479878 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIScrollView::MoveRelative(UnityEngine.Vector3)
extern "C"  void UIScrollView_MoveRelative_m1062396365 (UIScrollView_t2113479878 * __this, Vector3_t4282066566  ___relative0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIScrollView::MoveAbsolute(UnityEngine.Vector3)
extern "C"  void UIScrollView_MoveAbsolute_m1943582722 (UIScrollView_t2113479878 * __this, Vector3_t4282066566  ___absolute0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIScrollView::Press(System.Boolean)
extern "C"  void UIScrollView_Press_m1143812141 (UIScrollView_t2113479878 * __this, bool ___pressed0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIScrollView::Drag()
extern "C"  void UIScrollView_Drag_m3270453827 (UIScrollView_t2113479878 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIScrollView::Scroll(System.Single)
extern "C"  void UIScrollView_Scroll_m2157705487 (UIScrollView_t2113479878 * __this, float ___delta0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIScrollView::LateUpdate()
extern "C"  void UIScrollView_LateUpdate_m596775646 (UIScrollView_t2113479878 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIScrollView::OnPan(UnityEngine.Vector2)
extern "C"  void UIScrollView_OnPan_m2320124841 (UIScrollView_t2113479878 * __this, Vector2_t4282066565  ___delta0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UIScrollView::ilo_get_height1(UIPanel)
extern "C"  float UIScrollView_ilo_get_height1_m222164877 (Il2CppObject * __this /* static, unused */, UIPanel_t295209936 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Bounds UIScrollView::ilo_get_bounds2(UIScrollView)
extern "C"  Bounds_t2711641849  UIScrollView_ilo_get_bounds2_m3130472637 (Il2CppObject * __this /* static, unused */, UIScrollView_t2113479878 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIDrawCall/Clipping UIScrollView::ilo_get_clipping3(UIPanel)
extern "C"  int32_t UIScrollView_ilo_get_clipping3_m1287068402 (Il2CppObject * __this /* static, unused */, UIPanel_t295209936 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIWidget/Pivot UIScrollView::ilo_GetPivot4(UnityEngine.Vector2)
extern "C"  int32_t UIScrollView_ilo_GetPivot4_m2320411396 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___offset0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIScrollView::ilo_CheckScrollbars5(UIScrollView)
extern "C"  void UIScrollView_ilo_CheckScrollbars5_m860918940 (Il2CppObject * __this /* static, unused */, UIScrollView_t2113479878 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// EventDelegate UIScrollView::ilo_Add6(System.Collections.Generic.List`1<EventDelegate>,EventDelegate/Callback)
extern "C"  EventDelegate_t4004424223 * UIScrollView_ilo_Add6_m3837772131 (Il2CppObject * __this /* static, unused */, List_1_t1077642479 * ___list0, Callback_t1094463061 * ___callback1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIScrollView::ilo_PanelUp7()
extern "C"  void UIScrollView_ilo_PanelUp7_m399586522 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIScrollView::ilo_get_shouldMove8(UIScrollView)
extern "C"  bool UIScrollView_ilo_get_shouldMove8_m1175159473 (Il2CppObject * __this /* static, unused */, UIScrollView_t2113479878 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIScrollView::ilo_UpdateScrollbars9(UIScrollView,UIProgressBar,System.Single,System.Single,System.Single,System.Single,System.Boolean)
extern "C"  void UIScrollView_ilo_UpdateScrollbars9_m2060962802 (Il2CppObject * __this /* static, unused */, UIScrollView_t2113479878 * ____this0, UIProgressBar_t168062834 * ___slider1, float ___contentMin2, float ___contentMax3, float ___contentSize4, float ___viewSize5, bool ___inverted6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector4 UIScrollView::ilo_get_finalClipRegion10(UIPanel)
extern "C"  Vector4_t4282066567  UIScrollView_ilo_get_finalClipRegion10_m2445263755 (Il2CppObject * __this /* static, unused */, UIPanel_t295209936 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UIScrollView::ilo_get_clipSoftness11(UIPanel)
extern "C"  Vector2_t4282066565  UIScrollView_ilo_get_clipSoftness11_m1933234219 (Il2CppObject * __this /* static, unused */, UIPanel_t295209936 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIScrollView::ilo_set_value12(UIProgressBar,System.Single)
extern "C"  void UIScrollView_ilo_set_value12_m515155728 (Il2CppObject * __this /* static, unused */, UIProgressBar_t168062834 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIScrollView::ilo_DisableSpring13(UIScrollView)
extern "C"  void UIScrollView_ilo_DisableSpring13_m3626853777 (Il2CppObject * __this /* static, unused */, UIScrollView_t2113479878 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIScrollView::ilo_get_canMoveHorizontally14(UIScrollView)
extern "C"  bool UIScrollView_ilo_get_canMoveHorizontally14_m3949547154 (Il2CppObject * __this /* static, unused */, UIScrollView_t2113479878 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector4 UIScrollView::ilo_get_baseClipRegion15(UIPanel)
extern "C"  Vector4_t4282066567  UIScrollView_ilo_get_baseClipRegion15_m1560806469 (Il2CppObject * __this /* static, unused */, UIPanel_t295209936 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIScrollView::ilo_GetActive16(UnityEngine.Behaviour)
extern "C"  bool UIScrollView_ilo_GetActive16_m1355181727 (Il2CppObject * __this /* static, unused */, Behaviour_t200106419 * ___mb0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UIScrollView::ilo_GetPivotOffset17(UIWidget/Pivot)
extern "C"  Vector2_t4282066565  UIScrollView_ilo_GetPivotOffset17_m2411233559 (Il2CppObject * __this /* static, unused */, int32_t ___pv0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIScrollView::ilo_SetDragAmount18(UIScrollView,System.Single,System.Single,System.Boolean)
extern "C"  void UIScrollView_ilo_SetDragAmount18_m3779318116 (Il2CppObject * __this /* static, unused */, UIScrollView_t2113479878 * ____this0, float ___x1, float ___y2, bool ___updateScrollbars3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIScrollView::ilo_UpdateScrollbars19(UIScrollView,System.Boolean)
extern "C"  void UIScrollView_ilo_UpdateScrollbars19_m3548851107 (Il2CppObject * __this /* static, unused */, UIScrollView_t2113479878 * ____this0, bool ___recalculateBounds1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIScrollView::ilo_set_clipOffset20(UIPanel,UnityEngine.Vector2)
extern "C"  void UIScrollView_ilo_set_clipOffset20_m1469846262 (Il2CppObject * __this /* static, unused */, UIPanel_t295209936 * ____this0, Vector2_t4282066565  ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIScrollView::ilo_MoveRelative21(UIScrollView,UnityEngine.Vector3)
extern "C"  void UIScrollView_ilo_MoveRelative21_m3641085323 (Il2CppObject * __this /* static, unused */, UIScrollView_t2113479878 * ____this0, Vector3_t4282066566  ___relative1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIScrollView::ilo_GetActive22(UnityEngine.GameObject)
extern "C"  bool UIScrollView_ilo_GetActive22_m972635744 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIScrollView::ilo_Invoke23(UIScrollView/OnDragNotification)
extern "C"  void UIScrollView_ilo_Invoke23_m3863489968 (Il2CppObject * __this /* static, unused */, OnDragNotification_t2323474503 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIScrollView::ilo_Recenter24(UICenterOnChild)
extern "C"  void UIScrollView_ilo_Recenter24_m755117554 (Il2CppObject * __this /* static, unused */, UICenterOnChild_t854454836 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIScrollView::ilo_MoveAbsolute25(UIScrollView,UnityEngine.Vector3)
extern "C"  void UIScrollView_ilo_MoveAbsolute25_m2817516986 (Il2CppObject * __this /* static, unused */, UIScrollView_t2113479878 * ____this0, Vector3_t4282066566  ___absolute1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UIScrollView::ilo_CalculateConstrainOffset26(UIPanel,UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  Vector3_t4282066566  UIScrollView_ilo_CalculateConstrainOffset26_m1589556454 (Il2CppObject * __this /* static, unused */, UIPanel_t295209936 * ____this0, Vector2_t4282066565  ___min1, Vector2_t4282066565  ___max2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIScrollView::ilo_get_shouldMoveHorizontally27(UIScrollView)
extern "C"  bool UIScrollView_ilo_get_shouldMoveHorizontally27_m2479955603 (Il2CppObject * __this /* static, unused */, UIScrollView_t2113479878 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UIScrollView::ilo_get_alpha28(UIProgressBar)
extern "C"  float UIScrollView_ilo_get_alpha28_m507138481 (Il2CppObject * __this /* static, unused */, UIProgressBar_t168062834 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIScrollView::ilo_set_alpha29(UIProgressBar,System.Single)
extern "C"  void UIScrollView_ilo_set_alpha29_m2319467293 (Il2CppObject * __this /* static, unused */, UIProgressBar_t168062834 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UIScrollView::ilo_SpringLerp30(System.Single,System.Single,System.Single,System.Single)
extern "C"  float UIScrollView_ilo_SpringLerp30_m2826187883 (Il2CppObject * __this /* static, unused */, float ___from0, float ___to1, float ___strength2, float ___deltaTime3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UIScrollView::ilo_SpringDampen31(UnityEngine.Vector3&,System.Single,System.Single)
extern "C"  Vector3_t4282066566  UIScrollView_ilo_SpringDampen31_m523565613 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566 * ___velocity0, float ___strength1, float ___deltaTime2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIScrollView::ilo_Scroll32(UIScrollView,System.Single)
extern "C"  void UIScrollView_ilo_Scroll32_m471892685 (Il2CppObject * __this /* static, unused */, UIScrollView_t2113479878 * ____this0, float ___delta1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
