﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._e4b0ebf9ffc639842c3478a5ddd2e8c0
struct _e4b0ebf9ffc639842c3478a5ddd2e8c0_t2931369648;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._e4b0ebf9ffc639842c3478a5ddd2e8c0::.ctor()
extern "C"  void _e4b0ebf9ffc639842c3478a5ddd2e8c0__ctor_m1718781309 (_e4b0ebf9ffc639842c3478a5ddd2e8c0_t2931369648 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._e4b0ebf9ffc639842c3478a5ddd2e8c0::_e4b0ebf9ffc639842c3478a5ddd2e8c0m2(System.Int32)
extern "C"  int32_t _e4b0ebf9ffc639842c3478a5ddd2e8c0__e4b0ebf9ffc639842c3478a5ddd2e8c0m2_m1030755225 (_e4b0ebf9ffc639842c3478a5ddd2e8c0_t2931369648 * __this, int32_t ____e4b0ebf9ffc639842c3478a5ddd2e8c0a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._e4b0ebf9ffc639842c3478a5ddd2e8c0::_e4b0ebf9ffc639842c3478a5ddd2e8c0m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _e4b0ebf9ffc639842c3478a5ddd2e8c0__e4b0ebf9ffc639842c3478a5ddd2e8c0m_m1083888445 (_e4b0ebf9ffc639842c3478a5ddd2e8c0_t2931369648 * __this, int32_t ____e4b0ebf9ffc639842c3478a5ddd2e8c0a0, int32_t ____e4b0ebf9ffc639842c3478a5ddd2e8c0661, int32_t ____e4b0ebf9ffc639842c3478a5ddd2e8c0c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
