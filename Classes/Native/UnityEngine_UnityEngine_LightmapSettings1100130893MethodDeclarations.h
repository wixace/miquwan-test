﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.LightmapSettings
struct LightmapSettings_t1100130893;
// UnityEngine.LightmapData[]
struct LightmapDataU5BU5D_t705514461;
// UnityEngine.LightProbes
struct LightProbes_t2995644975;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_LightmapsMode643144806.h"
#include "UnityEngine_UnityEngine_LightProbes2995644975.h"

// System.Void UnityEngine.LightmapSettings::.ctor()
extern "C"  void LightmapSettings__ctor_m1369794244 (LightmapSettings_t1100130893 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.LightmapData[] UnityEngine.LightmapSettings::get_lightmaps()
extern "C"  LightmapDataU5BU5D_t705514461* LightmapSettings_get_lightmaps_m1625896736 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.LightmapSettings::set_lightmaps(UnityEngine.LightmapData[])
extern "C"  void LightmapSettings_set_lightmaps_m211591753 (Il2CppObject * __this /* static, unused */, LightmapDataU5BU5D_t705514461* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.LightmapsMode UnityEngine.LightmapSettings::get_lightmapsMode()
extern "C"  int32_t LightmapSettings_get_lightmapsMode_m926427055 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.LightmapSettings::set_lightmapsMode(UnityEngine.LightmapsMode)
extern "C"  void LightmapSettings_set_lightmapsMode_m4157505604 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.LightProbes UnityEngine.LightmapSettings::get_lightProbes()
extern "C"  LightProbes_t2995644975 * LightmapSettings_get_lightProbes_m4095370177 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.LightmapSettings::set_lightProbes(UnityEngine.LightProbes)
extern "C"  void LightmapSettings_set_lightProbes_m780430386 (Il2CppObject * __this /* static, unused */, LightProbes_t2995644975 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
