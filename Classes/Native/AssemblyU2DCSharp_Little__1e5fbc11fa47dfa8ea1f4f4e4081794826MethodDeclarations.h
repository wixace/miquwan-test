﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._1e5fbc11fa47dfa8ea1f4f4e8391a2ea
struct _1e5fbc11fa47dfa8ea1f4f4e8391a2ea_t4081794826;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__1e5fbc11fa47dfa8ea1f4f4e4081794826.h"

// System.Void Little._1e5fbc11fa47dfa8ea1f4f4e8391a2ea::.ctor()
extern "C"  void _1e5fbc11fa47dfa8ea1f4f4e8391a2ea__ctor_m3935122019 (_1e5fbc11fa47dfa8ea1f4f4e8391a2ea_t4081794826 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._1e5fbc11fa47dfa8ea1f4f4e8391a2ea::_1e5fbc11fa47dfa8ea1f4f4e8391a2eam2(System.Int32)
extern "C"  int32_t _1e5fbc11fa47dfa8ea1f4f4e8391a2ea__1e5fbc11fa47dfa8ea1f4f4e8391a2eam2_m425157977 (_1e5fbc11fa47dfa8ea1f4f4e8391a2ea_t4081794826 * __this, int32_t ____1e5fbc11fa47dfa8ea1f4f4e8391a2eaa0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._1e5fbc11fa47dfa8ea1f4f4e8391a2ea::_1e5fbc11fa47dfa8ea1f4f4e8391a2eam(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _1e5fbc11fa47dfa8ea1f4f4e8391a2ea__1e5fbc11fa47dfa8ea1f4f4e8391a2eam_m3974035837 (_1e5fbc11fa47dfa8ea1f4f4e8391a2ea_t4081794826 * __this, int32_t ____1e5fbc11fa47dfa8ea1f4f4e8391a2eaa0, int32_t ____1e5fbc11fa47dfa8ea1f4f4e8391a2ea891, int32_t ____1e5fbc11fa47dfa8ea1f4f4e8391a2eac2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._1e5fbc11fa47dfa8ea1f4f4e8391a2ea::ilo__1e5fbc11fa47dfa8ea1f4f4e8391a2eam21(Little._1e5fbc11fa47dfa8ea1f4f4e8391a2ea,System.Int32)
extern "C"  int32_t _1e5fbc11fa47dfa8ea1f4f4e8391a2ea_ilo__1e5fbc11fa47dfa8ea1f4f4e8391a2eam21_m2715673617 (Il2CppObject * __this /* static, unused */, _1e5fbc11fa47dfa8ea1f4f4e8391a2ea_t4081794826 * ____this0, int32_t ____1e5fbc11fa47dfa8ea1f4f4e8391a2eaa1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
