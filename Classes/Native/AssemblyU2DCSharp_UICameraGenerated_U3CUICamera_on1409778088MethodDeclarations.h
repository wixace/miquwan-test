﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UICameraGenerated/<UICamera_onScroll_GetDelegate_member47_arg0>c__AnonStoreyA8
struct U3CUICamera_onScroll_GetDelegate_member47_arg0U3Ec__AnonStoreyA8_t1409778088;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"

// System.Void UICameraGenerated/<UICamera_onScroll_GetDelegate_member47_arg0>c__AnonStoreyA8::.ctor()
extern "C"  void U3CUICamera_onScroll_GetDelegate_member47_arg0U3Ec__AnonStoreyA8__ctor_m1240786931 (U3CUICamera_onScroll_GetDelegate_member47_arg0U3Ec__AnonStoreyA8_t1409778088 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICameraGenerated/<UICamera_onScroll_GetDelegate_member47_arg0>c__AnonStoreyA8::<>m__114(UnityEngine.GameObject,System.Single)
extern "C"  void U3CUICamera_onScroll_GetDelegate_member47_arg0U3Ec__AnonStoreyA8_U3CU3Em__114_m872866775 (U3CUICamera_onScroll_GetDelegate_member47_arg0U3Ec__AnonStoreyA8_t1409778088 * __this, GameObject_t3674682005 * ___go0, float ___delta1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
