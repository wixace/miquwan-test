﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_semsir0
struct  M_semsir0_t193557231  : public Il2CppObject
{
public:
	// System.Boolean GarbageiOS.M_semsir0::_kemhaMereca
	bool ____kemhaMereca_0;
	// System.Single GarbageiOS.M_semsir0::_qadelstasBeci
	float ____qadelstasBeci_1;
	// System.Boolean GarbageiOS.M_semsir0::_nearcuceaMegista
	bool ____nearcuceaMegista_2;
	// System.Int32 GarbageiOS.M_semsir0::_xipe
	int32_t ____xipe_3;
	// System.Boolean GarbageiOS.M_semsir0::_challloLalqeci
	bool ____challloLalqeci_4;
	// System.Boolean GarbageiOS.M_semsir0::_soucerkeaToce
	bool ____soucerkeaToce_5;
	// System.Int32 GarbageiOS.M_semsir0::_madisJashel
	int32_t ____madisJashel_6;
	// System.Boolean GarbageiOS.M_semsir0::_mesazowChirgeeku
	bool ____mesazowChirgeeku_7;
	// System.String GarbageiOS.M_semsir0::_fese
	String_t* ____fese_8;
	// System.Boolean GarbageiOS.M_semsir0::_nadearNedistray
	bool ____nadearNedistray_9;
	// System.Int32 GarbageiOS.M_semsir0::_bivertalLukaw
	int32_t ____bivertalLukaw_10;

public:
	inline static int32_t get_offset_of__kemhaMereca_0() { return static_cast<int32_t>(offsetof(M_semsir0_t193557231, ____kemhaMereca_0)); }
	inline bool get__kemhaMereca_0() const { return ____kemhaMereca_0; }
	inline bool* get_address_of__kemhaMereca_0() { return &____kemhaMereca_0; }
	inline void set__kemhaMereca_0(bool value)
	{
		____kemhaMereca_0 = value;
	}

	inline static int32_t get_offset_of__qadelstasBeci_1() { return static_cast<int32_t>(offsetof(M_semsir0_t193557231, ____qadelstasBeci_1)); }
	inline float get__qadelstasBeci_1() const { return ____qadelstasBeci_1; }
	inline float* get_address_of__qadelstasBeci_1() { return &____qadelstasBeci_1; }
	inline void set__qadelstasBeci_1(float value)
	{
		____qadelstasBeci_1 = value;
	}

	inline static int32_t get_offset_of__nearcuceaMegista_2() { return static_cast<int32_t>(offsetof(M_semsir0_t193557231, ____nearcuceaMegista_2)); }
	inline bool get__nearcuceaMegista_2() const { return ____nearcuceaMegista_2; }
	inline bool* get_address_of__nearcuceaMegista_2() { return &____nearcuceaMegista_2; }
	inline void set__nearcuceaMegista_2(bool value)
	{
		____nearcuceaMegista_2 = value;
	}

	inline static int32_t get_offset_of__xipe_3() { return static_cast<int32_t>(offsetof(M_semsir0_t193557231, ____xipe_3)); }
	inline int32_t get__xipe_3() const { return ____xipe_3; }
	inline int32_t* get_address_of__xipe_3() { return &____xipe_3; }
	inline void set__xipe_3(int32_t value)
	{
		____xipe_3 = value;
	}

	inline static int32_t get_offset_of__challloLalqeci_4() { return static_cast<int32_t>(offsetof(M_semsir0_t193557231, ____challloLalqeci_4)); }
	inline bool get__challloLalqeci_4() const { return ____challloLalqeci_4; }
	inline bool* get_address_of__challloLalqeci_4() { return &____challloLalqeci_4; }
	inline void set__challloLalqeci_4(bool value)
	{
		____challloLalqeci_4 = value;
	}

	inline static int32_t get_offset_of__soucerkeaToce_5() { return static_cast<int32_t>(offsetof(M_semsir0_t193557231, ____soucerkeaToce_5)); }
	inline bool get__soucerkeaToce_5() const { return ____soucerkeaToce_5; }
	inline bool* get_address_of__soucerkeaToce_5() { return &____soucerkeaToce_5; }
	inline void set__soucerkeaToce_5(bool value)
	{
		____soucerkeaToce_5 = value;
	}

	inline static int32_t get_offset_of__madisJashel_6() { return static_cast<int32_t>(offsetof(M_semsir0_t193557231, ____madisJashel_6)); }
	inline int32_t get__madisJashel_6() const { return ____madisJashel_6; }
	inline int32_t* get_address_of__madisJashel_6() { return &____madisJashel_6; }
	inline void set__madisJashel_6(int32_t value)
	{
		____madisJashel_6 = value;
	}

	inline static int32_t get_offset_of__mesazowChirgeeku_7() { return static_cast<int32_t>(offsetof(M_semsir0_t193557231, ____mesazowChirgeeku_7)); }
	inline bool get__mesazowChirgeeku_7() const { return ____mesazowChirgeeku_7; }
	inline bool* get_address_of__mesazowChirgeeku_7() { return &____mesazowChirgeeku_7; }
	inline void set__mesazowChirgeeku_7(bool value)
	{
		____mesazowChirgeeku_7 = value;
	}

	inline static int32_t get_offset_of__fese_8() { return static_cast<int32_t>(offsetof(M_semsir0_t193557231, ____fese_8)); }
	inline String_t* get__fese_8() const { return ____fese_8; }
	inline String_t** get_address_of__fese_8() { return &____fese_8; }
	inline void set__fese_8(String_t* value)
	{
		____fese_8 = value;
		Il2CppCodeGenWriteBarrier(&____fese_8, value);
	}

	inline static int32_t get_offset_of__nadearNedistray_9() { return static_cast<int32_t>(offsetof(M_semsir0_t193557231, ____nadearNedistray_9)); }
	inline bool get__nadearNedistray_9() const { return ____nadearNedistray_9; }
	inline bool* get_address_of__nadearNedistray_9() { return &____nadearNedistray_9; }
	inline void set__nadearNedistray_9(bool value)
	{
		____nadearNedistray_9 = value;
	}

	inline static int32_t get_offset_of__bivertalLukaw_10() { return static_cast<int32_t>(offsetof(M_semsir0_t193557231, ____bivertalLukaw_10)); }
	inline int32_t get__bivertalLukaw_10() const { return ____bivertalLukaw_10; }
	inline int32_t* get_address_of__bivertalLukaw_10() { return &____bivertalLukaw_10; }
	inline void set__bivertalLukaw_10(int32_t value)
	{
		____bivertalLukaw_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
