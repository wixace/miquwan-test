﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AnimatedAlphaGenerated
struct AnimatedAlphaGenerated_t817117812;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void AnimatedAlphaGenerated::.ctor()
extern "C"  void AnimatedAlphaGenerated__ctor_m1303411879 (AnimatedAlphaGenerated_t817117812 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AnimatedAlphaGenerated::AnimatedAlpha_AnimatedAlpha1(JSVCall,System.Int32)
extern "C"  bool AnimatedAlphaGenerated_AnimatedAlpha_AnimatedAlpha1_m813844859 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimatedAlphaGenerated::AnimatedAlpha_alpha(JSVCall)
extern "C"  void AnimatedAlphaGenerated_AnimatedAlpha_alpha_m1447096040 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimatedAlphaGenerated::__Register()
extern "C"  void AnimatedAlphaGenerated___Register_m3688353920 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 AnimatedAlphaGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t AnimatedAlphaGenerated_ilo_getObject1_m2418736715 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimatedAlphaGenerated::ilo_addJSCSRel2(System.Int32,System.Object)
extern "C"  void AnimatedAlphaGenerated_ilo_addJSCSRel2_m3380408420 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
