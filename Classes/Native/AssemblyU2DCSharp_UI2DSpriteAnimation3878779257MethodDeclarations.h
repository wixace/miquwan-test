﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UI2DSpriteAnimation
struct UI2DSpriteAnimation_t3878779257;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_UI2DSpriteAnimation3878779257.h"

// System.Void UI2DSpriteAnimation::.ctor()
extern "C"  void UI2DSpriteAnimation__ctor_m1866150418 (UI2DSpriteAnimation_t3878779257 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UI2DSpriteAnimation::get_isPlaying()
extern "C"  bool UI2DSpriteAnimation_get_isPlaying_m3617603647 (UI2DSpriteAnimation_t3878779257 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UI2DSpriteAnimation::get_framesPerSecond()
extern "C"  int32_t UI2DSpriteAnimation_get_framesPerSecond_m202115104 (UI2DSpriteAnimation_t3878779257 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UI2DSpriteAnimation::set_framesPerSecond(System.Int32)
extern "C"  void UI2DSpriteAnimation_set_framesPerSecond_m1771639279 (UI2DSpriteAnimation_t3878779257 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UI2DSpriteAnimation::Play()
extern "C"  void UI2DSpriteAnimation_Play_m764247846 (UI2DSpriteAnimation_t3878779257 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UI2DSpriteAnimation::Pause()
extern "C"  void UI2DSpriteAnimation_Pause_m1920276390 (UI2DSpriteAnimation_t3878779257 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UI2DSpriteAnimation::ResetToBeginning()
extern "C"  void UI2DSpriteAnimation_ResetToBeginning_m3433568901 (UI2DSpriteAnimation_t3878779257 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UI2DSpriteAnimation::Start()
extern "C"  void UI2DSpriteAnimation_Start_m813288210 (UI2DSpriteAnimation_t3878779257 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UI2DSpriteAnimation::Update()
extern "C"  void UI2DSpriteAnimation_Update_m3742950203 (UI2DSpriteAnimation_t3878779257 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UI2DSpriteAnimation::UpdateSprite()
extern "C"  void UI2DSpriteAnimation_UpdateSprite_m1810209120 (UI2DSpriteAnimation_t3878779257 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UI2DSpriteAnimation::ilo_UpdateSprite1(UI2DSpriteAnimation)
extern "C"  void UI2DSpriteAnimation_ilo_UpdateSprite1_m2519398969 (Il2CppObject * __this /* static, unused */, UI2DSpriteAnimation_t3878779257 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UI2DSpriteAnimation::ilo_Play2(UI2DSpriteAnimation)
extern "C"  void UI2DSpriteAnimation_ilo_Play2_m1134534302 (Il2CppObject * __this /* static, unused */, UI2DSpriteAnimation_t3878779257 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
