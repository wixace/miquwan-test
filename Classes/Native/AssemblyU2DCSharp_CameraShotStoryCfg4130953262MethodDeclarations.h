﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraShotStoryCfg
struct CameraShotStoryCfg_t4130953262;
// ProtoBuf.IExtension
struct IExtension_t1606339106;

#include "codegen/il2cpp-codegen.h"

// System.Void CameraShotStoryCfg::.ctor()
extern "C"  void CameraShotStoryCfg__ctor_m4129422381 (CameraShotStoryCfg_t4130953262 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.IExtension CameraShotStoryCfg::ProtoBuf.IExtensible.GetExtensionObject(System.Boolean)
extern "C"  Il2CppObject * CameraShotStoryCfg_ProtoBuf_IExtensible_GetExtensionObject_m2675655293 (CameraShotStoryCfg_t4130953262 * __this, bool ___createIfMissing0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CameraShotStoryCfg::get_id()
extern "C"  int32_t CameraShotStoryCfg_get_id_m1147334793 (CameraShotStoryCfg_t4130953262 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotStoryCfg::set_id(System.Int32)
extern "C"  void CameraShotStoryCfg_set_id_m1447145664 (CameraShotStoryCfg_t4130953262 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.IExtension CameraShotStoryCfg::ilo_GetExtensionObject1(ProtoBuf.IExtension&,System.Boolean)
extern "C"  Il2CppObject * CameraShotStoryCfg_ilo_GetExtensionObject1_m1358409379 (Il2CppObject * __this /* static, unused */, Il2CppObject ** ___extensionObject0, bool ___createIfMissing1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
