﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.LevelGridNode
struct LevelGridNode_t489265774;
// AstarPath
struct AstarPath_t4090270936;
// Pathfinding.LayerGridGraph
struct LayerGridGraph_t3415576653;
// Pathfinding.GraphNodeDelegate
struct GraphNodeDelegate_t1466738551;
// System.Collections.Generic.Stack`1<Pathfinding.GraphNode>
struct Stack_1_t3122173294;
// Pathfinding.GraphNode
struct GraphNode_t23612370;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t1355284822;
// Pathfinding.Path
struct Path_t1974241691;
// Pathfinding.PathNode
struct PathNode_t417131581;
// Pathfinding.PathHandler
struct PathHandler_t918952263;
// Pathfinding.Serialization.GraphSerializationContext
struct GraphSerializationContext_t3256954663;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_AstarPath4090270936.h"
#include "AssemblyU2DCSharp_Pathfinding_LayerGridGraph3415576653.h"
#include "AssemblyU2DCSharp_Pathfinding_Int31974045594.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphNodeDelegate1466738551.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphNode23612370.h"
#include "AssemblyU2DCSharp_Pathfinding_Path1974241691.h"
#include "AssemblyU2DCSharp_Pathfinding_PathNode417131581.h"
#include "AssemblyU2DCSharp_Pathfinding_PathHandler918952263.h"
#include "AssemblyU2DCSharp_Pathfinding_Serialization_GraphS3256954663.h"
#include "AssemblyU2DCSharp_Pathfinding_LevelGridNode489265774.h"

// System.Void Pathfinding.LevelGridNode::.ctor(AstarPath)
extern "C"  void LevelGridNode__ctor_m1776759793 (LevelGridNode_t489265774 * __this, AstarPath_t4090270936 * ___astar0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.LevelGridNode::.cctor()
extern "C"  void LevelGridNode__cctor_m1203543620 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.LayerGridGraph Pathfinding.LevelGridNode::GetGridGraph(System.UInt32)
extern "C"  LayerGridGraph_t3415576653 * LevelGridNode_GetGridGraph_m2448635305 (Il2CppObject * __this /* static, unused */, uint32_t ___graphIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.LevelGridNode::SetGridGraph(System.Int32,Pathfinding.LayerGridGraph)
extern "C"  void LevelGridNode_SetGridGraph_m1445462739 (Il2CppObject * __this /* static, unused */, int32_t ___graphIndex0, LayerGridGraph_t3415576653 * ___graph1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.LevelGridNode::ResetAllGridConnections()
extern "C"  void LevelGridNode_ResetAllGridConnections_m760412388 (LevelGridNode_t489265774 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.LevelGridNode::get_WalkableErosion()
extern "C"  bool LevelGridNode_get_WalkableErosion_m3575728422 (LevelGridNode_t489265774 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.LevelGridNode::set_WalkableErosion(System.Boolean)
extern "C"  void LevelGridNode_set_WalkableErosion_m436245877 (LevelGridNode_t489265774 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.LevelGridNode::get_TmpWalkable()
extern "C"  bool LevelGridNode_get_TmpWalkable_m4294683148 (LevelGridNode_t489265774 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.LevelGridNode::set_TmpWalkable(System.Boolean)
extern "C"  void LevelGridNode_set_TmpWalkable_m3024279515 (LevelGridNode_t489265774 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pathfinding.LevelGridNode::get_NodeInGridIndex()
extern "C"  int32_t LevelGridNode_get_NodeInGridIndex_m880491377 (LevelGridNode_t489265774 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.LevelGridNode::set_NodeInGridIndex(System.Int32)
extern "C"  void LevelGridNode_set_NodeInGridIndex_m3082303040 (LevelGridNode_t489265774 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.LevelGridNode::HasAnyGridConnections()
extern "C"  bool LevelGridNode_HasAnyGridConnections_m3688137976 (LevelGridNode_t489265774 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.LevelGridNode::SetPosition(Pathfinding.Int3)
extern "C"  void LevelGridNode_SetPosition_m2285650308 (LevelGridNode_t489265774 * __this, Int3_t1974045594  ___position0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.LevelGridNode::ClearConnections(System.Boolean)
extern "C"  void LevelGridNode_ClearConnections_m3646581146 (LevelGridNode_t489265774 * __this, bool ___alsoReverse0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.LevelGridNode::GetConnections(Pathfinding.GraphNodeDelegate)
extern "C"  void LevelGridNode_GetConnections_m1773690797 (LevelGridNode_t489265774 * __this, GraphNodeDelegate_t1466738551 * ___del0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.LevelGridNode::FloodFill(System.Collections.Generic.Stack`1<Pathfinding.GraphNode>,System.UInt32)
extern "C"  void LevelGridNode_FloodFill_m2978061460 (LevelGridNode_t489265774 * __this, Stack_1_t3122173294 * ___stack0, uint32_t ___region1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.LevelGridNode::AddConnection(Pathfinding.GraphNode,System.UInt32)
extern "C"  void LevelGridNode_AddConnection_m2417272346 (LevelGridNode_t489265774 * __this, GraphNode_t23612370 * ___node0, uint32_t ___cost1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.LevelGridNode::RemoveConnection(Pathfinding.GraphNode)
extern "C"  void LevelGridNode_RemoveConnection_m3411524207 (LevelGridNode_t489265774 * __this, GraphNode_t23612370 * ___node0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.LevelGridNode::GetConnection(System.Int32)
extern "C"  bool LevelGridNode_GetConnection_m1545724928 (LevelGridNode_t489265774 * __this, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.LevelGridNode::SetConnectionValue(System.Int32,System.Int32)
extern "C"  void LevelGridNode_SetConnectionValue_m447253594 (LevelGridNode_t489265774 * __this, int32_t ___dir0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pathfinding.LevelGridNode::GetConnectionValue(System.Int32)
extern "C"  int32_t LevelGridNode_GetConnectionValue_m3221116507 (LevelGridNode_t489265774 * __this, int32_t ___dir0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.LevelGridNode::GetPortal(Pathfinding.GraphNode,System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Boolean)
extern "C"  bool LevelGridNode_GetPortal_m3999241582 (LevelGridNode_t489265774 * __this, GraphNode_t23612370 * ___other0, List_1_t1355284822 * ___left1, List_1_t1355284822 * ___right2, bool ___backwards3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.LevelGridNode::UpdateRecursiveG(Pathfinding.Path,Pathfinding.PathNode,Pathfinding.PathHandler)
extern "C"  void LevelGridNode_UpdateRecursiveG_m1137397340 (LevelGridNode_t489265774 * __this, Path_t1974241691 * ___path0, PathNode_t417131581 * ___pathNode1, PathHandler_t918952263 * ___handler2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.LevelGridNode::Open(Pathfinding.Path,Pathfinding.PathNode,Pathfinding.PathHandler)
extern "C"  void LevelGridNode_Open_m1963026320 (LevelGridNode_t489265774 * __this, Path_t1974241691 * ___path0, PathNode_t417131581 * ___pathNode1, PathHandler_t918952263 * ___handler2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.LevelGridNode::SerializeNode(Pathfinding.Serialization.GraphSerializationContext)
extern "C"  void LevelGridNode_SerializeNode_m3782268356 (LevelGridNode_t489265774 * __this, GraphSerializationContext_t3256954663 * ___ctx0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.LevelGridNode::DeserializeNode(Pathfinding.Serialization.GraphSerializationContext)
extern "C"  void LevelGridNode_DeserializeNode_m1217609605 (LevelGridNode_t489265774 * __this, GraphSerializationContext_t3256954663 * ___ctx0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Pathfinding.LevelGridNode::ilo_get_GraphIndex1(Pathfinding.GraphNode)
extern "C"  uint32_t LevelGridNode_ilo_get_GraphIndex1_m1256941369 (Il2CppObject * __this /* static, unused */, GraphNode_t23612370 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.LevelGridNode::ilo_ResetAllGridConnections2(Pathfinding.LevelGridNode)
extern "C"  void LevelGridNode_ilo_ResetAllGridConnections2_m2482381837 (Il2CppObject * __this /* static, unused */, LevelGridNode_t489265774 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pathfinding.LevelGridNode::ilo_GetConnectionValue3(Pathfinding.LevelGridNode,System.Int32)
extern "C"  int32_t LevelGridNode_ilo_GetConnectionValue3_m2399616447 (Il2CppObject * __this /* static, unused */, LevelGridNode_t489265774 * ____this0, int32_t ___dir1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pathfinding.LevelGridNode::ilo_get_NodeInGridIndex4(Pathfinding.LevelGridNode)
extern "C"  int32_t LevelGridNode_ilo_get_NodeInGridIndex4_m1822701208 (Il2CppObject * __this /* static, unused */, LevelGridNode_t489265774 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.LayerGridGraph Pathfinding.LevelGridNode::ilo_GetGridGraph5(System.UInt32)
extern "C"  LayerGridGraph_t3415576653 * LevelGridNode_ilo_GetGridGraph5_m2045160295 (Il2CppObject * __this /* static, unused */, uint32_t ___graphIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.LevelGridNode::ilo_set_Area6(Pathfinding.GraphNode,System.UInt32)
extern "C"  void LevelGridNode_ilo_set_Area6_m2464472672 (Il2CppObject * __this /* static, unused */, GraphNode_t23612370 * ____this0, uint32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Int3 Pathfinding.LevelGridNode::ilo_op_Addition7(Pathfinding.Int3,Pathfinding.Int3)
extern "C"  Int3_t1974045594  LevelGridNode_ilo_op_Addition7_m3677184298 (Il2CppObject * __this /* static, unused */, Int3_t1974045594  ___lhs0, Int3_t1974045594  ___rhs1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.LevelGridNode::ilo_PushNode8(Pathfinding.PathHandler,Pathfinding.PathNode)
extern "C"  void LevelGridNode_ilo_PushNode8_m240003022 (Il2CppObject * __this /* static, unused */, PathHandler_t918952263 * ____this0, PathNode_t417131581 * ___node1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt16 Pathfinding.LevelGridNode::ilo_get_PathID9(Pathfinding.PathHandler)
extern "C"  uint16_t LevelGridNode_ilo_get_PathID9_m3348941566 (Il2CppObject * __this /* static, unused */, PathHandler_t918952263 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Pathfinding.LevelGridNode::ilo_GetTraversalCost10(Pathfinding.Path,Pathfinding.GraphNode)
extern "C"  uint32_t LevelGridNode_ilo_GetTraversalCost10_m3496804702 (Il2CppObject * __this /* static, unused */, Path_t1974241691 * ____this0, GraphNode_t23612370 * ___node1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Pathfinding.LevelGridNode::ilo_get_G11(Pathfinding.PathNode)
extern "C"  uint32_t LevelGridNode_ilo_get_G11_m14508412 (Il2CppObject * __this /* static, unused */, PathNode_t417131581 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.LevelGridNode::ilo_set_cost12(Pathfinding.PathNode,System.UInt32)
extern "C"  void LevelGridNode_ilo_set_cost12_m2745962620 (Il2CppObject * __this /* static, unused */, PathNode_t417131581 * ____this0, uint32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
