﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_capeepasCaskow140
struct M_capeepasCaskow140_t3916443583;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_capeepasCaskow1403916443583.h"

// System.Void GarbageiOS.M_capeepasCaskow140::.ctor()
extern "C"  void M_capeepasCaskow140__ctor_m1738776708 (M_capeepasCaskow140_t3916443583 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_capeepasCaskow140::M_mallborDearqow0(System.String[],System.Int32)
extern "C"  void M_capeepasCaskow140_M_mallborDearqow0_m2658591105 (M_capeepasCaskow140_t3916443583 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_capeepasCaskow140::M_keaceepi1(System.String[],System.Int32)
extern "C"  void M_capeepasCaskow140_M_keaceepi1_m163064195 (M_capeepasCaskow140_t3916443583 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_capeepasCaskow140::M_bofojaCeelalal2(System.String[],System.Int32)
extern "C"  void M_capeepasCaskow140_M_bofojaCeelalal2_m94416109 (M_capeepasCaskow140_t3916443583 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_capeepasCaskow140::M_saybugaJerece3(System.String[],System.Int32)
extern "C"  void M_capeepasCaskow140_M_saybugaJerece3_m2289872116 (M_capeepasCaskow140_t3916443583 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_capeepasCaskow140::M_bemdayjem4(System.String[],System.Int32)
extern "C"  void M_capeepasCaskow140_M_bemdayjem4_m2568442557 (M_capeepasCaskow140_t3916443583 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_capeepasCaskow140::M_rurminallNoumar5(System.String[],System.Int32)
extern "C"  void M_capeepasCaskow140_M_rurminallNoumar5_m361996982 (M_capeepasCaskow140_t3916443583 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_capeepasCaskow140::ilo_M_saybugaJerece31(GarbageiOS.M_capeepasCaskow140,System.String[],System.Int32)
extern "C"  void M_capeepasCaskow140_ilo_M_saybugaJerece31_m3103153713 (Il2CppObject * __this /* static, unused */, M_capeepasCaskow140_t3916443583 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_capeepasCaskow140::ilo_M_bemdayjem42(GarbageiOS.M_capeepasCaskow140,System.String[],System.Int32)
extern "C"  void M_capeepasCaskow140_ilo_M_bemdayjem42_m3427451161 (Il2CppObject * __this /* static, unused */, M_capeepasCaskow140_t3916443583 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
