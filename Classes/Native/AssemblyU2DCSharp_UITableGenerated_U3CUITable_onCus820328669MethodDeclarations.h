﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UITableGenerated/<UITable_onCustomSort_GetDelegate_member9_arg0>c__AnonStoreyD0
struct U3CUITable_onCustomSort_GetDelegate_member9_arg0U3Ec__AnonStoreyD0_t820328669;
// UnityEngine.Transform
struct Transform_t1659122786;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Transform1659122786.h"

// System.Void UITableGenerated/<UITable_onCustomSort_GetDelegate_member9_arg0>c__AnonStoreyD0::.ctor()
extern "C"  void U3CUITable_onCustomSort_GetDelegate_member9_arg0U3Ec__AnonStoreyD0__ctor_m865924910 (U3CUITable_onCustomSort_GetDelegate_member9_arg0U3Ec__AnonStoreyD0_t820328669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UITableGenerated/<UITable_onCustomSort_GetDelegate_member9_arg0>c__AnonStoreyD0::<>m__165(UnityEngine.Transform,UnityEngine.Transform)
extern "C"  int32_t U3CUITable_onCustomSort_GetDelegate_member9_arg0U3Ec__AnonStoreyD0_U3CU3Em__165_m163456423 (U3CUITable_onCustomSort_GetDelegate_member9_arg0U3Ec__AnonStoreyD0_t820328669 * __this, Transform_t1659122786 * ___x0, Transform_t1659122786 * ___y1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
