﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.ParticleAnimator
struct ParticleAnimator_t1365784337;
// UnityEngine.Color[]
struct ColorU5BU5D_t2441545636;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void UnityEngine.ParticleAnimator::.ctor()
extern "C"  void ParticleAnimator__ctor_m1938290304 (ParticleAnimator_t1365784337 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.ParticleAnimator::get_doesAnimateColor()
extern "C"  bool ParticleAnimator_get_doesAnimateColor_m930856814 (ParticleAnimator_t1365784337 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleAnimator::set_doesAnimateColor(System.Boolean)
extern "C"  void ParticleAnimator_set_doesAnimateColor_m3512818547 (ParticleAnimator_t1365784337 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.ParticleAnimator::get_worldRotationAxis()
extern "C"  Vector3_t4282066566  ParticleAnimator_get_worldRotationAxis_m1258271180 (ParticleAnimator_t1365784337 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleAnimator::set_worldRotationAxis(UnityEngine.Vector3)
extern "C"  void ParticleAnimator_set_worldRotationAxis_m1012484999 (ParticleAnimator_t1365784337 * __this, Vector3_t4282066566  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleAnimator::INTERNAL_get_worldRotationAxis(UnityEngine.Vector3&)
extern "C"  void ParticleAnimator_INTERNAL_get_worldRotationAxis_m521210301 (ParticleAnimator_t1365784337 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleAnimator::INTERNAL_set_worldRotationAxis(UnityEngine.Vector3&)
extern "C"  void ParticleAnimator_INTERNAL_set_worldRotationAxis_m3726785225 (ParticleAnimator_t1365784337 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.ParticleAnimator::get_localRotationAxis()
extern "C"  Vector3_t4282066566  ParticleAnimator_get_localRotationAxis_m3629139429 (ParticleAnimator_t1365784337 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleAnimator::set_localRotationAxis(UnityEngine.Vector3)
extern "C"  void ParticleAnimator_set_localRotationAxis_m3150325454 (ParticleAnimator_t1365784337 * __this, Vector3_t4282066566  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleAnimator::INTERNAL_get_localRotationAxis(UnityEngine.Vector3&)
extern "C"  void ParticleAnimator_INTERNAL_get_localRotationAxis_m2369754966 (ParticleAnimator_t1365784337 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleAnimator::INTERNAL_set_localRotationAxis(UnityEngine.Vector3&)
extern "C"  void ParticleAnimator_INTERNAL_set_localRotationAxis_m1280362594 (ParticleAnimator_t1365784337 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.ParticleAnimator::get_sizeGrow()
extern "C"  float ParticleAnimator_get_sizeGrow_m3525902317 (ParticleAnimator_t1365784337 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleAnimator::set_sizeGrow(System.Single)
extern "C"  void ParticleAnimator_set_sizeGrow_m2619492726 (ParticleAnimator_t1365784337 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.ParticleAnimator::get_rndForce()
extern "C"  Vector3_t4282066566  ParticleAnimator_get_rndForce_m695650570 (ParticleAnimator_t1365784337 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleAnimator::set_rndForce(UnityEngine.Vector3)
extern "C"  void ParticleAnimator_set_rndForce_m3357846613 (ParticleAnimator_t1365784337 * __this, Vector3_t4282066566  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleAnimator::INTERNAL_get_rndForce(UnityEngine.Vector3&)
extern "C"  void ParticleAnimator_INTERNAL_get_rndForce_m1761078911 (ParticleAnimator_t1365784337 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleAnimator::INTERNAL_set_rndForce(UnityEngine.Vector3&)
extern "C"  void ParticleAnimator_INTERNAL_set_rndForce_m1182081011 (ParticleAnimator_t1365784337 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.ParticleAnimator::get_force()
extern "C"  Vector3_t4282066566  ParticleAnimator_get_force_m2839698502 (ParticleAnimator_t1365784337 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleAnimator::set_force(UnityEngine.Vector3)
extern "C"  void ParticleAnimator_set_force_m109359565 (ParticleAnimator_t1365784337 * __this, Vector3_t4282066566  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleAnimator::INTERNAL_get_force(UnityEngine.Vector3&)
extern "C"  void ParticleAnimator_INTERNAL_get_force_m2110951479 (ParticleAnimator_t1365784337 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleAnimator::INTERNAL_set_force(UnityEngine.Vector3&)
extern "C"  void ParticleAnimator_INTERNAL_set_force_m2025583427 (ParticleAnimator_t1365784337 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.ParticleAnimator::get_damping()
extern "C"  float ParticleAnimator_get_damping_m3061547595 (ParticleAnimator_t1365784337 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleAnimator::set_damping(System.Single)
extern "C"  void ParticleAnimator_set_damping_m3956826024 (ParticleAnimator_t1365784337 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.ParticleAnimator::get_autodestruct()
extern "C"  bool ParticleAnimator_get_autodestruct_m2565123608 (ParticleAnimator_t1365784337 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleAnimator::set_autodestruct(System.Boolean)
extern "C"  void ParticleAnimator_set_autodestruct_m520884125 (ParticleAnimator_t1365784337 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color[] UnityEngine.ParticleAnimator::get_colorAnimation()
extern "C"  ColorU5BU5D_t2441545636* ParticleAnimator_get_colorAnimation_m440474995 (ParticleAnimator_t1365784337 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleAnimator::set_colorAnimation(UnityEngine.Color[])
extern "C"  void ParticleAnimator_set_colorAnimation_m200484290 (ParticleAnimator_t1365784337 * __this, ColorU5BU5D_t2441545636* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
