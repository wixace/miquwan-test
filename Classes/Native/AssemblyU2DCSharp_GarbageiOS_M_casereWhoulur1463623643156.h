﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_casereWhoulur146
struct  M_casereWhoulur146_t3623643156  : public Il2CppObject
{
public:
	// System.Boolean GarbageiOS.M_casereWhoulur146::_jirpar
	bool ____jirpar_0;
	// System.UInt32 GarbageiOS.M_casereWhoulur146::_temporKagar
	uint32_t ____temporKagar_1;
	// System.Boolean GarbageiOS.M_casereWhoulur146::_beyuPisea
	bool ____beyuPisea_2;
	// System.String GarbageiOS.M_casereWhoulur146::_robisbaw
	String_t* ____robisbaw_3;
	// System.Single GarbageiOS.M_casereWhoulur146::_dige
	float ____dige_4;
	// System.Boolean GarbageiOS.M_casereWhoulur146::_chalee
	bool ____chalee_5;
	// System.Single GarbageiOS.M_casereWhoulur146::_lortopay
	float ____lortopay_6;
	// System.Int32 GarbageiOS.M_casereWhoulur146::_purrou
	int32_t ____purrou_7;
	// System.Boolean GarbageiOS.M_casereWhoulur146::_celbirse
	bool ____celbirse_8;
	// System.Int32 GarbageiOS.M_casereWhoulur146::_cerqe
	int32_t ____cerqe_9;
	// System.Int32 GarbageiOS.M_casereWhoulur146::_jallmo
	int32_t ____jallmo_10;
	// System.Single GarbageiOS.M_casereWhoulur146::_bistelsooJerjee
	float ____bistelsooJerjee_11;
	// System.String GarbageiOS.M_casereWhoulur146::_qairrarwerePelere
	String_t* ____qairrarwerePelere_12;
	// System.Single GarbageiOS.M_casereWhoulur146::_zelselcaWaifawhu
	float ____zelselcaWaifawhu_13;
	// System.UInt32 GarbageiOS.M_casereWhoulur146::_sarfeaMemsearjee
	uint32_t ____sarfeaMemsearjee_14;
	// System.Boolean GarbageiOS.M_casereWhoulur146::_cefor
	bool ____cefor_15;
	// System.Int32 GarbageiOS.M_casereWhoulur146::_merepaTearcuya
	int32_t ____merepaTearcuya_16;
	// System.Single GarbageiOS.M_casereWhoulur146::_salcowMorerdraw
	float ____salcowMorerdraw_17;
	// System.String GarbageiOS.M_casereWhoulur146::_falldall
	String_t* ____falldall_18;
	// System.String GarbageiOS.M_casereWhoulur146::_janemzi
	String_t* ____janemzi_19;
	// System.String GarbageiOS.M_casereWhoulur146::_cobecerTelzou
	String_t* ____cobecerTelzou_20;
	// System.Boolean GarbageiOS.M_casereWhoulur146::_luza
	bool ____luza_21;
	// System.Int32 GarbageiOS.M_casereWhoulur146::_mifallma
	int32_t ____mifallma_22;
	// System.UInt32 GarbageiOS.M_casereWhoulur146::_sejairNomema
	uint32_t ____sejairNomema_23;
	// System.UInt32 GarbageiOS.M_casereWhoulur146::_bibaChojemnor
	uint32_t ____bibaChojemnor_24;
	// System.UInt32 GarbageiOS.M_casereWhoulur146::_lelas
	uint32_t ____lelas_25;
	// System.String GarbageiOS.M_casereWhoulur146::_dedroudis
	String_t* ____dedroudis_26;
	// System.UInt32 GarbageiOS.M_casereWhoulur146::_ticee
	uint32_t ____ticee_27;
	// System.UInt32 GarbageiOS.M_casereWhoulur146::_stamear
	uint32_t ____stamear_28;
	// System.String GarbageiOS.M_casereWhoulur146::_sikiCairdelee
	String_t* ____sikiCairdelee_29;
	// System.UInt32 GarbageiOS.M_casereWhoulur146::_mearcake
	uint32_t ____mearcake_30;
	// System.Single GarbageiOS.M_casereWhoulur146::_whemear
	float ____whemear_31;
	// System.Int32 GarbageiOS.M_casereWhoulur146::_drelkeabem
	int32_t ____drelkeabem_32;
	// System.String GarbageiOS.M_casereWhoulur146::_halba
	String_t* ____halba_33;
	// System.String GarbageiOS.M_casereWhoulur146::_whaimusaiJairmir
	String_t* ____whaimusaiJairmir_34;

public:
	inline static int32_t get_offset_of__jirpar_0() { return static_cast<int32_t>(offsetof(M_casereWhoulur146_t3623643156, ____jirpar_0)); }
	inline bool get__jirpar_0() const { return ____jirpar_0; }
	inline bool* get_address_of__jirpar_0() { return &____jirpar_0; }
	inline void set__jirpar_0(bool value)
	{
		____jirpar_0 = value;
	}

	inline static int32_t get_offset_of__temporKagar_1() { return static_cast<int32_t>(offsetof(M_casereWhoulur146_t3623643156, ____temporKagar_1)); }
	inline uint32_t get__temporKagar_1() const { return ____temporKagar_1; }
	inline uint32_t* get_address_of__temporKagar_1() { return &____temporKagar_1; }
	inline void set__temporKagar_1(uint32_t value)
	{
		____temporKagar_1 = value;
	}

	inline static int32_t get_offset_of__beyuPisea_2() { return static_cast<int32_t>(offsetof(M_casereWhoulur146_t3623643156, ____beyuPisea_2)); }
	inline bool get__beyuPisea_2() const { return ____beyuPisea_2; }
	inline bool* get_address_of__beyuPisea_2() { return &____beyuPisea_2; }
	inline void set__beyuPisea_2(bool value)
	{
		____beyuPisea_2 = value;
	}

	inline static int32_t get_offset_of__robisbaw_3() { return static_cast<int32_t>(offsetof(M_casereWhoulur146_t3623643156, ____robisbaw_3)); }
	inline String_t* get__robisbaw_3() const { return ____robisbaw_3; }
	inline String_t** get_address_of__robisbaw_3() { return &____robisbaw_3; }
	inline void set__robisbaw_3(String_t* value)
	{
		____robisbaw_3 = value;
		Il2CppCodeGenWriteBarrier(&____robisbaw_3, value);
	}

	inline static int32_t get_offset_of__dige_4() { return static_cast<int32_t>(offsetof(M_casereWhoulur146_t3623643156, ____dige_4)); }
	inline float get__dige_4() const { return ____dige_4; }
	inline float* get_address_of__dige_4() { return &____dige_4; }
	inline void set__dige_4(float value)
	{
		____dige_4 = value;
	}

	inline static int32_t get_offset_of__chalee_5() { return static_cast<int32_t>(offsetof(M_casereWhoulur146_t3623643156, ____chalee_5)); }
	inline bool get__chalee_5() const { return ____chalee_5; }
	inline bool* get_address_of__chalee_5() { return &____chalee_5; }
	inline void set__chalee_5(bool value)
	{
		____chalee_5 = value;
	}

	inline static int32_t get_offset_of__lortopay_6() { return static_cast<int32_t>(offsetof(M_casereWhoulur146_t3623643156, ____lortopay_6)); }
	inline float get__lortopay_6() const { return ____lortopay_6; }
	inline float* get_address_of__lortopay_6() { return &____lortopay_6; }
	inline void set__lortopay_6(float value)
	{
		____lortopay_6 = value;
	}

	inline static int32_t get_offset_of__purrou_7() { return static_cast<int32_t>(offsetof(M_casereWhoulur146_t3623643156, ____purrou_7)); }
	inline int32_t get__purrou_7() const { return ____purrou_7; }
	inline int32_t* get_address_of__purrou_7() { return &____purrou_7; }
	inline void set__purrou_7(int32_t value)
	{
		____purrou_7 = value;
	}

	inline static int32_t get_offset_of__celbirse_8() { return static_cast<int32_t>(offsetof(M_casereWhoulur146_t3623643156, ____celbirse_8)); }
	inline bool get__celbirse_8() const { return ____celbirse_8; }
	inline bool* get_address_of__celbirse_8() { return &____celbirse_8; }
	inline void set__celbirse_8(bool value)
	{
		____celbirse_8 = value;
	}

	inline static int32_t get_offset_of__cerqe_9() { return static_cast<int32_t>(offsetof(M_casereWhoulur146_t3623643156, ____cerqe_9)); }
	inline int32_t get__cerqe_9() const { return ____cerqe_9; }
	inline int32_t* get_address_of__cerqe_9() { return &____cerqe_9; }
	inline void set__cerqe_9(int32_t value)
	{
		____cerqe_9 = value;
	}

	inline static int32_t get_offset_of__jallmo_10() { return static_cast<int32_t>(offsetof(M_casereWhoulur146_t3623643156, ____jallmo_10)); }
	inline int32_t get__jallmo_10() const { return ____jallmo_10; }
	inline int32_t* get_address_of__jallmo_10() { return &____jallmo_10; }
	inline void set__jallmo_10(int32_t value)
	{
		____jallmo_10 = value;
	}

	inline static int32_t get_offset_of__bistelsooJerjee_11() { return static_cast<int32_t>(offsetof(M_casereWhoulur146_t3623643156, ____bistelsooJerjee_11)); }
	inline float get__bistelsooJerjee_11() const { return ____bistelsooJerjee_11; }
	inline float* get_address_of__bistelsooJerjee_11() { return &____bistelsooJerjee_11; }
	inline void set__bistelsooJerjee_11(float value)
	{
		____bistelsooJerjee_11 = value;
	}

	inline static int32_t get_offset_of__qairrarwerePelere_12() { return static_cast<int32_t>(offsetof(M_casereWhoulur146_t3623643156, ____qairrarwerePelere_12)); }
	inline String_t* get__qairrarwerePelere_12() const { return ____qairrarwerePelere_12; }
	inline String_t** get_address_of__qairrarwerePelere_12() { return &____qairrarwerePelere_12; }
	inline void set__qairrarwerePelere_12(String_t* value)
	{
		____qairrarwerePelere_12 = value;
		Il2CppCodeGenWriteBarrier(&____qairrarwerePelere_12, value);
	}

	inline static int32_t get_offset_of__zelselcaWaifawhu_13() { return static_cast<int32_t>(offsetof(M_casereWhoulur146_t3623643156, ____zelselcaWaifawhu_13)); }
	inline float get__zelselcaWaifawhu_13() const { return ____zelselcaWaifawhu_13; }
	inline float* get_address_of__zelselcaWaifawhu_13() { return &____zelselcaWaifawhu_13; }
	inline void set__zelselcaWaifawhu_13(float value)
	{
		____zelselcaWaifawhu_13 = value;
	}

	inline static int32_t get_offset_of__sarfeaMemsearjee_14() { return static_cast<int32_t>(offsetof(M_casereWhoulur146_t3623643156, ____sarfeaMemsearjee_14)); }
	inline uint32_t get__sarfeaMemsearjee_14() const { return ____sarfeaMemsearjee_14; }
	inline uint32_t* get_address_of__sarfeaMemsearjee_14() { return &____sarfeaMemsearjee_14; }
	inline void set__sarfeaMemsearjee_14(uint32_t value)
	{
		____sarfeaMemsearjee_14 = value;
	}

	inline static int32_t get_offset_of__cefor_15() { return static_cast<int32_t>(offsetof(M_casereWhoulur146_t3623643156, ____cefor_15)); }
	inline bool get__cefor_15() const { return ____cefor_15; }
	inline bool* get_address_of__cefor_15() { return &____cefor_15; }
	inline void set__cefor_15(bool value)
	{
		____cefor_15 = value;
	}

	inline static int32_t get_offset_of__merepaTearcuya_16() { return static_cast<int32_t>(offsetof(M_casereWhoulur146_t3623643156, ____merepaTearcuya_16)); }
	inline int32_t get__merepaTearcuya_16() const { return ____merepaTearcuya_16; }
	inline int32_t* get_address_of__merepaTearcuya_16() { return &____merepaTearcuya_16; }
	inline void set__merepaTearcuya_16(int32_t value)
	{
		____merepaTearcuya_16 = value;
	}

	inline static int32_t get_offset_of__salcowMorerdraw_17() { return static_cast<int32_t>(offsetof(M_casereWhoulur146_t3623643156, ____salcowMorerdraw_17)); }
	inline float get__salcowMorerdraw_17() const { return ____salcowMorerdraw_17; }
	inline float* get_address_of__salcowMorerdraw_17() { return &____salcowMorerdraw_17; }
	inline void set__salcowMorerdraw_17(float value)
	{
		____salcowMorerdraw_17 = value;
	}

	inline static int32_t get_offset_of__falldall_18() { return static_cast<int32_t>(offsetof(M_casereWhoulur146_t3623643156, ____falldall_18)); }
	inline String_t* get__falldall_18() const { return ____falldall_18; }
	inline String_t** get_address_of__falldall_18() { return &____falldall_18; }
	inline void set__falldall_18(String_t* value)
	{
		____falldall_18 = value;
		Il2CppCodeGenWriteBarrier(&____falldall_18, value);
	}

	inline static int32_t get_offset_of__janemzi_19() { return static_cast<int32_t>(offsetof(M_casereWhoulur146_t3623643156, ____janemzi_19)); }
	inline String_t* get__janemzi_19() const { return ____janemzi_19; }
	inline String_t** get_address_of__janemzi_19() { return &____janemzi_19; }
	inline void set__janemzi_19(String_t* value)
	{
		____janemzi_19 = value;
		Il2CppCodeGenWriteBarrier(&____janemzi_19, value);
	}

	inline static int32_t get_offset_of__cobecerTelzou_20() { return static_cast<int32_t>(offsetof(M_casereWhoulur146_t3623643156, ____cobecerTelzou_20)); }
	inline String_t* get__cobecerTelzou_20() const { return ____cobecerTelzou_20; }
	inline String_t** get_address_of__cobecerTelzou_20() { return &____cobecerTelzou_20; }
	inline void set__cobecerTelzou_20(String_t* value)
	{
		____cobecerTelzou_20 = value;
		Il2CppCodeGenWriteBarrier(&____cobecerTelzou_20, value);
	}

	inline static int32_t get_offset_of__luza_21() { return static_cast<int32_t>(offsetof(M_casereWhoulur146_t3623643156, ____luza_21)); }
	inline bool get__luza_21() const { return ____luza_21; }
	inline bool* get_address_of__luza_21() { return &____luza_21; }
	inline void set__luza_21(bool value)
	{
		____luza_21 = value;
	}

	inline static int32_t get_offset_of__mifallma_22() { return static_cast<int32_t>(offsetof(M_casereWhoulur146_t3623643156, ____mifallma_22)); }
	inline int32_t get__mifallma_22() const { return ____mifallma_22; }
	inline int32_t* get_address_of__mifallma_22() { return &____mifallma_22; }
	inline void set__mifallma_22(int32_t value)
	{
		____mifallma_22 = value;
	}

	inline static int32_t get_offset_of__sejairNomema_23() { return static_cast<int32_t>(offsetof(M_casereWhoulur146_t3623643156, ____sejairNomema_23)); }
	inline uint32_t get__sejairNomema_23() const { return ____sejairNomema_23; }
	inline uint32_t* get_address_of__sejairNomema_23() { return &____sejairNomema_23; }
	inline void set__sejairNomema_23(uint32_t value)
	{
		____sejairNomema_23 = value;
	}

	inline static int32_t get_offset_of__bibaChojemnor_24() { return static_cast<int32_t>(offsetof(M_casereWhoulur146_t3623643156, ____bibaChojemnor_24)); }
	inline uint32_t get__bibaChojemnor_24() const { return ____bibaChojemnor_24; }
	inline uint32_t* get_address_of__bibaChojemnor_24() { return &____bibaChojemnor_24; }
	inline void set__bibaChojemnor_24(uint32_t value)
	{
		____bibaChojemnor_24 = value;
	}

	inline static int32_t get_offset_of__lelas_25() { return static_cast<int32_t>(offsetof(M_casereWhoulur146_t3623643156, ____lelas_25)); }
	inline uint32_t get__lelas_25() const { return ____lelas_25; }
	inline uint32_t* get_address_of__lelas_25() { return &____lelas_25; }
	inline void set__lelas_25(uint32_t value)
	{
		____lelas_25 = value;
	}

	inline static int32_t get_offset_of__dedroudis_26() { return static_cast<int32_t>(offsetof(M_casereWhoulur146_t3623643156, ____dedroudis_26)); }
	inline String_t* get__dedroudis_26() const { return ____dedroudis_26; }
	inline String_t** get_address_of__dedroudis_26() { return &____dedroudis_26; }
	inline void set__dedroudis_26(String_t* value)
	{
		____dedroudis_26 = value;
		Il2CppCodeGenWriteBarrier(&____dedroudis_26, value);
	}

	inline static int32_t get_offset_of__ticee_27() { return static_cast<int32_t>(offsetof(M_casereWhoulur146_t3623643156, ____ticee_27)); }
	inline uint32_t get__ticee_27() const { return ____ticee_27; }
	inline uint32_t* get_address_of__ticee_27() { return &____ticee_27; }
	inline void set__ticee_27(uint32_t value)
	{
		____ticee_27 = value;
	}

	inline static int32_t get_offset_of__stamear_28() { return static_cast<int32_t>(offsetof(M_casereWhoulur146_t3623643156, ____stamear_28)); }
	inline uint32_t get__stamear_28() const { return ____stamear_28; }
	inline uint32_t* get_address_of__stamear_28() { return &____stamear_28; }
	inline void set__stamear_28(uint32_t value)
	{
		____stamear_28 = value;
	}

	inline static int32_t get_offset_of__sikiCairdelee_29() { return static_cast<int32_t>(offsetof(M_casereWhoulur146_t3623643156, ____sikiCairdelee_29)); }
	inline String_t* get__sikiCairdelee_29() const { return ____sikiCairdelee_29; }
	inline String_t** get_address_of__sikiCairdelee_29() { return &____sikiCairdelee_29; }
	inline void set__sikiCairdelee_29(String_t* value)
	{
		____sikiCairdelee_29 = value;
		Il2CppCodeGenWriteBarrier(&____sikiCairdelee_29, value);
	}

	inline static int32_t get_offset_of__mearcake_30() { return static_cast<int32_t>(offsetof(M_casereWhoulur146_t3623643156, ____mearcake_30)); }
	inline uint32_t get__mearcake_30() const { return ____mearcake_30; }
	inline uint32_t* get_address_of__mearcake_30() { return &____mearcake_30; }
	inline void set__mearcake_30(uint32_t value)
	{
		____mearcake_30 = value;
	}

	inline static int32_t get_offset_of__whemear_31() { return static_cast<int32_t>(offsetof(M_casereWhoulur146_t3623643156, ____whemear_31)); }
	inline float get__whemear_31() const { return ____whemear_31; }
	inline float* get_address_of__whemear_31() { return &____whemear_31; }
	inline void set__whemear_31(float value)
	{
		____whemear_31 = value;
	}

	inline static int32_t get_offset_of__drelkeabem_32() { return static_cast<int32_t>(offsetof(M_casereWhoulur146_t3623643156, ____drelkeabem_32)); }
	inline int32_t get__drelkeabem_32() const { return ____drelkeabem_32; }
	inline int32_t* get_address_of__drelkeabem_32() { return &____drelkeabem_32; }
	inline void set__drelkeabem_32(int32_t value)
	{
		____drelkeabem_32 = value;
	}

	inline static int32_t get_offset_of__halba_33() { return static_cast<int32_t>(offsetof(M_casereWhoulur146_t3623643156, ____halba_33)); }
	inline String_t* get__halba_33() const { return ____halba_33; }
	inline String_t** get_address_of__halba_33() { return &____halba_33; }
	inline void set__halba_33(String_t* value)
	{
		____halba_33 = value;
		Il2CppCodeGenWriteBarrier(&____halba_33, value);
	}

	inline static int32_t get_offset_of__whaimusaiJairmir_34() { return static_cast<int32_t>(offsetof(M_casereWhoulur146_t3623643156, ____whaimusaiJairmir_34)); }
	inline String_t* get__whaimusaiJairmir_34() const { return ____whaimusaiJairmir_34; }
	inline String_t** get_address_of__whaimusaiJairmir_34() { return &____whaimusaiJairmir_34; }
	inline void set__whaimusaiJairmir_34(String_t* value)
	{
		____whaimusaiJairmir_34 = value;
		Il2CppCodeGenWriteBarrier(&____whaimusaiJairmir_34, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
