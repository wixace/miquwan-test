﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Pathfinding.Serialization.GraphSerializationContext
struct GraphSerializationContext_t3256954663;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.Serialization.AstarSerializer/<DeserializeExtraInfo>c__AnonStorey111
struct  U3CDeserializeExtraInfoU3Ec__AnonStorey111_t3805251455  : public Il2CppObject
{
public:
	// Pathfinding.Serialization.GraphSerializationContext Pathfinding.Serialization.AstarSerializer/<DeserializeExtraInfo>c__AnonStorey111::ctx
	GraphSerializationContext_t3256954663 * ___ctx_0;

public:
	inline static int32_t get_offset_of_ctx_0() { return static_cast<int32_t>(offsetof(U3CDeserializeExtraInfoU3Ec__AnonStorey111_t3805251455, ___ctx_0)); }
	inline GraphSerializationContext_t3256954663 * get_ctx_0() const { return ___ctx_0; }
	inline GraphSerializationContext_t3256954663 ** get_address_of_ctx_0() { return &___ctx_0; }
	inline void set_ctx_0(GraphSerializationContext_t3256954663 * value)
	{
		___ctx_0 = value;
		Il2CppCodeGenWriteBarrier(&___ctx_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
