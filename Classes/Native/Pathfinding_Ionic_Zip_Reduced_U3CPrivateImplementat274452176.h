﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object4170816371.h"
#include "Pathfinding_Ionic_Zip_Reduced_U3CPrivateImplementa3949510224.h"
#include "Pathfinding_Ionic_Zip_Reduced_U3CPrivateImplementa3023352554.h"
#include "Pathfinding_Ionic_Zip_Reduced_U3CPrivateImplementa2175734816.h"
#include "Pathfinding_Ionic_Zip_Reduced_U3CPrivateImplementa2175732708.h"
#include "Pathfinding_Ionic_Zip_Reduced_U3CPrivateImplementa2175732704.h"
#include "Pathfinding_Ionic_Zip_Reduced_U3CPrivateImplementa3949510414.h"
#include "Pathfinding_Ionic_Zip_Reduced_U3CPrivateImplementa3949510385.h"
#include "Pathfinding_Ionic_Zip_Reduced_U3CPrivateImplementa2175732679.h"
#include "Pathfinding_Ionic_Zip_Reduced_U3CPrivateImplementa3949510253.h"
#include "Pathfinding_Ionic_Zip_Reduced_U3CPrivateImplementa2175736519.h"
#include "Pathfinding_Ionic_Zip_Reduced_U3CPrivateImplementa2175733764.h"
#include "Pathfinding_Ionic_Zip_Reduced_U3CPrivateImplementa3023203628.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>{99f15b47-93f0-442d-a950-84e2e79a92c8}
struct  U3CPrivateImplementationDetailsU3EU7B99f15b47U2D93f0U2D442dU2Da950U2D84e2e79a92c8U7D_t274452176  : public Il2CppObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3EU7B99f15b47U2D93f0U2D442dU2Da950U2D84e2e79a92c8U7D_t274452176_StaticFields
{
public:
	// <PrivateImplementationDetails>{99f15b47-93f0-442d-a950-84e2e79a92c8}/$ArrayType=12 <PrivateImplementationDetails>{99f15b47-93f0-442d-a950-84e2e79a92c8}::$field-0
	U24ArrayTypeU3D12_t3949510224  ___U24fieldU2D0_0;
	// <PrivateImplementationDetails>{99f15b47-93f0-442d-a950-84e2e79a92c8}/$ArrayType=6144 <PrivateImplementationDetails>{99f15b47-93f0-442d-a950-84e2e79a92c8}::$field-5
	U24ArrayTypeU3D6144_t3023352554  ___U24fieldU2D5_1;
	// <PrivateImplementationDetails>{99f15b47-93f0-442d-a950-84e2e79a92c8}/$ArrayType=384 <PrivateImplementationDetails>{99f15b47-93f0-442d-a950-84e2e79a92c8}::$field-6
	U24ArrayTypeU3D384_t2175734816  ___U24fieldU2D6_2;
	// <PrivateImplementationDetails>{99f15b47-93f0-442d-a950-84e2e79a92c8}/$ArrayType=124 <PrivateImplementationDetails>{99f15b47-93f0-442d-a950-84e2e79a92c8}::$field-7
	U24ArrayTypeU3D124_t2175732708  ___U24fieldU2D7_3;
	// <PrivateImplementationDetails>{99f15b47-93f0-442d-a950-84e2e79a92c8}/$ArrayType=124 <PrivateImplementationDetails>{99f15b47-93f0-442d-a950-84e2e79a92c8}::$field-8
	U24ArrayTypeU3D124_t2175732708  ___U24fieldU2D8_4;
	// <PrivateImplementationDetails>{99f15b47-93f0-442d-a950-84e2e79a92c8}/$ArrayType=120 <PrivateImplementationDetails>{99f15b47-93f0-442d-a950-84e2e79a92c8}::$field-9
	U24ArrayTypeU3D120_t2175732704  ___U24fieldU2D9_5;
	// <PrivateImplementationDetails>{99f15b47-93f0-442d-a950-84e2e79a92c8}/$ArrayType=120 <PrivateImplementationDetails>{99f15b47-93f0-442d-a950-84e2e79a92c8}::$field-A
	U24ArrayTypeU3D120_t2175732704  ___U24fieldU2DA_6;
	// <PrivateImplementationDetails>{99f15b47-93f0-442d-a950-84e2e79a92c8}/$ArrayType=76 <PrivateImplementationDetails>{99f15b47-93f0-442d-a950-84e2e79a92c8}::$field-B
	U24ArrayTypeU3D76_t3949510414  ___U24fieldU2DB_7;
	// <PrivateImplementationDetails>{99f15b47-93f0-442d-a950-84e2e79a92c8}/$ArrayType=68 <PrivateImplementationDetails>{99f15b47-93f0-442d-a950-84e2e79a92c8}::$field-C
	U24ArrayTypeU3D68_t3949510385  ___U24fieldU2DC_8;
	// <PrivateImplementationDetails>{99f15b47-93f0-442d-a950-84e2e79a92c8}/$ArrayType=116 <PrivateImplementationDetails>{99f15b47-93f0-442d-a950-84e2e79a92c8}::$field-D
	U24ArrayTypeU3D116_t2175732679  ___U24fieldU2DD_9;
	// <PrivateImplementationDetails>{99f15b47-93f0-442d-a950-84e2e79a92c8}/$ArrayType=120 <PrivateImplementationDetails>{99f15b47-93f0-442d-a950-84e2e79a92c8}::$field-E
	U24ArrayTypeU3D120_t2175732704  ___U24fieldU2DE_10;
	// <PrivateImplementationDetails>{99f15b47-93f0-442d-a950-84e2e79a92c8}/$ArrayType=76 <PrivateImplementationDetails>{99f15b47-93f0-442d-a950-84e2e79a92c8}::$field-F
	U24ArrayTypeU3D76_t3949510414  ___U24fieldU2DF_11;
	// <PrivateImplementationDetails>{99f15b47-93f0-442d-a950-84e2e79a92c8}/$ArrayType=20 <PrivateImplementationDetails>{99f15b47-93f0-442d-a950-84e2e79a92c8}::$field-10
	U24ArrayTypeU3D20_t3949510253  ___U24fieldU2D10_12;
	// <PrivateImplementationDetails>{99f15b47-93f0-442d-a950-84e2e79a92c8}/$ArrayType=512 <PrivateImplementationDetails>{99f15b47-93f0-442d-a950-84e2e79a92c8}::$field-11
	U24ArrayTypeU3D512_t2175736519  ___U24fieldU2D11_13;
	// <PrivateImplementationDetails>{99f15b47-93f0-442d-a950-84e2e79a92c8}/$ArrayType=256 <PrivateImplementationDetails>{99f15b47-93f0-442d-a950-84e2e79a92c8}::$field-12
	U24ArrayTypeU3D256_t2175733764  ___U24fieldU2D12_14;
	// <PrivateImplementationDetails>{99f15b47-93f0-442d-a950-84e2e79a92c8}/$ArrayType=116 <PrivateImplementationDetails>{99f15b47-93f0-442d-a950-84e2e79a92c8}::$field-13
	U24ArrayTypeU3D116_t2175732679  ___U24fieldU2D13_15;
	// <PrivateImplementationDetails>{99f15b47-93f0-442d-a950-84e2e79a92c8}/$ArrayType=120 <PrivateImplementationDetails>{99f15b47-93f0-442d-a950-84e2e79a92c8}::$field-14
	U24ArrayTypeU3D120_t2175732704  ___U24fieldU2D14_16;
	// <PrivateImplementationDetails>{99f15b47-93f0-442d-a950-84e2e79a92c8}/$ArrayType=1152 <PrivateImplementationDetails>{99f15b47-93f0-442d-a950-84e2e79a92c8}::$field-15
	U24ArrayTypeU3D1152_t3023203628  ___U24fieldU2D15_17;
	// <PrivateImplementationDetails>{99f15b47-93f0-442d-a950-84e2e79a92c8}/$ArrayType=120 <PrivateImplementationDetails>{99f15b47-93f0-442d-a950-84e2e79a92c8}::$field-16
	U24ArrayTypeU3D120_t2175732704  ___U24fieldU2D16_18;

public:
	inline static int32_t get_offset_of_U24fieldU2D0_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3EU7B99f15b47U2D93f0U2D442dU2Da950U2D84e2e79a92c8U7D_t274452176_StaticFields, ___U24fieldU2D0_0)); }
	inline U24ArrayTypeU3D12_t3949510224  get_U24fieldU2D0_0() const { return ___U24fieldU2D0_0; }
	inline U24ArrayTypeU3D12_t3949510224 * get_address_of_U24fieldU2D0_0() { return &___U24fieldU2D0_0; }
	inline void set_U24fieldU2D0_0(U24ArrayTypeU3D12_t3949510224  value)
	{
		___U24fieldU2D0_0 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D5_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3EU7B99f15b47U2D93f0U2D442dU2Da950U2D84e2e79a92c8U7D_t274452176_StaticFields, ___U24fieldU2D5_1)); }
	inline U24ArrayTypeU3D6144_t3023352554  get_U24fieldU2D5_1() const { return ___U24fieldU2D5_1; }
	inline U24ArrayTypeU3D6144_t3023352554 * get_address_of_U24fieldU2D5_1() { return &___U24fieldU2D5_1; }
	inline void set_U24fieldU2D5_1(U24ArrayTypeU3D6144_t3023352554  value)
	{
		___U24fieldU2D5_1 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D6_2() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3EU7B99f15b47U2D93f0U2D442dU2Da950U2D84e2e79a92c8U7D_t274452176_StaticFields, ___U24fieldU2D6_2)); }
	inline U24ArrayTypeU3D384_t2175734816  get_U24fieldU2D6_2() const { return ___U24fieldU2D6_2; }
	inline U24ArrayTypeU3D384_t2175734816 * get_address_of_U24fieldU2D6_2() { return &___U24fieldU2D6_2; }
	inline void set_U24fieldU2D6_2(U24ArrayTypeU3D384_t2175734816  value)
	{
		___U24fieldU2D6_2 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D7_3() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3EU7B99f15b47U2D93f0U2D442dU2Da950U2D84e2e79a92c8U7D_t274452176_StaticFields, ___U24fieldU2D7_3)); }
	inline U24ArrayTypeU3D124_t2175732708  get_U24fieldU2D7_3() const { return ___U24fieldU2D7_3; }
	inline U24ArrayTypeU3D124_t2175732708 * get_address_of_U24fieldU2D7_3() { return &___U24fieldU2D7_3; }
	inline void set_U24fieldU2D7_3(U24ArrayTypeU3D124_t2175732708  value)
	{
		___U24fieldU2D7_3 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D8_4() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3EU7B99f15b47U2D93f0U2D442dU2Da950U2D84e2e79a92c8U7D_t274452176_StaticFields, ___U24fieldU2D8_4)); }
	inline U24ArrayTypeU3D124_t2175732708  get_U24fieldU2D8_4() const { return ___U24fieldU2D8_4; }
	inline U24ArrayTypeU3D124_t2175732708 * get_address_of_U24fieldU2D8_4() { return &___U24fieldU2D8_4; }
	inline void set_U24fieldU2D8_4(U24ArrayTypeU3D124_t2175732708  value)
	{
		___U24fieldU2D8_4 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D9_5() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3EU7B99f15b47U2D93f0U2D442dU2Da950U2D84e2e79a92c8U7D_t274452176_StaticFields, ___U24fieldU2D9_5)); }
	inline U24ArrayTypeU3D120_t2175732704  get_U24fieldU2D9_5() const { return ___U24fieldU2D9_5; }
	inline U24ArrayTypeU3D120_t2175732704 * get_address_of_U24fieldU2D9_5() { return &___U24fieldU2D9_5; }
	inline void set_U24fieldU2D9_5(U24ArrayTypeU3D120_t2175732704  value)
	{
		___U24fieldU2D9_5 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DA_6() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3EU7B99f15b47U2D93f0U2D442dU2Da950U2D84e2e79a92c8U7D_t274452176_StaticFields, ___U24fieldU2DA_6)); }
	inline U24ArrayTypeU3D120_t2175732704  get_U24fieldU2DA_6() const { return ___U24fieldU2DA_6; }
	inline U24ArrayTypeU3D120_t2175732704 * get_address_of_U24fieldU2DA_6() { return &___U24fieldU2DA_6; }
	inline void set_U24fieldU2DA_6(U24ArrayTypeU3D120_t2175732704  value)
	{
		___U24fieldU2DA_6 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DB_7() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3EU7B99f15b47U2D93f0U2D442dU2Da950U2D84e2e79a92c8U7D_t274452176_StaticFields, ___U24fieldU2DB_7)); }
	inline U24ArrayTypeU3D76_t3949510414  get_U24fieldU2DB_7() const { return ___U24fieldU2DB_7; }
	inline U24ArrayTypeU3D76_t3949510414 * get_address_of_U24fieldU2DB_7() { return &___U24fieldU2DB_7; }
	inline void set_U24fieldU2DB_7(U24ArrayTypeU3D76_t3949510414  value)
	{
		___U24fieldU2DB_7 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DC_8() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3EU7B99f15b47U2D93f0U2D442dU2Da950U2D84e2e79a92c8U7D_t274452176_StaticFields, ___U24fieldU2DC_8)); }
	inline U24ArrayTypeU3D68_t3949510385  get_U24fieldU2DC_8() const { return ___U24fieldU2DC_8; }
	inline U24ArrayTypeU3D68_t3949510385 * get_address_of_U24fieldU2DC_8() { return &___U24fieldU2DC_8; }
	inline void set_U24fieldU2DC_8(U24ArrayTypeU3D68_t3949510385  value)
	{
		___U24fieldU2DC_8 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DD_9() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3EU7B99f15b47U2D93f0U2D442dU2Da950U2D84e2e79a92c8U7D_t274452176_StaticFields, ___U24fieldU2DD_9)); }
	inline U24ArrayTypeU3D116_t2175732679  get_U24fieldU2DD_9() const { return ___U24fieldU2DD_9; }
	inline U24ArrayTypeU3D116_t2175732679 * get_address_of_U24fieldU2DD_9() { return &___U24fieldU2DD_9; }
	inline void set_U24fieldU2DD_9(U24ArrayTypeU3D116_t2175732679  value)
	{
		___U24fieldU2DD_9 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DE_10() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3EU7B99f15b47U2D93f0U2D442dU2Da950U2D84e2e79a92c8U7D_t274452176_StaticFields, ___U24fieldU2DE_10)); }
	inline U24ArrayTypeU3D120_t2175732704  get_U24fieldU2DE_10() const { return ___U24fieldU2DE_10; }
	inline U24ArrayTypeU3D120_t2175732704 * get_address_of_U24fieldU2DE_10() { return &___U24fieldU2DE_10; }
	inline void set_U24fieldU2DE_10(U24ArrayTypeU3D120_t2175732704  value)
	{
		___U24fieldU2DE_10 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DF_11() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3EU7B99f15b47U2D93f0U2D442dU2Da950U2D84e2e79a92c8U7D_t274452176_StaticFields, ___U24fieldU2DF_11)); }
	inline U24ArrayTypeU3D76_t3949510414  get_U24fieldU2DF_11() const { return ___U24fieldU2DF_11; }
	inline U24ArrayTypeU3D76_t3949510414 * get_address_of_U24fieldU2DF_11() { return &___U24fieldU2DF_11; }
	inline void set_U24fieldU2DF_11(U24ArrayTypeU3D76_t3949510414  value)
	{
		___U24fieldU2DF_11 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D10_12() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3EU7B99f15b47U2D93f0U2D442dU2Da950U2D84e2e79a92c8U7D_t274452176_StaticFields, ___U24fieldU2D10_12)); }
	inline U24ArrayTypeU3D20_t3949510253  get_U24fieldU2D10_12() const { return ___U24fieldU2D10_12; }
	inline U24ArrayTypeU3D20_t3949510253 * get_address_of_U24fieldU2D10_12() { return &___U24fieldU2D10_12; }
	inline void set_U24fieldU2D10_12(U24ArrayTypeU3D20_t3949510253  value)
	{
		___U24fieldU2D10_12 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D11_13() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3EU7B99f15b47U2D93f0U2D442dU2Da950U2D84e2e79a92c8U7D_t274452176_StaticFields, ___U24fieldU2D11_13)); }
	inline U24ArrayTypeU3D512_t2175736519  get_U24fieldU2D11_13() const { return ___U24fieldU2D11_13; }
	inline U24ArrayTypeU3D512_t2175736519 * get_address_of_U24fieldU2D11_13() { return &___U24fieldU2D11_13; }
	inline void set_U24fieldU2D11_13(U24ArrayTypeU3D512_t2175736519  value)
	{
		___U24fieldU2D11_13 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D12_14() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3EU7B99f15b47U2D93f0U2D442dU2Da950U2D84e2e79a92c8U7D_t274452176_StaticFields, ___U24fieldU2D12_14)); }
	inline U24ArrayTypeU3D256_t2175733764  get_U24fieldU2D12_14() const { return ___U24fieldU2D12_14; }
	inline U24ArrayTypeU3D256_t2175733764 * get_address_of_U24fieldU2D12_14() { return &___U24fieldU2D12_14; }
	inline void set_U24fieldU2D12_14(U24ArrayTypeU3D256_t2175733764  value)
	{
		___U24fieldU2D12_14 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D13_15() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3EU7B99f15b47U2D93f0U2D442dU2Da950U2D84e2e79a92c8U7D_t274452176_StaticFields, ___U24fieldU2D13_15)); }
	inline U24ArrayTypeU3D116_t2175732679  get_U24fieldU2D13_15() const { return ___U24fieldU2D13_15; }
	inline U24ArrayTypeU3D116_t2175732679 * get_address_of_U24fieldU2D13_15() { return &___U24fieldU2D13_15; }
	inline void set_U24fieldU2D13_15(U24ArrayTypeU3D116_t2175732679  value)
	{
		___U24fieldU2D13_15 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D14_16() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3EU7B99f15b47U2D93f0U2D442dU2Da950U2D84e2e79a92c8U7D_t274452176_StaticFields, ___U24fieldU2D14_16)); }
	inline U24ArrayTypeU3D120_t2175732704  get_U24fieldU2D14_16() const { return ___U24fieldU2D14_16; }
	inline U24ArrayTypeU3D120_t2175732704 * get_address_of_U24fieldU2D14_16() { return &___U24fieldU2D14_16; }
	inline void set_U24fieldU2D14_16(U24ArrayTypeU3D120_t2175732704  value)
	{
		___U24fieldU2D14_16 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D15_17() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3EU7B99f15b47U2D93f0U2D442dU2Da950U2D84e2e79a92c8U7D_t274452176_StaticFields, ___U24fieldU2D15_17)); }
	inline U24ArrayTypeU3D1152_t3023203628  get_U24fieldU2D15_17() const { return ___U24fieldU2D15_17; }
	inline U24ArrayTypeU3D1152_t3023203628 * get_address_of_U24fieldU2D15_17() { return &___U24fieldU2D15_17; }
	inline void set_U24fieldU2D15_17(U24ArrayTypeU3D1152_t3023203628  value)
	{
		___U24fieldU2D15_17 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D16_18() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3EU7B99f15b47U2D93f0U2D442dU2Da950U2D84e2e79a92c8U7D_t274452176_StaticFields, ___U24fieldU2D16_18)); }
	inline U24ArrayTypeU3D120_t2175732704  get_U24fieldU2D16_18() const { return ___U24fieldU2D16_18; }
	inline U24ArrayTypeU3D120_t2175732704 * get_address_of_U24fieldU2D16_18() { return &___U24fieldU2D16_18; }
	inline void set_U24fieldU2D16_18(U24ArrayTypeU3D120_t2175732704  value)
	{
		___U24fieldU2D16_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
