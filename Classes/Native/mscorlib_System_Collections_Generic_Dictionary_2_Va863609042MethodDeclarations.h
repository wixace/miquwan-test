﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>
struct ValueCollection_t863609042;
// System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>
struct Dictionary_2_t2163003329;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>
struct IEnumerator_1_t1904829182;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>[]
struct KeyValuePair_2U5BU5D_t204068264;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24287931429.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Val94836737.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ValueCollection__ctor_m852717223_gshared (ValueCollection_t863609042 * __this, Dictionary_2_t2163003329 * ___dictionary0, const MethodInfo* method);
#define ValueCollection__ctor_m852717223(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t863609042 *, Dictionary_2_t2163003329 *, const MethodInfo*))ValueCollection__ctor_m852717223_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m2252266667_gshared (ValueCollection_t863609042 * __this, KeyValuePair_2_t4287931429  ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m2252266667(__this, ___item0, method) ((  void (*) (ValueCollection_t863609042 *, KeyValuePair_2_t4287931429 , const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m2252266667_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::System.Collections.Generic.ICollection<TValue>.Clear()
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m962320116_gshared (ValueCollection_t863609042 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m962320116(__this, method) ((  void (*) (ValueCollection_t863609042 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m962320116_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3760656475_gshared (ValueCollection_t863609042 * __this, KeyValuePair_2_t4287931429  ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3760656475(__this, ___item0, method) ((  bool (*) (ValueCollection_t863609042 *, KeyValuePair_2_t4287931429 , const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3760656475_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m179209152_gshared (ValueCollection_t863609042 * __this, KeyValuePair_2_t4287931429  ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m179209152(__this, ___item0, method) ((  bool (*) (ValueCollection_t863609042 *, KeyValuePair_2_t4287931429 , const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m179209152_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
extern "C"  Il2CppObject* ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1370173186_gshared (ValueCollection_t863609042 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1370173186(__this, method) ((  Il2CppObject* (*) (ValueCollection_t863609042 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1370173186_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ValueCollection_System_Collections_ICollection_CopyTo_m3879294712_gshared (ValueCollection_t863609042 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_CopyTo_m3879294712(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t863609042 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m3879294712_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ValueCollection_System_Collections_IEnumerable_GetEnumerator_m435170547_gshared (ValueCollection_t863609042 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m435170547(__this, method) ((  Il2CppObject * (*) (ValueCollection_t863609042 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m435170547_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m2035832334_gshared (ValueCollection_t863609042 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m2035832334(__this, method) ((  bool (*) (ValueCollection_t863609042 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m2035832334_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ValueCollection_System_Collections_ICollection_get_IsSynchronized_m664060142_gshared (ValueCollection_t863609042 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m664060142(__this, method) ((  bool (*) (ValueCollection_t863609042 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m664060142_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ValueCollection_System_Collections_ICollection_get_SyncRoot_m1334723546_gshared (ValueCollection_t863609042 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m1334723546(__this, method) ((  Il2CppObject * (*) (ValueCollection_t863609042 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m1334723546_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::CopyTo(TValue[],System.Int32)
extern "C"  void ValueCollection_CopyTo_m515277102_gshared (ValueCollection_t863609042 * __this, KeyValuePair_2U5BU5D_t204068264* ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_CopyTo_m515277102(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t863609042 *, KeyValuePair_2U5BU5D_t204068264*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m515277102_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::GetEnumerator()
extern "C"  Enumerator_t94836737  ValueCollection_GetEnumerator_m3179477905_gshared (ValueCollection_t863609042 * __this, const MethodInfo* method);
#define ValueCollection_GetEnumerator_m3179477905(__this, method) ((  Enumerator_t94836737  (*) (ValueCollection_t863609042 *, const MethodInfo*))ValueCollection_GetEnumerator_m3179477905_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::get_Count()
extern "C"  int32_t ValueCollection_get_Count_m4201271604_gshared (ValueCollection_t863609042 * __this, const MethodInfo* method);
#define ValueCollection_get_Count_m4201271604(__this, method) ((  int32_t (*) (ValueCollection_t863609042 *, const MethodInfo*))ValueCollection_get_Count_m4201271604_gshared)(__this, method)
