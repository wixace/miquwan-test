﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Dictionary`2<Pathfinding.GraphNode,System.Int32>
struct Dictionary_2_t3142670469;
// System.Collections.Generic.List`1<Pathfinding.GraphNode>
struct List_1_t1391797922;
// System.Collections.Generic.Queue`1<Pathfinding.GraphNode>
struct Queue_1_t2259854799;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.PathUtilities/<BFS>c__AnonStorey126
struct  U3CBFSU3Ec__AnonStorey126_t1732117893  : public Il2CppObject
{
public:
	// System.Collections.Generic.Dictionary`2<Pathfinding.GraphNode,System.Int32> Pathfinding.PathUtilities/<BFS>c__AnonStorey126::map
	Dictionary_2_t3142670469 * ___map_0;
	// System.Int32 Pathfinding.PathUtilities/<BFS>c__AnonStorey126::currentDist
	int32_t ___currentDist_1;
	// System.Collections.Generic.List`1<Pathfinding.GraphNode> Pathfinding.PathUtilities/<BFS>c__AnonStorey126::result
	List_1_t1391797922 * ___result_2;
	// System.Collections.Generic.Queue`1<Pathfinding.GraphNode> Pathfinding.PathUtilities/<BFS>c__AnonStorey126::que
	Queue_1_t2259854799 * ___que_3;
	// System.Int32 Pathfinding.PathUtilities/<BFS>c__AnonStorey126::tagMask
	int32_t ___tagMask_4;

public:
	inline static int32_t get_offset_of_map_0() { return static_cast<int32_t>(offsetof(U3CBFSU3Ec__AnonStorey126_t1732117893, ___map_0)); }
	inline Dictionary_2_t3142670469 * get_map_0() const { return ___map_0; }
	inline Dictionary_2_t3142670469 ** get_address_of_map_0() { return &___map_0; }
	inline void set_map_0(Dictionary_2_t3142670469 * value)
	{
		___map_0 = value;
		Il2CppCodeGenWriteBarrier(&___map_0, value);
	}

	inline static int32_t get_offset_of_currentDist_1() { return static_cast<int32_t>(offsetof(U3CBFSU3Ec__AnonStorey126_t1732117893, ___currentDist_1)); }
	inline int32_t get_currentDist_1() const { return ___currentDist_1; }
	inline int32_t* get_address_of_currentDist_1() { return &___currentDist_1; }
	inline void set_currentDist_1(int32_t value)
	{
		___currentDist_1 = value;
	}

	inline static int32_t get_offset_of_result_2() { return static_cast<int32_t>(offsetof(U3CBFSU3Ec__AnonStorey126_t1732117893, ___result_2)); }
	inline List_1_t1391797922 * get_result_2() const { return ___result_2; }
	inline List_1_t1391797922 ** get_address_of_result_2() { return &___result_2; }
	inline void set_result_2(List_1_t1391797922 * value)
	{
		___result_2 = value;
		Il2CppCodeGenWriteBarrier(&___result_2, value);
	}

	inline static int32_t get_offset_of_que_3() { return static_cast<int32_t>(offsetof(U3CBFSU3Ec__AnonStorey126_t1732117893, ___que_3)); }
	inline Queue_1_t2259854799 * get_que_3() const { return ___que_3; }
	inline Queue_1_t2259854799 ** get_address_of_que_3() { return &___que_3; }
	inline void set_que_3(Queue_1_t2259854799 * value)
	{
		___que_3 = value;
		Il2CppCodeGenWriteBarrier(&___que_3, value);
	}

	inline static int32_t get_offset_of_tagMask_4() { return static_cast<int32_t>(offsetof(U3CBFSU3Ec__AnonStorey126_t1732117893, ___tagMask_4)); }
	inline int32_t get_tagMask_4() const { return ___tagMask_4; }
	inline int32_t* get_address_of_tagMask_4() { return &___tagMask_4; }
	inline void set_tagMask_4(int32_t value)
	{
		___tagMask_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
