﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1726398178.h"
#include "mscorlib_System_Array1146569071.h"
#include "UnityEngine_UnityEngine_ClothSkinningCoefficient2944055502.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.ClothSkinningCoefficient>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2802642898_gshared (InternalEnumerator_1_t1726398178 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m2802642898(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t1726398178 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m2802642898_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.ClothSkinningCoefficient>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1269219470_gshared (InternalEnumerator_1_t1726398178 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1269219470(__this, method) ((  void (*) (InternalEnumerator_1_t1726398178 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1269219470_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.ClothSkinningCoefficient>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4229265082_gshared (InternalEnumerator_1_t1726398178 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4229265082(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t1726398178 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4229265082_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.ClothSkinningCoefficient>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m63617065_gshared (InternalEnumerator_1_t1726398178 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m63617065(__this, method) ((  void (*) (InternalEnumerator_1_t1726398178 *, const MethodInfo*))InternalEnumerator_1_Dispose_m63617065_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.ClothSkinningCoefficient>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m500130938_gshared (InternalEnumerator_1_t1726398178 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m500130938(__this, method) ((  bool (*) (InternalEnumerator_1_t1726398178 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m500130938_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.ClothSkinningCoefficient>::get_Current()
extern "C"  ClothSkinningCoefficient_t2944055502  InternalEnumerator_1_get_Current_m131058777_gshared (InternalEnumerator_1_t1726398178 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m131058777(__this, method) ((  ClothSkinningCoefficient_t2944055502  (*) (InternalEnumerator_1_t1726398178 *, const MethodInfo*))InternalEnumerator_1_get_Current_m131058777_gshared)(__this, method)
