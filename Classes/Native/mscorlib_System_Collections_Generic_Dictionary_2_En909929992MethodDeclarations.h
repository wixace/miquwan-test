﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1190435706MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,friendNpcCfg>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m752303695(__this, ___dictionary0, method) ((  void (*) (Enumerator_t909929992 *, Dictionary_2_t3887573896 *, const MethodInfo*))Enumerator__ctor_m2377115088_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,friendNpcCfg>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1538698994(__this, method) ((  Il2CppObject * (*) (Enumerator_t909929992 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1037642267_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,friendNpcCfg>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3194953478(__this, method) ((  void (*) (Enumerator_t909929992 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2809374949_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,friendNpcCfg>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m108172623(__this, method) ((  DictionaryEntry_t1751606614  (*) (Enumerator_t909929992 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2434214620_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,friendNpcCfg>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2259643982(__this, method) ((  Il2CppObject * (*) (Enumerator_t909929992 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3735627447_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,friendNpcCfg>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3607818592(__this, method) ((  Il2CppObject * (*) (Enumerator_t909929992 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m393753481_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,friendNpcCfg>::MoveNext()
#define Enumerator_MoveNext_m3161055346(__this, method) ((  bool (*) (Enumerator_t909929992 *, const MethodInfo*))Enumerator_MoveNext_m1213995029_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,friendNpcCfg>::get_Current()
#define Enumerator_get_Current_m827622142(__this, method) ((  KeyValuePair_2_t3786354602  (*) (Enumerator_t909929992 *, const MethodInfo*))Enumerator_get_Current_m1399860359_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,friendNpcCfg>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m3944806207(__this, method) ((  int32_t (*) (Enumerator_t909929992 *, const MethodInfo*))Enumerator_get_CurrentKey_m1767398110_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,friendNpcCfg>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m4135363747(__this, method) ((  friendNpcCfg_t3890310657 * (*) (Enumerator_t909929992 *, const MethodInfo*))Enumerator_get_CurrentValue_m3384846750_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,friendNpcCfg>::Reset()
#define Enumerator_Reset_m2773733921(__this, method) ((  void (*) (Enumerator_t909929992 *, const MethodInfo*))Enumerator_Reset_m1080084514_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,friendNpcCfg>::VerifyState()
#define Enumerator_VerifyState_m4218373482(__this, method) ((  void (*) (Enumerator_t909929992 *, const MethodInfo*))Enumerator_VerifyState_m2404513451_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,friendNpcCfg>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m2152660562(__this, method) ((  void (*) (Enumerator_t909929992 *, const MethodInfo*))Enumerator_VerifyCurrent_m2789892947_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,friendNpcCfg>::Dispose()
#define Enumerator_Dispose_m907036337(__this, method) ((  void (*) (Enumerator_t909929992 *, const MethodInfo*))Enumerator_Dispose_m1102561394_gshared)(__this, method)
