﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._a374f891d5e5218c4881761c3d496bcf
struct _a374f891d5e5218c4881761c3d496bcf_t661000592;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__a374f891d5e5218c4881761c3661000592.h"

// System.Void Little._a374f891d5e5218c4881761c3d496bcf::.ctor()
extern "C"  void _a374f891d5e5218c4881761c3d496bcf__ctor_m3505378973 (_a374f891d5e5218c4881761c3d496bcf_t661000592 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._a374f891d5e5218c4881761c3d496bcf::_a374f891d5e5218c4881761c3d496bcfm2(System.Int32)
extern "C"  int32_t _a374f891d5e5218c4881761c3d496bcf__a374f891d5e5218c4881761c3d496bcfm2_m304162713 (_a374f891d5e5218c4881761c3d496bcf_t661000592 * __this, int32_t ____a374f891d5e5218c4881761c3d496bcfa0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._a374f891d5e5218c4881761c3d496bcf::_a374f891d5e5218c4881761c3d496bcfm(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _a374f891d5e5218c4881761c3d496bcf__a374f891d5e5218c4881761c3d496bcfm_m1602764605 (_a374f891d5e5218c4881761c3d496bcf_t661000592 * __this, int32_t ____a374f891d5e5218c4881761c3d496bcfa0, int32_t ____a374f891d5e5218c4881761c3d496bcf521, int32_t ____a374f891d5e5218c4881761c3d496bcfc2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._a374f891d5e5218c4881761c3d496bcf::ilo__a374f891d5e5218c4881761c3d496bcfm21(Little._a374f891d5e5218c4881761c3d496bcf,System.Int32)
extern "C"  int32_t _a374f891d5e5218c4881761c3d496bcf_ilo__a374f891d5e5218c4881761c3d496bcfm21_m2643302743 (Il2CppObject * __this /* static, unused */, _a374f891d5e5218c4881761c3d496bcf_t661000592 * ____this0, int32_t ____a374f891d5e5218c4881761c3d496bcfa1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
