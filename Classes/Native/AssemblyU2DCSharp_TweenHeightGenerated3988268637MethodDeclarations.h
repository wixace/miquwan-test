﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TweenHeightGenerated
struct TweenHeightGenerated_t3988268637;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// TweenHeight
struct TweenHeight_t529510226;
// UIWidget
struct UIWidget_t769069560;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "AssemblyU2DCSharp_TweenHeight529510226.h"
#include "AssemblyU2DCSharp_UIWidget769069560.h"

// System.Void TweenHeightGenerated::.ctor()
extern "C"  void TweenHeightGenerated__ctor_m3186167006 (TweenHeightGenerated_t3988268637 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TweenHeightGenerated::TweenHeight_TweenHeight1(JSVCall,System.Int32)
extern "C"  bool TweenHeightGenerated_TweenHeight_TweenHeight1_m4123333174 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenHeightGenerated::TweenHeight_from(JSVCall)
extern "C"  void TweenHeightGenerated_TweenHeight_from_m2917352266 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenHeightGenerated::TweenHeight_to(JSVCall)
extern "C"  void TweenHeightGenerated_TweenHeight_to_m1687100185 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenHeightGenerated::TweenHeight_updateTable(JSVCall)
extern "C"  void TweenHeightGenerated_TweenHeight_updateTable_m669052083 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenHeightGenerated::TweenHeight_cachedWidget(JSVCall)
extern "C"  void TweenHeightGenerated_TweenHeight_cachedWidget_m3602189934 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenHeightGenerated::TweenHeight_value(JSVCall)
extern "C"  void TweenHeightGenerated_TweenHeight_value_m1129005767 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TweenHeightGenerated::TweenHeight_SetEndToCurrentValue(JSVCall,System.Int32)
extern "C"  bool TweenHeightGenerated_TweenHeight_SetEndToCurrentValue_m2264261763 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TweenHeightGenerated::TweenHeight_SetStartToCurrentValue(JSVCall,System.Int32)
extern "C"  bool TweenHeightGenerated_TweenHeight_SetStartToCurrentValue_m1433079690 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TweenHeightGenerated::TweenHeight_Begin__UIWidget__Single__Int32(JSVCall,System.Int32)
extern "C"  bool TweenHeightGenerated_TweenHeight_Begin__UIWidget__Single__Int32_m3521160476 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenHeightGenerated::__Register()
extern "C"  void TweenHeightGenerated___Register_m3864251049 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 TweenHeightGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t TweenHeightGenerated_ilo_getObject1_m4264423924 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenHeightGenerated::ilo_addJSCSRel2(System.Int32,System.Object)
extern "C"  void TweenHeightGenerated_ilo_addJSCSRel2_m498856219 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenHeightGenerated::ilo_setInt323(System.Int32,System.Int32)
extern "C"  void TweenHeightGenerated_ilo_setInt323_m4282788214 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenHeightGenerated::ilo_setBooleanS4(System.Int32,System.Boolean)
extern "C"  void TweenHeightGenerated_ilo_setBooleanS4_m3018578860 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 TweenHeightGenerated::ilo_setObject5(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t TweenHeightGenerated_ilo_setObject5_m348083824 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenHeightGenerated::ilo_SetEndToCurrentValue6(TweenHeight)
extern "C"  void TweenHeightGenerated_ilo_SetEndToCurrentValue6_m1948650989 (Il2CppObject * __this /* static, unused */, TweenHeight_t529510226 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TweenHeight TweenHeightGenerated::ilo_Begin7(UIWidget,System.Single,System.Int32)
extern "C"  TweenHeight_t529510226 * TweenHeightGenerated_ilo_Begin7_m754732674 (Il2CppObject * __this /* static, unused */, UIWidget_t769069560 * ___widget0, float ___duration1, int32_t ___height2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
