﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_RectOffsetGenerated
struct UnityEngine_RectOffsetGenerated_t1099108688;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"

// System.Void UnityEngine_RectOffsetGenerated::.ctor()
extern "C"  void UnityEngine_RectOffsetGenerated__ctor_m4012924955 (UnityEngine_RectOffsetGenerated_t1099108688 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RectOffsetGenerated::RectOffset_RectOffset1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RectOffsetGenerated_RectOffset_RectOffset1_m1727573719 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RectOffsetGenerated::RectOffset_RectOffset2(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RectOffsetGenerated_RectOffset_RectOffset2_m2972338200 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RectOffsetGenerated::RectOffset_left(JSVCall)
extern "C"  void UnityEngine_RectOffsetGenerated_RectOffset_left_m1979730303 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RectOffsetGenerated::RectOffset_right(JSVCall)
extern "C"  void UnityEngine_RectOffsetGenerated_RectOffset_right_m169536202 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RectOffsetGenerated::RectOffset_top(JSVCall)
extern "C"  void UnityEngine_RectOffsetGenerated_RectOffset_top_m602960817 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RectOffsetGenerated::RectOffset_bottom(JSVCall)
extern "C"  void UnityEngine_RectOffsetGenerated_RectOffset_bottom_m1998188507 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RectOffsetGenerated::RectOffset_horizontal(JSVCall)
extern "C"  void UnityEngine_RectOffsetGenerated_RectOffset_horizontal_m3977582082 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RectOffsetGenerated::RectOffset_vertical(JSVCall)
extern "C"  void UnityEngine_RectOffsetGenerated_RectOffset_vertical_m170876656 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RectOffsetGenerated::RectOffset_Add__Rect(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RectOffsetGenerated_RectOffset_Add__Rect_m3062725090 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RectOffsetGenerated::RectOffset_Remove__Rect(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RectOffsetGenerated_RectOffset_Remove__Rect_m126040621 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RectOffsetGenerated::RectOffset_ToString(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RectOffsetGenerated_RectOffset_ToString_m3585227825 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RectOffsetGenerated::__Register()
extern "C"  void UnityEngine_RectOffsetGenerated___Register_m1941960076 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RectOffsetGenerated::ilo_addJSCSRel1(System.Int32,System.Object)
extern "C"  void UnityEngine_RectOffsetGenerated_ilo_addJSCSRel1_m1724073303 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_RectOffsetGenerated::ilo_getObject2(System.Int32)
extern "C"  int32_t UnityEngine_RectOffsetGenerated_ilo_getObject2_m1657362236 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RectOffsetGenerated::ilo_attachFinalizerObject3(System.Int32)
extern "C"  bool UnityEngine_RectOffsetGenerated_ilo_attachFinalizerObject3_m423817662 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_RectOffsetGenerated::ilo_getInt324(System.Int32)
extern "C"  int32_t UnityEngine_RectOffsetGenerated_ilo_getInt324_m772232193 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RectOffsetGenerated::ilo_setInt325(System.Int32,System.Int32)
extern "C"  void UnityEngine_RectOffsetGenerated_ilo_setInt325_m296288407 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_RectOffsetGenerated::ilo_getObject6(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_RectOffsetGenerated_ilo_getObject6_m1058590575 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
