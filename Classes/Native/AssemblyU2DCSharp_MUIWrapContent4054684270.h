﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// MUIWrapContent/OnInitializeItem
struct OnInitializeItem_t4000839715;
// UnityEngine.Transform
struct Transform_t1659122786;
// UIPanel
struct UIPanel_t295209936;
// UIScrollView
struct UIScrollView_t2113479878;
// System.Collections.Generic.List`1<UnityEngine.Transform>
struct List_1_t3027308338;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MUIWrapContent
struct  MUIWrapContent_t4054684270  : public MonoBehaviour_t667441552
{
public:
	// System.Int32 MUIWrapContent::cellWidth
	int32_t ___cellWidth_2;
	// System.Int32 MUIWrapContent::cellHeight
	int32_t ___cellHeight_3;
	// System.Int32 MUIWrapContent::itemSize
	int32_t ___itemSize_4;
	// System.Boolean MUIWrapContent::cullContent
	bool ___cullContent_5;
	// System.Int32 MUIWrapContent::minIndex
	int32_t ___minIndex_6;
	// System.Single MUIWrapContent::LieShu
	float ___LieShu_7;
	// System.Int32 MUIWrapContent::maxIndex
	int32_t ___maxIndex_8;
	// MUIWrapContent/OnInitializeItem MUIWrapContent::onInitializeItem
	OnInitializeItem_t4000839715 * ___onInitializeItem_9;
	// UnityEngine.Transform MUIWrapContent::mTrans
	Transform_t1659122786 * ___mTrans_10;
	// UIPanel MUIWrapContent::mPanel
	UIPanel_t295209936 * ___mPanel_11;
	// UIScrollView MUIWrapContent::mScroll
	UIScrollView_t2113479878 * ___mScroll_12;
	// System.Boolean MUIWrapContent::mHorizontal
	bool ___mHorizontal_13;
	// System.Boolean MUIWrapContent::mFirstTime
	bool ___mFirstTime_14;
	// System.Collections.Generic.List`1<UnityEngine.Transform> MUIWrapContent::mChildren
	List_1_t3027308338 * ___mChildren_15;
	// System.Boolean MUIWrapContent::isStart
	bool ___isStart_16;

public:
	inline static int32_t get_offset_of_cellWidth_2() { return static_cast<int32_t>(offsetof(MUIWrapContent_t4054684270, ___cellWidth_2)); }
	inline int32_t get_cellWidth_2() const { return ___cellWidth_2; }
	inline int32_t* get_address_of_cellWidth_2() { return &___cellWidth_2; }
	inline void set_cellWidth_2(int32_t value)
	{
		___cellWidth_2 = value;
	}

	inline static int32_t get_offset_of_cellHeight_3() { return static_cast<int32_t>(offsetof(MUIWrapContent_t4054684270, ___cellHeight_3)); }
	inline int32_t get_cellHeight_3() const { return ___cellHeight_3; }
	inline int32_t* get_address_of_cellHeight_3() { return &___cellHeight_3; }
	inline void set_cellHeight_3(int32_t value)
	{
		___cellHeight_3 = value;
	}

	inline static int32_t get_offset_of_itemSize_4() { return static_cast<int32_t>(offsetof(MUIWrapContent_t4054684270, ___itemSize_4)); }
	inline int32_t get_itemSize_4() const { return ___itemSize_4; }
	inline int32_t* get_address_of_itemSize_4() { return &___itemSize_4; }
	inline void set_itemSize_4(int32_t value)
	{
		___itemSize_4 = value;
	}

	inline static int32_t get_offset_of_cullContent_5() { return static_cast<int32_t>(offsetof(MUIWrapContent_t4054684270, ___cullContent_5)); }
	inline bool get_cullContent_5() const { return ___cullContent_5; }
	inline bool* get_address_of_cullContent_5() { return &___cullContent_5; }
	inline void set_cullContent_5(bool value)
	{
		___cullContent_5 = value;
	}

	inline static int32_t get_offset_of_minIndex_6() { return static_cast<int32_t>(offsetof(MUIWrapContent_t4054684270, ___minIndex_6)); }
	inline int32_t get_minIndex_6() const { return ___minIndex_6; }
	inline int32_t* get_address_of_minIndex_6() { return &___minIndex_6; }
	inline void set_minIndex_6(int32_t value)
	{
		___minIndex_6 = value;
	}

	inline static int32_t get_offset_of_LieShu_7() { return static_cast<int32_t>(offsetof(MUIWrapContent_t4054684270, ___LieShu_7)); }
	inline float get_LieShu_7() const { return ___LieShu_7; }
	inline float* get_address_of_LieShu_7() { return &___LieShu_7; }
	inline void set_LieShu_7(float value)
	{
		___LieShu_7 = value;
	}

	inline static int32_t get_offset_of_maxIndex_8() { return static_cast<int32_t>(offsetof(MUIWrapContent_t4054684270, ___maxIndex_8)); }
	inline int32_t get_maxIndex_8() const { return ___maxIndex_8; }
	inline int32_t* get_address_of_maxIndex_8() { return &___maxIndex_8; }
	inline void set_maxIndex_8(int32_t value)
	{
		___maxIndex_8 = value;
	}

	inline static int32_t get_offset_of_onInitializeItem_9() { return static_cast<int32_t>(offsetof(MUIWrapContent_t4054684270, ___onInitializeItem_9)); }
	inline OnInitializeItem_t4000839715 * get_onInitializeItem_9() const { return ___onInitializeItem_9; }
	inline OnInitializeItem_t4000839715 ** get_address_of_onInitializeItem_9() { return &___onInitializeItem_9; }
	inline void set_onInitializeItem_9(OnInitializeItem_t4000839715 * value)
	{
		___onInitializeItem_9 = value;
		Il2CppCodeGenWriteBarrier(&___onInitializeItem_9, value);
	}

	inline static int32_t get_offset_of_mTrans_10() { return static_cast<int32_t>(offsetof(MUIWrapContent_t4054684270, ___mTrans_10)); }
	inline Transform_t1659122786 * get_mTrans_10() const { return ___mTrans_10; }
	inline Transform_t1659122786 ** get_address_of_mTrans_10() { return &___mTrans_10; }
	inline void set_mTrans_10(Transform_t1659122786 * value)
	{
		___mTrans_10 = value;
		Il2CppCodeGenWriteBarrier(&___mTrans_10, value);
	}

	inline static int32_t get_offset_of_mPanel_11() { return static_cast<int32_t>(offsetof(MUIWrapContent_t4054684270, ___mPanel_11)); }
	inline UIPanel_t295209936 * get_mPanel_11() const { return ___mPanel_11; }
	inline UIPanel_t295209936 ** get_address_of_mPanel_11() { return &___mPanel_11; }
	inline void set_mPanel_11(UIPanel_t295209936 * value)
	{
		___mPanel_11 = value;
		Il2CppCodeGenWriteBarrier(&___mPanel_11, value);
	}

	inline static int32_t get_offset_of_mScroll_12() { return static_cast<int32_t>(offsetof(MUIWrapContent_t4054684270, ___mScroll_12)); }
	inline UIScrollView_t2113479878 * get_mScroll_12() const { return ___mScroll_12; }
	inline UIScrollView_t2113479878 ** get_address_of_mScroll_12() { return &___mScroll_12; }
	inline void set_mScroll_12(UIScrollView_t2113479878 * value)
	{
		___mScroll_12 = value;
		Il2CppCodeGenWriteBarrier(&___mScroll_12, value);
	}

	inline static int32_t get_offset_of_mHorizontal_13() { return static_cast<int32_t>(offsetof(MUIWrapContent_t4054684270, ___mHorizontal_13)); }
	inline bool get_mHorizontal_13() const { return ___mHorizontal_13; }
	inline bool* get_address_of_mHorizontal_13() { return &___mHorizontal_13; }
	inline void set_mHorizontal_13(bool value)
	{
		___mHorizontal_13 = value;
	}

	inline static int32_t get_offset_of_mFirstTime_14() { return static_cast<int32_t>(offsetof(MUIWrapContent_t4054684270, ___mFirstTime_14)); }
	inline bool get_mFirstTime_14() const { return ___mFirstTime_14; }
	inline bool* get_address_of_mFirstTime_14() { return &___mFirstTime_14; }
	inline void set_mFirstTime_14(bool value)
	{
		___mFirstTime_14 = value;
	}

	inline static int32_t get_offset_of_mChildren_15() { return static_cast<int32_t>(offsetof(MUIWrapContent_t4054684270, ___mChildren_15)); }
	inline List_1_t3027308338 * get_mChildren_15() const { return ___mChildren_15; }
	inline List_1_t3027308338 ** get_address_of_mChildren_15() { return &___mChildren_15; }
	inline void set_mChildren_15(List_1_t3027308338 * value)
	{
		___mChildren_15 = value;
		Il2CppCodeGenWriteBarrier(&___mChildren_15, value);
	}

	inline static int32_t get_offset_of_isStart_16() { return static_cast<int32_t>(offsetof(MUIWrapContent_t4054684270, ___isStart_16)); }
	inline bool get_isStart_16() const { return ___isStart_16; }
	inline bool* get_address_of_isStart_16() { return &___isStart_16; }
	inline void set_isStart_16(bool value)
	{
		___isStart_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
