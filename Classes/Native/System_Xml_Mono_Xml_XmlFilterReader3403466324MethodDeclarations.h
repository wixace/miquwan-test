﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Xml.XmlFilterReader
struct XmlFilterReader_t3403466324;
// System.Xml.XmlReader
struct XmlReader_t4123196108;
// System.Xml.XmlReaderSettings
struct XmlReaderSettings_t4229224207;
// System.String
struct String_t;
// System.Xml.XmlNameTable
struct XmlNameTable_t1216706026;

#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_XmlReader4123196108.h"
#include "System_Xml_System_Xml_XmlReaderSettings4229224207.h"
#include "System_Xml_System_Xml_XmlNodeType992114213.h"
#include "System_Xml_System_Xml_XmlSpace557686381.h"
#include "mscorlib_System_String7231557.h"
#include "System_Xml_System_Xml_ReadState352099245.h"

// System.Void Mono.Xml.XmlFilterReader::.ctor(System.Xml.XmlReader,System.Xml.XmlReaderSettings)
extern "C"  void XmlFilterReader__ctor_m1270422638 (XmlFilterReader_t3403466324 * __this, XmlReader_t4123196108 * ___reader0, XmlReaderSettings_t4229224207 * ___settings1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Xml.XmlFilterReader::get_LineNumber()
extern "C"  int32_t XmlFilterReader_get_LineNumber_m1072744343 (XmlFilterReader_t3403466324 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Xml.XmlFilterReader::get_LinePosition()
extern "C"  int32_t XmlFilterReader_get_LinePosition_m1889754679 (XmlFilterReader_t3403466324 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlNodeType Mono.Xml.XmlFilterReader::get_NodeType()
extern "C"  int32_t XmlFilterReader_get_NodeType_m2738114760 (XmlFilterReader_t3403466324 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Xml.XmlFilterReader::get_Name()
extern "C"  String_t* XmlFilterReader_get_Name_m3628346694 (XmlFilterReader_t3403466324 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Xml.XmlFilterReader::get_LocalName()
extern "C"  String_t* XmlFilterReader_get_LocalName_m228716509 (XmlFilterReader_t3403466324 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Xml.XmlFilterReader::get_NamespaceURI()
extern "C"  String_t* XmlFilterReader_get_NamespaceURI_m428147020 (XmlFilterReader_t3403466324 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Xml.XmlFilterReader::get_Prefix()
extern "C"  String_t* XmlFilterReader_get_Prefix_m494946573 (XmlFilterReader_t3403466324 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Xml.XmlFilterReader::get_HasValue()
extern "C"  bool XmlFilterReader_get_HasValue_m400180759 (XmlFilterReader_t3403466324 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Xml.XmlFilterReader::get_Depth()
extern "C"  int32_t XmlFilterReader_get_Depth_m934292971 (XmlFilterReader_t3403466324 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Xml.XmlFilterReader::get_Value()
extern "C"  String_t* XmlFilterReader_get_Value_m3614271736 (XmlFilterReader_t3403466324 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Xml.XmlFilterReader::get_BaseURI()
extern "C"  String_t* XmlFilterReader_get_BaseURI_m1921945698 (XmlFilterReader_t3403466324 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Xml.XmlFilterReader::get_IsEmptyElement()
extern "C"  bool XmlFilterReader_get_IsEmptyElement_m752287385 (XmlFilterReader_t3403466324 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Xml.XmlFilterReader::get_IsDefault()
extern "C"  bool XmlFilterReader_get_IsDefault_m2027718905 (XmlFilterReader_t3403466324 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Xml.XmlFilterReader::get_XmlLang()
extern "C"  String_t* XmlFilterReader_get_XmlLang_m1925165996 (XmlFilterReader_t3403466324 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlSpace Mono.Xml.XmlFilterReader::get_XmlSpace()
extern "C"  int32_t XmlFilterReader_get_XmlSpace_m870266581 (XmlFilterReader_t3403466324 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Xml.XmlFilterReader::get_AttributeCount()
extern "C"  int32_t XmlFilterReader_get_AttributeCount_m4037846285 (XmlFilterReader_t3403466324 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Xml.XmlFilterReader::get_Item(System.String)
extern "C"  String_t* XmlFilterReader_get_Item_m1702072820 (XmlFilterReader_t3403466324 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Xml.XmlFilterReader::get_Item(System.String,System.String)
extern "C"  String_t* XmlFilterReader_get_Item_m1560035120 (XmlFilterReader_t3403466324 * __this, String_t* ___localName0, String_t* ___namespaceURI1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Xml.XmlFilterReader::get_EOF()
extern "C"  bool XmlFilterReader_get_EOF_m1832028094 (XmlFilterReader_t3403466324 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.ReadState Mono.Xml.XmlFilterReader::get_ReadState()
extern "C"  int32_t XmlFilterReader_get_ReadState_m1746545049 (XmlFilterReader_t3403466324 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlNameTable Mono.Xml.XmlFilterReader::get_NameTable()
extern "C"  XmlNameTable_t1216706026 * XmlFilterReader_get_NameTable_m660299324 (XmlFilterReader_t3403466324 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlReaderSettings Mono.Xml.XmlFilterReader::get_Settings()
extern "C"  XmlReaderSettings_t4229224207 * XmlFilterReader_get_Settings_m3071860197 (XmlFilterReader_t3403466324 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Xml.XmlFilterReader::GetAttribute(System.String)
extern "C"  String_t* XmlFilterReader_GetAttribute_m1408900010 (XmlFilterReader_t3403466324 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Xml.XmlFilterReader::GetAttribute(System.String,System.String)
extern "C"  String_t* XmlFilterReader_GetAttribute_m3067908710 (XmlFilterReader_t3403466324 * __this, String_t* ___localName0, String_t* ___namespaceURI1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Xml.XmlFilterReader::HasLineInfo()
extern "C"  bool XmlFilterReader_HasLineInfo_m1478590311 (XmlFilterReader_t3403466324 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Xml.XmlFilterReader::MoveToAttribute(System.String)
extern "C"  bool XmlFilterReader_MoveToAttribute_m898881319 (XmlFilterReader_t3403466324 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Xml.XmlFilterReader::MoveToAttribute(System.String,System.String)
extern "C"  bool XmlFilterReader_MoveToAttribute_m2138435363 (XmlFilterReader_t3403466324 * __this, String_t* ___localName0, String_t* ___namespaceURI1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Xml.XmlFilterReader::MoveToAttribute(System.Int32)
extern "C"  void XmlFilterReader_MoveToAttribute_m2159458196 (XmlFilterReader_t3403466324 * __this, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Xml.XmlFilterReader::MoveToFirstAttribute()
extern "C"  bool XmlFilterReader_MoveToFirstAttribute_m3870002031 (XmlFilterReader_t3403466324 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Xml.XmlFilterReader::MoveToNextAttribute()
extern "C"  bool XmlFilterReader_MoveToNextAttribute_m1076594632 (XmlFilterReader_t3403466324 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Xml.XmlFilterReader::MoveToElement()
extern "C"  bool XmlFilterReader_MoveToElement_m356219803 (XmlFilterReader_t3403466324 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Xml.XmlFilterReader::Close()
extern "C"  void XmlFilterReader_Close_m3828876139 (XmlFilterReader_t3403466324 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Xml.XmlFilterReader::Read()
extern "C"  bool XmlFilterReader_Read_m2942273133 (XmlFilterReader_t3403466324 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Xml.XmlFilterReader::LookupNamespace(System.String)
extern "C"  String_t* XmlFilterReader_LookupNamespace_m984563057 (XmlFilterReader_t3403466324 * __this, String_t* ___prefix0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Xml.XmlFilterReader::ResolveEntity()
extern "C"  void XmlFilterReader_ResolveEntity_m1802101410 (XmlFilterReader_t3403466324 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Xml.XmlFilterReader::ReadAttributeValue()
extern "C"  bool XmlFilterReader_ReadAttributeValue_m1264704642 (XmlFilterReader_t3403466324 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
