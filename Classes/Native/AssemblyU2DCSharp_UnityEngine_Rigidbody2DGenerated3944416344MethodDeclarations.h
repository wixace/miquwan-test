﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_Rigidbody2DGenerated
struct UnityEngine_Rigidbody2DGenerated_t3944416344;
// JSVCall
struct JSVCall_t3708497963;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"

// System.Void UnityEngine_Rigidbody2DGenerated::.ctor()
extern "C"  void UnityEngine_Rigidbody2DGenerated__ctor_m3180221763 (UnityEngine_Rigidbody2DGenerated_t3944416344 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Rigidbody2DGenerated::Rigidbody2D_Rigidbody2D1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Rigidbody2DGenerated_Rigidbody2D_Rigidbody2D1_m267583127 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Rigidbody2DGenerated::Rigidbody2D_position(JSVCall)
extern "C"  void UnityEngine_Rigidbody2DGenerated_Rigidbody2D_position_m2780873981 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Rigidbody2DGenerated::Rigidbody2D_rotation(JSVCall)
extern "C"  void UnityEngine_Rigidbody2DGenerated_Rigidbody2D_rotation_m2449579592 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Rigidbody2DGenerated::Rigidbody2D_velocity(JSVCall)
extern "C"  void UnityEngine_Rigidbody2DGenerated_Rigidbody2D_velocity_m556272585 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Rigidbody2DGenerated::Rigidbody2D_angularVelocity(JSVCall)
extern "C"  void UnityEngine_Rigidbody2DGenerated_Rigidbody2D_angularVelocity_m2552760583 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Rigidbody2DGenerated::Rigidbody2D_useAutoMass(JSVCall)
extern "C"  void UnityEngine_Rigidbody2DGenerated_Rigidbody2D_useAutoMass_m2082821724 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Rigidbody2DGenerated::Rigidbody2D_mass(JSVCall)
extern "C"  void UnityEngine_Rigidbody2DGenerated_Rigidbody2D_mass_m1684335506 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Rigidbody2DGenerated::Rigidbody2D_centerOfMass(JSVCall)
extern "C"  void UnityEngine_Rigidbody2DGenerated_Rigidbody2D_centerOfMass_m1900550630 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Rigidbody2DGenerated::Rigidbody2D_worldCenterOfMass(JSVCall)
extern "C"  void UnityEngine_Rigidbody2DGenerated_Rigidbody2D_worldCenterOfMass_m1467074676 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Rigidbody2DGenerated::Rigidbody2D_inertia(JSVCall)
extern "C"  void UnityEngine_Rigidbody2DGenerated_Rigidbody2D_inertia_m2423333260 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Rigidbody2DGenerated::Rigidbody2D_drag(JSVCall)
extern "C"  void UnityEngine_Rigidbody2DGenerated_Rigidbody2D_drag_m2567949650 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Rigidbody2DGenerated::Rigidbody2D_angularDrag(JSVCall)
extern "C"  void UnityEngine_Rigidbody2DGenerated_Rigidbody2D_angularDrag_m2343311760 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Rigidbody2DGenerated::Rigidbody2D_gravityScale(JSVCall)
extern "C"  void UnityEngine_Rigidbody2DGenerated_Rigidbody2D_gravityScale_m2410858474 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Rigidbody2DGenerated::Rigidbody2D_isKinematic(JSVCall)
extern "C"  void UnityEngine_Rigidbody2DGenerated_Rigidbody2D_isKinematic_m815456107 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Rigidbody2DGenerated::Rigidbody2D_freezeRotation(JSVCall)
extern "C"  void UnityEngine_Rigidbody2DGenerated_Rigidbody2D_freezeRotation_m501834865 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Rigidbody2DGenerated::Rigidbody2D_constraints(JSVCall)
extern "C"  void UnityEngine_Rigidbody2DGenerated_Rigidbody2D_constraints_m3325960592 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Rigidbody2DGenerated::Rigidbody2D_simulated(JSVCall)
extern "C"  void UnityEngine_Rigidbody2DGenerated_Rigidbody2D_simulated_m966418502 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Rigidbody2DGenerated::Rigidbody2D_interpolation(JSVCall)
extern "C"  void UnityEngine_Rigidbody2DGenerated_Rigidbody2D_interpolation_m86878690 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Rigidbody2DGenerated::Rigidbody2D_sleepMode(JSVCall)
extern "C"  void UnityEngine_Rigidbody2DGenerated_Rigidbody2D_sleepMode_m2675543852 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Rigidbody2DGenerated::Rigidbody2D_collisionDetectionMode(JSVCall)
extern "C"  void UnityEngine_Rigidbody2DGenerated_Rigidbody2D_collisionDetectionMode_m2447357200 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Rigidbody2DGenerated::Rigidbody2D_AddForce__Vector2__ForceMode2D(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Rigidbody2DGenerated_Rigidbody2D_AddForce__Vector2__ForceMode2D_m587382912 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Rigidbody2DGenerated::Rigidbody2D_AddForce__Vector2(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Rigidbody2DGenerated_Rigidbody2D_AddForce__Vector2_m1196586818 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Rigidbody2DGenerated::Rigidbody2D_AddForceAtPosition__Vector2__Vector2__ForceMode2D(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Rigidbody2DGenerated_Rigidbody2D_AddForceAtPosition__Vector2__Vector2__ForceMode2D_m2827373879 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Rigidbody2DGenerated::Rigidbody2D_AddForceAtPosition__Vector2__Vector2(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Rigidbody2DGenerated_Rigidbody2D_AddForceAtPosition__Vector2__Vector2_m4212493611 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Rigidbody2DGenerated::Rigidbody2D_AddRelativeForce__Vector2__ForceMode2D(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Rigidbody2DGenerated_Rigidbody2D_AddRelativeForce__Vector2__ForceMode2D_m976012308 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Rigidbody2DGenerated::Rigidbody2D_AddRelativeForce__Vector2(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Rigidbody2DGenerated_Rigidbody2D_AddRelativeForce__Vector2_m3183883054 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Rigidbody2DGenerated::Rigidbody2D_AddTorque__Single__ForceMode2D(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Rigidbody2DGenerated_Rigidbody2D_AddTorque__Single__ForceMode2D_m3635912242 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Rigidbody2DGenerated::Rigidbody2D_AddTorque__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Rigidbody2DGenerated_Rigidbody2D_AddTorque__Single_m357384272 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Rigidbody2DGenerated::Rigidbody2D_GetPoint__Vector2(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Rigidbody2DGenerated_Rigidbody2D_GetPoint__Vector2_m1620785202 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Rigidbody2DGenerated::Rigidbody2D_GetPointVelocity__Vector2(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Rigidbody2DGenerated_Rigidbody2D_GetPointVelocity__Vector2_m734561685 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Rigidbody2DGenerated::Rigidbody2D_GetRelativePoint__Vector2(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Rigidbody2DGenerated_Rigidbody2D_GetRelativePoint__Vector2_m2845437726 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Rigidbody2DGenerated::Rigidbody2D_GetRelativePointVelocity__Vector2(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Rigidbody2DGenerated_Rigidbody2D_GetRelativePointVelocity__Vector2_m3972501633 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Rigidbody2DGenerated::Rigidbody2D_GetRelativeVector__Vector2(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Rigidbody2DGenerated_Rigidbody2D_GetRelativeVector__Vector2_m2580801455 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Rigidbody2DGenerated::Rigidbody2D_GetVector__Vector2(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Rigidbody2DGenerated_Rigidbody2D_GetVector__Vector2_m3271278875 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Rigidbody2DGenerated::Rigidbody2D_IsAwake(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Rigidbody2DGenerated_Rigidbody2D_IsAwake_m1206369400 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Rigidbody2DGenerated::Rigidbody2D_IsSleeping(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Rigidbody2DGenerated_Rigidbody2D_IsSleeping_m3343422874 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Rigidbody2DGenerated::Rigidbody2D_IsTouching__Collider2D(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Rigidbody2DGenerated_Rigidbody2D_IsTouching__Collider2D_m3916507512 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Rigidbody2DGenerated::Rigidbody2D_IsTouchingLayers__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Rigidbody2DGenerated_Rigidbody2D_IsTouchingLayers__Int32_m3783788412 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Rigidbody2DGenerated::Rigidbody2D_IsTouchingLayers(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Rigidbody2DGenerated_Rigidbody2D_IsTouchingLayers_m1105934676 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Rigidbody2DGenerated::Rigidbody2D_MovePosition__Vector2(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Rigidbody2DGenerated_Rigidbody2D_MovePosition__Vector2_m3254377970 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Rigidbody2DGenerated::Rigidbody2D_MoveRotation__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Rigidbody2DGenerated_Rigidbody2D_MoveRotation__Single_m3985839580 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Rigidbody2DGenerated::Rigidbody2D_Sleep(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Rigidbody2DGenerated_Rigidbody2D_Sleep_m3156849364 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Rigidbody2DGenerated::Rigidbody2D_WakeUp(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Rigidbody2DGenerated_Rigidbody2D_WakeUp_m3535932036 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Rigidbody2DGenerated::__Register()
extern "C"  void UnityEngine_Rigidbody2DGenerated___Register_m1863645540 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_Rigidbody2DGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UnityEngine_Rigidbody2DGenerated_ilo_getObject1_m3225609647 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine_Rigidbody2DGenerated::ilo_getVector2S2(System.Int32)
extern "C"  Vector2_t4282066565  UnityEngine_Rigidbody2DGenerated_ilo_getVector2S2_m2177934904 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine_Rigidbody2DGenerated::ilo_getSingle3(System.Int32)
extern "C"  float UnityEngine_Rigidbody2DGenerated_ilo_getSingle3_m2235770950 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Rigidbody2DGenerated::ilo_setVector2S4(System.Int32,UnityEngine.Vector2)
extern "C"  void UnityEngine_Rigidbody2DGenerated_ilo_setVector2S4_m3816063175 (Il2CppObject * __this /* static, unused */, int32_t ___e0, Vector2_t4282066565  ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Rigidbody2DGenerated::ilo_setSingle5(System.Int32,System.Single)
extern "C"  void UnityEngine_Rigidbody2DGenerated_ilo_setSingle5_m3084645973 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Rigidbody2DGenerated::ilo_getBooleanS6(System.Int32)
extern "C"  bool UnityEngine_Rigidbody2DGenerated_ilo_getBooleanS6_m3022082926 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Rigidbody2DGenerated::ilo_setEnum7(System.Int32,System.Int32)
extern "C"  void UnityEngine_Rigidbody2DGenerated_ilo_setEnum7_m96256798 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_Rigidbody2DGenerated::ilo_getEnum8(System.Int32)
extern "C"  int32_t UnityEngine_Rigidbody2DGenerated_ilo_getEnum8_m499926580 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Rigidbody2DGenerated::ilo_setBooleanS9(System.Int32,System.Boolean)
extern "C"  void UnityEngine_Rigidbody2DGenerated_ilo_setBooleanS9_m1713113708 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
