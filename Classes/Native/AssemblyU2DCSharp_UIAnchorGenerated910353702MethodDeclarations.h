﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIAnchorGenerated
struct UIAnchorGenerated_t910353702;
// JSVCall
struct JSVCall_t3708497963;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"

// System.Void UIAnchorGenerated::.ctor()
extern "C"  void UIAnchorGenerated__ctor_m1089024773 (UIAnchorGenerated_t910353702 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIAnchorGenerated::UIAnchor_UIAnchor1(JSVCall,System.Int32)
extern "C"  bool UIAnchorGenerated_UIAnchor_UIAnchor1_m441998829 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIAnchorGenerated::UIAnchor_uiCamera(JSVCall)
extern "C"  void UIAnchorGenerated_UIAnchor_uiCamera_m4281776117 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIAnchorGenerated::UIAnchor_container(JSVCall)
extern "C"  void UIAnchorGenerated_UIAnchor_container_m2782700029 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIAnchorGenerated::UIAnchor_side(JSVCall)
extern "C"  void UIAnchorGenerated_UIAnchor_side_m1507302199 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIAnchorGenerated::UIAnchor_runOnlyOnce(JSVCall)
extern "C"  void UIAnchorGenerated_UIAnchor_runOnlyOnce_m2103570022 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIAnchorGenerated::UIAnchor_relativeOffset(JSVCall)
extern "C"  void UIAnchorGenerated_UIAnchor_relativeOffset_m4022694159 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIAnchorGenerated::UIAnchor_pixelOffset(JSVCall)
extern "C"  void UIAnchorGenerated_UIAnchor_pixelOffset_m5091525 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIAnchorGenerated::__Register()
extern "C"  void UIAnchorGenerated___Register_m1577608418 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIAnchorGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UIAnchorGenerated_ilo_getObject1_m1721774033 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIAnchorGenerated::ilo_setObject2(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UIAnchorGenerated_ilo_setObject2_m3754716554 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UIAnchorGenerated::ilo_getObject3(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UIAnchorGenerated_ilo_getObject3_m2722229378 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIAnchorGenerated::ilo_setEnum4(System.Int32,System.Int32)
extern "C"  void UIAnchorGenerated_ilo_setEnum4_m1768751811 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIAnchorGenerated::ilo_setBooleanS5(System.Int32,System.Boolean)
extern "C"  void UIAnchorGenerated_ilo_setBooleanS5_m3144800306 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIAnchorGenerated::ilo_getBooleanS6(System.Int32)
extern "C"  bool UIAnchorGenerated_ilo_getBooleanS6_m2169662596 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIAnchorGenerated::ilo_setVector2S7(System.Int32,UnityEngine.Vector2)
extern "C"  void UIAnchorGenerated_ilo_setVector2S7_m2390990088 (Il2CppObject * __this /* static, unused */, int32_t ___e0, Vector2_t4282066565  ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UIAnchorGenerated::ilo_getVector2S8(System.Int32)
extern "C"  Vector2_t4282066565  UIAnchorGenerated_ilo_getVector2S8_m2497119846 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
