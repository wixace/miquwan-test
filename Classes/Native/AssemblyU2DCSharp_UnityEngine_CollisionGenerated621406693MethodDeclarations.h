﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_CollisionGenerated
struct UnityEngine_CollisionGenerated_t621406693;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"

// System.Void UnityEngine_CollisionGenerated::.ctor()
extern "C"  void UnityEngine_CollisionGenerated__ctor_m834208854 (UnityEngine_CollisionGenerated_t621406693 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_CollisionGenerated::Collision_Collision1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_CollisionGenerated_Collision_Collision1_m816532222 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CollisionGenerated::Collision_relativeVelocity(JSVCall)
extern "C"  void UnityEngine_CollisionGenerated_Collision_relativeVelocity_m265912067 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CollisionGenerated::Collision_rigidbody(JSVCall)
extern "C"  void UnityEngine_CollisionGenerated_Collision_rigidbody_m3834672499 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CollisionGenerated::Collision_collider(JSVCall)
extern "C"  void UnityEngine_CollisionGenerated_Collision_collider_m2686549304 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CollisionGenerated::Collision_transform(JSVCall)
extern "C"  void UnityEngine_CollisionGenerated_Collision_transform_m1655065396 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CollisionGenerated::Collision_gameObject(JSVCall)
extern "C"  void UnityEngine_CollisionGenerated_Collision_gameObject_m4070968955 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CollisionGenerated::Collision_contacts(JSVCall)
extern "C"  void UnityEngine_CollisionGenerated_Collision_contacts_m2494345145 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CollisionGenerated::Collision_impulse(JSVCall)
extern "C"  void UnityEngine_CollisionGenerated_Collision_impulse_m2296240715 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_CollisionGenerated::Collision_GetEnumerator(JSVCall,System.Int32)
extern "C"  bool UnityEngine_CollisionGenerated_Collision_GetEnumerator_m633599645 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CollisionGenerated::__Register()
extern "C"  void UnityEngine_CollisionGenerated___Register_m3316366897 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_CollisionGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UnityEngine_CollisionGenerated_ilo_getObject1_m2515434492 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CollisionGenerated::ilo_addJSCSRel2(System.Int32,System.Object)
extern "C"  void UnityEngine_CollisionGenerated_ilo_addJSCSRel2_m969378963 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_CollisionGenerated::ilo_setObject3(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UnityEngine_CollisionGenerated_ilo_setObject3_m4221662198 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
