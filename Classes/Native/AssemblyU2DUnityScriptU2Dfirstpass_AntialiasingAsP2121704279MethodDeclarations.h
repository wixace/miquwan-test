﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AntialiasingAsPostEffect
struct AntialiasingAsPostEffect_t2121704279;
// UnityEngine.Material
struct Material_t3870600107;
// UnityEngine.RenderTexture
struct RenderTexture_t1963041563;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture1963041563.h"

// System.Void AntialiasingAsPostEffect::.ctor()
extern "C"  void AntialiasingAsPostEffect__ctor_m3203954829 (AntialiasingAsPostEffect_t2121704279 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material AntialiasingAsPostEffect::CurrentAAMaterial()
extern "C"  Material_t3870600107 * AntialiasingAsPostEffect_CurrentAAMaterial_m3800539482 (AntialiasingAsPostEffect_t2121704279 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AntialiasingAsPostEffect::CheckResources()
extern "C"  bool AntialiasingAsPostEffect_CheckResources_m2907376442 (AntialiasingAsPostEffect_t2121704279 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AntialiasingAsPostEffect::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void AntialiasingAsPostEffect_OnRenderImage_m141068209 (AntialiasingAsPostEffect_t2121704279 * __this, RenderTexture_t1963041563 * ___source0, RenderTexture_t1963041563 * ___destination1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AntialiasingAsPostEffect::Main()
extern "C"  void AntialiasingAsPostEffect_Main_m711584400 (AntialiasingAsPostEffect_t2121704279 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
