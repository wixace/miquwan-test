﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// TargetMgr/AngleEntity
struct AngleEntity_t3194128142;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.Collections.Generic.List`1<UnityEngine.SkinnedMeshRenderer>
struct List_1_t1059259750;
// System.Collections.Generic.Dictionary`2<System.UInt32,CSSkillData>
struct Dictionary_2_t429438501;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t2522024052;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>
struct Dictionary_2_t1151101739;
// System.Collections.Generic.List`1<System.UInt32>
struct List_1_t1392853533;
// UnityEngine.ParticleSystem
struct ParticleSystem_t381473177;
// GameMgr
struct GameMgr_t1469029542;
// System.Collections.Generic.List`1<UnitStateBase>
struct List_1_t2408404110;
// CombatEntity
struct CombatEntity_t684137495;
// UnityEngine.ParticleSystem[]
struct ParticleSystemU5BU5D_t1536434148;
// System.Single[]
struct SingleU5BU5D_t2316563989;
// System.Boolean[]
struct BooleanU5BU5D_t3456302923;
// System.Collections.Generic.List`1<CombatEntity>
struct List_1_t2052323047;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// UnityEngine.Collider
struct Collider_t2939674232;
// System.Collections.Generic.List`1<System.Action>
struct List_1_t844452154;
// HatredCtrl
struct HatredCtrl_t891253697;
// CombatAttPlus
struct CombatAttPlus_t649400871;
// UnityEngine.SkinnedMeshRenderer[]
struct SkinnedMeshRendererU5BU5D_t1155881555;
// UnityEngine.Shader
struct Shader_t3191267369;
// System.Collections.Generic.List`1<shaderBuff>
struct List_1_t56138920;
// BloomEffect
struct BloomEffect_t3352511828;
// System.Collections.Generic.List`1<EnemyDropData>
struct List_1_t2836773265;
// System.Collections.Generic.List`1<AIObject>
struct List_1_t1405465687;
// AnimationRunner
struct AnimationRunner_t1015409588;
// FightCtrl
struct FightCtrl_t648967803;
// BuffCtrl
struct BuffCtrl_t2836564350;
// Entity.Behavior.IBehaviorCtrl
struct IBehaviorCtrl_t4225040900;
// UnityEngine.SkinnedMeshRenderer
struct SkinnedMeshRenderer_t3986041494;
// Blood
struct Blood_t64280026;
// PlotFightTalkMgr
struct PlotFightTalkMgr_t866599741;
// FS_ShadowSimple
struct FS_ShadowSimple_t4208748868;

#include "AssemblyU2DCSharp_AIPath1930792045.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"
#include "AssemblyU2DCSharp_EntityEnum_UnitCamp642829979.h"
#include "AssemblyU2DCSharp_EntityEnum_ModelScaleTween3031370284.h"
#include "AssemblyU2DCSharp_EntityEnum_PetrifiedTween965315849.h"
#include "AssemblyU2DCSharp_HERO_TYPE820012127.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882.h"
#include "AssemblyU2DCSharp_SEX_TYPE2342271891.h"
#include "AssemblyU2DCSharp_EntityEnum_UnitTag299076696.h"
#include "AssemblyU2DCSharp_EntityEnum_UnitType643359572.h"
#include "AssemblyU2DCSharp_EntityEnum_DeadType1438946868.h"
#include "AssemblyU2DCSharp_HERO_ELEMENT2692579223.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CombatEntity
struct  CombatEntity_t684137495  : public AIPath_t1930792045
{
public:
	// UnityEngine.Vector3 CombatEntity::_position
	Vector3_t4282066566  ____position_34;
	// TargetMgr/AngleEntity CombatEntity::_angleEntity
	AngleEntity_t3194128142 * ____angleEntity_35;
	// UnityEngine.GameObject CombatEntity::mModel
	GameObject_t3674682005 * ___mModel_37;
	// System.Collections.Generic.List`1<UnityEngine.SkinnedMeshRenderer> CombatEntity::hideList
	List_1_t1059259750 * ___hideList_38;
	// System.Single CombatEntity::_halfwidth
	float ____halfwidth_40;
	// System.Collections.Generic.Dictionary`2<System.UInt32,CSSkillData> CombatEntity::skillDic
	Dictionary_2_t429438501 * ___skillDic_41;
	// System.Collections.Generic.List`1<System.Int32> CombatEntity::godEquipAwakenSkillIDs
	List_1_t2522024052 * ___godEquipAwakenSkillIDs_42;
	// System.Collections.Generic.List`1<System.Int32> CombatEntity::godEquipAwakenSkillGradeIDs
	List_1_t2522024052 * ___godEquipAwakenSkillGradeIDs_43;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32> CombatEntity::NDPNum
	Dictionary_2_t1151101739 * ___NDPNum_44;
	// System.Single CombatEntity::oneSecTimer
	float ___oneSecTimer_45;
	// System.Collections.Generic.List`1<System.UInt32> CombatEntity::delayList
	List_1_t1392853533 * ___delayList_46;
	// System.Boolean CombatEntity::isSummoning
	bool ___isSummoning_47;
	// System.Int32 CombatEntity::effect_leader
	int32_t ___effect_leader_48;
	// System.Boolean CombatEntity::isRuneffect
	bool ___isRuneffect_49;
	// System.Single CombatEntity::curSuperArmorTime
	float ___curSuperArmorTime_50;
	// System.Single CombatEntity::curSuperArmor
	float ___curSuperArmor_51;
	// System.Single CombatEntity::runeTime
	float ___runeTime_52;
	// System.Single CombatEntity::godDownValue
	float ___godDownValue_53;
	// System.Single CombatEntity::godDownAddValue
	float ___godDownAddValue_54;
	// System.Single CombatEntity::curPetrifiedValue
	float ___curPetrifiedValue_55;
	// System.Boolean CombatEntity::_isSkillFrozen
	bool ____isSkillFrozen_56;
	// System.Boolean CombatEntity::isImmediateActive
	bool ___isImmediateActive_57;
	// System.Boolean CombatEntity::isAttacking
	bool ___isAttacking_58;
	// System.Int32 CombatEntity::country
	int32_t ___country_59;
	// UnityEngine.Color CombatEntity::colorchange
	Color_t4194546905  ___colorchange_60;
	// UnityEngine.ParticleSystem CombatEntity::lucencyps
	ParticleSystem_t381473177 * ___lucencyps_61;
	// System.Single CombatEntity::atkBaseTime
	float ___atkBaseTime_62;
	// GameMgr CombatEntity::gameMgr
	GameMgr_t1469029542 * ___gameMgr_63;
	// System.Collections.Generic.List`1<UnitStateBase> CombatEntity::unitstatelist
	List_1_t2408404110 * ___unitstatelist_64;
	// CombatEntity CombatEntity::_atkTarget
	CombatEntity_t684137495 * ____atkTarget_65;
	// CombatEntity CombatEntity::_aKill
	CombatEntity_t684137495 * ____aKill_66;
	// UnityEngine.ParticleSystem CombatEntity::runEffPS
	ParticleSystem_t381473177 * ___runEffPS_67;
	// UnityEngine.ParticleSystem[] CombatEntity::atkedEffPS
	ParticleSystemU5BU5D_t1536434148* ___atkedEffPS_68;
	// System.Single[] CombatEntity::atkedEffpsTimes
	SingleU5BU5D_t2316563989* ___atkedEffpsTimes_69;
	// UnityEngine.GameObject CombatEntity::pTarget
	GameObject_t3674682005 * ___pTarget_70;
	// System.Int32 CombatEntity::atkpoint
	int32_t ___atkpoint_71;
	// CombatEntity CombatEntity::atkpointEntity
	CombatEntity_t684137495 * ___atkpointEntity_72;
	// System.Single CombatEntity::deathTime
	float ___deathTime_73;
	// System.Boolean CombatEntity::isFrishSkill
	bool ___isFrishSkill_74;
	// CombatEntity CombatEntity::lastEntity
	CombatEntity_t684137495 * ___lastEntity_75;
	// System.Boolean[] CombatEntity::isAtkpoint
	BooleanU5BU5D_t3456302923* ___isAtkpoint_76;
	// System.Int32 CombatEntity::pointNum
	int32_t ___pointNum_77;
	// System.Single CombatEntity::_radius
	float ____radius_78;
	// System.Int32 CombatEntity::_effectid
	int32_t ____effectid_79;
	// System.Int32 CombatEntity::_effectSpeed
	int32_t ____effectSpeed_80;
	// System.Collections.Generic.List`1<CombatEntity> CombatEntity::hitList
	List_1_t2052323047 * ___hitList_81;
	// System.Int32 CombatEntity::staticEffectUId
	int32_t ___staticEffectUId_82;
	// System.Collections.Generic.List`1<System.Int32> CombatEntity::AddDamageSkillIDs
	List_1_t2522024052 * ___AddDamageSkillIDs_83;
	// System.Collections.Generic.List`1<System.Int32> CombatEntity::AddDamageValue
	List_1_t2522024052 * ___AddDamageValue_84;
	// System.Collections.Generic.List`1<System.Int32> CombatEntity::AddDamageValuePer
	List_1_t2522024052 * ___AddDamageValuePer_85;
	// CEvent.ZEvent CombatEntity::evNpcReduceHp
	ZEvent_t3638018500 * ___evNpcReduceHp_86;
	// CEvent.ZEvent CombatEntity::evBekilled
	ZEvent_t3638018500 * ___evBekilled_87;
	// System.Int32 CombatEntity::shaderLv
	int32_t ___shaderLv_88;
	// UnityEngine.Color CombatEntity::_RimColor
	Color_t4194546905  ____RimColor_89;
	// System.Single CombatEntity::_RimPower
	float ____RimPower_90;
	// System.Single CombatEntity::_EmissionP
	float ____EmissionP_91;
	// System.Int32 CombatEntity::curShaderlv
	int32_t ___curShaderlv_92;
	// System.String CombatEntity::AtkedEffName
	String_t* ___AtkedEffName_93;
	// System.Boolean CombatEntity::IsShowRender
	bool ___IsShowRender_94;
	// System.Single CombatEntity::dealyBackMoveCameraTime
	float ___dealyBackMoveCameraTime_95;
	// System.Single CombatEntity::oneSecAddHPTickTime
	float ___oneSecAddHPTickTime_96;
	// System.Single CombatEntity::oneSecReduceHPTickTime
	float ___oneSecReduceHPTickTime_97;
	// System.Single CombatEntity::oneSecAddAngerTickTime
	float ___oneSecAddAngerTickTime_98;
	// System.Single CombatEntity::oneSecAuraAddHPTime
	float ___oneSecAuraAddHPTime_99;
	// System.Single CombatEntity::oneSecAuraReduceHPTime
	float ___oneSecAuraReduceHPTime_100;
	// System.Single CombatEntity::radius
	float ___radius_101;
	// UnityEngine.Vector3 CombatEntity::historyPos
	Vector3_t4282066566  ___historyPos_102;
	// System.Boolean CombatEntity::bStartMove
	bool ___bStartMove_103;
	// UnityEngine.Collider CombatEntity::_collider
	Collider_t2939674232 * ____collider_104;
	// System.Collections.Generic.List`1<System.Action> CombatEntity::OnTargetCalls
	List_1_t844452154 * ___OnTargetCalls_105;
	// System.Boolean CombatEntity::isDoubleClick
	bool ___isDoubleClick_106;
	// System.Boolean CombatEntity::_forceShift
	bool ____forceShift_107;
	// HatredCtrl CombatEntity::hatredCtrl
	HatredCtrl_t891253697 * ___hatredCtrl_108;
	// CombatAttPlus CombatEntity::attPlus
	CombatAttPlus_t649400871 * ___attPlus_109;
	// System.Single CombatEntity::duration
	float ___duration_110;
	// System.Single CombatEntity::interval
	float ___interval_111;
	// System.Single CombatEntity::Intension
	float ___Intension_112;
	// UnityEngine.SkinnedMeshRenderer[] CombatEntity::meshRender
	SkinnedMeshRendererU5BU5D_t1155881555* ___meshRender_113;
	// UnityEngine.Shader CombatEntity::ghostShader
	Shader_t3191267369 * ___ghostShader_114;
	// System.Boolean CombatEntity::isShowGhost
	bool ___isShowGhost_115;
	// System.Collections.Generic.List`1<shaderBuff> CombatEntity::ghostShowList
	List_1_t56138920 * ___ghostShowList_116;
	// System.Single CombatEntity::lastTime
	float ___lastTime_117;
	// UnityEngine.Vector3 CombatEntity::lastPos
	Vector3_t4282066566  ___lastPos_118;
	// System.Collections.Generic.List`1<shaderBuff> CombatEntity::susanooList
	List_1_t56138920 * ___susanooList_119;
	// System.Single CombatEntity::susanooScale
	float ___susanooScale_120;
	// UnityEngine.Vector3 CombatEntity::offset
	Vector3_t4282066566  ___offset_121;
	// UnityEngine.GameObject CombatEntity::susanooObj
	GameObject_t3674682005 * ___susanooObj_122;
	// UnityEngine.Shader CombatEntity::SusanooShader
	Shader_t3191267369 * ___SusanooShader_123;
	// BloomEffect CombatEntity::curBloom
	BloomEffect_t3352511828 * ___curBloom_124;
	// System.Collections.Generic.List`1<shaderBuff> CombatEntity::bloomList
	List_1_t56138920 * ___bloomList_125;
	// System.Int32 CombatEntity::curIndex
	int32_t ___curIndex_126;
	// System.String CombatEntity::<showname>k__BackingField
	String_t* ___U3CshownameU3Ek__BackingField_127;
	// EntityEnum.UnitCamp CombatEntity::<unitCamp>k__BackingField
	int32_t ___U3CunitCampU3Ek__BackingField_128;
	// EntityEnum.ModelScaleTween CombatEntity::<modelScaleTween>k__BackingField
	int32_t ___U3CmodelScaleTweenU3Ek__BackingField_129;
	// EntityEnum.PetrifiedTween CombatEntity::<petrifiedTween>k__BackingField
	int32_t ___U3CpetrifiedTweenU3Ek__BackingField_130;
	// System.UInt32 CombatEntity::<playerId>k__BackingField
	uint32_t ___U3CplayerIdU3Ek__BackingField_131;
	// System.Int32 CombatEntity::<id>k__BackingField
	int32_t ___U3CidU3Ek__BackingField_132;
	// System.String CombatEntity::<icon>k__BackingField
	String_t* ___U3CiconU3Ek__BackingField_133;
	// System.String CombatEntity::<largeIcon>k__BackingField
	String_t* ___U3ClargeIconU3Ek__BackingField_134;
	// System.String CombatEntity::<bossshow>k__BackingField
	String_t* ___U3CbossshowU3Ek__BackingField_135;
	// System.String CombatEntity::<headicon>k__BackingField
	String_t* ___U3CheadiconU3Ek__BackingField_136;
	// System.Int32 CombatEntity::<IntelligenceTip>k__BackingField
	int32_t ___U3CIntelligenceTipU3Ek__BackingField_137;
	// System.Single CombatEntity::<scale>k__BackingField
	float ___U3CscaleU3Ek__BackingField_138;
	// System.Boolean CombatEntity::<isSummonMonster>k__BackingField
	bool ___U3CisSummonMonsterU3Ek__BackingField_139;
	// HERO_TYPE CombatEntity::<heromajor>k__BackingField
	int32_t ___U3CheromajorU3Ek__BackingField_140;
	// System.Single CombatEntity::<atkEffDelayTime>k__BackingField
	float ___U3CatkEffDelayTimeU3Ek__BackingField_141;
	// System.String CombatEntity::<skills>k__BackingField
	String_t* ___U3CskillsU3Ek__BackingField_142;
	// System.Int32 CombatEntity::<awakenLv>k__BackingField
	int32_t ___U3CawakenLvU3Ek__BackingField_143;
	// System.Single CombatEntity::<lucencyWait>k__BackingField
	float ___U3ClucencyWaitU3Ek__BackingField_144;
	// System.Int32 CombatEntity::<islucency>k__BackingField
	int32_t ___U3CislucencyU3Ek__BackingField_145;
	// System.Single CombatEntity::<lucencytime>k__BackingField
	float ___U3ClucencytimeU3Ek__BackingField_146;
	// System.Boolean CombatEntity::<elite>k__BackingField
	bool ___U3CeliteU3Ek__BackingField_147;
	// System.Boolean CombatEntity::<isAim>k__BackingField
	bool ___U3CisAimU3Ek__BackingField_148;
	// System.Int32 CombatEntity::<dutytype>k__BackingField
	int32_t ___U3CdutytypeU3Ek__BackingField_149;
	// System.Int32 CombatEntity::<ifattacked>k__BackingField
	int32_t ___U3CifattackedU3Ek__BackingField_150;
	// System.Collections.Generic.List`1<EnemyDropData> CombatEntity::<dropRwards>k__BackingField
	List_1_t2836773265 * ___U3CdropRwardsU3Ek__BackingField_151;
	// System.String CombatEntity::<storyId>k__BackingField
	String_t* ___U3CstoryIdU3Ek__BackingField_152;
	// System.Int32 CombatEntity::<elementEff>k__BackingField
	int32_t ___U3CelementEffU3Ek__BackingField_153;
	// System.Int32 CombatEntity::<atkedEff>k__BackingField
	int32_t ___U3CatkedEffU3Ek__BackingField_154;
	// System.Int32 CombatEntity::<deathEffect>k__BackingField
	int32_t ___U3CdeathEffectU3Ek__BackingField_155;
	// System.Int32 CombatEntity::<taixudeatheffect>k__BackingField
	int32_t ___U3CtaixudeatheffectU3Ek__BackingField_156;
	// System.Int32 CombatEntity::<reviveEffectId>k__BackingField
	int32_t ___U3CreviveEffectIdU3Ek__BackingField_157;
	// System.Int32 CombatEntity::<appeareffect>k__BackingField
	int32_t ___U3CappeareffectU3Ek__BackingField_158;
	// UnityEngine.Vector3 CombatEntity::<startPos>k__BackingField
	Vector3_t4282066566  ___U3CstartPosU3Ek__BackingField_159;
	// UnityEngine.Quaternion CombatEntity::<startRotation>k__BackingField
	Quaternion_t1553702882  ___U3CstartRotationU3Ek__BackingField_160;
	// System.Single CombatEntity::<atkDis>k__BackingField
	float ___U3CatkDisU3Ek__BackingField_161;
	// System.Single CombatEntity::<viewDis>k__BackingField
	float ___U3CviewDisU3Ek__BackingField_162;
	// System.Single CombatEntity::<originSpeed>k__BackingField
	float ___U3CoriginSpeedU3Ek__BackingField_163;
	// System.Boolean CombatEntity::<canAttack>k__BackingField
	bool ___U3CcanAttackU3Ek__BackingField_164;
	// System.Boolean CombatEntity::<isStun>k__BackingField
	bool ___U3CisStunU3Ek__BackingField_165;
	// System.Boolean CombatEntity::<isDeath>k__BackingField
	bool ___U3CisDeathU3Ek__BackingField_166;
	// System.Boolean CombatEntity::<isRevive>k__BackingField
	bool ___U3CisReviveU3Ek__BackingField_167;
	// System.Boolean CombatEntity::<isFend>k__BackingField
	bool ___U3CisFendU3Ek__BackingField_168;
	// System.Boolean CombatEntity::<isKnockingUp>k__BackingField
	bool ___U3CisKnockingUpU3Ek__BackingField_169;
	// System.Boolean CombatEntity::<isKnockingDown>k__BackingField
	bool ___U3CisKnockingDownU3Ek__BackingField_170;
	// System.Boolean CombatEntity::<isMovement>k__BackingField
	bool ___U3CisMovementU3Ek__BackingField_171;
	// System.Boolean CombatEntity::<isFlame>k__BackingField
	bool ___U3CisFlameU3Ek__BackingField_172;
	// System.Boolean CombatEntity::<isNotSelected>k__BackingField
	bool ___U3CisNotSelectedU3Ek__BackingField_173;
	// System.Boolean CombatEntity::<IsCheckTarget>k__BackingField
	bool ___U3CIsCheckTargetU3Ek__BackingField_174;
	// System.Boolean CombatEntity::<IsAutoMove>k__BackingField
	bool ___U3CIsAutoMoveU3Ek__BackingField_175;
	// System.Boolean CombatEntity::<IsMarshalMoving>k__BackingField
	bool ___U3CIsMarshalMovingU3Ek__BackingField_176;
	// System.Single CombatEntity::<RvoRadius>k__BackingField
	float ___U3CRvoRadiusU3Ek__BackingField_177;
	// System.Boolean CombatEntity::<IsBirthEffect>k__BackingField
	bool ___U3CIsBirthEffectU3Ek__BackingField_178;
	// System.Single CombatEntity::<starAttackTime>k__BackingField
	float ___U3CstarAttackTimeU3Ek__BackingField_179;
	// System.Boolean CombatEntity::<isAttack>k__BackingField
	bool ___U3CisAttackU3Ek__BackingField_180;
	// System.Boolean CombatEntity::<canFight>k__BackingField
	bool ___U3CcanFightU3Ek__BackingField_181;
	// UnityEngine.GameObject CombatEntity::<waistPoint>k__BackingField
	GameObject_t3674682005 * ___U3CwaistPointU3Ek__BackingField_182;
	// UnityEngine.Vector3 CombatEntity::<InitPosition>k__BackingField
	Vector3_t4282066566  ___U3CInitPositionU3Ek__BackingField_183;
	// System.Int32 CombatEntity::<UINamePos>k__BackingField
	int32_t ___U3CUINamePosU3Ek__BackingField_184;
	// System.Single CombatEntity::<headBloodPos>k__BackingField
	float ___U3CheadBloodPosU3Ek__BackingField_185;
	// SEX_TYPE CombatEntity::<sexType>k__BackingField
	int32_t ___U3CsexTypeU3Ek__BackingField_186;
	// System.Collections.Generic.List`1<AIObject> CombatEntity::<AIDelayList>k__BackingField
	List_1_t1405465687 * ___U3CAIDelayListU3Ek__BackingField_187;
	// System.Int32 CombatEntity::<initBehavior>k__BackingField
	int32_t ___U3CinitBehaviorU3Ek__BackingField_188;
	// AnimationRunner CombatEntity::<characterAnim>k__BackingField
	AnimationRunner_t1015409588 * ___U3CcharacterAnimU3Ek__BackingField_189;
	// System.Single CombatEntity::<totalDamage>k__BackingField
	float ___U3CtotalDamageU3Ek__BackingField_190;
	// EntityEnum.UnitTag CombatEntity::<atkUnitTag>k__BackingField
	int32_t ___U3CatkUnitTagU3Ek__BackingField_191;
	// EntityEnum.UnitTag CombatEntity::<unitTag>k__BackingField
	int32_t ___U3CunitTagU3Ek__BackingField_192;
	// FightCtrl CombatEntity::<fightCtrl>k__BackingField
	FightCtrl_t648967803 * ___U3CfightCtrlU3Ek__BackingField_193;
	// BuffCtrl CombatEntity::<buffCtrl>k__BackingField
	BuffCtrl_t2836564350 * ___U3CbuffCtrlU3Ek__BackingField_194;
	// Entity.Behavior.IBehaviorCtrl CombatEntity::<bvrCtrl>k__BackingField
	IBehaviorCtrl_t4225040900 * ___U3CbvrCtrlU3Ek__BackingField_195;
	// UnityEngine.SkinnedMeshRenderer CombatEntity::<skinMeshRender>k__BackingField
	SkinnedMeshRenderer_t3986041494 * ___U3CskinMeshRenderU3Ek__BackingField_196;
	// Blood CombatEntity::<blood>k__BackingField
	Blood_t64280026 * ___U3CbloodU3Ek__BackingField_197;
	// PlotFightTalkMgr CombatEntity::<PlotFightMgr>k__BackingField
	PlotFightTalkMgr_t866599741 * ___U3CPlotFightMgrU3Ek__BackingField_198;
	// FS_ShadowSimple CombatEntity::<shadow>k__BackingField
	FS_ShadowSimple_t4208748868 * ___U3CshadowU3Ek__BackingField_199;
	// EntityEnum.UnitType CombatEntity::<unitType>k__BackingField
	int32_t ___U3CunitTypeU3Ek__BackingField_200;
	// EntityEnum.DeadType CombatEntity::<deadType>k__BackingField
	int32_t ___U3CdeadTypeU3Ek__BackingField_201;
	// System.Int32 CombatEntity::<viewLevel>k__BackingField
	int32_t ___U3CviewLevelU3Ek__BackingField_202;
	// System.Int32 CombatEntity::<viewGrade>k__BackingField
	int32_t ___U3CviewGradeU3Ek__BackingField_203;
	// System.Int32 CombatEntity::<level>k__BackingField
	int32_t ___U3ClevelU3Ek__BackingField_204;
	// System.Int32 CombatEntity::<atkType>k__BackingField
	int32_t ___U3CatkTypeU3Ek__BackingField_205;
	// System.Int32 CombatEntity::<damageType>k__BackingField
	int32_t ___U3CdamageTypeU3Ek__BackingField_206;
	// System.Single CombatEntity::<atkPower>k__BackingField
	float ___U3CatkPowerU3Ek__BackingField_207;
	// System.Single CombatEntity::<atkPowerP>k__BackingField
	float ___U3CatkPowerPU3Ek__BackingField_208;
	// System.Single CombatEntity::<atkPowerL>k__BackingField
	float ___U3CatkPowerLU3Ek__BackingField_209;
	// System.Single CombatEntity::<PD>k__BackingField
	float ___U3CPDU3Ek__BackingField_210;
	// System.Single CombatEntity::<MD>k__BackingField
	float ___U3CMDU3Ek__BackingField_211;
	// System.Single CombatEntity::<defenseP>k__BackingField
	float ___U3CdefensePU3Ek__BackingField_212;
	// System.Single CombatEntity::<defenseL>k__BackingField
	float ___U3CdefenseLU3Ek__BackingField_213;
	// System.Single CombatEntity::<hp>k__BackingField
	float ___U3ChpU3Ek__BackingField_214;
	// System.Single CombatEntity::<maxHp>k__BackingField
	float ___U3CmaxHpU3Ek__BackingField_215;
	// System.Single CombatEntity::<HPSecDecPer>k__BackingField
	float ___U3CHPSecDecPerU3Ek__BackingField_216;
	// System.Single CombatEntity::<HPSecDecNum>k__BackingField
	float ___U3CHPSecDecNumU3Ek__BackingField_217;
	// System.Single CombatEntity::<HPRstNum>k__BackingField
	float ___U3CHPRstNumU3Ek__BackingField_218;
	// System.Single CombatEntity::<HPRstPer>k__BackingField
	float ___U3CHPRstPerU3Ek__BackingField_219;
	// System.Int32 CombatEntity::<starLevel>k__BackingField
	int32_t ___U3CstarLevelU3Ek__BackingField_220;
	// System.Single CombatEntity::<hits>k__BackingField
	float ___U3ChitsU3Ek__BackingField_221;
	// System.Single CombatEntity::<dodge>k__BackingField
	float ___U3CdodgeU3Ek__BackingField_222;
	// System.Single CombatEntity::<wreck>k__BackingField
	float ___U3CwreckU3Ek__BackingField_223;
	// System.Single CombatEntity::<withstand>k__BackingField
	float ___U3CwithstandU3Ek__BackingField_224;
	// System.Single CombatEntity::<crit>k__BackingField
	float ___U3CcritU3Ek__BackingField_225;
	// System.Single CombatEntity::<tenacity>k__BackingField
	float ___U3CtenacityU3Ek__BackingField_226;
	// System.Single CombatEntity::<hitsP>k__BackingField
	float ___U3ChitsPU3Ek__BackingField_227;
	// System.Single CombatEntity::<dodgeP>k__BackingField
	float ___U3CdodgePU3Ek__BackingField_228;
	// System.Single CombatEntity::<wreckP>k__BackingField
	float ___U3CwreckPU3Ek__BackingField_229;
	// System.Single CombatEntity::<withstandP>k__BackingField
	float ___U3CwithstandPU3Ek__BackingField_230;
	// System.Single CombatEntity::<critP>k__BackingField
	float ___U3CcritPU3Ek__BackingField_231;
	// System.Single CombatEntity::<tenacityP>k__BackingField
	float ___U3CtenacityPU3Ek__BackingField_232;
	// System.Single CombatEntity::<critHurt>k__BackingField
	float ___U3CcritHurtU3Ek__BackingField_233;
	// System.Single CombatEntity::<rHurt>k__BackingField
	float ___U3CrHurtU3Ek__BackingField_234;
	// System.Single CombatEntity::<cureP>k__BackingField
	float ___U3CcurePU3Ek__BackingField_235;
	// System.Single CombatEntity::<hurtP>k__BackingField
	float ___U3ChurtPU3Ek__BackingField_236;
	// System.Single CombatEntity::<hurtL>k__BackingField
	float ___U3ChurtLU3Ek__BackingField_237;
	// System.Single CombatEntity::<finalHurt>k__BackingField
	float ___U3CfinalHurtU3Ek__BackingField_238;
	// System.Single CombatEntity::<atkSpeed>k__BackingField
	float ___U3CatkSpeedU3Ek__BackingField_239;
	// System.Single CombatEntity::<runSpeed>k__BackingField
	float ___U3CrunSpeedU3Ek__BackingField_240;
	// System.Single CombatEntity::<atkRange>k__BackingField
	float ___U3CatkRangeU3Ek__BackingField_241;
	// System.Single CombatEntity::<maxRage>k__BackingField
	float ___U3CmaxRageU3Ek__BackingField_242;
	// System.Single CombatEntity::<findEnemyRange>k__BackingField
	float ___U3CfindEnemyRangeU3Ek__BackingField_243;
	// System.Single CombatEntity::<rage>k__BackingField
	float ___U3CrageU3Ek__BackingField_244;
	// System.Int32 CombatEntity::<rageGrowth>k__BackingField
	int32_t ___U3CrageGrowthU3Ek__BackingField_245;
	// HERO_ELEMENT CombatEntity::<element>k__BackingField
	int32_t ___U3CelementU3Ek__BackingField_246;
	// System.Int32 CombatEntity::<stable>k__BackingField
	int32_t ___U3CstableU3Ek__BackingField_247;
	// System.Int32 CombatEntity::<notInvade>k__BackingField
	int32_t ___U3CnotInvadeU3Ek__BackingField_248;
	// System.Int32 CombatEntity::<unyielding>k__BackingField
	int32_t ___U3CunyieldingU3Ek__BackingField_249;
	// System.Single CombatEntity::<modelScale>k__BackingField
	float ___U3CmodelScaleU3Ek__BackingField_250;
	// System.Single CombatEntity::<beforModelScale>k__BackingField
	float ___U3CbeforModelScaleU3Ek__BackingField_251;
	// System.Single CombatEntity::<modelScaleBetweenValue>k__BackingField
	float ___U3CmodelScaleBetweenValueU3Ek__BackingField_252;
	// System.Single CombatEntity::<curModelScaleValue>k__BackingField
	float ___U3CcurModelScaleValueU3Ek__BackingField_253;
	// System.Single CombatEntity::<vampire>k__BackingField
	float ___U3CvampireU3Ek__BackingField_254;
	// System.Single CombatEntity::<tauntDistance>k__BackingField
	float ___U3CtauntDistanceU3Ek__BackingField_255;
	// System.Int32 CombatEntity::<angerSecDecNum>k__BackingField
	int32_t ___U3CangerSecDecNumU3Ek__BackingField_256;
	// System.Int32 CombatEntity::<curAntiNum>k__BackingField
	int32_t ___U3CcurAntiNumU3Ek__BackingField_257;
	// System.Int32 CombatEntity::<inUseAntiNum>k__BackingField
	int32_t ___U3CinUseAntiNumU3Ek__BackingField_258;
	// System.Int32 CombatEntity::<curShieldNum>k__BackingField
	int32_t ___U3CcurShieldNumU3Ek__BackingField_259;
	// System.Int32 CombatEntity::<hidePart>k__BackingField
	int32_t ___U3ChidePartU3Ek__BackingField_260;
	// System.Int32 CombatEntity::<restraintCountryType>k__BackingField
	int32_t ___U3CrestraintCountryTypeU3Ek__BackingField_261;
	// System.Single CombatEntity::<restraintCountryPer>k__BackingField
	float ___U3CrestraintCountryPerU3Ek__BackingField_262;
	// System.Int32 CombatEntity::<restraintStateType>k__BackingField
	int32_t ___U3CrestraintStateTypeU3Ek__BackingField_263;
	// System.Single CombatEntity::<restraintStatePer>k__BackingField
	float ___U3CrestraintStatePerU3Ek__BackingField_264;
	// System.Int32 CombatEntity::<NDPAddBuff>k__BackingField
	int32_t ___U3CNDPAddBuffU3Ek__BackingField_265;
	// System.Int32 CombatEntity::<NDPAddBuff02>k__BackingField
	int32_t ___U3CNDPAddBuff02U3Ek__BackingField_266;
	// System.Int32 CombatEntity::<BeAttackedLaterSkillID>k__BackingField
	int32_t ___U3CBeAttackedLaterSkillIDU3Ek__BackingField_267;
	// System.Int32 CombatEntity::<BeAttackedLaterEffectID>k__BackingField
	int32_t ___U3CBeAttackedLaterEffectIDU3Ek__BackingField_268;
	// System.Int32 CombatEntity::<DeathLaterEffectID>k__BackingField
	int32_t ___U3CDeathLaterEffectIDU3Ek__BackingField_269;
	// System.Int32 CombatEntity::<DeathLaterHeroID>k__BackingField
	int32_t ___U3CDeathLaterHeroIDU3Ek__BackingField_270;
	// System.Int32 CombatEntity::<BeattackedLaterSpeed>k__BackingField
	int32_t ___U3CBeattackedLaterSpeedU3Ek__BackingField_271;
	// System.Int32 CombatEntity::<DeathLaterSpeed>k__BackingField
	int32_t ___U3CDeathLaterSpeedU3Ek__BackingField_272;
	// System.Int32 CombatEntity::<AntiEndEffectID>k__BackingField
	int32_t ___U3CAntiEndEffectIDU3Ek__BackingField_273;
	// System.Int32 CombatEntity::<AuraAddHPRadius>k__BackingField
	int32_t ___U3CAuraAddHPRadiusU3Ek__BackingField_274;
	// System.Int32 CombatEntity::<AuraAddHPNum>k__BackingField
	int32_t ___U3CAuraAddHPNumU3Ek__BackingField_275;
	// System.Int32 CombatEntity::<AuraAddHPPer>k__BackingField
	int32_t ___U3CAuraAddHPPerU3Ek__BackingField_276;
	// System.Int32 CombatEntity::<AuraReduceHPRadius>k__BackingField
	int32_t ___U3CAuraReduceHPRadiusU3Ek__BackingField_277;
	// System.Int32 CombatEntity::<AuraReduceHPNum>k__BackingField
	int32_t ___U3CAuraReduceHPNumU3Ek__BackingField_278;
	// System.Int32 CombatEntity::<AuraReduceHPPer>k__BackingField
	int32_t ___U3CAuraReduceHPPerU3Ek__BackingField_279;
	// System.Single CombatEntity::<ActiveSkillDamageAddPer>k__BackingField
	float ___U3CActiveSkillDamageAddPerU3Ek__BackingField_280;
	// System.Single CombatEntity::<AddAngerRatio>k__BackingField
	float ___U3CAddAngerRatioU3Ek__BackingField_281;
	// System.Int32 CombatEntity::<beHurtAnger>k__BackingField
	int32_t ___U3CbeHurtAngerU3Ek__BackingField_282;
	// System.Int32 CombatEntity::<superArmor>k__BackingField
	int32_t ___U3CsuperArmorU3Ek__BackingField_283;
	// System.Single CombatEntity::<superArmorTime>k__BackingField
	float ___U3CsuperArmorTimeU3Ek__BackingField_284;
	// System.Int32 CombatEntity::<hpLineNum>k__BackingField
	int32_t ___U3ChpLineNumU3Ek__BackingField_285;
	// System.Int32 CombatEntity::<hpLineBuffID>k__BackingField
	int32_t ___U3ChpLineBuffIDU3Ek__BackingField_286;
	// System.Int32 CombatEntity::<hpLineUpBuffID>k__BackingField
	int32_t ___U3ChpLineUpBuffIDU3Ek__BackingField_287;
	// System.Int32 CombatEntity::<hpLineCleaveAddBuff>k__BackingField
	int32_t ___U3ChpLineCleaveAddBuffU3Ek__BackingField_288;
	// System.Int32 CombatEntity::<hpLineUpFrameCount>k__BackingField
	int32_t ___U3ChpLineUpFrameCountU3Ek__BackingField_289;
	// System.Single CombatEntity::<controlBuffTimeBonus>k__BackingField
	float ___U3CcontrolBuffTimeBonusU3Ek__BackingField_290;
	// System.Single CombatEntity::<controlBuffTimeBonusPer>k__BackingField
	float ___U3CcontrolBuffTimeBonusPerU3Ek__BackingField_291;
	// System.Int32 CombatEntity::<runeNum>k__BackingField
	int32_t ___U3CruneNumU3Ek__BackingField_292;
	// System.Int32 CombatEntity::<runeNumAllSkill>k__BackingField
	int32_t ___U3CruneNumAllSkillU3Ek__BackingField_293;
	// System.Int32 CombatEntity::<runeDisappearTime>k__BackingField
	int32_t ___U3CruneDisappearTimeU3Ek__BackingField_294;
	// System.Int32 CombatEntity::<runeTakeEffectNum>k__BackingField
	int32_t ___U3CruneTakeEffectNumU3Ek__BackingField_295;
	// System.Int32 CombatEntity::<runeTakeEffectBuffID>k__BackingField
	int32_t ___U3CruneTakeEffectBuffIDU3Ek__BackingField_296;
	// System.Int32 CombatEntity::<runeTakeEffectBuffID01>k__BackingField
	int32_t ___U3CruneTakeEffectBuffID01U3Ek__BackingField_297;
	// System.Int32 CombatEntity::<runeTakeEffectBuffID02>k__BackingField
	int32_t ___U3CruneTakeEffectBuffID02U3Ek__BackingField_298;
	// System.Single CombatEntity::<godDownTweenTime>k__BackingField
	float ___U3CgodDownTweenTimeU3Ek__BackingField_299;
	// System.Int32 CombatEntity::<antiDebuffDiscreteID>k__BackingField
	int32_t ___U3CantiDebuffDiscreteIDU3Ek__BackingField_300;
	// System.Int32 CombatEntity::<antiDebuffDiscreteAddBuffID>k__BackingField
	int32_t ___U3CantiDebuffDiscreteAddBuffIDU3Ek__BackingField_301;
	// System.Int32 CombatEntity::<discreteBuffID>k__BackingField
	int32_t ___U3CdiscreteBuffIDU3Ek__BackingField_302;
	// System.Single CombatEntity::<discreteBuffTime>k__BackingField
	float ___U3CdiscreteBuffTimeU3Ek__BackingField_303;
	// System.Single CombatEntity::<discreteBuffTimer>k__BackingField
	float ___U3CdiscreteBuffTimerU3Ek__BackingField_304;
	// System.Single CombatEntity::<petrifiedChangeTime>k__BackingField
	float ___U3CpetrifiedChangeTimeU3Ek__BackingField_305;
	// System.Int32 CombatEntity::<changeOldSkillID>k__BackingField
	int32_t ___U3CchangeOldSkillIDU3Ek__BackingField_306;
	// System.Int32 CombatEntity::<changeNewSkillID>k__BackingField
	int32_t ___U3CchangeNewSkillIDU3Ek__BackingField_307;
	// System.Int32 CombatEntity::<changeSkillPro>k__BackingField
	int32_t ___U3CchangeSkillProU3Ek__BackingField_308;
	// System.Int32 CombatEntity::<whenCritAddBuffID>k__BackingField
	int32_t ___U3CwhenCritAddBuffIDU3Ek__BackingField_309;
	// System.Int32 CombatEntity::<thumpSkillID>k__BackingField
	int32_t ___U3CthumpSkillIDU3Ek__BackingField_310;
	// System.Single CombatEntity::<thumpAddValue>k__BackingField
	float ___U3CthumpAddValueU3Ek__BackingField_311;
	// System.Int32 CombatEntity::<tankBuffID>k__BackingField
	int32_t ___U3CtankBuffIDU3Ek__BackingField_312;
	// System.Int32 CombatEntity::<HTBuffID>k__BackingField
	int32_t ___U3CHTBuffIDU3Ek__BackingField_313;
	// System.Int32 CombatEntity::<cleaveSkillID>k__BackingField
	int32_t ___U3CcleaveSkillIDU3Ek__BackingField_314;
	// System.Int32 CombatEntity::<cleaveBuffID>k__BackingField
	int32_t ___U3CcleaveBuffIDU3Ek__BackingField_315;
	// System.Int32 CombatEntity::<reduceRageNum>k__BackingField
	int32_t ___U3CreduceRageNumU3Ek__BackingField_316;
	// System.Int32 CombatEntity::<deathHarvestLine>k__BackingField
	int32_t ___U3CdeathHarvestLineU3Ek__BackingField_317;
	// System.Int32 CombatEntity::<deathHarvestPer>k__BackingField
	int32_t ___U3CdeathHarvestPerU3Ek__BackingField_318;
	// System.Int32 CombatEntity::<resistanceCountryType>k__BackingField
	int32_t ___U3CresistanceCountryTypeU3Ek__BackingField_319;
	// System.Single CombatEntity::<resistanceCountryPer>k__BackingField
	float ___U3CresistanceCountryPerU3Ek__BackingField_320;

public:
	inline static int32_t get_offset_of__position_34() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ____position_34)); }
	inline Vector3_t4282066566  get__position_34() const { return ____position_34; }
	inline Vector3_t4282066566 * get_address_of__position_34() { return &____position_34; }
	inline void set__position_34(Vector3_t4282066566  value)
	{
		____position_34 = value;
	}

	inline static int32_t get_offset_of__angleEntity_35() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ____angleEntity_35)); }
	inline AngleEntity_t3194128142 * get__angleEntity_35() const { return ____angleEntity_35; }
	inline AngleEntity_t3194128142 ** get_address_of__angleEntity_35() { return &____angleEntity_35; }
	inline void set__angleEntity_35(AngleEntity_t3194128142 * value)
	{
		____angleEntity_35 = value;
		Il2CppCodeGenWriteBarrier(&____angleEntity_35, value);
	}

	inline static int32_t get_offset_of_mModel_37() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___mModel_37)); }
	inline GameObject_t3674682005 * get_mModel_37() const { return ___mModel_37; }
	inline GameObject_t3674682005 ** get_address_of_mModel_37() { return &___mModel_37; }
	inline void set_mModel_37(GameObject_t3674682005 * value)
	{
		___mModel_37 = value;
		Il2CppCodeGenWriteBarrier(&___mModel_37, value);
	}

	inline static int32_t get_offset_of_hideList_38() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___hideList_38)); }
	inline List_1_t1059259750 * get_hideList_38() const { return ___hideList_38; }
	inline List_1_t1059259750 ** get_address_of_hideList_38() { return &___hideList_38; }
	inline void set_hideList_38(List_1_t1059259750 * value)
	{
		___hideList_38 = value;
		Il2CppCodeGenWriteBarrier(&___hideList_38, value);
	}

	inline static int32_t get_offset_of__halfwidth_40() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ____halfwidth_40)); }
	inline float get__halfwidth_40() const { return ____halfwidth_40; }
	inline float* get_address_of__halfwidth_40() { return &____halfwidth_40; }
	inline void set__halfwidth_40(float value)
	{
		____halfwidth_40 = value;
	}

	inline static int32_t get_offset_of_skillDic_41() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___skillDic_41)); }
	inline Dictionary_2_t429438501 * get_skillDic_41() const { return ___skillDic_41; }
	inline Dictionary_2_t429438501 ** get_address_of_skillDic_41() { return &___skillDic_41; }
	inline void set_skillDic_41(Dictionary_2_t429438501 * value)
	{
		___skillDic_41 = value;
		Il2CppCodeGenWriteBarrier(&___skillDic_41, value);
	}

	inline static int32_t get_offset_of_godEquipAwakenSkillIDs_42() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___godEquipAwakenSkillIDs_42)); }
	inline List_1_t2522024052 * get_godEquipAwakenSkillIDs_42() const { return ___godEquipAwakenSkillIDs_42; }
	inline List_1_t2522024052 ** get_address_of_godEquipAwakenSkillIDs_42() { return &___godEquipAwakenSkillIDs_42; }
	inline void set_godEquipAwakenSkillIDs_42(List_1_t2522024052 * value)
	{
		___godEquipAwakenSkillIDs_42 = value;
		Il2CppCodeGenWriteBarrier(&___godEquipAwakenSkillIDs_42, value);
	}

	inline static int32_t get_offset_of_godEquipAwakenSkillGradeIDs_43() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___godEquipAwakenSkillGradeIDs_43)); }
	inline List_1_t2522024052 * get_godEquipAwakenSkillGradeIDs_43() const { return ___godEquipAwakenSkillGradeIDs_43; }
	inline List_1_t2522024052 ** get_address_of_godEquipAwakenSkillGradeIDs_43() { return &___godEquipAwakenSkillGradeIDs_43; }
	inline void set_godEquipAwakenSkillGradeIDs_43(List_1_t2522024052 * value)
	{
		___godEquipAwakenSkillGradeIDs_43 = value;
		Il2CppCodeGenWriteBarrier(&___godEquipAwakenSkillGradeIDs_43, value);
	}

	inline static int32_t get_offset_of_NDPNum_44() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___NDPNum_44)); }
	inline Dictionary_2_t1151101739 * get_NDPNum_44() const { return ___NDPNum_44; }
	inline Dictionary_2_t1151101739 ** get_address_of_NDPNum_44() { return &___NDPNum_44; }
	inline void set_NDPNum_44(Dictionary_2_t1151101739 * value)
	{
		___NDPNum_44 = value;
		Il2CppCodeGenWriteBarrier(&___NDPNum_44, value);
	}

	inline static int32_t get_offset_of_oneSecTimer_45() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___oneSecTimer_45)); }
	inline float get_oneSecTimer_45() const { return ___oneSecTimer_45; }
	inline float* get_address_of_oneSecTimer_45() { return &___oneSecTimer_45; }
	inline void set_oneSecTimer_45(float value)
	{
		___oneSecTimer_45 = value;
	}

	inline static int32_t get_offset_of_delayList_46() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___delayList_46)); }
	inline List_1_t1392853533 * get_delayList_46() const { return ___delayList_46; }
	inline List_1_t1392853533 ** get_address_of_delayList_46() { return &___delayList_46; }
	inline void set_delayList_46(List_1_t1392853533 * value)
	{
		___delayList_46 = value;
		Il2CppCodeGenWriteBarrier(&___delayList_46, value);
	}

	inline static int32_t get_offset_of_isSummoning_47() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___isSummoning_47)); }
	inline bool get_isSummoning_47() const { return ___isSummoning_47; }
	inline bool* get_address_of_isSummoning_47() { return &___isSummoning_47; }
	inline void set_isSummoning_47(bool value)
	{
		___isSummoning_47 = value;
	}

	inline static int32_t get_offset_of_effect_leader_48() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___effect_leader_48)); }
	inline int32_t get_effect_leader_48() const { return ___effect_leader_48; }
	inline int32_t* get_address_of_effect_leader_48() { return &___effect_leader_48; }
	inline void set_effect_leader_48(int32_t value)
	{
		___effect_leader_48 = value;
	}

	inline static int32_t get_offset_of_isRuneffect_49() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___isRuneffect_49)); }
	inline bool get_isRuneffect_49() const { return ___isRuneffect_49; }
	inline bool* get_address_of_isRuneffect_49() { return &___isRuneffect_49; }
	inline void set_isRuneffect_49(bool value)
	{
		___isRuneffect_49 = value;
	}

	inline static int32_t get_offset_of_curSuperArmorTime_50() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___curSuperArmorTime_50)); }
	inline float get_curSuperArmorTime_50() const { return ___curSuperArmorTime_50; }
	inline float* get_address_of_curSuperArmorTime_50() { return &___curSuperArmorTime_50; }
	inline void set_curSuperArmorTime_50(float value)
	{
		___curSuperArmorTime_50 = value;
	}

	inline static int32_t get_offset_of_curSuperArmor_51() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___curSuperArmor_51)); }
	inline float get_curSuperArmor_51() const { return ___curSuperArmor_51; }
	inline float* get_address_of_curSuperArmor_51() { return &___curSuperArmor_51; }
	inline void set_curSuperArmor_51(float value)
	{
		___curSuperArmor_51 = value;
	}

	inline static int32_t get_offset_of_runeTime_52() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___runeTime_52)); }
	inline float get_runeTime_52() const { return ___runeTime_52; }
	inline float* get_address_of_runeTime_52() { return &___runeTime_52; }
	inline void set_runeTime_52(float value)
	{
		___runeTime_52 = value;
	}

	inline static int32_t get_offset_of_godDownValue_53() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___godDownValue_53)); }
	inline float get_godDownValue_53() const { return ___godDownValue_53; }
	inline float* get_address_of_godDownValue_53() { return &___godDownValue_53; }
	inline void set_godDownValue_53(float value)
	{
		___godDownValue_53 = value;
	}

	inline static int32_t get_offset_of_godDownAddValue_54() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___godDownAddValue_54)); }
	inline float get_godDownAddValue_54() const { return ___godDownAddValue_54; }
	inline float* get_address_of_godDownAddValue_54() { return &___godDownAddValue_54; }
	inline void set_godDownAddValue_54(float value)
	{
		___godDownAddValue_54 = value;
	}

	inline static int32_t get_offset_of_curPetrifiedValue_55() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___curPetrifiedValue_55)); }
	inline float get_curPetrifiedValue_55() const { return ___curPetrifiedValue_55; }
	inline float* get_address_of_curPetrifiedValue_55() { return &___curPetrifiedValue_55; }
	inline void set_curPetrifiedValue_55(float value)
	{
		___curPetrifiedValue_55 = value;
	}

	inline static int32_t get_offset_of__isSkillFrozen_56() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ____isSkillFrozen_56)); }
	inline bool get__isSkillFrozen_56() const { return ____isSkillFrozen_56; }
	inline bool* get_address_of__isSkillFrozen_56() { return &____isSkillFrozen_56; }
	inline void set__isSkillFrozen_56(bool value)
	{
		____isSkillFrozen_56 = value;
	}

	inline static int32_t get_offset_of_isImmediateActive_57() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___isImmediateActive_57)); }
	inline bool get_isImmediateActive_57() const { return ___isImmediateActive_57; }
	inline bool* get_address_of_isImmediateActive_57() { return &___isImmediateActive_57; }
	inline void set_isImmediateActive_57(bool value)
	{
		___isImmediateActive_57 = value;
	}

	inline static int32_t get_offset_of_isAttacking_58() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___isAttacking_58)); }
	inline bool get_isAttacking_58() const { return ___isAttacking_58; }
	inline bool* get_address_of_isAttacking_58() { return &___isAttacking_58; }
	inline void set_isAttacking_58(bool value)
	{
		___isAttacking_58 = value;
	}

	inline static int32_t get_offset_of_country_59() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___country_59)); }
	inline int32_t get_country_59() const { return ___country_59; }
	inline int32_t* get_address_of_country_59() { return &___country_59; }
	inline void set_country_59(int32_t value)
	{
		___country_59 = value;
	}

	inline static int32_t get_offset_of_colorchange_60() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___colorchange_60)); }
	inline Color_t4194546905  get_colorchange_60() const { return ___colorchange_60; }
	inline Color_t4194546905 * get_address_of_colorchange_60() { return &___colorchange_60; }
	inline void set_colorchange_60(Color_t4194546905  value)
	{
		___colorchange_60 = value;
	}

	inline static int32_t get_offset_of_lucencyps_61() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___lucencyps_61)); }
	inline ParticleSystem_t381473177 * get_lucencyps_61() const { return ___lucencyps_61; }
	inline ParticleSystem_t381473177 ** get_address_of_lucencyps_61() { return &___lucencyps_61; }
	inline void set_lucencyps_61(ParticleSystem_t381473177 * value)
	{
		___lucencyps_61 = value;
		Il2CppCodeGenWriteBarrier(&___lucencyps_61, value);
	}

	inline static int32_t get_offset_of_atkBaseTime_62() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___atkBaseTime_62)); }
	inline float get_atkBaseTime_62() const { return ___atkBaseTime_62; }
	inline float* get_address_of_atkBaseTime_62() { return &___atkBaseTime_62; }
	inline void set_atkBaseTime_62(float value)
	{
		___atkBaseTime_62 = value;
	}

	inline static int32_t get_offset_of_gameMgr_63() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___gameMgr_63)); }
	inline GameMgr_t1469029542 * get_gameMgr_63() const { return ___gameMgr_63; }
	inline GameMgr_t1469029542 ** get_address_of_gameMgr_63() { return &___gameMgr_63; }
	inline void set_gameMgr_63(GameMgr_t1469029542 * value)
	{
		___gameMgr_63 = value;
		Il2CppCodeGenWriteBarrier(&___gameMgr_63, value);
	}

	inline static int32_t get_offset_of_unitstatelist_64() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___unitstatelist_64)); }
	inline List_1_t2408404110 * get_unitstatelist_64() const { return ___unitstatelist_64; }
	inline List_1_t2408404110 ** get_address_of_unitstatelist_64() { return &___unitstatelist_64; }
	inline void set_unitstatelist_64(List_1_t2408404110 * value)
	{
		___unitstatelist_64 = value;
		Il2CppCodeGenWriteBarrier(&___unitstatelist_64, value);
	}

	inline static int32_t get_offset_of__atkTarget_65() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ____atkTarget_65)); }
	inline CombatEntity_t684137495 * get__atkTarget_65() const { return ____atkTarget_65; }
	inline CombatEntity_t684137495 ** get_address_of__atkTarget_65() { return &____atkTarget_65; }
	inline void set__atkTarget_65(CombatEntity_t684137495 * value)
	{
		____atkTarget_65 = value;
		Il2CppCodeGenWriteBarrier(&____atkTarget_65, value);
	}

	inline static int32_t get_offset_of__aKill_66() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ____aKill_66)); }
	inline CombatEntity_t684137495 * get__aKill_66() const { return ____aKill_66; }
	inline CombatEntity_t684137495 ** get_address_of__aKill_66() { return &____aKill_66; }
	inline void set__aKill_66(CombatEntity_t684137495 * value)
	{
		____aKill_66 = value;
		Il2CppCodeGenWriteBarrier(&____aKill_66, value);
	}

	inline static int32_t get_offset_of_runEffPS_67() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___runEffPS_67)); }
	inline ParticleSystem_t381473177 * get_runEffPS_67() const { return ___runEffPS_67; }
	inline ParticleSystem_t381473177 ** get_address_of_runEffPS_67() { return &___runEffPS_67; }
	inline void set_runEffPS_67(ParticleSystem_t381473177 * value)
	{
		___runEffPS_67 = value;
		Il2CppCodeGenWriteBarrier(&___runEffPS_67, value);
	}

	inline static int32_t get_offset_of_atkedEffPS_68() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___atkedEffPS_68)); }
	inline ParticleSystemU5BU5D_t1536434148* get_atkedEffPS_68() const { return ___atkedEffPS_68; }
	inline ParticleSystemU5BU5D_t1536434148** get_address_of_atkedEffPS_68() { return &___atkedEffPS_68; }
	inline void set_atkedEffPS_68(ParticleSystemU5BU5D_t1536434148* value)
	{
		___atkedEffPS_68 = value;
		Il2CppCodeGenWriteBarrier(&___atkedEffPS_68, value);
	}

	inline static int32_t get_offset_of_atkedEffpsTimes_69() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___atkedEffpsTimes_69)); }
	inline SingleU5BU5D_t2316563989* get_atkedEffpsTimes_69() const { return ___atkedEffpsTimes_69; }
	inline SingleU5BU5D_t2316563989** get_address_of_atkedEffpsTimes_69() { return &___atkedEffpsTimes_69; }
	inline void set_atkedEffpsTimes_69(SingleU5BU5D_t2316563989* value)
	{
		___atkedEffpsTimes_69 = value;
		Il2CppCodeGenWriteBarrier(&___atkedEffpsTimes_69, value);
	}

	inline static int32_t get_offset_of_pTarget_70() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___pTarget_70)); }
	inline GameObject_t3674682005 * get_pTarget_70() const { return ___pTarget_70; }
	inline GameObject_t3674682005 ** get_address_of_pTarget_70() { return &___pTarget_70; }
	inline void set_pTarget_70(GameObject_t3674682005 * value)
	{
		___pTarget_70 = value;
		Il2CppCodeGenWriteBarrier(&___pTarget_70, value);
	}

	inline static int32_t get_offset_of_atkpoint_71() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___atkpoint_71)); }
	inline int32_t get_atkpoint_71() const { return ___atkpoint_71; }
	inline int32_t* get_address_of_atkpoint_71() { return &___atkpoint_71; }
	inline void set_atkpoint_71(int32_t value)
	{
		___atkpoint_71 = value;
	}

	inline static int32_t get_offset_of_atkpointEntity_72() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___atkpointEntity_72)); }
	inline CombatEntity_t684137495 * get_atkpointEntity_72() const { return ___atkpointEntity_72; }
	inline CombatEntity_t684137495 ** get_address_of_atkpointEntity_72() { return &___atkpointEntity_72; }
	inline void set_atkpointEntity_72(CombatEntity_t684137495 * value)
	{
		___atkpointEntity_72 = value;
		Il2CppCodeGenWriteBarrier(&___atkpointEntity_72, value);
	}

	inline static int32_t get_offset_of_deathTime_73() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___deathTime_73)); }
	inline float get_deathTime_73() const { return ___deathTime_73; }
	inline float* get_address_of_deathTime_73() { return &___deathTime_73; }
	inline void set_deathTime_73(float value)
	{
		___deathTime_73 = value;
	}

	inline static int32_t get_offset_of_isFrishSkill_74() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___isFrishSkill_74)); }
	inline bool get_isFrishSkill_74() const { return ___isFrishSkill_74; }
	inline bool* get_address_of_isFrishSkill_74() { return &___isFrishSkill_74; }
	inline void set_isFrishSkill_74(bool value)
	{
		___isFrishSkill_74 = value;
	}

	inline static int32_t get_offset_of_lastEntity_75() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___lastEntity_75)); }
	inline CombatEntity_t684137495 * get_lastEntity_75() const { return ___lastEntity_75; }
	inline CombatEntity_t684137495 ** get_address_of_lastEntity_75() { return &___lastEntity_75; }
	inline void set_lastEntity_75(CombatEntity_t684137495 * value)
	{
		___lastEntity_75 = value;
		Il2CppCodeGenWriteBarrier(&___lastEntity_75, value);
	}

	inline static int32_t get_offset_of_isAtkpoint_76() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___isAtkpoint_76)); }
	inline BooleanU5BU5D_t3456302923* get_isAtkpoint_76() const { return ___isAtkpoint_76; }
	inline BooleanU5BU5D_t3456302923** get_address_of_isAtkpoint_76() { return &___isAtkpoint_76; }
	inline void set_isAtkpoint_76(BooleanU5BU5D_t3456302923* value)
	{
		___isAtkpoint_76 = value;
		Il2CppCodeGenWriteBarrier(&___isAtkpoint_76, value);
	}

	inline static int32_t get_offset_of_pointNum_77() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___pointNum_77)); }
	inline int32_t get_pointNum_77() const { return ___pointNum_77; }
	inline int32_t* get_address_of_pointNum_77() { return &___pointNum_77; }
	inline void set_pointNum_77(int32_t value)
	{
		___pointNum_77 = value;
	}

	inline static int32_t get_offset_of__radius_78() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ____radius_78)); }
	inline float get__radius_78() const { return ____radius_78; }
	inline float* get_address_of__radius_78() { return &____radius_78; }
	inline void set__radius_78(float value)
	{
		____radius_78 = value;
	}

	inline static int32_t get_offset_of__effectid_79() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ____effectid_79)); }
	inline int32_t get__effectid_79() const { return ____effectid_79; }
	inline int32_t* get_address_of__effectid_79() { return &____effectid_79; }
	inline void set__effectid_79(int32_t value)
	{
		____effectid_79 = value;
	}

	inline static int32_t get_offset_of__effectSpeed_80() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ____effectSpeed_80)); }
	inline int32_t get__effectSpeed_80() const { return ____effectSpeed_80; }
	inline int32_t* get_address_of__effectSpeed_80() { return &____effectSpeed_80; }
	inline void set__effectSpeed_80(int32_t value)
	{
		____effectSpeed_80 = value;
	}

	inline static int32_t get_offset_of_hitList_81() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___hitList_81)); }
	inline List_1_t2052323047 * get_hitList_81() const { return ___hitList_81; }
	inline List_1_t2052323047 ** get_address_of_hitList_81() { return &___hitList_81; }
	inline void set_hitList_81(List_1_t2052323047 * value)
	{
		___hitList_81 = value;
		Il2CppCodeGenWriteBarrier(&___hitList_81, value);
	}

	inline static int32_t get_offset_of_staticEffectUId_82() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___staticEffectUId_82)); }
	inline int32_t get_staticEffectUId_82() const { return ___staticEffectUId_82; }
	inline int32_t* get_address_of_staticEffectUId_82() { return &___staticEffectUId_82; }
	inline void set_staticEffectUId_82(int32_t value)
	{
		___staticEffectUId_82 = value;
	}

	inline static int32_t get_offset_of_AddDamageSkillIDs_83() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___AddDamageSkillIDs_83)); }
	inline List_1_t2522024052 * get_AddDamageSkillIDs_83() const { return ___AddDamageSkillIDs_83; }
	inline List_1_t2522024052 ** get_address_of_AddDamageSkillIDs_83() { return &___AddDamageSkillIDs_83; }
	inline void set_AddDamageSkillIDs_83(List_1_t2522024052 * value)
	{
		___AddDamageSkillIDs_83 = value;
		Il2CppCodeGenWriteBarrier(&___AddDamageSkillIDs_83, value);
	}

	inline static int32_t get_offset_of_AddDamageValue_84() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___AddDamageValue_84)); }
	inline List_1_t2522024052 * get_AddDamageValue_84() const { return ___AddDamageValue_84; }
	inline List_1_t2522024052 ** get_address_of_AddDamageValue_84() { return &___AddDamageValue_84; }
	inline void set_AddDamageValue_84(List_1_t2522024052 * value)
	{
		___AddDamageValue_84 = value;
		Il2CppCodeGenWriteBarrier(&___AddDamageValue_84, value);
	}

	inline static int32_t get_offset_of_AddDamageValuePer_85() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___AddDamageValuePer_85)); }
	inline List_1_t2522024052 * get_AddDamageValuePer_85() const { return ___AddDamageValuePer_85; }
	inline List_1_t2522024052 ** get_address_of_AddDamageValuePer_85() { return &___AddDamageValuePer_85; }
	inline void set_AddDamageValuePer_85(List_1_t2522024052 * value)
	{
		___AddDamageValuePer_85 = value;
		Il2CppCodeGenWriteBarrier(&___AddDamageValuePer_85, value);
	}

	inline static int32_t get_offset_of_evNpcReduceHp_86() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___evNpcReduceHp_86)); }
	inline ZEvent_t3638018500 * get_evNpcReduceHp_86() const { return ___evNpcReduceHp_86; }
	inline ZEvent_t3638018500 ** get_address_of_evNpcReduceHp_86() { return &___evNpcReduceHp_86; }
	inline void set_evNpcReduceHp_86(ZEvent_t3638018500 * value)
	{
		___evNpcReduceHp_86 = value;
		Il2CppCodeGenWriteBarrier(&___evNpcReduceHp_86, value);
	}

	inline static int32_t get_offset_of_evBekilled_87() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___evBekilled_87)); }
	inline ZEvent_t3638018500 * get_evBekilled_87() const { return ___evBekilled_87; }
	inline ZEvent_t3638018500 ** get_address_of_evBekilled_87() { return &___evBekilled_87; }
	inline void set_evBekilled_87(ZEvent_t3638018500 * value)
	{
		___evBekilled_87 = value;
		Il2CppCodeGenWriteBarrier(&___evBekilled_87, value);
	}

	inline static int32_t get_offset_of_shaderLv_88() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___shaderLv_88)); }
	inline int32_t get_shaderLv_88() const { return ___shaderLv_88; }
	inline int32_t* get_address_of_shaderLv_88() { return &___shaderLv_88; }
	inline void set_shaderLv_88(int32_t value)
	{
		___shaderLv_88 = value;
	}

	inline static int32_t get_offset_of__RimColor_89() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ____RimColor_89)); }
	inline Color_t4194546905  get__RimColor_89() const { return ____RimColor_89; }
	inline Color_t4194546905 * get_address_of__RimColor_89() { return &____RimColor_89; }
	inline void set__RimColor_89(Color_t4194546905  value)
	{
		____RimColor_89 = value;
	}

	inline static int32_t get_offset_of__RimPower_90() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ____RimPower_90)); }
	inline float get__RimPower_90() const { return ____RimPower_90; }
	inline float* get_address_of__RimPower_90() { return &____RimPower_90; }
	inline void set__RimPower_90(float value)
	{
		____RimPower_90 = value;
	}

	inline static int32_t get_offset_of__EmissionP_91() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ____EmissionP_91)); }
	inline float get__EmissionP_91() const { return ____EmissionP_91; }
	inline float* get_address_of__EmissionP_91() { return &____EmissionP_91; }
	inline void set__EmissionP_91(float value)
	{
		____EmissionP_91 = value;
	}

	inline static int32_t get_offset_of_curShaderlv_92() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___curShaderlv_92)); }
	inline int32_t get_curShaderlv_92() const { return ___curShaderlv_92; }
	inline int32_t* get_address_of_curShaderlv_92() { return &___curShaderlv_92; }
	inline void set_curShaderlv_92(int32_t value)
	{
		___curShaderlv_92 = value;
	}

	inline static int32_t get_offset_of_AtkedEffName_93() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___AtkedEffName_93)); }
	inline String_t* get_AtkedEffName_93() const { return ___AtkedEffName_93; }
	inline String_t** get_address_of_AtkedEffName_93() { return &___AtkedEffName_93; }
	inline void set_AtkedEffName_93(String_t* value)
	{
		___AtkedEffName_93 = value;
		Il2CppCodeGenWriteBarrier(&___AtkedEffName_93, value);
	}

	inline static int32_t get_offset_of_IsShowRender_94() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___IsShowRender_94)); }
	inline bool get_IsShowRender_94() const { return ___IsShowRender_94; }
	inline bool* get_address_of_IsShowRender_94() { return &___IsShowRender_94; }
	inline void set_IsShowRender_94(bool value)
	{
		___IsShowRender_94 = value;
	}

	inline static int32_t get_offset_of_dealyBackMoveCameraTime_95() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___dealyBackMoveCameraTime_95)); }
	inline float get_dealyBackMoveCameraTime_95() const { return ___dealyBackMoveCameraTime_95; }
	inline float* get_address_of_dealyBackMoveCameraTime_95() { return &___dealyBackMoveCameraTime_95; }
	inline void set_dealyBackMoveCameraTime_95(float value)
	{
		___dealyBackMoveCameraTime_95 = value;
	}

	inline static int32_t get_offset_of_oneSecAddHPTickTime_96() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___oneSecAddHPTickTime_96)); }
	inline float get_oneSecAddHPTickTime_96() const { return ___oneSecAddHPTickTime_96; }
	inline float* get_address_of_oneSecAddHPTickTime_96() { return &___oneSecAddHPTickTime_96; }
	inline void set_oneSecAddHPTickTime_96(float value)
	{
		___oneSecAddHPTickTime_96 = value;
	}

	inline static int32_t get_offset_of_oneSecReduceHPTickTime_97() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___oneSecReduceHPTickTime_97)); }
	inline float get_oneSecReduceHPTickTime_97() const { return ___oneSecReduceHPTickTime_97; }
	inline float* get_address_of_oneSecReduceHPTickTime_97() { return &___oneSecReduceHPTickTime_97; }
	inline void set_oneSecReduceHPTickTime_97(float value)
	{
		___oneSecReduceHPTickTime_97 = value;
	}

	inline static int32_t get_offset_of_oneSecAddAngerTickTime_98() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___oneSecAddAngerTickTime_98)); }
	inline float get_oneSecAddAngerTickTime_98() const { return ___oneSecAddAngerTickTime_98; }
	inline float* get_address_of_oneSecAddAngerTickTime_98() { return &___oneSecAddAngerTickTime_98; }
	inline void set_oneSecAddAngerTickTime_98(float value)
	{
		___oneSecAddAngerTickTime_98 = value;
	}

	inline static int32_t get_offset_of_oneSecAuraAddHPTime_99() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___oneSecAuraAddHPTime_99)); }
	inline float get_oneSecAuraAddHPTime_99() const { return ___oneSecAuraAddHPTime_99; }
	inline float* get_address_of_oneSecAuraAddHPTime_99() { return &___oneSecAuraAddHPTime_99; }
	inline void set_oneSecAuraAddHPTime_99(float value)
	{
		___oneSecAuraAddHPTime_99 = value;
	}

	inline static int32_t get_offset_of_oneSecAuraReduceHPTime_100() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___oneSecAuraReduceHPTime_100)); }
	inline float get_oneSecAuraReduceHPTime_100() const { return ___oneSecAuraReduceHPTime_100; }
	inline float* get_address_of_oneSecAuraReduceHPTime_100() { return &___oneSecAuraReduceHPTime_100; }
	inline void set_oneSecAuraReduceHPTime_100(float value)
	{
		___oneSecAuraReduceHPTime_100 = value;
	}

	inline static int32_t get_offset_of_radius_101() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___radius_101)); }
	inline float get_radius_101() const { return ___radius_101; }
	inline float* get_address_of_radius_101() { return &___radius_101; }
	inline void set_radius_101(float value)
	{
		___radius_101 = value;
	}

	inline static int32_t get_offset_of_historyPos_102() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___historyPos_102)); }
	inline Vector3_t4282066566  get_historyPos_102() const { return ___historyPos_102; }
	inline Vector3_t4282066566 * get_address_of_historyPos_102() { return &___historyPos_102; }
	inline void set_historyPos_102(Vector3_t4282066566  value)
	{
		___historyPos_102 = value;
	}

	inline static int32_t get_offset_of_bStartMove_103() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___bStartMove_103)); }
	inline bool get_bStartMove_103() const { return ___bStartMove_103; }
	inline bool* get_address_of_bStartMove_103() { return &___bStartMove_103; }
	inline void set_bStartMove_103(bool value)
	{
		___bStartMove_103 = value;
	}

	inline static int32_t get_offset_of__collider_104() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ____collider_104)); }
	inline Collider_t2939674232 * get__collider_104() const { return ____collider_104; }
	inline Collider_t2939674232 ** get_address_of__collider_104() { return &____collider_104; }
	inline void set__collider_104(Collider_t2939674232 * value)
	{
		____collider_104 = value;
		Il2CppCodeGenWriteBarrier(&____collider_104, value);
	}

	inline static int32_t get_offset_of_OnTargetCalls_105() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___OnTargetCalls_105)); }
	inline List_1_t844452154 * get_OnTargetCalls_105() const { return ___OnTargetCalls_105; }
	inline List_1_t844452154 ** get_address_of_OnTargetCalls_105() { return &___OnTargetCalls_105; }
	inline void set_OnTargetCalls_105(List_1_t844452154 * value)
	{
		___OnTargetCalls_105 = value;
		Il2CppCodeGenWriteBarrier(&___OnTargetCalls_105, value);
	}

	inline static int32_t get_offset_of_isDoubleClick_106() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___isDoubleClick_106)); }
	inline bool get_isDoubleClick_106() const { return ___isDoubleClick_106; }
	inline bool* get_address_of_isDoubleClick_106() { return &___isDoubleClick_106; }
	inline void set_isDoubleClick_106(bool value)
	{
		___isDoubleClick_106 = value;
	}

	inline static int32_t get_offset_of__forceShift_107() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ____forceShift_107)); }
	inline bool get__forceShift_107() const { return ____forceShift_107; }
	inline bool* get_address_of__forceShift_107() { return &____forceShift_107; }
	inline void set__forceShift_107(bool value)
	{
		____forceShift_107 = value;
	}

	inline static int32_t get_offset_of_hatredCtrl_108() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___hatredCtrl_108)); }
	inline HatredCtrl_t891253697 * get_hatredCtrl_108() const { return ___hatredCtrl_108; }
	inline HatredCtrl_t891253697 ** get_address_of_hatredCtrl_108() { return &___hatredCtrl_108; }
	inline void set_hatredCtrl_108(HatredCtrl_t891253697 * value)
	{
		___hatredCtrl_108 = value;
		Il2CppCodeGenWriteBarrier(&___hatredCtrl_108, value);
	}

	inline static int32_t get_offset_of_attPlus_109() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___attPlus_109)); }
	inline CombatAttPlus_t649400871 * get_attPlus_109() const { return ___attPlus_109; }
	inline CombatAttPlus_t649400871 ** get_address_of_attPlus_109() { return &___attPlus_109; }
	inline void set_attPlus_109(CombatAttPlus_t649400871 * value)
	{
		___attPlus_109 = value;
		Il2CppCodeGenWriteBarrier(&___attPlus_109, value);
	}

	inline static int32_t get_offset_of_duration_110() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___duration_110)); }
	inline float get_duration_110() const { return ___duration_110; }
	inline float* get_address_of_duration_110() { return &___duration_110; }
	inline void set_duration_110(float value)
	{
		___duration_110 = value;
	}

	inline static int32_t get_offset_of_interval_111() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___interval_111)); }
	inline float get_interval_111() const { return ___interval_111; }
	inline float* get_address_of_interval_111() { return &___interval_111; }
	inline void set_interval_111(float value)
	{
		___interval_111 = value;
	}

	inline static int32_t get_offset_of_Intension_112() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___Intension_112)); }
	inline float get_Intension_112() const { return ___Intension_112; }
	inline float* get_address_of_Intension_112() { return &___Intension_112; }
	inline void set_Intension_112(float value)
	{
		___Intension_112 = value;
	}

	inline static int32_t get_offset_of_meshRender_113() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___meshRender_113)); }
	inline SkinnedMeshRendererU5BU5D_t1155881555* get_meshRender_113() const { return ___meshRender_113; }
	inline SkinnedMeshRendererU5BU5D_t1155881555** get_address_of_meshRender_113() { return &___meshRender_113; }
	inline void set_meshRender_113(SkinnedMeshRendererU5BU5D_t1155881555* value)
	{
		___meshRender_113 = value;
		Il2CppCodeGenWriteBarrier(&___meshRender_113, value);
	}

	inline static int32_t get_offset_of_ghostShader_114() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___ghostShader_114)); }
	inline Shader_t3191267369 * get_ghostShader_114() const { return ___ghostShader_114; }
	inline Shader_t3191267369 ** get_address_of_ghostShader_114() { return &___ghostShader_114; }
	inline void set_ghostShader_114(Shader_t3191267369 * value)
	{
		___ghostShader_114 = value;
		Il2CppCodeGenWriteBarrier(&___ghostShader_114, value);
	}

	inline static int32_t get_offset_of_isShowGhost_115() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___isShowGhost_115)); }
	inline bool get_isShowGhost_115() const { return ___isShowGhost_115; }
	inline bool* get_address_of_isShowGhost_115() { return &___isShowGhost_115; }
	inline void set_isShowGhost_115(bool value)
	{
		___isShowGhost_115 = value;
	}

	inline static int32_t get_offset_of_ghostShowList_116() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___ghostShowList_116)); }
	inline List_1_t56138920 * get_ghostShowList_116() const { return ___ghostShowList_116; }
	inline List_1_t56138920 ** get_address_of_ghostShowList_116() { return &___ghostShowList_116; }
	inline void set_ghostShowList_116(List_1_t56138920 * value)
	{
		___ghostShowList_116 = value;
		Il2CppCodeGenWriteBarrier(&___ghostShowList_116, value);
	}

	inline static int32_t get_offset_of_lastTime_117() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___lastTime_117)); }
	inline float get_lastTime_117() const { return ___lastTime_117; }
	inline float* get_address_of_lastTime_117() { return &___lastTime_117; }
	inline void set_lastTime_117(float value)
	{
		___lastTime_117 = value;
	}

	inline static int32_t get_offset_of_lastPos_118() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___lastPos_118)); }
	inline Vector3_t4282066566  get_lastPos_118() const { return ___lastPos_118; }
	inline Vector3_t4282066566 * get_address_of_lastPos_118() { return &___lastPos_118; }
	inline void set_lastPos_118(Vector3_t4282066566  value)
	{
		___lastPos_118 = value;
	}

	inline static int32_t get_offset_of_susanooList_119() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___susanooList_119)); }
	inline List_1_t56138920 * get_susanooList_119() const { return ___susanooList_119; }
	inline List_1_t56138920 ** get_address_of_susanooList_119() { return &___susanooList_119; }
	inline void set_susanooList_119(List_1_t56138920 * value)
	{
		___susanooList_119 = value;
		Il2CppCodeGenWriteBarrier(&___susanooList_119, value);
	}

	inline static int32_t get_offset_of_susanooScale_120() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___susanooScale_120)); }
	inline float get_susanooScale_120() const { return ___susanooScale_120; }
	inline float* get_address_of_susanooScale_120() { return &___susanooScale_120; }
	inline void set_susanooScale_120(float value)
	{
		___susanooScale_120 = value;
	}

	inline static int32_t get_offset_of_offset_121() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___offset_121)); }
	inline Vector3_t4282066566  get_offset_121() const { return ___offset_121; }
	inline Vector3_t4282066566 * get_address_of_offset_121() { return &___offset_121; }
	inline void set_offset_121(Vector3_t4282066566  value)
	{
		___offset_121 = value;
	}

	inline static int32_t get_offset_of_susanooObj_122() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___susanooObj_122)); }
	inline GameObject_t3674682005 * get_susanooObj_122() const { return ___susanooObj_122; }
	inline GameObject_t3674682005 ** get_address_of_susanooObj_122() { return &___susanooObj_122; }
	inline void set_susanooObj_122(GameObject_t3674682005 * value)
	{
		___susanooObj_122 = value;
		Il2CppCodeGenWriteBarrier(&___susanooObj_122, value);
	}

	inline static int32_t get_offset_of_SusanooShader_123() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___SusanooShader_123)); }
	inline Shader_t3191267369 * get_SusanooShader_123() const { return ___SusanooShader_123; }
	inline Shader_t3191267369 ** get_address_of_SusanooShader_123() { return &___SusanooShader_123; }
	inline void set_SusanooShader_123(Shader_t3191267369 * value)
	{
		___SusanooShader_123 = value;
		Il2CppCodeGenWriteBarrier(&___SusanooShader_123, value);
	}

	inline static int32_t get_offset_of_curBloom_124() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___curBloom_124)); }
	inline BloomEffect_t3352511828 * get_curBloom_124() const { return ___curBloom_124; }
	inline BloomEffect_t3352511828 ** get_address_of_curBloom_124() { return &___curBloom_124; }
	inline void set_curBloom_124(BloomEffect_t3352511828 * value)
	{
		___curBloom_124 = value;
		Il2CppCodeGenWriteBarrier(&___curBloom_124, value);
	}

	inline static int32_t get_offset_of_bloomList_125() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___bloomList_125)); }
	inline List_1_t56138920 * get_bloomList_125() const { return ___bloomList_125; }
	inline List_1_t56138920 ** get_address_of_bloomList_125() { return &___bloomList_125; }
	inline void set_bloomList_125(List_1_t56138920 * value)
	{
		___bloomList_125 = value;
		Il2CppCodeGenWriteBarrier(&___bloomList_125, value);
	}

	inline static int32_t get_offset_of_curIndex_126() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___curIndex_126)); }
	inline int32_t get_curIndex_126() const { return ___curIndex_126; }
	inline int32_t* get_address_of_curIndex_126() { return &___curIndex_126; }
	inline void set_curIndex_126(int32_t value)
	{
		___curIndex_126 = value;
	}

	inline static int32_t get_offset_of_U3CshownameU3Ek__BackingField_127() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CshownameU3Ek__BackingField_127)); }
	inline String_t* get_U3CshownameU3Ek__BackingField_127() const { return ___U3CshownameU3Ek__BackingField_127; }
	inline String_t** get_address_of_U3CshownameU3Ek__BackingField_127() { return &___U3CshownameU3Ek__BackingField_127; }
	inline void set_U3CshownameU3Ek__BackingField_127(String_t* value)
	{
		___U3CshownameU3Ek__BackingField_127 = value;
		Il2CppCodeGenWriteBarrier(&___U3CshownameU3Ek__BackingField_127, value);
	}

	inline static int32_t get_offset_of_U3CunitCampU3Ek__BackingField_128() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CunitCampU3Ek__BackingField_128)); }
	inline int32_t get_U3CunitCampU3Ek__BackingField_128() const { return ___U3CunitCampU3Ek__BackingField_128; }
	inline int32_t* get_address_of_U3CunitCampU3Ek__BackingField_128() { return &___U3CunitCampU3Ek__BackingField_128; }
	inline void set_U3CunitCampU3Ek__BackingField_128(int32_t value)
	{
		___U3CunitCampU3Ek__BackingField_128 = value;
	}

	inline static int32_t get_offset_of_U3CmodelScaleTweenU3Ek__BackingField_129() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CmodelScaleTweenU3Ek__BackingField_129)); }
	inline int32_t get_U3CmodelScaleTweenU3Ek__BackingField_129() const { return ___U3CmodelScaleTweenU3Ek__BackingField_129; }
	inline int32_t* get_address_of_U3CmodelScaleTweenU3Ek__BackingField_129() { return &___U3CmodelScaleTweenU3Ek__BackingField_129; }
	inline void set_U3CmodelScaleTweenU3Ek__BackingField_129(int32_t value)
	{
		___U3CmodelScaleTweenU3Ek__BackingField_129 = value;
	}

	inline static int32_t get_offset_of_U3CpetrifiedTweenU3Ek__BackingField_130() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CpetrifiedTweenU3Ek__BackingField_130)); }
	inline int32_t get_U3CpetrifiedTweenU3Ek__BackingField_130() const { return ___U3CpetrifiedTweenU3Ek__BackingField_130; }
	inline int32_t* get_address_of_U3CpetrifiedTweenU3Ek__BackingField_130() { return &___U3CpetrifiedTweenU3Ek__BackingField_130; }
	inline void set_U3CpetrifiedTweenU3Ek__BackingField_130(int32_t value)
	{
		___U3CpetrifiedTweenU3Ek__BackingField_130 = value;
	}

	inline static int32_t get_offset_of_U3CplayerIdU3Ek__BackingField_131() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CplayerIdU3Ek__BackingField_131)); }
	inline uint32_t get_U3CplayerIdU3Ek__BackingField_131() const { return ___U3CplayerIdU3Ek__BackingField_131; }
	inline uint32_t* get_address_of_U3CplayerIdU3Ek__BackingField_131() { return &___U3CplayerIdU3Ek__BackingField_131; }
	inline void set_U3CplayerIdU3Ek__BackingField_131(uint32_t value)
	{
		___U3CplayerIdU3Ek__BackingField_131 = value;
	}

	inline static int32_t get_offset_of_U3CidU3Ek__BackingField_132() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CidU3Ek__BackingField_132)); }
	inline int32_t get_U3CidU3Ek__BackingField_132() const { return ___U3CidU3Ek__BackingField_132; }
	inline int32_t* get_address_of_U3CidU3Ek__BackingField_132() { return &___U3CidU3Ek__BackingField_132; }
	inline void set_U3CidU3Ek__BackingField_132(int32_t value)
	{
		___U3CidU3Ek__BackingField_132 = value;
	}

	inline static int32_t get_offset_of_U3CiconU3Ek__BackingField_133() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CiconU3Ek__BackingField_133)); }
	inline String_t* get_U3CiconU3Ek__BackingField_133() const { return ___U3CiconU3Ek__BackingField_133; }
	inline String_t** get_address_of_U3CiconU3Ek__BackingField_133() { return &___U3CiconU3Ek__BackingField_133; }
	inline void set_U3CiconU3Ek__BackingField_133(String_t* value)
	{
		___U3CiconU3Ek__BackingField_133 = value;
		Il2CppCodeGenWriteBarrier(&___U3CiconU3Ek__BackingField_133, value);
	}

	inline static int32_t get_offset_of_U3ClargeIconU3Ek__BackingField_134() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3ClargeIconU3Ek__BackingField_134)); }
	inline String_t* get_U3ClargeIconU3Ek__BackingField_134() const { return ___U3ClargeIconU3Ek__BackingField_134; }
	inline String_t** get_address_of_U3ClargeIconU3Ek__BackingField_134() { return &___U3ClargeIconU3Ek__BackingField_134; }
	inline void set_U3ClargeIconU3Ek__BackingField_134(String_t* value)
	{
		___U3ClargeIconU3Ek__BackingField_134 = value;
		Il2CppCodeGenWriteBarrier(&___U3ClargeIconU3Ek__BackingField_134, value);
	}

	inline static int32_t get_offset_of_U3CbossshowU3Ek__BackingField_135() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CbossshowU3Ek__BackingField_135)); }
	inline String_t* get_U3CbossshowU3Ek__BackingField_135() const { return ___U3CbossshowU3Ek__BackingField_135; }
	inline String_t** get_address_of_U3CbossshowU3Ek__BackingField_135() { return &___U3CbossshowU3Ek__BackingField_135; }
	inline void set_U3CbossshowU3Ek__BackingField_135(String_t* value)
	{
		___U3CbossshowU3Ek__BackingField_135 = value;
		Il2CppCodeGenWriteBarrier(&___U3CbossshowU3Ek__BackingField_135, value);
	}

	inline static int32_t get_offset_of_U3CheadiconU3Ek__BackingField_136() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CheadiconU3Ek__BackingField_136)); }
	inline String_t* get_U3CheadiconU3Ek__BackingField_136() const { return ___U3CheadiconU3Ek__BackingField_136; }
	inline String_t** get_address_of_U3CheadiconU3Ek__BackingField_136() { return &___U3CheadiconU3Ek__BackingField_136; }
	inline void set_U3CheadiconU3Ek__BackingField_136(String_t* value)
	{
		___U3CheadiconU3Ek__BackingField_136 = value;
		Il2CppCodeGenWriteBarrier(&___U3CheadiconU3Ek__BackingField_136, value);
	}

	inline static int32_t get_offset_of_U3CIntelligenceTipU3Ek__BackingField_137() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CIntelligenceTipU3Ek__BackingField_137)); }
	inline int32_t get_U3CIntelligenceTipU3Ek__BackingField_137() const { return ___U3CIntelligenceTipU3Ek__BackingField_137; }
	inline int32_t* get_address_of_U3CIntelligenceTipU3Ek__BackingField_137() { return &___U3CIntelligenceTipU3Ek__BackingField_137; }
	inline void set_U3CIntelligenceTipU3Ek__BackingField_137(int32_t value)
	{
		___U3CIntelligenceTipU3Ek__BackingField_137 = value;
	}

	inline static int32_t get_offset_of_U3CscaleU3Ek__BackingField_138() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CscaleU3Ek__BackingField_138)); }
	inline float get_U3CscaleU3Ek__BackingField_138() const { return ___U3CscaleU3Ek__BackingField_138; }
	inline float* get_address_of_U3CscaleU3Ek__BackingField_138() { return &___U3CscaleU3Ek__BackingField_138; }
	inline void set_U3CscaleU3Ek__BackingField_138(float value)
	{
		___U3CscaleU3Ek__BackingField_138 = value;
	}

	inline static int32_t get_offset_of_U3CisSummonMonsterU3Ek__BackingField_139() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CisSummonMonsterU3Ek__BackingField_139)); }
	inline bool get_U3CisSummonMonsterU3Ek__BackingField_139() const { return ___U3CisSummonMonsterU3Ek__BackingField_139; }
	inline bool* get_address_of_U3CisSummonMonsterU3Ek__BackingField_139() { return &___U3CisSummonMonsterU3Ek__BackingField_139; }
	inline void set_U3CisSummonMonsterU3Ek__BackingField_139(bool value)
	{
		___U3CisSummonMonsterU3Ek__BackingField_139 = value;
	}

	inline static int32_t get_offset_of_U3CheromajorU3Ek__BackingField_140() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CheromajorU3Ek__BackingField_140)); }
	inline int32_t get_U3CheromajorU3Ek__BackingField_140() const { return ___U3CheromajorU3Ek__BackingField_140; }
	inline int32_t* get_address_of_U3CheromajorU3Ek__BackingField_140() { return &___U3CheromajorU3Ek__BackingField_140; }
	inline void set_U3CheromajorU3Ek__BackingField_140(int32_t value)
	{
		___U3CheromajorU3Ek__BackingField_140 = value;
	}

	inline static int32_t get_offset_of_U3CatkEffDelayTimeU3Ek__BackingField_141() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CatkEffDelayTimeU3Ek__BackingField_141)); }
	inline float get_U3CatkEffDelayTimeU3Ek__BackingField_141() const { return ___U3CatkEffDelayTimeU3Ek__BackingField_141; }
	inline float* get_address_of_U3CatkEffDelayTimeU3Ek__BackingField_141() { return &___U3CatkEffDelayTimeU3Ek__BackingField_141; }
	inline void set_U3CatkEffDelayTimeU3Ek__BackingField_141(float value)
	{
		___U3CatkEffDelayTimeU3Ek__BackingField_141 = value;
	}

	inline static int32_t get_offset_of_U3CskillsU3Ek__BackingField_142() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CskillsU3Ek__BackingField_142)); }
	inline String_t* get_U3CskillsU3Ek__BackingField_142() const { return ___U3CskillsU3Ek__BackingField_142; }
	inline String_t** get_address_of_U3CskillsU3Ek__BackingField_142() { return &___U3CskillsU3Ek__BackingField_142; }
	inline void set_U3CskillsU3Ek__BackingField_142(String_t* value)
	{
		___U3CskillsU3Ek__BackingField_142 = value;
		Il2CppCodeGenWriteBarrier(&___U3CskillsU3Ek__BackingField_142, value);
	}

	inline static int32_t get_offset_of_U3CawakenLvU3Ek__BackingField_143() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CawakenLvU3Ek__BackingField_143)); }
	inline int32_t get_U3CawakenLvU3Ek__BackingField_143() const { return ___U3CawakenLvU3Ek__BackingField_143; }
	inline int32_t* get_address_of_U3CawakenLvU3Ek__BackingField_143() { return &___U3CawakenLvU3Ek__BackingField_143; }
	inline void set_U3CawakenLvU3Ek__BackingField_143(int32_t value)
	{
		___U3CawakenLvU3Ek__BackingField_143 = value;
	}

	inline static int32_t get_offset_of_U3ClucencyWaitU3Ek__BackingField_144() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3ClucencyWaitU3Ek__BackingField_144)); }
	inline float get_U3ClucencyWaitU3Ek__BackingField_144() const { return ___U3ClucencyWaitU3Ek__BackingField_144; }
	inline float* get_address_of_U3ClucencyWaitU3Ek__BackingField_144() { return &___U3ClucencyWaitU3Ek__BackingField_144; }
	inline void set_U3ClucencyWaitU3Ek__BackingField_144(float value)
	{
		___U3ClucencyWaitU3Ek__BackingField_144 = value;
	}

	inline static int32_t get_offset_of_U3CislucencyU3Ek__BackingField_145() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CislucencyU3Ek__BackingField_145)); }
	inline int32_t get_U3CislucencyU3Ek__BackingField_145() const { return ___U3CislucencyU3Ek__BackingField_145; }
	inline int32_t* get_address_of_U3CislucencyU3Ek__BackingField_145() { return &___U3CislucencyU3Ek__BackingField_145; }
	inline void set_U3CislucencyU3Ek__BackingField_145(int32_t value)
	{
		___U3CislucencyU3Ek__BackingField_145 = value;
	}

	inline static int32_t get_offset_of_U3ClucencytimeU3Ek__BackingField_146() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3ClucencytimeU3Ek__BackingField_146)); }
	inline float get_U3ClucencytimeU3Ek__BackingField_146() const { return ___U3ClucencytimeU3Ek__BackingField_146; }
	inline float* get_address_of_U3ClucencytimeU3Ek__BackingField_146() { return &___U3ClucencytimeU3Ek__BackingField_146; }
	inline void set_U3ClucencytimeU3Ek__BackingField_146(float value)
	{
		___U3ClucencytimeU3Ek__BackingField_146 = value;
	}

	inline static int32_t get_offset_of_U3CeliteU3Ek__BackingField_147() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CeliteU3Ek__BackingField_147)); }
	inline bool get_U3CeliteU3Ek__BackingField_147() const { return ___U3CeliteU3Ek__BackingField_147; }
	inline bool* get_address_of_U3CeliteU3Ek__BackingField_147() { return &___U3CeliteU3Ek__BackingField_147; }
	inline void set_U3CeliteU3Ek__BackingField_147(bool value)
	{
		___U3CeliteU3Ek__BackingField_147 = value;
	}

	inline static int32_t get_offset_of_U3CisAimU3Ek__BackingField_148() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CisAimU3Ek__BackingField_148)); }
	inline bool get_U3CisAimU3Ek__BackingField_148() const { return ___U3CisAimU3Ek__BackingField_148; }
	inline bool* get_address_of_U3CisAimU3Ek__BackingField_148() { return &___U3CisAimU3Ek__BackingField_148; }
	inline void set_U3CisAimU3Ek__BackingField_148(bool value)
	{
		___U3CisAimU3Ek__BackingField_148 = value;
	}

	inline static int32_t get_offset_of_U3CdutytypeU3Ek__BackingField_149() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CdutytypeU3Ek__BackingField_149)); }
	inline int32_t get_U3CdutytypeU3Ek__BackingField_149() const { return ___U3CdutytypeU3Ek__BackingField_149; }
	inline int32_t* get_address_of_U3CdutytypeU3Ek__BackingField_149() { return &___U3CdutytypeU3Ek__BackingField_149; }
	inline void set_U3CdutytypeU3Ek__BackingField_149(int32_t value)
	{
		___U3CdutytypeU3Ek__BackingField_149 = value;
	}

	inline static int32_t get_offset_of_U3CifattackedU3Ek__BackingField_150() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CifattackedU3Ek__BackingField_150)); }
	inline int32_t get_U3CifattackedU3Ek__BackingField_150() const { return ___U3CifattackedU3Ek__BackingField_150; }
	inline int32_t* get_address_of_U3CifattackedU3Ek__BackingField_150() { return &___U3CifattackedU3Ek__BackingField_150; }
	inline void set_U3CifattackedU3Ek__BackingField_150(int32_t value)
	{
		___U3CifattackedU3Ek__BackingField_150 = value;
	}

	inline static int32_t get_offset_of_U3CdropRwardsU3Ek__BackingField_151() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CdropRwardsU3Ek__BackingField_151)); }
	inline List_1_t2836773265 * get_U3CdropRwardsU3Ek__BackingField_151() const { return ___U3CdropRwardsU3Ek__BackingField_151; }
	inline List_1_t2836773265 ** get_address_of_U3CdropRwardsU3Ek__BackingField_151() { return &___U3CdropRwardsU3Ek__BackingField_151; }
	inline void set_U3CdropRwardsU3Ek__BackingField_151(List_1_t2836773265 * value)
	{
		___U3CdropRwardsU3Ek__BackingField_151 = value;
		Il2CppCodeGenWriteBarrier(&___U3CdropRwardsU3Ek__BackingField_151, value);
	}

	inline static int32_t get_offset_of_U3CstoryIdU3Ek__BackingField_152() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CstoryIdU3Ek__BackingField_152)); }
	inline String_t* get_U3CstoryIdU3Ek__BackingField_152() const { return ___U3CstoryIdU3Ek__BackingField_152; }
	inline String_t** get_address_of_U3CstoryIdU3Ek__BackingField_152() { return &___U3CstoryIdU3Ek__BackingField_152; }
	inline void set_U3CstoryIdU3Ek__BackingField_152(String_t* value)
	{
		___U3CstoryIdU3Ek__BackingField_152 = value;
		Il2CppCodeGenWriteBarrier(&___U3CstoryIdU3Ek__BackingField_152, value);
	}

	inline static int32_t get_offset_of_U3CelementEffU3Ek__BackingField_153() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CelementEffU3Ek__BackingField_153)); }
	inline int32_t get_U3CelementEffU3Ek__BackingField_153() const { return ___U3CelementEffU3Ek__BackingField_153; }
	inline int32_t* get_address_of_U3CelementEffU3Ek__BackingField_153() { return &___U3CelementEffU3Ek__BackingField_153; }
	inline void set_U3CelementEffU3Ek__BackingField_153(int32_t value)
	{
		___U3CelementEffU3Ek__BackingField_153 = value;
	}

	inline static int32_t get_offset_of_U3CatkedEffU3Ek__BackingField_154() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CatkedEffU3Ek__BackingField_154)); }
	inline int32_t get_U3CatkedEffU3Ek__BackingField_154() const { return ___U3CatkedEffU3Ek__BackingField_154; }
	inline int32_t* get_address_of_U3CatkedEffU3Ek__BackingField_154() { return &___U3CatkedEffU3Ek__BackingField_154; }
	inline void set_U3CatkedEffU3Ek__BackingField_154(int32_t value)
	{
		___U3CatkedEffU3Ek__BackingField_154 = value;
	}

	inline static int32_t get_offset_of_U3CdeathEffectU3Ek__BackingField_155() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CdeathEffectU3Ek__BackingField_155)); }
	inline int32_t get_U3CdeathEffectU3Ek__BackingField_155() const { return ___U3CdeathEffectU3Ek__BackingField_155; }
	inline int32_t* get_address_of_U3CdeathEffectU3Ek__BackingField_155() { return &___U3CdeathEffectU3Ek__BackingField_155; }
	inline void set_U3CdeathEffectU3Ek__BackingField_155(int32_t value)
	{
		___U3CdeathEffectU3Ek__BackingField_155 = value;
	}

	inline static int32_t get_offset_of_U3CtaixudeatheffectU3Ek__BackingField_156() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CtaixudeatheffectU3Ek__BackingField_156)); }
	inline int32_t get_U3CtaixudeatheffectU3Ek__BackingField_156() const { return ___U3CtaixudeatheffectU3Ek__BackingField_156; }
	inline int32_t* get_address_of_U3CtaixudeatheffectU3Ek__BackingField_156() { return &___U3CtaixudeatheffectU3Ek__BackingField_156; }
	inline void set_U3CtaixudeatheffectU3Ek__BackingField_156(int32_t value)
	{
		___U3CtaixudeatheffectU3Ek__BackingField_156 = value;
	}

	inline static int32_t get_offset_of_U3CreviveEffectIdU3Ek__BackingField_157() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CreviveEffectIdU3Ek__BackingField_157)); }
	inline int32_t get_U3CreviveEffectIdU3Ek__BackingField_157() const { return ___U3CreviveEffectIdU3Ek__BackingField_157; }
	inline int32_t* get_address_of_U3CreviveEffectIdU3Ek__BackingField_157() { return &___U3CreviveEffectIdU3Ek__BackingField_157; }
	inline void set_U3CreviveEffectIdU3Ek__BackingField_157(int32_t value)
	{
		___U3CreviveEffectIdU3Ek__BackingField_157 = value;
	}

	inline static int32_t get_offset_of_U3CappeareffectU3Ek__BackingField_158() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CappeareffectU3Ek__BackingField_158)); }
	inline int32_t get_U3CappeareffectU3Ek__BackingField_158() const { return ___U3CappeareffectU3Ek__BackingField_158; }
	inline int32_t* get_address_of_U3CappeareffectU3Ek__BackingField_158() { return &___U3CappeareffectU3Ek__BackingField_158; }
	inline void set_U3CappeareffectU3Ek__BackingField_158(int32_t value)
	{
		___U3CappeareffectU3Ek__BackingField_158 = value;
	}

	inline static int32_t get_offset_of_U3CstartPosU3Ek__BackingField_159() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CstartPosU3Ek__BackingField_159)); }
	inline Vector3_t4282066566  get_U3CstartPosU3Ek__BackingField_159() const { return ___U3CstartPosU3Ek__BackingField_159; }
	inline Vector3_t4282066566 * get_address_of_U3CstartPosU3Ek__BackingField_159() { return &___U3CstartPosU3Ek__BackingField_159; }
	inline void set_U3CstartPosU3Ek__BackingField_159(Vector3_t4282066566  value)
	{
		___U3CstartPosU3Ek__BackingField_159 = value;
	}

	inline static int32_t get_offset_of_U3CstartRotationU3Ek__BackingField_160() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CstartRotationU3Ek__BackingField_160)); }
	inline Quaternion_t1553702882  get_U3CstartRotationU3Ek__BackingField_160() const { return ___U3CstartRotationU3Ek__BackingField_160; }
	inline Quaternion_t1553702882 * get_address_of_U3CstartRotationU3Ek__BackingField_160() { return &___U3CstartRotationU3Ek__BackingField_160; }
	inline void set_U3CstartRotationU3Ek__BackingField_160(Quaternion_t1553702882  value)
	{
		___U3CstartRotationU3Ek__BackingField_160 = value;
	}

	inline static int32_t get_offset_of_U3CatkDisU3Ek__BackingField_161() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CatkDisU3Ek__BackingField_161)); }
	inline float get_U3CatkDisU3Ek__BackingField_161() const { return ___U3CatkDisU3Ek__BackingField_161; }
	inline float* get_address_of_U3CatkDisU3Ek__BackingField_161() { return &___U3CatkDisU3Ek__BackingField_161; }
	inline void set_U3CatkDisU3Ek__BackingField_161(float value)
	{
		___U3CatkDisU3Ek__BackingField_161 = value;
	}

	inline static int32_t get_offset_of_U3CviewDisU3Ek__BackingField_162() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CviewDisU3Ek__BackingField_162)); }
	inline float get_U3CviewDisU3Ek__BackingField_162() const { return ___U3CviewDisU3Ek__BackingField_162; }
	inline float* get_address_of_U3CviewDisU3Ek__BackingField_162() { return &___U3CviewDisU3Ek__BackingField_162; }
	inline void set_U3CviewDisU3Ek__BackingField_162(float value)
	{
		___U3CviewDisU3Ek__BackingField_162 = value;
	}

	inline static int32_t get_offset_of_U3CoriginSpeedU3Ek__BackingField_163() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CoriginSpeedU3Ek__BackingField_163)); }
	inline float get_U3CoriginSpeedU3Ek__BackingField_163() const { return ___U3CoriginSpeedU3Ek__BackingField_163; }
	inline float* get_address_of_U3CoriginSpeedU3Ek__BackingField_163() { return &___U3CoriginSpeedU3Ek__BackingField_163; }
	inline void set_U3CoriginSpeedU3Ek__BackingField_163(float value)
	{
		___U3CoriginSpeedU3Ek__BackingField_163 = value;
	}

	inline static int32_t get_offset_of_U3CcanAttackU3Ek__BackingField_164() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CcanAttackU3Ek__BackingField_164)); }
	inline bool get_U3CcanAttackU3Ek__BackingField_164() const { return ___U3CcanAttackU3Ek__BackingField_164; }
	inline bool* get_address_of_U3CcanAttackU3Ek__BackingField_164() { return &___U3CcanAttackU3Ek__BackingField_164; }
	inline void set_U3CcanAttackU3Ek__BackingField_164(bool value)
	{
		___U3CcanAttackU3Ek__BackingField_164 = value;
	}

	inline static int32_t get_offset_of_U3CisStunU3Ek__BackingField_165() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CisStunU3Ek__BackingField_165)); }
	inline bool get_U3CisStunU3Ek__BackingField_165() const { return ___U3CisStunU3Ek__BackingField_165; }
	inline bool* get_address_of_U3CisStunU3Ek__BackingField_165() { return &___U3CisStunU3Ek__BackingField_165; }
	inline void set_U3CisStunU3Ek__BackingField_165(bool value)
	{
		___U3CisStunU3Ek__BackingField_165 = value;
	}

	inline static int32_t get_offset_of_U3CisDeathU3Ek__BackingField_166() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CisDeathU3Ek__BackingField_166)); }
	inline bool get_U3CisDeathU3Ek__BackingField_166() const { return ___U3CisDeathU3Ek__BackingField_166; }
	inline bool* get_address_of_U3CisDeathU3Ek__BackingField_166() { return &___U3CisDeathU3Ek__BackingField_166; }
	inline void set_U3CisDeathU3Ek__BackingField_166(bool value)
	{
		___U3CisDeathU3Ek__BackingField_166 = value;
	}

	inline static int32_t get_offset_of_U3CisReviveU3Ek__BackingField_167() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CisReviveU3Ek__BackingField_167)); }
	inline bool get_U3CisReviveU3Ek__BackingField_167() const { return ___U3CisReviveU3Ek__BackingField_167; }
	inline bool* get_address_of_U3CisReviveU3Ek__BackingField_167() { return &___U3CisReviveU3Ek__BackingField_167; }
	inline void set_U3CisReviveU3Ek__BackingField_167(bool value)
	{
		___U3CisReviveU3Ek__BackingField_167 = value;
	}

	inline static int32_t get_offset_of_U3CisFendU3Ek__BackingField_168() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CisFendU3Ek__BackingField_168)); }
	inline bool get_U3CisFendU3Ek__BackingField_168() const { return ___U3CisFendU3Ek__BackingField_168; }
	inline bool* get_address_of_U3CisFendU3Ek__BackingField_168() { return &___U3CisFendU3Ek__BackingField_168; }
	inline void set_U3CisFendU3Ek__BackingField_168(bool value)
	{
		___U3CisFendU3Ek__BackingField_168 = value;
	}

	inline static int32_t get_offset_of_U3CisKnockingUpU3Ek__BackingField_169() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CisKnockingUpU3Ek__BackingField_169)); }
	inline bool get_U3CisKnockingUpU3Ek__BackingField_169() const { return ___U3CisKnockingUpU3Ek__BackingField_169; }
	inline bool* get_address_of_U3CisKnockingUpU3Ek__BackingField_169() { return &___U3CisKnockingUpU3Ek__BackingField_169; }
	inline void set_U3CisKnockingUpU3Ek__BackingField_169(bool value)
	{
		___U3CisKnockingUpU3Ek__BackingField_169 = value;
	}

	inline static int32_t get_offset_of_U3CisKnockingDownU3Ek__BackingField_170() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CisKnockingDownU3Ek__BackingField_170)); }
	inline bool get_U3CisKnockingDownU3Ek__BackingField_170() const { return ___U3CisKnockingDownU3Ek__BackingField_170; }
	inline bool* get_address_of_U3CisKnockingDownU3Ek__BackingField_170() { return &___U3CisKnockingDownU3Ek__BackingField_170; }
	inline void set_U3CisKnockingDownU3Ek__BackingField_170(bool value)
	{
		___U3CisKnockingDownU3Ek__BackingField_170 = value;
	}

	inline static int32_t get_offset_of_U3CisMovementU3Ek__BackingField_171() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CisMovementU3Ek__BackingField_171)); }
	inline bool get_U3CisMovementU3Ek__BackingField_171() const { return ___U3CisMovementU3Ek__BackingField_171; }
	inline bool* get_address_of_U3CisMovementU3Ek__BackingField_171() { return &___U3CisMovementU3Ek__BackingField_171; }
	inline void set_U3CisMovementU3Ek__BackingField_171(bool value)
	{
		___U3CisMovementU3Ek__BackingField_171 = value;
	}

	inline static int32_t get_offset_of_U3CisFlameU3Ek__BackingField_172() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CisFlameU3Ek__BackingField_172)); }
	inline bool get_U3CisFlameU3Ek__BackingField_172() const { return ___U3CisFlameU3Ek__BackingField_172; }
	inline bool* get_address_of_U3CisFlameU3Ek__BackingField_172() { return &___U3CisFlameU3Ek__BackingField_172; }
	inline void set_U3CisFlameU3Ek__BackingField_172(bool value)
	{
		___U3CisFlameU3Ek__BackingField_172 = value;
	}

	inline static int32_t get_offset_of_U3CisNotSelectedU3Ek__BackingField_173() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CisNotSelectedU3Ek__BackingField_173)); }
	inline bool get_U3CisNotSelectedU3Ek__BackingField_173() const { return ___U3CisNotSelectedU3Ek__BackingField_173; }
	inline bool* get_address_of_U3CisNotSelectedU3Ek__BackingField_173() { return &___U3CisNotSelectedU3Ek__BackingField_173; }
	inline void set_U3CisNotSelectedU3Ek__BackingField_173(bool value)
	{
		___U3CisNotSelectedU3Ek__BackingField_173 = value;
	}

	inline static int32_t get_offset_of_U3CIsCheckTargetU3Ek__BackingField_174() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CIsCheckTargetU3Ek__BackingField_174)); }
	inline bool get_U3CIsCheckTargetU3Ek__BackingField_174() const { return ___U3CIsCheckTargetU3Ek__BackingField_174; }
	inline bool* get_address_of_U3CIsCheckTargetU3Ek__BackingField_174() { return &___U3CIsCheckTargetU3Ek__BackingField_174; }
	inline void set_U3CIsCheckTargetU3Ek__BackingField_174(bool value)
	{
		___U3CIsCheckTargetU3Ek__BackingField_174 = value;
	}

	inline static int32_t get_offset_of_U3CIsAutoMoveU3Ek__BackingField_175() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CIsAutoMoveU3Ek__BackingField_175)); }
	inline bool get_U3CIsAutoMoveU3Ek__BackingField_175() const { return ___U3CIsAutoMoveU3Ek__BackingField_175; }
	inline bool* get_address_of_U3CIsAutoMoveU3Ek__BackingField_175() { return &___U3CIsAutoMoveU3Ek__BackingField_175; }
	inline void set_U3CIsAutoMoveU3Ek__BackingField_175(bool value)
	{
		___U3CIsAutoMoveU3Ek__BackingField_175 = value;
	}

	inline static int32_t get_offset_of_U3CIsMarshalMovingU3Ek__BackingField_176() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CIsMarshalMovingU3Ek__BackingField_176)); }
	inline bool get_U3CIsMarshalMovingU3Ek__BackingField_176() const { return ___U3CIsMarshalMovingU3Ek__BackingField_176; }
	inline bool* get_address_of_U3CIsMarshalMovingU3Ek__BackingField_176() { return &___U3CIsMarshalMovingU3Ek__BackingField_176; }
	inline void set_U3CIsMarshalMovingU3Ek__BackingField_176(bool value)
	{
		___U3CIsMarshalMovingU3Ek__BackingField_176 = value;
	}

	inline static int32_t get_offset_of_U3CRvoRadiusU3Ek__BackingField_177() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CRvoRadiusU3Ek__BackingField_177)); }
	inline float get_U3CRvoRadiusU3Ek__BackingField_177() const { return ___U3CRvoRadiusU3Ek__BackingField_177; }
	inline float* get_address_of_U3CRvoRadiusU3Ek__BackingField_177() { return &___U3CRvoRadiusU3Ek__BackingField_177; }
	inline void set_U3CRvoRadiusU3Ek__BackingField_177(float value)
	{
		___U3CRvoRadiusU3Ek__BackingField_177 = value;
	}

	inline static int32_t get_offset_of_U3CIsBirthEffectU3Ek__BackingField_178() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CIsBirthEffectU3Ek__BackingField_178)); }
	inline bool get_U3CIsBirthEffectU3Ek__BackingField_178() const { return ___U3CIsBirthEffectU3Ek__BackingField_178; }
	inline bool* get_address_of_U3CIsBirthEffectU3Ek__BackingField_178() { return &___U3CIsBirthEffectU3Ek__BackingField_178; }
	inline void set_U3CIsBirthEffectU3Ek__BackingField_178(bool value)
	{
		___U3CIsBirthEffectU3Ek__BackingField_178 = value;
	}

	inline static int32_t get_offset_of_U3CstarAttackTimeU3Ek__BackingField_179() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CstarAttackTimeU3Ek__BackingField_179)); }
	inline float get_U3CstarAttackTimeU3Ek__BackingField_179() const { return ___U3CstarAttackTimeU3Ek__BackingField_179; }
	inline float* get_address_of_U3CstarAttackTimeU3Ek__BackingField_179() { return &___U3CstarAttackTimeU3Ek__BackingField_179; }
	inline void set_U3CstarAttackTimeU3Ek__BackingField_179(float value)
	{
		___U3CstarAttackTimeU3Ek__BackingField_179 = value;
	}

	inline static int32_t get_offset_of_U3CisAttackU3Ek__BackingField_180() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CisAttackU3Ek__BackingField_180)); }
	inline bool get_U3CisAttackU3Ek__BackingField_180() const { return ___U3CisAttackU3Ek__BackingField_180; }
	inline bool* get_address_of_U3CisAttackU3Ek__BackingField_180() { return &___U3CisAttackU3Ek__BackingField_180; }
	inline void set_U3CisAttackU3Ek__BackingField_180(bool value)
	{
		___U3CisAttackU3Ek__BackingField_180 = value;
	}

	inline static int32_t get_offset_of_U3CcanFightU3Ek__BackingField_181() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CcanFightU3Ek__BackingField_181)); }
	inline bool get_U3CcanFightU3Ek__BackingField_181() const { return ___U3CcanFightU3Ek__BackingField_181; }
	inline bool* get_address_of_U3CcanFightU3Ek__BackingField_181() { return &___U3CcanFightU3Ek__BackingField_181; }
	inline void set_U3CcanFightU3Ek__BackingField_181(bool value)
	{
		___U3CcanFightU3Ek__BackingField_181 = value;
	}

	inline static int32_t get_offset_of_U3CwaistPointU3Ek__BackingField_182() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CwaistPointU3Ek__BackingField_182)); }
	inline GameObject_t3674682005 * get_U3CwaistPointU3Ek__BackingField_182() const { return ___U3CwaistPointU3Ek__BackingField_182; }
	inline GameObject_t3674682005 ** get_address_of_U3CwaistPointU3Ek__BackingField_182() { return &___U3CwaistPointU3Ek__BackingField_182; }
	inline void set_U3CwaistPointU3Ek__BackingField_182(GameObject_t3674682005 * value)
	{
		___U3CwaistPointU3Ek__BackingField_182 = value;
		Il2CppCodeGenWriteBarrier(&___U3CwaistPointU3Ek__BackingField_182, value);
	}

	inline static int32_t get_offset_of_U3CInitPositionU3Ek__BackingField_183() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CInitPositionU3Ek__BackingField_183)); }
	inline Vector3_t4282066566  get_U3CInitPositionU3Ek__BackingField_183() const { return ___U3CInitPositionU3Ek__BackingField_183; }
	inline Vector3_t4282066566 * get_address_of_U3CInitPositionU3Ek__BackingField_183() { return &___U3CInitPositionU3Ek__BackingField_183; }
	inline void set_U3CInitPositionU3Ek__BackingField_183(Vector3_t4282066566  value)
	{
		___U3CInitPositionU3Ek__BackingField_183 = value;
	}

	inline static int32_t get_offset_of_U3CUINamePosU3Ek__BackingField_184() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CUINamePosU3Ek__BackingField_184)); }
	inline int32_t get_U3CUINamePosU3Ek__BackingField_184() const { return ___U3CUINamePosU3Ek__BackingField_184; }
	inline int32_t* get_address_of_U3CUINamePosU3Ek__BackingField_184() { return &___U3CUINamePosU3Ek__BackingField_184; }
	inline void set_U3CUINamePosU3Ek__BackingField_184(int32_t value)
	{
		___U3CUINamePosU3Ek__BackingField_184 = value;
	}

	inline static int32_t get_offset_of_U3CheadBloodPosU3Ek__BackingField_185() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CheadBloodPosU3Ek__BackingField_185)); }
	inline float get_U3CheadBloodPosU3Ek__BackingField_185() const { return ___U3CheadBloodPosU3Ek__BackingField_185; }
	inline float* get_address_of_U3CheadBloodPosU3Ek__BackingField_185() { return &___U3CheadBloodPosU3Ek__BackingField_185; }
	inline void set_U3CheadBloodPosU3Ek__BackingField_185(float value)
	{
		___U3CheadBloodPosU3Ek__BackingField_185 = value;
	}

	inline static int32_t get_offset_of_U3CsexTypeU3Ek__BackingField_186() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CsexTypeU3Ek__BackingField_186)); }
	inline int32_t get_U3CsexTypeU3Ek__BackingField_186() const { return ___U3CsexTypeU3Ek__BackingField_186; }
	inline int32_t* get_address_of_U3CsexTypeU3Ek__BackingField_186() { return &___U3CsexTypeU3Ek__BackingField_186; }
	inline void set_U3CsexTypeU3Ek__BackingField_186(int32_t value)
	{
		___U3CsexTypeU3Ek__BackingField_186 = value;
	}

	inline static int32_t get_offset_of_U3CAIDelayListU3Ek__BackingField_187() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CAIDelayListU3Ek__BackingField_187)); }
	inline List_1_t1405465687 * get_U3CAIDelayListU3Ek__BackingField_187() const { return ___U3CAIDelayListU3Ek__BackingField_187; }
	inline List_1_t1405465687 ** get_address_of_U3CAIDelayListU3Ek__BackingField_187() { return &___U3CAIDelayListU3Ek__BackingField_187; }
	inline void set_U3CAIDelayListU3Ek__BackingField_187(List_1_t1405465687 * value)
	{
		___U3CAIDelayListU3Ek__BackingField_187 = value;
		Il2CppCodeGenWriteBarrier(&___U3CAIDelayListU3Ek__BackingField_187, value);
	}

	inline static int32_t get_offset_of_U3CinitBehaviorU3Ek__BackingField_188() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CinitBehaviorU3Ek__BackingField_188)); }
	inline int32_t get_U3CinitBehaviorU3Ek__BackingField_188() const { return ___U3CinitBehaviorU3Ek__BackingField_188; }
	inline int32_t* get_address_of_U3CinitBehaviorU3Ek__BackingField_188() { return &___U3CinitBehaviorU3Ek__BackingField_188; }
	inline void set_U3CinitBehaviorU3Ek__BackingField_188(int32_t value)
	{
		___U3CinitBehaviorU3Ek__BackingField_188 = value;
	}

	inline static int32_t get_offset_of_U3CcharacterAnimU3Ek__BackingField_189() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CcharacterAnimU3Ek__BackingField_189)); }
	inline AnimationRunner_t1015409588 * get_U3CcharacterAnimU3Ek__BackingField_189() const { return ___U3CcharacterAnimU3Ek__BackingField_189; }
	inline AnimationRunner_t1015409588 ** get_address_of_U3CcharacterAnimU3Ek__BackingField_189() { return &___U3CcharacterAnimU3Ek__BackingField_189; }
	inline void set_U3CcharacterAnimU3Ek__BackingField_189(AnimationRunner_t1015409588 * value)
	{
		___U3CcharacterAnimU3Ek__BackingField_189 = value;
		Il2CppCodeGenWriteBarrier(&___U3CcharacterAnimU3Ek__BackingField_189, value);
	}

	inline static int32_t get_offset_of_U3CtotalDamageU3Ek__BackingField_190() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CtotalDamageU3Ek__BackingField_190)); }
	inline float get_U3CtotalDamageU3Ek__BackingField_190() const { return ___U3CtotalDamageU3Ek__BackingField_190; }
	inline float* get_address_of_U3CtotalDamageU3Ek__BackingField_190() { return &___U3CtotalDamageU3Ek__BackingField_190; }
	inline void set_U3CtotalDamageU3Ek__BackingField_190(float value)
	{
		___U3CtotalDamageU3Ek__BackingField_190 = value;
	}

	inline static int32_t get_offset_of_U3CatkUnitTagU3Ek__BackingField_191() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CatkUnitTagU3Ek__BackingField_191)); }
	inline int32_t get_U3CatkUnitTagU3Ek__BackingField_191() const { return ___U3CatkUnitTagU3Ek__BackingField_191; }
	inline int32_t* get_address_of_U3CatkUnitTagU3Ek__BackingField_191() { return &___U3CatkUnitTagU3Ek__BackingField_191; }
	inline void set_U3CatkUnitTagU3Ek__BackingField_191(int32_t value)
	{
		___U3CatkUnitTagU3Ek__BackingField_191 = value;
	}

	inline static int32_t get_offset_of_U3CunitTagU3Ek__BackingField_192() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CunitTagU3Ek__BackingField_192)); }
	inline int32_t get_U3CunitTagU3Ek__BackingField_192() const { return ___U3CunitTagU3Ek__BackingField_192; }
	inline int32_t* get_address_of_U3CunitTagU3Ek__BackingField_192() { return &___U3CunitTagU3Ek__BackingField_192; }
	inline void set_U3CunitTagU3Ek__BackingField_192(int32_t value)
	{
		___U3CunitTagU3Ek__BackingField_192 = value;
	}

	inline static int32_t get_offset_of_U3CfightCtrlU3Ek__BackingField_193() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CfightCtrlU3Ek__BackingField_193)); }
	inline FightCtrl_t648967803 * get_U3CfightCtrlU3Ek__BackingField_193() const { return ___U3CfightCtrlU3Ek__BackingField_193; }
	inline FightCtrl_t648967803 ** get_address_of_U3CfightCtrlU3Ek__BackingField_193() { return &___U3CfightCtrlU3Ek__BackingField_193; }
	inline void set_U3CfightCtrlU3Ek__BackingField_193(FightCtrl_t648967803 * value)
	{
		___U3CfightCtrlU3Ek__BackingField_193 = value;
		Il2CppCodeGenWriteBarrier(&___U3CfightCtrlU3Ek__BackingField_193, value);
	}

	inline static int32_t get_offset_of_U3CbuffCtrlU3Ek__BackingField_194() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CbuffCtrlU3Ek__BackingField_194)); }
	inline BuffCtrl_t2836564350 * get_U3CbuffCtrlU3Ek__BackingField_194() const { return ___U3CbuffCtrlU3Ek__BackingField_194; }
	inline BuffCtrl_t2836564350 ** get_address_of_U3CbuffCtrlU3Ek__BackingField_194() { return &___U3CbuffCtrlU3Ek__BackingField_194; }
	inline void set_U3CbuffCtrlU3Ek__BackingField_194(BuffCtrl_t2836564350 * value)
	{
		___U3CbuffCtrlU3Ek__BackingField_194 = value;
		Il2CppCodeGenWriteBarrier(&___U3CbuffCtrlU3Ek__BackingField_194, value);
	}

	inline static int32_t get_offset_of_U3CbvrCtrlU3Ek__BackingField_195() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CbvrCtrlU3Ek__BackingField_195)); }
	inline IBehaviorCtrl_t4225040900 * get_U3CbvrCtrlU3Ek__BackingField_195() const { return ___U3CbvrCtrlU3Ek__BackingField_195; }
	inline IBehaviorCtrl_t4225040900 ** get_address_of_U3CbvrCtrlU3Ek__BackingField_195() { return &___U3CbvrCtrlU3Ek__BackingField_195; }
	inline void set_U3CbvrCtrlU3Ek__BackingField_195(IBehaviorCtrl_t4225040900 * value)
	{
		___U3CbvrCtrlU3Ek__BackingField_195 = value;
		Il2CppCodeGenWriteBarrier(&___U3CbvrCtrlU3Ek__BackingField_195, value);
	}

	inline static int32_t get_offset_of_U3CskinMeshRenderU3Ek__BackingField_196() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CskinMeshRenderU3Ek__BackingField_196)); }
	inline SkinnedMeshRenderer_t3986041494 * get_U3CskinMeshRenderU3Ek__BackingField_196() const { return ___U3CskinMeshRenderU3Ek__BackingField_196; }
	inline SkinnedMeshRenderer_t3986041494 ** get_address_of_U3CskinMeshRenderU3Ek__BackingField_196() { return &___U3CskinMeshRenderU3Ek__BackingField_196; }
	inline void set_U3CskinMeshRenderU3Ek__BackingField_196(SkinnedMeshRenderer_t3986041494 * value)
	{
		___U3CskinMeshRenderU3Ek__BackingField_196 = value;
		Il2CppCodeGenWriteBarrier(&___U3CskinMeshRenderU3Ek__BackingField_196, value);
	}

	inline static int32_t get_offset_of_U3CbloodU3Ek__BackingField_197() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CbloodU3Ek__BackingField_197)); }
	inline Blood_t64280026 * get_U3CbloodU3Ek__BackingField_197() const { return ___U3CbloodU3Ek__BackingField_197; }
	inline Blood_t64280026 ** get_address_of_U3CbloodU3Ek__BackingField_197() { return &___U3CbloodU3Ek__BackingField_197; }
	inline void set_U3CbloodU3Ek__BackingField_197(Blood_t64280026 * value)
	{
		___U3CbloodU3Ek__BackingField_197 = value;
		Il2CppCodeGenWriteBarrier(&___U3CbloodU3Ek__BackingField_197, value);
	}

	inline static int32_t get_offset_of_U3CPlotFightMgrU3Ek__BackingField_198() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CPlotFightMgrU3Ek__BackingField_198)); }
	inline PlotFightTalkMgr_t866599741 * get_U3CPlotFightMgrU3Ek__BackingField_198() const { return ___U3CPlotFightMgrU3Ek__BackingField_198; }
	inline PlotFightTalkMgr_t866599741 ** get_address_of_U3CPlotFightMgrU3Ek__BackingField_198() { return &___U3CPlotFightMgrU3Ek__BackingField_198; }
	inline void set_U3CPlotFightMgrU3Ek__BackingField_198(PlotFightTalkMgr_t866599741 * value)
	{
		___U3CPlotFightMgrU3Ek__BackingField_198 = value;
		Il2CppCodeGenWriteBarrier(&___U3CPlotFightMgrU3Ek__BackingField_198, value);
	}

	inline static int32_t get_offset_of_U3CshadowU3Ek__BackingField_199() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CshadowU3Ek__BackingField_199)); }
	inline FS_ShadowSimple_t4208748868 * get_U3CshadowU3Ek__BackingField_199() const { return ___U3CshadowU3Ek__BackingField_199; }
	inline FS_ShadowSimple_t4208748868 ** get_address_of_U3CshadowU3Ek__BackingField_199() { return &___U3CshadowU3Ek__BackingField_199; }
	inline void set_U3CshadowU3Ek__BackingField_199(FS_ShadowSimple_t4208748868 * value)
	{
		___U3CshadowU3Ek__BackingField_199 = value;
		Il2CppCodeGenWriteBarrier(&___U3CshadowU3Ek__BackingField_199, value);
	}

	inline static int32_t get_offset_of_U3CunitTypeU3Ek__BackingField_200() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CunitTypeU3Ek__BackingField_200)); }
	inline int32_t get_U3CunitTypeU3Ek__BackingField_200() const { return ___U3CunitTypeU3Ek__BackingField_200; }
	inline int32_t* get_address_of_U3CunitTypeU3Ek__BackingField_200() { return &___U3CunitTypeU3Ek__BackingField_200; }
	inline void set_U3CunitTypeU3Ek__BackingField_200(int32_t value)
	{
		___U3CunitTypeU3Ek__BackingField_200 = value;
	}

	inline static int32_t get_offset_of_U3CdeadTypeU3Ek__BackingField_201() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CdeadTypeU3Ek__BackingField_201)); }
	inline int32_t get_U3CdeadTypeU3Ek__BackingField_201() const { return ___U3CdeadTypeU3Ek__BackingField_201; }
	inline int32_t* get_address_of_U3CdeadTypeU3Ek__BackingField_201() { return &___U3CdeadTypeU3Ek__BackingField_201; }
	inline void set_U3CdeadTypeU3Ek__BackingField_201(int32_t value)
	{
		___U3CdeadTypeU3Ek__BackingField_201 = value;
	}

	inline static int32_t get_offset_of_U3CviewLevelU3Ek__BackingField_202() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CviewLevelU3Ek__BackingField_202)); }
	inline int32_t get_U3CviewLevelU3Ek__BackingField_202() const { return ___U3CviewLevelU3Ek__BackingField_202; }
	inline int32_t* get_address_of_U3CviewLevelU3Ek__BackingField_202() { return &___U3CviewLevelU3Ek__BackingField_202; }
	inline void set_U3CviewLevelU3Ek__BackingField_202(int32_t value)
	{
		___U3CviewLevelU3Ek__BackingField_202 = value;
	}

	inline static int32_t get_offset_of_U3CviewGradeU3Ek__BackingField_203() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CviewGradeU3Ek__BackingField_203)); }
	inline int32_t get_U3CviewGradeU3Ek__BackingField_203() const { return ___U3CviewGradeU3Ek__BackingField_203; }
	inline int32_t* get_address_of_U3CviewGradeU3Ek__BackingField_203() { return &___U3CviewGradeU3Ek__BackingField_203; }
	inline void set_U3CviewGradeU3Ek__BackingField_203(int32_t value)
	{
		___U3CviewGradeU3Ek__BackingField_203 = value;
	}

	inline static int32_t get_offset_of_U3ClevelU3Ek__BackingField_204() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3ClevelU3Ek__BackingField_204)); }
	inline int32_t get_U3ClevelU3Ek__BackingField_204() const { return ___U3ClevelU3Ek__BackingField_204; }
	inline int32_t* get_address_of_U3ClevelU3Ek__BackingField_204() { return &___U3ClevelU3Ek__BackingField_204; }
	inline void set_U3ClevelU3Ek__BackingField_204(int32_t value)
	{
		___U3ClevelU3Ek__BackingField_204 = value;
	}

	inline static int32_t get_offset_of_U3CatkTypeU3Ek__BackingField_205() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CatkTypeU3Ek__BackingField_205)); }
	inline int32_t get_U3CatkTypeU3Ek__BackingField_205() const { return ___U3CatkTypeU3Ek__BackingField_205; }
	inline int32_t* get_address_of_U3CatkTypeU3Ek__BackingField_205() { return &___U3CatkTypeU3Ek__BackingField_205; }
	inline void set_U3CatkTypeU3Ek__BackingField_205(int32_t value)
	{
		___U3CatkTypeU3Ek__BackingField_205 = value;
	}

	inline static int32_t get_offset_of_U3CdamageTypeU3Ek__BackingField_206() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CdamageTypeU3Ek__BackingField_206)); }
	inline int32_t get_U3CdamageTypeU3Ek__BackingField_206() const { return ___U3CdamageTypeU3Ek__BackingField_206; }
	inline int32_t* get_address_of_U3CdamageTypeU3Ek__BackingField_206() { return &___U3CdamageTypeU3Ek__BackingField_206; }
	inline void set_U3CdamageTypeU3Ek__BackingField_206(int32_t value)
	{
		___U3CdamageTypeU3Ek__BackingField_206 = value;
	}

	inline static int32_t get_offset_of_U3CatkPowerU3Ek__BackingField_207() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CatkPowerU3Ek__BackingField_207)); }
	inline float get_U3CatkPowerU3Ek__BackingField_207() const { return ___U3CatkPowerU3Ek__BackingField_207; }
	inline float* get_address_of_U3CatkPowerU3Ek__BackingField_207() { return &___U3CatkPowerU3Ek__BackingField_207; }
	inline void set_U3CatkPowerU3Ek__BackingField_207(float value)
	{
		___U3CatkPowerU3Ek__BackingField_207 = value;
	}

	inline static int32_t get_offset_of_U3CatkPowerPU3Ek__BackingField_208() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CatkPowerPU3Ek__BackingField_208)); }
	inline float get_U3CatkPowerPU3Ek__BackingField_208() const { return ___U3CatkPowerPU3Ek__BackingField_208; }
	inline float* get_address_of_U3CatkPowerPU3Ek__BackingField_208() { return &___U3CatkPowerPU3Ek__BackingField_208; }
	inline void set_U3CatkPowerPU3Ek__BackingField_208(float value)
	{
		___U3CatkPowerPU3Ek__BackingField_208 = value;
	}

	inline static int32_t get_offset_of_U3CatkPowerLU3Ek__BackingField_209() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CatkPowerLU3Ek__BackingField_209)); }
	inline float get_U3CatkPowerLU3Ek__BackingField_209() const { return ___U3CatkPowerLU3Ek__BackingField_209; }
	inline float* get_address_of_U3CatkPowerLU3Ek__BackingField_209() { return &___U3CatkPowerLU3Ek__BackingField_209; }
	inline void set_U3CatkPowerLU3Ek__BackingField_209(float value)
	{
		___U3CatkPowerLU3Ek__BackingField_209 = value;
	}

	inline static int32_t get_offset_of_U3CPDU3Ek__BackingField_210() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CPDU3Ek__BackingField_210)); }
	inline float get_U3CPDU3Ek__BackingField_210() const { return ___U3CPDU3Ek__BackingField_210; }
	inline float* get_address_of_U3CPDU3Ek__BackingField_210() { return &___U3CPDU3Ek__BackingField_210; }
	inline void set_U3CPDU3Ek__BackingField_210(float value)
	{
		___U3CPDU3Ek__BackingField_210 = value;
	}

	inline static int32_t get_offset_of_U3CMDU3Ek__BackingField_211() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CMDU3Ek__BackingField_211)); }
	inline float get_U3CMDU3Ek__BackingField_211() const { return ___U3CMDU3Ek__BackingField_211; }
	inline float* get_address_of_U3CMDU3Ek__BackingField_211() { return &___U3CMDU3Ek__BackingField_211; }
	inline void set_U3CMDU3Ek__BackingField_211(float value)
	{
		___U3CMDU3Ek__BackingField_211 = value;
	}

	inline static int32_t get_offset_of_U3CdefensePU3Ek__BackingField_212() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CdefensePU3Ek__BackingField_212)); }
	inline float get_U3CdefensePU3Ek__BackingField_212() const { return ___U3CdefensePU3Ek__BackingField_212; }
	inline float* get_address_of_U3CdefensePU3Ek__BackingField_212() { return &___U3CdefensePU3Ek__BackingField_212; }
	inline void set_U3CdefensePU3Ek__BackingField_212(float value)
	{
		___U3CdefensePU3Ek__BackingField_212 = value;
	}

	inline static int32_t get_offset_of_U3CdefenseLU3Ek__BackingField_213() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CdefenseLU3Ek__BackingField_213)); }
	inline float get_U3CdefenseLU3Ek__BackingField_213() const { return ___U3CdefenseLU3Ek__BackingField_213; }
	inline float* get_address_of_U3CdefenseLU3Ek__BackingField_213() { return &___U3CdefenseLU3Ek__BackingField_213; }
	inline void set_U3CdefenseLU3Ek__BackingField_213(float value)
	{
		___U3CdefenseLU3Ek__BackingField_213 = value;
	}

	inline static int32_t get_offset_of_U3ChpU3Ek__BackingField_214() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3ChpU3Ek__BackingField_214)); }
	inline float get_U3ChpU3Ek__BackingField_214() const { return ___U3ChpU3Ek__BackingField_214; }
	inline float* get_address_of_U3ChpU3Ek__BackingField_214() { return &___U3ChpU3Ek__BackingField_214; }
	inline void set_U3ChpU3Ek__BackingField_214(float value)
	{
		___U3ChpU3Ek__BackingField_214 = value;
	}

	inline static int32_t get_offset_of_U3CmaxHpU3Ek__BackingField_215() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CmaxHpU3Ek__BackingField_215)); }
	inline float get_U3CmaxHpU3Ek__BackingField_215() const { return ___U3CmaxHpU3Ek__BackingField_215; }
	inline float* get_address_of_U3CmaxHpU3Ek__BackingField_215() { return &___U3CmaxHpU3Ek__BackingField_215; }
	inline void set_U3CmaxHpU3Ek__BackingField_215(float value)
	{
		___U3CmaxHpU3Ek__BackingField_215 = value;
	}

	inline static int32_t get_offset_of_U3CHPSecDecPerU3Ek__BackingField_216() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CHPSecDecPerU3Ek__BackingField_216)); }
	inline float get_U3CHPSecDecPerU3Ek__BackingField_216() const { return ___U3CHPSecDecPerU3Ek__BackingField_216; }
	inline float* get_address_of_U3CHPSecDecPerU3Ek__BackingField_216() { return &___U3CHPSecDecPerU3Ek__BackingField_216; }
	inline void set_U3CHPSecDecPerU3Ek__BackingField_216(float value)
	{
		___U3CHPSecDecPerU3Ek__BackingField_216 = value;
	}

	inline static int32_t get_offset_of_U3CHPSecDecNumU3Ek__BackingField_217() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CHPSecDecNumU3Ek__BackingField_217)); }
	inline float get_U3CHPSecDecNumU3Ek__BackingField_217() const { return ___U3CHPSecDecNumU3Ek__BackingField_217; }
	inline float* get_address_of_U3CHPSecDecNumU3Ek__BackingField_217() { return &___U3CHPSecDecNumU3Ek__BackingField_217; }
	inline void set_U3CHPSecDecNumU3Ek__BackingField_217(float value)
	{
		___U3CHPSecDecNumU3Ek__BackingField_217 = value;
	}

	inline static int32_t get_offset_of_U3CHPRstNumU3Ek__BackingField_218() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CHPRstNumU3Ek__BackingField_218)); }
	inline float get_U3CHPRstNumU3Ek__BackingField_218() const { return ___U3CHPRstNumU3Ek__BackingField_218; }
	inline float* get_address_of_U3CHPRstNumU3Ek__BackingField_218() { return &___U3CHPRstNumU3Ek__BackingField_218; }
	inline void set_U3CHPRstNumU3Ek__BackingField_218(float value)
	{
		___U3CHPRstNumU3Ek__BackingField_218 = value;
	}

	inline static int32_t get_offset_of_U3CHPRstPerU3Ek__BackingField_219() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CHPRstPerU3Ek__BackingField_219)); }
	inline float get_U3CHPRstPerU3Ek__BackingField_219() const { return ___U3CHPRstPerU3Ek__BackingField_219; }
	inline float* get_address_of_U3CHPRstPerU3Ek__BackingField_219() { return &___U3CHPRstPerU3Ek__BackingField_219; }
	inline void set_U3CHPRstPerU3Ek__BackingField_219(float value)
	{
		___U3CHPRstPerU3Ek__BackingField_219 = value;
	}

	inline static int32_t get_offset_of_U3CstarLevelU3Ek__BackingField_220() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CstarLevelU3Ek__BackingField_220)); }
	inline int32_t get_U3CstarLevelU3Ek__BackingField_220() const { return ___U3CstarLevelU3Ek__BackingField_220; }
	inline int32_t* get_address_of_U3CstarLevelU3Ek__BackingField_220() { return &___U3CstarLevelU3Ek__BackingField_220; }
	inline void set_U3CstarLevelU3Ek__BackingField_220(int32_t value)
	{
		___U3CstarLevelU3Ek__BackingField_220 = value;
	}

	inline static int32_t get_offset_of_U3ChitsU3Ek__BackingField_221() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3ChitsU3Ek__BackingField_221)); }
	inline float get_U3ChitsU3Ek__BackingField_221() const { return ___U3ChitsU3Ek__BackingField_221; }
	inline float* get_address_of_U3ChitsU3Ek__BackingField_221() { return &___U3ChitsU3Ek__BackingField_221; }
	inline void set_U3ChitsU3Ek__BackingField_221(float value)
	{
		___U3ChitsU3Ek__BackingField_221 = value;
	}

	inline static int32_t get_offset_of_U3CdodgeU3Ek__BackingField_222() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CdodgeU3Ek__BackingField_222)); }
	inline float get_U3CdodgeU3Ek__BackingField_222() const { return ___U3CdodgeU3Ek__BackingField_222; }
	inline float* get_address_of_U3CdodgeU3Ek__BackingField_222() { return &___U3CdodgeU3Ek__BackingField_222; }
	inline void set_U3CdodgeU3Ek__BackingField_222(float value)
	{
		___U3CdodgeU3Ek__BackingField_222 = value;
	}

	inline static int32_t get_offset_of_U3CwreckU3Ek__BackingField_223() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CwreckU3Ek__BackingField_223)); }
	inline float get_U3CwreckU3Ek__BackingField_223() const { return ___U3CwreckU3Ek__BackingField_223; }
	inline float* get_address_of_U3CwreckU3Ek__BackingField_223() { return &___U3CwreckU3Ek__BackingField_223; }
	inline void set_U3CwreckU3Ek__BackingField_223(float value)
	{
		___U3CwreckU3Ek__BackingField_223 = value;
	}

	inline static int32_t get_offset_of_U3CwithstandU3Ek__BackingField_224() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CwithstandU3Ek__BackingField_224)); }
	inline float get_U3CwithstandU3Ek__BackingField_224() const { return ___U3CwithstandU3Ek__BackingField_224; }
	inline float* get_address_of_U3CwithstandU3Ek__BackingField_224() { return &___U3CwithstandU3Ek__BackingField_224; }
	inline void set_U3CwithstandU3Ek__BackingField_224(float value)
	{
		___U3CwithstandU3Ek__BackingField_224 = value;
	}

	inline static int32_t get_offset_of_U3CcritU3Ek__BackingField_225() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CcritU3Ek__BackingField_225)); }
	inline float get_U3CcritU3Ek__BackingField_225() const { return ___U3CcritU3Ek__BackingField_225; }
	inline float* get_address_of_U3CcritU3Ek__BackingField_225() { return &___U3CcritU3Ek__BackingField_225; }
	inline void set_U3CcritU3Ek__BackingField_225(float value)
	{
		___U3CcritU3Ek__BackingField_225 = value;
	}

	inline static int32_t get_offset_of_U3CtenacityU3Ek__BackingField_226() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CtenacityU3Ek__BackingField_226)); }
	inline float get_U3CtenacityU3Ek__BackingField_226() const { return ___U3CtenacityU3Ek__BackingField_226; }
	inline float* get_address_of_U3CtenacityU3Ek__BackingField_226() { return &___U3CtenacityU3Ek__BackingField_226; }
	inline void set_U3CtenacityU3Ek__BackingField_226(float value)
	{
		___U3CtenacityU3Ek__BackingField_226 = value;
	}

	inline static int32_t get_offset_of_U3ChitsPU3Ek__BackingField_227() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3ChitsPU3Ek__BackingField_227)); }
	inline float get_U3ChitsPU3Ek__BackingField_227() const { return ___U3ChitsPU3Ek__BackingField_227; }
	inline float* get_address_of_U3ChitsPU3Ek__BackingField_227() { return &___U3ChitsPU3Ek__BackingField_227; }
	inline void set_U3ChitsPU3Ek__BackingField_227(float value)
	{
		___U3ChitsPU3Ek__BackingField_227 = value;
	}

	inline static int32_t get_offset_of_U3CdodgePU3Ek__BackingField_228() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CdodgePU3Ek__BackingField_228)); }
	inline float get_U3CdodgePU3Ek__BackingField_228() const { return ___U3CdodgePU3Ek__BackingField_228; }
	inline float* get_address_of_U3CdodgePU3Ek__BackingField_228() { return &___U3CdodgePU3Ek__BackingField_228; }
	inline void set_U3CdodgePU3Ek__BackingField_228(float value)
	{
		___U3CdodgePU3Ek__BackingField_228 = value;
	}

	inline static int32_t get_offset_of_U3CwreckPU3Ek__BackingField_229() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CwreckPU3Ek__BackingField_229)); }
	inline float get_U3CwreckPU3Ek__BackingField_229() const { return ___U3CwreckPU3Ek__BackingField_229; }
	inline float* get_address_of_U3CwreckPU3Ek__BackingField_229() { return &___U3CwreckPU3Ek__BackingField_229; }
	inline void set_U3CwreckPU3Ek__BackingField_229(float value)
	{
		___U3CwreckPU3Ek__BackingField_229 = value;
	}

	inline static int32_t get_offset_of_U3CwithstandPU3Ek__BackingField_230() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CwithstandPU3Ek__BackingField_230)); }
	inline float get_U3CwithstandPU3Ek__BackingField_230() const { return ___U3CwithstandPU3Ek__BackingField_230; }
	inline float* get_address_of_U3CwithstandPU3Ek__BackingField_230() { return &___U3CwithstandPU3Ek__BackingField_230; }
	inline void set_U3CwithstandPU3Ek__BackingField_230(float value)
	{
		___U3CwithstandPU3Ek__BackingField_230 = value;
	}

	inline static int32_t get_offset_of_U3CcritPU3Ek__BackingField_231() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CcritPU3Ek__BackingField_231)); }
	inline float get_U3CcritPU3Ek__BackingField_231() const { return ___U3CcritPU3Ek__BackingField_231; }
	inline float* get_address_of_U3CcritPU3Ek__BackingField_231() { return &___U3CcritPU3Ek__BackingField_231; }
	inline void set_U3CcritPU3Ek__BackingField_231(float value)
	{
		___U3CcritPU3Ek__BackingField_231 = value;
	}

	inline static int32_t get_offset_of_U3CtenacityPU3Ek__BackingField_232() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CtenacityPU3Ek__BackingField_232)); }
	inline float get_U3CtenacityPU3Ek__BackingField_232() const { return ___U3CtenacityPU3Ek__BackingField_232; }
	inline float* get_address_of_U3CtenacityPU3Ek__BackingField_232() { return &___U3CtenacityPU3Ek__BackingField_232; }
	inline void set_U3CtenacityPU3Ek__BackingField_232(float value)
	{
		___U3CtenacityPU3Ek__BackingField_232 = value;
	}

	inline static int32_t get_offset_of_U3CcritHurtU3Ek__BackingField_233() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CcritHurtU3Ek__BackingField_233)); }
	inline float get_U3CcritHurtU3Ek__BackingField_233() const { return ___U3CcritHurtU3Ek__BackingField_233; }
	inline float* get_address_of_U3CcritHurtU3Ek__BackingField_233() { return &___U3CcritHurtU3Ek__BackingField_233; }
	inline void set_U3CcritHurtU3Ek__BackingField_233(float value)
	{
		___U3CcritHurtU3Ek__BackingField_233 = value;
	}

	inline static int32_t get_offset_of_U3CrHurtU3Ek__BackingField_234() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CrHurtU3Ek__BackingField_234)); }
	inline float get_U3CrHurtU3Ek__BackingField_234() const { return ___U3CrHurtU3Ek__BackingField_234; }
	inline float* get_address_of_U3CrHurtU3Ek__BackingField_234() { return &___U3CrHurtU3Ek__BackingField_234; }
	inline void set_U3CrHurtU3Ek__BackingField_234(float value)
	{
		___U3CrHurtU3Ek__BackingField_234 = value;
	}

	inline static int32_t get_offset_of_U3CcurePU3Ek__BackingField_235() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CcurePU3Ek__BackingField_235)); }
	inline float get_U3CcurePU3Ek__BackingField_235() const { return ___U3CcurePU3Ek__BackingField_235; }
	inline float* get_address_of_U3CcurePU3Ek__BackingField_235() { return &___U3CcurePU3Ek__BackingField_235; }
	inline void set_U3CcurePU3Ek__BackingField_235(float value)
	{
		___U3CcurePU3Ek__BackingField_235 = value;
	}

	inline static int32_t get_offset_of_U3ChurtPU3Ek__BackingField_236() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3ChurtPU3Ek__BackingField_236)); }
	inline float get_U3ChurtPU3Ek__BackingField_236() const { return ___U3ChurtPU3Ek__BackingField_236; }
	inline float* get_address_of_U3ChurtPU3Ek__BackingField_236() { return &___U3ChurtPU3Ek__BackingField_236; }
	inline void set_U3ChurtPU3Ek__BackingField_236(float value)
	{
		___U3ChurtPU3Ek__BackingField_236 = value;
	}

	inline static int32_t get_offset_of_U3ChurtLU3Ek__BackingField_237() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3ChurtLU3Ek__BackingField_237)); }
	inline float get_U3ChurtLU3Ek__BackingField_237() const { return ___U3ChurtLU3Ek__BackingField_237; }
	inline float* get_address_of_U3ChurtLU3Ek__BackingField_237() { return &___U3ChurtLU3Ek__BackingField_237; }
	inline void set_U3ChurtLU3Ek__BackingField_237(float value)
	{
		___U3ChurtLU3Ek__BackingField_237 = value;
	}

	inline static int32_t get_offset_of_U3CfinalHurtU3Ek__BackingField_238() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CfinalHurtU3Ek__BackingField_238)); }
	inline float get_U3CfinalHurtU3Ek__BackingField_238() const { return ___U3CfinalHurtU3Ek__BackingField_238; }
	inline float* get_address_of_U3CfinalHurtU3Ek__BackingField_238() { return &___U3CfinalHurtU3Ek__BackingField_238; }
	inline void set_U3CfinalHurtU3Ek__BackingField_238(float value)
	{
		___U3CfinalHurtU3Ek__BackingField_238 = value;
	}

	inline static int32_t get_offset_of_U3CatkSpeedU3Ek__BackingField_239() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CatkSpeedU3Ek__BackingField_239)); }
	inline float get_U3CatkSpeedU3Ek__BackingField_239() const { return ___U3CatkSpeedU3Ek__BackingField_239; }
	inline float* get_address_of_U3CatkSpeedU3Ek__BackingField_239() { return &___U3CatkSpeedU3Ek__BackingField_239; }
	inline void set_U3CatkSpeedU3Ek__BackingField_239(float value)
	{
		___U3CatkSpeedU3Ek__BackingField_239 = value;
	}

	inline static int32_t get_offset_of_U3CrunSpeedU3Ek__BackingField_240() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CrunSpeedU3Ek__BackingField_240)); }
	inline float get_U3CrunSpeedU3Ek__BackingField_240() const { return ___U3CrunSpeedU3Ek__BackingField_240; }
	inline float* get_address_of_U3CrunSpeedU3Ek__BackingField_240() { return &___U3CrunSpeedU3Ek__BackingField_240; }
	inline void set_U3CrunSpeedU3Ek__BackingField_240(float value)
	{
		___U3CrunSpeedU3Ek__BackingField_240 = value;
	}

	inline static int32_t get_offset_of_U3CatkRangeU3Ek__BackingField_241() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CatkRangeU3Ek__BackingField_241)); }
	inline float get_U3CatkRangeU3Ek__BackingField_241() const { return ___U3CatkRangeU3Ek__BackingField_241; }
	inline float* get_address_of_U3CatkRangeU3Ek__BackingField_241() { return &___U3CatkRangeU3Ek__BackingField_241; }
	inline void set_U3CatkRangeU3Ek__BackingField_241(float value)
	{
		___U3CatkRangeU3Ek__BackingField_241 = value;
	}

	inline static int32_t get_offset_of_U3CmaxRageU3Ek__BackingField_242() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CmaxRageU3Ek__BackingField_242)); }
	inline float get_U3CmaxRageU3Ek__BackingField_242() const { return ___U3CmaxRageU3Ek__BackingField_242; }
	inline float* get_address_of_U3CmaxRageU3Ek__BackingField_242() { return &___U3CmaxRageU3Ek__BackingField_242; }
	inline void set_U3CmaxRageU3Ek__BackingField_242(float value)
	{
		___U3CmaxRageU3Ek__BackingField_242 = value;
	}

	inline static int32_t get_offset_of_U3CfindEnemyRangeU3Ek__BackingField_243() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CfindEnemyRangeU3Ek__BackingField_243)); }
	inline float get_U3CfindEnemyRangeU3Ek__BackingField_243() const { return ___U3CfindEnemyRangeU3Ek__BackingField_243; }
	inline float* get_address_of_U3CfindEnemyRangeU3Ek__BackingField_243() { return &___U3CfindEnemyRangeU3Ek__BackingField_243; }
	inline void set_U3CfindEnemyRangeU3Ek__BackingField_243(float value)
	{
		___U3CfindEnemyRangeU3Ek__BackingField_243 = value;
	}

	inline static int32_t get_offset_of_U3CrageU3Ek__BackingField_244() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CrageU3Ek__BackingField_244)); }
	inline float get_U3CrageU3Ek__BackingField_244() const { return ___U3CrageU3Ek__BackingField_244; }
	inline float* get_address_of_U3CrageU3Ek__BackingField_244() { return &___U3CrageU3Ek__BackingField_244; }
	inline void set_U3CrageU3Ek__BackingField_244(float value)
	{
		___U3CrageU3Ek__BackingField_244 = value;
	}

	inline static int32_t get_offset_of_U3CrageGrowthU3Ek__BackingField_245() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CrageGrowthU3Ek__BackingField_245)); }
	inline int32_t get_U3CrageGrowthU3Ek__BackingField_245() const { return ___U3CrageGrowthU3Ek__BackingField_245; }
	inline int32_t* get_address_of_U3CrageGrowthU3Ek__BackingField_245() { return &___U3CrageGrowthU3Ek__BackingField_245; }
	inline void set_U3CrageGrowthU3Ek__BackingField_245(int32_t value)
	{
		___U3CrageGrowthU3Ek__BackingField_245 = value;
	}

	inline static int32_t get_offset_of_U3CelementU3Ek__BackingField_246() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CelementU3Ek__BackingField_246)); }
	inline int32_t get_U3CelementU3Ek__BackingField_246() const { return ___U3CelementU3Ek__BackingField_246; }
	inline int32_t* get_address_of_U3CelementU3Ek__BackingField_246() { return &___U3CelementU3Ek__BackingField_246; }
	inline void set_U3CelementU3Ek__BackingField_246(int32_t value)
	{
		___U3CelementU3Ek__BackingField_246 = value;
	}

	inline static int32_t get_offset_of_U3CstableU3Ek__BackingField_247() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CstableU3Ek__BackingField_247)); }
	inline int32_t get_U3CstableU3Ek__BackingField_247() const { return ___U3CstableU3Ek__BackingField_247; }
	inline int32_t* get_address_of_U3CstableU3Ek__BackingField_247() { return &___U3CstableU3Ek__BackingField_247; }
	inline void set_U3CstableU3Ek__BackingField_247(int32_t value)
	{
		___U3CstableU3Ek__BackingField_247 = value;
	}

	inline static int32_t get_offset_of_U3CnotInvadeU3Ek__BackingField_248() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CnotInvadeU3Ek__BackingField_248)); }
	inline int32_t get_U3CnotInvadeU3Ek__BackingField_248() const { return ___U3CnotInvadeU3Ek__BackingField_248; }
	inline int32_t* get_address_of_U3CnotInvadeU3Ek__BackingField_248() { return &___U3CnotInvadeU3Ek__BackingField_248; }
	inline void set_U3CnotInvadeU3Ek__BackingField_248(int32_t value)
	{
		___U3CnotInvadeU3Ek__BackingField_248 = value;
	}

	inline static int32_t get_offset_of_U3CunyieldingU3Ek__BackingField_249() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CunyieldingU3Ek__BackingField_249)); }
	inline int32_t get_U3CunyieldingU3Ek__BackingField_249() const { return ___U3CunyieldingU3Ek__BackingField_249; }
	inline int32_t* get_address_of_U3CunyieldingU3Ek__BackingField_249() { return &___U3CunyieldingU3Ek__BackingField_249; }
	inline void set_U3CunyieldingU3Ek__BackingField_249(int32_t value)
	{
		___U3CunyieldingU3Ek__BackingField_249 = value;
	}

	inline static int32_t get_offset_of_U3CmodelScaleU3Ek__BackingField_250() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CmodelScaleU3Ek__BackingField_250)); }
	inline float get_U3CmodelScaleU3Ek__BackingField_250() const { return ___U3CmodelScaleU3Ek__BackingField_250; }
	inline float* get_address_of_U3CmodelScaleU3Ek__BackingField_250() { return &___U3CmodelScaleU3Ek__BackingField_250; }
	inline void set_U3CmodelScaleU3Ek__BackingField_250(float value)
	{
		___U3CmodelScaleU3Ek__BackingField_250 = value;
	}

	inline static int32_t get_offset_of_U3CbeforModelScaleU3Ek__BackingField_251() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CbeforModelScaleU3Ek__BackingField_251)); }
	inline float get_U3CbeforModelScaleU3Ek__BackingField_251() const { return ___U3CbeforModelScaleU3Ek__BackingField_251; }
	inline float* get_address_of_U3CbeforModelScaleU3Ek__BackingField_251() { return &___U3CbeforModelScaleU3Ek__BackingField_251; }
	inline void set_U3CbeforModelScaleU3Ek__BackingField_251(float value)
	{
		___U3CbeforModelScaleU3Ek__BackingField_251 = value;
	}

	inline static int32_t get_offset_of_U3CmodelScaleBetweenValueU3Ek__BackingField_252() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CmodelScaleBetweenValueU3Ek__BackingField_252)); }
	inline float get_U3CmodelScaleBetweenValueU3Ek__BackingField_252() const { return ___U3CmodelScaleBetweenValueU3Ek__BackingField_252; }
	inline float* get_address_of_U3CmodelScaleBetweenValueU3Ek__BackingField_252() { return &___U3CmodelScaleBetweenValueU3Ek__BackingField_252; }
	inline void set_U3CmodelScaleBetweenValueU3Ek__BackingField_252(float value)
	{
		___U3CmodelScaleBetweenValueU3Ek__BackingField_252 = value;
	}

	inline static int32_t get_offset_of_U3CcurModelScaleValueU3Ek__BackingField_253() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CcurModelScaleValueU3Ek__BackingField_253)); }
	inline float get_U3CcurModelScaleValueU3Ek__BackingField_253() const { return ___U3CcurModelScaleValueU3Ek__BackingField_253; }
	inline float* get_address_of_U3CcurModelScaleValueU3Ek__BackingField_253() { return &___U3CcurModelScaleValueU3Ek__BackingField_253; }
	inline void set_U3CcurModelScaleValueU3Ek__BackingField_253(float value)
	{
		___U3CcurModelScaleValueU3Ek__BackingField_253 = value;
	}

	inline static int32_t get_offset_of_U3CvampireU3Ek__BackingField_254() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CvampireU3Ek__BackingField_254)); }
	inline float get_U3CvampireU3Ek__BackingField_254() const { return ___U3CvampireU3Ek__BackingField_254; }
	inline float* get_address_of_U3CvampireU3Ek__BackingField_254() { return &___U3CvampireU3Ek__BackingField_254; }
	inline void set_U3CvampireU3Ek__BackingField_254(float value)
	{
		___U3CvampireU3Ek__BackingField_254 = value;
	}

	inline static int32_t get_offset_of_U3CtauntDistanceU3Ek__BackingField_255() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CtauntDistanceU3Ek__BackingField_255)); }
	inline float get_U3CtauntDistanceU3Ek__BackingField_255() const { return ___U3CtauntDistanceU3Ek__BackingField_255; }
	inline float* get_address_of_U3CtauntDistanceU3Ek__BackingField_255() { return &___U3CtauntDistanceU3Ek__BackingField_255; }
	inline void set_U3CtauntDistanceU3Ek__BackingField_255(float value)
	{
		___U3CtauntDistanceU3Ek__BackingField_255 = value;
	}

	inline static int32_t get_offset_of_U3CangerSecDecNumU3Ek__BackingField_256() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CangerSecDecNumU3Ek__BackingField_256)); }
	inline int32_t get_U3CangerSecDecNumU3Ek__BackingField_256() const { return ___U3CangerSecDecNumU3Ek__BackingField_256; }
	inline int32_t* get_address_of_U3CangerSecDecNumU3Ek__BackingField_256() { return &___U3CangerSecDecNumU3Ek__BackingField_256; }
	inline void set_U3CangerSecDecNumU3Ek__BackingField_256(int32_t value)
	{
		___U3CangerSecDecNumU3Ek__BackingField_256 = value;
	}

	inline static int32_t get_offset_of_U3CcurAntiNumU3Ek__BackingField_257() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CcurAntiNumU3Ek__BackingField_257)); }
	inline int32_t get_U3CcurAntiNumU3Ek__BackingField_257() const { return ___U3CcurAntiNumU3Ek__BackingField_257; }
	inline int32_t* get_address_of_U3CcurAntiNumU3Ek__BackingField_257() { return &___U3CcurAntiNumU3Ek__BackingField_257; }
	inline void set_U3CcurAntiNumU3Ek__BackingField_257(int32_t value)
	{
		___U3CcurAntiNumU3Ek__BackingField_257 = value;
	}

	inline static int32_t get_offset_of_U3CinUseAntiNumU3Ek__BackingField_258() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CinUseAntiNumU3Ek__BackingField_258)); }
	inline int32_t get_U3CinUseAntiNumU3Ek__BackingField_258() const { return ___U3CinUseAntiNumU3Ek__BackingField_258; }
	inline int32_t* get_address_of_U3CinUseAntiNumU3Ek__BackingField_258() { return &___U3CinUseAntiNumU3Ek__BackingField_258; }
	inline void set_U3CinUseAntiNumU3Ek__BackingField_258(int32_t value)
	{
		___U3CinUseAntiNumU3Ek__BackingField_258 = value;
	}

	inline static int32_t get_offset_of_U3CcurShieldNumU3Ek__BackingField_259() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CcurShieldNumU3Ek__BackingField_259)); }
	inline int32_t get_U3CcurShieldNumU3Ek__BackingField_259() const { return ___U3CcurShieldNumU3Ek__BackingField_259; }
	inline int32_t* get_address_of_U3CcurShieldNumU3Ek__BackingField_259() { return &___U3CcurShieldNumU3Ek__BackingField_259; }
	inline void set_U3CcurShieldNumU3Ek__BackingField_259(int32_t value)
	{
		___U3CcurShieldNumU3Ek__BackingField_259 = value;
	}

	inline static int32_t get_offset_of_U3ChidePartU3Ek__BackingField_260() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3ChidePartU3Ek__BackingField_260)); }
	inline int32_t get_U3ChidePartU3Ek__BackingField_260() const { return ___U3ChidePartU3Ek__BackingField_260; }
	inline int32_t* get_address_of_U3ChidePartU3Ek__BackingField_260() { return &___U3ChidePartU3Ek__BackingField_260; }
	inline void set_U3ChidePartU3Ek__BackingField_260(int32_t value)
	{
		___U3ChidePartU3Ek__BackingField_260 = value;
	}

	inline static int32_t get_offset_of_U3CrestraintCountryTypeU3Ek__BackingField_261() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CrestraintCountryTypeU3Ek__BackingField_261)); }
	inline int32_t get_U3CrestraintCountryTypeU3Ek__BackingField_261() const { return ___U3CrestraintCountryTypeU3Ek__BackingField_261; }
	inline int32_t* get_address_of_U3CrestraintCountryTypeU3Ek__BackingField_261() { return &___U3CrestraintCountryTypeU3Ek__BackingField_261; }
	inline void set_U3CrestraintCountryTypeU3Ek__BackingField_261(int32_t value)
	{
		___U3CrestraintCountryTypeU3Ek__BackingField_261 = value;
	}

	inline static int32_t get_offset_of_U3CrestraintCountryPerU3Ek__BackingField_262() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CrestraintCountryPerU3Ek__BackingField_262)); }
	inline float get_U3CrestraintCountryPerU3Ek__BackingField_262() const { return ___U3CrestraintCountryPerU3Ek__BackingField_262; }
	inline float* get_address_of_U3CrestraintCountryPerU3Ek__BackingField_262() { return &___U3CrestraintCountryPerU3Ek__BackingField_262; }
	inline void set_U3CrestraintCountryPerU3Ek__BackingField_262(float value)
	{
		___U3CrestraintCountryPerU3Ek__BackingField_262 = value;
	}

	inline static int32_t get_offset_of_U3CrestraintStateTypeU3Ek__BackingField_263() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CrestraintStateTypeU3Ek__BackingField_263)); }
	inline int32_t get_U3CrestraintStateTypeU3Ek__BackingField_263() const { return ___U3CrestraintStateTypeU3Ek__BackingField_263; }
	inline int32_t* get_address_of_U3CrestraintStateTypeU3Ek__BackingField_263() { return &___U3CrestraintStateTypeU3Ek__BackingField_263; }
	inline void set_U3CrestraintStateTypeU3Ek__BackingField_263(int32_t value)
	{
		___U3CrestraintStateTypeU3Ek__BackingField_263 = value;
	}

	inline static int32_t get_offset_of_U3CrestraintStatePerU3Ek__BackingField_264() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CrestraintStatePerU3Ek__BackingField_264)); }
	inline float get_U3CrestraintStatePerU3Ek__BackingField_264() const { return ___U3CrestraintStatePerU3Ek__BackingField_264; }
	inline float* get_address_of_U3CrestraintStatePerU3Ek__BackingField_264() { return &___U3CrestraintStatePerU3Ek__BackingField_264; }
	inline void set_U3CrestraintStatePerU3Ek__BackingField_264(float value)
	{
		___U3CrestraintStatePerU3Ek__BackingField_264 = value;
	}

	inline static int32_t get_offset_of_U3CNDPAddBuffU3Ek__BackingField_265() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CNDPAddBuffU3Ek__BackingField_265)); }
	inline int32_t get_U3CNDPAddBuffU3Ek__BackingField_265() const { return ___U3CNDPAddBuffU3Ek__BackingField_265; }
	inline int32_t* get_address_of_U3CNDPAddBuffU3Ek__BackingField_265() { return &___U3CNDPAddBuffU3Ek__BackingField_265; }
	inline void set_U3CNDPAddBuffU3Ek__BackingField_265(int32_t value)
	{
		___U3CNDPAddBuffU3Ek__BackingField_265 = value;
	}

	inline static int32_t get_offset_of_U3CNDPAddBuff02U3Ek__BackingField_266() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CNDPAddBuff02U3Ek__BackingField_266)); }
	inline int32_t get_U3CNDPAddBuff02U3Ek__BackingField_266() const { return ___U3CNDPAddBuff02U3Ek__BackingField_266; }
	inline int32_t* get_address_of_U3CNDPAddBuff02U3Ek__BackingField_266() { return &___U3CNDPAddBuff02U3Ek__BackingField_266; }
	inline void set_U3CNDPAddBuff02U3Ek__BackingField_266(int32_t value)
	{
		___U3CNDPAddBuff02U3Ek__BackingField_266 = value;
	}

	inline static int32_t get_offset_of_U3CBeAttackedLaterSkillIDU3Ek__BackingField_267() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CBeAttackedLaterSkillIDU3Ek__BackingField_267)); }
	inline int32_t get_U3CBeAttackedLaterSkillIDU3Ek__BackingField_267() const { return ___U3CBeAttackedLaterSkillIDU3Ek__BackingField_267; }
	inline int32_t* get_address_of_U3CBeAttackedLaterSkillIDU3Ek__BackingField_267() { return &___U3CBeAttackedLaterSkillIDU3Ek__BackingField_267; }
	inline void set_U3CBeAttackedLaterSkillIDU3Ek__BackingField_267(int32_t value)
	{
		___U3CBeAttackedLaterSkillIDU3Ek__BackingField_267 = value;
	}

	inline static int32_t get_offset_of_U3CBeAttackedLaterEffectIDU3Ek__BackingField_268() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CBeAttackedLaterEffectIDU3Ek__BackingField_268)); }
	inline int32_t get_U3CBeAttackedLaterEffectIDU3Ek__BackingField_268() const { return ___U3CBeAttackedLaterEffectIDU3Ek__BackingField_268; }
	inline int32_t* get_address_of_U3CBeAttackedLaterEffectIDU3Ek__BackingField_268() { return &___U3CBeAttackedLaterEffectIDU3Ek__BackingField_268; }
	inline void set_U3CBeAttackedLaterEffectIDU3Ek__BackingField_268(int32_t value)
	{
		___U3CBeAttackedLaterEffectIDU3Ek__BackingField_268 = value;
	}

	inline static int32_t get_offset_of_U3CDeathLaterEffectIDU3Ek__BackingField_269() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CDeathLaterEffectIDU3Ek__BackingField_269)); }
	inline int32_t get_U3CDeathLaterEffectIDU3Ek__BackingField_269() const { return ___U3CDeathLaterEffectIDU3Ek__BackingField_269; }
	inline int32_t* get_address_of_U3CDeathLaterEffectIDU3Ek__BackingField_269() { return &___U3CDeathLaterEffectIDU3Ek__BackingField_269; }
	inline void set_U3CDeathLaterEffectIDU3Ek__BackingField_269(int32_t value)
	{
		___U3CDeathLaterEffectIDU3Ek__BackingField_269 = value;
	}

	inline static int32_t get_offset_of_U3CDeathLaterHeroIDU3Ek__BackingField_270() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CDeathLaterHeroIDU3Ek__BackingField_270)); }
	inline int32_t get_U3CDeathLaterHeroIDU3Ek__BackingField_270() const { return ___U3CDeathLaterHeroIDU3Ek__BackingField_270; }
	inline int32_t* get_address_of_U3CDeathLaterHeroIDU3Ek__BackingField_270() { return &___U3CDeathLaterHeroIDU3Ek__BackingField_270; }
	inline void set_U3CDeathLaterHeroIDU3Ek__BackingField_270(int32_t value)
	{
		___U3CDeathLaterHeroIDU3Ek__BackingField_270 = value;
	}

	inline static int32_t get_offset_of_U3CBeattackedLaterSpeedU3Ek__BackingField_271() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CBeattackedLaterSpeedU3Ek__BackingField_271)); }
	inline int32_t get_U3CBeattackedLaterSpeedU3Ek__BackingField_271() const { return ___U3CBeattackedLaterSpeedU3Ek__BackingField_271; }
	inline int32_t* get_address_of_U3CBeattackedLaterSpeedU3Ek__BackingField_271() { return &___U3CBeattackedLaterSpeedU3Ek__BackingField_271; }
	inline void set_U3CBeattackedLaterSpeedU3Ek__BackingField_271(int32_t value)
	{
		___U3CBeattackedLaterSpeedU3Ek__BackingField_271 = value;
	}

	inline static int32_t get_offset_of_U3CDeathLaterSpeedU3Ek__BackingField_272() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CDeathLaterSpeedU3Ek__BackingField_272)); }
	inline int32_t get_U3CDeathLaterSpeedU3Ek__BackingField_272() const { return ___U3CDeathLaterSpeedU3Ek__BackingField_272; }
	inline int32_t* get_address_of_U3CDeathLaterSpeedU3Ek__BackingField_272() { return &___U3CDeathLaterSpeedU3Ek__BackingField_272; }
	inline void set_U3CDeathLaterSpeedU3Ek__BackingField_272(int32_t value)
	{
		___U3CDeathLaterSpeedU3Ek__BackingField_272 = value;
	}

	inline static int32_t get_offset_of_U3CAntiEndEffectIDU3Ek__BackingField_273() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CAntiEndEffectIDU3Ek__BackingField_273)); }
	inline int32_t get_U3CAntiEndEffectIDU3Ek__BackingField_273() const { return ___U3CAntiEndEffectIDU3Ek__BackingField_273; }
	inline int32_t* get_address_of_U3CAntiEndEffectIDU3Ek__BackingField_273() { return &___U3CAntiEndEffectIDU3Ek__BackingField_273; }
	inline void set_U3CAntiEndEffectIDU3Ek__BackingField_273(int32_t value)
	{
		___U3CAntiEndEffectIDU3Ek__BackingField_273 = value;
	}

	inline static int32_t get_offset_of_U3CAuraAddHPRadiusU3Ek__BackingField_274() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CAuraAddHPRadiusU3Ek__BackingField_274)); }
	inline int32_t get_U3CAuraAddHPRadiusU3Ek__BackingField_274() const { return ___U3CAuraAddHPRadiusU3Ek__BackingField_274; }
	inline int32_t* get_address_of_U3CAuraAddHPRadiusU3Ek__BackingField_274() { return &___U3CAuraAddHPRadiusU3Ek__BackingField_274; }
	inline void set_U3CAuraAddHPRadiusU3Ek__BackingField_274(int32_t value)
	{
		___U3CAuraAddHPRadiusU3Ek__BackingField_274 = value;
	}

	inline static int32_t get_offset_of_U3CAuraAddHPNumU3Ek__BackingField_275() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CAuraAddHPNumU3Ek__BackingField_275)); }
	inline int32_t get_U3CAuraAddHPNumU3Ek__BackingField_275() const { return ___U3CAuraAddHPNumU3Ek__BackingField_275; }
	inline int32_t* get_address_of_U3CAuraAddHPNumU3Ek__BackingField_275() { return &___U3CAuraAddHPNumU3Ek__BackingField_275; }
	inline void set_U3CAuraAddHPNumU3Ek__BackingField_275(int32_t value)
	{
		___U3CAuraAddHPNumU3Ek__BackingField_275 = value;
	}

	inline static int32_t get_offset_of_U3CAuraAddHPPerU3Ek__BackingField_276() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CAuraAddHPPerU3Ek__BackingField_276)); }
	inline int32_t get_U3CAuraAddHPPerU3Ek__BackingField_276() const { return ___U3CAuraAddHPPerU3Ek__BackingField_276; }
	inline int32_t* get_address_of_U3CAuraAddHPPerU3Ek__BackingField_276() { return &___U3CAuraAddHPPerU3Ek__BackingField_276; }
	inline void set_U3CAuraAddHPPerU3Ek__BackingField_276(int32_t value)
	{
		___U3CAuraAddHPPerU3Ek__BackingField_276 = value;
	}

	inline static int32_t get_offset_of_U3CAuraReduceHPRadiusU3Ek__BackingField_277() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CAuraReduceHPRadiusU3Ek__BackingField_277)); }
	inline int32_t get_U3CAuraReduceHPRadiusU3Ek__BackingField_277() const { return ___U3CAuraReduceHPRadiusU3Ek__BackingField_277; }
	inline int32_t* get_address_of_U3CAuraReduceHPRadiusU3Ek__BackingField_277() { return &___U3CAuraReduceHPRadiusU3Ek__BackingField_277; }
	inline void set_U3CAuraReduceHPRadiusU3Ek__BackingField_277(int32_t value)
	{
		___U3CAuraReduceHPRadiusU3Ek__BackingField_277 = value;
	}

	inline static int32_t get_offset_of_U3CAuraReduceHPNumU3Ek__BackingField_278() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CAuraReduceHPNumU3Ek__BackingField_278)); }
	inline int32_t get_U3CAuraReduceHPNumU3Ek__BackingField_278() const { return ___U3CAuraReduceHPNumU3Ek__BackingField_278; }
	inline int32_t* get_address_of_U3CAuraReduceHPNumU3Ek__BackingField_278() { return &___U3CAuraReduceHPNumU3Ek__BackingField_278; }
	inline void set_U3CAuraReduceHPNumU3Ek__BackingField_278(int32_t value)
	{
		___U3CAuraReduceHPNumU3Ek__BackingField_278 = value;
	}

	inline static int32_t get_offset_of_U3CAuraReduceHPPerU3Ek__BackingField_279() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CAuraReduceHPPerU3Ek__BackingField_279)); }
	inline int32_t get_U3CAuraReduceHPPerU3Ek__BackingField_279() const { return ___U3CAuraReduceHPPerU3Ek__BackingField_279; }
	inline int32_t* get_address_of_U3CAuraReduceHPPerU3Ek__BackingField_279() { return &___U3CAuraReduceHPPerU3Ek__BackingField_279; }
	inline void set_U3CAuraReduceHPPerU3Ek__BackingField_279(int32_t value)
	{
		___U3CAuraReduceHPPerU3Ek__BackingField_279 = value;
	}

	inline static int32_t get_offset_of_U3CActiveSkillDamageAddPerU3Ek__BackingField_280() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CActiveSkillDamageAddPerU3Ek__BackingField_280)); }
	inline float get_U3CActiveSkillDamageAddPerU3Ek__BackingField_280() const { return ___U3CActiveSkillDamageAddPerU3Ek__BackingField_280; }
	inline float* get_address_of_U3CActiveSkillDamageAddPerU3Ek__BackingField_280() { return &___U3CActiveSkillDamageAddPerU3Ek__BackingField_280; }
	inline void set_U3CActiveSkillDamageAddPerU3Ek__BackingField_280(float value)
	{
		___U3CActiveSkillDamageAddPerU3Ek__BackingField_280 = value;
	}

	inline static int32_t get_offset_of_U3CAddAngerRatioU3Ek__BackingField_281() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CAddAngerRatioU3Ek__BackingField_281)); }
	inline float get_U3CAddAngerRatioU3Ek__BackingField_281() const { return ___U3CAddAngerRatioU3Ek__BackingField_281; }
	inline float* get_address_of_U3CAddAngerRatioU3Ek__BackingField_281() { return &___U3CAddAngerRatioU3Ek__BackingField_281; }
	inline void set_U3CAddAngerRatioU3Ek__BackingField_281(float value)
	{
		___U3CAddAngerRatioU3Ek__BackingField_281 = value;
	}

	inline static int32_t get_offset_of_U3CbeHurtAngerU3Ek__BackingField_282() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CbeHurtAngerU3Ek__BackingField_282)); }
	inline int32_t get_U3CbeHurtAngerU3Ek__BackingField_282() const { return ___U3CbeHurtAngerU3Ek__BackingField_282; }
	inline int32_t* get_address_of_U3CbeHurtAngerU3Ek__BackingField_282() { return &___U3CbeHurtAngerU3Ek__BackingField_282; }
	inline void set_U3CbeHurtAngerU3Ek__BackingField_282(int32_t value)
	{
		___U3CbeHurtAngerU3Ek__BackingField_282 = value;
	}

	inline static int32_t get_offset_of_U3CsuperArmorU3Ek__BackingField_283() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CsuperArmorU3Ek__BackingField_283)); }
	inline int32_t get_U3CsuperArmorU3Ek__BackingField_283() const { return ___U3CsuperArmorU3Ek__BackingField_283; }
	inline int32_t* get_address_of_U3CsuperArmorU3Ek__BackingField_283() { return &___U3CsuperArmorU3Ek__BackingField_283; }
	inline void set_U3CsuperArmorU3Ek__BackingField_283(int32_t value)
	{
		___U3CsuperArmorU3Ek__BackingField_283 = value;
	}

	inline static int32_t get_offset_of_U3CsuperArmorTimeU3Ek__BackingField_284() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CsuperArmorTimeU3Ek__BackingField_284)); }
	inline float get_U3CsuperArmorTimeU3Ek__BackingField_284() const { return ___U3CsuperArmorTimeU3Ek__BackingField_284; }
	inline float* get_address_of_U3CsuperArmorTimeU3Ek__BackingField_284() { return &___U3CsuperArmorTimeU3Ek__BackingField_284; }
	inline void set_U3CsuperArmorTimeU3Ek__BackingField_284(float value)
	{
		___U3CsuperArmorTimeU3Ek__BackingField_284 = value;
	}

	inline static int32_t get_offset_of_U3ChpLineNumU3Ek__BackingField_285() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3ChpLineNumU3Ek__BackingField_285)); }
	inline int32_t get_U3ChpLineNumU3Ek__BackingField_285() const { return ___U3ChpLineNumU3Ek__BackingField_285; }
	inline int32_t* get_address_of_U3ChpLineNumU3Ek__BackingField_285() { return &___U3ChpLineNumU3Ek__BackingField_285; }
	inline void set_U3ChpLineNumU3Ek__BackingField_285(int32_t value)
	{
		___U3ChpLineNumU3Ek__BackingField_285 = value;
	}

	inline static int32_t get_offset_of_U3ChpLineBuffIDU3Ek__BackingField_286() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3ChpLineBuffIDU3Ek__BackingField_286)); }
	inline int32_t get_U3ChpLineBuffIDU3Ek__BackingField_286() const { return ___U3ChpLineBuffIDU3Ek__BackingField_286; }
	inline int32_t* get_address_of_U3ChpLineBuffIDU3Ek__BackingField_286() { return &___U3ChpLineBuffIDU3Ek__BackingField_286; }
	inline void set_U3ChpLineBuffIDU3Ek__BackingField_286(int32_t value)
	{
		___U3ChpLineBuffIDU3Ek__BackingField_286 = value;
	}

	inline static int32_t get_offset_of_U3ChpLineUpBuffIDU3Ek__BackingField_287() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3ChpLineUpBuffIDU3Ek__BackingField_287)); }
	inline int32_t get_U3ChpLineUpBuffIDU3Ek__BackingField_287() const { return ___U3ChpLineUpBuffIDU3Ek__BackingField_287; }
	inline int32_t* get_address_of_U3ChpLineUpBuffIDU3Ek__BackingField_287() { return &___U3ChpLineUpBuffIDU3Ek__BackingField_287; }
	inline void set_U3ChpLineUpBuffIDU3Ek__BackingField_287(int32_t value)
	{
		___U3ChpLineUpBuffIDU3Ek__BackingField_287 = value;
	}

	inline static int32_t get_offset_of_U3ChpLineCleaveAddBuffU3Ek__BackingField_288() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3ChpLineCleaveAddBuffU3Ek__BackingField_288)); }
	inline int32_t get_U3ChpLineCleaveAddBuffU3Ek__BackingField_288() const { return ___U3ChpLineCleaveAddBuffU3Ek__BackingField_288; }
	inline int32_t* get_address_of_U3ChpLineCleaveAddBuffU3Ek__BackingField_288() { return &___U3ChpLineCleaveAddBuffU3Ek__BackingField_288; }
	inline void set_U3ChpLineCleaveAddBuffU3Ek__BackingField_288(int32_t value)
	{
		___U3ChpLineCleaveAddBuffU3Ek__BackingField_288 = value;
	}

	inline static int32_t get_offset_of_U3ChpLineUpFrameCountU3Ek__BackingField_289() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3ChpLineUpFrameCountU3Ek__BackingField_289)); }
	inline int32_t get_U3ChpLineUpFrameCountU3Ek__BackingField_289() const { return ___U3ChpLineUpFrameCountU3Ek__BackingField_289; }
	inline int32_t* get_address_of_U3ChpLineUpFrameCountU3Ek__BackingField_289() { return &___U3ChpLineUpFrameCountU3Ek__BackingField_289; }
	inline void set_U3ChpLineUpFrameCountU3Ek__BackingField_289(int32_t value)
	{
		___U3ChpLineUpFrameCountU3Ek__BackingField_289 = value;
	}

	inline static int32_t get_offset_of_U3CcontrolBuffTimeBonusU3Ek__BackingField_290() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CcontrolBuffTimeBonusU3Ek__BackingField_290)); }
	inline float get_U3CcontrolBuffTimeBonusU3Ek__BackingField_290() const { return ___U3CcontrolBuffTimeBonusU3Ek__BackingField_290; }
	inline float* get_address_of_U3CcontrolBuffTimeBonusU3Ek__BackingField_290() { return &___U3CcontrolBuffTimeBonusU3Ek__BackingField_290; }
	inline void set_U3CcontrolBuffTimeBonusU3Ek__BackingField_290(float value)
	{
		___U3CcontrolBuffTimeBonusU3Ek__BackingField_290 = value;
	}

	inline static int32_t get_offset_of_U3CcontrolBuffTimeBonusPerU3Ek__BackingField_291() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CcontrolBuffTimeBonusPerU3Ek__BackingField_291)); }
	inline float get_U3CcontrolBuffTimeBonusPerU3Ek__BackingField_291() const { return ___U3CcontrolBuffTimeBonusPerU3Ek__BackingField_291; }
	inline float* get_address_of_U3CcontrolBuffTimeBonusPerU3Ek__BackingField_291() { return &___U3CcontrolBuffTimeBonusPerU3Ek__BackingField_291; }
	inline void set_U3CcontrolBuffTimeBonusPerU3Ek__BackingField_291(float value)
	{
		___U3CcontrolBuffTimeBonusPerU3Ek__BackingField_291 = value;
	}

	inline static int32_t get_offset_of_U3CruneNumU3Ek__BackingField_292() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CruneNumU3Ek__BackingField_292)); }
	inline int32_t get_U3CruneNumU3Ek__BackingField_292() const { return ___U3CruneNumU3Ek__BackingField_292; }
	inline int32_t* get_address_of_U3CruneNumU3Ek__BackingField_292() { return &___U3CruneNumU3Ek__BackingField_292; }
	inline void set_U3CruneNumU3Ek__BackingField_292(int32_t value)
	{
		___U3CruneNumU3Ek__BackingField_292 = value;
	}

	inline static int32_t get_offset_of_U3CruneNumAllSkillU3Ek__BackingField_293() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CruneNumAllSkillU3Ek__BackingField_293)); }
	inline int32_t get_U3CruneNumAllSkillU3Ek__BackingField_293() const { return ___U3CruneNumAllSkillU3Ek__BackingField_293; }
	inline int32_t* get_address_of_U3CruneNumAllSkillU3Ek__BackingField_293() { return &___U3CruneNumAllSkillU3Ek__BackingField_293; }
	inline void set_U3CruneNumAllSkillU3Ek__BackingField_293(int32_t value)
	{
		___U3CruneNumAllSkillU3Ek__BackingField_293 = value;
	}

	inline static int32_t get_offset_of_U3CruneDisappearTimeU3Ek__BackingField_294() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CruneDisappearTimeU3Ek__BackingField_294)); }
	inline int32_t get_U3CruneDisappearTimeU3Ek__BackingField_294() const { return ___U3CruneDisappearTimeU3Ek__BackingField_294; }
	inline int32_t* get_address_of_U3CruneDisappearTimeU3Ek__BackingField_294() { return &___U3CruneDisappearTimeU3Ek__BackingField_294; }
	inline void set_U3CruneDisappearTimeU3Ek__BackingField_294(int32_t value)
	{
		___U3CruneDisappearTimeU3Ek__BackingField_294 = value;
	}

	inline static int32_t get_offset_of_U3CruneTakeEffectNumU3Ek__BackingField_295() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CruneTakeEffectNumU3Ek__BackingField_295)); }
	inline int32_t get_U3CruneTakeEffectNumU3Ek__BackingField_295() const { return ___U3CruneTakeEffectNumU3Ek__BackingField_295; }
	inline int32_t* get_address_of_U3CruneTakeEffectNumU3Ek__BackingField_295() { return &___U3CruneTakeEffectNumU3Ek__BackingField_295; }
	inline void set_U3CruneTakeEffectNumU3Ek__BackingField_295(int32_t value)
	{
		___U3CruneTakeEffectNumU3Ek__BackingField_295 = value;
	}

	inline static int32_t get_offset_of_U3CruneTakeEffectBuffIDU3Ek__BackingField_296() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CruneTakeEffectBuffIDU3Ek__BackingField_296)); }
	inline int32_t get_U3CruneTakeEffectBuffIDU3Ek__BackingField_296() const { return ___U3CruneTakeEffectBuffIDU3Ek__BackingField_296; }
	inline int32_t* get_address_of_U3CruneTakeEffectBuffIDU3Ek__BackingField_296() { return &___U3CruneTakeEffectBuffIDU3Ek__BackingField_296; }
	inline void set_U3CruneTakeEffectBuffIDU3Ek__BackingField_296(int32_t value)
	{
		___U3CruneTakeEffectBuffIDU3Ek__BackingField_296 = value;
	}

	inline static int32_t get_offset_of_U3CruneTakeEffectBuffID01U3Ek__BackingField_297() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CruneTakeEffectBuffID01U3Ek__BackingField_297)); }
	inline int32_t get_U3CruneTakeEffectBuffID01U3Ek__BackingField_297() const { return ___U3CruneTakeEffectBuffID01U3Ek__BackingField_297; }
	inline int32_t* get_address_of_U3CruneTakeEffectBuffID01U3Ek__BackingField_297() { return &___U3CruneTakeEffectBuffID01U3Ek__BackingField_297; }
	inline void set_U3CruneTakeEffectBuffID01U3Ek__BackingField_297(int32_t value)
	{
		___U3CruneTakeEffectBuffID01U3Ek__BackingField_297 = value;
	}

	inline static int32_t get_offset_of_U3CruneTakeEffectBuffID02U3Ek__BackingField_298() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CruneTakeEffectBuffID02U3Ek__BackingField_298)); }
	inline int32_t get_U3CruneTakeEffectBuffID02U3Ek__BackingField_298() const { return ___U3CruneTakeEffectBuffID02U3Ek__BackingField_298; }
	inline int32_t* get_address_of_U3CruneTakeEffectBuffID02U3Ek__BackingField_298() { return &___U3CruneTakeEffectBuffID02U3Ek__BackingField_298; }
	inline void set_U3CruneTakeEffectBuffID02U3Ek__BackingField_298(int32_t value)
	{
		___U3CruneTakeEffectBuffID02U3Ek__BackingField_298 = value;
	}

	inline static int32_t get_offset_of_U3CgodDownTweenTimeU3Ek__BackingField_299() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CgodDownTweenTimeU3Ek__BackingField_299)); }
	inline float get_U3CgodDownTweenTimeU3Ek__BackingField_299() const { return ___U3CgodDownTweenTimeU3Ek__BackingField_299; }
	inline float* get_address_of_U3CgodDownTweenTimeU3Ek__BackingField_299() { return &___U3CgodDownTweenTimeU3Ek__BackingField_299; }
	inline void set_U3CgodDownTweenTimeU3Ek__BackingField_299(float value)
	{
		___U3CgodDownTweenTimeU3Ek__BackingField_299 = value;
	}

	inline static int32_t get_offset_of_U3CantiDebuffDiscreteIDU3Ek__BackingField_300() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CantiDebuffDiscreteIDU3Ek__BackingField_300)); }
	inline int32_t get_U3CantiDebuffDiscreteIDU3Ek__BackingField_300() const { return ___U3CantiDebuffDiscreteIDU3Ek__BackingField_300; }
	inline int32_t* get_address_of_U3CantiDebuffDiscreteIDU3Ek__BackingField_300() { return &___U3CantiDebuffDiscreteIDU3Ek__BackingField_300; }
	inline void set_U3CantiDebuffDiscreteIDU3Ek__BackingField_300(int32_t value)
	{
		___U3CantiDebuffDiscreteIDU3Ek__BackingField_300 = value;
	}

	inline static int32_t get_offset_of_U3CantiDebuffDiscreteAddBuffIDU3Ek__BackingField_301() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CantiDebuffDiscreteAddBuffIDU3Ek__BackingField_301)); }
	inline int32_t get_U3CantiDebuffDiscreteAddBuffIDU3Ek__BackingField_301() const { return ___U3CantiDebuffDiscreteAddBuffIDU3Ek__BackingField_301; }
	inline int32_t* get_address_of_U3CantiDebuffDiscreteAddBuffIDU3Ek__BackingField_301() { return &___U3CantiDebuffDiscreteAddBuffIDU3Ek__BackingField_301; }
	inline void set_U3CantiDebuffDiscreteAddBuffIDU3Ek__BackingField_301(int32_t value)
	{
		___U3CantiDebuffDiscreteAddBuffIDU3Ek__BackingField_301 = value;
	}

	inline static int32_t get_offset_of_U3CdiscreteBuffIDU3Ek__BackingField_302() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CdiscreteBuffIDU3Ek__BackingField_302)); }
	inline int32_t get_U3CdiscreteBuffIDU3Ek__BackingField_302() const { return ___U3CdiscreteBuffIDU3Ek__BackingField_302; }
	inline int32_t* get_address_of_U3CdiscreteBuffIDU3Ek__BackingField_302() { return &___U3CdiscreteBuffIDU3Ek__BackingField_302; }
	inline void set_U3CdiscreteBuffIDU3Ek__BackingField_302(int32_t value)
	{
		___U3CdiscreteBuffIDU3Ek__BackingField_302 = value;
	}

	inline static int32_t get_offset_of_U3CdiscreteBuffTimeU3Ek__BackingField_303() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CdiscreteBuffTimeU3Ek__BackingField_303)); }
	inline float get_U3CdiscreteBuffTimeU3Ek__BackingField_303() const { return ___U3CdiscreteBuffTimeU3Ek__BackingField_303; }
	inline float* get_address_of_U3CdiscreteBuffTimeU3Ek__BackingField_303() { return &___U3CdiscreteBuffTimeU3Ek__BackingField_303; }
	inline void set_U3CdiscreteBuffTimeU3Ek__BackingField_303(float value)
	{
		___U3CdiscreteBuffTimeU3Ek__BackingField_303 = value;
	}

	inline static int32_t get_offset_of_U3CdiscreteBuffTimerU3Ek__BackingField_304() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CdiscreteBuffTimerU3Ek__BackingField_304)); }
	inline float get_U3CdiscreteBuffTimerU3Ek__BackingField_304() const { return ___U3CdiscreteBuffTimerU3Ek__BackingField_304; }
	inline float* get_address_of_U3CdiscreteBuffTimerU3Ek__BackingField_304() { return &___U3CdiscreteBuffTimerU3Ek__BackingField_304; }
	inline void set_U3CdiscreteBuffTimerU3Ek__BackingField_304(float value)
	{
		___U3CdiscreteBuffTimerU3Ek__BackingField_304 = value;
	}

	inline static int32_t get_offset_of_U3CpetrifiedChangeTimeU3Ek__BackingField_305() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CpetrifiedChangeTimeU3Ek__BackingField_305)); }
	inline float get_U3CpetrifiedChangeTimeU3Ek__BackingField_305() const { return ___U3CpetrifiedChangeTimeU3Ek__BackingField_305; }
	inline float* get_address_of_U3CpetrifiedChangeTimeU3Ek__BackingField_305() { return &___U3CpetrifiedChangeTimeU3Ek__BackingField_305; }
	inline void set_U3CpetrifiedChangeTimeU3Ek__BackingField_305(float value)
	{
		___U3CpetrifiedChangeTimeU3Ek__BackingField_305 = value;
	}

	inline static int32_t get_offset_of_U3CchangeOldSkillIDU3Ek__BackingField_306() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CchangeOldSkillIDU3Ek__BackingField_306)); }
	inline int32_t get_U3CchangeOldSkillIDU3Ek__BackingField_306() const { return ___U3CchangeOldSkillIDU3Ek__BackingField_306; }
	inline int32_t* get_address_of_U3CchangeOldSkillIDU3Ek__BackingField_306() { return &___U3CchangeOldSkillIDU3Ek__BackingField_306; }
	inline void set_U3CchangeOldSkillIDU3Ek__BackingField_306(int32_t value)
	{
		___U3CchangeOldSkillIDU3Ek__BackingField_306 = value;
	}

	inline static int32_t get_offset_of_U3CchangeNewSkillIDU3Ek__BackingField_307() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CchangeNewSkillIDU3Ek__BackingField_307)); }
	inline int32_t get_U3CchangeNewSkillIDU3Ek__BackingField_307() const { return ___U3CchangeNewSkillIDU3Ek__BackingField_307; }
	inline int32_t* get_address_of_U3CchangeNewSkillIDU3Ek__BackingField_307() { return &___U3CchangeNewSkillIDU3Ek__BackingField_307; }
	inline void set_U3CchangeNewSkillIDU3Ek__BackingField_307(int32_t value)
	{
		___U3CchangeNewSkillIDU3Ek__BackingField_307 = value;
	}

	inline static int32_t get_offset_of_U3CchangeSkillProU3Ek__BackingField_308() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CchangeSkillProU3Ek__BackingField_308)); }
	inline int32_t get_U3CchangeSkillProU3Ek__BackingField_308() const { return ___U3CchangeSkillProU3Ek__BackingField_308; }
	inline int32_t* get_address_of_U3CchangeSkillProU3Ek__BackingField_308() { return &___U3CchangeSkillProU3Ek__BackingField_308; }
	inline void set_U3CchangeSkillProU3Ek__BackingField_308(int32_t value)
	{
		___U3CchangeSkillProU3Ek__BackingField_308 = value;
	}

	inline static int32_t get_offset_of_U3CwhenCritAddBuffIDU3Ek__BackingField_309() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CwhenCritAddBuffIDU3Ek__BackingField_309)); }
	inline int32_t get_U3CwhenCritAddBuffIDU3Ek__BackingField_309() const { return ___U3CwhenCritAddBuffIDU3Ek__BackingField_309; }
	inline int32_t* get_address_of_U3CwhenCritAddBuffIDU3Ek__BackingField_309() { return &___U3CwhenCritAddBuffIDU3Ek__BackingField_309; }
	inline void set_U3CwhenCritAddBuffIDU3Ek__BackingField_309(int32_t value)
	{
		___U3CwhenCritAddBuffIDU3Ek__BackingField_309 = value;
	}

	inline static int32_t get_offset_of_U3CthumpSkillIDU3Ek__BackingField_310() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CthumpSkillIDU3Ek__BackingField_310)); }
	inline int32_t get_U3CthumpSkillIDU3Ek__BackingField_310() const { return ___U3CthumpSkillIDU3Ek__BackingField_310; }
	inline int32_t* get_address_of_U3CthumpSkillIDU3Ek__BackingField_310() { return &___U3CthumpSkillIDU3Ek__BackingField_310; }
	inline void set_U3CthumpSkillIDU3Ek__BackingField_310(int32_t value)
	{
		___U3CthumpSkillIDU3Ek__BackingField_310 = value;
	}

	inline static int32_t get_offset_of_U3CthumpAddValueU3Ek__BackingField_311() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CthumpAddValueU3Ek__BackingField_311)); }
	inline float get_U3CthumpAddValueU3Ek__BackingField_311() const { return ___U3CthumpAddValueU3Ek__BackingField_311; }
	inline float* get_address_of_U3CthumpAddValueU3Ek__BackingField_311() { return &___U3CthumpAddValueU3Ek__BackingField_311; }
	inline void set_U3CthumpAddValueU3Ek__BackingField_311(float value)
	{
		___U3CthumpAddValueU3Ek__BackingField_311 = value;
	}

	inline static int32_t get_offset_of_U3CtankBuffIDU3Ek__BackingField_312() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CtankBuffIDU3Ek__BackingField_312)); }
	inline int32_t get_U3CtankBuffIDU3Ek__BackingField_312() const { return ___U3CtankBuffIDU3Ek__BackingField_312; }
	inline int32_t* get_address_of_U3CtankBuffIDU3Ek__BackingField_312() { return &___U3CtankBuffIDU3Ek__BackingField_312; }
	inline void set_U3CtankBuffIDU3Ek__BackingField_312(int32_t value)
	{
		___U3CtankBuffIDU3Ek__BackingField_312 = value;
	}

	inline static int32_t get_offset_of_U3CHTBuffIDU3Ek__BackingField_313() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CHTBuffIDU3Ek__BackingField_313)); }
	inline int32_t get_U3CHTBuffIDU3Ek__BackingField_313() const { return ___U3CHTBuffIDU3Ek__BackingField_313; }
	inline int32_t* get_address_of_U3CHTBuffIDU3Ek__BackingField_313() { return &___U3CHTBuffIDU3Ek__BackingField_313; }
	inline void set_U3CHTBuffIDU3Ek__BackingField_313(int32_t value)
	{
		___U3CHTBuffIDU3Ek__BackingField_313 = value;
	}

	inline static int32_t get_offset_of_U3CcleaveSkillIDU3Ek__BackingField_314() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CcleaveSkillIDU3Ek__BackingField_314)); }
	inline int32_t get_U3CcleaveSkillIDU3Ek__BackingField_314() const { return ___U3CcleaveSkillIDU3Ek__BackingField_314; }
	inline int32_t* get_address_of_U3CcleaveSkillIDU3Ek__BackingField_314() { return &___U3CcleaveSkillIDU3Ek__BackingField_314; }
	inline void set_U3CcleaveSkillIDU3Ek__BackingField_314(int32_t value)
	{
		___U3CcleaveSkillIDU3Ek__BackingField_314 = value;
	}

	inline static int32_t get_offset_of_U3CcleaveBuffIDU3Ek__BackingField_315() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CcleaveBuffIDU3Ek__BackingField_315)); }
	inline int32_t get_U3CcleaveBuffIDU3Ek__BackingField_315() const { return ___U3CcleaveBuffIDU3Ek__BackingField_315; }
	inline int32_t* get_address_of_U3CcleaveBuffIDU3Ek__BackingField_315() { return &___U3CcleaveBuffIDU3Ek__BackingField_315; }
	inline void set_U3CcleaveBuffIDU3Ek__BackingField_315(int32_t value)
	{
		___U3CcleaveBuffIDU3Ek__BackingField_315 = value;
	}

	inline static int32_t get_offset_of_U3CreduceRageNumU3Ek__BackingField_316() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CreduceRageNumU3Ek__BackingField_316)); }
	inline int32_t get_U3CreduceRageNumU3Ek__BackingField_316() const { return ___U3CreduceRageNumU3Ek__BackingField_316; }
	inline int32_t* get_address_of_U3CreduceRageNumU3Ek__BackingField_316() { return &___U3CreduceRageNumU3Ek__BackingField_316; }
	inline void set_U3CreduceRageNumU3Ek__BackingField_316(int32_t value)
	{
		___U3CreduceRageNumU3Ek__BackingField_316 = value;
	}

	inline static int32_t get_offset_of_U3CdeathHarvestLineU3Ek__BackingField_317() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CdeathHarvestLineU3Ek__BackingField_317)); }
	inline int32_t get_U3CdeathHarvestLineU3Ek__BackingField_317() const { return ___U3CdeathHarvestLineU3Ek__BackingField_317; }
	inline int32_t* get_address_of_U3CdeathHarvestLineU3Ek__BackingField_317() { return &___U3CdeathHarvestLineU3Ek__BackingField_317; }
	inline void set_U3CdeathHarvestLineU3Ek__BackingField_317(int32_t value)
	{
		___U3CdeathHarvestLineU3Ek__BackingField_317 = value;
	}

	inline static int32_t get_offset_of_U3CdeathHarvestPerU3Ek__BackingField_318() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CdeathHarvestPerU3Ek__BackingField_318)); }
	inline int32_t get_U3CdeathHarvestPerU3Ek__BackingField_318() const { return ___U3CdeathHarvestPerU3Ek__BackingField_318; }
	inline int32_t* get_address_of_U3CdeathHarvestPerU3Ek__BackingField_318() { return &___U3CdeathHarvestPerU3Ek__BackingField_318; }
	inline void set_U3CdeathHarvestPerU3Ek__BackingField_318(int32_t value)
	{
		___U3CdeathHarvestPerU3Ek__BackingField_318 = value;
	}

	inline static int32_t get_offset_of_U3CresistanceCountryTypeU3Ek__BackingField_319() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CresistanceCountryTypeU3Ek__BackingField_319)); }
	inline int32_t get_U3CresistanceCountryTypeU3Ek__BackingField_319() const { return ___U3CresistanceCountryTypeU3Ek__BackingField_319; }
	inline int32_t* get_address_of_U3CresistanceCountryTypeU3Ek__BackingField_319() { return &___U3CresistanceCountryTypeU3Ek__BackingField_319; }
	inline void set_U3CresistanceCountryTypeU3Ek__BackingField_319(int32_t value)
	{
		___U3CresistanceCountryTypeU3Ek__BackingField_319 = value;
	}

	inline static int32_t get_offset_of_U3CresistanceCountryPerU3Ek__BackingField_320() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495, ___U3CresistanceCountryPerU3Ek__BackingField_320)); }
	inline float get_U3CresistanceCountryPerU3Ek__BackingField_320() const { return ___U3CresistanceCountryPerU3Ek__BackingField_320; }
	inline float* get_address_of_U3CresistanceCountryPerU3Ek__BackingField_320() { return &___U3CresistanceCountryPerU3Ek__BackingField_320; }
	inline void set_U3CresistanceCountryPerU3Ek__BackingField_320(float value)
	{
		___U3CresistanceCountryPerU3Ek__BackingField_320 = value;
	}
};

struct CombatEntity_t684137495_StaticFields
{
public:
	// UnityEngine.GameObject CombatEntity::_moveTargetRoot
	GameObject_t3674682005 * ____moveTargetRoot_36;
	// System.Int32 CombatEntity::layermask
	int32_t ___layermask_39;

public:
	inline static int32_t get_offset_of__moveTargetRoot_36() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495_StaticFields, ____moveTargetRoot_36)); }
	inline GameObject_t3674682005 * get__moveTargetRoot_36() const { return ____moveTargetRoot_36; }
	inline GameObject_t3674682005 ** get_address_of__moveTargetRoot_36() { return &____moveTargetRoot_36; }
	inline void set__moveTargetRoot_36(GameObject_t3674682005 * value)
	{
		____moveTargetRoot_36 = value;
		Il2CppCodeGenWriteBarrier(&____moveTargetRoot_36, value);
	}

	inline static int32_t get_offset_of_layermask_39() { return static_cast<int32_t>(offsetof(CombatEntity_t684137495_StaticFields, ___layermask_39)); }
	inline int32_t get_layermask_39() const { return ___layermask_39; }
	inline int32_t* get_address_of_layermask_39() { return &___layermask_39; }
	inline void set_layermask_39(int32_t value)
	{
		___layermask_39 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
