﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Linq.JTokenReader
struct JTokenReader_t3546959010;
// Newtonsoft.Json.Linq.JToken
struct JToken_t3412245951;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// Newtonsoft.Json.Linq.JContainer
struct JContainer_t3364442311;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// Newtonsoft.Json.IJsonLineInfo
struct IJsonLineInfo_t1008519681;
// Newtonsoft.Json.JsonReader
struct JsonReader_t816925123;
// System.IFormatProvider
struct IFormatProvider_t192740775;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// Newtonsoft.Json.Linq.JProperty
struct JProperty_t2616415645;
// Newtonsoft.Json.Linq.JValue
struct JValue_t3413677367;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JToken3412245951.h"
#include "mscorlib_System_Nullable_1_gen2038477154.h"
#include "mscorlib_System_Nullable_1_gen3968840829.h"
#include "mscorlib_System_Nullable_1_gen4257204698.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JContainer3364442311.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonReader_State2338717890.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonReader816925123.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JTokenReade3546959010.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonToken4173078175.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JTokenType3916897561.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JProperty2616415645.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JValue3413677367.h"

// System.Void Newtonsoft.Json.Linq.JTokenReader::.ctor(Newtonsoft.Json.Linq.JToken)
extern "C"  void JTokenReader__ctor_m3146962954 (JTokenReader_t3546959010 * __this, JToken_t3412245951 * ___token0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Linq.JTokenReader::Newtonsoft.Json.IJsonLineInfo.HasLineInfo()
extern "C"  bool JTokenReader_Newtonsoft_Json_IJsonLineInfo_HasLineInfo_m3600556638 (JTokenReader_t3546959010 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Newtonsoft.Json.Linq.JTokenReader::Newtonsoft.Json.IJsonLineInfo.get_LineNumber()
extern "C"  int32_t JTokenReader_Newtonsoft_Json_IJsonLineInfo_get_LineNumber_m3610966988 (JTokenReader_t3546959010 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Newtonsoft.Json.Linq.JTokenReader::Newtonsoft.Json.IJsonLineInfo.get_LinePosition()
extern "C"  int32_t JTokenReader_Newtonsoft_Json_IJsonLineInfo_get_LinePosition_m1580292396 (JTokenReader_t3546959010 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Newtonsoft.Json.Linq.JTokenReader::ReadAsBytes()
extern "C"  ByteU5BU5D_t4260760469* JTokenReader_ReadAsBytes_m2952856715 (JTokenReader_t3546959010 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Decimal> Newtonsoft.Json.Linq.JTokenReader::ReadAsDecimal()
extern "C"  Nullable_1_t2038477154  JTokenReader_ReadAsDecimal_m1214326905 (JTokenReader_t3546959010 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.DateTimeOffset> Newtonsoft.Json.Linq.JTokenReader::ReadAsDateTimeOffset()
extern "C"  Nullable_1_t3968840829  JTokenReader_ReadAsDateTimeOffset_m597993817 (JTokenReader_t3546959010 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Linq.JTokenReader::Read()
extern "C"  bool JTokenReader_Read_m2443153158 (JTokenReader_t3546959010 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Linq.JTokenReader::ReadOver(Newtonsoft.Json.Linq.JToken)
extern "C"  bool JTokenReader_ReadOver_m2908174704 (JTokenReader_t3546959010 * __this, JToken_t3412245951 * ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Linq.JTokenReader::ReadToEnd()
extern "C"  bool JTokenReader_ReadToEnd_m1006903900 (JTokenReader_t3546959010 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Linq.JTokenReader::get_IsEndElement()
extern "C"  bool JTokenReader_get_IsEndElement_m4037807140 (JTokenReader_t3546959010 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<Newtonsoft.Json.JsonToken> Newtonsoft.Json.Linq.JTokenReader::GetEndToken(Newtonsoft.Json.Linq.JContainer)
extern "C"  Nullable_1_t4257204698  JTokenReader_GetEndToken_m4080662016 (JTokenReader_t3546959010 * __this, JContainer_t3364442311 * ___c0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Linq.JTokenReader::ReadInto(Newtonsoft.Json.Linq.JContainer)
extern "C"  bool JTokenReader_ReadInto_m2423463900 (JTokenReader_t3546959010 * __this, JContainer_t3364442311 * ___c0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Linq.JTokenReader::SetEnd(Newtonsoft.Json.Linq.JContainer)
extern "C"  bool JTokenReader_SetEnd_m2814491769 (JTokenReader_t3546959010 * __this, JContainer_t3364442311 * ___c0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JTokenReader::SetToken(Newtonsoft.Json.Linq.JToken)
extern "C"  void JTokenReader_SetToken_m662224559 (JTokenReader_t3546959010 * __this, JToken_t3412245951 * ___token0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Linq.JTokenReader::SafeToString(System.Object)
extern "C"  String_t* JTokenReader_SafeToString_m2278660250 (JTokenReader_t3546959010 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Linq.JTokenReader::ilo_HasLineInfo1(Newtonsoft.Json.IJsonLineInfo)
extern "C"  bool JTokenReader_ilo_HasLineInfo1_m4096833212 (Il2CppObject * __this /* static, unused */, Il2CppObject * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.JsonReader/State Newtonsoft.Json.Linq.JTokenReader::ilo_get_CurrentState2(Newtonsoft.Json.JsonReader)
extern "C"  int32_t JTokenReader_ilo_get_CurrentState2_m3919234103 (Il2CppObject * __this /* static, unused */, JsonReader_t816925123 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Linq.JTokenReader::ilo_get_IsEndElement3(Newtonsoft.Json.Linq.JTokenReader)
extern "C"  bool JTokenReader_ilo_get_IsEndElement3_m1702069385 (Il2CppObject * __this /* static, unused */, JTokenReader_t3546959010 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Newtonsoft.Json.Linq.JTokenReader::ilo_get_LineNumber4(Newtonsoft.Json.IJsonLineInfo)
extern "C"  int32_t JTokenReader_ilo_get_LineNumber4_m852035117 (Il2CppObject * __this /* static, unused */, Il2CppObject * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Newtonsoft.Json.Linq.JTokenReader::ilo_get_LinePosition5(Newtonsoft.Json.IJsonLineInfo)
extern "C"  int32_t JTokenReader_ilo_get_LinePosition5_m2901892780 (Il2CppObject * __this /* static, unused */, Il2CppObject * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Linq.JTokenReader::ilo_Read6(Newtonsoft.Json.Linq.JTokenReader)
extern "C"  bool JTokenReader_ilo_Read6_m41839752 (Il2CppObject * __this /* static, unused */, JTokenReader_t3546959010 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.JsonToken Newtonsoft.Json.Linq.JTokenReader::ilo_get_TokenType7(Newtonsoft.Json.JsonReader)
extern "C"  int32_t JTokenReader_ilo_get_TokenType7_m4132655949 (Il2CppObject * __this /* static, unused */, JsonReader_t816925123 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JTokenReader::ilo_SetToken8(Newtonsoft.Json.JsonReader,Newtonsoft.Json.JsonToken,System.Object)
extern "C"  void JTokenReader_ilo_SetToken8_m3644460586 (Il2CppObject * __this /* static, unused */, JsonReader_t816925123 * ____this0, int32_t ___newToken1, Il2CppObject * ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Linq.JTokenReader::ilo_get_Value9(Newtonsoft.Json.JsonReader)
extern "C"  Il2CppObject * JTokenReader_ilo_get_Value9_m3984669779 (Il2CppObject * __this /* static, unused */, JsonReader_t816925123 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Linq.JTokenReader::ilo_FormatWith10(System.String,System.IFormatProvider,System.Object[])
extern "C"  String_t* JTokenReader_ilo_FormatWith10_m892026030 (Il2CppObject * __this /* static, unused */, String_t* ___format0, Il2CppObject * ___provider1, ObjectU5BU5D_t1108656482* ___args2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Linq.JTokenReader::ilo_ReadInto11(Newtonsoft.Json.Linq.JTokenReader,Newtonsoft.Json.Linq.JContainer)
extern "C"  bool JTokenReader_ilo_ReadInto11_m1535038538 (Il2CppObject * __this /* static, unused */, JTokenReader_t3546959010 * ____this0, JContainer_t3364442311 * ___c1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JTokenReader::ilo_SetToken12(Newtonsoft.Json.Linq.JTokenReader,Newtonsoft.Json.Linq.JToken)
extern "C"  void JTokenReader_ilo_SetToken12_m986304124 (Il2CppObject * __this /* static, unused */, JTokenReader_t3546959010 * ____this0, JToken_t3412245951 * ___token1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Linq.JTokenReader::ilo_ReadToEnd13(Newtonsoft.Json.Linq.JTokenReader)
extern "C"  bool JTokenReader_ilo_ReadToEnd13_m2516807900 (Il2CppObject * __this /* static, unused */, JTokenReader_t3546959010 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JTokenReader::ilo_get_Next14(Newtonsoft.Json.Linq.JToken)
extern "C"  JToken_t3412245951 * JTokenReader_ilo_get_Next14_m1460600895 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JContainer Newtonsoft.Json.Linq.JTokenReader::ilo_get_Parent15(Newtonsoft.Json.Linq.JToken)
extern "C"  JContainer_t3364442311 * JTokenReader_ilo_get_Parent15_m2325597375 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JTokenReader::ilo_get_Last16(Newtonsoft.Json.Linq.JContainer)
extern "C"  JToken_t3412245951 * JTokenReader_ilo_get_Last16_m1098757362 (Il2CppObject * __this /* static, unused */, JContainer_t3364442311 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JTokenType Newtonsoft.Json.Linq.JTokenReader::ilo_get_Type17(Newtonsoft.Json.Linq.JToken)
extern "C"  int32_t JTokenReader_ilo_get_Type17_m986628571 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JTokenReader::ilo_get_First18(Newtonsoft.Json.Linq.JContainer)
extern "C"  JToken_t3412245951 * JTokenReader_ilo_get_First18_m2104155070 (Il2CppObject * __this /* static, unused */, JContainer_t3364442311 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Linq.JTokenReader::ilo_SetEnd19(Newtonsoft.Json.Linq.JTokenReader,Newtonsoft.Json.Linq.JContainer)
extern "C"  bool JTokenReader_ilo_SetEnd19_m3683463327 (Il2CppObject * __this /* static, unused */, JTokenReader_t3546959010 * ____this0, JContainer_t3364442311 * ___c1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<Newtonsoft.Json.JsonToken> Newtonsoft.Json.Linq.JTokenReader::ilo_GetEndToken20(Newtonsoft.Json.Linq.JTokenReader,Newtonsoft.Json.Linq.JContainer)
extern "C"  Nullable_1_t4257204698  JTokenReader_ilo_GetEndToken20_m3159119382 (Il2CppObject * __this /* static, unused */, JTokenReader_t3546959010 * ____this0, JContainer_t3364442311 * ___c1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Linq.JTokenReader::ilo_ReadOver21(Newtonsoft.Json.Linq.JTokenReader,Newtonsoft.Json.Linq.JToken)
extern "C"  bool JTokenReader_ilo_ReadOver21_m3921196767 (Il2CppObject * __this /* static, unused */, JTokenReader_t3546959010 * ____this0, JToken_t3412245951 * ___t1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Linq.JTokenReader::ilo_get_Name22(Newtonsoft.Json.Linq.JProperty)
extern "C"  String_t* JTokenReader_ilo_get_Name22_m602075138 (Il2CppObject * __this /* static, unused */, JProperty_t2616415645 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Linq.JTokenReader::ilo_get_Value23(Newtonsoft.Json.Linq.JValue)
extern "C"  Il2CppObject * JTokenReader_ilo_get_Value23_m1918683881 (Il2CppObject * __this /* static, unused */, JValue_t3413677367 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
