﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Vector3[]
struct Vector3U5BU5D_t215400611;
// System.Single[]
struct SingleU5BU5D_t2316563989;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_FunType1154568505.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraShotMgr/CameraShotVO
struct  CameraShotVO_t1900810094  : public Il2CppObject
{
public:
	// System.Int32 CameraShotMgr/CameraShotVO::id
	int32_t ___id_0;
	// FunType CameraShotMgr/CameraShotVO::funType
	int32_t ___funType_1;
	// System.Int32 CameraShotMgr/CameraShotVO::isNextWith
	int32_t ___isNextWith_2;
	// System.Int32 CameraShotMgr/CameraShotVO::preId
	int32_t ___preId_3;
	// UnityEngine.Vector3[] CameraShotMgr/CameraShotVO::paths
	Vector3U5BU5D_t215400611* ___paths_4;
	// UnityEngine.Vector3[] CameraShotMgr/CameraShotVO::rotations
	Vector3U5BU5D_t215400611* ___rotations_5;
	// System.Single[] CameraShotMgr/CameraShotVO::speeds
	SingleU5BU5D_t2316563989* ___speeds_6;
	// System.Int32[] CameraShotMgr/CameraShotVO::focusIds
	Int32U5BU5D_t3230847821* ___focusIds_7;
	// System.Int32[] CameraShotMgr/CameraShotVO::isHeros
	Int32U5BU5D_t3230847821* ___isHeros_8;
	// System.Single[] CameraShotMgr/CameraShotVO::smooths
	SingleU5BU5D_t2316563989* ___smooths_9;
	// System.Single[] CameraShotMgr/CameraShotVO::views
	SingleU5BU5D_t2316563989* ___views_10;
	// System.Single[] CameraShotMgr/CameraShotVO::brights
	SingleU5BU5D_t2316563989* ___brights_11;
	// System.Single[] CameraShotMgr/CameraShotVO::rotSpeed
	SingleU5BU5D_t2316563989* ___rotSpeed_12;
	// System.Int32[] CameraShotMgr/CameraShotVO::isClockWise
	Int32U5BU5D_t3230847821* ___isClockWise_13;
	// System.Int32[] CameraShotMgr/CameraShotVO::isConstant
	Int32U5BU5D_t3230847821* ___isConstant_14;
	// System.Single[] CameraShotMgr/CameraShotVO::orthograhics
	SingleU5BU5D_t2316563989* ___orthograhics_15;
	// System.Int32 CameraShotMgr/CameraShotVO::storyId
	int32_t ___storyId_16;
	// System.Single CameraShotMgr/CameraShotVO::gameSpeed
	float ___gameSpeed_17;
	// System.Single CameraShotMgr/CameraShotVO::fromNormalTime
	float ___fromNormalTime_18;
	// System.Single CameraShotMgr/CameraShotVO::toNormalTime
	float ___toNormalTime_19;
	// System.Int32 CameraShotMgr/CameraShotVO::heroOrNpcId
	int32_t ___heroOrNpcId_20;
	// System.Int32 CameraShotMgr/CameraShotVO::isHero
	int32_t ___isHero_21;
	// System.String CameraShotMgr/CameraShotVO::actionName
	String_t* ___actionName_22;
	// System.Single CameraShotMgr/CameraShotVO::actionTime
	float ___actionTime_23;
	// System.Int32 CameraShotMgr/CameraShotVO::isLoop
	int32_t ___isLoop_24;
	// System.Int32 CameraShotMgr/CameraShotVO::skillId
	int32_t ___skillId_25;
	// UnityEngine.Vector3 CameraShotMgr/CameraShotVO::targetPoint
	Vector3_t4282066566  ___targetPoint_26;
	// System.Int32 CameraShotMgr/CameraShotVO::npcType
	int32_t ___npcType_27;
	// System.Int32 CameraShotMgr/CameraShotVO::npcRoundIndex
	int32_t ___npcRoundIndex_28;
	// System.Int32 CameraShotMgr/CameraShotVO::npcId
	int32_t ___npcId_29;
	// System.Int32[] CameraShotMgr/CameraShotVO::clearAniShow_I
	Int32U5BU5D_t3230847821* ___clearAniShow_I_30;
	// System.Single CameraShotMgr/CameraShotVO::waitTime
	float ___waitTime_31;
	// System.Int32 CameraShotMgr/CameraShotVO::isLockNpc
	int32_t ___isLockNpc_32;
	// System.Int32 CameraShotMgr/CameraShotVO::isCloseFightCtrlUpdate
	int32_t ___isCloseFightCtrlUpdate_33;
	// System.Int32 CameraShotMgr/CameraShotVO::isStopCameraShot
	int32_t ___isStopCameraShot_34;
	// System.Int32 CameraShotMgr/CameraShotVO::clearAniShow
	int32_t ___clearAniShow_35;
	// System.Int32 CameraShotMgr/CameraShotVO::isCircle
	int32_t ___isCircle_36;
	// System.Int32 CameraShotMgr/CameraShotVO::circle_TargetId
	int32_t ___circle_TargetId_37;
	// UnityEngine.Vector3 CameraShotMgr/CameraShotVO::circle_TargetPos
	Vector3_t4282066566  ___circle_TargetPos_38;
	// System.Single CameraShotMgr/CameraShotVO::circle_Speed
	float ___circle_Speed_39;
	// System.Single CameraShotMgr/CameraShotVO::circle_Distance
	float ___circle_Distance_40;
	// System.Single CameraShotMgr/CameraShotVO::circle_High
	float ___circle_High_41;
	// System.Single CameraShotMgr/CameraShotVO::circle_Angle
	float ___circle_Angle_42;
	// System.Int32 CameraShotMgr/CameraShotVO::isRight
	int32_t ___isRight_43;
	// System.String CameraShotMgr/CameraShotVO::boneName
	String_t* ___boneName_44;
	// System.Int32 CameraShotMgr/CameraShotVO::effectId
	int32_t ___effectId_45;
	// UnityEngine.Vector3 CameraShotMgr/CameraShotVO::effectPos
	Vector3_t4282066566  ___effectPos_46;
	// System.Single CameraShotMgr/CameraShotVO::lifeTime
	float ___lifeTime_47;
	// System.Int32 CameraShotMgr/CameraShotVO::hostId
	int32_t ___hostId_48;
	// System.Int32 CameraShotMgr/CameraShotVO::isShowInBack
	int32_t ___isShowInBack_49;
	// System.Single CameraShotMgr/CameraShotVO::playSpeed
	float ___playSpeed_50;
	// System.Int32 CameraShotMgr/CameraShotVO::brightId
	int32_t ___brightId_51;
	// System.Int32 CameraShotMgr/CameraShotVO::isatk
	int32_t ___isatk_52;
	// System.Int32 CameraShotMgr/CameraShotVO::tag
	int32_t ___tag_53;
	// System.Int32 CameraShotMgr/CameraShotVO::isTruth
	int32_t ___isTruth_54;
	// System.Int32[] CameraShotMgr/CameraShotVO::specialInTruth
	Int32U5BU5D_t3230847821* ___specialInTruth_55;
	// System.String CameraShotMgr/CameraShotVO::remark
	String_t* ___remark_56;
	// System.Single CameraShotMgr/CameraShotVO::dis
	float ___dis_57;
	// System.Single CameraShotMgr/CameraShotVO::time
	float ___time_58;
	// System.Int32 CameraShotMgr/CameraShotVO::isDamp
	int32_t ___isDamp_59;
	// System.Int32 CameraShotMgr/CameraShotVO::type
	int32_t ___type_60;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(CameraShotVO_t1900810094, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_funType_1() { return static_cast<int32_t>(offsetof(CameraShotVO_t1900810094, ___funType_1)); }
	inline int32_t get_funType_1() const { return ___funType_1; }
	inline int32_t* get_address_of_funType_1() { return &___funType_1; }
	inline void set_funType_1(int32_t value)
	{
		___funType_1 = value;
	}

	inline static int32_t get_offset_of_isNextWith_2() { return static_cast<int32_t>(offsetof(CameraShotVO_t1900810094, ___isNextWith_2)); }
	inline int32_t get_isNextWith_2() const { return ___isNextWith_2; }
	inline int32_t* get_address_of_isNextWith_2() { return &___isNextWith_2; }
	inline void set_isNextWith_2(int32_t value)
	{
		___isNextWith_2 = value;
	}

	inline static int32_t get_offset_of_preId_3() { return static_cast<int32_t>(offsetof(CameraShotVO_t1900810094, ___preId_3)); }
	inline int32_t get_preId_3() const { return ___preId_3; }
	inline int32_t* get_address_of_preId_3() { return &___preId_3; }
	inline void set_preId_3(int32_t value)
	{
		___preId_3 = value;
	}

	inline static int32_t get_offset_of_paths_4() { return static_cast<int32_t>(offsetof(CameraShotVO_t1900810094, ___paths_4)); }
	inline Vector3U5BU5D_t215400611* get_paths_4() const { return ___paths_4; }
	inline Vector3U5BU5D_t215400611** get_address_of_paths_4() { return &___paths_4; }
	inline void set_paths_4(Vector3U5BU5D_t215400611* value)
	{
		___paths_4 = value;
		Il2CppCodeGenWriteBarrier(&___paths_4, value);
	}

	inline static int32_t get_offset_of_rotations_5() { return static_cast<int32_t>(offsetof(CameraShotVO_t1900810094, ___rotations_5)); }
	inline Vector3U5BU5D_t215400611* get_rotations_5() const { return ___rotations_5; }
	inline Vector3U5BU5D_t215400611** get_address_of_rotations_5() { return &___rotations_5; }
	inline void set_rotations_5(Vector3U5BU5D_t215400611* value)
	{
		___rotations_5 = value;
		Il2CppCodeGenWriteBarrier(&___rotations_5, value);
	}

	inline static int32_t get_offset_of_speeds_6() { return static_cast<int32_t>(offsetof(CameraShotVO_t1900810094, ___speeds_6)); }
	inline SingleU5BU5D_t2316563989* get_speeds_6() const { return ___speeds_6; }
	inline SingleU5BU5D_t2316563989** get_address_of_speeds_6() { return &___speeds_6; }
	inline void set_speeds_6(SingleU5BU5D_t2316563989* value)
	{
		___speeds_6 = value;
		Il2CppCodeGenWriteBarrier(&___speeds_6, value);
	}

	inline static int32_t get_offset_of_focusIds_7() { return static_cast<int32_t>(offsetof(CameraShotVO_t1900810094, ___focusIds_7)); }
	inline Int32U5BU5D_t3230847821* get_focusIds_7() const { return ___focusIds_7; }
	inline Int32U5BU5D_t3230847821** get_address_of_focusIds_7() { return &___focusIds_7; }
	inline void set_focusIds_7(Int32U5BU5D_t3230847821* value)
	{
		___focusIds_7 = value;
		Il2CppCodeGenWriteBarrier(&___focusIds_7, value);
	}

	inline static int32_t get_offset_of_isHeros_8() { return static_cast<int32_t>(offsetof(CameraShotVO_t1900810094, ___isHeros_8)); }
	inline Int32U5BU5D_t3230847821* get_isHeros_8() const { return ___isHeros_8; }
	inline Int32U5BU5D_t3230847821** get_address_of_isHeros_8() { return &___isHeros_8; }
	inline void set_isHeros_8(Int32U5BU5D_t3230847821* value)
	{
		___isHeros_8 = value;
		Il2CppCodeGenWriteBarrier(&___isHeros_8, value);
	}

	inline static int32_t get_offset_of_smooths_9() { return static_cast<int32_t>(offsetof(CameraShotVO_t1900810094, ___smooths_9)); }
	inline SingleU5BU5D_t2316563989* get_smooths_9() const { return ___smooths_9; }
	inline SingleU5BU5D_t2316563989** get_address_of_smooths_9() { return &___smooths_9; }
	inline void set_smooths_9(SingleU5BU5D_t2316563989* value)
	{
		___smooths_9 = value;
		Il2CppCodeGenWriteBarrier(&___smooths_9, value);
	}

	inline static int32_t get_offset_of_views_10() { return static_cast<int32_t>(offsetof(CameraShotVO_t1900810094, ___views_10)); }
	inline SingleU5BU5D_t2316563989* get_views_10() const { return ___views_10; }
	inline SingleU5BU5D_t2316563989** get_address_of_views_10() { return &___views_10; }
	inline void set_views_10(SingleU5BU5D_t2316563989* value)
	{
		___views_10 = value;
		Il2CppCodeGenWriteBarrier(&___views_10, value);
	}

	inline static int32_t get_offset_of_brights_11() { return static_cast<int32_t>(offsetof(CameraShotVO_t1900810094, ___brights_11)); }
	inline SingleU5BU5D_t2316563989* get_brights_11() const { return ___brights_11; }
	inline SingleU5BU5D_t2316563989** get_address_of_brights_11() { return &___brights_11; }
	inline void set_brights_11(SingleU5BU5D_t2316563989* value)
	{
		___brights_11 = value;
		Il2CppCodeGenWriteBarrier(&___brights_11, value);
	}

	inline static int32_t get_offset_of_rotSpeed_12() { return static_cast<int32_t>(offsetof(CameraShotVO_t1900810094, ___rotSpeed_12)); }
	inline SingleU5BU5D_t2316563989* get_rotSpeed_12() const { return ___rotSpeed_12; }
	inline SingleU5BU5D_t2316563989** get_address_of_rotSpeed_12() { return &___rotSpeed_12; }
	inline void set_rotSpeed_12(SingleU5BU5D_t2316563989* value)
	{
		___rotSpeed_12 = value;
		Il2CppCodeGenWriteBarrier(&___rotSpeed_12, value);
	}

	inline static int32_t get_offset_of_isClockWise_13() { return static_cast<int32_t>(offsetof(CameraShotVO_t1900810094, ___isClockWise_13)); }
	inline Int32U5BU5D_t3230847821* get_isClockWise_13() const { return ___isClockWise_13; }
	inline Int32U5BU5D_t3230847821** get_address_of_isClockWise_13() { return &___isClockWise_13; }
	inline void set_isClockWise_13(Int32U5BU5D_t3230847821* value)
	{
		___isClockWise_13 = value;
		Il2CppCodeGenWriteBarrier(&___isClockWise_13, value);
	}

	inline static int32_t get_offset_of_isConstant_14() { return static_cast<int32_t>(offsetof(CameraShotVO_t1900810094, ___isConstant_14)); }
	inline Int32U5BU5D_t3230847821* get_isConstant_14() const { return ___isConstant_14; }
	inline Int32U5BU5D_t3230847821** get_address_of_isConstant_14() { return &___isConstant_14; }
	inline void set_isConstant_14(Int32U5BU5D_t3230847821* value)
	{
		___isConstant_14 = value;
		Il2CppCodeGenWriteBarrier(&___isConstant_14, value);
	}

	inline static int32_t get_offset_of_orthograhics_15() { return static_cast<int32_t>(offsetof(CameraShotVO_t1900810094, ___orthograhics_15)); }
	inline SingleU5BU5D_t2316563989* get_orthograhics_15() const { return ___orthograhics_15; }
	inline SingleU5BU5D_t2316563989** get_address_of_orthograhics_15() { return &___orthograhics_15; }
	inline void set_orthograhics_15(SingleU5BU5D_t2316563989* value)
	{
		___orthograhics_15 = value;
		Il2CppCodeGenWriteBarrier(&___orthograhics_15, value);
	}

	inline static int32_t get_offset_of_storyId_16() { return static_cast<int32_t>(offsetof(CameraShotVO_t1900810094, ___storyId_16)); }
	inline int32_t get_storyId_16() const { return ___storyId_16; }
	inline int32_t* get_address_of_storyId_16() { return &___storyId_16; }
	inline void set_storyId_16(int32_t value)
	{
		___storyId_16 = value;
	}

	inline static int32_t get_offset_of_gameSpeed_17() { return static_cast<int32_t>(offsetof(CameraShotVO_t1900810094, ___gameSpeed_17)); }
	inline float get_gameSpeed_17() const { return ___gameSpeed_17; }
	inline float* get_address_of_gameSpeed_17() { return &___gameSpeed_17; }
	inline void set_gameSpeed_17(float value)
	{
		___gameSpeed_17 = value;
	}

	inline static int32_t get_offset_of_fromNormalTime_18() { return static_cast<int32_t>(offsetof(CameraShotVO_t1900810094, ___fromNormalTime_18)); }
	inline float get_fromNormalTime_18() const { return ___fromNormalTime_18; }
	inline float* get_address_of_fromNormalTime_18() { return &___fromNormalTime_18; }
	inline void set_fromNormalTime_18(float value)
	{
		___fromNormalTime_18 = value;
	}

	inline static int32_t get_offset_of_toNormalTime_19() { return static_cast<int32_t>(offsetof(CameraShotVO_t1900810094, ___toNormalTime_19)); }
	inline float get_toNormalTime_19() const { return ___toNormalTime_19; }
	inline float* get_address_of_toNormalTime_19() { return &___toNormalTime_19; }
	inline void set_toNormalTime_19(float value)
	{
		___toNormalTime_19 = value;
	}

	inline static int32_t get_offset_of_heroOrNpcId_20() { return static_cast<int32_t>(offsetof(CameraShotVO_t1900810094, ___heroOrNpcId_20)); }
	inline int32_t get_heroOrNpcId_20() const { return ___heroOrNpcId_20; }
	inline int32_t* get_address_of_heroOrNpcId_20() { return &___heroOrNpcId_20; }
	inline void set_heroOrNpcId_20(int32_t value)
	{
		___heroOrNpcId_20 = value;
	}

	inline static int32_t get_offset_of_isHero_21() { return static_cast<int32_t>(offsetof(CameraShotVO_t1900810094, ___isHero_21)); }
	inline int32_t get_isHero_21() const { return ___isHero_21; }
	inline int32_t* get_address_of_isHero_21() { return &___isHero_21; }
	inline void set_isHero_21(int32_t value)
	{
		___isHero_21 = value;
	}

	inline static int32_t get_offset_of_actionName_22() { return static_cast<int32_t>(offsetof(CameraShotVO_t1900810094, ___actionName_22)); }
	inline String_t* get_actionName_22() const { return ___actionName_22; }
	inline String_t** get_address_of_actionName_22() { return &___actionName_22; }
	inline void set_actionName_22(String_t* value)
	{
		___actionName_22 = value;
		Il2CppCodeGenWriteBarrier(&___actionName_22, value);
	}

	inline static int32_t get_offset_of_actionTime_23() { return static_cast<int32_t>(offsetof(CameraShotVO_t1900810094, ___actionTime_23)); }
	inline float get_actionTime_23() const { return ___actionTime_23; }
	inline float* get_address_of_actionTime_23() { return &___actionTime_23; }
	inline void set_actionTime_23(float value)
	{
		___actionTime_23 = value;
	}

	inline static int32_t get_offset_of_isLoop_24() { return static_cast<int32_t>(offsetof(CameraShotVO_t1900810094, ___isLoop_24)); }
	inline int32_t get_isLoop_24() const { return ___isLoop_24; }
	inline int32_t* get_address_of_isLoop_24() { return &___isLoop_24; }
	inline void set_isLoop_24(int32_t value)
	{
		___isLoop_24 = value;
	}

	inline static int32_t get_offset_of_skillId_25() { return static_cast<int32_t>(offsetof(CameraShotVO_t1900810094, ___skillId_25)); }
	inline int32_t get_skillId_25() const { return ___skillId_25; }
	inline int32_t* get_address_of_skillId_25() { return &___skillId_25; }
	inline void set_skillId_25(int32_t value)
	{
		___skillId_25 = value;
	}

	inline static int32_t get_offset_of_targetPoint_26() { return static_cast<int32_t>(offsetof(CameraShotVO_t1900810094, ___targetPoint_26)); }
	inline Vector3_t4282066566  get_targetPoint_26() const { return ___targetPoint_26; }
	inline Vector3_t4282066566 * get_address_of_targetPoint_26() { return &___targetPoint_26; }
	inline void set_targetPoint_26(Vector3_t4282066566  value)
	{
		___targetPoint_26 = value;
	}

	inline static int32_t get_offset_of_npcType_27() { return static_cast<int32_t>(offsetof(CameraShotVO_t1900810094, ___npcType_27)); }
	inline int32_t get_npcType_27() const { return ___npcType_27; }
	inline int32_t* get_address_of_npcType_27() { return &___npcType_27; }
	inline void set_npcType_27(int32_t value)
	{
		___npcType_27 = value;
	}

	inline static int32_t get_offset_of_npcRoundIndex_28() { return static_cast<int32_t>(offsetof(CameraShotVO_t1900810094, ___npcRoundIndex_28)); }
	inline int32_t get_npcRoundIndex_28() const { return ___npcRoundIndex_28; }
	inline int32_t* get_address_of_npcRoundIndex_28() { return &___npcRoundIndex_28; }
	inline void set_npcRoundIndex_28(int32_t value)
	{
		___npcRoundIndex_28 = value;
	}

	inline static int32_t get_offset_of_npcId_29() { return static_cast<int32_t>(offsetof(CameraShotVO_t1900810094, ___npcId_29)); }
	inline int32_t get_npcId_29() const { return ___npcId_29; }
	inline int32_t* get_address_of_npcId_29() { return &___npcId_29; }
	inline void set_npcId_29(int32_t value)
	{
		___npcId_29 = value;
	}

	inline static int32_t get_offset_of_clearAniShow_I_30() { return static_cast<int32_t>(offsetof(CameraShotVO_t1900810094, ___clearAniShow_I_30)); }
	inline Int32U5BU5D_t3230847821* get_clearAniShow_I_30() const { return ___clearAniShow_I_30; }
	inline Int32U5BU5D_t3230847821** get_address_of_clearAniShow_I_30() { return &___clearAniShow_I_30; }
	inline void set_clearAniShow_I_30(Int32U5BU5D_t3230847821* value)
	{
		___clearAniShow_I_30 = value;
		Il2CppCodeGenWriteBarrier(&___clearAniShow_I_30, value);
	}

	inline static int32_t get_offset_of_waitTime_31() { return static_cast<int32_t>(offsetof(CameraShotVO_t1900810094, ___waitTime_31)); }
	inline float get_waitTime_31() const { return ___waitTime_31; }
	inline float* get_address_of_waitTime_31() { return &___waitTime_31; }
	inline void set_waitTime_31(float value)
	{
		___waitTime_31 = value;
	}

	inline static int32_t get_offset_of_isLockNpc_32() { return static_cast<int32_t>(offsetof(CameraShotVO_t1900810094, ___isLockNpc_32)); }
	inline int32_t get_isLockNpc_32() const { return ___isLockNpc_32; }
	inline int32_t* get_address_of_isLockNpc_32() { return &___isLockNpc_32; }
	inline void set_isLockNpc_32(int32_t value)
	{
		___isLockNpc_32 = value;
	}

	inline static int32_t get_offset_of_isCloseFightCtrlUpdate_33() { return static_cast<int32_t>(offsetof(CameraShotVO_t1900810094, ___isCloseFightCtrlUpdate_33)); }
	inline int32_t get_isCloseFightCtrlUpdate_33() const { return ___isCloseFightCtrlUpdate_33; }
	inline int32_t* get_address_of_isCloseFightCtrlUpdate_33() { return &___isCloseFightCtrlUpdate_33; }
	inline void set_isCloseFightCtrlUpdate_33(int32_t value)
	{
		___isCloseFightCtrlUpdate_33 = value;
	}

	inline static int32_t get_offset_of_isStopCameraShot_34() { return static_cast<int32_t>(offsetof(CameraShotVO_t1900810094, ___isStopCameraShot_34)); }
	inline int32_t get_isStopCameraShot_34() const { return ___isStopCameraShot_34; }
	inline int32_t* get_address_of_isStopCameraShot_34() { return &___isStopCameraShot_34; }
	inline void set_isStopCameraShot_34(int32_t value)
	{
		___isStopCameraShot_34 = value;
	}

	inline static int32_t get_offset_of_clearAniShow_35() { return static_cast<int32_t>(offsetof(CameraShotVO_t1900810094, ___clearAniShow_35)); }
	inline int32_t get_clearAniShow_35() const { return ___clearAniShow_35; }
	inline int32_t* get_address_of_clearAniShow_35() { return &___clearAniShow_35; }
	inline void set_clearAniShow_35(int32_t value)
	{
		___clearAniShow_35 = value;
	}

	inline static int32_t get_offset_of_isCircle_36() { return static_cast<int32_t>(offsetof(CameraShotVO_t1900810094, ___isCircle_36)); }
	inline int32_t get_isCircle_36() const { return ___isCircle_36; }
	inline int32_t* get_address_of_isCircle_36() { return &___isCircle_36; }
	inline void set_isCircle_36(int32_t value)
	{
		___isCircle_36 = value;
	}

	inline static int32_t get_offset_of_circle_TargetId_37() { return static_cast<int32_t>(offsetof(CameraShotVO_t1900810094, ___circle_TargetId_37)); }
	inline int32_t get_circle_TargetId_37() const { return ___circle_TargetId_37; }
	inline int32_t* get_address_of_circle_TargetId_37() { return &___circle_TargetId_37; }
	inline void set_circle_TargetId_37(int32_t value)
	{
		___circle_TargetId_37 = value;
	}

	inline static int32_t get_offset_of_circle_TargetPos_38() { return static_cast<int32_t>(offsetof(CameraShotVO_t1900810094, ___circle_TargetPos_38)); }
	inline Vector3_t4282066566  get_circle_TargetPos_38() const { return ___circle_TargetPos_38; }
	inline Vector3_t4282066566 * get_address_of_circle_TargetPos_38() { return &___circle_TargetPos_38; }
	inline void set_circle_TargetPos_38(Vector3_t4282066566  value)
	{
		___circle_TargetPos_38 = value;
	}

	inline static int32_t get_offset_of_circle_Speed_39() { return static_cast<int32_t>(offsetof(CameraShotVO_t1900810094, ___circle_Speed_39)); }
	inline float get_circle_Speed_39() const { return ___circle_Speed_39; }
	inline float* get_address_of_circle_Speed_39() { return &___circle_Speed_39; }
	inline void set_circle_Speed_39(float value)
	{
		___circle_Speed_39 = value;
	}

	inline static int32_t get_offset_of_circle_Distance_40() { return static_cast<int32_t>(offsetof(CameraShotVO_t1900810094, ___circle_Distance_40)); }
	inline float get_circle_Distance_40() const { return ___circle_Distance_40; }
	inline float* get_address_of_circle_Distance_40() { return &___circle_Distance_40; }
	inline void set_circle_Distance_40(float value)
	{
		___circle_Distance_40 = value;
	}

	inline static int32_t get_offset_of_circle_High_41() { return static_cast<int32_t>(offsetof(CameraShotVO_t1900810094, ___circle_High_41)); }
	inline float get_circle_High_41() const { return ___circle_High_41; }
	inline float* get_address_of_circle_High_41() { return &___circle_High_41; }
	inline void set_circle_High_41(float value)
	{
		___circle_High_41 = value;
	}

	inline static int32_t get_offset_of_circle_Angle_42() { return static_cast<int32_t>(offsetof(CameraShotVO_t1900810094, ___circle_Angle_42)); }
	inline float get_circle_Angle_42() const { return ___circle_Angle_42; }
	inline float* get_address_of_circle_Angle_42() { return &___circle_Angle_42; }
	inline void set_circle_Angle_42(float value)
	{
		___circle_Angle_42 = value;
	}

	inline static int32_t get_offset_of_isRight_43() { return static_cast<int32_t>(offsetof(CameraShotVO_t1900810094, ___isRight_43)); }
	inline int32_t get_isRight_43() const { return ___isRight_43; }
	inline int32_t* get_address_of_isRight_43() { return &___isRight_43; }
	inline void set_isRight_43(int32_t value)
	{
		___isRight_43 = value;
	}

	inline static int32_t get_offset_of_boneName_44() { return static_cast<int32_t>(offsetof(CameraShotVO_t1900810094, ___boneName_44)); }
	inline String_t* get_boneName_44() const { return ___boneName_44; }
	inline String_t** get_address_of_boneName_44() { return &___boneName_44; }
	inline void set_boneName_44(String_t* value)
	{
		___boneName_44 = value;
		Il2CppCodeGenWriteBarrier(&___boneName_44, value);
	}

	inline static int32_t get_offset_of_effectId_45() { return static_cast<int32_t>(offsetof(CameraShotVO_t1900810094, ___effectId_45)); }
	inline int32_t get_effectId_45() const { return ___effectId_45; }
	inline int32_t* get_address_of_effectId_45() { return &___effectId_45; }
	inline void set_effectId_45(int32_t value)
	{
		___effectId_45 = value;
	}

	inline static int32_t get_offset_of_effectPos_46() { return static_cast<int32_t>(offsetof(CameraShotVO_t1900810094, ___effectPos_46)); }
	inline Vector3_t4282066566  get_effectPos_46() const { return ___effectPos_46; }
	inline Vector3_t4282066566 * get_address_of_effectPos_46() { return &___effectPos_46; }
	inline void set_effectPos_46(Vector3_t4282066566  value)
	{
		___effectPos_46 = value;
	}

	inline static int32_t get_offset_of_lifeTime_47() { return static_cast<int32_t>(offsetof(CameraShotVO_t1900810094, ___lifeTime_47)); }
	inline float get_lifeTime_47() const { return ___lifeTime_47; }
	inline float* get_address_of_lifeTime_47() { return &___lifeTime_47; }
	inline void set_lifeTime_47(float value)
	{
		___lifeTime_47 = value;
	}

	inline static int32_t get_offset_of_hostId_48() { return static_cast<int32_t>(offsetof(CameraShotVO_t1900810094, ___hostId_48)); }
	inline int32_t get_hostId_48() const { return ___hostId_48; }
	inline int32_t* get_address_of_hostId_48() { return &___hostId_48; }
	inline void set_hostId_48(int32_t value)
	{
		___hostId_48 = value;
	}

	inline static int32_t get_offset_of_isShowInBack_49() { return static_cast<int32_t>(offsetof(CameraShotVO_t1900810094, ___isShowInBack_49)); }
	inline int32_t get_isShowInBack_49() const { return ___isShowInBack_49; }
	inline int32_t* get_address_of_isShowInBack_49() { return &___isShowInBack_49; }
	inline void set_isShowInBack_49(int32_t value)
	{
		___isShowInBack_49 = value;
	}

	inline static int32_t get_offset_of_playSpeed_50() { return static_cast<int32_t>(offsetof(CameraShotVO_t1900810094, ___playSpeed_50)); }
	inline float get_playSpeed_50() const { return ___playSpeed_50; }
	inline float* get_address_of_playSpeed_50() { return &___playSpeed_50; }
	inline void set_playSpeed_50(float value)
	{
		___playSpeed_50 = value;
	}

	inline static int32_t get_offset_of_brightId_51() { return static_cast<int32_t>(offsetof(CameraShotVO_t1900810094, ___brightId_51)); }
	inline int32_t get_brightId_51() const { return ___brightId_51; }
	inline int32_t* get_address_of_brightId_51() { return &___brightId_51; }
	inline void set_brightId_51(int32_t value)
	{
		___brightId_51 = value;
	}

	inline static int32_t get_offset_of_isatk_52() { return static_cast<int32_t>(offsetof(CameraShotVO_t1900810094, ___isatk_52)); }
	inline int32_t get_isatk_52() const { return ___isatk_52; }
	inline int32_t* get_address_of_isatk_52() { return &___isatk_52; }
	inline void set_isatk_52(int32_t value)
	{
		___isatk_52 = value;
	}

	inline static int32_t get_offset_of_tag_53() { return static_cast<int32_t>(offsetof(CameraShotVO_t1900810094, ___tag_53)); }
	inline int32_t get_tag_53() const { return ___tag_53; }
	inline int32_t* get_address_of_tag_53() { return &___tag_53; }
	inline void set_tag_53(int32_t value)
	{
		___tag_53 = value;
	}

	inline static int32_t get_offset_of_isTruth_54() { return static_cast<int32_t>(offsetof(CameraShotVO_t1900810094, ___isTruth_54)); }
	inline int32_t get_isTruth_54() const { return ___isTruth_54; }
	inline int32_t* get_address_of_isTruth_54() { return &___isTruth_54; }
	inline void set_isTruth_54(int32_t value)
	{
		___isTruth_54 = value;
	}

	inline static int32_t get_offset_of_specialInTruth_55() { return static_cast<int32_t>(offsetof(CameraShotVO_t1900810094, ___specialInTruth_55)); }
	inline Int32U5BU5D_t3230847821* get_specialInTruth_55() const { return ___specialInTruth_55; }
	inline Int32U5BU5D_t3230847821** get_address_of_specialInTruth_55() { return &___specialInTruth_55; }
	inline void set_specialInTruth_55(Int32U5BU5D_t3230847821* value)
	{
		___specialInTruth_55 = value;
		Il2CppCodeGenWriteBarrier(&___specialInTruth_55, value);
	}

	inline static int32_t get_offset_of_remark_56() { return static_cast<int32_t>(offsetof(CameraShotVO_t1900810094, ___remark_56)); }
	inline String_t* get_remark_56() const { return ___remark_56; }
	inline String_t** get_address_of_remark_56() { return &___remark_56; }
	inline void set_remark_56(String_t* value)
	{
		___remark_56 = value;
		Il2CppCodeGenWriteBarrier(&___remark_56, value);
	}

	inline static int32_t get_offset_of_dis_57() { return static_cast<int32_t>(offsetof(CameraShotVO_t1900810094, ___dis_57)); }
	inline float get_dis_57() const { return ___dis_57; }
	inline float* get_address_of_dis_57() { return &___dis_57; }
	inline void set_dis_57(float value)
	{
		___dis_57 = value;
	}

	inline static int32_t get_offset_of_time_58() { return static_cast<int32_t>(offsetof(CameraShotVO_t1900810094, ___time_58)); }
	inline float get_time_58() const { return ___time_58; }
	inline float* get_address_of_time_58() { return &___time_58; }
	inline void set_time_58(float value)
	{
		___time_58 = value;
	}

	inline static int32_t get_offset_of_isDamp_59() { return static_cast<int32_t>(offsetof(CameraShotVO_t1900810094, ___isDamp_59)); }
	inline int32_t get_isDamp_59() const { return ___isDamp_59; }
	inline int32_t* get_address_of_isDamp_59() { return &___isDamp_59; }
	inline void set_isDamp_59(int32_t value)
	{
		___isDamp_59 = value;
	}

	inline static int32_t get_offset_of_type_60() { return static_cast<int32_t>(offsetof(CameraShotVO_t1900810094, ___type_60)); }
	inline int32_t get_type_60() const { return ___type_60; }
	inline int32_t* get_address_of_type_60() { return &___type_60; }
	inline void set_type_60(int32_t value)
	{
		___type_60 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
