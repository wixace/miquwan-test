﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.BBTree
struct BBTree_t1216325332;
// Pathfinding.MeshNode
struct MeshNode_t3005053445;
// Pathfinding.BBTree/BBTreeBox
struct BBTreeBox_t1958396198;
struct BBTreeBox_t1958396198_marshaled_pinvoke;
struct BBTreeBox_t1958396198_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_BBTree_BBTreeBox1958396198.h"
#include "AssemblyU2DCSharp_Pathfinding_BBTree1216325332.h"
#include "AssemblyU2DCSharp_Pathfinding_MeshNode3005053445.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void Pathfinding.BBTree/BBTreeBox::.ctor(Pathfinding.BBTree,Pathfinding.MeshNode)
extern "C"  void BBTreeBox__ctor_m3932184544 (BBTreeBox_t1958396198 * __this, BBTree_t1216325332 * ___tree0, MeshNode_t3005053445 * ___node1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.BBTree/BBTreeBox::get_IsLeaf()
extern "C"  bool BBTreeBox_get_IsLeaf_m3443699220 (BBTreeBox_t1958396198 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.BBTree/BBTreeBox::Contains(UnityEngine.Vector3)
extern "C"  bool BBTreeBox_Contains_m686103 (BBTreeBox_t1958396198 * __this, Vector3_t4282066566  ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct BBTreeBox_t1958396198;
struct BBTreeBox_t1958396198_marshaled_pinvoke;

extern "C" void BBTreeBox_t1958396198_marshal_pinvoke(const BBTreeBox_t1958396198& unmarshaled, BBTreeBox_t1958396198_marshaled_pinvoke& marshaled);
extern "C" void BBTreeBox_t1958396198_marshal_pinvoke_back(const BBTreeBox_t1958396198_marshaled_pinvoke& marshaled, BBTreeBox_t1958396198& unmarshaled);
extern "C" void BBTreeBox_t1958396198_marshal_pinvoke_cleanup(BBTreeBox_t1958396198_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct BBTreeBox_t1958396198;
struct BBTreeBox_t1958396198_marshaled_com;

extern "C" void BBTreeBox_t1958396198_marshal_com(const BBTreeBox_t1958396198& unmarshaled, BBTreeBox_t1958396198_marshaled_com& marshaled);
extern "C" void BBTreeBox_t1958396198_marshal_com_back(const BBTreeBox_t1958396198_marshaled_com& marshaled, BBTreeBox_t1958396198& unmarshaled);
extern "C" void BBTreeBox_t1958396198_marshal_com_cleanup(BBTreeBox_t1958396198_marshaled_com& marshaled);
