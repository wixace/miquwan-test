﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Pathfinding.Voxels.CompactVoxelSpan[]
struct CompactVoxelSpanU5BU5D_t1459321228;
// Pathfinding.Voxels.CompactVoxelCell[]
struct CompactVoxelCellU5BU5D_t3900941332;
// System.UInt16[]
struct UInt16U5BU5D_t801649474;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t215400611;
// Pathfinding.Voxels.LinkedVoxelSpan[]
struct LinkedVoxelSpanU5BU5D_t2863720544;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.Voxels.VoxelArea
struct  VoxelArea_t3943332841  : public Il2CppObject
{
public:
	// System.Int32 Pathfinding.Voxels.VoxelArea::width
	int32_t ___width_4;
	// System.Int32 Pathfinding.Voxels.VoxelArea::depth
	int32_t ___depth_5;
	// Pathfinding.Voxels.CompactVoxelSpan[] Pathfinding.Voxels.VoxelArea::compactSpans
	CompactVoxelSpanU5BU5D_t1459321228* ___compactSpans_6;
	// Pathfinding.Voxels.CompactVoxelCell[] Pathfinding.Voxels.VoxelArea::compactCells
	CompactVoxelCellU5BU5D_t3900941332* ___compactCells_7;
	// System.Int32 Pathfinding.Voxels.VoxelArea::compactSpanCount
	int32_t ___compactSpanCount_8;
	// System.UInt16[] Pathfinding.Voxels.VoxelArea::tmpUShortArr
	UInt16U5BU5D_t801649474* ___tmpUShortArr_9;
	// System.Int32[] Pathfinding.Voxels.VoxelArea::areaTypes
	Int32U5BU5D_t3230847821* ___areaTypes_10;
	// System.UInt16[] Pathfinding.Voxels.VoxelArea::dist
	UInt16U5BU5D_t801649474* ___dist_11;
	// System.UInt16 Pathfinding.Voxels.VoxelArea::maxDistance
	uint16_t ___maxDistance_12;
	// System.Int32 Pathfinding.Voxels.VoxelArea::maxRegions
	int32_t ___maxRegions_13;
	// System.Int32[] Pathfinding.Voxels.VoxelArea::DirectionX
	Int32U5BU5D_t3230847821* ___DirectionX_14;
	// System.Int32[] Pathfinding.Voxels.VoxelArea::DirectionZ
	Int32U5BU5D_t3230847821* ___DirectionZ_15;
	// UnityEngine.Vector3[] Pathfinding.Voxels.VoxelArea::VectorDirection
	Vector3U5BU5D_t215400611* ___VectorDirection_16;
	// System.Int32 Pathfinding.Voxels.VoxelArea::linkedSpanCount
	int32_t ___linkedSpanCount_17;
	// Pathfinding.Voxels.LinkedVoxelSpan[] Pathfinding.Voxels.VoxelArea::linkedSpans
	LinkedVoxelSpanU5BU5D_t2863720544* ___linkedSpans_18;
	// System.Int32[] Pathfinding.Voxels.VoxelArea::removedStack
	Int32U5BU5D_t3230847821* ___removedStack_19;
	// System.Int32 Pathfinding.Voxels.VoxelArea::removedStackCount
	int32_t ___removedStackCount_20;

public:
	inline static int32_t get_offset_of_width_4() { return static_cast<int32_t>(offsetof(VoxelArea_t3943332841, ___width_4)); }
	inline int32_t get_width_4() const { return ___width_4; }
	inline int32_t* get_address_of_width_4() { return &___width_4; }
	inline void set_width_4(int32_t value)
	{
		___width_4 = value;
	}

	inline static int32_t get_offset_of_depth_5() { return static_cast<int32_t>(offsetof(VoxelArea_t3943332841, ___depth_5)); }
	inline int32_t get_depth_5() const { return ___depth_5; }
	inline int32_t* get_address_of_depth_5() { return &___depth_5; }
	inline void set_depth_5(int32_t value)
	{
		___depth_5 = value;
	}

	inline static int32_t get_offset_of_compactSpans_6() { return static_cast<int32_t>(offsetof(VoxelArea_t3943332841, ___compactSpans_6)); }
	inline CompactVoxelSpanU5BU5D_t1459321228* get_compactSpans_6() const { return ___compactSpans_6; }
	inline CompactVoxelSpanU5BU5D_t1459321228** get_address_of_compactSpans_6() { return &___compactSpans_6; }
	inline void set_compactSpans_6(CompactVoxelSpanU5BU5D_t1459321228* value)
	{
		___compactSpans_6 = value;
		Il2CppCodeGenWriteBarrier(&___compactSpans_6, value);
	}

	inline static int32_t get_offset_of_compactCells_7() { return static_cast<int32_t>(offsetof(VoxelArea_t3943332841, ___compactCells_7)); }
	inline CompactVoxelCellU5BU5D_t3900941332* get_compactCells_7() const { return ___compactCells_7; }
	inline CompactVoxelCellU5BU5D_t3900941332** get_address_of_compactCells_7() { return &___compactCells_7; }
	inline void set_compactCells_7(CompactVoxelCellU5BU5D_t3900941332* value)
	{
		___compactCells_7 = value;
		Il2CppCodeGenWriteBarrier(&___compactCells_7, value);
	}

	inline static int32_t get_offset_of_compactSpanCount_8() { return static_cast<int32_t>(offsetof(VoxelArea_t3943332841, ___compactSpanCount_8)); }
	inline int32_t get_compactSpanCount_8() const { return ___compactSpanCount_8; }
	inline int32_t* get_address_of_compactSpanCount_8() { return &___compactSpanCount_8; }
	inline void set_compactSpanCount_8(int32_t value)
	{
		___compactSpanCount_8 = value;
	}

	inline static int32_t get_offset_of_tmpUShortArr_9() { return static_cast<int32_t>(offsetof(VoxelArea_t3943332841, ___tmpUShortArr_9)); }
	inline UInt16U5BU5D_t801649474* get_tmpUShortArr_9() const { return ___tmpUShortArr_9; }
	inline UInt16U5BU5D_t801649474** get_address_of_tmpUShortArr_9() { return &___tmpUShortArr_9; }
	inline void set_tmpUShortArr_9(UInt16U5BU5D_t801649474* value)
	{
		___tmpUShortArr_9 = value;
		Il2CppCodeGenWriteBarrier(&___tmpUShortArr_9, value);
	}

	inline static int32_t get_offset_of_areaTypes_10() { return static_cast<int32_t>(offsetof(VoxelArea_t3943332841, ___areaTypes_10)); }
	inline Int32U5BU5D_t3230847821* get_areaTypes_10() const { return ___areaTypes_10; }
	inline Int32U5BU5D_t3230847821** get_address_of_areaTypes_10() { return &___areaTypes_10; }
	inline void set_areaTypes_10(Int32U5BU5D_t3230847821* value)
	{
		___areaTypes_10 = value;
		Il2CppCodeGenWriteBarrier(&___areaTypes_10, value);
	}

	inline static int32_t get_offset_of_dist_11() { return static_cast<int32_t>(offsetof(VoxelArea_t3943332841, ___dist_11)); }
	inline UInt16U5BU5D_t801649474* get_dist_11() const { return ___dist_11; }
	inline UInt16U5BU5D_t801649474** get_address_of_dist_11() { return &___dist_11; }
	inline void set_dist_11(UInt16U5BU5D_t801649474* value)
	{
		___dist_11 = value;
		Il2CppCodeGenWriteBarrier(&___dist_11, value);
	}

	inline static int32_t get_offset_of_maxDistance_12() { return static_cast<int32_t>(offsetof(VoxelArea_t3943332841, ___maxDistance_12)); }
	inline uint16_t get_maxDistance_12() const { return ___maxDistance_12; }
	inline uint16_t* get_address_of_maxDistance_12() { return &___maxDistance_12; }
	inline void set_maxDistance_12(uint16_t value)
	{
		___maxDistance_12 = value;
	}

	inline static int32_t get_offset_of_maxRegions_13() { return static_cast<int32_t>(offsetof(VoxelArea_t3943332841, ___maxRegions_13)); }
	inline int32_t get_maxRegions_13() const { return ___maxRegions_13; }
	inline int32_t* get_address_of_maxRegions_13() { return &___maxRegions_13; }
	inline void set_maxRegions_13(int32_t value)
	{
		___maxRegions_13 = value;
	}

	inline static int32_t get_offset_of_DirectionX_14() { return static_cast<int32_t>(offsetof(VoxelArea_t3943332841, ___DirectionX_14)); }
	inline Int32U5BU5D_t3230847821* get_DirectionX_14() const { return ___DirectionX_14; }
	inline Int32U5BU5D_t3230847821** get_address_of_DirectionX_14() { return &___DirectionX_14; }
	inline void set_DirectionX_14(Int32U5BU5D_t3230847821* value)
	{
		___DirectionX_14 = value;
		Il2CppCodeGenWriteBarrier(&___DirectionX_14, value);
	}

	inline static int32_t get_offset_of_DirectionZ_15() { return static_cast<int32_t>(offsetof(VoxelArea_t3943332841, ___DirectionZ_15)); }
	inline Int32U5BU5D_t3230847821* get_DirectionZ_15() const { return ___DirectionZ_15; }
	inline Int32U5BU5D_t3230847821** get_address_of_DirectionZ_15() { return &___DirectionZ_15; }
	inline void set_DirectionZ_15(Int32U5BU5D_t3230847821* value)
	{
		___DirectionZ_15 = value;
		Il2CppCodeGenWriteBarrier(&___DirectionZ_15, value);
	}

	inline static int32_t get_offset_of_VectorDirection_16() { return static_cast<int32_t>(offsetof(VoxelArea_t3943332841, ___VectorDirection_16)); }
	inline Vector3U5BU5D_t215400611* get_VectorDirection_16() const { return ___VectorDirection_16; }
	inline Vector3U5BU5D_t215400611** get_address_of_VectorDirection_16() { return &___VectorDirection_16; }
	inline void set_VectorDirection_16(Vector3U5BU5D_t215400611* value)
	{
		___VectorDirection_16 = value;
		Il2CppCodeGenWriteBarrier(&___VectorDirection_16, value);
	}

	inline static int32_t get_offset_of_linkedSpanCount_17() { return static_cast<int32_t>(offsetof(VoxelArea_t3943332841, ___linkedSpanCount_17)); }
	inline int32_t get_linkedSpanCount_17() const { return ___linkedSpanCount_17; }
	inline int32_t* get_address_of_linkedSpanCount_17() { return &___linkedSpanCount_17; }
	inline void set_linkedSpanCount_17(int32_t value)
	{
		___linkedSpanCount_17 = value;
	}

	inline static int32_t get_offset_of_linkedSpans_18() { return static_cast<int32_t>(offsetof(VoxelArea_t3943332841, ___linkedSpans_18)); }
	inline LinkedVoxelSpanU5BU5D_t2863720544* get_linkedSpans_18() const { return ___linkedSpans_18; }
	inline LinkedVoxelSpanU5BU5D_t2863720544** get_address_of_linkedSpans_18() { return &___linkedSpans_18; }
	inline void set_linkedSpans_18(LinkedVoxelSpanU5BU5D_t2863720544* value)
	{
		___linkedSpans_18 = value;
		Il2CppCodeGenWriteBarrier(&___linkedSpans_18, value);
	}

	inline static int32_t get_offset_of_removedStack_19() { return static_cast<int32_t>(offsetof(VoxelArea_t3943332841, ___removedStack_19)); }
	inline Int32U5BU5D_t3230847821* get_removedStack_19() const { return ___removedStack_19; }
	inline Int32U5BU5D_t3230847821** get_address_of_removedStack_19() { return &___removedStack_19; }
	inline void set_removedStack_19(Int32U5BU5D_t3230847821* value)
	{
		___removedStack_19 = value;
		Il2CppCodeGenWriteBarrier(&___removedStack_19, value);
	}

	inline static int32_t get_offset_of_removedStackCount_20() { return static_cast<int32_t>(offsetof(VoxelArea_t3943332841, ___removedStackCount_20)); }
	inline int32_t get_removedStackCount_20() const { return ___removedStackCount_20; }
	inline int32_t* get_address_of_removedStackCount_20() { return &___removedStackCount_20; }
	inline void set_removedStackCount_20(int32_t value)
	{
		___removedStackCount_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
