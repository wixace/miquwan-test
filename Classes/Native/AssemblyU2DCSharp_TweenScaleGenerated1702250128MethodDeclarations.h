﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TweenScaleGenerated
struct TweenScaleGenerated_t1702250128;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// TweenScale
struct TweenScale_t2936666559;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_TweenScale2936666559.h"

// System.Void TweenScaleGenerated::.ctor()
extern "C"  void TweenScaleGenerated__ctor_m2141597403 (TweenScaleGenerated_t1702250128 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TweenScaleGenerated::TweenScale_TweenScale1(JSVCall,System.Int32)
extern "C"  bool TweenScaleGenerated_TweenScale_TweenScale1_m2984615703 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenScaleGenerated::TweenScale_from(JSVCall)
extern "C"  void TweenScaleGenerated_TweenScale_from_m2693300868 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenScaleGenerated::TweenScale_to(JSVCall)
extern "C"  void TweenScaleGenerated_TweenScale_to_m940499155 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenScaleGenerated::TweenScale_updateTable(JSVCall)
extern "C"  void TweenScaleGenerated_TweenScale_updateTable_m3567974457 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenScaleGenerated::TweenScale_cachedTransform(JSVCall)
extern "C"  void TweenScaleGenerated_TweenScale_cachedTransform_m2507486708 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenScaleGenerated::TweenScale_value(JSVCall)
extern "C"  void TweenScaleGenerated_TweenScale_value_m2773347021 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TweenScaleGenerated::TweenScale_SetEndToCurrentValue(JSVCall,System.Int32)
extern "C"  bool TweenScaleGenerated_TweenScale_SetEndToCurrentValue_m2809431177 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TweenScaleGenerated::TweenScale_SetStartToCurrentValue(JSVCall,System.Int32)
extern "C"  bool TweenScaleGenerated_TweenScale_SetStartToCurrentValue_m1354876432 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TweenScaleGenerated::TweenScale_Begin__GameObject__Single__Vector3(JSVCall,System.Int32)
extern "C"  bool TweenScaleGenerated_TweenScale_Begin__GameObject__Single__Vector3_m1270544715 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenScaleGenerated::__Register()
extern "C"  void TweenScaleGenerated___Register_m2992463564 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 TweenScaleGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t TweenScaleGenerated_ilo_getObject1_m1084936507 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TweenScaleGenerated::ilo_attachFinalizerObject2(System.Int32)
extern "C"  bool TweenScaleGenerated_ilo_attachFinalizerObject2_m1348002493 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenScaleGenerated::ilo_addJSCSRel3(System.Int32,System.Object)
extern "C"  void TweenScaleGenerated_ilo_addJSCSRel3_m3177741593 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 TweenScaleGenerated::ilo_getVector3S4(System.Int32)
extern "C"  Vector3_t4282066566  TweenScaleGenerated_ilo_getVector3S4_m2200310542 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenScaleGenerated::ilo_setBooleanS5(System.Int32,System.Boolean)
extern "C"  void TweenScaleGenerated_ilo_setBooleanS5_m107561864 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenScaleGenerated::ilo_set_value6(TweenScale,UnityEngine.Vector3)
extern "C"  void TweenScaleGenerated_ilo_set_value6_m2081998194 (Il2CppObject * __this /* static, unused */, TweenScale_t2936666559 * ____this0, Vector3_t4282066566  ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TweenScaleGenerated::ilo_getSingle7(System.Int32)
extern "C"  float TweenScaleGenerated_ilo_getSingle7_m931834778 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
