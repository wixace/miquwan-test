﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_peesalzel182
struct  M_peesalzel182_t1693384776  : public Il2CppObject
{
public:
	// System.Int32 GarbageiOS.M_peesalzel182::_fabervai
	int32_t ____fabervai_0;
	// System.UInt32 GarbageiOS.M_peesalzel182::_rousai
	uint32_t ____rousai_1;
	// System.Int32 GarbageiOS.M_peesalzel182::_fisasqemDesar
	int32_t ____fisasqemDesar_2;
	// System.Boolean GarbageiOS.M_peesalzel182::_qairtasee
	bool ____qairtasee_3;
	// System.UInt32 GarbageiOS.M_peesalzel182::_poulaybis
	uint32_t ____poulaybis_4;
	// System.Int32 GarbageiOS.M_peesalzel182::_chasmileSaselhi
	int32_t ____chasmileSaselhi_5;
	// System.Single GarbageiOS.M_peesalzel182::_kahou
	float ____kahou_6;
	// System.UInt32 GarbageiOS.M_peesalzel182::_nawhallSafayya
	uint32_t ____nawhallSafayya_7;
	// System.Single GarbageiOS.M_peesalzel182::_hetadeRijur
	float ____hetadeRijur_8;
	// System.UInt32 GarbageiOS.M_peesalzel182::_fetadeGurjayrur
	uint32_t ____fetadeGurjayrur_9;
	// System.UInt32 GarbageiOS.M_peesalzel182::_tispow
	uint32_t ____tispow_10;
	// System.Int32 GarbageiOS.M_peesalzel182::_supalerKusitrou
	int32_t ____supalerKusitrou_11;
	// System.Int32 GarbageiOS.M_peesalzel182::_loteasowBairtasfa
	int32_t ____loteasowBairtasfa_12;
	// System.Boolean GarbageiOS.M_peesalzel182::_meju
	bool ____meju_13;
	// System.String GarbageiOS.M_peesalzel182::_jerere
	String_t* ____jerere_14;
	// System.Single GarbageiOS.M_peesalzel182::_fidaJoonirce
	float ____fidaJoonirce_15;
	// System.String GarbageiOS.M_peesalzel182::_curallBerqair
	String_t* ____curallBerqair_16;
	// System.String GarbageiOS.M_peesalzel182::_selall
	String_t* ____selall_17;
	// System.Int32 GarbageiOS.M_peesalzel182::_masca
	int32_t ____masca_18;
	// System.Single GarbageiOS.M_peesalzel182::_hiselcai
	float ____hiselcai_19;
	// System.Single GarbageiOS.M_peesalzel182::_nawtoCiba
	float ____nawtoCiba_20;
	// System.String GarbageiOS.M_peesalzel182::_soowitel
	String_t* ____soowitel_21;
	// System.Int32 GarbageiOS.M_peesalzel182::_fouzasdooKivasis
	int32_t ____fouzasdooKivasis_22;
	// System.Boolean GarbageiOS.M_peesalzel182::_jarcese
	bool ____jarcese_23;
	// System.Single GarbageiOS.M_peesalzel182::_coreeKehijow
	float ____coreeKehijow_24;

public:
	inline static int32_t get_offset_of__fabervai_0() { return static_cast<int32_t>(offsetof(M_peesalzel182_t1693384776, ____fabervai_0)); }
	inline int32_t get__fabervai_0() const { return ____fabervai_0; }
	inline int32_t* get_address_of__fabervai_0() { return &____fabervai_0; }
	inline void set__fabervai_0(int32_t value)
	{
		____fabervai_0 = value;
	}

	inline static int32_t get_offset_of__rousai_1() { return static_cast<int32_t>(offsetof(M_peesalzel182_t1693384776, ____rousai_1)); }
	inline uint32_t get__rousai_1() const { return ____rousai_1; }
	inline uint32_t* get_address_of__rousai_1() { return &____rousai_1; }
	inline void set__rousai_1(uint32_t value)
	{
		____rousai_1 = value;
	}

	inline static int32_t get_offset_of__fisasqemDesar_2() { return static_cast<int32_t>(offsetof(M_peesalzel182_t1693384776, ____fisasqemDesar_2)); }
	inline int32_t get__fisasqemDesar_2() const { return ____fisasqemDesar_2; }
	inline int32_t* get_address_of__fisasqemDesar_2() { return &____fisasqemDesar_2; }
	inline void set__fisasqemDesar_2(int32_t value)
	{
		____fisasqemDesar_2 = value;
	}

	inline static int32_t get_offset_of__qairtasee_3() { return static_cast<int32_t>(offsetof(M_peesalzel182_t1693384776, ____qairtasee_3)); }
	inline bool get__qairtasee_3() const { return ____qairtasee_3; }
	inline bool* get_address_of__qairtasee_3() { return &____qairtasee_3; }
	inline void set__qairtasee_3(bool value)
	{
		____qairtasee_3 = value;
	}

	inline static int32_t get_offset_of__poulaybis_4() { return static_cast<int32_t>(offsetof(M_peesalzel182_t1693384776, ____poulaybis_4)); }
	inline uint32_t get__poulaybis_4() const { return ____poulaybis_4; }
	inline uint32_t* get_address_of__poulaybis_4() { return &____poulaybis_4; }
	inline void set__poulaybis_4(uint32_t value)
	{
		____poulaybis_4 = value;
	}

	inline static int32_t get_offset_of__chasmileSaselhi_5() { return static_cast<int32_t>(offsetof(M_peesalzel182_t1693384776, ____chasmileSaselhi_5)); }
	inline int32_t get__chasmileSaselhi_5() const { return ____chasmileSaselhi_5; }
	inline int32_t* get_address_of__chasmileSaselhi_5() { return &____chasmileSaselhi_5; }
	inline void set__chasmileSaselhi_5(int32_t value)
	{
		____chasmileSaselhi_5 = value;
	}

	inline static int32_t get_offset_of__kahou_6() { return static_cast<int32_t>(offsetof(M_peesalzel182_t1693384776, ____kahou_6)); }
	inline float get__kahou_6() const { return ____kahou_6; }
	inline float* get_address_of__kahou_6() { return &____kahou_6; }
	inline void set__kahou_6(float value)
	{
		____kahou_6 = value;
	}

	inline static int32_t get_offset_of__nawhallSafayya_7() { return static_cast<int32_t>(offsetof(M_peesalzel182_t1693384776, ____nawhallSafayya_7)); }
	inline uint32_t get__nawhallSafayya_7() const { return ____nawhallSafayya_7; }
	inline uint32_t* get_address_of__nawhallSafayya_7() { return &____nawhallSafayya_7; }
	inline void set__nawhallSafayya_7(uint32_t value)
	{
		____nawhallSafayya_7 = value;
	}

	inline static int32_t get_offset_of__hetadeRijur_8() { return static_cast<int32_t>(offsetof(M_peesalzel182_t1693384776, ____hetadeRijur_8)); }
	inline float get__hetadeRijur_8() const { return ____hetadeRijur_8; }
	inline float* get_address_of__hetadeRijur_8() { return &____hetadeRijur_8; }
	inline void set__hetadeRijur_8(float value)
	{
		____hetadeRijur_8 = value;
	}

	inline static int32_t get_offset_of__fetadeGurjayrur_9() { return static_cast<int32_t>(offsetof(M_peesalzel182_t1693384776, ____fetadeGurjayrur_9)); }
	inline uint32_t get__fetadeGurjayrur_9() const { return ____fetadeGurjayrur_9; }
	inline uint32_t* get_address_of__fetadeGurjayrur_9() { return &____fetadeGurjayrur_9; }
	inline void set__fetadeGurjayrur_9(uint32_t value)
	{
		____fetadeGurjayrur_9 = value;
	}

	inline static int32_t get_offset_of__tispow_10() { return static_cast<int32_t>(offsetof(M_peesalzel182_t1693384776, ____tispow_10)); }
	inline uint32_t get__tispow_10() const { return ____tispow_10; }
	inline uint32_t* get_address_of__tispow_10() { return &____tispow_10; }
	inline void set__tispow_10(uint32_t value)
	{
		____tispow_10 = value;
	}

	inline static int32_t get_offset_of__supalerKusitrou_11() { return static_cast<int32_t>(offsetof(M_peesalzel182_t1693384776, ____supalerKusitrou_11)); }
	inline int32_t get__supalerKusitrou_11() const { return ____supalerKusitrou_11; }
	inline int32_t* get_address_of__supalerKusitrou_11() { return &____supalerKusitrou_11; }
	inline void set__supalerKusitrou_11(int32_t value)
	{
		____supalerKusitrou_11 = value;
	}

	inline static int32_t get_offset_of__loteasowBairtasfa_12() { return static_cast<int32_t>(offsetof(M_peesalzel182_t1693384776, ____loteasowBairtasfa_12)); }
	inline int32_t get__loteasowBairtasfa_12() const { return ____loteasowBairtasfa_12; }
	inline int32_t* get_address_of__loteasowBairtasfa_12() { return &____loteasowBairtasfa_12; }
	inline void set__loteasowBairtasfa_12(int32_t value)
	{
		____loteasowBairtasfa_12 = value;
	}

	inline static int32_t get_offset_of__meju_13() { return static_cast<int32_t>(offsetof(M_peesalzel182_t1693384776, ____meju_13)); }
	inline bool get__meju_13() const { return ____meju_13; }
	inline bool* get_address_of__meju_13() { return &____meju_13; }
	inline void set__meju_13(bool value)
	{
		____meju_13 = value;
	}

	inline static int32_t get_offset_of__jerere_14() { return static_cast<int32_t>(offsetof(M_peesalzel182_t1693384776, ____jerere_14)); }
	inline String_t* get__jerere_14() const { return ____jerere_14; }
	inline String_t** get_address_of__jerere_14() { return &____jerere_14; }
	inline void set__jerere_14(String_t* value)
	{
		____jerere_14 = value;
		Il2CppCodeGenWriteBarrier(&____jerere_14, value);
	}

	inline static int32_t get_offset_of__fidaJoonirce_15() { return static_cast<int32_t>(offsetof(M_peesalzel182_t1693384776, ____fidaJoonirce_15)); }
	inline float get__fidaJoonirce_15() const { return ____fidaJoonirce_15; }
	inline float* get_address_of__fidaJoonirce_15() { return &____fidaJoonirce_15; }
	inline void set__fidaJoonirce_15(float value)
	{
		____fidaJoonirce_15 = value;
	}

	inline static int32_t get_offset_of__curallBerqair_16() { return static_cast<int32_t>(offsetof(M_peesalzel182_t1693384776, ____curallBerqair_16)); }
	inline String_t* get__curallBerqair_16() const { return ____curallBerqair_16; }
	inline String_t** get_address_of__curallBerqair_16() { return &____curallBerqair_16; }
	inline void set__curallBerqair_16(String_t* value)
	{
		____curallBerqair_16 = value;
		Il2CppCodeGenWriteBarrier(&____curallBerqair_16, value);
	}

	inline static int32_t get_offset_of__selall_17() { return static_cast<int32_t>(offsetof(M_peesalzel182_t1693384776, ____selall_17)); }
	inline String_t* get__selall_17() const { return ____selall_17; }
	inline String_t** get_address_of__selall_17() { return &____selall_17; }
	inline void set__selall_17(String_t* value)
	{
		____selall_17 = value;
		Il2CppCodeGenWriteBarrier(&____selall_17, value);
	}

	inline static int32_t get_offset_of__masca_18() { return static_cast<int32_t>(offsetof(M_peesalzel182_t1693384776, ____masca_18)); }
	inline int32_t get__masca_18() const { return ____masca_18; }
	inline int32_t* get_address_of__masca_18() { return &____masca_18; }
	inline void set__masca_18(int32_t value)
	{
		____masca_18 = value;
	}

	inline static int32_t get_offset_of__hiselcai_19() { return static_cast<int32_t>(offsetof(M_peesalzel182_t1693384776, ____hiselcai_19)); }
	inline float get__hiselcai_19() const { return ____hiselcai_19; }
	inline float* get_address_of__hiselcai_19() { return &____hiselcai_19; }
	inline void set__hiselcai_19(float value)
	{
		____hiselcai_19 = value;
	}

	inline static int32_t get_offset_of__nawtoCiba_20() { return static_cast<int32_t>(offsetof(M_peesalzel182_t1693384776, ____nawtoCiba_20)); }
	inline float get__nawtoCiba_20() const { return ____nawtoCiba_20; }
	inline float* get_address_of__nawtoCiba_20() { return &____nawtoCiba_20; }
	inline void set__nawtoCiba_20(float value)
	{
		____nawtoCiba_20 = value;
	}

	inline static int32_t get_offset_of__soowitel_21() { return static_cast<int32_t>(offsetof(M_peesalzel182_t1693384776, ____soowitel_21)); }
	inline String_t* get__soowitel_21() const { return ____soowitel_21; }
	inline String_t** get_address_of__soowitel_21() { return &____soowitel_21; }
	inline void set__soowitel_21(String_t* value)
	{
		____soowitel_21 = value;
		Il2CppCodeGenWriteBarrier(&____soowitel_21, value);
	}

	inline static int32_t get_offset_of__fouzasdooKivasis_22() { return static_cast<int32_t>(offsetof(M_peesalzel182_t1693384776, ____fouzasdooKivasis_22)); }
	inline int32_t get__fouzasdooKivasis_22() const { return ____fouzasdooKivasis_22; }
	inline int32_t* get_address_of__fouzasdooKivasis_22() { return &____fouzasdooKivasis_22; }
	inline void set__fouzasdooKivasis_22(int32_t value)
	{
		____fouzasdooKivasis_22 = value;
	}

	inline static int32_t get_offset_of__jarcese_23() { return static_cast<int32_t>(offsetof(M_peesalzel182_t1693384776, ____jarcese_23)); }
	inline bool get__jarcese_23() const { return ____jarcese_23; }
	inline bool* get_address_of__jarcese_23() { return &____jarcese_23; }
	inline void set__jarcese_23(bool value)
	{
		____jarcese_23 = value;
	}

	inline static int32_t get_offset_of__coreeKehijow_24() { return static_cast<int32_t>(offsetof(M_peesalzel182_t1693384776, ____coreeKehijow_24)); }
	inline float get__coreeKehijow_24() const { return ____coreeKehijow_24; }
	inline float* get_address_of__coreeKehijow_24() { return &____coreeKehijow_24; }
	inline void set__coreeKehijow_24(float value)
	{
		____coreeKehijow_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
