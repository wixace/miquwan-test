﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WebUtil/<GetText>c__Iterator3
struct U3CGetTextU3Ec__Iterator3_t3632695840;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void WebUtil/<GetText>c__Iterator3::.ctor()
extern "C"  void U3CGetTextU3Ec__Iterator3__ctor_m1415473995 (U3CGetTextU3Ec__Iterator3_t3632695840 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object WebUtil/<GetText>c__Iterator3::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CGetTextU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3689439143 (U3CGetTextU3Ec__Iterator3_t3632695840 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object WebUtil/<GetText>c__Iterator3::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetTextU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m4010053947 (U3CGetTextU3Ec__Iterator3_t3632695840 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebUtil/<GetText>c__Iterator3::MoveNext()
extern "C"  bool U3CGetTextU3Ec__Iterator3_MoveNext_m4271091145 (U3CGetTextU3Ec__Iterator3_t3632695840 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebUtil/<GetText>c__Iterator3::Dispose()
extern "C"  void U3CGetTextU3Ec__Iterator3_Dispose_m2959126728 (U3CGetTextU3Ec__Iterator3_t3632695840 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebUtil/<GetText>c__Iterator3::Reset()
extern "C"  void U3CGetTextU3Ec__Iterator3_Reset_m3356874232 (U3CGetTextU3Ec__Iterator3_t3632695840 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
