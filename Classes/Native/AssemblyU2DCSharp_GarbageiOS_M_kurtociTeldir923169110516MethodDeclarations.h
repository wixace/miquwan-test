﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_kurtociTeldir92
struct M_kurtociTeldir92_t3169110516;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"

// System.Void GarbageiOS.M_kurtociTeldir92::.ctor()
extern "C"  void M_kurtociTeldir92__ctor_m2895566831 (M_kurtociTeldir92_t3169110516 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_kurtociTeldir92::M_surdalSojay0(System.String[],System.Int32)
extern "C"  void M_kurtociTeldir92_M_surdalSojay0_m3251215431 (M_kurtociTeldir92_t3169110516 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_kurtociTeldir92::M_ciscurbarRuku1(System.String[],System.Int32)
extern "C"  void M_kurtociTeldir92_M_ciscurbarRuku1_m2322514082 (M_kurtociTeldir92_t3169110516 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_kurtociTeldir92::M_stujaKowwe2(System.String[],System.Int32)
extern "C"  void M_kurtociTeldir92_M_stujaKowwe2_m811071854 (M_kurtociTeldir92_t3169110516 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
