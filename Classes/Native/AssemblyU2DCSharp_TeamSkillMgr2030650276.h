﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.List`1<TeamSkill>
struct List_1_t3185083556;
// TeamSkill
struct TeamSkill_t1816898004;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>
struct Dictionary_2_t1151101739;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TeamSkillMgr
struct  TeamSkillMgr_t2030650276  : public Il2CppObject
{
public:
	// System.Collections.Generic.List`1<TeamSkill> TeamSkillMgr::skillList
	List_1_t3185083556 * ___skillList_10;
	// System.Int32 TeamSkillMgr::rage
	int32_t ___rage_11;
	// System.Int32 TeamSkillMgr::maxRage
	int32_t ___maxRage_12;
	// System.Int32 TeamSkillMgr::curSelectIndex
	int32_t ___curSelectIndex_13;
	// System.Int32 TeamSkillMgr::curUseSkillIndex
	int32_t ___curUseSkillIndex_14;
	// TeamSkill TeamSkillMgr::curUseingSkill
	TeamSkill_t1816898004 * ___curUseingSkill_15;
	// System.Single TeamSkillMgr::startTime
	float ___startTime_16;
	// System.Single TeamSkillMgr::holdTime
	float ___holdTime_17;
	// System.Single TeamSkillMgr::curHoldTime
	float ___curHoldTime_18;
	// System.Single TeamSkillMgr::curOneHoldTime
	float ___curOneHoldTime_19;
	// System.Single TeamSkillMgr::curOneTime
	float ___curOneTime_20;
	// System.Single TeamSkillMgr::gradePoint
	float ___gradePoint_21;
	// System.Single TeamSkillMgr::gradeAllNum
	float ___gradeAllNum_22;
	// System.Boolean TeamSkillMgr::isCanClick
	bool ___isCanClick_23;
	// System.Single TeamSkillMgr::hideTime
	float ___hideTime_24;
	// System.Int32 TeamSkillMgr::maxLevel
	int32_t ___maxLevel_25;
	// System.Int32 TeamSkillMgr::level
	int32_t ___level_26;
	// System.Int32 TeamSkillMgr::curLevelExp
	int32_t ___curLevelExp_27;
	// System.Int32 TeamSkillMgr::accExp
	int32_t ___accExp_28;
	// System.Single TeamSkillMgr::curRatio
	float ___curRatio_29;
	// System.Single TeamSkillMgr::maxRatio
	float ___maxRatio_30;
	// System.Int32 TeamSkillMgr::effectid
	int32_t ___effectid_31;
	// System.Int32 TeamSkillMgr::residueDegree
	int32_t ___residueDegree_32;
	// System.Int32 TeamSkillMgr::allTimes
	int32_t ___allTimes_33;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32> TeamSkillMgr::indexDic
	Dictionary_2_t1151101739 * ___indexDic_34;
	// System.Boolean TeamSkillMgr::isInitSkill
	bool ___isInitSkill_35;
	// System.UInt32 TeamSkillMgr::delayID
	uint32_t ___delayID_36;
	// System.UInt32 TeamSkillMgr::hurtShowDelayID
	uint32_t ___hurtShowDelayID_37;
	// System.Int32 TeamSkillMgr::<selectid>k__BackingField
	int32_t ___U3CselectidU3Ek__BackingField_38;

public:
	inline static int32_t get_offset_of_skillList_10() { return static_cast<int32_t>(offsetof(TeamSkillMgr_t2030650276, ___skillList_10)); }
	inline List_1_t3185083556 * get_skillList_10() const { return ___skillList_10; }
	inline List_1_t3185083556 ** get_address_of_skillList_10() { return &___skillList_10; }
	inline void set_skillList_10(List_1_t3185083556 * value)
	{
		___skillList_10 = value;
		Il2CppCodeGenWriteBarrier(&___skillList_10, value);
	}

	inline static int32_t get_offset_of_rage_11() { return static_cast<int32_t>(offsetof(TeamSkillMgr_t2030650276, ___rage_11)); }
	inline int32_t get_rage_11() const { return ___rage_11; }
	inline int32_t* get_address_of_rage_11() { return &___rage_11; }
	inline void set_rage_11(int32_t value)
	{
		___rage_11 = value;
	}

	inline static int32_t get_offset_of_maxRage_12() { return static_cast<int32_t>(offsetof(TeamSkillMgr_t2030650276, ___maxRage_12)); }
	inline int32_t get_maxRage_12() const { return ___maxRage_12; }
	inline int32_t* get_address_of_maxRage_12() { return &___maxRage_12; }
	inline void set_maxRage_12(int32_t value)
	{
		___maxRage_12 = value;
	}

	inline static int32_t get_offset_of_curSelectIndex_13() { return static_cast<int32_t>(offsetof(TeamSkillMgr_t2030650276, ___curSelectIndex_13)); }
	inline int32_t get_curSelectIndex_13() const { return ___curSelectIndex_13; }
	inline int32_t* get_address_of_curSelectIndex_13() { return &___curSelectIndex_13; }
	inline void set_curSelectIndex_13(int32_t value)
	{
		___curSelectIndex_13 = value;
	}

	inline static int32_t get_offset_of_curUseSkillIndex_14() { return static_cast<int32_t>(offsetof(TeamSkillMgr_t2030650276, ___curUseSkillIndex_14)); }
	inline int32_t get_curUseSkillIndex_14() const { return ___curUseSkillIndex_14; }
	inline int32_t* get_address_of_curUseSkillIndex_14() { return &___curUseSkillIndex_14; }
	inline void set_curUseSkillIndex_14(int32_t value)
	{
		___curUseSkillIndex_14 = value;
	}

	inline static int32_t get_offset_of_curUseingSkill_15() { return static_cast<int32_t>(offsetof(TeamSkillMgr_t2030650276, ___curUseingSkill_15)); }
	inline TeamSkill_t1816898004 * get_curUseingSkill_15() const { return ___curUseingSkill_15; }
	inline TeamSkill_t1816898004 ** get_address_of_curUseingSkill_15() { return &___curUseingSkill_15; }
	inline void set_curUseingSkill_15(TeamSkill_t1816898004 * value)
	{
		___curUseingSkill_15 = value;
		Il2CppCodeGenWriteBarrier(&___curUseingSkill_15, value);
	}

	inline static int32_t get_offset_of_startTime_16() { return static_cast<int32_t>(offsetof(TeamSkillMgr_t2030650276, ___startTime_16)); }
	inline float get_startTime_16() const { return ___startTime_16; }
	inline float* get_address_of_startTime_16() { return &___startTime_16; }
	inline void set_startTime_16(float value)
	{
		___startTime_16 = value;
	}

	inline static int32_t get_offset_of_holdTime_17() { return static_cast<int32_t>(offsetof(TeamSkillMgr_t2030650276, ___holdTime_17)); }
	inline float get_holdTime_17() const { return ___holdTime_17; }
	inline float* get_address_of_holdTime_17() { return &___holdTime_17; }
	inline void set_holdTime_17(float value)
	{
		___holdTime_17 = value;
	}

	inline static int32_t get_offset_of_curHoldTime_18() { return static_cast<int32_t>(offsetof(TeamSkillMgr_t2030650276, ___curHoldTime_18)); }
	inline float get_curHoldTime_18() const { return ___curHoldTime_18; }
	inline float* get_address_of_curHoldTime_18() { return &___curHoldTime_18; }
	inline void set_curHoldTime_18(float value)
	{
		___curHoldTime_18 = value;
	}

	inline static int32_t get_offset_of_curOneHoldTime_19() { return static_cast<int32_t>(offsetof(TeamSkillMgr_t2030650276, ___curOneHoldTime_19)); }
	inline float get_curOneHoldTime_19() const { return ___curOneHoldTime_19; }
	inline float* get_address_of_curOneHoldTime_19() { return &___curOneHoldTime_19; }
	inline void set_curOneHoldTime_19(float value)
	{
		___curOneHoldTime_19 = value;
	}

	inline static int32_t get_offset_of_curOneTime_20() { return static_cast<int32_t>(offsetof(TeamSkillMgr_t2030650276, ___curOneTime_20)); }
	inline float get_curOneTime_20() const { return ___curOneTime_20; }
	inline float* get_address_of_curOneTime_20() { return &___curOneTime_20; }
	inline void set_curOneTime_20(float value)
	{
		___curOneTime_20 = value;
	}

	inline static int32_t get_offset_of_gradePoint_21() { return static_cast<int32_t>(offsetof(TeamSkillMgr_t2030650276, ___gradePoint_21)); }
	inline float get_gradePoint_21() const { return ___gradePoint_21; }
	inline float* get_address_of_gradePoint_21() { return &___gradePoint_21; }
	inline void set_gradePoint_21(float value)
	{
		___gradePoint_21 = value;
	}

	inline static int32_t get_offset_of_gradeAllNum_22() { return static_cast<int32_t>(offsetof(TeamSkillMgr_t2030650276, ___gradeAllNum_22)); }
	inline float get_gradeAllNum_22() const { return ___gradeAllNum_22; }
	inline float* get_address_of_gradeAllNum_22() { return &___gradeAllNum_22; }
	inline void set_gradeAllNum_22(float value)
	{
		___gradeAllNum_22 = value;
	}

	inline static int32_t get_offset_of_isCanClick_23() { return static_cast<int32_t>(offsetof(TeamSkillMgr_t2030650276, ___isCanClick_23)); }
	inline bool get_isCanClick_23() const { return ___isCanClick_23; }
	inline bool* get_address_of_isCanClick_23() { return &___isCanClick_23; }
	inline void set_isCanClick_23(bool value)
	{
		___isCanClick_23 = value;
	}

	inline static int32_t get_offset_of_hideTime_24() { return static_cast<int32_t>(offsetof(TeamSkillMgr_t2030650276, ___hideTime_24)); }
	inline float get_hideTime_24() const { return ___hideTime_24; }
	inline float* get_address_of_hideTime_24() { return &___hideTime_24; }
	inline void set_hideTime_24(float value)
	{
		___hideTime_24 = value;
	}

	inline static int32_t get_offset_of_maxLevel_25() { return static_cast<int32_t>(offsetof(TeamSkillMgr_t2030650276, ___maxLevel_25)); }
	inline int32_t get_maxLevel_25() const { return ___maxLevel_25; }
	inline int32_t* get_address_of_maxLevel_25() { return &___maxLevel_25; }
	inline void set_maxLevel_25(int32_t value)
	{
		___maxLevel_25 = value;
	}

	inline static int32_t get_offset_of_level_26() { return static_cast<int32_t>(offsetof(TeamSkillMgr_t2030650276, ___level_26)); }
	inline int32_t get_level_26() const { return ___level_26; }
	inline int32_t* get_address_of_level_26() { return &___level_26; }
	inline void set_level_26(int32_t value)
	{
		___level_26 = value;
	}

	inline static int32_t get_offset_of_curLevelExp_27() { return static_cast<int32_t>(offsetof(TeamSkillMgr_t2030650276, ___curLevelExp_27)); }
	inline int32_t get_curLevelExp_27() const { return ___curLevelExp_27; }
	inline int32_t* get_address_of_curLevelExp_27() { return &___curLevelExp_27; }
	inline void set_curLevelExp_27(int32_t value)
	{
		___curLevelExp_27 = value;
	}

	inline static int32_t get_offset_of_accExp_28() { return static_cast<int32_t>(offsetof(TeamSkillMgr_t2030650276, ___accExp_28)); }
	inline int32_t get_accExp_28() const { return ___accExp_28; }
	inline int32_t* get_address_of_accExp_28() { return &___accExp_28; }
	inline void set_accExp_28(int32_t value)
	{
		___accExp_28 = value;
	}

	inline static int32_t get_offset_of_curRatio_29() { return static_cast<int32_t>(offsetof(TeamSkillMgr_t2030650276, ___curRatio_29)); }
	inline float get_curRatio_29() const { return ___curRatio_29; }
	inline float* get_address_of_curRatio_29() { return &___curRatio_29; }
	inline void set_curRatio_29(float value)
	{
		___curRatio_29 = value;
	}

	inline static int32_t get_offset_of_maxRatio_30() { return static_cast<int32_t>(offsetof(TeamSkillMgr_t2030650276, ___maxRatio_30)); }
	inline float get_maxRatio_30() const { return ___maxRatio_30; }
	inline float* get_address_of_maxRatio_30() { return &___maxRatio_30; }
	inline void set_maxRatio_30(float value)
	{
		___maxRatio_30 = value;
	}

	inline static int32_t get_offset_of_effectid_31() { return static_cast<int32_t>(offsetof(TeamSkillMgr_t2030650276, ___effectid_31)); }
	inline int32_t get_effectid_31() const { return ___effectid_31; }
	inline int32_t* get_address_of_effectid_31() { return &___effectid_31; }
	inline void set_effectid_31(int32_t value)
	{
		___effectid_31 = value;
	}

	inline static int32_t get_offset_of_residueDegree_32() { return static_cast<int32_t>(offsetof(TeamSkillMgr_t2030650276, ___residueDegree_32)); }
	inline int32_t get_residueDegree_32() const { return ___residueDegree_32; }
	inline int32_t* get_address_of_residueDegree_32() { return &___residueDegree_32; }
	inline void set_residueDegree_32(int32_t value)
	{
		___residueDegree_32 = value;
	}

	inline static int32_t get_offset_of_allTimes_33() { return static_cast<int32_t>(offsetof(TeamSkillMgr_t2030650276, ___allTimes_33)); }
	inline int32_t get_allTimes_33() const { return ___allTimes_33; }
	inline int32_t* get_address_of_allTimes_33() { return &___allTimes_33; }
	inline void set_allTimes_33(int32_t value)
	{
		___allTimes_33 = value;
	}

	inline static int32_t get_offset_of_indexDic_34() { return static_cast<int32_t>(offsetof(TeamSkillMgr_t2030650276, ___indexDic_34)); }
	inline Dictionary_2_t1151101739 * get_indexDic_34() const { return ___indexDic_34; }
	inline Dictionary_2_t1151101739 ** get_address_of_indexDic_34() { return &___indexDic_34; }
	inline void set_indexDic_34(Dictionary_2_t1151101739 * value)
	{
		___indexDic_34 = value;
		Il2CppCodeGenWriteBarrier(&___indexDic_34, value);
	}

	inline static int32_t get_offset_of_isInitSkill_35() { return static_cast<int32_t>(offsetof(TeamSkillMgr_t2030650276, ___isInitSkill_35)); }
	inline bool get_isInitSkill_35() const { return ___isInitSkill_35; }
	inline bool* get_address_of_isInitSkill_35() { return &___isInitSkill_35; }
	inline void set_isInitSkill_35(bool value)
	{
		___isInitSkill_35 = value;
	}

	inline static int32_t get_offset_of_delayID_36() { return static_cast<int32_t>(offsetof(TeamSkillMgr_t2030650276, ___delayID_36)); }
	inline uint32_t get_delayID_36() const { return ___delayID_36; }
	inline uint32_t* get_address_of_delayID_36() { return &___delayID_36; }
	inline void set_delayID_36(uint32_t value)
	{
		___delayID_36 = value;
	}

	inline static int32_t get_offset_of_hurtShowDelayID_37() { return static_cast<int32_t>(offsetof(TeamSkillMgr_t2030650276, ___hurtShowDelayID_37)); }
	inline uint32_t get_hurtShowDelayID_37() const { return ___hurtShowDelayID_37; }
	inline uint32_t* get_address_of_hurtShowDelayID_37() { return &___hurtShowDelayID_37; }
	inline void set_hurtShowDelayID_37(uint32_t value)
	{
		___hurtShowDelayID_37 = value;
	}

	inline static int32_t get_offset_of_U3CselectidU3Ek__BackingField_38() { return static_cast<int32_t>(offsetof(TeamSkillMgr_t2030650276, ___U3CselectidU3Ek__BackingField_38)); }
	inline int32_t get_U3CselectidU3Ek__BackingField_38() const { return ___U3CselectidU3Ek__BackingField_38; }
	inline int32_t* get_address_of_U3CselectidU3Ek__BackingField_38() { return &___U3CselectidU3Ek__BackingField_38; }
	inline void set_U3CselectidU3Ek__BackingField_38(int32_t value)
	{
		___U3CselectidU3Ek__BackingField_38 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
