﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Light
struct Light_t4202674828;
// UnityEngine.Texture
struct Texture_t2526458961;
// UnityEngine.Flare
struct Flare_t4197217604;
// UnityEngine.Rendering.CommandBuffer
struct CommandBuffer_t1654378665;
// UnityEngine.Rendering.CommandBuffer[]
struct CommandBufferU5BU5D_t912290452;
// UnityEngine.Light[]
struct LightU5BU5D_t2617847237;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_LightType1292142182.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"
#include "UnityEngine_UnityEngine_LightShadows2297142369.h"
#include "UnityEngine_UnityEngine_Texture2526458961.h"
#include "UnityEngine_UnityEngine_Flare4197217604.h"
#include "UnityEngine_UnityEngine_LightRenderMode1347373125.h"
#include "UnityEngine_UnityEngine_Rendering_LightEvent2859988980.h"
#include "UnityEngine_UnityEngine_Rendering_CommandBuffer1654378665.h"

// System.Void UnityEngine.Light::.ctor()
extern "C"  void Light__ctor_m39748483 (Light_t4202674828 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.LightType UnityEngine.Light::get_type()
extern "C"  int32_t Light_get_type_m2820868906 (Light_t4202674828 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Light::set_type(UnityEngine.LightType)
extern "C"  void Light_set_type_m1196490817 (Light_t4202674828 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.Light::get_color()
extern "C"  Color_t4194546905  Light_get_color_m2336101442 (Light_t4202674828 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Light::set_color(UnityEngine.Color)
extern "C"  void Light_set_color_m763171967 (Light_t4202674828 * __this, Color_t4194546905  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Light::INTERNAL_get_color(UnityEngine.Color&)
extern "C"  void Light_INTERNAL_get_color_m4212442015 (Light_t4202674828 * __this, Color_t4194546905 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Light::INTERNAL_set_color(UnityEngine.Color&)
extern "C"  void Light_INTERNAL_set_color_m3939727787 (Light_t4202674828 * __this, Color_t4194546905 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Light::get_intensity()
extern "C"  float Light_get_intensity_m2688167127 (Light_t4202674828 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Light::set_intensity(System.Single)
extern "C"  void Light_set_intensity_m2689709876 (Light_t4202674828 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Light::get_bounceIntensity()
extern "C"  float Light_get_bounceIntensity_m3922518767 (Light_t4202674828 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Light::set_bounceIntensity(System.Single)
extern "C"  void Light_set_bounceIntensity_m2372134940 (Light_t4202674828 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.LightShadows UnityEngine.Light::get_shadows()
extern "C"  int32_t Light_get_shadows_m2514416582 (Light_t4202674828 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Light::set_shadows(UnityEngine.LightShadows)
extern "C"  void Light_set_shadows_m424232995 (Light_t4202674828 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Light::get_shadowStrength()
extern "C"  float Light_get_shadowStrength_m2533436351 (Light_t4202674828 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Light::set_shadowStrength(System.Single)
extern "C"  void Light_set_shadowStrength_m561788748 (Light_t4202674828 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Light::get_shadowBias()
extern "C"  float Light_get_shadowBias_m2033198775 (Light_t4202674828 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Light::set_shadowBias(System.Single)
extern "C"  void Light_set_shadowBias_m4199567188 (Light_t4202674828 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Light::get_shadowNormalBias()
extern "C"  float Light_get_shadowNormalBias_m3682575550 (Light_t4202674828 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Light::set_shadowNormalBias(System.Single)
extern "C"  void Light_set_shadowNormalBias_m2410927277 (Light_t4202674828 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Light::get_shadowNearPlane()
extern "C"  float Light_get_shadowNearPlane_m1388855288 (Light_t4202674828 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Light::set_shadowNearPlane(System.Single)
extern "C"  void Light_set_shadowNearPlane_m1465106867 (Light_t4202674828 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Light::get_range()
extern "C"  float Light_get_range_m1212996833 (Light_t4202674828 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Light::set_range(System.Single)
extern "C"  void Light_set_range_m1834313578 (Light_t4202674828 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Light::get_spotAngle()
extern "C"  float Light_get_spotAngle_m896974357 (Light_t4202674828 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Light::set_spotAngle(System.Single)
extern "C"  void Light_set_spotAngle_m3212810934 (Light_t4202674828 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Light::get_cookieSize()
extern "C"  float Light_get_cookieSize_m1875918915 (Light_t4202674828 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Light::set_cookieSize(System.Single)
extern "C"  void Light_set_cookieSize_m4121631048 (Light_t4202674828 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture UnityEngine.Light::get_cookie()
extern "C"  Texture_t2526458961 * Light_get_cookie_m3100121471 (Light_t4202674828 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Light::set_cookie(UnityEngine.Texture)
extern "C"  void Light_set_cookie_m2487166540 (Light_t4202674828 * __this, Texture_t2526458961 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Flare UnityEngine.Light::get_flare()
extern "C"  Flare_t4197217604 * Light_get_flare_m992106530 (Light_t4202674828 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Light::set_flare(UnityEngine.Flare)
extern "C"  void Light_set_flare_m3803200297 (Light_t4202674828 * __this, Flare_t4197217604 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.LightRenderMode UnityEngine.Light::get_renderMode()
extern "C"  int32_t Light_get_renderMode_m4104001832 (Light_t4202674828 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Light::set_renderMode(UnityEngine.LightRenderMode)
extern "C"  void Light_set_renderMode_m978994819 (Light_t4202674828 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Light::get_alreadyLightmapped()
extern "C"  bool Light_get_alreadyLightmapped_m4056043253 (Light_t4202674828 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Light::set_alreadyLightmapped(System.Boolean)
extern "C"  void Light_set_alreadyLightmapped_m1002339142 (Light_t4202674828 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Light::get_cullingMask()
extern "C"  int32_t Light_get_cullingMask_m1131851092 (Light_t4202674828 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Light::set_cullingMask(System.Int32)
extern "C"  void Light_set_cullingMask_m3653896753 (Light_t4202674828 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Light::AddCommandBuffer(UnityEngine.Rendering.LightEvent,UnityEngine.Rendering.CommandBuffer)
extern "C"  void Light_AddCommandBuffer_m3648265416 (Light_t4202674828 * __this, int32_t ___evt0, CommandBuffer_t1654378665 * ___buffer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Light::RemoveCommandBuffer(UnityEngine.Rendering.LightEvent,UnityEngine.Rendering.CommandBuffer)
extern "C"  void Light_RemoveCommandBuffer_m2098773829 (Light_t4202674828 * __this, int32_t ___evt0, CommandBuffer_t1654378665 * ___buffer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Light::RemoveCommandBuffers(UnityEngine.Rendering.LightEvent)
extern "C"  void Light_RemoveCommandBuffers_m1956243056 (Light_t4202674828 * __this, int32_t ___evt0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Light::RemoveAllCommandBuffers()
extern "C"  void Light_RemoveAllCommandBuffers_m2190667174 (Light_t4202674828 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rendering.CommandBuffer[] UnityEngine.Light::GetCommandBuffers(UnityEngine.Rendering.LightEvent)
extern "C"  CommandBufferU5BU5D_t912290452* Light_GetCommandBuffers_m4152762785 (Light_t4202674828 * __this, int32_t ___evt0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Light::get_commandBufferCount()
extern "C"  int32_t Light_get_commandBufferCount_m1931220334 (Light_t4202674828 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Light[] UnityEngine.Light::GetLights(UnityEngine.LightType,System.Int32)
extern "C"  LightU5BU5D_t2617847237* Light_GetLights_m390516612 (Il2CppObject * __this /* static, unused */, int32_t ___type0, int32_t ___layer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
