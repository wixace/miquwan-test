﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// JsDBMgrGenerated
struct JsDBMgrGenerated_t2552224670;
// JSVCall
struct JSVCall_t3708497963;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_String7231557.h"

// System.Void JsDBMgrGenerated::.ctor()
extern "C"  void JsDBMgrGenerated__ctor_m2620357821 (JsDBMgrGenerated_t2552224670 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JsDBMgrGenerated::JsDBMgr_progress(JSVCall)
extern "C"  void JsDBMgrGenerated_JsDBMgr_progress_m3144750245 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JsDBMgrGenerated::JsDBMgr_inited(JSVCall)
extern "C"  void JsDBMgrGenerated_JsDBMgr_inited_m1394538083 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JsDBMgrGenerated::JsDBMgr_Clear(JSVCall,System.Int32)
extern "C"  bool JsDBMgrGenerated_JsDBMgr_Clear_m3775711478 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JsDBMgrGenerated::JsDBMgr_GetCount__String(JSVCall,System.Int32)
extern "C"  bool JsDBMgrGenerated_JsDBMgr_GetCount__String_m1664770979 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JsDBMgrGenerated::JsDBMgr_GetCsInfo__String__Int32(JSVCall,System.Int32)
extern "C"  bool JsDBMgrGenerated_JsDBMgr_GetCsInfo__String__Int32_m4116385442 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JsDBMgrGenerated::JsDBMgr_GetCsList(JSVCall,System.Int32)
extern "C"  bool JsDBMgrGenerated_JsDBMgr_GetCsList_m2556704653 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JsDBMgrGenerated::JsDBMgr_GetInfo__String__Int32(JSVCall,System.Int32)
extern "C"  bool JsDBMgrGenerated_JsDBMgr_GetInfo__String__Int32_m3955274162 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JsDBMgrGenerated::JsDBMgr_GetKeys__String(JSVCall,System.Int32)
extern "C"  bool JsDBMgrGenerated_JsDBMgr_GetKeys__String_m518769316 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JsDBMgrGenerated::JsDBMgr_Load__ListT1_String(JSVCall,System.Int32)
extern "C"  bool JsDBMgrGenerated_JsDBMgr_Load__ListT1_String_m1895291640 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JsDBMgrGenerated::__Register()
extern "C"  void JsDBMgrGenerated___Register_m3930287658 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single JsDBMgrGenerated::ilo_get_progress1()
extern "C"  float JsDBMgrGenerated_ilo_get_progress1_m3039998551 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JsDBMgrGenerated::ilo_get_inited2()
extern "C"  bool JsDBMgrGenerated_ilo_get_inited2_m2775444718 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String JsDBMgrGenerated::ilo_getStringS3(System.Int32)
extern "C"  String_t* JsDBMgrGenerated_ilo_getStringS3_m1088943677 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 JsDBMgrGenerated::ilo_getInt324(System.Int32)
extern "C"  int32_t JsDBMgrGenerated_ilo_getInt324_m1411301191 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> JsDBMgrGenerated::ilo_GetCsInfo5(System.String,System.Int32)
extern "C"  Dictionary_2_t827649927 * JsDBMgrGenerated_ilo_GetCsInfo5_m1812265886 (Il2CppObject * __this /* static, unused */, String_t* ___tabName0, int32_t ___id1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JsDBMgrGenerated::ilo_setStringS6(System.Int32,System.String)
extern "C"  void JsDBMgrGenerated_ilo_setStringS6_m1787849611 (Il2CppObject * __this /* static, unused */, int32_t ___e0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
