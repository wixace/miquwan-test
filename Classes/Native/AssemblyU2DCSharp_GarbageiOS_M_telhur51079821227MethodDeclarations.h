﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_telhur5
struct M_telhur5_t1079821227;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"

// System.Void GarbageiOS.M_telhur5::.ctor()
extern "C"  void M_telhur5__ctor_m839808024 (M_telhur5_t1079821227 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_telhur5::M_corste0(System.String[],System.Int32)
extern "C"  void M_telhur5_M_corste0_m1466551341 (M_telhur5_t1079821227 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_telhur5::M_bosobawBouyem1(System.String[],System.Int32)
extern "C"  void M_telhur5_M_bosobawBouyem1_m13079518 (M_telhur5_t1079821227 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_telhur5::M_salsair2(System.String[],System.Int32)
extern "C"  void M_telhur5_M_salsair2_m1512044658 (M_telhur5_t1079821227 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_telhur5::M_troutuga3(System.String[],System.Int32)
extern "C"  void M_telhur5_M_troutuga3_m1799403759 (M_telhur5_t1079821227 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
