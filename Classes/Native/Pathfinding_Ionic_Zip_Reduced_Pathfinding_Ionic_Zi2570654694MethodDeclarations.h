﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.Ionic.Zip.BadCrcException
struct BadCrcException_t2570654694;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void Pathfinding.Ionic.Zip.BadCrcException::.ctor(System.String)
extern "C"  void BadCrcException__ctor_m4041793505 (BadCrcException_t2570654694 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
