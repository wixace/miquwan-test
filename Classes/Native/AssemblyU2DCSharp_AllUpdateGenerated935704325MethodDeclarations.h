﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AllUpdateGenerated
struct AllUpdateGenerated_t935704325;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void AllUpdateGenerated::.ctor()
extern "C"  void AllUpdateGenerated__ctor_m3545063222 (AllUpdateGenerated_t935704325 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AllUpdateGenerated::AllUpdate_AllUpdate1(JSVCall,System.Int32)
extern "C"  bool AllUpdateGenerated_AllUpdate_AllUpdate1_m477725934 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllUpdateGenerated::AllUpdate_ON_ENTER_APP(JSVCall)
extern "C"  void AllUpdateGenerated_AllUpdate_ON_ENTER_APP_m411174602 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllUpdateGenerated::AllUpdate_ON_QUIT_APP(JSVCall)
extern "C"  void AllUpdateGenerated_AllUpdate_ON_QUIT_APP_m2733805783 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AllUpdateGenerated::AllUpdate_AddUpdate__Object(JSVCall,System.Int32)
extern "C"  bool AllUpdateGenerated_AllUpdate_AddUpdate__Object_m3200014884 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AllUpdateGenerated::AllUpdate_RemoveUpdate__Object(JSVCall,System.Int32)
extern "C"  bool AllUpdateGenerated_AllUpdate_RemoveUpdate__Object_m334721203 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllUpdateGenerated::__Register()
extern "C"  void AllUpdateGenerated___Register_m318582609 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllUpdateGenerated::ilo_addJSCSRel1(System.Int32,System.Object)
extern "C"  void AllUpdateGenerated_ilo_addJSCSRel1_m2873103090 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllUpdateGenerated::ilo_AddUpdate2(System.Object)
extern "C"  void AllUpdateGenerated_ilo_AddUpdate2_m4213840075 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___update0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
