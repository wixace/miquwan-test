﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CSDatacfgManager
struct CSDatacfgManager_t1565254243;
// ChannelConfigCfg
struct ChannelConfigCfg_t4272979999;
// System.Collections.Generic.Dictionary`2<System.Int32,ChannelConfigCfg>
struct Dictionary_2_t4270243238;
// HeroEmbattleCfg
struct HeroEmbattleCfg_t4134124650;
// System.Collections.Generic.Dictionary`2<System.Int32,HeroEmbattleCfg>
struct Dictionary_2_t4131387889;
// JJCCoefficientCfg
struct JJCCoefficientCfg_t2755051730;
// System.Collections.Generic.Dictionary`2<System.Int32,JJCCoefficientCfg>
struct Dictionary_2_t2752314969;
// PetsCfg
struct PetsCfg_t988016752;
// System.Collections.Generic.Dictionary`2<System.Int32,PetsCfg>
struct Dictionary_2_t985279991;
// SoundCfg
struct SoundCfg_t1807275253;
// System.Collections.Generic.Dictionary`2<System.Int32,SoundCfg>
struct Dictionary_2_t1804538492;
// StoryCfg
struct StoryCfg_t1782371151;
// System.Collections.Generic.Dictionary`2<System.Int32,StoryCfg>
struct Dictionary_2_t1779634390;
// TextStringCfg
struct TextStringCfg_t884535238;
// System.Collections.Generic.Dictionary`2<System.Int32,TextStringCfg>
struct Dictionary_2_t881798477;
// UIEffectCfg
struct UIEffectCfg_t952653023;
// System.Collections.Generic.Dictionary`2<System.Int32,UIEffectCfg>
struct Dictionary_2_t949916262;
// all_configCfg
struct all_configCfg_t3865068708;
// System.Collections.Generic.Dictionary`2<System.Int32,all_configCfg>
struct Dictionary_2_t3862331947;
// asset_sharedCfg
struct asset_sharedCfg_t1901313840;
// System.Collections.Generic.Dictionary`2<System.Int32,asset_sharedCfg>
struct Dictionary_2_t1898577079;
// barrierGateCfg
struct barrierGateCfg_t3705088994;
// System.Collections.Generic.Dictionary`2<System.Int32,barrierGateCfg>
struct Dictionary_2_t3702352233;
// buffCfg
struct buffCfg_t227963665;
// System.Collections.Generic.Dictionary`2<System.Int32,buffCfg>
struct Dictionary_2_t225226904;
// cameraShotCfg
struct cameraShotCfg_t781157413;
// System.Collections.Generic.Dictionary`2<System.Int32,cameraShotCfg>
struct Dictionary_2_t778420652;
// checkpointCfg
struct checkpointCfg_t2816107964;
// System.Collections.Generic.Dictionary`2<System.Int32,checkpointCfg>
struct Dictionary_2_t2813371203;
// demoStoryCfg
struct demoStoryCfg_t1993162290;
// System.Collections.Generic.Dictionary`2<System.Int32,demoStoryCfg>
struct Dictionary_2_t1990425529;
// effectCfg
struct effectCfg_t2826279187;
// System.Collections.Generic.Dictionary`2<System.Int32,effectCfg>
struct Dictionary_2_t2823542426;
// elementCfg
struct elementCfg_t575911880;
// System.Collections.Generic.Dictionary`2<System.Int32,elementCfg>
struct Dictionary_2_t573175119;
// enemy_dropCfg
struct enemy_dropCfg_t1026960702;
// System.Collections.Generic.Dictionary`2<System.Int32,enemy_dropCfg>
struct Dictionary_2_t1024223941;
// friendNpcCfg
struct friendNpcCfg_t3890310657;
// System.Collections.Generic.Dictionary`2<System.Int32,friendNpcCfg>
struct Dictionary_2_t3887573896;
// guideCfg
struct guideCfg_t2981043144;
// System.Collections.Generic.Dictionary`2<System.Int32,guideCfg>
struct Dictionary_2_t2978306383;
// herosCfg
struct herosCfg_t3676934635;
// System.Collections.Generic.Dictionary`2<System.Int32,herosCfg>
struct Dictionary_2_t3674197874;
// monstersCfg
struct monstersCfg_t1542396363;
// System.Collections.Generic.Dictionary`2<System.Int32,monstersCfg>
struct Dictionary_2_t1539659602;
// npcAICfg
struct npcAICfg_t1948330203;
// System.Collections.Generic.Dictionary`2<System.Int32,npcAICfg>
struct Dictionary_2_t1945593442;
// npcAIBehaviorCfg
struct npcAIBehaviorCfg_t3456736809;
// System.Collections.Generic.Dictionary`2<System.Int32,npcAIBehaviorCfg>
struct Dictionary_2_t3454000048;
// npcAIConditionCfg
struct npcAIConditionCfg_t930197170;
// System.Collections.Generic.Dictionary`2<System.Int32,npcAIConditionCfg>
struct Dictionary_2_t927460409;
// portalCfg
struct portalCfg_t1117034584;
// System.Collections.Generic.Dictionary`2<System.Int32,portalCfg>
struct Dictionary_2_t1114297823;
// skillCfg
struct skillCfg_t2142425171;
// System.Collections.Generic.Dictionary`2<System.Int32,skillCfg>
struct Dictionary_2_t2139688410;
// skill_upgradeCfg
struct skill_upgradeCfg_t790726486;
// System.Collections.Generic.Dictionary`2<System.Int32,skill_upgradeCfg>
struct Dictionary_2_t787989725;
// superskillCfg
struct superskillCfg_t3024131278;
// System.Collections.Generic.Dictionary`2<System.Int32,superskillCfg>
struct Dictionary_2_t3021394517;
// taixuTipCfg
struct taixuTipCfg_t1268722370;
// System.Collections.Generic.Dictionary`2<System.Int32,taixuTipCfg>
struct Dictionary_2_t1265985609;
// towerbuffCfg
struct towerbuffCfg_t1755856872;
// System.Collections.Generic.Dictionary`2<System.Int32,towerbuffCfg>
struct Dictionary_2_t1753120111;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t2522024052;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void CSDatacfgManager::.ctor()
extern "C"  void CSDatacfgManager__ctor_m2645443480 (CSDatacfgManager_t1565254243 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CSDatacfgManager CSDatacfgManager::get_Instance()
extern "C"  CSDatacfgManager_t1565254243 * CSDatacfgManager_get_Instance_m1100340548 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ChannelConfigCfg CSDatacfgManager::GetChannelConfigCfg(System.Int32)
extern "C"  ChannelConfigCfg_t4272979999 * CSDatacfgManager_GetChannelConfigCfg_m4261555902 (CSDatacfgManager_t1565254243 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.Int32,ChannelConfigCfg> CSDatacfgManager::GetChannelConfigCfgDic()
extern "C"  Dictionary_2_t4270243238 * CSDatacfgManager_GetChannelConfigCfgDic_m255856203 (CSDatacfgManager_t1565254243 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HeroEmbattleCfg CSDatacfgManager::GetHeroEmbattleCfg(System.Int32)
extern "C"  HeroEmbattleCfg_t4134124650 * CSDatacfgManager_GetHeroEmbattleCfg_m3380588730 (CSDatacfgManager_t1565254243 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.Int32,HeroEmbattleCfg> CSDatacfgManager::GetHeroEmbattleCfgDic()
extern "C"  Dictionary_2_t4131387889 * CSDatacfgManager_GetHeroEmbattleCfgDic_m243007193 (CSDatacfgManager_t1565254243 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// JJCCoefficientCfg CSDatacfgManager::GetJJCCoefficientCfg(System.Int32)
extern "C"  JJCCoefficientCfg_t2755051730 * CSDatacfgManager_GetJJCCoefficientCfg_m1713674618 (CSDatacfgManager_t1565254243 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.Int32,JJCCoefficientCfg> CSDatacfgManager::GetJJCCoefficientCfgDic()
extern "C"  Dictionary_2_t2752314969 * CSDatacfgManager_GetJJCCoefficientCfgDic_m2342534089 (CSDatacfgManager_t1565254243 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PetsCfg CSDatacfgManager::GetPetsCfg(System.Int32)
extern "C"  PetsCfg_t988016752 * CSDatacfgManager_GetPetsCfg_m1748324218 (CSDatacfgManager_t1565254243 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.Int32,PetsCfg> CSDatacfgManager::GetPetsCfgDic()
extern "C"  Dictionary_2_t985279991 * CSDatacfgManager_GetPetsCfgDic_m1906437581 (CSDatacfgManager_t1565254243 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SoundCfg CSDatacfgManager::GetSoundCfg(System.Int32)
extern "C"  SoundCfg_t1807275253 * CSDatacfgManager_GetSoundCfg_m3656001770 (CSDatacfgManager_t1565254243 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.Int32,SoundCfg> CSDatacfgManager::GetSoundCfgDic()
extern "C"  Dictionary_2_t1804538492 * CSDatacfgManager_GetSoundCfgDic_m3894614283 (CSDatacfgManager_t1565254243 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// StoryCfg CSDatacfgManager::GetStoryCfg(System.Int32)
extern "C"  StoryCfg_t1782371151 * CSDatacfgManager_GetStoryCfg_m1698717982 (CSDatacfgManager_t1565254243 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.Int32,StoryCfg> CSDatacfgManager::GetStoryCfgDic()
extern "C"  Dictionary_2_t1779634390 * CSDatacfgManager_GetStoryCfgDic_m836721739 (CSDatacfgManager_t1565254243 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TextStringCfg CSDatacfgManager::GetTextStringCfg(System.Int32)
extern "C"  TextStringCfg_t884535238 * CSDatacfgManager_GetTextStringCfg_m3716470138 (CSDatacfgManager_t1565254243 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.Int32,TextStringCfg> CSDatacfgManager::GetTextStringCfgDic()
extern "C"  Dictionary_2_t881798477 * CSDatacfgManager_GetTextStringCfgDic_m2488253793 (CSDatacfgManager_t1565254243 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIEffectCfg CSDatacfgManager::GetUIEffectCfg(System.Int32)
extern "C"  UIEffectCfg_t952653023 * CSDatacfgManager_GetUIEffectCfg_m1782935514 (CSDatacfgManager_t1565254243 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.Int32,UIEffectCfg> CSDatacfgManager::GetUIEffectCfgDic()
extern "C"  Dictionary_2_t949916262 * CSDatacfgManager_GetUIEffectCfgDic_m1054375407 (CSDatacfgManager_t1565254243 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// all_configCfg CSDatacfgManager::Getall_configCfg(System.Int32)
extern "C"  all_configCfg_t3865068708 * CSDatacfgManager_Getall_configCfg_m317229242 (CSDatacfgManager_t1565254243 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.Int32,all_configCfg> CSDatacfgManager::Getall_configCfgDic()
extern "C"  Dictionary_2_t3862331947 * CSDatacfgManager_Getall_configCfgDic_m1515978789 (CSDatacfgManager_t1565254243 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// asset_sharedCfg CSDatacfgManager::Getasset_sharedCfg(System.Int32)
extern "C"  asset_sharedCfg_t1901313840 * CSDatacfgManager_Getasset_sharedCfg_m2701427834 (CSDatacfgManager_t1565254243 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.Int32,asset_sharedCfg> CSDatacfgManager::Getasset_sharedCfgDic()
extern "C"  Dictionary_2_t1898577079 * CSDatacfgManager_Getasset_sharedCfgDic_m3286566221 (CSDatacfgManager_t1565254243 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// barrierGateCfg CSDatacfgManager::GetbarrierGateCfg(System.Int32)
extern "C"  barrierGateCfg_t3705088994 * CSDatacfgManager_GetbarrierGateCfg_m2173110340 (CSDatacfgManager_t1565254243 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.Int32,barrierGateCfg> CSDatacfgManager::GetbarrierGateCfgDic()
extern "C"  Dictionary_2_t3702352233 * CSDatacfgManager_GetbarrierGateCfgDic_m1012172395 (CSDatacfgManager_t1565254243 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// buffCfg CSDatacfgManager::GetbuffCfg(System.Int32)
extern "C"  buffCfg_t227963665 * CSDatacfgManager_GetbuffCfg_m1620616986 (CSDatacfgManager_t1565254243 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.Int32,buffCfg> CSDatacfgManager::GetbuffCfgDic()
extern "C"  Dictionary_2_t225226904 * CSDatacfgManager_GetbuffCfgDic_m3177027979 (CSDatacfgManager_t1565254243 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// cameraShotCfg CSDatacfgManager::GetcameraShotCfg(System.Int32)
extern "C"  cameraShotCfg_t781157413 * CSDatacfgManager_GetcameraShotCfg_m3785189658 (CSDatacfgManager_t1565254243 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.Int32,cameraShotCfg> CSDatacfgManager::GetcameraShotCfgDic()
extern "C"  Dictionary_2_t778420652 * CSDatacfgManager_GetcameraShotCfgDic_m1338027747 (CSDatacfgManager_t1565254243 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// checkpointCfg CSDatacfgManager::GetcheckpointCfg(System.Int32)
extern "C"  checkpointCfg_t2816107964 * CSDatacfgManager_GetcheckpointCfg_m2084467130 (CSDatacfgManager_t1565254243 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.Int32,checkpointCfg> CSDatacfgManager::GetcheckpointCfgDic()
extern "C"  Dictionary_2_t2813371203 * CSDatacfgManager_GetcheckpointCfgDic_m1528743925 (CSDatacfgManager_t1565254243 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// demoStoryCfg CSDatacfgManager::GetdemoStoryCfg(System.Int32)
extern "C"  demoStoryCfg_t1993162290 * CSDatacfgManager_GetdemoStoryCfg_m1723084708 (CSDatacfgManager_t1565254243 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.Int32,demoStoryCfg> CSDatacfgManager::GetdemoStoryCfgDic()
extern "C"  Dictionary_2_t1990425529 * CSDatacfgManager_GetdemoStoryCfgDic_m216803115 (CSDatacfgManager_t1565254243 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// effectCfg CSDatacfgManager::GeteffectCfg(System.Int32)
extern "C"  effectCfg_t2826279187 * CSDatacfgManager_GeteffectCfg_m846400090 (CSDatacfgManager_t1565254243 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.Int32,effectCfg> CSDatacfgManager::GeteffectCfgDic()
extern "C"  Dictionary_2_t2823542426 * CSDatacfgManager_GeteffectCfgDic_m1139186055 (CSDatacfgManager_t1565254243 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// elementCfg CSDatacfgManager::GetelementCfg(System.Int32)
extern "C"  elementCfg_t575911880 * CSDatacfgManager_GetelementCfg_m2728124560 (CSDatacfgManager_t1565254243 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.Int32,elementCfg> CSDatacfgManager::GetelementCfgDic()
extern "C"  Dictionary_2_t573175119 * CSDatacfgManager_GetelementCfgDic_m859748139 (CSDatacfgManager_t1565254243 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// enemy_dropCfg CSDatacfgManager::Getenemy_dropCfg(System.Int32)
extern "C"  enemy_dropCfg_t1026960702 * CSDatacfgManager_Getenemy_dropCfg_m4155204218 (CSDatacfgManager_t1565254243 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.Int32,enemy_dropCfg> CSDatacfgManager::Getenemy_dropCfgDic()
extern "C"  Dictionary_2_t1024223941 * CSDatacfgManager_Getenemy_dropCfgDic_m3012719729 (CSDatacfgManager_t1565254243 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// friendNpcCfg CSDatacfgManager::GetfriendNpcCfg(System.Int32)
extern "C"  friendNpcCfg_t3890310657 * CSDatacfgManager_GetfriendNpcCfg_m2991767298 (CSDatacfgManager_t1565254243 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.Int32,friendNpcCfg> CSDatacfgManager::GetfriendNpcCfgDic()
extern "C"  Dictionary_2_t3887573896 * CSDatacfgManager_GetfriendNpcCfgDic_m2815718795 (CSDatacfgManager_t1565254243 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// guideCfg CSDatacfgManager::GetguideCfg(System.Int32)
extern "C"  guideCfg_t2981043144 * CSDatacfgManager_GetguideCfg_m2632718544 (CSDatacfgManager_t1565254243 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.Int32,guideCfg> CSDatacfgManager::GetguideCfgDic()
extern "C"  Dictionary_2_t2978306383 * CSDatacfgManager_GetguideCfgDic_m3780491115 (CSDatacfgManager_t1565254243 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// herosCfg CSDatacfgManager::GetherosCfg(System.Int32)
extern "C"  herosCfg_t3676934635 * CSDatacfgManager_GetherosCfg_m2902172502 (CSDatacfgManager_t1565254243 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.Int32,herosCfg> CSDatacfgManager::GetherosCfgDic()
extern "C"  Dictionary_2_t3674197874 * CSDatacfgManager_GetherosCfgDic_m2878865355 (CSDatacfgManager_t1565254243 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// monstersCfg CSDatacfgManager::GetmonstersCfg(System.Int32)
extern "C"  monstersCfg_t1542396363 * CSDatacfgManager_GetmonstersCfg_m3579911514 (CSDatacfgManager_t1565254243 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.Int32,monstersCfg> CSDatacfgManager::GetmonstersCfgDic()
extern "C"  Dictionary_2_t1539659602 * CSDatacfgManager_GetmonstersCfgDic_m2787292695 (CSDatacfgManager_t1565254243 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// npcAICfg CSDatacfgManager::GetnpcAICfg(System.Int32)
extern "C"  npcAICfg_t1948330203 * CSDatacfgManager_GetnpcAICfg_m47007030 (CSDatacfgManager_t1565254243 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.Int32,npcAICfg> CSDatacfgManager::GetnpcAICfgDic()
extern "C"  Dictionary_2_t1945593442 * CSDatacfgManager_GetnpcAICfgDic_m305405387 (CSDatacfgManager_t1565254243 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// npcAIBehaviorCfg CSDatacfgManager::GetnpcAIBehaviorCfg(System.Int32)
extern "C"  npcAIBehaviorCfg_t3456736809 * CSDatacfgManager_GetnpcAIBehaviorCfg_m546439250 (CSDatacfgManager_t1565254243 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.Int32,npcAIBehaviorCfg> CSDatacfgManager::GetnpcAIBehaviorCfgDic()
extern "C"  Dictionary_2_t3454000048 * CSDatacfgManager_GetnpcAIBehaviorCfgDic_m15064459 (CSDatacfgManager_t1565254243 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// npcAIConditionCfg CSDatacfgManager::GetnpcAIConditionCfg(System.Int32)
extern "C"  npcAIConditionCfg_t930197170 * CSDatacfgManager_GetnpcAIConditionCfg_m3599220090 (CSDatacfgManager_t1565254243 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.Int32,npcAIConditionCfg> CSDatacfgManager::GetnpcAIConditionCfgDic()
extern "C"  Dictionary_2_t927460409 * CSDatacfgManager_GetnpcAIConditionCfgDic_m825038857 (CSDatacfgManager_t1565254243 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// portalCfg CSDatacfgManager::GetportalCfg(System.Int32)
extern "C"  portalCfg_t1117034584 * CSDatacfgManager_GetportalCfg_m3640129978 (CSDatacfgManager_t1565254243 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.Int32,portalCfg> CSDatacfgManager::GetportalCfgDic()
extern "C"  Dictionary_2_t1114297823 * CSDatacfgManager_GetportalCfgDic_m1811281213 (CSDatacfgManager_t1565254243 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// skillCfg CSDatacfgManager::GetskillCfg(System.Int32)
extern "C"  skillCfg_t2142425171 * CSDatacfgManager_GetskillCfg_m1310117414 (CSDatacfgManager_t1565254243 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.Int32,skillCfg> CSDatacfgManager::GetskillCfgDic()
extern "C"  Dictionary_2_t2139688410 * CSDatacfgManager_GetskillCfgDic_m1745726667 (CSDatacfgManager_t1565254243 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// skill_upgradeCfg CSDatacfgManager::Getskill_upgradeCfg(System.Int32)
extern "C"  skill_upgradeCfg_t790726486 * CSDatacfgManager_Getskill_upgradeCfg_m3743839084 (CSDatacfgManager_t1565254243 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.Int32,skill_upgradeCfg> CSDatacfgManager::Getskill_upgradeCfgDic()
extern "C"  Dictionary_2_t787989725 * CSDatacfgManager_Getskill_upgradeCfgDic_m1979859499 (CSDatacfgManager_t1565254243 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// superskillCfg CSDatacfgManager::GetsuperskillCfg(System.Int32)
extern "C"  superskillCfg_t3024131278 * CSDatacfgManager_GetsuperskillCfg_m1071331450 (CSDatacfgManager_t1565254243 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.Int32,superskillCfg> CSDatacfgManager::GetsuperskillCfgDic()
extern "C"  Dictionary_2_t3021394517 * CSDatacfgManager_GetsuperskillCfgDic_m690856273 (CSDatacfgManager_t1565254243 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// taixuTipCfg CSDatacfgManager::GettaixuTipCfg(System.Int32)
extern "C"  taixuTipCfg_t1268722370 * CSDatacfgManager_GettaixuTipCfg_m3780694074 (CSDatacfgManager_t1565254243 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.Int32,taixuTipCfg> CSDatacfgManager::GettaixuTipCfgDic()
extern "C"  Dictionary_2_t1265985609 * CSDatacfgManager_GettaixuTipCfgDic_m2931490473 (CSDatacfgManager_t1565254243 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// towerbuffCfg CSDatacfgManager::GettowerbuffCfg(System.Int32)
extern "C"  towerbuffCfg_t1755856872 * CSDatacfgManager_GettowerbuffCfg_m536836240 (CSDatacfgManager_t1565254243 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.Int32,towerbuffCfg> CSDatacfgManager::GettowerbuffCfgDic()
extern "C"  Dictionary_2_t1753120111 * CSDatacfgManager_GettowerbuffCfgDic_m1279285995 (CSDatacfgManager_t1565254243 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.Int32> CSDatacfgManager::ilo_GetKeys1(System.String)
extern "C"  List_1_t2522024052 * CSDatacfgManager_ilo_GetKeys1_m3780676346 (Il2CppObject * __this /* static, unused */, String_t* ___tabName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CSDatacfgManager::ilo_GetCount2(System.String)
extern "C"  int32_t CSDatacfgManager_ilo_GetCount2_m2260511924 (Il2CppObject * __this /* static, unused */, String_t* ___tabName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
