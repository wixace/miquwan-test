﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PluginShoumeng
struct PluginShoumeng_t2011739967;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// System.String
struct String_t;
// CEvent.EventFunc`1<CEvent.ZEvent>
struct EventFunc_1_t1114914310;
// PluginsSdkMgr
struct PluginsSdkMgr_t3884624670;
// VersionMgr
struct VersionMgr_t1322950208;
// ProductsCfgMgr
struct ProductsCfgMgr_t2493714872;
// System.Object
struct Il2CppObject;
// System.Action
struct Action_t3771233898;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_PluginsSdkMgr3884624670.h"
#include "AssemblyU2DCSharp_PluginShoumeng2011739967.h"
#include "AssemblyU2DCSharp_ProductsCfgMgr2493714872.h"
#include "mscorlib_System_Object4170816371.h"
#include "System_Core_System_Action3771233898.h"

// System.Void PluginShoumeng::.ctor()
extern "C"  void PluginShoumeng__ctor_m4045261884 (PluginShoumeng_t2011739967 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginShoumeng::Init()
extern "C"  void PluginShoumeng_Init_m359123608 (PluginShoumeng_t2011739967 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginShoumeng::ReqSDKHttpLogin()
extern "C"  Dictionary_2_t827649927 * PluginShoumeng_ReqSDKHttpLogin_m914201873 (PluginShoumeng_t2011739967 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginShoumeng::IsLoginSuccess(System.Boolean)
extern "C"  bool PluginShoumeng_IsLoginSuccess_m303518359 (PluginShoumeng_t2011739967 * __this, bool ___isOpenUseLogin0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginShoumeng::OpenUserLogin()
extern "C"  void PluginShoumeng_OpenUserLogin_m106230414 (PluginShoumeng_t2011739967 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginShoumeng::RolelvUpinfo(CEvent.ZEvent)
extern "C"  void PluginShoumeng_RolelvUpinfo_m1768963434 (PluginShoumeng_t2011739967 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginShoumeng::UserPay(CEvent.ZEvent)
extern "C"  void PluginShoumeng_UserPay_m1138836772 (PluginShoumeng_t2011739967 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginShoumeng::OnInitSuccess(System.String)
extern "C"  void PluginShoumeng_OnInitSuccess_m3747543284 (PluginShoumeng_t2011739967 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginShoumeng::OnInitFail(System.String)
extern "C"  void PluginShoumeng_OnInitFail_m96877773 (PluginShoumeng_t2011739967 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginShoumeng::OnLoginSuccess(System.String)
extern "C"  void PluginShoumeng_OnLoginSuccess_m599436865 (PluginShoumeng_t2011739967 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginShoumeng::OnLoginFail(System.String)
extern "C"  void PluginShoumeng_OnLoginFail_m1633047200 (PluginShoumeng_t2011739967 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginShoumeng::OnPaySuccess(System.String)
extern "C"  void PluginShoumeng_OnPaySuccess_m3595152576 (PluginShoumeng_t2011739967 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginShoumeng::OnPayFail(System.String)
extern "C"  void PluginShoumeng_OnPayFail_m3955293313 (PluginShoumeng_t2011739967 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginShoumeng::OnExitGameSuccess(System.String)
extern "C"  void PluginShoumeng_OnExitGameSuccess_m319554324 (PluginShoumeng_t2011739967 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginShoumeng::LogoutSuccess()
extern "C"  void PluginShoumeng_LogoutSuccess_m57522483 (PluginShoumeng_t2011739967 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginShoumeng::OnLogoutSuccess(System.String)
extern "C"  void PluginShoumeng_OnLogoutSuccess_m1406216302 (PluginShoumeng_t2011739967 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginShoumeng::OnLogoutFail(System.String)
extern "C"  void PluginShoumeng_OnLogoutFail_m3192704915 (PluginShoumeng_t2011739967 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginShoumeng::EnterGame(CEvent.ZEvent)
extern "C"  void PluginShoumeng_EnterGame_m1422024119 (PluginShoumeng_t2011739967 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginShoumeng::SMSDKInit(System.String,System.String)
extern "C"  void PluginShoumeng_SMSDKInit_m3504627668 (PluginShoumeng_t2011739967 * __this, String_t* ___gameId0, String_t* ___pakcageId1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginShoumeng::SMSDKLogin()
extern "C"  void PluginShoumeng_SMSDKLogin_m3262535089 (PluginShoumeng_t2011739967 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginShoumeng::SMSDKLogout()
extern "C"  void PluginShoumeng_SMSDKLogout_m2360162660 (PluginShoumeng_t2011739967 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginShoumeng::SMSDKShut()
extern "C"  void PluginShoumeng_SMSDKShut_m2377817934 (PluginShoumeng_t2011739967 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginShoumeng::SMSDKCreate(System.String,System.String,System.String,System.String)
extern "C"  void PluginShoumeng_SMSDKCreate_m1063375136 (PluginShoumeng_t2011739967 * __this, String_t* ___roleName0, String_t* ___roleId1, String_t* ___serverId2, String_t* ___serverName3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginShoumeng::SMSDKRole(System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginShoumeng_SMSDKRole_m555801186 (PluginShoumeng_t2011739967 * __this, String_t* ___serverId0, String_t* ___serverName1, String_t* ___roleId2, String_t* ___roleName3, String_t* ___roleLevel4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginShoumeng::SMSDKPay(System.String,System.String,System.String,System.String)
extern "C"  void PluginShoumeng_SMSDKPay_m1180656454 (PluginShoumeng_t2011739967 * __this, String_t* ___serverId0, String_t* ___extendInfo1, String_t* ___Price2, String_t* ___productId3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginShoumeng::<OnLogoutSuccess>m__454()
extern "C"  void PluginShoumeng_U3COnLogoutSuccessU3Em__454_m2110885312 (PluginShoumeng_t2011739967 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginShoumeng::ilo_AddEventListener1(System.String,CEvent.EventFunc`1<CEvent.ZEvent>)
extern "C"  void PluginShoumeng_ilo_AddEventListener1_m141135160 (Il2CppObject * __this /* static, unused */, String_t* ___eventType0, EventFunc_1_t1114914310 * ___fun1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PluginShoumeng::ilo_get_DeviceID2(PluginsSdkMgr)
extern "C"  String_t* PluginShoumeng_ilo_get_DeviceID2_m2592761560 (Il2CppObject * __this /* static, unused */, PluginsSdkMgr_t3884624670 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionMgr PluginShoumeng::ilo_get_Instance3()
extern "C"  VersionMgr_t1322950208 * PluginShoumeng_ilo_get_Instance3_m864956991 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginShoumeng::ilo_OpenUserLogin4(PluginShoumeng)
extern "C"  void PluginShoumeng_ilo_OpenUserLogin4_m1903645212 (Il2CppObject * __this /* static, unused */, PluginShoumeng_t2011739967 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginShoumeng::ilo_SMSDKLogin5(PluginShoumeng)
extern "C"  void PluginShoumeng_ilo_SMSDKLogin5_m2956116116 (Il2CppObject * __this /* static, unused */, PluginShoumeng_t2011739967 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProductsCfgMgr PluginShoumeng::ilo_get_ProductsCfgMgr6()
extern "C"  ProductsCfgMgr_t2493714872 * PluginShoumeng_ilo_get_ProductsCfgMgr6_m3304251799 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PluginShoumeng::ilo_GetProductID7(ProductsCfgMgr,System.String)
extern "C"  String_t* PluginShoumeng_ilo_GetProductID7_m1070468459 (Il2CppObject * __this /* static, unused */, ProductsCfgMgr_t2493714872 * ____this0, String_t* ___name1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginShoumeng::ilo_SMSDKPay8(PluginShoumeng,System.String,System.String,System.String,System.String)
extern "C"  void PluginShoumeng_ilo_SMSDKPay8_m978381736 (Il2CppObject * __this /* static, unused */, PluginShoumeng_t2011739967 * ____this0, String_t* ___serverId1, String_t* ___extendInfo2, String_t* ___Price3, String_t* ___productId4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PluginsSdkMgr PluginShoumeng::ilo_get_PluginsSdkMgr9()
extern "C"  PluginsSdkMgr_t3884624670 * PluginShoumeng_ilo_get_PluginsSdkMgr9_m36626612 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginShoumeng::ilo_Log10(System.Object,System.Boolean)
extern "C"  void PluginShoumeng_ilo_Log10_m3060404051 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginShoumeng::ilo_Logout11(System.Action)
extern "C"  void PluginShoumeng_ilo_Logout11_m3084148216 (Il2CppObject * __this /* static, unused */, Action_t3771233898 * ___call0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
