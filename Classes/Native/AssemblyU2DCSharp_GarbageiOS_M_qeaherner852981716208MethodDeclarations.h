﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_qeaherner85
struct M_qeaherner85_t2981716208;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_qeaherner852981716208.h"

// System.Void GarbageiOS.M_qeaherner85::.ctor()
extern "C"  void M_qeaherner85__ctor_m3564578931 (M_qeaherner85_t2981716208 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_qeaherner85::M_caihowDever0(System.String[],System.Int32)
extern "C"  void M_qeaherner85_M_caihowDever0_m413991917 (M_qeaherner85_t2981716208 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_qeaherner85::M_melcejo1(System.String[],System.Int32)
extern "C"  void M_qeaherner85_M_melcejo1_m317285968 (M_qeaherner85_t2981716208 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_qeaherner85::M_risbesoRisracher2(System.String[],System.Int32)
extern "C"  void M_qeaherner85_M_risbesoRisracher2_m1346209286 (M_qeaherner85_t2981716208 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_qeaherner85::M_xawtrairDurmamow3(System.String[],System.Int32)
extern "C"  void M_qeaherner85_M_xawtrairDurmamow3_m1348420811 (M_qeaherner85_t2981716208 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_qeaherner85::ilo_M_melcejo11(GarbageiOS.M_qeaherner85,System.String[],System.Int32)
extern "C"  void M_qeaherner85_ilo_M_melcejo11_m624283710 (Il2CppObject * __this /* static, unused */, M_qeaherner85_t2981716208 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_qeaherner85::ilo_M_risbesoRisracher22(GarbageiOS.M_qeaherner85,System.String[],System.Int32)
extern "C"  void M_qeaherner85_ilo_M_risbesoRisracher22_m419897805 (Il2CppObject * __this /* static, unused */, M_qeaherner85_t2981716208 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
