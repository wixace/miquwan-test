﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_morheewhowSarvee220
struct  M_morheewhowSarvee220_t1650565453  : public Il2CppObject
{
public:
	// System.Single GarbageiOS.M_morheewhowSarvee220::_wusowgor
	float ____wusowgor_0;
	// System.Int32 GarbageiOS.M_morheewhowSarvee220::_barra
	int32_t ____barra_1;
	// System.Int32 GarbageiOS.M_morheewhowSarvee220::_nasemwhaiRoufairke
	int32_t ____nasemwhaiRoufairke_2;
	// System.Boolean GarbageiOS.M_morheewhowSarvee220::_kejoJasitrel
	bool ____kejoJasitrel_3;
	// System.Int32 GarbageiOS.M_morheewhowSarvee220::_foosturqa
	int32_t ____foosturqa_4;
	// System.Boolean GarbageiOS.M_morheewhowSarvee220::_wheetaWakousay
	bool ____wheetaWakousay_5;
	// System.String GarbageiOS.M_morheewhowSarvee220::_baysti
	String_t* ____baysti_6;
	// System.String GarbageiOS.M_morheewhowSarvee220::_roudurtemZemci
	String_t* ____roudurtemZemci_7;
	// System.Boolean GarbageiOS.M_morheewhowSarvee220::_seata
	bool ____seata_8;
	// System.Single GarbageiOS.M_morheewhowSarvee220::_regikere
	float ____regikere_9;
	// System.Single GarbageiOS.M_morheewhowSarvee220::_raiwhow
	float ____raiwhow_10;
	// System.Boolean GarbageiOS.M_morheewhowSarvee220::_ditercha
	bool ____ditercha_11;
	// System.Single GarbageiOS.M_morheewhowSarvee220::_souniPusa
	float ____souniPusa_12;
	// System.UInt32 GarbageiOS.M_morheewhowSarvee220::_xerediWayvawnas
	uint32_t ____xerediWayvawnas_13;
	// System.UInt32 GarbageiOS.M_morheewhowSarvee220::_lisrelsurNairdersi
	uint32_t ____lisrelsurNairdersi_14;
	// System.UInt32 GarbageiOS.M_morheewhowSarvee220::_gaidejouTolousta
	uint32_t ____gaidejouTolousta_15;
	// System.Single GarbageiOS.M_morheewhowSarvee220::_maytirle
	float ____maytirle_16;
	// System.Boolean GarbageiOS.M_morheewhowSarvee220::_neseesis
	bool ____neseesis_17;
	// System.String GarbageiOS.M_morheewhowSarvee220::_hairca
	String_t* ____hairca_18;
	// System.String GarbageiOS.M_morheewhowSarvee220::_lecere
	String_t* ____lecere_19;
	// System.Boolean GarbageiOS.M_morheewhowSarvee220::_whowwa
	bool ____whowwa_20;
	// System.Boolean GarbageiOS.M_morheewhowSarvee220::_starwirjeaJistamou
	bool ____starwirjeaJistamou_21;
	// System.Int32 GarbageiOS.M_morheewhowSarvee220::_hallsisce
	int32_t ____hallsisce_22;
	// System.Int32 GarbageiOS.M_morheewhowSarvee220::_waqasmiCardoo
	int32_t ____waqasmiCardoo_23;
	// System.UInt32 GarbageiOS.M_morheewhowSarvee220::_herechurChewhay
	uint32_t ____herechurChewhay_24;
	// System.UInt32 GarbageiOS.M_morheewhowSarvee220::_ricairstallKavalcer
	uint32_t ____ricairstallKavalcer_25;
	// System.Int32 GarbageiOS.M_morheewhowSarvee220::_duseeche
	int32_t ____duseeche_26;
	// System.Boolean GarbageiOS.M_morheewhowSarvee220::_koopere
	bool ____koopere_27;
	// System.Boolean GarbageiOS.M_morheewhowSarvee220::_jenerXermur
	bool ____jenerXermur_28;
	// System.Single GarbageiOS.M_morheewhowSarvee220::_nallife
	float ____nallife_29;
	// System.Int32 GarbageiOS.M_morheewhowSarvee220::_nouta
	int32_t ____nouta_30;
	// System.UInt32 GarbageiOS.M_morheewhowSarvee220::_gasowLetri
	uint32_t ____gasowLetri_31;
	// System.Single GarbageiOS.M_morheewhowSarvee220::_jistair
	float ____jistair_32;
	// System.Single GarbageiOS.M_morheewhowSarvee220::_gelaMemone
	float ____gelaMemone_33;
	// System.Boolean GarbageiOS.M_morheewhowSarvee220::_moumafo
	bool ____moumafo_34;
	// System.UInt32 GarbageiOS.M_morheewhowSarvee220::_surqiJalcer
	uint32_t ____surqiJalcer_35;

public:
	inline static int32_t get_offset_of__wusowgor_0() { return static_cast<int32_t>(offsetof(M_morheewhowSarvee220_t1650565453, ____wusowgor_0)); }
	inline float get__wusowgor_0() const { return ____wusowgor_0; }
	inline float* get_address_of__wusowgor_0() { return &____wusowgor_0; }
	inline void set__wusowgor_0(float value)
	{
		____wusowgor_0 = value;
	}

	inline static int32_t get_offset_of__barra_1() { return static_cast<int32_t>(offsetof(M_morheewhowSarvee220_t1650565453, ____barra_1)); }
	inline int32_t get__barra_1() const { return ____barra_1; }
	inline int32_t* get_address_of__barra_1() { return &____barra_1; }
	inline void set__barra_1(int32_t value)
	{
		____barra_1 = value;
	}

	inline static int32_t get_offset_of__nasemwhaiRoufairke_2() { return static_cast<int32_t>(offsetof(M_morheewhowSarvee220_t1650565453, ____nasemwhaiRoufairke_2)); }
	inline int32_t get__nasemwhaiRoufairke_2() const { return ____nasemwhaiRoufairke_2; }
	inline int32_t* get_address_of__nasemwhaiRoufairke_2() { return &____nasemwhaiRoufairke_2; }
	inline void set__nasemwhaiRoufairke_2(int32_t value)
	{
		____nasemwhaiRoufairke_2 = value;
	}

	inline static int32_t get_offset_of__kejoJasitrel_3() { return static_cast<int32_t>(offsetof(M_morheewhowSarvee220_t1650565453, ____kejoJasitrel_3)); }
	inline bool get__kejoJasitrel_3() const { return ____kejoJasitrel_3; }
	inline bool* get_address_of__kejoJasitrel_3() { return &____kejoJasitrel_3; }
	inline void set__kejoJasitrel_3(bool value)
	{
		____kejoJasitrel_3 = value;
	}

	inline static int32_t get_offset_of__foosturqa_4() { return static_cast<int32_t>(offsetof(M_morheewhowSarvee220_t1650565453, ____foosturqa_4)); }
	inline int32_t get__foosturqa_4() const { return ____foosturqa_4; }
	inline int32_t* get_address_of__foosturqa_4() { return &____foosturqa_4; }
	inline void set__foosturqa_4(int32_t value)
	{
		____foosturqa_4 = value;
	}

	inline static int32_t get_offset_of__wheetaWakousay_5() { return static_cast<int32_t>(offsetof(M_morheewhowSarvee220_t1650565453, ____wheetaWakousay_5)); }
	inline bool get__wheetaWakousay_5() const { return ____wheetaWakousay_5; }
	inline bool* get_address_of__wheetaWakousay_5() { return &____wheetaWakousay_5; }
	inline void set__wheetaWakousay_5(bool value)
	{
		____wheetaWakousay_5 = value;
	}

	inline static int32_t get_offset_of__baysti_6() { return static_cast<int32_t>(offsetof(M_morheewhowSarvee220_t1650565453, ____baysti_6)); }
	inline String_t* get__baysti_6() const { return ____baysti_6; }
	inline String_t** get_address_of__baysti_6() { return &____baysti_6; }
	inline void set__baysti_6(String_t* value)
	{
		____baysti_6 = value;
		Il2CppCodeGenWriteBarrier(&____baysti_6, value);
	}

	inline static int32_t get_offset_of__roudurtemZemci_7() { return static_cast<int32_t>(offsetof(M_morheewhowSarvee220_t1650565453, ____roudurtemZemci_7)); }
	inline String_t* get__roudurtemZemci_7() const { return ____roudurtemZemci_7; }
	inline String_t** get_address_of__roudurtemZemci_7() { return &____roudurtemZemci_7; }
	inline void set__roudurtemZemci_7(String_t* value)
	{
		____roudurtemZemci_7 = value;
		Il2CppCodeGenWriteBarrier(&____roudurtemZemci_7, value);
	}

	inline static int32_t get_offset_of__seata_8() { return static_cast<int32_t>(offsetof(M_morheewhowSarvee220_t1650565453, ____seata_8)); }
	inline bool get__seata_8() const { return ____seata_8; }
	inline bool* get_address_of__seata_8() { return &____seata_8; }
	inline void set__seata_8(bool value)
	{
		____seata_8 = value;
	}

	inline static int32_t get_offset_of__regikere_9() { return static_cast<int32_t>(offsetof(M_morheewhowSarvee220_t1650565453, ____regikere_9)); }
	inline float get__regikere_9() const { return ____regikere_9; }
	inline float* get_address_of__regikere_9() { return &____regikere_9; }
	inline void set__regikere_9(float value)
	{
		____regikere_9 = value;
	}

	inline static int32_t get_offset_of__raiwhow_10() { return static_cast<int32_t>(offsetof(M_morheewhowSarvee220_t1650565453, ____raiwhow_10)); }
	inline float get__raiwhow_10() const { return ____raiwhow_10; }
	inline float* get_address_of__raiwhow_10() { return &____raiwhow_10; }
	inline void set__raiwhow_10(float value)
	{
		____raiwhow_10 = value;
	}

	inline static int32_t get_offset_of__ditercha_11() { return static_cast<int32_t>(offsetof(M_morheewhowSarvee220_t1650565453, ____ditercha_11)); }
	inline bool get__ditercha_11() const { return ____ditercha_11; }
	inline bool* get_address_of__ditercha_11() { return &____ditercha_11; }
	inline void set__ditercha_11(bool value)
	{
		____ditercha_11 = value;
	}

	inline static int32_t get_offset_of__souniPusa_12() { return static_cast<int32_t>(offsetof(M_morheewhowSarvee220_t1650565453, ____souniPusa_12)); }
	inline float get__souniPusa_12() const { return ____souniPusa_12; }
	inline float* get_address_of__souniPusa_12() { return &____souniPusa_12; }
	inline void set__souniPusa_12(float value)
	{
		____souniPusa_12 = value;
	}

	inline static int32_t get_offset_of__xerediWayvawnas_13() { return static_cast<int32_t>(offsetof(M_morheewhowSarvee220_t1650565453, ____xerediWayvawnas_13)); }
	inline uint32_t get__xerediWayvawnas_13() const { return ____xerediWayvawnas_13; }
	inline uint32_t* get_address_of__xerediWayvawnas_13() { return &____xerediWayvawnas_13; }
	inline void set__xerediWayvawnas_13(uint32_t value)
	{
		____xerediWayvawnas_13 = value;
	}

	inline static int32_t get_offset_of__lisrelsurNairdersi_14() { return static_cast<int32_t>(offsetof(M_morheewhowSarvee220_t1650565453, ____lisrelsurNairdersi_14)); }
	inline uint32_t get__lisrelsurNairdersi_14() const { return ____lisrelsurNairdersi_14; }
	inline uint32_t* get_address_of__lisrelsurNairdersi_14() { return &____lisrelsurNairdersi_14; }
	inline void set__lisrelsurNairdersi_14(uint32_t value)
	{
		____lisrelsurNairdersi_14 = value;
	}

	inline static int32_t get_offset_of__gaidejouTolousta_15() { return static_cast<int32_t>(offsetof(M_morheewhowSarvee220_t1650565453, ____gaidejouTolousta_15)); }
	inline uint32_t get__gaidejouTolousta_15() const { return ____gaidejouTolousta_15; }
	inline uint32_t* get_address_of__gaidejouTolousta_15() { return &____gaidejouTolousta_15; }
	inline void set__gaidejouTolousta_15(uint32_t value)
	{
		____gaidejouTolousta_15 = value;
	}

	inline static int32_t get_offset_of__maytirle_16() { return static_cast<int32_t>(offsetof(M_morheewhowSarvee220_t1650565453, ____maytirle_16)); }
	inline float get__maytirle_16() const { return ____maytirle_16; }
	inline float* get_address_of__maytirle_16() { return &____maytirle_16; }
	inline void set__maytirle_16(float value)
	{
		____maytirle_16 = value;
	}

	inline static int32_t get_offset_of__neseesis_17() { return static_cast<int32_t>(offsetof(M_morheewhowSarvee220_t1650565453, ____neseesis_17)); }
	inline bool get__neseesis_17() const { return ____neseesis_17; }
	inline bool* get_address_of__neseesis_17() { return &____neseesis_17; }
	inline void set__neseesis_17(bool value)
	{
		____neseesis_17 = value;
	}

	inline static int32_t get_offset_of__hairca_18() { return static_cast<int32_t>(offsetof(M_morheewhowSarvee220_t1650565453, ____hairca_18)); }
	inline String_t* get__hairca_18() const { return ____hairca_18; }
	inline String_t** get_address_of__hairca_18() { return &____hairca_18; }
	inline void set__hairca_18(String_t* value)
	{
		____hairca_18 = value;
		Il2CppCodeGenWriteBarrier(&____hairca_18, value);
	}

	inline static int32_t get_offset_of__lecere_19() { return static_cast<int32_t>(offsetof(M_morheewhowSarvee220_t1650565453, ____lecere_19)); }
	inline String_t* get__lecere_19() const { return ____lecere_19; }
	inline String_t** get_address_of__lecere_19() { return &____lecere_19; }
	inline void set__lecere_19(String_t* value)
	{
		____lecere_19 = value;
		Il2CppCodeGenWriteBarrier(&____lecere_19, value);
	}

	inline static int32_t get_offset_of__whowwa_20() { return static_cast<int32_t>(offsetof(M_morheewhowSarvee220_t1650565453, ____whowwa_20)); }
	inline bool get__whowwa_20() const { return ____whowwa_20; }
	inline bool* get_address_of__whowwa_20() { return &____whowwa_20; }
	inline void set__whowwa_20(bool value)
	{
		____whowwa_20 = value;
	}

	inline static int32_t get_offset_of__starwirjeaJistamou_21() { return static_cast<int32_t>(offsetof(M_morheewhowSarvee220_t1650565453, ____starwirjeaJistamou_21)); }
	inline bool get__starwirjeaJistamou_21() const { return ____starwirjeaJistamou_21; }
	inline bool* get_address_of__starwirjeaJistamou_21() { return &____starwirjeaJistamou_21; }
	inline void set__starwirjeaJistamou_21(bool value)
	{
		____starwirjeaJistamou_21 = value;
	}

	inline static int32_t get_offset_of__hallsisce_22() { return static_cast<int32_t>(offsetof(M_morheewhowSarvee220_t1650565453, ____hallsisce_22)); }
	inline int32_t get__hallsisce_22() const { return ____hallsisce_22; }
	inline int32_t* get_address_of__hallsisce_22() { return &____hallsisce_22; }
	inline void set__hallsisce_22(int32_t value)
	{
		____hallsisce_22 = value;
	}

	inline static int32_t get_offset_of__waqasmiCardoo_23() { return static_cast<int32_t>(offsetof(M_morheewhowSarvee220_t1650565453, ____waqasmiCardoo_23)); }
	inline int32_t get__waqasmiCardoo_23() const { return ____waqasmiCardoo_23; }
	inline int32_t* get_address_of__waqasmiCardoo_23() { return &____waqasmiCardoo_23; }
	inline void set__waqasmiCardoo_23(int32_t value)
	{
		____waqasmiCardoo_23 = value;
	}

	inline static int32_t get_offset_of__herechurChewhay_24() { return static_cast<int32_t>(offsetof(M_morheewhowSarvee220_t1650565453, ____herechurChewhay_24)); }
	inline uint32_t get__herechurChewhay_24() const { return ____herechurChewhay_24; }
	inline uint32_t* get_address_of__herechurChewhay_24() { return &____herechurChewhay_24; }
	inline void set__herechurChewhay_24(uint32_t value)
	{
		____herechurChewhay_24 = value;
	}

	inline static int32_t get_offset_of__ricairstallKavalcer_25() { return static_cast<int32_t>(offsetof(M_morheewhowSarvee220_t1650565453, ____ricairstallKavalcer_25)); }
	inline uint32_t get__ricairstallKavalcer_25() const { return ____ricairstallKavalcer_25; }
	inline uint32_t* get_address_of__ricairstallKavalcer_25() { return &____ricairstallKavalcer_25; }
	inline void set__ricairstallKavalcer_25(uint32_t value)
	{
		____ricairstallKavalcer_25 = value;
	}

	inline static int32_t get_offset_of__duseeche_26() { return static_cast<int32_t>(offsetof(M_morheewhowSarvee220_t1650565453, ____duseeche_26)); }
	inline int32_t get__duseeche_26() const { return ____duseeche_26; }
	inline int32_t* get_address_of__duseeche_26() { return &____duseeche_26; }
	inline void set__duseeche_26(int32_t value)
	{
		____duseeche_26 = value;
	}

	inline static int32_t get_offset_of__koopere_27() { return static_cast<int32_t>(offsetof(M_morheewhowSarvee220_t1650565453, ____koopere_27)); }
	inline bool get__koopere_27() const { return ____koopere_27; }
	inline bool* get_address_of__koopere_27() { return &____koopere_27; }
	inline void set__koopere_27(bool value)
	{
		____koopere_27 = value;
	}

	inline static int32_t get_offset_of__jenerXermur_28() { return static_cast<int32_t>(offsetof(M_morheewhowSarvee220_t1650565453, ____jenerXermur_28)); }
	inline bool get__jenerXermur_28() const { return ____jenerXermur_28; }
	inline bool* get_address_of__jenerXermur_28() { return &____jenerXermur_28; }
	inline void set__jenerXermur_28(bool value)
	{
		____jenerXermur_28 = value;
	}

	inline static int32_t get_offset_of__nallife_29() { return static_cast<int32_t>(offsetof(M_morheewhowSarvee220_t1650565453, ____nallife_29)); }
	inline float get__nallife_29() const { return ____nallife_29; }
	inline float* get_address_of__nallife_29() { return &____nallife_29; }
	inline void set__nallife_29(float value)
	{
		____nallife_29 = value;
	}

	inline static int32_t get_offset_of__nouta_30() { return static_cast<int32_t>(offsetof(M_morheewhowSarvee220_t1650565453, ____nouta_30)); }
	inline int32_t get__nouta_30() const { return ____nouta_30; }
	inline int32_t* get_address_of__nouta_30() { return &____nouta_30; }
	inline void set__nouta_30(int32_t value)
	{
		____nouta_30 = value;
	}

	inline static int32_t get_offset_of__gasowLetri_31() { return static_cast<int32_t>(offsetof(M_morheewhowSarvee220_t1650565453, ____gasowLetri_31)); }
	inline uint32_t get__gasowLetri_31() const { return ____gasowLetri_31; }
	inline uint32_t* get_address_of__gasowLetri_31() { return &____gasowLetri_31; }
	inline void set__gasowLetri_31(uint32_t value)
	{
		____gasowLetri_31 = value;
	}

	inline static int32_t get_offset_of__jistair_32() { return static_cast<int32_t>(offsetof(M_morheewhowSarvee220_t1650565453, ____jistair_32)); }
	inline float get__jistair_32() const { return ____jistair_32; }
	inline float* get_address_of__jistair_32() { return &____jistair_32; }
	inline void set__jistair_32(float value)
	{
		____jistair_32 = value;
	}

	inline static int32_t get_offset_of__gelaMemone_33() { return static_cast<int32_t>(offsetof(M_morheewhowSarvee220_t1650565453, ____gelaMemone_33)); }
	inline float get__gelaMemone_33() const { return ____gelaMemone_33; }
	inline float* get_address_of__gelaMemone_33() { return &____gelaMemone_33; }
	inline void set__gelaMemone_33(float value)
	{
		____gelaMemone_33 = value;
	}

	inline static int32_t get_offset_of__moumafo_34() { return static_cast<int32_t>(offsetof(M_morheewhowSarvee220_t1650565453, ____moumafo_34)); }
	inline bool get__moumafo_34() const { return ____moumafo_34; }
	inline bool* get_address_of__moumafo_34() { return &____moumafo_34; }
	inline void set__moumafo_34(bool value)
	{
		____moumafo_34 = value;
	}

	inline static int32_t get_offset_of__surqiJalcer_35() { return static_cast<int32_t>(offsetof(M_morheewhowSarvee220_t1650565453, ____surqiJalcer_35)); }
	inline uint32_t get__surqiJalcer_35() const { return ____surqiJalcer_35; }
	inline uint32_t* get_address_of__surqiJalcer_35() { return &____surqiJalcer_35; }
	inline void set__surqiJalcer_35(uint32_t value)
	{
		____surqiJalcer_35 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
