﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2970305593.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24187962917.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Single>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m757015786_gshared (InternalEnumerator_1_t2970305593 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m757015786(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t2970305593 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m757015786_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Single>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1357022326_gshared (InternalEnumerator_1_t2970305593 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1357022326(__this, method) ((  void (*) (InternalEnumerator_1_t2970305593 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1357022326_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Single>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m74472620_gshared (InternalEnumerator_1_t2970305593 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m74472620(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t2970305593 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m74472620_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Single>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2534402881_gshared (InternalEnumerator_1_t2970305593 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m2534402881(__this, method) ((  void (*) (InternalEnumerator_1_t2970305593 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2534402881_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Single>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2662844582_gshared (InternalEnumerator_1_t2970305593 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m2662844582(__this, method) ((  bool (*) (InternalEnumerator_1_t2970305593 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m2662844582_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Single>>::get_Current()
extern "C"  KeyValuePair_2_t4187962917  InternalEnumerator_1_get_Current_m3209870419_gshared (InternalEnumerator_1_t2970305593 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m3209870419(__this, method) ((  KeyValuePair_2_t4187962917  (*) (InternalEnumerator_1_t2970305593 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3209870419_gshared)(__this, method)
