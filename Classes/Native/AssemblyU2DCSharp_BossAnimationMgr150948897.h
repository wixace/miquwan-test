﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// UnityEngine.Camera
struct Camera_t2727095145;
// CombatEntity
struct CombatEntity_t684137495;
// System.Collections.Generic.List`1<BAMCameras>
struct List_1_t3029200112;
// ShaderBossShow
struct ShaderBossShow_t500563759;
// BossInofShow
struct BossInofShow_t241030;
// BossAnimationInfo
struct BossAnimationInfo_t384335813;
// AnimationPointJSC
struct AnimationPointJSC_t487595022;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BossAnimationMgr
struct  BossAnimationMgr_t150948897  : public MonoBehaviour_t667441552
{
public:
	// System.Boolean BossAnimationMgr::_isEditorPattern
	bool ____isEditorPattern_4;
	// UnityEngine.GameObject BossAnimationMgr::BossUIAsset
	GameObject_t3674682005 * ___BossUIAsset_5;
	// UnityEngine.GameObject BossAnimationMgr::Targer
	GameObject_t3674682005 * ___Targer_6;
	// UnityEngine.Camera BossAnimationMgr::BossCamera
	Camera_t2727095145 * ___BossCamera_7;
	// System.Int32 BossAnimationMgr::BossAniId
	int32_t ___BossAniId_8;
	// System.Boolean BossAnimationMgr::_isTriger
	bool ____isTriger_9;
	// CombatEntity BossAnimationMgr::_boss
	CombatEntity_t684137495 * ____boss_10;
	// UnityEngine.Vector3 BossAnimationMgr::_bossPos
	Vector3_t4282066566  ____bossPos_11;
	// UnityEngine.Quaternion BossAnimationMgr::_bossRot
	Quaternion_t1553702882  ____bossRot_12;
	// System.Collections.Generic.List`1<BAMCameras> BossAnimationMgr::cameraList
	List_1_t3029200112 * ___cameraList_13;
	// ShaderBossShow BossAnimationMgr::_shaderShow
	ShaderBossShow_t500563759 * ____shaderShow_14;
	// BossInofShow BossAnimationMgr::_bossInfoShow
	BossInofShow_t241030 * ____bossInfoShow_15;
	// BossAnimationInfo BossAnimationMgr::_aniJsc
	BossAnimationInfo_t384335813 * ____aniJsc_16;
	// System.Int32 BossAnimationMgr::_camPointIndex
	int32_t ____camPointIndex_17;
	// System.Int32 BossAnimationMgr::_bosPointIndex
	int32_t ____bosPointIndex_18;
	// System.Boolean BossAnimationMgr::_isCamPlaying
	bool ____isCamPlaying_19;
	// System.Boolean BossAnimationMgr::_isBosPlaying
	bool ____isBosPlaying_20;
	// AnimationPointJSC BossAnimationMgr::_curCamAni
	AnimationPointJSC_t487595022 * ____curCamAni_21;
	// AnimationPointJSC BossAnimationMgr::_curBosAni
	AnimationPointJSC_t487595022 * ____curBosAni_22;
	// System.Boolean BossAnimationMgr::<IsPlaying>k__BackingField
	bool ___U3CIsPlayingU3Ek__BackingField_23;

public:
	inline static int32_t get_offset_of__isEditorPattern_4() { return static_cast<int32_t>(offsetof(BossAnimationMgr_t150948897, ____isEditorPattern_4)); }
	inline bool get__isEditorPattern_4() const { return ____isEditorPattern_4; }
	inline bool* get_address_of__isEditorPattern_4() { return &____isEditorPattern_4; }
	inline void set__isEditorPattern_4(bool value)
	{
		____isEditorPattern_4 = value;
	}

	inline static int32_t get_offset_of_BossUIAsset_5() { return static_cast<int32_t>(offsetof(BossAnimationMgr_t150948897, ___BossUIAsset_5)); }
	inline GameObject_t3674682005 * get_BossUIAsset_5() const { return ___BossUIAsset_5; }
	inline GameObject_t3674682005 ** get_address_of_BossUIAsset_5() { return &___BossUIAsset_5; }
	inline void set_BossUIAsset_5(GameObject_t3674682005 * value)
	{
		___BossUIAsset_5 = value;
		Il2CppCodeGenWriteBarrier(&___BossUIAsset_5, value);
	}

	inline static int32_t get_offset_of_Targer_6() { return static_cast<int32_t>(offsetof(BossAnimationMgr_t150948897, ___Targer_6)); }
	inline GameObject_t3674682005 * get_Targer_6() const { return ___Targer_6; }
	inline GameObject_t3674682005 ** get_address_of_Targer_6() { return &___Targer_6; }
	inline void set_Targer_6(GameObject_t3674682005 * value)
	{
		___Targer_6 = value;
		Il2CppCodeGenWriteBarrier(&___Targer_6, value);
	}

	inline static int32_t get_offset_of_BossCamera_7() { return static_cast<int32_t>(offsetof(BossAnimationMgr_t150948897, ___BossCamera_7)); }
	inline Camera_t2727095145 * get_BossCamera_7() const { return ___BossCamera_7; }
	inline Camera_t2727095145 ** get_address_of_BossCamera_7() { return &___BossCamera_7; }
	inline void set_BossCamera_7(Camera_t2727095145 * value)
	{
		___BossCamera_7 = value;
		Il2CppCodeGenWriteBarrier(&___BossCamera_7, value);
	}

	inline static int32_t get_offset_of_BossAniId_8() { return static_cast<int32_t>(offsetof(BossAnimationMgr_t150948897, ___BossAniId_8)); }
	inline int32_t get_BossAniId_8() const { return ___BossAniId_8; }
	inline int32_t* get_address_of_BossAniId_8() { return &___BossAniId_8; }
	inline void set_BossAniId_8(int32_t value)
	{
		___BossAniId_8 = value;
	}

	inline static int32_t get_offset_of__isTriger_9() { return static_cast<int32_t>(offsetof(BossAnimationMgr_t150948897, ____isTriger_9)); }
	inline bool get__isTriger_9() const { return ____isTriger_9; }
	inline bool* get_address_of__isTriger_9() { return &____isTriger_9; }
	inline void set__isTriger_9(bool value)
	{
		____isTriger_9 = value;
	}

	inline static int32_t get_offset_of__boss_10() { return static_cast<int32_t>(offsetof(BossAnimationMgr_t150948897, ____boss_10)); }
	inline CombatEntity_t684137495 * get__boss_10() const { return ____boss_10; }
	inline CombatEntity_t684137495 ** get_address_of__boss_10() { return &____boss_10; }
	inline void set__boss_10(CombatEntity_t684137495 * value)
	{
		____boss_10 = value;
		Il2CppCodeGenWriteBarrier(&____boss_10, value);
	}

	inline static int32_t get_offset_of__bossPos_11() { return static_cast<int32_t>(offsetof(BossAnimationMgr_t150948897, ____bossPos_11)); }
	inline Vector3_t4282066566  get__bossPos_11() const { return ____bossPos_11; }
	inline Vector3_t4282066566 * get_address_of__bossPos_11() { return &____bossPos_11; }
	inline void set__bossPos_11(Vector3_t4282066566  value)
	{
		____bossPos_11 = value;
	}

	inline static int32_t get_offset_of__bossRot_12() { return static_cast<int32_t>(offsetof(BossAnimationMgr_t150948897, ____bossRot_12)); }
	inline Quaternion_t1553702882  get__bossRot_12() const { return ____bossRot_12; }
	inline Quaternion_t1553702882 * get_address_of__bossRot_12() { return &____bossRot_12; }
	inline void set__bossRot_12(Quaternion_t1553702882  value)
	{
		____bossRot_12 = value;
	}

	inline static int32_t get_offset_of_cameraList_13() { return static_cast<int32_t>(offsetof(BossAnimationMgr_t150948897, ___cameraList_13)); }
	inline List_1_t3029200112 * get_cameraList_13() const { return ___cameraList_13; }
	inline List_1_t3029200112 ** get_address_of_cameraList_13() { return &___cameraList_13; }
	inline void set_cameraList_13(List_1_t3029200112 * value)
	{
		___cameraList_13 = value;
		Il2CppCodeGenWriteBarrier(&___cameraList_13, value);
	}

	inline static int32_t get_offset_of__shaderShow_14() { return static_cast<int32_t>(offsetof(BossAnimationMgr_t150948897, ____shaderShow_14)); }
	inline ShaderBossShow_t500563759 * get__shaderShow_14() const { return ____shaderShow_14; }
	inline ShaderBossShow_t500563759 ** get_address_of__shaderShow_14() { return &____shaderShow_14; }
	inline void set__shaderShow_14(ShaderBossShow_t500563759 * value)
	{
		____shaderShow_14 = value;
		Il2CppCodeGenWriteBarrier(&____shaderShow_14, value);
	}

	inline static int32_t get_offset_of__bossInfoShow_15() { return static_cast<int32_t>(offsetof(BossAnimationMgr_t150948897, ____bossInfoShow_15)); }
	inline BossInofShow_t241030 * get__bossInfoShow_15() const { return ____bossInfoShow_15; }
	inline BossInofShow_t241030 ** get_address_of__bossInfoShow_15() { return &____bossInfoShow_15; }
	inline void set__bossInfoShow_15(BossInofShow_t241030 * value)
	{
		____bossInfoShow_15 = value;
		Il2CppCodeGenWriteBarrier(&____bossInfoShow_15, value);
	}

	inline static int32_t get_offset_of__aniJsc_16() { return static_cast<int32_t>(offsetof(BossAnimationMgr_t150948897, ____aniJsc_16)); }
	inline BossAnimationInfo_t384335813 * get__aniJsc_16() const { return ____aniJsc_16; }
	inline BossAnimationInfo_t384335813 ** get_address_of__aniJsc_16() { return &____aniJsc_16; }
	inline void set__aniJsc_16(BossAnimationInfo_t384335813 * value)
	{
		____aniJsc_16 = value;
		Il2CppCodeGenWriteBarrier(&____aniJsc_16, value);
	}

	inline static int32_t get_offset_of__camPointIndex_17() { return static_cast<int32_t>(offsetof(BossAnimationMgr_t150948897, ____camPointIndex_17)); }
	inline int32_t get__camPointIndex_17() const { return ____camPointIndex_17; }
	inline int32_t* get_address_of__camPointIndex_17() { return &____camPointIndex_17; }
	inline void set__camPointIndex_17(int32_t value)
	{
		____camPointIndex_17 = value;
	}

	inline static int32_t get_offset_of__bosPointIndex_18() { return static_cast<int32_t>(offsetof(BossAnimationMgr_t150948897, ____bosPointIndex_18)); }
	inline int32_t get__bosPointIndex_18() const { return ____bosPointIndex_18; }
	inline int32_t* get_address_of__bosPointIndex_18() { return &____bosPointIndex_18; }
	inline void set__bosPointIndex_18(int32_t value)
	{
		____bosPointIndex_18 = value;
	}

	inline static int32_t get_offset_of__isCamPlaying_19() { return static_cast<int32_t>(offsetof(BossAnimationMgr_t150948897, ____isCamPlaying_19)); }
	inline bool get__isCamPlaying_19() const { return ____isCamPlaying_19; }
	inline bool* get_address_of__isCamPlaying_19() { return &____isCamPlaying_19; }
	inline void set__isCamPlaying_19(bool value)
	{
		____isCamPlaying_19 = value;
	}

	inline static int32_t get_offset_of__isBosPlaying_20() { return static_cast<int32_t>(offsetof(BossAnimationMgr_t150948897, ____isBosPlaying_20)); }
	inline bool get__isBosPlaying_20() const { return ____isBosPlaying_20; }
	inline bool* get_address_of__isBosPlaying_20() { return &____isBosPlaying_20; }
	inline void set__isBosPlaying_20(bool value)
	{
		____isBosPlaying_20 = value;
	}

	inline static int32_t get_offset_of__curCamAni_21() { return static_cast<int32_t>(offsetof(BossAnimationMgr_t150948897, ____curCamAni_21)); }
	inline AnimationPointJSC_t487595022 * get__curCamAni_21() const { return ____curCamAni_21; }
	inline AnimationPointJSC_t487595022 ** get_address_of__curCamAni_21() { return &____curCamAni_21; }
	inline void set__curCamAni_21(AnimationPointJSC_t487595022 * value)
	{
		____curCamAni_21 = value;
		Il2CppCodeGenWriteBarrier(&____curCamAni_21, value);
	}

	inline static int32_t get_offset_of__curBosAni_22() { return static_cast<int32_t>(offsetof(BossAnimationMgr_t150948897, ____curBosAni_22)); }
	inline AnimationPointJSC_t487595022 * get__curBosAni_22() const { return ____curBosAni_22; }
	inline AnimationPointJSC_t487595022 ** get_address_of__curBosAni_22() { return &____curBosAni_22; }
	inline void set__curBosAni_22(AnimationPointJSC_t487595022 * value)
	{
		____curBosAni_22 = value;
		Il2CppCodeGenWriteBarrier(&____curBosAni_22, value);
	}

	inline static int32_t get_offset_of_U3CIsPlayingU3Ek__BackingField_23() { return static_cast<int32_t>(offsetof(BossAnimationMgr_t150948897, ___U3CIsPlayingU3Ek__BackingField_23)); }
	inline bool get_U3CIsPlayingU3Ek__BackingField_23() const { return ___U3CIsPlayingU3Ek__BackingField_23; }
	inline bool* get_address_of_U3CIsPlayingU3Ek__BackingField_23() { return &___U3CIsPlayingU3Ek__BackingField_23; }
	inline void set_U3CIsPlayingU3Ek__BackingField_23(bool value)
	{
		___U3CIsPlayingU3Ek__BackingField_23 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
