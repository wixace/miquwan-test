﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Pathfinding.PointNode
struct PointNode_t2761813780;

#include "mscorlib_System_ValueType1744280289.h"
#include "AssemblyU2DCSharp_Pathfinding_Int31974045594.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<Pathfinding.Int3,Pathfinding.PointNode>
struct  KeyValuePair_2_t4238261807 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	Int3_t1974045594  ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	PointNode_t2761813780 * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t4238261807, ___key_0)); }
	inline Int3_t1974045594  get_key_0() const { return ___key_0; }
	inline Int3_t1974045594 * get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(Int3_t1974045594  value)
	{
		___key_0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t4238261807, ___value_1)); }
	inline PointNode_t2761813780 * get_value_1() const { return ___value_1; }
	inline PointNode_t2761813780 ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(PointNode_t2761813780 * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier(&___value_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
