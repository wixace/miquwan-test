﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ProceduralWorld/ProceduralTile/<InternalGenerate>c__Iterator13
struct U3CInternalGenerateU3Ec__Iterator13_t2030525401;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void ProceduralWorld/ProceduralTile/<InternalGenerate>c__Iterator13::.ctor()
extern "C"  void U3CInternalGenerateU3Ec__Iterator13__ctor_m2989761762 (U3CInternalGenerateU3Ec__Iterator13_t2030525401 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProceduralWorld/ProceduralTile/<InternalGenerate>c__Iterator13::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CInternalGenerateU3Ec__Iterator13_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2220630906 (U3CInternalGenerateU3Ec__Iterator13_t2030525401 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProceduralWorld/ProceduralTile/<InternalGenerate>c__Iterator13::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CInternalGenerateU3Ec__Iterator13_System_Collections_IEnumerator_get_Current_m1646811918 (U3CInternalGenerateU3Ec__Iterator13_t2030525401 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProceduralWorld/ProceduralTile/<InternalGenerate>c__Iterator13::MoveNext()
extern "C"  bool U3CInternalGenerateU3Ec__Iterator13_MoveNext_m1806125754 (U3CInternalGenerateU3Ec__Iterator13_t2030525401 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProceduralWorld/ProceduralTile/<InternalGenerate>c__Iterator13::Dispose()
extern "C"  void U3CInternalGenerateU3Ec__Iterator13_Dispose_m4021182623 (U3CInternalGenerateU3Ec__Iterator13_t2030525401 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProceduralWorld/ProceduralTile/<InternalGenerate>c__Iterator13::Reset()
extern "C"  void U3CInternalGenerateU3Ec__Iterator13_Reset_m636194703 (U3CInternalGenerateU3Ec__Iterator13_t2030525401 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
