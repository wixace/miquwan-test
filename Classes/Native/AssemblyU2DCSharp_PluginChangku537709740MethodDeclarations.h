﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PluginChangku
struct PluginChangku_t537709740;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// System.String
struct String_t;
// VersionMgr
struct VersionMgr_t1322950208;
// ProductsCfgMgr
struct ProductsCfgMgr_t2493714872;
// PluginsSdkMgr
struct PluginsSdkMgr_t3884624670;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_PluginChangku537709740.h"
#include "AssemblyU2DCSharp_VersionMgr1322950208.h"
#include "AssemblyU2DCSharp_ProductsCfgMgr2493714872.h"
#include "AssemblyU2DCSharp_PluginsSdkMgr3884624670.h"

// System.Void PluginChangku::.ctor()
extern "C"  void PluginChangku__ctor_m3148551743 (PluginChangku_t537709740 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginChangku::Init()
extern "C"  void PluginChangku_Init_m3101144117 (PluginChangku_t537709740 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginChangku::ReqSDKHttpLogin()
extern "C"  Dictionary_2_t827649927 * PluginChangku_ReqSDKHttpLogin_m2917002608 (PluginChangku_t537709740 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginChangku::IsLoginSuccess(System.Boolean)
extern "C"  bool PluginChangku_IsLoginSuccess_m109021004 (PluginChangku_t537709740 * __this, bool ___isOpenUseLogin0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginChangku::OpenUserLogin()
extern "C"  void PluginChangku_OpenUserLogin_m2026211729 (PluginChangku_t537709740 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginChangku::UserPay(CEvent.ZEvent)
extern "C"  void PluginChangku_UserPay_m1015829697 (PluginChangku_t537709740 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginChangku::EnterGame(CEvent.ZEvent)
extern "C"  void PluginChangku_EnterGame_m3471309332 (PluginChangku_t537709740 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginChangku::RoleUpgrade(CEvent.ZEvent)
extern "C"  void PluginChangku_RoleUpgrade_m3369109432 (PluginChangku_t537709740 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginChangku::OnLoginSuccess(System.String)
extern "C"  void PluginChangku_OnLoginSuccess_m191448068 (PluginChangku_t537709740 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginChangku::OnLogoutSuccess(System.String)
extern "C"  void PluginChangku_OnLogoutSuccess_m1643465483 (PluginChangku_t537709740 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginChangku::OnLogout(System.String)
extern "C"  void PluginChangku_OnLogout_m3164838548 (PluginChangku_t537709740 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginChangku::initSdk(System.String,System.String)
extern "C"  void PluginChangku_initSdk_m321851479 (PluginChangku_t537709740 * __this, String_t* ___appid0, String_t* ___appscheme1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginChangku::login()
extern "C"  void PluginChangku_login_m2670566566 (PluginChangku_t537709740 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginChangku::logout()
extern "C"  void PluginChangku_logout_m1189007631 (PluginChangku_t537709740 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginChangku::userInfo(System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginChangku_userInfo_m2539782672 (PluginChangku_t537709740 * __this, String_t* ___serverid0, String_t* ___servername1, String_t* ___roleid2, String_t* ___roleName3, String_t* ___roleLv4, String_t* ___CreatRoleTime5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginChangku::pay(System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginChangku_pay_m2805734373 (PluginChangku_t537709740 * __this, String_t* ___amount0, String_t* ___productid1, String_t* ___productName2, String_t* ___roleid3, String_t* ___serverId4, String_t* ___extra5, String_t* ___orderId6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginChangku::<OnLogoutSuccess>m__414()
extern "C"  void PluginChangku_U3COnLogoutSuccessU3Em__414_m1214098311 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginChangku::<OnLogout>m__415()
extern "C"  void PluginChangku_U3COnLogoutU3Em__415_m866502313 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginChangku::ilo_initSdk1(PluginChangku,System.String,System.String)
extern "C"  void PluginChangku_ilo_initSdk1_m2070578111 (Il2CppObject * __this /* static, unused */, PluginChangku_t537709740 * ____this0, String_t* ___appid1, String_t* ___appscheme2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginChangku::ilo_get_isSdkLogin2(VersionMgr)
extern "C"  bool PluginChangku_ilo_get_isSdkLogin2_m2917487150 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginChangku::ilo_OpenUserLogin3(PluginChangku)
extern "C"  void PluginChangku_ilo_OpenUserLogin3_m995578831 (Il2CppObject * __this /* static, unused */, PluginChangku_t537709740 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PluginChangku::ilo_GetProductID4(ProductsCfgMgr,System.String)
extern "C"  String_t* PluginChangku_ilo_GetProductID4_m3474735377 (Il2CppObject * __this /* static, unused */, ProductsCfgMgr_t2493714872 * ____this0, String_t* ___name1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginChangku::ilo_pay5(PluginChangku,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginChangku_ilo_pay5_m2353274053 (Il2CppObject * __this /* static, unused */, PluginChangku_t537709740 * ____this0, String_t* ___amount1, String_t* ___productid2, String_t* ___productName3, String_t* ___roleid4, String_t* ___serverId5, String_t* ___extra6, String_t* ___orderId7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginChangku::ilo_userInfo6(PluginChangku,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginChangku_ilo_userInfo6_m3641380455 (Il2CppObject * __this /* static, unused */, PluginChangku_t537709740 * ____this0, String_t* ___serverid1, String_t* ___servername2, String_t* ___roleid3, String_t* ___roleName4, String_t* ___roleLv5, String_t* ___CreatRoleTime6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PluginsSdkMgr PluginChangku::ilo_get_PluginsSdkMgr7()
extern "C"  PluginsSdkMgr_t3884624670 * PluginChangku_ilo_get_PluginsSdkMgr7_m3827413509 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginChangku::ilo_ReqSDKHttpLogin8(PluginsSdkMgr)
extern "C"  void PluginChangku_ilo_ReqSDKHttpLogin8_m1107473897 (Il2CppObject * __this /* static, unused */, PluginsSdkMgr_t3884624670 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
