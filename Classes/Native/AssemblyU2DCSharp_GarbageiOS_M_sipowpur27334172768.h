﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_sipowpur27
struct  M_sipowpur27_t334172768  : public Il2CppObject
{
public:
	// System.UInt32 GarbageiOS.M_sipowpur27::_naswaBinainall
	uint32_t ____naswaBinainall_0;
	// System.String GarbageiOS.M_sipowpur27::_taypelTritel
	String_t* ____taypelTritel_1;
	// System.UInt32 GarbageiOS.M_sipowpur27::_meyee
	uint32_t ____meyee_2;
	// System.String GarbageiOS.M_sipowpur27::_mejairno
	String_t* ____mejairno_3;
	// System.Boolean GarbageiOS.M_sipowpur27::_wharyawi
	bool ____wharyawi_4;
	// System.Single GarbageiOS.M_sipowpur27::_nitreejall
	float ____nitreejall_5;
	// System.String GarbageiOS.M_sipowpur27::_ralnelPalgurri
	String_t* ____ralnelPalgurri_6;
	// System.String GarbageiOS.M_sipowpur27::_qinouMirselall
	String_t* ____qinouMirselall_7;
	// System.Int32 GarbageiOS.M_sipowpur27::_nelpa
	int32_t ____nelpa_8;
	// System.Boolean GarbageiOS.M_sipowpur27::_doceba
	bool ____doceba_9;
	// System.String GarbageiOS.M_sipowpur27::_herejai
	String_t* ____herejai_10;
	// System.UInt32 GarbageiOS.M_sipowpur27::_gewerRaloucir
	uint32_t ____gewerRaloucir_11;

public:
	inline static int32_t get_offset_of__naswaBinainall_0() { return static_cast<int32_t>(offsetof(M_sipowpur27_t334172768, ____naswaBinainall_0)); }
	inline uint32_t get__naswaBinainall_0() const { return ____naswaBinainall_0; }
	inline uint32_t* get_address_of__naswaBinainall_0() { return &____naswaBinainall_0; }
	inline void set__naswaBinainall_0(uint32_t value)
	{
		____naswaBinainall_0 = value;
	}

	inline static int32_t get_offset_of__taypelTritel_1() { return static_cast<int32_t>(offsetof(M_sipowpur27_t334172768, ____taypelTritel_1)); }
	inline String_t* get__taypelTritel_1() const { return ____taypelTritel_1; }
	inline String_t** get_address_of__taypelTritel_1() { return &____taypelTritel_1; }
	inline void set__taypelTritel_1(String_t* value)
	{
		____taypelTritel_1 = value;
		Il2CppCodeGenWriteBarrier(&____taypelTritel_1, value);
	}

	inline static int32_t get_offset_of__meyee_2() { return static_cast<int32_t>(offsetof(M_sipowpur27_t334172768, ____meyee_2)); }
	inline uint32_t get__meyee_2() const { return ____meyee_2; }
	inline uint32_t* get_address_of__meyee_2() { return &____meyee_2; }
	inline void set__meyee_2(uint32_t value)
	{
		____meyee_2 = value;
	}

	inline static int32_t get_offset_of__mejairno_3() { return static_cast<int32_t>(offsetof(M_sipowpur27_t334172768, ____mejairno_3)); }
	inline String_t* get__mejairno_3() const { return ____mejairno_3; }
	inline String_t** get_address_of__mejairno_3() { return &____mejairno_3; }
	inline void set__mejairno_3(String_t* value)
	{
		____mejairno_3 = value;
		Il2CppCodeGenWriteBarrier(&____mejairno_3, value);
	}

	inline static int32_t get_offset_of__wharyawi_4() { return static_cast<int32_t>(offsetof(M_sipowpur27_t334172768, ____wharyawi_4)); }
	inline bool get__wharyawi_4() const { return ____wharyawi_4; }
	inline bool* get_address_of__wharyawi_4() { return &____wharyawi_4; }
	inline void set__wharyawi_4(bool value)
	{
		____wharyawi_4 = value;
	}

	inline static int32_t get_offset_of__nitreejall_5() { return static_cast<int32_t>(offsetof(M_sipowpur27_t334172768, ____nitreejall_5)); }
	inline float get__nitreejall_5() const { return ____nitreejall_5; }
	inline float* get_address_of__nitreejall_5() { return &____nitreejall_5; }
	inline void set__nitreejall_5(float value)
	{
		____nitreejall_5 = value;
	}

	inline static int32_t get_offset_of__ralnelPalgurri_6() { return static_cast<int32_t>(offsetof(M_sipowpur27_t334172768, ____ralnelPalgurri_6)); }
	inline String_t* get__ralnelPalgurri_6() const { return ____ralnelPalgurri_6; }
	inline String_t** get_address_of__ralnelPalgurri_6() { return &____ralnelPalgurri_6; }
	inline void set__ralnelPalgurri_6(String_t* value)
	{
		____ralnelPalgurri_6 = value;
		Il2CppCodeGenWriteBarrier(&____ralnelPalgurri_6, value);
	}

	inline static int32_t get_offset_of__qinouMirselall_7() { return static_cast<int32_t>(offsetof(M_sipowpur27_t334172768, ____qinouMirselall_7)); }
	inline String_t* get__qinouMirselall_7() const { return ____qinouMirselall_7; }
	inline String_t** get_address_of__qinouMirselall_7() { return &____qinouMirselall_7; }
	inline void set__qinouMirselall_7(String_t* value)
	{
		____qinouMirselall_7 = value;
		Il2CppCodeGenWriteBarrier(&____qinouMirselall_7, value);
	}

	inline static int32_t get_offset_of__nelpa_8() { return static_cast<int32_t>(offsetof(M_sipowpur27_t334172768, ____nelpa_8)); }
	inline int32_t get__nelpa_8() const { return ____nelpa_8; }
	inline int32_t* get_address_of__nelpa_8() { return &____nelpa_8; }
	inline void set__nelpa_8(int32_t value)
	{
		____nelpa_8 = value;
	}

	inline static int32_t get_offset_of__doceba_9() { return static_cast<int32_t>(offsetof(M_sipowpur27_t334172768, ____doceba_9)); }
	inline bool get__doceba_9() const { return ____doceba_9; }
	inline bool* get_address_of__doceba_9() { return &____doceba_9; }
	inline void set__doceba_9(bool value)
	{
		____doceba_9 = value;
	}

	inline static int32_t get_offset_of__herejai_10() { return static_cast<int32_t>(offsetof(M_sipowpur27_t334172768, ____herejai_10)); }
	inline String_t* get__herejai_10() const { return ____herejai_10; }
	inline String_t** get_address_of__herejai_10() { return &____herejai_10; }
	inline void set__herejai_10(String_t* value)
	{
		____herejai_10 = value;
		Il2CppCodeGenWriteBarrier(&____herejai_10, value);
	}

	inline static int32_t get_offset_of__gewerRaloucir_11() { return static_cast<int32_t>(offsetof(M_sipowpur27_t334172768, ____gewerRaloucir_11)); }
	inline uint32_t get__gewerRaloucir_11() const { return ____gewerRaloucir_11; }
	inline uint32_t* get_address_of__gewerRaloucir_11() { return &____gewerRaloucir_11; }
	inline void set__gewerRaloucir_11(uint32_t value)
	{
		____gewerRaloucir_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
