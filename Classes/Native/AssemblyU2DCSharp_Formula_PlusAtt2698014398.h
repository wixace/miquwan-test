﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Formula/PlusAtt
struct  PlusAtt_t2698014398  : public Il2CppObject
{
public:
	// System.UInt32 Formula/PlusAtt::hp
	uint32_t ___hp_0;
	// System.UInt32 Formula/PlusAtt::attack
	uint32_t ___attack_1;
	// System.UInt32 Formula/PlusAtt::phy_def
	uint32_t ___phy_def_2;
	// System.UInt32 Formula/PlusAtt::mag_def
	uint32_t ___mag_def_3;

public:
	inline static int32_t get_offset_of_hp_0() { return static_cast<int32_t>(offsetof(PlusAtt_t2698014398, ___hp_0)); }
	inline uint32_t get_hp_0() const { return ___hp_0; }
	inline uint32_t* get_address_of_hp_0() { return &___hp_0; }
	inline void set_hp_0(uint32_t value)
	{
		___hp_0 = value;
	}

	inline static int32_t get_offset_of_attack_1() { return static_cast<int32_t>(offsetof(PlusAtt_t2698014398, ___attack_1)); }
	inline uint32_t get_attack_1() const { return ___attack_1; }
	inline uint32_t* get_address_of_attack_1() { return &___attack_1; }
	inline void set_attack_1(uint32_t value)
	{
		___attack_1 = value;
	}

	inline static int32_t get_offset_of_phy_def_2() { return static_cast<int32_t>(offsetof(PlusAtt_t2698014398, ___phy_def_2)); }
	inline uint32_t get_phy_def_2() const { return ___phy_def_2; }
	inline uint32_t* get_address_of_phy_def_2() { return &___phy_def_2; }
	inline void set_phy_def_2(uint32_t value)
	{
		___phy_def_2 = value;
	}

	inline static int32_t get_offset_of_mag_def_3() { return static_cast<int32_t>(offsetof(PlusAtt_t2698014398, ___mag_def_3)); }
	inline uint32_t get_mag_def_3() const { return ___mag_def_3; }
	inline uint32_t* get_address_of_mag_def_3() { return &___mag_def_3; }
	inline void set_mag_def_3(uint32_t value)
	{
		___mag_def_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
