﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.MonoModifier
struct MonoModifier_t200043088;
// Pathfinding.Path
struct Path_t1974241691;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t215400611;
// Pathfinding.GraphNode[]
struct GraphNodeU5BU5D_t927449255;
// Pathfinding.NavGraph
struct NavGraph_t1254319713;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_Path1974241691.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_Pathfinding_NavGraph1254319713.h"
#include "AssemblyU2DCSharp_Pathfinding_Int31974045594.h"

// System.Void Pathfinding.MonoModifier::.ctor()
extern "C"  void MonoModifier__ctor_m2751477271 (MonoModifier_t200043088 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.MonoModifier::OnEnable()
extern "C"  void MonoModifier_OnEnable_m1880114703 (MonoModifier_t200043088 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.MonoModifier::OnDisable()
extern "C"  void MonoModifier_OnDisable_m2889918206 (MonoModifier_t200043088 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pathfinding.MonoModifier::get_Priority()
extern "C"  int32_t MonoModifier_get_Priority_m2079780232 (MonoModifier_t200043088 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.MonoModifier::set_Priority(System.Int32)
extern "C"  void MonoModifier_set_Priority_m1896391999 (MonoModifier_t200043088 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.MonoModifier::Awake()
extern "C"  void MonoModifier_Awake_m2989082490 (MonoModifier_t200043088 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.MonoModifier::OnDestroy()
extern "C"  void MonoModifier_OnDestroy_m772482960 (MonoModifier_t200043088 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.MonoModifier::ApplyOriginal(Pathfinding.Path)
extern "C"  void MonoModifier_ApplyOriginal_m916514405 (MonoModifier_t200043088 * __this, Path_t1974241691 * ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.MonoModifier::PreProcess(Pathfinding.Path)
extern "C"  void MonoModifier_PreProcess_m1546514474 (MonoModifier_t200043088 * __this, Path_t1974241691 * ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3[] Pathfinding.MonoModifier::Apply(Pathfinding.GraphNode[],UnityEngine.Vector3,UnityEngine.Vector3,System.Int32,System.Int32,Pathfinding.NavGraph)
extern "C"  Vector3U5BU5D_t215400611* MonoModifier_Apply_m1571803788 (MonoModifier_t200043088 * __this, GraphNodeU5BU5D_t927449255* ___path0, Vector3_t4282066566  ___start1, Vector3_t4282066566  ___end2, int32_t ___startIndex3, int32_t ___endIndex4, NavGraph_t1254319713 * ___graph5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3[] Pathfinding.MonoModifier::Apply(UnityEngine.Vector3[],UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3U5BU5D_t215400611* MonoModifier_Apply_m879186162 (MonoModifier_t200043088 * __this, Vector3U5BU5D_t215400611* ___path0, Vector3_t4282066566  ___start1, Vector3_t4282066566  ___end2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Pathfinding.MonoModifier::ilo_op_Explicit1(Pathfinding.Int3)
extern "C"  Vector3_t4282066566  MonoModifier_ilo_op_Explicit1_m1402389707 (Il2CppObject * __this /* static, unused */, Int3_t1974045594  ___ob0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
