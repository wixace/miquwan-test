﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// demoStoryCfg
struct demoStoryCfg_t1993162290;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;

#include "codegen/il2cpp-codegen.h"

// System.Void demoStoryCfg::.ctor()
extern "C"  void demoStoryCfg__ctor_m3881340073 (demoStoryCfg_t1993162290 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void demoStoryCfg::Init(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C"  void demoStoryCfg_Init_m2487514684 (demoStoryCfg_t1993162290 * __this, Dictionary_2_t827649927 * ____info0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
