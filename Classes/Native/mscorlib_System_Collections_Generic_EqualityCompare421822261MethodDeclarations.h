﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<Pathfinding.Int3>
struct DefaultComparer_t421822261;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_Int31974045594.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<Pathfinding.Int3>::.ctor()
extern "C"  void DefaultComparer__ctor_m2163113146_gshared (DefaultComparer_t421822261 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m2163113146(__this, method) ((  void (*) (DefaultComparer_t421822261 *, const MethodInfo*))DefaultComparer__ctor_m2163113146_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<Pathfinding.Int3>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m2402489593_gshared (DefaultComparer_t421822261 * __this, Int3_t1974045594  ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m2402489593(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t421822261 *, Int3_t1974045594 , const MethodInfo*))DefaultComparer_GetHashCode_m2402489593_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<Pathfinding.Int3>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m4220468175_gshared (DefaultComparer_t421822261 * __this, Int3_t1974045594  ___x0, Int3_t1974045594  ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m4220468175(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t421822261 *, Int3_t1974045594 , Int3_t1974045594 , const MethodInfo*))DefaultComparer_Equals_m4220468175_gshared)(__this, ___x0, ___y1, method)
