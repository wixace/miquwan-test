﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.RecastMeshObj
struct RecastMeshObj_t2069195738;
// System.Collections.Generic.List`1<Pathfinding.RecastMeshObj>
struct List_1_t3437381290;
// UnityEngine.MeshFilter
struct MeshFilter_t3839065225;
// UnityEngine.Collider
struct Collider_t2939674232;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Bounds2711641849.h"
#include "AssemblyU2DCSharp_Pathfinding_RecastMeshObj2069195738.h"

// System.Void Pathfinding.RecastMeshObj::.ctor()
extern "C"  void RecastMeshObj__ctor_m250742429 (RecastMeshObj_t2069195738 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RecastMeshObj::.cctor()
extern "C"  void RecastMeshObj__cctor_m2995951792 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RecastMeshObj::GetAllInBounds(System.Collections.Generic.List`1<Pathfinding.RecastMeshObj>,UnityEngine.Bounds)
extern "C"  void RecastMeshObj_GetAllInBounds_m2740243516 (Il2CppObject * __this /* static, unused */, List_1_t3437381290 * ___buffer0, Bounds_t2711641849  ___bounds1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RecastMeshObj::OnEnable()
extern "C"  void RecastMeshObj_OnEnable_m2991153097 (RecastMeshObj_t2069195738 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RecastMeshObj::Register()
extern "C"  void RecastMeshObj_Register_m1004043754 (RecastMeshObj_t2069195738 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RecastMeshObj::RecalculateBounds()
extern "C"  void RecastMeshObj_RecalculateBounds_m1909498339 (RecastMeshObj_t2069195738 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Bounds Pathfinding.RecastMeshObj::GetBounds()
extern "C"  Bounds_t2711641849  RecastMeshObj_GetBounds_m2128968247 (RecastMeshObj_t2069195738 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.MeshFilter Pathfinding.RecastMeshObj::GetMeshFilter()
extern "C"  MeshFilter_t3839065225 * RecastMeshObj_GetMeshFilter_m4216757431 (RecastMeshObj_t2069195738 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider Pathfinding.RecastMeshObj::GetCollider()
extern "C"  Collider_t2939674232 * RecastMeshObj_GetCollider_m3331824023 (RecastMeshObj_t2069195738 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RecastMeshObj::OnDisable()
extern "C"  void RecastMeshObj_OnDisable_m2972370052 (RecastMeshObj_t2069195738 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Bounds Pathfinding.RecastMeshObj::ilo_GetBounds1(Pathfinding.RecastMeshObj)
extern "C"  Bounds_t2711641849  RecastMeshObj_ilo_GetBounds1_m2810060949 (Il2CppObject * __this /* static, unused */, RecastMeshObj_t2069195738 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RecastMeshObj::ilo_Register2(Pathfinding.RecastMeshObj)
extern "C"  void RecastMeshObj_ilo_Register2_m2237609837 (Il2CppObject * __this /* static, unused */, RecastMeshObj_t2069195738 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
