﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// cameraShotCfg
struct cameraShotCfg_t781157413;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;

#include "codegen/il2cpp-codegen.h"

// System.Void cameraShotCfg::.ctor()
extern "C"  void cameraShotCfg__ctor_m3590108902 (cameraShotCfg_t781157413 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void cameraShotCfg::Init(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C"  void cameraShotCfg_Init_m3837603551 (cameraShotCfg_t781157413 * __this, Dictionary_2_t827649927 * ____info0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
