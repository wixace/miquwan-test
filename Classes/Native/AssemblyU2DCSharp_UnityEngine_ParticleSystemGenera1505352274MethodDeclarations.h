﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_ParticleSystemGenerated
struct UnityEngine_ParticleSystemGenerated_t1505352274;
// JSVCall
struct JSVCall_t3708497963;
// UnityEngine.ParticleSystem/Particle[]
struct ParticleU5BU5D_t343690676;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void UnityEngine_ParticleSystemGenerated::.ctor()
extern "C"  void UnityEngine_ParticleSystemGenerated__ctor_m3623675225 (UnityEngine_ParticleSystemGenerated_t1505352274 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ParticleSystemGenerated::ParticleSystem_ParticleSystem1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ParticleSystemGenerated_ParticleSystem_ParticleSystem1_m1833118745 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleSystemGenerated::ParticleSystem_startDelay(JSVCall)
extern "C"  void UnityEngine_ParticleSystemGenerated_ParticleSystem_startDelay_m21370437 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleSystemGenerated::ParticleSystem_isPlaying(JSVCall)
extern "C"  void UnityEngine_ParticleSystemGenerated_ParticleSystem_isPlaying_m1728860290 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleSystemGenerated::ParticleSystem_isStopped(JSVCall)
extern "C"  void UnityEngine_ParticleSystemGenerated_ParticleSystem_isStopped_m4011323843 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleSystemGenerated::ParticleSystem_isPaused(JSVCall)
extern "C"  void UnityEngine_ParticleSystemGenerated_ParticleSystem_isPaused_m1993266030 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleSystemGenerated::ParticleSystem_loop(JSVCall)
extern "C"  void UnityEngine_ParticleSystemGenerated_ParticleSystem_loop_m3061311810 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleSystemGenerated::ParticleSystem_playOnAwake(JSVCall)
extern "C"  void UnityEngine_ParticleSystemGenerated_ParticleSystem_playOnAwake_m290390740 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleSystemGenerated::ParticleSystem_time(JSVCall)
extern "C"  void UnityEngine_ParticleSystemGenerated_ParticleSystem_time_m1400175417 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleSystemGenerated::ParticleSystem_duration(JSVCall)
extern "C"  void UnityEngine_ParticleSystemGenerated_ParticleSystem_duration_m3055501394 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleSystemGenerated::ParticleSystem_playbackSpeed(JSVCall)
extern "C"  void UnityEngine_ParticleSystemGenerated_ParticleSystem_playbackSpeed_m1116845626 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleSystemGenerated::ParticleSystem_particleCount(JSVCall)
extern "C"  void UnityEngine_ParticleSystemGenerated_ParticleSystem_particleCount_m3641952157 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleSystemGenerated::ParticleSystem_startSpeed(JSVCall)
extern "C"  void UnityEngine_ParticleSystemGenerated_ParticleSystem_startSpeed_m2425826305 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleSystemGenerated::ParticleSystem_startSize(JSVCall)
extern "C"  void UnityEngine_ParticleSystemGenerated_ParticleSystem_startSize_m3915758467 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleSystemGenerated::ParticleSystem_startColor(JSVCall)
extern "C"  void UnityEngine_ParticleSystemGenerated_ParticleSystem_startColor_m3875945573 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleSystemGenerated::ParticleSystem_startRotation(JSVCall)
extern "C"  void UnityEngine_ParticleSystemGenerated_ParticleSystem_startRotation_m4259187110 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleSystemGenerated::ParticleSystem_startRotation3D(JSVCall)
extern "C"  void UnityEngine_ParticleSystemGenerated_ParticleSystem_startRotation3D_m501819989 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleSystemGenerated::ParticleSystem_startLifetime(JSVCall)
extern "C"  void UnityEngine_ParticleSystemGenerated_ParticleSystem_startLifetime_m2911934907 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleSystemGenerated::ParticleSystem_gravityModifier(JSVCall)
extern "C"  void UnityEngine_ParticleSystemGenerated_ParticleSystem_gravityModifier_m3580045825 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleSystemGenerated::ParticleSystem_maxParticles(JSVCall)
extern "C"  void UnityEngine_ParticleSystemGenerated_ParticleSystem_maxParticles_m1524250589 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleSystemGenerated::ParticleSystem_simulationSpace(JSVCall)
extern "C"  void UnityEngine_ParticleSystemGenerated_ParticleSystem_simulationSpace_m1441670983 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleSystemGenerated::ParticleSystem_scalingMode(JSVCall)
extern "C"  void UnityEngine_ParticleSystemGenerated_ParticleSystem_scalingMode_m474635548 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleSystemGenerated::ParticleSystem_randomSeed(JSVCall)
extern "C"  void UnityEngine_ParticleSystemGenerated_ParticleSystem_randomSeed_m421842962 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleSystemGenerated::ParticleSystem_emission(JSVCall)
extern "C"  void UnityEngine_ParticleSystemGenerated_ParticleSystem_emission_m3298335167 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleSystemGenerated::ParticleSystem_shape(JSVCall)
extern "C"  void UnityEngine_ParticleSystemGenerated_ParticleSystem_shape_m2983580965 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleSystemGenerated::ParticleSystem_velocityOverLifetime(JSVCall)
extern "C"  void UnityEngine_ParticleSystemGenerated_ParticleSystem_velocityOverLifetime_m3226024556 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleSystemGenerated::ParticleSystem_limitVelocityOverLifetime(JSVCall)
extern "C"  void UnityEngine_ParticleSystemGenerated_ParticleSystem_limitVelocityOverLifetime_m4294358193 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleSystemGenerated::ParticleSystem_inheritVelocity(JSVCall)
extern "C"  void UnityEngine_ParticleSystemGenerated_ParticleSystem_inheritVelocity_m2950186926 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleSystemGenerated::ParticleSystem_forceOverLifetime(JSVCall)
extern "C"  void UnityEngine_ParticleSystemGenerated_ParticleSystem_forceOverLifetime_m2643663198 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleSystemGenerated::ParticleSystem_colorOverLifetime(JSVCall)
extern "C"  void UnityEngine_ParticleSystemGenerated_ParticleSystem_colorOverLifetime_m4279988198 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleSystemGenerated::ParticleSystem_colorBySpeed(JSVCall)
extern "C"  void UnityEngine_ParticleSystemGenerated_ParticleSystem_colorBySpeed_m2953722041 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleSystemGenerated::ParticleSystem_sizeOverLifetime(JSVCall)
extern "C"  void UnityEngine_ParticleSystemGenerated_ParticleSystem_sizeOverLifetime_m2707137992 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleSystemGenerated::ParticleSystem_sizeBySpeed(JSVCall)
extern "C"  void UnityEngine_ParticleSystemGenerated_ParticleSystem_sizeBySpeed_m3042467223 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleSystemGenerated::ParticleSystem_rotationOverLifetime(JSVCall)
extern "C"  void UnityEngine_ParticleSystemGenerated_ParticleSystem_rotationOverLifetime_m3226994283 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleSystemGenerated::ParticleSystem_rotationBySpeed(JSVCall)
extern "C"  void UnityEngine_ParticleSystemGenerated_ParticleSystem_rotationBySpeed_m3115639572 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleSystemGenerated::ParticleSystem_externalForces(JSVCall)
extern "C"  void UnityEngine_ParticleSystemGenerated_ParticleSystem_externalForces_m75593267 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleSystemGenerated::ParticleSystem_collision(JSVCall)
extern "C"  void UnityEngine_ParticleSystemGenerated_ParticleSystem_collision_m4021338292 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleSystemGenerated::ParticleSystem_subEmitters(JSVCall)
extern "C"  void UnityEngine_ParticleSystemGenerated_ParticleSystem_subEmitters_m3552799777 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleSystemGenerated::ParticleSystem_textureSheetAnimation(JSVCall)
extern "C"  void UnityEngine_ParticleSystemGenerated_ParticleSystem_textureSheetAnimation_m1320881350 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ParticleSystemGenerated::ParticleSystem_Clear__Boolean(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ParticleSystemGenerated_ParticleSystem_Clear__Boolean_m1821452352 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ParticleSystemGenerated::ParticleSystem_Clear(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ParticleSystemGenerated_ParticleSystem_Clear_m44450858 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ParticleSystemGenerated::ParticleSystem_Emit__EmitParams__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ParticleSystemGenerated_ParticleSystem_Emit__EmitParams__Int32_m4250292127 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ParticleSystemGenerated::ParticleSystem_Emit__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ParticleSystemGenerated_ParticleSystem_Emit__Int32_m3868127672 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ParticleSystemGenerated::ParticleSystem_GetParticles__Particle_Array(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ParticleSystemGenerated_ParticleSystem_GetParticles__Particle_Array_m1141209340 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ParticleSystemGenerated::ParticleSystem_IsAlive__Boolean(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ParticleSystemGenerated_ParticleSystem_IsAlive__Boolean_m3228810506 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ParticleSystemGenerated::ParticleSystem_IsAlive(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ParticleSystemGenerated_ParticleSystem_IsAlive_m1455768608 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ParticleSystemGenerated::ParticleSystem_Pause__Boolean(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ParticleSystemGenerated_ParticleSystem_Pause__Boolean_m2838168439 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ParticleSystemGenerated::ParticleSystem_Pause(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ParticleSystemGenerated_ParticleSystem_Pause_m235511571 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ParticleSystemGenerated::ParticleSystem_Play__Boolean(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ParticleSystemGenerated_ParticleSystem_Play__Boolean_m1047889425 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ParticleSystemGenerated::ParticleSystem_Play(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ParticleSystemGenerated_ParticleSystem_Play_m2719409977 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ParticleSystemGenerated::ParticleSystem_SetParticles__Particle_Array__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ParticleSystemGenerated_ParticleSystem_SetParticles__Particle_Array__Int32_m2263795424 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ParticleSystemGenerated::ParticleSystem_Simulate__Single__Boolean__Boolean(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ParticleSystemGenerated_ParticleSystem_Simulate__Single__Boolean__Boolean_m2791693169 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ParticleSystemGenerated::ParticleSystem_Simulate__Single__Boolean(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ParticleSystemGenerated_ParticleSystem_Simulate__Single__Boolean_m1112475097 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ParticleSystemGenerated::ParticleSystem_Simulate__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ParticleSystemGenerated_ParticleSystem_Simulate__Single_m2185299569 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ParticleSystemGenerated::ParticleSystem_Stop__Boolean(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ParticleSystemGenerated_ParticleSystem_Stop__Boolean_m1947725699 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ParticleSystemGenerated::ParticleSystem_Stop(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ParticleSystemGenerated_ParticleSystem_Stop_m4118590855 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleSystemGenerated::__Register()
extern "C"  void UnityEngine_ParticleSystemGenerated___Register_m2295006990 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ParticleSystem/Particle[] UnityEngine_ParticleSystemGenerated::<ParticleSystem_GetParticles__Particle_Array>m__285()
extern "C"  ParticleU5BU5D_t343690676* UnityEngine_ParticleSystemGenerated_U3CParticleSystem_GetParticles__Particle_ArrayU3Em__285_m3464766682 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ParticleSystem/Particle[] UnityEngine_ParticleSystemGenerated::<ParticleSystem_SetParticles__Particle_Array__Int32>m__286()
extern "C"  ParticleU5BU5D_t343690676* UnityEngine_ParticleSystemGenerated_U3CParticleSystem_SetParticles__Particle_Array__Int32U3Em__286_m1696455919 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleSystemGenerated::ilo_addJSCSRel1(System.Int32,System.Object)
extern "C"  void UnityEngine_ParticleSystemGenerated_ilo_addJSCSRel1_m3440804757 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleSystemGenerated::ilo_setSingle2(System.Int32,System.Single)
extern "C"  void UnityEngine_ParticleSystemGenerated_ilo_setSingle2_m2816643324 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleSystemGenerated::ilo_setBooleanS3(System.Int32,System.Boolean)
extern "C"  void UnityEngine_ParticleSystemGenerated_ilo_setBooleanS3_m187159624 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleSystemGenerated::ilo_setInt324(System.Int32,System.Int32)
extern "C"  void UnityEngine_ParticleSystemGenerated_ilo_setInt324_m3491861434 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_ParticleSystemGenerated::ilo_getObject5(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_ParticleSystemGenerated_ilo_getObject5_m2942170864 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine_ParticleSystemGenerated::ilo_getSingle6(System.Int32)
extern "C"  float UnityEngine_ParticleSystemGenerated_ilo_getSingle6_m4244651419 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleSystemGenerated::ilo_setVector3S7(System.Int32,UnityEngine.Vector3)
extern "C"  void UnityEngine_ParticleSystemGenerated_ilo_setVector3S7_m3557757396 (Il2CppObject * __this /* static, unused */, int32_t ___e0, Vector3_t4282066566  ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleSystemGenerated::ilo_setEnum8(System.Int32,System.Int32)
extern "C"  void UnityEngine_ParticleSystemGenerated_ilo_setEnum8_m54952595 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_ParticleSystemGenerated::ilo_setObject9(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UnityEngine_ParticleSystemGenerated_ilo_setObject9_m1553705725 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ParticleSystemGenerated::ilo_getBooleanS10(System.Int32)
extern "C"  bool UnityEngine_ParticleSystemGenerated_ilo_getBooleanS10_m2466222985 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_ParticleSystemGenerated::ilo_getArrayLength11(System.Int32)
extern "C"  int32_t UnityEngine_ParticleSystemGenerated_ilo_getArrayLength11_m956882986 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_ParticleSystemGenerated::ilo_getElement12(System.Int32,System.Int32)
extern "C"  int32_t UnityEngine_ParticleSystemGenerated_ilo_getElement12_m188670095 (Il2CppObject * __this /* static, unused */, int32_t ___id0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
