﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1375417109;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PluginXunFei
struct  PluginXunFei_t2016385516  : public MonoBehaviour_t667441552
{
public:
	// System.UInt32 PluginXunFei::server_id
	uint32_t ___server_id_2;
	// System.String PluginXunFei::serverName
	String_t* ___serverName_3;
	// System.UInt32 PluginXunFei::userId
	uint32_t ___userId_4;
	// System.String PluginXunFei::userName
	String_t* ___userName_5;
	// System.Boolean PluginXunFei::isHasPermission
	bool ___isHasPermission_6;
	// System.Boolean PluginXunFei::IsLoginSuccess
	bool ___IsLoginSuccess_7;
	// System.Collections.Generic.List`1<System.String> PluginXunFei::msgSeqIdList
	List_1_t1375417109 * ___msgSeqIdList_8;

public:
	inline static int32_t get_offset_of_server_id_2() { return static_cast<int32_t>(offsetof(PluginXunFei_t2016385516, ___server_id_2)); }
	inline uint32_t get_server_id_2() const { return ___server_id_2; }
	inline uint32_t* get_address_of_server_id_2() { return &___server_id_2; }
	inline void set_server_id_2(uint32_t value)
	{
		___server_id_2 = value;
	}

	inline static int32_t get_offset_of_serverName_3() { return static_cast<int32_t>(offsetof(PluginXunFei_t2016385516, ___serverName_3)); }
	inline String_t* get_serverName_3() const { return ___serverName_3; }
	inline String_t** get_address_of_serverName_3() { return &___serverName_3; }
	inline void set_serverName_3(String_t* value)
	{
		___serverName_3 = value;
		Il2CppCodeGenWriteBarrier(&___serverName_3, value);
	}

	inline static int32_t get_offset_of_userId_4() { return static_cast<int32_t>(offsetof(PluginXunFei_t2016385516, ___userId_4)); }
	inline uint32_t get_userId_4() const { return ___userId_4; }
	inline uint32_t* get_address_of_userId_4() { return &___userId_4; }
	inline void set_userId_4(uint32_t value)
	{
		___userId_4 = value;
	}

	inline static int32_t get_offset_of_userName_5() { return static_cast<int32_t>(offsetof(PluginXunFei_t2016385516, ___userName_5)); }
	inline String_t* get_userName_5() const { return ___userName_5; }
	inline String_t** get_address_of_userName_5() { return &___userName_5; }
	inline void set_userName_5(String_t* value)
	{
		___userName_5 = value;
		Il2CppCodeGenWriteBarrier(&___userName_5, value);
	}

	inline static int32_t get_offset_of_isHasPermission_6() { return static_cast<int32_t>(offsetof(PluginXunFei_t2016385516, ___isHasPermission_6)); }
	inline bool get_isHasPermission_6() const { return ___isHasPermission_6; }
	inline bool* get_address_of_isHasPermission_6() { return &___isHasPermission_6; }
	inline void set_isHasPermission_6(bool value)
	{
		___isHasPermission_6 = value;
	}

	inline static int32_t get_offset_of_IsLoginSuccess_7() { return static_cast<int32_t>(offsetof(PluginXunFei_t2016385516, ___IsLoginSuccess_7)); }
	inline bool get_IsLoginSuccess_7() const { return ___IsLoginSuccess_7; }
	inline bool* get_address_of_IsLoginSuccess_7() { return &___IsLoginSuccess_7; }
	inline void set_IsLoginSuccess_7(bool value)
	{
		___IsLoginSuccess_7 = value;
	}

	inline static int32_t get_offset_of_msgSeqIdList_8() { return static_cast<int32_t>(offsetof(PluginXunFei_t2016385516, ___msgSeqIdList_8)); }
	inline List_1_t1375417109 * get_msgSeqIdList_8() const { return ___msgSeqIdList_8; }
	inline List_1_t1375417109 ** get_address_of_msgSeqIdList_8() { return &___msgSeqIdList_8; }
	inline void set_msgSeqIdList_8(List_1_t1375417109 * value)
	{
		___msgSeqIdList_8 = value;
		Il2CppCodeGenWriteBarrier(&___msgSeqIdList_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
