﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<Entity.Behavior.EBehaviorID,System.Object>
struct Dictionary_2_t1311121246;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2628444638.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21209901952.h"
#include "AssemblyU2DCSharp_Entity_Behavior_EBehaviorID1238533136.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Entity.Behavior.EBehaviorID,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m1371663652_gshared (Enumerator_t2628444638 * __this, Dictionary_2_t1311121246 * ___dictionary0, const MethodInfo* method);
#define Enumerator__ctor_m1371663652(__this, ___dictionary0, method) ((  void (*) (Enumerator_t2628444638 *, Dictionary_2_t1311121246 *, const MethodInfo*))Enumerator__ctor_m1371663652_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Entity.Behavior.EBehaviorID,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1188600509_gshared (Enumerator_t2628444638 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1188600509(__this, method) ((  Il2CppObject * (*) (Enumerator_t2628444638 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1188600509_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Entity.Behavior.EBehaviorID,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1846359313_gshared (Enumerator_t2628444638 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1846359313(__this, method) ((  void (*) (Enumerator_t2628444638 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1846359313_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<Entity.Behavior.EBehaviorID,System.Object>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t1751606614  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1515285018_gshared (Enumerator_t2628444638 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1515285018(__this, method) ((  DictionaryEntry_t1751606614  (*) (Enumerator_t2628444638 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1515285018_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Entity.Behavior.EBehaviorID,System.Object>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m537269721_gshared (Enumerator_t2628444638 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m537269721(__this, method) ((  Il2CppObject * (*) (Enumerator_t2628444638 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m537269721_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Entity.Behavior.EBehaviorID,System.Object>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1968562731_gshared (Enumerator_t2628444638 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1968562731(__this, method) ((  Il2CppObject * (*) (Enumerator_t2628444638 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1968562731_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<Entity.Behavior.EBehaviorID,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2288691453_gshared (Enumerator_t2628444638 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m2288691453(__this, method) ((  bool (*) (Enumerator_t2628444638 *, const MethodInfo*))Enumerator_MoveNext_m2288691453_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<Entity.Behavior.EBehaviorID,System.Object>::get_Current()
extern "C"  KeyValuePair_2_t1209901952  Enumerator_get_Current_m3934484115_gshared (Enumerator_t2628444638 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m3934484115(__this, method) ((  KeyValuePair_2_t1209901952  (*) (Enumerator_t2628444638 *, const MethodInfo*))Enumerator_get_Current_m3934484115_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<Entity.Behavior.EBehaviorID,System.Object>::get_CurrentKey()
extern "C"  uint8_t Enumerator_get_CurrentKey_m2729682826_gshared (Enumerator_t2628444638 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m2729682826(__this, method) ((  uint8_t (*) (Enumerator_t2628444638 *, const MethodInfo*))Enumerator_get_CurrentKey_m2729682826_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<Entity.Behavior.EBehaviorID,System.Object>::get_CurrentValue()
extern "C"  Il2CppObject * Enumerator_get_CurrentValue_m1468074542_gshared (Enumerator_t2628444638 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m1468074542(__this, method) ((  Il2CppObject * (*) (Enumerator_t2628444638 *, const MethodInfo*))Enumerator_get_CurrentValue_m1468074542_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Entity.Behavior.EBehaviorID,System.Object>::Reset()
extern "C"  void Enumerator_Reset_m3314749814_gshared (Enumerator_t2628444638 * __this, const MethodInfo* method);
#define Enumerator_Reset_m3314749814(__this, method) ((  void (*) (Enumerator_t2628444638 *, const MethodInfo*))Enumerator_Reset_m3314749814_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Entity.Behavior.EBehaviorID,System.Object>::VerifyState()
extern "C"  void Enumerator_VerifyState_m3916755199_gshared (Enumerator_t2628444638 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m3916755199(__this, method) ((  void (*) (Enumerator_t2628444638 *, const MethodInfo*))Enumerator_VerifyState_m3916755199_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Entity.Behavior.EBehaviorID,System.Object>::VerifyCurrent()
extern "C"  void Enumerator_VerifyCurrent_m60299431_gshared (Enumerator_t2628444638 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m60299431(__this, method) ((  void (*) (Enumerator_t2628444638 *, const MethodInfo*))Enumerator_VerifyCurrent_m60299431_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Entity.Behavior.EBehaviorID,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m1132266694_gshared (Enumerator_t2628444638 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1132266694(__this, method) ((  void (*) (Enumerator_t2628444638 *, const MethodInfo*))Enumerator_Dispose_m1132266694_gshared)(__this, method)
