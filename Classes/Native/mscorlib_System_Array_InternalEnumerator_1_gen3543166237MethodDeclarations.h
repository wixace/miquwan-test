﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3543166237.h"
#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DCSharp_FightEnum_ESkillBuffEvent465856265.h"

// System.Void System.Array/InternalEnumerator`1<FightEnum.ESkillBuffEvent>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3270389123_gshared (InternalEnumerator_1_t3543166237 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m3270389123(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t3543166237 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m3270389123_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<FightEnum.ESkillBuffEvent>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1897896765_gshared (InternalEnumerator_1_t3543166237 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1897896765(__this, method) ((  void (*) (InternalEnumerator_1_t3543166237 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1897896765_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<FightEnum.ESkillBuffEvent>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2491596211_gshared (InternalEnumerator_1_t3543166237 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2491596211(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t3543166237 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2491596211_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<FightEnum.ESkillBuffEvent>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2247831322_gshared (InternalEnumerator_1_t3543166237 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m2247831322(__this, method) ((  void (*) (InternalEnumerator_1_t3543166237 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2247831322_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<FightEnum.ESkillBuffEvent>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m4047698669_gshared (InternalEnumerator_1_t3543166237 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m4047698669(__this, method) ((  bool (*) (InternalEnumerator_1_t3543166237 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m4047698669_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<FightEnum.ESkillBuffEvent>::get_Current()
extern "C"  uint8_t InternalEnumerator_1_get_Current_m2677668140_gshared (InternalEnumerator_1_t3543166237 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m2677668140(__this, method) ((  uint8_t (*) (InternalEnumerator_1_t3543166237 *, const MethodInfo*))InternalEnumerator_1_get_Current_m2677668140_gshared)(__this, method)
