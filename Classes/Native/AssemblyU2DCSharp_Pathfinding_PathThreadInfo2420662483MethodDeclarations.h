﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AstarPath
struct AstarPath_t4090270936;
// Pathfinding.PathHandler
struct PathHandler_t918952263;
// System.Object
struct Il2CppObject;
// Pathfinding.PathThreadInfo
struct PathThreadInfo_t2420662483;
struct PathThreadInfo_t2420662483_marshaled_pinvoke;
struct PathThreadInfo_t2420662483_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_PathThreadInfo2420662483.h"
#include "AssemblyU2DCSharp_AstarPath4090270936.h"
#include "AssemblyU2DCSharp_Pathfinding_PathHandler918952263.h"

// System.Void Pathfinding.PathThreadInfo::.ctor(System.Int32,AstarPath,Pathfinding.PathHandler)
extern "C"  void PathThreadInfo__ctor_m4095537194 (PathThreadInfo_t2420662483 * __this, int32_t ___index0, AstarPath_t4090270936 * ___astar1, PathHandler_t918952263 * ___runData2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Pathfinding.PathThreadInfo::get_Lock()
extern "C"  Il2CppObject * PathThreadInfo_get_Lock_m1747519161 (PathThreadInfo_t2420662483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct PathThreadInfo_t2420662483;
struct PathThreadInfo_t2420662483_marshaled_pinvoke;

extern "C" void PathThreadInfo_t2420662483_marshal_pinvoke(const PathThreadInfo_t2420662483& unmarshaled, PathThreadInfo_t2420662483_marshaled_pinvoke& marshaled);
extern "C" void PathThreadInfo_t2420662483_marshal_pinvoke_back(const PathThreadInfo_t2420662483_marshaled_pinvoke& marshaled, PathThreadInfo_t2420662483& unmarshaled);
extern "C" void PathThreadInfo_t2420662483_marshal_pinvoke_cleanup(PathThreadInfo_t2420662483_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct PathThreadInfo_t2420662483;
struct PathThreadInfo_t2420662483_marshaled_com;

extern "C" void PathThreadInfo_t2420662483_marshal_com(const PathThreadInfo_t2420662483& unmarshaled, PathThreadInfo_t2420662483_marshaled_com& marshaled);
extern "C" void PathThreadInfo_t2420662483_marshal_com_back(const PathThreadInfo_t2420662483_marshaled_com& marshaled, PathThreadInfo_t2420662483& unmarshaled);
extern "C" void PathThreadInfo_t2420662483_marshal_com_cleanup(PathThreadInfo_t2420662483_marshaled_com& marshaled);
