﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pomelo.DotNetClient.PomeloClient/<initClient>c__AnonStorey162
struct U3CinitClientU3Ec__AnonStorey162_t2596269692;
// System.IAsyncResult
struct IAsyncResult_t2754620036;

#include "codegen/il2cpp-codegen.h"

// System.Void Pomelo.DotNetClient.PomeloClient/<initClient>c__AnonStorey162::.ctor()
extern "C"  void U3CinitClientU3Ec__AnonStorey162__ctor_m3258748527 (U3CinitClientU3Ec__AnonStorey162_t2596269692 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.DotNetClient.PomeloClient/<initClient>c__AnonStorey162::<>m__403(System.IAsyncResult)
extern "C"  void U3CinitClientU3Ec__AnonStorey162_U3CU3Em__403_m1155358050 (U3CinitClientU3Ec__AnonStorey162_t2596269692 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
