﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIDragObjectGenerated
struct UIDragObjectGenerated_t2205070856;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void UIDragObjectGenerated::.ctor()
extern "C"  void UIDragObjectGenerated__ctor_m2490403939 (UIDragObjectGenerated_t2205070856 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIDragObjectGenerated::UIDragObject_UIDragObject1(JSVCall,System.Int32)
extern "C"  bool UIDragObjectGenerated_UIDragObject_UIDragObject1_m3170836623 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDragObjectGenerated::UIDragObject_target(JSVCall)
extern "C"  void UIDragObjectGenerated_UIDragObject_target_m1759209853 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDragObjectGenerated::UIDragObject_panelRegion(JSVCall)
extern "C"  void UIDragObjectGenerated_UIDragObject_panelRegion_m1032471814 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDragObjectGenerated::UIDragObject_scrollMomentum(JSVCall)
extern "C"  void UIDragObjectGenerated_UIDragObject_scrollMomentum_m4048815881 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDragObjectGenerated::UIDragObject_restrictWithinPanel(JSVCall)
extern "C"  void UIDragObjectGenerated_UIDragObject_restrictWithinPanel_m1452599489 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDragObjectGenerated::UIDragObject_contentRect(JSVCall)
extern "C"  void UIDragObjectGenerated_UIDragObject_contentRect_m1746092737 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDragObjectGenerated::UIDragObject_dragEffect(JSVCall)
extern "C"  void UIDragObjectGenerated_UIDragObject_dragEffect_m3482692009 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDragObjectGenerated::UIDragObject_momentumAmount(JSVCall)
extern "C"  void UIDragObjectGenerated_UIDragObject_momentumAmount_m4254643934 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDragObjectGenerated::UIDragObject_dragMovement(JSVCall)
extern "C"  void UIDragObjectGenerated_UIDragObject_dragMovement_m1838100299 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIDragObjectGenerated::UIDragObject_CancelMovement(JSVCall,System.Int32)
extern "C"  bool UIDragObjectGenerated_UIDragObject_CancelMovement_m3153259270 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIDragObjectGenerated::UIDragObject_CancelSpring(JSVCall,System.Int32)
extern "C"  bool UIDragObjectGenerated_UIDragObject_CancelSpring_m2320194404 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDragObjectGenerated::__Register()
extern "C"  void UIDragObjectGenerated___Register_m2794993220 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIDragObjectGenerated::ilo_attachFinalizerObject1(System.Int32)
extern "C"  bool UIDragObjectGenerated_ilo_attachFinalizerObject1_m745974004 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDragObjectGenerated::ilo_addJSCSRel2(System.Int32,System.Object)
extern "C"  void UIDragObjectGenerated_ilo_addJSCSRel2_m4108374560 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UIDragObjectGenerated::ilo_getObject3(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UIDragObjectGenerated_ilo_getObject3_m816707556 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDragObjectGenerated::ilo_setVector3S4(System.Int32,UnityEngine.Vector3)
extern "C"  void UIDragObjectGenerated_ilo_setVector3S4_m2401954119 (Il2CppObject * __this /* static, unused */, int32_t ___e0, Vector3_t4282066566  ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UIDragObjectGenerated::ilo_getVector3S5(System.Int32)
extern "C"  Vector3_t4282066566  UIDragObjectGenerated_ilo_getVector3S5_m3359927623 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDragObjectGenerated::ilo_setBooleanS6(System.Int32,System.Boolean)
extern "C"  void UIDragObjectGenerated_ilo_setBooleanS6_m1454761647 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIDragObjectGenerated::ilo_setObject7(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UIDragObjectGenerated_ilo_setObject7_m4031678769 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDragObjectGenerated::ilo_setSingle8(System.Int32,System.Single)
extern "C"  void UIDragObjectGenerated_ilo_setSingle8_m2125273272 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UIDragObjectGenerated::ilo_getSingle9(System.Int32)
extern "C"  float UIDragObjectGenerated_ilo_getSingle9_m1584401044 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
