﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_zaheechem114
struct M_zaheechem114_t3290481366;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_zaheechem1143290481366.h"

// System.Void GarbageiOS.M_zaheechem114::.ctor()
extern "C"  void M_zaheechem114__ctor_m1861959709 (M_zaheechem114_t3290481366 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_zaheechem114::M_miryar0(System.String[],System.Int32)
extern "C"  void M_zaheechem114_M_miryar0_m1352603644 (M_zaheechem114_t3290481366 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_zaheechem114::M_soujisYerdrur1(System.String[],System.Int32)
extern "C"  void M_zaheechem114_M_soujisYerdrur1_m1380820811 (M_zaheechem114_t3290481366 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_zaheechem114::M_suter2(System.String[],System.Int32)
extern "C"  void M_zaheechem114_M_suter2_m524128291 (M_zaheechem114_t3290481366 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_zaheechem114::M_cigooRairpal3(System.String[],System.Int32)
extern "C"  void M_zaheechem114_M_cigooRairpal3_m1045876977 (M_zaheechem114_t3290481366 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_zaheechem114::M_koovi4(System.String[],System.Int32)
extern "C"  void M_zaheechem114_M_koovi4_m2651015110 (M_zaheechem114_t3290481366 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_zaheechem114::M_qowtarste5(System.String[],System.Int32)
extern "C"  void M_zaheechem114_M_qowtarste5_m2649330829 (M_zaheechem114_t3290481366 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_zaheechem114::M_hemjer6(System.String[],System.Int32)
extern "C"  void M_zaheechem114_M_hemjer6_m4212554223 (M_zaheechem114_t3290481366 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_zaheechem114::ilo_M_soujisYerdrur11(GarbageiOS.M_zaheechem114,System.String[],System.Int32)
extern "C"  void M_zaheechem114_ilo_M_soujisYerdrur11_m2581280645 (Il2CppObject * __this /* static, unused */, M_zaheechem114_t3290481366 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_zaheechem114::ilo_M_suter22(GarbageiOS.M_zaheechem114,System.String[],System.Int32)
extern "C"  void M_zaheechem114_ilo_M_suter22_m3487042990 (Il2CppObject * __this /* static, unused */, M_zaheechem114_t3290481366 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_zaheechem114::ilo_M_koovi43(GarbageiOS.M_zaheechem114,System.String[],System.Int32)
extern "C"  void M_zaheechem114_ilo_M_koovi43_m618901804 (Il2CppObject * __this /* static, unused */, M_zaheechem114_t3290481366 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
