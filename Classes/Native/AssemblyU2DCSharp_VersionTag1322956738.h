﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_ScriptableObject2970544072.h"
#include "AssemblyU2DCSharp_VSType2541381527.h"
#include "AssemblyU2DCSharp_UserType4093146565.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VersionTag
struct  VersionTag_t1322956738  : public ScriptableObject_t2970544072
{
public:
	// VSType VersionTag::type
	int32_t ___type_2;
	// UserType VersionTag::userType
	int32_t ___userType_3;

public:
	inline static int32_t get_offset_of_type_2() { return static_cast<int32_t>(offsetof(VersionTag_t1322956738, ___type_2)); }
	inline int32_t get_type_2() const { return ___type_2; }
	inline int32_t* get_address_of_type_2() { return &___type_2; }
	inline void set_type_2(int32_t value)
	{
		___type_2 = value;
	}

	inline static int32_t get_offset_of_userType_3() { return static_cast<int32_t>(offsetof(VersionTag_t1322956738, ___userType_3)); }
	inline int32_t get_userType_3() const { return ___userType_3; }
	inline int32_t* get_address_of_userType_3() { return &___userType_3; }
	inline void set_userType_3(int32_t value)
	{
		___userType_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
