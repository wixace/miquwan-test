﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GesturesMgrGenerated
struct GesturesMgrGenerated_t587998177;
// JSVCall
struct JSVCall_t3708497963;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;
// GesturesMgr
struct GesturesMgr_t1320398158;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_GesturesMgr1320398158.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void GesturesMgrGenerated::.ctor()
extern "C"  void GesturesMgrGenerated__ctor_m1621099482 (GesturesMgrGenerated_t587998177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GesturesMgrGenerated::GesturesMgr_GesturesMgr1(JSVCall,System.Int32)
extern "C"  bool GesturesMgrGenerated_GesturesMgr_GesturesMgr1_m4251246274 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GesturesMgrGenerated::GesturesMgr_zoomSpeed(JSVCall)
extern "C"  void GesturesMgrGenerated_GesturesMgr_zoomSpeed_m704193644 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GesturesMgrGenerated::GesturesMgr_minZoomAmount(JSVCall)
extern "C"  void GesturesMgrGenerated_GesturesMgr_minZoomAmount_m1139234755 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GesturesMgrGenerated::GesturesMgr_maxZoomAmount(JSVCall)
extern "C"  void GesturesMgrGenerated_GesturesMgr_maxZoomAmount_m3950119793 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GesturesMgrGenerated::GesturesMgr_SmoothSpeed(JSVCall)
extern "C"  void GesturesMgrGenerated_GesturesMgr_SmoothSpeed_m3028684967 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GesturesMgrGenerated::GesturesMgr_instance(JSVCall)
extern "C"  void GesturesMgrGenerated_GesturesMgr_instance_m2881003703 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GesturesMgrGenerated::GesturesMgr_DefaultFov(JSVCall)
extern "C"  void GesturesMgrGenerated_GesturesMgr_DefaultFov_m198217408 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GesturesMgrGenerated::GesturesMgr_IdealZoomAmount(JSVCall)
extern "C"  void GesturesMgrGenerated_GesturesMgr_IdealZoomAmount_m322979456 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GesturesMgrGenerated::GesturesMgr_ZoomAmount(JSVCall)
extern "C"  void GesturesMgrGenerated_GesturesMgr_ZoomAmount_m3017398881 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GesturesMgrGenerated::GesturesMgr_ZoomPercent(JSVCall)
extern "C"  void GesturesMgrGenerated_GesturesMgr_ZoomPercent_m4222874382 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GesturesMgrGenerated::GesturesMgr_OnDestroy(JSVCall,System.Int32)
extern "C"  bool GesturesMgrGenerated_GesturesMgr_OnDestroy_m2743048510 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GesturesMgrGenerated::GesturesMgr_OnTap__Vector3(JSVCall,System.Int32)
extern "C"  bool GesturesMgrGenerated_GesturesMgr_OnTap__Vector3_m2430787723 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GesturesMgrGenerated::GesturesMgr_SetPinch__Boolean(JSVCall,System.Int32)
extern "C"  bool GesturesMgrGenerated_GesturesMgr_SetPinch__Boolean_m4264873491 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GesturesMgrGenerated::GesturesMgr_SetZoomAmount__Single__Single(JSVCall,System.Int32)
extern "C"  bool GesturesMgrGenerated_GesturesMgr_SetZoomAmount__Single__Single_m3270037568 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GesturesMgrGenerated::__Register()
extern "C"  void GesturesMgrGenerated___Register_m4220244269 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 GesturesMgrGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t GesturesMgrGenerated_ilo_getObject1_m909144184 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GesturesMgrGenerated::ilo_setSingle2(System.Int32,System.Single)
extern "C"  void GesturesMgrGenerated_ilo_setSingle2_m14031579 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single GesturesMgrGenerated::ilo_getSingle3(System.Int32)
extern "C"  float GesturesMgrGenerated_ilo_getSingle3_m2421399567 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 GesturesMgrGenerated::ilo_setObject4(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t GesturesMgrGenerated_ilo_setObject4_m1628217011 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GesturesMgrGenerated::ilo_set_DefaultFov5(GesturesMgr,System.Single)
extern "C"  void GesturesMgrGenerated_ilo_set_DefaultFov5_m2834730744 (Il2CppObject * __this /* static, unused */, GesturesMgr_t1320398158 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GesturesMgrGenerated::ilo_set_IdealZoomAmount6(GesturesMgr,System.Single)
extern "C"  void GesturesMgrGenerated_ilo_set_IdealZoomAmount6_m4147760953 (Il2CppObject * __this /* static, unused */, GesturesMgr_t1320398158 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single GesturesMgrGenerated::ilo_get_ZoomAmount7(GesturesMgr)
extern "C"  float GesturesMgrGenerated_ilo_get_ZoomAmount7_m1745971432 (Il2CppObject * __this /* static, unused */, GesturesMgr_t1320398158 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 GesturesMgrGenerated::ilo_getVector3S8(System.Int32)
extern "C"  Vector3_t4282066566  GesturesMgrGenerated_ilo_getVector3S8_m1407535207 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
