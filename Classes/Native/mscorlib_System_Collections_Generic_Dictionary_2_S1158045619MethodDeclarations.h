﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<MScrollView/MoveWay,System.Object>
struct ShimEnumerator_t1158045619;
// System.Collections.Generic.Dictionary`2<MScrollView/MoveWay,System.Object>
struct Dictionary_2_t1442267592;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<MScrollView/MoveWay,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m4171657994_gshared (ShimEnumerator_t1158045619 * __this, Dictionary_2_t1442267592 * ___host0, const MethodInfo* method);
#define ShimEnumerator__ctor_m4171657994(__this, ___host0, method) ((  void (*) (ShimEnumerator_t1158045619 *, Dictionary_2_t1442267592 *, const MethodInfo*))ShimEnumerator__ctor_m4171657994_gshared)(__this, ___host0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<MScrollView/MoveWay,System.Object>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m584841559_gshared (ShimEnumerator_t1158045619 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m584841559(__this, method) ((  bool (*) (ShimEnumerator_t1158045619 *, const MethodInfo*))ShimEnumerator_MoveNext_m584841559_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<MScrollView/MoveWay,System.Object>::get_Entry()
extern "C"  DictionaryEntry_t1751606614  ShimEnumerator_get_Entry_m1757353373_gshared (ShimEnumerator_t1158045619 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m1757353373(__this, method) ((  DictionaryEntry_t1751606614  (*) (ShimEnumerator_t1158045619 *, const MethodInfo*))ShimEnumerator_get_Entry_m1757353373_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<MScrollView/MoveWay,System.Object>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m3913417272_gshared (ShimEnumerator_t1158045619 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m3913417272(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t1158045619 *, const MethodInfo*))ShimEnumerator_get_Key_m3913417272_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<MScrollView/MoveWay,System.Object>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m3746050762_gshared (ShimEnumerator_t1158045619 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m3746050762(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t1158045619 *, const MethodInfo*))ShimEnumerator_get_Value_m3746050762_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<MScrollView/MoveWay,System.Object>::get_Current()
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m2196879058_gshared (ShimEnumerator_t1158045619 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m2196879058(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t1158045619 *, const MethodInfo*))ShimEnumerator_get_Current_m2196879058_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<MScrollView/MoveWay,System.Object>::Reset()
extern "C"  void ShimEnumerator_Reset_m4142538332_gshared (ShimEnumerator_t1158045619 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m4142538332(__this, method) ((  void (*) (ShimEnumerator_t1158045619 *, const MethodInfo*))ShimEnumerator_Reset_m4142538332_gshared)(__this, method)
