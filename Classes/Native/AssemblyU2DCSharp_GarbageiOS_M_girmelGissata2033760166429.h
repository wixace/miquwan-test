﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_girmelGissata203
struct  M_girmelGissata203_t3760166429  : public Il2CppObject
{
public:
	// System.Single GarbageiOS.M_girmelGissata203::_cearba
	float ____cearba_0;
	// System.String GarbageiOS.M_girmelGissata203::_vapawkerMegisboo
	String_t* ____vapawkerMegisboo_1;
	// System.String GarbageiOS.M_girmelGissata203::_xeewhembairXearrall
	String_t* ____xeewhembairXearrall_2;
	// System.String GarbageiOS.M_girmelGissata203::_sowser
	String_t* ____sowser_3;
	// System.Boolean GarbageiOS.M_girmelGissata203::_mastalba
	bool ____mastalba_4;
	// System.Single GarbageiOS.M_girmelGissata203::_sirmee
	float ____sirmee_5;
	// System.Boolean GarbageiOS.M_girmelGissata203::_kelmuJoma
	bool ____kelmuJoma_6;
	// System.Boolean GarbageiOS.M_girmelGissata203::_tallpijou
	bool ____tallpijou_7;
	// System.String GarbageiOS.M_girmelGissata203::_sallcea
	String_t* ____sallcea_8;
	// System.String GarbageiOS.M_girmelGissata203::_lawwaro
	String_t* ____lawwaro_9;
	// System.Single GarbageiOS.M_girmelGissata203::_mouceasa
	float ____mouceasa_10;
	// System.Boolean GarbageiOS.M_girmelGissata203::_bomalirDiwowda
	bool ____bomalirDiwowda_11;
	// System.UInt32 GarbageiOS.M_girmelGissata203::_drelpu
	uint32_t ____drelpu_12;
	// System.String GarbageiOS.M_girmelGissata203::_cerepaytir
	String_t* ____cerepaytir_13;
	// System.String GarbageiOS.M_girmelGissata203::_tica
	String_t* ____tica_14;
	// System.String GarbageiOS.M_girmelGissata203::_kaswuJageeja
	String_t* ____kaswuJageeja_15;
	// System.String GarbageiOS.M_girmelGissata203::_papesis
	String_t* ____papesis_16;
	// System.Single GarbageiOS.M_girmelGissata203::_dairtrousal
	float ____dairtrousal_17;
	// System.String GarbageiOS.M_girmelGissata203::_betalKatrem
	String_t* ____betalKatrem_18;
	// System.UInt32 GarbageiOS.M_girmelGissata203::_trowhisFallvoofea
	uint32_t ____trowhisFallvoofea_19;
	// System.String GarbageiOS.M_girmelGissata203::_mumirrem
	String_t* ____mumirrem_20;
	// System.Boolean GarbageiOS.M_girmelGissata203::_jowbeJalxear
	bool ____jowbeJalxear_21;
	// System.String GarbageiOS.M_girmelGissata203::_sellearno
	String_t* ____sellearno_22;
	// System.Single GarbageiOS.M_girmelGissata203::_lorcaybi
	float ____lorcaybi_23;
	// System.String GarbageiOS.M_girmelGissata203::_sazeecis
	String_t* ____sazeecis_24;
	// System.String GarbageiOS.M_girmelGissata203::_tawkemMofer
	String_t* ____tawkemMofer_25;
	// System.Single GarbageiOS.M_girmelGissata203::_latejer
	float ____latejer_26;
	// System.Boolean GarbageiOS.M_girmelGissata203::_drorciDremesu
	bool ____drorciDremesu_27;
	// System.String GarbageiOS.M_girmelGissata203::_jagirkear
	String_t* ____jagirkear_28;
	// System.Boolean GarbageiOS.M_girmelGissata203::_stowsagere
	bool ____stowsagere_29;
	// System.Single GarbageiOS.M_girmelGissata203::_leremeLotu
	float ____leremeLotu_30;

public:
	inline static int32_t get_offset_of__cearba_0() { return static_cast<int32_t>(offsetof(M_girmelGissata203_t3760166429, ____cearba_0)); }
	inline float get__cearba_0() const { return ____cearba_0; }
	inline float* get_address_of__cearba_0() { return &____cearba_0; }
	inline void set__cearba_0(float value)
	{
		____cearba_0 = value;
	}

	inline static int32_t get_offset_of__vapawkerMegisboo_1() { return static_cast<int32_t>(offsetof(M_girmelGissata203_t3760166429, ____vapawkerMegisboo_1)); }
	inline String_t* get__vapawkerMegisboo_1() const { return ____vapawkerMegisboo_1; }
	inline String_t** get_address_of__vapawkerMegisboo_1() { return &____vapawkerMegisboo_1; }
	inline void set__vapawkerMegisboo_1(String_t* value)
	{
		____vapawkerMegisboo_1 = value;
		Il2CppCodeGenWriteBarrier(&____vapawkerMegisboo_1, value);
	}

	inline static int32_t get_offset_of__xeewhembairXearrall_2() { return static_cast<int32_t>(offsetof(M_girmelGissata203_t3760166429, ____xeewhembairXearrall_2)); }
	inline String_t* get__xeewhembairXearrall_2() const { return ____xeewhembairXearrall_2; }
	inline String_t** get_address_of__xeewhembairXearrall_2() { return &____xeewhembairXearrall_2; }
	inline void set__xeewhembairXearrall_2(String_t* value)
	{
		____xeewhembairXearrall_2 = value;
		Il2CppCodeGenWriteBarrier(&____xeewhembairXearrall_2, value);
	}

	inline static int32_t get_offset_of__sowser_3() { return static_cast<int32_t>(offsetof(M_girmelGissata203_t3760166429, ____sowser_3)); }
	inline String_t* get__sowser_3() const { return ____sowser_3; }
	inline String_t** get_address_of__sowser_3() { return &____sowser_3; }
	inline void set__sowser_3(String_t* value)
	{
		____sowser_3 = value;
		Il2CppCodeGenWriteBarrier(&____sowser_3, value);
	}

	inline static int32_t get_offset_of__mastalba_4() { return static_cast<int32_t>(offsetof(M_girmelGissata203_t3760166429, ____mastalba_4)); }
	inline bool get__mastalba_4() const { return ____mastalba_4; }
	inline bool* get_address_of__mastalba_4() { return &____mastalba_4; }
	inline void set__mastalba_4(bool value)
	{
		____mastalba_4 = value;
	}

	inline static int32_t get_offset_of__sirmee_5() { return static_cast<int32_t>(offsetof(M_girmelGissata203_t3760166429, ____sirmee_5)); }
	inline float get__sirmee_5() const { return ____sirmee_5; }
	inline float* get_address_of__sirmee_5() { return &____sirmee_5; }
	inline void set__sirmee_5(float value)
	{
		____sirmee_5 = value;
	}

	inline static int32_t get_offset_of__kelmuJoma_6() { return static_cast<int32_t>(offsetof(M_girmelGissata203_t3760166429, ____kelmuJoma_6)); }
	inline bool get__kelmuJoma_6() const { return ____kelmuJoma_6; }
	inline bool* get_address_of__kelmuJoma_6() { return &____kelmuJoma_6; }
	inline void set__kelmuJoma_6(bool value)
	{
		____kelmuJoma_6 = value;
	}

	inline static int32_t get_offset_of__tallpijou_7() { return static_cast<int32_t>(offsetof(M_girmelGissata203_t3760166429, ____tallpijou_7)); }
	inline bool get__tallpijou_7() const { return ____tallpijou_7; }
	inline bool* get_address_of__tallpijou_7() { return &____tallpijou_7; }
	inline void set__tallpijou_7(bool value)
	{
		____tallpijou_7 = value;
	}

	inline static int32_t get_offset_of__sallcea_8() { return static_cast<int32_t>(offsetof(M_girmelGissata203_t3760166429, ____sallcea_8)); }
	inline String_t* get__sallcea_8() const { return ____sallcea_8; }
	inline String_t** get_address_of__sallcea_8() { return &____sallcea_8; }
	inline void set__sallcea_8(String_t* value)
	{
		____sallcea_8 = value;
		Il2CppCodeGenWriteBarrier(&____sallcea_8, value);
	}

	inline static int32_t get_offset_of__lawwaro_9() { return static_cast<int32_t>(offsetof(M_girmelGissata203_t3760166429, ____lawwaro_9)); }
	inline String_t* get__lawwaro_9() const { return ____lawwaro_9; }
	inline String_t** get_address_of__lawwaro_9() { return &____lawwaro_9; }
	inline void set__lawwaro_9(String_t* value)
	{
		____lawwaro_9 = value;
		Il2CppCodeGenWriteBarrier(&____lawwaro_9, value);
	}

	inline static int32_t get_offset_of__mouceasa_10() { return static_cast<int32_t>(offsetof(M_girmelGissata203_t3760166429, ____mouceasa_10)); }
	inline float get__mouceasa_10() const { return ____mouceasa_10; }
	inline float* get_address_of__mouceasa_10() { return &____mouceasa_10; }
	inline void set__mouceasa_10(float value)
	{
		____mouceasa_10 = value;
	}

	inline static int32_t get_offset_of__bomalirDiwowda_11() { return static_cast<int32_t>(offsetof(M_girmelGissata203_t3760166429, ____bomalirDiwowda_11)); }
	inline bool get__bomalirDiwowda_11() const { return ____bomalirDiwowda_11; }
	inline bool* get_address_of__bomalirDiwowda_11() { return &____bomalirDiwowda_11; }
	inline void set__bomalirDiwowda_11(bool value)
	{
		____bomalirDiwowda_11 = value;
	}

	inline static int32_t get_offset_of__drelpu_12() { return static_cast<int32_t>(offsetof(M_girmelGissata203_t3760166429, ____drelpu_12)); }
	inline uint32_t get__drelpu_12() const { return ____drelpu_12; }
	inline uint32_t* get_address_of__drelpu_12() { return &____drelpu_12; }
	inline void set__drelpu_12(uint32_t value)
	{
		____drelpu_12 = value;
	}

	inline static int32_t get_offset_of__cerepaytir_13() { return static_cast<int32_t>(offsetof(M_girmelGissata203_t3760166429, ____cerepaytir_13)); }
	inline String_t* get__cerepaytir_13() const { return ____cerepaytir_13; }
	inline String_t** get_address_of__cerepaytir_13() { return &____cerepaytir_13; }
	inline void set__cerepaytir_13(String_t* value)
	{
		____cerepaytir_13 = value;
		Il2CppCodeGenWriteBarrier(&____cerepaytir_13, value);
	}

	inline static int32_t get_offset_of__tica_14() { return static_cast<int32_t>(offsetof(M_girmelGissata203_t3760166429, ____tica_14)); }
	inline String_t* get__tica_14() const { return ____tica_14; }
	inline String_t** get_address_of__tica_14() { return &____tica_14; }
	inline void set__tica_14(String_t* value)
	{
		____tica_14 = value;
		Il2CppCodeGenWriteBarrier(&____tica_14, value);
	}

	inline static int32_t get_offset_of__kaswuJageeja_15() { return static_cast<int32_t>(offsetof(M_girmelGissata203_t3760166429, ____kaswuJageeja_15)); }
	inline String_t* get__kaswuJageeja_15() const { return ____kaswuJageeja_15; }
	inline String_t** get_address_of__kaswuJageeja_15() { return &____kaswuJageeja_15; }
	inline void set__kaswuJageeja_15(String_t* value)
	{
		____kaswuJageeja_15 = value;
		Il2CppCodeGenWriteBarrier(&____kaswuJageeja_15, value);
	}

	inline static int32_t get_offset_of__papesis_16() { return static_cast<int32_t>(offsetof(M_girmelGissata203_t3760166429, ____papesis_16)); }
	inline String_t* get__papesis_16() const { return ____papesis_16; }
	inline String_t** get_address_of__papesis_16() { return &____papesis_16; }
	inline void set__papesis_16(String_t* value)
	{
		____papesis_16 = value;
		Il2CppCodeGenWriteBarrier(&____papesis_16, value);
	}

	inline static int32_t get_offset_of__dairtrousal_17() { return static_cast<int32_t>(offsetof(M_girmelGissata203_t3760166429, ____dairtrousal_17)); }
	inline float get__dairtrousal_17() const { return ____dairtrousal_17; }
	inline float* get_address_of__dairtrousal_17() { return &____dairtrousal_17; }
	inline void set__dairtrousal_17(float value)
	{
		____dairtrousal_17 = value;
	}

	inline static int32_t get_offset_of__betalKatrem_18() { return static_cast<int32_t>(offsetof(M_girmelGissata203_t3760166429, ____betalKatrem_18)); }
	inline String_t* get__betalKatrem_18() const { return ____betalKatrem_18; }
	inline String_t** get_address_of__betalKatrem_18() { return &____betalKatrem_18; }
	inline void set__betalKatrem_18(String_t* value)
	{
		____betalKatrem_18 = value;
		Il2CppCodeGenWriteBarrier(&____betalKatrem_18, value);
	}

	inline static int32_t get_offset_of__trowhisFallvoofea_19() { return static_cast<int32_t>(offsetof(M_girmelGissata203_t3760166429, ____trowhisFallvoofea_19)); }
	inline uint32_t get__trowhisFallvoofea_19() const { return ____trowhisFallvoofea_19; }
	inline uint32_t* get_address_of__trowhisFallvoofea_19() { return &____trowhisFallvoofea_19; }
	inline void set__trowhisFallvoofea_19(uint32_t value)
	{
		____trowhisFallvoofea_19 = value;
	}

	inline static int32_t get_offset_of__mumirrem_20() { return static_cast<int32_t>(offsetof(M_girmelGissata203_t3760166429, ____mumirrem_20)); }
	inline String_t* get__mumirrem_20() const { return ____mumirrem_20; }
	inline String_t** get_address_of__mumirrem_20() { return &____mumirrem_20; }
	inline void set__mumirrem_20(String_t* value)
	{
		____mumirrem_20 = value;
		Il2CppCodeGenWriteBarrier(&____mumirrem_20, value);
	}

	inline static int32_t get_offset_of__jowbeJalxear_21() { return static_cast<int32_t>(offsetof(M_girmelGissata203_t3760166429, ____jowbeJalxear_21)); }
	inline bool get__jowbeJalxear_21() const { return ____jowbeJalxear_21; }
	inline bool* get_address_of__jowbeJalxear_21() { return &____jowbeJalxear_21; }
	inline void set__jowbeJalxear_21(bool value)
	{
		____jowbeJalxear_21 = value;
	}

	inline static int32_t get_offset_of__sellearno_22() { return static_cast<int32_t>(offsetof(M_girmelGissata203_t3760166429, ____sellearno_22)); }
	inline String_t* get__sellearno_22() const { return ____sellearno_22; }
	inline String_t** get_address_of__sellearno_22() { return &____sellearno_22; }
	inline void set__sellearno_22(String_t* value)
	{
		____sellearno_22 = value;
		Il2CppCodeGenWriteBarrier(&____sellearno_22, value);
	}

	inline static int32_t get_offset_of__lorcaybi_23() { return static_cast<int32_t>(offsetof(M_girmelGissata203_t3760166429, ____lorcaybi_23)); }
	inline float get__lorcaybi_23() const { return ____lorcaybi_23; }
	inline float* get_address_of__lorcaybi_23() { return &____lorcaybi_23; }
	inline void set__lorcaybi_23(float value)
	{
		____lorcaybi_23 = value;
	}

	inline static int32_t get_offset_of__sazeecis_24() { return static_cast<int32_t>(offsetof(M_girmelGissata203_t3760166429, ____sazeecis_24)); }
	inline String_t* get__sazeecis_24() const { return ____sazeecis_24; }
	inline String_t** get_address_of__sazeecis_24() { return &____sazeecis_24; }
	inline void set__sazeecis_24(String_t* value)
	{
		____sazeecis_24 = value;
		Il2CppCodeGenWriteBarrier(&____sazeecis_24, value);
	}

	inline static int32_t get_offset_of__tawkemMofer_25() { return static_cast<int32_t>(offsetof(M_girmelGissata203_t3760166429, ____tawkemMofer_25)); }
	inline String_t* get__tawkemMofer_25() const { return ____tawkemMofer_25; }
	inline String_t** get_address_of__tawkemMofer_25() { return &____tawkemMofer_25; }
	inline void set__tawkemMofer_25(String_t* value)
	{
		____tawkemMofer_25 = value;
		Il2CppCodeGenWriteBarrier(&____tawkemMofer_25, value);
	}

	inline static int32_t get_offset_of__latejer_26() { return static_cast<int32_t>(offsetof(M_girmelGissata203_t3760166429, ____latejer_26)); }
	inline float get__latejer_26() const { return ____latejer_26; }
	inline float* get_address_of__latejer_26() { return &____latejer_26; }
	inline void set__latejer_26(float value)
	{
		____latejer_26 = value;
	}

	inline static int32_t get_offset_of__drorciDremesu_27() { return static_cast<int32_t>(offsetof(M_girmelGissata203_t3760166429, ____drorciDremesu_27)); }
	inline bool get__drorciDremesu_27() const { return ____drorciDremesu_27; }
	inline bool* get_address_of__drorciDremesu_27() { return &____drorciDremesu_27; }
	inline void set__drorciDremesu_27(bool value)
	{
		____drorciDremesu_27 = value;
	}

	inline static int32_t get_offset_of__jagirkear_28() { return static_cast<int32_t>(offsetof(M_girmelGissata203_t3760166429, ____jagirkear_28)); }
	inline String_t* get__jagirkear_28() const { return ____jagirkear_28; }
	inline String_t** get_address_of__jagirkear_28() { return &____jagirkear_28; }
	inline void set__jagirkear_28(String_t* value)
	{
		____jagirkear_28 = value;
		Il2CppCodeGenWriteBarrier(&____jagirkear_28, value);
	}

	inline static int32_t get_offset_of__stowsagere_29() { return static_cast<int32_t>(offsetof(M_girmelGissata203_t3760166429, ____stowsagere_29)); }
	inline bool get__stowsagere_29() const { return ____stowsagere_29; }
	inline bool* get_address_of__stowsagere_29() { return &____stowsagere_29; }
	inline void set__stowsagere_29(bool value)
	{
		____stowsagere_29 = value;
	}

	inline static int32_t get_offset_of__leremeLotu_30() { return static_cast<int32_t>(offsetof(M_girmelGissata203_t3760166429, ____leremeLotu_30)); }
	inline float get__leremeLotu_30() const { return ____leremeLotu_30; }
	inline float* get_address_of__leremeLotu_30() { return &____leremeLotu_30; }
	inline void set__leremeLotu_30(float value)
	{
		____leremeLotu_30 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
