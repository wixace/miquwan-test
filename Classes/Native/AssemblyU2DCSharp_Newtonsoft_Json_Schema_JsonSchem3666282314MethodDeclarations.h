﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Schema.JsonSchemaGenerator/<GenerateInternal>c__AnonStorey12E
struct U3CGenerateInternalU3Ec__AnonStorey12E_t3666282314;
// Newtonsoft.Json.Schema.JsonSchemaGenerator/TypeSchema
struct TypeSchema_t486414904;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Schema_JsonSchema486414904.h"

// System.Void Newtonsoft.Json.Schema.JsonSchemaGenerator/<GenerateInternal>c__AnonStorey12E::.ctor()
extern "C"  void U3CGenerateInternalU3Ec__AnonStorey12E__ctor_m2653597025 (U3CGenerateInternalU3Ec__AnonStorey12E_t3666282314 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Schema.JsonSchemaGenerator/<GenerateInternal>c__AnonStorey12E::<>m__37B(Newtonsoft.Json.Schema.JsonSchemaGenerator/TypeSchema)
extern "C"  bool U3CGenerateInternalU3Ec__AnonStorey12E_U3CU3Em__37B_m461709528 (U3CGenerateInternalU3Ec__AnonStorey12E_t3666282314 * __this, TypeSchema_t486414904 * ___tc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
