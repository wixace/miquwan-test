﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_ceahelhe63
struct M_ceahelhe63_t3603911226;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_ceahelhe633603911226.h"

// System.Void GarbageiOS.M_ceahelhe63::.ctor()
extern "C"  void M_ceahelhe63__ctor_m2357713721 (M_ceahelhe63_t3603911226 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_ceahelhe63::M_joudrihowTrajo0(System.String[],System.Int32)
extern "C"  void M_ceahelhe63_M_joudrihowTrajo0_m2743992585 (M_ceahelhe63_t3603911226 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_ceahelhe63::M_tesisdro1(System.String[],System.Int32)
extern "C"  void M_ceahelhe63_M_tesisdro1_m2217347384 (M_ceahelhe63_t3603911226 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_ceahelhe63::M_nursurDalke2(System.String[],System.Int32)
extern "C"  void M_ceahelhe63_M_nursurDalke2_m4224847138 (M_ceahelhe63_t3603911226 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_ceahelhe63::ilo_M_joudrihowTrajo01(GarbageiOS.M_ceahelhe63,System.String[],System.Int32)
extern "C"  void M_ceahelhe63_ilo_M_joudrihowTrajo01_m376819057 (Il2CppObject * __this /* static, unused */, M_ceahelhe63_t3603911226 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
