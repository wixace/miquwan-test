﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Newtonsoft.Json.JsonSerializer
struct JsonSerializer_t251850770;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.JsonFormatterConverter
struct  JsonFormatterConverter_t4063403306  : public Il2CppObject
{
public:
	// Newtonsoft.Json.JsonSerializer Newtonsoft.Json.Serialization.JsonFormatterConverter::_serializer
	JsonSerializer_t251850770 * ____serializer_0;

public:
	inline static int32_t get_offset_of__serializer_0() { return static_cast<int32_t>(offsetof(JsonFormatterConverter_t4063403306, ____serializer_0)); }
	inline JsonSerializer_t251850770 * get__serializer_0() const { return ____serializer_0; }
	inline JsonSerializer_t251850770 ** get_address_of__serializer_0() { return &____serializer_0; }
	inline void set__serializer_0(JsonSerializer_t251850770 * value)
	{
		____serializer_0 = value;
		Il2CppCodeGenWriteBarrier(&____serializer_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
