﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._57f8cb4c0c3f73fe08e46335ac989d7d
struct _57f8cb4c0c3f73fe08e46335ac989d7d_t2260031403;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__57f8cb4c0c3f73fe08e463352260031403.h"

// System.Void Little._57f8cb4c0c3f73fe08e46335ac989d7d::.ctor()
extern "C"  void _57f8cb4c0c3f73fe08e46335ac989d7d__ctor_m321797858 (_57f8cb4c0c3f73fe08e46335ac989d7d_t2260031403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._57f8cb4c0c3f73fe08e46335ac989d7d::_57f8cb4c0c3f73fe08e46335ac989d7dm2(System.Int32)
extern "C"  int32_t _57f8cb4c0c3f73fe08e46335ac989d7d__57f8cb4c0c3f73fe08e46335ac989d7dm2_m3698203065 (_57f8cb4c0c3f73fe08e46335ac989d7d_t2260031403 * __this, int32_t ____57f8cb4c0c3f73fe08e46335ac989d7da0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._57f8cb4c0c3f73fe08e46335ac989d7d::_57f8cb4c0c3f73fe08e46335ac989d7dm(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _57f8cb4c0c3f73fe08e46335ac989d7d__57f8cb4c0c3f73fe08e46335ac989d7dm_m779743517 (_57f8cb4c0c3f73fe08e46335ac989d7d_t2260031403 * __this, int32_t ____57f8cb4c0c3f73fe08e46335ac989d7da0, int32_t ____57f8cb4c0c3f73fe08e46335ac989d7d491, int32_t ____57f8cb4c0c3f73fe08e46335ac989d7dc2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._57f8cb4c0c3f73fe08e46335ac989d7d::ilo__57f8cb4c0c3f73fe08e46335ac989d7dm21(Little._57f8cb4c0c3f73fe08e46335ac989d7d,System.Int32)
extern "C"  int32_t _57f8cb4c0c3f73fe08e46335ac989d7d_ilo__57f8cb4c0c3f73fe08e46335ac989d7dm21_m921007058 (Il2CppObject * __this /* static, unused */, _57f8cb4c0c3f73fe08e46335ac989d7d_t2260031403 * ____this0, int32_t ____57f8cb4c0c3f73fe08e46335ac989d7da1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
