﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.LayerGridGraph
struct LayerGridGraph_t3415576653;
// Pathfinding.GraphNodeDelegateCancelable
struct GraphNodeDelegateCancelable_t3591372971;
// Pathfinding.GraphUpdateObject
struct GraphUpdateObject_t430843704;
// OnScanStatus
struct OnScanStatus_t2412749870;
// Pathfinding.LevelGridNode
struct LevelGridNode_t489265774;
// Pathfinding.GraphNode[]
struct GraphNodeU5BU5D_t927449255;
// Pathfinding.GraphNode
struct GraphNode_t23612370;
// Pathfinding.NNConstraint
struct NNConstraint_t758567699;
// Pathfinding.Serialization.GraphSerializationContext
struct GraphSerializationContext_t3256954663;
// Pathfinding.GridGraph
struct GridGraph_t2455707914;
// Pathfinding.GraphCollision
struct GraphCollision_t2160440954;
// Pathfinding.NavGraph
struct NavGraph_t1254319713;
// Pathfinding.PathHandler
struct PathHandler_t918952263;
// AstarPath
struct AstarPath_t4090270936;
// Pathfinding.Path
struct Path_t1974241691;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphNodeDelegateCan3591372971.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphUpdateObject430843704.h"
#include "AssemblyU2DCSharp_OnScanStatus2412749870.h"
#include "AssemblyU2DCSharp_Pathfinding_LevelGridNode489265774.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphNode23612370.h"
#include "AssemblyU2DCSharp_Pathfinding_NNInfo1570625892.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_Pathfinding_NNConstraint758567699.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphHitInfo3082627369.h"
#include "AssemblyU2DCSharp_Pathfinding_Serialization_GraphS3256954663.h"
#include "AssemblyU2DCSharp_Pathfinding_GridGraph2455707914.h"
#include "UnityEngine_UnityEngine_Bounds2711641849.h"
#include "UnityEngine_UnityEngine_Matrix4x41651859333.h"
#include "AssemblyU2DCSharp_Pathfinding_IntRect3015058261.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphCollision2160440954.h"
#include "AssemblyU2DCSharp_Pathfinding_LayerGridGraph3415576653.h"
#include "AssemblyU2DCSharp_Pathfinding_Int31974045594.h"
#include "AssemblyU2DCSharp_Pathfinding_NavGraph1254319713.h"
#include "AssemblyU2DCSharp_AstarPath4090270936.h"
#include "AssemblyU2DCSharp_Pathfinding_Path1974241691.h"

// System.Void Pathfinding.LayerGridGraph::.ctor()
extern "C"  void LayerGridGraph__ctor_m3803993786 (LayerGridGraph_t3415576653 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.LayerGridGraph::OnDestroy()
extern "C"  void LayerGridGraph_OnDestroy_m2058370739 (LayerGridGraph_t3415576653 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.LayerGridGraph::RemoveGridGraphFromStatic()
extern "C"  void LayerGridGraph_RemoveGridGraphFromStatic_m2196443828 (LayerGridGraph_t3415576653 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.LayerGridGraph::get_uniformWidthDepthGrid()
extern "C"  bool LayerGridGraph_get_uniformWidthDepthGrid_m1812704754 (LayerGridGraph_t3415576653 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.LayerGridGraph::GetNodes(Pathfinding.GraphNodeDelegateCancelable)
extern "C"  void LayerGridGraph_GetNodes_m585918542 (LayerGridGraph_t3415576653 * __this, GraphNodeDelegateCancelable_t3591372971 * ___del0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.LayerGridGraph::UpdateArea(Pathfinding.GraphUpdateObject)
extern "C"  void LayerGridGraph_UpdateArea_m2553790278 (LayerGridGraph_t3415576653 * __this, GraphUpdateObject_t430843704 * ___o0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.LayerGridGraph::ScanInternal(OnScanStatus)
extern "C"  void LayerGridGraph_ScanInternal_m1779987766 (LayerGridGraph_t3415576653 * __this, OnScanStatus_t2412749870 * ___status0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.LayerGridGraph::RecalculateCell(System.Int32,System.Int32,System.Boolean)
extern "C"  bool LayerGridGraph_RecalculateCell_m2997832912 (LayerGridGraph_t3415576653 * __this, int32_t ___x0, int32_t ___z1, bool ___preserveExistingNodes2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.LayerGridGraph::AddLayers(System.Int32)
extern "C"  void LayerGridGraph_AddLayers_m10417580 (LayerGridGraph_t3415576653 * __this, int32_t ___count0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.LayerGridGraph::UpdatePenalty(Pathfinding.LevelGridNode)
extern "C"  void LayerGridGraph_UpdatePenalty_m2048856568 (LayerGridGraph_t3415576653 * __this, LevelGridNode_t489265774 * ___node0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.LayerGridGraph::ErodeWalkableArea(System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  void LayerGridGraph_ErodeWalkableArea_m1464946619 (LayerGridGraph_t3415576653 * __this, int32_t ___xmin0, int32_t ___zmin1, int32_t ___xmax2, int32_t ___zmax3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.LayerGridGraph::CalculateConnections(Pathfinding.GraphNode[],Pathfinding.GraphNode,System.Int32,System.Int32,System.Int32)
extern "C"  void LayerGridGraph_CalculateConnections_m624676064 (LayerGridGraph_t3415576653 * __this, GraphNodeU5BU5D_t927449255* ___nodes0, GraphNode_t23612370 * ___node1, int32_t ___x2, int32_t ___z3, int32_t ___layerIndex4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.NNInfo Pathfinding.LayerGridGraph::GetNearest(UnityEngine.Vector3,Pathfinding.NNConstraint,Pathfinding.GraphNode)
extern "C"  NNInfo_t1570625892  LayerGridGraph_GetNearest_m1591563747 (LayerGridGraph_t3415576653 * __this, Vector3_t4282066566  ___position0, NNConstraint_t758567699 * ___constraint1, GraphNode_t23612370 * ___hint2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.LevelGridNode Pathfinding.LayerGridGraph::GetNearestNode(UnityEngine.Vector3,System.Int32,System.Int32,Pathfinding.NNConstraint)
extern "C"  LevelGridNode_t489265774 * LayerGridGraph_GetNearestNode_m2376806603 (LayerGridGraph_t3415576653 * __this, Vector3_t4282066566  ___position0, int32_t ___x1, int32_t ___z2, NNConstraint_t758567699 * ___constraint3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.NNInfo Pathfinding.LayerGridGraph::GetNearestForce(UnityEngine.Vector3,Pathfinding.NNConstraint)
extern "C"  NNInfo_t1570625892  LayerGridGraph_GetNearestForce_m2757418652 (LayerGridGraph_t3415576653 * __this, Vector3_t4282066566  ___position0, NNConstraint_t758567699 * ___constraint1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.LayerGridGraph::Linecast(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  bool LayerGridGraph_Linecast_m2583727835 (LayerGridGraph_t3415576653 * __this, Vector3_t4282066566  ____a0, Vector3_t4282066566  ____b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.LayerGridGraph::Linecast(UnityEngine.Vector3,UnityEngine.Vector3,Pathfinding.GraphNode)
extern "C"  bool LayerGridGraph_Linecast_m2888902337 (LayerGridGraph_t3415576653 * __this, Vector3_t4282066566  ____a0, Vector3_t4282066566  ____b1, GraphNode_t23612370 * ___hint2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.LayerGridGraph::Linecast(UnityEngine.Vector3,UnityEngine.Vector3,Pathfinding.GraphNode,Pathfinding.GraphHitInfo&)
extern "C"  bool LayerGridGraph_Linecast_m2372630308 (LayerGridGraph_t3415576653 * __this, Vector3_t4282066566  ____a0, Vector3_t4282066566  ____b1, GraphNode_t23612370 * ___hint2, GraphHitInfo_t3082627369 * ___hit3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.LayerGridGraph::SnappedLinecast(UnityEngine.Vector3,UnityEngine.Vector3,Pathfinding.GraphNode,Pathfinding.GraphHitInfo&)
extern "C"  bool LayerGridGraph_SnappedLinecast_m20647289 (LayerGridGraph_t3415576653 * __this, Vector3_t4282066566  ____a0, Vector3_t4282066566  ____b1, GraphNode_t23612370 * ___hint2, GraphHitInfo_t3082627369 * ___hit3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.LayerGridGraph::CheckConnection(Pathfinding.LevelGridNode,System.Int32)
extern "C"  bool LayerGridGraph_CheckConnection_m3052316017 (LayerGridGraph_t3415576653 * __this, LevelGridNode_t489265774 * ___node0, int32_t ___dir1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.LayerGridGraph::OnDrawGizmos(System.Boolean)
extern "C"  void LayerGridGraph_OnDrawGizmos_m4077604893 (LayerGridGraph_t3415576653 * __this, bool ___drawNodes0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.LayerGridGraph::SerializeExtraInfo(Pathfinding.Serialization.GraphSerializationContext)
extern "C"  void LayerGridGraph_SerializeExtraInfo_m4109247397 (LayerGridGraph_t3415576653 * __this, GraphSerializationContext_t3256954663 * ___ctx0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.LayerGridGraph::DeserializeExtraInfo(Pathfinding.Serialization.GraphSerializationContext)
extern "C"  void LayerGridGraph_DeserializeExtraInfo_m3349766724 (LayerGridGraph_t3415576653 * __this, GraphSerializationContext_t3256954663 * ___ctx0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.LayerGridGraph::PostDeserialization()
extern "C"  void LayerGridGraph_PostDeserialization_m1367106599 (LayerGridGraph_t3415576653 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.LayerGridGraph::ilo_OnDestroy1(Pathfinding.GridGraph)
extern "C"  void LayerGridGraph_ilo_OnDestroy1_m3071399905 (Il2CppObject * __this /* static, unused */, GridGraph_t2455707914 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.LayerGridGraph::ilo_Invoke2(Pathfinding.GraphNodeDelegateCancelable,Pathfinding.GraphNode)
extern "C"  bool LayerGridGraph_ilo_Invoke2_m2626131182 (Il2CppObject * __this /* static, unused */, GraphNodeDelegateCancelable_t3591372971 * ____this0, GraphNode_t23612370 * ___node1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.LayerGridGraph::ilo_GetBoundsMinMax3(Pathfinding.GridGraph,UnityEngine.Bounds,UnityEngine.Matrix4x4,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C"  void LayerGridGraph_ilo_GetBoundsMinMax3_m2600960525 (Il2CppObject * __this /* static, unused */, GridGraph_t2455707914 * ____this0, Bounds_t2711641849  ___b1, Matrix4x4_t1651859333  ___matrix2, Vector3_t4282066566 * ___min3, Vector3_t4282066566 * ___max4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.IntRect Pathfinding.LayerGridGraph::ilo_Intersection4(Pathfinding.IntRect,Pathfinding.IntRect)
extern "C"  IntRect_t3015058261  LayerGridGraph_ilo_Intersection4_m2276184742 (Il2CppObject * __this /* static, unused */, IntRect_t3015058261  ___a0, IntRect_t3015058261  ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.LayerGridGraph::ilo_set_Walkable5(Pathfinding.GraphNode,System.Boolean)
extern "C"  void LayerGridGraph_ilo_set_Walkable5_m628076011 (Il2CppObject * __this /* static, unused */, GraphNode_t23612370 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.LayerGridGraph::ilo_Apply6(Pathfinding.GraphUpdateObject,Pathfinding.GraphNode)
extern "C"  void LayerGridGraph_ilo_Apply6_m2226821255 (Il2CppObject * __this /* static, unused */, GraphUpdateObject_t430843704 * ____this0, GraphNode_t23612370 * ___node1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.IntRect Pathfinding.LayerGridGraph::ilo_Union7(Pathfinding.IntRect,Pathfinding.IntRect)
extern "C"  IntRect_t3015058261  LayerGridGraph_ilo_Union7_m2094238141 (Il2CppObject * __this /* static, unused */, IntRect_t3015058261  ___a0, IntRect_t3015058261  ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.IntRect Pathfinding.LayerGridGraph::ilo_Expand8(Pathfinding.IntRect&,System.Int32)
extern "C"  IntRect_t3015058261  LayerGridGraph_ilo_Expand8_m365452091 (Il2CppObject * __this /* static, unused */, IntRect_t3015058261 * ____this0, int32_t ___range1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.LayerGridGraph::ilo_get_Walkable9(Pathfinding.GraphNode)
extern "C"  bool LayerGridGraph_ilo_get_Walkable9_m2900640526 (Il2CppObject * __this /* static, unused */, GraphNode_t23612370 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.LayerGridGraph::ilo_get_WalkableErosion10(Pathfinding.LevelGridNode)
extern "C"  bool LayerGridGraph_ilo_get_WalkableErosion10_m2828611893 (Il2CppObject * __this /* static, unused */, LevelGridNode_t489265774 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.LayerGridGraph::ilo_Contains11(Pathfinding.IntRect&,System.Int32,System.Int32)
extern "C"  bool LayerGridGraph_ilo_Contains11_m3061558707 (Il2CppObject * __this /* static, unused */, IntRect_t3015058261 * ____this0, int32_t ___x1, int32_t ___y2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.LayerGridGraph::ilo_SetUpOffsetsAndCosts12(Pathfinding.GridGraph)
extern "C"  void LayerGridGraph_ilo_SetUpOffsetsAndCosts12_m3312194916 (Il2CppObject * __this /* static, unused */, GridGraph_t2455707914 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.LayerGridGraph::ilo_Check13(Pathfinding.GraphCollision,UnityEngine.Vector3)
extern "C"  bool LayerGridGraph_ilo_Check13_m2334962528 (Il2CppObject * __this /* static, unused */, GraphCollision_t2160440954 * ____this0, Vector3_t4282066566  ___position1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.LayerGridGraph::ilo_set_Penalty14(Pathfinding.GraphNode,System.UInt32)
extern "C"  void LayerGridGraph_ilo_set_Penalty14_m3090872268 (Il2CppObject * __this /* static, unused */, GraphNode_t23612370 * ____this0, uint32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.LayerGridGraph::ilo_set_WalkableErosion15(Pathfinding.LevelGridNode,System.Boolean)
extern "C"  void LayerGridGraph_ilo_set_WalkableErosion15_m3086119853 (Il2CppObject * __this /* static, unused */, LevelGridNode_t489265774 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.LayerGridGraph::ilo_set_GraphIndex16(Pathfinding.GraphNode,System.UInt32)
extern "C"  void LayerGridGraph_ilo_set_GraphIndex16_m2045274109 (Il2CppObject * __this /* static, unused */, GraphNode_t23612370 * ____this0, uint32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Pathfinding.LayerGridGraph::ilo_get_Penalty17(Pathfinding.GraphNode)
extern "C"  uint32_t LayerGridGraph_ilo_get_Penalty17_m939712092 (Il2CppObject * __this /* static, unused */, GraphNode_t23612370 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.LayerGridGraph::ilo_set_NodeInGridIndex18(Pathfinding.LevelGridNode,System.Int32)
extern "C"  void LayerGridGraph_ilo_set_NodeInGridIndex18_m2730565627 (Il2CppObject * __this /* static, unused */, LevelGridNode_t489265774 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.LayerGridGraph::ilo_GetConnection19(Pathfinding.LevelGridNode,System.Int32)
extern "C"  bool LayerGridGraph_ilo_GetConnection19_m174261332 (Il2CppObject * __this /* static, unused */, LevelGridNode_t489265774 * ____this0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.LayerGridGraph::ilo_CalculateConnections20(Pathfinding.LayerGridGraph,Pathfinding.GraphNode[],Pathfinding.GraphNode,System.Int32,System.Int32,System.Int32)
extern "C"  void LayerGridGraph_ilo_CalculateConnections20_m1340985860 (Il2CppObject * __this /* static, unused */, LayerGridGraph_t3415576653 * ____this0, GraphNodeU5BU5D_t927449255* ___nodes1, GraphNode_t23612370 * ___node2, int32_t ___x3, int32_t ___z4, int32_t ___layerIndex5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.LayerGridGraph::ilo_ResetAllGridConnections21(Pathfinding.LevelGridNode)
extern "C"  void LayerGridGraph_ilo_ResetAllGridConnections21_m2923640751 (Il2CppObject * __this /* static, unused */, LevelGridNode_t489265774 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pathfinding.LayerGridGraph::ilo_get_NodeInGridIndex22(Pathfinding.LevelGridNode)
extern "C"  int32_t LayerGridGraph_ilo_get_NodeInGridIndex22_m3936594493 (Il2CppObject * __this /* static, unused */, LevelGridNode_t489265774 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Pathfinding.LayerGridGraph::ilo_op_Explicit23(Pathfinding.Int3)
extern "C"  Vector3_t4282066566  LayerGridGraph_ilo_op_Explicit23_m3532659600 (Il2CppObject * __this /* static, unused */, Int3_t1974045594  ___ob0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.LevelGridNode Pathfinding.LayerGridGraph::ilo_GetNearestNode24(Pathfinding.LayerGridGraph,UnityEngine.Vector3,System.Int32,System.Int32,Pathfinding.NNConstraint)
extern "C"  LevelGridNode_t489265774 * LayerGridGraph_ilo_GetNearestNode24_m2521260499 (Il2CppObject * __this /* static, unused */, LayerGridGraph_t3415576653 * ____this0, Vector3_t4282066566  ___position1, int32_t ___x2, int32_t ___z3, NNConstraint_t758567699 * ___constraint4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.NNConstraint Pathfinding.LayerGridGraph::ilo_get_None25()
extern "C"  NNConstraint_t758567699 * LayerGridGraph_ilo_get_None25_m3746684439 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.NNInfo Pathfinding.LayerGridGraph::ilo_GetNearest26(Pathfinding.NavGraph,UnityEngine.Vector3,Pathfinding.NNConstraint)
extern "C"  NNInfo_t1570625892  LayerGridGraph_ilo_GetNearest26_m492464435 (Il2CppObject * __this /* static, unused */, NavGraph_t1254319713 * ____this0, Vector3_t4282066566  ___position1, NNConstraint_t758567699 * ___constraint2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.LayerGridGraph::ilo_CheckConnection27(Pathfinding.LayerGridGraph,Pathfinding.LevelGridNode,System.Int32)
extern "C"  bool LayerGridGraph_ilo_CheckConnection27_m3418992178 (Il2CppObject * __this /* static, unused */, LayerGridGraph_t3415576653 * ____this0, LevelGridNode_t489265774 * ___node1, int32_t ___dir2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pathfinding.LayerGridGraph::ilo_GetConnectionValue28(Pathfinding.LevelGridNode,System.Int32)
extern "C"  int32_t LayerGridGraph_ilo_GetConnectionValue28_m583117973 (Il2CppObject * __this /* static, unused */, LevelGridNode_t489265774 * ____this0, int32_t ___dir1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Int3 Pathfinding.LayerGridGraph::ilo_op_Explicit29(UnityEngine.Vector3)
extern "C"  Int3_t1974045594  LayerGridGraph_ilo_op_Explicit29_m1985102122 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___ob0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.LayerGridGraph::ilo_OnDrawGizmos30(Pathfinding.GridGraph,System.Boolean)
extern "C"  void LayerGridGraph_ilo_OnDrawGizmos30_m1592627199 (Il2CppObject * __this /* static, unused */, GridGraph_t2455707914 * ____this0, bool ___drawNodes1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.PathHandler Pathfinding.LayerGridGraph::ilo_get_debugPathData31(AstarPath)
extern "C"  PathHandler_t918952263 * LayerGridGraph_ilo_get_debugPathData31_m1449255510 (Il2CppObject * __this /* static, unused */, AstarPath_t4090270936 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.LayerGridGraph::ilo_InSearchTree32(Pathfinding.GraphNode,Pathfinding.Path)
extern "C"  bool LayerGridGraph_ilo_InSearchTree32_m1831345766 (Il2CppObject * __this /* static, unused */, GraphNode_t23612370 * ___node0, Path_t1974241691 * ___path1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.LayerGridGraph::ilo_DeserializeNode33(Pathfinding.LevelGridNode,Pathfinding.Serialization.GraphSerializationContext)
extern "C"  void LayerGridGraph_ilo_DeserializeNode33_m12818577 (Il2CppObject * __this /* static, unused */, LevelGridNode_t489265774 * ____this0, GraphSerializationContext_t3256954663 * ___ctx1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.LayerGridGraph::ilo_GenerateMatrix34(Pathfinding.GridGraph)
extern "C"  void LayerGridGraph_ilo_GenerateMatrix34_m1459740608 (Il2CppObject * __this /* static, unused */, GridGraph_t2455707914 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
