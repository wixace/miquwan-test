﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VersionTagGenerated
struct VersionTagGenerated_t4000816109;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void VersionTagGenerated::.ctor()
extern "C"  void VersionTagGenerated__ctor_m2835517982 (VersionTagGenerated_t4000816109 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VersionTagGenerated::VersionTag_VersionTag1(JSVCall,System.Int32)
extern "C"  bool VersionTagGenerated_VersionTag_VersionTag1_m1310967188 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VersionTagGenerated::VersionTag_type(JSVCall)
extern "C"  void VersionTagGenerated_VersionTag_type_m2234135764 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VersionTagGenerated::VersionTag_userType(JSVCall)
extern "C"  void VersionTagGenerated_VersionTag_userType_m3140179657 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VersionTagGenerated::__Register()
extern "C"  void VersionTagGenerated___Register_m1687915881 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 VersionTagGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t VersionTagGenerated_ilo_getObject1_m3036292568 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VersionTagGenerated::ilo_addJSCSRel2(System.Int32,System.Object)
extern "C"  void VersionTagGenerated_ilo_addJSCSRel2_m1261164123 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VersionTagGenerated::ilo_setEnum3(System.Int32,System.Int32)
extern "C"  void VersionTagGenerated_ilo_setEnum3_m2508895869 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 VersionTagGenerated::ilo_getEnum4(System.Int32)
extern "C"  int32_t VersionTagGenerated_ilo_getEnum4_m2835993497 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
