﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GestureRecognizerTS`1<System.Object>
struct GestureRecognizerTS_1_t1264015277;
// GestureRecognizerTS`1/GestureEventHandler<System.Object>
struct GestureEventHandler_t2002909991;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t1244034627;
// FingerGestures/IFingerList
struct IFingerList_t1026994100;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.Type
struct Type_t;
// Gesture
struct Gesture_t1589572905;
// FingerClusterManager/Cluster
struct Cluster_t3870231303;
// GestureRecognizer
struct GestureRecognizer_t3512875949;
// FingerGestures/Finger
struct Finger_t182428197;
// FingerGestures
struct FingerGestures_t2907604723;
// FingerClusterManager
struct FingerClusterManager_t3376029756;
// System.Collections.Generic.List`1<FingerClusterManager/Cluster>
struct List_1_t943449559;
// FingerGestures/FingerList
struct FingerList_t1886137443;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_Gesture1589572905.h"
#include "AssemblyU2DCSharp_FingerClusterManager_Cluster3870231303.h"
#include "AssemblyU2DCSharp_GestureRecognizer3512875949.h"
#include "AssemblyU2DCSharp_FingerClusterManager3376029756.h"
#include "AssemblyU2DCSharp_FingerGestures_FingerList1886137443.h"
#include "AssemblyU2DCSharp_FingerGestures_Finger182428197.h"

// System.Void GestureRecognizerTS`1<System.Object>::.ctor()
extern "C"  void GestureRecognizerTS_1__ctor_m2148280188_gshared (GestureRecognizerTS_1_t1264015277 * __this, const MethodInfo* method);
#define GestureRecognizerTS_1__ctor_m2148280188(__this, method) ((  void (*) (GestureRecognizerTS_1_t1264015277 *, const MethodInfo*))GestureRecognizerTS_1__ctor_m2148280188_gshared)(__this, method)
// System.Void GestureRecognizerTS`1<System.Object>::.cctor()
extern "C"  void GestureRecognizerTS_1__cctor_m1690080177_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define GestureRecognizerTS_1__cctor_m1690080177(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))GestureRecognizerTS_1__cctor_m1690080177_gshared)(__this /* static, unused */, method)
// System.Void GestureRecognizerTS`1<System.Object>::add_OnGesture(GestureRecognizerTS`1/GestureEventHandler<T>)
extern "C"  void GestureRecognizerTS_1_add_OnGesture_m405556423_gshared (GestureRecognizerTS_1_t1264015277 * __this, GestureEventHandler_t2002909991 * ___value0, const MethodInfo* method);
#define GestureRecognizerTS_1_add_OnGesture_m405556423(__this, ___value0, method) ((  void (*) (GestureRecognizerTS_1_t1264015277 *, GestureEventHandler_t2002909991 *, const MethodInfo*))GestureRecognizerTS_1_add_OnGesture_m405556423_gshared)(__this, ___value0, method)
// System.Void GestureRecognizerTS`1<System.Object>::remove_OnGesture(GestureRecognizerTS`1/GestureEventHandler<T>)
extern "C"  void GestureRecognizerTS_1_remove_OnGesture_m3869451928_gshared (GestureRecognizerTS_1_t1264015277 * __this, GestureEventHandler_t2002909991 * ___value0, const MethodInfo* method);
#define GestureRecognizerTS_1_remove_OnGesture_m3869451928(__this, ___value0, method) ((  void (*) (GestureRecognizerTS_1_t1264015277 *, GestureEventHandler_t2002909991 *, const MethodInfo*))GestureRecognizerTS_1_remove_OnGesture_m3869451928_gshared)(__this, ___value0, method)
// System.Void GestureRecognizerTS`1<System.Object>::Start()
extern "C"  void GestureRecognizerTS_1_Start_m1095417980_gshared (GestureRecognizerTS_1_t1264015277 * __this, const MethodInfo* method);
#define GestureRecognizerTS_1_Start_m1095417980(__this, method) ((  void (*) (GestureRecognizerTS_1_t1264015277 *, const MethodInfo*))GestureRecognizerTS_1_Start_m1095417980_gshared)(__this, method)
// System.Void GestureRecognizerTS`1<System.Object>::OnEnable()
extern "C"  void GestureRecognizerTS_1_OnEnable_m2178981514_gshared (GestureRecognizerTS_1_t1264015277 * __this, const MethodInfo* method);
#define GestureRecognizerTS_1_OnEnable_m2178981514(__this, method) ((  void (*) (GestureRecognizerTS_1_t1264015277 *, const MethodInfo*))GestureRecognizerTS_1_OnEnable_m2178981514_gshared)(__this, method)
// System.Void GestureRecognizerTS`1<System.Object>::InitGestures()
extern "C"  void GestureRecognizerTS_1_InitGestures_m4172676674_gshared (GestureRecognizerTS_1_t1264015277 * __this, const MethodInfo* method);
#define GestureRecognizerTS_1_InitGestures_m4172676674(__this, method) ((  void (*) (GestureRecognizerTS_1_t1264015277 *, const MethodInfo*))GestureRecognizerTS_1_InitGestures_m4172676674_gshared)(__this, method)
// T GestureRecognizerTS`1<System.Object>::AddGesture()
extern "C"  Il2CppObject * GestureRecognizerTS_1_AddGesture_m4018306607_gshared (GestureRecognizerTS_1_t1264015277 * __this, const MethodInfo* method);
#define GestureRecognizerTS_1_AddGesture_m4018306607(__this, method) ((  Il2CppObject * (*) (GestureRecognizerTS_1_t1264015277 *, const MethodInfo*))GestureRecognizerTS_1_AddGesture_m4018306607_gshared)(__this, method)
// System.Collections.Generic.List`1<T> GestureRecognizerTS`1<System.Object>::get_Gestures()
extern "C"  List_1_t1244034627 * GestureRecognizerTS_1_get_Gestures_m3071766576_gshared (GestureRecognizerTS_1_t1264015277 * __this, const MethodInfo* method);
#define GestureRecognizerTS_1_get_Gestures_m3071766576(__this, method) ((  List_1_t1244034627 * (*) (GestureRecognizerTS_1_t1264015277 *, const MethodInfo*))GestureRecognizerTS_1_get_Gestures_m3071766576_gshared)(__this, method)
// System.Boolean GestureRecognizerTS`1<System.Object>::CanBegin(T,FingerGestures/IFingerList)
extern "C"  bool GestureRecognizerTS_1_CanBegin_m1069325673_gshared (GestureRecognizerTS_1_t1264015277 * __this, Il2CppObject * ___gesture0, Il2CppObject * ___touches1, const MethodInfo* method);
#define GestureRecognizerTS_1_CanBegin_m1069325673(__this, ___gesture0, ___touches1, method) ((  bool (*) (GestureRecognizerTS_1_t1264015277 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))GestureRecognizerTS_1_CanBegin_m1069325673_gshared)(__this, ___gesture0, ___touches1, method)
// UnityEngine.GameObject GestureRecognizerTS`1<System.Object>::GetDefaultSelectionForSendMessage(T)
extern "C"  GameObject_t3674682005 * GestureRecognizerTS_1_GetDefaultSelectionForSendMessage_m1693692834_gshared (GestureRecognizerTS_1_t1264015277 * __this, Il2CppObject * ___gesture0, const MethodInfo* method);
#define GestureRecognizerTS_1_GetDefaultSelectionForSendMessage_m1693692834(__this, ___gesture0, method) ((  GameObject_t3674682005 * (*) (GestureRecognizerTS_1_t1264015277 *, Il2CppObject *, const MethodInfo*))GestureRecognizerTS_1_GetDefaultSelectionForSendMessage_m1693692834_gshared)(__this, ___gesture0, method)
// T GestureRecognizerTS`1<System.Object>::CreateGesture()
extern "C"  Il2CppObject * GestureRecognizerTS_1_CreateGesture_m3911611112_gshared (GestureRecognizerTS_1_t1264015277 * __this, const MethodInfo* method);
#define GestureRecognizerTS_1_CreateGesture_m3911611112(__this, method) ((  Il2CppObject * (*) (GestureRecognizerTS_1_t1264015277 *, const MethodInfo*))GestureRecognizerTS_1_CreateGesture_m3911611112_gshared)(__this, method)
// System.Type GestureRecognizerTS`1<System.Object>::GetGestureType()
extern "C"  Type_t * GestureRecognizerTS_1_GetGestureType_m1995353295_gshared (GestureRecognizerTS_1_t1264015277 * __this, const MethodInfo* method);
#define GestureRecognizerTS_1_GetGestureType_m1995353295(__this, method) ((  Type_t * (*) (GestureRecognizerTS_1_t1264015277 *, const MethodInfo*))GestureRecognizerTS_1_GetGestureType_m1995353295_gshared)(__this, method)
// System.Void GestureRecognizerTS`1<System.Object>::OnStateChanged(Gesture)
extern "C"  void GestureRecognizerTS_1_OnStateChanged_m929390527_gshared (GestureRecognizerTS_1_t1264015277 * __this, Gesture_t1589572905 * ___gesture0, const MethodInfo* method);
#define GestureRecognizerTS_1_OnStateChanged_m929390527(__this, ___gesture0, method) ((  void (*) (GestureRecognizerTS_1_t1264015277 *, Gesture_t1589572905 *, const MethodInfo*))GestureRecognizerTS_1_OnStateChanged_m929390527_gshared)(__this, ___gesture0, method)
// T GestureRecognizerTS`1<System.Object>::FindGestureByCluster(FingerClusterManager/Cluster)
extern "C"  Il2CppObject * GestureRecognizerTS_1_FindGestureByCluster_m1791831827_gshared (GestureRecognizerTS_1_t1264015277 * __this, Cluster_t3870231303 * ___cluster0, const MethodInfo* method);
#define GestureRecognizerTS_1_FindGestureByCluster_m1791831827(__this, ___cluster0, method) ((  Il2CppObject * (*) (GestureRecognizerTS_1_t1264015277 *, Cluster_t3870231303 *, const MethodInfo*))GestureRecognizerTS_1_FindGestureByCluster_m1791831827_gshared)(__this, ___cluster0, method)
// T GestureRecognizerTS`1<System.Object>::MatchActiveGestureToCluster(FingerClusterManager/Cluster)
extern "C"  Il2CppObject * GestureRecognizerTS_1_MatchActiveGestureToCluster_m76322325_gshared (GestureRecognizerTS_1_t1264015277 * __this, Cluster_t3870231303 * ___cluster0, const MethodInfo* method);
#define GestureRecognizerTS_1_MatchActiveGestureToCluster_m76322325(__this, ___cluster0, method) ((  Il2CppObject * (*) (GestureRecognizerTS_1_t1264015277 *, Cluster_t3870231303 *, const MethodInfo*))GestureRecognizerTS_1_MatchActiveGestureToCluster_m76322325_gshared)(__this, ___cluster0, method)
// T GestureRecognizerTS`1<System.Object>::FindFreeGesture()
extern "C"  Il2CppObject * GestureRecognizerTS_1_FindFreeGesture_m259388351_gshared (GestureRecognizerTS_1_t1264015277 * __this, const MethodInfo* method);
#define GestureRecognizerTS_1_FindFreeGesture_m259388351(__this, method) ((  Il2CppObject * (*) (GestureRecognizerTS_1_t1264015277 *, const MethodInfo*))GestureRecognizerTS_1_FindFreeGesture_m259388351_gshared)(__this, method)
// System.Void GestureRecognizerTS`1<System.Object>::Reset(T)
extern "C"  void GestureRecognizerTS_1_Reset_m2226042965_gshared (GestureRecognizerTS_1_t1264015277 * __this, Il2CppObject * ___gesture0, const MethodInfo* method);
#define GestureRecognizerTS_1_Reset_m2226042965(__this, ___gesture0, method) ((  void (*) (GestureRecognizerTS_1_t1264015277 *, Il2CppObject *, const MethodInfo*))GestureRecognizerTS_1_Reset_m2226042965_gshared)(__this, ___gesture0, method)
// System.Void GestureRecognizerTS`1<System.Object>::Update()
extern "C"  void GestureRecognizerTS_1_Update_m3899038481_gshared (GestureRecognizerTS_1_t1264015277 * __this, const MethodInfo* method);
#define GestureRecognizerTS_1_Update_m3899038481(__this, method) ((  void (*) (GestureRecognizerTS_1_t1264015277 *, const MethodInfo*))GestureRecognizerTS_1_Update_m3899038481_gshared)(__this, method)
// System.Void GestureRecognizerTS`1<System.Object>::UpdateExclusive()
extern "C"  void GestureRecognizerTS_1_UpdateExclusive_m1010137071_gshared (GestureRecognizerTS_1_t1264015277 * __this, const MethodInfo* method);
#define GestureRecognizerTS_1_UpdateExclusive_m1010137071(__this, method) ((  void (*) (GestureRecognizerTS_1_t1264015277 *, const MethodInfo*))GestureRecognizerTS_1_UpdateExclusive_m1010137071_gshared)(__this, method)
// System.Void GestureRecognizerTS`1<System.Object>::UpdatePerFinger()
extern "C"  void GestureRecognizerTS_1_UpdatePerFinger_m1102372407_gshared (GestureRecognizerTS_1_t1264015277 * __this, const MethodInfo* method);
#define GestureRecognizerTS_1_UpdatePerFinger_m1102372407(__this, method) ((  void (*) (GestureRecognizerTS_1_t1264015277 *, const MethodInfo*))GestureRecognizerTS_1_UpdatePerFinger_m1102372407_gshared)(__this, method)
// System.Void GestureRecognizerTS`1<System.Object>::UpdateUsingClusters()
extern "C"  void GestureRecognizerTS_1_UpdateUsingClusters_m1527826414_gshared (GestureRecognizerTS_1_t1264015277 * __this, const MethodInfo* method);
#define GestureRecognizerTS_1_UpdateUsingClusters_m1527826414(__this, method) ((  void (*) (GestureRecognizerTS_1_t1264015277 *, const MethodInfo*))GestureRecognizerTS_1_UpdateUsingClusters_m1527826414_gshared)(__this, method)
// System.Void GestureRecognizerTS`1<System.Object>::ProcessCluster(FingerClusterManager/Cluster)
extern "C"  void GestureRecognizerTS_1_ProcessCluster_m2152044076_gshared (GestureRecognizerTS_1_t1264015277 * __this, Cluster_t3870231303 * ___cluster0, const MethodInfo* method);
#define GestureRecognizerTS_1_ProcessCluster_m2152044076(__this, ___cluster0, method) ((  void (*) (GestureRecognizerTS_1_t1264015277 *, Cluster_t3870231303 *, const MethodInfo*))GestureRecognizerTS_1_ProcessCluster_m2152044076_gshared)(__this, ___cluster0, method)
// System.Void GestureRecognizerTS`1<System.Object>::ReleaseFingers(T)
extern "C"  void GestureRecognizerTS_1_ReleaseFingers_m3672117715_gshared (GestureRecognizerTS_1_t1264015277 * __this, Il2CppObject * ___gesture0, const MethodInfo* method);
#define GestureRecognizerTS_1_ReleaseFingers_m3672117715(__this, ___gesture0, method) ((  void (*) (GestureRecognizerTS_1_t1264015277 *, Il2CppObject *, const MethodInfo*))GestureRecognizerTS_1_ReleaseFingers_m3672117715_gshared)(__this, ___gesture0, method)
// System.Void GestureRecognizerTS`1<System.Object>::Begin(T,System.Int32,FingerGestures/IFingerList)
extern "C"  void GestureRecognizerTS_1_Begin_m618906006_gshared (GestureRecognizerTS_1_t1264015277 * __this, Il2CppObject * ___gesture0, int32_t ___clusterId1, Il2CppObject * ___touches2, const MethodInfo* method);
#define GestureRecognizerTS_1_Begin_m618906006(__this, ___gesture0, ___clusterId1, ___touches2, method) ((  void (*) (GestureRecognizerTS_1_t1264015277 *, Il2CppObject *, int32_t, Il2CppObject *, const MethodInfo*))GestureRecognizerTS_1_Begin_m618906006_gshared)(__this, ___gesture0, ___clusterId1, ___touches2, method)
// FingerGestures/IFingerList GestureRecognizerTS`1<System.Object>::GetTouches(T)
extern "C"  Il2CppObject * GestureRecognizerTS_1_GetTouches_m1971515158_gshared (GestureRecognizerTS_1_t1264015277 * __this, Il2CppObject * ___gesture0, const MethodInfo* method);
#define GestureRecognizerTS_1_GetTouches_m1971515158(__this, ___gesture0, method) ((  Il2CppObject * (*) (GestureRecognizerTS_1_t1264015277 *, Il2CppObject *, const MethodInfo*))GestureRecognizerTS_1_GetTouches_m1971515158_gshared)(__this, ___gesture0, method)
// System.Void GestureRecognizerTS`1<System.Object>::UpdateGesture(T,FingerGestures/IFingerList)
extern "C"  void GestureRecognizerTS_1_UpdateGesture_m4072656334_gshared (GestureRecognizerTS_1_t1264015277 * __this, Il2CppObject * ___gesture0, Il2CppObject * ___touches1, const MethodInfo* method);
#define GestureRecognizerTS_1_UpdateGesture_m4072656334(__this, ___gesture0, ___touches1, method) ((  void (*) (GestureRecognizerTS_1_t1264015277 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))GestureRecognizerTS_1_UpdateGesture_m4072656334_gshared)(__this, ___gesture0, ___touches1, method)
// System.Void GestureRecognizerTS`1<System.Object>::RaiseEvent(T)
extern "C"  void GestureRecognizerTS_1_RaiseEvent_m3101196424_gshared (GestureRecognizerTS_1_t1264015277 * __this, Il2CppObject * ___gesture0, const MethodInfo* method);
#define GestureRecognizerTS_1_RaiseEvent_m3101196424(__this, ___gesture0, method) ((  void (*) (GestureRecognizerTS_1_t1264015277 *, Il2CppObject *, const MethodInfo*))GestureRecognizerTS_1_RaiseEvent_m3101196424_gshared)(__this, ___gesture0, method)
// System.Boolean GestureRecognizerTS`1<System.Object>::<FindFreeGesture>m__361(T)
extern "C"  bool GestureRecognizerTS_1_U3CFindFreeGestureU3Em__361_m2315768481_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___g0, const MethodInfo* method);
#define GestureRecognizerTS_1_U3CFindFreeGestureU3Em__361_m2315768481(__this /* static, unused */, ___g0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))GestureRecognizerTS_1_U3CFindFreeGestureU3Em__361_m2315768481_gshared)(__this /* static, unused */, ___g0, method)
// System.Int32 GestureRecognizerTS`1<System.Object>::ilo_get_RequiredFingerCount1(GestureRecognizer)
extern "C"  int32_t GestureRecognizerTS_1_ilo_get_RequiredFingerCount1_m2202618441_gshared (Il2CppObject * __this /* static, unused */, GestureRecognizer_t3512875949 * ____this0, const MethodInfo* method);
#define GestureRecognizerTS_1_ilo_get_RequiredFingerCount1_m2202618441(__this /* static, unused */, ____this0, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, GestureRecognizer_t3512875949 *, const MethodInfo*))GestureRecognizerTS_1_ilo_get_RequiredFingerCount1_m2202618441_gshared)(__this /* static, unused */, ____this0, method)
// FingerGestures/IFingerList GestureRecognizerTS`1<System.Object>::ilo_get_Touches2()
extern "C"  Il2CppObject * GestureRecognizerTS_1_ilo_get_Touches2_m4216108786_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define GestureRecognizerTS_1_ilo_get_Touches2_m4216108786(__this /* static, unused */, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))GestureRecognizerTS_1_ilo_get_Touches2_m4216108786_gshared)(__this /* static, unused */, method)
// FingerGestures/Finger GestureRecognizerTS`1<System.Object>::ilo_GetFinger3(System.Int32)
extern "C"  Finger_t182428197 * GestureRecognizerTS_1_ilo_GetFinger3_m935976846_gshared (Il2CppObject * __this /* static, unused */, int32_t ___index0, const MethodInfo* method);
#define GestureRecognizerTS_1_ilo_GetFinger3_m935976846(__this /* static, unused */, ___index0, method) ((  Finger_t182428197 * (*) (Il2CppObject * /* static, unused */, int32_t, const MethodInfo*))GestureRecognizerTS_1_ilo_GetFinger3_m935976846_gshared)(__this /* static, unused */, ___index0, method)
// FingerGestures GestureRecognizerTS`1<System.Object>::ilo_get_Instance4()
extern "C"  FingerGestures_t2907604723 * GestureRecognizerTS_1_ilo_get_Instance4_m3398988403_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define GestureRecognizerTS_1_ilo_get_Instance4_m3398988403(__this /* static, unused */, method) ((  FingerGestures_t2907604723 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))GestureRecognizerTS_1_ilo_get_Instance4_m3398988403_gshared)(__this /* static, unused */, method)
// System.Void GestureRecognizerTS`1<System.Object>::ilo_Update5(FingerClusterManager)
extern "C"  void GestureRecognizerTS_1_ilo_Update5_m3295284439_gshared (Il2CppObject * __this /* static, unused */, FingerClusterManager_t3376029756 * ____this0, const MethodInfo* method);
#define GestureRecognizerTS_1_ilo_Update5_m3295284439(__this /* static, unused */, ____this0, method) ((  void (*) (Il2CppObject * /* static, unused */, FingerClusterManager_t3376029756 *, const MethodInfo*))GestureRecognizerTS_1_ilo_Update5_m3295284439_gshared)(__this /* static, unused */, ____this0, method)
// System.Collections.Generic.List`1<FingerClusterManager/Cluster> GestureRecognizerTS`1<System.Object>::ilo_get_Clusters6(FingerClusterManager)
extern "C"  List_1_t943449559 * GestureRecognizerTS_1_ilo_get_Clusters6_m1476424037_gshared (Il2CppObject * __this /* static, unused */, FingerClusterManager_t3376029756 * ____this0, const MethodInfo* method);
#define GestureRecognizerTS_1_ilo_get_Clusters6_m1476424037(__this /* static, unused */, ____this0, method) ((  List_1_t943449559 * (*) (Il2CppObject * /* static, unused */, FingerClusterManager_t3376029756 *, const MethodInfo*))GestureRecognizerTS_1_ilo_get_Clusters6_m1476424037_gshared)(__this /* static, unused */, ____this0, method)
// FingerGestures/Finger GestureRecognizerTS`1<System.Object>::ilo_get_Item7(FingerGestures/FingerList,System.Int32)
extern "C"  Finger_t182428197 * GestureRecognizerTS_1_ilo_get_Item7_m4265974940_gshared (Il2CppObject * __this /* static, unused */, FingerList_t1886137443 * ____this0, int32_t ___index1, const MethodInfo* method);
#define GestureRecognizerTS_1_ilo_get_Item7_m4265974940(__this /* static, unused */, ____this0, ___index1, method) ((  Finger_t182428197 * (*) (Il2CppObject * /* static, unused */, FingerList_t1886137443 *, int32_t, const MethodInfo*))GestureRecognizerTS_1_ilo_get_Item7_m4265974940_gshared)(__this /* static, unused */, ____this0, ___index1, method)
// System.Int32 GestureRecognizerTS`1<System.Object>::ilo_get_Count8(FingerGestures/FingerList)
extern "C"  int32_t GestureRecognizerTS_1_ilo_get_Count8_m2320029748_gshared (Il2CppObject * __this /* static, unused */, FingerList_t1886137443 * ____this0, const MethodInfo* method);
#define GestureRecognizerTS_1_ilo_get_Count8_m2320029748(__this /* static, unused */, ____this0, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, FingerList_t1886137443 *, const MethodInfo*))GestureRecognizerTS_1_ilo_get_Count8_m2320029748_gshared)(__this /* static, unused */, ____this0, method)
// System.Void GestureRecognizerTS`1<System.Object>::ilo_Add9(FingerGestures/FingerList,FingerGestures/Finger)
extern "C"  void GestureRecognizerTS_1_ilo_Add9_m3059082371_gshared (Il2CppObject * __this /* static, unused */, FingerList_t1886137443 * ____this0, Finger_t182428197 * ___touch1, const MethodInfo* method);
#define GestureRecognizerTS_1_ilo_Add9_m3059082371(__this /* static, unused */, ____this0, ___touch1, method) ((  void (*) (Il2CppObject * /* static, unused */, FingerList_t1886137443 *, Finger_t182428197 *, const MethodInfo*))GestureRecognizerTS_1_ilo_Add9_m3059082371_gshared)(__this /* static, unused */, ____this0, ___touch1, method)
// System.Void GestureRecognizerTS`1<System.Object>::ilo_Acquire10(GestureRecognizer,FingerGestures/Finger)
extern "C"  void GestureRecognizerTS_1_ilo_Acquire10_m4079727760_gshared (Il2CppObject * __this /* static, unused */, GestureRecognizer_t3512875949 * ____this0, Finger_t182428197 * ___finger1, const MethodInfo* method);
#define GestureRecognizerTS_1_ilo_Acquire10_m4079727760(__this /* static, unused */, ____this0, ___finger1, method) ((  void (*) (Il2CppObject * /* static, unused */, GestureRecognizer_t3512875949 *, Finger_t182428197 *, const MethodInfo*))GestureRecognizerTS_1_ilo_Acquire10_m4079727760_gshared)(__this /* static, unused */, ____this0, ___finger1, method)
// System.Int32 GestureRecognizerTS`1<System.Object>::ilo_get_Count11(FingerGestures/IFingerList)
extern "C"  int32_t GestureRecognizerTS_1_ilo_get_Count11_m3633344907_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ____this0, const MethodInfo* method);
#define GestureRecognizerTS_1_ilo_get_Count11_m3633344907(__this /* static, unused */, ____this0, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))GestureRecognizerTS_1_ilo_get_Count11_m3633344907_gshared)(__this /* static, unused */, ____this0, method)
// FingerClusterManager/Cluster GestureRecognizerTS`1<System.Object>::ilo_FindClusterById12(FingerClusterManager,System.Int32)
extern "C"  Cluster_t3870231303 * GestureRecognizerTS_1_ilo_FindClusterById12_m3575248174_gshared (Il2CppObject * __this /* static, unused */, FingerClusterManager_t3376029756 * ____this0, int32_t ___clusterId1, const MethodInfo* method);
#define GestureRecognizerTS_1_ilo_FindClusterById12_m3575248174(__this /* static, unused */, ____this0, ___clusterId1, method) ((  Cluster_t3870231303 * (*) (Il2CppObject * /* static, unused */, FingerClusterManager_t3376029756 *, int32_t, const MethodInfo*))GestureRecognizerTS_1_ilo_FindClusterById12_m3575248174_gshared)(__this /* static, unused */, ____this0, ___clusterId1, method)
