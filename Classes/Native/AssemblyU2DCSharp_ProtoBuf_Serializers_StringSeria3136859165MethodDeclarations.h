﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ProtoBuf.Serializers.StringSerializer
struct StringSerializer_t3136859165;
// ProtoBuf.Meta.TypeModel
struct TypeModel_t2730011105;
// System.Type
struct Type_t;
// System.Object
struct Il2CppObject;
// ProtoBuf.ProtoWriter
struct ProtoWriter_t4117914721;
// ProtoBuf.ProtoReader
struct ProtoReader_t3962509489;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ProtoBuf_Meta_TypeModel2730011105.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_ProtoBuf_ProtoWriter4117914721.h"
#include "AssemblyU2DCSharp_ProtoBuf_ProtoReader3962509489.h"

// System.Void ProtoBuf.Serializers.StringSerializer::.ctor(ProtoBuf.Meta.TypeModel)
extern "C"  void StringSerializer__ctor_m91129617 (StringSerializer_t3136859165 * __this, TypeModel_t2730011105 * ___model0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Serializers.StringSerializer::.cctor()
extern "C"  void StringSerializer__cctor_m4055128405 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Serializers.StringSerializer::ProtoBuf.Serializers.IProtoSerializer.get_RequiresOldValue()
extern "C"  bool StringSerializer_ProtoBuf_Serializers_IProtoSerializer_get_RequiresOldValue_m2103786502 (StringSerializer_t3136859165 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Serializers.StringSerializer::ProtoBuf.Serializers.IProtoSerializer.get_ReturnsValue()
extern "C"  bool StringSerializer_ProtoBuf_Serializers_IProtoSerializer_get_ReturnsValue_m1920737692 (StringSerializer_t3136859165 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type ProtoBuf.Serializers.StringSerializer::get_ExpectedType()
extern "C"  Type_t * StringSerializer_get_ExpectedType_m718686253 (StringSerializer_t3136859165 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Serializers.StringSerializer::Write(System.Object,ProtoBuf.ProtoWriter)
extern "C"  void StringSerializer_Write_m1417711823 (StringSerializer_t3136859165 * __this, Il2CppObject * ___value0, ProtoWriter_t4117914721 * ___dest1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProtoBuf.Serializers.StringSerializer::Read(System.Object,ProtoBuf.ProtoReader)
extern "C"  Il2CppObject * StringSerializer_Read_m3535054935 (StringSerializer_t3136859165 * __this, Il2CppObject * ___value0, ProtoReader_t3962509489 * ___source1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
