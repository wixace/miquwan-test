﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<Pathfinding.IntRect>
struct List_1_t88276517;
// System.Collections.Generic.IEnumerable`1<Pathfinding.IntRect>
struct IEnumerable_1_t2021003922;
// System.Collections.Generic.IEnumerator`1<Pathfinding.IntRect>
struct IEnumerator_1_t631956014;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.ICollection`1<Pathfinding.IntRect>
struct ICollection_1_t3909648248;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.IntRect>
struct ReadOnlyCollection_1_t277168501;
// Pathfinding.IntRect[]
struct IntRectU5BU5D_t3425567672;
// System.Predicate`1<Pathfinding.IntRect>
struct Predicate_1_t2626115144;
// System.Collections.Generic.IComparer`1<Pathfinding.IntRect>
struct IComparer_1_t1295105007;
// System.Comparison`1<Pathfinding.IntRect>
struct Comparison_1_t1731419448;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_Pathfinding_IntRect3015058261.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat107949287.h"

// System.Void System.Collections.Generic.List`1<Pathfinding.IntRect>::.ctor()
extern "C"  void List_1__ctor_m2264102591_gshared (List_1_t88276517 * __this, const MethodInfo* method);
#define List_1__ctor_m2264102591(__this, method) ((  void (*) (List_1_t88276517 *, const MethodInfo*))List_1__ctor_m2264102591_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.IntRect>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1__ctor_m4033303648_gshared (List_1_t88276517 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1__ctor_m4033303648(__this, ___collection0, method) ((  void (*) (List_1_t88276517 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m4033303648_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.IntRect>::.ctor(System.Int32)
extern "C"  void List_1__ctor_m566363536_gshared (List_1_t88276517 * __this, int32_t ___capacity0, const MethodInfo* method);
#define List_1__ctor_m566363536(__this, ___capacity0, method) ((  void (*) (List_1_t88276517 *, int32_t, const MethodInfo*))List_1__ctor_m566363536_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.IntRect>::.cctor()
extern "C"  void List_1__cctor_m985607374_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m985607374(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m985607374_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Pathfinding.IntRect>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2734991185_gshared (List_1_t88276517 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2734991185(__this, method) ((  Il2CppObject* (*) (List_1_t88276517 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2734991185_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.IntRect>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m1699129189_gshared (List_1_t88276517 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m1699129189(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t88276517 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m1699129189_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<Pathfinding.IntRect>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m2286433056_gshared (List_1_t88276517 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m2286433056(__this, method) ((  Il2CppObject * (*) (List_1_t88276517 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m2286433056_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Pathfinding.IntRect>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_Add_m4217894481_gshared (List_1_t88276517 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m4217894481(__this, ___item0, method) ((  int32_t (*) (List_1_t88276517 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m4217894481_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<Pathfinding.IntRect>::System.Collections.IList.Contains(System.Object)
extern "C"  bool List_1_System_Collections_IList_Contains_m1415100315_gshared (List_1_t88276517 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m1415100315(__this, ___item0, method) ((  bool (*) (List_1_t88276517 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m1415100315_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<Pathfinding.IntRect>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m775068713_gshared (List_1_t88276517 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m775068713(__this, ___item0, method) ((  int32_t (*) (List_1_t88276517 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m775068713_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.IntRect>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_Insert_m3657732564_gshared (List_1_t88276517 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m3657732564(__this, ___index0, ___item1, method) ((  void (*) (List_1_t88276517 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m3657732564_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.IntRect>::System.Collections.IList.Remove(System.Object)
extern "C"  void List_1_System_Collections_IList_Remove_m992128724_gshared (List_1_t88276517 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m992128724(__this, ___item0, method) ((  void (*) (List_1_t88276517 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m992128724_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<Pathfinding.IntRect>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m138682844_gshared (List_1_t88276517 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m138682844(__this, method) ((  bool (*) (List_1_t88276517 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m138682844_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Pathfinding.IntRect>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool List_1_System_Collections_ICollection_get_IsSynchronized_m2144450017_gshared (List_1_t88276517 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m2144450017(__this, method) ((  bool (*) (List_1_t88276517 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m2144450017_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Pathfinding.IntRect>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m3711652557_gshared (List_1_t88276517 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m3711652557(__this, method) ((  Il2CppObject * (*) (List_1_t88276517 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m3711652557_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Pathfinding.IntRect>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool List_1_System_Collections_IList_get_IsFixedSize_m3535599434_gshared (List_1_t88276517 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m3535599434(__this, method) ((  bool (*) (List_1_t88276517 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m3535599434_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Pathfinding.IntRect>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_IList_get_IsReadOnly_m3893196399_gshared (List_1_t88276517 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m3893196399(__this, method) ((  bool (*) (List_1_t88276517 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m3893196399_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Pathfinding.IntRect>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m3140863252_gshared (List_1_t88276517 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m3140863252(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t88276517 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m3140863252_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.IntRect>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_set_Item_m2411698347_gshared (List_1_t88276517 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m2411698347(__this, ___index0, ___value1, method) ((  void (*) (List_1_t88276517 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m2411698347_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.IntRect>::Add(T)
extern "C"  void List_1_Add_m1805062232_gshared (List_1_t88276517 * __this, IntRect_t3015058261  ___item0, const MethodInfo* method);
#define List_1_Add_m1805062232(__this, ___item0, method) ((  void (*) (List_1_t88276517 *, IntRect_t3015058261 , const MethodInfo*))List_1_Add_m1805062232_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.IntRect>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m3488068635_gshared (List_1_t88276517 * __this, int32_t ___newCount0, const MethodInfo* method);
#define List_1_GrowIfNeeded_m3488068635(__this, ___newCount0, method) ((  void (*) (List_1_t88276517 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m3488068635_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.IntRect>::CheckRange(System.Int32,System.Int32)
extern "C"  void List_1_CheckRange_m2451988716_gshared (List_1_t88276517 * __this, int32_t ___idx0, int32_t ___count1, const MethodInfo* method);
#define List_1_CheckRange_m2451988716(__this, ___idx0, ___count1, method) ((  void (*) (List_1_t88276517 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m2451988716_gshared)(__this, ___idx0, ___count1, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.IntRect>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m2529746713_gshared (List_1_t88276517 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddCollection_m2529746713(__this, ___collection0, method) ((  void (*) (List_1_t88276517 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m2529746713_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.IntRect>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddEnumerable_m1790718937_gshared (List_1_t88276517 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method);
#define List_1_AddEnumerable_m1790718937(__this, ___enumerable0, method) ((  void (*) (List_1_t88276517 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m1790718937_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.IntRect>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m4194826014_gshared (List_1_t88276517 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddRange_m4194826014(__this, ___collection0, method) ((  void (*) (List_1_t88276517 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m4194826014_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<Pathfinding.IntRect>::AsReadOnly()
extern "C"  ReadOnlyCollection_1_t277168501 * List_1_AsReadOnly_m2755844043_gshared (List_1_t88276517 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m2755844043(__this, method) ((  ReadOnlyCollection_1_t277168501 * (*) (List_1_t88276517 *, const MethodInfo*))List_1_AsReadOnly_m2755844043_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Pathfinding.IntRect>::BinarySearch(T)
extern "C"  int32_t List_1_BinarySearch_m613901844_gshared (List_1_t88276517 * __this, IntRect_t3015058261  ___item0, const MethodInfo* method);
#define List_1_BinarySearch_m613901844(__this, ___item0, method) ((  int32_t (*) (List_1_t88276517 *, IntRect_t3015058261 , const MethodInfo*))List_1_BinarySearch_m613901844_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.IntRect>::Clear()
extern "C"  void List_1_Clear_m3965203178_gshared (List_1_t88276517 * __this, const MethodInfo* method);
#define List_1_Clear_m3965203178(__this, method) ((  void (*) (List_1_t88276517 *, const MethodInfo*))List_1_Clear_m3965203178_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Pathfinding.IntRect>::Contains(T)
extern "C"  bool List_1_Contains_m1397539992_gshared (List_1_t88276517 * __this, IntRect_t3015058261  ___item0, const MethodInfo* method);
#define List_1_Contains_m1397539992(__this, ___item0, method) ((  bool (*) (List_1_t88276517 *, IntRect_t3015058261 , const MethodInfo*))List_1_Contains_m1397539992_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.IntRect>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m3598869968_gshared (List_1_t88276517 * __this, IntRectU5BU5D_t3425567672* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_CopyTo_m3598869968(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t88276517 *, IntRectU5BU5D_t3425567672*, int32_t, const MethodInfo*))List_1_CopyTo_m3598869968_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<Pathfinding.IntRect>::Find(System.Predicate`1<T>)
extern "C"  IntRect_t3015058261  List_1_Find_m4007674712_gshared (List_1_t88276517 * __this, Predicate_1_t2626115144 * ___match0, const MethodInfo* method);
#define List_1_Find_m4007674712(__this, ___match0, method) ((  IntRect_t3015058261  (*) (List_1_t88276517 *, Predicate_1_t2626115144 *, const MethodInfo*))List_1_Find_m4007674712_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.IntRect>::CheckMatch(System.Predicate`1<T>)
extern "C"  void List_1_CheckMatch_m1062028051_gshared (Il2CppObject * __this /* static, unused */, Predicate_1_t2626115144 * ___match0, const MethodInfo* method);
#define List_1_CheckMatch_m1062028051(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t2626115144 *, const MethodInfo*))List_1_CheckMatch_m1062028051_gshared)(__this /* static, unused */, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<Pathfinding.IntRect>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C"  int32_t List_1_GetIndex_m2878582328_gshared (List_1_t88276517 * __this, int32_t ___startIndex0, int32_t ___count1, Predicate_1_t2626115144 * ___match2, const MethodInfo* method);
#define List_1_GetIndex_m2878582328(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t88276517 *, int32_t, int32_t, Predicate_1_t2626115144 *, const MethodInfo*))List_1_GetIndex_m2878582328_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Pathfinding.IntRect>::GetEnumerator()
extern "C"  Enumerator_t107949287  List_1_GetEnumerator_m3412831701_gshared (List_1_t88276517 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m3412831701(__this, method) ((  Enumerator_t107949287  (*) (List_1_t88276517 *, const MethodInfo*))List_1_GetEnumerator_m3412831701_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Pathfinding.IntRect>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m2006211156_gshared (List_1_t88276517 * __this, IntRect_t3015058261  ___item0, const MethodInfo* method);
#define List_1_IndexOf_m2006211156(__this, ___item0, method) ((  int32_t (*) (List_1_t88276517 *, IntRect_t3015058261 , const MethodInfo*))List_1_IndexOf_m2006211156_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.IntRect>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m406809639_gshared (List_1_t88276517 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method);
#define List_1_Shift_m406809639(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t88276517 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m406809639_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.IntRect>::CheckIndex(System.Int32)
extern "C"  void List_1_CheckIndex_m3345237152_gshared (List_1_t88276517 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_CheckIndex_m3345237152(__this, ___index0, method) ((  void (*) (List_1_t88276517 *, int32_t, const MethodInfo*))List_1_CheckIndex_m3345237152_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.IntRect>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m3323965511_gshared (List_1_t88276517 * __this, int32_t ___index0, IntRect_t3015058261  ___item1, const MethodInfo* method);
#define List_1_Insert_m3323965511(__this, ___index0, ___item1, method) ((  void (*) (List_1_t88276517 *, int32_t, IntRect_t3015058261 , const MethodInfo*))List_1_Insert_m3323965511_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.IntRect>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_CheckCollection_m661770108_gshared (List_1_t88276517 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_CheckCollection_m661770108(__this, ___collection0, method) ((  void (*) (List_1_t88276517 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m661770108_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<Pathfinding.IntRect>::Remove(T)
extern "C"  bool List_1_Remove_m832162451_gshared (List_1_t88276517 * __this, IntRect_t3015058261  ___item0, const MethodInfo* method);
#define List_1_Remove_m832162451(__this, ___item0, method) ((  bool (*) (List_1_t88276517 *, IntRect_t3015058261 , const MethodInfo*))List_1_Remove_m832162451_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<Pathfinding.IntRect>::RemoveAll(System.Predicate`1<T>)
extern "C"  int32_t List_1_RemoveAll_m3587566359_gshared (List_1_t88276517 * __this, Predicate_1_t2626115144 * ___match0, const MethodInfo* method);
#define List_1_RemoveAll_m3587566359(__this, ___match0, method) ((  int32_t (*) (List_1_t88276517 *, Predicate_1_t2626115144 *, const MethodInfo*))List_1_RemoveAll_m3587566359_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.IntRect>::RemoveAt(System.Int32)
extern "C"  void List_1_RemoveAt_m1197818381_gshared (List_1_t88276517 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_RemoveAt_m1197818381(__this, ___index0, method) ((  void (*) (List_1_t88276517 *, int32_t, const MethodInfo*))List_1_RemoveAt_m1197818381_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.IntRect>::RemoveRange(System.Int32,System.Int32)
extern "C"  void List_1_RemoveRange_m2732418096_gshared (List_1_t88276517 * __this, int32_t ___index0, int32_t ___count1, const MethodInfo* method);
#define List_1_RemoveRange_m2732418096(__this, ___index0, ___count1, method) ((  void (*) (List_1_t88276517 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m2732418096_gshared)(__this, ___index0, ___count1, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.IntRect>::Reverse()
extern "C"  void List_1_Reverse_m2586949439_gshared (List_1_t88276517 * __this, const MethodInfo* method);
#define List_1_Reverse_m2586949439(__this, method) ((  void (*) (List_1_t88276517 *, const MethodInfo*))List_1_Reverse_m2586949439_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.IntRect>::Sort()
extern "C"  void List_1_Sort_m1004792003_gshared (List_1_t88276517 * __this, const MethodInfo* method);
#define List_1_Sort_m1004792003(__this, method) ((  void (*) (List_1_t88276517 *, const MethodInfo*))List_1_Sort_m1004792003_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.IntRect>::Sort(System.Collections.Generic.IComparer`1<T>)
extern "C"  void List_1_Sort_m694015105_gshared (List_1_t88276517 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define List_1_Sort_m694015105(__this, ___comparer0, method) ((  void (*) (List_1_t88276517 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m694015105_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.IntRect>::Sort(System.Comparison`1<T>)
extern "C"  void List_1_Sort_m4292063446_gshared (List_1_t88276517 * __this, Comparison_1_t1731419448 * ___comparison0, const MethodInfo* method);
#define List_1_Sort_m4292063446(__this, ___comparison0, method) ((  void (*) (List_1_t88276517 *, Comparison_1_t1731419448 *, const MethodInfo*))List_1_Sort_m4292063446_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<Pathfinding.IntRect>::ToArray()
extern "C"  IntRectU5BU5D_t3425567672* List_1_ToArray_m130013694_gshared (List_1_t88276517 * __this, const MethodInfo* method);
#define List_1_ToArray_m130013694(__this, method) ((  IntRectU5BU5D_t3425567672* (*) (List_1_t88276517 *, const MethodInfo*))List_1_ToArray_m130013694_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.IntRect>::TrimExcess()
extern "C"  void List_1_TrimExcess_m1592011292_gshared (List_1_t88276517 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m1592011292(__this, method) ((  void (*) (List_1_t88276517 *, const MethodInfo*))List_1_TrimExcess_m1592011292_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Pathfinding.IntRect>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m1721174148_gshared (List_1_t88276517 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m1721174148(__this, method) ((  int32_t (*) (List_1_t88276517 *, const MethodInfo*))List_1_get_Capacity_m1721174148_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.IntRect>::set_Capacity(System.Int32)
extern "C"  void List_1_set_Capacity_m698257453_gshared (List_1_t88276517 * __this, int32_t ___value0, const MethodInfo* method);
#define List_1_set_Capacity_m698257453(__this, ___value0, method) ((  void (*) (List_1_t88276517 *, int32_t, const MethodInfo*))List_1_set_Capacity_m698257453_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<Pathfinding.IntRect>::get_Count()
extern "C"  int32_t List_1_get_Count_m1880311207_gshared (List_1_t88276517 * __this, const MethodInfo* method);
#define List_1_get_Count_m1880311207(__this, method) ((  int32_t (*) (List_1_t88276517 *, const MethodInfo*))List_1_get_Count_m1880311207_gshared)(__this, method)
// T System.Collections.Generic.List`1<Pathfinding.IntRect>::get_Item(System.Int32)
extern "C"  IntRect_t3015058261  List_1_get_Item_m99593655_gshared (List_1_t88276517 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_get_Item_m99593655(__this, ___index0, method) ((  IntRect_t3015058261  (*) (List_1_t88276517 *, int32_t, const MethodInfo*))List_1_get_Item_m99593655_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.IntRect>::set_Item(System.Int32,T)
extern "C"  void List_1_set_Item_m684784478_gshared (List_1_t88276517 * __this, int32_t ___index0, IntRect_t3015058261  ___value1, const MethodInfo* method);
#define List_1_set_Item_m684784478(__this, ___index0, ___value1, method) ((  void (*) (List_1_t88276517 *, int32_t, IntRect_t3015058261 , const MethodInfo*))List_1_set_Item_m684784478_gshared)(__this, ___index0, ___value1, method)
