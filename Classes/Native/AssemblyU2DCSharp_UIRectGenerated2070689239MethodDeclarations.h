﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIRectGenerated
struct UIRectGenerated_t2070689239;
// JSVCall
struct JSVCall_t3708497963;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;
// UnityEngine.Transform
struct Transform_t1659122786;
// UIRect
struct UIRect_t2503437976;
// UnityEngine.Camera
struct Camera_t2727095145;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_UIRect2503437976.h"

// System.Void UIRectGenerated::.ctor()
extern "C"  void UIRectGenerated__ctor_m3461690228 (UIRectGenerated_t2070689239 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIRectGenerated::UIRect_leftAnchor(JSVCall)
extern "C"  void UIRectGenerated_UIRect_leftAnchor_m877915122 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIRectGenerated::UIRect_rightAnchor(JSVCall)
extern "C"  void UIRectGenerated_UIRect_rightAnchor_m4167898413 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIRectGenerated::UIRect_bottomAnchor(JSVCall)
extern "C"  void UIRectGenerated_UIRect_bottomAnchor_m2765349710 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIRectGenerated::UIRect_topAnchor(JSVCall)
extern "C"  void UIRectGenerated_UIRect_topAnchor_m4219624148 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIRectGenerated::UIRect_updateAnchors(JSVCall)
extern "C"  void UIRectGenerated_UIRect_updateAnchors_m1267115081 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIRectGenerated::UIRect_finalAlpha(JSVCall)
extern "C"  void UIRectGenerated_UIRect_finalAlpha_m4110289286 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIRectGenerated::UIRect_cachedGameObject(JSVCall)
extern "C"  void UIRectGenerated_UIRect_cachedGameObject_m1495040219 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIRectGenerated::UIRect_cachedTransform(JSVCall)
extern "C"  void UIRectGenerated_UIRect_cachedTransform_m879234260 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIRectGenerated::UIRect_anchorCamera(JSVCall)
extern "C"  void UIRectGenerated_UIRect_anchorCamera_m882234132 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIRectGenerated::UIRect_isFullyAnchored(JSVCall)
extern "C"  void UIRectGenerated_UIRect_isFullyAnchored_m2899296650 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIRectGenerated::UIRect_isAnchoredHorizontally(JSVCall)
extern "C"  void UIRectGenerated_UIRect_isAnchoredHorizontally_m832253375 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIRectGenerated::UIRect_isAnchoredVertically(JSVCall)
extern "C"  void UIRectGenerated_UIRect_isAnchoredVertically_m2308461997 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIRectGenerated::UIRect_canBeAnchored(JSVCall)
extern "C"  void UIRectGenerated_UIRect_canBeAnchored_m1370815607 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIRectGenerated::UIRect_parent(JSVCall)
extern "C"  void UIRectGenerated_UIRect_parent_m2394743972 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIRectGenerated::UIRect_root(JSVCall)
extern "C"  void UIRectGenerated_UIRect_root_m1711844716 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIRectGenerated::UIRect_isAnchored(JSVCall)
extern "C"  void UIRectGenerated_UIRect_isAnchored_m2338969200 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIRectGenerated::UIRect_alpha(JSVCall)
extern "C"  void UIRectGenerated_UIRect_alpha_m690574432 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIRectGenerated::UIRect_localCorners(JSVCall)
extern "C"  void UIRectGenerated_UIRect_localCorners_m2015711899 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIRectGenerated::UIRect_worldCorners(JSVCall)
extern "C"  void UIRectGenerated_UIRect_worldCorners_m4238958786 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIRectGenerated::UIRect_CalculateFinalAlpha__Int32(JSVCall,System.Int32)
extern "C"  bool UIRectGenerated_UIRect_CalculateFinalAlpha__Int32_m3239549341 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIRectGenerated::UIRect_GetSides__Transform(JSVCall,System.Int32)
extern "C"  bool UIRectGenerated_UIRect_GetSides__Transform_m4233705003 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIRectGenerated::UIRect_Invalidate__Boolean(JSVCall,System.Int32)
extern "C"  bool UIRectGenerated_UIRect_Invalidate__Boolean_m2927394002 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIRectGenerated::UIRect_ParentHasChanged(JSVCall,System.Int32)
extern "C"  bool UIRectGenerated_UIRect_ParentHasChanged_m4025129825 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIRectGenerated::UIRect_ResetAnchors(JSVCall,System.Int32)
extern "C"  bool UIRectGenerated_UIRect_ResetAnchors_m3281780780 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIRectGenerated::UIRect_ResetAndUpdateAnchors(JSVCall,System.Int32)
extern "C"  bool UIRectGenerated_UIRect_ResetAndUpdateAnchors_m2747316402 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIRectGenerated::UIRect_SetAnchor__GameObject__Int32__Int32__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool UIRectGenerated_UIRect_SetAnchor__GameObject__Int32__Int32__Int32__Int32_m328861645 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIRectGenerated::UIRect_SetAnchor__Transform(JSVCall,System.Int32)
extern "C"  bool UIRectGenerated_UIRect_SetAnchor__Transform_m664808690 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIRectGenerated::UIRect_SetAnchor__GameObject(JSVCall,System.Int32)
extern "C"  bool UIRectGenerated_UIRect_SetAnchor__GameObject_m656408909 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIRectGenerated::UIRect_SetRect__Single__Single__Single__Single(JSVCall,System.Int32)
extern "C"  bool UIRectGenerated_UIRect_SetRect__Single__Single__Single__Single_m4078694219 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIRectGenerated::UIRect_Update(JSVCall,System.Int32)
extern "C"  bool UIRectGenerated_UIRect_Update_m1765503590 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIRectGenerated::UIRect_UpdateAnchors(JSVCall,System.Int32)
extern "C"  bool UIRectGenerated_UIRect_UpdateAnchors_m2736377818 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIRectGenerated::__Register()
extern "C"  void UIRectGenerated___Register_m294015443 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIRectGenerated::ilo_setObject1(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UIRectGenerated_ilo_setObject1_m2443481786 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIRectGenerated::ilo_getEnum2(System.Int32)
extern "C"  int32_t UIRectGenerated_ilo_getEnum2_m1170778881 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UIRectGenerated::ilo_get_cachedTransform3(UIRect)
extern "C"  Transform_t1659122786 * UIRectGenerated_ilo_get_cachedTransform3_m3312075967 (Il2CppObject * __this /* static, unused */, UIRect_t2503437976 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Camera UIRectGenerated::ilo_get_anchorCamera4(UIRect)
extern "C"  Camera_t2727095145 * UIRectGenerated_ilo_get_anchorCamera4_m2867868089 (Il2CppObject * __this /* static, unused */, UIRect_t2503437976 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIRectGenerated::ilo_setBooleanS5(System.Int32,System.Boolean)
extern "C"  void UIRectGenerated_ilo_setBooleanS5_m4282977633 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIRectGenerated::ilo_setArrayS6(System.Int32,System.Int32,System.Boolean)
extern "C"  void UIRectGenerated_ilo_setArrayS6_m3714543060 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___count1, bool ___bClear2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UIRectGenerated::ilo_CalculateFinalAlpha7(UIRect,System.Int32)
extern "C"  float UIRectGenerated_ilo_CalculateFinalAlpha7_m591509775 (Il2CppObject * __this /* static, unused */, UIRect_t2503437976 * ____this0, int32_t ___frameID1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UIRectGenerated::ilo_getObject8(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UIRectGenerated_ilo_getObject8_m257787896 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIRectGenerated::ilo_getInt329(System.Int32)
extern "C"  int32_t UIRectGenerated_ilo_getInt329_m2866475039 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UIRectGenerated::ilo_getSingle10(System.Int32)
extern "C"  float UIRectGenerated_ilo_getSingle10_m2921286105 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
