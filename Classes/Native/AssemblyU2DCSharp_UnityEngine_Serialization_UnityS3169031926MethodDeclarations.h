﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_Serialization_UnitySurrogateSelectorGenerated
struct UnityEngine_Serialization_UnitySurrogateSelectorGenerated_t3169031926;
// JSVCall
struct JSVCall_t3708497963;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void UnityEngine_Serialization_UnitySurrogateSelectorGenerated::.ctor()
extern "C"  void UnityEngine_Serialization_UnitySurrogateSelectorGenerated__ctor_m190743349 (UnityEngine_Serialization_UnitySurrogateSelectorGenerated_t3169031926 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Serialization_UnitySurrogateSelectorGenerated::UnitySurrogateSelector_UnitySurrogateSelector1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Serialization_UnitySurrogateSelectorGenerated_UnitySurrogateSelector_UnitySurrogateSelector1_m1380080477 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Serialization_UnitySurrogateSelectorGenerated::UnitySurrogateSelector_ChainSelector__ISurrogateSelector(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Serialization_UnitySurrogateSelectorGenerated_UnitySurrogateSelector_ChainSelector__ISurrogateSelector_m2873348220 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Serialization_UnitySurrogateSelectorGenerated::UnitySurrogateSelector_GetNextSelector(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Serialization_UnitySurrogateSelectorGenerated_UnitySurrogateSelector_GetNextSelector_m2214690166 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Serialization_UnitySurrogateSelectorGenerated::UnitySurrogateSelector_GetSurrogate__Type__StreamingContext__ISurrogateSelector(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Serialization_UnitySurrogateSelectorGenerated_UnitySurrogateSelector_GetSurrogate__Type__StreamingContext__ISurrogateSelector_m2526091051 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Serialization_UnitySurrogateSelectorGenerated::__Register()
extern "C"  void UnityEngine_Serialization_UnitySurrogateSelectorGenerated___Register_m2857218738 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Serialization_UnitySurrogateSelectorGenerated::ilo_attachFinalizerObject1(System.Int32)
extern "C"  bool UnityEngine_Serialization_UnitySurrogateSelectorGenerated_ilo_attachFinalizerObject1_m372104802 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_Serialization_UnitySurrogateSelectorGenerated::ilo_setObject2(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UnityEngine_Serialization_UnitySurrogateSelectorGenerated_ilo_setObject2_m2894676314 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_Serialization_UnitySurrogateSelectorGenerated::ilo_incArgIndex3()
extern "C"  int32_t UnityEngine_Serialization_UnitySurrogateSelectorGenerated_ilo_incArgIndex3_m3644549901 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
