﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.AstarProfiler/ProfilePoint
struct ProfilePoint_t940090916;

#include "codegen/il2cpp-codegen.h"

// System.Void Pathfinding.AstarProfiler/ProfilePoint::.ctor()
extern "C"  void ProfilePoint__ctor_m1488389879 (ProfilePoint_t940090916 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
