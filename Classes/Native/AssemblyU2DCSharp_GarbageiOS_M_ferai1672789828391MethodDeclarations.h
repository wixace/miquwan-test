﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_ferai167
struct M_ferai167_t2789828391;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"

// System.Void GarbageiOS.M_ferai167::.ctor()
extern "C"  void M_ferai167__ctor_m4168784620 (M_ferai167_t2789828391 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_ferai167::M_jasnepooWhoustallwo0(System.String[],System.Int32)
extern "C"  void M_ferai167_M_jasnepooWhoustallwo0_m3678692301 (M_ferai167_t2789828391 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_ferai167::M_borwu1(System.String[],System.Int32)
extern "C"  void M_ferai167_M_borwu1_m4238542287 (M_ferai167_t2789828391 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_ferai167::M_dasxopirDrebai2(System.String[],System.Int32)
extern "C"  void M_ferai167_M_dasxopirDrebai2_m4167212962 (M_ferai167_t2789828391 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_ferai167::M_zeargeGirvoukair3(System.String[],System.Int32)
extern "C"  void M_ferai167_M_zeargeGirvoukair3_m955802845 (M_ferai167_t2789828391 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
