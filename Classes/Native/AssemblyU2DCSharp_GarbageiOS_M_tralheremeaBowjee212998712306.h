﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_tralheremeaBowjee211
struct  M_tralheremeaBowjee211_t2998712306  : public Il2CppObject
{
public:
	// System.Int32 GarbageiOS.M_tralheremeaBowjee211::_balcu
	int32_t ____balcu_0;
	// System.UInt32 GarbageiOS.M_tralheremeaBowjee211::_jenel
	uint32_t ____jenel_1;
	// System.Int32 GarbageiOS.M_tralheremeaBowjee211::_sevekaSoosallje
	int32_t ____sevekaSoosallje_2;
	// System.Int32 GarbageiOS.M_tralheremeaBowjee211::_lalpishere
	int32_t ____lalpishere_3;
	// System.Boolean GarbageiOS.M_tralheremeaBowjee211::_drallsaszoo
	bool ____drallsaszoo_4;
	// System.Single GarbageiOS.M_tralheremeaBowjee211::_zowqalqearQoutrasjem
	float ____zowqalqearQoutrasjem_5;
	// System.Int32 GarbageiOS.M_tralheremeaBowjee211::_kulaNipi
	int32_t ____kulaNipi_6;
	// System.Boolean GarbageiOS.M_tralheremeaBowjee211::_sowall
	bool ____sowall_7;
	// System.Boolean GarbageiOS.M_tralheremeaBowjee211::_cidawgee
	bool ____cidawgee_8;

public:
	inline static int32_t get_offset_of__balcu_0() { return static_cast<int32_t>(offsetof(M_tralheremeaBowjee211_t2998712306, ____balcu_0)); }
	inline int32_t get__balcu_0() const { return ____balcu_0; }
	inline int32_t* get_address_of__balcu_0() { return &____balcu_0; }
	inline void set__balcu_0(int32_t value)
	{
		____balcu_0 = value;
	}

	inline static int32_t get_offset_of__jenel_1() { return static_cast<int32_t>(offsetof(M_tralheremeaBowjee211_t2998712306, ____jenel_1)); }
	inline uint32_t get__jenel_1() const { return ____jenel_1; }
	inline uint32_t* get_address_of__jenel_1() { return &____jenel_1; }
	inline void set__jenel_1(uint32_t value)
	{
		____jenel_1 = value;
	}

	inline static int32_t get_offset_of__sevekaSoosallje_2() { return static_cast<int32_t>(offsetof(M_tralheremeaBowjee211_t2998712306, ____sevekaSoosallje_2)); }
	inline int32_t get__sevekaSoosallje_2() const { return ____sevekaSoosallje_2; }
	inline int32_t* get_address_of__sevekaSoosallje_2() { return &____sevekaSoosallje_2; }
	inline void set__sevekaSoosallje_2(int32_t value)
	{
		____sevekaSoosallje_2 = value;
	}

	inline static int32_t get_offset_of__lalpishere_3() { return static_cast<int32_t>(offsetof(M_tralheremeaBowjee211_t2998712306, ____lalpishere_3)); }
	inline int32_t get__lalpishere_3() const { return ____lalpishere_3; }
	inline int32_t* get_address_of__lalpishere_3() { return &____lalpishere_3; }
	inline void set__lalpishere_3(int32_t value)
	{
		____lalpishere_3 = value;
	}

	inline static int32_t get_offset_of__drallsaszoo_4() { return static_cast<int32_t>(offsetof(M_tralheremeaBowjee211_t2998712306, ____drallsaszoo_4)); }
	inline bool get__drallsaszoo_4() const { return ____drallsaszoo_4; }
	inline bool* get_address_of__drallsaszoo_4() { return &____drallsaszoo_4; }
	inline void set__drallsaszoo_4(bool value)
	{
		____drallsaszoo_4 = value;
	}

	inline static int32_t get_offset_of__zowqalqearQoutrasjem_5() { return static_cast<int32_t>(offsetof(M_tralheremeaBowjee211_t2998712306, ____zowqalqearQoutrasjem_5)); }
	inline float get__zowqalqearQoutrasjem_5() const { return ____zowqalqearQoutrasjem_5; }
	inline float* get_address_of__zowqalqearQoutrasjem_5() { return &____zowqalqearQoutrasjem_5; }
	inline void set__zowqalqearQoutrasjem_5(float value)
	{
		____zowqalqearQoutrasjem_5 = value;
	}

	inline static int32_t get_offset_of__kulaNipi_6() { return static_cast<int32_t>(offsetof(M_tralheremeaBowjee211_t2998712306, ____kulaNipi_6)); }
	inline int32_t get__kulaNipi_6() const { return ____kulaNipi_6; }
	inline int32_t* get_address_of__kulaNipi_6() { return &____kulaNipi_6; }
	inline void set__kulaNipi_6(int32_t value)
	{
		____kulaNipi_6 = value;
	}

	inline static int32_t get_offset_of__sowall_7() { return static_cast<int32_t>(offsetof(M_tralheremeaBowjee211_t2998712306, ____sowall_7)); }
	inline bool get__sowall_7() const { return ____sowall_7; }
	inline bool* get_address_of__sowall_7() { return &____sowall_7; }
	inline void set__sowall_7(bool value)
	{
		____sowall_7 = value;
	}

	inline static int32_t get_offset_of__cidawgee_8() { return static_cast<int32_t>(offsetof(M_tralheremeaBowjee211_t2998712306, ____cidawgee_8)); }
	inline bool get__cidawgee_8() const { return ____cidawgee_8; }
	inline bool* get_address_of__cidawgee_8() { return &____cidawgee_8; }
	inline void set__cidawgee_8(bool value)
	{
		____cidawgee_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
