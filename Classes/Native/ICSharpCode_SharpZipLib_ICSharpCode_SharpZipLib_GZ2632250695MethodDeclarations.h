﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ICSharpCode.SharpZipLib.GZip.GZipOutputStream
struct GZipOutputStream_t2632250695;
// System.IO.Stream
struct Stream_t1561764144;
// System.Byte[]
struct ByteU5BU5D_t4260760469;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IO_Stream1561764144.h"

// System.Void ICSharpCode.SharpZipLib.GZip.GZipOutputStream::.ctor(System.IO.Stream)
extern "C"  void GZipOutputStream__ctor_m497768309 (GZipOutputStream_t2632250695 * __this, Stream_t1561764144 * ___baseOutputStream0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.GZip.GZipOutputStream::.ctor(System.IO.Stream,System.Int32)
extern "C"  void GZipOutputStream__ctor_m960121730 (GZipOutputStream_t2632250695 * __this, Stream_t1561764144 * ___baseOutputStream0, int32_t ___size1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.GZip.GZipOutputStream::Write(System.Byte[],System.Int32,System.Int32)
extern "C"  void GZipOutputStream_Write_m3786020206 (GZipOutputStream_t2632250695 * __this, ByteU5BU5D_t4260760469* ___buffer0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.GZip.GZipOutputStream::Close()
extern "C"  void GZipOutputStream_Close_m2584621556 (GZipOutputStream_t2632250695 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.GZip.GZipOutputStream::Finish()
extern "C"  void GZipOutputStream_Finish_m1047423801 (GZipOutputStream_t2632250695 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.GZip.GZipOutputStream::WriteHeader()
extern "C"  void GZipOutputStream_WriteHeader_m687598088 (GZipOutputStream_t2632250695 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
