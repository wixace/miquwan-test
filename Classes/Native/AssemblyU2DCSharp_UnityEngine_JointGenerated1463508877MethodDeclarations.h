﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_JointGenerated
struct UnityEngine_JointGenerated_t1463508877;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void UnityEngine_JointGenerated::.ctor()
extern "C"  void UnityEngine_JointGenerated__ctor_m1345009582 (UnityEngine_JointGenerated_t1463508877 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_JointGenerated::Joint_Joint1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_JointGenerated_Joint_Joint1_m1473844086 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_JointGenerated::Joint_connectedBody(JSVCall)
extern "C"  void UnityEngine_JointGenerated_Joint_connectedBody_m1733344773 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_JointGenerated::Joint_axis(JSVCall)
extern "C"  void UnityEngine_JointGenerated_Joint_axis_m3353176923 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_JointGenerated::Joint_anchor(JSVCall)
extern "C"  void UnityEngine_JointGenerated_Joint_anchor_m1912932679 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_JointGenerated::Joint_connectedAnchor(JSVCall)
extern "C"  void UnityEngine_JointGenerated_Joint_connectedAnchor_m323241490 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_JointGenerated::Joint_autoConfigureConnectedAnchor(JSVCall)
extern "C"  void UnityEngine_JointGenerated_Joint_autoConfigureConnectedAnchor_m1410785173 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_JointGenerated::Joint_breakForce(JSVCall)
extern "C"  void UnityEngine_JointGenerated_Joint_breakForce_m953269680 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_JointGenerated::Joint_breakTorque(JSVCall)
extern "C"  void UnityEngine_JointGenerated_Joint_breakTorque_m3807660711 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_JointGenerated::Joint_enableCollision(JSVCall)
extern "C"  void UnityEngine_JointGenerated_Joint_enableCollision_m2791895905 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_JointGenerated::Joint_enablePreprocessing(JSVCall)
extern "C"  void UnityEngine_JointGenerated_Joint_enablePreprocessing_m750368413 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_JointGenerated::__Register()
extern "C"  void UnityEngine_JointGenerated___Register_m2076959193 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_JointGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UnityEngine_JointGenerated_ilo_getObject1_m1408042660 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_JointGenerated::ilo_attachFinalizerObject2(System.Int32)
extern "C"  bool UnityEngine_JointGenerated_ilo_attachFinalizerObject2_m3189507890 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_JointGenerated::ilo_addJSCSRel3(System.Int32,System.Object)
extern "C"  void UnityEngine_JointGenerated_ilo_addJSCSRel3_m3780958316 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_JointGenerated::ilo_getObject4(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_JointGenerated_ilo_getObject4_m3704577092 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_JointGenerated::ilo_setVector3S5(System.Int32,UnityEngine.Vector3)
extern "C"  void UnityEngine_JointGenerated_ilo_setVector3S5_m4021222621 (Il2CppObject * __this /* static, unused */, int32_t ___e0, Vector3_t4282066566  ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine_JointGenerated::ilo_getVector3S6(System.Int32)
extern "C"  Vector3_t4282066566  UnityEngine_JointGenerated_ilo_getVector3S6_m2802019857 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_JointGenerated::ilo_setBooleanS7(System.Int32,System.Boolean)
extern "C"  void UnityEngine_JointGenerated_ilo_setBooleanS7_m879961 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine_JointGenerated::ilo_getSingle8(System.Int32)
extern "C"  float UnityEngine_JointGenerated_ilo_getSingle8_m1299473152 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_JointGenerated::ilo_getBooleanS9(System.Int32)
extern "C"  bool UnityEngine_JointGenerated_ilo_getBooleanS9_m2770157158 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
