﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1244034627MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1<Pathfinding.RichFunnel>::.ctor()
#define List_1__ctor_m2918250448(__this, method) ((  void (*) (List_1_t3821711480 *, const MethodInfo*))List_1__ctor_m1239231859_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.RichFunnel>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m1075196591(__this, ___collection0, method) ((  void (*) (List_1_t3821711480 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m1160795371_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.RichFunnel>::.ctor(System.Int32)
#define List_1__ctor_m2453239073(__this, ___capacity0, method) ((  void (*) (List_1_t3821711480 *, int32_t, const MethodInfo*))List_1__ctor_m3643386469_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.RichFunnel>::.cctor()
#define List_1__cctor_m4084321757(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m3826137881_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Pathfinding.RichFunnel>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2078547674(__this, method) ((  Il2CppObject* (*) (List_1_t3821711480 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2808422246_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.RichFunnel>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m2907623284(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t3821711480 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m4034025648_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<Pathfinding.RichFunnel>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m2261348611(__this, method) ((  Il2CppObject * (*) (List_1_t3821711480 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m1841330603_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Pathfinding.RichFunnel>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m704043930(__this, ___item0, method) ((  int32_t (*) (List_1_t3821711480 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m3794749222_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<Pathfinding.RichFunnel>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m1931107430(__this, ___item0, method) ((  bool (*) (List_1_t3821711480 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m2659633254_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<Pathfinding.RichFunnel>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m80458994(__this, ___item0, method) ((  int32_t (*) (List_1_t3821711480 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m3431692926_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.RichFunnel>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m1624857701(__this, ___index0, ___item1, method) ((  void (*) (List_1_t3821711480 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m2067529129_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.RichFunnel>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m1088621731(__this, ___item0, method) ((  void (*) (List_1_t3821711480 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m1644145887_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<Pathfinding.RichFunnel>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m4255286503(__this, method) ((  bool (*) (List_1_t3821711480 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1299706087_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Pathfinding.RichFunnel>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m2824460598(__this, method) ((  bool (*) (List_1_t3821711480 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m3867536694_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Pathfinding.RichFunnel>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m3693788648(__this, method) ((  Il2CppObject * (*) (List_1_t3821711480 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m4244374434_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Pathfinding.RichFunnel>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m2040582229(__this, method) ((  bool (*) (List_1_t3821711480 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m432946261_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Pathfinding.RichFunnel>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m2182402052(__this, method) ((  bool (*) (List_1_t3821711480 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m2961826820_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Pathfinding.RichFunnel>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m3153515887(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t3821711480 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m3985478825_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.RichFunnel>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m3029074684(__this, ___index0, ___value1, method) ((  void (*) (List_1_t3821711480 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m3234554688_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.RichFunnel>::Add(T)
#define List_1_Add_m3963604655(__this, ___item0, method) ((  void (*) (List_1_t3821711480 *, RichFunnel_t2453525928 *, const MethodInfo*))List_1_Add_m933592675_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.RichFunnel>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m3029323626(__this, ___newCount0, method) ((  void (*) (List_1_t3821711480 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m4122600870_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.RichFunnel>::CheckRange(System.Int32,System.Int32)
#define List_1_CheckRange_m3502337597(__this, ___idx0, ___count1, method) ((  void (*) (List_1_t3821711480 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m590371457_gshared)(__this, ___idx0, ___count1, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.RichFunnel>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m1909980776(__this, ___collection0, method) ((  void (*) (List_1_t3821711480 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m2478449828_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.RichFunnel>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m1170953000(__this, ___enumerable0, method) ((  void (*) (List_1_t3821711480 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m1739422052_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.RichFunnel>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m3366470255(__this, ___collection0, method) ((  void (*) (List_1_t3821711480 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m2229151411_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<Pathfinding.RichFunnel>::AsReadOnly()
#define List_1_AsReadOnly_m267588406(__this, method) ((  ReadOnlyCollection_1_t4010603464 * (*) (List_1_t3821711480 *, const MethodInfo*))List_1_AsReadOnly_m769820182_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Pathfinding.RichFunnel>::BinarySearch(T)
#define List_1_BinarySearch_m3317555933(__this, ___item0, method) ((  int32_t (*) (List_1_t3821711480 *, RichFunnel_t2453525928 *, const MethodInfo*))List_1_BinarySearch_m2761349225_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.RichFunnel>::Clear()
#define List_1_Clear_m324383739(__this, method) ((  void (*) (List_1_t3821711480 *, const MethodInfo*))List_1_Clear_m2940332446_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Pathfinding.RichFunnel>::Contains(T)
#define List_1_Contains_m783458541(__this, ___item0, method) ((  bool (*) (List_1_t3821711480 *, RichFunnel_t2453525928 *, const MethodInfo*))List_1_Contains_m4186092781_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.RichFunnel>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m2043087071(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t3821711480 *, RichFunnelU5BU5D_t1110054585*, int32_t, const MethodInfo*))List_1_CopyTo_m3988356635_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<Pathfinding.RichFunnel>::Find(System.Predicate`1<T>)
#define List_1_Find_m4073483399(__this, ___match0, method) ((  RichFunnel_t2453525928 * (*) (List_1_t3821711480 *, Predicate_1_t2064582811 *, const MethodInfo*))List_1_Find_m3379773421_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.RichFunnel>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m467509476(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t2064582811 *, const MethodInfo*))List_1_CheckMatch_m3390394152_gshared)(__this /* static, unused */, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<Pathfinding.RichFunnel>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m792666049(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t3821711480 *, int32_t, int32_t, Predicate_1_t2064582811 *, const MethodInfo*))List_1_GetIndex_m4275988045_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Pathfinding.RichFunnel>::GetEnumerator()
#define List_1_GetEnumerator_m1700766634(__this, method) ((  Enumerator_t3841384250  (*) (List_1_t3821711480 *, const MethodInfo*))List_1_GetEnumerator_m1919240000_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Pathfinding.RichFunnel>::IndexOf(T)
#define List_1_IndexOf_m1174619115(__this, ___item0, method) ((  int32_t (*) (List_1_t3821711480 *, RichFunnel_t2453525928 *, const MethodInfo*))List_1_IndexOf_m2776798407_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.RichFunnel>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m2050199606(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t3821711480 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m3807054194_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.RichFunnel>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m1789454255(__this, ___index0, method) ((  void (*) (List_1_t3821711480 *, int32_t, const MethodInfo*))List_1_CheckIndex_m3734723819_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.RichFunnel>::Insert(System.Int32,T)
#define List_1_Insert_m2701118230(__this, ___index0, ___item1, method) ((  void (*) (List_1_t3821711480 *, int32_t, RichFunnel_t2453525928 *, const MethodInfo*))List_1_Insert_m3427163986_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.RichFunnel>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m2067158795(__this, ___collection0, method) ((  void (*) (List_1_t3821711480 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m2905071175_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<Pathfinding.RichFunnel>::Remove(T)
#define List_1_Remove_m4228167720(__this, ___item0, method) ((  bool (*) (List_1_t3821711480 *, RichFunnel_t2453525928 *, const MethodInfo*))List_1_Remove_m2747911208_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<Pathfinding.RichFunnel>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m3850467694(__this, ___match0, method) ((  int32_t (*) (List_1_t3821711480 *, Predicate_1_t2064582811 *, const MethodInfo*))List_1_RemoveAll_m2933443938_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.RichFunnel>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m574971100(__this, ___index0, method) ((  void (*) (List_1_t3821711480 *, int32_t, const MethodInfo*))List_1_RemoveAt_m2208877785_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.RichFunnel>::RemoveRange(System.Int32,System.Int32)
#define List_1_RemoveRange_m933495039(__this, ___index0, ___count1, method) ((  void (*) (List_1_t3821711480 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m856857915_gshared)(__this, ___index0, ___count1, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.RichFunnel>::Reverse()
#define List_1_Reverse_m4157814800(__this, method) ((  void (*) (List_1_t3821711480 *, const MethodInfo*))List_1_Reverse_m449081940_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.RichFunnel>::Sort()
#define List_1_Sort_m194609554(__this, method) ((  void (*) (List_1_t3821711480 *, const MethodInfo*))List_1_Sort_m1168641486_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.RichFunnel>::Sort(System.Collections.Generic.IComparer`1<T>)
#define List_1_Sort_m3685298322(__this, ___comparer0, method) ((  void (*) (List_1_t3821711480 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m3726677974_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.RichFunnel>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m1524773989(__this, ___comparison0, method) ((  void (*) (List_1_t3821711480 *, Comparison_1_t1169887115 *, const MethodInfo*))List_1_Sort_m4192185249_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<Pathfinding.RichFunnel>::ToArray()
#define List_1_ToArray_m580509673(__this, method) ((  RichFunnelU5BU5D_t1110054585* (*) (List_1_t3821711480 *, const MethodInfo*))List_1_ToArray_m1449333879_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.RichFunnel>::TrimExcess()
#define List_1_TrimExcess_m1278323627(__this, method) ((  void (*) (List_1_t3821711480 *, const MethodInfo*))List_1_TrimExcess_m2451380967_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Pathfinding.RichFunnel>::get_Capacity()
#define List_1_get_Capacity_m700010139(__this, method) ((  int32_t (*) (List_1_t3821711480 *, const MethodInfo*))List_1_get_Capacity_m543520655_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.RichFunnel>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m239512444(__this, ___value0, method) ((  void (*) (List_1_t3821711480 *, int32_t, const MethodInfo*))List_1_set_Capacity_m1332789688_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<Pathfinding.RichFunnel>::get_Count()
#define List_1_get_Count_m1870761712(__this, method) ((  int32_t (*) (List_1_t3821711480 *, const MethodInfo*))List_1_get_Count_m3549598589_gshared)(__this, method)
// T System.Collections.Generic.List`1<Pathfinding.RichFunnel>::get_Item(System.Int32)
#define List_1_get_Item_m2002785730(__this, ___index0, method) ((  RichFunnel_t2453525928 * (*) (List_1_t3821711480 *, int32_t, const MethodInfo*))List_1_get_Item_m404118264_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.RichFunnel>::set_Item(System.Int32,T)
#define List_1_set_Item_m3423968877(__this, ___index0, ___value1, method) ((  void (*) (List_1_t3821711480 *, int32_t, RichFunnel_t2453525928 *, const MethodInfo*))List_1_set_Item_m1074271145_gshared)(__this, ___index0, ___value1, method)
