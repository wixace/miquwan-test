﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1263707397MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.ClipperLib.OutRec>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m1507665973(__this, ___l0, method) ((  void (*) (Enumerator_t338696310 *, List_1_t319023540 *, const MethodInfo*))Enumerator__ctor_m1029849669_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.ClipperLib.OutRec>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2230116093(__this, method) ((  void (*) (Enumerator_t338696310 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m771996397_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Pathfinding.ClipperLib.OutRec>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m662075497(__this, method) ((  Il2CppObject * (*) (Enumerator_t338696310 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.ClipperLib.OutRec>::Dispose()
#define Enumerator_Dispose_m360403802(__this, method) ((  void (*) (Enumerator_t338696310 *, const MethodInfo*))Enumerator_Dispose_m2904289642_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.ClipperLib.OutRec>::VerifyState()
#define Enumerator_VerifyState_m3754022291(__this, method) ((  void (*) (Enumerator_t338696310 *, const MethodInfo*))Enumerator_VerifyState_m1522854819_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Pathfinding.ClipperLib.OutRec>::MoveNext()
#define Enumerator_MoveNext_m3537270924(__this, method) ((  bool (*) (Enumerator_t338696310 *, const MethodInfo*))Enumerator_MoveNext_m4284703760_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Pathfinding.ClipperLib.OutRec>::get_Current()
#define Enumerator_get_Current_m972253188(__this, method) ((  OutRec_t3245805284 * (*) (Enumerator_t338696310 *, const MethodInfo*))Enumerator_get_Current_m396252160_gshared)(__this, method)
