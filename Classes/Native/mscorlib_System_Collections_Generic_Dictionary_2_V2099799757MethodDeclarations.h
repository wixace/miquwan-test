﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>
struct Dictionary_2_t4167966349;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V2099799757.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.UInt32,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m781792443_gshared (Enumerator_t2099799757 * __this, Dictionary_2_t4167966349 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m781792443(__this, ___host0, method) ((  void (*) (Enumerator_t2099799757 *, Dictionary_2_t4167966349 *, const MethodInfo*))Enumerator__ctor_m781792443_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.UInt32,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1030490438_gshared (Enumerator_t2099799757 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1030490438(__this, method) ((  Il2CppObject * (*) (Enumerator_t2099799757 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1030490438_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.UInt32,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3456531738_gshared (Enumerator_t2099799757 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m3456531738(__this, method) ((  void (*) (Enumerator_t2099799757 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3456531738_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.UInt32,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m129888285_gshared (Enumerator_t2099799757 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m129888285(__this, method) ((  void (*) (Enumerator_t2099799757 *, const MethodInfo*))Enumerator_Dispose_m129888285_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.UInt32,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m53825798_gshared (Enumerator_t2099799757 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m53825798(__this, method) ((  bool (*) (Enumerator_t2099799757 *, const MethodInfo*))Enumerator_MoveNext_m53825798_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.UInt32,System.Object>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m1200354236_gshared (Enumerator_t2099799757 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m1200354236(__this, method) ((  Il2CppObject * (*) (Enumerator_t2099799757 *, const MethodInfo*))Enumerator_get_Current_m1200354236_gshared)(__this, method)
