﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Schema.JsonSchemaNode
struct JsonSchemaNode_t2115227093;
// Newtonsoft.Json.Schema.JsonSchema
struct JsonSchema_t460567603;
// System.String
struct String_t;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Newtonsoft.Json.Schema.JsonSchema>
struct ReadOnlyCollection_1_t2017645139;
// System.Collections.Generic.Dictionary`2<System.String,Newtonsoft.Json.Schema.JsonSchemaNode>
struct Dictionary_2_t2935645463;
// System.Collections.Generic.List`1<Newtonsoft.Json.Schema.JsonSchemaNode>
struct List_1_t3483412645;
// System.Collections.Generic.IEnumerable`1<Newtonsoft.Json.Schema.JsonSchema>
struct IEnumerable_1_t3761480560;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Schema_JsonSchema460567603.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Schema_JsonSchem2115227093.h"
#include "mscorlib_System_String7231557.h"

// System.Void Newtonsoft.Json.Schema.JsonSchemaNode::.ctor(Newtonsoft.Json.Schema.JsonSchema)
extern "C"  void JsonSchemaNode__ctor_m1278434601 (JsonSchemaNode_t2115227093 * __this, JsonSchema_t460567603 * ___schema0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchemaNode::.ctor(Newtonsoft.Json.Schema.JsonSchemaNode,Newtonsoft.Json.Schema.JsonSchema)
extern "C"  void JsonSchemaNode__ctor_m3910165202 (JsonSchemaNode_t2115227093 * __this, JsonSchemaNode_t2115227093 * ___source0, JsonSchema_t460567603 * ___schema1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Schema.JsonSchemaNode::get_Id()
extern "C"  String_t* JsonSchemaNode_get_Id_m3812282813 (JsonSchemaNode_t2115227093 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchemaNode::set_Id(System.String)
extern "C"  void JsonSchemaNode_set_Id_m3118735054 (JsonSchemaNode_t2115227093 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Newtonsoft.Json.Schema.JsonSchema> Newtonsoft.Json.Schema.JsonSchemaNode::get_Schemas()
extern "C"  ReadOnlyCollection_1_t2017645139 * JsonSchemaNode_get_Schemas_m2927624032 (JsonSchemaNode_t2115227093 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchemaNode::set_Schemas(System.Collections.ObjectModel.ReadOnlyCollection`1<Newtonsoft.Json.Schema.JsonSchema>)
extern "C"  void JsonSchemaNode_set_Schemas_m817478255 (JsonSchemaNode_t2115227093 * __this, ReadOnlyCollection_1_t2017645139 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,Newtonsoft.Json.Schema.JsonSchemaNode> Newtonsoft.Json.Schema.JsonSchemaNode::get_Properties()
extern "C"  Dictionary_2_t2935645463 * JsonSchemaNode_get_Properties_m3675965143 (JsonSchemaNode_t2115227093 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchemaNode::set_Properties(System.Collections.Generic.Dictionary`2<System.String,Newtonsoft.Json.Schema.JsonSchemaNode>)
extern "C"  void JsonSchemaNode_set_Properties_m3530988656 (JsonSchemaNode_t2115227093 * __this, Dictionary_2_t2935645463 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,Newtonsoft.Json.Schema.JsonSchemaNode> Newtonsoft.Json.Schema.JsonSchemaNode::get_PatternProperties()
extern "C"  Dictionary_2_t2935645463 * JsonSchemaNode_get_PatternProperties_m2481028705 (JsonSchemaNode_t2115227093 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchemaNode::set_PatternProperties(System.Collections.Generic.Dictionary`2<System.String,Newtonsoft.Json.Schema.JsonSchemaNode>)
extern "C"  void JsonSchemaNode_set_PatternProperties_m3707332208 (JsonSchemaNode_t2115227093 * __this, Dictionary_2_t2935645463 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<Newtonsoft.Json.Schema.JsonSchemaNode> Newtonsoft.Json.Schema.JsonSchemaNode::get_Items()
extern "C"  List_1_t3483412645 * JsonSchemaNode_get_Items_m1136502209 (JsonSchemaNode_t2115227093 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchemaNode::set_Items(System.Collections.Generic.List`1<Newtonsoft.Json.Schema.JsonSchemaNode>)
extern "C"  void JsonSchemaNode_set_Items_m469361424 (JsonSchemaNode_t2115227093 * __this, List_1_t3483412645 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Schema.JsonSchemaNode Newtonsoft.Json.Schema.JsonSchemaNode::get_AdditionalProperties()
extern "C"  JsonSchemaNode_t2115227093 * JsonSchemaNode_get_AdditionalProperties_m2752292559 (JsonSchemaNode_t2115227093 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchemaNode::set_AdditionalProperties(Newtonsoft.Json.Schema.JsonSchemaNode)
extern "C"  void JsonSchemaNode_set_AdditionalProperties_m3153307772 (JsonSchemaNode_t2115227093 * __this, JsonSchemaNode_t2115227093 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Schema.JsonSchemaNode Newtonsoft.Json.Schema.JsonSchemaNode::Combine(Newtonsoft.Json.Schema.JsonSchema)
extern "C"  JsonSchemaNode_t2115227093 * JsonSchemaNode_Combine_m293848348 (JsonSchemaNode_t2115227093 * __this, JsonSchema_t460567603 * ___schema0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Schema.JsonSchemaNode::GetId(System.Collections.Generic.IEnumerable`1<Newtonsoft.Json.Schema.JsonSchema>)
extern "C"  String_t* JsonSchemaNode_GetId_m3957288242 (Il2CppObject * __this /* static, unused */, Il2CppObject* ___schemata0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Schema.JsonSchemaNode::<GetId>m__37C(Newtonsoft.Json.Schema.JsonSchema)
extern "C"  String_t* JsonSchemaNode_U3CGetIdU3Em__37C_m3933912877 (Il2CppObject * __this /* static, unused */, JsonSchema_t460567603 * ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Schema.JsonSchemaNode::<GetId>m__37D(System.String)
extern "C"  String_t* JsonSchemaNode_U3CGetIdU3Em__37D_m1712919805 (Il2CppObject * __this /* static, unused */, String_t* ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
