﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HttpsWebRequest
struct HttpsWebRequest_t3736652742;
// System.Object
struct Il2CppObject;
// System.Security.Cryptography.X509Certificates.X509Certificate
struct X509Certificate_t3076817455;
// System.Security.Cryptography.X509Certificates.X509Chain
struct X509Chain_t1111884825;
// System.Net.HttpWebResponse
struct HttpWebResponse_t3793423559;
// System.String
struct String_t;
// System.Collections.Generic.IDictionary`2<System.String,System.String>
struct IDictionary_2_t405523272;
// System.Text.Encoding
struct Encoding_t2012439129;
// HttpsData
struct HttpsData_t2200837909;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Security_Cryptography_X509Certific3076817455.h"
#include "System_System_Security_Cryptography_X509Certificat1111884825.h"
#include "System_System_Net_Security_SslPolicyErrors3099591579.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Text_Encoding2012439129.h"
#include "AssemblyU2DCSharp_HttpsData2200837909.h"

// System.Void HttpsWebRequest::.ctor()
extern "C"  void HttpsWebRequest__ctor_m804313701 (HttpsWebRequest_t3736652742 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HttpsWebRequest::.cctor()
extern "C"  void HttpsWebRequest__cctor_m2976792040 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HttpsWebRequest::CheckValidationResult(System.Object,System.Security.Cryptography.X509Certificates.X509Certificate,System.Security.Cryptography.X509Certificates.X509Chain,System.Net.Security.SslPolicyErrors)
extern "C"  bool HttpsWebRequest_CheckValidationResult_m1053476062 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___sender0, X509Certificate_t3076817455 * ___certificate1, X509Chain_t1111884825 * ___chain2, int32_t ___errors3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.HttpWebResponse HttpsWebRequest::CreatePostHttpResponse(System.String,System.Collections.Generic.IDictionary`2<System.String,System.String>,System.Text.Encoding)
extern "C"  HttpWebResponse_t3793423559 * HttpsWebRequest_CreatePostHttpResponse_m1020312363 (Il2CppObject * __this /* static, unused */, String_t* ___url0, Il2CppObject* ___parameters1, Encoding_t2012439129 * ___charset2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HttpsData HttpsWebRequest::SendHTTPSMessage(System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C"  HttpsData_t2200837909 * HttpsWebRequest_SendHTTPSMessage_m1049885082 (Il2CppObject * __this /* static, unused */, String_t* ___url0, Dictionary_2_t827649927 * ___parameters1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HttpsWebRequest::DownFileHTTPSThread(System.Object)
extern "C"  void HttpsWebRequest_DownFileHTTPSThread_m2484289242 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___downUrl0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HttpsData HttpsWebRequest::DownFileHTTPS(System.String)
extern "C"  HttpsData_t2200837909 * HttpsWebRequest_DownFileHTTPS_m2810079060 (Il2CppObject * __this /* static, unused */, String_t* ___downUrl0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HttpsWebRequest::WriteDownFileHTTPS(HttpsData,System.String)
extern "C"  void HttpsWebRequest_WriteDownFileHTTPS_m224130476 (Il2CppObject * __this /* static, unused */, HttpsData_t2200837909 * ___hData0, String_t* ___writeUrl1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.HttpWebResponse HttpsWebRequest::ilo_CreatePostHttpResponse1(System.String,System.Collections.Generic.IDictionary`2<System.String,System.String>,System.Text.Encoding)
extern "C"  HttpWebResponse_t3793423559 * HttpsWebRequest_ilo_CreatePostHttpResponse1_m3033371811 (Il2CppObject * __this /* static, unused */, String_t* ___url0, Il2CppObject* ___parameters1, Encoding_t2012439129 * ___charset2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HttpsData HttpsWebRequest::ilo_SendHTTPSMessage2(System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C"  HttpsData_t2200837909 * HttpsWebRequest_ilo_SendHTTPSMessage2_m407623473 (Il2CppObject * __this /* static, unused */, String_t* ___url0, Dictionary_2_t827649927 * ___parameters1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
