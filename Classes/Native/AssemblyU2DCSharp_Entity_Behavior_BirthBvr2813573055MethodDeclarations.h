﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Entity.Behavior.BirthBvr
struct BirthBvr_t2813573055;
// EffectMgr
struct EffectMgr_t535289511;
// CombatEntity
struct CombatEntity_t684137495;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Entity_Behavior_EBehaviorID1238533136.h"
#include "AssemblyU2DCSharp_EffectMgr535289511.h"
#include "AssemblyU2DCSharp_CombatEntity684137495.h"

// System.Void Entity.Behavior.BirthBvr::.ctor()
extern "C"  void BirthBvr__ctor_m589335627 (BirthBvr_t2813573055 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Entity.Behavior.EBehaviorID Entity.Behavior.BirthBvr::get_id()
extern "C"  uint8_t BirthBvr_get_id_m3626979247 (BirthBvr_t2813573055 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.BirthBvr::Reason()
extern "C"  void BirthBvr_Reason_m640923357 (BirthBvr_t2813573055 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.BirthBvr::Action()
extern "C"  void BirthBvr_Action_m4132597327 (BirthBvr_t2813573055 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.BirthBvr::DoEntering()
extern "C"  void BirthBvr_DoEntering_m1522153486 (BirthBvr_t2813573055 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.BirthBvr::DoLeaving()
extern "C"  void BirthBvr_DoLeaving_m1463843282 (BirthBvr_t2813573055 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// EffectMgr Entity.Behavior.BirthBvr::ilo_get_EffectMgr1()
extern "C"  EffectMgr_t535289511 * BirthBvr_ilo_get_EffectMgr1_m641308523 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.BirthBvr::ilo_PlayEffect2(EffectMgr,System.Int32,CombatEntity)
extern "C"  void BirthBvr_ilo_PlayEffect2_m4258438752 (Il2CppObject * __this /* static, unused */, EffectMgr_t535289511 * ____this0, int32_t ___id1, CombatEntity_t684137495 * ___srcTf2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
