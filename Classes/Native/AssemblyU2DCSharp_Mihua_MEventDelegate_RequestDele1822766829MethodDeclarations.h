﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mihua.MEventDelegate/RequestDelegate
struct RequestDelegate_t1822766829;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void Mihua.MEventDelegate/RequestDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void RequestDelegate__ctor_m1601580308 (RequestDelegate_t1822766829 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mihua.MEventDelegate/RequestDelegate::Invoke()
extern "C"  bool RequestDelegate_Invoke_m2654428706 (RequestDelegate_t1822766829 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Mihua.MEventDelegate/RequestDelegate::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * RequestDelegate_BeginInvoke_m1460608213 (RequestDelegate_t1822766829 * __this, AsyncCallback_t1369114871 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mihua.MEventDelegate/RequestDelegate::EndInvoke(System.IAsyncResult)
extern "C"  bool RequestDelegate_EndInvoke_m1550261720 (RequestDelegate_t1822766829 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
