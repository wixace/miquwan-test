﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// JSDataExchangeMgr/DGetV`1<UnityEngine.Sprite[]>
struct DGetV_1_t2638962241;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UI2DSpriteAnimationGenerated
struct  UI2DSpriteAnimationGenerated_t1437194262  : public Il2CppObject
{
public:

public:
};

struct UI2DSpriteAnimationGenerated_t1437194262_StaticFields
{
public:
	// JSDataExchangeMgr/DGetV`1<UnityEngine.Sprite[]> UI2DSpriteAnimationGenerated::<>f__am$cache0
	DGetV_1_t2638962241 * ___U3CU3Ef__amU24cache0_0;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_0() { return static_cast<int32_t>(offsetof(UI2DSpriteAnimationGenerated_t1437194262_StaticFields, ___U3CU3Ef__amU24cache0_0)); }
	inline DGetV_1_t2638962241 * get_U3CU3Ef__amU24cache0_0() const { return ___U3CU3Ef__amU24cache0_0; }
	inline DGetV_1_t2638962241 ** get_address_of_U3CU3Ef__amU24cache0_0() { return &___U3CU3Ef__amU24cache0_0; }
	inline void set_U3CU3Ef__amU24cache0_0(DGetV_1_t2638962241 * value)
	{
		___U3CU3Ef__amU24cache0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
