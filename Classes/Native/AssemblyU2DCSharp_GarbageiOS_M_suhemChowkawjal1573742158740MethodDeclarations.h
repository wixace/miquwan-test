﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_suhemChowkawjal157
struct M_suhemChowkawjal157_t3742158740;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_suhemChowkawjal1573742158740.h"

// System.Void GarbageiOS.M_suhemChowkawjal157::.ctor()
extern "C"  void M_suhemChowkawjal157__ctor_m1992776479 (M_suhemChowkawjal157_t3742158740 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_suhemChowkawjal157::M_poutalsowFoucakall0(System.String[],System.Int32)
extern "C"  void M_suhemChowkawjal157_M_poutalsowFoucakall0_m342578660 (M_suhemChowkawjal157_t3742158740 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_suhemChowkawjal157::M_zejairdreJujai1(System.String[],System.Int32)
extern "C"  void M_suhemChowkawjal157_M_zejairdreJujai1_m281966552 (M_suhemChowkawjal157_t3742158740 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_suhemChowkawjal157::M_kelsirqall2(System.String[],System.Int32)
extern "C"  void M_suhemChowkawjal157_M_kelsirqall2_m1060799034 (M_suhemChowkawjal157_t3742158740 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_suhemChowkawjal157::M_kewhaxi3(System.String[],System.Int32)
extern "C"  void M_suhemChowkawjal157_M_kewhaxi3_m1686795162 (M_suhemChowkawjal157_t3742158740 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_suhemChowkawjal157::M_sisbar4(System.String[],System.Int32)
extern "C"  void M_suhemChowkawjal157_M_sisbar4_m2541675008 (M_suhemChowkawjal157_t3742158740 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_suhemChowkawjal157::M_zelrijalZarferesi5(System.String[],System.Int32)
extern "C"  void M_suhemChowkawjal157_M_zelrijalZarferesi5_m1452828493 (M_suhemChowkawjal157_t3742158740 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_suhemChowkawjal157::ilo_M_zejairdreJujai11(GarbageiOS.M_suhemChowkawjal157,System.String[],System.Int32)
extern "C"  void M_suhemChowkawjal157_ilo_M_zejairdreJujai11_m2120833116 (Il2CppObject * __this /* static, unused */, M_suhemChowkawjal157_t3742158740 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_suhemChowkawjal157::ilo_M_kewhaxi32(GarbageiOS.M_suhemChowkawjal157,System.String[],System.Int32)
extern "C"  void M_suhemChowkawjal157_ilo_M_kewhaxi32_m3250358293 (Il2CppObject * __this /* static, unused */, M_suhemChowkawjal157_t3742158740 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_suhemChowkawjal157::ilo_M_sisbar43(GarbageiOS.M_suhemChowkawjal157,System.String[],System.Int32)
extern "C"  void M_suhemChowkawjal157_ilo_M_sisbar43_m3886202550 (Il2CppObject * __this /* static, unused */, M_suhemChowkawjal157_t3742158740 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
