﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<System.String>
struct List_1_t1375417109;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t2522024052;
// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mihua.Asset.AssetCollect
struct  AssetCollect_t217590220  : public Il2CppObject
{
public:

public:
};

struct AssetCollect_t217590220_StaticFields
{
public:
	// System.Collections.Generic.List`1<System.String> Mihua.Asset.AssetCollect::uiNeedList
	List_1_t1375417109 * ___uiNeedList_0;
	// System.Collections.Generic.List`1<System.String> Mihua.Asset.AssetCollect::nextList
	List_1_t1375417109 * ___nextList_1;
	// System.Collections.Generic.List`1<System.Int32> Mihua.Asset.AssetCollect::summonList
	List_1_t2522024052 * ___summonList_2;
	// System.String Mihua.Asset.AssetCollect::AssetsJArray
	String_t* ___AssetsJArray_3;

public:
	inline static int32_t get_offset_of_uiNeedList_0() { return static_cast<int32_t>(offsetof(AssetCollect_t217590220_StaticFields, ___uiNeedList_0)); }
	inline List_1_t1375417109 * get_uiNeedList_0() const { return ___uiNeedList_0; }
	inline List_1_t1375417109 ** get_address_of_uiNeedList_0() { return &___uiNeedList_0; }
	inline void set_uiNeedList_0(List_1_t1375417109 * value)
	{
		___uiNeedList_0 = value;
		Il2CppCodeGenWriteBarrier(&___uiNeedList_0, value);
	}

	inline static int32_t get_offset_of_nextList_1() { return static_cast<int32_t>(offsetof(AssetCollect_t217590220_StaticFields, ___nextList_1)); }
	inline List_1_t1375417109 * get_nextList_1() const { return ___nextList_1; }
	inline List_1_t1375417109 ** get_address_of_nextList_1() { return &___nextList_1; }
	inline void set_nextList_1(List_1_t1375417109 * value)
	{
		___nextList_1 = value;
		Il2CppCodeGenWriteBarrier(&___nextList_1, value);
	}

	inline static int32_t get_offset_of_summonList_2() { return static_cast<int32_t>(offsetof(AssetCollect_t217590220_StaticFields, ___summonList_2)); }
	inline List_1_t2522024052 * get_summonList_2() const { return ___summonList_2; }
	inline List_1_t2522024052 ** get_address_of_summonList_2() { return &___summonList_2; }
	inline void set_summonList_2(List_1_t2522024052 * value)
	{
		___summonList_2 = value;
		Il2CppCodeGenWriteBarrier(&___summonList_2, value);
	}

	inline static int32_t get_offset_of_AssetsJArray_3() { return static_cast<int32_t>(offsetof(AssetCollect_t217590220_StaticFields, ___AssetsJArray_3)); }
	inline String_t* get_AssetsJArray_3() const { return ___AssetsJArray_3; }
	inline String_t** get_address_of_AssetsJArray_3() { return &___AssetsJArray_3; }
	inline void set_AssetsJArray_3(String_t* value)
	{
		___AssetsJArray_3 = value;
		Il2CppCodeGenWriteBarrier(&___AssetsJArray_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
