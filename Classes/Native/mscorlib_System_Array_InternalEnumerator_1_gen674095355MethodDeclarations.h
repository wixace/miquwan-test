﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen674095355.h"
#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DCSharp_UIModelDisplayType1891752679.h"

// System.Void System.Array/InternalEnumerator`1<UIModelDisplayType>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2326463288_gshared (InternalEnumerator_1_t674095355 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m2326463288(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t674095355 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m2326463288_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<UIModelDisplayType>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m615331688_gshared (InternalEnumerator_1_t674095355 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m615331688(__this, method) ((  void (*) (InternalEnumerator_1_t674095355 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m615331688_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<UIModelDisplayType>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3289138900_gshared (InternalEnumerator_1_t674095355 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3289138900(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t674095355 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3289138900_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UIModelDisplayType>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1369486095_gshared (InternalEnumerator_1_t674095355 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m1369486095(__this, method) ((  void (*) (InternalEnumerator_1_t674095355 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1369486095_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UIModelDisplayType>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m228022740_gshared (InternalEnumerator_1_t674095355 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m228022740(__this, method) ((  bool (*) (InternalEnumerator_1_t674095355 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m228022740_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UIModelDisplayType>::get_Current()
extern "C"  int32_t InternalEnumerator_1_get_Current_m184980991_gshared (InternalEnumerator_1_t674095355 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m184980991(__this, method) ((  int32_t (*) (InternalEnumerator_1_t674095355 *, const MethodInfo*))InternalEnumerator_1_get_Current_m184980991_gshared)(__this, method)
