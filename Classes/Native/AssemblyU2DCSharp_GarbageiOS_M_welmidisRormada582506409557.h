﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_welmidisRormada58
struct  M_welmidisRormada58_t2506409557  : public Il2CppObject
{
public:
	// System.UInt32 GarbageiOS.M_welmidisRormada58::_soracal
	uint32_t ____soracal_0;
	// System.Boolean GarbageiOS.M_welmidisRormada58::_jalcirDrasmair
	bool ____jalcirDrasmair_1;
	// System.UInt32 GarbageiOS.M_welmidisRormada58::_darterdaKigelcas
	uint32_t ____darterdaKigelcas_2;
	// System.String GarbageiOS.M_welmidisRormada58::_gissoo
	String_t* ____gissoo_3;
	// System.UInt32 GarbageiOS.M_welmidisRormada58::_basfasarRistaw
	uint32_t ____basfasarRistaw_4;
	// System.UInt32 GarbageiOS.M_welmidisRormada58::_draxal
	uint32_t ____draxal_5;
	// System.Int32 GarbageiOS.M_welmidisRormada58::_lougihiCirja
	int32_t ____lougihiCirja_6;
	// System.Single GarbageiOS.M_welmidisRormada58::_likea
	float ____likea_7;
	// System.Int32 GarbageiOS.M_welmidisRormada58::_pecumiTicir
	int32_t ____pecumiTicir_8;
	// System.Int32 GarbageiOS.M_welmidisRormada58::_picai
	int32_t ____picai_9;
	// System.UInt32 GarbageiOS.M_welmidisRormada58::_daysawPearqi
	uint32_t ____daysawPearqi_10;
	// System.String GarbageiOS.M_welmidisRormada58::_yecoochirTejay
	String_t* ____yecoochirTejay_11;
	// System.Single GarbageiOS.M_welmidisRormada58::_woocallsta
	float ____woocallsta_12;
	// System.Boolean GarbageiOS.M_welmidisRormada58::_fascaice
	bool ____fascaice_13;
	// System.String GarbageiOS.M_welmidisRormada58::_raljaBuree
	String_t* ____raljaBuree_14;
	// System.UInt32 GarbageiOS.M_welmidisRormada58::_nirboo
	uint32_t ____nirboo_15;

public:
	inline static int32_t get_offset_of__soracal_0() { return static_cast<int32_t>(offsetof(M_welmidisRormada58_t2506409557, ____soracal_0)); }
	inline uint32_t get__soracal_0() const { return ____soracal_0; }
	inline uint32_t* get_address_of__soracal_0() { return &____soracal_0; }
	inline void set__soracal_0(uint32_t value)
	{
		____soracal_0 = value;
	}

	inline static int32_t get_offset_of__jalcirDrasmair_1() { return static_cast<int32_t>(offsetof(M_welmidisRormada58_t2506409557, ____jalcirDrasmair_1)); }
	inline bool get__jalcirDrasmair_1() const { return ____jalcirDrasmair_1; }
	inline bool* get_address_of__jalcirDrasmair_1() { return &____jalcirDrasmair_1; }
	inline void set__jalcirDrasmair_1(bool value)
	{
		____jalcirDrasmair_1 = value;
	}

	inline static int32_t get_offset_of__darterdaKigelcas_2() { return static_cast<int32_t>(offsetof(M_welmidisRormada58_t2506409557, ____darterdaKigelcas_2)); }
	inline uint32_t get__darterdaKigelcas_2() const { return ____darterdaKigelcas_2; }
	inline uint32_t* get_address_of__darterdaKigelcas_2() { return &____darterdaKigelcas_2; }
	inline void set__darterdaKigelcas_2(uint32_t value)
	{
		____darterdaKigelcas_2 = value;
	}

	inline static int32_t get_offset_of__gissoo_3() { return static_cast<int32_t>(offsetof(M_welmidisRormada58_t2506409557, ____gissoo_3)); }
	inline String_t* get__gissoo_3() const { return ____gissoo_3; }
	inline String_t** get_address_of__gissoo_3() { return &____gissoo_3; }
	inline void set__gissoo_3(String_t* value)
	{
		____gissoo_3 = value;
		Il2CppCodeGenWriteBarrier(&____gissoo_3, value);
	}

	inline static int32_t get_offset_of__basfasarRistaw_4() { return static_cast<int32_t>(offsetof(M_welmidisRormada58_t2506409557, ____basfasarRistaw_4)); }
	inline uint32_t get__basfasarRistaw_4() const { return ____basfasarRistaw_4; }
	inline uint32_t* get_address_of__basfasarRistaw_4() { return &____basfasarRistaw_4; }
	inline void set__basfasarRistaw_4(uint32_t value)
	{
		____basfasarRistaw_4 = value;
	}

	inline static int32_t get_offset_of__draxal_5() { return static_cast<int32_t>(offsetof(M_welmidisRormada58_t2506409557, ____draxal_5)); }
	inline uint32_t get__draxal_5() const { return ____draxal_5; }
	inline uint32_t* get_address_of__draxal_5() { return &____draxal_5; }
	inline void set__draxal_5(uint32_t value)
	{
		____draxal_5 = value;
	}

	inline static int32_t get_offset_of__lougihiCirja_6() { return static_cast<int32_t>(offsetof(M_welmidisRormada58_t2506409557, ____lougihiCirja_6)); }
	inline int32_t get__lougihiCirja_6() const { return ____lougihiCirja_6; }
	inline int32_t* get_address_of__lougihiCirja_6() { return &____lougihiCirja_6; }
	inline void set__lougihiCirja_6(int32_t value)
	{
		____lougihiCirja_6 = value;
	}

	inline static int32_t get_offset_of__likea_7() { return static_cast<int32_t>(offsetof(M_welmidisRormada58_t2506409557, ____likea_7)); }
	inline float get__likea_7() const { return ____likea_7; }
	inline float* get_address_of__likea_7() { return &____likea_7; }
	inline void set__likea_7(float value)
	{
		____likea_7 = value;
	}

	inline static int32_t get_offset_of__pecumiTicir_8() { return static_cast<int32_t>(offsetof(M_welmidisRormada58_t2506409557, ____pecumiTicir_8)); }
	inline int32_t get__pecumiTicir_8() const { return ____pecumiTicir_8; }
	inline int32_t* get_address_of__pecumiTicir_8() { return &____pecumiTicir_8; }
	inline void set__pecumiTicir_8(int32_t value)
	{
		____pecumiTicir_8 = value;
	}

	inline static int32_t get_offset_of__picai_9() { return static_cast<int32_t>(offsetof(M_welmidisRormada58_t2506409557, ____picai_9)); }
	inline int32_t get__picai_9() const { return ____picai_9; }
	inline int32_t* get_address_of__picai_9() { return &____picai_9; }
	inline void set__picai_9(int32_t value)
	{
		____picai_9 = value;
	}

	inline static int32_t get_offset_of__daysawPearqi_10() { return static_cast<int32_t>(offsetof(M_welmidisRormada58_t2506409557, ____daysawPearqi_10)); }
	inline uint32_t get__daysawPearqi_10() const { return ____daysawPearqi_10; }
	inline uint32_t* get_address_of__daysawPearqi_10() { return &____daysawPearqi_10; }
	inline void set__daysawPearqi_10(uint32_t value)
	{
		____daysawPearqi_10 = value;
	}

	inline static int32_t get_offset_of__yecoochirTejay_11() { return static_cast<int32_t>(offsetof(M_welmidisRormada58_t2506409557, ____yecoochirTejay_11)); }
	inline String_t* get__yecoochirTejay_11() const { return ____yecoochirTejay_11; }
	inline String_t** get_address_of__yecoochirTejay_11() { return &____yecoochirTejay_11; }
	inline void set__yecoochirTejay_11(String_t* value)
	{
		____yecoochirTejay_11 = value;
		Il2CppCodeGenWriteBarrier(&____yecoochirTejay_11, value);
	}

	inline static int32_t get_offset_of__woocallsta_12() { return static_cast<int32_t>(offsetof(M_welmidisRormada58_t2506409557, ____woocallsta_12)); }
	inline float get__woocallsta_12() const { return ____woocallsta_12; }
	inline float* get_address_of__woocallsta_12() { return &____woocallsta_12; }
	inline void set__woocallsta_12(float value)
	{
		____woocallsta_12 = value;
	}

	inline static int32_t get_offset_of__fascaice_13() { return static_cast<int32_t>(offsetof(M_welmidisRormada58_t2506409557, ____fascaice_13)); }
	inline bool get__fascaice_13() const { return ____fascaice_13; }
	inline bool* get_address_of__fascaice_13() { return &____fascaice_13; }
	inline void set__fascaice_13(bool value)
	{
		____fascaice_13 = value;
	}

	inline static int32_t get_offset_of__raljaBuree_14() { return static_cast<int32_t>(offsetof(M_welmidisRormada58_t2506409557, ____raljaBuree_14)); }
	inline String_t* get__raljaBuree_14() const { return ____raljaBuree_14; }
	inline String_t** get_address_of__raljaBuree_14() { return &____raljaBuree_14; }
	inline void set__raljaBuree_14(String_t* value)
	{
		____raljaBuree_14 = value;
		Il2CppCodeGenWriteBarrier(&____raljaBuree_14, value);
	}

	inline static int32_t get_offset_of__nirboo_15() { return static_cast<int32_t>(offsetof(M_welmidisRormada58_t2506409557, ____nirboo_15)); }
	inline uint32_t get__nirboo_15() const { return ____nirboo_15; }
	inline uint32_t* get_address_of__nirboo_15() { return &____nirboo_15; }
	inline void set__nirboo_15(uint32_t value)
	{
		____nirboo_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
