﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_Newtonsoft_Json_Converters_DateT4250504584.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Converters.JavaScriptDateTimeConverter
struct  JavaScriptDateTimeConverter_t81069962  : public DateTimeConverterBase_t4250504584
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
