﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_AudioClipGenerated/<AudioClip_Create_GetDelegate_member4_arg5>c__AnonStoreyDB
struct U3CAudioClip_Create_GetDelegate_member4_arg5U3Ec__AnonStoreyDB_t1666683515;
// System.Single[]
struct SingleU5BU5D_t2316563989;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine_AudioClipGenerated/<AudioClip_Create_GetDelegate_member4_arg5>c__AnonStoreyDB::.ctor()
extern "C"  void U3CAudioClip_Create_GetDelegate_member4_arg5U3Ec__AnonStoreyDB__ctor_m376376656 (U3CAudioClip_Create_GetDelegate_member4_arg5U3Ec__AnonStoreyDB_t1666683515 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioClipGenerated/<AudioClip_Create_GetDelegate_member4_arg5>c__AnonStoreyDB::<>m__186(System.Single[])
extern "C"  void U3CAudioClip_Create_GetDelegate_member4_arg5U3Ec__AnonStoreyDB_U3CU3Em__186_m776015601 (U3CAudioClip_Create_GetDelegate_member4_arg5U3Ec__AnonStoreyDB_t1666683515 * __this, SingleU5BU5D_t2316563989* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
