﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.JObjectAttribute
struct JObjectAttribute_t3411920523;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_MemberSerializat1550301796.h"
#include "mscorlib_System_String7231557.h"

// System.Void Newtonsoft.Json.JObjectAttribute::.ctor()
extern "C"  void JObjectAttribute__ctor_m3061587891 (JObjectAttribute_t3411920523 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JObjectAttribute::.ctor(Newtonsoft.Json.MemberSerialization)
extern "C"  void JObjectAttribute__ctor_m2470259902 (JObjectAttribute_t3411920523 * __this, int32_t ___memberSerialization0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JObjectAttribute::.ctor(System.String)
extern "C"  void JObjectAttribute__ctor_m1390664239 (JObjectAttribute_t3411920523 * __this, String_t* ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.MemberSerialization Newtonsoft.Json.JObjectAttribute::get_MemberSerialization()
extern "C"  int32_t JObjectAttribute_get_MemberSerialization_m351182956 (JObjectAttribute_t3411920523 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JObjectAttribute::set_MemberSerialization(Newtonsoft.Json.MemberSerialization)
extern "C"  void JObjectAttribute_set_MemberSerialization_m3032355431 (JObjectAttribute_t3411920523 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
