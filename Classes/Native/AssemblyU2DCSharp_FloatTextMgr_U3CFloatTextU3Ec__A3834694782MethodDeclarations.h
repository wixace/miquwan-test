﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FloatTextMgr/<FloatText>c__AnonStorey158
struct U3CFloatTextU3Ec__AnonStorey158_t3834694782;
// Mihua.Asset.ABLoadOperation.AssetOperation
struct AssetOperation_t778728221;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Mihua_Asset_ABLoadOperation_Asset778728221.h"

// System.Void FloatTextMgr/<FloatText>c__AnonStorey158::.ctor()
extern "C"  void U3CFloatTextU3Ec__AnonStorey158__ctor_m2115813341 (U3CFloatTextU3Ec__AnonStorey158_t3834694782 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FloatTextMgr/<FloatText>c__AnonStorey158::<>m__3DD(Mihua.Asset.ABLoadOperation.AssetOperation)
extern "C"  void U3CFloatTextU3Ec__AnonStorey158_U3CU3Em__3DD_m1721565462 (U3CFloatTextU3Ec__AnonStorey158_t3834694782 * __this, AssetOperation_t778728221 * ___ao0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
