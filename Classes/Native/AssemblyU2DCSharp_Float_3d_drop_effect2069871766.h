﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.String
struct String_t;
// TweenPosition
struct TweenPosition_t3684358292;

#include "AssemblyU2DCSharp_FloatTextUnit2362298029.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Float_3d_drop_effect
struct  Float_3d_drop_effect_t2069871766  : public FloatTextUnit_t2362298029
{
public:
	// UnityEngine.GameObject Float_3d_drop_effect::_copper_effect
	GameObject_t3674682005 * ____copper_effect_3;
	// System.String Float_3d_drop_effect::_effect_name
	String_t* ____effect_name_4;
	// UnityEngine.Vector3 Float_3d_drop_effect::_aim
	Vector3_t4282066566  ____aim_5;
	// System.Single Float_3d_drop_effect::_time
	float ____time_6;
	// TweenPosition Float_3d_drop_effect::tp
	TweenPosition_t3684358292 * ___tp_7;

public:
	inline static int32_t get_offset_of__copper_effect_3() { return static_cast<int32_t>(offsetof(Float_3d_drop_effect_t2069871766, ____copper_effect_3)); }
	inline GameObject_t3674682005 * get__copper_effect_3() const { return ____copper_effect_3; }
	inline GameObject_t3674682005 ** get_address_of__copper_effect_3() { return &____copper_effect_3; }
	inline void set__copper_effect_3(GameObject_t3674682005 * value)
	{
		____copper_effect_3 = value;
		Il2CppCodeGenWriteBarrier(&____copper_effect_3, value);
	}

	inline static int32_t get_offset_of__effect_name_4() { return static_cast<int32_t>(offsetof(Float_3d_drop_effect_t2069871766, ____effect_name_4)); }
	inline String_t* get__effect_name_4() const { return ____effect_name_4; }
	inline String_t** get_address_of__effect_name_4() { return &____effect_name_4; }
	inline void set__effect_name_4(String_t* value)
	{
		____effect_name_4 = value;
		Il2CppCodeGenWriteBarrier(&____effect_name_4, value);
	}

	inline static int32_t get_offset_of__aim_5() { return static_cast<int32_t>(offsetof(Float_3d_drop_effect_t2069871766, ____aim_5)); }
	inline Vector3_t4282066566  get__aim_5() const { return ____aim_5; }
	inline Vector3_t4282066566 * get_address_of__aim_5() { return &____aim_5; }
	inline void set__aim_5(Vector3_t4282066566  value)
	{
		____aim_5 = value;
	}

	inline static int32_t get_offset_of__time_6() { return static_cast<int32_t>(offsetof(Float_3d_drop_effect_t2069871766, ____time_6)); }
	inline float get__time_6() const { return ____time_6; }
	inline float* get_address_of__time_6() { return &____time_6; }
	inline void set__time_6(float value)
	{
		____time_6 = value;
	}

	inline static int32_t get_offset_of_tp_7() { return static_cast<int32_t>(offsetof(Float_3d_drop_effect_t2069871766, ___tp_7)); }
	inline TweenPosition_t3684358292 * get_tp_7() const { return ___tp_7; }
	inline TweenPosition_t3684358292 ** get_address_of_tp_7() { return &___tp_7; }
	inline void set_tp_7(TweenPosition_t3684358292 * value)
	{
		___tp_7 = value;
		Il2CppCodeGenWriteBarrier(&___tp_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
