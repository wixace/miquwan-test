﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._0d5c71543f6c7dc9008979d319600e44
struct _0d5c71543f6c7dc9008979d319600e44_t1147517344;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__0d5c71543f6c7dc9008979d31147517344.h"

// System.Void Little._0d5c71543f6c7dc9008979d319600e44::.ctor()
extern "C"  void _0d5c71543f6c7dc9008979d319600e44__ctor_m1503936653 (_0d5c71543f6c7dc9008979d319600e44_t1147517344 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._0d5c71543f6c7dc9008979d319600e44::_0d5c71543f6c7dc9008979d319600e44m2(System.Int32)
extern "C"  int32_t _0d5c71543f6c7dc9008979d319600e44__0d5c71543f6c7dc9008979d319600e44m2_m15183257 (_0d5c71543f6c7dc9008979d319600e44_t1147517344 * __this, int32_t ____0d5c71543f6c7dc9008979d319600e44a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._0d5c71543f6c7dc9008979d319600e44::_0d5c71543f6c7dc9008979d319600e44m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _0d5c71543f6c7dc9008979d319600e44__0d5c71543f6c7dc9008979d319600e44m_m1023703357 (_0d5c71543f6c7dc9008979d319600e44_t1147517344 * __this, int32_t ____0d5c71543f6c7dc9008979d319600e44a0, int32_t ____0d5c71543f6c7dc9008979d319600e44861, int32_t ____0d5c71543f6c7dc9008979d319600e44c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._0d5c71543f6c7dc9008979d319600e44::ilo__0d5c71543f6c7dc9008979d319600e44m21(Little._0d5c71543f6c7dc9008979d319600e44,System.Int32)
extern "C"  int32_t _0d5c71543f6c7dc9008979d319600e44_ilo__0d5c71543f6c7dc9008979d319600e44m21_m1233285991 (Il2CppObject * __this /* static, unused */, _0d5c71543f6c7dc9008979d319600e44_t1147517344 * ____this0, int32_t ____0d5c71543f6c7dc9008979d319600e44a1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
