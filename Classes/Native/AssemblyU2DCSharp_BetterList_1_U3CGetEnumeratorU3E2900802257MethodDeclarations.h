﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BetterList`1/<GetEnumerator>c__Iterator29<System.Int32>
struct U3CGetEnumeratorU3Ec__Iterator29_t2900802257;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void BetterList`1/<GetEnumerator>c__Iterator29<System.Int32>::.ctor()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator29__ctor_m3581114768_gshared (U3CGetEnumeratorU3Ec__Iterator29_t2900802257 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator29__ctor_m3581114768(__this, method) ((  void (*) (U3CGetEnumeratorU3Ec__Iterator29_t2900802257 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator29__ctor_m3581114768_gshared)(__this, method)
// T BetterList`1/<GetEnumerator>c__Iterator29<System.Int32>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  int32_t U3CGetEnumeratorU3Ec__Iterator29_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m3920091543_gshared (U3CGetEnumeratorU3Ec__Iterator29_t2900802257 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator29_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m3920091543(__this, method) ((  int32_t (*) (U3CGetEnumeratorU3Ec__Iterator29_t2900802257 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator29_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m3920091543_gshared)(__this, method)
// System.Object BetterList`1/<GetEnumerator>c__Iterator29<System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator29_System_Collections_IEnumerator_get_Current_m3054258774_gshared (U3CGetEnumeratorU3Ec__Iterator29_t2900802257 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator29_System_Collections_IEnumerator_get_Current_m3054258774(__this, method) ((  Il2CppObject * (*) (U3CGetEnumeratorU3Ec__Iterator29_t2900802257 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator29_System_Collections_IEnumerator_get_Current_m3054258774_gshared)(__this, method)
// System.Boolean BetterList`1/<GetEnumerator>c__Iterator29<System.Int32>::MoveNext()
extern "C"  bool U3CGetEnumeratorU3Ec__Iterator29_MoveNext_m3867465316_gshared (U3CGetEnumeratorU3Ec__Iterator29_t2900802257 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator29_MoveNext_m3867465316(__this, method) ((  bool (*) (U3CGetEnumeratorU3Ec__Iterator29_t2900802257 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator29_MoveNext_m3867465316_gshared)(__this, method)
// System.Void BetterList`1/<GetEnumerator>c__Iterator29<System.Int32>::Dispose()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator29_Dispose_m1080771021_gshared (U3CGetEnumeratorU3Ec__Iterator29_t2900802257 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator29_Dispose_m1080771021(__this, method) ((  void (*) (U3CGetEnumeratorU3Ec__Iterator29_t2900802257 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator29_Dispose_m1080771021_gshared)(__this, method)
// System.Void BetterList`1/<GetEnumerator>c__Iterator29<System.Int32>::Reset()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator29_Reset_m1227547709_gshared (U3CGetEnumeratorU3Ec__Iterator29_t2900802257 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator29_Reset_m1227547709(__this, method) ((  void (*) (U3CGetEnumeratorU3Ec__Iterator29_t2900802257 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator29_Reset_m1227547709_gshared)(__this, method)
