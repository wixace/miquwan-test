﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_siwalstar15
struct M_siwalstar15_t129305858;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"

// System.Void GarbageiOS.M_siwalstar15::.ctor()
extern "C"  void M_siwalstar15__ctor_m2382819489 (M_siwalstar15_t129305858 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_siwalstar15::M_serelrurChisnou0(System.String[],System.Int32)
extern "C"  void M_siwalstar15_M_serelrurChisnou0_m2493687167 (M_siwalstar15_t129305858 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
