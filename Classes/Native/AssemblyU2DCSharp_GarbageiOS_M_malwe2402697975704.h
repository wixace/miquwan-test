﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_malwe240
struct  M_malwe240_t2697975704  : public Il2CppObject
{
public:
	// System.Single GarbageiOS.M_malwe240::_baxiski
	float ____baxiski_0;
	// System.Int32 GarbageiOS.M_malwe240::_coovi
	int32_t ____coovi_1;
	// System.Boolean GarbageiOS.M_malwe240::_nairrousouCecabem
	bool ____nairrousouCecabem_2;
	// System.String GarbageiOS.M_malwe240::_poohourer
	String_t* ____poohourer_3;
	// System.Int32 GarbageiOS.M_malwe240::_trashu
	int32_t ____trashu_4;
	// System.Single GarbageiOS.M_malwe240::_ballfosee
	float ____ballfosee_5;
	// System.String GarbageiOS.M_malwe240::_majarar
	String_t* ____majarar_6;
	// System.Single GarbageiOS.M_malwe240::_saybir
	float ____saybir_7;
	// System.Int32 GarbageiOS.M_malwe240::_haywiryel
	int32_t ____haywiryel_8;
	// System.Int32 GarbageiOS.M_malwe240::_jouna
	int32_t ____jouna_9;
	// System.Int32 GarbageiOS.M_malwe240::_gelixurGurfo
	int32_t ____gelixurGurfo_10;
	// System.Int32 GarbageiOS.M_malwe240::_rallsorxo
	int32_t ____rallsorxo_11;
	// System.String GarbageiOS.M_malwe240::_hooraygi
	String_t* ____hooraygi_12;
	// System.Single GarbageiOS.M_malwe240::_segemrar
	float ____segemrar_13;
	// System.Int32 GarbageiOS.M_malwe240::_fallsaJavo
	int32_t ____fallsaJavo_14;
	// System.Int32 GarbageiOS.M_malwe240::_cearoselKirstuqow
	int32_t ____cearoselKirstuqow_15;
	// System.String GarbageiOS.M_malwe240::_cereegi
	String_t* ____cereegi_16;
	// System.Boolean GarbageiOS.M_malwe240::_qimar
	bool ____qimar_17;
	// System.UInt32 GarbageiOS.M_malwe240::_hoolejeTreako
	uint32_t ____hoolejeTreako_18;
	// System.String GarbageiOS.M_malwe240::_doryawhay
	String_t* ____doryawhay_19;
	// System.Boolean GarbageiOS.M_malwe240::_stairjair
	bool ____stairjair_20;
	// System.Boolean GarbageiOS.M_malwe240::_tounee
	bool ____tounee_21;
	// System.Single GarbageiOS.M_malwe240::_gorsi
	float ____gorsi_22;
	// System.Boolean GarbageiOS.M_malwe240::_genaleaLucow
	bool ____genaleaLucow_23;
	// System.UInt32 GarbageiOS.M_malwe240::_herowRepou
	uint32_t ____herowRepou_24;
	// System.Boolean GarbageiOS.M_malwe240::_woujiTrara
	bool ____woujiTrara_25;
	// System.UInt32 GarbageiOS.M_malwe240::_curou
	uint32_t ____curou_26;
	// System.Single GarbageiOS.M_malwe240::_bawmou
	float ____bawmou_27;
	// System.UInt32 GarbageiOS.M_malwe240::_najalJaxaldas
	uint32_t ____najalJaxaldas_28;
	// System.Single GarbageiOS.M_malwe240::_nadooCasar
	float ____nadooCasar_29;
	// System.Boolean GarbageiOS.M_malwe240::_powjiRelsasju
	bool ____powjiRelsasju_30;
	// System.Single GarbageiOS.M_malwe240::_nateeceJasemsem
	float ____nateeceJasemsem_31;
	// System.String GarbageiOS.M_malwe240::_pesiZasordi
	String_t* ____pesiZasordi_32;
	// System.UInt32 GarbageiOS.M_malwe240::_lowtri
	uint32_t ____lowtri_33;
	// System.Int32 GarbageiOS.M_malwe240::_qecorcou
	int32_t ____qecorcou_34;

public:
	inline static int32_t get_offset_of__baxiski_0() { return static_cast<int32_t>(offsetof(M_malwe240_t2697975704, ____baxiski_0)); }
	inline float get__baxiski_0() const { return ____baxiski_0; }
	inline float* get_address_of__baxiski_0() { return &____baxiski_0; }
	inline void set__baxiski_0(float value)
	{
		____baxiski_0 = value;
	}

	inline static int32_t get_offset_of__coovi_1() { return static_cast<int32_t>(offsetof(M_malwe240_t2697975704, ____coovi_1)); }
	inline int32_t get__coovi_1() const { return ____coovi_1; }
	inline int32_t* get_address_of__coovi_1() { return &____coovi_1; }
	inline void set__coovi_1(int32_t value)
	{
		____coovi_1 = value;
	}

	inline static int32_t get_offset_of__nairrousouCecabem_2() { return static_cast<int32_t>(offsetof(M_malwe240_t2697975704, ____nairrousouCecabem_2)); }
	inline bool get__nairrousouCecabem_2() const { return ____nairrousouCecabem_2; }
	inline bool* get_address_of__nairrousouCecabem_2() { return &____nairrousouCecabem_2; }
	inline void set__nairrousouCecabem_2(bool value)
	{
		____nairrousouCecabem_2 = value;
	}

	inline static int32_t get_offset_of__poohourer_3() { return static_cast<int32_t>(offsetof(M_malwe240_t2697975704, ____poohourer_3)); }
	inline String_t* get__poohourer_3() const { return ____poohourer_3; }
	inline String_t** get_address_of__poohourer_3() { return &____poohourer_3; }
	inline void set__poohourer_3(String_t* value)
	{
		____poohourer_3 = value;
		Il2CppCodeGenWriteBarrier(&____poohourer_3, value);
	}

	inline static int32_t get_offset_of__trashu_4() { return static_cast<int32_t>(offsetof(M_malwe240_t2697975704, ____trashu_4)); }
	inline int32_t get__trashu_4() const { return ____trashu_4; }
	inline int32_t* get_address_of__trashu_4() { return &____trashu_4; }
	inline void set__trashu_4(int32_t value)
	{
		____trashu_4 = value;
	}

	inline static int32_t get_offset_of__ballfosee_5() { return static_cast<int32_t>(offsetof(M_malwe240_t2697975704, ____ballfosee_5)); }
	inline float get__ballfosee_5() const { return ____ballfosee_5; }
	inline float* get_address_of__ballfosee_5() { return &____ballfosee_5; }
	inline void set__ballfosee_5(float value)
	{
		____ballfosee_5 = value;
	}

	inline static int32_t get_offset_of__majarar_6() { return static_cast<int32_t>(offsetof(M_malwe240_t2697975704, ____majarar_6)); }
	inline String_t* get__majarar_6() const { return ____majarar_6; }
	inline String_t** get_address_of__majarar_6() { return &____majarar_6; }
	inline void set__majarar_6(String_t* value)
	{
		____majarar_6 = value;
		Il2CppCodeGenWriteBarrier(&____majarar_6, value);
	}

	inline static int32_t get_offset_of__saybir_7() { return static_cast<int32_t>(offsetof(M_malwe240_t2697975704, ____saybir_7)); }
	inline float get__saybir_7() const { return ____saybir_7; }
	inline float* get_address_of__saybir_7() { return &____saybir_7; }
	inline void set__saybir_7(float value)
	{
		____saybir_7 = value;
	}

	inline static int32_t get_offset_of__haywiryel_8() { return static_cast<int32_t>(offsetof(M_malwe240_t2697975704, ____haywiryel_8)); }
	inline int32_t get__haywiryel_8() const { return ____haywiryel_8; }
	inline int32_t* get_address_of__haywiryel_8() { return &____haywiryel_8; }
	inline void set__haywiryel_8(int32_t value)
	{
		____haywiryel_8 = value;
	}

	inline static int32_t get_offset_of__jouna_9() { return static_cast<int32_t>(offsetof(M_malwe240_t2697975704, ____jouna_9)); }
	inline int32_t get__jouna_9() const { return ____jouna_9; }
	inline int32_t* get_address_of__jouna_9() { return &____jouna_9; }
	inline void set__jouna_9(int32_t value)
	{
		____jouna_9 = value;
	}

	inline static int32_t get_offset_of__gelixurGurfo_10() { return static_cast<int32_t>(offsetof(M_malwe240_t2697975704, ____gelixurGurfo_10)); }
	inline int32_t get__gelixurGurfo_10() const { return ____gelixurGurfo_10; }
	inline int32_t* get_address_of__gelixurGurfo_10() { return &____gelixurGurfo_10; }
	inline void set__gelixurGurfo_10(int32_t value)
	{
		____gelixurGurfo_10 = value;
	}

	inline static int32_t get_offset_of__rallsorxo_11() { return static_cast<int32_t>(offsetof(M_malwe240_t2697975704, ____rallsorxo_11)); }
	inline int32_t get__rallsorxo_11() const { return ____rallsorxo_11; }
	inline int32_t* get_address_of__rallsorxo_11() { return &____rallsorxo_11; }
	inline void set__rallsorxo_11(int32_t value)
	{
		____rallsorxo_11 = value;
	}

	inline static int32_t get_offset_of__hooraygi_12() { return static_cast<int32_t>(offsetof(M_malwe240_t2697975704, ____hooraygi_12)); }
	inline String_t* get__hooraygi_12() const { return ____hooraygi_12; }
	inline String_t** get_address_of__hooraygi_12() { return &____hooraygi_12; }
	inline void set__hooraygi_12(String_t* value)
	{
		____hooraygi_12 = value;
		Il2CppCodeGenWriteBarrier(&____hooraygi_12, value);
	}

	inline static int32_t get_offset_of__segemrar_13() { return static_cast<int32_t>(offsetof(M_malwe240_t2697975704, ____segemrar_13)); }
	inline float get__segemrar_13() const { return ____segemrar_13; }
	inline float* get_address_of__segemrar_13() { return &____segemrar_13; }
	inline void set__segemrar_13(float value)
	{
		____segemrar_13 = value;
	}

	inline static int32_t get_offset_of__fallsaJavo_14() { return static_cast<int32_t>(offsetof(M_malwe240_t2697975704, ____fallsaJavo_14)); }
	inline int32_t get__fallsaJavo_14() const { return ____fallsaJavo_14; }
	inline int32_t* get_address_of__fallsaJavo_14() { return &____fallsaJavo_14; }
	inline void set__fallsaJavo_14(int32_t value)
	{
		____fallsaJavo_14 = value;
	}

	inline static int32_t get_offset_of__cearoselKirstuqow_15() { return static_cast<int32_t>(offsetof(M_malwe240_t2697975704, ____cearoselKirstuqow_15)); }
	inline int32_t get__cearoselKirstuqow_15() const { return ____cearoselKirstuqow_15; }
	inline int32_t* get_address_of__cearoselKirstuqow_15() { return &____cearoselKirstuqow_15; }
	inline void set__cearoselKirstuqow_15(int32_t value)
	{
		____cearoselKirstuqow_15 = value;
	}

	inline static int32_t get_offset_of__cereegi_16() { return static_cast<int32_t>(offsetof(M_malwe240_t2697975704, ____cereegi_16)); }
	inline String_t* get__cereegi_16() const { return ____cereegi_16; }
	inline String_t** get_address_of__cereegi_16() { return &____cereegi_16; }
	inline void set__cereegi_16(String_t* value)
	{
		____cereegi_16 = value;
		Il2CppCodeGenWriteBarrier(&____cereegi_16, value);
	}

	inline static int32_t get_offset_of__qimar_17() { return static_cast<int32_t>(offsetof(M_malwe240_t2697975704, ____qimar_17)); }
	inline bool get__qimar_17() const { return ____qimar_17; }
	inline bool* get_address_of__qimar_17() { return &____qimar_17; }
	inline void set__qimar_17(bool value)
	{
		____qimar_17 = value;
	}

	inline static int32_t get_offset_of__hoolejeTreako_18() { return static_cast<int32_t>(offsetof(M_malwe240_t2697975704, ____hoolejeTreako_18)); }
	inline uint32_t get__hoolejeTreako_18() const { return ____hoolejeTreako_18; }
	inline uint32_t* get_address_of__hoolejeTreako_18() { return &____hoolejeTreako_18; }
	inline void set__hoolejeTreako_18(uint32_t value)
	{
		____hoolejeTreako_18 = value;
	}

	inline static int32_t get_offset_of__doryawhay_19() { return static_cast<int32_t>(offsetof(M_malwe240_t2697975704, ____doryawhay_19)); }
	inline String_t* get__doryawhay_19() const { return ____doryawhay_19; }
	inline String_t** get_address_of__doryawhay_19() { return &____doryawhay_19; }
	inline void set__doryawhay_19(String_t* value)
	{
		____doryawhay_19 = value;
		Il2CppCodeGenWriteBarrier(&____doryawhay_19, value);
	}

	inline static int32_t get_offset_of__stairjair_20() { return static_cast<int32_t>(offsetof(M_malwe240_t2697975704, ____stairjair_20)); }
	inline bool get__stairjair_20() const { return ____stairjair_20; }
	inline bool* get_address_of__stairjair_20() { return &____stairjair_20; }
	inline void set__stairjair_20(bool value)
	{
		____stairjair_20 = value;
	}

	inline static int32_t get_offset_of__tounee_21() { return static_cast<int32_t>(offsetof(M_malwe240_t2697975704, ____tounee_21)); }
	inline bool get__tounee_21() const { return ____tounee_21; }
	inline bool* get_address_of__tounee_21() { return &____tounee_21; }
	inline void set__tounee_21(bool value)
	{
		____tounee_21 = value;
	}

	inline static int32_t get_offset_of__gorsi_22() { return static_cast<int32_t>(offsetof(M_malwe240_t2697975704, ____gorsi_22)); }
	inline float get__gorsi_22() const { return ____gorsi_22; }
	inline float* get_address_of__gorsi_22() { return &____gorsi_22; }
	inline void set__gorsi_22(float value)
	{
		____gorsi_22 = value;
	}

	inline static int32_t get_offset_of__genaleaLucow_23() { return static_cast<int32_t>(offsetof(M_malwe240_t2697975704, ____genaleaLucow_23)); }
	inline bool get__genaleaLucow_23() const { return ____genaleaLucow_23; }
	inline bool* get_address_of__genaleaLucow_23() { return &____genaleaLucow_23; }
	inline void set__genaleaLucow_23(bool value)
	{
		____genaleaLucow_23 = value;
	}

	inline static int32_t get_offset_of__herowRepou_24() { return static_cast<int32_t>(offsetof(M_malwe240_t2697975704, ____herowRepou_24)); }
	inline uint32_t get__herowRepou_24() const { return ____herowRepou_24; }
	inline uint32_t* get_address_of__herowRepou_24() { return &____herowRepou_24; }
	inline void set__herowRepou_24(uint32_t value)
	{
		____herowRepou_24 = value;
	}

	inline static int32_t get_offset_of__woujiTrara_25() { return static_cast<int32_t>(offsetof(M_malwe240_t2697975704, ____woujiTrara_25)); }
	inline bool get__woujiTrara_25() const { return ____woujiTrara_25; }
	inline bool* get_address_of__woujiTrara_25() { return &____woujiTrara_25; }
	inline void set__woujiTrara_25(bool value)
	{
		____woujiTrara_25 = value;
	}

	inline static int32_t get_offset_of__curou_26() { return static_cast<int32_t>(offsetof(M_malwe240_t2697975704, ____curou_26)); }
	inline uint32_t get__curou_26() const { return ____curou_26; }
	inline uint32_t* get_address_of__curou_26() { return &____curou_26; }
	inline void set__curou_26(uint32_t value)
	{
		____curou_26 = value;
	}

	inline static int32_t get_offset_of__bawmou_27() { return static_cast<int32_t>(offsetof(M_malwe240_t2697975704, ____bawmou_27)); }
	inline float get__bawmou_27() const { return ____bawmou_27; }
	inline float* get_address_of__bawmou_27() { return &____bawmou_27; }
	inline void set__bawmou_27(float value)
	{
		____bawmou_27 = value;
	}

	inline static int32_t get_offset_of__najalJaxaldas_28() { return static_cast<int32_t>(offsetof(M_malwe240_t2697975704, ____najalJaxaldas_28)); }
	inline uint32_t get__najalJaxaldas_28() const { return ____najalJaxaldas_28; }
	inline uint32_t* get_address_of__najalJaxaldas_28() { return &____najalJaxaldas_28; }
	inline void set__najalJaxaldas_28(uint32_t value)
	{
		____najalJaxaldas_28 = value;
	}

	inline static int32_t get_offset_of__nadooCasar_29() { return static_cast<int32_t>(offsetof(M_malwe240_t2697975704, ____nadooCasar_29)); }
	inline float get__nadooCasar_29() const { return ____nadooCasar_29; }
	inline float* get_address_of__nadooCasar_29() { return &____nadooCasar_29; }
	inline void set__nadooCasar_29(float value)
	{
		____nadooCasar_29 = value;
	}

	inline static int32_t get_offset_of__powjiRelsasju_30() { return static_cast<int32_t>(offsetof(M_malwe240_t2697975704, ____powjiRelsasju_30)); }
	inline bool get__powjiRelsasju_30() const { return ____powjiRelsasju_30; }
	inline bool* get_address_of__powjiRelsasju_30() { return &____powjiRelsasju_30; }
	inline void set__powjiRelsasju_30(bool value)
	{
		____powjiRelsasju_30 = value;
	}

	inline static int32_t get_offset_of__nateeceJasemsem_31() { return static_cast<int32_t>(offsetof(M_malwe240_t2697975704, ____nateeceJasemsem_31)); }
	inline float get__nateeceJasemsem_31() const { return ____nateeceJasemsem_31; }
	inline float* get_address_of__nateeceJasemsem_31() { return &____nateeceJasemsem_31; }
	inline void set__nateeceJasemsem_31(float value)
	{
		____nateeceJasemsem_31 = value;
	}

	inline static int32_t get_offset_of__pesiZasordi_32() { return static_cast<int32_t>(offsetof(M_malwe240_t2697975704, ____pesiZasordi_32)); }
	inline String_t* get__pesiZasordi_32() const { return ____pesiZasordi_32; }
	inline String_t** get_address_of__pesiZasordi_32() { return &____pesiZasordi_32; }
	inline void set__pesiZasordi_32(String_t* value)
	{
		____pesiZasordi_32 = value;
		Il2CppCodeGenWriteBarrier(&____pesiZasordi_32, value);
	}

	inline static int32_t get_offset_of__lowtri_33() { return static_cast<int32_t>(offsetof(M_malwe240_t2697975704, ____lowtri_33)); }
	inline uint32_t get__lowtri_33() const { return ____lowtri_33; }
	inline uint32_t* get_address_of__lowtri_33() { return &____lowtri_33; }
	inline void set__lowtri_33(uint32_t value)
	{
		____lowtri_33 = value;
	}

	inline static int32_t get_offset_of__qecorcou_34() { return static_cast<int32_t>(offsetof(M_malwe240_t2697975704, ____qecorcou_34)); }
	inline int32_t get__qecorcou_34() const { return ____qecorcou_34; }
	inline int32_t* get_address_of__qecorcou_34() { return &____qecorcou_34; }
	inline void set__qecorcou_34(int32_t value)
	{
		____qecorcou_34 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
