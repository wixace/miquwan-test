﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_parmainouMaysalhas144
struct M_parmainouMaysalhas144_t3284296656;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_parmainouMaysalhas13284296656.h"

// System.Void GarbageiOS.M_parmainouMaysalhas144::.ctor()
extern "C"  void M_parmainouMaysalhas144__ctor_m9741203 (M_parmainouMaysalhas144_t3284296656 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_parmainouMaysalhas144::M_dearcheTotroofer0(System.String[],System.Int32)
extern "C"  void M_parmainouMaysalhas144_M_dearcheTotroofer0_m3530498938 (M_parmainouMaysalhas144_t3284296656 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_parmainouMaysalhas144::M_ralawru1(System.String[],System.Int32)
extern "C"  void M_parmainouMaysalhas144_M_ralawru1_m3154590261 (M_parmainouMaysalhas144_t3284296656 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_parmainouMaysalhas144::M_geebereki2(System.String[],System.Int32)
extern "C"  void M_parmainouMaysalhas144_M_geebereki2_m2575404113 (M_parmainouMaysalhas144_t3284296656 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_parmainouMaysalhas144::M_zearboFelball3(System.String[],System.Int32)
extern "C"  void M_parmainouMaysalhas144_M_zearboFelball3_m3127129418 (M_parmainouMaysalhas144_t3284296656 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_parmainouMaysalhas144::M_sedoKorselbis4(System.String[],System.Int32)
extern "C"  void M_parmainouMaysalhas144_M_sedoKorselbis4_m947672267 (M_parmainouMaysalhas144_t3284296656 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_parmainouMaysalhas144::ilo_M_dearcheTotroofer01(GarbageiOS.M_parmainouMaysalhas144,System.String[],System.Int32)
extern "C"  void M_parmainouMaysalhas144_ilo_M_dearcheTotroofer01_m1080843970 (Il2CppObject * __this /* static, unused */, M_parmainouMaysalhas144_t3284296656 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_parmainouMaysalhas144::ilo_M_ralawru12(GarbageiOS.M_parmainouMaysalhas144,System.String[],System.Int32)
extern "C"  void M_parmainouMaysalhas144_ilo_M_ralawru12_m2677552674 (Il2CppObject * __this /* static, unused */, M_parmainouMaysalhas144_t3284296656 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_parmainouMaysalhas144::ilo_M_zearboFelball33(GarbageiOS.M_parmainouMaysalhas144,System.String[],System.Int32)
extern "C"  void M_parmainouMaysalhas144_ilo_M_zearboFelball33_m3756277910 (Il2CppObject * __this /* static, unused */, M_parmainouMaysalhas144_t3284296656 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
