﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_rotalsowLeawhuqe160
struct M_rotalsowLeawhuqe160_t438113858;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"

// System.Void GarbageiOS.M_rotalsowLeawhuqe160::.ctor()
extern "C"  void M_rotalsowLeawhuqe160__ctor_m301578081 (M_rotalsowLeawhuqe160_t438113858 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_rotalsowLeawhuqe160::M_durhoo0(System.String[],System.Int32)
extern "C"  void M_rotalsowLeawhuqe160_M_durhoo0_m763311757 (M_rotalsowLeawhuqe160_t438113858 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_rotalsowLeawhuqe160::M_tutairTilis1(System.String[],System.Int32)
extern "C"  void M_rotalsowLeawhuqe160_M_tutairTilis1_m4059124307 (M_rotalsowLeawhuqe160_t438113858 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
