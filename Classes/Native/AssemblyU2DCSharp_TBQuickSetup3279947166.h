﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;
// ScreenRaycaster
struct ScreenRaycaster_t4188861866;
// FingerDownDetector
struct FingerDownDetector_t1553505457;
// FingerUpDetector
struct FingerUpDetector_t544994282;
// FingerHoverDetector
struct FingerHoverDetector_t92426265;
// FingerMotionDetector
struct FingerMotionDetector_t1792212357;
// DragRecognizer
struct DragRecognizer_t2550591000;
// LongPressRecognizer
struct LongPressRecognizer_t1602536459;
// SwipeRecognizer
struct SwipeRecognizer_t2690489438;
// TapRecognizer
struct TapRecognizer_t3788301703;
// PinchRecognizer
struct PinchRecognizer_t1004677598;
// TwistRecognizer
struct TwistRecognizer_t3400822795;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TBQuickSetup
struct  TBQuickSetup_t3279947166  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.GameObject TBQuickSetup::MessageTarget
	GameObject_t3674682005 * ___MessageTarget_2;
	// System.Int32 TBQuickSetup::MaxSimultaneousGestures
	int32_t ___MaxSimultaneousGestures_3;
	// ScreenRaycaster TBQuickSetup::screenRaycaster
	ScreenRaycaster_t4188861866 * ___screenRaycaster_4;
	// FingerDownDetector TBQuickSetup::<FingerDown>k__BackingField
	FingerDownDetector_t1553505457 * ___U3CFingerDownU3Ek__BackingField_5;
	// FingerUpDetector TBQuickSetup::<FingerUp>k__BackingField
	FingerUpDetector_t544994282 * ___U3CFingerUpU3Ek__BackingField_6;
	// FingerHoverDetector TBQuickSetup::<FingerHover>k__BackingField
	FingerHoverDetector_t92426265 * ___U3CFingerHoverU3Ek__BackingField_7;
	// FingerMotionDetector TBQuickSetup::<FingerMotion>k__BackingField
	FingerMotionDetector_t1792212357 * ___U3CFingerMotionU3Ek__BackingField_8;
	// DragRecognizer TBQuickSetup::<Drag>k__BackingField
	DragRecognizer_t2550591000 * ___U3CDragU3Ek__BackingField_9;
	// LongPressRecognizer TBQuickSetup::<LongPress>k__BackingField
	LongPressRecognizer_t1602536459 * ___U3CLongPressU3Ek__BackingField_10;
	// SwipeRecognizer TBQuickSetup::<Swipe>k__BackingField
	SwipeRecognizer_t2690489438 * ___U3CSwipeU3Ek__BackingField_11;
	// TapRecognizer TBQuickSetup::<Tap>k__BackingField
	TapRecognizer_t3788301703 * ___U3CTapU3Ek__BackingField_12;
	// TapRecognizer TBQuickSetup::<DoubleTap>k__BackingField
	TapRecognizer_t3788301703 * ___U3CDoubleTapU3Ek__BackingField_13;
	// PinchRecognizer TBQuickSetup::<Pinch>k__BackingField
	PinchRecognizer_t1004677598 * ___U3CPinchU3Ek__BackingField_14;
	// TwistRecognizer TBQuickSetup::<Twist>k__BackingField
	TwistRecognizer_t3400822795 * ___U3CTwistU3Ek__BackingField_15;
	// DragRecognizer TBQuickSetup::<TwoFingerDrag>k__BackingField
	DragRecognizer_t2550591000 * ___U3CTwoFingerDragU3Ek__BackingField_16;
	// TapRecognizer TBQuickSetup::<TwoFingerTap>k__BackingField
	TapRecognizer_t3788301703 * ___U3CTwoFingerTapU3Ek__BackingField_17;
	// SwipeRecognizer TBQuickSetup::<TwoFingerSwipe>k__BackingField
	SwipeRecognizer_t2690489438 * ___U3CTwoFingerSwipeU3Ek__BackingField_18;
	// LongPressRecognizer TBQuickSetup::<TwoFingerLongPress>k__BackingField
	LongPressRecognizer_t1602536459 * ___U3CTwoFingerLongPressU3Ek__BackingField_19;

public:
	inline static int32_t get_offset_of_MessageTarget_2() { return static_cast<int32_t>(offsetof(TBQuickSetup_t3279947166, ___MessageTarget_2)); }
	inline GameObject_t3674682005 * get_MessageTarget_2() const { return ___MessageTarget_2; }
	inline GameObject_t3674682005 ** get_address_of_MessageTarget_2() { return &___MessageTarget_2; }
	inline void set_MessageTarget_2(GameObject_t3674682005 * value)
	{
		___MessageTarget_2 = value;
		Il2CppCodeGenWriteBarrier(&___MessageTarget_2, value);
	}

	inline static int32_t get_offset_of_MaxSimultaneousGestures_3() { return static_cast<int32_t>(offsetof(TBQuickSetup_t3279947166, ___MaxSimultaneousGestures_3)); }
	inline int32_t get_MaxSimultaneousGestures_3() const { return ___MaxSimultaneousGestures_3; }
	inline int32_t* get_address_of_MaxSimultaneousGestures_3() { return &___MaxSimultaneousGestures_3; }
	inline void set_MaxSimultaneousGestures_3(int32_t value)
	{
		___MaxSimultaneousGestures_3 = value;
	}

	inline static int32_t get_offset_of_screenRaycaster_4() { return static_cast<int32_t>(offsetof(TBQuickSetup_t3279947166, ___screenRaycaster_4)); }
	inline ScreenRaycaster_t4188861866 * get_screenRaycaster_4() const { return ___screenRaycaster_4; }
	inline ScreenRaycaster_t4188861866 ** get_address_of_screenRaycaster_4() { return &___screenRaycaster_4; }
	inline void set_screenRaycaster_4(ScreenRaycaster_t4188861866 * value)
	{
		___screenRaycaster_4 = value;
		Il2CppCodeGenWriteBarrier(&___screenRaycaster_4, value);
	}

	inline static int32_t get_offset_of_U3CFingerDownU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(TBQuickSetup_t3279947166, ___U3CFingerDownU3Ek__BackingField_5)); }
	inline FingerDownDetector_t1553505457 * get_U3CFingerDownU3Ek__BackingField_5() const { return ___U3CFingerDownU3Ek__BackingField_5; }
	inline FingerDownDetector_t1553505457 ** get_address_of_U3CFingerDownU3Ek__BackingField_5() { return &___U3CFingerDownU3Ek__BackingField_5; }
	inline void set_U3CFingerDownU3Ek__BackingField_5(FingerDownDetector_t1553505457 * value)
	{
		___U3CFingerDownU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CFingerDownU3Ek__BackingField_5, value);
	}

	inline static int32_t get_offset_of_U3CFingerUpU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(TBQuickSetup_t3279947166, ___U3CFingerUpU3Ek__BackingField_6)); }
	inline FingerUpDetector_t544994282 * get_U3CFingerUpU3Ek__BackingField_6() const { return ___U3CFingerUpU3Ek__BackingField_6; }
	inline FingerUpDetector_t544994282 ** get_address_of_U3CFingerUpU3Ek__BackingField_6() { return &___U3CFingerUpU3Ek__BackingField_6; }
	inline void set_U3CFingerUpU3Ek__BackingField_6(FingerUpDetector_t544994282 * value)
	{
		___U3CFingerUpU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CFingerUpU3Ek__BackingField_6, value);
	}

	inline static int32_t get_offset_of_U3CFingerHoverU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(TBQuickSetup_t3279947166, ___U3CFingerHoverU3Ek__BackingField_7)); }
	inline FingerHoverDetector_t92426265 * get_U3CFingerHoverU3Ek__BackingField_7() const { return ___U3CFingerHoverU3Ek__BackingField_7; }
	inline FingerHoverDetector_t92426265 ** get_address_of_U3CFingerHoverU3Ek__BackingField_7() { return &___U3CFingerHoverU3Ek__BackingField_7; }
	inline void set_U3CFingerHoverU3Ek__BackingField_7(FingerHoverDetector_t92426265 * value)
	{
		___U3CFingerHoverU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CFingerHoverU3Ek__BackingField_7, value);
	}

	inline static int32_t get_offset_of_U3CFingerMotionU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(TBQuickSetup_t3279947166, ___U3CFingerMotionU3Ek__BackingField_8)); }
	inline FingerMotionDetector_t1792212357 * get_U3CFingerMotionU3Ek__BackingField_8() const { return ___U3CFingerMotionU3Ek__BackingField_8; }
	inline FingerMotionDetector_t1792212357 ** get_address_of_U3CFingerMotionU3Ek__BackingField_8() { return &___U3CFingerMotionU3Ek__BackingField_8; }
	inline void set_U3CFingerMotionU3Ek__BackingField_8(FingerMotionDetector_t1792212357 * value)
	{
		___U3CFingerMotionU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CFingerMotionU3Ek__BackingField_8, value);
	}

	inline static int32_t get_offset_of_U3CDragU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(TBQuickSetup_t3279947166, ___U3CDragU3Ek__BackingField_9)); }
	inline DragRecognizer_t2550591000 * get_U3CDragU3Ek__BackingField_9() const { return ___U3CDragU3Ek__BackingField_9; }
	inline DragRecognizer_t2550591000 ** get_address_of_U3CDragU3Ek__BackingField_9() { return &___U3CDragU3Ek__BackingField_9; }
	inline void set_U3CDragU3Ek__BackingField_9(DragRecognizer_t2550591000 * value)
	{
		___U3CDragU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CDragU3Ek__BackingField_9, value);
	}

	inline static int32_t get_offset_of_U3CLongPressU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(TBQuickSetup_t3279947166, ___U3CLongPressU3Ek__BackingField_10)); }
	inline LongPressRecognizer_t1602536459 * get_U3CLongPressU3Ek__BackingField_10() const { return ___U3CLongPressU3Ek__BackingField_10; }
	inline LongPressRecognizer_t1602536459 ** get_address_of_U3CLongPressU3Ek__BackingField_10() { return &___U3CLongPressU3Ek__BackingField_10; }
	inline void set_U3CLongPressU3Ek__BackingField_10(LongPressRecognizer_t1602536459 * value)
	{
		___U3CLongPressU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CLongPressU3Ek__BackingField_10, value);
	}

	inline static int32_t get_offset_of_U3CSwipeU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(TBQuickSetup_t3279947166, ___U3CSwipeU3Ek__BackingField_11)); }
	inline SwipeRecognizer_t2690489438 * get_U3CSwipeU3Ek__BackingField_11() const { return ___U3CSwipeU3Ek__BackingField_11; }
	inline SwipeRecognizer_t2690489438 ** get_address_of_U3CSwipeU3Ek__BackingField_11() { return &___U3CSwipeU3Ek__BackingField_11; }
	inline void set_U3CSwipeU3Ek__BackingField_11(SwipeRecognizer_t2690489438 * value)
	{
		___U3CSwipeU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CSwipeU3Ek__BackingField_11, value);
	}

	inline static int32_t get_offset_of_U3CTapU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(TBQuickSetup_t3279947166, ___U3CTapU3Ek__BackingField_12)); }
	inline TapRecognizer_t3788301703 * get_U3CTapU3Ek__BackingField_12() const { return ___U3CTapU3Ek__BackingField_12; }
	inline TapRecognizer_t3788301703 ** get_address_of_U3CTapU3Ek__BackingField_12() { return &___U3CTapU3Ek__BackingField_12; }
	inline void set_U3CTapU3Ek__BackingField_12(TapRecognizer_t3788301703 * value)
	{
		___U3CTapU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3CTapU3Ek__BackingField_12, value);
	}

	inline static int32_t get_offset_of_U3CDoubleTapU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(TBQuickSetup_t3279947166, ___U3CDoubleTapU3Ek__BackingField_13)); }
	inline TapRecognizer_t3788301703 * get_U3CDoubleTapU3Ek__BackingField_13() const { return ___U3CDoubleTapU3Ek__BackingField_13; }
	inline TapRecognizer_t3788301703 ** get_address_of_U3CDoubleTapU3Ek__BackingField_13() { return &___U3CDoubleTapU3Ek__BackingField_13; }
	inline void set_U3CDoubleTapU3Ek__BackingField_13(TapRecognizer_t3788301703 * value)
	{
		___U3CDoubleTapU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier(&___U3CDoubleTapU3Ek__BackingField_13, value);
	}

	inline static int32_t get_offset_of_U3CPinchU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(TBQuickSetup_t3279947166, ___U3CPinchU3Ek__BackingField_14)); }
	inline PinchRecognizer_t1004677598 * get_U3CPinchU3Ek__BackingField_14() const { return ___U3CPinchU3Ek__BackingField_14; }
	inline PinchRecognizer_t1004677598 ** get_address_of_U3CPinchU3Ek__BackingField_14() { return &___U3CPinchU3Ek__BackingField_14; }
	inline void set_U3CPinchU3Ek__BackingField_14(PinchRecognizer_t1004677598 * value)
	{
		___U3CPinchU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier(&___U3CPinchU3Ek__BackingField_14, value);
	}

	inline static int32_t get_offset_of_U3CTwistU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(TBQuickSetup_t3279947166, ___U3CTwistU3Ek__BackingField_15)); }
	inline TwistRecognizer_t3400822795 * get_U3CTwistU3Ek__BackingField_15() const { return ___U3CTwistU3Ek__BackingField_15; }
	inline TwistRecognizer_t3400822795 ** get_address_of_U3CTwistU3Ek__BackingField_15() { return &___U3CTwistU3Ek__BackingField_15; }
	inline void set_U3CTwistU3Ek__BackingField_15(TwistRecognizer_t3400822795 * value)
	{
		___U3CTwistU3Ek__BackingField_15 = value;
		Il2CppCodeGenWriteBarrier(&___U3CTwistU3Ek__BackingField_15, value);
	}

	inline static int32_t get_offset_of_U3CTwoFingerDragU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(TBQuickSetup_t3279947166, ___U3CTwoFingerDragU3Ek__BackingField_16)); }
	inline DragRecognizer_t2550591000 * get_U3CTwoFingerDragU3Ek__BackingField_16() const { return ___U3CTwoFingerDragU3Ek__BackingField_16; }
	inline DragRecognizer_t2550591000 ** get_address_of_U3CTwoFingerDragU3Ek__BackingField_16() { return &___U3CTwoFingerDragU3Ek__BackingField_16; }
	inline void set_U3CTwoFingerDragU3Ek__BackingField_16(DragRecognizer_t2550591000 * value)
	{
		___U3CTwoFingerDragU3Ek__BackingField_16 = value;
		Il2CppCodeGenWriteBarrier(&___U3CTwoFingerDragU3Ek__BackingField_16, value);
	}

	inline static int32_t get_offset_of_U3CTwoFingerTapU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(TBQuickSetup_t3279947166, ___U3CTwoFingerTapU3Ek__BackingField_17)); }
	inline TapRecognizer_t3788301703 * get_U3CTwoFingerTapU3Ek__BackingField_17() const { return ___U3CTwoFingerTapU3Ek__BackingField_17; }
	inline TapRecognizer_t3788301703 ** get_address_of_U3CTwoFingerTapU3Ek__BackingField_17() { return &___U3CTwoFingerTapU3Ek__BackingField_17; }
	inline void set_U3CTwoFingerTapU3Ek__BackingField_17(TapRecognizer_t3788301703 * value)
	{
		___U3CTwoFingerTapU3Ek__BackingField_17 = value;
		Il2CppCodeGenWriteBarrier(&___U3CTwoFingerTapU3Ek__BackingField_17, value);
	}

	inline static int32_t get_offset_of_U3CTwoFingerSwipeU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(TBQuickSetup_t3279947166, ___U3CTwoFingerSwipeU3Ek__BackingField_18)); }
	inline SwipeRecognizer_t2690489438 * get_U3CTwoFingerSwipeU3Ek__BackingField_18() const { return ___U3CTwoFingerSwipeU3Ek__BackingField_18; }
	inline SwipeRecognizer_t2690489438 ** get_address_of_U3CTwoFingerSwipeU3Ek__BackingField_18() { return &___U3CTwoFingerSwipeU3Ek__BackingField_18; }
	inline void set_U3CTwoFingerSwipeU3Ek__BackingField_18(SwipeRecognizer_t2690489438 * value)
	{
		___U3CTwoFingerSwipeU3Ek__BackingField_18 = value;
		Il2CppCodeGenWriteBarrier(&___U3CTwoFingerSwipeU3Ek__BackingField_18, value);
	}

	inline static int32_t get_offset_of_U3CTwoFingerLongPressU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(TBQuickSetup_t3279947166, ___U3CTwoFingerLongPressU3Ek__BackingField_19)); }
	inline LongPressRecognizer_t1602536459 * get_U3CTwoFingerLongPressU3Ek__BackingField_19() const { return ___U3CTwoFingerLongPressU3Ek__BackingField_19; }
	inline LongPressRecognizer_t1602536459 ** get_address_of_U3CTwoFingerLongPressU3Ek__BackingField_19() { return &___U3CTwoFingerLongPressU3Ek__BackingField_19; }
	inline void set_U3CTwoFingerLongPressU3Ek__BackingField_19(LongPressRecognizer_t1602536459 * value)
	{
		___U3CTwoFingerLongPressU3Ek__BackingField_19 = value;
		Il2CppCodeGenWriteBarrier(&___U3CTwoFingerLongPressU3Ek__BackingField_19, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
