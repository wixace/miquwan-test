﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24066860316MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,taixuTipCfg>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m3806304778(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t1164766315 *, int32_t, taixuTipCfg_t1268722370 *, const MethodInfo*))KeyValuePair_2__ctor_m11197230_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,taixuTipCfg>::get_Key()
#define KeyValuePair_2_get_Key_m2270936734(__this, method) ((  int32_t (*) (KeyValuePair_2_t1164766315 *, const MethodInfo*))KeyValuePair_2_get_Key_m494458106_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,taixuTipCfg>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m3181781471(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t1164766315 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m4229413435_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,taixuTipCfg>::get_Value()
#define KeyValuePair_2_get_Value_m3606465758(__this, method) ((  taixuTipCfg_t1268722370 * (*) (KeyValuePair_2_t1164766315 *, const MethodInfo*))KeyValuePair_2_get_Value_m1563175098_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,taixuTipCfg>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m1280274015(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t1164766315 *, taixuTipCfg_t1268722370 *, const MethodInfo*))KeyValuePair_2_set_Value_m1296398523_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,taixuTipCfg>::ToString()
#define KeyValuePair_2_ToString_m487093795(__this, method) ((  String_t* (*) (KeyValuePair_2_t1164766315 *, const MethodInfo*))KeyValuePair_2_ToString_m491888647_gshared)(__this, method)
