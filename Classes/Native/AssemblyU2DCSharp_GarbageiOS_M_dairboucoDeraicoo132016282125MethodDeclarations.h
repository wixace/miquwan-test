﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_dairboucoDeraicoo137
struct M_dairboucoDeraicoo137_t2016282125;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"

// System.Void GarbageiOS.M_dairboucoDeraicoo137::.ctor()
extern "C"  void M_dairboucoDeraicoo137__ctor_m880362182 (M_dairboucoDeraicoo137_t2016282125 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_dairboucoDeraicoo137::M_basse0(System.String[],System.Int32)
extern "C"  void M_dairboucoDeraicoo137_M_basse0_m1100968977 (M_dairboucoDeraicoo137_t2016282125 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
