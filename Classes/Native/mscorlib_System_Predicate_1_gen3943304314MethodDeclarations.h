﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Predicate_1_gen3781873254MethodDeclarations.h"

// System.Void System.Predicate`1<AIObject>::.ctor(System.Object,System.IntPtr)
#define Predicate_1__ctor_m432891988(__this, ___object0, ___method1, method) ((  void (*) (Predicate_1_t3943304314 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Predicate_1__ctor_m231416842_gshared)(__this, ___object0, ___method1, method)
// System.Boolean System.Predicate`1<AIObject>::Invoke(T)
#define Predicate_1_Invoke_m379029550(__this, ___obj0, method) ((  bool (*) (Predicate_1_t3943304314 *, AIObject_t37280135 *, const MethodInfo*))Predicate_1_Invoke_m1328901537_gshared)(__this, ___obj0, method)
// System.IAsyncResult System.Predicate`1<AIObject>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Predicate_1_BeginInvoke_m4169774397(__this, ___obj0, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Predicate_1_t3943304314 *, AIObject_t37280135 *, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))Predicate_1_BeginInvoke_m2038073176_gshared)(__this, ___obj0, ___callback1, ___object2, method)
// System.Boolean System.Predicate`1<AIObject>::EndInvoke(System.IAsyncResult)
#define Predicate_1_EndInvoke_m868828262(__this, ___result0, method) ((  bool (*) (Predicate_1_t3943304314 *, Il2CppObject *, const MethodInfo*))Predicate_1_EndInvoke_m3970497007_gshared)(__this, ___result0, method)
