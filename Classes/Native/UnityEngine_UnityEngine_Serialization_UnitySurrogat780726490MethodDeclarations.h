﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Serialization.UnitySurrogateSelector
struct UnitySurrogateSelector_t780726490;
// System.Runtime.Serialization.ISerializationSurrogate
struct ISerializationSurrogate_t132171319;
// System.Type
struct Type_t;
// System.Runtime.Serialization.ISurrogateSelector
struct ISurrogateSelector_t2300150170;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Type2863145774.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon2761351129.h"

// System.Void UnityEngine.Serialization.UnitySurrogateSelector::.ctor()
extern "C"  void UnitySurrogateSelector__ctor_m3965242029 (UnitySurrogateSelector_t780726490 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Serialization.ISerializationSurrogate UnityEngine.Serialization.UnitySurrogateSelector::GetSurrogate(System.Type,System.Runtime.Serialization.StreamingContext,System.Runtime.Serialization.ISurrogateSelector&)
extern "C"  Il2CppObject * UnitySurrogateSelector_GetSurrogate_m1626387878 (UnitySurrogateSelector_t780726490 * __this, Type_t * ___type0, StreamingContext_t2761351129  ___context1, Il2CppObject ** ___selector2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Serialization.UnitySurrogateSelector::ChainSelector(System.Runtime.Serialization.ISurrogateSelector)
extern "C"  void UnitySurrogateSelector_ChainSelector_m1112345606 (UnitySurrogateSelector_t780726490 * __this, Il2CppObject * ___selector0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Serialization.ISurrogateSelector UnityEngine.Serialization.UnitySurrogateSelector::GetNextSelector()
extern "C"  Il2CppObject * UnitySurrogateSelector_GetNextSelector_m2435567713 (UnitySurrogateSelector_t780726490 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
