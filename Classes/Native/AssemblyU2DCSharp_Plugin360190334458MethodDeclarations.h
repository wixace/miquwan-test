﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Plugin360
struct Plugin360_t190334458;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// System.String
struct String_t;
// CEvent.EventFunc`1<CEvent.ZEvent>
struct EventFunc_1_t1114914310;
// VersionMgr
struct VersionMgr_t1322950208;
// VersionInfo
struct VersionInfo_t2356638086;
// System.Object
struct Il2CppObject;
// PluginsSdkMgr
struct PluginsSdkMgr_t3884624670;
// System.Action
struct Action_t3771233898;
// FloatTextMgr
struct FloatTextMgr_t630384591;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_VersionMgr1322950208.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_PluginsSdkMgr3884624670.h"
#include "System_Core_System_Action3771233898.h"

// System.Void Plugin360::.ctor()
extern "C"  void Plugin360__ctor_m267970737 (Plugin360_t190334458 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Plugin360::Init()
extern "C"  void Plugin360_Init_m791464835 (Plugin360_t190334458 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> Plugin360::ReqSDKHttpLogin()
extern "C"  Dictionary_2_t827649927 * Plugin360_ReqSDKHttpLogin_m4106813538 (Plugin360_t190334458 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Plugin360::IsLoginSuccess(System.Boolean)
extern "C"  bool Plugin360_IsLoginSuccess_m3083459226 (Plugin360_t190334458 * __this, bool ___isOpenUseLogin0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Plugin360::OpenUserLogin()
extern "C"  void Plugin360_OpenUserLogin_m1503316483 (Plugin360_t190334458 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Plugin360::UserPay(CEvent.ZEvent)
extern "C"  void Plugin360_UserPay_m1066733583 (Plugin360_t190334458 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Plugin360::RecRoleUpLv(CEvent.ZEvent)
extern "C"  void Plugin360_RecRoleUpLv_m446631521 (Plugin360_t190334458 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Plugin360::RecGotoGame(CEvent.ZEvent)
extern "C"  void Plugin360_RecGotoGame_m49270343 (Plugin360_t190334458 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Plugin360::RecCreateRole(CEvent.ZEvent)
extern "C"  void Plugin360_RecCreateRole_m3176511626 (Plugin360_t190334458 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Plugin360::UpGameInfo(System.Object[])
extern "C"  void Plugin360_UpGameInfo_m2610234436 (Plugin360_t190334458 * __this, ObjectU5BU5D_t1108656482* ___datas0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Plugin360::OnInitSuccess(System.String)
extern "C"  void Plugin360_OnInitSuccess_m1122740127 (Plugin360_t190334458 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Plugin360::OnInitFail(System.String)
extern "C"  void Plugin360_OnInitFail_m3849389570 (Plugin360_t190334458 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Plugin360::OnLoginSuccess(System.String)
extern "C"  void Plugin360_OnLoginSuccess_m834917622 (Plugin360_t190334458 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Plugin360::OnLoginFail(System.String)
extern "C"  void Plugin360_OnLoginFail_m1996795915 (Plugin360_t190334458 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Plugin360::OnUserSwitchSuccess(System.String)
extern "C"  void Plugin360_OnUserSwitchSuccess_m3133755246 (Plugin360_t190334458 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Plugin360::OnPaySuccess(System.String)
extern "C"  void Plugin360_OnPaySuccess_m1986460853 (Plugin360_t190334458 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Plugin360::OnPayFail(System.String)
extern "C"  void Plugin360_OnPayFail_m3383605420 (Plugin360_t190334458 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Plugin360::<OnUserSwitchSuccess>m__404()
extern "C"  void Plugin360_U3COnUserSwitchSuccessU3Em__404_m2015924207 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Plugin360::ilo_AddEventListener1(System.String,CEvent.EventFunc`1<CEvent.ZEvent>)
extern "C"  void Plugin360_ilo_AddEventListener1_m3639719459 (Il2CppObject * __this /* static, unused */, String_t* ___eventType0, EventFunc_1_t1114914310 * ___fun1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionMgr Plugin360::ilo_get_Instance2()
extern "C"  VersionMgr_t1322950208 * Plugin360_ilo_get_Instance2_m1005944397 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionInfo Plugin360::ilo_get_currentVS3(VersionMgr)
extern "C"  VersionInfo_t2356638086 * Plugin360_ilo_get_currentVS3_m308720863 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Plugin360::ilo_Log4(System.Object,System.Boolean)
extern "C"  void Plugin360_ilo_Log4_m549912895 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Plugin360::ilo_ReqSDKHttpLogin5(PluginsSdkMgr)
extern "C"  void Plugin360_ilo_ReqSDKHttpLogin5_m2602545086 (Il2CppObject * __this /* static, unused */, PluginsSdkMgr_t3884624670 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Plugin360::ilo_Logout6(System.Action)
extern "C"  void Plugin360_ilo_Logout6_m2399299349 (Il2CppObject * __this /* static, unused */, Action_t3771233898 * ___call0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Plugin360::ilo_DispatchEvent7(CEvent.ZEvent)
extern "C"  void Plugin360_ilo_DispatchEvent7_m439804990 (Il2CppObject * __this /* static, unused */, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FloatTextMgr Plugin360::ilo_get_FloatTextMgr8()
extern "C"  FloatTextMgr_t630384591 * Plugin360_ilo_get_FloatTextMgr8_m1794682570 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
