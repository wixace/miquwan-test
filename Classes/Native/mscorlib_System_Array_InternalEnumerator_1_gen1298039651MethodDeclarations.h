﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1298039651.h"
#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DCSharp_Pathfinding_BinaryHeapM_Tuple2515696975.h"

// System.Void System.Array/InternalEnumerator`1<Pathfinding.BinaryHeapM/Tuple>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m503891860_gshared (InternalEnumerator_1_t1298039651 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m503891860(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t1298039651 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m503891860_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<Pathfinding.BinaryHeapM/Tuple>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3095002508_gshared (InternalEnumerator_1_t1298039651 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3095002508(__this, method) ((  void (*) (InternalEnumerator_1_t1298039651 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3095002508_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<Pathfinding.BinaryHeapM/Tuple>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2396571202_gshared (InternalEnumerator_1_t1298039651 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2396571202(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t1298039651 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2396571202_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Pathfinding.BinaryHeapM/Tuple>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3451583083_gshared (InternalEnumerator_1_t1298039651 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m3451583083(__this, method) ((  void (*) (InternalEnumerator_1_t1298039651 *, const MethodInfo*))InternalEnumerator_1_Dispose_m3451583083_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Pathfinding.BinaryHeapM/Tuple>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2570733756_gshared (InternalEnumerator_1_t1298039651 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m2570733756(__this, method) ((  bool (*) (InternalEnumerator_1_t1298039651 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m2570733756_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Pathfinding.BinaryHeapM/Tuple>::get_Current()
extern "C"  Tuple_t2515696975  InternalEnumerator_1_get_Current_m3050888061_gshared (InternalEnumerator_1_t1298039651 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m3050888061(__this, method) ((  Tuple_t2515696975  (*) (InternalEnumerator_1_t1298039651 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3050888061_gshared)(__this, method)
