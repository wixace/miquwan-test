﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen4068395185.h"
#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DCSharp_AstarDebugger_GraphPoint991085213.h"

// System.Void System.Array/InternalEnumerator`1<AstarDebugger/GraphPoint>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2159274478_gshared (InternalEnumerator_1_t4068395185 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m2159274478(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t4068395185 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m2159274478_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<AstarDebugger/GraphPoint>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m894236914_gshared (InternalEnumerator_1_t4068395185 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m894236914(__this, method) ((  void (*) (InternalEnumerator_1_t4068395185 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m894236914_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<AstarDebugger/GraphPoint>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1269841054_gshared (InternalEnumerator_1_t4068395185 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1269841054(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t4068395185 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1269841054_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<AstarDebugger/GraphPoint>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2556585285_gshared (InternalEnumerator_1_t4068395185 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m2556585285(__this, method) ((  void (*) (InternalEnumerator_1_t4068395185 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2556585285_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<AstarDebugger/GraphPoint>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2950531038_gshared (InternalEnumerator_1_t4068395185 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m2950531038(__this, method) ((  bool (*) (InternalEnumerator_1_t4068395185 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m2950531038_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<AstarDebugger/GraphPoint>::get_Current()
extern "C"  GraphPoint_t991085213  InternalEnumerator_1_get_Current_m858427893_gshared (InternalEnumerator_1_t4068395185 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m858427893(__this, method) ((  GraphPoint_t991085213  (*) (InternalEnumerator_1_t4068395185 *, const MethodInfo*))InternalEnumerator_1_get_Current_m858427893_gshared)(__this, method)
