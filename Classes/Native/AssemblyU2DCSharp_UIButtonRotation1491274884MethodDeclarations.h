﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIButtonRotation
struct UIButtonRotation_t1491274884;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// TweenRotation
struct TweenRotation_t2896252649;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "AssemblyU2DCSharp_UIButtonRotation1491274884.h"
#include "AssemblyU2DCSharp_TweenRotation2896252649.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882.h"

// System.Void UIButtonRotation::.ctor()
extern "C"  void UIButtonRotation__ctor_m1174269591 (UIButtonRotation_t1491274884 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonRotation::Start()
extern "C"  void UIButtonRotation_Start_m121407383 (UIButtonRotation_t1491274884 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonRotation::OnEnable()
extern "C"  void UIButtonRotation_OnEnable_m2228338063 (UIButtonRotation_t1491274884 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonRotation::OnDisable()
extern "C"  void UIButtonRotation_OnDisable_m799940478 (UIButtonRotation_t1491274884 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonRotation::OnPress(System.Boolean)
extern "C"  void UIButtonRotation_OnPress_m2443214096 (UIButtonRotation_t1491274884 * __this, bool ___isPressed0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonRotation::OnHover(System.Boolean)
extern "C"  void UIButtonRotation_OnHover_m228560905 (UIButtonRotation_t1491274884 * __this, bool ___isOver0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonRotation::OnSelect(System.Boolean)
extern "C"  void UIButtonRotation_OnSelect_m2351958591 (UIButtonRotation_t1491274884 * __this, bool ___isSelected0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIButtonRotation::ilo_IsHighlighted1(UnityEngine.GameObject)
extern "C"  bool UIButtonRotation_ilo_IsHighlighted1_m1790772148 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonRotation::ilo_OnHover2(UIButtonRotation,System.Boolean)
extern "C"  void UIButtonRotation_ilo_OnHover2_m1138971948 (Il2CppObject * __this /* static, unused */, UIButtonRotation_t1491274884 * ____this0, bool ___isOver1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonRotation::ilo_set_value3(TweenRotation,UnityEngine.Quaternion)
extern "C"  void UIButtonRotation_ilo_set_value3_m4020374599 (Il2CppObject * __this /* static, unused */, TweenRotation_t2896252649 * ____this0, Quaternion_t1553702882  ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TweenRotation UIButtonRotation::ilo_Begin4(UnityEngine.GameObject,System.Single,UnityEngine.Quaternion)
extern "C"  TweenRotation_t2896252649 * UIButtonRotation_ilo_Begin4_m1890268223 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___go0, float ___duration1, Quaternion_t1553702882  ___rot2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonRotation::ilo_Start5(UIButtonRotation)
extern "C"  void UIButtonRotation_ilo_Start5_m3188937039 (Il2CppObject * __this /* static, unused */, UIButtonRotation_t1491274884 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
