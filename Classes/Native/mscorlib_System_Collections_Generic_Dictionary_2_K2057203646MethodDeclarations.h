﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<MScrollView/MoveWay,System.Object>
struct Dictionary_2_t1442267592;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K2057203646.h"
#include "AssemblyU2DCSharp_MScrollView_MoveWay3474941550.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<MScrollView/MoveWay,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m1397839811_gshared (Enumerator_t2057203646 * __this, Dictionary_2_t1442267592 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m1397839811(__this, ___host0, method) ((  void (*) (Enumerator_t2057203646 *, Dictionary_2_t1442267592 *, const MethodInfo*))Enumerator__ctor_m1397839811_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<MScrollView/MoveWay,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1336502718_gshared (Enumerator_t2057203646 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1336502718(__this, method) ((  Il2CppObject * (*) (Enumerator_t2057203646 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1336502718_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<MScrollView/MoveWay,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3126166802_gshared (Enumerator_t2057203646 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m3126166802(__this, method) ((  void (*) (Enumerator_t2057203646 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3126166802_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<MScrollView/MoveWay,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m2567202085_gshared (Enumerator_t2057203646 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m2567202085(__this, method) ((  void (*) (Enumerator_t2057203646 *, const MethodInfo*))Enumerator_Dispose_m2567202085_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<MScrollView/MoveWay,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m426339838_gshared (Enumerator_t2057203646 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m426339838(__this, method) ((  bool (*) (Enumerator_t2057203646 *, const MethodInfo*))Enumerator_MoveNext_m426339838_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<MScrollView/MoveWay,System.Object>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m1944119830_gshared (Enumerator_t2057203646 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m1944119830(__this, method) ((  int32_t (*) (Enumerator_t2057203646 *, const MethodInfo*))Enumerator_get_Current_m1944119830_gshared)(__this, method)
