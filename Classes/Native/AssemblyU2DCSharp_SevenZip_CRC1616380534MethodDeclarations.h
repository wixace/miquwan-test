﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SevenZip.CRC
struct CRC_t1616380534;
// System.Byte[]
struct ByteU5BU5D_t4260760469;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SevenZip_CRC1616380534.h"

// System.Void SevenZip.CRC::.ctor()
extern "C"  void CRC__ctor_m2082464545 (CRC_t1616380534 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.CRC::.cctor()
extern "C"  void CRC__cctor_m3944762540 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.CRC::Init()
extern "C"  void CRC_Init_m2651112211 (CRC_t1616380534 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.CRC::UpdateByte(System.Byte)
extern "C"  void CRC_UpdateByte_m3116240183 (CRC_t1616380534 * __this, uint8_t ___b0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.CRC::Update(System.Byte[],System.UInt32,System.UInt32)
extern "C"  void CRC_Update_m3202915109 (CRC_t1616380534 * __this, ByteU5BU5D_t4260760469* ___data0, uint32_t ___offset1, uint32_t ___size2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 SevenZip.CRC::GetDigest()
extern "C"  uint32_t CRC_GetDigest_m2587732926 (CRC_t1616380534 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 SevenZip.CRC::CalculateDigest(System.Byte[],System.UInt32,System.UInt32)
extern "C"  uint32_t CRC_CalculateDigest_m400500707 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___data0, uint32_t ___offset1, uint32_t ___size2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SevenZip.CRC::VerifyDigest(System.UInt32,System.Byte[],System.UInt32,System.UInt32)
extern "C"  bool CRC_VerifyDigest_m1673803985 (Il2CppObject * __this /* static, unused */, uint32_t ___digest0, ByteU5BU5D_t4260760469* ___data1, uint32_t ___offset2, uint32_t ___size3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 SevenZip.CRC::ilo_GetDigest1(SevenZip.CRC)
extern "C"  uint32_t CRC_ilo_GetDigest1_m2443505934 (Il2CppObject * __this /* static, unused */, CRC_t1616380534 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 SevenZip.CRC::ilo_CalculateDigest2(System.Byte[],System.UInt32,System.UInt32)
extern "C"  uint32_t CRC_ilo_CalculateDigest2_m3909160728 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___data0, uint32_t ___offset1, uint32_t ___size2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
