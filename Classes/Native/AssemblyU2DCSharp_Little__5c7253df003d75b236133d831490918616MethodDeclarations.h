﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._5c7253df003d75b236133d834b152e12
struct _5c7253df003d75b236133d834b152e12_t1490918616;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__5c7253df003d75b236133d831490918616.h"

// System.Void Little._5c7253df003d75b236133d834b152e12::.ctor()
extern "C"  void _5c7253df003d75b236133d834b152e12__ctor_m3932978261 (_5c7253df003d75b236133d834b152e12_t1490918616 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._5c7253df003d75b236133d834b152e12::_5c7253df003d75b236133d834b152e12m2(System.Int32)
extern "C"  int32_t _5c7253df003d75b236133d834b152e12__5c7253df003d75b236133d834b152e12m2_m3536790169 (_5c7253df003d75b236133d834b152e12_t1490918616 * __this, int32_t ____5c7253df003d75b236133d834b152e12a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._5c7253df003d75b236133d834b152e12::_5c7253df003d75b236133d834b152e12m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _5c7253df003d75b236133d834b152e12__5c7253df003d75b236133d834b152e12m_m3757745213 (_5c7253df003d75b236133d834b152e12_t1490918616 * __this, int32_t ____5c7253df003d75b236133d834b152e12a0, int32_t ____5c7253df003d75b236133d834b152e12391, int32_t ____5c7253df003d75b236133d834b152e12c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._5c7253df003d75b236133d834b152e12::ilo__5c7253df003d75b236133d834b152e12m21(Little._5c7253df003d75b236133d834b152e12,System.Int32)
extern "C"  int32_t _5c7253df003d75b236133d834b152e12_ilo__5c7253df003d75b236133d834b152e12m21_m1748788639 (Il2CppObject * __this /* static, unused */, _5c7253df003d75b236133d834b152e12_t1490918616 * ____this0, int32_t ____5c7253df003d75b236133d834b152e12a1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
