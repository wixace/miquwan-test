﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_GameObjectGenerated
struct UnityEngine_GameObjectGenerated_t3843327062;
// JSVCall
struct JSVCall_t3708497963;
// System.Type[]
struct TypeU5BU5D_t3339007067;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Type
struct Type_t;
// MethodID
struct MethodID_t3916401116;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Type2863145774.h"
#include "AssemblyU2DCSharp_MethodID3916401116.h"

// System.Void UnityEngine_GameObjectGenerated::.ctor()
extern "C"  void UnityEngine_GameObjectGenerated__ctor_m1984492501 (UnityEngine_GameObjectGenerated_t3843327062 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GameObjectGenerated::.cctor()
extern "C"  void UnityEngine_GameObjectGenerated__cctor_m907629176 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GameObjectGenerated::GameObject_GameObject1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GameObjectGenerated_GameObject_GameObject1_m3481556893 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GameObjectGenerated::GameObject_GameObject2(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GameObjectGenerated_GameObject_GameObject2_m431354078 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GameObjectGenerated::GameObject_GameObject3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GameObjectGenerated_GameObject_GameObject3_m1676118559 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GameObjectGenerated::GameObject_transform(JSVCall)
extern "C"  void UnityEngine_GameObjectGenerated_GameObject_transform_m2694732602 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GameObjectGenerated::GameObject_layer(JSVCall)
extern "C"  void UnityEngine_GameObjectGenerated_GameObject_layer_m4293268213 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GameObjectGenerated::GameObject_activeSelf(JSVCall)
extern "C"  void UnityEngine_GameObjectGenerated_GameObject_activeSelf_m2919830196 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GameObjectGenerated::GameObject_activeInHierarchy(JSVCall)
extern "C"  void UnityEngine_GameObjectGenerated_GameObject_activeInHierarchy_m3947152572 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GameObjectGenerated::GameObject_isStatic(JSVCall)
extern "C"  void UnityEngine_GameObjectGenerated_GameObject_isStatic_m3952031918 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GameObjectGenerated::GameObject_tag(JSVCall)
extern "C"  void UnityEngine_GameObjectGenerated_GameObject_tag_m2915832524 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GameObjectGenerated::GameObject_scene(JSVCall)
extern "C"  void UnityEngine_GameObjectGenerated_GameObject_scene_m1147879962 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GameObjectGenerated::GameObject_gameObject(JSVCall)
extern "C"  void UnityEngine_GameObjectGenerated_GameObject_gameObject_m1940913973 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GameObjectGenerated::GameObject_AddComponent__Type(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GameObjectGenerated_GameObject_AddComponent__Type_m1879085211 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GameObjectGenerated::GameObject_AddComponentT1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GameObjectGenerated_GameObject_AddComponentT1_m1413453694 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GameObjectGenerated::GameObject_BroadcastMessage__String__Object__SendMessageOptions(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GameObjectGenerated_GameObject_BroadcastMessage__String__Object__SendMessageOptions_m1562980442 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GameObjectGenerated::GameObject_BroadcastMessage__String__SendMessageOptions(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GameObjectGenerated_GameObject_BroadcastMessage__String__SendMessageOptions_m797275867 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GameObjectGenerated::GameObject_BroadcastMessage__String__Object(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GameObjectGenerated_GameObject_BroadcastMessage__String__Object_m406965339 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GameObjectGenerated::GameObject_BroadcastMessage__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GameObjectGenerated_GameObject_BroadcastMessage__String_m974626140 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GameObjectGenerated::GameObject_CompareTag__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GameObjectGenerated_GameObject_CompareTag__String_m783726731 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GameObjectGenerated::GameObject_GetComponent__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GameObjectGenerated_GameObject_GetComponent__String_m3459045853 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GameObjectGenerated::GameObject_GetComponent__Type(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GameObjectGenerated_GameObject_GetComponent__Type_m1491007238 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GameObjectGenerated::GameObject_GetComponentT1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GameObjectGenerated_GameObject_GetComponentT1_m1183837033 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GameObjectGenerated::GameObject_GetComponentInChildren__Type__Boolean(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GameObjectGenerated_GameObject_GetComponentInChildren__Type__Boolean_m3530422976 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GameObjectGenerated::GameObject_GetComponentInChildrenT1__Boolean(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GameObjectGenerated_GameObject_GetComponentInChildrenT1__Boolean_m150967229 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GameObjectGenerated::GameObject_GetComponentInChildren__Type(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GameObjectGenerated_GameObject_GetComponentInChildren__Type_m2451218858 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GameObjectGenerated::GameObject_GetComponentInChildrenT1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GameObjectGenerated_GameObject_GetComponentInChildrenT1_m561154061 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GameObjectGenerated::GameObject_GetComponentInParent__Type(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GameObjectGenerated_GameObject_GetComponentInParent__Type_m2248951253 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GameObjectGenerated::GameObject_GetComponentInParentT1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GameObjectGenerated_GameObject_GetComponentInParentT1_m664467896 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GameObjectGenerated::GameObject_GetComponents__Type__ListT1_Component(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GameObjectGenerated_GameObject_GetComponents__Type__ListT1_Component_m3002119100 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GameObjectGenerated::GameObject_GetComponentsT1__ListT1_T(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GameObjectGenerated_GameObject_GetComponentsT1__ListT1_T_m1995885846 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GameObjectGenerated::GameObject_GetComponents__Type(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GameObjectGenerated_GameObject_GetComponents__Type_m3203520483 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GameObjectGenerated::GameObject_GetComponentsT1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GameObjectGenerated_GameObject_GetComponentsT1_m88588998 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GameObjectGenerated::GameObject_GetComponentsInChildren__Type__Boolean(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GameObjectGenerated_GameObject_GetComponentsInChildren__Type__Boolean_m1365584579 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GameObjectGenerated::GameObject_GetComponentsInChildrenT1__Boolean__ListT1_T(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GameObjectGenerated_GameObject_GetComponentsInChildrenT1__Boolean__ListT1_T_m4015585040 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GameObjectGenerated::GameObject_GetComponentsInChildrenT1__Boolean(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GameObjectGenerated_GameObject_GetComponentsInChildrenT1__Boolean_m3653057856 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GameObjectGenerated::GameObject_GetComponentsInChildren__Type(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GameObjectGenerated_GameObject_GetComponentsInChildren__Type_m2944651847 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GameObjectGenerated::GameObject_GetComponentsInChildrenT1__ListT1_T(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GameObjectGenerated_GameObject_GetComponentsInChildrenT1__ListT1_T_m1989438586 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GameObjectGenerated::GameObject_GetComponentsInChildrenT1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GameObjectGenerated_GameObject_GetComponentsInChildrenT1_m1352568618 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GameObjectGenerated::GameObject_GetComponentsInParentT1__Boolean__ListT1_T(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GameObjectGenerated_GameObject_GetComponentsInParentT1__Boolean__ListT1_T_m2909711429 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GameObjectGenerated::GameObject_GetComponentsInParent__Type__Boolean(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GameObjectGenerated_GameObject_GetComponentsInParent__Type__Boolean_m1784949688 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GameObjectGenerated::GameObject_GetComponentsInParentT1__Boolean(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GameObjectGenerated_GameObject_GetComponentsInParentT1__Boolean_m2615476405 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GameObjectGenerated::GameObject_GetComponentsInParent__Type(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GameObjectGenerated_GameObject_GetComponentsInParent__Type_m2589129138 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GameObjectGenerated::GameObject_GetComponentsInParentT1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GameObjectGenerated_GameObject_GetComponentsInParentT1_m2506630165 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GameObjectGenerated::GameObject_SendMessage__String__Object__SendMessageOptions(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GameObjectGenerated_GameObject_SendMessage__String__Object__SendMessageOptions_m1400834827 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GameObjectGenerated::GameObject_SendMessage__String__Object(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GameObjectGenerated_GameObject_SendMessage__String__Object_m2040605068 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GameObjectGenerated::GameObject_SendMessage__String__SendMessageOptions(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GameObjectGenerated_GameObject_SendMessage__String__SendMessageOptions_m3134558860 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GameObjectGenerated::GameObject_SendMessage__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GameObjectGenerated_GameObject_SendMessage__String_m3109796749 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GameObjectGenerated::GameObject_SendMessageUpwards__String__Object__SendMessageOptions(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GameObjectGenerated_GameObject_SendMessageUpwards__String__Object__SendMessageOptions_m3108678641 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GameObjectGenerated::GameObject_SendMessageUpwards__String__Object(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GameObjectGenerated_GameObject_SendMessageUpwards__String__Object_m316520818 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GameObjectGenerated::GameObject_SendMessageUpwards__String__SendMessageOptions(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GameObjectGenerated_GameObject_SendMessageUpwards__String__SendMessageOptions_m3563813234 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GameObjectGenerated::GameObject_SendMessageUpwards__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GameObjectGenerated_GameObject_SendMessageUpwards__String_m1640075635 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GameObjectGenerated::GameObject_SetActive__Boolean(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GameObjectGenerated_GameObject_SetActive__Boolean_m1484236965 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GameObjectGenerated::GameObject_CreatePrimitive__PrimitiveType(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GameObjectGenerated_GameObject_CreatePrimitive__PrimitiveType_m2157968667 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GameObjectGenerated::GameObject_Find__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GameObjectGenerated_GameObject_Find__String_m2986861007 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GameObjectGenerated::GameObject_FindGameObjectsWithTag__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GameObjectGenerated_GameObject_FindGameObjectsWithTag__String_m2719522561 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GameObjectGenerated::GameObject_FindGameObjectWithTag__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GameObjectGenerated_GameObject_FindGameObjectWithTag__String_m598200600 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GameObjectGenerated::GameObject_FindWithTag__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GameObjectGenerated_GameObject_FindWithTag__String_m3799206953 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GameObjectGenerated::__Register()
extern "C"  void UnityEngine_GameObjectGenerated___Register_m2976949778 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type[] UnityEngine_GameObjectGenerated::<GameObject_GameObject3>m__250()
extern "C"  TypeU5BU5D_t3339007067* UnityEngine_GameObjectGenerated_U3CGameObject_GameObject3U3Em__250_m1291197637 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_GameObjectGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UnityEngine_GameObjectGenerated_ilo_getObject1_m3894989185 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GameObjectGenerated::ilo_addJSCSRel2(System.Int32,System.Object)
extern "C"  void UnityEngine_GameObjectGenerated_ilo_addJSCSRel2_m294512786 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GameObjectGenerated::ilo_attachFinalizerObject3(System.Int32)
extern "C"  bool UnityEngine_GameObjectGenerated_ilo_attachFinalizerObject3_m2078096708 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine_GameObjectGenerated::ilo_getStringS4(System.Int32)
extern "C"  String_t* UnityEngine_GameObjectGenerated_ilo_getStringS4_m2129665276 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_GameObjectGenerated::ilo_setObject5(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UnityEngine_GameObjectGenerated_ilo_setObject5_m3545285117 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GameObjectGenerated::ilo_setInt326(System.Int32,System.Int32)
extern "C"  void UnityEngine_GameObjectGenerated_ilo_setInt326_m1695830908 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GameObjectGenerated::ilo_setBooleanS7(System.Int32,System.Boolean)
extern "C"  void UnityEngine_GameObjectGenerated_ilo_setBooleanS7_m4246983744 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GameObjectGenerated::ilo_getBooleanS8(System.Int32)
extern "C"  bool UnityEngine_GameObjectGenerated_ilo_getBooleanS8_m3440116022 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GameObjectGenerated::ilo_setStringS9(System.Int32,System.String)
extern "C"  void UnityEngine_GameObjectGenerated_ilo_setStringS9_m1126556710 (Il2CppObject * __this /* static, unused */, int32_t ___e0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_GameObjectGenerated::ilo_getEnum10(System.Int32)
extern "C"  int32_t UnityEngine_GameObjectGenerated_ilo_getEnum10_m1489114293 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_GameObjectGenerated::ilo_getObject11(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_GameObjectGenerated_ilo_getObject11_m3778027729 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo UnityEngine_GameObjectGenerated::ilo_makeGenericMethod12(System.Type,MethodID,System.Int32)
extern "C"  MethodInfo_t * UnityEngine_GameObjectGenerated_ilo_makeGenericMethod12_m438695985 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, MethodID_t3916401116 * ___methodID1, int32_t ___TCount2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GameObjectGenerated::ilo_setArrayS13(System.Int32,System.Int32,System.Boolean)
extern "C"  void UnityEngine_GameObjectGenerated_ilo_setArrayS13_m1848565909 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___count1, bool ___bClear2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GameObjectGenerated::ilo_GameObject_GetComponentsInChildrenT1__Boolean14(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GameObjectGenerated_ilo_GameObject_GetComponentsInChildrenT1__Boolean14_m2858645008 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___count1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GameObjectGenerated::ilo_moveSaveID2Arr15(System.Int32)
extern "C"  void UnityEngine_GameObjectGenerated_ilo_moveSaveID2Arr15_m3330542191 (Il2CppObject * __this /* static, unused */, int32_t ___arrIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GameObjectGenerated::ilo_GameObject_GetComponentsInParentT1__Boolean16(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GameObjectGenerated_ilo_GameObject_GetComponentsInParentT1__Boolean16_m3790154247 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___count1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_GameObjectGenerated::ilo_getWhatever17(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_GameObjectGenerated_ilo_getWhatever17_m2132472890 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
