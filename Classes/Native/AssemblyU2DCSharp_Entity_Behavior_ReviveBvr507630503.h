﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_Entity_Behavior_IBehavior770859129.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entity.Behavior.ReviveBvr
struct  ReviveBvr_t507630503  : public IBehavior_t770859129
{
public:
	// System.Single Entity.Behavior.ReviveBvr::REVIVE_WAIT_TIME
	float ___REVIVE_WAIT_TIME_2;
	// System.Single Entity.Behavior.ReviveBvr::_time
	float ____time_3;
	// System.Single Entity.Behavior.ReviveBvr::_aniTime
	float ____aniTime_4;
	// System.Boolean Entity.Behavior.ReviveBvr::isLock
	bool ___isLock_5;

public:
	inline static int32_t get_offset_of_REVIVE_WAIT_TIME_2() { return static_cast<int32_t>(offsetof(ReviveBvr_t507630503, ___REVIVE_WAIT_TIME_2)); }
	inline float get_REVIVE_WAIT_TIME_2() const { return ___REVIVE_WAIT_TIME_2; }
	inline float* get_address_of_REVIVE_WAIT_TIME_2() { return &___REVIVE_WAIT_TIME_2; }
	inline void set_REVIVE_WAIT_TIME_2(float value)
	{
		___REVIVE_WAIT_TIME_2 = value;
	}

	inline static int32_t get_offset_of__time_3() { return static_cast<int32_t>(offsetof(ReviveBvr_t507630503, ____time_3)); }
	inline float get__time_3() const { return ____time_3; }
	inline float* get_address_of__time_3() { return &____time_3; }
	inline void set__time_3(float value)
	{
		____time_3 = value;
	}

	inline static int32_t get_offset_of__aniTime_4() { return static_cast<int32_t>(offsetof(ReviveBvr_t507630503, ____aniTime_4)); }
	inline float get__aniTime_4() const { return ____aniTime_4; }
	inline float* get_address_of__aniTime_4() { return &____aniTime_4; }
	inline void set__aniTime_4(float value)
	{
		____aniTime_4 = value;
	}

	inline static int32_t get_offset_of_isLock_5() { return static_cast<int32_t>(offsetof(ReviveBvr_t507630503, ___isLock_5)); }
	inline bool get_isLock_5() const { return ___isLock_5; }
	inline bool* get_address_of_isLock_5() { return &___isLock_5; }
	inline void set_isLock_5(bool value)
	{
		___isLock_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
