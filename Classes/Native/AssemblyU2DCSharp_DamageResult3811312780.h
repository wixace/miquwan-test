﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType1744280289.h"
#include "AssemblyU2DCSharp_FightEnum_EDamageType2535357340.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DamageResult
struct  DamageResult_t3811312780 
{
public:
	// System.Int32 DamageResult::value
	int32_t ___value_0;
	// FightEnum.EDamageType DamageResult::damageType
	int32_t ___damageType_1;
	// System.Boolean DamageResult::result
	bool ___result_2;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(DamageResult_t3811312780, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_damageType_1() { return static_cast<int32_t>(offsetof(DamageResult_t3811312780, ___damageType_1)); }
	inline int32_t get_damageType_1() const { return ___damageType_1; }
	inline int32_t* get_address_of_damageType_1() { return &___damageType_1; }
	inline void set_damageType_1(int32_t value)
	{
		___damageType_1 = value;
	}

	inline static int32_t get_offset_of_result_2() { return static_cast<int32_t>(offsetof(DamageResult_t3811312780, ___result_2)); }
	inline bool get_result_2() const { return ___result_2; }
	inline bool* get_address_of_result_2() { return &___result_2; }
	inline void set_result_2(bool value)
	{
		___result_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for marshalling of: DamageResult
struct DamageResult_t3811312780_marshaled_pinvoke
{
	int32_t ___value_0;
	int32_t ___damageType_1;
	int32_t ___result_2;
};
// Native definition for marshalling of: DamageResult
struct DamageResult_t3811312780_marshaled_com
{
	int32_t ___value_0;
	int32_t ___damageType_1;
	int32_t ___result_2;
};
