﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FriendNpcGenerated
struct FriendNpcGenerated_t1502271884;
// JSVCall
struct JSVCall_t3708497963;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;
// FriendNpc
struct FriendNpc_t838916931;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_FriendNpc838916931.h"

// System.Void FriendNpcGenerated::.ctor()
extern "C"  void FriendNpcGenerated__ctor_m1927881871 (FriendNpcGenerated_t1502271884 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FriendNpcGenerated::FriendNpc_FriendNpc1(JSVCall,System.Int32)
extern "C"  bool FriendNpcGenerated_FriendNpc_FriendNpc1_m2108203203 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendNpcGenerated::FriendNpc_FriendNpcCfg(JSVCall)
extern "C"  void FriendNpcGenerated_FriendNpc_FriendNpcCfg_m3242492245 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendNpcGenerated::FriendNpc_IsTrigger(JSVCall)
extern "C"  void FriendNpcGenerated_FriendNpc_IsTrigger_m353317224 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FriendNpcGenerated::FriendNpc_AGAINATTACK(JSVCall,System.Int32)
extern "C"  bool FriendNpcGenerated_FriendNpc_AGAINATTACK_m1324254261 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FriendNpcGenerated::FriendNpc_AIBeAttacked(JSVCall,System.Int32)
extern "C"  bool FriendNpcGenerated_FriendNpc_AIBeAttacked_m3688478727 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FriendNpcGenerated::FriendNpc_AIMonsterDead__Int32(JSVCall,System.Int32)
extern "C"  bool FriendNpcGenerated_FriendNpc_AIMonsterDead__Int32_m394457773 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FriendNpcGenerated::FriendNpc_ATKDEBUFF(JSVCall,System.Int32)
extern "C"  bool FriendNpcGenerated_FriendNpc_ATKDEBUFF_m1883762553 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FriendNpcGenerated::FriendNpc_Clear(JSVCall,System.Int32)
extern "C"  bool FriendNpcGenerated_FriendNpc_Clear_m3895937050 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FriendNpcGenerated::FriendNpc_InitPos__Vector3__Quaternion(JSVCall,System.Int32)
extern "C"  bool FriendNpcGenerated_FriendNpc_InitPos__Vector3__Quaternion_m778999647 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FriendNpcGenerated::FriendNpc_OnAttckTime(JSVCall,System.Int32)
extern "C"  bool FriendNpcGenerated_FriendNpc_OnAttckTime_m3654536036 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FriendNpcGenerated::FriendNpc_OnLevelTime(JSVCall,System.Int32)
extern "C"  bool FriendNpcGenerated_FriendNpc_OnLevelTime_m592003167 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FriendNpcGenerated::FriendNpc_OnTargetReached(JSVCall,System.Int32)
extern "C"  bool FriendNpcGenerated_FriendNpc_OnTargetReached_m731317231 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FriendNpcGenerated::FriendNpc_OnTriggerFun__Int32(JSVCall,System.Int32)
extern "C"  bool FriendNpcGenerated_FriendNpc_OnTriggerFun__Int32_m2654088821 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FriendNpcGenerated::FriendNpc_OnUseingSkill__Int32(JSVCall,System.Int32)
extern "C"  bool FriendNpcGenerated_FriendNpc_OnUseingSkill__Int32_m1678883276 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FriendNpcGenerated::FriendNpc_RangeMovetoBehacior(JSVCall,System.Int32)
extern "C"  bool FriendNpcGenerated_FriendNpc_RangeMovetoBehacior_m1588701723 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FriendNpcGenerated::FriendNpc_Update(JSVCall,System.Int32)
extern "C"  bool FriendNpcGenerated_FriendNpc_Update_m4272935966 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendNpcGenerated::__Register()
extern "C"  void FriendNpcGenerated___Register_m3201554840 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 FriendNpcGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t FriendNpcGenerated_ilo_getObject1_m4170419811 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FriendNpcGenerated::ilo_attachFinalizerObject2(System.Int32)
extern "C"  bool FriendNpcGenerated_ilo_attachFinalizerObject2_m1591799409 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 FriendNpcGenerated::ilo_setObject3(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t FriendNpcGenerated_ilo_setObject3_m1464543837 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FriendNpcGenerated::ilo_get_IsTrigger4(FriendNpc)
extern "C"  bool FriendNpcGenerated_ilo_get_IsTrigger4_m3526972260 (Il2CppObject * __this /* static, unused */, FriendNpc_t838916931 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendNpcGenerated::ilo_setBooleanS5(System.Int32,System.Boolean)
extern "C"  void FriendNpcGenerated_ilo_setBooleanS5_m3111801404 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendNpcGenerated::ilo_AGAINATTACK6(FriendNpc)
extern "C"  void FriendNpcGenerated_ilo_AGAINATTACK6_m2178606649 (Il2CppObject * __this /* static, unused */, FriendNpc_t838916931 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendNpcGenerated::ilo_AIMonsterDead7(FriendNpc,System.Int32)
extern "C"  void FriendNpcGenerated_ilo_AIMonsterDead7_m4056154545 (Il2CppObject * __this /* static, unused */, FriendNpc_t838916931 * ____this0, int32_t ___murderID1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendNpcGenerated::ilo_ATKDEBUFF8(FriendNpc)
extern "C"  void FriendNpcGenerated_ilo_ATKDEBUFF8_m3737494651 (Il2CppObject * __this /* static, unused */, FriendNpc_t838916931 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object FriendNpcGenerated::ilo_getObject9(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * FriendNpcGenerated_ilo_getObject9_m290166088 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendNpcGenerated::ilo_OnAttckTime10(FriendNpc)
extern "C"  void FriendNpcGenerated_ilo_OnAttckTime10_m1736926015 (Il2CppObject * __this /* static, unused */, FriendNpc_t838916931 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 FriendNpcGenerated::ilo_getInt3211(System.Int32)
extern "C"  int32_t FriendNpcGenerated_ilo_getInt3211_m3003969983 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendNpcGenerated::ilo_Update12(FriendNpc)
extern "C"  void FriendNpcGenerated_ilo_Update12_m3887812637 (Il2CppObject * __this /* static, unused */, FriendNpc_t838916931 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
