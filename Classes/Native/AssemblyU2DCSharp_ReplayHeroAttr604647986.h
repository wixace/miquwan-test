﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "AssemblyU2DCSharp_ReplayBase779703160.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ReplayHeroAttr
struct  ReplayHeroAttr_t604647986  : public ReplayBase_t779703160
{
public:
	// System.String ReplayHeroAttr::key
	String_t* ___key_8;
	// System.Single ReplayHeroAttr::value
	float ___value_9;

public:
	inline static int32_t get_offset_of_key_8() { return static_cast<int32_t>(offsetof(ReplayHeroAttr_t604647986, ___key_8)); }
	inline String_t* get_key_8() const { return ___key_8; }
	inline String_t** get_address_of_key_8() { return &___key_8; }
	inline void set_key_8(String_t* value)
	{
		___key_8 = value;
		Il2CppCodeGenWriteBarrier(&___key_8, value);
	}

	inline static int32_t get_offset_of_value_9() { return static_cast<int32_t>(offsetof(ReplayHeroAttr_t604647986, ___value_9)); }
	inline float get_value_9() const { return ___value_9; }
	inline float* get_address_of_value_9() { return &___value_9; }
	inline void set_value_9(float value)
	{
		___value_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
