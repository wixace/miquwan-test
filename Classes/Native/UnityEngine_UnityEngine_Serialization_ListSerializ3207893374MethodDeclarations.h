﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Serialization.ListSerializationSurrogate
struct ListSerializationSurrogate_t3207893374;
// System.Object
struct Il2CppObject;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t2185721892;
// System.Runtime.Serialization.ISurrogateSelector
struct ISurrogateSelector_t2300150170;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2185721892.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon2761351129.h"

// System.Void UnityEngine.Serialization.ListSerializationSurrogate::.ctor()
extern "C"  void ListSerializationSurrogate__ctor_m2833832457 (ListSerializationSurrogate_t3207893374 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Serialization.ListSerializationSurrogate::.cctor()
extern "C"  void ListSerializationSurrogate__cctor_m1467364036 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine.Serialization.ListSerializationSurrogate::SetObjectData(System.Object,System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext,System.Runtime.Serialization.ISurrogateSelector)
extern "C"  Il2CppObject * ListSerializationSurrogate_SetObjectData_m2586542907 (ListSerializationSurrogate_t3207893374 * __this, Il2CppObject * ___obj0, SerializationInfo_t2185721892 * ___info1, StreamingContext_t2761351129  ___context2, Il2CppObject * ___selector3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
