﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GameMgr
struct GameMgr_t1469029542;
// CSCheckPointUnit
struct CSCheckPointUnit_t3129216924;
// taixuTipCfg
struct taixuTipCfg_t1268722370;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// UISprite
struct UISprite_t661437049;
// CombatEntity
struct CombatEntity_t684137495;
// System.Collections.Generic.List`1<CombatEntity>
struct List_1_t2052323047;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1375417109;
// Mihua.Asset.ABLoadOperation.AssetOperation
struct AssetOperation_t778728221;
// CSDatacfgManager
struct CSDatacfgManager_t1565254243;
// System.String
struct String_t;
// CEvent.EventFunc`1<CEvent.ZEvent>
struct EventFunc_1_t1114914310;
// HeadUpBloodMgr
struct HeadUpBloodMgr_t2330866585;
// NpcMgr
struct NpcMgr_t2339534743;
// ProgressMgr
struct ProgressMgr_t2799388811;
// BossAnimationMgr
struct BossAnimationMgr_t150948897;
// UIWidget
struct UIWidget_t769069560;
// ReplayMgr
struct ReplayMgr_t1549183121;
// HeroMgr
struct HeroMgr_t2475965342;
// System.Collections.Generic.List`1<Hero>
struct List_1_t1370431210;
// Hero
struct Hero_t2245658;
// CameraAnimationMgr
struct CameraAnimationMgr_t3311695833;
// System.Action
struct Action_t3771233898;
// LevelConfigManager
struct LevelConfigManager_t657947911;
// CameraShotMgr
struct CameraShotMgr_t1580128697;
// Entity.Behavior.IBehaviorCtrl
struct IBehaviorCtrl_t4225040900;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// AnimationRunner
struct AnimationRunner_t1015409588;
// BuffCtrl
struct BuffCtrl_t2836564350;
// EffectMgr
struct EffectMgr_t535289511;
// GameQutilyCtr
struct GameQutilyCtr_t3963256169;
// SkillHurtShow
struct SkillHurtShow_t1728264445;
// CSGameDataMgr
struct CSGameDataMgr_t2623305516;
// System.Collections.Generic.List`1<CSPlusAtt>
struct List_1_t341533415;
// CSHeroData
struct CSHeroData_t3763839828;
// CSFightforData
struct CSFightforData_t215334547;
// CSCampfightData
struct CSCampfightData_t179152873;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// CameraMouseMove_PVP
struct CameraMouseMove_PVP_t2551559708;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CSCheckPointUnit3129216924.h"
#include "AssemblyU2DCSharp_taixuTipCfg1268722370.h"
#include "AssemblyU2DCSharp_ENavigateAIType3799782360.h"
#include "AssemblyU2DCSharp_NpcMgr_EditorNpcType2400575222.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "AssemblyU2DCSharp_UISprite661437049.h"
#include "AssemblyU2DCSharp_CombatEntity684137495.h"
#include "AssemblyU2DCSharp_Mihua_Asset_ABLoadOperation_Asset778728221.h"
#include "AssemblyU2DCSharp_GameMgr1469029542.h"
#include "AssemblyU2DCSharp_CSDatacfgManager1565254243.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_NpcMgr2339534743.h"
#include "AssemblyU2DCSharp_ProgressMgr2799388811.h"
#include "AssemblyU2DCSharp_BossAnimationMgr150948897.h"
#include "AssemblyU2DCSharp_UIWidget769069560.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"
#include "AssemblyU2DCSharp_HeroMgr2475965342.h"
#include "AssemblyU2DCSharp_ReplayMgr1549183121.h"
#include "AssemblyU2DCSharp_Hero2245658.h"
#include "AssemblyU2DCSharp_CameraAnimationMgr3311695833.h"
#include "System_Core_System_Action3771233898.h"
#include "AssemblyU2DCSharp_LevelConfigManager657947911.h"
#include "AssemblyU2DCSharp_EntityEnum_UnitCamp642829979.h"
#include "AssemblyU2DCSharp_EntityEnum_UnitType643359572.h"
#include "AssemblyU2DCSharp_Entity_Behavior_IBehaviorCtrl4225040900.h"
#include "AssemblyU2DCSharp_Entity_Behavior_EBehaviorID1238533136.h"
#include "AssemblyU2DCSharp_AnimationRunner1015409588.h"
#include "AssemblyU2DCSharp_AnimationRunner_AniType1006238651.h"
#include "AssemblyU2DCSharp_BuffCtrl2836564350.h"
#include "AssemblyU2DCSharp_EffectMgr535289511.h"
#include "AssemblyU2DCSharp_GameQutilyCtr3963256169.h"
#include "AssemblyU2DCSharp_HeadUpBloodMgr2330866585.h"
#include "AssemblyU2DCSharp_SkillHurtShow1728264445.h"
#include "AssemblyU2DCSharp_CSHeroData3763839828.h"
#include "AssemblyU2DCSharp_CSFightforData215334547.h"
#include "AssemblyU2DCSharp_CSCampfightData179152873.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "AssemblyU2DCSharp_CameraMouseMove_PVP2551559708.h"

// System.Void GameMgr::.ctor()
extern "C"  void GameMgr__ctor_m2483580293 (GameMgr_t1469029542 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgr::set_checkPoint(CSCheckPointUnit)
extern "C"  void GameMgr_set_checkPoint_m455196552 (GameMgr_t1469029542 * __this, CSCheckPointUnit_t3129216924 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CSCheckPointUnit GameMgr::get_checkPoint()
extern "C"  CSCheckPointUnit_t3129216924 * GameMgr_get_checkPoint_m2810066607 (GameMgr_t1469029542 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgr::set_taixuTip(taixuTipCfg)
extern "C"  void GameMgr_set_taixuTip_m1834123250 (GameMgr_t1469029542 * __this, taixuTipCfg_t1268722370 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// taixuTipCfg GameMgr::get_taixuTip()
extern "C"  taixuTipCfg_t1268722370 * GameMgr_get_taixuTip_m593424153 (GameMgr_t1469029542 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgr::set_time(System.Int32)
extern "C"  void GameMgr_set_time_m2660602330 (GameMgr_t1469029542 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 GameMgr::get_time()
extern "C"  int32_t GameMgr_get_time_m1389210567 (GameMgr_t1469029542 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgr::set_maxTime(System.Int32)
extern "C"  void GameMgr_set_maxTime_m2883594888 (GameMgr_t1469029542 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 GameMgr::get_maxTime()
extern "C"  int32_t GameMgr_get_maxTime_m3419866425 (GameMgr_t1469029542 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgr::set_IsTimeLock(System.Boolean)
extern "C"  void GameMgr_set_IsTimeLock_m2812100533 (GameMgr_t1469029542 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameMgr::get_IsTimeLock()
extern "C"  bool GameMgr_get_IsTimeLock_m745802774 (GameMgr_t1469029542 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgr::set_isAuto(System.Boolean)
extern "C"  void GameMgr_set_isAuto_m543916076 (GameMgr_t1469029542 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameMgr::get_isAuto()
extern "C"  bool GameMgr_get_isAuto_m2127742157 (GameMgr_t1469029542 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgr::set_isAutoSkill(System.Boolean)
extern "C"  void GameMgr_set_isAutoSkill_m3072174581 (GameMgr_t1469029542 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameMgr::get_isAutoSkill()
extern "C"  bool GameMgr_get_isAutoSkill_m559896230 (GameMgr_t1469029542 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgr::set_isPublicCD(System.Boolean)
extern "C"  void GameMgr_set_isPublicCD_m3133138567 (GameMgr_t1469029542 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameMgr::get_isPublicCD()
extern "C"  bool GameMgr_get_isPublicCD_m1897605736 (GameMgr_t1469029542 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgr::set_isFrozen(System.Boolean)
extern "C"  void GameMgr_set_isFrozen_m3034109405 (GameMgr_t1469029542 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameMgr::get_isFrozen()
extern "C"  bool GameMgr_get_isFrozen_m1981251134 (GameMgr_t1469029542 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgr::set_isFrozenIdle(System.Boolean)
extern "C"  void GameMgr_set_isFrozenIdle_m2584086353 (GameMgr_t1469029542 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameMgr::get_isFrozenIdle()
extern "C"  bool GameMgr_get_isFrozenIdle_m3243515570 (GameMgr_t1469029542 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgr::set_isBattleStart(System.Boolean)
extern "C"  void GameMgr_set_isBattleStart_m3854300125 (GameMgr_t1469029542 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameMgr::get_isBattleStart()
extern "C"  bool GameMgr_get_isBattleStart_m3988966542 (GameMgr_t1469029542 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgr::set_isBattleEnd(System.Boolean)
extern "C"  void GameMgr_set_isBattleEnd_m282032918 (GameMgr_t1469029542 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameMgr::get_isBattleEnd()
extern "C"  bool GameMgr_get_isBattleEnd_m3325117831 (GameMgr_t1469029542 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgr::set_isBattleEndBeforWin(System.Boolean)
extern "C"  void GameMgr_set_isBattleEndBeforWin_m3940100492 (GameMgr_t1469029542 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameMgr::get_isBattleEndBeforWin()
extern "C"  bool GameMgr_get_isBattleEndBeforWin_m1371648637 (GameMgr_t1469029542 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgr::set_isCameraScale(System.Boolean)
extern "C"  void GameMgr_set_isCameraScale_m2845289592 (GameMgr_t1469029542 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameMgr::get_isCameraScale()
extern "C"  bool GameMgr_get_isCameraScale_m919068777 (GameMgr_t1469029542 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgr::set_isWin(System.Boolean)
extern "C"  void GameMgr_set_isWin_m2242342959 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameMgr::get_isWin()
extern "C"  bool GameMgr_get_isWin_m1751156832 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgr::set_isStart(System.Boolean)
extern "C"  void GameMgr_set_isStart_m522920437 (GameMgr_t1469029542 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameMgr::get_isStart()
extern "C"  bool GameMgr_get_isStart_m284680870 (GameMgr_t1469029542 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgr::set_level(System.Int32)
extern "C"  void GameMgr_set_level_m356916155 (GameMgr_t1469029542 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 GameMgr::get_level()
extern "C"  int32_t GameMgr_get_level_m1499620204 (GameMgr_t1469029542 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgr::set_navAIType(ENavigateAIType)
extern "C"  void GameMgr_set_navAIType_m2040271279 (GameMgr_t1469029542 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ENavigateAIType GameMgr::get_navAIType()
extern "C"  int32_t GameMgr_get_navAIType_m112600762 (GameMgr_t1469029542 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgr::set_IsOnTap(System.Boolean)
extern "C"  void GameMgr_set_IsOnTap_m1992993463 (GameMgr_t1469029542 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameMgr::get_IsOnTap()
extern "C"  bool GameMgr_get_IsOnTap_m2849310440 (GameMgr_t1469029542 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgr::set_IsPlayPloting(System.Boolean)
extern "C"  void GameMgr_set_IsPlayPloting_m1113089216 (GameMgr_t1469029542 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameMgr::get_IsPlayPloting()
extern "C"  bool GameMgr_get_IsPlayPloting_m870397617 (GameMgr_t1469029542 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single GameMgr::CaptainViewRange(NpcMgr/EditorNpcType)
extern "C"  float GameMgr_CaptainViewRange_m2341915761 (GameMgr_t1469029542 * __this, int32_t ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgr::Awake()
extern "C"  void GameMgr_Awake_m2721185512 (GameMgr_t1469029542 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgr::UpdateOnOffEffect(CEvent.ZEvent)
extern "C"  void GameMgr_UpdateOnOffEffect_m1205511808 (GameMgr_t1469029542 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgr::OnSceneLoadOK(CEvent.ZEvent)
extern "C"  void GameMgr_OnSceneLoadOK_m709858121 (GameMgr_t1469029542 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgr::SetaddSpeedIcon(UISprite,System.Boolean)
extern "C"  void GameMgr_SetaddSpeedIcon_m985094002 (GameMgr_t1469029542 * __this, UISprite_t661437049 * ___sp0, bool ____IsActive1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgr::StartTime(CEvent.ZEvent)
extern "C"  void GameMgr_StartTime_m41423241 (GameMgr_t1469029542 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgr::Update()
extern "C"  void GameMgr_Update_m1408439848 (GameMgr_t1469029542 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgr::UpdateTime()
extern "C"  void GameMgr_UpdateTime_m2843339349 (GameMgr_t1469029542 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgr::UpdateCombatIndicator()
extern "C"  void GameMgr_UpdateCombatIndicator_m2295324565 (GameMgr_t1469029542 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgr::UpdataPublicCD()
extern "C"  void GameMgr_UpdataPublicCD_m1736038990 (GameMgr_t1469029542 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgr::OnCombatWin(CEvent.ZEvent)
extern "C"  void GameMgr_OnCombatWin_m349659119 (GameMgr_t1469029542 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgr::OnCombatLose(CEvent.ZEvent)
extern "C"  void GameMgr_OnCombatLose_m4203308372 (GameMgr_t1469029542 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgr::SendSaveHpMpData(System.Boolean)
extern "C"  void GameMgr_SendSaveHpMpData_m1818118320 (GameMgr_t1469029542 * __this, bool ___isWin0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgr::PlayCameraAnimationEnd(CEvent.ZEvent)
extern "C"  void GameMgr_PlayCameraAnimationEnd_m1026065516 (GameMgr_t1469029542 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgr::PlayBattleEndShow()
extern "C"  void GameMgr_PlayBattleEndShow_m1593811055 (GameMgr_t1469029542 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgr::PlayEndPlot(CEvent.ZEvent)
extern "C"  void GameMgr_PlayEndPlot_m654501040 (GameMgr_t1469029542 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgr::ReqCheckFinish()
extern "C"  void GameMgr_ReqCheckFinish_m3336882844 (GameMgr_t1469029542 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgr::ReActiveGameSpeedBtn(System.Boolean)
extern "C"  void GameMgr_ReActiveGameSpeedBtn_m2751314614 (GameMgr_t1469029542 * __this, bool ____IsActive0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgr::TriggerBattle(CombatEntity)
extern "C"  void GameMgr_TriggerBattle_m2369764604 (GameMgr_t1469029542 * __this, CombatEntity_t684137495 * ___src0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CombatEntity GameMgr::GetNearestEntity(CombatEntity,System.Boolean)
extern "C"  CombatEntity_t684137495 * GameMgr_GetNearestEntity_m1956528336 (GameMgr_t1469029542 * __this, CombatEntity_t684137495 * ___unit0, bool ___isCanMove1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CombatEntity GameMgr::GetNearestEnemy(CombatEntity)
extern "C"  CombatEntity_t684137495 * GameMgr_GetNearestEnemy_m1384259762 (GameMgr_t1469029542 * __this, CombatEntity_t684137495 * ___unit0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CombatEntity GameMgr::GetCameraShotNearestEnemy(CombatEntity)
extern "C"  CombatEntity_t684137495 * GameMgr_GetCameraShotNearestEnemy_m1207027281 (GameMgr_t1469029542 * __this, CombatEntity_t684137495 * ___unit0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CombatEntity GameMgr::GetNearestAtkRangedEnemy(CombatEntity,System.Single)
extern "C"  CombatEntity_t684137495 * GameMgr_GetNearestAtkRangedEnemy_m1739350424 (GameMgr_t1469029542 * __this, CombatEntity_t684137495 * ___unit0, float ___range1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CombatEntity GameMgr::GetNearestAtkRangedEnemy(CombatEntity)
extern "C"  CombatEntity_t684137495 * GameMgr_GetNearestAtkRangedEnemy_m732904435 (GameMgr_t1469029542 * __this, CombatEntity_t684137495 * ___unit0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CombatEntity GameMgr::GetEntityByID(System.Boolean,System.Int32)
extern "C"  CombatEntity_t684137495 * GameMgr_GetEntityByID_m934504748 (GameMgr_t1469029542 * __this, bool ___isHero0, int32_t ___id1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CombatEntity> GameMgr::GetAllEnemy(CombatEntity)
extern "C"  List_1_t2052323047 * GameMgr_GetAllEnemy_m978635059 (GameMgr_t1469029542 * __this, CombatEntity_t684137495 * ___entity0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CombatEntity> GameMgr::GetAllEnemy(CombatEntity,System.Boolean)
extern "C"  List_1_t2052323047 * GameMgr_GetAllEnemy_m3585056234 (GameMgr_t1469029542 * __this, CombatEntity_t684137495 * ___entity0, bool ___isshaix1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CombatEntity> GameMgr::GetAllAlly(CombatEntity)
extern "C"  List_1_t2052323047 * GameMgr_GetAllAlly_m1478701249 (GameMgr_t1469029542 * __this, CombatEntity_t684137495 * ___entity0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CombatEntity> GameMgr::GetAllAlly(CombatEntity,System.Boolean)
extern "C"  List_1_t2052323047 * GameMgr_GetAllAlly_m1901029532 (GameMgr_t1469029542 * __this, CombatEntity_t684137495 * ___entity0, bool ___isshaix1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CombatEntity> GameMgr::GetAllFriendsEntity()
extern "C"  List_1_t2052323047 * GameMgr_GetAllFriendsEntity_m3305028922 (GameMgr_t1469029542 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CombatEntity> GameMgr::GetAllHostilEntity()
extern "C"  List_1_t2052323047 * GameMgr_GetAllHostilEntity_m3253409166 (GameMgr_t1469029542 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgr::SetFightCtrlUpdateState(System.Boolean)
extern "C"  void GameMgr_SetFightCtrlUpdateState_m3968220041 (GameMgr_t1469029542 * __this, bool ___isState0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgr::LockNpcBehaviour()
extern "C"  void GameMgr_LockNpcBehaviour_m2956254214 (GameMgr_t1469029542 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgr::LockNpcBehaviourIdle()
extern "C"  void GameMgr_LockNpcBehaviourIdle_m3464321146 (GameMgr_t1469029542 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgr::UnLockNpcBehaviour()
extern "C"  void GameMgr_UnLockNpcBehaviour_m635159295 (GameMgr_t1469029542 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgr::UnLockNpcBehaviourIdle()
extern "C"  void GameMgr_UnLockNpcBehaviourIdle_m3086359283 (GameMgr_t1469029542 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgr::SetAutoMode(System.Boolean)
extern "C"  void GameMgr_SetAutoMode_m1244225486 (GameMgr_t1469029542 * __this, bool ___auto0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgr::EnterPublicCD(System.Single)
extern "C"  void GameMgr_EnterPublicCD_m4148250150 (GameMgr_t1469029542 * __this, float ___cdTime0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgr::FreezeSkill(CombatEntity,System.Boolean)
extern "C"  void GameMgr_FreezeSkill_m3082068311 (GameMgr_t1469029542 * __this, CombatEntity_t684137495 * ___src0, bool ___bFreeze1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgr::AllAwait()
extern "C"  void GameMgr_AllAwait_m1677618964 (GameMgr_t1469029542 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgr::AllDaed()
extern "C"  void GameMgr_AllDaed_m1782366720 (GameMgr_t1469029542 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgr::OnQuitGame(CEvent.ZEvent)
extern "C"  void GameMgr_OnQuitGame_m3351322716 (GameMgr_t1469029542 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgr::OnDestroy()
extern "C"  void GameMgr_OnDestroy_m3878549502 (GameMgr_t1469029542 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 GameMgr::GradeStar()
extern "C"  uint32_t GameMgr_GradeStar_m472746695 (GameMgr_t1469029542 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 GameMgr::GetListenStar()
extern "C"  uint32_t GameMgr_GetListenStar_m1517768877 (GameMgr_t1469029542 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameMgr::IsGradeStar1()
extern "C"  bool GameMgr_IsGradeStar1_m2638583485 (GameMgr_t1469029542 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameMgr::IsGradeStar2()
extern "C"  bool GameMgr_IsGradeStar2_m2638584446 (GameMgr_t1469029542 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameMgr::IsGradeStar3()
extern "C"  bool GameMgr_IsGradeStar3_m2638585407 (GameMgr_t1469029542 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameMgr::GradeStarType(System.Int32,System.Int32)
extern "C"  bool GameMgr_GradeStarType_m3973411884 (GameMgr_t1469029542 * __this, int32_t ___gradeTypeid0, int32_t ___gradeValue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 GameMgr::GradeStar(System.Int32,System.Int32,System.Boolean)
extern "C"  uint32_t GameMgr_GradeStar_m3393423198 (GameMgr_t1469029542 * __this, int32_t ___npcRound0, int32_t ___useTime1, bool ___isKillBoss2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 GameMgr::GradeStar(System.Single)
extern "C"  uint32_t GameMgr_GradeStar_m3127734852 (GameMgr_t1469029542 * __this, float ___bossHpPer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgr::StartPlotEvent(CEvent.ZEvent)
extern "C"  void GameMgr_StartPlotEvent_m3906613349 (GameMgr_t1469029542 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgr::EndPlotEvent(CEvent.ZEvent)
extern "C"  void GameMgr_EndPlotEvent_m3944807006 (GameMgr_t1469029542 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgr::EndDailyZhuanpanPlayEvent(CEvent.ZEvent)
extern "C"  void GameMgr_EndDailyZhuanpanPlayEvent_m4008088857 (GameMgr_t1469029542 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.String> GameMgr::GetLeadPlusAttStrList()
extern "C"  List_1_t1375417109 * GameMgr_GetLeadPlusAttStrList_m1234290766 (GameMgr_t1469029542 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.String> GameMgr::GetReinPlusAttStrList()
extern "C"  List_1_t1375417109 * GameMgr_GetReinPlusAttStrList_m42896266 (GameMgr_t1469029542 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.String> GameMgr::GetSpeCheckPlusAttStrList()
extern "C"  List_1_t1375417109 * GameMgr_GetSpeCheckPlusAttStrList_m3613602994 (GameMgr_t1469029542 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameMgr::get_IsLeaderPlusAtt()
extern "C"  bool GameMgr_get_IsLeaderPlusAtt_m3370465666 (GameMgr_t1469029542 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameMgr::get_IsReinPlusAtt()
extern "C"  bool GameMgr_get_IsReinPlusAtt_m1015628179 (GameMgr_t1469029542 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgr::<OnSceneLoadOK>m__3CC(Mihua.Asset.ABLoadOperation.AssetOperation)
extern "C"  void GameMgr_U3COnSceneLoadOKU3Em__3CC_m1776608827 (GameMgr_t1469029542 * __this, AssetOperation_t778728221 * ___ao0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgr::<OnSceneLoadOK>m__3CD()
extern "C"  void GameMgr_U3COnSceneLoadOKU3Em__3CD_m474135957 (GameMgr_t1469029542 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgr::<PlayBattleEndShow>m__3CE()
extern "C"  void GameMgr_U3CPlayBattleEndShowU3Em__3CE_m4200096953 (GameMgr_t1469029542 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgr::<PlayBattleEndShow>m__3CF()
extern "C"  void GameMgr_U3CPlayBattleEndShowU3Em__3CF_m4200097914 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CSCheckPointUnit GameMgr::ilo_get_checkPoint1(GameMgr)
extern "C"  CSCheckPointUnit_t3129216924 * GameMgr_ilo_get_checkPoint1_m4160195003 (Il2CppObject * __this /* static, unused */, GameMgr_t1469029542 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgr::ilo_set_navAIType2(GameMgr,ENavigateAIType)
extern "C"  void GameMgr_ilo_set_navAIType2_m3275663300 (Il2CppObject * __this /* static, unused */, GameMgr_t1469029542 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CSDatacfgManager GameMgr::ilo_get_CfgDataMgr3()
extern "C"  CSDatacfgManager_t1565254243 * GameMgr_ilo_get_CfgDataMgr3_m2492749450 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// taixuTipCfg GameMgr::ilo_GettaixuTipCfg4(CSDatacfgManager,System.Int32)
extern "C"  taixuTipCfg_t1268722370 * GameMgr_ilo_GettaixuTipCfg4_m3766633457 (Il2CppObject * __this /* static, unused */, CSDatacfgManager_t1565254243 * ____this0, int32_t ___id1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgr::ilo_AddEventListener5(System.String,CEvent.EventFunc`1<CEvent.ZEvent>,System.Boolean)
extern "C"  void GameMgr_ilo_AddEventListener5_m2620323026 (Il2CppObject * __this /* static, unused */, String_t* ___eventType0, EventFunc_1_t1114914310 * ___fun1, bool ___isInsert2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgr::ilo_AddEventListener6(System.String,CEvent.EventFunc`1<CEvent.ZEvent>)
extern "C"  void GameMgr_ilo_AddEventListener6_m1974019434 (Il2CppObject * __this /* static, unused */, String_t* ___eventType0, EventFunc_1_t1114914310 * ___fun1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgr::ilo_set_isPublicCD7(GameMgr,System.Boolean)
extern "C"  void GameMgr_ilo_set_isPublicCD7_m3482947431 (Il2CppObject * __this /* static, unused */, GameMgr_t1469029542 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgr::ilo_DispatchEvent8(CEvent.ZEvent)
extern "C"  void GameMgr_ilo_DispatchEvent8_m3202164209 (Il2CppObject * __this /* static, unused */, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HeadUpBloodMgr GameMgr::ilo_get_Instance9()
extern "C"  HeadUpBloodMgr_t2330866585 * GameMgr_ilo_get_Instance9_m1240122735 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgr::ilo_Init10(NpcMgr)
extern "C"  void GameMgr_ilo_Init10_m4093384842 (Il2CppObject * __this /* static, unused */, NpcMgr_t2339534743 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgr::ilo_Init11(ProgressMgr)
extern "C"  void GameMgr_ilo_Init11_m4068107397 (Il2CppObject * __this /* static, unused */, ProgressMgr_t2799388811 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgr::ilo_Init12(BossAnimationMgr,System.Int32)
extern "C"  void GameMgr_ilo_Init12_m1952517461 (Il2CppObject * __this /* static, unused */, BossAnimationMgr_t150948897 * ____this0, int32_t ___aniId1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgr::ilo_set_isAuto13(GameMgr,System.Boolean)
extern "C"  void GameMgr_ilo_set_isAuto13_m2545347579 (Il2CppObject * __this /* static, unused */, GameMgr_t1469029542 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgr::ilo_set_color14(UIWidget,UnityEngine.Color)
extern "C"  void GameMgr_ilo_set_color14_m83742145 (Il2CppObject * __this /* static, unused */, UIWidget_t769069560 * ____this0, Color_t4194546905  ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgr::ilo_RemoveEventListener15(System.String,CEvent.EventFunc`1<CEvent.ZEvent>)
extern "C"  void GameMgr_ilo_RemoveEventListener15_m3675215909 (Il2CppObject * __this /* static, unused */, String_t* ___eventType0, EventFunc_1_t1114914310 * ___func1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgr::ilo_set_isStart16(GameMgr,System.Boolean)
extern "C"  void GameMgr_ilo_set_isStart16_m162242689 (Il2CppObject * __this /* static, unused */, GameMgr_t1469029542 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgr::ilo_UpdataPublicCD17(GameMgr)
extern "C"  void GameMgr_ilo_UpdataPublicCD17_m3451201157 (Il2CppObject * __this /* static, unused */, GameMgr_t1469029542 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameMgr::ilo_get_IsTimeLock18(GameMgr)
extern "C"  bool GameMgr_ilo_get_IsTimeLock18_m632024284 (Il2CppObject * __this /* static, unused */, GameMgr_t1469029542 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ReplayMgr GameMgr::ilo_get_Instance19()
extern "C"  ReplayMgr_t1549183121 * GameMgr_ilo_get_Instance19_m3681132214 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 GameMgr::ilo_get_maxTime20(GameMgr)
extern "C"  int32_t GameMgr_ilo_get_maxTime20_m815201448 (Il2CppObject * __this /* static, unused */, GameMgr_t1469029542 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 GameMgr::ilo_get_time21(GameMgr)
extern "C"  int32_t GameMgr_ilo_get_time21_m3708200947 (Il2CppObject * __this /* static, unused */, GameMgr_t1469029542 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HeroMgr GameMgr::ilo_get_instance22()
extern "C"  HeroMgr_t2475965342 * GameMgr_ilo_get_instance22_m1608093755 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<Hero> GameMgr::ilo_GetAllHeros23(HeroMgr)
extern "C"  List_1_t1370431210 * GameMgr_ilo_GetAllHeros23_m614607496 (Il2CppObject * __this /* static, unused */, HeroMgr_t2475965342 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgr::ilo_AddPowerBuff24(CombatEntity,System.Int32,System.Int32)
extern "C"  void GameMgr_ilo_AddPowerBuff24_m3013931156 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___buff1, int32_t ___allConfigId2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgr::ilo_set_IsCheckTarget25(CombatEntity,System.Boolean)
extern "C"  void GameMgr_ilo_set_IsCheckTarget25_m3688770191 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgr::ilo_set_isBattleStart26(GameMgr,System.Boolean)
extern "C"  void GameMgr_ilo_set_isBattleStart26_m252808648 (Il2CppObject * __this /* static, unused */, GameMgr_t1469029542 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 GameMgr::ilo_GradeStar27(GameMgr)
extern "C"  uint32_t GameMgr_ilo_GradeStar27_m3723693555 (Il2CppObject * __this /* static, unused */, GameMgr_t1469029542 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// NpcMgr GameMgr::ilo_get_instance28()
extern "C"  NpcMgr_t2339534743 * GameMgr_ilo_get_instance28_m865132624 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameMgr::ilo_get_isKillBoss29(NpcMgr)
extern "C"  bool GameMgr_ilo_get_isKillBoss29_m2408767052 (Il2CppObject * __this /* static, unused */, NpcMgr_t2339534743 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgr::ilo_AllAwait30(GameMgr)
extern "C"  void GameMgr_ilo_AllAwait30_m4247176168 (Il2CppObject * __this /* static, unused */, GameMgr_t1469029542 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 GameMgr::ilo_get_ShiliBossConsumeHp31(NpcMgr)
extern "C"  int32_t GameMgr_ilo_get_ShiliBossConsumeHp31_m3175544698 (Il2CppObject * __this /* static, unused */, NpcMgr_t2339534743 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameMgr::ilo_get_IsPlayREC32(ReplayMgr)
extern "C"  bool GameMgr_ilo_get_IsPlayREC32_m2651947925 (Il2CppObject * __this /* static, unused */, ReplayMgr_t1549183121 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgr::ilo_PlayBattleEndShow33(GameMgr)
extern "C"  void GameMgr_ilo_PlayBattleEndShow33_m52611856 (Il2CppObject * __this /* static, unused */, GameMgr_t1469029542 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgr::ilo_SendSaveHpMpData34(GameMgr,System.Boolean)
extern "C"  void GameMgr_ilo_SendSaveHpMpData34_m1009352830 (Il2CppObject * __this /* static, unused */, GameMgr_t1469029542 * ____this0, bool ___isWin1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single GameMgr::ilo_get_hp35(Hero)
extern "C"  float GameMgr_ilo_get_hp35_m2586644543 (Il2CppObject * __this /* static, unused */, Hero_t2245658 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single GameMgr::ilo_get_maxHp36(CombatEntity)
extern "C"  float GameMgr_ilo_get_maxHp36_m2554694539 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single GameMgr::ilo_get_maxRage37(CombatEntity)
extern "C"  float GameMgr_ilo_get_maxRage37_m952564881 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgr::ilo_Stop38(CameraAnimationMgr)
extern "C"  void GameMgr_ilo_Stop38_m1438932800 (Il2CppObject * __this /* static, unused */, CameraAnimationMgr_t3311695833 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgr::ilo_LeaveBattleReqOnWin39(HeroMgr,System.Action)
extern "C"  void GameMgr_ilo_LeaveBattleReqOnWin39_m650562535 (Il2CppObject * __this /* static, unused */, HeroMgr_t2475965342 * ____this0, Action_t3771233898 * ___callBack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 GameMgr::ilo_DelayCall40(System.Single,System.Action)
extern "C"  uint32_t GameMgr_ilo_DelayCall40_m2529805690 (Il2CppObject * __this /* static, unused */, float ___delaytime0, Action_t3771233898 * ___act1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameMgr::ilo_get_IsPlaying41(BossAnimationMgr)
extern "C"  bool GameMgr_ilo_get_IsPlaying41_m1276534459 (Il2CppObject * __this /* static, unused */, BossAnimationMgr_t150948897 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 GameMgr::ilo_GetTotalRounds42(LevelConfigManager,System.Int32)
extern "C"  int32_t GameMgr_ilo_GetTotalRounds42_m2833760937 (Il2CppObject * __this /* static, unused */, LevelConfigManager_t657947911 * ____this0, int32_t ___level1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single GameMgr::ilo_CaptainViewRange43(GameMgr,NpcMgr/EditorNpcType)
extern "C"  float GameMgr_ilo_CaptainViewRange43_m65973725 (Il2CppObject * __this /* static, unused */, GameMgr_t1469029542 * ____this0, int32_t ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CombatEntity> GameMgr::ilo_GetCurRoundMonster44(NpcMgr)
extern "C"  List_1_t2052323047 * GameMgr_ilo_GetCurRoundMonster44_m3153189319 (Il2CppObject * __this /* static, unused */, NpcMgr_t2339534743 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single GameMgr::ilo_get_halfwidth45(CombatEntity)
extern "C"  float GameMgr_ilo_get_halfwidth45_m333260816 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single GameMgr::ilo_get_atkRange46(CombatEntity)
extern "C"  float GameMgr_ilo_get_atkRange46_m327236607 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 GameMgr::ilo_get_curRound47(NpcMgr)
extern "C"  int32_t GameMgr_ilo_get_curRound47_m4044090151 (Il2CppObject * __this /* static, unused */, NpcMgr_t2339534743 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgr::ilo_OnTriggerNpcs48(NpcMgr,CombatEntity)
extern "C"  void GameMgr_ilo_OnTriggerNpcs48_m1673287815 (Il2CppObject * __this /* static, unused */, NpcMgr_t2339534743 * ____this0, CombatEntity_t684137495 * ___main1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 GameMgr::ilo_GetTotalOtherRounds49(LevelConfigManager,System.Int32)
extern "C"  int32_t GameMgr_ilo_GetTotalOtherRounds49_m3627913172 (Il2CppObject * __this /* static, unused */, LevelConfigManager_t657947911 * ____this0, int32_t ___level1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 GameMgr::ilo_get_curOtherRound50(NpcMgr)
extern "C"  int32_t GameMgr_ilo_get_curOtherRound50_m2338682615 (Il2CppObject * __this /* static, unused */, NpcMgr_t2339534743 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CombatEntity> GameMgr::ilo_GetCurRoundOther51(NpcMgr)
extern "C"  List_1_t2052323047 * GameMgr_ilo_GetCurRoundOther51_m2427401817 (Il2CppObject * __this /* static, unused */, NpcMgr_t2339534743 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameMgr::ilo_get_isAttack52(CombatEntity)
extern "C"  bool GameMgr_ilo_get_isAttack52_m4083965055 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgr::ilo_OnTriggerOthers53(NpcMgr,CombatEntity)
extern "C"  void GameMgr_ilo_OnTriggerOthers53_m1138947548 (Il2CppObject * __this /* static, unused */, NpcMgr_t2339534743 * ____this0, CombatEntity_t684137495 * ___main1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CombatEntity> GameMgr::ilo_GetCurUnFinishNpc54(NpcMgr)
extern "C"  List_1_t2052323047 * GameMgr_ilo_GetCurUnFinishNpc54_m2373570221 (Il2CppObject * __this /* static, unused */, NpcMgr_t2339534743 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgr::ilo_OnTriggerUnfinish55(NpcMgr,CombatEntity)
extern "C"  void GameMgr_ilo_OnTriggerUnfinish55_m3672007153 (Il2CppObject * __this /* static, unused */, NpcMgr_t2339534743 * ____this0, CombatEntity_t684137495 * ___main1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CombatEntity> GameMgr::ilo_GetAllAniList56(NpcMgr)
extern "C"  List_1_t2052323047 * GameMgr_ilo_GetAllAniList56_m4042085011 (Il2CppObject * __this /* static, unused */, NpcMgr_t2339534743 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameMgr::ilo_get_isDeath57(CombatEntity)
extern "C"  bool GameMgr_ilo_get_isDeath57_m385834192 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameMgr::ilo_get_isNotSelected58(CombatEntity)
extern "C"  bool GameMgr_ilo_get_isNotSelected58_m1405807403 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameMgr::ilo_get_canMove59(CombatEntity)
extern "C"  bool GameMgr_ilo_get_canMove59_m3364432809 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CombatEntity> GameMgr::ilo_GetCameraShotMonsters60(NpcMgr)
extern "C"  List_1_t2052323047 * GameMgr_ilo_GetCameraShotMonsters60_m3695309035 (Il2CppObject * __this /* static, unused */, NpcMgr_t2339534743 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CameraShotMgr GameMgr::ilo_get_CameraShotMgr61()
extern "C"  CameraShotMgr_t1580128697 * GameMgr_ilo_get_CameraShotMgr61_m1541465653 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CombatEntity> GameMgr::ilo_GetAllFriendsEntity62(GameMgr)
extern "C"  List_1_t2052323047 * GameMgr_ilo_GetAllFriendsEntity62_m3533876745 (Il2CppObject * __this /* static, unused */, GameMgr_t1469029542 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// EntityEnum.UnitCamp GameMgr::ilo_get_unitCamp63(CombatEntity)
extern "C"  int32_t GameMgr_ilo_get_unitCamp63_m3281349698 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CombatEntity> GameMgr::ilo_GetCameraShotFriends64(NpcMgr)
extern "C"  List_1_t2052323047 * GameMgr_ilo_GetCameraShotFriends64_m1281477455 (Il2CppObject * __this /* static, unused */, NpcMgr_t2339534743 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single GameMgr::ilo_get_findEnemyRange65(CombatEntity)
extern "C"  float GameMgr_ilo_get_findEnemyRange65_m1827578021 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CombatEntity GameMgr::ilo_GetNearestEnemy66(GameMgr,CombatEntity)
extern "C"  CombatEntity_t684137495 * GameMgr_ilo_GetNearestEnemy66_m3407585625 (Il2CppObject * __this /* static, unused */, GameMgr_t1469029542 * ____this0, CombatEntity_t684137495 * ___unit1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Hero GameMgr::ilo_GetHeroByID67(HeroMgr,System.Int32)
extern "C"  Hero_t2245658 * GameMgr_ilo_GetHeroByID67_m2557489273 (Il2CppObject * __this /* static, unused */, HeroMgr_t2475965342 * ____this0, int32_t ___id1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CombatEntity GameMgr::ilo_GetNpc68(NpcMgr,System.Int32)
extern "C"  CombatEntity_t684137495 * GameMgr_ilo_GetNpc68_m1347243157 (Il2CppObject * __this /* static, unused */, NpcMgr_t2339534743 * ____this0, int32_t ___id1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CombatEntity> GameMgr::ilo_GetAllHostilEntity69(GameMgr)
extern "C"  List_1_t2052323047 * GameMgr_ilo_GetAllHostilEntity69_m459927496 (Il2CppObject * __this /* static, unused */, GameMgr_t1469029542 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CombatEntity> GameMgr::ilo_GetAllEnemy70(GameMgr,CombatEntity)
extern "C"  List_1_t2052323047 * GameMgr_ilo_GetAllEnemy70_m941400179 (Il2CppObject * __this /* static, unused */, GameMgr_t1469029542 * ____this0, CombatEntity_t684137495 * ___entity1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// EntityEnum.UnitType GameMgr::ilo_get_unitType71(CombatEntity)
extern "C"  int32_t GameMgr_ilo_get_unitType71_m2961839569 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CombatEntity> GameMgr::ilo_GetAllAlly72(GameMgr,CombatEntity)
extern "C"  List_1_t2052323047 * GameMgr_ilo_GetAllAlly72_m447381897 (Il2CppObject * __this /* static, unused */, GameMgr_t1469029542 * ____this0, CombatEntity_t684137495 * ___entity1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Entity.Behavior.IBehaviorCtrl GameMgr::ilo_get_bvrCtrl73(CombatEntity)
extern "C"  IBehaviorCtrl_t4225040900 * GameMgr_ilo_get_bvrCtrl73_m3061911155 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgr::ilo_TranBehavior74(Entity.Behavior.IBehaviorCtrl,Entity.Behavior.EBehaviorID,System.Object[])
extern "C"  void GameMgr_ilo_TranBehavior74_m3071069384 (Il2CppObject * __this /* static, unused */, IBehaviorCtrl_t4225040900 * ____this0, uint8_t ___id1, ObjectU5BU5D_t1108656482* ___arg2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgr::ilo_set_isFrozen75(GameMgr,System.Boolean)
extern "C"  void GameMgr_ilo_set_isFrozen75_m3925378152 (Il2CppObject * __this /* static, unused */, GameMgr_t1469029542 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// AnimationRunner GameMgr::ilo_get_characterAnim76(CombatEntity)
extern "C"  AnimationRunner_t1015409588 * GameMgr_ilo_get_characterAnim76_m2103861032 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgr::ilo_Play77(AnimationRunner,AnimationRunner/AniType,System.Single,System.Boolean)
extern "C"  void GameMgr_ilo_Play77_m1514596911 (Il2CppObject * __this /* static, unused */, AnimationRunner_t1015409588 * ____this0, int32_t ___aniType1, float ___playTime2, bool ___isskill3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgr::ilo_set_isFrozenIdle78(GameMgr,System.Boolean)
extern "C"  void GameMgr_ilo_set_isFrozenIdle78_m1035381343 (Il2CppObject * __this /* static, unused */, GameMgr_t1469029542 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CombatEntity> GameMgr::ilo_GetFriendsList79(NpcMgr)
extern "C"  List_1_t2052323047 * GameMgr_ilo_GetFriendsList79_m2233659120 (Il2CppObject * __this /* static, unused */, NpcMgr_t2339534743 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgr::ilo_set_isFrozen80(CombatEntity,System.Boolean)
extern "C"  void GameMgr_ilo_set_isFrozen80_m3567307171 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameMgr::ilo_get_isBattleStart81(GameMgr)
extern "C"  bool GameMgr_ilo_get_isBattleStart81_m2471798616 (Il2CppObject * __this /* static, unused */, GameMgr_t1469029542 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgr::ilo_LeaveAutoMode82(HeroMgr)
extern "C"  void GameMgr_ilo_LeaveAutoMode82_m2541044769 (Il2CppObject * __this /* static, unused */, HeroMgr_t2475965342 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgr::ilo_AddBuff83(BuffCtrl,System.Int32)
extern "C"  void GameMgr_ilo_AddBuff83_m2695829206 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, int32_t ___id1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// BuffCtrl GameMgr::ilo_get_buffCtrl84(CombatEntity)
extern "C"  BuffCtrl_t2836564350 * GameMgr_ilo_get_buffCtrl84_m3482493951 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgr::ilo_RemoveBuffByID85(BuffCtrl,System.Int32)
extern "C"  void GameMgr_ilo_RemoveBuffByID85_m1662454461 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, int32_t ___id1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CombatEntity> GameMgr::ilo_GetAllFriendsEntity86(GameMgr)
extern "C"  List_1_t2052323047 * GameMgr_ilo_GetAllFriendsEntity86_m3448887303 (Il2CppObject * __this /* static, unused */, GameMgr_t1469029542 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgr::ilo_WinDead87(CombatEntity)
extern "C"  void GameMgr_ilo_WinDead87_m552231928 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgr::ilo_Clear88(HeroMgr)
extern "C"  void GameMgr_ilo_Clear88_m4174791383 (Il2CppObject * __this /* static, unused */, HeroMgr_t2475965342 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// EffectMgr GameMgr::ilo_get_EffectMgr89()
extern "C"  EffectMgr_t535289511 * GameMgr_ilo_get_EffectMgr89_m3107458491 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgr::ilo_Clear90(EffectMgr)
extern "C"  void GameMgr_ilo_Clear90_m3142871415 (Il2CppObject * __this /* static, unused */, EffectMgr_t535289511 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgr::ilo_Clear91(GameQutilyCtr)
extern "C"  void GameMgr_ilo_Clear91_m2426269268 (Il2CppObject * __this /* static, unused */, GameQutilyCtr_t3963256169 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgr::ilo_Clear92(HeadUpBloodMgr)
extern "C"  void GameMgr_ilo_Clear92_m3742444989 (Il2CppObject * __this /* static, unused */, HeadUpBloodMgr_t2330866585 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgr::ilo_OnDestroy93(SkillHurtShow)
extern "C"  void GameMgr_ilo_OnDestroy93_m3542137712 (Il2CppObject * __this /* static, unused */, SkillHurtShow_t1728264445 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameMgr::ilo_GradeStarType94(GameMgr,System.Int32,System.Int32)
extern "C"  bool GameMgr_ilo_GradeStarType94_m1179551146 (Il2CppObject * __this /* static, unused */, GameMgr_t1469029542 * ____this0, int32_t ___gradeTypeid1, int32_t ___gradeValue2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 GameMgr::ilo_get_time95(GameMgr)
extern "C"  int32_t GameMgr_ilo_get_time95_m3228389302 (Il2CppObject * __this /* static, unused */, GameMgr_t1469029542 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 GameMgr::ilo_GetCurHostilNpcCount96(NpcMgr)
extern "C"  int32_t GameMgr_ilo_GetCurHostilNpcCount96_m461452333 (Il2CppObject * __this /* static, unused */, NpcMgr_t2339534743 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgr::ilo_set_IsPlayPloting97(GameMgr,System.Boolean)
extern "C"  void GameMgr_ilo_set_IsPlayPloting97_m4274917829 (Il2CppObject * __this /* static, unused */, GameMgr_t1469029542 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CSGameDataMgr GameMgr::ilo_get_CSGameDataMgr98()
extern "C"  CSGameDataMgr_t2623305516 * GameMgr_ilo_get_CSGameDataMgr98_m1324982073 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CSPlusAtt> GameMgr::ilo_GetLeaderPlusAtt99(CSHeroData)
extern "C"  List_1_t341533415 * GameMgr_ilo_GetLeaderPlusAtt99_m1005265646 (Il2CppObject * __this /* static, unused */, CSHeroData_t3763839828 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GameMgr::ilo_GetAttributeName100(System.String)
extern "C"  String_t* GameMgr_ilo_GetAttributeName100_m151228911 (Il2CppObject * __this /* static, unused */, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GameMgr::ilo_FormatUIString101(System.Int32,System.Object[])
extern "C"  String_t* GameMgr_ilo_FormatUIString101_m2276277286 (Il2CppObject * __this /* static, unused */, int32_t ___key0, ObjectU5BU5D_t1108656482* ___datas1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CSPlusAtt> GameMgr::ilo_GetReinPlusAtts102(CSHeroData)
extern "C"  List_1_t341533415 * GameMgr_ilo_GetReinPlusAtts102_m2871343743 (Il2CppObject * __this /* static, unused */, CSHeroData_t3763839828 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GameMgr::ilo_GetCountryName103(CSFightforData)
extern "C"  String_t* GameMgr_ilo_GetCountryName103_m572250121 (Il2CppObject * __this /* static, unused */, CSFightforData_t215334547 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CSPlusAtt> GameMgr::ilo_GetAttPlus104(CSCampfightData)
extern "C"  List_1_t341533415 * GameMgr_ilo_GetAttPlus104_m3696715089 (Il2CppObject * __this /* static, unused */, CSCampfightData_t179152873 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject GameMgr::ilo_Instantiate105(UnityEngine.GameObject)
extern "C"  GameObject_t3674682005 * GameMgr_ilo_Instantiate105_m1575042753 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgr::ilo_ChangeParent106(UnityEngine.GameObject,UnityEngine.GameObject,System.Boolean)
extern "C"  void GameMgr_ilo_ChangeParent106_m3781549444 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ____this0, GameObject_t3674682005 * ___parent1, bool ___resetMat2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMgr::ilo_Init107(CameraMouseMove_PVP,System.String)
extern "C"  void GameMgr_ilo_Init107_m1818937306 (Il2CppObject * __this /* static, unused */, CameraMouseMove_PVP_t2551559708 * ____this0, String_t* ___camera_limit1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
