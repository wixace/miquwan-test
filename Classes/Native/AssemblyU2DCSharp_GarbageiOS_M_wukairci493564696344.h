﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_wukairci49
struct  M_wukairci49_t3564696344  : public Il2CppObject
{
public:
	// System.Single GarbageiOS.M_wukairci49::_rerorke
	float ____rerorke_0;
	// System.Int32 GarbageiOS.M_wukairci49::_gaygouMouhezea
	int32_t ____gaygouMouhezea_1;
	// System.UInt32 GarbageiOS.M_wukairci49::_mispal
	uint32_t ____mispal_2;
	// System.Boolean GarbageiOS.M_wukairci49::_sismememVesaime
	bool ____sismememVesaime_3;
	// System.Single GarbageiOS.M_wukairci49::_neyawQenalmow
	float ____neyawQenalmow_4;
	// System.Int32 GarbageiOS.M_wukairci49::_hiscoosel
	int32_t ____hiscoosel_5;
	// System.Boolean GarbageiOS.M_wukairci49::_neemiLuneeqe
	bool ____neemiLuneeqe_6;
	// System.Boolean GarbageiOS.M_wukairci49::_qinaldar
	bool ____qinaldar_7;
	// System.Int32 GarbageiOS.M_wukairci49::_xonawgerePeha
	int32_t ____xonawgerePeha_8;
	// System.Int32 GarbageiOS.M_wukairci49::_neruzalNallmalha
	int32_t ____neruzalNallmalha_9;
	// System.UInt32 GarbageiOS.M_wukairci49::_detrowte
	uint32_t ____detrowte_10;
	// System.Boolean GarbageiOS.M_wukairci49::_nemissee
	bool ____nemissee_11;
	// System.Single GarbageiOS.M_wukairci49::_neve
	float ____neve_12;
	// System.Single GarbageiOS.M_wukairci49::_cirurDajir
	float ____cirurDajir_13;
	// System.UInt32 GarbageiOS.M_wukairci49::_cowgorKirvem
	uint32_t ____cowgorKirvem_14;
	// System.Int32 GarbageiOS.M_wukairci49::_lowreforCiroris
	int32_t ____lowreforCiroris_15;
	// System.Int32 GarbageiOS.M_wukairci49::_cestirdrir
	int32_t ____cestirdrir_16;
	// System.Single GarbageiOS.M_wukairci49::_douherSudi
	float ____douherSudi_17;
	// System.Single GarbageiOS.M_wukairci49::_stigu
	float ____stigu_18;
	// System.UInt32 GarbageiOS.M_wukairci49::_rawtas
	uint32_t ____rawtas_19;
	// System.String GarbageiOS.M_wukairci49::_rebu
	String_t* ____rebu_20;
	// System.String GarbageiOS.M_wukairci49::_kamayDraimelra
	String_t* ____kamayDraimelra_21;
	// System.Single GarbageiOS.M_wukairci49::_callornaJallre
	float ____callornaJallre_22;
	// System.Boolean GarbageiOS.M_wukairci49::_rocaCirmay
	bool ____rocaCirmay_23;
	// System.UInt32 GarbageiOS.M_wukairci49::_ceterestow
	uint32_t ____ceterestow_24;
	// System.Single GarbageiOS.M_wukairci49::_meqursarDelo
	float ____meqursarDelo_25;
	// System.UInt32 GarbageiOS.M_wukairci49::_wasisjou
	uint32_t ____wasisjou_26;
	// System.Single GarbageiOS.M_wukairci49::_qurzaijouLihe
	float ____qurzaijouLihe_27;
	// System.UInt32 GarbageiOS.M_wukairci49::_lacirkearHeme
	uint32_t ____lacirkearHeme_28;
	// System.Int32 GarbageiOS.M_wukairci49::_joojekallPisnownere
	int32_t ____joojekallPisnownere_29;
	// System.Single GarbageiOS.M_wukairci49::_besemserTarrel
	float ____besemserTarrel_30;
	// System.Single GarbageiOS.M_wukairci49::_joudairbel
	float ____joudairbel_31;
	// System.Boolean GarbageiOS.M_wukairci49::_sishanaDituti
	bool ____sishanaDituti_32;
	// System.String GarbageiOS.M_wukairci49::_perejarborWhemtair
	String_t* ____perejarborWhemtair_33;
	// System.Single GarbageiOS.M_wukairci49::_stehurboo
	float ____stehurboo_34;
	// System.String GarbageiOS.M_wukairci49::_kisqo
	String_t* ____kisqo_35;

public:
	inline static int32_t get_offset_of__rerorke_0() { return static_cast<int32_t>(offsetof(M_wukairci49_t3564696344, ____rerorke_0)); }
	inline float get__rerorke_0() const { return ____rerorke_0; }
	inline float* get_address_of__rerorke_0() { return &____rerorke_0; }
	inline void set__rerorke_0(float value)
	{
		____rerorke_0 = value;
	}

	inline static int32_t get_offset_of__gaygouMouhezea_1() { return static_cast<int32_t>(offsetof(M_wukairci49_t3564696344, ____gaygouMouhezea_1)); }
	inline int32_t get__gaygouMouhezea_1() const { return ____gaygouMouhezea_1; }
	inline int32_t* get_address_of__gaygouMouhezea_1() { return &____gaygouMouhezea_1; }
	inline void set__gaygouMouhezea_1(int32_t value)
	{
		____gaygouMouhezea_1 = value;
	}

	inline static int32_t get_offset_of__mispal_2() { return static_cast<int32_t>(offsetof(M_wukairci49_t3564696344, ____mispal_2)); }
	inline uint32_t get__mispal_2() const { return ____mispal_2; }
	inline uint32_t* get_address_of__mispal_2() { return &____mispal_2; }
	inline void set__mispal_2(uint32_t value)
	{
		____mispal_2 = value;
	}

	inline static int32_t get_offset_of__sismememVesaime_3() { return static_cast<int32_t>(offsetof(M_wukairci49_t3564696344, ____sismememVesaime_3)); }
	inline bool get__sismememVesaime_3() const { return ____sismememVesaime_3; }
	inline bool* get_address_of__sismememVesaime_3() { return &____sismememVesaime_3; }
	inline void set__sismememVesaime_3(bool value)
	{
		____sismememVesaime_3 = value;
	}

	inline static int32_t get_offset_of__neyawQenalmow_4() { return static_cast<int32_t>(offsetof(M_wukairci49_t3564696344, ____neyawQenalmow_4)); }
	inline float get__neyawQenalmow_4() const { return ____neyawQenalmow_4; }
	inline float* get_address_of__neyawQenalmow_4() { return &____neyawQenalmow_4; }
	inline void set__neyawQenalmow_4(float value)
	{
		____neyawQenalmow_4 = value;
	}

	inline static int32_t get_offset_of__hiscoosel_5() { return static_cast<int32_t>(offsetof(M_wukairci49_t3564696344, ____hiscoosel_5)); }
	inline int32_t get__hiscoosel_5() const { return ____hiscoosel_5; }
	inline int32_t* get_address_of__hiscoosel_5() { return &____hiscoosel_5; }
	inline void set__hiscoosel_5(int32_t value)
	{
		____hiscoosel_5 = value;
	}

	inline static int32_t get_offset_of__neemiLuneeqe_6() { return static_cast<int32_t>(offsetof(M_wukairci49_t3564696344, ____neemiLuneeqe_6)); }
	inline bool get__neemiLuneeqe_6() const { return ____neemiLuneeqe_6; }
	inline bool* get_address_of__neemiLuneeqe_6() { return &____neemiLuneeqe_6; }
	inline void set__neemiLuneeqe_6(bool value)
	{
		____neemiLuneeqe_6 = value;
	}

	inline static int32_t get_offset_of__qinaldar_7() { return static_cast<int32_t>(offsetof(M_wukairci49_t3564696344, ____qinaldar_7)); }
	inline bool get__qinaldar_7() const { return ____qinaldar_7; }
	inline bool* get_address_of__qinaldar_7() { return &____qinaldar_7; }
	inline void set__qinaldar_7(bool value)
	{
		____qinaldar_7 = value;
	}

	inline static int32_t get_offset_of__xonawgerePeha_8() { return static_cast<int32_t>(offsetof(M_wukairci49_t3564696344, ____xonawgerePeha_8)); }
	inline int32_t get__xonawgerePeha_8() const { return ____xonawgerePeha_8; }
	inline int32_t* get_address_of__xonawgerePeha_8() { return &____xonawgerePeha_8; }
	inline void set__xonawgerePeha_8(int32_t value)
	{
		____xonawgerePeha_8 = value;
	}

	inline static int32_t get_offset_of__neruzalNallmalha_9() { return static_cast<int32_t>(offsetof(M_wukairci49_t3564696344, ____neruzalNallmalha_9)); }
	inline int32_t get__neruzalNallmalha_9() const { return ____neruzalNallmalha_9; }
	inline int32_t* get_address_of__neruzalNallmalha_9() { return &____neruzalNallmalha_9; }
	inline void set__neruzalNallmalha_9(int32_t value)
	{
		____neruzalNallmalha_9 = value;
	}

	inline static int32_t get_offset_of__detrowte_10() { return static_cast<int32_t>(offsetof(M_wukairci49_t3564696344, ____detrowte_10)); }
	inline uint32_t get__detrowte_10() const { return ____detrowte_10; }
	inline uint32_t* get_address_of__detrowte_10() { return &____detrowte_10; }
	inline void set__detrowte_10(uint32_t value)
	{
		____detrowte_10 = value;
	}

	inline static int32_t get_offset_of__nemissee_11() { return static_cast<int32_t>(offsetof(M_wukairci49_t3564696344, ____nemissee_11)); }
	inline bool get__nemissee_11() const { return ____nemissee_11; }
	inline bool* get_address_of__nemissee_11() { return &____nemissee_11; }
	inline void set__nemissee_11(bool value)
	{
		____nemissee_11 = value;
	}

	inline static int32_t get_offset_of__neve_12() { return static_cast<int32_t>(offsetof(M_wukairci49_t3564696344, ____neve_12)); }
	inline float get__neve_12() const { return ____neve_12; }
	inline float* get_address_of__neve_12() { return &____neve_12; }
	inline void set__neve_12(float value)
	{
		____neve_12 = value;
	}

	inline static int32_t get_offset_of__cirurDajir_13() { return static_cast<int32_t>(offsetof(M_wukairci49_t3564696344, ____cirurDajir_13)); }
	inline float get__cirurDajir_13() const { return ____cirurDajir_13; }
	inline float* get_address_of__cirurDajir_13() { return &____cirurDajir_13; }
	inline void set__cirurDajir_13(float value)
	{
		____cirurDajir_13 = value;
	}

	inline static int32_t get_offset_of__cowgorKirvem_14() { return static_cast<int32_t>(offsetof(M_wukairci49_t3564696344, ____cowgorKirvem_14)); }
	inline uint32_t get__cowgorKirvem_14() const { return ____cowgorKirvem_14; }
	inline uint32_t* get_address_of__cowgorKirvem_14() { return &____cowgorKirvem_14; }
	inline void set__cowgorKirvem_14(uint32_t value)
	{
		____cowgorKirvem_14 = value;
	}

	inline static int32_t get_offset_of__lowreforCiroris_15() { return static_cast<int32_t>(offsetof(M_wukairci49_t3564696344, ____lowreforCiroris_15)); }
	inline int32_t get__lowreforCiroris_15() const { return ____lowreforCiroris_15; }
	inline int32_t* get_address_of__lowreforCiroris_15() { return &____lowreforCiroris_15; }
	inline void set__lowreforCiroris_15(int32_t value)
	{
		____lowreforCiroris_15 = value;
	}

	inline static int32_t get_offset_of__cestirdrir_16() { return static_cast<int32_t>(offsetof(M_wukairci49_t3564696344, ____cestirdrir_16)); }
	inline int32_t get__cestirdrir_16() const { return ____cestirdrir_16; }
	inline int32_t* get_address_of__cestirdrir_16() { return &____cestirdrir_16; }
	inline void set__cestirdrir_16(int32_t value)
	{
		____cestirdrir_16 = value;
	}

	inline static int32_t get_offset_of__douherSudi_17() { return static_cast<int32_t>(offsetof(M_wukairci49_t3564696344, ____douherSudi_17)); }
	inline float get__douherSudi_17() const { return ____douherSudi_17; }
	inline float* get_address_of__douherSudi_17() { return &____douherSudi_17; }
	inline void set__douherSudi_17(float value)
	{
		____douherSudi_17 = value;
	}

	inline static int32_t get_offset_of__stigu_18() { return static_cast<int32_t>(offsetof(M_wukairci49_t3564696344, ____stigu_18)); }
	inline float get__stigu_18() const { return ____stigu_18; }
	inline float* get_address_of__stigu_18() { return &____stigu_18; }
	inline void set__stigu_18(float value)
	{
		____stigu_18 = value;
	}

	inline static int32_t get_offset_of__rawtas_19() { return static_cast<int32_t>(offsetof(M_wukairci49_t3564696344, ____rawtas_19)); }
	inline uint32_t get__rawtas_19() const { return ____rawtas_19; }
	inline uint32_t* get_address_of__rawtas_19() { return &____rawtas_19; }
	inline void set__rawtas_19(uint32_t value)
	{
		____rawtas_19 = value;
	}

	inline static int32_t get_offset_of__rebu_20() { return static_cast<int32_t>(offsetof(M_wukairci49_t3564696344, ____rebu_20)); }
	inline String_t* get__rebu_20() const { return ____rebu_20; }
	inline String_t** get_address_of__rebu_20() { return &____rebu_20; }
	inline void set__rebu_20(String_t* value)
	{
		____rebu_20 = value;
		Il2CppCodeGenWriteBarrier(&____rebu_20, value);
	}

	inline static int32_t get_offset_of__kamayDraimelra_21() { return static_cast<int32_t>(offsetof(M_wukairci49_t3564696344, ____kamayDraimelra_21)); }
	inline String_t* get__kamayDraimelra_21() const { return ____kamayDraimelra_21; }
	inline String_t** get_address_of__kamayDraimelra_21() { return &____kamayDraimelra_21; }
	inline void set__kamayDraimelra_21(String_t* value)
	{
		____kamayDraimelra_21 = value;
		Il2CppCodeGenWriteBarrier(&____kamayDraimelra_21, value);
	}

	inline static int32_t get_offset_of__callornaJallre_22() { return static_cast<int32_t>(offsetof(M_wukairci49_t3564696344, ____callornaJallre_22)); }
	inline float get__callornaJallre_22() const { return ____callornaJallre_22; }
	inline float* get_address_of__callornaJallre_22() { return &____callornaJallre_22; }
	inline void set__callornaJallre_22(float value)
	{
		____callornaJallre_22 = value;
	}

	inline static int32_t get_offset_of__rocaCirmay_23() { return static_cast<int32_t>(offsetof(M_wukairci49_t3564696344, ____rocaCirmay_23)); }
	inline bool get__rocaCirmay_23() const { return ____rocaCirmay_23; }
	inline bool* get_address_of__rocaCirmay_23() { return &____rocaCirmay_23; }
	inline void set__rocaCirmay_23(bool value)
	{
		____rocaCirmay_23 = value;
	}

	inline static int32_t get_offset_of__ceterestow_24() { return static_cast<int32_t>(offsetof(M_wukairci49_t3564696344, ____ceterestow_24)); }
	inline uint32_t get__ceterestow_24() const { return ____ceterestow_24; }
	inline uint32_t* get_address_of__ceterestow_24() { return &____ceterestow_24; }
	inline void set__ceterestow_24(uint32_t value)
	{
		____ceterestow_24 = value;
	}

	inline static int32_t get_offset_of__meqursarDelo_25() { return static_cast<int32_t>(offsetof(M_wukairci49_t3564696344, ____meqursarDelo_25)); }
	inline float get__meqursarDelo_25() const { return ____meqursarDelo_25; }
	inline float* get_address_of__meqursarDelo_25() { return &____meqursarDelo_25; }
	inline void set__meqursarDelo_25(float value)
	{
		____meqursarDelo_25 = value;
	}

	inline static int32_t get_offset_of__wasisjou_26() { return static_cast<int32_t>(offsetof(M_wukairci49_t3564696344, ____wasisjou_26)); }
	inline uint32_t get__wasisjou_26() const { return ____wasisjou_26; }
	inline uint32_t* get_address_of__wasisjou_26() { return &____wasisjou_26; }
	inline void set__wasisjou_26(uint32_t value)
	{
		____wasisjou_26 = value;
	}

	inline static int32_t get_offset_of__qurzaijouLihe_27() { return static_cast<int32_t>(offsetof(M_wukairci49_t3564696344, ____qurzaijouLihe_27)); }
	inline float get__qurzaijouLihe_27() const { return ____qurzaijouLihe_27; }
	inline float* get_address_of__qurzaijouLihe_27() { return &____qurzaijouLihe_27; }
	inline void set__qurzaijouLihe_27(float value)
	{
		____qurzaijouLihe_27 = value;
	}

	inline static int32_t get_offset_of__lacirkearHeme_28() { return static_cast<int32_t>(offsetof(M_wukairci49_t3564696344, ____lacirkearHeme_28)); }
	inline uint32_t get__lacirkearHeme_28() const { return ____lacirkearHeme_28; }
	inline uint32_t* get_address_of__lacirkearHeme_28() { return &____lacirkearHeme_28; }
	inline void set__lacirkearHeme_28(uint32_t value)
	{
		____lacirkearHeme_28 = value;
	}

	inline static int32_t get_offset_of__joojekallPisnownere_29() { return static_cast<int32_t>(offsetof(M_wukairci49_t3564696344, ____joojekallPisnownere_29)); }
	inline int32_t get__joojekallPisnownere_29() const { return ____joojekallPisnownere_29; }
	inline int32_t* get_address_of__joojekallPisnownere_29() { return &____joojekallPisnownere_29; }
	inline void set__joojekallPisnownere_29(int32_t value)
	{
		____joojekallPisnownere_29 = value;
	}

	inline static int32_t get_offset_of__besemserTarrel_30() { return static_cast<int32_t>(offsetof(M_wukairci49_t3564696344, ____besemserTarrel_30)); }
	inline float get__besemserTarrel_30() const { return ____besemserTarrel_30; }
	inline float* get_address_of__besemserTarrel_30() { return &____besemserTarrel_30; }
	inline void set__besemserTarrel_30(float value)
	{
		____besemserTarrel_30 = value;
	}

	inline static int32_t get_offset_of__joudairbel_31() { return static_cast<int32_t>(offsetof(M_wukairci49_t3564696344, ____joudairbel_31)); }
	inline float get__joudairbel_31() const { return ____joudairbel_31; }
	inline float* get_address_of__joudairbel_31() { return &____joudairbel_31; }
	inline void set__joudairbel_31(float value)
	{
		____joudairbel_31 = value;
	}

	inline static int32_t get_offset_of__sishanaDituti_32() { return static_cast<int32_t>(offsetof(M_wukairci49_t3564696344, ____sishanaDituti_32)); }
	inline bool get__sishanaDituti_32() const { return ____sishanaDituti_32; }
	inline bool* get_address_of__sishanaDituti_32() { return &____sishanaDituti_32; }
	inline void set__sishanaDituti_32(bool value)
	{
		____sishanaDituti_32 = value;
	}

	inline static int32_t get_offset_of__perejarborWhemtair_33() { return static_cast<int32_t>(offsetof(M_wukairci49_t3564696344, ____perejarborWhemtair_33)); }
	inline String_t* get__perejarborWhemtair_33() const { return ____perejarborWhemtair_33; }
	inline String_t** get_address_of__perejarborWhemtair_33() { return &____perejarborWhemtair_33; }
	inline void set__perejarborWhemtair_33(String_t* value)
	{
		____perejarborWhemtair_33 = value;
		Il2CppCodeGenWriteBarrier(&____perejarborWhemtair_33, value);
	}

	inline static int32_t get_offset_of__stehurboo_34() { return static_cast<int32_t>(offsetof(M_wukairci49_t3564696344, ____stehurboo_34)); }
	inline float get__stehurboo_34() const { return ____stehurboo_34; }
	inline float* get_address_of__stehurboo_34() { return &____stehurboo_34; }
	inline void set__stehurboo_34(float value)
	{
		____stehurboo_34 = value;
	}

	inline static int32_t get_offset_of__kisqo_35() { return static_cast<int32_t>(offsetof(M_wukairci49_t3564696344, ____kisqo_35)); }
	inline String_t* get__kisqo_35() const { return ____kisqo_35; }
	inline String_t** get_address_of__kisqo_35() { return &____kisqo_35; }
	inline void set__kisqo_35(String_t* value)
	{
		____kisqo_35 = value;
		Il2CppCodeGenWriteBarrier(&____kisqo_35, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
