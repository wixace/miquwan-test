﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.Mail.LinkedResourceCollection
struct LinkedResourceCollection_t690461993;
// System.Net.Mail.LinkedResource
struct LinkedResource_t2218228715;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Net_Mail_LinkedResource2218228715.h"

// System.Void System.Net.Mail.LinkedResourceCollection::.ctor()
extern "C"  void LinkedResourceCollection__ctor_m939336720 (LinkedResourceCollection_t690461993 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Mail.LinkedResourceCollection::Dispose()
extern "C"  void LinkedResourceCollection_Dispose_m657738829 (LinkedResourceCollection_t690461993 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Mail.LinkedResourceCollection::Dispose(System.Boolean)
extern "C"  void LinkedResourceCollection_Dispose_m2837772036 (LinkedResourceCollection_t690461993 * __this, bool ___disposing0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Mail.LinkedResourceCollection::ClearItems()
extern "C"  void LinkedResourceCollection_ClearItems_m2430840231 (LinkedResourceCollection_t690461993 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Mail.LinkedResourceCollection::InsertItem(System.Int32,System.Net.Mail.LinkedResource)
extern "C"  void LinkedResourceCollection_InsertItem_m3282943207 (LinkedResourceCollection_t690461993 * __this, int32_t ___index0, LinkedResource_t2218228715 * ___item1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Mail.LinkedResourceCollection::RemoveItem(System.Int32)
extern "C"  void LinkedResourceCollection_RemoveItem_m3657446844 (LinkedResourceCollection_t690461993 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Mail.LinkedResourceCollection::SetItem(System.Int32,System.Net.Mail.LinkedResource)
extern "C"  void LinkedResourceCollection_SetItem_m679598372 (LinkedResourceCollection_t690461993 * __this, int32_t ___index0, LinkedResource_t2218228715 * ___item1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
