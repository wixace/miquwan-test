﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_daqole83
struct M_daqole83_t33599839;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_daqole8333599839.h"

// System.Void GarbageiOS.M_daqole83::.ctor()
extern "C"  void M_daqole83__ctor_m2238140340 (M_daqole83_t33599839 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_daqole83::M_nerhemleFairkere0(System.String[],System.Int32)
extern "C"  void M_daqole83_M_nerhemleFairkere0_m1714945960 (M_daqole83_t33599839 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_daqole83::M_xaco1(System.String[],System.Int32)
extern "C"  void M_daqole83_M_xaco1_m548523699 (M_daqole83_t33599839 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_daqole83::M_sojemgear2(System.String[],System.Int32)
extern "C"  void M_daqole83_M_sojemgear2_m1885965734 (M_daqole83_t33599839 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_daqole83::M_checotaCherrall3(System.String[],System.Int32)
extern "C"  void M_daqole83_M_checotaCherrall3_m3968491890 (M_daqole83_t33599839 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_daqole83::ilo_M_sojemgear21(GarbageiOS.M_daqole83,System.String[],System.Int32)
extern "C"  void M_daqole83_ilo_M_sojemgear21_m1478125587 (Il2CppObject * __this /* static, unused */, M_daqole83_t33599839 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
