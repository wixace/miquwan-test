﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t1659122786;
// System.Collections.Generic.Dictionary`2<MScrollView/MoveWay,MScrollView/OnMoveCallBackFun>
struct Dictionary_2_t807438799;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MScrollView
struct  MScrollView_t1133512511  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.Transform MScrollView::DragTarge
	Transform_t1659122786 * ___DragTarge_2;
	// System.Single MScrollView::FingerDragDistance
	float ___FingerDragDistance_3;
	// System.Single MScrollView::MoveDistanceUnit
	float ___MoveDistanceUnit_4;
	// System.Single MScrollView::MoveSumTime
	float ___MoveSumTime_5;
	// System.Int32 MScrollView::Index
	int32_t ___Index_6;
	// System.Int32 MScrollView::Size
	int32_t ___Size_7;
	// System.Collections.Generic.Dictionary`2<MScrollView/MoveWay,MScrollView/OnMoveCallBackFun> MScrollView::dicMovefunc
	Dictionary_2_t807438799 * ___dicMovefunc_8;
	// System.Single MScrollView::curTime
	float ___curTime_9;
	// UnityEngine.Vector3 MScrollView::initPosition
	Vector3_t4282066566  ___initPosition_10;
	// System.Boolean MScrollView::isTouch
	bool ___isTouch_11;
	// System.Boolean MScrollView::isRight
	bool ___isRight_12;
	// System.Boolean MScrollView::isLeft
	bool ___isLeft_13;
	// System.Boolean MScrollView::isOnDrag
	bool ___isOnDrag_14;

public:
	inline static int32_t get_offset_of_DragTarge_2() { return static_cast<int32_t>(offsetof(MScrollView_t1133512511, ___DragTarge_2)); }
	inline Transform_t1659122786 * get_DragTarge_2() const { return ___DragTarge_2; }
	inline Transform_t1659122786 ** get_address_of_DragTarge_2() { return &___DragTarge_2; }
	inline void set_DragTarge_2(Transform_t1659122786 * value)
	{
		___DragTarge_2 = value;
		Il2CppCodeGenWriteBarrier(&___DragTarge_2, value);
	}

	inline static int32_t get_offset_of_FingerDragDistance_3() { return static_cast<int32_t>(offsetof(MScrollView_t1133512511, ___FingerDragDistance_3)); }
	inline float get_FingerDragDistance_3() const { return ___FingerDragDistance_3; }
	inline float* get_address_of_FingerDragDistance_3() { return &___FingerDragDistance_3; }
	inline void set_FingerDragDistance_3(float value)
	{
		___FingerDragDistance_3 = value;
	}

	inline static int32_t get_offset_of_MoveDistanceUnit_4() { return static_cast<int32_t>(offsetof(MScrollView_t1133512511, ___MoveDistanceUnit_4)); }
	inline float get_MoveDistanceUnit_4() const { return ___MoveDistanceUnit_4; }
	inline float* get_address_of_MoveDistanceUnit_4() { return &___MoveDistanceUnit_4; }
	inline void set_MoveDistanceUnit_4(float value)
	{
		___MoveDistanceUnit_4 = value;
	}

	inline static int32_t get_offset_of_MoveSumTime_5() { return static_cast<int32_t>(offsetof(MScrollView_t1133512511, ___MoveSumTime_5)); }
	inline float get_MoveSumTime_5() const { return ___MoveSumTime_5; }
	inline float* get_address_of_MoveSumTime_5() { return &___MoveSumTime_5; }
	inline void set_MoveSumTime_5(float value)
	{
		___MoveSumTime_5 = value;
	}

	inline static int32_t get_offset_of_Index_6() { return static_cast<int32_t>(offsetof(MScrollView_t1133512511, ___Index_6)); }
	inline int32_t get_Index_6() const { return ___Index_6; }
	inline int32_t* get_address_of_Index_6() { return &___Index_6; }
	inline void set_Index_6(int32_t value)
	{
		___Index_6 = value;
	}

	inline static int32_t get_offset_of_Size_7() { return static_cast<int32_t>(offsetof(MScrollView_t1133512511, ___Size_7)); }
	inline int32_t get_Size_7() const { return ___Size_7; }
	inline int32_t* get_address_of_Size_7() { return &___Size_7; }
	inline void set_Size_7(int32_t value)
	{
		___Size_7 = value;
	}

	inline static int32_t get_offset_of_dicMovefunc_8() { return static_cast<int32_t>(offsetof(MScrollView_t1133512511, ___dicMovefunc_8)); }
	inline Dictionary_2_t807438799 * get_dicMovefunc_8() const { return ___dicMovefunc_8; }
	inline Dictionary_2_t807438799 ** get_address_of_dicMovefunc_8() { return &___dicMovefunc_8; }
	inline void set_dicMovefunc_8(Dictionary_2_t807438799 * value)
	{
		___dicMovefunc_8 = value;
		Il2CppCodeGenWriteBarrier(&___dicMovefunc_8, value);
	}

	inline static int32_t get_offset_of_curTime_9() { return static_cast<int32_t>(offsetof(MScrollView_t1133512511, ___curTime_9)); }
	inline float get_curTime_9() const { return ___curTime_9; }
	inline float* get_address_of_curTime_9() { return &___curTime_9; }
	inline void set_curTime_9(float value)
	{
		___curTime_9 = value;
	}

	inline static int32_t get_offset_of_initPosition_10() { return static_cast<int32_t>(offsetof(MScrollView_t1133512511, ___initPosition_10)); }
	inline Vector3_t4282066566  get_initPosition_10() const { return ___initPosition_10; }
	inline Vector3_t4282066566 * get_address_of_initPosition_10() { return &___initPosition_10; }
	inline void set_initPosition_10(Vector3_t4282066566  value)
	{
		___initPosition_10 = value;
	}

	inline static int32_t get_offset_of_isTouch_11() { return static_cast<int32_t>(offsetof(MScrollView_t1133512511, ___isTouch_11)); }
	inline bool get_isTouch_11() const { return ___isTouch_11; }
	inline bool* get_address_of_isTouch_11() { return &___isTouch_11; }
	inline void set_isTouch_11(bool value)
	{
		___isTouch_11 = value;
	}

	inline static int32_t get_offset_of_isRight_12() { return static_cast<int32_t>(offsetof(MScrollView_t1133512511, ___isRight_12)); }
	inline bool get_isRight_12() const { return ___isRight_12; }
	inline bool* get_address_of_isRight_12() { return &___isRight_12; }
	inline void set_isRight_12(bool value)
	{
		___isRight_12 = value;
	}

	inline static int32_t get_offset_of_isLeft_13() { return static_cast<int32_t>(offsetof(MScrollView_t1133512511, ___isLeft_13)); }
	inline bool get_isLeft_13() const { return ___isLeft_13; }
	inline bool* get_address_of_isLeft_13() { return &___isLeft_13; }
	inline void set_isLeft_13(bool value)
	{
		___isLeft_13 = value;
	}

	inline static int32_t get_offset_of_isOnDrag_14() { return static_cast<int32_t>(offsetof(MScrollView_t1133512511, ___isOnDrag_14)); }
	inline bool get_isOnDrag_14() const { return ___isOnDrag_14; }
	inline bool* get_address_of_isOnDrag_14() { return &___isOnDrag_14; }
	inline void set_isOnDrag_14(bool value)
	{
		___isOnDrag_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
