﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._304177eed9d89c98e915e25f0423c8df
struct _304177eed9d89c98e915e25f0423c8df_t3085014039;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__304177eed9d89c98e915e25f3085014039.h"

// System.Void Little._304177eed9d89c98e915e25f0423c8df::.ctor()
extern "C"  void _304177eed9d89c98e915e25f0423c8df__ctor_m3103865846 (_304177eed9d89c98e915e25f0423c8df_t3085014039 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._304177eed9d89c98e915e25f0423c8df::_304177eed9d89c98e915e25f0423c8dfm2(System.Int32)
extern "C"  int32_t _304177eed9d89c98e915e25f0423c8df__304177eed9d89c98e915e25f0423c8dfm2_m3761201721 (_304177eed9d89c98e915e25f0423c8df_t3085014039 * __this, int32_t ____304177eed9d89c98e915e25f0423c8dfa0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._304177eed9d89c98e915e25f0423c8df::_304177eed9d89c98e915e25f0423c8dfm(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _304177eed9d89c98e915e25f0423c8df__304177eed9d89c98e915e25f0423c8dfm_m1105410205 (_304177eed9d89c98e915e25f0423c8df_t3085014039 * __this, int32_t ____304177eed9d89c98e915e25f0423c8dfa0, int32_t ____304177eed9d89c98e915e25f0423c8df241, int32_t ____304177eed9d89c98e915e25f0423c8dfc2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._304177eed9d89c98e915e25f0423c8df::ilo__304177eed9d89c98e915e25f0423c8dfm21(Little._304177eed9d89c98e915e25f0423c8df,System.Int32)
extern "C"  int32_t _304177eed9d89c98e915e25f0423c8df_ilo__304177eed9d89c98e915e25f0423c8dfm21_m763658686 (Il2CppObject * __this /* static, unused */, _304177eed9d89c98e915e25f0423c8df_t3085014039 * ____this0, int32_t ____304177eed9d89c98e915e25f0423c8dfa1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
