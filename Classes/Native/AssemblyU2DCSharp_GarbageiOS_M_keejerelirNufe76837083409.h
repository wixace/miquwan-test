﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_keejerelirNufe76
struct  M_keejerelirNufe76_t837083409  : public Il2CppObject
{
public:
	// System.UInt32 GarbageiOS.M_keejerelirNufe76::_bowhalJawdeabe
	uint32_t ____bowhalJawdeabe_0;
	// System.Int32 GarbageiOS.M_keejerelirNufe76::_ceretiCouke
	int32_t ____ceretiCouke_1;
	// System.Boolean GarbageiOS.M_keejerelirNufe76::_jarhedaFooro
	bool ____jarhedaFooro_2;
	// System.Int32 GarbageiOS.M_keejerelirNufe76::_kaslouqa
	int32_t ____kaslouqa_3;
	// System.UInt32 GarbageiOS.M_keejerelirNufe76::_wheagislal
	uint32_t ____wheagislal_4;
	// System.Int32 GarbageiOS.M_keejerelirNufe76::_vawallserZaschecu
	int32_t ____vawallserZaschecu_5;
	// System.UInt32 GarbageiOS.M_keejerelirNufe76::_kerejayBeja
	uint32_t ____kerejayBeja_6;
	// System.Boolean GarbageiOS.M_keejerelirNufe76::_nelearge
	bool ____nelearge_7;
	// System.Boolean GarbageiOS.M_keejerelirNufe76::_chearerzallWhadaiqe
	bool ____chearerzallWhadaiqe_8;
	// System.String GarbageiOS.M_keejerelirNufe76::_zurtouSerelurmar
	String_t* ____zurtouSerelurmar_9;
	// System.Int32 GarbageiOS.M_keejerelirNufe76::_pimaycere
	int32_t ____pimaycere_10;
	// System.String GarbageiOS.M_keejerelirNufe76::_kijal
	String_t* ____kijal_11;
	// System.Single GarbageiOS.M_keejerelirNufe76::_yocu
	float ____yocu_12;
	// System.Boolean GarbageiOS.M_keejerelirNufe76::_haporlaYemkis
	bool ____haporlaYemkis_13;
	// System.String GarbageiOS.M_keejerelirNufe76::_rija
	String_t* ____rija_14;
	// System.Int32 GarbageiOS.M_keejerelirNufe76::_doudu
	int32_t ____doudu_15;
	// System.Int32 GarbageiOS.M_keejerelirNufe76::_tritorjis
	int32_t ____tritorjis_16;
	// System.Single GarbageiOS.M_keejerelirNufe76::_nelelChemtearboo
	float ____nelelChemtearboo_17;
	// System.Single GarbageiOS.M_keejerelirNufe76::_firro
	float ____firro_18;
	// System.Boolean GarbageiOS.M_keejerelirNufe76::_drallradrur
	bool ____drallradrur_19;
	// System.Boolean GarbageiOS.M_keejerelirNufe76::_tarjemiWibo
	bool ____tarjemiWibo_20;
	// System.Int32 GarbageiOS.M_keejerelirNufe76::_melmawhow
	int32_t ____melmawhow_21;

public:
	inline static int32_t get_offset_of__bowhalJawdeabe_0() { return static_cast<int32_t>(offsetof(M_keejerelirNufe76_t837083409, ____bowhalJawdeabe_0)); }
	inline uint32_t get__bowhalJawdeabe_0() const { return ____bowhalJawdeabe_0; }
	inline uint32_t* get_address_of__bowhalJawdeabe_0() { return &____bowhalJawdeabe_0; }
	inline void set__bowhalJawdeabe_0(uint32_t value)
	{
		____bowhalJawdeabe_0 = value;
	}

	inline static int32_t get_offset_of__ceretiCouke_1() { return static_cast<int32_t>(offsetof(M_keejerelirNufe76_t837083409, ____ceretiCouke_1)); }
	inline int32_t get__ceretiCouke_1() const { return ____ceretiCouke_1; }
	inline int32_t* get_address_of__ceretiCouke_1() { return &____ceretiCouke_1; }
	inline void set__ceretiCouke_1(int32_t value)
	{
		____ceretiCouke_1 = value;
	}

	inline static int32_t get_offset_of__jarhedaFooro_2() { return static_cast<int32_t>(offsetof(M_keejerelirNufe76_t837083409, ____jarhedaFooro_2)); }
	inline bool get__jarhedaFooro_2() const { return ____jarhedaFooro_2; }
	inline bool* get_address_of__jarhedaFooro_2() { return &____jarhedaFooro_2; }
	inline void set__jarhedaFooro_2(bool value)
	{
		____jarhedaFooro_2 = value;
	}

	inline static int32_t get_offset_of__kaslouqa_3() { return static_cast<int32_t>(offsetof(M_keejerelirNufe76_t837083409, ____kaslouqa_3)); }
	inline int32_t get__kaslouqa_3() const { return ____kaslouqa_3; }
	inline int32_t* get_address_of__kaslouqa_3() { return &____kaslouqa_3; }
	inline void set__kaslouqa_3(int32_t value)
	{
		____kaslouqa_3 = value;
	}

	inline static int32_t get_offset_of__wheagislal_4() { return static_cast<int32_t>(offsetof(M_keejerelirNufe76_t837083409, ____wheagislal_4)); }
	inline uint32_t get__wheagislal_4() const { return ____wheagislal_4; }
	inline uint32_t* get_address_of__wheagislal_4() { return &____wheagislal_4; }
	inline void set__wheagislal_4(uint32_t value)
	{
		____wheagislal_4 = value;
	}

	inline static int32_t get_offset_of__vawallserZaschecu_5() { return static_cast<int32_t>(offsetof(M_keejerelirNufe76_t837083409, ____vawallserZaschecu_5)); }
	inline int32_t get__vawallserZaschecu_5() const { return ____vawallserZaschecu_5; }
	inline int32_t* get_address_of__vawallserZaschecu_5() { return &____vawallserZaschecu_5; }
	inline void set__vawallserZaschecu_5(int32_t value)
	{
		____vawallserZaschecu_5 = value;
	}

	inline static int32_t get_offset_of__kerejayBeja_6() { return static_cast<int32_t>(offsetof(M_keejerelirNufe76_t837083409, ____kerejayBeja_6)); }
	inline uint32_t get__kerejayBeja_6() const { return ____kerejayBeja_6; }
	inline uint32_t* get_address_of__kerejayBeja_6() { return &____kerejayBeja_6; }
	inline void set__kerejayBeja_6(uint32_t value)
	{
		____kerejayBeja_6 = value;
	}

	inline static int32_t get_offset_of__nelearge_7() { return static_cast<int32_t>(offsetof(M_keejerelirNufe76_t837083409, ____nelearge_7)); }
	inline bool get__nelearge_7() const { return ____nelearge_7; }
	inline bool* get_address_of__nelearge_7() { return &____nelearge_7; }
	inline void set__nelearge_7(bool value)
	{
		____nelearge_7 = value;
	}

	inline static int32_t get_offset_of__chearerzallWhadaiqe_8() { return static_cast<int32_t>(offsetof(M_keejerelirNufe76_t837083409, ____chearerzallWhadaiqe_8)); }
	inline bool get__chearerzallWhadaiqe_8() const { return ____chearerzallWhadaiqe_8; }
	inline bool* get_address_of__chearerzallWhadaiqe_8() { return &____chearerzallWhadaiqe_8; }
	inline void set__chearerzallWhadaiqe_8(bool value)
	{
		____chearerzallWhadaiqe_8 = value;
	}

	inline static int32_t get_offset_of__zurtouSerelurmar_9() { return static_cast<int32_t>(offsetof(M_keejerelirNufe76_t837083409, ____zurtouSerelurmar_9)); }
	inline String_t* get__zurtouSerelurmar_9() const { return ____zurtouSerelurmar_9; }
	inline String_t** get_address_of__zurtouSerelurmar_9() { return &____zurtouSerelurmar_9; }
	inline void set__zurtouSerelurmar_9(String_t* value)
	{
		____zurtouSerelurmar_9 = value;
		Il2CppCodeGenWriteBarrier(&____zurtouSerelurmar_9, value);
	}

	inline static int32_t get_offset_of__pimaycere_10() { return static_cast<int32_t>(offsetof(M_keejerelirNufe76_t837083409, ____pimaycere_10)); }
	inline int32_t get__pimaycere_10() const { return ____pimaycere_10; }
	inline int32_t* get_address_of__pimaycere_10() { return &____pimaycere_10; }
	inline void set__pimaycere_10(int32_t value)
	{
		____pimaycere_10 = value;
	}

	inline static int32_t get_offset_of__kijal_11() { return static_cast<int32_t>(offsetof(M_keejerelirNufe76_t837083409, ____kijal_11)); }
	inline String_t* get__kijal_11() const { return ____kijal_11; }
	inline String_t** get_address_of__kijal_11() { return &____kijal_11; }
	inline void set__kijal_11(String_t* value)
	{
		____kijal_11 = value;
		Il2CppCodeGenWriteBarrier(&____kijal_11, value);
	}

	inline static int32_t get_offset_of__yocu_12() { return static_cast<int32_t>(offsetof(M_keejerelirNufe76_t837083409, ____yocu_12)); }
	inline float get__yocu_12() const { return ____yocu_12; }
	inline float* get_address_of__yocu_12() { return &____yocu_12; }
	inline void set__yocu_12(float value)
	{
		____yocu_12 = value;
	}

	inline static int32_t get_offset_of__haporlaYemkis_13() { return static_cast<int32_t>(offsetof(M_keejerelirNufe76_t837083409, ____haporlaYemkis_13)); }
	inline bool get__haporlaYemkis_13() const { return ____haporlaYemkis_13; }
	inline bool* get_address_of__haporlaYemkis_13() { return &____haporlaYemkis_13; }
	inline void set__haporlaYemkis_13(bool value)
	{
		____haporlaYemkis_13 = value;
	}

	inline static int32_t get_offset_of__rija_14() { return static_cast<int32_t>(offsetof(M_keejerelirNufe76_t837083409, ____rija_14)); }
	inline String_t* get__rija_14() const { return ____rija_14; }
	inline String_t** get_address_of__rija_14() { return &____rija_14; }
	inline void set__rija_14(String_t* value)
	{
		____rija_14 = value;
		Il2CppCodeGenWriteBarrier(&____rija_14, value);
	}

	inline static int32_t get_offset_of__doudu_15() { return static_cast<int32_t>(offsetof(M_keejerelirNufe76_t837083409, ____doudu_15)); }
	inline int32_t get__doudu_15() const { return ____doudu_15; }
	inline int32_t* get_address_of__doudu_15() { return &____doudu_15; }
	inline void set__doudu_15(int32_t value)
	{
		____doudu_15 = value;
	}

	inline static int32_t get_offset_of__tritorjis_16() { return static_cast<int32_t>(offsetof(M_keejerelirNufe76_t837083409, ____tritorjis_16)); }
	inline int32_t get__tritorjis_16() const { return ____tritorjis_16; }
	inline int32_t* get_address_of__tritorjis_16() { return &____tritorjis_16; }
	inline void set__tritorjis_16(int32_t value)
	{
		____tritorjis_16 = value;
	}

	inline static int32_t get_offset_of__nelelChemtearboo_17() { return static_cast<int32_t>(offsetof(M_keejerelirNufe76_t837083409, ____nelelChemtearboo_17)); }
	inline float get__nelelChemtearboo_17() const { return ____nelelChemtearboo_17; }
	inline float* get_address_of__nelelChemtearboo_17() { return &____nelelChemtearboo_17; }
	inline void set__nelelChemtearboo_17(float value)
	{
		____nelelChemtearboo_17 = value;
	}

	inline static int32_t get_offset_of__firro_18() { return static_cast<int32_t>(offsetof(M_keejerelirNufe76_t837083409, ____firro_18)); }
	inline float get__firro_18() const { return ____firro_18; }
	inline float* get_address_of__firro_18() { return &____firro_18; }
	inline void set__firro_18(float value)
	{
		____firro_18 = value;
	}

	inline static int32_t get_offset_of__drallradrur_19() { return static_cast<int32_t>(offsetof(M_keejerelirNufe76_t837083409, ____drallradrur_19)); }
	inline bool get__drallradrur_19() const { return ____drallradrur_19; }
	inline bool* get_address_of__drallradrur_19() { return &____drallradrur_19; }
	inline void set__drallradrur_19(bool value)
	{
		____drallradrur_19 = value;
	}

	inline static int32_t get_offset_of__tarjemiWibo_20() { return static_cast<int32_t>(offsetof(M_keejerelirNufe76_t837083409, ____tarjemiWibo_20)); }
	inline bool get__tarjemiWibo_20() const { return ____tarjemiWibo_20; }
	inline bool* get_address_of__tarjemiWibo_20() { return &____tarjemiWibo_20; }
	inline void set__tarjemiWibo_20(bool value)
	{
		____tarjemiWibo_20 = value;
	}

	inline static int32_t get_offset_of__melmawhow_21() { return static_cast<int32_t>(offsetof(M_keejerelirNufe76_t837083409, ____melmawhow_21)); }
	inline int32_t get__melmawhow_21() const { return ____melmawhow_21; }
	inline int32_t* get_address_of__melmawhow_21() { return &____melmawhow_21; }
	inline void set__melmawhow_21(int32_t value)
	{
		____melmawhow_21 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
