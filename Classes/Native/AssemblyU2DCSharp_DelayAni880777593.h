﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Animation
struct Animation_t1724966010;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DelayAni
struct  DelayAni_t880777593  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.Animation DelayAni::ani
	Animation_t1724966010 * ___ani_2;

public:
	inline static int32_t get_offset_of_ani_2() { return static_cast<int32_t>(offsetof(DelayAni_t880777593, ___ani_2)); }
	inline Animation_t1724966010 * get_ani_2() const { return ___ani_2; }
	inline Animation_t1724966010 ** get_address_of_ani_2() { return &___ani_2; }
	inline void set_ani_2(Animation_t1724966010 * value)
	{
		___ani_2 = value;
		Il2CppCodeGenWriteBarrier(&___ani_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
