﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ReadFileToolGenerated
struct ReadFileToolGenerated_t2054321061;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Type
struct Type_t;
// MethodID
struct MethodID_t3916401116;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Type2863145774.h"
#include "AssemblyU2DCSharp_MethodID3916401116.h"
#include "mscorlib_System_String7231557.h"

// System.Void ReadFileToolGenerated::.ctor()
extern "C"  void ReadFileToolGenerated__ctor_m3663449958 (ReadFileToolGenerated_t2054321061 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReadFileToolGenerated::.cctor()
extern "C"  void ReadFileToolGenerated__cctor_m1415702791 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ReadFileToolGenerated::ReadFileTool_ReadFileTool1(JSVCall,System.Int32)
extern "C"  bool ReadFileToolGenerated_ReadFileTool_ReadFileTool1_m1057991692 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ReadFileToolGenerated::ReadFileTool_AddressToClassT1__String(JSVCall,System.Int32)
extern "C"  bool ReadFileToolGenerated_ReadFileTool_AddressToClassT1__String_m2663940852 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ReadFileToolGenerated::ReadFileTool_GetBytesAsset__String(JSVCall,System.Int32)
extern "C"  bool ReadFileToolGenerated_ReadFileTool_GetBytesAsset__String_m1702873105 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ReadFileToolGenerated::ReadFileTool_GetTextAsset__String(JSVCall,System.Int32)
extern "C"  bool ReadFileToolGenerated_ReadFileTool_GetTextAsset__String_m675498107 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ReadFileToolGenerated::ReadFileTool_JsonToClassT1__String(JSVCall,System.Int32)
extern "C"  bool ReadFileToolGenerated_ReadFileTool_JsonToClassT1__String_m1342402408 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ReadFileToolGenerated::ReadFileTool_SetData__AssetBundle(JSVCall,System.Int32)
extern "C"  bool ReadFileToolGenerated_ReadFileTool_SetData__AssetBundle_m119980099 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReadFileToolGenerated::__Register()
extern "C"  void ReadFileToolGenerated___Register_m3042794785 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ReadFileToolGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t ReadFileToolGenerated_ilo_getObject1_m471294224 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReadFileToolGenerated::ilo_addJSCSRel2(System.Int32,System.Object)
extern "C"  void ReadFileToolGenerated_ilo_addJSCSRel2_m144576419 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo ReadFileToolGenerated::ilo_makeGenericMethod3(System.Type,MethodID,System.Int32)
extern "C"  MethodInfo_t * ReadFileToolGenerated_ilo_makeGenericMethod3_m3977010358 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, MethodID_t3916401116 * ___methodID1, int32_t ___TCount2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ReadFileToolGenerated::ilo_getStringS4(System.Int32)
extern "C"  String_t* ReadFileToolGenerated_ilo_getStringS4_m1698444685 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ReadFileToolGenerated::ilo_GetTextAsset5(System.String)
extern "C"  String_t* ReadFileToolGenerated_ilo_GetTextAsset5_m4229733350 (Il2CppObject * __this /* static, unused */, String_t* ___txtAddress0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
