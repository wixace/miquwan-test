﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_AudioReverbZoneGenerated
struct UnityEngine_AudioReverbZoneGenerated_t817239107;
// JSVCall
struct JSVCall_t3708497963;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"

// System.Void UnityEngine_AudioReverbZoneGenerated::.ctor()
extern "C"  void UnityEngine_AudioReverbZoneGenerated__ctor_m3444191672 (UnityEngine_AudioReverbZoneGenerated_t817239107 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AudioReverbZoneGenerated::AudioReverbZone_AudioReverbZone1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AudioReverbZoneGenerated_AudioReverbZone_AudioReverbZone1_m1150481112 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioReverbZoneGenerated::AudioReverbZone_minDistance(JSVCall)
extern "C"  void UnityEngine_AudioReverbZoneGenerated_AudioReverbZone_minDistance_m3756607893 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioReverbZoneGenerated::AudioReverbZone_maxDistance(JSVCall)
extern "C"  void UnityEngine_AudioReverbZoneGenerated_AudioReverbZone_maxDistance_m541659331 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioReverbZoneGenerated::AudioReverbZone_reverbPreset(JSVCall)
extern "C"  void UnityEngine_AudioReverbZoneGenerated_AudioReverbZone_reverbPreset_m2153257855 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioReverbZoneGenerated::AudioReverbZone_room(JSVCall)
extern "C"  void UnityEngine_AudioReverbZoneGenerated_AudioReverbZone_room_m3928789813 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioReverbZoneGenerated::AudioReverbZone_roomHF(JSVCall)
extern "C"  void UnityEngine_AudioReverbZoneGenerated_AudioReverbZone_roomHF_m1343297591 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioReverbZoneGenerated::AudioReverbZone_roomLF(JSVCall)
extern "C"  void UnityEngine_AudioReverbZoneGenerated_AudioReverbZone_roomLF_m2745426747 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioReverbZoneGenerated::AudioReverbZone_decayTime(JSVCall)
extern "C"  void UnityEngine_AudioReverbZoneGenerated_AudioReverbZone_decayTime_m2941227477 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioReverbZoneGenerated::AudioReverbZone_decayHFRatio(JSVCall)
extern "C"  void UnityEngine_AudioReverbZoneGenerated_AudioReverbZone_decayHFRatio_m3814732893 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioReverbZoneGenerated::AudioReverbZone_reflections(JSVCall)
extern "C"  void UnityEngine_AudioReverbZoneGenerated_AudioReverbZone_reflections_m3204984180 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioReverbZoneGenerated::AudioReverbZone_reflectionsDelay(JSVCall)
extern "C"  void UnityEngine_AudioReverbZoneGenerated_AudioReverbZone_reflectionsDelay_m3603171413 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioReverbZoneGenerated::AudioReverbZone_reverb(JSVCall)
extern "C"  void UnityEngine_AudioReverbZoneGenerated_AudioReverbZone_reverb_m3942444446 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioReverbZoneGenerated::AudioReverbZone_reverbDelay(JSVCall)
extern "C"  void UnityEngine_AudioReverbZoneGenerated_AudioReverbZone_reverbDelay_m3983725163 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioReverbZoneGenerated::AudioReverbZone_HFReference(JSVCall)
extern "C"  void UnityEngine_AudioReverbZoneGenerated_AudioReverbZone_HFReference_m2496000207 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioReverbZoneGenerated::AudioReverbZone_LFReference(JSVCall)
extern "C"  void UnityEngine_AudioReverbZoneGenerated_AudioReverbZone_LFReference_m860703307 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioReverbZoneGenerated::AudioReverbZone_roomRolloffFactor(JSVCall)
extern "C"  void UnityEngine_AudioReverbZoneGenerated_AudioReverbZone_roomRolloffFactor_m1870877110 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioReverbZoneGenerated::AudioReverbZone_diffusion(JSVCall)
extern "C"  void UnityEngine_AudioReverbZoneGenerated_AudioReverbZone_diffusion_m1924839799 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioReverbZoneGenerated::AudioReverbZone_density(JSVCall)
extern "C"  void UnityEngine_AudioReverbZoneGenerated_AudioReverbZone_density_m3238106420 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioReverbZoneGenerated::__Register()
extern "C"  void UnityEngine_AudioReverbZoneGenerated___Register_m772382223 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_AudioReverbZoneGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UnityEngine_AudioReverbZoneGenerated_ilo_getObject1_m117529946 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_AudioReverbZoneGenerated::ilo_getInt322(System.Int32)
extern "C"  int32_t UnityEngine_AudioReverbZoneGenerated_ilo_getInt322_m804128576 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioReverbZoneGenerated::ilo_setInt323(System.Int32,System.Int32)
extern "C"  void UnityEngine_AudioReverbZoneGenerated_ilo_setInt323_m2762377180 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine_AudioReverbZoneGenerated::ilo_getSingle4(System.Int32)
extern "C"  float UnityEngine_AudioReverbZoneGenerated_ilo_getSingle4_m1160100658 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioReverbZoneGenerated::ilo_setSingle5(System.Int32,System.Single)
extern "C"  void UnityEngine_AudioReverbZoneGenerated_ilo_setSingle5_m3952565696 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
