﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CSProvingGroundDataGenerated
struct CSProvingGroundDataGenerated_t2654362293;
// JSVCall
struct JSVCall_t3708497963;
// CSProvingGroundsHeroData[]
struct CSProvingGroundsHeroDataU5BU5D_t1350178142;
// CSPVPRobotNPC[]
struct CSPVPRobotNPCU5BU5D_t1447782092;
// CSHeroUnit
struct CSHeroUnit_t3764358446;
// CSProvingGroundData
struct CSProvingGroundData_t325892090;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_CSProvingGroundData325892090.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_String7231557.h"

// System.Void CSProvingGroundDataGenerated::.ctor()
extern "C"  void CSProvingGroundDataGenerated__ctor_m2614928262 (CSProvingGroundDataGenerated_t2654362293 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CSProvingGroundDataGenerated::CSProvingGroundData_CSProvingGroundData1(JSVCall,System.Int32)
extern "C"  bool CSProvingGroundDataGenerated_CSProvingGroundData_CSProvingGroundData1_m4005696574 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSProvingGroundDataGenerated::CSProvingGroundData_heroRealInfos(JSVCall)
extern "C"  void CSProvingGroundDataGenerated_CSProvingGroundData_heroRealInfos_m2436730811 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSProvingGroundDataGenerated::CSProvingGroundData_robotNpcs(JSVCall)
extern "C"  void CSProvingGroundDataGenerated_CSProvingGroundData_robotNpcs_m107620876 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CSProvingGroundDataGenerated::CSProvingGroundData_GetCSPVPRobotNPCLevelById__Int32(JSVCall,System.Int32)
extern "C"  bool CSProvingGroundDataGenerated_CSProvingGroundData_GetCSPVPRobotNPCLevelById__Int32_m11528346 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CSProvingGroundDataGenerated::CSProvingGroundData_GetHeroUnit__UInt32(JSVCall,System.Int32)
extern "C"  bool CSProvingGroundDataGenerated_CSProvingGroundData_GetHeroUnit__UInt32_m1126167720 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CSProvingGroundDataGenerated::CSProvingGroundData_LoadData__String(JSVCall,System.Int32)
extern "C"  bool CSProvingGroundDataGenerated_CSProvingGroundData_LoadData__String_m3574691368 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CSProvingGroundDataGenerated::CSProvingGroundData_UnLoadData(JSVCall,System.Int32)
extern "C"  bool CSProvingGroundDataGenerated_CSProvingGroundData_UnLoadData_m4235500112 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSProvingGroundDataGenerated::__Register()
extern "C"  void CSProvingGroundDataGenerated___Register_m3722190081 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CSProvingGroundsHeroData[] CSProvingGroundDataGenerated::<CSProvingGroundData_heroRealInfos>m__29()
extern "C"  CSProvingGroundsHeroDataU5BU5D_t1350178142* CSProvingGroundDataGenerated_U3CCSProvingGroundData_heroRealInfosU3Em__29_m1875924404 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CSPVPRobotNPC[] CSProvingGroundDataGenerated::<CSProvingGroundData_robotNpcs>m__2A()
extern "C"  CSPVPRobotNPCU5BU5D_t1447782092* CSProvingGroundDataGenerated_U3CCSProvingGroundData_robotNpcsU3Em__2A_m519430647 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSProvingGroundDataGenerated::ilo_setArrayS1(System.Int32,System.Int32,System.Boolean)
extern "C"  void CSProvingGroundDataGenerated_ilo_setArrayS1_m118784573 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___count1, bool ___bClear2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 CSProvingGroundDataGenerated::ilo_getUInt322(System.Int32)
extern "C"  uint32_t CSProvingGroundDataGenerated_ilo_getUInt322_m96183616 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CSHeroUnit CSProvingGroundDataGenerated::ilo_GetHeroUnit3(CSProvingGroundData,System.UInt32)
extern "C"  CSHeroUnit_t3764358446 * CSProvingGroundDataGenerated_ilo_GetHeroUnit3_m2762753261 (Il2CppObject * __this /* static, unused */, CSProvingGroundData_t325892090 * ____this0, uint32_t ___id1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CSProvingGroundDataGenerated::ilo_setObject4(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t CSProvingGroundDataGenerated_ilo_setObject4_m1060766855 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String CSProvingGroundDataGenerated::ilo_getStringS5(System.Int32)
extern "C"  String_t* CSProvingGroundDataGenerated_ilo_getStringS5_m1572106568 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSProvingGroundDataGenerated::ilo_LoadData6(CSProvingGroundData,System.String)
extern "C"  void CSProvingGroundDataGenerated_ilo_LoadData6_m2988025533 (Il2CppObject * __this /* static, unused */, CSProvingGroundData_t325892090 * ____this0, String_t* ___jsonStr1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSProvingGroundDataGenerated::ilo_UnLoadData7(CSProvingGroundData)
extern "C"  void CSProvingGroundDataGenerated_ilo_UnLoadData7_m1862429657 (Il2CppObject * __this /* static, unused */, CSProvingGroundData_t325892090 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object CSProvingGroundDataGenerated::ilo_getObject8(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * CSProvingGroundDataGenerated_ilo_getObject8_m2879517744 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
