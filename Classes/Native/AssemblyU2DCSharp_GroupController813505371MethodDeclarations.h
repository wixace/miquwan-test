﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GroupController
struct GroupController_t813505371;
// System.Collections.Generic.List`1<Pathfinding.RVO.Sampled.Agent>
struct List_1_t2658239795;
// Pathfinding.RVO.Simulator
struct Simulator_t2705969170;
// RVOExampleAgent
struct RVOExampleAgent_t1174908390;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"
#include "AssemblyU2DCSharp_Pathfinding_RVO_Simulator2705969170.h"
#include "AssemblyU2DCSharp_GroupController813505371.h"
#include "AssemblyU2DCSharp_RVOExampleAgent1174908390.h"

// System.Void GroupController::.ctor()
extern "C"  void GroupController__ctor_m1934901872 (GroupController_t813505371 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GroupController::Start()
extern "C"  void GroupController_Start_m882039664 (GroupController_t813505371 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GroupController::Update()
extern "C"  void GroupController_Update_m1579277981 (GroupController_t813505371 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GroupController::OnGUI()
extern "C"  void GroupController_OnGUI_m1430300522 (GroupController_t813505371 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GroupController::Order()
extern "C"  void GroupController_Order_m1572115292 (GroupController_t813505371 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GroupController::Select(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  void GroupController_Select_m2454497310 (GroupController_t813505371 * __this, Vector2_t4282066565  ____start0, Vector2_t4282066565  ____end1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color GroupController::GetColor(System.Single)
extern "C"  Color_t4194546905  GroupController_GetColor_m3573842641 (GroupController_t813505371 * __this, float ___angle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color GroupController::HSVToRGB(System.Single,System.Single,System.Single)
extern "C"  Color_t4194546905  GroupController_HSVToRGB_m1470385025 (Il2CppObject * __this /* static, unused */, float ___h0, float ___s1, float ___v2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<Pathfinding.RVO.Sampled.Agent> GroupController::ilo_GetAgents1(Pathfinding.RVO.Simulator)
extern "C"  List_1_t2658239795 * GroupController_ilo_GetAgents1_m2961945292 (Il2CppObject * __this /* static, unused */, Simulator_t2705969170 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GroupController::ilo_Order2(GroupController)
extern "C"  void GroupController_ilo_Order2_m3772505260 (Il2CppObject * __this /* static, unused */, GroupController_t813505371 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GroupController::ilo_RecalculatePath3(RVOExampleAgent)
extern "C"  void GroupController_ilo_RecalculatePath3_m937135274 (Il2CppObject * __this /* static, unused */, RVOExampleAgent_t1174908390 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
