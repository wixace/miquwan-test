﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Action
struct Action_t3771233898;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PluginHuawei
struct  PluginHuawei_t1557978906  : public MonoBehaviour_t667441552
{
public:
	// System.String PluginHuawei::gameAuthSign
	String_t* ___gameAuthSign_2;
	// System.String PluginHuawei::playerId
	String_t* ___playerId_3;
	// System.String PluginHuawei::appid
	String_t* ___appid_4;
	// System.String PluginHuawei::ts
	String_t* ___ts_5;

public:
	inline static int32_t get_offset_of_gameAuthSign_2() { return static_cast<int32_t>(offsetof(PluginHuawei_t1557978906, ___gameAuthSign_2)); }
	inline String_t* get_gameAuthSign_2() const { return ___gameAuthSign_2; }
	inline String_t** get_address_of_gameAuthSign_2() { return &___gameAuthSign_2; }
	inline void set_gameAuthSign_2(String_t* value)
	{
		___gameAuthSign_2 = value;
		Il2CppCodeGenWriteBarrier(&___gameAuthSign_2, value);
	}

	inline static int32_t get_offset_of_playerId_3() { return static_cast<int32_t>(offsetof(PluginHuawei_t1557978906, ___playerId_3)); }
	inline String_t* get_playerId_3() const { return ___playerId_3; }
	inline String_t** get_address_of_playerId_3() { return &___playerId_3; }
	inline void set_playerId_3(String_t* value)
	{
		___playerId_3 = value;
		Il2CppCodeGenWriteBarrier(&___playerId_3, value);
	}

	inline static int32_t get_offset_of_appid_4() { return static_cast<int32_t>(offsetof(PluginHuawei_t1557978906, ___appid_4)); }
	inline String_t* get_appid_4() const { return ___appid_4; }
	inline String_t** get_address_of_appid_4() { return &___appid_4; }
	inline void set_appid_4(String_t* value)
	{
		___appid_4 = value;
		Il2CppCodeGenWriteBarrier(&___appid_4, value);
	}

	inline static int32_t get_offset_of_ts_5() { return static_cast<int32_t>(offsetof(PluginHuawei_t1557978906, ___ts_5)); }
	inline String_t* get_ts_5() const { return ___ts_5; }
	inline String_t** get_address_of_ts_5() { return &___ts_5; }
	inline void set_ts_5(String_t* value)
	{
		___ts_5 = value;
		Il2CppCodeGenWriteBarrier(&___ts_5, value);
	}
};

struct PluginHuawei_t1557978906_StaticFields
{
public:
	// System.Action PluginHuawei::<>f__am$cache4
	Action_t3771233898 * ___U3CU3Ef__amU24cache4_6;
	// System.Action PluginHuawei::<>f__am$cache5
	Action_t3771233898 * ___U3CU3Ef__amU24cache5_7;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache4_6() { return static_cast<int32_t>(offsetof(PluginHuawei_t1557978906_StaticFields, ___U3CU3Ef__amU24cache4_6)); }
	inline Action_t3771233898 * get_U3CU3Ef__amU24cache4_6() const { return ___U3CU3Ef__amU24cache4_6; }
	inline Action_t3771233898 ** get_address_of_U3CU3Ef__amU24cache4_6() { return &___U3CU3Ef__amU24cache4_6; }
	inline void set_U3CU3Ef__amU24cache4_6(Action_t3771233898 * value)
	{
		___U3CU3Ef__amU24cache4_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache4_6, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache5_7() { return static_cast<int32_t>(offsetof(PluginHuawei_t1557978906_StaticFields, ___U3CU3Ef__amU24cache5_7)); }
	inline Action_t3771233898 * get_U3CU3Ef__amU24cache5_7() const { return ___U3CU3Ef__amU24cache5_7; }
	inline Action_t3771233898 ** get_address_of_U3CU3Ef__amU24cache5_7() { return &___U3CU3Ef__amU24cache5_7; }
	inline void set_U3CU3Ef__amU24cache5_7(Action_t3771233898 * value)
	{
		___U3CU3Ef__amU24cache5_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache5_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
