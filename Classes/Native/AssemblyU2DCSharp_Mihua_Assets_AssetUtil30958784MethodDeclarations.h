﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mihua.Assets.AssetUtil
struct AssetUtil_t30958784;
// Filelist`1<FileInfoRes>
struct Filelist_1_t2494814894;
// System.Collections.Generic.List`1<FileInfoRes>
struct List_1_t2585245350;
// System.String
struct String_t;
// System.IO.MemoryStream
struct MemoryStream_t418716369;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void Mihua.Assets.AssetUtil::.ctor()
extern "C"  void AssetUtil__ctor_m216979806 (AssetUtil_t30958784 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua.Assets.AssetUtil::.cctor()
extern "C"  void AssetUtil__cctor_m1949310479 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua.Assets.AssetUtil::ZUpdate()
extern "C"  void AssetUtil_ZUpdate_m1927692511 (AssetUtil_t30958784 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mihua.Assets.AssetUtil::get_IsWifiNetwork()
extern "C"  bool AssetUtil_get_IsWifiNetwork_m2185569774 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua.Assets.AssetUtil::SetAssetCfg(Filelist`1<FileInfoRes>)
extern "C"  void AssetUtil_SetAssetCfg_m1895019783 (Il2CppObject * __this /* static, unused */, Filelist_1_t2494814894 * ___fileList0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<FileInfoRes> Mihua.Assets.AssetUtil::GetNeedLoadList()
extern "C"  List_1_t2585245350 * AssetUtil_GetNeedLoadList_m3983358229 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mihua.Assets.AssetUtil::Exists(System.String)
extern "C"  bool AssetUtil_Exists_m1540208588 (Il2CppObject * __this /* static, unused */, String_t* ___abName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.MemoryStream Mihua.Assets.AssetUtil::LoadDataFromStreamPath(System.String)
extern "C"  MemoryStream_t418716369 * AssetUtil_LoadDataFromStreamPath_m403949290 (Il2CppObject * __this /* static, unused */, String_t* ___fileName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua.Assets.AssetUtil::SaveCacheBundleFile(System.String,System.Byte[])
extern "C"  void AssetUtil_SaveCacheBundleFile_m3522963366 (Il2CppObject * __this /* static, unused */, String_t* ___assetBundleName0, ByteU5BU5D_t4260760469* ___p11, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua.Assets.AssetUtil::ilo_LogWarning1(System.Object,System.Boolean)
extern "C"  void AssetUtil_ilo_LogWarning1_m1271692043 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mihua.Assets.AssetUtil::ilo_BZip2DeCompression2(System.Byte[])
extern "C"  ByteU5BU5D_t4260760469* AssetUtil_ilo_BZip2DeCompression2_m272302122 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___bytes0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
