﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21944668977MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<Pathfinding.GraphNode,Pathfinding.NodeLink2>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m153166217(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t3533017339 *, GraphNode_t23612370 *, NodeLink2_t1645404664 *, const MethodInfo*))KeyValuePair_2__ctor_m4168265535_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<Pathfinding.GraphNode,Pathfinding.NodeLink2>::get_Key()
#define KeyValuePair_2_get_Key_m2569950143(__this, method) ((  GraphNode_t23612370 * (*) (KeyValuePair_2_t3533017339 *, const MethodInfo*))KeyValuePair_2_get_Key_m3256475977_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Pathfinding.GraphNode,Pathfinding.NodeLink2>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m1071621888(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3533017339 *, GraphNode_t23612370 *, const MethodInfo*))KeyValuePair_2_set_Key_m1278074762_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<Pathfinding.GraphNode,Pathfinding.NodeLink2>::get_Value()
#define KeyValuePair_2_get_Value_m25592995(__this, method) ((  NodeLink2_t1645404664 * (*) (KeyValuePair_2_t3533017339 *, const MethodInfo*))KeyValuePair_2_get_Value_m3899079597_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Pathfinding.GraphNode,Pathfinding.NodeLink2>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m1578072832(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3533017339 *, NodeLink2_t1645404664 *, const MethodInfo*))KeyValuePair_2_set_Value_m2954518154_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<Pathfinding.GraphNode,Pathfinding.NodeLink2>::ToString()
#define KeyValuePair_2_ToString_m1741266440(__this, method) ((  String_t* (*) (KeyValuePair_2_t3533017339 *, const MethodInfo*))KeyValuePair_2_ToString_m1313859518_gshared)(__this, method)
