﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType1744280289.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SevenZip.Compression.RangeCoder.BitDecoder
struct  BitDecoder_t4202293321 
{
public:
	// System.UInt32 SevenZip.Compression.RangeCoder.BitDecoder::Prob
	uint32_t ___Prob_3;

public:
	inline static int32_t get_offset_of_Prob_3() { return static_cast<int32_t>(offsetof(BitDecoder_t4202293321, ___Prob_3)); }
	inline uint32_t get_Prob_3() const { return ___Prob_3; }
	inline uint32_t* get_address_of_Prob_3() { return &___Prob_3; }
	inline void set_Prob_3(uint32_t value)
	{
		___Prob_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for marshalling of: SevenZip.Compression.RangeCoder.BitDecoder
struct BitDecoder_t4202293321_marshaled_pinvoke
{
	uint32_t ___Prob_3;
};
// Native definition for marshalling of: SevenZip.Compression.RangeCoder.BitDecoder
struct BitDecoder_t4202293321_marshaled_com
{
	uint32_t ___Prob_3;
};
