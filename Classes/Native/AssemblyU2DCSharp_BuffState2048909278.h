﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_BuffEnum_EBuffState2159802479.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BuffState
struct  BuffState_t2048909278  : public Il2CppObject
{
public:
	// System.Int32 BuffState::buffID
	int32_t ___buffID_0;
	// BuffEnum.EBuffState BuffState::buffState
	uint64_t ___buffState_1;
	// System.Int32 BuffState::para1
	int32_t ___para1_2;
	// System.Int32 BuffState::para2
	int32_t ___para2_3;
	// System.Int32 BuffState::para3
	int32_t ___para3_4;
	// System.Int32 BuffState::para4
	int32_t ___para4_5;
	// System.Int32 BuffState::srcID
	int32_t ___srcID_6;
	// System.Int32 BuffState::count
	int32_t ___count_7;
	// System.Single BuffState::timer
	float ___timer_8;

public:
	inline static int32_t get_offset_of_buffID_0() { return static_cast<int32_t>(offsetof(BuffState_t2048909278, ___buffID_0)); }
	inline int32_t get_buffID_0() const { return ___buffID_0; }
	inline int32_t* get_address_of_buffID_0() { return &___buffID_0; }
	inline void set_buffID_0(int32_t value)
	{
		___buffID_0 = value;
	}

	inline static int32_t get_offset_of_buffState_1() { return static_cast<int32_t>(offsetof(BuffState_t2048909278, ___buffState_1)); }
	inline uint64_t get_buffState_1() const { return ___buffState_1; }
	inline uint64_t* get_address_of_buffState_1() { return &___buffState_1; }
	inline void set_buffState_1(uint64_t value)
	{
		___buffState_1 = value;
	}

	inline static int32_t get_offset_of_para1_2() { return static_cast<int32_t>(offsetof(BuffState_t2048909278, ___para1_2)); }
	inline int32_t get_para1_2() const { return ___para1_2; }
	inline int32_t* get_address_of_para1_2() { return &___para1_2; }
	inline void set_para1_2(int32_t value)
	{
		___para1_2 = value;
	}

	inline static int32_t get_offset_of_para2_3() { return static_cast<int32_t>(offsetof(BuffState_t2048909278, ___para2_3)); }
	inline int32_t get_para2_3() const { return ___para2_3; }
	inline int32_t* get_address_of_para2_3() { return &___para2_3; }
	inline void set_para2_3(int32_t value)
	{
		___para2_3 = value;
	}

	inline static int32_t get_offset_of_para3_4() { return static_cast<int32_t>(offsetof(BuffState_t2048909278, ___para3_4)); }
	inline int32_t get_para3_4() const { return ___para3_4; }
	inline int32_t* get_address_of_para3_4() { return &___para3_4; }
	inline void set_para3_4(int32_t value)
	{
		___para3_4 = value;
	}

	inline static int32_t get_offset_of_para4_5() { return static_cast<int32_t>(offsetof(BuffState_t2048909278, ___para4_5)); }
	inline int32_t get_para4_5() const { return ___para4_5; }
	inline int32_t* get_address_of_para4_5() { return &___para4_5; }
	inline void set_para4_5(int32_t value)
	{
		___para4_5 = value;
	}

	inline static int32_t get_offset_of_srcID_6() { return static_cast<int32_t>(offsetof(BuffState_t2048909278, ___srcID_6)); }
	inline int32_t get_srcID_6() const { return ___srcID_6; }
	inline int32_t* get_address_of_srcID_6() { return &___srcID_6; }
	inline void set_srcID_6(int32_t value)
	{
		___srcID_6 = value;
	}

	inline static int32_t get_offset_of_count_7() { return static_cast<int32_t>(offsetof(BuffState_t2048909278, ___count_7)); }
	inline int32_t get_count_7() const { return ___count_7; }
	inline int32_t* get_address_of_count_7() { return &___count_7; }
	inline void set_count_7(int32_t value)
	{
		___count_7 = value;
	}

	inline static int32_t get_offset_of_timer_8() { return static_cast<int32_t>(offsetof(BuffState_t2048909278, ___timer_8)); }
	inline float get_timer_8() const { return ___timer_8; }
	inline float* get_address_of_timer_8() { return &___timer_8; }
	inline void set_timer_8(float value)
	{
		___timer_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
