﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Utilities.ConvertUtils/<TryConvert>c__AnonStorey13B`1<System.Object>
struct U3CTryConvertU3Ec__AnonStorey13B_1_t3526582148;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Newtonsoft.Json.Utilities.ConvertUtils/<TryConvert>c__AnonStorey13B`1<System.Object>::.ctor()
extern "C"  void U3CTryConvertU3Ec__AnonStorey13B_1__ctor_m3197431083_gshared (U3CTryConvertU3Ec__AnonStorey13B_1_t3526582148 * __this, const MethodInfo* method);
#define U3CTryConvertU3Ec__AnonStorey13B_1__ctor_m3197431083(__this, method) ((  void (*) (U3CTryConvertU3Ec__AnonStorey13B_1_t3526582148 *, const MethodInfo*))U3CTryConvertU3Ec__AnonStorey13B_1__ctor_m3197431083_gshared)(__this, method)
// T Newtonsoft.Json.Utilities.ConvertUtils/<TryConvert>c__AnonStorey13B`1<System.Object>::<>m__397()
extern "C"  Il2CppObject * U3CTryConvertU3Ec__AnonStorey13B_1_U3CU3Em__397_m90617086_gshared (U3CTryConvertU3Ec__AnonStorey13B_1_t3526582148 * __this, const MethodInfo* method);
#define U3CTryConvertU3Ec__AnonStorey13B_1_U3CU3Em__397_m90617086(__this, method) ((  Il2CppObject * (*) (U3CTryConvertU3Ec__AnonStorey13B_1_t3526582148 *, const MethodInfo*))U3CTryConvertU3Ec__AnonStorey13B_1_U3CU3Em__397_m90617086_gshared)(__this, method)
