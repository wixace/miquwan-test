﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_fearteenasWallger10
struct  M_fearteenasWallger10_t1129392077  : public Il2CppObject
{
public:
	// System.String GarbageiOS.M_fearteenasWallger10::_kesem
	String_t* ____kesem_0;
	// System.Int32 GarbageiOS.M_fearteenasWallger10::_sideremaCalneartu
	int32_t ____sideremaCalneartu_1;
	// System.Single GarbageiOS.M_fearteenasWallger10::_gasxemsorTirwhadas
	float ____gasxemsorTirwhadas_2;
	// System.UInt32 GarbageiOS.M_fearteenasWallger10::_corkarCehe
	uint32_t ____corkarCehe_3;
	// System.UInt32 GarbageiOS.M_fearteenasWallger10::_neakor
	uint32_t ____neakor_4;
	// System.Boolean GarbageiOS.M_fearteenasWallger10::_teawoonai
	bool ____teawoonai_5;
	// System.UInt32 GarbageiOS.M_fearteenasWallger10::_hearfo
	uint32_t ____hearfo_6;
	// System.String GarbageiOS.M_fearteenasWallger10::_yobo
	String_t* ____yobo_7;
	// System.UInt32 GarbageiOS.M_fearteenasWallger10::_mezegoRisnem
	uint32_t ____mezegoRisnem_8;
	// System.Single GarbageiOS.M_fearteenasWallger10::_kohajaNaske
	float ____kohajaNaske_9;
	// System.Single GarbageiOS.M_fearteenasWallger10::_falne
	float ____falne_10;
	// System.Boolean GarbageiOS.M_fearteenasWallger10::_choujerlur
	bool ____choujerlur_11;
	// System.UInt32 GarbageiOS.M_fearteenasWallger10::_naysti
	uint32_t ____naysti_12;
	// System.Boolean GarbageiOS.M_fearteenasWallger10::_fouxeca
	bool ____fouxeca_13;
	// System.Single GarbageiOS.M_fearteenasWallger10::_xeatawmiJelstu
	float ____xeatawmiJelstu_14;
	// System.Int32 GarbageiOS.M_fearteenasWallger10::_huwihas
	int32_t ____huwihas_15;
	// System.String GarbageiOS.M_fearteenasWallger10::_drizurTige
	String_t* ____drizurTige_16;
	// System.Int32 GarbageiOS.M_fearteenasWallger10::_trorraLeldraju
	int32_t ____trorraLeldraju_17;
	// System.Boolean GarbageiOS.M_fearteenasWallger10::_sorpisgiMalarfor
	bool ____sorpisgiMalarfor_18;
	// System.UInt32 GarbageiOS.M_fearteenasWallger10::_rego
	uint32_t ____rego_19;
	// System.Single GarbageiOS.M_fearteenasWallger10::_vanair
	float ____vanair_20;
	// System.Boolean GarbageiOS.M_fearteenasWallger10::_gealearsorGiwercer
	bool ____gealearsorGiwercer_21;
	// System.UInt32 GarbageiOS.M_fearteenasWallger10::_faimere
	uint32_t ____faimere_22;
	// System.Int32 GarbageiOS.M_fearteenasWallger10::_saijedi
	int32_t ____saijedi_23;
	// System.String GarbageiOS.M_fearteenasWallger10::_coumemkalMamawte
	String_t* ____coumemkalMamawte_24;
	// System.String GarbageiOS.M_fearteenasWallger10::_repota
	String_t* ____repota_25;
	// System.UInt32 GarbageiOS.M_fearteenasWallger10::_noochiBairmou
	uint32_t ____noochiBairmou_26;
	// System.Int32 GarbageiOS.M_fearteenasWallger10::_drahear
	int32_t ____drahear_27;
	// System.String GarbageiOS.M_fearteenasWallger10::_malnel
	String_t* ____malnel_28;
	// System.Int32 GarbageiOS.M_fearteenasWallger10::_xekarwee
	int32_t ____xekarwee_29;
	// System.Single GarbageiOS.M_fearteenasWallger10::_lounalKurdral
	float ____lounalKurdral_30;
	// System.Single GarbageiOS.M_fearteenasWallger10::_durma
	float ____durma_31;
	// System.Single GarbageiOS.M_fearteenasWallger10::_saseTawheljou
	float ____saseTawheljou_32;
	// System.UInt32 GarbageiOS.M_fearteenasWallger10::_fuse
	uint32_t ____fuse_33;
	// System.String GarbageiOS.M_fearteenasWallger10::_roofouvairLearponu
	String_t* ____roofouvairLearponu_34;
	// System.Boolean GarbageiOS.M_fearteenasWallger10::_rarmaldralWenaw
	bool ____rarmaldralWenaw_35;
	// System.Int32 GarbageiOS.M_fearteenasWallger10::_jaihuVelcoo
	int32_t ____jaihuVelcoo_36;
	// System.Single GarbageiOS.M_fearteenasWallger10::_guwirwaChasemji
	float ____guwirwaChasemji_37;
	// System.Int32 GarbageiOS.M_fearteenasWallger10::_drabaKimardaw
	int32_t ____drabaKimardaw_38;
	// System.Int32 GarbageiOS.M_fearteenasWallger10::_recisTrowjalda
	int32_t ____recisTrowjalda_39;
	// System.Boolean GarbageiOS.M_fearteenasWallger10::_casdras
	bool ____casdras_40;
	// System.Single GarbageiOS.M_fearteenasWallger10::_milouTosa
	float ____milouTosa_41;
	// System.UInt32 GarbageiOS.M_fearteenasWallger10::_pisweCebaigo
	uint32_t ____pisweCebaigo_42;
	// System.Int32 GarbageiOS.M_fearteenasWallger10::_seetayGeanou
	int32_t ____seetayGeanou_43;
	// System.Single GarbageiOS.M_fearteenasWallger10::_serloucuBovesto
	float ____serloucuBovesto_44;
	// System.Single GarbageiOS.M_fearteenasWallger10::_ceenee
	float ____ceenee_45;
	// System.Int32 GarbageiOS.M_fearteenasWallger10::_zaseca
	int32_t ____zaseca_46;
	// System.String GarbageiOS.M_fearteenasWallger10::_staiza
	String_t* ____staiza_47;
	// System.UInt32 GarbageiOS.M_fearteenasWallger10::_stikoujeeJesu
	uint32_t ____stikoujeeJesu_48;
	// System.Boolean GarbageiOS.M_fearteenasWallger10::_durrerenem
	bool ____durrerenem_49;

public:
	inline static int32_t get_offset_of__kesem_0() { return static_cast<int32_t>(offsetof(M_fearteenasWallger10_t1129392077, ____kesem_0)); }
	inline String_t* get__kesem_0() const { return ____kesem_0; }
	inline String_t** get_address_of__kesem_0() { return &____kesem_0; }
	inline void set__kesem_0(String_t* value)
	{
		____kesem_0 = value;
		Il2CppCodeGenWriteBarrier(&____kesem_0, value);
	}

	inline static int32_t get_offset_of__sideremaCalneartu_1() { return static_cast<int32_t>(offsetof(M_fearteenasWallger10_t1129392077, ____sideremaCalneartu_1)); }
	inline int32_t get__sideremaCalneartu_1() const { return ____sideremaCalneartu_1; }
	inline int32_t* get_address_of__sideremaCalneartu_1() { return &____sideremaCalneartu_1; }
	inline void set__sideremaCalneartu_1(int32_t value)
	{
		____sideremaCalneartu_1 = value;
	}

	inline static int32_t get_offset_of__gasxemsorTirwhadas_2() { return static_cast<int32_t>(offsetof(M_fearteenasWallger10_t1129392077, ____gasxemsorTirwhadas_2)); }
	inline float get__gasxemsorTirwhadas_2() const { return ____gasxemsorTirwhadas_2; }
	inline float* get_address_of__gasxemsorTirwhadas_2() { return &____gasxemsorTirwhadas_2; }
	inline void set__gasxemsorTirwhadas_2(float value)
	{
		____gasxemsorTirwhadas_2 = value;
	}

	inline static int32_t get_offset_of__corkarCehe_3() { return static_cast<int32_t>(offsetof(M_fearteenasWallger10_t1129392077, ____corkarCehe_3)); }
	inline uint32_t get__corkarCehe_3() const { return ____corkarCehe_3; }
	inline uint32_t* get_address_of__corkarCehe_3() { return &____corkarCehe_3; }
	inline void set__corkarCehe_3(uint32_t value)
	{
		____corkarCehe_3 = value;
	}

	inline static int32_t get_offset_of__neakor_4() { return static_cast<int32_t>(offsetof(M_fearteenasWallger10_t1129392077, ____neakor_4)); }
	inline uint32_t get__neakor_4() const { return ____neakor_4; }
	inline uint32_t* get_address_of__neakor_4() { return &____neakor_4; }
	inline void set__neakor_4(uint32_t value)
	{
		____neakor_4 = value;
	}

	inline static int32_t get_offset_of__teawoonai_5() { return static_cast<int32_t>(offsetof(M_fearteenasWallger10_t1129392077, ____teawoonai_5)); }
	inline bool get__teawoonai_5() const { return ____teawoonai_5; }
	inline bool* get_address_of__teawoonai_5() { return &____teawoonai_5; }
	inline void set__teawoonai_5(bool value)
	{
		____teawoonai_5 = value;
	}

	inline static int32_t get_offset_of__hearfo_6() { return static_cast<int32_t>(offsetof(M_fearteenasWallger10_t1129392077, ____hearfo_6)); }
	inline uint32_t get__hearfo_6() const { return ____hearfo_6; }
	inline uint32_t* get_address_of__hearfo_6() { return &____hearfo_6; }
	inline void set__hearfo_6(uint32_t value)
	{
		____hearfo_6 = value;
	}

	inline static int32_t get_offset_of__yobo_7() { return static_cast<int32_t>(offsetof(M_fearteenasWallger10_t1129392077, ____yobo_7)); }
	inline String_t* get__yobo_7() const { return ____yobo_7; }
	inline String_t** get_address_of__yobo_7() { return &____yobo_7; }
	inline void set__yobo_7(String_t* value)
	{
		____yobo_7 = value;
		Il2CppCodeGenWriteBarrier(&____yobo_7, value);
	}

	inline static int32_t get_offset_of__mezegoRisnem_8() { return static_cast<int32_t>(offsetof(M_fearteenasWallger10_t1129392077, ____mezegoRisnem_8)); }
	inline uint32_t get__mezegoRisnem_8() const { return ____mezegoRisnem_8; }
	inline uint32_t* get_address_of__mezegoRisnem_8() { return &____mezegoRisnem_8; }
	inline void set__mezegoRisnem_8(uint32_t value)
	{
		____mezegoRisnem_8 = value;
	}

	inline static int32_t get_offset_of__kohajaNaske_9() { return static_cast<int32_t>(offsetof(M_fearteenasWallger10_t1129392077, ____kohajaNaske_9)); }
	inline float get__kohajaNaske_9() const { return ____kohajaNaske_9; }
	inline float* get_address_of__kohajaNaske_9() { return &____kohajaNaske_9; }
	inline void set__kohajaNaske_9(float value)
	{
		____kohajaNaske_9 = value;
	}

	inline static int32_t get_offset_of__falne_10() { return static_cast<int32_t>(offsetof(M_fearteenasWallger10_t1129392077, ____falne_10)); }
	inline float get__falne_10() const { return ____falne_10; }
	inline float* get_address_of__falne_10() { return &____falne_10; }
	inline void set__falne_10(float value)
	{
		____falne_10 = value;
	}

	inline static int32_t get_offset_of__choujerlur_11() { return static_cast<int32_t>(offsetof(M_fearteenasWallger10_t1129392077, ____choujerlur_11)); }
	inline bool get__choujerlur_11() const { return ____choujerlur_11; }
	inline bool* get_address_of__choujerlur_11() { return &____choujerlur_11; }
	inline void set__choujerlur_11(bool value)
	{
		____choujerlur_11 = value;
	}

	inline static int32_t get_offset_of__naysti_12() { return static_cast<int32_t>(offsetof(M_fearteenasWallger10_t1129392077, ____naysti_12)); }
	inline uint32_t get__naysti_12() const { return ____naysti_12; }
	inline uint32_t* get_address_of__naysti_12() { return &____naysti_12; }
	inline void set__naysti_12(uint32_t value)
	{
		____naysti_12 = value;
	}

	inline static int32_t get_offset_of__fouxeca_13() { return static_cast<int32_t>(offsetof(M_fearteenasWallger10_t1129392077, ____fouxeca_13)); }
	inline bool get__fouxeca_13() const { return ____fouxeca_13; }
	inline bool* get_address_of__fouxeca_13() { return &____fouxeca_13; }
	inline void set__fouxeca_13(bool value)
	{
		____fouxeca_13 = value;
	}

	inline static int32_t get_offset_of__xeatawmiJelstu_14() { return static_cast<int32_t>(offsetof(M_fearteenasWallger10_t1129392077, ____xeatawmiJelstu_14)); }
	inline float get__xeatawmiJelstu_14() const { return ____xeatawmiJelstu_14; }
	inline float* get_address_of__xeatawmiJelstu_14() { return &____xeatawmiJelstu_14; }
	inline void set__xeatawmiJelstu_14(float value)
	{
		____xeatawmiJelstu_14 = value;
	}

	inline static int32_t get_offset_of__huwihas_15() { return static_cast<int32_t>(offsetof(M_fearteenasWallger10_t1129392077, ____huwihas_15)); }
	inline int32_t get__huwihas_15() const { return ____huwihas_15; }
	inline int32_t* get_address_of__huwihas_15() { return &____huwihas_15; }
	inline void set__huwihas_15(int32_t value)
	{
		____huwihas_15 = value;
	}

	inline static int32_t get_offset_of__drizurTige_16() { return static_cast<int32_t>(offsetof(M_fearteenasWallger10_t1129392077, ____drizurTige_16)); }
	inline String_t* get__drizurTige_16() const { return ____drizurTige_16; }
	inline String_t** get_address_of__drizurTige_16() { return &____drizurTige_16; }
	inline void set__drizurTige_16(String_t* value)
	{
		____drizurTige_16 = value;
		Il2CppCodeGenWriteBarrier(&____drizurTige_16, value);
	}

	inline static int32_t get_offset_of__trorraLeldraju_17() { return static_cast<int32_t>(offsetof(M_fearteenasWallger10_t1129392077, ____trorraLeldraju_17)); }
	inline int32_t get__trorraLeldraju_17() const { return ____trorraLeldraju_17; }
	inline int32_t* get_address_of__trorraLeldraju_17() { return &____trorraLeldraju_17; }
	inline void set__trorraLeldraju_17(int32_t value)
	{
		____trorraLeldraju_17 = value;
	}

	inline static int32_t get_offset_of__sorpisgiMalarfor_18() { return static_cast<int32_t>(offsetof(M_fearteenasWallger10_t1129392077, ____sorpisgiMalarfor_18)); }
	inline bool get__sorpisgiMalarfor_18() const { return ____sorpisgiMalarfor_18; }
	inline bool* get_address_of__sorpisgiMalarfor_18() { return &____sorpisgiMalarfor_18; }
	inline void set__sorpisgiMalarfor_18(bool value)
	{
		____sorpisgiMalarfor_18 = value;
	}

	inline static int32_t get_offset_of__rego_19() { return static_cast<int32_t>(offsetof(M_fearteenasWallger10_t1129392077, ____rego_19)); }
	inline uint32_t get__rego_19() const { return ____rego_19; }
	inline uint32_t* get_address_of__rego_19() { return &____rego_19; }
	inline void set__rego_19(uint32_t value)
	{
		____rego_19 = value;
	}

	inline static int32_t get_offset_of__vanair_20() { return static_cast<int32_t>(offsetof(M_fearteenasWallger10_t1129392077, ____vanair_20)); }
	inline float get__vanair_20() const { return ____vanair_20; }
	inline float* get_address_of__vanair_20() { return &____vanair_20; }
	inline void set__vanair_20(float value)
	{
		____vanair_20 = value;
	}

	inline static int32_t get_offset_of__gealearsorGiwercer_21() { return static_cast<int32_t>(offsetof(M_fearteenasWallger10_t1129392077, ____gealearsorGiwercer_21)); }
	inline bool get__gealearsorGiwercer_21() const { return ____gealearsorGiwercer_21; }
	inline bool* get_address_of__gealearsorGiwercer_21() { return &____gealearsorGiwercer_21; }
	inline void set__gealearsorGiwercer_21(bool value)
	{
		____gealearsorGiwercer_21 = value;
	}

	inline static int32_t get_offset_of__faimere_22() { return static_cast<int32_t>(offsetof(M_fearteenasWallger10_t1129392077, ____faimere_22)); }
	inline uint32_t get__faimere_22() const { return ____faimere_22; }
	inline uint32_t* get_address_of__faimere_22() { return &____faimere_22; }
	inline void set__faimere_22(uint32_t value)
	{
		____faimere_22 = value;
	}

	inline static int32_t get_offset_of__saijedi_23() { return static_cast<int32_t>(offsetof(M_fearteenasWallger10_t1129392077, ____saijedi_23)); }
	inline int32_t get__saijedi_23() const { return ____saijedi_23; }
	inline int32_t* get_address_of__saijedi_23() { return &____saijedi_23; }
	inline void set__saijedi_23(int32_t value)
	{
		____saijedi_23 = value;
	}

	inline static int32_t get_offset_of__coumemkalMamawte_24() { return static_cast<int32_t>(offsetof(M_fearteenasWallger10_t1129392077, ____coumemkalMamawte_24)); }
	inline String_t* get__coumemkalMamawte_24() const { return ____coumemkalMamawte_24; }
	inline String_t** get_address_of__coumemkalMamawte_24() { return &____coumemkalMamawte_24; }
	inline void set__coumemkalMamawte_24(String_t* value)
	{
		____coumemkalMamawte_24 = value;
		Il2CppCodeGenWriteBarrier(&____coumemkalMamawte_24, value);
	}

	inline static int32_t get_offset_of__repota_25() { return static_cast<int32_t>(offsetof(M_fearteenasWallger10_t1129392077, ____repota_25)); }
	inline String_t* get__repota_25() const { return ____repota_25; }
	inline String_t** get_address_of__repota_25() { return &____repota_25; }
	inline void set__repota_25(String_t* value)
	{
		____repota_25 = value;
		Il2CppCodeGenWriteBarrier(&____repota_25, value);
	}

	inline static int32_t get_offset_of__noochiBairmou_26() { return static_cast<int32_t>(offsetof(M_fearteenasWallger10_t1129392077, ____noochiBairmou_26)); }
	inline uint32_t get__noochiBairmou_26() const { return ____noochiBairmou_26; }
	inline uint32_t* get_address_of__noochiBairmou_26() { return &____noochiBairmou_26; }
	inline void set__noochiBairmou_26(uint32_t value)
	{
		____noochiBairmou_26 = value;
	}

	inline static int32_t get_offset_of__drahear_27() { return static_cast<int32_t>(offsetof(M_fearteenasWallger10_t1129392077, ____drahear_27)); }
	inline int32_t get__drahear_27() const { return ____drahear_27; }
	inline int32_t* get_address_of__drahear_27() { return &____drahear_27; }
	inline void set__drahear_27(int32_t value)
	{
		____drahear_27 = value;
	}

	inline static int32_t get_offset_of__malnel_28() { return static_cast<int32_t>(offsetof(M_fearteenasWallger10_t1129392077, ____malnel_28)); }
	inline String_t* get__malnel_28() const { return ____malnel_28; }
	inline String_t** get_address_of__malnel_28() { return &____malnel_28; }
	inline void set__malnel_28(String_t* value)
	{
		____malnel_28 = value;
		Il2CppCodeGenWriteBarrier(&____malnel_28, value);
	}

	inline static int32_t get_offset_of__xekarwee_29() { return static_cast<int32_t>(offsetof(M_fearteenasWallger10_t1129392077, ____xekarwee_29)); }
	inline int32_t get__xekarwee_29() const { return ____xekarwee_29; }
	inline int32_t* get_address_of__xekarwee_29() { return &____xekarwee_29; }
	inline void set__xekarwee_29(int32_t value)
	{
		____xekarwee_29 = value;
	}

	inline static int32_t get_offset_of__lounalKurdral_30() { return static_cast<int32_t>(offsetof(M_fearteenasWallger10_t1129392077, ____lounalKurdral_30)); }
	inline float get__lounalKurdral_30() const { return ____lounalKurdral_30; }
	inline float* get_address_of__lounalKurdral_30() { return &____lounalKurdral_30; }
	inline void set__lounalKurdral_30(float value)
	{
		____lounalKurdral_30 = value;
	}

	inline static int32_t get_offset_of__durma_31() { return static_cast<int32_t>(offsetof(M_fearteenasWallger10_t1129392077, ____durma_31)); }
	inline float get__durma_31() const { return ____durma_31; }
	inline float* get_address_of__durma_31() { return &____durma_31; }
	inline void set__durma_31(float value)
	{
		____durma_31 = value;
	}

	inline static int32_t get_offset_of__saseTawheljou_32() { return static_cast<int32_t>(offsetof(M_fearteenasWallger10_t1129392077, ____saseTawheljou_32)); }
	inline float get__saseTawheljou_32() const { return ____saseTawheljou_32; }
	inline float* get_address_of__saseTawheljou_32() { return &____saseTawheljou_32; }
	inline void set__saseTawheljou_32(float value)
	{
		____saseTawheljou_32 = value;
	}

	inline static int32_t get_offset_of__fuse_33() { return static_cast<int32_t>(offsetof(M_fearteenasWallger10_t1129392077, ____fuse_33)); }
	inline uint32_t get__fuse_33() const { return ____fuse_33; }
	inline uint32_t* get_address_of__fuse_33() { return &____fuse_33; }
	inline void set__fuse_33(uint32_t value)
	{
		____fuse_33 = value;
	}

	inline static int32_t get_offset_of__roofouvairLearponu_34() { return static_cast<int32_t>(offsetof(M_fearteenasWallger10_t1129392077, ____roofouvairLearponu_34)); }
	inline String_t* get__roofouvairLearponu_34() const { return ____roofouvairLearponu_34; }
	inline String_t** get_address_of__roofouvairLearponu_34() { return &____roofouvairLearponu_34; }
	inline void set__roofouvairLearponu_34(String_t* value)
	{
		____roofouvairLearponu_34 = value;
		Il2CppCodeGenWriteBarrier(&____roofouvairLearponu_34, value);
	}

	inline static int32_t get_offset_of__rarmaldralWenaw_35() { return static_cast<int32_t>(offsetof(M_fearteenasWallger10_t1129392077, ____rarmaldralWenaw_35)); }
	inline bool get__rarmaldralWenaw_35() const { return ____rarmaldralWenaw_35; }
	inline bool* get_address_of__rarmaldralWenaw_35() { return &____rarmaldralWenaw_35; }
	inline void set__rarmaldralWenaw_35(bool value)
	{
		____rarmaldralWenaw_35 = value;
	}

	inline static int32_t get_offset_of__jaihuVelcoo_36() { return static_cast<int32_t>(offsetof(M_fearteenasWallger10_t1129392077, ____jaihuVelcoo_36)); }
	inline int32_t get__jaihuVelcoo_36() const { return ____jaihuVelcoo_36; }
	inline int32_t* get_address_of__jaihuVelcoo_36() { return &____jaihuVelcoo_36; }
	inline void set__jaihuVelcoo_36(int32_t value)
	{
		____jaihuVelcoo_36 = value;
	}

	inline static int32_t get_offset_of__guwirwaChasemji_37() { return static_cast<int32_t>(offsetof(M_fearteenasWallger10_t1129392077, ____guwirwaChasemji_37)); }
	inline float get__guwirwaChasemji_37() const { return ____guwirwaChasemji_37; }
	inline float* get_address_of__guwirwaChasemji_37() { return &____guwirwaChasemji_37; }
	inline void set__guwirwaChasemji_37(float value)
	{
		____guwirwaChasemji_37 = value;
	}

	inline static int32_t get_offset_of__drabaKimardaw_38() { return static_cast<int32_t>(offsetof(M_fearteenasWallger10_t1129392077, ____drabaKimardaw_38)); }
	inline int32_t get__drabaKimardaw_38() const { return ____drabaKimardaw_38; }
	inline int32_t* get_address_of__drabaKimardaw_38() { return &____drabaKimardaw_38; }
	inline void set__drabaKimardaw_38(int32_t value)
	{
		____drabaKimardaw_38 = value;
	}

	inline static int32_t get_offset_of__recisTrowjalda_39() { return static_cast<int32_t>(offsetof(M_fearteenasWallger10_t1129392077, ____recisTrowjalda_39)); }
	inline int32_t get__recisTrowjalda_39() const { return ____recisTrowjalda_39; }
	inline int32_t* get_address_of__recisTrowjalda_39() { return &____recisTrowjalda_39; }
	inline void set__recisTrowjalda_39(int32_t value)
	{
		____recisTrowjalda_39 = value;
	}

	inline static int32_t get_offset_of__casdras_40() { return static_cast<int32_t>(offsetof(M_fearteenasWallger10_t1129392077, ____casdras_40)); }
	inline bool get__casdras_40() const { return ____casdras_40; }
	inline bool* get_address_of__casdras_40() { return &____casdras_40; }
	inline void set__casdras_40(bool value)
	{
		____casdras_40 = value;
	}

	inline static int32_t get_offset_of__milouTosa_41() { return static_cast<int32_t>(offsetof(M_fearteenasWallger10_t1129392077, ____milouTosa_41)); }
	inline float get__milouTosa_41() const { return ____milouTosa_41; }
	inline float* get_address_of__milouTosa_41() { return &____milouTosa_41; }
	inline void set__milouTosa_41(float value)
	{
		____milouTosa_41 = value;
	}

	inline static int32_t get_offset_of__pisweCebaigo_42() { return static_cast<int32_t>(offsetof(M_fearteenasWallger10_t1129392077, ____pisweCebaigo_42)); }
	inline uint32_t get__pisweCebaigo_42() const { return ____pisweCebaigo_42; }
	inline uint32_t* get_address_of__pisweCebaigo_42() { return &____pisweCebaigo_42; }
	inline void set__pisweCebaigo_42(uint32_t value)
	{
		____pisweCebaigo_42 = value;
	}

	inline static int32_t get_offset_of__seetayGeanou_43() { return static_cast<int32_t>(offsetof(M_fearteenasWallger10_t1129392077, ____seetayGeanou_43)); }
	inline int32_t get__seetayGeanou_43() const { return ____seetayGeanou_43; }
	inline int32_t* get_address_of__seetayGeanou_43() { return &____seetayGeanou_43; }
	inline void set__seetayGeanou_43(int32_t value)
	{
		____seetayGeanou_43 = value;
	}

	inline static int32_t get_offset_of__serloucuBovesto_44() { return static_cast<int32_t>(offsetof(M_fearteenasWallger10_t1129392077, ____serloucuBovesto_44)); }
	inline float get__serloucuBovesto_44() const { return ____serloucuBovesto_44; }
	inline float* get_address_of__serloucuBovesto_44() { return &____serloucuBovesto_44; }
	inline void set__serloucuBovesto_44(float value)
	{
		____serloucuBovesto_44 = value;
	}

	inline static int32_t get_offset_of__ceenee_45() { return static_cast<int32_t>(offsetof(M_fearteenasWallger10_t1129392077, ____ceenee_45)); }
	inline float get__ceenee_45() const { return ____ceenee_45; }
	inline float* get_address_of__ceenee_45() { return &____ceenee_45; }
	inline void set__ceenee_45(float value)
	{
		____ceenee_45 = value;
	}

	inline static int32_t get_offset_of__zaseca_46() { return static_cast<int32_t>(offsetof(M_fearteenasWallger10_t1129392077, ____zaseca_46)); }
	inline int32_t get__zaseca_46() const { return ____zaseca_46; }
	inline int32_t* get_address_of__zaseca_46() { return &____zaseca_46; }
	inline void set__zaseca_46(int32_t value)
	{
		____zaseca_46 = value;
	}

	inline static int32_t get_offset_of__staiza_47() { return static_cast<int32_t>(offsetof(M_fearteenasWallger10_t1129392077, ____staiza_47)); }
	inline String_t* get__staiza_47() const { return ____staiza_47; }
	inline String_t** get_address_of__staiza_47() { return &____staiza_47; }
	inline void set__staiza_47(String_t* value)
	{
		____staiza_47 = value;
		Il2CppCodeGenWriteBarrier(&____staiza_47, value);
	}

	inline static int32_t get_offset_of__stikoujeeJesu_48() { return static_cast<int32_t>(offsetof(M_fearteenasWallger10_t1129392077, ____stikoujeeJesu_48)); }
	inline uint32_t get__stikoujeeJesu_48() const { return ____stikoujeeJesu_48; }
	inline uint32_t* get_address_of__stikoujeeJesu_48() { return &____stikoujeeJesu_48; }
	inline void set__stikoujeeJesu_48(uint32_t value)
	{
		____stikoujeeJesu_48 = value;
	}

	inline static int32_t get_offset_of__durrerenem_49() { return static_cast<int32_t>(offsetof(M_fearteenasWallger10_t1129392077, ____durrerenem_49)); }
	inline bool get__durrerenem_49() const { return ____durrerenem_49; }
	inline bool* get_address_of__durrerenem_49() { return &____durrerenem_49; }
	inline void set__durrerenem_49(bool value)
	{
		____durrerenem_49 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
