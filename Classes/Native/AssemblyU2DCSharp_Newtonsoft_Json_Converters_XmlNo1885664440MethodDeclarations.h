﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Converters.XmlNodeConverter/<ReadArrayElements>c__AnonStorey12B
struct U3CReadArrayElementsU3Ec__AnonStorey12B_t1885664440;
// Newtonsoft.Json.Converters.IXmlElement
struct IXmlElement_t2583566720;

#include "codegen/il2cpp-codegen.h"

// System.Void Newtonsoft.Json.Converters.XmlNodeConverter/<ReadArrayElements>c__AnonStorey12B::.ctor()
extern "C"  void U3CReadArrayElementsU3Ec__AnonStorey12B__ctor_m2473812403 (U3CReadArrayElementsU3Ec__AnonStorey12B_t1885664440 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Converters.XmlNodeConverter/<ReadArrayElements>c__AnonStorey12B::<>m__36C(Newtonsoft.Json.Converters.IXmlElement)
extern "C"  bool U3CReadArrayElementsU3Ec__AnonStorey12B_U3CU3Em__36C_m1884029354 (U3CReadArrayElementsU3Ec__AnonStorey12B_t1885664440 * __this, Il2CppObject * ___n0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
