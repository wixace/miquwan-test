﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIDrawCallGenerated
struct UIDrawCallGenerated_t1698233;
// JSVCall
struct JSVCall_t3708497963;
// UIDrawCall/OnRenderCallback
struct OnRenderCallback_t2651898963;
// CSRepresentedObject
struct CSRepresentedObject_t3994124630;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;
// System.Delegate
struct Delegate_t3310234105;
// BetterList`1<UIDrawCall>
struct BetterList_1_t2410241986;
// UIDrawCall
struct UIDrawCall_t913273974;
// UnityEngine.Material
struct Material_t3870600107;
// UIPanel
struct UIPanel_t295209936;
// UnityEngine.Texture
struct Texture_t2526458961;
// UnityEngine.Shader
struct Shader_t3191267369;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_CSRepresentedObject3994124630.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Delegate3310234105.h"
#include "AssemblyU2DCSharp_UIDrawCall913273974.h"
#include "UnityEngine_UnityEngine_Material3870600107.h"
#include "AssemblyU2DCSharp_UIPanel295209936.h"
#include "UnityEngine_UnityEngine_Texture2526458961.h"
#include "UnityEngine_UnityEngine_Shader3191267369.h"

// System.Void UIDrawCallGenerated::.ctor()
extern "C"  void UIDrawCallGenerated__ctor_m254609874 (UIDrawCallGenerated_t1698233 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIDrawCallGenerated::UIDrawCall_UIDrawCall1(JSVCall,System.Int32)
extern "C"  bool UIDrawCallGenerated_UIDrawCall_UIDrawCall1_m3604649696 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDrawCallGenerated::UIDrawCall_widgetCount(JSVCall)
extern "C"  void UIDrawCallGenerated_UIDrawCall_widgetCount_m4097637875 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDrawCallGenerated::UIDrawCall_depthStart(JSVCall)
extern "C"  void UIDrawCallGenerated_UIDrawCall_depthStart_m3578508495 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDrawCallGenerated::UIDrawCall_depthEnd(JSVCall)
extern "C"  void UIDrawCallGenerated_UIDrawCall_depthEnd_m3157149494 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDrawCallGenerated::UIDrawCall_manager(JSVCall)
extern "C"  void UIDrawCallGenerated_UIDrawCall_manager_m2012564913 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDrawCallGenerated::UIDrawCall_panel(JSVCall)
extern "C"  void UIDrawCallGenerated_UIDrawCall_panel_m786101050 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDrawCallGenerated::UIDrawCall_clipTexture(JSVCall)
extern "C"  void UIDrawCallGenerated_UIDrawCall_clipTexture_m563982195 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDrawCallGenerated::UIDrawCall_alwaysOnScreen(JSVCall)
extern "C"  void UIDrawCallGenerated_UIDrawCall_alwaysOnScreen_m549976116 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDrawCallGenerated::UIDrawCall_verts(JSVCall)
extern "C"  void UIDrawCallGenerated_UIDrawCall_verts_m3125341596 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDrawCallGenerated::UIDrawCall_norms(JSVCall)
extern "C"  void UIDrawCallGenerated_UIDrawCall_norms_m3929538151 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDrawCallGenerated::UIDrawCall_tans(JSVCall)
extern "C"  void UIDrawCallGenerated_UIDrawCall_tans_m3714406044 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDrawCallGenerated::UIDrawCall_uvs(JSVCall)
extern "C"  void UIDrawCallGenerated_UIDrawCall_uvs_m1747202316 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDrawCallGenerated::UIDrawCall_cols(JSVCall)
extern "C"  void UIDrawCallGenerated_UIDrawCall_cols_m1218416059 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDrawCallGenerated::UIDrawCall_isDirty(JSVCall)
extern "C"  void UIDrawCallGenerated_UIDrawCall_isDirty_m1692080886 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIDrawCall/OnRenderCallback UIDrawCallGenerated::UIDrawCall_onRender_GetDelegate_member13_arg0(CSRepresentedObject)
extern "C"  OnRenderCallback_t2651898963 * UIDrawCallGenerated_UIDrawCall_onRender_GetDelegate_member13_arg0_m893501012 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDrawCallGenerated::UIDrawCall_onRender(JSVCall)
extern "C"  void UIDrawCallGenerated_UIDrawCall_onRender_m58027673 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDrawCallGenerated::UIDrawCall_activeList(JSVCall)
extern "C"  void UIDrawCallGenerated_UIDrawCall_activeList_m3457978634 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDrawCallGenerated::UIDrawCall_inactiveList(JSVCall)
extern "C"  void UIDrawCallGenerated_UIDrawCall_inactiveList_m3391393221 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDrawCallGenerated::UIDrawCall_renderQueue(JSVCall)
extern "C"  void UIDrawCallGenerated_UIDrawCall_renderQueue_m2004142083 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDrawCallGenerated::UIDrawCall_sortingOrder(JSVCall)
extern "C"  void UIDrawCallGenerated_UIDrawCall_sortingOrder_m1565052516 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDrawCallGenerated::UIDrawCall_finalRenderQueue(JSVCall)
extern "C"  void UIDrawCallGenerated_UIDrawCall_finalRenderQueue_m1555719977 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDrawCallGenerated::UIDrawCall_cachedTransform(JSVCall)
extern "C"  void UIDrawCallGenerated_UIDrawCall_cachedTransform_m752400788 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDrawCallGenerated::UIDrawCall_baseMaterial(JSVCall)
extern "C"  void UIDrawCallGenerated_UIDrawCall_baseMaterial_m4039696182 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDrawCallGenerated::UIDrawCall_dynamicMaterial(JSVCall)
extern "C"  void UIDrawCallGenerated_UIDrawCall_dynamicMaterial_m2482458360 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDrawCallGenerated::UIDrawCall_mainTexture(JSVCall)
extern "C"  void UIDrawCallGenerated_UIDrawCall_mainTexture_m3058311132 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDrawCallGenerated::UIDrawCall_shader(JSVCall)
extern "C"  void UIDrawCallGenerated_UIDrawCall_shader_m1976787721 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDrawCallGenerated::UIDrawCall_triangles(JSVCall)
extern "C"  void UIDrawCallGenerated_UIDrawCall_triangles_m2361298355 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDrawCallGenerated::UIDrawCall_isClipped(JSVCall)
extern "C"  void UIDrawCallGenerated_UIDrawCall_isClipped_m2477364521 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIDrawCallGenerated::UIDrawCall_UpdateGeometry__Int32(JSVCall,System.Int32)
extern "C"  bool UIDrawCallGenerated_UIDrawCall_UpdateGeometry__Int32_m75144984 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIDrawCallGenerated::UIDrawCall_ClearAll(JSVCall,System.Int32)
extern "C"  bool UIDrawCallGenerated_UIDrawCall_ClearAll_m1265266993 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIDrawCallGenerated::UIDrawCall_Count__UIPanel(JSVCall,System.Int32)
extern "C"  bool UIDrawCallGenerated_UIDrawCall_Count__UIPanel_m2289340446 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIDrawCallGenerated::UIDrawCall_Create__UIPanel__Material__Texture__Shader(JSVCall,System.Int32)
extern "C"  bool UIDrawCallGenerated_UIDrawCall_Create__UIPanel__Material__Texture__Shader_m2152771618 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIDrawCallGenerated::UIDrawCall_Destroy__UIDrawCall(JSVCall,System.Int32)
extern "C"  bool UIDrawCallGenerated_UIDrawCall_Destroy__UIDrawCall_m3164400597 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIDrawCallGenerated::UIDrawCall_ReleaseAll(JSVCall,System.Int32)
extern "C"  bool UIDrawCallGenerated_UIDrawCall_ReleaseAll_m3338531255 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIDrawCallGenerated::UIDrawCall_ReleaseInactive(JSVCall,System.Int32)
extern "C"  bool UIDrawCallGenerated_UIDrawCall_ReleaseInactive_m2477955223 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDrawCallGenerated::__Register()
extern "C"  void UIDrawCallGenerated___Register_m2357970485 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIDrawCall/OnRenderCallback UIDrawCallGenerated::<UIDrawCall_onRender>m__12D()
extern "C"  OnRenderCallback_t2651898963 * UIDrawCallGenerated_U3CUIDrawCall_onRenderU3Em__12D_m4276543042 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDrawCallGenerated::ilo_setInt321(System.Int32,System.Int32)
extern "C"  void UIDrawCallGenerated_ilo_setInt321_m3356920900 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIDrawCallGenerated::ilo_getInt322(System.Int32)
extern "C"  int32_t UIDrawCallGenerated_ilo_getInt322_m443600054 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIDrawCallGenerated::ilo_setObject3(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UIDrawCallGenerated_ilo_setObject3_m41262750 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDrawCallGenerated::ilo_addJSFunCSDelegateRel4(System.Int32,System.Delegate)
extern "C"  void UIDrawCallGenerated_ilo_addJSFunCSDelegateRel4_m700751875 (Il2CppObject * __this /* static, unused */, int32_t ___funID0, Delegate_t3310234105 * ___del1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// BetterList`1<UIDrawCall> UIDrawCallGenerated::ilo_get_inactiveList5()
extern "C"  BetterList_1_t2410241986 * UIDrawCallGenerated_ilo_get_inactiveList5_m1106464626 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDrawCallGenerated::ilo_set_sortingOrder6(UIDrawCall,System.Int32)
extern "C"  void UIDrawCallGenerated_ilo_set_sortingOrder6_m1574038945 (Il2CppObject * __this /* static, unused */, UIDrawCall_t913273974 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDrawCallGenerated::ilo_set_baseMaterial7(UIDrawCall,UnityEngine.Material)
extern "C"  void UIDrawCallGenerated_ilo_set_baseMaterial7_m2978029247 (Il2CppObject * __this /* static, unused */, UIDrawCall_t913273974 * ____this0, Material_t3870600107 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDrawCallGenerated::ilo_setBooleanS8(System.Int32,System.Boolean)
extern "C"  void UIDrawCallGenerated_ilo_setBooleanS8_m2247176220 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UIDrawCallGenerated::ilo_getObject9(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UIDrawCallGenerated_ilo_getObject9_m4203795035 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIDrawCall UIDrawCallGenerated::ilo_Create10(UIPanel,UnityEngine.Material,UnityEngine.Texture,UnityEngine.Shader)
extern "C"  UIDrawCall_t913273974 * UIDrawCallGenerated_ilo_Create10_m1769255811 (Il2CppObject * __this /* static, unused */, UIPanel_t295209936 * ___panel0, Material_t3870600107 * ___mat1, Texture_t2526458961 * ___tex2, Shader_t3191267369 * ___shader3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDrawCallGenerated::ilo_ReleaseInactive11()
extern "C"  void UIDrawCallGenerated_ilo_ReleaseInactive11_m3539603087 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
