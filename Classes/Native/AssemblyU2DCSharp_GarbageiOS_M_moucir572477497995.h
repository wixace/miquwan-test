﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_moucir57
struct  M_moucir57_t2477497995  : public Il2CppObject
{
public:
	// System.String GarbageiOS.M_moucir57::_chedou
	String_t* ____chedou_0;
	// System.UInt32 GarbageiOS.M_moucir57::_vayreserePapis
	uint32_t ____vayreserePapis_1;
	// System.UInt32 GarbageiOS.M_moucir57::_gallrelMorpay
	uint32_t ____gallrelMorpay_2;
	// System.String GarbageiOS.M_moucir57::_fouwirrereTikearre
	String_t* ____fouwirrereTikearre_3;
	// System.Single GarbageiOS.M_moucir57::_koneMavurdrur
	float ____koneMavurdrur_4;
	// System.Single GarbageiOS.M_moucir57::_tete
	float ____tete_5;
	// System.Int32 GarbageiOS.M_moucir57::_lowloJuwhaijir
	int32_t ____lowloJuwhaijir_6;
	// System.Single GarbageiOS.M_moucir57::_deheTroubou
	float ____deheTroubou_7;
	// System.String GarbageiOS.M_moucir57::_tawnair
	String_t* ____tawnair_8;
	// System.Single GarbageiOS.M_moucir57::_jajiSardoca
	float ____jajiSardoca_9;
	// System.Int32 GarbageiOS.M_moucir57::_telu
	int32_t ____telu_10;
	// System.Single GarbageiOS.M_moucir57::_geerear
	float ____geerear_11;
	// System.Boolean GarbageiOS.M_moucir57::_kowtebeRairsuwou
	bool ____kowtebeRairsuwou_12;
	// System.Boolean GarbageiOS.M_moucir57::_rursefou
	bool ____rursefou_13;
	// System.UInt32 GarbageiOS.M_moucir57::_chujawTajall
	uint32_t ____chujawTajall_14;
	// System.Int32 GarbageiOS.M_moucir57::_toupalya
	int32_t ____toupalya_15;
	// System.Single GarbageiOS.M_moucir57::_stewemjo
	float ____stewemjo_16;
	// System.Int32 GarbageiOS.M_moucir57::_laija
	int32_t ____laija_17;
	// System.Boolean GarbageiOS.M_moucir57::_forceja
	bool ____forceja_18;
	// System.String GarbageiOS.M_moucir57::_powdeecher
	String_t* ____powdeecher_19;
	// System.Boolean GarbageiOS.M_moucir57::_geadouMouti
	bool ____geadouMouti_20;
	// System.Single GarbageiOS.M_moucir57::_jereyouso
	float ____jereyouso_21;
	// System.UInt32 GarbageiOS.M_moucir57::_liju
	uint32_t ____liju_22;
	// System.Int32 GarbageiOS.M_moucir57::_jetor
	int32_t ____jetor_23;
	// System.Int32 GarbageiOS.M_moucir57::_keldis
	int32_t ____keldis_24;
	// System.UInt32 GarbageiOS.M_moucir57::_cadraForrir
	uint32_t ____cadraForrir_25;
	// System.String GarbageiOS.M_moucir57::_jallker
	String_t* ____jallker_26;
	// System.Boolean GarbageiOS.M_moucir57::_lereseKairnersow
	bool ____lereseKairnersow_27;
	// System.UInt32 GarbageiOS.M_moucir57::_loujurBaceare
	uint32_t ____loujurBaceare_28;
	// System.Int32 GarbageiOS.M_moucir57::_racere
	int32_t ____racere_29;
	// System.Int32 GarbageiOS.M_moucir57::_dasisaiKeja
	int32_t ____dasisaiKeja_30;
	// System.Boolean GarbageiOS.M_moucir57::_hapitel
	bool ____hapitel_31;
	// System.String GarbageiOS.M_moucir57::_yisjayMejador
	String_t* ____yisjayMejador_32;
	// System.Boolean GarbageiOS.M_moucir57::_mokurlou
	bool ____mokurlou_33;
	// System.String GarbageiOS.M_moucir57::_nabirDerebase
	String_t* ____nabirDerebase_34;
	// System.Int32 GarbageiOS.M_moucir57::_bissou
	int32_t ____bissou_35;
	// System.String GarbageiOS.M_moucir57::_boutaisereRitrerwhir
	String_t* ____boutaisereRitrerwhir_36;
	// System.Single GarbageiOS.M_moucir57::_carwi
	float ____carwi_37;
	// System.Single GarbageiOS.M_moucir57::_risecou
	float ____risecou_38;
	// System.String GarbageiOS.M_moucir57::_faydikaWhayfu
	String_t* ____faydikaWhayfu_39;
	// System.Boolean GarbageiOS.M_moucir57::_rainartaRousairba
	bool ____rainartaRousairba_40;
	// System.Boolean GarbageiOS.M_moucir57::_neteeji
	bool ____neteeji_41;
	// System.UInt32 GarbageiOS.M_moucir57::_moutehelBomeeto
	uint32_t ____moutehelBomeeto_42;
	// System.UInt32 GarbageiOS.M_moucir57::_talairdereRelcairdor
	uint32_t ____talairdereRelcairdor_43;
	// System.Single GarbageiOS.M_moucir57::_wayherebur
	float ____wayherebur_44;
	// System.Single GarbageiOS.M_moucir57::_souvar
	float ____souvar_45;
	// System.Int32 GarbageiOS.M_moucir57::_pawee
	int32_t ____pawee_46;
	// System.UInt32 GarbageiOS.M_moucir57::_raypotroJegucis
	uint32_t ____raypotroJegucis_47;
	// System.Single GarbageiOS.M_moucir57::_raygaqarRerbarla
	float ____raygaqarRerbarla_48;
	// System.Boolean GarbageiOS.M_moucir57::_nochuco
	bool ____nochuco_49;
	// System.Int32 GarbageiOS.M_moucir57::_nemfarKomawchar
	int32_t ____nemfarKomawchar_50;
	// System.UInt32 GarbageiOS.M_moucir57::_fosa
	uint32_t ____fosa_51;
	// System.Boolean GarbageiOS.M_moucir57::_lasre
	bool ____lasre_52;
	// System.Single GarbageiOS.M_moucir57::_roseeZallpearlaw
	float ____roseeZallpearlaw_53;
	// System.String GarbageiOS.M_moucir57::_tusici
	String_t* ____tusici_54;
	// System.String GarbageiOS.M_moucir57::_rolall
	String_t* ____rolall_55;
	// System.UInt32 GarbageiOS.M_moucir57::_neasairno
	uint32_t ____neasairno_56;
	// System.String GarbageiOS.M_moucir57::_sowhewherHorsou
	String_t* ____sowhewherHorsou_57;
	// System.String GarbageiOS.M_moucir57::_meepama
	String_t* ____meepama_58;
	// System.String GarbageiOS.M_moucir57::_pouseaHigere
	String_t* ____pouseaHigere_59;
	// System.Int32 GarbageiOS.M_moucir57::_pemastemVorlo
	int32_t ____pemastemVorlo_60;
	// System.Single GarbageiOS.M_moucir57::_meawhemirSega
	float ____meawhemirSega_61;
	// System.UInt32 GarbageiOS.M_moucir57::_choule
	uint32_t ____choule_62;
	// System.UInt32 GarbageiOS.M_moucir57::_sisnarkairBuseavem
	uint32_t ____sisnarkairBuseavem_63;
	// System.Int32 GarbageiOS.M_moucir57::_tisoguKamooji
	int32_t ____tisoguKamooji_64;
	// System.UInt32 GarbageiOS.M_moucir57::_nazeDrasfas
	uint32_t ____nazeDrasfas_65;
	// System.Boolean GarbageiOS.M_moucir57::_tropairReremoba
	bool ____tropairReremoba_66;
	// System.Boolean GarbageiOS.M_moucir57::_liverFenal
	bool ____liverFenal_67;
	// System.String GarbageiOS.M_moucir57::_pupawdu
	String_t* ____pupawdu_68;
	// System.Int32 GarbageiOS.M_moucir57::_teneecou
	int32_t ____teneecou_69;
	// System.Single GarbageiOS.M_moucir57::_maselTewegis
	float ____maselTewegis_70;
	// System.Boolean GarbageiOS.M_moucir57::_sarheku
	bool ____sarheku_71;
	// System.Boolean GarbageiOS.M_moucir57::_mecair
	bool ____mecair_72;

public:
	inline static int32_t get_offset_of__chedou_0() { return static_cast<int32_t>(offsetof(M_moucir57_t2477497995, ____chedou_0)); }
	inline String_t* get__chedou_0() const { return ____chedou_0; }
	inline String_t** get_address_of__chedou_0() { return &____chedou_0; }
	inline void set__chedou_0(String_t* value)
	{
		____chedou_0 = value;
		Il2CppCodeGenWriteBarrier(&____chedou_0, value);
	}

	inline static int32_t get_offset_of__vayreserePapis_1() { return static_cast<int32_t>(offsetof(M_moucir57_t2477497995, ____vayreserePapis_1)); }
	inline uint32_t get__vayreserePapis_1() const { return ____vayreserePapis_1; }
	inline uint32_t* get_address_of__vayreserePapis_1() { return &____vayreserePapis_1; }
	inline void set__vayreserePapis_1(uint32_t value)
	{
		____vayreserePapis_1 = value;
	}

	inline static int32_t get_offset_of__gallrelMorpay_2() { return static_cast<int32_t>(offsetof(M_moucir57_t2477497995, ____gallrelMorpay_2)); }
	inline uint32_t get__gallrelMorpay_2() const { return ____gallrelMorpay_2; }
	inline uint32_t* get_address_of__gallrelMorpay_2() { return &____gallrelMorpay_2; }
	inline void set__gallrelMorpay_2(uint32_t value)
	{
		____gallrelMorpay_2 = value;
	}

	inline static int32_t get_offset_of__fouwirrereTikearre_3() { return static_cast<int32_t>(offsetof(M_moucir57_t2477497995, ____fouwirrereTikearre_3)); }
	inline String_t* get__fouwirrereTikearre_3() const { return ____fouwirrereTikearre_3; }
	inline String_t** get_address_of__fouwirrereTikearre_3() { return &____fouwirrereTikearre_3; }
	inline void set__fouwirrereTikearre_3(String_t* value)
	{
		____fouwirrereTikearre_3 = value;
		Il2CppCodeGenWriteBarrier(&____fouwirrereTikearre_3, value);
	}

	inline static int32_t get_offset_of__koneMavurdrur_4() { return static_cast<int32_t>(offsetof(M_moucir57_t2477497995, ____koneMavurdrur_4)); }
	inline float get__koneMavurdrur_4() const { return ____koneMavurdrur_4; }
	inline float* get_address_of__koneMavurdrur_4() { return &____koneMavurdrur_4; }
	inline void set__koneMavurdrur_4(float value)
	{
		____koneMavurdrur_4 = value;
	}

	inline static int32_t get_offset_of__tete_5() { return static_cast<int32_t>(offsetof(M_moucir57_t2477497995, ____tete_5)); }
	inline float get__tete_5() const { return ____tete_5; }
	inline float* get_address_of__tete_5() { return &____tete_5; }
	inline void set__tete_5(float value)
	{
		____tete_5 = value;
	}

	inline static int32_t get_offset_of__lowloJuwhaijir_6() { return static_cast<int32_t>(offsetof(M_moucir57_t2477497995, ____lowloJuwhaijir_6)); }
	inline int32_t get__lowloJuwhaijir_6() const { return ____lowloJuwhaijir_6; }
	inline int32_t* get_address_of__lowloJuwhaijir_6() { return &____lowloJuwhaijir_6; }
	inline void set__lowloJuwhaijir_6(int32_t value)
	{
		____lowloJuwhaijir_6 = value;
	}

	inline static int32_t get_offset_of__deheTroubou_7() { return static_cast<int32_t>(offsetof(M_moucir57_t2477497995, ____deheTroubou_7)); }
	inline float get__deheTroubou_7() const { return ____deheTroubou_7; }
	inline float* get_address_of__deheTroubou_7() { return &____deheTroubou_7; }
	inline void set__deheTroubou_7(float value)
	{
		____deheTroubou_7 = value;
	}

	inline static int32_t get_offset_of__tawnair_8() { return static_cast<int32_t>(offsetof(M_moucir57_t2477497995, ____tawnair_8)); }
	inline String_t* get__tawnair_8() const { return ____tawnair_8; }
	inline String_t** get_address_of__tawnair_8() { return &____tawnair_8; }
	inline void set__tawnair_8(String_t* value)
	{
		____tawnair_8 = value;
		Il2CppCodeGenWriteBarrier(&____tawnair_8, value);
	}

	inline static int32_t get_offset_of__jajiSardoca_9() { return static_cast<int32_t>(offsetof(M_moucir57_t2477497995, ____jajiSardoca_9)); }
	inline float get__jajiSardoca_9() const { return ____jajiSardoca_9; }
	inline float* get_address_of__jajiSardoca_9() { return &____jajiSardoca_9; }
	inline void set__jajiSardoca_9(float value)
	{
		____jajiSardoca_9 = value;
	}

	inline static int32_t get_offset_of__telu_10() { return static_cast<int32_t>(offsetof(M_moucir57_t2477497995, ____telu_10)); }
	inline int32_t get__telu_10() const { return ____telu_10; }
	inline int32_t* get_address_of__telu_10() { return &____telu_10; }
	inline void set__telu_10(int32_t value)
	{
		____telu_10 = value;
	}

	inline static int32_t get_offset_of__geerear_11() { return static_cast<int32_t>(offsetof(M_moucir57_t2477497995, ____geerear_11)); }
	inline float get__geerear_11() const { return ____geerear_11; }
	inline float* get_address_of__geerear_11() { return &____geerear_11; }
	inline void set__geerear_11(float value)
	{
		____geerear_11 = value;
	}

	inline static int32_t get_offset_of__kowtebeRairsuwou_12() { return static_cast<int32_t>(offsetof(M_moucir57_t2477497995, ____kowtebeRairsuwou_12)); }
	inline bool get__kowtebeRairsuwou_12() const { return ____kowtebeRairsuwou_12; }
	inline bool* get_address_of__kowtebeRairsuwou_12() { return &____kowtebeRairsuwou_12; }
	inline void set__kowtebeRairsuwou_12(bool value)
	{
		____kowtebeRairsuwou_12 = value;
	}

	inline static int32_t get_offset_of__rursefou_13() { return static_cast<int32_t>(offsetof(M_moucir57_t2477497995, ____rursefou_13)); }
	inline bool get__rursefou_13() const { return ____rursefou_13; }
	inline bool* get_address_of__rursefou_13() { return &____rursefou_13; }
	inline void set__rursefou_13(bool value)
	{
		____rursefou_13 = value;
	}

	inline static int32_t get_offset_of__chujawTajall_14() { return static_cast<int32_t>(offsetof(M_moucir57_t2477497995, ____chujawTajall_14)); }
	inline uint32_t get__chujawTajall_14() const { return ____chujawTajall_14; }
	inline uint32_t* get_address_of__chujawTajall_14() { return &____chujawTajall_14; }
	inline void set__chujawTajall_14(uint32_t value)
	{
		____chujawTajall_14 = value;
	}

	inline static int32_t get_offset_of__toupalya_15() { return static_cast<int32_t>(offsetof(M_moucir57_t2477497995, ____toupalya_15)); }
	inline int32_t get__toupalya_15() const { return ____toupalya_15; }
	inline int32_t* get_address_of__toupalya_15() { return &____toupalya_15; }
	inline void set__toupalya_15(int32_t value)
	{
		____toupalya_15 = value;
	}

	inline static int32_t get_offset_of__stewemjo_16() { return static_cast<int32_t>(offsetof(M_moucir57_t2477497995, ____stewemjo_16)); }
	inline float get__stewemjo_16() const { return ____stewemjo_16; }
	inline float* get_address_of__stewemjo_16() { return &____stewemjo_16; }
	inline void set__stewemjo_16(float value)
	{
		____stewemjo_16 = value;
	}

	inline static int32_t get_offset_of__laija_17() { return static_cast<int32_t>(offsetof(M_moucir57_t2477497995, ____laija_17)); }
	inline int32_t get__laija_17() const { return ____laija_17; }
	inline int32_t* get_address_of__laija_17() { return &____laija_17; }
	inline void set__laija_17(int32_t value)
	{
		____laija_17 = value;
	}

	inline static int32_t get_offset_of__forceja_18() { return static_cast<int32_t>(offsetof(M_moucir57_t2477497995, ____forceja_18)); }
	inline bool get__forceja_18() const { return ____forceja_18; }
	inline bool* get_address_of__forceja_18() { return &____forceja_18; }
	inline void set__forceja_18(bool value)
	{
		____forceja_18 = value;
	}

	inline static int32_t get_offset_of__powdeecher_19() { return static_cast<int32_t>(offsetof(M_moucir57_t2477497995, ____powdeecher_19)); }
	inline String_t* get__powdeecher_19() const { return ____powdeecher_19; }
	inline String_t** get_address_of__powdeecher_19() { return &____powdeecher_19; }
	inline void set__powdeecher_19(String_t* value)
	{
		____powdeecher_19 = value;
		Il2CppCodeGenWriteBarrier(&____powdeecher_19, value);
	}

	inline static int32_t get_offset_of__geadouMouti_20() { return static_cast<int32_t>(offsetof(M_moucir57_t2477497995, ____geadouMouti_20)); }
	inline bool get__geadouMouti_20() const { return ____geadouMouti_20; }
	inline bool* get_address_of__geadouMouti_20() { return &____geadouMouti_20; }
	inline void set__geadouMouti_20(bool value)
	{
		____geadouMouti_20 = value;
	}

	inline static int32_t get_offset_of__jereyouso_21() { return static_cast<int32_t>(offsetof(M_moucir57_t2477497995, ____jereyouso_21)); }
	inline float get__jereyouso_21() const { return ____jereyouso_21; }
	inline float* get_address_of__jereyouso_21() { return &____jereyouso_21; }
	inline void set__jereyouso_21(float value)
	{
		____jereyouso_21 = value;
	}

	inline static int32_t get_offset_of__liju_22() { return static_cast<int32_t>(offsetof(M_moucir57_t2477497995, ____liju_22)); }
	inline uint32_t get__liju_22() const { return ____liju_22; }
	inline uint32_t* get_address_of__liju_22() { return &____liju_22; }
	inline void set__liju_22(uint32_t value)
	{
		____liju_22 = value;
	}

	inline static int32_t get_offset_of__jetor_23() { return static_cast<int32_t>(offsetof(M_moucir57_t2477497995, ____jetor_23)); }
	inline int32_t get__jetor_23() const { return ____jetor_23; }
	inline int32_t* get_address_of__jetor_23() { return &____jetor_23; }
	inline void set__jetor_23(int32_t value)
	{
		____jetor_23 = value;
	}

	inline static int32_t get_offset_of__keldis_24() { return static_cast<int32_t>(offsetof(M_moucir57_t2477497995, ____keldis_24)); }
	inline int32_t get__keldis_24() const { return ____keldis_24; }
	inline int32_t* get_address_of__keldis_24() { return &____keldis_24; }
	inline void set__keldis_24(int32_t value)
	{
		____keldis_24 = value;
	}

	inline static int32_t get_offset_of__cadraForrir_25() { return static_cast<int32_t>(offsetof(M_moucir57_t2477497995, ____cadraForrir_25)); }
	inline uint32_t get__cadraForrir_25() const { return ____cadraForrir_25; }
	inline uint32_t* get_address_of__cadraForrir_25() { return &____cadraForrir_25; }
	inline void set__cadraForrir_25(uint32_t value)
	{
		____cadraForrir_25 = value;
	}

	inline static int32_t get_offset_of__jallker_26() { return static_cast<int32_t>(offsetof(M_moucir57_t2477497995, ____jallker_26)); }
	inline String_t* get__jallker_26() const { return ____jallker_26; }
	inline String_t** get_address_of__jallker_26() { return &____jallker_26; }
	inline void set__jallker_26(String_t* value)
	{
		____jallker_26 = value;
		Il2CppCodeGenWriteBarrier(&____jallker_26, value);
	}

	inline static int32_t get_offset_of__lereseKairnersow_27() { return static_cast<int32_t>(offsetof(M_moucir57_t2477497995, ____lereseKairnersow_27)); }
	inline bool get__lereseKairnersow_27() const { return ____lereseKairnersow_27; }
	inline bool* get_address_of__lereseKairnersow_27() { return &____lereseKairnersow_27; }
	inline void set__lereseKairnersow_27(bool value)
	{
		____lereseKairnersow_27 = value;
	}

	inline static int32_t get_offset_of__loujurBaceare_28() { return static_cast<int32_t>(offsetof(M_moucir57_t2477497995, ____loujurBaceare_28)); }
	inline uint32_t get__loujurBaceare_28() const { return ____loujurBaceare_28; }
	inline uint32_t* get_address_of__loujurBaceare_28() { return &____loujurBaceare_28; }
	inline void set__loujurBaceare_28(uint32_t value)
	{
		____loujurBaceare_28 = value;
	}

	inline static int32_t get_offset_of__racere_29() { return static_cast<int32_t>(offsetof(M_moucir57_t2477497995, ____racere_29)); }
	inline int32_t get__racere_29() const { return ____racere_29; }
	inline int32_t* get_address_of__racere_29() { return &____racere_29; }
	inline void set__racere_29(int32_t value)
	{
		____racere_29 = value;
	}

	inline static int32_t get_offset_of__dasisaiKeja_30() { return static_cast<int32_t>(offsetof(M_moucir57_t2477497995, ____dasisaiKeja_30)); }
	inline int32_t get__dasisaiKeja_30() const { return ____dasisaiKeja_30; }
	inline int32_t* get_address_of__dasisaiKeja_30() { return &____dasisaiKeja_30; }
	inline void set__dasisaiKeja_30(int32_t value)
	{
		____dasisaiKeja_30 = value;
	}

	inline static int32_t get_offset_of__hapitel_31() { return static_cast<int32_t>(offsetof(M_moucir57_t2477497995, ____hapitel_31)); }
	inline bool get__hapitel_31() const { return ____hapitel_31; }
	inline bool* get_address_of__hapitel_31() { return &____hapitel_31; }
	inline void set__hapitel_31(bool value)
	{
		____hapitel_31 = value;
	}

	inline static int32_t get_offset_of__yisjayMejador_32() { return static_cast<int32_t>(offsetof(M_moucir57_t2477497995, ____yisjayMejador_32)); }
	inline String_t* get__yisjayMejador_32() const { return ____yisjayMejador_32; }
	inline String_t** get_address_of__yisjayMejador_32() { return &____yisjayMejador_32; }
	inline void set__yisjayMejador_32(String_t* value)
	{
		____yisjayMejador_32 = value;
		Il2CppCodeGenWriteBarrier(&____yisjayMejador_32, value);
	}

	inline static int32_t get_offset_of__mokurlou_33() { return static_cast<int32_t>(offsetof(M_moucir57_t2477497995, ____mokurlou_33)); }
	inline bool get__mokurlou_33() const { return ____mokurlou_33; }
	inline bool* get_address_of__mokurlou_33() { return &____mokurlou_33; }
	inline void set__mokurlou_33(bool value)
	{
		____mokurlou_33 = value;
	}

	inline static int32_t get_offset_of__nabirDerebase_34() { return static_cast<int32_t>(offsetof(M_moucir57_t2477497995, ____nabirDerebase_34)); }
	inline String_t* get__nabirDerebase_34() const { return ____nabirDerebase_34; }
	inline String_t** get_address_of__nabirDerebase_34() { return &____nabirDerebase_34; }
	inline void set__nabirDerebase_34(String_t* value)
	{
		____nabirDerebase_34 = value;
		Il2CppCodeGenWriteBarrier(&____nabirDerebase_34, value);
	}

	inline static int32_t get_offset_of__bissou_35() { return static_cast<int32_t>(offsetof(M_moucir57_t2477497995, ____bissou_35)); }
	inline int32_t get__bissou_35() const { return ____bissou_35; }
	inline int32_t* get_address_of__bissou_35() { return &____bissou_35; }
	inline void set__bissou_35(int32_t value)
	{
		____bissou_35 = value;
	}

	inline static int32_t get_offset_of__boutaisereRitrerwhir_36() { return static_cast<int32_t>(offsetof(M_moucir57_t2477497995, ____boutaisereRitrerwhir_36)); }
	inline String_t* get__boutaisereRitrerwhir_36() const { return ____boutaisereRitrerwhir_36; }
	inline String_t** get_address_of__boutaisereRitrerwhir_36() { return &____boutaisereRitrerwhir_36; }
	inline void set__boutaisereRitrerwhir_36(String_t* value)
	{
		____boutaisereRitrerwhir_36 = value;
		Il2CppCodeGenWriteBarrier(&____boutaisereRitrerwhir_36, value);
	}

	inline static int32_t get_offset_of__carwi_37() { return static_cast<int32_t>(offsetof(M_moucir57_t2477497995, ____carwi_37)); }
	inline float get__carwi_37() const { return ____carwi_37; }
	inline float* get_address_of__carwi_37() { return &____carwi_37; }
	inline void set__carwi_37(float value)
	{
		____carwi_37 = value;
	}

	inline static int32_t get_offset_of__risecou_38() { return static_cast<int32_t>(offsetof(M_moucir57_t2477497995, ____risecou_38)); }
	inline float get__risecou_38() const { return ____risecou_38; }
	inline float* get_address_of__risecou_38() { return &____risecou_38; }
	inline void set__risecou_38(float value)
	{
		____risecou_38 = value;
	}

	inline static int32_t get_offset_of__faydikaWhayfu_39() { return static_cast<int32_t>(offsetof(M_moucir57_t2477497995, ____faydikaWhayfu_39)); }
	inline String_t* get__faydikaWhayfu_39() const { return ____faydikaWhayfu_39; }
	inline String_t** get_address_of__faydikaWhayfu_39() { return &____faydikaWhayfu_39; }
	inline void set__faydikaWhayfu_39(String_t* value)
	{
		____faydikaWhayfu_39 = value;
		Il2CppCodeGenWriteBarrier(&____faydikaWhayfu_39, value);
	}

	inline static int32_t get_offset_of__rainartaRousairba_40() { return static_cast<int32_t>(offsetof(M_moucir57_t2477497995, ____rainartaRousairba_40)); }
	inline bool get__rainartaRousairba_40() const { return ____rainartaRousairba_40; }
	inline bool* get_address_of__rainartaRousairba_40() { return &____rainartaRousairba_40; }
	inline void set__rainartaRousairba_40(bool value)
	{
		____rainartaRousairba_40 = value;
	}

	inline static int32_t get_offset_of__neteeji_41() { return static_cast<int32_t>(offsetof(M_moucir57_t2477497995, ____neteeji_41)); }
	inline bool get__neteeji_41() const { return ____neteeji_41; }
	inline bool* get_address_of__neteeji_41() { return &____neteeji_41; }
	inline void set__neteeji_41(bool value)
	{
		____neteeji_41 = value;
	}

	inline static int32_t get_offset_of__moutehelBomeeto_42() { return static_cast<int32_t>(offsetof(M_moucir57_t2477497995, ____moutehelBomeeto_42)); }
	inline uint32_t get__moutehelBomeeto_42() const { return ____moutehelBomeeto_42; }
	inline uint32_t* get_address_of__moutehelBomeeto_42() { return &____moutehelBomeeto_42; }
	inline void set__moutehelBomeeto_42(uint32_t value)
	{
		____moutehelBomeeto_42 = value;
	}

	inline static int32_t get_offset_of__talairdereRelcairdor_43() { return static_cast<int32_t>(offsetof(M_moucir57_t2477497995, ____talairdereRelcairdor_43)); }
	inline uint32_t get__talairdereRelcairdor_43() const { return ____talairdereRelcairdor_43; }
	inline uint32_t* get_address_of__talairdereRelcairdor_43() { return &____talairdereRelcairdor_43; }
	inline void set__talairdereRelcairdor_43(uint32_t value)
	{
		____talairdereRelcairdor_43 = value;
	}

	inline static int32_t get_offset_of__wayherebur_44() { return static_cast<int32_t>(offsetof(M_moucir57_t2477497995, ____wayherebur_44)); }
	inline float get__wayherebur_44() const { return ____wayherebur_44; }
	inline float* get_address_of__wayherebur_44() { return &____wayherebur_44; }
	inline void set__wayherebur_44(float value)
	{
		____wayherebur_44 = value;
	}

	inline static int32_t get_offset_of__souvar_45() { return static_cast<int32_t>(offsetof(M_moucir57_t2477497995, ____souvar_45)); }
	inline float get__souvar_45() const { return ____souvar_45; }
	inline float* get_address_of__souvar_45() { return &____souvar_45; }
	inline void set__souvar_45(float value)
	{
		____souvar_45 = value;
	}

	inline static int32_t get_offset_of__pawee_46() { return static_cast<int32_t>(offsetof(M_moucir57_t2477497995, ____pawee_46)); }
	inline int32_t get__pawee_46() const { return ____pawee_46; }
	inline int32_t* get_address_of__pawee_46() { return &____pawee_46; }
	inline void set__pawee_46(int32_t value)
	{
		____pawee_46 = value;
	}

	inline static int32_t get_offset_of__raypotroJegucis_47() { return static_cast<int32_t>(offsetof(M_moucir57_t2477497995, ____raypotroJegucis_47)); }
	inline uint32_t get__raypotroJegucis_47() const { return ____raypotroJegucis_47; }
	inline uint32_t* get_address_of__raypotroJegucis_47() { return &____raypotroJegucis_47; }
	inline void set__raypotroJegucis_47(uint32_t value)
	{
		____raypotroJegucis_47 = value;
	}

	inline static int32_t get_offset_of__raygaqarRerbarla_48() { return static_cast<int32_t>(offsetof(M_moucir57_t2477497995, ____raygaqarRerbarla_48)); }
	inline float get__raygaqarRerbarla_48() const { return ____raygaqarRerbarla_48; }
	inline float* get_address_of__raygaqarRerbarla_48() { return &____raygaqarRerbarla_48; }
	inline void set__raygaqarRerbarla_48(float value)
	{
		____raygaqarRerbarla_48 = value;
	}

	inline static int32_t get_offset_of__nochuco_49() { return static_cast<int32_t>(offsetof(M_moucir57_t2477497995, ____nochuco_49)); }
	inline bool get__nochuco_49() const { return ____nochuco_49; }
	inline bool* get_address_of__nochuco_49() { return &____nochuco_49; }
	inline void set__nochuco_49(bool value)
	{
		____nochuco_49 = value;
	}

	inline static int32_t get_offset_of__nemfarKomawchar_50() { return static_cast<int32_t>(offsetof(M_moucir57_t2477497995, ____nemfarKomawchar_50)); }
	inline int32_t get__nemfarKomawchar_50() const { return ____nemfarKomawchar_50; }
	inline int32_t* get_address_of__nemfarKomawchar_50() { return &____nemfarKomawchar_50; }
	inline void set__nemfarKomawchar_50(int32_t value)
	{
		____nemfarKomawchar_50 = value;
	}

	inline static int32_t get_offset_of__fosa_51() { return static_cast<int32_t>(offsetof(M_moucir57_t2477497995, ____fosa_51)); }
	inline uint32_t get__fosa_51() const { return ____fosa_51; }
	inline uint32_t* get_address_of__fosa_51() { return &____fosa_51; }
	inline void set__fosa_51(uint32_t value)
	{
		____fosa_51 = value;
	}

	inline static int32_t get_offset_of__lasre_52() { return static_cast<int32_t>(offsetof(M_moucir57_t2477497995, ____lasre_52)); }
	inline bool get__lasre_52() const { return ____lasre_52; }
	inline bool* get_address_of__lasre_52() { return &____lasre_52; }
	inline void set__lasre_52(bool value)
	{
		____lasre_52 = value;
	}

	inline static int32_t get_offset_of__roseeZallpearlaw_53() { return static_cast<int32_t>(offsetof(M_moucir57_t2477497995, ____roseeZallpearlaw_53)); }
	inline float get__roseeZallpearlaw_53() const { return ____roseeZallpearlaw_53; }
	inline float* get_address_of__roseeZallpearlaw_53() { return &____roseeZallpearlaw_53; }
	inline void set__roseeZallpearlaw_53(float value)
	{
		____roseeZallpearlaw_53 = value;
	}

	inline static int32_t get_offset_of__tusici_54() { return static_cast<int32_t>(offsetof(M_moucir57_t2477497995, ____tusici_54)); }
	inline String_t* get__tusici_54() const { return ____tusici_54; }
	inline String_t** get_address_of__tusici_54() { return &____tusici_54; }
	inline void set__tusici_54(String_t* value)
	{
		____tusici_54 = value;
		Il2CppCodeGenWriteBarrier(&____tusici_54, value);
	}

	inline static int32_t get_offset_of__rolall_55() { return static_cast<int32_t>(offsetof(M_moucir57_t2477497995, ____rolall_55)); }
	inline String_t* get__rolall_55() const { return ____rolall_55; }
	inline String_t** get_address_of__rolall_55() { return &____rolall_55; }
	inline void set__rolall_55(String_t* value)
	{
		____rolall_55 = value;
		Il2CppCodeGenWriteBarrier(&____rolall_55, value);
	}

	inline static int32_t get_offset_of__neasairno_56() { return static_cast<int32_t>(offsetof(M_moucir57_t2477497995, ____neasairno_56)); }
	inline uint32_t get__neasairno_56() const { return ____neasairno_56; }
	inline uint32_t* get_address_of__neasairno_56() { return &____neasairno_56; }
	inline void set__neasairno_56(uint32_t value)
	{
		____neasairno_56 = value;
	}

	inline static int32_t get_offset_of__sowhewherHorsou_57() { return static_cast<int32_t>(offsetof(M_moucir57_t2477497995, ____sowhewherHorsou_57)); }
	inline String_t* get__sowhewherHorsou_57() const { return ____sowhewherHorsou_57; }
	inline String_t** get_address_of__sowhewherHorsou_57() { return &____sowhewherHorsou_57; }
	inline void set__sowhewherHorsou_57(String_t* value)
	{
		____sowhewherHorsou_57 = value;
		Il2CppCodeGenWriteBarrier(&____sowhewherHorsou_57, value);
	}

	inline static int32_t get_offset_of__meepama_58() { return static_cast<int32_t>(offsetof(M_moucir57_t2477497995, ____meepama_58)); }
	inline String_t* get__meepama_58() const { return ____meepama_58; }
	inline String_t** get_address_of__meepama_58() { return &____meepama_58; }
	inline void set__meepama_58(String_t* value)
	{
		____meepama_58 = value;
		Il2CppCodeGenWriteBarrier(&____meepama_58, value);
	}

	inline static int32_t get_offset_of__pouseaHigere_59() { return static_cast<int32_t>(offsetof(M_moucir57_t2477497995, ____pouseaHigere_59)); }
	inline String_t* get__pouseaHigere_59() const { return ____pouseaHigere_59; }
	inline String_t** get_address_of__pouseaHigere_59() { return &____pouseaHigere_59; }
	inline void set__pouseaHigere_59(String_t* value)
	{
		____pouseaHigere_59 = value;
		Il2CppCodeGenWriteBarrier(&____pouseaHigere_59, value);
	}

	inline static int32_t get_offset_of__pemastemVorlo_60() { return static_cast<int32_t>(offsetof(M_moucir57_t2477497995, ____pemastemVorlo_60)); }
	inline int32_t get__pemastemVorlo_60() const { return ____pemastemVorlo_60; }
	inline int32_t* get_address_of__pemastemVorlo_60() { return &____pemastemVorlo_60; }
	inline void set__pemastemVorlo_60(int32_t value)
	{
		____pemastemVorlo_60 = value;
	}

	inline static int32_t get_offset_of__meawhemirSega_61() { return static_cast<int32_t>(offsetof(M_moucir57_t2477497995, ____meawhemirSega_61)); }
	inline float get__meawhemirSega_61() const { return ____meawhemirSega_61; }
	inline float* get_address_of__meawhemirSega_61() { return &____meawhemirSega_61; }
	inline void set__meawhemirSega_61(float value)
	{
		____meawhemirSega_61 = value;
	}

	inline static int32_t get_offset_of__choule_62() { return static_cast<int32_t>(offsetof(M_moucir57_t2477497995, ____choule_62)); }
	inline uint32_t get__choule_62() const { return ____choule_62; }
	inline uint32_t* get_address_of__choule_62() { return &____choule_62; }
	inline void set__choule_62(uint32_t value)
	{
		____choule_62 = value;
	}

	inline static int32_t get_offset_of__sisnarkairBuseavem_63() { return static_cast<int32_t>(offsetof(M_moucir57_t2477497995, ____sisnarkairBuseavem_63)); }
	inline uint32_t get__sisnarkairBuseavem_63() const { return ____sisnarkairBuseavem_63; }
	inline uint32_t* get_address_of__sisnarkairBuseavem_63() { return &____sisnarkairBuseavem_63; }
	inline void set__sisnarkairBuseavem_63(uint32_t value)
	{
		____sisnarkairBuseavem_63 = value;
	}

	inline static int32_t get_offset_of__tisoguKamooji_64() { return static_cast<int32_t>(offsetof(M_moucir57_t2477497995, ____tisoguKamooji_64)); }
	inline int32_t get__tisoguKamooji_64() const { return ____tisoguKamooji_64; }
	inline int32_t* get_address_of__tisoguKamooji_64() { return &____tisoguKamooji_64; }
	inline void set__tisoguKamooji_64(int32_t value)
	{
		____tisoguKamooji_64 = value;
	}

	inline static int32_t get_offset_of__nazeDrasfas_65() { return static_cast<int32_t>(offsetof(M_moucir57_t2477497995, ____nazeDrasfas_65)); }
	inline uint32_t get__nazeDrasfas_65() const { return ____nazeDrasfas_65; }
	inline uint32_t* get_address_of__nazeDrasfas_65() { return &____nazeDrasfas_65; }
	inline void set__nazeDrasfas_65(uint32_t value)
	{
		____nazeDrasfas_65 = value;
	}

	inline static int32_t get_offset_of__tropairReremoba_66() { return static_cast<int32_t>(offsetof(M_moucir57_t2477497995, ____tropairReremoba_66)); }
	inline bool get__tropairReremoba_66() const { return ____tropairReremoba_66; }
	inline bool* get_address_of__tropairReremoba_66() { return &____tropairReremoba_66; }
	inline void set__tropairReremoba_66(bool value)
	{
		____tropairReremoba_66 = value;
	}

	inline static int32_t get_offset_of__liverFenal_67() { return static_cast<int32_t>(offsetof(M_moucir57_t2477497995, ____liverFenal_67)); }
	inline bool get__liverFenal_67() const { return ____liverFenal_67; }
	inline bool* get_address_of__liverFenal_67() { return &____liverFenal_67; }
	inline void set__liverFenal_67(bool value)
	{
		____liverFenal_67 = value;
	}

	inline static int32_t get_offset_of__pupawdu_68() { return static_cast<int32_t>(offsetof(M_moucir57_t2477497995, ____pupawdu_68)); }
	inline String_t* get__pupawdu_68() const { return ____pupawdu_68; }
	inline String_t** get_address_of__pupawdu_68() { return &____pupawdu_68; }
	inline void set__pupawdu_68(String_t* value)
	{
		____pupawdu_68 = value;
		Il2CppCodeGenWriteBarrier(&____pupawdu_68, value);
	}

	inline static int32_t get_offset_of__teneecou_69() { return static_cast<int32_t>(offsetof(M_moucir57_t2477497995, ____teneecou_69)); }
	inline int32_t get__teneecou_69() const { return ____teneecou_69; }
	inline int32_t* get_address_of__teneecou_69() { return &____teneecou_69; }
	inline void set__teneecou_69(int32_t value)
	{
		____teneecou_69 = value;
	}

	inline static int32_t get_offset_of__maselTewegis_70() { return static_cast<int32_t>(offsetof(M_moucir57_t2477497995, ____maselTewegis_70)); }
	inline float get__maselTewegis_70() const { return ____maselTewegis_70; }
	inline float* get_address_of__maselTewegis_70() { return &____maselTewegis_70; }
	inline void set__maselTewegis_70(float value)
	{
		____maselTewegis_70 = value;
	}

	inline static int32_t get_offset_of__sarheku_71() { return static_cast<int32_t>(offsetof(M_moucir57_t2477497995, ____sarheku_71)); }
	inline bool get__sarheku_71() const { return ____sarheku_71; }
	inline bool* get_address_of__sarheku_71() { return &____sarheku_71; }
	inline void set__sarheku_71(bool value)
	{
		____sarheku_71 = value;
	}

	inline static int32_t get_offset_of__mecair_72() { return static_cast<int32_t>(offsetof(M_moucir57_t2477497995, ____mecair_72)); }
	inline bool get__mecair_72() const { return ____mecair_72; }
	inline bool* get_address_of__mecair_72() { return &____mecair_72; }
	inline void set__mecair_72(bool value)
	{
		____mecair_72 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
