﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Entity.Behavior.FollowBvr
struct FollowBvr_t765338571;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// Entity.Behavior.IBehavior
struct IBehavior_t770859129;
// CombatEntity
struct CombatEntity_t684137495;
// Entity.Behavior.IBehaviorCtrl
struct IBehaviorCtrl_t4225040900;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Entity_Behavior_EBehaviorID1238533136.h"
#include "AssemblyU2DCSharp_Entity_Behavior_IBehavior770859129.h"
#include "AssemblyU2DCSharp_CombatEntity684137495.h"
#include "AssemblyU2DCSharp_Entity_Behavior_IBehaviorCtrl4225040900.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void Entity.Behavior.FollowBvr::.ctor()
extern "C"  void FollowBvr__ctor_m2933105807 (FollowBvr_t765338571 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Entity.Behavior.EBehaviorID Entity.Behavior.FollowBvr::get_id()
extern "C"  uint8_t FollowBvr_get_id_m1323940103 (FollowBvr_t765338571 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.FollowBvr::Reason()
extern "C"  void FollowBvr_Reason_m283354905 (FollowBvr_t765338571 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.FollowBvr::Action()
extern "C"  void FollowBvr_Action_m3775028875 (FollowBvr_t765338571 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.FollowBvr::SetParams(System.Object[])
extern "C"  void FollowBvr_SetParams_m360579293 (FollowBvr_t765338571 * __this, ObjectU5BU5D_t1108656482* ___args0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.FollowBvr::DoEntering()
extern "C"  void FollowBvr_DoEntering_m2403314250 (FollowBvr_t765338571 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.FollowBvr::UpdatePos()
extern "C"  void FollowBvr_UpdatePos_m343485880 (FollowBvr_t765338571 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.FollowBvr::DoLeaving()
extern "C"  void FollowBvr_DoLeaving_m660983830 (FollowBvr_t765338571 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.FollowBvr::Pause()
extern "C"  void FollowBvr_Pause_m2987231779 (FollowBvr_t765338571 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.FollowBvr::Play()
extern "C"  void FollowBvr_Play_m4262349065 (FollowBvr_t765338571 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Entity.Behavior.FollowBvr::CanTran2OtherBehavior(Entity.Behavior.IBehavior)
extern "C"  bool FollowBvr_CanTran2OtherBehavior_m3509255932 (FollowBvr_t765338571 * __this, IBehavior_t770859129 * ___behavior0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CombatEntity Entity.Behavior.FollowBvr::ilo_get_entity1(Entity.Behavior.IBehavior)
extern "C"  CombatEntity_t684137495 * FollowBvr_ilo_get_entity1_m1522501891 (Il2CppObject * __this /* static, unused */, IBehavior_t770859129 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CombatEntity Entity.Behavior.FollowBvr::ilo_get_atkTarget2(CombatEntity)
extern "C"  CombatEntity_t684137495 * FollowBvr_ilo_get_atkTarget2_m3034908541 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.FollowBvr::ilo_TranBehavior3(Entity.Behavior.IBehaviorCtrl,Entity.Behavior.EBehaviorID,System.Object[])
extern "C"  void FollowBvr_ilo_TranBehavior3_m1157271300 (Il2CppObject * __this /* static, unused */, IBehaviorCtrl_t4225040900 * ____this0, uint8_t ___id1, ObjectU5BU5D_t1108656482* ___arg2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Entity.Behavior.FollowBvr::ilo_get_position4(CombatEntity)
extern "C"  Vector3_t4282066566  FollowBvr_ilo_get_position4_m1243228799 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Entity.Behavior.FollowBvr::ilo_get_halfwidth5(CombatEntity)
extern "C"  float FollowBvr_ilo_get_halfwidth5_m2171328080 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Entity.Behavior.IBehaviorCtrl Entity.Behavior.FollowBvr::ilo_get_bvrCtrl6(CombatEntity)
extern "C"  IBehaviorCtrl_t4225040900 * FollowBvr_ilo_get_bvrCtrl6_m3769588809 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Entity.Behavior.FollowBvr::ilo_get_isDeath7(CombatEntity)
extern "C"  bool FollowBvr_ilo_get_isDeath7_m998252563 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.FollowBvr::ilo_Play8(Entity.Behavior.IBehavior)
extern "C"  void FollowBvr_ilo_Play8_m2554886538 (Il2CppObject * __this /* static, unused */, IBehavior_t770859129 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
