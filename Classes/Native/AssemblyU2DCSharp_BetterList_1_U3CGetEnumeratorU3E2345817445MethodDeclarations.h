﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BetterList`1/<GetEnumerator>c__Iterator29<UnityEngine.Color32>
struct U3CGetEnumeratorU3Ec__Iterator29_t2345817445;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Color32598853688.h"

// System.Void BetterList`1/<GetEnumerator>c__Iterator29<UnityEngine.Color32>::.ctor()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator29__ctor_m2564299892_gshared (U3CGetEnumeratorU3Ec__Iterator29_t2345817445 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator29__ctor_m2564299892(__this, method) ((  void (*) (U3CGetEnumeratorU3Ec__Iterator29_t2345817445 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator29__ctor_m2564299892_gshared)(__this, method)
// T BetterList`1/<GetEnumerator>c__Iterator29<UnityEngine.Color32>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  Color32_t598853688  U3CGetEnumeratorU3Ec__Iterator29_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m4271281309_gshared (U3CGetEnumeratorU3Ec__Iterator29_t2345817445 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator29_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m4271281309(__this, method) ((  Color32_t598853688  (*) (U3CGetEnumeratorU3Ec__Iterator29_t2345817445 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator29_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m4271281309_gshared)(__this, method)
// System.Object BetterList`1/<GetEnumerator>c__Iterator29<UnityEngine.Color32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator29_System_Collections_IEnumerator_get_Current_m731911868_gshared (U3CGetEnumeratorU3Ec__Iterator29_t2345817445 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator29_System_Collections_IEnumerator_get_Current_m731911868(__this, method) ((  Il2CppObject * (*) (U3CGetEnumeratorU3Ec__Iterator29_t2345817445 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator29_System_Collections_IEnumerator_get_Current_m731911868_gshared)(__this, method)
// System.Boolean BetterList`1/<GetEnumerator>c__Iterator29<UnityEngine.Color32>::MoveNext()
extern "C"  bool U3CGetEnumeratorU3Ec__Iterator29_MoveNext_m1320047080_gshared (U3CGetEnumeratorU3Ec__Iterator29_t2345817445 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator29_MoveNext_m1320047080(__this, method) ((  bool (*) (U3CGetEnumeratorU3Ec__Iterator29_t2345817445 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator29_MoveNext_m1320047080_gshared)(__this, method)
// System.Void BetterList`1/<GetEnumerator>c__Iterator29<UnityEngine.Color32>::Dispose()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator29_Dispose_m3174218673_gshared (U3CGetEnumeratorU3Ec__Iterator29_t2345817445 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator29_Dispose_m3174218673(__this, method) ((  void (*) (U3CGetEnumeratorU3Ec__Iterator29_t2345817445 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator29_Dispose_m3174218673_gshared)(__this, method)
// System.Void BetterList`1/<GetEnumerator>c__Iterator29<UnityEngine.Color32>::Reset()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator29_Reset_m210732833_gshared (U3CGetEnumeratorU3Ec__Iterator29_t2345817445 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator29_Reset_m210732833(__this, method) ((  void (*) (U3CGetEnumeratorU3Ec__Iterator29_t2345817445 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator29_Reset_m210732833_gshared)(__this, method)
