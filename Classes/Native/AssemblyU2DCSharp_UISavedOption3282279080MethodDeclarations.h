﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UISavedOption
struct UISavedOption_t3282279080;
// System.String
struct String_t;
// EventDelegate
struct EventDelegate_t4004424223;
// System.Collections.Generic.List`1<EventDelegate>
struct List_1_t1077642479;
// EventDelegate/Callback
struct Callback_t1094463061;
// UIToggle
struct UIToggle_t688812808;
// UIPopupList
struct UIPopupList_t1804933942;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_UISavedOption3282279080.h"
#include "AssemblyU2DCSharp_EventDelegate_Callback1094463061.h"
#include "AssemblyU2DCSharp_UIToggle688812808.h"
#include "AssemblyU2DCSharp_UIPopupList1804933942.h"

// System.Void UISavedOption::.ctor()
extern "C"  void UISavedOption__ctor_m146162627 (UISavedOption_t3282279080 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UISavedOption::get_key()
extern "C"  String_t* UISavedOption_get_key_m3640815258 (UISavedOption_t3282279080 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UISavedOption::Awake()
extern "C"  void UISavedOption_Awake_m383767846 (UISavedOption_t3282279080 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UISavedOption::OnEnable()
extern "C"  void UISavedOption_OnEnable_m1305561315 (UISavedOption_t3282279080 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UISavedOption::OnDisable()
extern "C"  void UISavedOption_OnDisable_m2258632362 (UISavedOption_t3282279080 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UISavedOption::SaveSelection()
extern "C"  void UISavedOption_SaveSelection_m3447262640 (UISavedOption_t3282279080 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UISavedOption::SaveState()
extern "C"  void UISavedOption_SaveState_m1273707765 (UISavedOption_t3282279080 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UISavedOption::SaveProgress()
extern "C"  void UISavedOption_SaveProgress_m3604141419 (UISavedOption_t3282279080 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UISavedOption::ilo_get_key1(UISavedOption)
extern "C"  String_t* UISavedOption_ilo_get_key1_m762225950 (Il2CppObject * __this /* static, unused */, UISavedOption_t3282279080 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// EventDelegate UISavedOption::ilo_Add2(System.Collections.Generic.List`1<EventDelegate>,EventDelegate/Callback)
extern "C"  EventDelegate_t4004424223 * UISavedOption_ilo_Add2_m3614104125 (Il2CppObject * __this /* static, unused */, List_1_t1077642479 * ___list0, Callback_t1094463061 * ___callback1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UISavedOption::ilo_set_value3(UIToggle,System.Boolean)
extern "C"  void UISavedOption_ilo_set_value3_m3425181138 (Il2CppObject * __this /* static, unused */, UIToggle_t688812808 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UISavedOption::ilo_Remove4(System.Collections.Generic.List`1<EventDelegate>,EventDelegate/Callback)
extern "C"  bool UISavedOption_ilo_Remove4_m3165447836 (Il2CppObject * __this /* static, unused */, List_1_t1077642479 * ___list0, Callback_t1094463061 * ___callback1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UISavedOption::ilo_get_value5(UIPopupList)
extern "C"  String_t* UISavedOption_ilo_get_value5_m2323815902 (Il2CppObject * __this /* static, unused */, UIPopupList_t1804933942 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
