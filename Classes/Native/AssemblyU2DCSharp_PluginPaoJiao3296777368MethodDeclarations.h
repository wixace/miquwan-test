﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PluginPaoJiao
struct PluginPaoJiao_t3296777368;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// System.String
struct String_t;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// PluginsSdkMgr
struct PluginsSdkMgr_t3884624670;
// ProductsCfgMgr
struct ProductsCfgMgr_t2493714872;
// Mihua.SDK.CreatRoleInfo
struct CreatRoleInfo_t2858846543;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// Mihua.SDK.LvUpInfo
struct LvUpInfo_t411687177;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "AssemblyU2DCSharp_PluginPaoJiao3296777368.h"
#include "AssemblyU2DCSharp_PluginsSdkMgr3884624670.h"
#include "AssemblyU2DCSharp_ProductsCfgMgr2493714872.h"

// System.Void PluginPaoJiao::.ctor()
extern "C"  void PluginPaoJiao__ctor_m260257747 (PluginPaoJiao_t3296777368 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginPaoJiao::Init()
extern "C"  void PluginPaoJiao_Init_m929763361 (PluginPaoJiao_t3296777368 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginPaoJiao::ReqSDKHttpLogin()
extern "C"  Dictionary_2_t827649927 * PluginPaoJiao_ReqSDKHttpLogin_m2734171652 (PluginPaoJiao_t3296777368 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginPaoJiao::IsLoginSuccess(System.Boolean)
extern "C"  bool PluginPaoJiao_IsLoginSuccess_m3287867192 (PluginPaoJiao_t3296777368 * __this, bool ___isOpenUseLogin0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginPaoJiao::OpenUserLogin()
extern "C"  void PluginPaoJiao_OpenUserLogin_m4269594405 (PluginPaoJiao_t3296777368 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginPaoJiao::OnLoginSuccess(System.String)
extern "C"  void PluginPaoJiao_OnLoginSuccess_m3064938136 (PluginPaoJiao_t3296777368 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginPaoJiao::OnLogoutSuccess(System.String)
extern "C"  void PluginPaoJiao_OnLogoutSuccess_m527344375 (PluginPaoJiao_t3296777368 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginPaoJiao::OnSDKLogout(System.String)
extern "C"  void PluginPaoJiao_OnSDKLogout_m2763689932 (PluginPaoJiao_t3296777368 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginPaoJiao::OnGameSwitchSuccess(System.String)
extern "C"  void PluginPaoJiao_OnGameSwitchSuccess_m3546139315 (PluginPaoJiao_t3296777368 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginPaoJiao::UserPay(CEvent.ZEvent)
extern "C"  void PluginPaoJiao_UserPay_m4087503021 (PluginPaoJiao_t3296777368 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginPaoJiao::OnPaySuccess(System.String)
extern "C"  void PluginPaoJiao_OnPaySuccess_m755263191 (PluginPaoJiao_t3296777368 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginPaoJiao::OnPayFail(System.String)
extern "C"  void PluginPaoJiao_OnPayFail_m2945143242 (PluginPaoJiao_t3296777368 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginPaoJiao::OnEnterGame(CEvent.ZEvent)
extern "C"  void PluginPaoJiao_OnEnterGame_m3890066431 (PluginPaoJiao_t3296777368 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginPaoJiao::OnCreatRole(CEvent.ZEvent)
extern "C"  void PluginPaoJiao_OnCreatRole_m424514986 (PluginPaoJiao_t3296777368 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginPaoJiao::OnLevelUp(CEvent.ZEvent)
extern "C"  void PluginPaoJiao_OnLevelUp_m936533194 (PluginPaoJiao_t3296777368 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginPaoJiao::ClollectData(System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginPaoJiao_ClollectData_m1141852087 (PluginPaoJiao_t3296777368 * __this, String_t* ___type0, String_t* ___serverid1, String_t* ___servername2, String_t* ___roleid3, String_t* ___rolename4, String_t* ___rolelevel5, String_t* ___roleGold6, String_t* ___VIPLv7, String_t* ___GameName8, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginPaoJiao::init(System.String,System.String,System.String,System.String)
extern "C"  void PluginPaoJiao_init_m270883093 (PluginPaoJiao_t3296777368 * __this, String_t* ___Appkey0, String_t* ___appid1, String_t* ___channelid2, String_t* ___channel3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginPaoJiao::login()
extern "C"  void PluginPaoJiao_login_m4077239866 (PluginPaoJiao_t3296777368 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginPaoJiao::goZF(System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginPaoJiao_goZF_m3545906297 (PluginPaoJiao_t3296777368 * __this, String_t* ___amount0, String_t* ___orderId1, String_t* ___extra2, String_t* ___tempId3, String_t* ___serverId4, String_t* ___serverName5, String_t* ___roleId6, String_t* ___roleName7, String_t* ___setProductNumber8, String_t* ___setProductName9, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginPaoJiao::coloectData(System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginPaoJiao_coloectData_m917535048 (PluginPaoJiao_t3296777368 * __this, String_t* ___serverId0, String_t* ___servername1, String_t* ___roleid2, String_t* ___rolename3, String_t* ___rolelevel4, String_t* ___roleGold5, String_t* ___type6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginPaoJiao::<OnLogoutSuccess>m__449()
extern "C"  void PluginPaoJiao_U3COnLogoutSuccessU3Em__449_m860979197 (PluginPaoJiao_t3296777368 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginPaoJiao::ilo_OpenUserLogin1(PluginPaoJiao)
extern "C"  void PluginPaoJiao_ilo_OpenUserLogin1_m2921274937 (Il2CppObject * __this /* static, unused */, PluginPaoJiao_t3296777368 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginPaoJiao::ilo_ReqSDKHttpLogin2(PluginsSdkMgr)
extern "C"  void PluginPaoJiao_ilo_ReqSDKHttpLogin2_m3339992643 (Il2CppObject * __this /* static, unused */, PluginsSdkMgr_t3884624670 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PluginPaoJiao::ilo_GetProductID3(ProductsCfgMgr,System.String)
extern "C"  String_t* PluginPaoJiao_ilo_GetProductID3_m1211070052 (Il2CppObject * __this /* static, unused */, ProductsCfgMgr_t2493714872 * ____this0, String_t* ___name1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mihua.SDK.CreatRoleInfo PluginPaoJiao::ilo_Parse4(System.Object[])
extern "C"  CreatRoleInfo_t2858846543 * PluginPaoJiao_ilo_Parse4_m2536770141 (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t1108656482* ___crtRoleOjs0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mihua.SDK.LvUpInfo PluginPaoJiao::ilo_Parse5(System.Object[])
extern "C"  LvUpInfo_t411687177 * PluginPaoJiao_ilo_Parse5_m3775686508 (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t1108656482* ___crtRoleOjs0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginPaoJiao::ilo_ClollectData6(PluginPaoJiao,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginPaoJiao_ilo_ClollectData6_m514612764 (Il2CppObject * __this /* static, unused */, PluginPaoJiao_t3296777368 * ____this0, String_t* ___type1, String_t* ___serverid2, String_t* ___servername3, String_t* ___roleid4, String_t* ___rolename5, String_t* ___rolelevel6, String_t* ___roleGold7, String_t* ___VIPLv8, String_t* ___GameName9, const MethodInfo* method) IL2CPP_METHOD_ATTR;
