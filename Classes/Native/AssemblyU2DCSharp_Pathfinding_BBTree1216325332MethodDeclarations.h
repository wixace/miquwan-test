﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.BBTree
struct BBTree_t1216325332;
// Pathfinding.INavmeshHolder
struct INavmeshHolder_t1019551337;
// Pathfinding.MeshNode
struct MeshNode_t3005053445;
// Pathfinding.NNConstraint
struct NNConstraint_t758567699;
// Pathfinding.GraphNode
struct GraphNode_t23612370;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Rect4241904616.h"
#include "AssemblyU2DCSharp_Pathfinding_MeshNode3005053445.h"
#include "AssemblyU2DCSharp_Pathfinding_NNInfo1570625892.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_Pathfinding_NNConstraint758567699.h"
#include "AssemblyU2DCSharp_Pathfinding_BBTree1216325332.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphNode23612370.h"
#include "AssemblyU2DCSharp_Pathfinding_Int31974045594.h"

// System.Void Pathfinding.BBTree::.ctor(Pathfinding.INavmeshHolder)
extern "C"  void BBTree__ctor_m3723854358 (BBTree_t1216325332 * __this, Il2CppObject * ___graph0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect Pathfinding.BBTree::get_Size()
extern "C"  Rect_t4241904616  BBTree_get_Size_m1486722973 (BBTree_t1216325332 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.BBTree::Clear()
extern "C"  void BBTree_Clear_m3312974910 (BBTree_t1216325332 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.BBTree::EnsureCapacity(System.Int32)
extern "C"  void BBTree_EnsureCapacity_m3552267194 (BBTree_t1216325332 * __this, int32_t ___c0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pathfinding.BBTree::GetBox(Pathfinding.MeshNode)
extern "C"  int32_t BBTree_GetBox_m4278173723 (BBTree_t1216325332 * __this, MeshNode_t3005053445 * ___node0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.BBTree::Insert(Pathfinding.MeshNode)
extern "C"  void BBTree_Insert_m1383945009 (BBTree_t1216325332 * __this, MeshNode_t3005053445 * ___node0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.NNInfo Pathfinding.BBTree::Query(UnityEngine.Vector3,Pathfinding.NNConstraint)
extern "C"  NNInfo_t1570625892  BBTree_Query_m598288474 (BBTree_t1216325332 * __this, Vector3_t4282066566  ___p0, NNConstraint_t758567699 * ___constraint1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.NNInfo Pathfinding.BBTree::QueryCircle(UnityEngine.Vector3,System.Single,Pathfinding.NNConstraint)
extern "C"  NNInfo_t1570625892  BBTree_QueryCircle_m3138528389 (BBTree_t1216325332 * __this, Vector3_t4282066566  ___p0, float ___radius1, NNConstraint_t758567699 * ___constraint2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.NNInfo Pathfinding.BBTree::QueryClosest(UnityEngine.Vector3,Pathfinding.NNConstraint,System.Single&)
extern "C"  NNInfo_t1570625892  BBTree_QueryClosest_m853374554 (BBTree_t1216325332 * __this, Vector3_t4282066566  ___p0, NNConstraint_t758567699 * ___constraint1, float* ___distance2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.NNInfo Pathfinding.BBTree::QueryClosestXZ(UnityEngine.Vector3,Pathfinding.NNConstraint,System.Single&,Pathfinding.NNInfo)
extern "C"  NNInfo_t1570625892  BBTree_QueryClosestXZ_m3328075542 (BBTree_t1216325332 * __this, Vector3_t4282066566  ___p0, NNConstraint_t758567699 * ___constraint1, float* ___distance2, NNInfo_t1570625892  ___previous3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.BBTree::SearchBoxClosestXZ(System.Int32,UnityEngine.Vector3,System.Single&,Pathfinding.NNConstraint,Pathfinding.NNInfo&)
extern "C"  void BBTree_SearchBoxClosestXZ_m3137999715 (BBTree_t1216325332 * __this, int32_t ___boxi0, Vector3_t4282066566  ___p1, float* ___closestDist2, NNConstraint_t758567699 * ___constraint3, NNInfo_t1570625892 * ___nnInfo4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.NNInfo Pathfinding.BBTree::QueryClosest(UnityEngine.Vector3,Pathfinding.NNConstraint,System.Single&,Pathfinding.NNInfo)
extern "C"  NNInfo_t1570625892  BBTree_QueryClosest_m1061552532 (BBTree_t1216325332 * __this, Vector3_t4282066566  ___p0, NNConstraint_t758567699 * ___constraint1, float* ___distance2, NNInfo_t1570625892  ___previous3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.BBTree::SearchBoxClosest(System.Int32,UnityEngine.Vector3,System.Single&,Pathfinding.NNConstraint,Pathfinding.NNInfo&)
extern "C"  void BBTree_SearchBoxClosest_m846297441 (BBTree_t1216325332 * __this, int32_t ___boxi0, Vector3_t4282066566  ___p1, float* ___closestDist2, NNConstraint_t758567699 * ___constraint3, NNInfo_t1570625892 * ___nnInfo4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.MeshNode Pathfinding.BBTree::QueryInside(UnityEngine.Vector3,Pathfinding.NNConstraint)
extern "C"  MeshNode_t3005053445 * BBTree_QueryInside_m1948791127 (BBTree_t1216325332 * __this, Vector3_t4282066566  ___p0, NNConstraint_t758567699 * ___constraint1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.MeshNode Pathfinding.BBTree::SearchBoxInside(System.Int32,UnityEngine.Vector3,Pathfinding.NNConstraint)
extern "C"  MeshNode_t3005053445 * BBTree_SearchBoxInside_m4145020175 (BBTree_t1216325332 * __this, int32_t ___boxi0, Vector3_t4282066566  ___p1, NNConstraint_t758567699 * ___constraint2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.BBTree::SearchBoxCircle(System.Int32,UnityEngine.Vector3,System.Single,Pathfinding.NNConstraint,Pathfinding.NNInfo&)
extern "C"  void BBTree_SearchBoxCircle_m273180720 (BBTree_t1216325332 * __this, int32_t ___boxi0, Vector3_t4282066566  ___p1, float ___radius2, NNConstraint_t758567699 * ___constraint3, NNInfo_t1570625892 * ___nnInfo4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.BBTree::SearchBox(System.Int32,UnityEngine.Vector3,Pathfinding.NNConstraint,Pathfinding.NNInfo&)
extern "C"  void BBTree_SearchBox_m3066339749 (BBTree_t1216325332 * __this, int32_t ___boxi0, Vector3_t4282066566  ___p1, NNConstraint_t758567699 * ___constraint2, NNInfo_t1570625892 * ___nnInfo3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.BBTree::OnDrawGizmos()
extern "C"  void BBTree_OnDrawGizmos_m3716490669 (BBTree_t1216325332 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.BBTree::OnDrawGizmos(System.Int32,System.Int32)
extern "C"  void BBTree_OnDrawGizmos_m2272000537 (BBTree_t1216325332 * __this, int32_t ___boxi0, int32_t ___depth1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.BBTree::NodeIntersectsCircle(Pathfinding.MeshNode,UnityEngine.Vector3,System.Single)
extern "C"  bool BBTree_NodeIntersectsCircle_m915086954 (Il2CppObject * __this /* static, unused */, MeshNode_t3005053445 * ___node0, Vector3_t4282066566  ___p1, float ___radius2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.BBTree::RectIntersectsCircle(UnityEngine.Rect,UnityEngine.Vector3,System.Single)
extern "C"  bool BBTree_RectIntersectsCircle_m2171599914 (Il2CppObject * __this /* static, unused */, Rect_t4241904616  ___r0, Vector3_t4282066566  ___p1, float ___radius2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.BBTree::RectContains(UnityEngine.Rect,UnityEngine.Vector3)
extern "C"  bool BBTree_RectContains_m1578047168 (Il2CppObject * __this /* static, unused */, Rect_t4241904616  ___r0, Vector3_t4282066566  ___p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Pathfinding.BBTree::ExpansionRequired(UnityEngine.Rect,UnityEngine.Rect)
extern "C"  float BBTree_ExpansionRequired_m3959473827 (Il2CppObject * __this /* static, unused */, Rect_t4241904616  ___r0, Rect_t4241904616  ___r21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect Pathfinding.BBTree::ExpandToContain(UnityEngine.Rect,UnityEngine.Rect)
extern "C"  Rect_t4241904616  BBTree_ExpandToContain_m3559479640 (Il2CppObject * __this /* static, unused */, Rect_t4241904616  ___r0, Rect_t4241904616  ___r21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Pathfinding.BBTree::RectArea(UnityEngine.Rect)
extern "C"  float BBTree_RectArea_m1552280755 (Il2CppObject * __this /* static, unused */, Rect_t4241904616  ___r0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect Pathfinding.BBTree::ilo_ExpandToContain1(UnityEngine.Rect,UnityEngine.Rect)
extern "C"  Rect_t4241904616  BBTree_ilo_ExpandToContain1_m4076333998 (Il2CppObject * __this /* static, unused */, Rect_t4241904616  ___r0, Rect_t4241904616  ___r21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pathfinding.BBTree::ilo_GetBox2(Pathfinding.BBTree,Pathfinding.MeshNode)
extern "C"  int32_t BBTree_ilo_GetBox2_m3400895496 (Il2CppObject * __this /* static, unused */, BBTree_t1216325332 * ____this0, MeshNode_t3005053445 * ___node1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Pathfinding.BBTree::ilo_ExpansionRequired3(UnityEngine.Rect,UnityEngine.Rect)
extern "C"  float BBTree_ilo_ExpansionRequired3_m3036898881 (Il2CppObject * __this /* static, unused */, Rect_t4241904616  ___r0, Rect_t4241904616  ___r21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.NNInfo Pathfinding.BBTree::ilo_QueryClosest4(Pathfinding.BBTree,UnityEngine.Vector3,Pathfinding.NNConstraint,System.Single&,Pathfinding.NNInfo)
extern "C"  NNInfo_t1570625892  BBTree_ilo_QueryClosest4_m1549098687 (Il2CppObject * __this /* static, unused */, BBTree_t1216325332 * ____this0, Vector3_t4282066566  ___p1, NNConstraint_t758567699 * ___constraint2, float* ___distance3, NNInfo_t1570625892  ___previous4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Pathfinding.BBTree::ilo_ClosestPointOnNodeXZ5(Pathfinding.MeshNode,UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  BBTree_ilo_ClosestPointOnNodeXZ5_m3008035961 (Il2CppObject * __this /* static, unused */, MeshNode_t3005053445 * ____this0, Vector3_t4282066566  ___p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.BBTree::ilo_SearchBoxClosestXZ6(Pathfinding.BBTree,System.Int32,UnityEngine.Vector3,System.Single&,Pathfinding.NNConstraint,Pathfinding.NNInfo&)
extern "C"  void BBTree_ilo_SearchBoxClosestXZ6_m1938827468 (Il2CppObject * __this /* static, unused */, BBTree_t1216325332 * ____this0, int32_t ___boxi1, Vector3_t4282066566  ___p2, float* ___closestDist3, NNConstraint_t758567699 * ___constraint4, NNInfo_t1570625892 * ___nnInfo5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.BBTree::ilo_NodeIntersectsCircle7(Pathfinding.MeshNode,UnityEngine.Vector3,System.Single)
extern "C"  bool BBTree_ilo_NodeIntersectsCircle7_m1158207994 (Il2CppObject * __this /* static, unused */, MeshNode_t3005053445 * ___node0, Vector3_t4282066566  ___p1, float ___radius2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.BBTree::ilo_Suitable8(Pathfinding.NNConstraint,Pathfinding.GraphNode)
extern "C"  bool BBTree_ilo_Suitable8_m4057169178 (Il2CppObject * __this /* static, unused */, NNConstraint_t758567699 * ____this0, GraphNode_t23612370 * ___node1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.BBTree::ilo_RectIntersectsCircle9(UnityEngine.Rect,UnityEngine.Vector3,System.Single)
extern "C"  bool BBTree_ilo_RectIntersectsCircle9_m2417035576 (Il2CppObject * __this /* static, unused */, Rect_t4241904616  ___r0, Vector3_t4282066566  ___p1, float ___radius2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.BBTree::ilo_SearchBoxClosest10(Pathfinding.BBTree,System.Int32,UnityEngine.Vector3,System.Single&,Pathfinding.NNConstraint,Pathfinding.NNInfo&)
extern "C"  void BBTree_ilo_SearchBoxClosest10_m3605976495 (Il2CppObject * __this /* static, unused */, BBTree_t1216325332 * ____this0, int32_t ___boxi1, Vector3_t4282066566  ___p2, float* ___closestDist3, NNConstraint_t758567699 * ___constraint4, NNInfo_t1570625892 * ___nnInfo5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.BBTree::ilo_ContainsPoint11(Pathfinding.MeshNode,Pathfinding.Int3)
extern "C"  bool BBTree_ilo_ContainsPoint11_m2714173782 (Il2CppObject * __this /* static, unused */, MeshNode_t3005053445 * ____this0, Int3_t1974045594  ___p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.MeshNode Pathfinding.BBTree::ilo_SearchBoxInside12(Pathfinding.BBTree,System.Int32,UnityEngine.Vector3,Pathfinding.NNConstraint)
extern "C"  MeshNode_t3005053445 * BBTree_ilo_SearchBoxInside12_m518526695 (Il2CppObject * __this /* static, unused */, BBTree_t1216325332 * ____this0, int32_t ___boxi1, Vector3_t4282066566  ___p2, NNConstraint_t758567699 * ___constraint3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.BBTree::ilo_SearchBoxCircle13(Pathfinding.BBTree,System.Int32,UnityEngine.Vector3,System.Single,Pathfinding.NNConstraint,Pathfinding.NNInfo&)
extern "C"  void BBTree_ilo_SearchBoxCircle13_m108841209 (Il2CppObject * __this /* static, unused */, BBTree_t1216325332 * ____this0, int32_t ___boxi1, Vector3_t4282066566  ___p2, float ___radius3, NNConstraint_t758567699 * ___constraint4, NNInfo_t1570625892 * ___nnInfo5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Int3 Pathfinding.BBTree::ilo_op_Explicit14(UnityEngine.Vector3)
extern "C"  Int3_t1974045594  BBTree_ilo_op_Explicit14_m3999534421 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___ob0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Pathfinding.BBTree::ilo_op_Explicit15(Pathfinding.Int3)
extern "C"  Vector3_t4282066566  BBTree_ilo_op_Explicit15_m1671137420 (Il2CppObject * __this /* static, unused */, Int3_t1974045594  ___ob0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.BBTree::ilo_RectContains16(UnityEngine.Rect,UnityEngine.Vector3)
extern "C"  bool BBTree_ilo_RectContains16_m2804220952 (Il2CppObject * __this /* static, unused */, Rect_t4241904616  ___r0, Vector3_t4282066566  ___p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.BBTree::ilo_OnDrawGizmos17(Pathfinding.BBTree,System.Int32,System.Int32)
extern "C"  void BBTree_ilo_OnDrawGizmos17_m2322464158 (Il2CppObject * __this /* static, unused */, BBTree_t1216325332 * ____this0, int32_t ___boxi1, int32_t ___depth2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Pathfinding.BBTree::ilo_ClosestPointOnNode18(Pathfinding.MeshNode,UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  BBTree_ilo_ClosestPointOnNode18_m1448706871 (Il2CppObject * __this /* static, unused */, MeshNode_t3005053445 * ____this0, Vector3_t4282066566  ___p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
