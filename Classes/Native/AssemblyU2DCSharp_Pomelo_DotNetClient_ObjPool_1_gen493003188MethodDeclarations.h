﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pomelo_DotNetClient_ObjPool_1_gen286528818MethodDeclarations.h"

// System.Void Pomelo.DotNetClient.ObjPool`1<Pomelo.DotNetClient.MsgEvent>::.ctor(System.Int32)
#define ObjPool_1__ctor_m1036522629(__this, ___maxNum0, method) ((  void (*) (ObjPool_1_t493003188 *, int32_t, const MethodInfo*))ObjPool_1__ctor_m2650734553_gshared)(__this, ___maxNum0, method)
// T Pomelo.DotNetClient.ObjPool`1<Pomelo.DotNetClient.MsgEvent>::PopObj()
#define ObjPool_1_PopObj_m3607681303(__this, method) ((  MsgEvent_t82323445 * (*) (ObjPool_1_t493003188 *, const MethodInfo*))ObjPool_1_PopObj_m3392359585_gshared)(__this, method)
// System.Void Pomelo.DotNetClient.ObjPool`1<Pomelo.DotNetClient.MsgEvent>::PushObj(T)
#define ObjPool_1_PushObj_m2569134319(__this, ___obj0, method) ((  void (*) (ObjPool_1_t493003188 *, MsgEvent_t82323445 *, const MethodInfo*))ObjPool_1_PushObj_m2019108635_gshared)(__this, ___obj0, method)
// System.Void Pomelo.DotNetClient.ObjPool`1<Pomelo.DotNetClient.MsgEvent>::Dispose()
#define ObjPool_1_Dispose_m4027186289(__this, method) ((  void (*) (ObjPool_1_t493003188 *, const MethodInfo*))ObjPool_1_Dispose_m4009443525_gshared)(__this, method)
