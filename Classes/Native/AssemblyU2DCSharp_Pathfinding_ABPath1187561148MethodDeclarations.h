﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.ABPath
struct ABPath_t1187561148;
// OnPathDelegate
struct OnPathDelegate_t598607977;
// Pathfinding.GraphNode
struct GraphNode_t23612370;
// Pathfinding.Path
struct Path_t1974241691;
// System.String
struct String_t;
// AstarPath
struct AstarPath_t4090270936;
// Pathfinding.NNConstraint
struct NNConstraint_t758567699;
// Pathfinding.PathNode
struct PathNode_t417131581;
// Pathfinding.PathHandler
struct PathHandler_t918952263;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_OnPathDelegate598607977.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphNode23612370.h"
#include "AssemblyU2DCSharp_Pathfinding_Path1974241691.h"
#include "AssemblyU2DCSharp_PathLog873181375.h"
#include "AssemblyU2DCSharp_Pathfinding_ABPath1187561148.h"
#include "AssemblyU2DCSharp_Pathfinding_Int31974045594.h"
#include "AssemblyU2DCSharp_Pathfinding_NNInfo1570625892.h"
#include "AssemblyU2DCSharp_AstarPath4090270936.h"
#include "AssemblyU2DCSharp_Pathfinding_NNConstraint758567699.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_Pathfinding_PathNode417131581.h"
#include "AssemblyU2DCSharp_Pathfinding_PathHandler918952263.h"
#include "AssemblyU2DCSharp_PathCompleteState1625108115.h"

// System.Void Pathfinding.ABPath::.ctor(UnityEngine.Vector3,UnityEngine.Vector3,OnPathDelegate)
extern "C"  void ABPath__ctor_m2430687484 (ABPath_t1187561148 * __this, Vector3_t4282066566  ___start0, Vector3_t4282066566  ___end1, OnPathDelegate_t598607977 * ___callbackDelegate2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.ABPath::.ctor()
extern "C"  void ABPath__ctor_m1604488491 (ABPath_t1187561148 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.ABPath Pathfinding.ABPath::Construct(UnityEngine.Vector3,UnityEngine.Vector3,OnPathDelegate)
extern "C"  ABPath_t1187561148 * ABPath_Construct_m4082230660 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___start0, Vector3_t4282066566  ___end1, OnPathDelegate_t598607977 * ___callback2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.ABPath::Setup(UnityEngine.Vector3,UnityEngine.Vector3,OnPathDelegate)
extern "C"  void ABPath_Setup_m2454690583 (ABPath_t1187561148 * __this, Vector3_t4282066566  ___start0, Vector3_t4282066566  ___end1, OnPathDelegate_t598607977 * ___callbackDelegate2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.ABPath::UpdateStartEnd(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  void ABPath_UpdateStartEnd_m2791417169 (ABPath_t1187561148 * __this, Vector3_t4282066566  ___start0, Vector3_t4282066566  ___end1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Pathfinding.ABPath::GetConnectionSpecialCost(Pathfinding.GraphNode,Pathfinding.GraphNode,System.UInt32)
extern "C"  uint32_t ABPath_GetConnectionSpecialCost_m1145754048 (ABPath_t1187561148 * __this, GraphNode_t23612370 * ___a0, GraphNode_t23612370 * ___b1, uint32_t ___currentCost2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.ABPath::Reset()
extern "C"  void ABPath_Reset_m3545888728 (ABPath_t1187561148 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.ABPath::Prepare()
extern "C"  void ABPath_Prepare_m2951128976 (ABPath_t1187561148 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.ABPath::Initialize()
extern "C"  void ABPath_Initialize_m783961161 (ABPath_t1187561148 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.ABPath::Cleanup()
extern "C"  void ABPath_Cleanup_m2688941549 (ABPath_t1187561148 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.ABPath::CalculateStep(System.Int64)
extern "C"  void ABPath_CalculateStep_m4127779181 (ABPath_t1187561148 * __this, int64_t ___targetTick0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.ABPath::ResetCosts(Pathfinding.Path)
extern "C"  void ABPath_ResetCosts_m2529716417 (ABPath_t1187561148 * __this, Path_t1974241691 * ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Pathfinding.ABPath::DebugString(PathLog)
extern "C"  String_t* ABPath_DebugString_m2207168137 (ABPath_t1187561148 * __this, int32_t ___logMode0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.ABPath::Recycle()
extern "C"  void ABPath_Recycle_m933812764 (ABPath_t1187561148 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Pathfinding.ABPath::GetMovementVector(UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  ABPath_GetMovementVector_m4125238562 (ABPath_t1187561148 * __this, Vector3_t4282066566  ___point0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.ABPath::ilo_Setup1(Pathfinding.ABPath,UnityEngine.Vector3,UnityEngine.Vector3,OnPathDelegate)
extern "C"  void ABPath_ilo_Setup1_m1349567239 (Il2CppObject * __this /* static, unused */, ABPath_t1187561148 * ____this0, Vector3_t4282066566  ___start1, Vector3_t4282066566  ___end2, OnPathDelegate_t598607977 * ___callbackDelegate3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.ABPath::ilo_UpdateStartEnd2(Pathfinding.ABPath,UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  void ABPath_ilo_UpdateStartEnd2_m287848156 (Il2CppObject * __this /* static, unused */, ABPath_t1187561148 * ____this0, Vector3_t4282066566  ___start1, Vector3_t4282066566  ___end2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Int3 Pathfinding.ABPath::ilo_op_Subtraction3(Pathfinding.Int3,Pathfinding.Int3)
extern "C"  Int3_t1974045594  ABPath_ilo_op_Subtraction3_m3274920514 (Il2CppObject * __this /* static, unused */, Int3_t1974045594  ___lhs0, Int3_t1974045594  ___rhs1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pathfinding.ABPath::ilo_get_costMagnitude4(Pathfinding.Int3&)
extern "C"  int32_t ABPath_ilo_get_costMagnitude4_m3273743718 (Il2CppObject * __this /* static, unused */, Int3_t1974045594 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.ABPath::ilo_Reset5(Pathfinding.Path)
extern "C"  void ABPath_ilo_Reset5_m2803343043 (Il2CppObject * __this /* static, unused */, Path_t1974241691 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.NNInfo Pathfinding.ABPath::ilo_GetNearest6(AstarPath,UnityEngine.Vector3,Pathfinding.NNConstraint,Pathfinding.GraphNode)
extern "C"  NNInfo_t1570625892  ABPath_ilo_GetNearest6_m3575670455 (Il2CppObject * __this /* static, unused */, AstarPath_t4090270936 * ____this0, Vector3_t4282066566  ___position1, NNConstraint_t758567699 * ___constraint2, GraphNode_t23612370 * ___hint3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.ABPath::ilo_Error7(Pathfinding.Path)
extern "C"  void ABPath_ilo_Error7_m1661802732 (Il2CppObject * __this /* static, unused */, Path_t1974241691 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.ABPath::ilo_get_Walkable8(Pathfinding.GraphNode)
extern "C"  bool ABPath_ilo_get_Walkable8_m1296745438 (Il2CppObject * __this /* static, unused */, GraphNode_t23612370 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.ABPath::ilo_LogError9(Pathfinding.Path,System.String)
extern "C"  void ABPath_ilo_LogError9_m3250309464 (Il2CppObject * __this /* static, unused */, Path_t1974241691 * ____this0, String_t* ___msg1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Pathfinding.ABPath::ilo_get_Area10(Pathfinding.GraphNode)
extern "C"  uint32_t ABPath_ilo_get_Area10_m1326981168 (Il2CppObject * __this /* static, unused */, GraphNode_t23612370 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.ABPath::ilo_set_flag211(Pathfinding.PathNode,System.Boolean)
extern "C"  void ABPath_ilo_set_flag211_m2187143855 (Il2CppObject * __this /* static, unused */, PathNode_t417131581 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.PathNode Pathfinding.ABPath::ilo_GetPathNode12(Pathfinding.PathHandler,Pathfinding.GraphNode)
extern "C"  PathNode_t417131581 * ABPath_ilo_GetPathNode12_m2724358997 (Il2CppObject * __this /* static, unused */, PathHandler_t918952263 * ____this0, GraphNode_t23612370 * ___node1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.ABPath::ilo_set_cost13(Pathfinding.PathNode,System.UInt32)
extern "C"  void ABPath_ilo_set_cost13_m1699103771 (Il2CppObject * __this /* static, unused */, PathNode_t417131581 * ____this0, uint32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Pathfinding.ABPath::ilo_GetTraversalCost14(Pathfinding.Path,Pathfinding.GraphNode)
extern "C"  uint32_t ABPath_ilo_GetTraversalCost14_m80626582 (Il2CppObject * __this /* static, unused */, Path_t1974241691 * ____this0, GraphNode_t23612370 * ___node1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Pathfinding.ABPath::ilo_CalculateHScore15(Pathfinding.Path,Pathfinding.GraphNode)
extern "C"  uint32_t ABPath_ilo_CalculateHScore15_m2693085478 (Il2CppObject * __this /* static, unused */, Path_t1974241691 * ____this0, GraphNode_t23612370 * ___node1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.ABPath::ilo_Trace16(Pathfinding.Path,Pathfinding.PathNode)
extern "C"  void ABPath_ilo_Trace16_m3604097316 (Il2CppObject * __this /* static, unused */, Path_t1974241691 * ____this0, PathNode_t417131581 * ___from1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.PathNode Pathfinding.ABPath::ilo_PopNode17(Pathfinding.PathHandler)
extern "C"  PathNode_t417131581 * ABPath_ilo_PopNode17_m2716147988 (Il2CppObject * __this /* static, unused */, PathHandler_t918952263 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PathCompleteState Pathfinding.ABPath::ilo_get_CompleteState18(Pathfinding.Path)
extern "C"  int32_t ABPath_ilo_get_CompleteState18_m1314998013 (Il2CppObject * __this /* static, unused */, Path_t1974241691 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.ABPath::ilo_set_CompleteState19(Pathfinding.Path,PathCompleteState)
extern "C"  void ABPath_ilo_set_CompleteState19_m281366691 (Il2CppObject * __this /* static, unused */, Path_t1974241691 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.ABPath::ilo_get_error20(Pathfinding.Path)
extern "C"  bool ABPath_ilo_get_error20_m1669876624 (Il2CppObject * __this /* static, unused */, Path_t1974241691 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Pathfinding.ABPath::ilo_get_F21(Pathfinding.PathNode)
extern "C"  uint32_t ABPath_ilo_get_F21_m943595686 (Il2CppObject * __this /* static, unused */, PathNode_t417131581 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Pathfinding.ABPath::ilo_get_GraphIndex22(Pathfinding.GraphNode)
extern "C"  uint32_t ABPath_ilo_get_GraphIndex22_m693090936 (Il2CppObject * __this /* static, unused */, GraphNode_t23612370 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Pathfinding.ABPath::ilo_get_errorLog23(Pathfinding.Path)
extern "C"  String_t* ABPath_ilo_get_errorLog23_m1081966566 (Il2CppObject * __this /* static, unused */, Path_t1974241691 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.ABPath::ilo_get_IsUsingMultithreading24()
extern "C"  bool ABPath_ilo_get_IsUsingMultithreading24_m3158167284 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Pathfinding.ABPath::ilo_NearestPointStrict25(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  ABPath_ilo_NearestPointStrict25_m3254742555 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___lineStart0, Vector3_t4282066566  ___lineEnd1, Vector3_t4282066566  ___point2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
