﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LabelWriteEffectGenerated/<LabelWriteEffect_AddFunctionList_GetDelegate_member0_arg0>c__AnonStorey6A
struct U3CLabelWriteEffect_AddFunctionList_GetDelegate_member0_arg0U3Ec__AnonStorey6A_t2419707430;

#include "codegen/il2cpp-codegen.h"

// System.Void LabelWriteEffectGenerated/<LabelWriteEffect_AddFunctionList_GetDelegate_member0_arg0>c__AnonStorey6A::.ctor()
extern "C"  void U3CLabelWriteEffect_AddFunctionList_GetDelegate_member0_arg0U3Ec__AnonStorey6A__ctor_m619259701 (U3CLabelWriteEffect_AddFunctionList_GetDelegate_member0_arg0U3Ec__AnonStorey6A_t2419707430 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LabelWriteEffectGenerated/<LabelWriteEffect_AddFunctionList_GetDelegate_member0_arg0>c__AnonStorey6A::<>m__69()
extern "C"  void U3CLabelWriteEffect_AddFunctionList_GetDelegate_member0_arg0U3Ec__AnonStorey6A_U3CU3Em__69_m416316097 (U3CLabelWriteEffect_AddFunctionList_GetDelegate_member0_arg0U3Ec__AnonStorey6A_t2419707430 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
