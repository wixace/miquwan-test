﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TweenTool
struct TweenTool_t2172982915;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"

// System.Void TweenTool::.ctor()
extern "C"  void TweenTool__ctor_m847487560 (TweenTool_t2172982915 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenTool::Start()
extern "C"  void TweenTool_Start_m4089592648 (TweenTool_t2172982915 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenTool::ActiveAnimation(System.Boolean)
extern "C"  void TweenTool_ActiveAnimation_m2558267195 (TweenTool_t2172982915 * __this, bool ___isActive0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenTool::PlayAnimation(UnityEngine.GameObject)
extern "C"  void TweenTool_PlayAnimation_m1143720974 (TweenTool_t2172982915 * __this, GameObject_t3674682005 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenTool::Update()
extern "C"  void TweenTool_Update_m2229172677 (TweenTool_t2172982915 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
