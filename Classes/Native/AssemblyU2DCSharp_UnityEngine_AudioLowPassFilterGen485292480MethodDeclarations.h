﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_AudioLowPassFilterGenerated
struct UnityEngine_AudioLowPassFilterGenerated_t485292480;
// JSVCall
struct JSVCall_t3708497963;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"

// System.Void UnityEngine_AudioLowPassFilterGenerated::.ctor()
extern "C"  void UnityEngine_AudioLowPassFilterGenerated__ctor_m2084801963 (UnityEngine_AudioLowPassFilterGenerated_t485292480 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AudioLowPassFilterGenerated::AudioLowPassFilter_AudioLowPassFilter1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AudioLowPassFilterGenerated_AudioLowPassFilter_AudioLowPassFilter1_m4047124807 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioLowPassFilterGenerated::AudioLowPassFilter_cutoffFrequency(JSVCall)
extern "C"  void UnityEngine_AudioLowPassFilterGenerated_AudioLowPassFilter_cutoffFrequency_m3649367127 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioLowPassFilterGenerated::AudioLowPassFilter_customCutoffCurve(JSVCall)
extern "C"  void UnityEngine_AudioLowPassFilterGenerated_AudioLowPassFilter_customCutoffCurve_m21551541 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioLowPassFilterGenerated::AudioLowPassFilter_lowpassResonanceQ(JSVCall)
extern "C"  void UnityEngine_AudioLowPassFilterGenerated_AudioLowPassFilter_lowpassResonanceQ_m25662302 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioLowPassFilterGenerated::__Register()
extern "C"  void UnityEngine_AudioLowPassFilterGenerated___Register_m3663380988 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_AudioLowPassFilterGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UnityEngine_AudioLowPassFilterGenerated_ilo_getObject1_m4171171691 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AudioLowPassFilterGenerated::ilo_attachFinalizerObject2(System.Int32)
extern "C"  bool UnityEngine_AudioLowPassFilterGenerated_ilo_attachFinalizerObject2_m2674507757 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioLowPassFilterGenerated::ilo_setSingle3(System.Int32,System.Single)
extern "C"  void UnityEngine_AudioLowPassFilterGenerated_ilo_setSingle3_m3496664043 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine_AudioLowPassFilterGenerated::ilo_getSingle4(System.Int32)
extern "C"  float UnityEngine_AudioLowPassFilterGenerated_ilo_getSingle4_m3861646855 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
