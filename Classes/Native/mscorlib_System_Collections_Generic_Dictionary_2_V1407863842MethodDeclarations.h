﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,ProductsCfgMgr/ProductInfo>
struct Dictionary_2_t3476030434;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V1407863842.h"
#include "AssemblyU2DCSharp_ProductsCfgMgr_ProductInfo1305991238.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,ProductsCfgMgr/ProductInfo>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m3317800675_gshared (Enumerator_t1407863842 * __this, Dictionary_2_t3476030434 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m3317800675(__this, ___host0, method) ((  void (*) (Enumerator_t1407863842 *, Dictionary_2_t3476030434 *, const MethodInfo*))Enumerator__ctor_m3317800675_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,ProductsCfgMgr/ProductInfo>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2150134440_gshared (Enumerator_t1407863842 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m2150134440(__this, method) ((  Il2CppObject * (*) (Enumerator_t1407863842 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2150134440_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,ProductsCfgMgr/ProductInfo>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m927140338_gshared (Enumerator_t1407863842 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m927140338(__this, method) ((  void (*) (Enumerator_t1407863842 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m927140338_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,ProductsCfgMgr/ProductInfo>::Dispose()
extern "C"  void Enumerator_Dispose_m1063433285_gshared (Enumerator_t1407863842 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1063433285(__this, method) ((  void (*) (Enumerator_t1407863842 *, const MethodInfo*))Enumerator_Dispose_m1063433285_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,ProductsCfgMgr/ProductInfo>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m4126232354_gshared (Enumerator_t1407863842 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m4126232354(__this, method) ((  bool (*) (Enumerator_t1407863842 *, const MethodInfo*))Enumerator_MoveNext_m4126232354_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,ProductsCfgMgr/ProductInfo>::get_Current()
extern "C"  ProductInfo_t1305991238  Enumerator_get_Current_m2534842568_gshared (Enumerator_t1407863842 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m2534842568(__this, method) ((  ProductInfo_t1305991238  (*) (Enumerator_t1407863842 *, const MethodInfo*))Enumerator_get_Current_m2534842568_gshared)(__this, method)
