﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BehaviourUtilGenerated/<BehaviourUtil_JSDelayCall_GetDelegate_member10_arg1>c__AnonStorey4F
struct U3CBehaviourUtil_JSDelayCall_GetDelegate_member10_arg1U3Ec__AnonStorey4F_t3009675636;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void BehaviourUtilGenerated/<BehaviourUtil_JSDelayCall_GetDelegate_member10_arg1>c__AnonStorey4F::.ctor()
extern "C"  void U3CBehaviourUtil_JSDelayCall_GetDelegate_member10_arg1U3Ec__AnonStorey4F__ctor_m3426864247 (U3CBehaviourUtil_JSDelayCall_GetDelegate_member10_arg1U3Ec__AnonStorey4F_t3009675636 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BehaviourUtilGenerated/<BehaviourUtil_JSDelayCall_GetDelegate_member10_arg1>c__AnonStorey4F::<>m__12(System.Object)
extern "C"  void U3CBehaviourUtil_JSDelayCall_GetDelegate_member10_arg1U3Ec__AnonStorey4F_U3CU3Em__12_m3255766995 (U3CBehaviourUtil_JSDelayCall_GetDelegate_member10_arg1U3Ec__AnonStorey4F_t3009675636 * __this, Il2CppObject * ___arg10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
