﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.SortedDictionary`2<System.Int32,System.Int32>
struct SortedDictionary_2_t3586408994;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.SortedDictionary`2/Node<System.Int32,System.Int32>
struct Node_t930589582;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_SortedDictionary2151556692.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21049882445.h"

// System.Void System.Collections.Generic.SortedDictionary`2/Enumerator<System.Int32,System.Int32>::.ctor(System.Collections.Generic.SortedDictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m654735123_gshared (Enumerator_t2151556692 * __this, SortedDictionary_2_t3586408994 * ___dic0, const MethodInfo* method);
#define Enumerator__ctor_m654735123(__this, ___dic0, method) ((  void (*) (Enumerator_t2151556692 *, SortedDictionary_2_t3586408994 *, const MethodInfo*))Enumerator__ctor_m654735123_gshared)(__this, ___dic0, method)
// System.Collections.DictionaryEntry System.Collections.Generic.SortedDictionary`2/Enumerator<System.Int32,System.Int32>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t1751606614  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1577970036_gshared (Enumerator_t2151556692 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1577970036(__this, method) ((  DictionaryEntry_t1751606614  (*) (Enumerator_t2151556692 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1577970036_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedDictionary`2/Enumerator<System.Int32,System.Int32>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2417929927_gshared (Enumerator_t2151556692 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2417929927(__this, method) ((  Il2CppObject * (*) (Enumerator_t2151556692 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2417929927_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedDictionary`2/Enumerator<System.Int32,System.Int32>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1101789081_gshared (Enumerator_t2151556692 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1101789081(__this, method) ((  Il2CppObject * (*) (Enumerator_t2151556692 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1101789081_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedDictionary`2/Enumerator<System.Int32,System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2010775083_gshared (Enumerator_t2151556692 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m2010775083(__this, method) ((  Il2CppObject * (*) (Enumerator_t2151556692 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2010775083_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedDictionary`2/Enumerator<System.Int32,System.Int32>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1628703679_gshared (Enumerator_t2151556692 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1628703679(__this, method) ((  void (*) (Enumerator_t2151556692 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1628703679_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.SortedDictionary`2/Enumerator<System.Int32,System.Int32>::get_Current()
extern "C"  KeyValuePair_2_t1049882445  Enumerator_get_Current_m3305147833_gshared (Enumerator_t2151556692 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m3305147833(__this, method) ((  KeyValuePair_2_t1049882445  (*) (Enumerator_t2151556692 *, const MethodInfo*))Enumerator_get_Current_m3305147833_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2/Enumerator<System.Int32,System.Int32>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3833326103_gshared (Enumerator_t2151556692 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m3833326103(__this, method) ((  bool (*) (Enumerator_t2151556692 *, const MethodInfo*))Enumerator_MoveNext_m3833326103_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedDictionary`2/Enumerator<System.Int32,System.Int32>::Dispose()
extern "C"  void Enumerator_Dispose_m3874246488_gshared (Enumerator_t2151556692 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m3874246488(__this, method) ((  void (*) (Enumerator_t2151556692 *, const MethodInfo*))Enumerator_Dispose_m3874246488_gshared)(__this, method)
// System.Collections.Generic.SortedDictionary`2/Node<TKey,TValue> System.Collections.Generic.SortedDictionary`2/Enumerator<System.Int32,System.Int32>::get_CurrentNode()
extern "C"  Node_t930589582 * Enumerator_get_CurrentNode_m344695263_gshared (Enumerator_t2151556692 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentNode_m344695263(__this, method) ((  Node_t930589582 * (*) (Enumerator_t2151556692 *, const MethodInfo*))Enumerator_get_CurrentNode_m344695263_gshared)(__this, method)
