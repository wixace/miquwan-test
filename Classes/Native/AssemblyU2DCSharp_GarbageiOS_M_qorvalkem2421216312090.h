﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_qorvalkem242
struct  M_qorvalkem242_t1216312090  : public Il2CppObject
{
public:
	// System.Int32 GarbageiOS.M_qorvalkem242::_relzo
	int32_t ____relzo_0;
	// System.Int32 GarbageiOS.M_qorvalkem242::_whawmijel
	int32_t ____whawmijel_1;
	// System.UInt32 GarbageiOS.M_qorvalkem242::_herelouwalParnerechall
	uint32_t ____herelouwalParnerechall_2;
	// System.Int32 GarbageiOS.M_qorvalkem242::_fiserehouSeremo
	int32_t ____fiserehouSeremo_3;
	// System.Single GarbageiOS.M_qorvalkem242::_dejalMarzaldi
	float ____dejalMarzaldi_4;
	// System.Int32 GarbageiOS.M_qorvalkem242::_drertere
	int32_t ____drertere_5;
	// System.Single GarbageiOS.M_qorvalkem242::_mernairruStadaiba
	float ____mernairruStadaiba_6;
	// System.UInt32 GarbageiOS.M_qorvalkem242::_lodirhoJawfou
	uint32_t ____lodirhoJawfou_7;
	// System.UInt32 GarbageiOS.M_qorvalkem242::_foofaSeamazi
	uint32_t ____foofaSeamazi_8;
	// System.Int32 GarbageiOS.M_qorvalkem242::_dipiwaw
	int32_t ____dipiwaw_9;
	// System.Single GarbageiOS.M_qorvalkem242::_jestewou
	float ____jestewou_10;
	// System.Single GarbageiOS.M_qorvalkem242::_poodre
	float ____poodre_11;
	// System.Int32 GarbageiOS.M_qorvalkem242::_nallwerelou
	int32_t ____nallwerelou_12;
	// System.UInt32 GarbageiOS.M_qorvalkem242::_seesir
	uint32_t ____seesir_13;
	// System.String GarbageiOS.M_qorvalkem242::_pecavem
	String_t* ____pecavem_14;
	// System.UInt32 GarbageiOS.M_qorvalkem242::_deroocasTikerefur
	uint32_t ____deroocasTikerefur_15;
	// System.Single GarbageiOS.M_qorvalkem242::_herehaw
	float ____herehaw_16;
	// System.UInt32 GarbageiOS.M_qorvalkem242::_jabalem
	uint32_t ____jabalem_17;
	// System.Int32 GarbageiOS.M_qorvalkem242::_jiberluDrowracel
	int32_t ____jiberluDrowracel_18;
	// System.Boolean GarbageiOS.M_qorvalkem242::_ceecair
	bool ____ceecair_19;
	// System.String GarbageiOS.M_qorvalkem242::_gaveZouzacer
	String_t* ____gaveZouzacer_20;
	// System.Single GarbageiOS.M_qorvalkem242::_dalllay
	float ____dalllay_21;
	// System.Int32 GarbageiOS.M_qorvalkem242::_jurpuser
	int32_t ____jurpuser_22;
	// System.Int32 GarbageiOS.M_qorvalkem242::_cemaselDrarmel
	int32_t ____cemaselDrarmel_23;
	// System.String GarbageiOS.M_qorvalkem242::_solirgo
	String_t* ____solirgo_24;
	// System.String GarbageiOS.M_qorvalkem242::_kaircerewhe
	String_t* ____kaircerewhe_25;
	// System.UInt32 GarbageiOS.M_qorvalkem242::_towstumee
	uint32_t ____towstumee_26;
	// System.UInt32 GarbageiOS.M_qorvalkem242::_tecere
	uint32_t ____tecere_27;
	// System.UInt32 GarbageiOS.M_qorvalkem242::_fasereKerraipe
	uint32_t ____fasereKerraipe_28;
	// System.Boolean GarbageiOS.M_qorvalkem242::_juxeCerju
	bool ____juxeCerju_29;
	// System.UInt32 GarbageiOS.M_qorvalkem242::_relpastemHalda
	uint32_t ____relpastemHalda_30;
	// System.Boolean GarbageiOS.M_qorvalkem242::_lalldeetral
	bool ____lalldeetral_31;
	// System.Int32 GarbageiOS.M_qorvalkem242::_latemlaMaizas
	int32_t ____latemlaMaizas_32;
	// System.Int32 GarbageiOS.M_qorvalkem242::_rooteaxal
	int32_t ____rooteaxal_33;
	// System.Int32 GarbageiOS.M_qorvalkem242::_bawsear
	int32_t ____bawsear_34;
	// System.Single GarbageiOS.M_qorvalkem242::_vurdayhiJallcelmo
	float ____vurdayhiJallcelmo_35;
	// System.Int32 GarbageiOS.M_qorvalkem242::_rirlealal
	int32_t ____rirlealal_36;
	// System.UInt32 GarbageiOS.M_qorvalkem242::_cacay
	uint32_t ____cacay_37;

public:
	inline static int32_t get_offset_of__relzo_0() { return static_cast<int32_t>(offsetof(M_qorvalkem242_t1216312090, ____relzo_0)); }
	inline int32_t get__relzo_0() const { return ____relzo_0; }
	inline int32_t* get_address_of__relzo_0() { return &____relzo_0; }
	inline void set__relzo_0(int32_t value)
	{
		____relzo_0 = value;
	}

	inline static int32_t get_offset_of__whawmijel_1() { return static_cast<int32_t>(offsetof(M_qorvalkem242_t1216312090, ____whawmijel_1)); }
	inline int32_t get__whawmijel_1() const { return ____whawmijel_1; }
	inline int32_t* get_address_of__whawmijel_1() { return &____whawmijel_1; }
	inline void set__whawmijel_1(int32_t value)
	{
		____whawmijel_1 = value;
	}

	inline static int32_t get_offset_of__herelouwalParnerechall_2() { return static_cast<int32_t>(offsetof(M_qorvalkem242_t1216312090, ____herelouwalParnerechall_2)); }
	inline uint32_t get__herelouwalParnerechall_2() const { return ____herelouwalParnerechall_2; }
	inline uint32_t* get_address_of__herelouwalParnerechall_2() { return &____herelouwalParnerechall_2; }
	inline void set__herelouwalParnerechall_2(uint32_t value)
	{
		____herelouwalParnerechall_2 = value;
	}

	inline static int32_t get_offset_of__fiserehouSeremo_3() { return static_cast<int32_t>(offsetof(M_qorvalkem242_t1216312090, ____fiserehouSeremo_3)); }
	inline int32_t get__fiserehouSeremo_3() const { return ____fiserehouSeremo_3; }
	inline int32_t* get_address_of__fiserehouSeremo_3() { return &____fiserehouSeremo_3; }
	inline void set__fiserehouSeremo_3(int32_t value)
	{
		____fiserehouSeremo_3 = value;
	}

	inline static int32_t get_offset_of__dejalMarzaldi_4() { return static_cast<int32_t>(offsetof(M_qorvalkem242_t1216312090, ____dejalMarzaldi_4)); }
	inline float get__dejalMarzaldi_4() const { return ____dejalMarzaldi_4; }
	inline float* get_address_of__dejalMarzaldi_4() { return &____dejalMarzaldi_4; }
	inline void set__dejalMarzaldi_4(float value)
	{
		____dejalMarzaldi_4 = value;
	}

	inline static int32_t get_offset_of__drertere_5() { return static_cast<int32_t>(offsetof(M_qorvalkem242_t1216312090, ____drertere_5)); }
	inline int32_t get__drertere_5() const { return ____drertere_5; }
	inline int32_t* get_address_of__drertere_5() { return &____drertere_5; }
	inline void set__drertere_5(int32_t value)
	{
		____drertere_5 = value;
	}

	inline static int32_t get_offset_of__mernairruStadaiba_6() { return static_cast<int32_t>(offsetof(M_qorvalkem242_t1216312090, ____mernairruStadaiba_6)); }
	inline float get__mernairruStadaiba_6() const { return ____mernairruStadaiba_6; }
	inline float* get_address_of__mernairruStadaiba_6() { return &____mernairruStadaiba_6; }
	inline void set__mernairruStadaiba_6(float value)
	{
		____mernairruStadaiba_6 = value;
	}

	inline static int32_t get_offset_of__lodirhoJawfou_7() { return static_cast<int32_t>(offsetof(M_qorvalkem242_t1216312090, ____lodirhoJawfou_7)); }
	inline uint32_t get__lodirhoJawfou_7() const { return ____lodirhoJawfou_7; }
	inline uint32_t* get_address_of__lodirhoJawfou_7() { return &____lodirhoJawfou_7; }
	inline void set__lodirhoJawfou_7(uint32_t value)
	{
		____lodirhoJawfou_7 = value;
	}

	inline static int32_t get_offset_of__foofaSeamazi_8() { return static_cast<int32_t>(offsetof(M_qorvalkem242_t1216312090, ____foofaSeamazi_8)); }
	inline uint32_t get__foofaSeamazi_8() const { return ____foofaSeamazi_8; }
	inline uint32_t* get_address_of__foofaSeamazi_8() { return &____foofaSeamazi_8; }
	inline void set__foofaSeamazi_8(uint32_t value)
	{
		____foofaSeamazi_8 = value;
	}

	inline static int32_t get_offset_of__dipiwaw_9() { return static_cast<int32_t>(offsetof(M_qorvalkem242_t1216312090, ____dipiwaw_9)); }
	inline int32_t get__dipiwaw_9() const { return ____dipiwaw_9; }
	inline int32_t* get_address_of__dipiwaw_9() { return &____dipiwaw_9; }
	inline void set__dipiwaw_9(int32_t value)
	{
		____dipiwaw_9 = value;
	}

	inline static int32_t get_offset_of__jestewou_10() { return static_cast<int32_t>(offsetof(M_qorvalkem242_t1216312090, ____jestewou_10)); }
	inline float get__jestewou_10() const { return ____jestewou_10; }
	inline float* get_address_of__jestewou_10() { return &____jestewou_10; }
	inline void set__jestewou_10(float value)
	{
		____jestewou_10 = value;
	}

	inline static int32_t get_offset_of__poodre_11() { return static_cast<int32_t>(offsetof(M_qorvalkem242_t1216312090, ____poodre_11)); }
	inline float get__poodre_11() const { return ____poodre_11; }
	inline float* get_address_of__poodre_11() { return &____poodre_11; }
	inline void set__poodre_11(float value)
	{
		____poodre_11 = value;
	}

	inline static int32_t get_offset_of__nallwerelou_12() { return static_cast<int32_t>(offsetof(M_qorvalkem242_t1216312090, ____nallwerelou_12)); }
	inline int32_t get__nallwerelou_12() const { return ____nallwerelou_12; }
	inline int32_t* get_address_of__nallwerelou_12() { return &____nallwerelou_12; }
	inline void set__nallwerelou_12(int32_t value)
	{
		____nallwerelou_12 = value;
	}

	inline static int32_t get_offset_of__seesir_13() { return static_cast<int32_t>(offsetof(M_qorvalkem242_t1216312090, ____seesir_13)); }
	inline uint32_t get__seesir_13() const { return ____seesir_13; }
	inline uint32_t* get_address_of__seesir_13() { return &____seesir_13; }
	inline void set__seesir_13(uint32_t value)
	{
		____seesir_13 = value;
	}

	inline static int32_t get_offset_of__pecavem_14() { return static_cast<int32_t>(offsetof(M_qorvalkem242_t1216312090, ____pecavem_14)); }
	inline String_t* get__pecavem_14() const { return ____pecavem_14; }
	inline String_t** get_address_of__pecavem_14() { return &____pecavem_14; }
	inline void set__pecavem_14(String_t* value)
	{
		____pecavem_14 = value;
		Il2CppCodeGenWriteBarrier(&____pecavem_14, value);
	}

	inline static int32_t get_offset_of__deroocasTikerefur_15() { return static_cast<int32_t>(offsetof(M_qorvalkem242_t1216312090, ____deroocasTikerefur_15)); }
	inline uint32_t get__deroocasTikerefur_15() const { return ____deroocasTikerefur_15; }
	inline uint32_t* get_address_of__deroocasTikerefur_15() { return &____deroocasTikerefur_15; }
	inline void set__deroocasTikerefur_15(uint32_t value)
	{
		____deroocasTikerefur_15 = value;
	}

	inline static int32_t get_offset_of__herehaw_16() { return static_cast<int32_t>(offsetof(M_qorvalkem242_t1216312090, ____herehaw_16)); }
	inline float get__herehaw_16() const { return ____herehaw_16; }
	inline float* get_address_of__herehaw_16() { return &____herehaw_16; }
	inline void set__herehaw_16(float value)
	{
		____herehaw_16 = value;
	}

	inline static int32_t get_offset_of__jabalem_17() { return static_cast<int32_t>(offsetof(M_qorvalkem242_t1216312090, ____jabalem_17)); }
	inline uint32_t get__jabalem_17() const { return ____jabalem_17; }
	inline uint32_t* get_address_of__jabalem_17() { return &____jabalem_17; }
	inline void set__jabalem_17(uint32_t value)
	{
		____jabalem_17 = value;
	}

	inline static int32_t get_offset_of__jiberluDrowracel_18() { return static_cast<int32_t>(offsetof(M_qorvalkem242_t1216312090, ____jiberluDrowracel_18)); }
	inline int32_t get__jiberluDrowracel_18() const { return ____jiberluDrowracel_18; }
	inline int32_t* get_address_of__jiberluDrowracel_18() { return &____jiberluDrowracel_18; }
	inline void set__jiberluDrowracel_18(int32_t value)
	{
		____jiberluDrowracel_18 = value;
	}

	inline static int32_t get_offset_of__ceecair_19() { return static_cast<int32_t>(offsetof(M_qorvalkem242_t1216312090, ____ceecair_19)); }
	inline bool get__ceecair_19() const { return ____ceecair_19; }
	inline bool* get_address_of__ceecair_19() { return &____ceecair_19; }
	inline void set__ceecair_19(bool value)
	{
		____ceecair_19 = value;
	}

	inline static int32_t get_offset_of__gaveZouzacer_20() { return static_cast<int32_t>(offsetof(M_qorvalkem242_t1216312090, ____gaveZouzacer_20)); }
	inline String_t* get__gaveZouzacer_20() const { return ____gaveZouzacer_20; }
	inline String_t** get_address_of__gaveZouzacer_20() { return &____gaveZouzacer_20; }
	inline void set__gaveZouzacer_20(String_t* value)
	{
		____gaveZouzacer_20 = value;
		Il2CppCodeGenWriteBarrier(&____gaveZouzacer_20, value);
	}

	inline static int32_t get_offset_of__dalllay_21() { return static_cast<int32_t>(offsetof(M_qorvalkem242_t1216312090, ____dalllay_21)); }
	inline float get__dalllay_21() const { return ____dalllay_21; }
	inline float* get_address_of__dalllay_21() { return &____dalllay_21; }
	inline void set__dalllay_21(float value)
	{
		____dalllay_21 = value;
	}

	inline static int32_t get_offset_of__jurpuser_22() { return static_cast<int32_t>(offsetof(M_qorvalkem242_t1216312090, ____jurpuser_22)); }
	inline int32_t get__jurpuser_22() const { return ____jurpuser_22; }
	inline int32_t* get_address_of__jurpuser_22() { return &____jurpuser_22; }
	inline void set__jurpuser_22(int32_t value)
	{
		____jurpuser_22 = value;
	}

	inline static int32_t get_offset_of__cemaselDrarmel_23() { return static_cast<int32_t>(offsetof(M_qorvalkem242_t1216312090, ____cemaselDrarmel_23)); }
	inline int32_t get__cemaselDrarmel_23() const { return ____cemaselDrarmel_23; }
	inline int32_t* get_address_of__cemaselDrarmel_23() { return &____cemaselDrarmel_23; }
	inline void set__cemaselDrarmel_23(int32_t value)
	{
		____cemaselDrarmel_23 = value;
	}

	inline static int32_t get_offset_of__solirgo_24() { return static_cast<int32_t>(offsetof(M_qorvalkem242_t1216312090, ____solirgo_24)); }
	inline String_t* get__solirgo_24() const { return ____solirgo_24; }
	inline String_t** get_address_of__solirgo_24() { return &____solirgo_24; }
	inline void set__solirgo_24(String_t* value)
	{
		____solirgo_24 = value;
		Il2CppCodeGenWriteBarrier(&____solirgo_24, value);
	}

	inline static int32_t get_offset_of__kaircerewhe_25() { return static_cast<int32_t>(offsetof(M_qorvalkem242_t1216312090, ____kaircerewhe_25)); }
	inline String_t* get__kaircerewhe_25() const { return ____kaircerewhe_25; }
	inline String_t** get_address_of__kaircerewhe_25() { return &____kaircerewhe_25; }
	inline void set__kaircerewhe_25(String_t* value)
	{
		____kaircerewhe_25 = value;
		Il2CppCodeGenWriteBarrier(&____kaircerewhe_25, value);
	}

	inline static int32_t get_offset_of__towstumee_26() { return static_cast<int32_t>(offsetof(M_qorvalkem242_t1216312090, ____towstumee_26)); }
	inline uint32_t get__towstumee_26() const { return ____towstumee_26; }
	inline uint32_t* get_address_of__towstumee_26() { return &____towstumee_26; }
	inline void set__towstumee_26(uint32_t value)
	{
		____towstumee_26 = value;
	}

	inline static int32_t get_offset_of__tecere_27() { return static_cast<int32_t>(offsetof(M_qorvalkem242_t1216312090, ____tecere_27)); }
	inline uint32_t get__tecere_27() const { return ____tecere_27; }
	inline uint32_t* get_address_of__tecere_27() { return &____tecere_27; }
	inline void set__tecere_27(uint32_t value)
	{
		____tecere_27 = value;
	}

	inline static int32_t get_offset_of__fasereKerraipe_28() { return static_cast<int32_t>(offsetof(M_qorvalkem242_t1216312090, ____fasereKerraipe_28)); }
	inline uint32_t get__fasereKerraipe_28() const { return ____fasereKerraipe_28; }
	inline uint32_t* get_address_of__fasereKerraipe_28() { return &____fasereKerraipe_28; }
	inline void set__fasereKerraipe_28(uint32_t value)
	{
		____fasereKerraipe_28 = value;
	}

	inline static int32_t get_offset_of__juxeCerju_29() { return static_cast<int32_t>(offsetof(M_qorvalkem242_t1216312090, ____juxeCerju_29)); }
	inline bool get__juxeCerju_29() const { return ____juxeCerju_29; }
	inline bool* get_address_of__juxeCerju_29() { return &____juxeCerju_29; }
	inline void set__juxeCerju_29(bool value)
	{
		____juxeCerju_29 = value;
	}

	inline static int32_t get_offset_of__relpastemHalda_30() { return static_cast<int32_t>(offsetof(M_qorvalkem242_t1216312090, ____relpastemHalda_30)); }
	inline uint32_t get__relpastemHalda_30() const { return ____relpastemHalda_30; }
	inline uint32_t* get_address_of__relpastemHalda_30() { return &____relpastemHalda_30; }
	inline void set__relpastemHalda_30(uint32_t value)
	{
		____relpastemHalda_30 = value;
	}

	inline static int32_t get_offset_of__lalldeetral_31() { return static_cast<int32_t>(offsetof(M_qorvalkem242_t1216312090, ____lalldeetral_31)); }
	inline bool get__lalldeetral_31() const { return ____lalldeetral_31; }
	inline bool* get_address_of__lalldeetral_31() { return &____lalldeetral_31; }
	inline void set__lalldeetral_31(bool value)
	{
		____lalldeetral_31 = value;
	}

	inline static int32_t get_offset_of__latemlaMaizas_32() { return static_cast<int32_t>(offsetof(M_qorvalkem242_t1216312090, ____latemlaMaizas_32)); }
	inline int32_t get__latemlaMaizas_32() const { return ____latemlaMaizas_32; }
	inline int32_t* get_address_of__latemlaMaizas_32() { return &____latemlaMaizas_32; }
	inline void set__latemlaMaizas_32(int32_t value)
	{
		____latemlaMaizas_32 = value;
	}

	inline static int32_t get_offset_of__rooteaxal_33() { return static_cast<int32_t>(offsetof(M_qorvalkem242_t1216312090, ____rooteaxal_33)); }
	inline int32_t get__rooteaxal_33() const { return ____rooteaxal_33; }
	inline int32_t* get_address_of__rooteaxal_33() { return &____rooteaxal_33; }
	inline void set__rooteaxal_33(int32_t value)
	{
		____rooteaxal_33 = value;
	}

	inline static int32_t get_offset_of__bawsear_34() { return static_cast<int32_t>(offsetof(M_qorvalkem242_t1216312090, ____bawsear_34)); }
	inline int32_t get__bawsear_34() const { return ____bawsear_34; }
	inline int32_t* get_address_of__bawsear_34() { return &____bawsear_34; }
	inline void set__bawsear_34(int32_t value)
	{
		____bawsear_34 = value;
	}

	inline static int32_t get_offset_of__vurdayhiJallcelmo_35() { return static_cast<int32_t>(offsetof(M_qorvalkem242_t1216312090, ____vurdayhiJallcelmo_35)); }
	inline float get__vurdayhiJallcelmo_35() const { return ____vurdayhiJallcelmo_35; }
	inline float* get_address_of__vurdayhiJallcelmo_35() { return &____vurdayhiJallcelmo_35; }
	inline void set__vurdayhiJallcelmo_35(float value)
	{
		____vurdayhiJallcelmo_35 = value;
	}

	inline static int32_t get_offset_of__rirlealal_36() { return static_cast<int32_t>(offsetof(M_qorvalkem242_t1216312090, ____rirlealal_36)); }
	inline int32_t get__rirlealal_36() const { return ____rirlealal_36; }
	inline int32_t* get_address_of__rirlealal_36() { return &____rirlealal_36; }
	inline void set__rirlealal_36(int32_t value)
	{
		____rirlealal_36 = value;
	}

	inline static int32_t get_offset_of__cacay_37() { return static_cast<int32_t>(offsetof(M_qorvalkem242_t1216312090, ____cacay_37)); }
	inline uint32_t get__cacay_37() const { return ____cacay_37; }
	inline uint32_t* get_address_of__cacay_37() { return &____cacay_37; }
	inline void set__cacay_37(uint32_t value)
	{
		____cacay_37 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
