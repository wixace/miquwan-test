﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/KeyCollection<Entity.Behavior.EBehaviorID,System.Object>
struct KeyCollection_t2937880697;
// System.Collections.Generic.Dictionary`2<Entity.Behavior.EBehaviorID,System.Object>
struct Dictionary_2_t1311121246;
// System.Collections.Generic.IEnumerator`1<Entity.Behavior.EBehaviorID>
struct IEnumerator_1_t3150398185;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// Entity.Behavior.EBehaviorID[]
struct EBehaviorIDU5BU5D_t373889457;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Entity_Behavior_EBehaviorID1238533136.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K1926057300.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Entity.Behavior.EBehaviorID,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void KeyCollection__ctor_m4280222283_gshared (KeyCollection_t2937880697 * __this, Dictionary_2_t1311121246 * ___dictionary0, const MethodInfo* method);
#define KeyCollection__ctor_m4280222283(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t2937880697 *, Dictionary_2_t1311121246 *, const MethodInfo*))KeyCollection__ctor_m4280222283_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Entity.Behavior.EBehaviorID,System.Object>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m94032939_gshared (KeyCollection_t2937880697 * __this, uint8_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m94032939(__this, ___item0, method) ((  void (*) (KeyCollection_t2937880697 *, uint8_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m94032939_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Entity.Behavior.EBehaviorID,System.Object>::System.Collections.Generic.ICollection<TKey>.Clear()
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3763210338_gshared (KeyCollection_t2937880697 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3763210338(__this, method) ((  void (*) (KeyCollection_t2937880697 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3763210338_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Entity.Behavior.EBehaviorID,System.Object>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m2819840319_gshared (KeyCollection_t2937880697 * __this, uint8_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m2819840319(__this, ___item0, method) ((  bool (*) (KeyCollection_t2937880697 *, uint8_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m2819840319_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Entity.Behavior.EBehaviorID,System.Object>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3784511076_gshared (KeyCollection_t2937880697 * __this, uint8_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3784511076(__this, ___item0, method) ((  bool (*) (KeyCollection_t2937880697 *, uint8_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3784511076_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<Entity.Behavior.EBehaviorID,System.Object>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m3401802590_gshared (KeyCollection_t2937880697 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m3401802590(__this, method) ((  Il2CppObject* (*) (KeyCollection_t2937880697 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m3401802590_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Entity.Behavior.EBehaviorID,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m34367956_gshared (KeyCollection_t2937880697 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_CopyTo_m34367956(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t2937880697 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m34367956_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<Entity.Behavior.EBehaviorID,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1556449231_gshared (KeyCollection_t2937880697 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1556449231(__this, method) ((  Il2CppObject * (*) (KeyCollection_t2937880697 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1556449231_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Entity.Behavior.EBehaviorID,System.Object>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m2623241440_gshared (KeyCollection_t2937880697 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m2623241440(__this, method) ((  bool (*) (KeyCollection_t2937880697 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m2623241440_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Entity.Behavior.EBehaviorID,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m3213499794_gshared (KeyCollection_t2937880697 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m3213499794(__this, method) ((  bool (*) (KeyCollection_t2937880697 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m3213499794_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<Entity.Behavior.EBehaviorID,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m4228299518_gshared (KeyCollection_t2937880697 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m4228299518(__this, method) ((  Il2CppObject * (*) (KeyCollection_t2937880697 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m4228299518_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Entity.Behavior.EBehaviorID,System.Object>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m4262483200_gshared (KeyCollection_t2937880697 * __this, EBehaviorIDU5BU5D_t373889457* ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_CopyTo_m4262483200(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t2937880697 *, EBehaviorIDU5BU5D_t373889457*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m4262483200_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<Entity.Behavior.EBehaviorID,System.Object>::GetEnumerator()
extern "C"  Enumerator_t1926057300  KeyCollection_GetEnumerator_m2706716579_gshared (KeyCollection_t2937880697 * __this, const MethodInfo* method);
#define KeyCollection_GetEnumerator_m2706716579(__this, method) ((  Enumerator_t1926057300  (*) (KeyCollection_t2937880697 *, const MethodInfo*))KeyCollection_GetEnumerator_m2706716579_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<Entity.Behavior.EBehaviorID,System.Object>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m746767064_gshared (KeyCollection_t2937880697 * __this, const MethodInfo* method);
#define KeyCollection_get_Count_m746767064(__this, method) ((  int32_t (*) (KeyCollection_t2937880697 *, const MethodInfo*))KeyCollection_get_Count_m746767064_gshared)(__this, method)
