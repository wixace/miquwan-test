﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// npcAICfg
struct npcAICfg_t1948330203;
// System.Int32[]
struct Int32U5BU5D_t3230847821;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AIObject
struct  AIObject_t37280135  : public Il2CppObject
{
public:
	// npcAICfg AIObject::mpJson
	npcAICfg_t1948330203 * ___mpJson_0;
	// System.Int32 AIObject::id
	int32_t ___id_1;
	// System.Int32[] AIObject::behaviorID
	Int32U5BU5D_t3230847821* ___behaviorID_2;
	// System.Int32[] AIObject::conditionID
	Int32U5BU5D_t3230847821* ___conditionID_3;
	// System.Single AIObject::delayStartTime
	float ___delayStartTime_4;
	// System.Single AIObject::delayTime
	float ___delayTime_5;
	// System.Int32 AIObject::delayID
	int32_t ___delayID_6;
	// System.Single AIObject::mLastUseTime
	float ___mLastUseTime_7;
	// System.Int32 AIObject::curCount
	int32_t ___curCount_8;

public:
	inline static int32_t get_offset_of_mpJson_0() { return static_cast<int32_t>(offsetof(AIObject_t37280135, ___mpJson_0)); }
	inline npcAICfg_t1948330203 * get_mpJson_0() const { return ___mpJson_0; }
	inline npcAICfg_t1948330203 ** get_address_of_mpJson_0() { return &___mpJson_0; }
	inline void set_mpJson_0(npcAICfg_t1948330203 * value)
	{
		___mpJson_0 = value;
		Il2CppCodeGenWriteBarrier(&___mpJson_0, value);
	}

	inline static int32_t get_offset_of_id_1() { return static_cast<int32_t>(offsetof(AIObject_t37280135, ___id_1)); }
	inline int32_t get_id_1() const { return ___id_1; }
	inline int32_t* get_address_of_id_1() { return &___id_1; }
	inline void set_id_1(int32_t value)
	{
		___id_1 = value;
	}

	inline static int32_t get_offset_of_behaviorID_2() { return static_cast<int32_t>(offsetof(AIObject_t37280135, ___behaviorID_2)); }
	inline Int32U5BU5D_t3230847821* get_behaviorID_2() const { return ___behaviorID_2; }
	inline Int32U5BU5D_t3230847821** get_address_of_behaviorID_2() { return &___behaviorID_2; }
	inline void set_behaviorID_2(Int32U5BU5D_t3230847821* value)
	{
		___behaviorID_2 = value;
		Il2CppCodeGenWriteBarrier(&___behaviorID_2, value);
	}

	inline static int32_t get_offset_of_conditionID_3() { return static_cast<int32_t>(offsetof(AIObject_t37280135, ___conditionID_3)); }
	inline Int32U5BU5D_t3230847821* get_conditionID_3() const { return ___conditionID_3; }
	inline Int32U5BU5D_t3230847821** get_address_of_conditionID_3() { return &___conditionID_3; }
	inline void set_conditionID_3(Int32U5BU5D_t3230847821* value)
	{
		___conditionID_3 = value;
		Il2CppCodeGenWriteBarrier(&___conditionID_3, value);
	}

	inline static int32_t get_offset_of_delayStartTime_4() { return static_cast<int32_t>(offsetof(AIObject_t37280135, ___delayStartTime_4)); }
	inline float get_delayStartTime_4() const { return ___delayStartTime_4; }
	inline float* get_address_of_delayStartTime_4() { return &___delayStartTime_4; }
	inline void set_delayStartTime_4(float value)
	{
		___delayStartTime_4 = value;
	}

	inline static int32_t get_offset_of_delayTime_5() { return static_cast<int32_t>(offsetof(AIObject_t37280135, ___delayTime_5)); }
	inline float get_delayTime_5() const { return ___delayTime_5; }
	inline float* get_address_of_delayTime_5() { return &___delayTime_5; }
	inline void set_delayTime_5(float value)
	{
		___delayTime_5 = value;
	}

	inline static int32_t get_offset_of_delayID_6() { return static_cast<int32_t>(offsetof(AIObject_t37280135, ___delayID_6)); }
	inline int32_t get_delayID_6() const { return ___delayID_6; }
	inline int32_t* get_address_of_delayID_6() { return &___delayID_6; }
	inline void set_delayID_6(int32_t value)
	{
		___delayID_6 = value;
	}

	inline static int32_t get_offset_of_mLastUseTime_7() { return static_cast<int32_t>(offsetof(AIObject_t37280135, ___mLastUseTime_7)); }
	inline float get_mLastUseTime_7() const { return ___mLastUseTime_7; }
	inline float* get_address_of_mLastUseTime_7() { return &___mLastUseTime_7; }
	inline void set_mLastUseTime_7(float value)
	{
		___mLastUseTime_7 = value;
	}

	inline static int32_t get_offset_of_curCount_8() { return static_cast<int32_t>(offsetof(AIObject_t37280135, ___curCount_8)); }
	inline int32_t get_curCount_8() const { return ___curCount_8; }
	inline int32_t* get_address_of_curCount_8() { return &___curCount_8; }
	inline void set_curCount_8(int32_t value)
	{
		___curCount_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
