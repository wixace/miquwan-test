﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Linq.JTokenWriter
struct JTokenWriter_t3702364242;
// Newtonsoft.Json.Linq.JContainer
struct JContainer_t3364442311;
// Newtonsoft.Json.Linq.JToken
struct JToken_t3412245951;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// Newtonsoft.Json.Linq.JValue
struct JValue_t3413677367;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// SerializableGuid
struct SerializableGuid_t3998617672;
// System.Uri
struct Uri_t1116831938;
// Newtonsoft.Json.JsonWriter
struct JsonWriter_t972330355;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JContainer3364442311.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonToken4173078175.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JValue3413677367.h"
#include "mscorlib_System_Decimal1954350631.h"
#include "mscorlib_System_DateTime4283661327.h"
#include "mscorlib_System_DateTimeOffset3884714306.h"
#include "mscorlib_System_TimeSpan413522987.h"
#include "mscorlib_System_Guid2862754429.h"
#include "AssemblyU2DCSharp_SerializableGuid3998617672.h"
#include "System_System_Uri1116831938.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonWriter972330355.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JTokenWrite3702364242.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JToken3412245951.h"

// System.Void Newtonsoft.Json.Linq.JTokenWriter::.ctor(Newtonsoft.Json.Linq.JContainer)
extern "C"  void JTokenWriter__ctor_m1907918130 (JTokenWriter_t3702364242 * __this, JContainer_t3364442311 * ___container0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JTokenWriter::.ctor()
extern "C"  void JTokenWriter__ctor_m4075727984 (JTokenWriter_t3702364242 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JTokenWriter::get_Token()
extern "C"  JToken_t3412245951 * JTokenWriter_get_Token_m2502113641 (JTokenWriter_t3702364242 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JTokenWriter::Flush()
extern "C"  void JTokenWriter_Flush_m4159675282 (JTokenWriter_t3702364242 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JTokenWriter::Close()
extern "C"  void JTokenWriter_Close_m1491620230 (JTokenWriter_t3702364242 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JTokenWriter::WriteStartObject()
extern "C"  void JTokenWriter_WriteStartObject_m3471040182 (JTokenWriter_t3702364242 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JTokenWriter::AddParent(Newtonsoft.Json.Linq.JContainer)
extern "C"  void JTokenWriter_AddParent_m4063243145 (JTokenWriter_t3702364242 * __this, JContainer_t3364442311 * ___container0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JTokenWriter::RemoveParent()
extern "C"  void JTokenWriter_RemoveParent_m2023771810 (JTokenWriter_t3702364242 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JTokenWriter::WriteStartArray()
extern "C"  void JTokenWriter_WriteStartArray_m3392478244 (JTokenWriter_t3702364242 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JTokenWriter::WriteStartConstructor(System.String)
extern "C"  void JTokenWriter_WriteStartConstructor_m954704157 (JTokenWriter_t3702364242 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JTokenWriter::WriteEnd(Newtonsoft.Json.JsonToken)
extern "C"  void JTokenWriter_WriteEnd_m4106799846 (JTokenWriter_t3702364242 * __this, int32_t ___token0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JTokenWriter::WritePropertyName(System.String)
extern "C"  void JTokenWriter_WritePropertyName_m4061348437 (JTokenWriter_t3702364242 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JTokenWriter::AddValue(System.Object,Newtonsoft.Json.JsonToken)
extern "C"  void JTokenWriter_AddValue_m2425355104 (JTokenWriter_t3702364242 * __this, Il2CppObject * ___value0, int32_t ___token1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JTokenWriter::AddValue(Newtonsoft.Json.Linq.JValue,Newtonsoft.Json.JsonToken)
extern "C"  void JTokenWriter_AddValue_m3994850558 (JTokenWriter_t3702364242 * __this, JValue_t3413677367 * ___value0, int32_t ___token1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JTokenWriter::WriteNull()
extern "C"  void JTokenWriter_WriteNull_m2966365076 (JTokenWriter_t3702364242 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JTokenWriter::WriteUndefined()
extern "C"  void JTokenWriter_WriteUndefined_m1558516485 (JTokenWriter_t3702364242 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JTokenWriter::WriteRaw(System.String)
extern "C"  void JTokenWriter_WriteRaw_m3711189829 (JTokenWriter_t3702364242 * __this, String_t* ___json0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JTokenWriter::WriteComment(System.String)
extern "C"  void JTokenWriter_WriteComment_m2284368430 (JTokenWriter_t3702364242 * __this, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JTokenWriter::WriteValue(System.String)
extern "C"  void JTokenWriter_WriteValue_m4151610332 (JTokenWriter_t3702364242 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JTokenWriter::WriteValue(System.Int32)
extern "C"  void JTokenWriter_WriteValue_m257738039 (JTokenWriter_t3702364242 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JTokenWriter::WriteValue(System.UInt32)
extern "C"  void JTokenWriter_WriteValue_m397172180 (JTokenWriter_t3702364242 * __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JTokenWriter::WriteValue(System.Int64)
extern "C"  void JTokenWriter_WriteValue_m257740984 (JTokenWriter_t3702364242 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JTokenWriter::WriteValue(System.UInt64)
extern "C"  void JTokenWriter_WriteValue_m397175125 (JTokenWriter_t3702364242 * __this, uint64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JTokenWriter::WriteValue(System.Single)
extern "C"  void JTokenWriter_WriteValue_m3832934021 (JTokenWriter_t3702364242 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JTokenWriter::WriteValue(System.Double)
extern "C"  void JTokenWriter_WriteValue_m3583371292 (JTokenWriter_t3702364242 * __this, double ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JTokenWriter::WriteValue(System.Boolean)
extern "C"  void JTokenWriter_WriteValue_m61957085 (JTokenWriter_t3702364242 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JTokenWriter::WriteValue(System.Int16)
extern "C"  void JTokenWriter_WriteValue_m257736241 (JTokenWriter_t3702364242 * __this, int16_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JTokenWriter::WriteValue(System.UInt16)
extern "C"  void JTokenWriter_WriteValue_m397170382 (JTokenWriter_t3702364242 * __this, uint16_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JTokenWriter::WriteValue(System.Char)
extern "C"  void JTokenWriter_WriteValue_m1110956599 (JTokenWriter_t3702364242 * __this, Il2CppChar ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JTokenWriter::WriteValue(System.Byte)
extern "C"  void JTokenWriter_WriteValue_m1110557381 (JTokenWriter_t3702364242 * __this, uint8_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JTokenWriter::WriteValue(System.SByte)
extern "C"  void JTokenWriter_WriteValue_m503607626 (JTokenWriter_t3702364242 * __this, int8_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JTokenWriter::WriteValue(System.Decimal)
extern "C"  void JTokenWriter_WriteValue_m2916393428 (JTokenWriter_t3702364242 * __this, Decimal_t1954350631  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JTokenWriter::WriteValue(System.DateTime)
extern "C"  void JTokenWriter_WriteValue_m3897873554 (JTokenWriter_t3702364242 * __this, DateTime_t4283661327  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JTokenWriter::WriteValue(System.DateTimeOffset)
extern "C"  void JTokenWriter_WriteValue_m3150047103 (JTokenWriter_t3702364242 * __this, DateTimeOffset_t3884714306  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JTokenWriter::WriteValue(System.Byte[])
extern "C"  void JTokenWriter_WriteValue_m2093804707 (JTokenWriter_t3702364242 * __this, ByteU5BU5D_t4260760469* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JTokenWriter::WriteValue(System.TimeSpan)
extern "C"  void JTokenWriter_WriteValue_m4182669302 (JTokenWriter_t3702364242 * __this, TimeSpan_t413522987  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JTokenWriter::WriteValue(System.Guid)
extern "C"  void JTokenWriter_WriteValue_m1115045220 (JTokenWriter_t3702364242 * __this, Guid_t2862754429  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JTokenWriter::WriteValue(SerializableGuid)
extern "C"  void JTokenWriter_WriteValue_m3398440862 (JTokenWriter_t3702364242 * __this, SerializableGuid_t3998617672 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JTokenWriter::WriteValue(System.Uri)
extern "C"  void JTokenWriter_WriteValue_m1837498649 (JTokenWriter_t3702364242 * __this, Uri_t1116831938 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JTokenWriter::ilo_Add1(Newtonsoft.Json.Linq.JContainer,System.Object)
extern "C"  void JTokenWriter_ilo_Add1_m862285657 (Il2CppObject * __this /* static, unused */, JContainer_t3364442311 * ____this0, Il2CppObject * ___content1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JTokenWriter::ilo_WriteStartArray2(Newtonsoft.Json.JsonWriter)
extern "C"  void JTokenWriter_ilo_WriteStartArray2_m886722195 (Il2CppObject * __this /* static, unused */, JsonWriter_t972330355 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JTokenWriter::ilo_AddParent3(Newtonsoft.Json.Linq.JTokenWriter,Newtonsoft.Json.Linq.JContainer)
extern "C"  void JTokenWriter_ilo_AddParent3_m2822107076 (Il2CppObject * __this /* static, unused */, JTokenWriter_t3702364242 * ____this0, JContainer_t3364442311 * ___container1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JContainer Newtonsoft.Json.Linq.JTokenWriter::ilo_get_Parent4(Newtonsoft.Json.Linq.JToken)
extern "C"  JContainer_t3364442311 * JTokenWriter_ilo_get_Parent4_m425501035 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JTokenWriter::ilo_WriteUndefined5(Newtonsoft.Json.JsonWriter)
extern "C"  void JTokenWriter_ilo_WriteUndefined5_m2200671695 (Il2CppObject * __this /* static, unused */, JsonWriter_t972330355 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JTokenWriter::ilo_AddValue6(Newtonsoft.Json.Linq.JTokenWriter,Newtonsoft.Json.Linq.JValue,Newtonsoft.Json.JsonToken)
extern "C"  void JTokenWriter_ilo_AddValue6_m3821325138 (Il2CppObject * __this /* static, unused */, JTokenWriter_t3702364242 * ____this0, JValue_t3413677367 * ___value1, int32_t ___token2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JTokenWriter::ilo_WriteValue7(Newtonsoft.Json.JsonWriter,System.Int32)
extern "C"  void JTokenWriter_ilo_WriteValue7_m504552455 (Il2CppObject * __this /* static, unused */, JsonWriter_t972330355 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JTokenWriter::ilo_AddValue8(Newtonsoft.Json.Linq.JTokenWriter,System.Object,Newtonsoft.Json.JsonToken)
extern "C"  void JTokenWriter_ilo_AddValue8_m1690791794 (Il2CppObject * __this /* static, unused */, JTokenWriter_t3702364242 * ____this0, Il2CppObject * ___value1, int32_t ___token2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JTokenWriter::ilo_WriteValue9(Newtonsoft.Json.JsonWriter,System.Int64)
extern "C"  void JTokenWriter_ilo_WriteValue9_m752701894 (Il2CppObject * __this /* static, unused */, JsonWriter_t972330355 * ____this0, int64_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JTokenWriter::ilo_WriteValue10(Newtonsoft.Json.JsonWriter,System.UInt64)
extern "C"  void JTokenWriter_ilo_WriteValue10_m3748794269 (Il2CppObject * __this /* static, unused */, JsonWriter_t972330355 * ____this0, uint64_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JTokenWriter::ilo_WriteValue11(Newtonsoft.Json.JsonWriter,System.Double)
extern "C"  void JTokenWriter_ilo_WriteValue11_m2191326501 (Il2CppObject * __this /* static, unused */, JsonWriter_t972330355 * ____this0, double ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JTokenWriter::ilo_WriteValue12(Newtonsoft.Json.JsonWriter,System.UInt16)
extern "C"  void JTokenWriter_ilo_WriteValue12_m2851396248 (Il2CppObject * __this /* static, unused */, JsonWriter_t972330355 * ____this0, uint16_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JTokenWriter::ilo_WriteValue13(Newtonsoft.Json.JsonWriter,System.Char)
extern "C"  void JTokenWriter_ilo_WriteValue13_m2283991938 (Il2CppObject * __this /* static, unused */, JsonWriter_t972330355 * ____this0, Il2CppChar ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JTokenWriter::ilo_WriteValue14(Newtonsoft.Json.JsonWriter,System.Byte)
extern "C"  void JTokenWriter_ilo_WriteValue14_m347932433 (Il2CppObject * __this /* static, unused */, JsonWriter_t972330355 * ____this0, uint8_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JTokenWriter::ilo_WriteValue15(Newtonsoft.Json.JsonWriter,System.SByte)
extern "C"  void JTokenWriter_ilo_WriteValue15_m2756111261 (Il2CppObject * __this /* static, unused */, JsonWriter_t972330355 * ____this0, int8_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JTokenWriter::ilo_WriteValue16(Newtonsoft.Json.JsonWriter,System.Decimal)
extern "C"  void JTokenWriter_ilo_WriteValue16_m1884175558 (Il2CppObject * __this /* static, unused */, JsonWriter_t972330355 * ____this0, Decimal_t1954350631  ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JTokenWriter::ilo_WriteValue17(Newtonsoft.Json.JsonWriter,System.DateTimeOffset)
extern "C"  void JTokenWriter_ilo_WriteValue17_m3666777614 (Il2CppObject * __this /* static, unused */, JsonWriter_t972330355 * ____this0, DateTimeOffset_t3884714306  ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JTokenWriter::ilo_WriteValue18(Newtonsoft.Json.JsonWriter,System.Byte[])
extern "C"  void JTokenWriter_ilo_WriteValue18_m1855850739 (Il2CppObject * __this /* static, unused */, JsonWriter_t972330355 * ____this0, ByteU5BU5D_t4260760469* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
