﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_LineRendererGenerated
struct UnityEngine_LineRendererGenerated_t2631402000;
// JSVCall
struct JSVCall_t3708497963;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t215400611;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void UnityEngine_LineRendererGenerated::.ctor()
extern "C"  void UnityEngine_LineRendererGenerated__ctor_m3500027227 (UnityEngine_LineRendererGenerated_t2631402000 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_LineRendererGenerated::LineRenderer_LineRenderer1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_LineRendererGenerated_LineRenderer_LineRenderer1_m4274333335 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_LineRendererGenerated::LineRenderer_useWorldSpace(JSVCall)
extern "C"  void UnityEngine_LineRendererGenerated_LineRenderer_useWorldSpace_m440735819 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_LineRendererGenerated::LineRenderer_SetColors__Color__Color(JSVCall,System.Int32)
extern "C"  bool UnityEngine_LineRendererGenerated_LineRenderer_SetColors__Color__Color_m3368282863 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_LineRendererGenerated::LineRenderer_SetPosition__Int32__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_LineRendererGenerated_LineRenderer_SetPosition__Int32__Vector3_m2676128074 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_LineRendererGenerated::LineRenderer_SetPositions__Vector3_Array(JSVCall,System.Int32)
extern "C"  bool UnityEngine_LineRendererGenerated_LineRenderer_SetPositions__Vector3_Array_m1026889407 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_LineRendererGenerated::LineRenderer_SetVertexCount__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_LineRendererGenerated_LineRenderer_SetVertexCount__Int32_m2565861506 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_LineRendererGenerated::LineRenderer_SetWidth__Single__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_LineRendererGenerated_LineRenderer_SetWidth__Single__Single_m574113497 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_LineRendererGenerated::__Register()
extern "C"  void UnityEngine_LineRendererGenerated___Register_m2904584268 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3[] UnityEngine_LineRendererGenerated::<LineRenderer_SetPositions__Vector3_Array>m__261()
extern "C"  Vector3U5BU5D_t215400611* UnityEngine_LineRendererGenerated_U3CLineRenderer_SetPositions__Vector3_ArrayU3Em__261_m1951739033 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_LineRendererGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UnityEngine_LineRendererGenerated_ilo_getObject1_m399868731 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_LineRendererGenerated::ilo_setBooleanS2(System.Int32,System.Boolean)
extern "C"  void UnityEngine_LineRendererGenerated_ilo_setBooleanS2_m1508400939 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_LineRendererGenerated::ilo_getInt323(System.Int32)
extern "C"  int32_t UnityEngine_LineRendererGenerated_ilo_getInt323_m2597882752 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine_LineRendererGenerated::ilo_getVector3S4(System.Int32)
extern "C"  Vector3_t4282066566  UnityEngine_LineRendererGenerated_ilo_getVector3S4_m4035826190 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine_LineRendererGenerated::ilo_getSingle5(System.Int32)
extern "C"  float UnityEngine_LineRendererGenerated_ilo_getSingle5_m2716048792 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_LineRendererGenerated::ilo_getArrayLength6(System.Int32)
extern "C"  int32_t UnityEngine_LineRendererGenerated_ilo_getArrayLength6_m694011730 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
