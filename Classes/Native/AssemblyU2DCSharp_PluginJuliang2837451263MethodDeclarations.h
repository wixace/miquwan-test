﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PluginJuliang
struct PluginJuliang_t2837451263;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// System.String
struct String_t;
// VersionMgr
struct VersionMgr_t1322950208;
// PluginsSdkMgr
struct PluginsSdkMgr_t3884624670;
// System.Object
struct Il2CppObject;
// System.Action
struct Action_t3771233898;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_PluginJuliang2837451263.h"
#include "AssemblyU2DCSharp_VersionMgr1322950208.h"
#include "mscorlib_System_Object4170816371.h"
#include "System_Core_System_Action3771233898.h"

// System.Void PluginJuliang::.ctor()
extern "C"  void PluginJuliang__ctor_m3074760268 (PluginJuliang_t2837451263 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJuliang::Init()
extern "C"  void PluginJuliang_Init_m50722440 (PluginJuliang_t2837451263 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginJuliang::ReqSDKHttpLogin()
extern "C"  Dictionary_2_t827649927 * PluginJuliang_ReqSDKHttpLogin_m640354621 (PluginJuliang_t2837451263 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginJuliang::IsLoginSuccess(System.Boolean)
extern "C"  bool PluginJuliang_IsLoginSuccess_m1982906527 (PluginJuliang_t2837451263 * __this, bool ___isOpenUseLogin0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJuliang::OpenUserLogin()
extern "C"  void PluginJuliang_OpenUserLogin_m3847304350 (PluginJuliang_t2837451263 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJuliang::OnEnterGame(CEvent.ZEvent)
extern "C"  void PluginJuliang_OnEnterGame_m1620336870 (PluginJuliang_t2837451263 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJuliang::OnLevelUp(CEvent.ZEvent)
extern "C"  void PluginJuliang_OnLevelUp_m3830257521 (PluginJuliang_t2837451263 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJuliang::UserPay(CEvent.ZEvent)
extern "C"  void PluginJuliang_UserPay_m1560908052 (PluginJuliang_t2837451263 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJuliang::OnInitSuccess(System.String)
extern "C"  void PluginJuliang_OnInitSuccess_m2243596516 (PluginJuliang_t2837451263 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJuliang::OnInitFail(System.String)
extern "C"  void PluginJuliang_OnInitFail_m2653104861 (PluginJuliang_t2837451263 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJuliang::OnLoginSuccess(System.String)
extern "C"  void PluginJuliang_OnLoginSuccess_m1221727313 (PluginJuliang_t2837451263 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJuliang::OnLoginFail(System.String)
extern "C"  void PluginJuliang_OnLoginFail_m3566675600 (PluginJuliang_t2837451263 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJuliang::OnPaySuccess(System.String)
extern "C"  void PluginJuliang_OnPaySuccess_m3408090832 (PluginJuliang_t2837451263 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJuliang::OnPayFail(System.String)
extern "C"  void PluginJuliang_OnPayFail_m1543900273 (PluginJuliang_t2837451263 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJuliang::OnExitGameSuccess(System.String)
extern "C"  void PluginJuliang_OnExitGameSuccess_m1895441156 (PluginJuliang_t2837451263 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJuliang::LogoutSuccess()
extern "C"  void PluginJuliang_LogoutSuccess_m3798596419 (PluginJuliang_t2837451263 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJuliang::OnLogoutSuccess(System.String)
extern "C"  void PluginJuliang_OnLogoutSuccess_m3517351006 (PluginJuliang_t2837451263 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJuliang::OnLogoutFail(System.String)
extern "C"  void PluginJuliang_OnLogoutFail_m3005643171 (PluginJuliang_t2837451263 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJuliang::JLSDKInit(System.String,System.String,System.String)
extern "C"  void PluginJuliang_JLSDKInit_m3898966504 (PluginJuliang_t2837451263 * __this, String_t* ___pid0, String_t* ___scheme1, String_t* ___appKey2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJuliang::Login()
extern "C"  void PluginJuliang_Login_m4261428371 (PluginJuliang_t2837451263 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJuliang::Pay(System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginJuliang_Pay_m4050888280 (PluginJuliang_t2837451263 * __this, String_t* ___amount0, String_t* ___serverId1, String_t* ___serverName2, String_t* ___extendInfo3, String_t* ___roleId4, String_t* ___roleName5, String_t* ___productName6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJuliang::<OnLogoutSuccess>m__436()
extern "C"  void PluginJuliang_U3COnLogoutSuccessU3Em__436_m372087444 (PluginJuliang_t2837451263 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionMgr PluginJuliang::ilo_get_Instance1()
extern "C"  VersionMgr_t1322950208 * PluginJuliang_ilo_get_Instance1_m2706413863 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJuliang::ilo_JLSDKInit2(PluginJuliang,System.String,System.String,System.String)
extern "C"  void PluginJuliang_ilo_JLSDKInit2_m4018666930 (Il2CppObject * __this /* static, unused */, PluginJuliang_t2837451263 * ____this0, String_t* ___pid1, String_t* ___scheme2, String_t* ___appKey3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginJuliang::ilo_get_isSdkLogin3(VersionMgr)
extern "C"  bool PluginJuliang_ilo_get_isSdkLogin3_m584993212 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJuliang::ilo_Login4(PluginJuliang)
extern "C"  void PluginJuliang_ilo_Login4_m2616945853 (Il2CppObject * __this /* static, unused */, PluginJuliang_t2837451263 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PluginsSdkMgr PluginJuliang::ilo_get_PluginsSdkMgr5()
extern "C"  PluginsSdkMgr_t3884624670 * PluginJuliang_ilo_get_PluginsSdkMgr5_m3661700630 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJuliang::ilo_Log6(System.Object,System.Boolean)
extern "C"  void PluginJuliang_ilo_Log6_m742156486 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJuliang::ilo_DispatchEvent7(CEvent.ZEvent)
extern "C"  void PluginJuliang_ilo_DispatchEvent7_m1997536793 (Il2CppObject * __this /* static, unused */, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJuliang::ilo_Logout8(System.Action)
extern "C"  void PluginJuliang_ilo_Logout8_m2948110680 (Il2CppObject * __this /* static, unused */, Action_t3771233898 * ___call0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
