﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PluginQiZi
struct PluginQiZi_t1606345050;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// System.String
struct String_t;
// VersionMgr
struct VersionMgr_t1322950208;
// Mihua.SDK.PayInfo
struct PayInfo_t1775308120;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// ProductsCfgMgr
struct ProductsCfgMgr_t2493714872;
// Mihua.SDK.LvUpInfo
struct LvUpInfo_t411687177;
// PluginsSdkMgr
struct PluginsSdkMgr_t3884624670;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_PluginQiZi1606345050.h"
#include "AssemblyU2DCSharp_PluginsSdkMgr3884624670.h"

// System.Void PluginQiZi::.ctor()
extern "C"  void PluginQiZi__ctor_m2148244609 (PluginQiZi_t1606345050 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQiZi::Init()
extern "C"  void PluginQiZi_Init_m990666163 (PluginQiZi_t1606345050 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginQiZi::ReqSDKHttpLogin()
extern "C"  Dictionary_2_t827649927 * PluginQiZi_ReqSDKHttpLogin_m975983638 (PluginQiZi_t1606345050 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginQiZi::IsLoginSuccess(System.Boolean)
extern "C"  bool PluginQiZi_IsLoginSuccess_m3071344306 (PluginQiZi_t1606345050 * __this, bool ___isOpenUseLogin0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQiZi::OpenUserLogin()
extern "C"  void PluginQiZi_OpenUserLogin_m3907300819 (PluginQiZi_t1606345050 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQiZi::UserPay(CEvent.ZEvent)
extern "C"  void PluginQiZi_UserPay_m2917810751 (PluginQiZi_t1606345050 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQiZi::EnterGame(CEvent.ZEvent)
extern "C"  void PluginQiZi_EnterGame_m1619034130 (PluginQiZi_t1606345050 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQiZi::RoleUpgrade(CEvent.ZEvent)
extern "C"  void PluginQiZi_RoleUpgrade_m1449100854 (PluginQiZi_t1606345050 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQiZi::OnLoginSuccess(System.String)
extern "C"  void PluginQiZi_OnLoginSuccess_m1590348998 (PluginQiZi_t1606345050 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQiZi::OnLogoutSuccess(System.String)
extern "C"  void PluginQiZi_OnLogoutSuccess_m2059721353 (PluginQiZi_t1606345050 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQiZi::OnLogout(System.String)
extern "C"  void PluginQiZi_OnLogout_m1996709078 (PluginQiZi_t1606345050 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQiZi::initSdk()
extern "C"  void PluginQiZi_initSdk_m252976329 (PluginQiZi_t1606345050 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQiZi::login()
extern "C"  void PluginQiZi_login_m1670259432 (PluginQiZi_t1606345050 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQiZi::logout()
extern "C"  void PluginQiZi_logout_m244257549 (PluginQiZi_t1606345050 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQiZi::userInfo(System.String,System.String,System.String)
extern "C"  void PluginQiZi_userInfo_m2576921246 (PluginQiZi_t1606345050 * __this, String_t* ___serverid0, String_t* ___roleid1, String_t* ___rolename2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQiZi::pay(System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginQiZi_pay_m992981067 (PluginQiZi_t1606345050 * __this, String_t* ___XQPayWithServerId0, String_t* ___withServerName1, String_t* ___withExtInfo2, String_t* ___withAmount3, String_t* ___withProductName4, String_t* ___withProductId5, String_t* ___withRoleId6, String_t* ___withRoleName7, String_t* ___withRoleLevel8, String_t* ___withVipLel9, String_t* ___withUnion10, String_t* ___withBalance11, String_t* ___withDic12, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQiZi::configLanguage()
extern "C"  void PluginQiZi_configLanguage_m495380541 (PluginQiZi_t1606345050 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQiZi::ChooseServer(System.String,System.String,System.String)
extern "C"  void PluginQiZi_ChooseServer_m508490685 (PluginQiZi_t1606345050 * __this, String_t* ___serverid0, String_t* ___ServerName1, String_t* ___notify2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQiZi::SwitchCount()
extern "C"  void PluginQiZi_SwitchCount_m3725589146 (PluginQiZi_t1606345050 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQiZi::<OnLogout>m__44A()
extern "C"  void PluginQiZi_U3COnLogoutU3Em__44A_m3953372496 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionMgr PluginQiZi::ilo_get_Instance1()
extern "C"  VersionMgr_t1322950208 * PluginQiZi_ilo_get_Instance1_m2143749378 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQiZi::ilo_configLanguage2(PluginQiZi)
extern "C"  void PluginQiZi_ilo_configLanguage2_m2290069578 (Il2CppObject * __this /* static, unused */, PluginQiZi_t1606345050 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mihua.SDK.PayInfo PluginQiZi::ilo_Parse3(System.Object[])
extern "C"  PayInfo_t1775308120 * PluginQiZi_ilo_Parse3_m2629619797 (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t1108656482* ___payOjs0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProductsCfgMgr PluginQiZi::ilo_get_ProductsCfgMgr4()
extern "C"  ProductsCfgMgr_t2493714872 * PluginQiZi_ilo_get_ProductsCfgMgr4_m3780700314 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mihua.SDK.LvUpInfo PluginQiZi::ilo_Parse5(System.Object[])
extern "C"  LvUpInfo_t411687177 * PluginQiZi_ilo_Parse5_m2754248922 (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t1108656482* ___crtRoleOjs0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PluginsSdkMgr PluginQiZi::ilo_get_PluginsSdkMgr6()
extern "C"  PluginsSdkMgr_t3884624670 * PluginQiZi_ilo_get_PluginsSdkMgr6_m2719578700 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQiZi::ilo_ReqSDKHttpLogin7(PluginsSdkMgr)
extern "C"  void PluginQiZi_ilo_ReqSDKHttpLogin7_m1292191564 (Il2CppObject * __this /* static, unused */, PluginsSdkMgr_t3884624670 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
