﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Mesh
struct Mesh_t4241756145;

#include "mscorlib_System_ValueType1744280289.h"
#include "UnityEngine_UnityEngine_Matrix4x41651859333.h"
#include "UnityEngine_UnityEngine_Bounds2711641849.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.RecastGraph/SceneMesh
struct  SceneMesh_t509761468 
{
public:
	// UnityEngine.Mesh Pathfinding.RecastGraph/SceneMesh::mesh
	Mesh_t4241756145 * ___mesh_0;
	// UnityEngine.Matrix4x4 Pathfinding.RecastGraph/SceneMesh::matrix
	Matrix4x4_t1651859333  ___matrix_1;
	// UnityEngine.Bounds Pathfinding.RecastGraph/SceneMesh::bounds
	Bounds_t2711641849  ___bounds_2;

public:
	inline static int32_t get_offset_of_mesh_0() { return static_cast<int32_t>(offsetof(SceneMesh_t509761468, ___mesh_0)); }
	inline Mesh_t4241756145 * get_mesh_0() const { return ___mesh_0; }
	inline Mesh_t4241756145 ** get_address_of_mesh_0() { return &___mesh_0; }
	inline void set_mesh_0(Mesh_t4241756145 * value)
	{
		___mesh_0 = value;
		Il2CppCodeGenWriteBarrier(&___mesh_0, value);
	}

	inline static int32_t get_offset_of_matrix_1() { return static_cast<int32_t>(offsetof(SceneMesh_t509761468, ___matrix_1)); }
	inline Matrix4x4_t1651859333  get_matrix_1() const { return ___matrix_1; }
	inline Matrix4x4_t1651859333 * get_address_of_matrix_1() { return &___matrix_1; }
	inline void set_matrix_1(Matrix4x4_t1651859333  value)
	{
		___matrix_1 = value;
	}

	inline static int32_t get_offset_of_bounds_2() { return static_cast<int32_t>(offsetof(SceneMesh_t509761468, ___bounds_2)); }
	inline Bounds_t2711641849  get_bounds_2() const { return ___bounds_2; }
	inline Bounds_t2711641849 * get_address_of_bounds_2() { return &___bounds_2; }
	inline void set_bounds_2(Bounds_t2711641849  value)
	{
		___bounds_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for marshalling of: Pathfinding.RecastGraph/SceneMesh
struct SceneMesh_t509761468_marshaled_pinvoke
{
	Mesh_t4241756145 * ___mesh_0;
	Matrix4x4_t1651859333_marshaled_pinvoke ___matrix_1;
	Bounds_t2711641849_marshaled_pinvoke ___bounds_2;
};
// Native definition for marshalling of: Pathfinding.RecastGraph/SceneMesh
struct SceneMesh_t509761468_marshaled_com
{
	Mesh_t4241756145 * ___mesh_0;
	Matrix4x4_t1651859333_marshaled_com ___matrix_1;
	Bounds_t2711641849_marshaled_com ___bounds_2;
};
