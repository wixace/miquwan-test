//
//  TrackingIOReally.m
//  GameSDKCNDemo
//
//  Created by XYG on 2018/1/5.
//  Copyright © 2018年 iqiyigame. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ReYunGame.h"
#import "TrackingIO.h"
#import "ReYunTrack.h"
#import "ToUnityUtil.h"
@interface TrackingIOReally:NSObject

@end
@implementation TrackingIOReally
#if defined(__cplusplus)
extern "C"{
#endif
    struct strutDic{
        char* key;
        char* value;
    };
    //******************游戏运营平台*****************
    void initWithAppId(char* appId,char* channelId){
        [reyun initWithAppId:_CreateNSString(appId) channelID:_CreateNSString(channelId)];
    }
    void setNRegisterWithAccountID (char* account, gender genders, char* age, char* serverId, char* accountType, char* rolename){
        [reyun setRegisterWithAccountID:_CreateNSString(account) andGender:genders andage:_CreateNSString(age) andServerId:_CreateNSString(serverId) andAccountType:_CreateNSString(accountType) andRole:_CreateNSString(rolename)];
    }
    void setLoginWithAccountID (char* account, gender genders, char* age, char* serverId, int level, char* rolename){
        [reyun setLoginWithAccountID:_CreateNSString(account) andGender:(gender)genders andage:_CreateNSString(account) andServerId:_CreateNSString(account) andlevel:level andRole:_CreateNSString(account)];
    }
    void setEconomy (char* itemName, int itemAmount, float itemTotalPrice){
        [reyun setEconomy:_CreateNSString(itemName) andEconomyNumber:itemAmount andEconomyTotalPrice:itemTotalPrice];
    }
    void setNQuest (char* questId, questStatus questStatu, char* questType){
        [reyun setQuest:_CreateNSString(questId) andTaskState:questStatu andTaskType:_CreateNSString(questType)];
    }
    void setNEvent (char* eventName, int number,struct strutDic *dict[]){
        NSDictionary *nsDic=[[NSDictionary alloc] init];
        for(int i=0;i<number;i++){
            NSString *key =_CreateNSString((*dict[i]).key);
            NSString *value =_CreateNSString((*dict[i]).value);
            [nsDic setValue:value forKey:key];
        }
        [reyun setEvent:_CreateNSString(eventName) andExtra:nsDic];
    }
    void setNewEvent (char* eventName, char* json){
        
    }
    char* getDeviceId (){
        NSString *str = [reyun getDeviceId];
        const char *c = [str UTF8String];
        return strdup(c);
    }
    void setGamePrintLog (bool print){
        [reyun setPrintLog:print];
    }
    //******************移动广告平台*****************
    void initTrackWithappKey (char* appKey, char* channelId){
        //[ReYunChannel initWithappKey:_CreateNSString(appKey) withChannelId:_CreateNSString(channelId)];
    }
    void setTrackRegisterWithAccountID (char* account){
        //[ReYunChannel setRegisterWithAccountID:_CreateNSString(account)];
    }
    void setTrackLoginWithAccountID (char* account){
        //[ReYunChannel setLoginWithAccountID:_CreateNSString(account)];
    }
    void setTrackEvent (char* eventName){
        //[ReYunChannel setEvent:_CreateNSString(eventName)];
    }
    char* getTrackDeviceId (){
//        NSString *str = [ReYunChannel getDeviceId];
//        const char *c = [str UTF8String];
//        return strdup(c);
        return "";
    }
    void setTrackPrintLog (bool print){
        //[ReYunChannel setPrintLog:print];
    }
    
    //******************广告和分析平台*****************
    void initTrackIOWithappKey (char* appKey, char* channelId){
        [TrackingIO initWithappKey:_CreateNSString(appKey) withChannelId:_CreateNSString(channelId)];
    }
    void setTrackIORegisterWithAccountID (char* account){
        [TrackingIO setRegisterWithAccountID:_CreateNSString(account)];
    }
    void setTrackIOLoginWithAccountID (char* account){
        [TrackingIO setLoginWithAccountID:_CreateNSString(account)];
    }
    void setTrackIOEvent (char* eventName){
        [TrackingIO setEvent:_CreateNSString(eventName) andExtra:nil];
    }
    char* getTrackIODeviceId (){
        NSString *str = [TrackingIO getDeviceId];
        const char *c = [str UTF8String];
        return strdup(c);
    }
    void setTrackIOPrintLog (bool print){
        [TrackingIO setPrintLog:print];
    }
    

@end
