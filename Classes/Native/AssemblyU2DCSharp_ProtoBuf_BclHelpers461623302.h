﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_DateTime4283661327.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ProtoBuf.BclHelpers
struct  BclHelpers_t461623302  : public Il2CppObject
{
public:

public:
};

struct BclHelpers_t461623302_StaticFields
{
public:
	// System.DateTime ProtoBuf.BclHelpers::EpochOrigin
	DateTime_t4283661327  ___EpochOrigin_13;

public:
	inline static int32_t get_offset_of_EpochOrigin_13() { return static_cast<int32_t>(offsetof(BclHelpers_t461623302_StaticFields, ___EpochOrigin_13)); }
	inline DateTime_t4283661327  get_EpochOrigin_13() const { return ___EpochOrigin_13; }
	inline DateTime_t4283661327 * get_address_of_EpochOrigin_13() { return &___EpochOrigin_13; }
	inline void set_EpochOrigin_13(DateTime_t4283661327  value)
	{
		___EpochOrigin_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
