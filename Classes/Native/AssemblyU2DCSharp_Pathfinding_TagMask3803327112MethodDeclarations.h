﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.TagMask
struct TagMask_t3803327112;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void Pathfinding.TagMask::.ctor()
extern "C"  void TagMask__ctor_m1084209583 (TagMask_t3803327112 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.TagMask::.ctor(System.Int32,System.Int32)
extern "C"  void TagMask__ctor_m2639432791 (TagMask_t3803327112 * __this, int32_t ___change0, int32_t ___set1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.TagMask::SetValues(System.Object)
extern "C"  void TagMask_SetValues_m2681838915 (TagMask_t3803327112 * __this, Il2CppObject * ___boxedTagMask0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Pathfinding.TagMask::ToString()
extern "C"  String_t* TagMask_ToString_m3124637854 (TagMask_t3803327112 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
