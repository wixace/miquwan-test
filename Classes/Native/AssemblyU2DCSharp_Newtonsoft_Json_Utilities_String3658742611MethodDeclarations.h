﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Utilities.StringUtils/<NumberLines>c__AnonStorey14A
struct U3CNumberLinesU3Ec__AnonStorey14A_t3658742611;
// System.IO.TextWriter
struct TextWriter_t2304124208;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IO_TextWriter2304124208.h"
#include "mscorlib_System_String7231557.h"

// System.Void Newtonsoft.Json.Utilities.StringUtils/<NumberLines>c__AnonStorey14A::.ctor()
extern "C"  void U3CNumberLinesU3Ec__AnonStorey14A__ctor_m316388728 (U3CNumberLinesU3Ec__AnonStorey14A_t3658742611 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Utilities.StringUtils/<NumberLines>c__AnonStorey14A::<>m__3B6(System.IO.TextWriter,System.String)
extern "C"  void U3CNumberLinesU3Ec__AnonStorey14A_U3CU3Em__3B6_m4105802427 (U3CNumberLinesU3Ec__AnonStorey14A_t3658742611 * __this, TextWriter_t2304124208 * ___tw0, String_t* ___line1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
