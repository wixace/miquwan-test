﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// NavmeshController
struct NavmeshController_t1255162156;

#include "codegen/il2cpp-codegen.h"

// System.Void NavmeshController::.ctor()
extern "C"  void NavmeshController__ctor_m425459135 (NavmeshController_t1255162156 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
