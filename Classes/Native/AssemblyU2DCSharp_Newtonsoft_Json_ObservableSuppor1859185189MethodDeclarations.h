﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.ObservableSupport.AddingNewEventHandler
struct AddingNewEventHandler_t1859185189;
// System.Object
struct Il2CppObject;
// Newtonsoft.Json.ObservableSupport.AddingNewEventArgs
struct AddingNewEventArgs_t1516177436;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_ObservableSuppor1516177436.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void Newtonsoft.Json.ObservableSupport.AddingNewEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void AddingNewEventHandler__ctor_m3794410063 (AddingNewEventHandler_t1859185189 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.ObservableSupport.AddingNewEventHandler::Invoke(System.Object,Newtonsoft.Json.ObservableSupport.AddingNewEventArgs)
extern "C"  void AddingNewEventHandler_Invoke_m4271925132 (AddingNewEventHandler_t1859185189 * __this, Il2CppObject * ___sender0, AddingNewEventArgs_t1516177436 * ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Newtonsoft.Json.ObservableSupport.AddingNewEventHandler::BeginInvoke(System.Object,Newtonsoft.Json.ObservableSupport.AddingNewEventArgs,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * AddingNewEventHandler_BeginInvoke_m3188242777 (AddingNewEventHandler_t1859185189 * __this, Il2CppObject * ___sender0, AddingNewEventArgs_t1516177436 * ___e1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.ObservableSupport.AddingNewEventHandler::EndInvoke(System.IAsyncResult)
extern "C"  void AddingNewEventHandler_EndInvoke_m2542603231 (AddingNewEventHandler_t1859185189 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
