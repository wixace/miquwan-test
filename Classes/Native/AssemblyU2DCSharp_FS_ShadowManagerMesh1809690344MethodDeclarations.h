﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FS_ShadowManagerMesh
struct FS_ShadowManagerMesh_t1809690344;
// FS_ShadowSimple
struct FS_ShadowSimple_t4208748868;
// UnityEngine.Mesh
struct Mesh_t4241756145;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t215400611;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_FS_ShadowSimple4208748868.h"
#include "AssemblyU2DCSharp_FS_ShadowManagerMesh1809690344.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void FS_ShadowManagerMesh::.ctor()
extern "C"  void FS_ShadowManagerMesh__ctor_m22868147 (FS_ShadowManagerMesh_t1809690344 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 FS_ShadowManagerMesh::getNumShadows()
extern "C"  int32_t FS_ShadowManagerMesh_getNumShadows_m461913286 (FS_ShadowManagerMesh_t1809690344 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FS_ShadowManagerMesh::Start()
extern "C"  void FS_ShadowManagerMesh_Start_m3264973235 (FS_ShadowManagerMesh_t1809690344 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FS_ShadowManagerMesh::registerGeometry(FS_ShadowSimple)
extern "C"  void FS_ShadowManagerMesh_registerGeometry_m484861160 (FS_ShadowManagerMesh_t1809690344 * __this, FS_ShadowSimple_t4208748868 * ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FS_ShadowManagerMesh::recreateStaticGeometry()
extern "C"  void FS_ShadowManagerMesh_recreateStaticGeometry_m2260302368 (FS_ShadowManagerMesh_t1809690344 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FS_ShadowManagerMesh::LateUpdate()
extern "C"  void FS_ShadowManagerMesh_LateUpdate_m2910966144 (FS_ShadowManagerMesh_t1809690344 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Mesh FS_ShadowManagerMesh::_GetMesh()
extern "C"  Mesh_t4241756145 * FS_ShadowManagerMesh__GetMesh_m2762698414 (FS_ShadowManagerMesh_t1809690344 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FS_ShadowManagerMesh::_CreateGeometry()
extern "C"  void FS_ShadowManagerMesh__CreateGeometry_m341712510 (FS_ShadowManagerMesh_t1809690344 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FS_ShadowManagerMesh::ilo__CreateGeometry1(FS_ShadowManagerMesh)
extern "C"  void FS_ShadowManagerMesh_ilo__CreateGeometry1_m1186229024 (Il2CppObject * __this /* static, unused */, FS_ShadowManagerMesh_t1809690344 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3[] FS_ShadowManagerMesh::ilo_get_corners2(FS_ShadowSimple)
extern "C"  Vector3U5BU5D_t215400611* FS_ShadowManagerMesh_ilo_get_corners2_m2691974645 (Il2CppObject * __this /* static, unused */, FS_ShadowSimple_t4208748868 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 FS_ShadowManagerMesh::ilo_get_normal3(FS_ShadowSimple)
extern "C"  Vector3_t4282066566  FS_ShadowManagerMesh_ilo_get_normal3_m4199165031 (Il2CppObject * __this /* static, unused */, FS_ShadowSimple_t4208748868 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
