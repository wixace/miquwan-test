﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_chiryaNecaibe148
struct M_chiryaNecaibe148_t522537700;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_chiryaNecaibe148522537700.h"

// System.Void GarbageiOS.M_chiryaNecaibe148::.ctor()
extern "C"  void M_chiryaNecaibe148__ctor_m1377514447 (M_chiryaNecaibe148_t522537700 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_chiryaNecaibe148::M_pishousemTedici0(System.String[],System.Int32)
extern "C"  void M_chiryaNecaibe148_M_pishousemTedici0_m4001136715 (M_chiryaNecaibe148_t522537700 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_chiryaNecaibe148::M_pinisuGimaw1(System.String[],System.Int32)
extern "C"  void M_chiryaNecaibe148_M_pinisuGimaw1_m3492314724 (M_chiryaNecaibe148_t522537700 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_chiryaNecaibe148::M_poukou2(System.String[],System.Int32)
extern "C"  void M_chiryaNecaibe148_M_poukou2_m3030560297 (M_chiryaNecaibe148_t522537700 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_chiryaNecaibe148::M_sisziTifai3(System.String[],System.Int32)
extern "C"  void M_chiryaNecaibe148_M_sisziTifai3_m2754156952 (M_chiryaNecaibe148_t522537700 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_chiryaNecaibe148::M_palasemPawel4(System.String[],System.Int32)
extern "C"  void M_chiryaNecaibe148_M_palasemPawel4_m3067301806 (M_chiryaNecaibe148_t522537700 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_chiryaNecaibe148::M_stirjerepal5(System.String[],System.Int32)
extern "C"  void M_chiryaNecaibe148_M_stirjerepal5_m3295896016 (M_chiryaNecaibe148_t522537700 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_chiryaNecaibe148::M_malsasear6(System.String[],System.Int32)
extern "C"  void M_chiryaNecaibe148_M_malsasear6_m2677880011 (M_chiryaNecaibe148_t522537700 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_chiryaNecaibe148::ilo_M_poukou21(GarbageiOS.M_chiryaNecaibe148,System.String[],System.Int32)
extern "C"  void M_chiryaNecaibe148_ilo_M_poukou21_m4211951931 (Il2CppObject * __this /* static, unused */, M_chiryaNecaibe148_t522537700 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
