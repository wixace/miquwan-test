﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Linq.JContainer/<Descendants>c__Iterator1C
struct U3CDescendantsU3Ec__Iterator1C_t2959414494;
// Newtonsoft.Json.Linq.JToken
struct JToken_t3412245951;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.Linq.JToken>
struct IEnumerator_1_t1029143704;

#include "codegen/il2cpp-codegen.h"

// System.Void Newtonsoft.Json.Linq.JContainer/<Descendants>c__Iterator1C::.ctor()
extern "C"  void U3CDescendantsU3Ec__Iterator1C__ctor_m169053565 (U3CDescendantsU3Ec__Iterator1C_t2959414494 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JContainer/<Descendants>c__Iterator1C::System.Collections.Generic.IEnumerator<Newtonsoft.Json.Linq.JToken>.get_Current()
extern "C"  JToken_t3412245951 * U3CDescendantsU3Ec__Iterator1C_System_Collections_Generic_IEnumeratorU3CNewtonsoft_Json_Linq_JTokenU3E_get_Current_m2119742758 (U3CDescendantsU3Ec__Iterator1C_t2959414494 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Linq.JContainer/<Descendants>c__Iterator1C::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDescendantsU3Ec__Iterator1C_System_Collections_IEnumerator_get_Current_m2656938451 (U3CDescendantsU3Ec__Iterator1C_t2959414494 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Newtonsoft.Json.Linq.JContainer/<Descendants>c__Iterator1C::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CDescendantsU3Ec__Iterator1C_System_Collections_IEnumerable_GetEnumerator_m3784603348 (U3CDescendantsU3Ec__Iterator1C_t2959414494 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JContainer/<Descendants>c__Iterator1C::System.Collections.Generic.IEnumerable<Newtonsoft.Json.Linq.JToken>.GetEnumerator()
extern "C"  Il2CppObject* U3CDescendantsU3Ec__Iterator1C_System_Collections_Generic_IEnumerableU3CNewtonsoft_Json_Linq_JTokenU3E_GetEnumerator_m1528131623 (U3CDescendantsU3Ec__Iterator1C_t2959414494 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Linq.JContainer/<Descendants>c__Iterator1C::MoveNext()
extern "C"  bool U3CDescendantsU3Ec__Iterator1C_MoveNext_m1017139775 (U3CDescendantsU3Ec__Iterator1C_t2959414494 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JContainer/<Descendants>c__Iterator1C::Dispose()
extern "C"  void U3CDescendantsU3Ec__Iterator1C_Dispose_m3444969082 (U3CDescendantsU3Ec__Iterator1C_t2959414494 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JContainer/<Descendants>c__Iterator1C::Reset()
extern "C"  void U3CDescendantsU3Ec__Iterator1C_Reset_m2110453802 (U3CDescendantsU3Ec__Iterator1C_t2959414494 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
