﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2678631987.h"
#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DCSharp_AIEnum_EAIEventtype3896289311.h"

// System.Void System.Array/InternalEnumerator`1<AIEnum.EAIEventtype>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2605660081_gshared (InternalEnumerator_1_t2678631987 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m2605660081(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t2678631987 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m2605660081_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<AIEnum.EAIEventtype>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3297957455_gshared (InternalEnumerator_1_t2678631987 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3297957455(__this, method) ((  void (*) (InternalEnumerator_1_t2678631987 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3297957455_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<AIEnum.EAIEventtype>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m829058821_gshared (InternalEnumerator_1_t2678631987 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m829058821(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t2678631987 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m829058821_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<AIEnum.EAIEventtype>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2076998344_gshared (InternalEnumerator_1_t2678631987 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m2076998344(__this, method) ((  void (*) (InternalEnumerator_1_t2678631987 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2076998344_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<AIEnum.EAIEventtype>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2889453183_gshared (InternalEnumerator_1_t2678631987 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m2889453183(__this, method) ((  bool (*) (InternalEnumerator_1_t2678631987 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m2889453183_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<AIEnum.EAIEventtype>::get_Current()
extern "C"  uint8_t InternalEnumerator_1_get_Current_m2007595674_gshared (InternalEnumerator_1_t2678631987 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m2007595674(__this, method) ((  uint8_t (*) (InternalEnumerator_1_t2678631987 *, const MethodInfo*))InternalEnumerator_1_get_Current_m2007595674_gshared)(__this, method)
