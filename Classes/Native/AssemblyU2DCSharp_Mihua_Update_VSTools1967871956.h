﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.UInt32[]
struct UInt32U5BU5D_t3230734560;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mihua.Update.VSTools
struct  VSTools_t1967871956  : public Il2CppObject
{
public:

public:
};

struct VSTools_t1967871956_StaticFields
{
public:
	// System.UInt32[] Mihua.Update.VSTools::Crc32Table
	UInt32U5BU5D_t3230734560* ___Crc32Table_0;

public:
	inline static int32_t get_offset_of_Crc32Table_0() { return static_cast<int32_t>(offsetof(VSTools_t1967871956_StaticFields, ___Crc32Table_0)); }
	inline UInt32U5BU5D_t3230734560* get_Crc32Table_0() const { return ___Crc32Table_0; }
	inline UInt32U5BU5D_t3230734560** get_address_of_Crc32Table_0() { return &___Crc32Table_0; }
	inline void set_Crc32Table_0(UInt32U5BU5D_t3230734560* value)
	{
		___Crc32Table_0 = value;
		Il2CppCodeGenWriteBarrier(&___Crc32Table_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
