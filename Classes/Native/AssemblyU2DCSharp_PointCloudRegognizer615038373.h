﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<PointCloudGestureTemplate>
struct List_1_t579770958;
// PointCloudRegognizer/GestureNormalizer
struct GestureNormalizer_t660715300;
// System.Collections.Generic.List`1<PointCloudRegognizer/NormalizedTemplate>
struct List_1_t3392119851;
// System.Boolean[]
struct BooleanU5BU5D_t3456302923;
// PointCloudGesture
struct PointCloudGesture_t1959506660;
// PointCloudRegognizer/NormalizedTemplate
struct NormalizedTemplate_t2023934299;

#include "AssemblyU2DCSharp_DiscreteGestureRecognizer_1_gen1420501158.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PointCloudRegognizer
struct  PointCloudRegognizer_t615038373  : public DiscreteGestureRecognizer_1_t1420501158
{
public:
	// System.Single PointCloudRegognizer::MinDistanceBetweenSamples
	float ___MinDistanceBetweenSamples_21;
	// System.Single PointCloudRegognizer::MaxMatchDistance
	float ___MaxMatchDistance_22;
	// System.Collections.Generic.List`1<PointCloudGestureTemplate> PointCloudRegognizer::Templates
	List_1_t579770958 * ___Templates_23;
	// PointCloudRegognizer/GestureNormalizer PointCloudRegognizer::normalizer
	GestureNormalizer_t660715300 * ___normalizer_24;
	// System.Collections.Generic.List`1<PointCloudRegognizer/NormalizedTemplate> PointCloudRegognizer::normalizedTemplates
	List_1_t3392119851 * ___normalizedTemplates_25;
	// PointCloudGesture PointCloudRegognizer::debugLastGesture
	PointCloudGesture_t1959506660 * ___debugLastGesture_27;
	// PointCloudRegognizer/NormalizedTemplate PointCloudRegognizer::debugLastMatchedTemplate
	NormalizedTemplate_t2023934299 * ___debugLastMatchedTemplate_28;

public:
	inline static int32_t get_offset_of_MinDistanceBetweenSamples_21() { return static_cast<int32_t>(offsetof(PointCloudRegognizer_t615038373, ___MinDistanceBetweenSamples_21)); }
	inline float get_MinDistanceBetweenSamples_21() const { return ___MinDistanceBetweenSamples_21; }
	inline float* get_address_of_MinDistanceBetweenSamples_21() { return &___MinDistanceBetweenSamples_21; }
	inline void set_MinDistanceBetweenSamples_21(float value)
	{
		___MinDistanceBetweenSamples_21 = value;
	}

	inline static int32_t get_offset_of_MaxMatchDistance_22() { return static_cast<int32_t>(offsetof(PointCloudRegognizer_t615038373, ___MaxMatchDistance_22)); }
	inline float get_MaxMatchDistance_22() const { return ___MaxMatchDistance_22; }
	inline float* get_address_of_MaxMatchDistance_22() { return &___MaxMatchDistance_22; }
	inline void set_MaxMatchDistance_22(float value)
	{
		___MaxMatchDistance_22 = value;
	}

	inline static int32_t get_offset_of_Templates_23() { return static_cast<int32_t>(offsetof(PointCloudRegognizer_t615038373, ___Templates_23)); }
	inline List_1_t579770958 * get_Templates_23() const { return ___Templates_23; }
	inline List_1_t579770958 ** get_address_of_Templates_23() { return &___Templates_23; }
	inline void set_Templates_23(List_1_t579770958 * value)
	{
		___Templates_23 = value;
		Il2CppCodeGenWriteBarrier(&___Templates_23, value);
	}

	inline static int32_t get_offset_of_normalizer_24() { return static_cast<int32_t>(offsetof(PointCloudRegognizer_t615038373, ___normalizer_24)); }
	inline GestureNormalizer_t660715300 * get_normalizer_24() const { return ___normalizer_24; }
	inline GestureNormalizer_t660715300 ** get_address_of_normalizer_24() { return &___normalizer_24; }
	inline void set_normalizer_24(GestureNormalizer_t660715300 * value)
	{
		___normalizer_24 = value;
		Il2CppCodeGenWriteBarrier(&___normalizer_24, value);
	}

	inline static int32_t get_offset_of_normalizedTemplates_25() { return static_cast<int32_t>(offsetof(PointCloudRegognizer_t615038373, ___normalizedTemplates_25)); }
	inline List_1_t3392119851 * get_normalizedTemplates_25() const { return ___normalizedTemplates_25; }
	inline List_1_t3392119851 ** get_address_of_normalizedTemplates_25() { return &___normalizedTemplates_25; }
	inline void set_normalizedTemplates_25(List_1_t3392119851 * value)
	{
		___normalizedTemplates_25 = value;
		Il2CppCodeGenWriteBarrier(&___normalizedTemplates_25, value);
	}

	inline static int32_t get_offset_of_debugLastGesture_27() { return static_cast<int32_t>(offsetof(PointCloudRegognizer_t615038373, ___debugLastGesture_27)); }
	inline PointCloudGesture_t1959506660 * get_debugLastGesture_27() const { return ___debugLastGesture_27; }
	inline PointCloudGesture_t1959506660 ** get_address_of_debugLastGesture_27() { return &___debugLastGesture_27; }
	inline void set_debugLastGesture_27(PointCloudGesture_t1959506660 * value)
	{
		___debugLastGesture_27 = value;
		Il2CppCodeGenWriteBarrier(&___debugLastGesture_27, value);
	}

	inline static int32_t get_offset_of_debugLastMatchedTemplate_28() { return static_cast<int32_t>(offsetof(PointCloudRegognizer_t615038373, ___debugLastMatchedTemplate_28)); }
	inline NormalizedTemplate_t2023934299 * get_debugLastMatchedTemplate_28() const { return ___debugLastMatchedTemplate_28; }
	inline NormalizedTemplate_t2023934299 ** get_address_of_debugLastMatchedTemplate_28() { return &___debugLastMatchedTemplate_28; }
	inline void set_debugLastMatchedTemplate_28(NormalizedTemplate_t2023934299 * value)
	{
		___debugLastMatchedTemplate_28 = value;
		Il2CppCodeGenWriteBarrier(&___debugLastMatchedTemplate_28, value);
	}
};

struct PointCloudRegognizer_t615038373_StaticFields
{
public:
	// System.Boolean[] PointCloudRegognizer::matched
	BooleanU5BU5D_t3456302923* ___matched_26;

public:
	inline static int32_t get_offset_of_matched_26() { return static_cast<int32_t>(offsetof(PointCloudRegognizer_t615038373_StaticFields, ___matched_26)); }
	inline BooleanU5BU5D_t3456302923* get_matched_26() const { return ___matched_26; }
	inline BooleanU5BU5D_t3456302923** get_address_of_matched_26() { return &___matched_26; }
	inline void set_matched_26(BooleanU5BU5D_t3456302923* value)
	{
		___matched_26 = value;
		Il2CppCodeGenWriteBarrier(&___matched_26, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
