﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._6cbb6c301f3237ba4eb5838f24ef1aee
struct _6cbb6c301f3237ba4eb5838f24ef1aee_t3704541607;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__6cbb6c301f3237ba4eb5838f3704541607.h"

// System.Void Little._6cbb6c301f3237ba4eb5838f24ef1aee::.ctor()
extern "C"  void _6cbb6c301f3237ba4eb5838f24ef1aee__ctor_m3086499942 (_6cbb6c301f3237ba4eb5838f24ef1aee_t3704541607 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._6cbb6c301f3237ba4eb5838f24ef1aee::_6cbb6c301f3237ba4eb5838f24ef1aeem2(System.Int32)
extern "C"  int32_t _6cbb6c301f3237ba4eb5838f24ef1aee__6cbb6c301f3237ba4eb5838f24ef1aeem2_m287438905 (_6cbb6c301f3237ba4eb5838f24ef1aee_t3704541607 * __this, int32_t ____6cbb6c301f3237ba4eb5838f24ef1aeea0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._6cbb6c301f3237ba4eb5838f24ef1aee::_6cbb6c301f3237ba4eb5838f24ef1aeem(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _6cbb6c301f3237ba4eb5838f24ef1aee__6cbb6c301f3237ba4eb5838f24ef1aeem_m4226114205 (_6cbb6c301f3237ba4eb5838f24ef1aee_t3704541607 * __this, int32_t ____6cbb6c301f3237ba4eb5838f24ef1aeea0, int32_t ____6cbb6c301f3237ba4eb5838f24ef1aee61, int32_t ____6cbb6c301f3237ba4eb5838f24ef1aeec2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._6cbb6c301f3237ba4eb5838f24ef1aee::ilo__6cbb6c301f3237ba4eb5838f24ef1aeem21(Little._6cbb6c301f3237ba4eb5838f24ef1aee,System.Int32)
extern "C"  int32_t _6cbb6c301f3237ba4eb5838f24ef1aee_ilo__6cbb6c301f3237ba4eb5838f24ef1aeem21_m312090958 (Il2CppObject * __this /* static, unused */, _6cbb6c301f3237ba4eb5838f24ef1aee_t3704541607 * ____this0, int32_t ____6cbb6c301f3237ba4eb5838f24ef1aeea1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
