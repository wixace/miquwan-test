﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.JointLimits
struct JointLimits_t3539604472;
struct JointLimits_t3539604472_marshaled_pinvoke;
struct JointLimits_t3539604472_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_JointLimits3539604472.h"

// System.Single UnityEngine.JointLimits::get_min()
extern "C"  float JointLimits_get_min_m1901237322 (JointLimits_t3539604472 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.JointLimits::set_min(System.Single)
extern "C"  void JointLimits_set_min_m225618337 (JointLimits_t3539604472 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.JointLimits::get_max()
extern "C"  float JointLimits_get_max_m1901008604 (JointLimits_t3539604472 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.JointLimits::set_max(System.Single)
extern "C"  void JointLimits_set_max_m1473668175 (JointLimits_t3539604472 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.JointLimits::get_bounciness()
extern "C"  float JointLimits_get_bounciness_m1641363917 (JointLimits_t3539604472 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.JointLimits::set_bounciness(System.Single)
extern "C"  void JointLimits_set_bounciness_m728557822 (JointLimits_t3539604472 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.JointLimits::get_bounceMinVelocity()
extern "C"  float JointLimits_get_bounceMinVelocity_m2373628447 (JointLimits_t3539604472 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.JointLimits::set_bounceMinVelocity(System.Single)
extern "C"  void JointLimits_set_bounceMinVelocity_m3138350572 (JointLimits_t3539604472 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.JointLimits::get_contactDistance()
extern "C"  float JointLimits_get_contactDistance_m2288155373 (JointLimits_t3539604472 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.JointLimits::set_contactDistance(System.Single)
extern "C"  void JointLimits_set_contactDistance_m294773726 (JointLimits_t3539604472 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct JointLimits_t3539604472;
struct JointLimits_t3539604472_marshaled_pinvoke;

extern "C" void JointLimits_t3539604472_marshal_pinvoke(const JointLimits_t3539604472& unmarshaled, JointLimits_t3539604472_marshaled_pinvoke& marshaled);
extern "C" void JointLimits_t3539604472_marshal_pinvoke_back(const JointLimits_t3539604472_marshaled_pinvoke& marshaled, JointLimits_t3539604472& unmarshaled);
extern "C" void JointLimits_t3539604472_marshal_pinvoke_cleanup(JointLimits_t3539604472_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct JointLimits_t3539604472;
struct JointLimits_t3539604472_marshaled_com;

extern "C" void JointLimits_t3539604472_marshal_com(const JointLimits_t3539604472& unmarshaled, JointLimits_t3539604472_marshaled_com& marshaled);
extern "C" void JointLimits_t3539604472_marshal_com_back(const JointLimits_t3539604472_marshaled_com& marshaled, JointLimits_t3539604472& unmarshaled);
extern "C" void JointLimits_t3539604472_marshal_com_cleanup(JointLimits_t3539604472_marshaled_com& marshaled);
