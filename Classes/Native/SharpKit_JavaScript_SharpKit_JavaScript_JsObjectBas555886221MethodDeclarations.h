﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SharpKit.JavaScript.JsObjectBase
struct JsObjectBase_t555886221;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"

// System.Int32 SharpKit.JavaScript.JsObjectBase::GetHashCode()
extern "C"  int32_t JsObjectBase_GetHashCode_m1584299747 (JsObjectBase_t555886221 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SharpKit.JavaScript.JsObjectBase::Equals(System.Object)
extern "C"  bool JsObjectBase_Equals_m2732936447 (JsObjectBase_t555886221 * __this, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SharpKit.JavaScript.JsObjectBase::ToString()
extern "C"  String_t* JsObjectBase_ToString_m2391168879 (JsObjectBase_t555886221 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SharpKit.JavaScript.JsObjectBase::.ctor()
extern "C"  void JsObjectBase__ctor_m587599652 (JsObjectBase_t555886221 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
