﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// BossAnimationConfig
struct BossAnimationConfig_t4103877785;
// System.Collections.Generic.Dictionary`2<System.Int32,BossAnimationInfo>
struct Dictionary_2_t381599052;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BossAnimationConfig
struct  BossAnimationConfig_t4103877785  : public Il2CppObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.Int32,BossAnimationInfo> BossAnimationConfig::_dicBossCfg
	Dictionary_2_t381599052 * ____dicBossCfg_1;

public:
	inline static int32_t get_offset_of__dicBossCfg_1() { return static_cast<int32_t>(offsetof(BossAnimationConfig_t4103877785, ____dicBossCfg_1)); }
	inline Dictionary_2_t381599052 * get__dicBossCfg_1() const { return ____dicBossCfg_1; }
	inline Dictionary_2_t381599052 ** get_address_of__dicBossCfg_1() { return &____dicBossCfg_1; }
	inline void set__dicBossCfg_1(Dictionary_2_t381599052 * value)
	{
		____dicBossCfg_1 = value;
		Il2CppCodeGenWriteBarrier(&____dicBossCfg_1, value);
	}
};

struct BossAnimationConfig_t4103877785_StaticFields
{
public:
	// BossAnimationConfig BossAnimationConfig::_inst
	BossAnimationConfig_t4103877785 * ____inst_0;

public:
	inline static int32_t get_offset_of__inst_0() { return static_cast<int32_t>(offsetof(BossAnimationConfig_t4103877785_StaticFields, ____inst_0)); }
	inline BossAnimationConfig_t4103877785 * get__inst_0() const { return ____inst_0; }
	inline BossAnimationConfig_t4103877785 ** get_address_of__inst_0() { return &____inst_0; }
	inline void set__inst_0(BossAnimationConfig_t4103877785 * value)
	{
		____inst_0 = value;
		Il2CppCodeGenWriteBarrier(&____inst_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
