﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PluginsSdkMgr/<SendHttpLogin>c__Iterator37
struct U3CSendHttpLoginU3Ec__Iterator37_t3559813253;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void PluginsSdkMgr/<SendHttpLogin>c__Iterator37::.ctor()
extern "C"  void U3CSendHttpLoginU3Ec__Iterator37__ctor_m721194422 (U3CSendHttpLoginU3Ec__Iterator37_t3559813253 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PluginsSdkMgr/<SendHttpLogin>c__Iterator37::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CSendHttpLoginU3Ec__Iterator37_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1094681510 (U3CSendHttpLoginU3Ec__Iterator37_t3559813253 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PluginsSdkMgr/<SendHttpLogin>c__Iterator37::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CSendHttpLoginU3Ec__Iterator37_System_Collections_IEnumerator_get_Current_m2068419386 (U3CSendHttpLoginU3Ec__Iterator37_t3559813253 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginsSdkMgr/<SendHttpLogin>c__Iterator37::MoveNext()
extern "C"  bool U3CSendHttpLoginU3Ec__Iterator37_MoveNext_m610214758 (U3CSendHttpLoginU3Ec__Iterator37_t3559813253 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgr/<SendHttpLogin>c__Iterator37::Dispose()
extern "C"  void U3CSendHttpLoginU3Ec__Iterator37_Dispose_m1476387955 (U3CSendHttpLoginU3Ec__Iterator37_t3559813253 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgr/<SendHttpLogin>c__Iterator37::Reset()
extern "C"  void U3CSendHttpLoginU3Ec__Iterator37_Reset_m2662594659 (U3CSendHttpLoginU3Ec__Iterator37_t3559813253 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
