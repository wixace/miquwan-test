﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1983919073.h"
#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DCSharp_Pathfinding_Voxels_LinkedVoxelSp3201576397.h"

// System.Void System.Array/InternalEnumerator`1<Pathfinding.Voxels.LinkedVoxelSpan>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2506748081_gshared (InternalEnumerator_1_t1983919073 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m2506748081(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t1983919073 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m2506748081_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<Pathfinding.Voxels.LinkedVoxelSpan>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1143176527_gshared (InternalEnumerator_1_t1983919073 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1143176527(__this, method) ((  void (*) (InternalEnumerator_1_t1983919073 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1143176527_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<Pathfinding.Voxels.LinkedVoxelSpan>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3120565627_gshared (InternalEnumerator_1_t1983919073 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3120565627(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t1983919073 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3120565627_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Pathfinding.Voxels.LinkedVoxelSpan>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3158995912_gshared (InternalEnumerator_1_t1983919073 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m3158995912(__this, method) ((  void (*) (InternalEnumerator_1_t1983919073 *, const MethodInfo*))InternalEnumerator_1_Dispose_m3158995912_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Pathfinding.Voxels.LinkedVoxelSpan>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2080863291_gshared (InternalEnumerator_1_t1983919073 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m2080863291(__this, method) ((  bool (*) (InternalEnumerator_1_t1983919073 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m2080863291_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Pathfinding.Voxels.LinkedVoxelSpan>::get_Current()
extern "C"  LinkedVoxelSpan_t3201576397  InternalEnumerator_1_get_Current_m2544301112_gshared (InternalEnumerator_1_t1983919073 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m2544301112(__this, method) ((  LinkedVoxelSpan_t3201576397  (*) (InternalEnumerator_1_t1983919073 *, const MethodInfo*))InternalEnumerator_1_get_Current_m2544301112_gshared)(__this, method)
