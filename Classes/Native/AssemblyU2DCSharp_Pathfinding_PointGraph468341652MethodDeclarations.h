﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.PointGraph
struct PointGraph_t468341652;
// Pathfinding.GraphNodeDelegateCancelable
struct GraphNodeDelegateCancelable_t3591372971;
// Pathfinding.NNConstraint
struct NNConstraint_t758567699;
// Pathfinding.GraphNode
struct GraphNode_t23612370;
// Pathfinding.PointNode
struct PointNode_t2761813780;
// UnityEngine.Transform
struct Transform_t1659122786;
// OnScanStatus
struct OnScanStatus_t2412749870;
// Pathfinding.GraphUpdateObject
struct GraphUpdateObject_t430843704;
// Pathfinding.Serialization.GraphSerializationContext
struct GraphSerializationContext_t3256954663;
// AstarPath
struct AstarPath_t4090270936;
// Pathfinding.NavGraph
struct NavGraph_t1254319713;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_Int31974045594.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphNodeDelegateCan3591372971.h"
#include "AssemblyU2DCSharp_Pathfinding_NNInfo1570625892.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_Pathfinding_NNConstraint758567699.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphNode23612370.h"
#include "UnityEngine_UnityEngine_Transform1659122786.h"
#include "AssemblyU2DCSharp_Pathfinding_PointNode2761813780.h"
#include "AssemblyU2DCSharp_OnScanStatus2412749870.h"
#include "AssemblyU2DCSharp_GraphUpdateThreading3958092737.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphUpdateObject430843704.h"
#include "UnityEngine_UnityEngine_Matrix4x41651859333.h"
#include "AssemblyU2DCSharp_Pathfinding_Serialization_GraphS3256954663.h"
#include "AssemblyU2DCSharp_AstarPath4090270936.h"
#include "AssemblyU2DCSharp_Pathfinding_PointGraph468341652.h"
#include "UnityEngine_UnityEngine_Bounds2711641849.h"
#include "AssemblyU2DCSharp_Pathfinding_NavGraph1254319713.h"

// System.Void Pathfinding.PointGraph::.ctor()
extern "C"  void PointGraph__ctor_m433397587 (PointGraph_t468341652 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.PointGraph::.cctor()
extern "C"  void PointGraph__cctor_m68327098 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Int3 Pathfinding.PointGraph::WorldToLookupSpace(Pathfinding.Int3)
extern "C"  Int3_t1974045594  PointGraph_WorldToLookupSpace_m3587118001 (PointGraph_t468341652 * __this, Int3_t1974045594  ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.PointGraph::GetNodes(Pathfinding.GraphNodeDelegateCancelable)
extern "C"  void PointGraph_GetNodes_m514220455 (PointGraph_t468341652 * __this, GraphNodeDelegateCancelable_t3591372971 * ___del0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.NNInfo Pathfinding.PointGraph::GetNearest(UnityEngine.Vector3,Pathfinding.NNConstraint,Pathfinding.GraphNode)
extern "C"  NNInfo_t1570625892  PointGraph_GetNearest_m1758396842 (PointGraph_t468341652 * __this, Vector3_t4282066566  ___position0, NNConstraint_t758567699 * ___constraint1, GraphNode_t23612370 * ___hint2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.NNInfo Pathfinding.PointGraph::GetNearestForce(UnityEngine.Vector3,Pathfinding.NNConstraint)
extern "C"  NNInfo_t1570625892  PointGraph_GetNearestForce_m3060068341 (PointGraph_t468341652 * __this, Vector3_t4282066566  ___position0, NNConstraint_t758567699 * ___constraint1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.PointNode Pathfinding.PointGraph::AddNode(Pathfinding.Int3)
extern "C"  PointNode_t2761813780 * PointGraph_AddNode_m2476736955 (PointGraph_t468341652 * __this, Int3_t1974045594  ___position0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pathfinding.PointGraph::CountChildren(UnityEngine.Transform)
extern "C"  int32_t PointGraph_CountChildren_m3556285580 (Il2CppObject * __this /* static, unused */, Transform_t1659122786 * ___tr0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.PointGraph::AddChildren(System.Int32&,UnityEngine.Transform)
extern "C"  void PointGraph_AddChildren_m228609 (PointGraph_t468341652 * __this, int32_t* ___c0, Transform_t1659122786 * ___tr1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.PointGraph::RebuildNodeLookup()
extern "C"  void PointGraph_RebuildNodeLookup_m1925030728 (PointGraph_t468341652 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.PointGraph::AddToLookup(Pathfinding.PointNode)
extern "C"  void PointGraph_AddToLookup_m551408579 (PointGraph_t468341652 * __this, PointNode_t2761813780 * ___node0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.PointGraph::ScanInternal(OnScanStatus)
extern "C"  void PointGraph_ScanInternal_m3409169405 (PointGraph_t468341652 * __this, OnScanStatus_t2412749870 * ___statusCallback0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.PointGraph::IsValidConnection(Pathfinding.GraphNode,Pathfinding.GraphNode,System.Single&)
extern "C"  bool PointGraph_IsValidConnection_m2358701986 (PointGraph_t468341652 * __this, GraphNode_t23612370 * ___a0, GraphNode_t23612370 * ___b1, float* ___dist2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GraphUpdateThreading Pathfinding.PointGraph::CanUpdateAsync(Pathfinding.GraphUpdateObject)
extern "C"  int32_t PointGraph_CanUpdateAsync_m3217337238 (PointGraph_t468341652 * __this, GraphUpdateObject_t430843704 * ___o0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.PointGraph::UpdateAreaInit(Pathfinding.GraphUpdateObject)
extern "C"  void PointGraph_UpdateAreaInit_m3279074671 (PointGraph_t468341652 * __this, GraphUpdateObject_t430843704 * ___o0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.PointGraph::UpdateArea(Pathfinding.GraphUpdateObject)
extern "C"  void PointGraph_UpdateArea_m3601408415 (PointGraph_t468341652 * __this, GraphUpdateObject_t430843704 * ___guo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.PointGraph::PostDeserialization()
extern "C"  void PointGraph_PostDeserialization_m1572449536 (PointGraph_t468341652 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.PointGraph::RelocateNodes(UnityEngine.Matrix4x4,UnityEngine.Matrix4x4)
extern "C"  void PointGraph_RelocateNodes_m3486039217 (PointGraph_t468341652 * __this, Matrix4x4_t1651859333  ___oldMatrix0, Matrix4x4_t1651859333  ___newMatrix1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.PointGraph::SerializeExtraInfo(Pathfinding.Serialization.GraphSerializationContext)
extern "C"  void PointGraph_SerializeExtraInfo_m2033558590 (PointGraph_t468341652 * __this, GraphSerializationContext_t3256954663 * ___ctx0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.PointGraph::DeserializeExtraInfo(Pathfinding.Serialization.GraphSerializationContext)
extern "C"  void PointGraph_DeserializeExtraInfo_m1477648541 (PointGraph_t468341652 * __this, GraphSerializationContext_t3256954663 * ___ctx0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Int3 Pathfinding.PointGraph::ilo_get_zero1()
extern "C"  Int3_t1974045594  PointGraph_ilo_get_zero1_m923377359 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.PointGraph::ilo_Invoke2(Pathfinding.GraphNodeDelegateCancelable,Pathfinding.GraphNode)
extern "C"  bool PointGraph_ilo_Invoke2_m1428528053 (Il2CppObject * __this /* static, unused */, GraphNodeDelegateCancelable_t3591372971 * ____this0, GraphNode_t23612370 * ___node1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Pathfinding.PointGraph::ilo_get_maxNearestNodeDistanceSqr3(AstarPath)
extern "C"  float PointGraph_ilo_get_maxNearestNodeDistanceSqr3_m3421108657 (Il2CppObject * __this /* static, unused */, AstarPath_t4090270936 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Int3 Pathfinding.PointGraph::ilo_WorldToLookupSpace4(Pathfinding.PointGraph,Pathfinding.Int3)
extern "C"  Int3_t1974045594  PointGraph_ilo_WorldToLookupSpace4_m355047708 (Il2CppObject * __this /* static, unused */, PointGraph_t468341652 * ____this0, Int3_t1974045594  ___p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.PointGraph::ilo_Suitable5(Pathfinding.NNConstraint,Pathfinding.GraphNode)
extern "C"  bool PointGraph_ilo_Suitable5_m1241241687 (Il2CppObject * __this /* static, unused */, NNConstraint_t758567699 * ____this0, GraphNode_t23612370 * ___node1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Int3 Pathfinding.PointGraph::ilo_op_Addition6(Pathfinding.Int3,Pathfinding.Int3)
extern "C"  Int3_t1974045594  PointGraph_ilo_op_Addition6_m1077045015 (Il2CppObject * __this /* static, unused */, Int3_t1974045594  ___lhs0, Int3_t1974045594  ___rhs1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Pathfinding.PointGraph::ilo_op_Explicit7(Pathfinding.Int3)
extern "C"  Vector3_t4282066566  PointGraph_ilo_op_Explicit7_m4263679765 (Il2CppObject * __this /* static, unused */, Int3_t1974045594  ___ob0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.PointGraph::ilo_SetPosition8(Pathfinding.PointNode,Pathfinding.Int3)
extern "C"  void PointGraph_ilo_SetPosition8_m4274532799 (Il2CppObject * __this /* static, unused */, PointNode_t2761813780 * ____this0, Int3_t1974045594  ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Int3 Pathfinding.PointGraph::ilo_op_Explicit9(UnityEngine.Vector3)
extern "C"  Int3_t1974045594  PointGraph_ilo_op_Explicit9_m4174488319 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___ob0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.PointGraph::ilo_AddChildren10(Pathfinding.PointGraph,System.Int32&,UnityEngine.Transform)
extern "C"  void PointGraph_ilo_AddChildren10_m987208129 (Il2CppObject * __this /* static, unused */, PointGraph_t468341652 * ____this0, int32_t* ___c1, Transform_t1659122786 * ___tr2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.PointGraph::ilo_RebuildNodeLookup11(Pathfinding.PointGraph)
extern "C"  void PointGraph_ilo_RebuildNodeLookup11_m1556655373 (Il2CppObject * __this /* static, unused */, PointGraph_t468341652 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.PointGraph::ilo_IsValidConnection12(Pathfinding.PointGraph,Pathfinding.GraphNode,Pathfinding.GraphNode,System.Single&)
extern "C"  bool PointGraph_ilo_IsValidConnection12_m2067674088 (Il2CppObject * __this /* static, unused */, PointGraph_t468341652 * ____this0, GraphNode_t23612370 * ___a1, GraphNode_t23612370 * ___b2, float* ___dist3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.PointGraph::ilo_get_Walkable13(Pathfinding.GraphNode)
extern "C"  bool PointGraph_ilo_get_Walkable13_m4111928614 (Il2CppObject * __this /* static, unused */, GraphNode_t23612370 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Int3 Pathfinding.PointGraph::ilo_op_Subtraction14(Pathfinding.Int3,Pathfinding.Int3)
extern "C"  Int3_t1974045594  PointGraph_ilo_op_Subtraction14_m3482735386 (Il2CppObject * __this /* static, unused */, Int3_t1974045594  ___lhs0, Int3_t1974045594  ___rhs1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.PointGraph::ilo_LineIntersectsBounds15(UnityEngine.Bounds,UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  bool PointGraph_ilo_LineIntersectsBounds15_m3188647091 (Il2CppObject * __this /* static, unused */, Bounds_t2711641849  ___bounds0, Vector3_t4282066566  ___a1, Vector3_t4282066566  ___b2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.PointGraph::ilo_ContainsConnection16(Pathfinding.PointNode,Pathfinding.GraphNode)
extern "C"  bool PointGraph_ilo_ContainsConnection16_m1610620502 (Il2CppObject * __this /* static, unused */, PointNode_t2761813780 * ____this0, GraphNode_t23612370 * ___node1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.PointGraph::ilo_RelocateNodes17(Pathfinding.NavGraph,UnityEngine.Matrix4x4,UnityEngine.Matrix4x4)
extern "C"  void PointGraph_ilo_RelocateNodes17_m2144467875 (Il2CppObject * __this /* static, unused */, NavGraph_t1254319713 * ____this0, Matrix4x4_t1651859333  ___oldMatrix1, Matrix4x4_t1651859333  ___newMatrix2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
