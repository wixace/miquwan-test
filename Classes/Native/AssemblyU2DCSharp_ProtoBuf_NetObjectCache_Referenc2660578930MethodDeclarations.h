﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ProtoBuf.NetObjectCache/ReferenceComparer
struct ReferenceComparer_t2660578930;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void ProtoBuf.NetObjectCache/ReferenceComparer::.ctor()
extern "C"  void ReferenceComparer__ctor_m50945849 (ReferenceComparer_t2660578930 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.NetObjectCache/ReferenceComparer::.cctor()
extern "C"  void ReferenceComparer__cctor_m1097225108 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.NetObjectCache/ReferenceComparer::System.Collections.Generic.IEqualityComparer<object>.Equals(System.Object,System.Object)
extern "C"  bool ReferenceComparer_System_Collections_Generic_IEqualityComparerU3CobjectU3E_Equals_m3188115758 (ReferenceComparer_t2660578930 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ProtoBuf.NetObjectCache/ReferenceComparer::System.Collections.Generic.IEqualityComparer<object>.GetHashCode(System.Object)
extern "C"  int32_t ReferenceComparer_System_Collections_Generic_IEqualityComparerU3CobjectU3E_GetHashCode_m2917617936 (ReferenceComparer_t2660578930 * __this, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
