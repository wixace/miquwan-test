﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "UnityEngine_UnityEngine_Rect4241904616.h"
#include "UnityEngine_UnityEngine_Matrix4x41651859333.h"

// UnityEngine.Vector2 UnityEngine.GUIClip::Unclip(UnityEngine.Vector2)
extern "C"  Vector2_t4282066565  GUIClip_Unclip_m2253679574 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___pos0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.GUIClip::Clip(UnityEngine.Vector2)
extern "C"  Vector2_t4282066565  GUIClip_Clip_m3129963983 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___absolutePos0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIClip::Push(UnityEngine.Rect,UnityEngine.Vector2,UnityEngine.Vector2,System.Boolean)
extern "C"  void GUIClip_Push_m2350459808 (Il2CppObject * __this /* static, unused */, Rect_t4241904616  ___screenRect0, Vector2_t4282066565  ___scrollOffset1, Vector2_t4282066565  ___renderOffset2, bool ___resetOffset3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIClip::INTERNAL_CALL_Push(UnityEngine.Rect&,UnityEngine.Vector2&,UnityEngine.Vector2&,System.Boolean)
extern "C"  void GUIClip_INTERNAL_CALL_Push_m2072801093 (Il2CppObject * __this /* static, unused */, Rect_t4241904616 * ___screenRect0, Vector2_t4282066565 * ___scrollOffset1, Vector2_t4282066565 * ___renderOffset2, bool ___resetOffset3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIClip::Pop()
extern "C"  void GUIClip_Pop_m1032513917 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIClip::Unclip_Vector2(UnityEngine.Vector2&)
extern "C"  void GUIClip_Unclip_Vector2_m3965633441 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565 * ___pos0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIClip::INTERNAL_CALL_Unclip_Vector2(UnityEngine.Vector2&)
extern "C"  void GUIClip_INTERNAL_CALL_Unclip_Vector2_m76675360 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565 * ___pos0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIClip::Clip_Vector2(UnityEngine.Vector2&)
extern "C"  void GUIClip_Clip_Vector2_m3926926472 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565 * ___absolutePos0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIClip::INTERNAL_CALL_Clip_Vector2(UnityEngine.Vector2&)
extern "C"  void GUIClip_INTERNAL_CALL_Clip_Vector2_m3426790855 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565 * ___absolutePos0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Matrix4x4 UnityEngine.GUIClip::GetMatrix()
extern "C"  Matrix4x4_t1651859333  GUIClip_GetMatrix_m1079664894 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIClip::INTERNAL_CALL_GetMatrix(UnityEngine.Matrix4x4&)
extern "C"  void GUIClip_INTERNAL_CALL_GetMatrix_m496894294 (Il2CppObject * __this /* static, unused */, Matrix4x4_t1651859333 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIClip::SetMatrix(UnityEngine.Matrix4x4)
extern "C"  void GUIClip_SetMatrix_m2357843627 (Il2CppObject * __this /* static, unused */, Matrix4x4_t1651859333  ___m0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIClip::INTERNAL_CALL_SetMatrix(UnityEngine.Matrix4x4&)
extern "C"  void GUIClip_INTERNAL_CALL_SetMatrix_m62574946 (Il2CppObject * __this /* static, unused */, Matrix4x4_t1651859333 * ___m0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
