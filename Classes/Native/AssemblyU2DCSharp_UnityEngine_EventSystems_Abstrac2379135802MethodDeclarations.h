﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_EventSystems_AbstractEventDataGenerated
struct UnityEngine_EventSystems_AbstractEventDataGenerated_t2379135802;
// JSVCall
struct JSVCall_t3708497963;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"

// System.Void UnityEngine_EventSystems_AbstractEventDataGenerated::.ctor()
extern "C"  void UnityEngine_EventSystems_AbstractEventDataGenerated__ctor_m2234497393 (UnityEngine_EventSystems_AbstractEventDataGenerated_t2379135802 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_AbstractEventDataGenerated::AbstractEventData_used(JSVCall)
extern "C"  void UnityEngine_EventSystems_AbstractEventDataGenerated_AbstractEventData_used_m2868274138 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_EventSystems_AbstractEventDataGenerated::AbstractEventData_Reset(JSVCall,System.Int32)
extern "C"  bool UnityEngine_EventSystems_AbstractEventDataGenerated_AbstractEventData_Reset_m1615801957 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_EventSystems_AbstractEventDataGenerated::AbstractEventData_Use(JSVCall,System.Int32)
extern "C"  bool UnityEngine_EventSystems_AbstractEventDataGenerated_AbstractEventData_Use_m1778051549 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_AbstractEventDataGenerated::__Register()
extern "C"  void UnityEngine_EventSystems_AbstractEventDataGenerated___Register_m925958646 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_AbstractEventDataGenerated::ilo_setBooleanS1(System.Int32,System.Boolean)
extern "C"  void UnityEngine_EventSystems_AbstractEventDataGenerated_ilo_setBooleanS1_m3864624930 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
