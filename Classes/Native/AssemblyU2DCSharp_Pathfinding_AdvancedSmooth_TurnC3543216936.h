﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.AdvancedSmooth/TurnConstructor
struct  TurnConstructor_t3543216936  : public Il2CppObject
{
public:
	// System.Single Pathfinding.AdvancedSmooth/TurnConstructor::constantBias
	float ___constantBias_1;
	// System.Single Pathfinding.AdvancedSmooth/TurnConstructor::factorBias
	float ___factorBias_2;

public:
	inline static int32_t get_offset_of_constantBias_1() { return static_cast<int32_t>(offsetof(TurnConstructor_t3543216936, ___constantBias_1)); }
	inline float get_constantBias_1() const { return ___constantBias_1; }
	inline float* get_address_of_constantBias_1() { return &___constantBias_1; }
	inline void set_constantBias_1(float value)
	{
		___constantBias_1 = value;
	}

	inline static int32_t get_offset_of_factorBias_2() { return static_cast<int32_t>(offsetof(TurnConstructor_t3543216936, ___factorBias_2)); }
	inline float get_factorBias_2() const { return ___factorBias_2; }
	inline float* get_address_of_factorBias_2() { return &___factorBias_2; }
	inline void set_factorBias_2(float value)
	{
		___factorBias_2 = value;
	}
};

struct TurnConstructor_t3543216936_StaticFields
{
public:
	// System.Single Pathfinding.AdvancedSmooth/TurnConstructor::turningRadius
	float ___turningRadius_3;
	// UnityEngine.Vector3 Pathfinding.AdvancedSmooth/TurnConstructor::prev
	Vector3_t4282066566  ___prev_4;
	// UnityEngine.Vector3 Pathfinding.AdvancedSmooth/TurnConstructor::current
	Vector3_t4282066566  ___current_5;
	// UnityEngine.Vector3 Pathfinding.AdvancedSmooth/TurnConstructor::next
	Vector3_t4282066566  ___next_6;
	// UnityEngine.Vector3 Pathfinding.AdvancedSmooth/TurnConstructor::t1
	Vector3_t4282066566  ___t1_7;
	// UnityEngine.Vector3 Pathfinding.AdvancedSmooth/TurnConstructor::t2
	Vector3_t4282066566  ___t2_8;
	// UnityEngine.Vector3 Pathfinding.AdvancedSmooth/TurnConstructor::normal
	Vector3_t4282066566  ___normal_9;
	// UnityEngine.Vector3 Pathfinding.AdvancedSmooth/TurnConstructor::prevNormal
	Vector3_t4282066566  ___prevNormal_10;
	// System.Boolean Pathfinding.AdvancedSmooth/TurnConstructor::changedPreviousTangent
	bool ___changedPreviousTangent_11;

public:
	inline static int32_t get_offset_of_turningRadius_3() { return static_cast<int32_t>(offsetof(TurnConstructor_t3543216936_StaticFields, ___turningRadius_3)); }
	inline float get_turningRadius_3() const { return ___turningRadius_3; }
	inline float* get_address_of_turningRadius_3() { return &___turningRadius_3; }
	inline void set_turningRadius_3(float value)
	{
		___turningRadius_3 = value;
	}

	inline static int32_t get_offset_of_prev_4() { return static_cast<int32_t>(offsetof(TurnConstructor_t3543216936_StaticFields, ___prev_4)); }
	inline Vector3_t4282066566  get_prev_4() const { return ___prev_4; }
	inline Vector3_t4282066566 * get_address_of_prev_4() { return &___prev_4; }
	inline void set_prev_4(Vector3_t4282066566  value)
	{
		___prev_4 = value;
	}

	inline static int32_t get_offset_of_current_5() { return static_cast<int32_t>(offsetof(TurnConstructor_t3543216936_StaticFields, ___current_5)); }
	inline Vector3_t4282066566  get_current_5() const { return ___current_5; }
	inline Vector3_t4282066566 * get_address_of_current_5() { return &___current_5; }
	inline void set_current_5(Vector3_t4282066566  value)
	{
		___current_5 = value;
	}

	inline static int32_t get_offset_of_next_6() { return static_cast<int32_t>(offsetof(TurnConstructor_t3543216936_StaticFields, ___next_6)); }
	inline Vector3_t4282066566  get_next_6() const { return ___next_6; }
	inline Vector3_t4282066566 * get_address_of_next_6() { return &___next_6; }
	inline void set_next_6(Vector3_t4282066566  value)
	{
		___next_6 = value;
	}

	inline static int32_t get_offset_of_t1_7() { return static_cast<int32_t>(offsetof(TurnConstructor_t3543216936_StaticFields, ___t1_7)); }
	inline Vector3_t4282066566  get_t1_7() const { return ___t1_7; }
	inline Vector3_t4282066566 * get_address_of_t1_7() { return &___t1_7; }
	inline void set_t1_7(Vector3_t4282066566  value)
	{
		___t1_7 = value;
	}

	inline static int32_t get_offset_of_t2_8() { return static_cast<int32_t>(offsetof(TurnConstructor_t3543216936_StaticFields, ___t2_8)); }
	inline Vector3_t4282066566  get_t2_8() const { return ___t2_8; }
	inline Vector3_t4282066566 * get_address_of_t2_8() { return &___t2_8; }
	inline void set_t2_8(Vector3_t4282066566  value)
	{
		___t2_8 = value;
	}

	inline static int32_t get_offset_of_normal_9() { return static_cast<int32_t>(offsetof(TurnConstructor_t3543216936_StaticFields, ___normal_9)); }
	inline Vector3_t4282066566  get_normal_9() const { return ___normal_9; }
	inline Vector3_t4282066566 * get_address_of_normal_9() { return &___normal_9; }
	inline void set_normal_9(Vector3_t4282066566  value)
	{
		___normal_9 = value;
	}

	inline static int32_t get_offset_of_prevNormal_10() { return static_cast<int32_t>(offsetof(TurnConstructor_t3543216936_StaticFields, ___prevNormal_10)); }
	inline Vector3_t4282066566  get_prevNormal_10() const { return ___prevNormal_10; }
	inline Vector3_t4282066566 * get_address_of_prevNormal_10() { return &___prevNormal_10; }
	inline void set_prevNormal_10(Vector3_t4282066566  value)
	{
		___prevNormal_10 = value;
	}

	inline static int32_t get_offset_of_changedPreviousTangent_11() { return static_cast<int32_t>(offsetof(TurnConstructor_t3543216936_StaticFields, ___changedPreviousTangent_11)); }
	inline bool get_changedPreviousTangent_11() const { return ___changedPreviousTangent_11; }
	inline bool* get_address_of_changedPreviousTangent_11() { return &___changedPreviousTangent_11; }
	inline void set_changedPreviousTangent_11(bool value)
	{
		___changedPreviousTangent_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
