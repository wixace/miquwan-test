﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EArrayGenerated
struct EArrayGenerated_t2664647387;
// JSVCall
struct JSVCall_t3708497963;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Type
struct Type_t;
// MethodID
struct MethodID_t3916401116;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Type2863145774.h"
#include "AssemblyU2DCSharp_MethodID3916401116.h"

// System.Void EArrayGenerated::.ctor()
extern "C"  void EArrayGenerated__ctor_m1715176176 (EArrayGenerated_t2664647387 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EArrayGenerated::.cctor()
extern "C"  void EArrayGenerated__cctor_m1148757693 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean EArrayGenerated::EArray_IndexOf__Array__Object__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool EArrayGenerated_EArray_IndexOf__Array__Object__Int32__Int32_m1587762636 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean EArrayGenerated::EArray_IndexOfT1__T_Array__T__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool EArrayGenerated_EArray_IndexOfT1__T_Array__T__Int32__Int32_m98306417 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean EArrayGenerated::EArray_IndexOfT1__T_Array__T__Int32(JSVCall,System.Int32)
extern "C"  bool EArrayGenerated_EArray_IndexOfT1__T_Array__T__Int32_m1164464383 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean EArrayGenerated::EArray_IndexOf__Array__Object__Int32(JSVCall,System.Int32)
extern "C"  bool EArrayGenerated_EArray_IndexOf__Array__Object__Int32_m3884475652 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean EArrayGenerated::EArray_IndexOf__Array__Object(JSVCall,System.Int32)
extern "C"  bool EArrayGenerated_EArray_IndexOf__Array__Object_m22906060 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean EArrayGenerated::EArray_IndexOfT1__T_Array__T(JSVCall,System.Int32)
extern "C"  bool EArrayGenerated_EArray_IndexOfT1__T_Array__T_m2663599409 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean EArrayGenerated::EArray_LastIndexOf__Array__Object__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool EArrayGenerated_EArray_LastIndexOf__Array__Object__Int32__Int32_m2717202178 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean EArrayGenerated::EArray_LastIndexOfT1__T_Array__T__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool EArrayGenerated_EArray_LastIndexOfT1__T_Array__T__Int32__Int32_m550381947 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean EArrayGenerated::EArray_LastIndexOfT1__T_Array__T__Int32(JSVCall,System.Int32)
extern "C"  bool EArrayGenerated_EArray_LastIndexOfT1__T_Array__T__Int32_m3417425973 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean EArrayGenerated::EArray_LastIndexOf__Array__Object__Int32(JSVCall,System.Int32)
extern "C"  bool EArrayGenerated_EArray_LastIndexOf__Array__Object__Int32_m711840910 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean EArrayGenerated::EArray_LastIndexOf__Array__Object(JSVCall,System.Int32)
extern "C"  bool EArrayGenerated_EArray_LastIndexOf__Array__Object_m2097552514 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean EArrayGenerated::EArray_LastIndexOfT1__T_Array__T(JSVCall,System.Int32)
extern "C"  bool EArrayGenerated_EArray_LastIndexOfT1__T_Array__T_m1483597499 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EArrayGenerated::__Register()
extern "C"  void EArrayGenerated___Register_m466767831 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] EArrayGenerated::<EArray_IndexOfT1__T_Array__T__Int32__Int32>m__3A()
extern "C"  ObjectU5BU5D_t1108656482* EArrayGenerated_U3CEArray_IndexOfT1__T_Array__T__Int32__Int32U3Em__3A_m2303107867 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] EArrayGenerated::<EArray_IndexOfT1__T_Array__T__Int32>m__3B()
extern "C"  ObjectU5BU5D_t1108656482* EArrayGenerated_U3CEArray_IndexOfT1__T_Array__T__Int32U3Em__3B_m1446944016 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] EArrayGenerated::<EArray_IndexOfT1__T_Array__T>m__3C()
extern "C"  ObjectU5BU5D_t1108656482* EArrayGenerated_U3CEArray_IndexOfT1__T_Array__TU3Em__3C_m898661661 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] EArrayGenerated::<EArray_LastIndexOfT1__T_Array__T__Int32__Int32>m__3D()
extern "C"  ObjectU5BU5D_t1108656482* EArrayGenerated_U3CEArray_LastIndexOfT1__T_Array__T__Int32__Int32U3Em__3D_m1715855720 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] EArrayGenerated::<EArray_LastIndexOfT1__T_Array__T__Int32>m__3E()
extern "C"  ObjectU5BU5D_t1108656482* EArrayGenerated_U3CEArray_LastIndexOfT1__T_Array__T__Int32U3Em__3E_m414870665 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] EArrayGenerated::<EArray_LastIndexOfT1__T_Array__T>m__3F()
extern "C"  ObjectU5BU5D_t1108656482* EArrayGenerated_U3CEArray_LastIndexOfT1__T_Array__TU3Em__3F_m3277157610 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object EArrayGenerated::ilo_getWhatever1(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * EArrayGenerated_ilo_getWhatever1_m2440851442 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 EArrayGenerated::ilo_getInt322(System.Int32)
extern "C"  int32_t EArrayGenerated_ilo_getInt322_m748339668 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EArrayGenerated::ilo_setInt323(System.Int32,System.Int32)
extern "C"  void EArrayGenerated_ilo_setInt323_m433470884 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo EArrayGenerated::ilo_makeGenericMethod4(System.Type,MethodID,System.Int32)
extern "C"  MethodInfo_t * EArrayGenerated_ilo_makeGenericMethod4_m1997535647 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, MethodID_t3916401116 * ___methodID1, int32_t ___TCount2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 EArrayGenerated::ilo_getArrayLength5(System.Int32)
extern "C"  int32_t EArrayGenerated_ilo_getArrayLength5_m2154330598 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 EArrayGenerated::ilo_getElement6(System.Int32,System.Int32)
extern "C"  int32_t EArrayGenerated_ilo_getElement6_m370966509 (Il2CppObject * __this /* static, unused */, int32_t ___id0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 EArrayGenerated::ilo_getObject7(System.Int32)
extern "C"  int32_t EArrayGenerated_ilo_getObject7_m3138417356 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
