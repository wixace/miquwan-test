﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<Pathfinding.AdvancedSmooth/Turn>
struct Collection_1_t3945620832;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// Pathfinding.AdvancedSmooth/Turn[]
struct TurnU5BU5D_t2705444999;
// System.Collections.Generic.IEnumerator`1<Pathfinding.AdvancedSmooth/Turn>
struct IEnumerator_1_t2377060427;
// System.Collections.Generic.IList`1<Pathfinding.AdvancedSmooth/Turn>
struct IList_1_t3159842581;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_Pathfinding_AdvancedSmooth_Turn465195378.h"

// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.AdvancedSmooth/Turn>::.ctor()
extern "C"  void Collection_1__ctor_m2685333173_gshared (Collection_1_t3945620832 * __this, const MethodInfo* method);
#define Collection_1__ctor_m2685333173(__this, method) ((  void (*) (Collection_1_t3945620832 *, const MethodInfo*))Collection_1__ctor_m2685333173_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Pathfinding.AdvancedSmooth/Turn>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2819355814_gshared (Collection_1_t3945620832 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2819355814(__this, method) ((  bool (*) (Collection_1_t3945620832 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2819355814_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.AdvancedSmooth/Turn>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Collection_1_System_Collections_ICollection_CopyTo_m2115979311_gshared (Collection_1_t3945620832 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m2115979311(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t3945620832 *, Il2CppArray *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m2115979311_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<Pathfinding.AdvancedSmooth/Turn>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Collection_1_System_Collections_IEnumerable_GetEnumerator_m2176941546_gshared (Collection_1_t3945620832 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m2176941546(__this, method) ((  Il2CppObject * (*) (Collection_1_t3945620832 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m2176941546_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Pathfinding.AdvancedSmooth/Turn>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_Add_m1274185799_gshared (Collection_1_t3945620832 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m1274185799(__this, ___value0, method) ((  int32_t (*) (Collection_1_t3945620832 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m1274185799_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Pathfinding.AdvancedSmooth/Turn>::System.Collections.IList.Contains(System.Object)
extern "C"  bool Collection_1_System_Collections_IList_Contains_m2226757349_gshared (Collection_1_t3945620832 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m2226757349(__this, ___value0, method) ((  bool (*) (Collection_1_t3945620832 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m2226757349_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Pathfinding.AdvancedSmooth/Turn>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_IndexOf_m848773919_gshared (Collection_1_t3945620832 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m848773919(__this, ___value0, method) ((  int32_t (*) (Collection_1_t3945620832 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m848773919_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.AdvancedSmooth/Turn>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_Insert_m713628362_gshared (Collection_1_t3945620832 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m713628362(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t3945620832 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m713628362_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.AdvancedSmooth/Turn>::System.Collections.IList.Remove(System.Object)
extern "C"  void Collection_1_System_Collections_IList_Remove_m191155742_gshared (Collection_1_t3945620832 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m191155742(__this, ___value0, method) ((  void (*) (Collection_1_t3945620832 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m191155742_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Pathfinding.AdvancedSmooth/Turn>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m1553273431_gshared (Collection_1_t3945620832 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m1553273431(__this, method) ((  bool (*) (Collection_1_t3945620832 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m1553273431_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<Pathfinding.AdvancedSmooth/Turn>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Collection_1_System_Collections_ICollection_get_SyncRoot_m1947860995_gshared (Collection_1_t3945620832 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m1947860995(__this, method) ((  Il2CppObject * (*) (Collection_1_t3945620832 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m1947860995_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Pathfinding.AdvancedSmooth/Turn>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool Collection_1_System_Collections_IList_get_IsFixedSize_m2055708180_gshared (Collection_1_t3945620832 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m2055708180(__this, method) ((  bool (*) (Collection_1_t3945620832 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m2055708180_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Pathfinding.AdvancedSmooth/Turn>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_IList_get_IsReadOnly_m1213058661_gshared (Collection_1_t3945620832 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m1213058661(__this, method) ((  bool (*) (Collection_1_t3945620832 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m1213058661_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<Pathfinding.AdvancedSmooth/Turn>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * Collection_1_System_Collections_IList_get_Item_m354286794_gshared (Collection_1_t3945620832 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m354286794(__this, ___index0, method) ((  Il2CppObject * (*) (Collection_1_t3945620832 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m354286794_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.AdvancedSmooth/Turn>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_set_Item_m3511008289_gshared (Collection_1_t3945620832 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m3511008289(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t3945620832 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m3511008289_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.AdvancedSmooth/Turn>::Add(T)
extern "C"  void Collection_1_Add_m3401901866_gshared (Collection_1_t3945620832 * __this, Turn_t465195378  ___item0, const MethodInfo* method);
#define Collection_1_Add_m3401901866(__this, ___item0, method) ((  void (*) (Collection_1_t3945620832 *, Turn_t465195378 , const MethodInfo*))Collection_1_Add_m3401901866_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.AdvancedSmooth/Turn>::Clear()
extern "C"  void Collection_1_Clear_m91466464_gshared (Collection_1_t3945620832 * __this, const MethodInfo* method);
#define Collection_1_Clear_m91466464(__this, method) ((  void (*) (Collection_1_t3945620832 *, const MethodInfo*))Collection_1_Clear_m91466464_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.AdvancedSmooth/Turn>::ClearItems()
extern "C"  void Collection_1_ClearItems_m1475330594_gshared (Collection_1_t3945620832 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m1475330594(__this, method) ((  void (*) (Collection_1_t3945620832 *, const MethodInfo*))Collection_1_ClearItems_m1475330594_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Pathfinding.AdvancedSmooth/Turn>::Contains(T)
extern "C"  bool Collection_1_Contains_m2618183438_gshared (Collection_1_t3945620832 * __this, Turn_t465195378  ___item0, const MethodInfo* method);
#define Collection_1_Contains_m2618183438(__this, ___item0, method) ((  bool (*) (Collection_1_t3945620832 *, Turn_t465195378 , const MethodInfo*))Collection_1_Contains_m2618183438_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.AdvancedSmooth/Turn>::CopyTo(T[],System.Int32)
extern "C"  void Collection_1_CopyTo_m1796413594_gshared (Collection_1_t3945620832 * __this, TurnU5BU5D_t2705444999* ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_CopyTo_m1796413594(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t3945620832 *, TurnU5BU5D_t2705444999*, int32_t, const MethodInfo*))Collection_1_CopyTo_m1796413594_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<Pathfinding.AdvancedSmooth/Turn>::GetEnumerator()
extern "C"  Il2CppObject* Collection_1_GetEnumerator_m2805358961_gshared (Collection_1_t3945620832 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m2805358961(__this, method) ((  Il2CppObject* (*) (Collection_1_t3945620832 *, const MethodInfo*))Collection_1_GetEnumerator_m2805358961_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Pathfinding.AdvancedSmooth/Turn>::IndexOf(T)
extern "C"  int32_t Collection_1_IndexOf_m242506142_gshared (Collection_1_t3945620832 * __this, Turn_t465195378  ___item0, const MethodInfo* method);
#define Collection_1_IndexOf_m242506142(__this, ___item0, method) ((  int32_t (*) (Collection_1_t3945620832 *, Turn_t465195378 , const MethodInfo*))Collection_1_IndexOf_m242506142_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.AdvancedSmooth/Turn>::Insert(System.Int32,T)
extern "C"  void Collection_1_Insert_m2213711249_gshared (Collection_1_t3945620832 * __this, int32_t ___index0, Turn_t465195378  ___item1, const MethodInfo* method);
#define Collection_1_Insert_m2213711249(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t3945620832 *, int32_t, Turn_t465195378 , const MethodInfo*))Collection_1_Insert_m2213711249_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.AdvancedSmooth/Turn>::InsertItem(System.Int32,T)
extern "C"  void Collection_1_InsertItem_m1737919300_gshared (Collection_1_t3945620832 * __this, int32_t ___index0, Turn_t465195378  ___item1, const MethodInfo* method);
#define Collection_1_InsertItem_m1737919300(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t3945620832 *, int32_t, Turn_t465195378 , const MethodInfo*))Collection_1_InsertItem_m1737919300_gshared)(__this, ___index0, ___item1, method)
// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.Collection`1<Pathfinding.AdvancedSmooth/Turn>::get_Items()
extern "C"  Il2CppObject* Collection_1_get_Items_m2143711348_gshared (Collection_1_t3945620832 * __this, const MethodInfo* method);
#define Collection_1_get_Items_m2143711348(__this, method) ((  Il2CppObject* (*) (Collection_1_t3945620832 *, const MethodInfo*))Collection_1_get_Items_m2143711348_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Pathfinding.AdvancedSmooth/Turn>::Remove(T)
extern "C"  bool Collection_1_Remove_m520583817_gshared (Collection_1_t3945620832 * __this, Turn_t465195378  ___item0, const MethodInfo* method);
#define Collection_1_Remove_m520583817(__this, ___item0, method) ((  bool (*) (Collection_1_t3945620832 *, Turn_t465195378 , const MethodInfo*))Collection_1_Remove_m520583817_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.AdvancedSmooth/Turn>::RemoveAt(System.Int32)
extern "C"  void Collection_1_RemoveAt_m87564119_gshared (Collection_1_t3945620832 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveAt_m87564119(__this, ___index0, method) ((  void (*) (Collection_1_t3945620832 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m87564119_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.AdvancedSmooth/Turn>::RemoveItem(System.Int32)
extern "C"  void Collection_1_RemoveItem_m1149397431_gshared (Collection_1_t3945620832 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveItem_m1149397431(__this, ___index0, method) ((  void (*) (Collection_1_t3945620832 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m1149397431_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Pathfinding.AdvancedSmooth/Turn>::get_Count()
extern "C"  int32_t Collection_1_get_Count_m3040030621_gshared (Collection_1_t3945620832 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m3040030621(__this, method) ((  int32_t (*) (Collection_1_t3945620832 *, const MethodInfo*))Collection_1_get_Count_m3040030621_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<Pathfinding.AdvancedSmooth/Turn>::get_Item(System.Int32)
extern "C"  Turn_t465195378  Collection_1_get_Item_m2652464219_gshared (Collection_1_t3945620832 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_get_Item_m2652464219(__this, ___index0, method) ((  Turn_t465195378  (*) (Collection_1_t3945620832 *, int32_t, const MethodInfo*))Collection_1_get_Item_m2652464219_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.AdvancedSmooth/Turn>::set_Item(System.Int32,T)
extern "C"  void Collection_1_set_Item_m3177295400_gshared (Collection_1_t3945620832 * __this, int32_t ___index0, Turn_t465195378  ___value1, const MethodInfo* method);
#define Collection_1_set_Item_m3177295400(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t3945620832 *, int32_t, Turn_t465195378 , const MethodInfo*))Collection_1_set_Item_m3177295400_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.AdvancedSmooth/Turn>::SetItem(System.Int32,T)
extern "C"  void Collection_1_SetItem_m1222161745_gshared (Collection_1_t3945620832 * __this, int32_t ___index0, Turn_t465195378  ___item1, const MethodInfo* method);
#define Collection_1_SetItem_m1222161745(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t3945620832 *, int32_t, Turn_t465195378 , const MethodInfo*))Collection_1_SetItem_m1222161745_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Pathfinding.AdvancedSmooth/Turn>::IsValidItem(System.Object)
extern "C"  bool Collection_1_IsValidItem_m316958046_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_IsValidItem_m316958046(__this /* static, unused */, ___item0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_IsValidItem_m316958046_gshared)(__this /* static, unused */, ___item0, method)
// T System.Collections.ObjectModel.Collection`1<Pathfinding.AdvancedSmooth/Turn>::ConvertItem(System.Object)
extern "C"  Turn_t465195378  Collection_1_ConvertItem_m3271781818_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_ConvertItem_m3271781818(__this /* static, unused */, ___item0, method) ((  Turn_t465195378  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_ConvertItem_m3271781818_gshared)(__this /* static, unused */, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.AdvancedSmooth/Turn>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C"  void Collection_1_CheckWritable_m3551172442_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_CheckWritable_m3551172442(__this /* static, unused */, ___list0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_CheckWritable_m3551172442_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Pathfinding.AdvancedSmooth/Turn>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsSynchronized_m2859201190_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsSynchronized_m2859201190(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsSynchronized_m2859201190_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Pathfinding.AdvancedSmooth/Turn>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsFixedSize_m1131005369_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsFixedSize_m1131005369(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsFixedSize_m1131005369_gshared)(__this /* static, unused */, ___list0, method)
