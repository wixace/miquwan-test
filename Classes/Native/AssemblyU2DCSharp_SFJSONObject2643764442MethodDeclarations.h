﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SFJSONObject
struct SFJSONObject_t2643764442;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void SFJSONObject::.ctor()
extern "C"  void SFJSONObject__ctor_m62268161 (SFJSONObject_t2643764442 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SFJSONObject::.ctor(System.String)
extern "C"  void SFJSONObject__ctor_m1054483873 (SFJSONObject_t2643764442 * __this, String_t* ___json0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SFJSONObject::get(System.String)
extern "C"  Il2CppObject * SFJSONObject_get_m1043179586 (SFJSONObject_t2643764442 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SFJSONObject::put(System.String,System.Object)
extern "C"  void SFJSONObject_put_m299840130 (SFJSONObject_t2643764442 * __this, String_t* ___key0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SFJSONObject::toString()
extern "C"  String_t* SFJSONObject_toString_m3175944594 (SFJSONObject_t2643764442 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SFJSONObject::toInlineString()
extern "C"  String_t* SFJSONObject_toInlineString_m1059381067 (SFJSONObject_t2643764442 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
