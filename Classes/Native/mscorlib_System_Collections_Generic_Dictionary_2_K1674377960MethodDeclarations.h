﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K3675191317MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<SoundTypeID,ISound>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define KeyCollection__ctor_m1377159446(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t1674377960 *, Dictionary_2_t47618509 *, const MethodInfo*))KeyCollection__ctor_m1559252174_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<SoundTypeID,ISound>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1590672000(__this, ___item0, method) ((  void (*) (KeyCollection_t1674377960 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m4000302024_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<SoundTypeID,ISound>::System.Collections.Generic.ICollection<TKey>.Clear()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m2227339255(__this, method) ((  void (*) (KeyCollection_t1674377960 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3588504383_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<SoundTypeID,ISound>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m2759466318(__this, ___item0, method) ((  bool (*) (KeyCollection_t1674377960 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m3131629506_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<SoundTypeID,ISound>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2309589555(__this, ___item0, method) ((  bool (*) (KeyCollection_t1674377960 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m365844903_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<SoundTypeID,ISound>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m978711881(__this, method) ((  Il2CppObject* (*) (KeyCollection_t1674377960 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m552096571_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<SoundTypeID,ISound>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define KeyCollection_System_Collections_ICollection_CopyTo_m1388915177(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t1674377960 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m3305388337_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<SoundTypeID,ISound>::System.Collections.IEnumerable.GetEnumerator()
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m3222297272(__this, method) ((  Il2CppObject * (*) (KeyCollection_t1674377960 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1119244396_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<SoundTypeID,ISound>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m438401327(__this, method) ((  bool (*) (KeyCollection_t1674377960 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m1604939427_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<SoundTypeID,ISound>::System.Collections.ICollection.get_IsSynchronized()
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1776378017(__this, method) ((  bool (*) (KeyCollection_t1674377960 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m3890468629_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<SoundTypeID,ISound>::System.Collections.ICollection.get_SyncRoot()
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m427404179(__this, method) ((  Il2CppObject * (*) (KeyCollection_t1674377960 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m3050212545_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<SoundTypeID,ISound>::CopyTo(TKey[],System.Int32)
#define KeyCollection_CopyTo_m909969355(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t1674377960 *, SoundTypeIDU5BU5D_t1111200077*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m2085907331_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<SoundTypeID,ISound>::GetEnumerator()
#define KeyCollection_GetEnumerator_m928560024(__this, method) ((  Enumerator_t662554563  (*) (KeyCollection_t1674377960 *, const MethodInfo*))KeyCollection_GetEnumerator_m2696925350_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<SoundTypeID,ISound>::get_Count()
#define KeyCollection_get_Count_m3857361243(__this, method) ((  int32_t (*) (KeyCollection_t1674377960 *, const MethodInfo*))KeyCollection_get_Count_m1814660955_gshared)(__this, method)
