﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FPSCounter
struct  FPSCounter_t1808341236  : public MonoBehaviour_t667441552
{
public:
	// System.Single FPSCounter::updateInterval
	float ___updateInterval_2;
	// System.Int32 FPSCounter::x_location
	int32_t ___x_location_3;
	// System.Int32 FPSCounter::y_location
	int32_t ___y_location_4;
	// System.Double FPSCounter::lastInterval
	double ___lastInterval_5;
	// System.Int32 FPSCounter::frames
	int32_t ___frames_6;
	// System.Single FPSCounter::fps
	float ___fps_7;

public:
	inline static int32_t get_offset_of_updateInterval_2() { return static_cast<int32_t>(offsetof(FPSCounter_t1808341236, ___updateInterval_2)); }
	inline float get_updateInterval_2() const { return ___updateInterval_2; }
	inline float* get_address_of_updateInterval_2() { return &___updateInterval_2; }
	inline void set_updateInterval_2(float value)
	{
		___updateInterval_2 = value;
	}

	inline static int32_t get_offset_of_x_location_3() { return static_cast<int32_t>(offsetof(FPSCounter_t1808341236, ___x_location_3)); }
	inline int32_t get_x_location_3() const { return ___x_location_3; }
	inline int32_t* get_address_of_x_location_3() { return &___x_location_3; }
	inline void set_x_location_3(int32_t value)
	{
		___x_location_3 = value;
	}

	inline static int32_t get_offset_of_y_location_4() { return static_cast<int32_t>(offsetof(FPSCounter_t1808341236, ___y_location_4)); }
	inline int32_t get_y_location_4() const { return ___y_location_4; }
	inline int32_t* get_address_of_y_location_4() { return &___y_location_4; }
	inline void set_y_location_4(int32_t value)
	{
		___y_location_4 = value;
	}

	inline static int32_t get_offset_of_lastInterval_5() { return static_cast<int32_t>(offsetof(FPSCounter_t1808341236, ___lastInterval_5)); }
	inline double get_lastInterval_5() const { return ___lastInterval_5; }
	inline double* get_address_of_lastInterval_5() { return &___lastInterval_5; }
	inline void set_lastInterval_5(double value)
	{
		___lastInterval_5 = value;
	}

	inline static int32_t get_offset_of_frames_6() { return static_cast<int32_t>(offsetof(FPSCounter_t1808341236, ___frames_6)); }
	inline int32_t get_frames_6() const { return ___frames_6; }
	inline int32_t* get_address_of_frames_6() { return &___frames_6; }
	inline void set_frames_6(int32_t value)
	{
		___frames_6 = value;
	}

	inline static int32_t get_offset_of_fps_7() { return static_cast<int32_t>(offsetof(FPSCounter_t1808341236, ___fps_7)); }
	inline float get_fps_7() const { return ___fps_7; }
	inline float* get_address_of_fps_7() { return &___fps_7; }
	inline void set_fps_7(float value)
	{
		___fps_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
