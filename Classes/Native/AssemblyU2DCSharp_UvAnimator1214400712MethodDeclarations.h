﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UvAnimator
struct UvAnimator_t1214400712;

#include "codegen/il2cpp-codegen.h"

// System.Void UvAnimator::.ctor()
extern "C"  void UvAnimator__ctor_m584757459 (UvAnimator_t1214400712 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UvAnimator::Update()
extern "C"  void UvAnimator_Update_m2674474138 (UvAnimator_t1214400712 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
