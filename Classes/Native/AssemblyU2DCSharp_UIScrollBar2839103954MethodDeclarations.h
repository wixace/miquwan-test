﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIScrollBar
struct UIScrollBar_t2839103954;
// UIProgressBar
struct UIProgressBar_t168062834;
// System.Collections.Generic.List`1<EventDelegate>
struct List_1_t1077642479;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t215400611;
// UIWidget
struct UIWidget_t769069560;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "AssemblyU2DCSharp_UIProgressBar168062834.h"
#include "AssemblyU2DCSharp_UIWidget769069560.h"
#include "UnityEngine_UnityEngine_Vector44282066567.h"

// System.Void UIScrollBar::.ctor()
extern "C"  void UIScrollBar__ctor_m1335041497 (UIScrollBar_t2839103954 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UIScrollBar::get_scrollValue()
extern "C"  float UIScrollBar_get_scrollValue_m3211600862 (UIScrollBar_t2839103954 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIScrollBar::set_scrollValue(System.Single)
extern "C"  void UIScrollBar_set_scrollValue_m2774550093 (UIScrollBar_t2839103954 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UIScrollBar::get_barSize()
extern "C"  float UIScrollBar_get_barSize_m2815787118 (UIScrollBar_t2839103954 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIScrollBar::set_barSize(System.Single)
extern "C"  void UIScrollBar_set_barSize_m645706685 (UIScrollBar_t2839103954 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIScrollBar::Upgrade()
extern "C"  void UIScrollBar_Upgrade_m3904462995 (UIScrollBar_t2839103954 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIScrollBar::OnStart()
extern "C"  void UIScrollBar_OnStart_m2161119866 (UIScrollBar_t2839103954 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UIScrollBar::LocalToValue(UnityEngine.Vector2)
extern "C"  float UIScrollBar_LocalToValue_m1526164080 (UIScrollBar_t2839103954 * __this, Vector2_t4282066565  ___localPos0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIScrollBar::ForceUpdate()
extern "C"  void UIScrollBar_ForceUpdate_m867873035 (UIScrollBar_t2839103954 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UIScrollBar::ilo_get_value1(UIProgressBar)
extern "C"  float UIScrollBar_ilo_get_value1_m1094847653 (Il2CppObject * __this /* static, unused */, UIProgressBar_t168062834 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIScrollBar::ilo_set_value2(UIProgressBar,System.Single)
extern "C"  void UIScrollBar_ilo_set_value2_m2995850409 (Il2CppObject * __this /* static, unused */, UIProgressBar_t168062834 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIScrollBar::ilo_Execute3(System.Collections.Generic.List`1<EventDelegate>)
extern "C"  void UIScrollBar_ilo_Execute3_m1616440959 (Il2CppObject * __this /* static, unused */, List_1_t1077642479 * ___list0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3[] UIScrollBar::ilo_get_localCorners4(UIWidget)
extern "C"  Vector3U5BU5D_t215400611* UIScrollBar_ilo_get_localCorners4_m2832465532 (Il2CppObject * __this /* static, unused */, UIWidget_t769069560 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIScrollBar::ilo_get_isHorizontal5(UIProgressBar)
extern "C"  bool UIScrollBar_ilo_get_isHorizontal5_m3640476970 (Il2CppObject * __this /* static, unused */, UIProgressBar_t168062834 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIScrollBar::ilo_set_drawRegion6(UIWidget,UnityEngine.Vector4)
extern "C"  void UIScrollBar_ilo_set_drawRegion6_m830247487 (Il2CppObject * __this /* static, unused */, UIWidget_t769069560 * ____this0, Vector4_t4282066567  ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIScrollBar::ilo_get_isInverted7(UIProgressBar)
extern "C"  bool UIScrollBar_ilo_get_isInverted7_m2298364089 (Il2CppObject * __this /* static, unused */, UIProgressBar_t168062834 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector4 UIScrollBar::ilo_get_drawingDimensions8(UIWidget)
extern "C"  Vector4_t4282066567  UIScrollBar_ilo_get_drawingDimensions8_m881516675 (Il2CppObject * __this /* static, unused */, UIWidget_t769069560 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
