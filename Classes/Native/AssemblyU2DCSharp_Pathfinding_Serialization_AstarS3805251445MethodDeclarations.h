﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.Serialization.AstarSerializer/<DeserializeExtraInfo>c__AnonStorey10F
struct U3CDeserializeExtraInfoU3Ec__AnonStorey10F_t3805251445;
// Pathfinding.GraphNode
struct GraphNode_t23612370;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphNode23612370.h"

// System.Void Pathfinding.Serialization.AstarSerializer/<DeserializeExtraInfo>c__AnonStorey10F::.ctor()
extern "C"  void U3CDeserializeExtraInfoU3Ec__AnonStorey10F__ctor_m1151431366 (U3CDeserializeExtraInfoU3Ec__AnonStorey10F_t3805251445 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.Serialization.AstarSerializer/<DeserializeExtraInfo>c__AnonStorey10F::<>m__342(Pathfinding.GraphNode)
extern "C"  bool U3CDeserializeExtraInfoU3Ec__AnonStorey10F_U3CU3Em__342_m1286692820 (U3CDeserializeExtraInfoU3Ec__AnonStorey10F_t3805251445 * __this, GraphNode_t23612370 * ___node0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
