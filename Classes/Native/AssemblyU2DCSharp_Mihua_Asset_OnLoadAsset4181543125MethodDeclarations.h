﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mihua.Asset.OnLoadAsset
struct OnLoadAsset_t4181543125;
// System.Object
struct Il2CppObject;
// Mihua.Asset.ABLoadOperation.AssetOperation
struct AssetOperation_t778728221;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "AssemblyU2DCSharp_Mihua_Asset_ABLoadOperation_Asset778728221.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void Mihua.Asset.OnLoadAsset::.ctor(System.Object,System.IntPtr)
extern "C"  void OnLoadAsset__ctor_m2494295022 (OnLoadAsset_t4181543125 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua.Asset.OnLoadAsset::Invoke(Mihua.Asset.ABLoadOperation.AssetOperation)
extern "C"  void OnLoadAsset_Invoke_m453943599 (OnLoadAsset_t4181543125 * __this, AssetOperation_t778728221 * ___assetOperation0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Mihua.Asset.OnLoadAsset::BeginInvoke(Mihua.Asset.ABLoadOperation.AssetOperation,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnLoadAsset_BeginInvoke_m1541789300 (OnLoadAsset_t4181543125 * __this, AssetOperation_t778728221 * ___assetOperation0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua.Asset.OnLoadAsset::EndInvoke(System.IAsyncResult)
extern "C"  void OnLoadAsset_EndInvoke_m532579582 (OnLoadAsset_t4181543125 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
