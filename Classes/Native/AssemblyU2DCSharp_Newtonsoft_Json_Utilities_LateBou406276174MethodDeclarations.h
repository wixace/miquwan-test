﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<CreateGet>c__AnonStorey143`1<System.Object>
struct U3CCreateGetU3Ec__AnonStorey143_1_t406276174;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<CreateGet>c__AnonStorey143`1<System.Object>::.ctor()
extern "C"  void U3CCreateGetU3Ec__AnonStorey143_1__ctor_m3981334997_gshared (U3CCreateGetU3Ec__AnonStorey143_1_t406276174 * __this, const MethodInfo* method);
#define U3CCreateGetU3Ec__AnonStorey143_1__ctor_m3981334997(__this, method) ((  void (*) (U3CCreateGetU3Ec__AnonStorey143_1_t406276174 *, const MethodInfo*))U3CCreateGetU3Ec__AnonStorey143_1__ctor_m3981334997_gshared)(__this, method)
// System.Object Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<CreateGet>c__AnonStorey143`1<System.Object>::<>m__3AA(T)
extern "C"  Il2CppObject * U3CCreateGetU3Ec__AnonStorey143_1_U3CU3Em__3AA_m3471393308_gshared (U3CCreateGetU3Ec__AnonStorey143_1_t406276174 * __this, Il2CppObject * ___o0, const MethodInfo* method);
#define U3CCreateGetU3Ec__AnonStorey143_1_U3CU3Em__3AA_m3471393308(__this, ___o0, method) ((  Il2CppObject * (*) (U3CCreateGetU3Ec__AnonStorey143_1_t406276174 *, Il2CppObject *, const MethodInfo*))U3CCreateGetU3Ec__AnonStorey143_1_U3CU3Em__3AA_m3471393308_gshared)(__this, ___o0, method)
