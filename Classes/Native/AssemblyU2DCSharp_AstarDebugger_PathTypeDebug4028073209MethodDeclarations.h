﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;
// System.Func`1<System.Int32>
struct Func_1_t2279000074;
// System.Text.StringBuilder
struct StringBuilder_t243639308;
// AstarDebugger/PathTypeDebug
struct PathTypeDebug_t4028073209;
struct PathTypeDebug_t4028073209_marshaled_pinvoke;
struct PathTypeDebug_t4028073209_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_AstarDebugger_PathTypeDebug4028073209.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Text_StringBuilder243639308.h"

// System.Void AstarDebugger/PathTypeDebug::.ctor(System.String,System.Func`1<System.Int32>,System.Func`1<System.Int32>)
extern "C"  void PathTypeDebug__ctor_m1796206650 (PathTypeDebug_t4028073209 * __this, String_t* ___name0, Func_1_t2279000074 * ___getSize1, Func_1_t2279000074 * ___getTotalCreated2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AstarDebugger/PathTypeDebug::Print(System.Text.StringBuilder)
extern "C"  void PathTypeDebug_Print_m3883384685 (PathTypeDebug_t4028073209 * __this, StringBuilder_t243639308 * ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct PathTypeDebug_t4028073209;
struct PathTypeDebug_t4028073209_marshaled_pinvoke;

extern "C" void PathTypeDebug_t4028073209_marshal_pinvoke(const PathTypeDebug_t4028073209& unmarshaled, PathTypeDebug_t4028073209_marshaled_pinvoke& marshaled);
extern "C" void PathTypeDebug_t4028073209_marshal_pinvoke_back(const PathTypeDebug_t4028073209_marshaled_pinvoke& marshaled, PathTypeDebug_t4028073209& unmarshaled);
extern "C" void PathTypeDebug_t4028073209_marshal_pinvoke_cleanup(PathTypeDebug_t4028073209_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct PathTypeDebug_t4028073209;
struct PathTypeDebug_t4028073209_marshaled_com;

extern "C" void PathTypeDebug_t4028073209_marshal_com(const PathTypeDebug_t4028073209& unmarshaled, PathTypeDebug_t4028073209_marshaled_com& marshaled);
extern "C" void PathTypeDebug_t4028073209_marshal_com_back(const PathTypeDebug_t4028073209_marshaled_com& marshaled, PathTypeDebug_t4028073209& unmarshaled);
extern "C" void PathTypeDebug_t4028073209_marshal_com_cleanup(PathTypeDebug_t4028073209_marshaled_com& marshaled);
