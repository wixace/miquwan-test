﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FormulaGenerated
struct FormulaGenerated_t866115305;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// Formula/PlusAtt
struct PlusAtt_t2698014398;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"

// System.Void FormulaGenerated::.ctor()
extern "C"  void FormulaGenerated__ctor_m1585905106 (FormulaGenerated_t866115305 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FormulaGenerated::Formula_Formula1(JSVCall,System.Int32)
extern "C"  bool FormulaGenerated_Formula_Formula1_m2751147866 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FormulaGenerated::Formula_ComputHeroTrainUpperlimit__Int32__UInt32__Int32(JSVCall,System.Int32)
extern "C"  bool FormulaGenerated_Formula_ComputHeroTrainUpperlimit__Int32__UInt32__Int32_m4154431219 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FormulaGenerated::Formula_ComputReinPlusAtt__UInt32__UInt32__UInt32__UInt32__Int32(JSVCall,System.Int32)
extern "C"  bool FormulaGenerated_Formula_ComputReinPlusAtt__UInt32__UInt32__UInt32__UInt32__Int32_m1311233976 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FormulaGenerated::Formula_ComputStarUpCurAtt__UInt32__Int32__Int32__UInt32(JSVCall,System.Int32)
extern "C"  bool FormulaGenerated_Formula_ComputStarUpCurAtt__UInt32__Int32__Int32__UInt32_m1377618557 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FormulaGenerated::Formula_ComputStarUpGrowUp__UInt32__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool FormulaGenerated_Formula_ComputStarUpGrowUp__UInt32__Int32__Int32_m1058315633 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FormulaGenerated::Formula_ComputStarUpGrowUpAdd__Int32__Int32__UInt32(JSVCall,System.Int32)
extern "C"  bool FormulaGenerated_Formula_ComputStarUpGrowUpAdd__Int32__Int32__UInt32_m1720835012 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FormulaGenerated::__Register()
extern "C"  void FormulaGenerated___Register_m1263161909 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FormulaGenerated::ilo_addJSCSRel1(System.Int32,System.Object)
extern "C"  void FormulaGenerated_ilo_addJSCSRel1_m1556583310 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 FormulaGenerated::ilo_getInt322(System.Int32)
extern "C"  int32_t FormulaGenerated_ilo_getInt322_m3984658266 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 FormulaGenerated::ilo_getUInt323(System.Int32)
extern "C"  uint32_t FormulaGenerated_ilo_getUInt323_m1269026101 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Formula/PlusAtt FormulaGenerated::ilo_ComputReinPlusAtt4(System.UInt32,System.UInt32,System.UInt32,System.UInt32,System.Int32)
extern "C"  PlusAtt_t2698014398 * FormulaGenerated_ilo_ComputReinPlusAtt4_m3519073262 (Il2CppObject * __this /* static, unused */, uint32_t ___bsHp0, uint32_t ___bsAttack1, uint32_t ___bsPhy_def2, uint32_t ___bsMag_def3, int32_t ___ratio4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 FormulaGenerated::ilo_setObject5(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t FormulaGenerated_ilo_setObject5_m356748540 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FormulaGenerated::ilo_setInt326(System.Int32,System.Int32)
extern "C"  void FormulaGenerated_ilo_setInt326_m2195733791 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
