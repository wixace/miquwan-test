﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FingerEventDetector`1/FingerEventHandler<System.Object>
struct FingerEventHandler_t2484147800;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void FingerEventDetector`1/FingerEventHandler<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void FingerEventHandler__ctor_m425058771_gshared (FingerEventHandler_t2484147800 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define FingerEventHandler__ctor_m425058771(__this, ___object0, ___method1, method) ((  void (*) (FingerEventHandler_t2484147800 *, Il2CppObject *, IntPtr_t, const MethodInfo*))FingerEventHandler__ctor_m425058771_gshared)(__this, ___object0, ___method1, method)
// System.Void FingerEventDetector`1/FingerEventHandler<System.Object>::Invoke(T)
extern "C"  void FingerEventHandler_Invoke_m4182135569_gshared (FingerEventHandler_t2484147800 * __this, Il2CppObject * ___eventData0, const MethodInfo* method);
#define FingerEventHandler_Invoke_m4182135569(__this, ___eventData0, method) ((  void (*) (FingerEventHandler_t2484147800 *, Il2CppObject *, const MethodInfo*))FingerEventHandler_Invoke_m4182135569_gshared)(__this, ___eventData0, method)
// System.IAsyncResult FingerEventDetector`1/FingerEventHandler<System.Object>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * FingerEventHandler_BeginInvoke_m1896720230_gshared (FingerEventHandler_t2484147800 * __this, Il2CppObject * ___eventData0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method);
#define FingerEventHandler_BeginInvoke_m1896720230(__this, ___eventData0, ___callback1, ___object2, method) ((  Il2CppObject * (*) (FingerEventHandler_t2484147800 *, Il2CppObject *, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))FingerEventHandler_BeginInvoke_m1896720230_gshared)(__this, ___eventData0, ___callback1, ___object2, method)
// System.Void FingerEventDetector`1/FingerEventHandler<System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void FingerEventHandler_EndInvoke_m934360419_gshared (FingerEventHandler_t2484147800 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define FingerEventHandler_EndInvoke_m934360419(__this, ___result0, method) ((  void (*) (FingerEventHandler_t2484147800 *, Il2CppObject *, const MethodInfo*))FingerEventHandler_EndInvoke_m934360419_gshared)(__this, ___result0, method)
