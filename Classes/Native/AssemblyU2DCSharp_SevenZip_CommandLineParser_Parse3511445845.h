﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.ArrayList
struct ArrayList_t3948406897;
// SevenZip.CommandLineParser.SwitchResult[]
struct SwitchResultU5BU5D_t2817561950;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SevenZip.CommandLineParser.Parser
struct  Parser_t3511445845  : public Il2CppObject
{
public:
	// System.Collections.ArrayList SevenZip.CommandLineParser.Parser::NonSwitchStrings
	ArrayList_t3948406897 * ___NonSwitchStrings_4;
	// SevenZip.CommandLineParser.SwitchResult[] SevenZip.CommandLineParser.Parser::_switches
	SwitchResultU5BU5D_t2817561950* ____switches_5;

public:
	inline static int32_t get_offset_of_NonSwitchStrings_4() { return static_cast<int32_t>(offsetof(Parser_t3511445845, ___NonSwitchStrings_4)); }
	inline ArrayList_t3948406897 * get_NonSwitchStrings_4() const { return ___NonSwitchStrings_4; }
	inline ArrayList_t3948406897 ** get_address_of_NonSwitchStrings_4() { return &___NonSwitchStrings_4; }
	inline void set_NonSwitchStrings_4(ArrayList_t3948406897 * value)
	{
		___NonSwitchStrings_4 = value;
		Il2CppCodeGenWriteBarrier(&___NonSwitchStrings_4, value);
	}

	inline static int32_t get_offset_of__switches_5() { return static_cast<int32_t>(offsetof(Parser_t3511445845, ____switches_5)); }
	inline SwitchResultU5BU5D_t2817561950* get__switches_5() const { return ____switches_5; }
	inline SwitchResultU5BU5D_t2817561950** get_address_of__switches_5() { return &____switches_5; }
	inline void set__switches_5(SwitchResultU5BU5D_t2817561950* value)
	{
		____switches_5 = value;
		Il2CppCodeGenWriteBarrier(&____switches_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
