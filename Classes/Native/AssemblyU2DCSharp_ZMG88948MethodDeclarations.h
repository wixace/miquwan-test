﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZMG
struct ZMG_t88948;
// GlobalGOMgr
struct GlobalGOMgr_t803081773;
// SceneMgr
struct SceneMgr_t3584105996;
// TimeMgr
struct TimeMgr_t350708715;
// TimeUpdateMgr
struct TimeUpdateMgr_t880289826;
// EffectMgr
struct EffectMgr_t535289511;
// SoundMgr
struct SoundMgr_t1807284905;
// TeamSkillMgr
struct TeamSkillMgr_t2030650276;
// UIEffectMgr
struct UIEffectMgr_t952662675;
// FloatTextMgr
struct FloatTextMgr_t630384591;
// CSGameDataMgr
struct CSGameDataMgr_t2623305516;
// StoryMgr
struct StoryMgr_t1782380803;
// CSDatacfgManager
struct CSDatacfgManager_t1565254243;
// ZiShiYingMgr
struct ZiShiYingMgr_t4004457962;
// PluginsSdkMgr
struct PluginsSdkMgr_t3884624670;
// Mihua.Assets.SubAssetMgr
struct SubAssetMgr_t3564963414;
// GameQutilyCtr
struct GameQutilyCtr_t3963256169;
// CameraShotMgr
struct CameraShotMgr_t1580128697;
// AntiCheatMgr
struct AntiCheatMgr_t3474304487;
// ProductsCfgMgr
struct ProductsCfgMgr_t2493714872;
// MihuaPayMgr
struct MihuaPayMgr_t1240399912;
// CaptureScreenshotMgr
struct CaptureScreenshotMgr_t3951887692;
// System.Action
struct Action_t3771233898;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// System.String
struct String_t;
// CEvent.EventFunc`1<CEvent.ZEvent>
struct EventFunc_1_t1114914310;
// System.Object[]
struct ObjectU5BU5D_t1108656482;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action3771233898.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "AssemblyU2DCSharp_SceneMgr3584105996.h"
#include "AssemblyU2DCSharp_GlobalGOMgr803081773.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_AntiCheatMgr3474304487.h"

// System.Void ZMG::.ctor()
extern "C"  void ZMG__ctor_m968698487 (ZMG_t88948 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZMG::.cctor()
extern "C"  void ZMG__cctor_m3777753110 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZMG ZMG::get_zmg()
extern "C"  ZMG_t88948 * ZMG_get_zmg_m42716799 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZMG::get_isNull()
extern "C"  bool ZMG_get_isNull_m2291689203 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GlobalGOMgr ZMG::get_GlobalGOMgr()
extern "C"  GlobalGOMgr_t803081773 * ZMG_get_GlobalGOMgr_m596130559 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SceneMgr ZMG::get_SceneMgr()
extern "C"  SceneMgr_t3584105996 * ZMG_get_SceneMgr_m2278086545 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TimeMgr ZMG::get_TimeMgr()
extern "C"  TimeMgr_t350708715 * ZMG_get_TimeMgr_m4112512191 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TimeUpdateMgr ZMG::get_TimeUpdateMgr()
extern "C"  TimeUpdateMgr_t880289826 * ZMG_get_TimeUpdateMgr_m43111519 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// EffectMgr ZMG::get_EffectMgr()
extern "C"  EffectMgr_t535289511 * ZMG_get_EffectMgr_m2048884671 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SoundMgr ZMG::get_SoundMgr()
extern "C"  SoundMgr_t1807284905 * ZMG_get_SoundMgr_m3367791883 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TeamSkillMgr ZMG::get_TeamSkillMgr()
extern "C"  TeamSkillMgr_t2030650276 * ZMG_get_TeamSkillMgr_m3718286657 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIEffectMgr ZMG::get_UIEffectMgr()
extern "C"  UIEffectMgr_t952662675 * ZMG_get_UIEffectMgr_m1011099327 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FloatTextMgr ZMG::get_FloatTextMgr()
extern "C"  FloatTextMgr_t630384591 * ZMG_get_FloatTextMgr_m2504811991 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CSGameDataMgr ZMG::get_CSGameDataMgr()
extern "C"  CSGameDataMgr_t2623305516 * ZMG_get_CSGameDataMgr_m2791201055 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// StoryMgr ZMG::get_StoryMgr()
extern "C"  StoryMgr_t1782380803 * ZMG_get_StoryMgr_m110725695 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CSDatacfgManager ZMG::get_CfgDataMgr()
extern "C"  CSDatacfgManager_t1565254243 * ZMG_get_CfgDataMgr_m3301722182 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZiShiYingMgr ZMG::get_ZiShiYingMgr()
extern "C"  ZiShiYingMgr_t4004457962 * ZMG_get_ZiShiYingMgr_m3820090957 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PluginsSdkMgr ZMG::get_PluginsSdkMgr()
extern "C"  PluginsSdkMgr_t3884624670 * ZMG_get_PluginsSdkMgr_m3999340767 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mihua.Assets.SubAssetMgr ZMG::get_SubAssetMgr()
extern "C"  SubAssetMgr_t3564963414 * ZMG_get_SubAssetMgr_m1607933694 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GameQutilyCtr ZMG::get_GameQutilyCtr()
extern "C"  GameQutilyCtr_t3963256169 * ZMG_get_GameQutilyCtr_m2775665791 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CameraShotMgr ZMG::get_CameraShotMgr()
extern "C"  CameraShotMgr_t1580128697 * ZMG_get_CameraShotMgr_m34927231 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// AntiCheatMgr ZMG::get_AntiCheatMgr()
extern "C"  AntiCheatMgr_t3474304487 * ZMG_get_AntiCheatMgr_m1825440775 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProductsCfgMgr ZMG::get_ProductsCfgMgr()
extern "C"  ProductsCfgMgr_t2493714872 * ZMG_get_ProductsCfgMgr_m89690537 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MihuaPayMgr ZMG::get_MihuaPayMgr()
extern "C"  MihuaPayMgr_t1240399912 * ZMG_get_MihuaPayMgr_m505126367 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CaptureScreenshotMgr ZMG::get_CaptureScreenshotMgr()
extern "C"  CaptureScreenshotMgr_t3951887692 * ZMG_get_CaptureScreenshotMgr_m3749822353 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZMG::Init()
extern "C"  void ZMG_Init_m1922447613 (ZMG_t88948 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZMG::Logout(System.Action)
extern "C"  void ZMG_Logout_m1350993638 (Il2CppObject * __this /* static, unused */, Action_t3771233898 * ___call0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZMG::StartLoadScene(CEvent.ZEvent)
extern "C"  void ZMG_StartLoadScene_m408176426 (Il2CppObject * __this /* static, unused */, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZMG::Logout1()
extern "C"  void ZMG_Logout1_m1313568956 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZMG ZMG::ilo_get_zmg1()
extern "C"  ZMG_t88948 * ZMG_ilo_get_zmg1_m3471585063 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PluginsSdkMgr ZMG::ilo_get_Instance2()
extern "C"  PluginsSdkMgr_t3884624670 * ZMG_ilo_get_Instance2_m1057489163 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZMG::ilo_get_isloading3(SceneMgr)
extern "C"  bool ZMG_ilo_get_isloading3_m2316997866 (Il2CppObject * __this /* static, unused */, SceneMgr_t3584105996 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZMG::ilo_PlayOverEffect4(GlobalGOMgr)
extern "C"  void ZMG_ilo_PlayOverEffect4_m2143068616 (Il2CppObject * __this /* static, unused */, GlobalGOMgr_t803081773 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZMG::ilo_RemoveEventListener5(System.String,CEvent.EventFunc`1<CEvent.ZEvent>)
extern "C"  void ZMG_ilo_RemoveEventListener5_m2571508408 (Il2CppObject * __this /* static, unused */, String_t* ___eventType0, EventFunc_1_t1114914310 * ___func1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZMG::ilo_CallJsFun6(System.String,System.Object[])
extern "C"  void ZMG_ilo_CallJsFun6_m698395536 (Il2CppObject * __this /* static, unused */, String_t* ___funName0, ObjectU5BU5D_t1108656482* ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZMG::ilo_ClearLock7(GlobalGOMgr)
extern "C"  void ZMG_ilo_ClearLock7_m4269016326 (Il2CppObject * __this /* static, unused */, GlobalGOMgr_t803081773 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// AntiCheatMgr ZMG::ilo_get_AntiCheatMgr8()
extern "C"  AntiCheatMgr_t3474304487 * ZMG_ilo_get_AntiCheatMgr8_m279333600 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZMG::ilo_Clear9(AntiCheatMgr)
extern "C"  void ZMG_ilo_Clear9_m4243277093 (Il2CppObject * __this /* static, unused */, AntiCheatMgr_t3474304487 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
