﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType1744280289.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.Voxels.LinkedVoxelSpan
struct  LinkedVoxelSpan_t3201576397 
{
public:
	// System.UInt32 Pathfinding.Voxels.LinkedVoxelSpan::bottom
	uint32_t ___bottom_0;
	// System.UInt32 Pathfinding.Voxels.LinkedVoxelSpan::top
	uint32_t ___top_1;
	// System.Int32 Pathfinding.Voxels.LinkedVoxelSpan::next
	int32_t ___next_2;
	// System.Int32 Pathfinding.Voxels.LinkedVoxelSpan::area
	int32_t ___area_3;

public:
	inline static int32_t get_offset_of_bottom_0() { return static_cast<int32_t>(offsetof(LinkedVoxelSpan_t3201576397, ___bottom_0)); }
	inline uint32_t get_bottom_0() const { return ___bottom_0; }
	inline uint32_t* get_address_of_bottom_0() { return &___bottom_0; }
	inline void set_bottom_0(uint32_t value)
	{
		___bottom_0 = value;
	}

	inline static int32_t get_offset_of_top_1() { return static_cast<int32_t>(offsetof(LinkedVoxelSpan_t3201576397, ___top_1)); }
	inline uint32_t get_top_1() const { return ___top_1; }
	inline uint32_t* get_address_of_top_1() { return &___top_1; }
	inline void set_top_1(uint32_t value)
	{
		___top_1 = value;
	}

	inline static int32_t get_offset_of_next_2() { return static_cast<int32_t>(offsetof(LinkedVoxelSpan_t3201576397, ___next_2)); }
	inline int32_t get_next_2() const { return ___next_2; }
	inline int32_t* get_address_of_next_2() { return &___next_2; }
	inline void set_next_2(int32_t value)
	{
		___next_2 = value;
	}

	inline static int32_t get_offset_of_area_3() { return static_cast<int32_t>(offsetof(LinkedVoxelSpan_t3201576397, ___area_3)); }
	inline int32_t get_area_3() const { return ___area_3; }
	inline int32_t* get_address_of_area_3() { return &___area_3; }
	inline void set_area_3(int32_t value)
	{
		___area_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for marshalling of: Pathfinding.Voxels.LinkedVoxelSpan
struct LinkedVoxelSpan_t3201576397_marshaled_pinvoke
{
	uint32_t ___bottom_0;
	uint32_t ___top_1;
	int32_t ___next_2;
	int32_t ___area_3;
};
// Native definition for marshalling of: Pathfinding.Voxels.LinkedVoxelSpan
struct LinkedVoxelSpan_t3201576397_marshaled_com
{
	uint32_t ___bottom_0;
	uint32_t ___top_1;
	int32_t ___next_2;
	int32_t ___area_3;
};
