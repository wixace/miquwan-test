﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_whoodooce226
struct M_whoodooce226_t1454824529;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"

// System.Void GarbageiOS.M_whoodooce226::.ctor()
extern "C"  void M_whoodooce226__ctor_m4034568450 (M_whoodooce226_t1454824529 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_whoodooce226::M_pijel0(System.String[],System.Int32)
extern "C"  void M_whoodooce226_M_pijel0_m736359299 (M_whoodooce226_t1454824529 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
