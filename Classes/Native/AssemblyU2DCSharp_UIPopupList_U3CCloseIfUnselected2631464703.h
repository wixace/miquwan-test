﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.Object
struct Il2CppObject;
// UIPopupList
struct UIPopupList_t1804933942;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIPopupList/<CloseIfUnselected>c__Iterator28
struct  U3CCloseIfUnselectedU3Ec__Iterator28_t2631464703  : public Il2CppObject
{
public:
	// UnityEngine.GameObject UIPopupList/<CloseIfUnselected>c__Iterator28::<go>__0
	GameObject_t3674682005 * ___U3CgoU3E__0_0;
	// System.Int32 UIPopupList/<CloseIfUnselected>c__Iterator28::$PC
	int32_t ___U24PC_1;
	// System.Object UIPopupList/<CloseIfUnselected>c__Iterator28::$current
	Il2CppObject * ___U24current_2;
	// UIPopupList UIPopupList/<CloseIfUnselected>c__Iterator28::<>f__this
	UIPopupList_t1804933942 * ___U3CU3Ef__this_3;

public:
	inline static int32_t get_offset_of_U3CgoU3E__0_0() { return static_cast<int32_t>(offsetof(U3CCloseIfUnselectedU3Ec__Iterator28_t2631464703, ___U3CgoU3E__0_0)); }
	inline GameObject_t3674682005 * get_U3CgoU3E__0_0() const { return ___U3CgoU3E__0_0; }
	inline GameObject_t3674682005 ** get_address_of_U3CgoU3E__0_0() { return &___U3CgoU3E__0_0; }
	inline void set_U3CgoU3E__0_0(GameObject_t3674682005 * value)
	{
		___U3CgoU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CgoU3E__0_0, value);
	}

	inline static int32_t get_offset_of_U24PC_1() { return static_cast<int32_t>(offsetof(U3CCloseIfUnselectedU3Ec__Iterator28_t2631464703, ___U24PC_1)); }
	inline int32_t get_U24PC_1() const { return ___U24PC_1; }
	inline int32_t* get_address_of_U24PC_1() { return &___U24PC_1; }
	inline void set_U24PC_1(int32_t value)
	{
		___U24PC_1 = value;
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CCloseIfUnselectedU3Ec__Iterator28_t2631464703, ___U24current_2)); }
	inline Il2CppObject * get_U24current_2() const { return ___U24current_2; }
	inline Il2CppObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(Il2CppObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_2, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_3() { return static_cast<int32_t>(offsetof(U3CCloseIfUnselectedU3Ec__Iterator28_t2631464703, ___U3CU3Ef__this_3)); }
	inline UIPopupList_t1804933942 * get_U3CU3Ef__this_3() const { return ___U3CU3Ef__this_3; }
	inline UIPopupList_t1804933942 ** get_address_of_U3CU3Ef__this_3() { return &___U3CU3Ef__this_3; }
	inline void set_U3CU3Ef__this_3(UIPopupList_t1804933942 * value)
	{
		___U3CU3Ef__this_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
