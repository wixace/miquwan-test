﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En346233792MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.GraphNode,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m3575884868(__this, ___dictionary0, method) ((  void (*) (Enumerator_t165026565 *, Dictionary_2_t3142670469 *, const MethodInfo*))Enumerator__ctor_m584315628_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.GraphNode,System.Int32>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3042313511(__this, method) ((  Il2CppObject * (*) (Enumerator_t165026565 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m945293439_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.GraphNode,System.Int32>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1057898993(__this, method) ((  void (*) (Enumerator_t165026565 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2827100745_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.GraphNode,System.Int32>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m4190734312(__this, method) ((  DictionaryEntry_t1751606614  (*) (Enumerator_t165026565 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1888707904_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.GraphNode,System.Int32>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m950044867(__this, method) ((  Il2CppObject * (*) (Enumerator_t165026565 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3556289051_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.GraphNode,System.Int32>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3508486805(__this, method) ((  Il2CppObject * (*) (Enumerator_t165026565 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m4143214061_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.GraphNode,System.Int32>::MoveNext()
#define Enumerator_MoveNext_m3001735457(__this, method) ((  bool (*) (Enumerator_t165026565 *, const MethodInfo*))Enumerator_MoveNext_m2774388601_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.GraphNode,System.Int32>::get_Current()
#define Enumerator_get_Current_m268843515(__this, method) ((  KeyValuePair_2_t3041451175  (*) (Enumerator_t165026565 *, const MethodInfo*))Enumerator_get_Current_m2653719203_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.GraphNode,System.Int32>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m2291598570(__this, method) ((  GraphNode_t23612370 * (*) (Enumerator_t165026565 *, const MethodInfo*))Enumerator_get_CurrentKey_m2145646402_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.GraphNode,System.Int32>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m1029796522(__this, method) ((  int32_t (*) (Enumerator_t165026565 *, const MethodInfo*))Enumerator_get_CurrentValue_m1809235202_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.GraphNode,System.Int32>::Reset()
#define Enumerator_Reset_m2310545558(__this, method) ((  void (*) (Enumerator_t165026565 *, const MethodInfo*))Enumerator_Reset_m4006931262_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.GraphNode,System.Int32>::VerifyState()
#define Enumerator_VerifyState_m1478109215(__this, method) ((  void (*) (Enumerator_t165026565 *, const MethodInfo*))Enumerator_VerifyState_m3658372295_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.GraphNode,System.Int32>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m1573652423(__this, method) ((  void (*) (Enumerator_t165026565 *, const MethodInfo*))Enumerator_VerifyCurrent_m862431855_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.GraphNode,System.Int32>::Dispose()
#define Enumerator_Dispose_m2459618278(__this, method) ((  void (*) (Enumerator_t165026565 *, const MethodInfo*))Enumerator_Dispose_m598707342_gshared)(__this, method)
