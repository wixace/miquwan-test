﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._43fbac424f99e4d261f6144452fbc2b8
struct _43fbac424f99e4d261f6144452fbc2b8_t3274948303;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._43fbac424f99e4d261f6144452fbc2b8::.ctor()
extern "C"  void _43fbac424f99e4d261f6144452fbc2b8__ctor_m3947339838 (_43fbac424f99e4d261f6144452fbc2b8_t3274948303 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._43fbac424f99e4d261f6144452fbc2b8::_43fbac424f99e4d261f6144452fbc2b8m2(System.Int32)
extern "C"  int32_t _43fbac424f99e4d261f6144452fbc2b8__43fbac424f99e4d261f6144452fbc2b8m2_m3832915769 (_43fbac424f99e4d261f6144452fbc2b8_t3274948303 * __this, int32_t ____43fbac424f99e4d261f6144452fbc2b8a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._43fbac424f99e4d261f6144452fbc2b8::_43fbac424f99e4d261f6144452fbc2b8m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _43fbac424f99e4d261f6144452fbc2b8__43fbac424f99e4d261f6144452fbc2b8m_m197432221 (_43fbac424f99e4d261f6144452fbc2b8_t3274948303 * __this, int32_t ____43fbac424f99e4d261f6144452fbc2b8a0, int32_t ____43fbac424f99e4d261f6144452fbc2b8531, int32_t ____43fbac424f99e4d261f6144452fbc2b8c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
