﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.IList
struct IList_t1751339649;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t215400611;
// System.Collections.Generic.IEnumerable`1<System.Single>
struct IEnumerable_1_t3297864633;
// System.Collections.Generic.IEnumerator`1<System.Single>
struct IEnumerator_1_t1908816725;
// Interpolate/ToVector3`1<System.Object>
struct ToVector3_1_t573499088;
// Interpolate/Function
struct Function_t4102301030;

#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Interpolate/<NewBezier>c__Iterator24`1<System.Object>
struct  U3CNewBezierU3Ec__Iterator24_1_t516803767  : public Il2CppObject
{
public:
	// System.Collections.IList Interpolate/<NewBezier>c__Iterator24`1::nodes
	Il2CppObject * ___nodes_0;
	// UnityEngine.Vector3[] Interpolate/<NewBezier>c__Iterator24`1::<points>__0
	Vector3U5BU5D_t215400611* ___U3CpointsU3E__0_1;
	// System.Collections.Generic.IEnumerable`1<System.Single> Interpolate/<NewBezier>c__Iterator24`1::steps
	Il2CppObject* ___steps_2;
	// System.Collections.Generic.IEnumerator`1<System.Single> Interpolate/<NewBezier>c__Iterator24`1::<$s_176>__1
	Il2CppObject* ___U3CU24s_176U3E__1_3;
	// System.Single Interpolate/<NewBezier>c__Iterator24`1::<step>__2
	float ___U3CstepU3E__2_4;
	// System.Int32 Interpolate/<NewBezier>c__Iterator24`1::<i>__3
	int32_t ___U3CiU3E__3_5;
	// Interpolate/ToVector3`1<T> Interpolate/<NewBezier>c__Iterator24`1::toVector3
	ToVector3_1_t573499088 * ___toVector3_6;
	// Interpolate/Function Interpolate/<NewBezier>c__Iterator24`1::ease
	Function_t4102301030 * ___ease_7;
	// System.Single Interpolate/<NewBezier>c__Iterator24`1::maxStep
	float ___maxStep_8;
	// System.Int32 Interpolate/<NewBezier>c__Iterator24`1::$PC
	int32_t ___U24PC_9;
	// UnityEngine.Vector3 Interpolate/<NewBezier>c__Iterator24`1::$current
	Vector3_t4282066566  ___U24current_10;
	// System.Collections.IList Interpolate/<NewBezier>c__Iterator24`1::<$>nodes
	Il2CppObject * ___U3CU24U3Enodes_11;
	// System.Collections.Generic.IEnumerable`1<System.Single> Interpolate/<NewBezier>c__Iterator24`1::<$>steps
	Il2CppObject* ___U3CU24U3Esteps_12;
	// Interpolate/ToVector3`1<T> Interpolate/<NewBezier>c__Iterator24`1::<$>toVector3
	ToVector3_1_t573499088 * ___U3CU24U3EtoVector3_13;
	// Interpolate/Function Interpolate/<NewBezier>c__Iterator24`1::<$>ease
	Function_t4102301030 * ___U3CU24U3Eease_14;
	// System.Single Interpolate/<NewBezier>c__Iterator24`1::<$>maxStep
	float ___U3CU24U3EmaxStep_15;

public:
	inline static int32_t get_offset_of_nodes_0() { return static_cast<int32_t>(offsetof(U3CNewBezierU3Ec__Iterator24_1_t516803767, ___nodes_0)); }
	inline Il2CppObject * get_nodes_0() const { return ___nodes_0; }
	inline Il2CppObject ** get_address_of_nodes_0() { return &___nodes_0; }
	inline void set_nodes_0(Il2CppObject * value)
	{
		___nodes_0 = value;
		Il2CppCodeGenWriteBarrier(&___nodes_0, value);
	}

	inline static int32_t get_offset_of_U3CpointsU3E__0_1() { return static_cast<int32_t>(offsetof(U3CNewBezierU3Ec__Iterator24_1_t516803767, ___U3CpointsU3E__0_1)); }
	inline Vector3U5BU5D_t215400611* get_U3CpointsU3E__0_1() const { return ___U3CpointsU3E__0_1; }
	inline Vector3U5BU5D_t215400611** get_address_of_U3CpointsU3E__0_1() { return &___U3CpointsU3E__0_1; }
	inline void set_U3CpointsU3E__0_1(Vector3U5BU5D_t215400611* value)
	{
		___U3CpointsU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CpointsU3E__0_1, value);
	}

	inline static int32_t get_offset_of_steps_2() { return static_cast<int32_t>(offsetof(U3CNewBezierU3Ec__Iterator24_1_t516803767, ___steps_2)); }
	inline Il2CppObject* get_steps_2() const { return ___steps_2; }
	inline Il2CppObject** get_address_of_steps_2() { return &___steps_2; }
	inline void set_steps_2(Il2CppObject* value)
	{
		___steps_2 = value;
		Il2CppCodeGenWriteBarrier(&___steps_2, value);
	}

	inline static int32_t get_offset_of_U3CU24s_176U3E__1_3() { return static_cast<int32_t>(offsetof(U3CNewBezierU3Ec__Iterator24_1_t516803767, ___U3CU24s_176U3E__1_3)); }
	inline Il2CppObject* get_U3CU24s_176U3E__1_3() const { return ___U3CU24s_176U3E__1_3; }
	inline Il2CppObject** get_address_of_U3CU24s_176U3E__1_3() { return &___U3CU24s_176U3E__1_3; }
	inline void set_U3CU24s_176U3E__1_3(Il2CppObject* value)
	{
		___U3CU24s_176U3E__1_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24s_176U3E__1_3, value);
	}

	inline static int32_t get_offset_of_U3CstepU3E__2_4() { return static_cast<int32_t>(offsetof(U3CNewBezierU3Ec__Iterator24_1_t516803767, ___U3CstepU3E__2_4)); }
	inline float get_U3CstepU3E__2_4() const { return ___U3CstepU3E__2_4; }
	inline float* get_address_of_U3CstepU3E__2_4() { return &___U3CstepU3E__2_4; }
	inline void set_U3CstepU3E__2_4(float value)
	{
		___U3CstepU3E__2_4 = value;
	}

	inline static int32_t get_offset_of_U3CiU3E__3_5() { return static_cast<int32_t>(offsetof(U3CNewBezierU3Ec__Iterator24_1_t516803767, ___U3CiU3E__3_5)); }
	inline int32_t get_U3CiU3E__3_5() const { return ___U3CiU3E__3_5; }
	inline int32_t* get_address_of_U3CiU3E__3_5() { return &___U3CiU3E__3_5; }
	inline void set_U3CiU3E__3_5(int32_t value)
	{
		___U3CiU3E__3_5 = value;
	}

	inline static int32_t get_offset_of_toVector3_6() { return static_cast<int32_t>(offsetof(U3CNewBezierU3Ec__Iterator24_1_t516803767, ___toVector3_6)); }
	inline ToVector3_1_t573499088 * get_toVector3_6() const { return ___toVector3_6; }
	inline ToVector3_1_t573499088 ** get_address_of_toVector3_6() { return &___toVector3_6; }
	inline void set_toVector3_6(ToVector3_1_t573499088 * value)
	{
		___toVector3_6 = value;
		Il2CppCodeGenWriteBarrier(&___toVector3_6, value);
	}

	inline static int32_t get_offset_of_ease_7() { return static_cast<int32_t>(offsetof(U3CNewBezierU3Ec__Iterator24_1_t516803767, ___ease_7)); }
	inline Function_t4102301030 * get_ease_7() const { return ___ease_7; }
	inline Function_t4102301030 ** get_address_of_ease_7() { return &___ease_7; }
	inline void set_ease_7(Function_t4102301030 * value)
	{
		___ease_7 = value;
		Il2CppCodeGenWriteBarrier(&___ease_7, value);
	}

	inline static int32_t get_offset_of_maxStep_8() { return static_cast<int32_t>(offsetof(U3CNewBezierU3Ec__Iterator24_1_t516803767, ___maxStep_8)); }
	inline float get_maxStep_8() const { return ___maxStep_8; }
	inline float* get_address_of_maxStep_8() { return &___maxStep_8; }
	inline void set_maxStep_8(float value)
	{
		___maxStep_8 = value;
	}

	inline static int32_t get_offset_of_U24PC_9() { return static_cast<int32_t>(offsetof(U3CNewBezierU3Ec__Iterator24_1_t516803767, ___U24PC_9)); }
	inline int32_t get_U24PC_9() const { return ___U24PC_9; }
	inline int32_t* get_address_of_U24PC_9() { return &___U24PC_9; }
	inline void set_U24PC_9(int32_t value)
	{
		___U24PC_9 = value;
	}

	inline static int32_t get_offset_of_U24current_10() { return static_cast<int32_t>(offsetof(U3CNewBezierU3Ec__Iterator24_1_t516803767, ___U24current_10)); }
	inline Vector3_t4282066566  get_U24current_10() const { return ___U24current_10; }
	inline Vector3_t4282066566 * get_address_of_U24current_10() { return &___U24current_10; }
	inline void set_U24current_10(Vector3_t4282066566  value)
	{
		___U24current_10 = value;
	}

	inline static int32_t get_offset_of_U3CU24U3Enodes_11() { return static_cast<int32_t>(offsetof(U3CNewBezierU3Ec__Iterator24_1_t516803767, ___U3CU24U3Enodes_11)); }
	inline Il2CppObject * get_U3CU24U3Enodes_11() const { return ___U3CU24U3Enodes_11; }
	inline Il2CppObject ** get_address_of_U3CU24U3Enodes_11() { return &___U3CU24U3Enodes_11; }
	inline void set_U3CU24U3Enodes_11(Il2CppObject * value)
	{
		___U3CU24U3Enodes_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Enodes_11, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Esteps_12() { return static_cast<int32_t>(offsetof(U3CNewBezierU3Ec__Iterator24_1_t516803767, ___U3CU24U3Esteps_12)); }
	inline Il2CppObject* get_U3CU24U3Esteps_12() const { return ___U3CU24U3Esteps_12; }
	inline Il2CppObject** get_address_of_U3CU24U3Esteps_12() { return &___U3CU24U3Esteps_12; }
	inline void set_U3CU24U3Esteps_12(Il2CppObject* value)
	{
		___U3CU24U3Esteps_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Esteps_12, value);
	}

	inline static int32_t get_offset_of_U3CU24U3EtoVector3_13() { return static_cast<int32_t>(offsetof(U3CNewBezierU3Ec__Iterator24_1_t516803767, ___U3CU24U3EtoVector3_13)); }
	inline ToVector3_1_t573499088 * get_U3CU24U3EtoVector3_13() const { return ___U3CU24U3EtoVector3_13; }
	inline ToVector3_1_t573499088 ** get_address_of_U3CU24U3EtoVector3_13() { return &___U3CU24U3EtoVector3_13; }
	inline void set_U3CU24U3EtoVector3_13(ToVector3_1_t573499088 * value)
	{
		___U3CU24U3EtoVector3_13 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3EtoVector3_13, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Eease_14() { return static_cast<int32_t>(offsetof(U3CNewBezierU3Ec__Iterator24_1_t516803767, ___U3CU24U3Eease_14)); }
	inline Function_t4102301030 * get_U3CU24U3Eease_14() const { return ___U3CU24U3Eease_14; }
	inline Function_t4102301030 ** get_address_of_U3CU24U3Eease_14() { return &___U3CU24U3Eease_14; }
	inline void set_U3CU24U3Eease_14(Function_t4102301030 * value)
	{
		___U3CU24U3Eease_14 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Eease_14, value);
	}

	inline static int32_t get_offset_of_U3CU24U3EmaxStep_15() { return static_cast<int32_t>(offsetof(U3CNewBezierU3Ec__Iterator24_1_t516803767, ___U3CU24U3EmaxStep_15)); }
	inline float get_U3CU24U3EmaxStep_15() const { return ___U3CU24U3EmaxStep_15; }
	inline float* get_address_of_U3CU24U3EmaxStep_15() { return &___U3CU24U3EmaxStep_15; }
	inline void set_U3CU24U3EmaxStep_15(float value)
	{
		___U3CU24U3EmaxStep_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
