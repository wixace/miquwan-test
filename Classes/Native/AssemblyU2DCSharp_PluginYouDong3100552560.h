﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PluginYouDong
struct  PluginYouDong_t3100552560  : public MonoBehaviour_t667441552
{
public:
	// System.String PluginYouDong::openid
	String_t* ___openid_7;
	// System.String PluginYouDong::token
	String_t* ___token_8;
	// System.String PluginYouDong::sign
	String_t* ___sign_9;
	// System.String PluginYouDong::configId
	String_t* ___configId_10;
	// System.String PluginYouDong::APPID
	String_t* ___APPID_11;
	// System.String PluginYouDong::serverid
	String_t* ___serverid_12;
	// System.String PluginYouDong::servername
	String_t* ___servername_13;
	// System.String PluginYouDong::roleid
	String_t* ___roleid_14;
	// System.String PluginYouDong::rolename
	String_t* ___rolename_15;
	// System.String PluginYouDong::rolelevel
	String_t* ___rolelevel_16;

public:
	inline static int32_t get_offset_of_openid_7() { return static_cast<int32_t>(offsetof(PluginYouDong_t3100552560, ___openid_7)); }
	inline String_t* get_openid_7() const { return ___openid_7; }
	inline String_t** get_address_of_openid_7() { return &___openid_7; }
	inline void set_openid_7(String_t* value)
	{
		___openid_7 = value;
		Il2CppCodeGenWriteBarrier(&___openid_7, value);
	}

	inline static int32_t get_offset_of_token_8() { return static_cast<int32_t>(offsetof(PluginYouDong_t3100552560, ___token_8)); }
	inline String_t* get_token_8() const { return ___token_8; }
	inline String_t** get_address_of_token_8() { return &___token_8; }
	inline void set_token_8(String_t* value)
	{
		___token_8 = value;
		Il2CppCodeGenWriteBarrier(&___token_8, value);
	}

	inline static int32_t get_offset_of_sign_9() { return static_cast<int32_t>(offsetof(PluginYouDong_t3100552560, ___sign_9)); }
	inline String_t* get_sign_9() const { return ___sign_9; }
	inline String_t** get_address_of_sign_9() { return &___sign_9; }
	inline void set_sign_9(String_t* value)
	{
		___sign_9 = value;
		Il2CppCodeGenWriteBarrier(&___sign_9, value);
	}

	inline static int32_t get_offset_of_configId_10() { return static_cast<int32_t>(offsetof(PluginYouDong_t3100552560, ___configId_10)); }
	inline String_t* get_configId_10() const { return ___configId_10; }
	inline String_t** get_address_of_configId_10() { return &___configId_10; }
	inline void set_configId_10(String_t* value)
	{
		___configId_10 = value;
		Il2CppCodeGenWriteBarrier(&___configId_10, value);
	}

	inline static int32_t get_offset_of_APPID_11() { return static_cast<int32_t>(offsetof(PluginYouDong_t3100552560, ___APPID_11)); }
	inline String_t* get_APPID_11() const { return ___APPID_11; }
	inline String_t** get_address_of_APPID_11() { return &___APPID_11; }
	inline void set_APPID_11(String_t* value)
	{
		___APPID_11 = value;
		Il2CppCodeGenWriteBarrier(&___APPID_11, value);
	}

	inline static int32_t get_offset_of_serverid_12() { return static_cast<int32_t>(offsetof(PluginYouDong_t3100552560, ___serverid_12)); }
	inline String_t* get_serverid_12() const { return ___serverid_12; }
	inline String_t** get_address_of_serverid_12() { return &___serverid_12; }
	inline void set_serverid_12(String_t* value)
	{
		___serverid_12 = value;
		Il2CppCodeGenWriteBarrier(&___serverid_12, value);
	}

	inline static int32_t get_offset_of_servername_13() { return static_cast<int32_t>(offsetof(PluginYouDong_t3100552560, ___servername_13)); }
	inline String_t* get_servername_13() const { return ___servername_13; }
	inline String_t** get_address_of_servername_13() { return &___servername_13; }
	inline void set_servername_13(String_t* value)
	{
		___servername_13 = value;
		Il2CppCodeGenWriteBarrier(&___servername_13, value);
	}

	inline static int32_t get_offset_of_roleid_14() { return static_cast<int32_t>(offsetof(PluginYouDong_t3100552560, ___roleid_14)); }
	inline String_t* get_roleid_14() const { return ___roleid_14; }
	inline String_t** get_address_of_roleid_14() { return &___roleid_14; }
	inline void set_roleid_14(String_t* value)
	{
		___roleid_14 = value;
		Il2CppCodeGenWriteBarrier(&___roleid_14, value);
	}

	inline static int32_t get_offset_of_rolename_15() { return static_cast<int32_t>(offsetof(PluginYouDong_t3100552560, ___rolename_15)); }
	inline String_t* get_rolename_15() const { return ___rolename_15; }
	inline String_t** get_address_of_rolename_15() { return &___rolename_15; }
	inline void set_rolename_15(String_t* value)
	{
		___rolename_15 = value;
		Il2CppCodeGenWriteBarrier(&___rolename_15, value);
	}

	inline static int32_t get_offset_of_rolelevel_16() { return static_cast<int32_t>(offsetof(PluginYouDong_t3100552560, ___rolelevel_16)); }
	inline String_t* get_rolelevel_16() const { return ___rolelevel_16; }
	inline String_t** get_address_of_rolelevel_16() { return &___rolelevel_16; }
	inline void set_rolelevel_16(String_t* value)
	{
		___rolelevel_16 = value;
		Il2CppCodeGenWriteBarrier(&___rolelevel_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
