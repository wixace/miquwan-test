﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.ProceduralMaterial
struct ProceduralMaterial_t3619291528;
// UnityEngine.ProceduralPropertyDescription[]
struct ProceduralPropertyDescriptionU5BU5D_t3986076385;
// System.String
struct String_t;
// UnityEngine.Texture2D
struct Texture2D_t3884108195;
// UnityEngine.Texture[]
struct TextureU5BU5D_t606176076;
// UnityEngine.ProceduralTexture
struct ProceduralTexture_t1825615572;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "UnityEngine_UnityEngine_Vector44282066567.h"
#include "UnityEngine_UnityEngine_ProceduralMaterial3619291528.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"
#include "UnityEngine_UnityEngine_Texture2D3884108195.h"
#include "UnityEngine_UnityEngine_ProceduralCacheSize852205916.h"
#include "UnityEngine_UnityEngine_ProceduralLoadingBehavior4286556519.h"
#include "UnityEngine_UnityEngine_ProceduralProcessorUsage4145056144.h"

// UnityEngine.ProceduralPropertyDescription[] UnityEngine.ProceduralMaterial::GetProceduralPropertyDescriptions()
extern "C"  ProceduralPropertyDescriptionU5BU5D_t3986076385* ProceduralMaterial_GetProceduralPropertyDescriptions_m122027656 (ProceduralMaterial_t3619291528 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.ProceduralMaterial::HasProceduralProperty(System.String)
extern "C"  bool ProceduralMaterial_HasProceduralProperty_m795777365 (ProceduralMaterial_t3619291528 * __this, String_t* ___inputName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.ProceduralMaterial::GetProceduralBoolean(System.String)
extern "C"  bool ProceduralMaterial_GetProceduralBoolean_m2397973996 (ProceduralMaterial_t3619291528 * __this, String_t* ___inputName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.ProceduralMaterial::IsProceduralPropertyVisible(System.String)
extern "C"  bool ProceduralMaterial_IsProceduralPropertyVisible_m1100421931 (ProceduralMaterial_t3619291528 * __this, String_t* ___inputName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ProceduralMaterial::SetProceduralBoolean(System.String,System.Boolean)
extern "C"  void ProceduralMaterial_SetProceduralBoolean_m1836400671 (ProceduralMaterial_t3619291528 * __this, String_t* ___inputName0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.ProceduralMaterial::GetProceduralFloat(System.String)
extern "C"  float ProceduralMaterial_GetProceduralFloat_m1726696082 (ProceduralMaterial_t3619291528 * __this, String_t* ___inputName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ProceduralMaterial::SetProceduralFloat(System.String,System.Single)
extern "C"  void ProceduralMaterial_SetProceduralFloat_m303316367 (ProceduralMaterial_t3619291528 * __this, String_t* ___inputName0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector4 UnityEngine.ProceduralMaterial::GetProceduralVector(System.String)
extern "C"  Vector4_t4282066567  ProceduralMaterial_GetProceduralVector_m1183517982 (ProceduralMaterial_t3619291528 * __this, String_t* ___inputName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ProceduralMaterial::INTERNAL_CALL_GetProceduralVector(UnityEngine.ProceduralMaterial,System.String,UnityEngine.Vector4&)
extern "C"  void ProceduralMaterial_INTERNAL_CALL_GetProceduralVector_m1461568731 (Il2CppObject * __this /* static, unused */, ProceduralMaterial_t3619291528 * ___self0, String_t* ___inputName1, Vector4_t4282066567 * ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ProceduralMaterial::SetProceduralVector(System.String,UnityEngine.Vector4)
extern "C"  void ProceduralMaterial_SetProceduralVector_m3824028011 (ProceduralMaterial_t3619291528 * __this, String_t* ___inputName0, Vector4_t4282066567  ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ProceduralMaterial::INTERNAL_CALL_SetProceduralVector(UnityEngine.ProceduralMaterial,System.String,UnityEngine.Vector4&)
extern "C"  void ProceduralMaterial_INTERNAL_CALL_SetProceduralVector_m2595021903 (Il2CppObject * __this /* static, unused */, ProceduralMaterial_t3619291528 * ___self0, String_t* ___inputName1, Vector4_t4282066567 * ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.ProceduralMaterial::GetProceduralColor(System.String)
extern "C"  Color_t4194546905  ProceduralMaterial_GetProceduralColor_m3575867920 (ProceduralMaterial_t3619291528 * __this, String_t* ___inputName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ProceduralMaterial::INTERNAL_CALL_GetProceduralColor(UnityEngine.ProceduralMaterial,System.String,UnityEngine.Color&)
extern "C"  void ProceduralMaterial_INTERNAL_CALL_GetProceduralColor_m3053355969 (Il2CppObject * __this /* static, unused */, ProceduralMaterial_t3619291528 * ___self0, String_t* ___inputName1, Color_t4194546905 * ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ProceduralMaterial::SetProceduralColor(System.String,UnityEngine.Color)
extern "C"  void ProceduralMaterial_SetProceduralColor_m2268341219 (ProceduralMaterial_t3619291528 * __this, String_t* ___inputName0, Color_t4194546905  ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ProceduralMaterial::INTERNAL_CALL_SetProceduralColor(UnityEngine.ProceduralMaterial,System.String,UnityEngine.Color&)
extern "C"  void ProceduralMaterial_INTERNAL_CALL_SetProceduralColor_m2412558541 (Il2CppObject * __this /* static, unused */, ProceduralMaterial_t3619291528 * ___self0, String_t* ___inputName1, Color_t4194546905 * ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.ProceduralMaterial::GetProceduralEnum(System.String)
extern "C"  int32_t ProceduralMaterial_GetProceduralEnum_m3149240199 (ProceduralMaterial_t3619291528 * __this, String_t* ___inputName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ProceduralMaterial::SetProceduralEnum(System.String,System.Int32)
extern "C"  void ProceduralMaterial_SetProceduralEnum_m1579235132 (ProceduralMaterial_t3619291528 * __this, String_t* ___inputName0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture2D UnityEngine.ProceduralMaterial::GetProceduralTexture(System.String)
extern "C"  Texture2D_t3884108195 * ProceduralMaterial_GetProceduralTexture_m1726357922 (ProceduralMaterial_t3619291528 * __this, String_t* ___inputName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ProceduralMaterial::SetProceduralTexture(System.String,UnityEngine.Texture2D)
extern "C"  void ProceduralMaterial_SetProceduralTexture_m2102742369 (ProceduralMaterial_t3619291528 * __this, String_t* ___inputName0, Texture2D_t3884108195 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.ProceduralMaterial::IsProceduralPropertyCached(System.String)
extern "C"  bool ProceduralMaterial_IsProceduralPropertyCached_m2068752515 (ProceduralMaterial_t3619291528 * __this, String_t* ___inputName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ProceduralMaterial::CacheProceduralProperty(System.String,System.Boolean)
extern "C"  void ProceduralMaterial_CacheProceduralProperty_m14566806 (ProceduralMaterial_t3619291528 * __this, String_t* ___inputName0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ProceduralMaterial::ClearCache()
extern "C"  void ProceduralMaterial_ClearCache_m374067952 (ProceduralMaterial_t3619291528 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ProceduralCacheSize UnityEngine.ProceduralMaterial::get_cacheSize()
extern "C"  int32_t ProceduralMaterial_get_cacheSize_m1250223997 (ProceduralMaterial_t3619291528 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ProceduralMaterial::set_cacheSize(UnityEngine.ProceduralCacheSize)
extern "C"  void ProceduralMaterial_set_cacheSize_m1230746166 (ProceduralMaterial_t3619291528 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.ProceduralMaterial::get_animationUpdateRate()
extern "C"  int32_t ProceduralMaterial_get_animationUpdateRate_m866240875 (ProceduralMaterial_t3619291528 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ProceduralMaterial::set_animationUpdateRate(System.Int32)
extern "C"  void ProceduralMaterial_set_animationUpdateRate_m1891369160 (ProceduralMaterial_t3619291528 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ProceduralMaterial::RebuildTextures()
extern "C"  void ProceduralMaterial_RebuildTextures_m3073159802 (ProceduralMaterial_t3619291528 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ProceduralMaterial::RebuildTexturesImmediately()
extern "C"  void ProceduralMaterial_RebuildTexturesImmediately_m1919805254 (ProceduralMaterial_t3619291528 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.ProceduralMaterial::get_isProcessing()
extern "C"  bool ProceduralMaterial_get_isProcessing_m1175493991 (ProceduralMaterial_t3619291528 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ProceduralMaterial::StopRebuilds()
extern "C"  void ProceduralMaterial_StopRebuilds_m2501665749 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.ProceduralMaterial::get_isCachedDataAvailable()
extern "C"  bool ProceduralMaterial_get_isCachedDataAvailable_m2052046155 (ProceduralMaterial_t3619291528 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.ProceduralMaterial::get_isLoadTimeGenerated()
extern "C"  bool ProceduralMaterial_get_isLoadTimeGenerated_m2794649226 (ProceduralMaterial_t3619291528 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ProceduralMaterial::set_isLoadTimeGenerated(System.Boolean)
extern "C"  void ProceduralMaterial_set_isLoadTimeGenerated_m3928866611 (ProceduralMaterial_t3619291528 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ProceduralLoadingBehavior UnityEngine.ProceduralMaterial::get_loadingBehavior()
extern "C"  int32_t ProceduralMaterial_get_loadingBehavior_m3354655315 (ProceduralMaterial_t3619291528 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.ProceduralMaterial::get_isSupported()
extern "C"  bool ProceduralMaterial_get_isSupported_m477853116 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ProceduralProcessorUsage UnityEngine.ProceduralMaterial::get_substanceProcessorUsage()
extern "C"  int32_t ProceduralMaterial_get_substanceProcessorUsage_m1283844721 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ProceduralMaterial::set_substanceProcessorUsage(UnityEngine.ProceduralProcessorUsage)
extern "C"  void ProceduralMaterial_set_substanceProcessorUsage_m1877182950 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.ProceduralMaterial::get_preset()
extern "C"  String_t* ProceduralMaterial_get_preset_m1531441318 (ProceduralMaterial_t3619291528 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ProceduralMaterial::set_preset(System.String)
extern "C"  void ProceduralMaterial_set_preset_m2768412459 (ProceduralMaterial_t3619291528 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture[] UnityEngine.ProceduralMaterial::GetGeneratedTextures()
extern "C"  TextureU5BU5D_t606176076* ProceduralMaterial_GetGeneratedTextures_m206500505 (ProceduralMaterial_t3619291528 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ProceduralTexture UnityEngine.ProceduralMaterial::GetGeneratedTexture(System.String)
extern "C"  ProceduralTexture_t1825615572 * ProceduralMaterial_GetGeneratedTexture_m2199259205 (ProceduralMaterial_t3619291528 * __this, String_t* ___textureName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.ProceduralMaterial::get_isReadable()
extern "C"  bool ProceduralMaterial_get_isReadable_m2314225604 (ProceduralMaterial_t3619291528 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ProceduralMaterial::set_isReadable(System.Boolean)
extern "C"  void ProceduralMaterial_set_isReadable_m144202057 (ProceduralMaterial_t3619291528 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ProceduralMaterial::FreezeAndReleaseSourceData()
extern "C"  void ProceduralMaterial_FreezeAndReleaseSourceData_m2979763687 (ProceduralMaterial_t3619291528 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.ProceduralMaterial::get_isFrozen()
extern "C"  bool ProceduralMaterial_get_isFrozen_m4191605556 (ProceduralMaterial_t3619291528 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
