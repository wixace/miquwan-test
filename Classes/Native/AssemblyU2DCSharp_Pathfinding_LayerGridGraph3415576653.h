﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Int32[]
struct Int32U5BU5D_t3230847821;
// Pathfinding.LevelGridNode[]
struct LevelGridNodeU5BU5D_t498907163;

#include "AssemblyU2DCSharp_Pathfinding_GridGraph2455707914.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.LayerGridGraph
struct  LayerGridGraph_t3415576653  : public GridGraph_t2455707914
{
public:
	// System.Int32[] Pathfinding.LayerGridGraph::nodeCellIndices
	Int32U5BU5D_t3230847821* ___nodeCellIndices_50;
	// System.Int32 Pathfinding.LayerGridGraph::layerCount
	int32_t ___layerCount_51;
	// System.Single Pathfinding.LayerGridGraph::mergeSpanRange
	float ___mergeSpanRange_52;
	// System.Single Pathfinding.LayerGridGraph::characterHeight
	float ___characterHeight_53;
	// System.Int32 Pathfinding.LayerGridGraph::lastScannedWidth
	int32_t ___lastScannedWidth_54;
	// System.Int32 Pathfinding.LayerGridGraph::lastScannedDepth
	int32_t ___lastScannedDepth_55;
	// Pathfinding.LevelGridNode[] Pathfinding.LayerGridGraph::nodes
	LevelGridNodeU5BU5D_t498907163* ___nodes_56;

public:
	inline static int32_t get_offset_of_nodeCellIndices_50() { return static_cast<int32_t>(offsetof(LayerGridGraph_t3415576653, ___nodeCellIndices_50)); }
	inline Int32U5BU5D_t3230847821* get_nodeCellIndices_50() const { return ___nodeCellIndices_50; }
	inline Int32U5BU5D_t3230847821** get_address_of_nodeCellIndices_50() { return &___nodeCellIndices_50; }
	inline void set_nodeCellIndices_50(Int32U5BU5D_t3230847821* value)
	{
		___nodeCellIndices_50 = value;
		Il2CppCodeGenWriteBarrier(&___nodeCellIndices_50, value);
	}

	inline static int32_t get_offset_of_layerCount_51() { return static_cast<int32_t>(offsetof(LayerGridGraph_t3415576653, ___layerCount_51)); }
	inline int32_t get_layerCount_51() const { return ___layerCount_51; }
	inline int32_t* get_address_of_layerCount_51() { return &___layerCount_51; }
	inline void set_layerCount_51(int32_t value)
	{
		___layerCount_51 = value;
	}

	inline static int32_t get_offset_of_mergeSpanRange_52() { return static_cast<int32_t>(offsetof(LayerGridGraph_t3415576653, ___mergeSpanRange_52)); }
	inline float get_mergeSpanRange_52() const { return ___mergeSpanRange_52; }
	inline float* get_address_of_mergeSpanRange_52() { return &___mergeSpanRange_52; }
	inline void set_mergeSpanRange_52(float value)
	{
		___mergeSpanRange_52 = value;
	}

	inline static int32_t get_offset_of_characterHeight_53() { return static_cast<int32_t>(offsetof(LayerGridGraph_t3415576653, ___characterHeight_53)); }
	inline float get_characterHeight_53() const { return ___characterHeight_53; }
	inline float* get_address_of_characterHeight_53() { return &___characterHeight_53; }
	inline void set_characterHeight_53(float value)
	{
		___characterHeight_53 = value;
	}

	inline static int32_t get_offset_of_lastScannedWidth_54() { return static_cast<int32_t>(offsetof(LayerGridGraph_t3415576653, ___lastScannedWidth_54)); }
	inline int32_t get_lastScannedWidth_54() const { return ___lastScannedWidth_54; }
	inline int32_t* get_address_of_lastScannedWidth_54() { return &___lastScannedWidth_54; }
	inline void set_lastScannedWidth_54(int32_t value)
	{
		___lastScannedWidth_54 = value;
	}

	inline static int32_t get_offset_of_lastScannedDepth_55() { return static_cast<int32_t>(offsetof(LayerGridGraph_t3415576653, ___lastScannedDepth_55)); }
	inline int32_t get_lastScannedDepth_55() const { return ___lastScannedDepth_55; }
	inline int32_t* get_address_of_lastScannedDepth_55() { return &___lastScannedDepth_55; }
	inline void set_lastScannedDepth_55(int32_t value)
	{
		___lastScannedDepth_55 = value;
	}

	inline static int32_t get_offset_of_nodes_56() { return static_cast<int32_t>(offsetof(LayerGridGraph_t3415576653, ___nodes_56)); }
	inline LevelGridNodeU5BU5D_t498907163* get_nodes_56() const { return ___nodes_56; }
	inline LevelGridNodeU5BU5D_t498907163** get_address_of_nodes_56() { return &___nodes_56; }
	inline void set_nodes_56(LevelGridNodeU5BU5D_t498907163* value)
	{
		___nodes_56 = value;
		Il2CppCodeGenWriteBarrier(&___nodes_56, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
