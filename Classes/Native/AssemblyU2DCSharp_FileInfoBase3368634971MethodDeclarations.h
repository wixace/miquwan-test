﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FileInfoBase
struct FileInfoBase_t3368634971;

#include "codegen/il2cpp-codegen.h"

// System.Void FileInfoBase::.ctor()
extern "C"  void FileInfoBase__ctor_m2716671648 (FileInfoBase_t3368634971 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
