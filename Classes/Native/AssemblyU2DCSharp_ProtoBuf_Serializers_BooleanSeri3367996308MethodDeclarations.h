﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ProtoBuf.Serializers.BooleanSerializer
struct BooleanSerializer_t3367996308;
// ProtoBuf.Meta.TypeModel
struct TypeModel_t2730011105;
// System.Type
struct Type_t;
// System.Object
struct Il2CppObject;
// ProtoBuf.ProtoWriter
struct ProtoWriter_t4117914721;
// ProtoBuf.ProtoReader
struct ProtoReader_t3962509489;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ProtoBuf_Meta_TypeModel2730011105.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_ProtoBuf_ProtoWriter4117914721.h"
#include "AssemblyU2DCSharp_ProtoBuf_ProtoReader3962509489.h"

// System.Void ProtoBuf.Serializers.BooleanSerializer::.ctor(ProtoBuf.Meta.TypeModel)
extern "C"  void BooleanSerializer__ctor_m2586663256 (BooleanSerializer_t3367996308 * __this, TypeModel_t2730011105 * ___model0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Serializers.BooleanSerializer::.cctor()
extern "C"  void BooleanSerializer__cctor_m2307005148 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Serializers.BooleanSerializer::ProtoBuf.Serializers.IProtoSerializer.get_RequiresOldValue()
extern "C"  bool BooleanSerializer_ProtoBuf_Serializers_IProtoSerializer_get_RequiresOldValue_m2571995125 (BooleanSerializer_t3367996308 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Serializers.BooleanSerializer::ProtoBuf.Serializers.IProtoSerializer.get_ReturnsValue()
extern "C"  bool BooleanSerializer_ProtoBuf_Serializers_IProtoSerializer_get_ReturnsValue_m1888337163 (BooleanSerializer_t3367996308 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type ProtoBuf.Serializers.BooleanSerializer::get_ExpectedType()
extern "C"  Type_t * BooleanSerializer_get_ExpectedType_m3177303400 (BooleanSerializer_t3367996308 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Serializers.BooleanSerializer::Write(System.Object,ProtoBuf.ProtoWriter)
extern "C"  void BooleanSerializer_Write_m3639565352 (BooleanSerializer_t3367996308 * __this, Il2CppObject * ___value0, ProtoWriter_t4117914721 * ___dest1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProtoBuf.Serializers.BooleanSerializer::Read(System.Object,ProtoBuf.ProtoReader)
extern "C"  Il2CppObject * BooleanSerializer_Read_m460754408 (BooleanSerializer_t3367996308 * __this, Il2CppObject * ___value0, ProtoReader_t3962509489 * ___source1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
