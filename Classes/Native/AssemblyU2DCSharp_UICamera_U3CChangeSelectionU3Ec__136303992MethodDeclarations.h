﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UICamera/<ChangeSelection>c__Iterator2A
struct U3CChangeSelectionU3Ec__Iterator2A_t136303992;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void UICamera/<ChangeSelection>c__Iterator2A::.ctor()
extern "C"  void U3CChangeSelectionU3Ec__Iterator2A__ctor_m1910530291 (U3CChangeSelectionU3Ec__Iterator2A_t136303992 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UICamera/<ChangeSelection>c__Iterator2A::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CChangeSelectionU3Ec__Iterator2A_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1716529471 (U3CChangeSelectionU3Ec__Iterator2A_t136303992 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UICamera/<ChangeSelection>c__Iterator2A::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CChangeSelectionU3Ec__Iterator2A_System_Collections_IEnumerator_get_Current_m899844819 (U3CChangeSelectionU3Ec__Iterator2A_t136303992 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UICamera/<ChangeSelection>c__Iterator2A::MoveNext()
extern "C"  bool U3CChangeSelectionU3Ec__Iterator2A_MoveNext_m3886196769 (U3CChangeSelectionU3Ec__Iterator2A_t136303992 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICamera/<ChangeSelection>c__Iterator2A::Dispose()
extern "C"  void U3CChangeSelectionU3Ec__Iterator2A_Dispose_m1966857328 (U3CChangeSelectionU3Ec__Iterator2A_t136303992 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICamera/<ChangeSelection>c__Iterator2A::Reset()
extern "C"  void U3CChangeSelectionU3Ec__Iterator2A_Reset_m3851930528 (U3CChangeSelectionU3Ec__Iterator2A_t136303992 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
