﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t1659122786;
// Seeker
struct Seeker_t2472610117;
// Pathfinding.Path
struct Path_t1974241691;
// UnityEngine.CharacterController
struct CharacterController_t1618060635;
// NavmeshController
struct NavmeshController_t1255162156;
// Pathfinding.RVO.RVOController
struct RVOController_t1639282485;
// UnityEngine.Rigidbody
struct Rigidbody_t3346577219;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AIPath
struct  AIPath_t1930792045  : public MonoBehaviour_t667441552
{
public:
	// System.UInt32 AIPath::AIId
	uint32_t ___AIId_2;
	// System.Single AIPath::repathRate
	float ___repathRate_3;
	// UnityEngine.Transform AIPath::target
	Transform_t1659122786 * ___target_4;
	// System.Boolean AIPath::canSearch
	bool ___canSearch_5;
	// System.Boolean AIPath::_canMove
	bool ____canMove_6;
	// System.Boolean AIPath::canMoveNextWaypoint
	bool ___canMoveNextWaypoint_7;
	// System.Single AIPath::speed
	float ___speed_8;
	// System.Single AIPath::turningSpeed
	float ___turningSpeed_9;
	// System.Single AIPath::slowdownDistance
	float ___slowdownDistance_10;
	// System.Single AIPath::pickNextWaypointDist
	float ___pickNextWaypointDist_11;
	// System.Single AIPath::forwardLook
	float ___forwardLook_12;
	// System.Single AIPath::endReachedDistance
	float ___endReachedDistance_13;
	// System.Boolean AIPath::closestOnPathCheck
	bool ___closestOnPathCheck_14;
	// System.Single AIPath::minMoveScale
	float ___minMoveScale_15;
	// Seeker AIPath::seeker
	Seeker_t2472610117 * ___seeker_16;
	// UnityEngine.Transform AIPath::tr
	Transform_t1659122786 * ___tr_17;
	// System.Single AIPath::lastRepath
	float ___lastRepath_18;
	// Pathfinding.Path AIPath::path
	Path_t1974241691 * ___path_19;
	// UnityEngine.CharacterController AIPath::controller
	CharacterController_t1618060635 * ___controller_20;
	// NavmeshController AIPath::navController
	NavmeshController_t1255162156 * ___navController_21;
	// Pathfinding.RVO.RVOController AIPath::rvoController
	RVOController_t1639282485 * ___rvoController_22;
	// UnityEngine.Rigidbody AIPath::rigid
	Rigidbody_t3346577219 * ___rigid_23;
	// System.Int32 AIPath::currentWaypointIndex
	int32_t ___currentWaypointIndex_24;
	// System.Boolean AIPath::targetReached
	bool ___targetReached_25;
	// System.Boolean AIPath::canSearchAgain
	bool ___canSearchAgain_26;
	// UnityEngine.Vector3 AIPath::lastFoundWaypointPosition
	Vector3_t4282066566  ___lastFoundWaypointPosition_27;
	// System.Single AIPath::lastFoundWaypointTime
	float ___lastFoundWaypointTime_28;
	// System.Boolean AIPath::startHasRun
	bool ___startHasRun_29;
	// System.Boolean AIPath::IsSearchPathing
	bool ___IsSearchPathing_30;
	// UnityEngine.Vector3 AIPath::targetPoint
	Vector3_t4282066566  ___targetPoint_31;
	// UnityEngine.Vector3 AIPath::targetDirection
	Vector3_t4282066566  ___targetDirection_32;

public:
	inline static int32_t get_offset_of_AIId_2() { return static_cast<int32_t>(offsetof(AIPath_t1930792045, ___AIId_2)); }
	inline uint32_t get_AIId_2() const { return ___AIId_2; }
	inline uint32_t* get_address_of_AIId_2() { return &___AIId_2; }
	inline void set_AIId_2(uint32_t value)
	{
		___AIId_2 = value;
	}

	inline static int32_t get_offset_of_repathRate_3() { return static_cast<int32_t>(offsetof(AIPath_t1930792045, ___repathRate_3)); }
	inline float get_repathRate_3() const { return ___repathRate_3; }
	inline float* get_address_of_repathRate_3() { return &___repathRate_3; }
	inline void set_repathRate_3(float value)
	{
		___repathRate_3 = value;
	}

	inline static int32_t get_offset_of_target_4() { return static_cast<int32_t>(offsetof(AIPath_t1930792045, ___target_4)); }
	inline Transform_t1659122786 * get_target_4() const { return ___target_4; }
	inline Transform_t1659122786 ** get_address_of_target_4() { return &___target_4; }
	inline void set_target_4(Transform_t1659122786 * value)
	{
		___target_4 = value;
		Il2CppCodeGenWriteBarrier(&___target_4, value);
	}

	inline static int32_t get_offset_of_canSearch_5() { return static_cast<int32_t>(offsetof(AIPath_t1930792045, ___canSearch_5)); }
	inline bool get_canSearch_5() const { return ___canSearch_5; }
	inline bool* get_address_of_canSearch_5() { return &___canSearch_5; }
	inline void set_canSearch_5(bool value)
	{
		___canSearch_5 = value;
	}

	inline static int32_t get_offset_of__canMove_6() { return static_cast<int32_t>(offsetof(AIPath_t1930792045, ____canMove_6)); }
	inline bool get__canMove_6() const { return ____canMove_6; }
	inline bool* get_address_of__canMove_6() { return &____canMove_6; }
	inline void set__canMove_6(bool value)
	{
		____canMove_6 = value;
	}

	inline static int32_t get_offset_of_canMoveNextWaypoint_7() { return static_cast<int32_t>(offsetof(AIPath_t1930792045, ___canMoveNextWaypoint_7)); }
	inline bool get_canMoveNextWaypoint_7() const { return ___canMoveNextWaypoint_7; }
	inline bool* get_address_of_canMoveNextWaypoint_7() { return &___canMoveNextWaypoint_7; }
	inline void set_canMoveNextWaypoint_7(bool value)
	{
		___canMoveNextWaypoint_7 = value;
	}

	inline static int32_t get_offset_of_speed_8() { return static_cast<int32_t>(offsetof(AIPath_t1930792045, ___speed_8)); }
	inline float get_speed_8() const { return ___speed_8; }
	inline float* get_address_of_speed_8() { return &___speed_8; }
	inline void set_speed_8(float value)
	{
		___speed_8 = value;
	}

	inline static int32_t get_offset_of_turningSpeed_9() { return static_cast<int32_t>(offsetof(AIPath_t1930792045, ___turningSpeed_9)); }
	inline float get_turningSpeed_9() const { return ___turningSpeed_9; }
	inline float* get_address_of_turningSpeed_9() { return &___turningSpeed_9; }
	inline void set_turningSpeed_9(float value)
	{
		___turningSpeed_9 = value;
	}

	inline static int32_t get_offset_of_slowdownDistance_10() { return static_cast<int32_t>(offsetof(AIPath_t1930792045, ___slowdownDistance_10)); }
	inline float get_slowdownDistance_10() const { return ___slowdownDistance_10; }
	inline float* get_address_of_slowdownDistance_10() { return &___slowdownDistance_10; }
	inline void set_slowdownDistance_10(float value)
	{
		___slowdownDistance_10 = value;
	}

	inline static int32_t get_offset_of_pickNextWaypointDist_11() { return static_cast<int32_t>(offsetof(AIPath_t1930792045, ___pickNextWaypointDist_11)); }
	inline float get_pickNextWaypointDist_11() const { return ___pickNextWaypointDist_11; }
	inline float* get_address_of_pickNextWaypointDist_11() { return &___pickNextWaypointDist_11; }
	inline void set_pickNextWaypointDist_11(float value)
	{
		___pickNextWaypointDist_11 = value;
	}

	inline static int32_t get_offset_of_forwardLook_12() { return static_cast<int32_t>(offsetof(AIPath_t1930792045, ___forwardLook_12)); }
	inline float get_forwardLook_12() const { return ___forwardLook_12; }
	inline float* get_address_of_forwardLook_12() { return &___forwardLook_12; }
	inline void set_forwardLook_12(float value)
	{
		___forwardLook_12 = value;
	}

	inline static int32_t get_offset_of_endReachedDistance_13() { return static_cast<int32_t>(offsetof(AIPath_t1930792045, ___endReachedDistance_13)); }
	inline float get_endReachedDistance_13() const { return ___endReachedDistance_13; }
	inline float* get_address_of_endReachedDistance_13() { return &___endReachedDistance_13; }
	inline void set_endReachedDistance_13(float value)
	{
		___endReachedDistance_13 = value;
	}

	inline static int32_t get_offset_of_closestOnPathCheck_14() { return static_cast<int32_t>(offsetof(AIPath_t1930792045, ___closestOnPathCheck_14)); }
	inline bool get_closestOnPathCheck_14() const { return ___closestOnPathCheck_14; }
	inline bool* get_address_of_closestOnPathCheck_14() { return &___closestOnPathCheck_14; }
	inline void set_closestOnPathCheck_14(bool value)
	{
		___closestOnPathCheck_14 = value;
	}

	inline static int32_t get_offset_of_minMoveScale_15() { return static_cast<int32_t>(offsetof(AIPath_t1930792045, ___minMoveScale_15)); }
	inline float get_minMoveScale_15() const { return ___minMoveScale_15; }
	inline float* get_address_of_minMoveScale_15() { return &___minMoveScale_15; }
	inline void set_minMoveScale_15(float value)
	{
		___minMoveScale_15 = value;
	}

	inline static int32_t get_offset_of_seeker_16() { return static_cast<int32_t>(offsetof(AIPath_t1930792045, ___seeker_16)); }
	inline Seeker_t2472610117 * get_seeker_16() const { return ___seeker_16; }
	inline Seeker_t2472610117 ** get_address_of_seeker_16() { return &___seeker_16; }
	inline void set_seeker_16(Seeker_t2472610117 * value)
	{
		___seeker_16 = value;
		Il2CppCodeGenWriteBarrier(&___seeker_16, value);
	}

	inline static int32_t get_offset_of_tr_17() { return static_cast<int32_t>(offsetof(AIPath_t1930792045, ___tr_17)); }
	inline Transform_t1659122786 * get_tr_17() const { return ___tr_17; }
	inline Transform_t1659122786 ** get_address_of_tr_17() { return &___tr_17; }
	inline void set_tr_17(Transform_t1659122786 * value)
	{
		___tr_17 = value;
		Il2CppCodeGenWriteBarrier(&___tr_17, value);
	}

	inline static int32_t get_offset_of_lastRepath_18() { return static_cast<int32_t>(offsetof(AIPath_t1930792045, ___lastRepath_18)); }
	inline float get_lastRepath_18() const { return ___lastRepath_18; }
	inline float* get_address_of_lastRepath_18() { return &___lastRepath_18; }
	inline void set_lastRepath_18(float value)
	{
		___lastRepath_18 = value;
	}

	inline static int32_t get_offset_of_path_19() { return static_cast<int32_t>(offsetof(AIPath_t1930792045, ___path_19)); }
	inline Path_t1974241691 * get_path_19() const { return ___path_19; }
	inline Path_t1974241691 ** get_address_of_path_19() { return &___path_19; }
	inline void set_path_19(Path_t1974241691 * value)
	{
		___path_19 = value;
		Il2CppCodeGenWriteBarrier(&___path_19, value);
	}

	inline static int32_t get_offset_of_controller_20() { return static_cast<int32_t>(offsetof(AIPath_t1930792045, ___controller_20)); }
	inline CharacterController_t1618060635 * get_controller_20() const { return ___controller_20; }
	inline CharacterController_t1618060635 ** get_address_of_controller_20() { return &___controller_20; }
	inline void set_controller_20(CharacterController_t1618060635 * value)
	{
		___controller_20 = value;
		Il2CppCodeGenWriteBarrier(&___controller_20, value);
	}

	inline static int32_t get_offset_of_navController_21() { return static_cast<int32_t>(offsetof(AIPath_t1930792045, ___navController_21)); }
	inline NavmeshController_t1255162156 * get_navController_21() const { return ___navController_21; }
	inline NavmeshController_t1255162156 ** get_address_of_navController_21() { return &___navController_21; }
	inline void set_navController_21(NavmeshController_t1255162156 * value)
	{
		___navController_21 = value;
		Il2CppCodeGenWriteBarrier(&___navController_21, value);
	}

	inline static int32_t get_offset_of_rvoController_22() { return static_cast<int32_t>(offsetof(AIPath_t1930792045, ___rvoController_22)); }
	inline RVOController_t1639282485 * get_rvoController_22() const { return ___rvoController_22; }
	inline RVOController_t1639282485 ** get_address_of_rvoController_22() { return &___rvoController_22; }
	inline void set_rvoController_22(RVOController_t1639282485 * value)
	{
		___rvoController_22 = value;
		Il2CppCodeGenWriteBarrier(&___rvoController_22, value);
	}

	inline static int32_t get_offset_of_rigid_23() { return static_cast<int32_t>(offsetof(AIPath_t1930792045, ___rigid_23)); }
	inline Rigidbody_t3346577219 * get_rigid_23() const { return ___rigid_23; }
	inline Rigidbody_t3346577219 ** get_address_of_rigid_23() { return &___rigid_23; }
	inline void set_rigid_23(Rigidbody_t3346577219 * value)
	{
		___rigid_23 = value;
		Il2CppCodeGenWriteBarrier(&___rigid_23, value);
	}

	inline static int32_t get_offset_of_currentWaypointIndex_24() { return static_cast<int32_t>(offsetof(AIPath_t1930792045, ___currentWaypointIndex_24)); }
	inline int32_t get_currentWaypointIndex_24() const { return ___currentWaypointIndex_24; }
	inline int32_t* get_address_of_currentWaypointIndex_24() { return &___currentWaypointIndex_24; }
	inline void set_currentWaypointIndex_24(int32_t value)
	{
		___currentWaypointIndex_24 = value;
	}

	inline static int32_t get_offset_of_targetReached_25() { return static_cast<int32_t>(offsetof(AIPath_t1930792045, ___targetReached_25)); }
	inline bool get_targetReached_25() const { return ___targetReached_25; }
	inline bool* get_address_of_targetReached_25() { return &___targetReached_25; }
	inline void set_targetReached_25(bool value)
	{
		___targetReached_25 = value;
	}

	inline static int32_t get_offset_of_canSearchAgain_26() { return static_cast<int32_t>(offsetof(AIPath_t1930792045, ___canSearchAgain_26)); }
	inline bool get_canSearchAgain_26() const { return ___canSearchAgain_26; }
	inline bool* get_address_of_canSearchAgain_26() { return &___canSearchAgain_26; }
	inline void set_canSearchAgain_26(bool value)
	{
		___canSearchAgain_26 = value;
	}

	inline static int32_t get_offset_of_lastFoundWaypointPosition_27() { return static_cast<int32_t>(offsetof(AIPath_t1930792045, ___lastFoundWaypointPosition_27)); }
	inline Vector3_t4282066566  get_lastFoundWaypointPosition_27() const { return ___lastFoundWaypointPosition_27; }
	inline Vector3_t4282066566 * get_address_of_lastFoundWaypointPosition_27() { return &___lastFoundWaypointPosition_27; }
	inline void set_lastFoundWaypointPosition_27(Vector3_t4282066566  value)
	{
		___lastFoundWaypointPosition_27 = value;
	}

	inline static int32_t get_offset_of_lastFoundWaypointTime_28() { return static_cast<int32_t>(offsetof(AIPath_t1930792045, ___lastFoundWaypointTime_28)); }
	inline float get_lastFoundWaypointTime_28() const { return ___lastFoundWaypointTime_28; }
	inline float* get_address_of_lastFoundWaypointTime_28() { return &___lastFoundWaypointTime_28; }
	inline void set_lastFoundWaypointTime_28(float value)
	{
		___lastFoundWaypointTime_28 = value;
	}

	inline static int32_t get_offset_of_startHasRun_29() { return static_cast<int32_t>(offsetof(AIPath_t1930792045, ___startHasRun_29)); }
	inline bool get_startHasRun_29() const { return ___startHasRun_29; }
	inline bool* get_address_of_startHasRun_29() { return &___startHasRun_29; }
	inline void set_startHasRun_29(bool value)
	{
		___startHasRun_29 = value;
	}

	inline static int32_t get_offset_of_IsSearchPathing_30() { return static_cast<int32_t>(offsetof(AIPath_t1930792045, ___IsSearchPathing_30)); }
	inline bool get_IsSearchPathing_30() const { return ___IsSearchPathing_30; }
	inline bool* get_address_of_IsSearchPathing_30() { return &___IsSearchPathing_30; }
	inline void set_IsSearchPathing_30(bool value)
	{
		___IsSearchPathing_30 = value;
	}

	inline static int32_t get_offset_of_targetPoint_31() { return static_cast<int32_t>(offsetof(AIPath_t1930792045, ___targetPoint_31)); }
	inline Vector3_t4282066566  get_targetPoint_31() const { return ___targetPoint_31; }
	inline Vector3_t4282066566 * get_address_of_targetPoint_31() { return &___targetPoint_31; }
	inline void set_targetPoint_31(Vector3_t4282066566  value)
	{
		___targetPoint_31 = value;
	}

	inline static int32_t get_offset_of_targetDirection_32() { return static_cast<int32_t>(offsetof(AIPath_t1930792045, ___targetDirection_32)); }
	inline Vector3_t4282066566  get_targetDirection_32() const { return ___targetDirection_32; }
	inline Vector3_t4282066566 * get_address_of_targetDirection_32() { return &___targetDirection_32; }
	inline void set_targetDirection_32(Vector3_t4282066566  value)
	{
		___targetDirection_32 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
