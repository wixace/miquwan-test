﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._35fd586b9e3665e517fabff07fd60e36
struct _35fd586b9e3665e517fabff07fd60e36_t284208270;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._35fd586b9e3665e517fabff07fd60e36::.ctor()
extern "C"  void _35fd586b9e3665e517fabff07fd60e36__ctor_m2442180959 (_35fd586b9e3665e517fabff07fd60e36_t284208270 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._35fd586b9e3665e517fabff07fd60e36::_35fd586b9e3665e517fabff07fd60e36m2(System.Int32)
extern "C"  int32_t _35fd586b9e3665e517fabff07fd60e36__35fd586b9e3665e517fabff07fd60e36m2_m1472091865 (_35fd586b9e3665e517fabff07fd60e36_t284208270 * __this, int32_t ____35fd586b9e3665e517fabff07fd60e36a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._35fd586b9e3665e517fabff07fd60e36::_35fd586b9e3665e517fabff07fd60e36m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _35fd586b9e3665e517fabff07fd60e36__35fd586b9e3665e517fabff07fd60e36m_m287487997 (_35fd586b9e3665e517fabff07fd60e36_t284208270 * __this, int32_t ____35fd586b9e3665e517fabff07fd60e36a0, int32_t ____35fd586b9e3665e517fabff07fd60e36891, int32_t ____35fd586b9e3665e517fabff07fd60e36c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
