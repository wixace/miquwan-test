﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.IO.DirectoryInfo
struct DirectoryInfo_t89154617;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t4054002952;
// System.Collections.ArrayList
struct ArrayList_t3948406897;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_DateTime4283661327.h"
#include "mscorlib_System_IO_SearchOption4025787181.h"
#include "mscorlib_System_Collections_ArrayList3948406897.h"
#include "mscorlib_System_IO_FileAttributes2368997539.h"

// System.IO.DirectoryInfo System.IO.Directory::CreateDirectory(System.String)
extern "C"  DirectoryInfo_t89154617 * Directory_CreateDirectory_m677877474 (Il2CppObject * __this /* static, unused */, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.DirectoryInfo System.IO.Directory::CreateDirectoriesInternal(System.String)
extern "C"  DirectoryInfo_t89154617 * Directory_CreateDirectoriesInternal_m1545778887 (Il2CppObject * __this /* static, unused */, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.Directory::Delete(System.String)
extern "C"  void Directory_Delete_m1796764451 (Il2CppObject * __this /* static, unused */, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.Directory::RecursiveDelete(System.String)
extern "C"  void Directory_RecursiveDelete_m184278935 (Il2CppObject * __this /* static, unused */, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.Directory::Delete(System.String,System.Boolean)
extern "C"  void Directory_Delete_m1791284730 (Il2CppObject * __this /* static, unused */, String_t* ___path0, bool ___recursive1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.IO.Directory::Exists(System.String)
extern "C"  bool Directory_Exists_m4117375188 (Il2CppObject * __this /* static, unused */, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.IO.Directory::GetLastAccessTime(System.String)
extern "C"  DateTime_t4283661327  Directory_GetLastAccessTime_m2927004190 (Il2CppObject * __this /* static, unused */, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.IO.Directory::GetLastAccessTimeUtc(System.String)
extern "C"  DateTime_t4283661327  Directory_GetLastAccessTimeUtc_m575474176 (Il2CppObject * __this /* static, unused */, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.IO.Directory::GetLastWriteTime(System.String)
extern "C"  DateTime_t4283661327  Directory_GetLastWriteTime_m923129895 (Il2CppObject * __this /* static, unused */, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.IO.Directory::GetLastWriteTimeUtc(System.String)
extern "C"  DateTime_t4283661327  Directory_GetLastWriteTimeUtc_m3201766231 (Il2CppObject * __this /* static, unused */, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.IO.Directory::GetCreationTime(System.String)
extern "C"  DateTime_t4283661327  Directory_GetCreationTime_m1112594361 (Il2CppObject * __this /* static, unused */, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.IO.Directory::GetCreationTimeUtc(System.String)
extern "C"  DateTime_t4283661327  Directory_GetCreationTimeUtc_m3950645893 (Il2CppObject * __this /* static, unused */, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.IO.Directory::GetCurrentDirectory()
extern "C"  String_t* Directory_GetCurrentDirectory_m3403080603 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] System.IO.Directory::GetDirectories(System.String)
extern "C"  StringU5BU5D_t4054002952* Directory_GetDirectories_m888334714 (Il2CppObject * __this /* static, unused */, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] System.IO.Directory::GetDirectories(System.String,System.String)
extern "C"  StringU5BU5D_t4054002952* Directory_GetDirectories_m1009841206 (Il2CppObject * __this /* static, unused */, String_t* ___path0, String_t* ___searchPattern1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] System.IO.Directory::GetDirectories(System.String,System.String,System.IO.SearchOption)
extern "C"  StringU5BU5D_t4054002952* Directory_GetDirectories_m323776010 (Il2CppObject * __this /* static, unused */, String_t* ___path0, String_t* ___searchPattern1, int32_t ___searchOption2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.Directory::GetDirectoriesRecurse(System.String,System.String,System.Collections.ArrayList)
extern "C"  void Directory_GetDirectoriesRecurse_m2000140993 (Il2CppObject * __this /* static, unused */, String_t* ___path0, String_t* ___searchPattern1, ArrayList_t3948406897 * ___all2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.IO.Directory::GetDirectoryRoot(System.String)
extern "C"  String_t* Directory_GetDirectoryRoot_m2404920824 (Il2CppObject * __this /* static, unused */, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] System.IO.Directory::GetFiles(System.String)
extern "C"  StringU5BU5D_t4054002952* Directory_GetFiles_m3665304654 (Il2CppObject * __this /* static, unused */, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] System.IO.Directory::GetFiles(System.String,System.String)
extern "C"  StringU5BU5D_t4054002952* Directory_GetFiles_m2573902858 (Il2CppObject * __this /* static, unused */, String_t* ___path0, String_t* ___searchPattern1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] System.IO.Directory::GetFiles(System.String,System.String,System.IO.SearchOption)
extern "C"  StringU5BU5D_t4054002952* Directory_GetFiles_m2817011638 (Il2CppObject * __this /* static, unused */, String_t* ___path0, String_t* ___searchPattern1, int32_t ___searchOption2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.Directory::GetFilesRecurse(System.String,System.String,System.Collections.ArrayList)
extern "C"  void Directory_GetFilesRecurse_m1825262549 (Il2CppObject * __this /* static, unused */, String_t* ___path0, String_t* ___searchPattern1, ArrayList_t3948406897 * ___all2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] System.IO.Directory::GetFileSystemEntries(System.String)
extern "C"  StringU5BU5D_t4054002952* Directory_GetFileSystemEntries_m3802714048 (Il2CppObject * __this /* static, unused */, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] System.IO.Directory::GetFileSystemEntries(System.String,System.String)
extern "C"  StringU5BU5D_t4054002952* Directory_GetFileSystemEntries_m4238043132 (Il2CppObject * __this /* static, unused */, String_t* ___path0, String_t* ___searchPattern1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] System.IO.Directory::GetLogicalDrives()
extern "C"  StringU5BU5D_t4054002952* Directory_GetLogicalDrives_m1006877583 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.IO.Directory::IsRootDirectory(System.String)
extern "C"  bool Directory_IsRootDirectory_m3054681777 (Il2CppObject * __this /* static, unused */, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.DirectoryInfo System.IO.Directory::GetParent(System.String)
extern "C"  DirectoryInfo_t89154617 * Directory_GetParent_m1452284339 (Il2CppObject * __this /* static, unused */, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.Directory::Move(System.String,System.String)
extern "C"  void Directory_Move_m2989323513 (Il2CppObject * __this /* static, unused */, String_t* ___sourceDirName0, String_t* ___destDirName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.Directory::SetCreationTime(System.String,System.DateTime)
extern "C"  void Directory_SetCreationTime_m3302862104 (Il2CppObject * __this /* static, unused */, String_t* ___path0, DateTime_t4283661327  ___creationTime1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.Directory::SetCreationTimeUtc(System.String,System.DateTime)
extern "C"  void Directory_SetCreationTimeUtc_m2967699434 (Il2CppObject * __this /* static, unused */, String_t* ___path0, DateTime_t4283661327  ___creationTimeUtc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.Directory::SetCurrentDirectory(System.String)
extern "C"  void Directory_SetCurrentDirectory_m3337732094 (Il2CppObject * __this /* static, unused */, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.Directory::SetLastAccessTime(System.String,System.DateTime)
extern "C"  void Directory_SetLastAccessTime_m523898429 (Il2CppObject * __this /* static, unused */, String_t* ___path0, DateTime_t4283661327  ___lastAccessTime1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.Directory::SetLastAccessTimeUtc(System.String,System.DateTime)
extern "C"  void Directory_SetLastAccessTimeUtc_m355487909 (Il2CppObject * __this /* static, unused */, String_t* ___path0, DateTime_t4283661327  ___lastAccessTimeUtc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.Directory::SetLastWriteTime(System.String,System.DateTime)
extern "C"  void Directory_SetLastWriteTime_m399950412 (Il2CppObject * __this /* static, unused */, String_t* ___path0, DateTime_t4283661327  ___lastWriteTime1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.Directory::SetLastWriteTimeUtc(System.String,System.DateTime)
extern "C"  void Directory_SetLastWriteTimeUtc_m1491988022 (Il2CppObject * __this /* static, unused */, String_t* ___path0, DateTime_t4283661327  ___lastWriteTimeUtc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.Directory::CheckPathExceptions(System.String)
extern "C"  void Directory_CheckPathExceptions_m3281597667 (Il2CppObject * __this /* static, unused */, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] System.IO.Directory::GetFileSystemEntries(System.String,System.String,System.IO.FileAttributes,System.IO.FileAttributes)
extern "C"  StringU5BU5D_t4054002952* Directory_GetFileSystemEntries_m3873165372 (Il2CppObject * __this /* static, unused */, String_t* ___path0, String_t* ___searchPattern1, int32_t ___mask2, int32_t ___attrs3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
