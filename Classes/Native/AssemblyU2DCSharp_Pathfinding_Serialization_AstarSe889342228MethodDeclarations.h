﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.Serialization.AstarSerializer/<SerializeExtraInfo>c__AnonStorey10D
struct U3CSerializeExtraInfoU3Ec__AnonStorey10D_t889342228;
// Pathfinding.GraphNode
struct GraphNode_t23612370;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphNode23612370.h"

// System.Void Pathfinding.Serialization.AstarSerializer/<SerializeExtraInfo>c__AnonStorey10D::.ctor()
extern "C"  void U3CSerializeExtraInfoU3Ec__AnonStorey10D__ctor_m136690183 (U3CSerializeExtraInfoU3Ec__AnonStorey10D_t889342228 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.Serialization.AstarSerializer/<SerializeExtraInfo>c__AnonStorey10D::<>m__340(Pathfinding.GraphNode)
extern "C"  bool U3CSerializeExtraInfoU3Ec__AnonStorey10D_U3CU3Em__340_m87464279 (U3CSerializeExtraInfoU3Ec__AnonStorey10D_t889342228 * __this, GraphNode_t23612370 * ___node0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
