﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.NavGraph/<OnDrawGizmos>c__AnonStorey116
struct U3COnDrawGizmosU3Ec__AnonStorey116_t646220399;
// Pathfinding.GraphNode
struct GraphNode_t23612370;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphNode23612370.h"

// System.Void Pathfinding.NavGraph/<OnDrawGizmos>c__AnonStorey116::.ctor()
extern "C"  void U3COnDrawGizmosU3Ec__AnonStorey116__ctor_m3720820188 (U3COnDrawGizmosU3Ec__AnonStorey116_t646220399 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NavGraph/<OnDrawGizmos>c__AnonStorey116::<>m__34A(Pathfinding.GraphNode)
extern "C"  void U3COnDrawGizmosU3Ec__AnonStorey116_U3CU3Em__34A_m4252491823 (U3COnDrawGizmosU3Ec__AnonStorey116_t646220399 * __this, GraphNode_t23612370 * ___o0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.NavGraph/<OnDrawGizmos>c__AnonStorey116::<>m__34B(Pathfinding.GraphNode)
extern "C"  bool U3COnDrawGizmosU3Ec__AnonStorey116_U3CU3Em__34B_m3669318594 (U3COnDrawGizmosU3Ec__AnonStorey116_t646220399 * __this, GraphNode_t23612370 * ____node0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
