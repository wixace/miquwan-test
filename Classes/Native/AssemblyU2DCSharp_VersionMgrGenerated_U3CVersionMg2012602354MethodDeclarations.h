﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VersionMgrGenerated/<VersionMgr_LoadAllVSCfg_GetDelegate_member2_arg1>c__AnonStoreyF9
struct U3CVersionMgr_LoadAllVSCfg_GetDelegate_member2_arg1U3Ec__AnonStoreyF9_t2012602354;

#include "codegen/il2cpp-codegen.h"

// System.Void VersionMgrGenerated/<VersionMgr_LoadAllVSCfg_GetDelegate_member2_arg1>c__AnonStoreyF9::.ctor()
extern "C"  void U3CVersionMgr_LoadAllVSCfg_GetDelegate_member2_arg1U3Ec__AnonStoreyF9__ctor_m806201785 (U3CVersionMgr_LoadAllVSCfg_GetDelegate_member2_arg1U3Ec__AnonStoreyF9_t2012602354 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VersionMgrGenerated/<VersionMgr_LoadAllVSCfg_GetDelegate_member2_arg1>c__AnonStoreyF9::<>m__2F9()
extern "C"  void U3CVersionMgr_LoadAllVSCfg_GetDelegate_member2_arg1U3Ec__AnonStoreyF9_U3CU3Em__2F9_m2931615493 (U3CVersionMgr_LoadAllVSCfg_GetDelegate_member2_arg1U3Ec__AnonStoreyF9_t2012602354 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
