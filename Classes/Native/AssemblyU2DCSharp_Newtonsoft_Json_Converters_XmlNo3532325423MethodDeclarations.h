﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Converters.XmlNodeConverter/<SerializeNode>c__AnonStorey12A
struct U3CSerializeNodeU3Ec__AnonStorey12A_t3532325423;
// Newtonsoft.Json.Converters.IXmlNode
struct IXmlNode_t2349752174;

#include "codegen/il2cpp-codegen.h"

// System.Void Newtonsoft.Json.Converters.XmlNodeConverter/<SerializeNode>c__AnonStorey12A::.ctor()
extern "C"  void U3CSerializeNodeU3Ec__AnonStorey12A__ctor_m240336412 (U3CSerializeNodeU3Ec__AnonStorey12A_t3532325423 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Converters.XmlNodeConverter/<SerializeNode>c__AnonStorey12A::<>m__36B(Newtonsoft.Json.Converters.IXmlNode)
extern "C"  bool U3CSerializeNodeU3Ec__AnonStorey12A_U3CU3Em__36B_m1899322772 (U3CSerializeNodeU3Ec__AnonStorey12A_t3532325423 * __this, Il2CppObject * ___n0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
