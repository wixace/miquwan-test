﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_cheenallsair32
struct M_cheenallsair32_t3769312670;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_cheenallsair323769312670.h"

// System.Void GarbageiOS.M_cheenallsair32::.ctor()
extern "C"  void M_cheenallsair32__ctor_m686023765 (M_cheenallsair32_t3769312670 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_cheenallsair32::M_fineezere0(System.String[],System.Int32)
extern "C"  void M_cheenallsair32_M_fineezere0_m2377219743 (M_cheenallsair32_t3769312670 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_cheenallsair32::M_gameazi1(System.String[],System.Int32)
extern "C"  void M_cheenallsair32_M_gameazi1_m2307398955 (M_cheenallsair32_t3769312670 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_cheenallsair32::M_bayli2(System.String[],System.Int32)
extern "C"  void M_cheenallsair32_M_bayli2_m4085212499 (M_cheenallsair32_t3769312670 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_cheenallsair32::M_xibalZowdremjea3(System.String[],System.Int32)
extern "C"  void M_cheenallsair32_M_xibalZowdremjea3_m4049737409 (M_cheenallsair32_t3769312670 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_cheenallsair32::M_lemvisar4(System.String[],System.Int32)
extern "C"  void M_cheenallsair32_M_lemvisar4_m496276911 (M_cheenallsair32_t3769312670 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_cheenallsair32::M_jobeMirwhow5(System.String[],System.Int32)
extern "C"  void M_cheenallsair32_M_jobeMirwhow5_m2238366918 (M_cheenallsair32_t3769312670 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_cheenallsair32::M_nejalLigaw6(System.String[],System.Int32)
extern "C"  void M_cheenallsair32_M_nejalLigaw6_m1006320236 (M_cheenallsair32_t3769312670 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_cheenallsair32::M_losourea7(System.String[],System.Int32)
extern "C"  void M_cheenallsair32_M_losourea7_m4075905591 (M_cheenallsair32_t3769312670 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_cheenallsair32::M_puboonor8(System.String[],System.Int32)
extern "C"  void M_cheenallsair32_M_puboonor8_m3182866044 (M_cheenallsair32_t3769312670 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_cheenallsair32::M_quhirbuHiski9(System.String[],System.Int32)
extern "C"  void M_cheenallsair32_M_quhirbuHiski9_m726558465 (M_cheenallsair32_t3769312670 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_cheenallsair32::M_wowmicai10(System.String[],System.Int32)
extern "C"  void M_cheenallsair32_M_wowmicai10_m2658953191 (M_cheenallsair32_t3769312670 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_cheenallsair32::M_kerestouciNijena11(System.String[],System.Int32)
extern "C"  void M_cheenallsair32_M_kerestouciNijena11_m2992758715 (M_cheenallsair32_t3769312670 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_cheenallsair32::M_mirmirheaCevow12(System.String[],System.Int32)
extern "C"  void M_cheenallsair32_M_mirmirheaCevow12_m3333723601 (M_cheenallsair32_t3769312670 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_cheenallsair32::M_trirdu13(System.String[],System.Int32)
extern "C"  void M_cheenallsair32_M_trirdu13_m3528470578 (M_cheenallsair32_t3769312670 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_cheenallsair32::M_pisfosaBurweati14(System.String[],System.Int32)
extern "C"  void M_cheenallsair32_M_pisfosaBurweati14_m1521976373 (M_cheenallsair32_t3769312670 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_cheenallsair32::ilo_M_gameazi11(GarbageiOS.M_cheenallsair32,System.String[],System.Int32)
extern "C"  void M_cheenallsair32_ilo_M_gameazi11_m878024557 (Il2CppObject * __this /* static, unused */, M_cheenallsair32_t3769312670 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_cheenallsair32::ilo_M_lemvisar42(GarbageiOS.M_cheenallsair32,System.String[],System.Int32)
extern "C"  void M_cheenallsair32_ilo_M_lemvisar42_m3320311408 (Il2CppObject * __this /* static, unused */, M_cheenallsair32_t3769312670 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_cheenallsair32::ilo_M_quhirbuHiski93(GarbageiOS.M_cheenallsair32,System.String[],System.Int32)
extern "C"  void M_cheenallsair32_ilo_M_quhirbuHiski93_m4190818719 (Il2CppObject * __this /* static, unused */, M_cheenallsair32_t3769312670 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_cheenallsair32::ilo_M_kerestouciNijena114(GarbageiOS.M_cheenallsair32,System.String[],System.Int32)
extern "C"  void M_cheenallsair32_ilo_M_kerestouciNijena114_m1130352864 (Il2CppObject * __this /* static, unused */, M_cheenallsair32_t3769312670 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
