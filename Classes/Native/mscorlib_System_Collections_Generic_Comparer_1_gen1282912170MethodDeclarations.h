﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1<Pathfinding.ClipperLib.IntPoint>
struct Comparer_1_t1282912170;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System.Collections.Generic.Comparer`1<Pathfinding.ClipperLib.IntPoint>::.ctor()
extern "C"  void Comparer_1__ctor_m2917312402_gshared (Comparer_1_t1282912170 * __this, const MethodInfo* method);
#define Comparer_1__ctor_m2917312402(__this, method) ((  void (*) (Comparer_1_t1282912170 *, const MethodInfo*))Comparer_1__ctor_m2917312402_gshared)(__this, method)
// System.Void System.Collections.Generic.Comparer`1<Pathfinding.ClipperLib.IntPoint>::.cctor()
extern "C"  void Comparer_1__cctor_m4055242331_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1__cctor_m4055242331(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Comparer_1__cctor_m4055242331_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.Comparer`1<Pathfinding.ClipperLib.IntPoint>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern "C"  int32_t Comparer_1_System_Collections_IComparer_Compare_m1184215935_gshared (Comparer_1_t1282912170 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method);
#define Comparer_1_System_Collections_IComparer_Compare_m1184215935(__this, ___x0, ___y1, method) ((  int32_t (*) (Comparer_1_t1282912170 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Comparer_1_System_Collections_IComparer_Compare_m1184215935_gshared)(__this, ___x0, ___y1, method)
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<Pathfinding.ClipperLib.IntPoint>::get_Default()
extern "C"  Comparer_1_t1282912170 * Comparer_1_get_Default_m3396505110_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1_get_Default_m3396505110(__this /* static, unused */, method) ((  Comparer_1_t1282912170 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Comparer_1_get_Default_m3396505110_gshared)(__this /* static, unused */, method)
