﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.GraphCollision
struct GraphCollision_t2160440954;
// UnityEngine.RaycastHit[]
struct RaycastHitU5BU5D_t528650843;
// Pathfinding.Serialization.GraphSerializationContext
struct GraphSerializationContext_t3256954663;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Matrix4x41651859333.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_RaycastHit4003175726.h"
#include "AssemblyU2DCSharp_Pathfinding_Serialization_GraphS3256954663.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphCollision2160440954.h"

// System.Void Pathfinding.GraphCollision::.ctor()
extern "C"  void GraphCollision__ctor_m3678393133 (GraphCollision_t2160440954 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GraphCollision::Initialize(UnityEngine.Matrix4x4,System.Single)
extern "C"  void GraphCollision_Initialize_m2513082616 (GraphCollision_t2160440954 * __this, Matrix4x4_t1651859333  ___matrix0, float ___scale1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.GraphCollision::Check(UnityEngine.Vector3)
extern "C"  bool GraphCollision_Check_m1980577114 (GraphCollision_t2160440954 * __this, Vector3_t4282066566  ___position0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Pathfinding.GraphCollision::CheckHeight(UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  GraphCollision_CheckHeight_m2145166009 (GraphCollision_t2160440954 * __this, Vector3_t4282066566  ___position0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Pathfinding.GraphCollision::CheckHeight(UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Boolean&)
extern "C"  Vector3_t4282066566  GraphCollision_CheckHeight_m483431439 (GraphCollision_t2160440954 * __this, Vector3_t4282066566  ___position0, RaycastHit_t4003175726 * ___hit1, bool* ___walkable2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Pathfinding.GraphCollision::Raycast(UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Boolean&)
extern "C"  Vector3_t4282066566  GraphCollision_Raycast_m3506262069 (GraphCollision_t2160440954 * __this, Vector3_t4282066566  ___origin0, RaycastHit_t4003175726 * ___hit1, bool* ___walkable2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit[] Pathfinding.GraphCollision::CheckHeightAll(UnityEngine.Vector3)
extern "C"  RaycastHitU5BU5D_t528650843* GraphCollision_CheckHeightAll_m2644777878 (GraphCollision_t2160440954 * __this, Vector3_t4282066566  ___position0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GraphCollision::SerializeSettings(Pathfinding.Serialization.GraphSerializationContext)
extern "C"  void GraphCollision_SerializeSettings_m107750431 (GraphCollision_t2160440954 * __this, GraphSerializationContext_t3256954663 * ___ctx0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GraphCollision::DeserializeSettings(Pathfinding.Serialization.GraphSerializationContext)
extern "C"  void GraphCollision_DeserializeSettings_m178486880 (GraphCollision_t2160440954 * __this, GraphSerializationContext_t3256954663 * ___ctx0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Pathfinding.GraphCollision::ilo_CheckHeight1(Pathfinding.GraphCollision,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Boolean&)
extern "C"  Vector3_t4282066566  GraphCollision_ilo_CheckHeight1_m3156994273 (Il2CppObject * __this /* static, unused */, GraphCollision_t2160440954 * ____this0, Vector3_t4282066566  ___position1, RaycastHit_t4003175726 * ___hit2, bool* ___walkable3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
