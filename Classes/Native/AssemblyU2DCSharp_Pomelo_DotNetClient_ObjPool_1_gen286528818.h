﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Stack`1<System.Object>
struct Stack_1_t2974409999;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pomelo.DotNetClient.ObjPool`1<System.Object>
struct  ObjPool_1_t286528818  : public Il2CppObject
{
public:
	// System.Collections.Generic.Stack`1<T> Pomelo.DotNetClient.ObjPool`1::idleList
	Stack_1_t2974409999 * ___idleList_0;
	// System.Int32 Pomelo.DotNetClient.ObjPool`1::maxNum
	int32_t ___maxNum_1;

public:
	inline static int32_t get_offset_of_idleList_0() { return static_cast<int32_t>(offsetof(ObjPool_1_t286528818, ___idleList_0)); }
	inline Stack_1_t2974409999 * get_idleList_0() const { return ___idleList_0; }
	inline Stack_1_t2974409999 ** get_address_of_idleList_0() { return &___idleList_0; }
	inline void set_idleList_0(Stack_1_t2974409999 * value)
	{
		___idleList_0 = value;
		Il2CppCodeGenWriteBarrier(&___idleList_0, value);
	}

	inline static int32_t get_offset_of_maxNum_1() { return static_cast<int32_t>(offsetof(ObjPool_1_t286528818, ___maxNum_1)); }
	inline int32_t get_maxNum_1() const { return ___maxNum_1; }
	inline int32_t* get_address_of_maxNum_1() { return &___maxNum_1; }
	inline void set_maxNum_1(int32_t value)
	{
		___maxNum_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
