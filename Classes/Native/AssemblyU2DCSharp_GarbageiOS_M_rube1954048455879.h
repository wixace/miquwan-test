﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_rube195
struct  M_rube195_t4048455879  : public Il2CppObject
{
public:
	// System.Boolean GarbageiOS.M_rube195::_kewemTrallwhorjir
	bool ____kewemTrallwhorjir_0;
	// System.UInt32 GarbageiOS.M_rube195::_weagearras
	uint32_t ____weagearras_1;
	// System.UInt32 GarbageiOS.M_rube195::_lougecairSawdi
	uint32_t ____lougecairSawdi_2;
	// System.Int32 GarbageiOS.M_rube195::_jowsa
	int32_t ____jowsa_3;
	// System.Single GarbageiOS.M_rube195::_tralsirTenatar
	float ____tralsirTenatar_4;
	// System.Int32 GarbageiOS.M_rube195::_doumersairJalbisrear
	int32_t ____doumersairJalbisrear_5;
	// System.Boolean GarbageiOS.M_rube195::_fairsootou
	bool ____fairsootou_6;
	// System.String GarbageiOS.M_rube195::_sezee
	String_t* ____sezee_7;
	// System.Single GarbageiOS.M_rube195::_jerreLoostiser
	float ____jerreLoostiser_8;
	// System.Int32 GarbageiOS.M_rube195::_morrairsi
	int32_t ____morrairsi_9;
	// System.String GarbageiOS.M_rube195::_dehem
	String_t* ____dehem_10;
	// System.Single GarbageiOS.M_rube195::_chawzetawTexemna
	float ____chawzetawTexemna_11;
	// System.Boolean GarbageiOS.M_rube195::_qeaza
	bool ____qeaza_12;
	// System.String GarbageiOS.M_rube195::_memairSusaltis
	String_t* ____memairSusaltis_13;
	// System.Boolean GarbageiOS.M_rube195::_sehorfa
	bool ____sehorfa_14;
	// System.Boolean GarbageiOS.M_rube195::_cheedayhiHearperedem
	bool ____cheedayhiHearperedem_15;
	// System.Boolean GarbageiOS.M_rube195::_tapalDowtai
	bool ____tapalDowtai_16;
	// System.UInt32 GarbageiOS.M_rube195::_raidaipooPouji
	uint32_t ____raidaipooPouji_17;
	// System.Single GarbageiOS.M_rube195::_gasafair
	float ____gasafair_18;
	// System.Boolean GarbageiOS.M_rube195::_mekairhay
	bool ____mekairhay_19;
	// System.Int32 GarbageiOS.M_rube195::_dureretrur
	int32_t ____dureretrur_20;
	// System.Single GarbageiOS.M_rube195::_hereniStasterena
	float ____hereniStasterena_21;
	// System.Single GarbageiOS.M_rube195::_mamu
	float ____mamu_22;
	// System.UInt32 GarbageiOS.M_rube195::_dexea
	uint32_t ____dexea_23;
	// System.Int32 GarbageiOS.M_rube195::_newhereKeasanair
	int32_t ____newhereKeasanair_24;
	// System.Single GarbageiOS.M_rube195::_zirasSemhurlar
	float ____zirasSemhurlar_25;
	// System.UInt32 GarbageiOS.M_rube195::_dejaka
	uint32_t ____dejaka_26;
	// System.Single GarbageiOS.M_rube195::_jeegoomawDeerallkow
	float ____jeegoomawDeerallkow_27;
	// System.Single GarbageiOS.M_rube195::_nisbaiCherkowsu
	float ____nisbaiCherkowsu_28;
	// System.UInt32 GarbageiOS.M_rube195::_coure
	uint32_t ____coure_29;
	// System.Boolean GarbageiOS.M_rube195::_roudarlaJowporka
	bool ____roudarlaJowporka_30;
	// System.String GarbageiOS.M_rube195::_kurneseSirjurtor
	String_t* ____kurneseSirjurtor_31;
	// System.String GarbageiOS.M_rube195::_seejo
	String_t* ____seejo_32;
	// System.Int32 GarbageiOS.M_rube195::_celoDosarnas
	int32_t ____celoDosarnas_33;

public:
	inline static int32_t get_offset_of__kewemTrallwhorjir_0() { return static_cast<int32_t>(offsetof(M_rube195_t4048455879, ____kewemTrallwhorjir_0)); }
	inline bool get__kewemTrallwhorjir_0() const { return ____kewemTrallwhorjir_0; }
	inline bool* get_address_of__kewemTrallwhorjir_0() { return &____kewemTrallwhorjir_0; }
	inline void set__kewemTrallwhorjir_0(bool value)
	{
		____kewemTrallwhorjir_0 = value;
	}

	inline static int32_t get_offset_of__weagearras_1() { return static_cast<int32_t>(offsetof(M_rube195_t4048455879, ____weagearras_1)); }
	inline uint32_t get__weagearras_1() const { return ____weagearras_1; }
	inline uint32_t* get_address_of__weagearras_1() { return &____weagearras_1; }
	inline void set__weagearras_1(uint32_t value)
	{
		____weagearras_1 = value;
	}

	inline static int32_t get_offset_of__lougecairSawdi_2() { return static_cast<int32_t>(offsetof(M_rube195_t4048455879, ____lougecairSawdi_2)); }
	inline uint32_t get__lougecairSawdi_2() const { return ____lougecairSawdi_2; }
	inline uint32_t* get_address_of__lougecairSawdi_2() { return &____lougecairSawdi_2; }
	inline void set__lougecairSawdi_2(uint32_t value)
	{
		____lougecairSawdi_2 = value;
	}

	inline static int32_t get_offset_of__jowsa_3() { return static_cast<int32_t>(offsetof(M_rube195_t4048455879, ____jowsa_3)); }
	inline int32_t get__jowsa_3() const { return ____jowsa_3; }
	inline int32_t* get_address_of__jowsa_3() { return &____jowsa_3; }
	inline void set__jowsa_3(int32_t value)
	{
		____jowsa_3 = value;
	}

	inline static int32_t get_offset_of__tralsirTenatar_4() { return static_cast<int32_t>(offsetof(M_rube195_t4048455879, ____tralsirTenatar_4)); }
	inline float get__tralsirTenatar_4() const { return ____tralsirTenatar_4; }
	inline float* get_address_of__tralsirTenatar_4() { return &____tralsirTenatar_4; }
	inline void set__tralsirTenatar_4(float value)
	{
		____tralsirTenatar_4 = value;
	}

	inline static int32_t get_offset_of__doumersairJalbisrear_5() { return static_cast<int32_t>(offsetof(M_rube195_t4048455879, ____doumersairJalbisrear_5)); }
	inline int32_t get__doumersairJalbisrear_5() const { return ____doumersairJalbisrear_5; }
	inline int32_t* get_address_of__doumersairJalbisrear_5() { return &____doumersairJalbisrear_5; }
	inline void set__doumersairJalbisrear_5(int32_t value)
	{
		____doumersairJalbisrear_5 = value;
	}

	inline static int32_t get_offset_of__fairsootou_6() { return static_cast<int32_t>(offsetof(M_rube195_t4048455879, ____fairsootou_6)); }
	inline bool get__fairsootou_6() const { return ____fairsootou_6; }
	inline bool* get_address_of__fairsootou_6() { return &____fairsootou_6; }
	inline void set__fairsootou_6(bool value)
	{
		____fairsootou_6 = value;
	}

	inline static int32_t get_offset_of__sezee_7() { return static_cast<int32_t>(offsetof(M_rube195_t4048455879, ____sezee_7)); }
	inline String_t* get__sezee_7() const { return ____sezee_7; }
	inline String_t** get_address_of__sezee_7() { return &____sezee_7; }
	inline void set__sezee_7(String_t* value)
	{
		____sezee_7 = value;
		Il2CppCodeGenWriteBarrier(&____sezee_7, value);
	}

	inline static int32_t get_offset_of__jerreLoostiser_8() { return static_cast<int32_t>(offsetof(M_rube195_t4048455879, ____jerreLoostiser_8)); }
	inline float get__jerreLoostiser_8() const { return ____jerreLoostiser_8; }
	inline float* get_address_of__jerreLoostiser_8() { return &____jerreLoostiser_8; }
	inline void set__jerreLoostiser_8(float value)
	{
		____jerreLoostiser_8 = value;
	}

	inline static int32_t get_offset_of__morrairsi_9() { return static_cast<int32_t>(offsetof(M_rube195_t4048455879, ____morrairsi_9)); }
	inline int32_t get__morrairsi_9() const { return ____morrairsi_9; }
	inline int32_t* get_address_of__morrairsi_9() { return &____morrairsi_9; }
	inline void set__morrairsi_9(int32_t value)
	{
		____morrairsi_9 = value;
	}

	inline static int32_t get_offset_of__dehem_10() { return static_cast<int32_t>(offsetof(M_rube195_t4048455879, ____dehem_10)); }
	inline String_t* get__dehem_10() const { return ____dehem_10; }
	inline String_t** get_address_of__dehem_10() { return &____dehem_10; }
	inline void set__dehem_10(String_t* value)
	{
		____dehem_10 = value;
		Il2CppCodeGenWriteBarrier(&____dehem_10, value);
	}

	inline static int32_t get_offset_of__chawzetawTexemna_11() { return static_cast<int32_t>(offsetof(M_rube195_t4048455879, ____chawzetawTexemna_11)); }
	inline float get__chawzetawTexemna_11() const { return ____chawzetawTexemna_11; }
	inline float* get_address_of__chawzetawTexemna_11() { return &____chawzetawTexemna_11; }
	inline void set__chawzetawTexemna_11(float value)
	{
		____chawzetawTexemna_11 = value;
	}

	inline static int32_t get_offset_of__qeaza_12() { return static_cast<int32_t>(offsetof(M_rube195_t4048455879, ____qeaza_12)); }
	inline bool get__qeaza_12() const { return ____qeaza_12; }
	inline bool* get_address_of__qeaza_12() { return &____qeaza_12; }
	inline void set__qeaza_12(bool value)
	{
		____qeaza_12 = value;
	}

	inline static int32_t get_offset_of__memairSusaltis_13() { return static_cast<int32_t>(offsetof(M_rube195_t4048455879, ____memairSusaltis_13)); }
	inline String_t* get__memairSusaltis_13() const { return ____memairSusaltis_13; }
	inline String_t** get_address_of__memairSusaltis_13() { return &____memairSusaltis_13; }
	inline void set__memairSusaltis_13(String_t* value)
	{
		____memairSusaltis_13 = value;
		Il2CppCodeGenWriteBarrier(&____memairSusaltis_13, value);
	}

	inline static int32_t get_offset_of__sehorfa_14() { return static_cast<int32_t>(offsetof(M_rube195_t4048455879, ____sehorfa_14)); }
	inline bool get__sehorfa_14() const { return ____sehorfa_14; }
	inline bool* get_address_of__sehorfa_14() { return &____sehorfa_14; }
	inline void set__sehorfa_14(bool value)
	{
		____sehorfa_14 = value;
	}

	inline static int32_t get_offset_of__cheedayhiHearperedem_15() { return static_cast<int32_t>(offsetof(M_rube195_t4048455879, ____cheedayhiHearperedem_15)); }
	inline bool get__cheedayhiHearperedem_15() const { return ____cheedayhiHearperedem_15; }
	inline bool* get_address_of__cheedayhiHearperedem_15() { return &____cheedayhiHearperedem_15; }
	inline void set__cheedayhiHearperedem_15(bool value)
	{
		____cheedayhiHearperedem_15 = value;
	}

	inline static int32_t get_offset_of__tapalDowtai_16() { return static_cast<int32_t>(offsetof(M_rube195_t4048455879, ____tapalDowtai_16)); }
	inline bool get__tapalDowtai_16() const { return ____tapalDowtai_16; }
	inline bool* get_address_of__tapalDowtai_16() { return &____tapalDowtai_16; }
	inline void set__tapalDowtai_16(bool value)
	{
		____tapalDowtai_16 = value;
	}

	inline static int32_t get_offset_of__raidaipooPouji_17() { return static_cast<int32_t>(offsetof(M_rube195_t4048455879, ____raidaipooPouji_17)); }
	inline uint32_t get__raidaipooPouji_17() const { return ____raidaipooPouji_17; }
	inline uint32_t* get_address_of__raidaipooPouji_17() { return &____raidaipooPouji_17; }
	inline void set__raidaipooPouji_17(uint32_t value)
	{
		____raidaipooPouji_17 = value;
	}

	inline static int32_t get_offset_of__gasafair_18() { return static_cast<int32_t>(offsetof(M_rube195_t4048455879, ____gasafair_18)); }
	inline float get__gasafair_18() const { return ____gasafair_18; }
	inline float* get_address_of__gasafair_18() { return &____gasafair_18; }
	inline void set__gasafair_18(float value)
	{
		____gasafair_18 = value;
	}

	inline static int32_t get_offset_of__mekairhay_19() { return static_cast<int32_t>(offsetof(M_rube195_t4048455879, ____mekairhay_19)); }
	inline bool get__mekairhay_19() const { return ____mekairhay_19; }
	inline bool* get_address_of__mekairhay_19() { return &____mekairhay_19; }
	inline void set__mekairhay_19(bool value)
	{
		____mekairhay_19 = value;
	}

	inline static int32_t get_offset_of__dureretrur_20() { return static_cast<int32_t>(offsetof(M_rube195_t4048455879, ____dureretrur_20)); }
	inline int32_t get__dureretrur_20() const { return ____dureretrur_20; }
	inline int32_t* get_address_of__dureretrur_20() { return &____dureretrur_20; }
	inline void set__dureretrur_20(int32_t value)
	{
		____dureretrur_20 = value;
	}

	inline static int32_t get_offset_of__hereniStasterena_21() { return static_cast<int32_t>(offsetof(M_rube195_t4048455879, ____hereniStasterena_21)); }
	inline float get__hereniStasterena_21() const { return ____hereniStasterena_21; }
	inline float* get_address_of__hereniStasterena_21() { return &____hereniStasterena_21; }
	inline void set__hereniStasterena_21(float value)
	{
		____hereniStasterena_21 = value;
	}

	inline static int32_t get_offset_of__mamu_22() { return static_cast<int32_t>(offsetof(M_rube195_t4048455879, ____mamu_22)); }
	inline float get__mamu_22() const { return ____mamu_22; }
	inline float* get_address_of__mamu_22() { return &____mamu_22; }
	inline void set__mamu_22(float value)
	{
		____mamu_22 = value;
	}

	inline static int32_t get_offset_of__dexea_23() { return static_cast<int32_t>(offsetof(M_rube195_t4048455879, ____dexea_23)); }
	inline uint32_t get__dexea_23() const { return ____dexea_23; }
	inline uint32_t* get_address_of__dexea_23() { return &____dexea_23; }
	inline void set__dexea_23(uint32_t value)
	{
		____dexea_23 = value;
	}

	inline static int32_t get_offset_of__newhereKeasanair_24() { return static_cast<int32_t>(offsetof(M_rube195_t4048455879, ____newhereKeasanair_24)); }
	inline int32_t get__newhereKeasanair_24() const { return ____newhereKeasanair_24; }
	inline int32_t* get_address_of__newhereKeasanair_24() { return &____newhereKeasanair_24; }
	inline void set__newhereKeasanair_24(int32_t value)
	{
		____newhereKeasanair_24 = value;
	}

	inline static int32_t get_offset_of__zirasSemhurlar_25() { return static_cast<int32_t>(offsetof(M_rube195_t4048455879, ____zirasSemhurlar_25)); }
	inline float get__zirasSemhurlar_25() const { return ____zirasSemhurlar_25; }
	inline float* get_address_of__zirasSemhurlar_25() { return &____zirasSemhurlar_25; }
	inline void set__zirasSemhurlar_25(float value)
	{
		____zirasSemhurlar_25 = value;
	}

	inline static int32_t get_offset_of__dejaka_26() { return static_cast<int32_t>(offsetof(M_rube195_t4048455879, ____dejaka_26)); }
	inline uint32_t get__dejaka_26() const { return ____dejaka_26; }
	inline uint32_t* get_address_of__dejaka_26() { return &____dejaka_26; }
	inline void set__dejaka_26(uint32_t value)
	{
		____dejaka_26 = value;
	}

	inline static int32_t get_offset_of__jeegoomawDeerallkow_27() { return static_cast<int32_t>(offsetof(M_rube195_t4048455879, ____jeegoomawDeerallkow_27)); }
	inline float get__jeegoomawDeerallkow_27() const { return ____jeegoomawDeerallkow_27; }
	inline float* get_address_of__jeegoomawDeerallkow_27() { return &____jeegoomawDeerallkow_27; }
	inline void set__jeegoomawDeerallkow_27(float value)
	{
		____jeegoomawDeerallkow_27 = value;
	}

	inline static int32_t get_offset_of__nisbaiCherkowsu_28() { return static_cast<int32_t>(offsetof(M_rube195_t4048455879, ____nisbaiCherkowsu_28)); }
	inline float get__nisbaiCherkowsu_28() const { return ____nisbaiCherkowsu_28; }
	inline float* get_address_of__nisbaiCherkowsu_28() { return &____nisbaiCherkowsu_28; }
	inline void set__nisbaiCherkowsu_28(float value)
	{
		____nisbaiCherkowsu_28 = value;
	}

	inline static int32_t get_offset_of__coure_29() { return static_cast<int32_t>(offsetof(M_rube195_t4048455879, ____coure_29)); }
	inline uint32_t get__coure_29() const { return ____coure_29; }
	inline uint32_t* get_address_of__coure_29() { return &____coure_29; }
	inline void set__coure_29(uint32_t value)
	{
		____coure_29 = value;
	}

	inline static int32_t get_offset_of__roudarlaJowporka_30() { return static_cast<int32_t>(offsetof(M_rube195_t4048455879, ____roudarlaJowporka_30)); }
	inline bool get__roudarlaJowporka_30() const { return ____roudarlaJowporka_30; }
	inline bool* get_address_of__roudarlaJowporka_30() { return &____roudarlaJowporka_30; }
	inline void set__roudarlaJowporka_30(bool value)
	{
		____roudarlaJowporka_30 = value;
	}

	inline static int32_t get_offset_of__kurneseSirjurtor_31() { return static_cast<int32_t>(offsetof(M_rube195_t4048455879, ____kurneseSirjurtor_31)); }
	inline String_t* get__kurneseSirjurtor_31() const { return ____kurneseSirjurtor_31; }
	inline String_t** get_address_of__kurneseSirjurtor_31() { return &____kurneseSirjurtor_31; }
	inline void set__kurneseSirjurtor_31(String_t* value)
	{
		____kurneseSirjurtor_31 = value;
		Il2CppCodeGenWriteBarrier(&____kurneseSirjurtor_31, value);
	}

	inline static int32_t get_offset_of__seejo_32() { return static_cast<int32_t>(offsetof(M_rube195_t4048455879, ____seejo_32)); }
	inline String_t* get__seejo_32() const { return ____seejo_32; }
	inline String_t** get_address_of__seejo_32() { return &____seejo_32; }
	inline void set__seejo_32(String_t* value)
	{
		____seejo_32 = value;
		Il2CppCodeGenWriteBarrier(&____seejo_32, value);
	}

	inline static int32_t get_offset_of__celoDosarnas_33() { return static_cast<int32_t>(offsetof(M_rube195_t4048455879, ____celoDosarnas_33)); }
	inline int32_t get__celoDosarnas_33() const { return ____celoDosarnas_33; }
	inline int32_t* get_address_of__celoDosarnas_33() { return &____celoDosarnas_33; }
	inline void set__celoDosarnas_33(int32_t value)
	{
		____celoDosarnas_33 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
