﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_MaterialGenerated
struct UnityEngine_MaterialGenerated_t2864784320;
// JSVCall
struct JSVCall_t3708497963;
// System.String[]
struct StringU5BU5D_t4054002952;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"

// System.Void UnityEngine_MaterialGenerated::.ctor()
extern "C"  void UnityEngine_MaterialGenerated__ctor_m4125533611 (UnityEngine_MaterialGenerated_t2864784320 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MaterialGenerated::Material_Material1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MaterialGenerated_Material_Material1_m3862606919 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MaterialGenerated::Material_Material2(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MaterialGenerated_Material_Material2_m812404104 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_MaterialGenerated::Material_shader(JSVCall)
extern "C"  void UnityEngine_MaterialGenerated_Material_shader_m2223282017 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_MaterialGenerated::Material_color(JSVCall)
extern "C"  void UnityEngine_MaterialGenerated_Material_color_m61950307 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_MaterialGenerated::Material_mainTexture(JSVCall)
extern "C"  void UnityEngine_MaterialGenerated_Material_mainTexture_m2154029700 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_MaterialGenerated::Material_mainTextureOffset(JSVCall)
extern "C"  void UnityEngine_MaterialGenerated_Material_mainTextureOffset_m3872617649 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_MaterialGenerated::Material_mainTextureScale(JSVCall)
extern "C"  void UnityEngine_MaterialGenerated_Material_mainTextureScale_m153910622 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_MaterialGenerated::Material_passCount(JSVCall)
extern "C"  void UnityEngine_MaterialGenerated_Material_passCount_m4180907944 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_MaterialGenerated::Material_renderQueue(JSVCall)
extern "C"  void UnityEngine_MaterialGenerated_Material_renderQueue_m1099860651 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_MaterialGenerated::Material_shaderKeywords(JSVCall)
extern "C"  void UnityEngine_MaterialGenerated_Material_shaderKeywords_m1240738775 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_MaterialGenerated::Material_globalIlluminationFlags(JSVCall)
extern "C"  void UnityEngine_MaterialGenerated_Material_globalIlluminationFlags_m1931740273 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MaterialGenerated::Material_CopyPropertiesFromMaterial__Material(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MaterialGenerated_Material_CopyPropertiesFromMaterial__Material_m2639660613 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MaterialGenerated::Material_DisableKeyword__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MaterialGenerated_Material_DisableKeyword__String_m3372702007 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MaterialGenerated::Material_EnableKeyword__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MaterialGenerated_Material_EnableKeyword__String_m3834788756 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MaterialGenerated::Material_GetColor__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MaterialGenerated_Material_GetColor__Int32_m2696932446 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MaterialGenerated::Material_GetColor__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MaterialGenerated_Material_GetColor__String_m3496614659 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MaterialGenerated::Material_GetFloat__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MaterialGenerated_Material_GetFloat__Int32_m1405560837 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MaterialGenerated::Material_GetFloat__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MaterialGenerated_Material_GetFloat__String_m2118800444 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MaterialGenerated::Material_GetInt__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MaterialGenerated_Material_GetInt__Int32_m1874487250 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MaterialGenerated::Material_GetInt__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MaterialGenerated_Material_GetInt__String_m3770617359 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MaterialGenerated::Material_GetMatrix__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MaterialGenerated_Material_GetMatrix__String_m745705893 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MaterialGenerated::Material_GetMatrix__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MaterialGenerated_Material_GetMatrix__Int32_m945625468 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MaterialGenerated::Material_GetTag__String__Boolean__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MaterialGenerated_Material_GetTag__String__Boolean__String_m2609404993 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MaterialGenerated::Material_GetTag__String__Boolean(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MaterialGenerated_Material_GetTag__String__Boolean_m3652095472 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MaterialGenerated::Material_GetTexture__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MaterialGenerated_Material_GetTexture__Int32_m2916407590 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MaterialGenerated::Material_GetTexture__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MaterialGenerated_Material_GetTexture__String_m1710409531 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MaterialGenerated::Material_GetTextureOffset__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MaterialGenerated_Material_GetTextureOffset__String_m2151896558 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MaterialGenerated::Material_GetTextureScale__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MaterialGenerated_Material_GetTextureScale__String_m1992521939 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MaterialGenerated::Material_GetVector__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MaterialGenerated_Material_GetVector__String_m2119093159 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MaterialGenerated::Material_GetVector__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MaterialGenerated_Material_GetVector__Int32_m4176516922 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MaterialGenerated::Material_HasProperty__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MaterialGenerated_Material_HasProperty__String_m840279165 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MaterialGenerated::Material_HasProperty__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MaterialGenerated_Material_HasProperty__Int32_m2334149540 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MaterialGenerated::Material_IsKeywordEnabled__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MaterialGenerated_Material_IsKeywordEnabled__String_m4163062648 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MaterialGenerated::Material_Lerp__Material__Material__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MaterialGenerated_Material_Lerp__Material__Material__Single_m2697939410 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MaterialGenerated::Material_SetBuffer__String__ComputeBuffer(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MaterialGenerated_Material_SetBuffer__String__ComputeBuffer_m444310377 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MaterialGenerated::Material_SetColor__String__Color(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MaterialGenerated_Material_SetColor__String__Color_m4184155150 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MaterialGenerated::Material_SetColor__Int32__Color(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MaterialGenerated_Material_SetColor__Int32__Color_m3047749819 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MaterialGenerated::Material_SetFloat__String__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MaterialGenerated_Material_SetFloat__String__Single_m104975096 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MaterialGenerated::Material_SetFloat__Int32__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MaterialGenerated_Material_SetFloat__Int32__Single_m4276437081 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MaterialGenerated::Material_SetInt__String__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MaterialGenerated_Material_SetInt__String__Int32_m2567613293 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MaterialGenerated::Material_SetInt__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MaterialGenerated_Material_SetInt__Int32__Int32_m1283173554 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MaterialGenerated::Material_SetMatrix__Int32__Matrix4x4(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MaterialGenerated_Material_SetMatrix__Int32__Matrix4x4_m2995949857 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MaterialGenerated::Material_SetMatrix__String__Matrix4x4(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MaterialGenerated_Material_SetMatrix__String__Matrix4x4_m2849384128 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MaterialGenerated::Material_SetOverrideTag__String__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MaterialGenerated_Material_SetOverrideTag__String__String_m1841755347 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MaterialGenerated::Material_SetPass__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MaterialGenerated_Material_SetPass__Int32_m2985892640 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MaterialGenerated::Material_SetTexture__Int32__Texture(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MaterialGenerated_Material_SetTexture__Int32__Texture_m231532011 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MaterialGenerated::Material_SetTexture__String__Texture(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MaterialGenerated_Material_SetTexture__String__Texture_m3161760014 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MaterialGenerated::Material_SetTextureOffset__String__Vector2(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MaterialGenerated_Material_SetTextureOffset__String__Vector2_m2075444719 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MaterialGenerated::Material_SetTextureScale__String__Vector2(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MaterialGenerated_Material_SetTextureScale__String__Vector2_m1698891026 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MaterialGenerated::Material_SetVector__String__Vector4(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MaterialGenerated_Material_SetVector__String__Vector4_m2378070592 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MaterialGenerated::Material_SetVector__Int32__Vector4(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MaterialGenerated_Material_SetVector__Int32__Vector4_m3344382117 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_MaterialGenerated::__Register()
extern "C"  void UnityEngine_MaterialGenerated___Register_m3806767612 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] UnityEngine_MaterialGenerated::<Material_shaderKeywords>m__262()
extern "C"  StringU5BU5D_t4054002952* UnityEngine_MaterialGenerated_U3CMaterial_shaderKeywordsU3Em__262_m3376490718 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_MaterialGenerated::ilo_addJSCSRel1(System.Int32,System.Object)
extern "C"  void UnityEngine_MaterialGenerated_ilo_addJSCSRel1_m350356199 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_MaterialGenerated::ilo_getObject2(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_MaterialGenerated_ilo_getObject2_m1576543899 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine_MaterialGenerated::ilo_getStringS3(System.Int32)
extern "C"  String_t* UnityEngine_MaterialGenerated_ilo_getStringS3_m3566869713 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_MaterialGenerated::ilo_setObject4(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UnityEngine_MaterialGenerated_ilo_setObject4_m4276742182 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_MaterialGenerated::ilo_setSingle5(System.Int32,System.Single)
extern "C"  void UnityEngine_MaterialGenerated_ilo_setSingle5_m3687877869 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_MaterialGenerated::ilo_setInt326(System.Int32,System.Int32)
extern "C"  void UnityEngine_MaterialGenerated_ilo_setInt326_m473871462 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MaterialGenerated::ilo_getBooleanS7(System.Int32)
extern "C"  bool UnityEngine_MaterialGenerated_ilo_getBooleanS7_m1772451679 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_MaterialGenerated::ilo_setVector2S8(System.Int32,UnityEngine.Vector2)
extern "C"  void UnityEngine_MaterialGenerated_ilo_setVector2S8_m2423783011 (Il2CppObject * __this /* static, unused */, int32_t ___e0, Vector2_t4282066565  ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_MaterialGenerated::ilo_setBooleanS9(System.Int32,System.Boolean)
extern "C"  void UnityEngine_MaterialGenerated_ilo_setBooleanS9_m2451360980 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine_MaterialGenerated::ilo_getSingle10(System.Int32)
extern "C"  float UnityEngine_MaterialGenerated_ilo_getSingle10_m257659856 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_MaterialGenerated::ilo_getInt3211(System.Int32)
extern "C"  int32_t UnityEngine_MaterialGenerated_ilo_getInt3211_m1664198983 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine_MaterialGenerated::ilo_getVector2S12(System.Int32)
extern "C"  Vector2_t4282066565  UnityEngine_MaterialGenerated_ilo_getVector2S12_m1508126077 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
