﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Serialization.LateBoundMetadataTypeAttribute
struct LateBoundMetadataTypeAttribute_t3952996647;
// System.Object
struct Il2CppObject;
// System.Type
struct Type_t;
// System.Reflection.MemberInfo
struct MemberInfo_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Reflection_MemberInfo3995515898.h"

// System.Void Newtonsoft.Json.Serialization.LateBoundMetadataTypeAttribute::.ctor(System.Object)
extern "C"  void LateBoundMetadataTypeAttribute__ctor_m3600630343 (LateBoundMetadataTypeAttribute_t3952996647 * __this, Il2CppObject * ___attribute0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Newtonsoft.Json.Serialization.LateBoundMetadataTypeAttribute::get_MetadataClassType()
extern "C"  Type_t * LateBoundMetadataTypeAttribute_get_MetadataClassType_m2085816811 (LateBoundMetadataTypeAttribute_t3952996647 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Serialization.LateBoundMetadataTypeAttribute::ilo_GetMemberValue1(System.Reflection.MemberInfo,System.Object)
extern "C"  Il2CppObject * LateBoundMetadataTypeAttribute_ilo_GetMemberValue1_m458829141 (Il2CppObject * __this /* static, unused */, MemberInfo_t * ___member0, Il2CppObject * ___target1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
