﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ProtoBuf.IExtension
struct IExtension_t1606339106;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraAniGroup
struct  CameraAniGroup_t1251365992  : public Il2CppObject
{
public:
	// System.Int32 CameraAniGroup::_startID
	int32_t ____startID_0;
	// System.Int32 CameraAniGroup::_eventType
	int32_t ____eventType_1;
	// System.Int32 CameraAniGroup::_round
	int32_t ____round_2;
	// System.Int32 CameraAniGroup::_id
	int32_t ____id_3;
	// ProtoBuf.IExtension CameraAniGroup::extensionObject
	Il2CppObject * ___extensionObject_4;

public:
	inline static int32_t get_offset_of__startID_0() { return static_cast<int32_t>(offsetof(CameraAniGroup_t1251365992, ____startID_0)); }
	inline int32_t get__startID_0() const { return ____startID_0; }
	inline int32_t* get_address_of__startID_0() { return &____startID_0; }
	inline void set__startID_0(int32_t value)
	{
		____startID_0 = value;
	}

	inline static int32_t get_offset_of__eventType_1() { return static_cast<int32_t>(offsetof(CameraAniGroup_t1251365992, ____eventType_1)); }
	inline int32_t get__eventType_1() const { return ____eventType_1; }
	inline int32_t* get_address_of__eventType_1() { return &____eventType_1; }
	inline void set__eventType_1(int32_t value)
	{
		____eventType_1 = value;
	}

	inline static int32_t get_offset_of__round_2() { return static_cast<int32_t>(offsetof(CameraAniGroup_t1251365992, ____round_2)); }
	inline int32_t get__round_2() const { return ____round_2; }
	inline int32_t* get_address_of__round_2() { return &____round_2; }
	inline void set__round_2(int32_t value)
	{
		____round_2 = value;
	}

	inline static int32_t get_offset_of__id_3() { return static_cast<int32_t>(offsetof(CameraAniGroup_t1251365992, ____id_3)); }
	inline int32_t get__id_3() const { return ____id_3; }
	inline int32_t* get_address_of__id_3() { return &____id_3; }
	inline void set__id_3(int32_t value)
	{
		____id_3 = value;
	}

	inline static int32_t get_offset_of_extensionObject_4() { return static_cast<int32_t>(offsetof(CameraAniGroup_t1251365992, ___extensionObject_4)); }
	inline Il2CppObject * get_extensionObject_4() const { return ___extensionObject_4; }
	inline Il2CppObject ** get_address_of_extensionObject_4() { return &___extensionObject_4; }
	inline void set_extensionObject_4(Il2CppObject * value)
	{
		___extensionObject_4 = value;
		Il2CppCodeGenWriteBarrier(&___extensionObject_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
