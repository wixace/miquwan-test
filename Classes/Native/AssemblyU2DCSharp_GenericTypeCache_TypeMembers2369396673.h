﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Reflection.ConstructorInfo[]
struct ConstructorInfoU5BU5D_t2079826215;
// System.Reflection.FieldInfo[]
struct FieldInfoU5BU5D_t2567562023;
// System.Reflection.PropertyInfo[]
struct PropertyInfoU5BU5D_t4286713048;
// System.Reflection.MethodInfo[]
struct MethodInfoU5BU5D_t2824366364;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GenericTypeCache/TypeMembers
struct  TypeMembers_t2369396673  : public Il2CppObject
{
public:
	// System.Reflection.ConstructorInfo[] GenericTypeCache/TypeMembers::cons
	ConstructorInfoU5BU5D_t2079826215* ___cons_0;
	// System.Reflection.FieldInfo[] GenericTypeCache/TypeMembers::fields
	FieldInfoU5BU5D_t2567562023* ___fields_1;
	// System.Reflection.PropertyInfo[] GenericTypeCache/TypeMembers::properties
	PropertyInfoU5BU5D_t4286713048* ___properties_2;
	// System.Reflection.MethodInfo[] GenericTypeCache/TypeMembers::methods
	MethodInfoU5BU5D_t2824366364* ___methods_3;

public:
	inline static int32_t get_offset_of_cons_0() { return static_cast<int32_t>(offsetof(TypeMembers_t2369396673, ___cons_0)); }
	inline ConstructorInfoU5BU5D_t2079826215* get_cons_0() const { return ___cons_0; }
	inline ConstructorInfoU5BU5D_t2079826215** get_address_of_cons_0() { return &___cons_0; }
	inline void set_cons_0(ConstructorInfoU5BU5D_t2079826215* value)
	{
		___cons_0 = value;
		Il2CppCodeGenWriteBarrier(&___cons_0, value);
	}

	inline static int32_t get_offset_of_fields_1() { return static_cast<int32_t>(offsetof(TypeMembers_t2369396673, ___fields_1)); }
	inline FieldInfoU5BU5D_t2567562023* get_fields_1() const { return ___fields_1; }
	inline FieldInfoU5BU5D_t2567562023** get_address_of_fields_1() { return &___fields_1; }
	inline void set_fields_1(FieldInfoU5BU5D_t2567562023* value)
	{
		___fields_1 = value;
		Il2CppCodeGenWriteBarrier(&___fields_1, value);
	}

	inline static int32_t get_offset_of_properties_2() { return static_cast<int32_t>(offsetof(TypeMembers_t2369396673, ___properties_2)); }
	inline PropertyInfoU5BU5D_t4286713048* get_properties_2() const { return ___properties_2; }
	inline PropertyInfoU5BU5D_t4286713048** get_address_of_properties_2() { return &___properties_2; }
	inline void set_properties_2(PropertyInfoU5BU5D_t4286713048* value)
	{
		___properties_2 = value;
		Il2CppCodeGenWriteBarrier(&___properties_2, value);
	}

	inline static int32_t get_offset_of_methods_3() { return static_cast<int32_t>(offsetof(TypeMembers_t2369396673, ___methods_3)); }
	inline MethodInfoU5BU5D_t2824366364* get_methods_3() const { return ___methods_3; }
	inline MethodInfoU5BU5D_t2824366364** get_address_of_methods_3() { return &___methods_3; }
	inline void set_methods_3(MethodInfoU5BU5D_t2824366364* value)
	{
		___methods_3 = value;
		Il2CppCodeGenWriteBarrier(&___methods_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
