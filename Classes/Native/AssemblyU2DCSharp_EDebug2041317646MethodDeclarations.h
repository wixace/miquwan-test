﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EDebug
struct EDebug_t2041317646;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t3991598821;
// LogWriter
struct LogWriter_t4068659383;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Exception3991598821.h"
#include "AssemblyU2DCSharp_LogLevel2060375744.h"
#include "UnityEngine_UnityEngine_LogType4286006228.h"
#include "AssemblyU2DCSharp_LogWriter4068659383.h"

// System.Void EDebug::.ctor()
extern "C"  void EDebug__ctor_m3843664717 (EDebug_t2041317646 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EDebug::.cctor()
extern "C"  void EDebug__cctor_m2707393024 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EDebug::Release()
extern "C"  void EDebug_Release_m4121894962 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EDebug::UploadLogFile(System.String,System.String)
extern "C"  void EDebug_UploadLogFile_m2008162612 (Il2CppObject * __this /* static, unused */, String_t* ___date0, String_t* ___toAddress1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EDebug::Debug(System.Object,System.Boolean,System.Int32)
extern "C"  void EDebug_Debug_m3156635600 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, int32_t ___user2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EDebug::Debug(System.String,System.Object,System.Boolean)
extern "C"  void EDebug_Debug_m3820383147 (Il2CppObject * __this /* static, unused */, String_t* ___filter0, Il2CppObject * ___message1, bool ___isShowStack2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EDebug::Log(System.Object,System.Boolean)
extern "C"  void EDebug_Log_m256897048 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EDebug::LogWarning(System.Object,System.Boolean)
extern "C"  void EDebug_LogWarning_m777816184 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EDebug::LogError(System.Object,System.Boolean)
extern "C"  void EDebug_LogError_m875838052 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EDebug::Critical(System.Object,System.Boolean)
extern "C"  void EDebug_Critical_m1471397791 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EDebug::Except(System.Exception,System.Object)
extern "C"  void EDebug_Except_m280714542 (Il2CppObject * __this /* static, unused */, Exception_t3991598821 * ___ex0, Il2CppObject * ___message1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String EDebug::GetStacksInfo()
extern "C"  String_t* EDebug_GetStacksInfo_m3105716055 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EDebug::Log(System.String,LogLevel,System.Boolean)
extern "C"  void EDebug_Log_m3895160618 (Il2CppObject * __this /* static, unused */, String_t* ___message0, int32_t ___level1, bool ___writeEditorLog2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String EDebug::GetStackInfo()
extern "C"  String_t* EDebug_GetStackInfo_m1564097754 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EDebug::ProcessExceptionReport(System.String,System.String,UnityEngine.LogType)
extern "C"  void EDebug_ProcessExceptionReport_m653429144 (Il2CppObject * __this /* static, unused */, String_t* ___message0, String_t* ___stackTrace1, int32_t ___type2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EDebug::ilo_Release1(LogWriter)
extern "C"  void EDebug_ilo_Release1_m1543876487 (Il2CppObject * __this /* static, unused */, LogWriter_t4068659383 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String EDebug::ilo_GetStackInfo2()
extern "C"  String_t* EDebug_ilo_GetStackInfo2_m3279568359 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EDebug::ilo_Log3(System.String,LogLevel,System.Boolean)
extern "C"  void EDebug_ilo_Log3_m2491072416 (Il2CppObject * __this /* static, unused */, String_t* ___message0, int32_t ___level1, bool ___writeEditorLog2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
