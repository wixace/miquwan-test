﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.Serialization.AstarSerializer/<DeserializeExtraInfo>c__AnonStorey110
struct U3CDeserializeExtraInfoU3Ec__AnonStorey110_t3805251454;
// Pathfinding.GraphNode
struct GraphNode_t23612370;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphNode23612370.h"

// System.Void Pathfinding.Serialization.AstarSerializer/<DeserializeExtraInfo>c__AnonStorey110::.ctor()
extern "C"  void U3CDeserializeExtraInfoU3Ec__AnonStorey110__ctor_m3677777117 (U3CDeserializeExtraInfoU3Ec__AnonStorey110_t3805251454 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.Serialization.AstarSerializer/<DeserializeExtraInfo>c__AnonStorey110::<>m__343(Pathfinding.GraphNode)
extern "C"  bool U3CDeserializeExtraInfoU3Ec__AnonStorey110_U3CU3Em__343_m1859179466 (U3CDeserializeExtraInfoU3Ec__AnonStorey110_t3805251454 * __this, GraphNode_t23612370 * ___node0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
