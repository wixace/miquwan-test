﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PluginFengqi
struct PluginFengqi_t1486316547;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// System.String
struct String_t;
// UnityEngine.WWWForm
struct WWWForm_t461342257;
// Mihua.SDK.PayInfo
struct PayInfo_t1775308120;
// VersionInfo
struct VersionInfo_t2356638086;
// VersionMgr
struct VersionMgr_t1322950208;
// PluginsSdkMgr
struct PluginsSdkMgr_t3884624670;
// FloatTextMgr
struct FloatTextMgr_t630384591;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// Mihua.SDK.SdkwwwCallBack
struct SdkwwwCallBack_t3004559384;
// Newtonsoft.Json.Linq.JToken
struct JToken_t3412245951;
// Newtonsoft.Json.Linq.JObject
struct JObject_t1798544199;
// ProductsCfgMgr
struct ProductsCfgMgr_t2493714872;
// System.Object
struct Il2CppObject;
// System.Action
struct Action_t3771233898;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_Mihua_SDK_PayInfo1775308120.h"
#include "AssemblyU2DCSharp_VersionMgr1322950208.h"
#include "AssemblyU2DCSharp_PluginFengqi1486316547.h"
#include "AssemblyU2DCSharp_PluginsSdkMgr3884624670.h"
#include "AssemblyU2DCSharp_FloatTextMgr630384591.h"
#include "AssemblyU2DCSharp_FLOAT_TEXT_ID2199345770.h"
#include "UnityEngine_UnityEngine_WWWForm461342257.h"
#include "AssemblyU2DCSharp_Mihua_SDK_SdkwwwCallBack3004559384.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JObject1798544199.h"
#include "AssemblyU2DCSharp_ProductsCfgMgr2493714872.h"
#include "mscorlib_System_Object4170816371.h"
#include "System_Core_System_Action3771233898.h"

// System.Void PluginFengqi::.ctor()
extern "C"  void PluginFengqi__ctor_m1981637112 (PluginFengqi_t1486316547 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginFengqi::Init()
extern "C"  void PluginFengqi_Init_m1400933724 (PluginFengqi_t1486316547 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginFengqi::ReqSDKHttpLogin()
extern "C"  Dictionary_2_t827649927 * PluginFengqi_ReqSDKHttpLogin_m2936407117 (PluginFengqi_t1486316547 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginFengqi::IsLoginSuccess(System.Boolean)
extern "C"  bool PluginFengqi_IsLoginSuccess_m607020635 (PluginFengqi_t1486316547 * __this, bool ___isOpenUseLogin0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginFengqi::OpenUserLogin()
extern "C"  void PluginFengqi_OpenUserLogin_m3543206986 (PluginFengqi_t1486316547 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginFengqi::UserPay(CEvent.ZEvent)
extern "C"  void PluginFengqi_UserPay_m3460562920 (PluginFengqi_t1486316547 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginFengqi::SignCallBack(System.Boolean,System.String)
extern "C"  void PluginFengqi_SignCallBack_m2576364161 (PluginFengqi_t1486316547 * __this, bool ___success0, String_t* ___arg1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.WWWForm PluginFengqi::BuildOrderParam2WWWForm(Mihua.SDK.PayInfo)
extern "C"  WWWForm_t461342257 * PluginFengqi_BuildOrderParam2WWWForm_m3227059049 (PluginFengqi_t1486316547 * __this, PayInfo_t1775308120 * ___info0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginFengqi::OnLoginSuccess(System.String)
extern "C"  void PluginFengqi_OnLoginSuccess_m2853768957 (PluginFengqi_t1486316547 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginFengqi::OnLogoutSuccess(System.String)
extern "C"  void PluginFengqi_OnLogoutSuccess_m2571034418 (PluginFengqi_t1486316547 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginFengqi::initSdk(System.String,System.String)
extern "C"  void PluginFengqi_initSdk_m420260670 (PluginFengqi_t1486316547 * __this, String_t* ___gameID0, String_t* ___gameKey1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginFengqi::login()
extern "C"  void PluginFengqi_login_m1503651935 (PluginFengqi_t1486316547 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginFengqi::logout()
extern "C"  void PluginFengqi_logout_m3669359734 (PluginFengqi_t1486316547 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginFengqi::pay(System.String,System.String,System.Single,System.String,System.String,System.String)
extern "C"  void PluginFengqi_pay_m2814010425 (PluginFengqi_t1486316547 * __this, String_t* ___ProductId0, String_t* ___productname1, float ___amount2, String_t* ___serverid3, String_t* ___orderid4, String_t* ___extra5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginFengqi::<OnLogoutSuccess>m__41C()
extern "C"  void PluginFengqi_U3COnLogoutSuccessU3Em__41C_m2501120847 (PluginFengqi_t1486316547 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionInfo PluginFengqi::ilo_get_currentVS1(VersionMgr)
extern "C"  VersionInfo_t2356638086 * PluginFengqi_ilo_get_currentVS1_m3171686960 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginFengqi::ilo_initSdk2(PluginFengqi,System.String,System.String)
extern "C"  void PluginFengqi_ilo_initSdk2_m1725872860 (Il2CppObject * __this /* static, unused */, PluginFengqi_t1486316547 * ____this0, String_t* ___gameID1, String_t* ___gameKey2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginFengqi::ilo_get_isSdkLogin3(VersionMgr)
extern "C"  bool PluginFengqi_ilo_get_isSdkLogin3_m882203520 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PluginsSdkMgr PluginFengqi::ilo_get_PluginsSdkMgr4()
extern "C"  PluginsSdkMgr_t3884624670 * PluginFengqi_ilo_get_PluginsSdkMgr4_m2123989235 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PluginFengqi::ilo_get_DeviceID5(PluginsSdkMgr)
extern "C"  String_t* PluginFengqi_ilo_get_DeviceID5_m3072639737 (Il2CppObject * __this /* static, unused */, PluginsSdkMgr_t3884624670 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionMgr PluginFengqi::ilo_get_Instance6()
extern "C"  VersionMgr_t1322950208 * PluginFengqi_ilo_get_Instance6_m3082429950 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginFengqi::ilo_login7(PluginFengqi)
extern "C"  void PluginFengqi_ilo_login7_m1857474538 (Il2CppObject * __this /* static, unused */, PluginFengqi_t1486316547 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginFengqi::ilo_FloatText8(FloatTextMgr,FLOAT_TEXT_ID,System.Int32,System.Object[])
extern "C"  void PluginFengqi_ilo_FloatText8_m847221410 (Il2CppObject * __this /* static, unused */, FloatTextMgr_t630384591 * ____this0, int32_t ___textId1, int32_t ___offsetY2, ObjectU5BU5D_t1108656482* ___args3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mihua.SDK.PayInfo PluginFengqi::ilo_Parse9(System.Object[])
extern "C"  PayInfo_t1775308120 * PluginFengqi_ilo_Parse9_m3088023238 (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t1108656482* ___payOjs0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginFengqi::ilo_Request10(System.String,UnityEngine.WWWForm,Mihua.SDK.SdkwwwCallBack)
extern "C"  void PluginFengqi_ilo_Request10_m3651601953 (Il2CppObject * __this /* static, unused */, String_t* ___url0, WWWForm_t461342257 * ___wwwForm1, SdkwwwCallBack_t3004559384 * ___callBack2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken PluginFengqi::ilo_get_Item11(Newtonsoft.Json.Linq.JObject,System.String)
extern "C"  JToken_t3412245951 * PluginFengqi_ilo_get_Item11_m3045044018 (Il2CppObject * __this /* static, unused */, JObject_t1798544199 * ____this0, String_t* ___propertyName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PluginFengqi::ilo_GetProductID12(ProductsCfgMgr,System.String)
extern "C"  String_t* PluginFengqi_ilo_GetProductID12_m491284699 (Il2CppObject * __this /* static, unused */, ProductsCfgMgr_t2493714872 * ____this0, String_t* ___name1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginFengqi::ilo_Log13(System.Object,System.Boolean)
extern "C"  void PluginFengqi_ilo_Log13_m3103657426 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FloatTextMgr PluginFengqi::ilo_get_FloatTextMgr14()
extern "C"  FloatTextMgr_t630384591 * PluginFengqi_ilo_get_FloatTextMgr14_m192485224 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginFengqi::ilo_ReqSDKHttpLogin15(PluginsSdkMgr)
extern "C"  void PluginFengqi_ilo_ReqSDKHttpLogin15_m679200456 (Il2CppObject * __this /* static, unused */, PluginsSdkMgr_t3884624670 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginFengqi::ilo_Logout16(System.Action)
extern "C"  void PluginFengqi_ilo_Logout16_m1986335503 (Il2CppObject * __this /* static, unused */, Action_t3771233898 * ___call0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
