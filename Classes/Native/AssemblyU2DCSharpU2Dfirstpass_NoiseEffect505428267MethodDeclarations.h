﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// NoiseEffect
struct NoiseEffect_t505428267;
// UnityEngine.Material
struct Material_t3870600107;
// UnityEngine.RenderTexture
struct RenderTexture_t1963041563;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture1963041563.h"

// System.Void NoiseEffect::.ctor()
extern "C"  void NoiseEffect__ctor_m2631375724 (NoiseEffect_t505428267 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NoiseEffect::Start()
extern "C"  void NoiseEffect_Start_m1578513516 (NoiseEffect_t505428267 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material NoiseEffect::get_material()
extern "C"  Material_t3870600107 * NoiseEffect_get_material_m738060097 (NoiseEffect_t505428267 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NoiseEffect::OnDisable()
extern "C"  void NoiseEffect_OnDisable_m4119550419 (NoiseEffect_t505428267 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NoiseEffect::SanitizeParameters()
extern "C"  void NoiseEffect_SanitizeParameters_m4257769099 (NoiseEffect_t505428267 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NoiseEffect::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void NoiseEffect_OnRenderImage_m4051324274 (NoiseEffect_t505428267 * __this, RenderTexture_t1963041563 * ___source0, RenderTexture_t1963041563 * ___destination1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
