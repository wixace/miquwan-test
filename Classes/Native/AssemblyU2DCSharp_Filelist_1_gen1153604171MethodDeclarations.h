﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Filelist`1<System.Object>
struct Filelist_1_t1153604171;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_String7231557.h"

// System.Void Filelist`1<System.Object>::.ctor()
extern "C"  void Filelist_1__ctor_m1745091098_gshared (Filelist_1_t1153604171 * __this, const MethodInfo* method);
#define Filelist_1__ctor_m1745091098(__this, method) ((  void (*) (Filelist_1_t1153604171 *, const MethodInfo*))Filelist_1__ctor_m1745091098_gshared)(__this, method)
// Filelist`1<T> Filelist`1<System.Object>::CreatFromBytes(System.Byte[])
extern "C"  Filelist_1_t1153604171 * Filelist_1_CreatFromBytes_m2160290303_gshared (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___bytes0, const MethodInfo* method);
#define Filelist_1_CreatFromBytes_m2160290303(__this /* static, unused */, ___bytes0, method) ((  Filelist_1_t1153604171 * (*) (Il2CppObject * /* static, unused */, ByteU5BU5D_t4260760469*, const MethodInfo*))Filelist_1_CreatFromBytes_m2160290303_gshared)(__this /* static, unused */, ___bytes0, method)
// System.Void Filelist`1<System.Object>::AddResData(T)
extern "C"  void Filelist_1_AddResData_m934074443_gshared (Filelist_1_t1153604171 * __this, Il2CppObject * ___data0, const MethodInfo* method);
#define Filelist_1_AddResData_m934074443(__this, ___data0, method) ((  void (*) (Filelist_1_t1153604171 *, Il2CppObject *, const MethodInfo*))Filelist_1_AddResData_m934074443_gshared)(__this, ___data0, method)
// System.Void Filelist`1<System.Object>::AddOrReplaceResData(T)
extern "C"  void Filelist_1_AddOrReplaceResData_m1397803148_gshared (Filelist_1_t1153604171 * __this, Il2CppObject * ___data0, const MethodInfo* method);
#define Filelist_1_AddOrReplaceResData_m1397803148(__this, ___data0, method) ((  void (*) (Filelist_1_t1153604171 *, Il2CppObject *, const MethodInfo*))Filelist_1_AddOrReplaceResData_m1397803148_gshared)(__this, ___data0, method)
// System.Void Filelist`1<System.Object>::ReadBytes(System.Byte[])
extern "C"  void Filelist_1_ReadBytes_m528302588_gshared (Filelist_1_t1153604171 * __this, ByteU5BU5D_t4260760469* ___bytes0, const MethodInfo* method);
#define Filelist_1_ReadBytes_m528302588(__this, ___bytes0, method) ((  void (*) (Filelist_1_t1153604171 *, ByteU5BU5D_t4260760469*, const MethodInfo*))Filelist_1_ReadBytes_m528302588_gshared)(__this, ___bytes0, method)
// System.Void Filelist`1<System.Object>::RemoveResData(System.String)
extern "C"  void Filelist_1_RemoveResData_m2869673412_gshared (Filelist_1_t1153604171 * __this, String_t* ___dataPath0, const MethodInfo* method);
#define Filelist_1_RemoveResData_m2869673412(__this, ___dataPath0, method) ((  void (*) (Filelist_1_t1153604171 *, String_t*, const MethodInfo*))Filelist_1_RemoveResData_m2869673412_gshared)(__this, ___dataPath0, method)
// System.Void Filelist`1<System.Object>::RemoveResData(T)
extern "C"  void Filelist_1_RemoveResData_m1306572576_gshared (Filelist_1_t1153604171 * __this, Il2CppObject * ___data0, const MethodInfo* method);
#define Filelist_1_RemoveResData_m1306572576(__this, ___data0, method) ((  void (*) (Filelist_1_t1153604171 *, Il2CppObject *, const MethodInfo*))Filelist_1_RemoveResData_m1306572576_gshared)(__this, ___data0, method)
// T Filelist`1<System.Object>::GetResData(System.String)
extern "C"  Il2CppObject * Filelist_1_GetResData_m1855791459_gshared (Filelist_1_t1153604171 * __this, String_t* ___fileName0, const MethodInfo* method);
#define Filelist_1_GetResData_m1855791459(__this, ___fileName0, method) ((  Il2CppObject * (*) (Filelist_1_t1153604171 *, String_t*, const MethodInfo*))Filelist_1_GetResData_m1855791459_gshared)(__this, ___fileName0, method)
// System.String Filelist`1<System.Object>::ToString()
extern "C"  String_t* Filelist_1_ToString_m1484309139_gshared (Filelist_1_t1153604171 * __this, const MethodInfo* method);
#define Filelist_1_ToString_m1484309139(__this, method) ((  String_t* (*) (Filelist_1_t1153604171 *, const MethodInfo*))Filelist_1_ToString_m1484309139_gshared)(__this, method)
