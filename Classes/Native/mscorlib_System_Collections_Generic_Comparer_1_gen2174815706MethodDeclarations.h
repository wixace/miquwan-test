﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1<Pathfinding.Voxels.ExtraMesh>
struct Comparer_1_t2174815706;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System.Collections.Generic.Comparer`1<Pathfinding.Voxels.ExtraMesh>::.ctor()
extern "C"  void Comparer_1__ctor_m1425397227_gshared (Comparer_1_t2174815706 * __this, const MethodInfo* method);
#define Comparer_1__ctor_m1425397227(__this, method) ((  void (*) (Comparer_1_t2174815706 *, const MethodInfo*))Comparer_1__ctor_m1425397227_gshared)(__this, method)
// System.Void System.Collections.Generic.Comparer`1<Pathfinding.Voxels.ExtraMesh>::.cctor()
extern "C"  void Comparer_1__cctor_m755544866_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1__cctor_m755544866(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Comparer_1__cctor_m755544866_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.Comparer`1<Pathfinding.Voxels.ExtraMesh>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern "C"  int32_t Comparer_1_System_Collections_IComparer_Compare_m4131316816_gshared (Comparer_1_t2174815706 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method);
#define Comparer_1_System_Collections_IComparer_Compare_m4131316816(__this, ___x0, ___y1, method) ((  int32_t (*) (Comparer_1_t2174815706 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Comparer_1_System_Collections_IComparer_Compare_m4131316816_gshared)(__this, ___x0, ___y1, method)
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<Pathfinding.Voxels.ExtraMesh>::get_Default()
extern "C"  Comparer_1_t2174815706 * Comparer_1_get_Default_m483657683_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1_get_Default_m483657683(__this /* static, unused */, method) ((  Comparer_1_t2174815706 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Comparer_1_get_Default_m483657683_gshared)(__this /* static, unused */, method)
