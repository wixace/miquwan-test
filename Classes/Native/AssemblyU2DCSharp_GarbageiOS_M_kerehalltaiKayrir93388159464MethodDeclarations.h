﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_kerehalltaiKayrir93
struct M_kerehalltaiKayrir93_t388159464;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"

// System.Void GarbageiOS.M_kerehalltaiKayrir93::.ctor()
extern "C"  void M_kerehalltaiKayrir93__ctor_m2960945275 (M_kerehalltaiKayrir93_t388159464 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_kerehalltaiKayrir93::M_borjer0(System.String[],System.Int32)
extern "C"  void M_kerehalltaiKayrir93_M_borjer0_m979303996 (M_kerehalltaiKayrir93_t388159464 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
