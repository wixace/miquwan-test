﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CombatEntityGenerated/<CombatEntity_UpdateTargetPos_GetDelegate_member89_arg1>c__AnonStorey59
struct U3CCombatEntity_UpdateTargetPos_GetDelegate_member89_arg1U3Ec__AnonStorey59_t1712882848;

#include "codegen/il2cpp-codegen.h"

// System.Void CombatEntityGenerated/<CombatEntity_UpdateTargetPos_GetDelegate_member89_arg1>c__AnonStorey59::.ctor()
extern "C"  void U3CCombatEntity_UpdateTargetPos_GetDelegate_member89_arg1U3Ec__AnonStorey59__ctor_m3227713739 (U3CCombatEntity_UpdateTargetPos_GetDelegate_member89_arg1U3Ec__AnonStorey59_t1712882848 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated/<CombatEntity_UpdateTargetPos_GetDelegate_member89_arg1>c__AnonStorey59::<>m__38()
extern "C"  void U3CCombatEntity_UpdateTargetPos_GetDelegate_member89_arg1U3Ec__AnonStorey59_U3CU3Em__38_m3174622713 (U3CCombatEntity_UpdateTargetPos_GetDelegate_member89_arg1U3Ec__AnonStorey59_t1712882848 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
