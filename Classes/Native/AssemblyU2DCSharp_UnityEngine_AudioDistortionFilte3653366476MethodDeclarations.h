﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_AudioDistortionFilterGenerated
struct UnityEngine_AudioDistortionFilterGenerated_t3653366476;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void UnityEngine_AudioDistortionFilterGenerated::.ctor()
extern "C"  void UnityEngine_AudioDistortionFilterGenerated__ctor_m3331959631 (UnityEngine_AudioDistortionFilterGenerated_t3653366476 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AudioDistortionFilterGenerated::AudioDistortionFilter_AudioDistortionFilter1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AudioDistortionFilterGenerated_AudioDistortionFilter_AudioDistortionFilter1_m2066450931 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioDistortionFilterGenerated::AudioDistortionFilter_distortionLevel(JSVCall)
extern "C"  void UnityEngine_AudioDistortionFilterGenerated_AudioDistortionFilter_distortionLevel_m4216815335 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioDistortionFilterGenerated::__Register()
extern "C"  void UnityEngine_AudioDistortionFilterGenerated___Register_m707365592 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioDistortionFilterGenerated::ilo_addJSCSRel1(System.Int32,System.Object)
extern "C"  void UnityEngine_AudioDistortionFilterGenerated_ilo_addJSCSRel1_m2797209227 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
