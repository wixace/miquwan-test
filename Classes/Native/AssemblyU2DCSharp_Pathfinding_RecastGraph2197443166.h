﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<System.String>
struct List_1_t1375417109;
// Pathfinding.Voxels.Voxelize
struct Voxelize_t3998270866;
// Pathfinding.BBTree
struct BBTree_t1216325332;
// Pathfinding.Int3[]
struct Int3U5BU5D_t516284607;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t215400611;
// Pathfinding.RecastGraph/NavmeshTile[]
struct NavmeshTileU5BU5D_t3225070876;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t2522024052;
// System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Int32>
struct Dictionary_2_t2245318082;
// System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Int32>
struct Dictionary_2_t2731505821;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// System.Collections.Generic.List`1<Pathfinding.RecastGraph/CapsuleCache>
struct List_1_t3400845858;

#include "AssemblyU2DCSharp_Pathfinding_NavGraph1254319713.h"
#include "AssemblyU2DCSharp_Pathfinding_RecastGraph_Relevant3667774716.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_LayerMask3236759763.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.RecastGraph
struct  RecastGraph_t2197443166  : public NavGraph_t1254319713
{
public:
	// System.Boolean Pathfinding.RecastGraph::dynamic
	bool ___dynamic_16;
	// System.Single Pathfinding.RecastGraph::characterRadius
	float ___characterRadius_17;
	// System.Single Pathfinding.RecastGraph::contourMaxError
	float ___contourMaxError_18;
	// System.Single Pathfinding.RecastGraph::cellSize
	float ___cellSize_19;
	// System.Single Pathfinding.RecastGraph::cellHeight
	float ___cellHeight_20;
	// System.Single Pathfinding.RecastGraph::walkableHeight
	float ___walkableHeight_21;
	// System.Single Pathfinding.RecastGraph::walkableClimb
	float ___walkableClimb_22;
	// System.Single Pathfinding.RecastGraph::maxSlope
	float ___maxSlope_23;
	// System.Single Pathfinding.RecastGraph::maxEdgeLength
	float ___maxEdgeLength_24;
	// System.Single Pathfinding.RecastGraph::minRegionSize
	float ___minRegionSize_25;
	// System.Int32 Pathfinding.RecastGraph::editorTileSize
	int32_t ___editorTileSize_26;
	// System.Int32 Pathfinding.RecastGraph::tileSizeX
	int32_t ___tileSizeX_27;
	// System.Int32 Pathfinding.RecastGraph::tileSizeZ
	int32_t ___tileSizeZ_28;
	// System.Boolean Pathfinding.RecastGraph::nearestSearchOnlyXZ
	bool ___nearestSearchOnlyXZ_29;
	// System.Boolean Pathfinding.RecastGraph::useTiles
	bool ___useTiles_30;
	// System.Boolean Pathfinding.RecastGraph::scanEmptyGraph
	bool ___scanEmptyGraph_31;
	// Pathfinding.RecastGraph/RelevantGraphSurfaceMode Pathfinding.RecastGraph::relevantGraphSurfaceMode
	int32_t ___relevantGraphSurfaceMode_32;
	// System.Boolean Pathfinding.RecastGraph::rasterizeColliders
	bool ___rasterizeColliders_33;
	// System.Boolean Pathfinding.RecastGraph::rasterizeMeshes
	bool ___rasterizeMeshes_34;
	// System.Boolean Pathfinding.RecastGraph::rasterizeTerrain
	bool ___rasterizeTerrain_35;
	// System.Boolean Pathfinding.RecastGraph::rasterizeTrees
	bool ___rasterizeTrees_36;
	// System.Single Pathfinding.RecastGraph::colliderRasterizeDetail
	float ___colliderRasterizeDetail_37;
	// UnityEngine.Vector3 Pathfinding.RecastGraph::forcedBoundsCenter
	Vector3_t4282066566  ___forcedBoundsCenter_38;
	// UnityEngine.Vector3 Pathfinding.RecastGraph::forcedBoundsSize
	Vector3_t4282066566  ___forcedBoundsSize_39;
	// UnityEngine.LayerMask Pathfinding.RecastGraph::mask
	LayerMask_t3236759763  ___mask_40;
	// System.Collections.Generic.List`1<System.String> Pathfinding.RecastGraph::tagMask
	List_1_t1375417109 * ___tagMask_41;
	// System.Boolean Pathfinding.RecastGraph::showMeshOutline
	bool ___showMeshOutline_42;
	// System.Boolean Pathfinding.RecastGraph::showNodeConnections
	bool ___showNodeConnections_43;
	// System.Int32 Pathfinding.RecastGraph::terrainSampleSize
	int32_t ___terrainSampleSize_44;
	// Pathfinding.Voxels.Voxelize Pathfinding.RecastGraph::globalVox
	Voxelize_t3998270866 * ___globalVox_45;
	// Pathfinding.BBTree Pathfinding.RecastGraph::_bbTree
	BBTree_t1216325332 * ____bbTree_46;
	// Pathfinding.Int3[] Pathfinding.RecastGraph::_vertices
	Int3U5BU5D_t516284607* ____vertices_47;
	// UnityEngine.Vector3[] Pathfinding.RecastGraph::_vectorVertices
	Vector3U5BU5D_t215400611* ____vectorVertices_48;
	// System.Int32 Pathfinding.RecastGraph::tileXCount
	int32_t ___tileXCount_49;
	// System.Int32 Pathfinding.RecastGraph::tileZCount
	int32_t ___tileZCount_50;
	// Pathfinding.RecastGraph/NavmeshTile[] Pathfinding.RecastGraph::tiles
	NavmeshTileU5BU5D_t3225070876* ___tiles_51;
	// System.Boolean Pathfinding.RecastGraph::batchTileUpdate
	bool ___batchTileUpdate_52;
	// System.Collections.Generic.List`1<System.Int32> Pathfinding.RecastGraph::batchUpdatedTiles
	List_1_t2522024052 * ___batchUpdatedTiles_53;
	// System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Int32> Pathfinding.RecastGraph::cachedInt2_int_dict
	Dictionary_2_t2245318082 * ___cachedInt2_int_dict_54;
	// System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Int32> Pathfinding.RecastGraph::cachedInt3_int_dict
	Dictionary_2_t2731505821 * ___cachedInt3_int_dict_55;
	// System.Int32[] Pathfinding.RecastGraph::BoxColliderTris
	Int32U5BU5D_t3230847821* ___BoxColliderTris_56;
	// UnityEngine.Vector3[] Pathfinding.RecastGraph::BoxColliderVerts
	Vector3U5BU5D_t215400611* ___BoxColliderVerts_57;
	// System.Collections.Generic.List`1<Pathfinding.RecastGraph/CapsuleCache> Pathfinding.RecastGraph::capsuleCache
	List_1_t3400845858 * ___capsuleCache_58;

public:
	inline static int32_t get_offset_of_dynamic_16() { return static_cast<int32_t>(offsetof(RecastGraph_t2197443166, ___dynamic_16)); }
	inline bool get_dynamic_16() const { return ___dynamic_16; }
	inline bool* get_address_of_dynamic_16() { return &___dynamic_16; }
	inline void set_dynamic_16(bool value)
	{
		___dynamic_16 = value;
	}

	inline static int32_t get_offset_of_characterRadius_17() { return static_cast<int32_t>(offsetof(RecastGraph_t2197443166, ___characterRadius_17)); }
	inline float get_characterRadius_17() const { return ___characterRadius_17; }
	inline float* get_address_of_characterRadius_17() { return &___characterRadius_17; }
	inline void set_characterRadius_17(float value)
	{
		___characterRadius_17 = value;
	}

	inline static int32_t get_offset_of_contourMaxError_18() { return static_cast<int32_t>(offsetof(RecastGraph_t2197443166, ___contourMaxError_18)); }
	inline float get_contourMaxError_18() const { return ___contourMaxError_18; }
	inline float* get_address_of_contourMaxError_18() { return &___contourMaxError_18; }
	inline void set_contourMaxError_18(float value)
	{
		___contourMaxError_18 = value;
	}

	inline static int32_t get_offset_of_cellSize_19() { return static_cast<int32_t>(offsetof(RecastGraph_t2197443166, ___cellSize_19)); }
	inline float get_cellSize_19() const { return ___cellSize_19; }
	inline float* get_address_of_cellSize_19() { return &___cellSize_19; }
	inline void set_cellSize_19(float value)
	{
		___cellSize_19 = value;
	}

	inline static int32_t get_offset_of_cellHeight_20() { return static_cast<int32_t>(offsetof(RecastGraph_t2197443166, ___cellHeight_20)); }
	inline float get_cellHeight_20() const { return ___cellHeight_20; }
	inline float* get_address_of_cellHeight_20() { return &___cellHeight_20; }
	inline void set_cellHeight_20(float value)
	{
		___cellHeight_20 = value;
	}

	inline static int32_t get_offset_of_walkableHeight_21() { return static_cast<int32_t>(offsetof(RecastGraph_t2197443166, ___walkableHeight_21)); }
	inline float get_walkableHeight_21() const { return ___walkableHeight_21; }
	inline float* get_address_of_walkableHeight_21() { return &___walkableHeight_21; }
	inline void set_walkableHeight_21(float value)
	{
		___walkableHeight_21 = value;
	}

	inline static int32_t get_offset_of_walkableClimb_22() { return static_cast<int32_t>(offsetof(RecastGraph_t2197443166, ___walkableClimb_22)); }
	inline float get_walkableClimb_22() const { return ___walkableClimb_22; }
	inline float* get_address_of_walkableClimb_22() { return &___walkableClimb_22; }
	inline void set_walkableClimb_22(float value)
	{
		___walkableClimb_22 = value;
	}

	inline static int32_t get_offset_of_maxSlope_23() { return static_cast<int32_t>(offsetof(RecastGraph_t2197443166, ___maxSlope_23)); }
	inline float get_maxSlope_23() const { return ___maxSlope_23; }
	inline float* get_address_of_maxSlope_23() { return &___maxSlope_23; }
	inline void set_maxSlope_23(float value)
	{
		___maxSlope_23 = value;
	}

	inline static int32_t get_offset_of_maxEdgeLength_24() { return static_cast<int32_t>(offsetof(RecastGraph_t2197443166, ___maxEdgeLength_24)); }
	inline float get_maxEdgeLength_24() const { return ___maxEdgeLength_24; }
	inline float* get_address_of_maxEdgeLength_24() { return &___maxEdgeLength_24; }
	inline void set_maxEdgeLength_24(float value)
	{
		___maxEdgeLength_24 = value;
	}

	inline static int32_t get_offset_of_minRegionSize_25() { return static_cast<int32_t>(offsetof(RecastGraph_t2197443166, ___minRegionSize_25)); }
	inline float get_minRegionSize_25() const { return ___minRegionSize_25; }
	inline float* get_address_of_minRegionSize_25() { return &___minRegionSize_25; }
	inline void set_minRegionSize_25(float value)
	{
		___minRegionSize_25 = value;
	}

	inline static int32_t get_offset_of_editorTileSize_26() { return static_cast<int32_t>(offsetof(RecastGraph_t2197443166, ___editorTileSize_26)); }
	inline int32_t get_editorTileSize_26() const { return ___editorTileSize_26; }
	inline int32_t* get_address_of_editorTileSize_26() { return &___editorTileSize_26; }
	inline void set_editorTileSize_26(int32_t value)
	{
		___editorTileSize_26 = value;
	}

	inline static int32_t get_offset_of_tileSizeX_27() { return static_cast<int32_t>(offsetof(RecastGraph_t2197443166, ___tileSizeX_27)); }
	inline int32_t get_tileSizeX_27() const { return ___tileSizeX_27; }
	inline int32_t* get_address_of_tileSizeX_27() { return &___tileSizeX_27; }
	inline void set_tileSizeX_27(int32_t value)
	{
		___tileSizeX_27 = value;
	}

	inline static int32_t get_offset_of_tileSizeZ_28() { return static_cast<int32_t>(offsetof(RecastGraph_t2197443166, ___tileSizeZ_28)); }
	inline int32_t get_tileSizeZ_28() const { return ___tileSizeZ_28; }
	inline int32_t* get_address_of_tileSizeZ_28() { return &___tileSizeZ_28; }
	inline void set_tileSizeZ_28(int32_t value)
	{
		___tileSizeZ_28 = value;
	}

	inline static int32_t get_offset_of_nearestSearchOnlyXZ_29() { return static_cast<int32_t>(offsetof(RecastGraph_t2197443166, ___nearestSearchOnlyXZ_29)); }
	inline bool get_nearestSearchOnlyXZ_29() const { return ___nearestSearchOnlyXZ_29; }
	inline bool* get_address_of_nearestSearchOnlyXZ_29() { return &___nearestSearchOnlyXZ_29; }
	inline void set_nearestSearchOnlyXZ_29(bool value)
	{
		___nearestSearchOnlyXZ_29 = value;
	}

	inline static int32_t get_offset_of_useTiles_30() { return static_cast<int32_t>(offsetof(RecastGraph_t2197443166, ___useTiles_30)); }
	inline bool get_useTiles_30() const { return ___useTiles_30; }
	inline bool* get_address_of_useTiles_30() { return &___useTiles_30; }
	inline void set_useTiles_30(bool value)
	{
		___useTiles_30 = value;
	}

	inline static int32_t get_offset_of_scanEmptyGraph_31() { return static_cast<int32_t>(offsetof(RecastGraph_t2197443166, ___scanEmptyGraph_31)); }
	inline bool get_scanEmptyGraph_31() const { return ___scanEmptyGraph_31; }
	inline bool* get_address_of_scanEmptyGraph_31() { return &___scanEmptyGraph_31; }
	inline void set_scanEmptyGraph_31(bool value)
	{
		___scanEmptyGraph_31 = value;
	}

	inline static int32_t get_offset_of_relevantGraphSurfaceMode_32() { return static_cast<int32_t>(offsetof(RecastGraph_t2197443166, ___relevantGraphSurfaceMode_32)); }
	inline int32_t get_relevantGraphSurfaceMode_32() const { return ___relevantGraphSurfaceMode_32; }
	inline int32_t* get_address_of_relevantGraphSurfaceMode_32() { return &___relevantGraphSurfaceMode_32; }
	inline void set_relevantGraphSurfaceMode_32(int32_t value)
	{
		___relevantGraphSurfaceMode_32 = value;
	}

	inline static int32_t get_offset_of_rasterizeColliders_33() { return static_cast<int32_t>(offsetof(RecastGraph_t2197443166, ___rasterizeColliders_33)); }
	inline bool get_rasterizeColliders_33() const { return ___rasterizeColliders_33; }
	inline bool* get_address_of_rasterizeColliders_33() { return &___rasterizeColliders_33; }
	inline void set_rasterizeColliders_33(bool value)
	{
		___rasterizeColliders_33 = value;
	}

	inline static int32_t get_offset_of_rasterizeMeshes_34() { return static_cast<int32_t>(offsetof(RecastGraph_t2197443166, ___rasterizeMeshes_34)); }
	inline bool get_rasterizeMeshes_34() const { return ___rasterizeMeshes_34; }
	inline bool* get_address_of_rasterizeMeshes_34() { return &___rasterizeMeshes_34; }
	inline void set_rasterizeMeshes_34(bool value)
	{
		___rasterizeMeshes_34 = value;
	}

	inline static int32_t get_offset_of_rasterizeTerrain_35() { return static_cast<int32_t>(offsetof(RecastGraph_t2197443166, ___rasterizeTerrain_35)); }
	inline bool get_rasterizeTerrain_35() const { return ___rasterizeTerrain_35; }
	inline bool* get_address_of_rasterizeTerrain_35() { return &___rasterizeTerrain_35; }
	inline void set_rasterizeTerrain_35(bool value)
	{
		___rasterizeTerrain_35 = value;
	}

	inline static int32_t get_offset_of_rasterizeTrees_36() { return static_cast<int32_t>(offsetof(RecastGraph_t2197443166, ___rasterizeTrees_36)); }
	inline bool get_rasterizeTrees_36() const { return ___rasterizeTrees_36; }
	inline bool* get_address_of_rasterizeTrees_36() { return &___rasterizeTrees_36; }
	inline void set_rasterizeTrees_36(bool value)
	{
		___rasterizeTrees_36 = value;
	}

	inline static int32_t get_offset_of_colliderRasterizeDetail_37() { return static_cast<int32_t>(offsetof(RecastGraph_t2197443166, ___colliderRasterizeDetail_37)); }
	inline float get_colliderRasterizeDetail_37() const { return ___colliderRasterizeDetail_37; }
	inline float* get_address_of_colliderRasterizeDetail_37() { return &___colliderRasterizeDetail_37; }
	inline void set_colliderRasterizeDetail_37(float value)
	{
		___colliderRasterizeDetail_37 = value;
	}

	inline static int32_t get_offset_of_forcedBoundsCenter_38() { return static_cast<int32_t>(offsetof(RecastGraph_t2197443166, ___forcedBoundsCenter_38)); }
	inline Vector3_t4282066566  get_forcedBoundsCenter_38() const { return ___forcedBoundsCenter_38; }
	inline Vector3_t4282066566 * get_address_of_forcedBoundsCenter_38() { return &___forcedBoundsCenter_38; }
	inline void set_forcedBoundsCenter_38(Vector3_t4282066566  value)
	{
		___forcedBoundsCenter_38 = value;
	}

	inline static int32_t get_offset_of_forcedBoundsSize_39() { return static_cast<int32_t>(offsetof(RecastGraph_t2197443166, ___forcedBoundsSize_39)); }
	inline Vector3_t4282066566  get_forcedBoundsSize_39() const { return ___forcedBoundsSize_39; }
	inline Vector3_t4282066566 * get_address_of_forcedBoundsSize_39() { return &___forcedBoundsSize_39; }
	inline void set_forcedBoundsSize_39(Vector3_t4282066566  value)
	{
		___forcedBoundsSize_39 = value;
	}

	inline static int32_t get_offset_of_mask_40() { return static_cast<int32_t>(offsetof(RecastGraph_t2197443166, ___mask_40)); }
	inline LayerMask_t3236759763  get_mask_40() const { return ___mask_40; }
	inline LayerMask_t3236759763 * get_address_of_mask_40() { return &___mask_40; }
	inline void set_mask_40(LayerMask_t3236759763  value)
	{
		___mask_40 = value;
	}

	inline static int32_t get_offset_of_tagMask_41() { return static_cast<int32_t>(offsetof(RecastGraph_t2197443166, ___tagMask_41)); }
	inline List_1_t1375417109 * get_tagMask_41() const { return ___tagMask_41; }
	inline List_1_t1375417109 ** get_address_of_tagMask_41() { return &___tagMask_41; }
	inline void set_tagMask_41(List_1_t1375417109 * value)
	{
		___tagMask_41 = value;
		Il2CppCodeGenWriteBarrier(&___tagMask_41, value);
	}

	inline static int32_t get_offset_of_showMeshOutline_42() { return static_cast<int32_t>(offsetof(RecastGraph_t2197443166, ___showMeshOutline_42)); }
	inline bool get_showMeshOutline_42() const { return ___showMeshOutline_42; }
	inline bool* get_address_of_showMeshOutline_42() { return &___showMeshOutline_42; }
	inline void set_showMeshOutline_42(bool value)
	{
		___showMeshOutline_42 = value;
	}

	inline static int32_t get_offset_of_showNodeConnections_43() { return static_cast<int32_t>(offsetof(RecastGraph_t2197443166, ___showNodeConnections_43)); }
	inline bool get_showNodeConnections_43() const { return ___showNodeConnections_43; }
	inline bool* get_address_of_showNodeConnections_43() { return &___showNodeConnections_43; }
	inline void set_showNodeConnections_43(bool value)
	{
		___showNodeConnections_43 = value;
	}

	inline static int32_t get_offset_of_terrainSampleSize_44() { return static_cast<int32_t>(offsetof(RecastGraph_t2197443166, ___terrainSampleSize_44)); }
	inline int32_t get_terrainSampleSize_44() const { return ___terrainSampleSize_44; }
	inline int32_t* get_address_of_terrainSampleSize_44() { return &___terrainSampleSize_44; }
	inline void set_terrainSampleSize_44(int32_t value)
	{
		___terrainSampleSize_44 = value;
	}

	inline static int32_t get_offset_of_globalVox_45() { return static_cast<int32_t>(offsetof(RecastGraph_t2197443166, ___globalVox_45)); }
	inline Voxelize_t3998270866 * get_globalVox_45() const { return ___globalVox_45; }
	inline Voxelize_t3998270866 ** get_address_of_globalVox_45() { return &___globalVox_45; }
	inline void set_globalVox_45(Voxelize_t3998270866 * value)
	{
		___globalVox_45 = value;
		Il2CppCodeGenWriteBarrier(&___globalVox_45, value);
	}

	inline static int32_t get_offset_of__bbTree_46() { return static_cast<int32_t>(offsetof(RecastGraph_t2197443166, ____bbTree_46)); }
	inline BBTree_t1216325332 * get__bbTree_46() const { return ____bbTree_46; }
	inline BBTree_t1216325332 ** get_address_of__bbTree_46() { return &____bbTree_46; }
	inline void set__bbTree_46(BBTree_t1216325332 * value)
	{
		____bbTree_46 = value;
		Il2CppCodeGenWriteBarrier(&____bbTree_46, value);
	}

	inline static int32_t get_offset_of__vertices_47() { return static_cast<int32_t>(offsetof(RecastGraph_t2197443166, ____vertices_47)); }
	inline Int3U5BU5D_t516284607* get__vertices_47() const { return ____vertices_47; }
	inline Int3U5BU5D_t516284607** get_address_of__vertices_47() { return &____vertices_47; }
	inline void set__vertices_47(Int3U5BU5D_t516284607* value)
	{
		____vertices_47 = value;
		Il2CppCodeGenWriteBarrier(&____vertices_47, value);
	}

	inline static int32_t get_offset_of__vectorVertices_48() { return static_cast<int32_t>(offsetof(RecastGraph_t2197443166, ____vectorVertices_48)); }
	inline Vector3U5BU5D_t215400611* get__vectorVertices_48() const { return ____vectorVertices_48; }
	inline Vector3U5BU5D_t215400611** get_address_of__vectorVertices_48() { return &____vectorVertices_48; }
	inline void set__vectorVertices_48(Vector3U5BU5D_t215400611* value)
	{
		____vectorVertices_48 = value;
		Il2CppCodeGenWriteBarrier(&____vectorVertices_48, value);
	}

	inline static int32_t get_offset_of_tileXCount_49() { return static_cast<int32_t>(offsetof(RecastGraph_t2197443166, ___tileXCount_49)); }
	inline int32_t get_tileXCount_49() const { return ___tileXCount_49; }
	inline int32_t* get_address_of_tileXCount_49() { return &___tileXCount_49; }
	inline void set_tileXCount_49(int32_t value)
	{
		___tileXCount_49 = value;
	}

	inline static int32_t get_offset_of_tileZCount_50() { return static_cast<int32_t>(offsetof(RecastGraph_t2197443166, ___tileZCount_50)); }
	inline int32_t get_tileZCount_50() const { return ___tileZCount_50; }
	inline int32_t* get_address_of_tileZCount_50() { return &___tileZCount_50; }
	inline void set_tileZCount_50(int32_t value)
	{
		___tileZCount_50 = value;
	}

	inline static int32_t get_offset_of_tiles_51() { return static_cast<int32_t>(offsetof(RecastGraph_t2197443166, ___tiles_51)); }
	inline NavmeshTileU5BU5D_t3225070876* get_tiles_51() const { return ___tiles_51; }
	inline NavmeshTileU5BU5D_t3225070876** get_address_of_tiles_51() { return &___tiles_51; }
	inline void set_tiles_51(NavmeshTileU5BU5D_t3225070876* value)
	{
		___tiles_51 = value;
		Il2CppCodeGenWriteBarrier(&___tiles_51, value);
	}

	inline static int32_t get_offset_of_batchTileUpdate_52() { return static_cast<int32_t>(offsetof(RecastGraph_t2197443166, ___batchTileUpdate_52)); }
	inline bool get_batchTileUpdate_52() const { return ___batchTileUpdate_52; }
	inline bool* get_address_of_batchTileUpdate_52() { return &___batchTileUpdate_52; }
	inline void set_batchTileUpdate_52(bool value)
	{
		___batchTileUpdate_52 = value;
	}

	inline static int32_t get_offset_of_batchUpdatedTiles_53() { return static_cast<int32_t>(offsetof(RecastGraph_t2197443166, ___batchUpdatedTiles_53)); }
	inline List_1_t2522024052 * get_batchUpdatedTiles_53() const { return ___batchUpdatedTiles_53; }
	inline List_1_t2522024052 ** get_address_of_batchUpdatedTiles_53() { return &___batchUpdatedTiles_53; }
	inline void set_batchUpdatedTiles_53(List_1_t2522024052 * value)
	{
		___batchUpdatedTiles_53 = value;
		Il2CppCodeGenWriteBarrier(&___batchUpdatedTiles_53, value);
	}

	inline static int32_t get_offset_of_cachedInt2_int_dict_54() { return static_cast<int32_t>(offsetof(RecastGraph_t2197443166, ___cachedInt2_int_dict_54)); }
	inline Dictionary_2_t2245318082 * get_cachedInt2_int_dict_54() const { return ___cachedInt2_int_dict_54; }
	inline Dictionary_2_t2245318082 ** get_address_of_cachedInt2_int_dict_54() { return &___cachedInt2_int_dict_54; }
	inline void set_cachedInt2_int_dict_54(Dictionary_2_t2245318082 * value)
	{
		___cachedInt2_int_dict_54 = value;
		Il2CppCodeGenWriteBarrier(&___cachedInt2_int_dict_54, value);
	}

	inline static int32_t get_offset_of_cachedInt3_int_dict_55() { return static_cast<int32_t>(offsetof(RecastGraph_t2197443166, ___cachedInt3_int_dict_55)); }
	inline Dictionary_2_t2731505821 * get_cachedInt3_int_dict_55() const { return ___cachedInt3_int_dict_55; }
	inline Dictionary_2_t2731505821 ** get_address_of_cachedInt3_int_dict_55() { return &___cachedInt3_int_dict_55; }
	inline void set_cachedInt3_int_dict_55(Dictionary_2_t2731505821 * value)
	{
		___cachedInt3_int_dict_55 = value;
		Il2CppCodeGenWriteBarrier(&___cachedInt3_int_dict_55, value);
	}

	inline static int32_t get_offset_of_BoxColliderTris_56() { return static_cast<int32_t>(offsetof(RecastGraph_t2197443166, ___BoxColliderTris_56)); }
	inline Int32U5BU5D_t3230847821* get_BoxColliderTris_56() const { return ___BoxColliderTris_56; }
	inline Int32U5BU5D_t3230847821** get_address_of_BoxColliderTris_56() { return &___BoxColliderTris_56; }
	inline void set_BoxColliderTris_56(Int32U5BU5D_t3230847821* value)
	{
		___BoxColliderTris_56 = value;
		Il2CppCodeGenWriteBarrier(&___BoxColliderTris_56, value);
	}

	inline static int32_t get_offset_of_BoxColliderVerts_57() { return static_cast<int32_t>(offsetof(RecastGraph_t2197443166, ___BoxColliderVerts_57)); }
	inline Vector3U5BU5D_t215400611* get_BoxColliderVerts_57() const { return ___BoxColliderVerts_57; }
	inline Vector3U5BU5D_t215400611** get_address_of_BoxColliderVerts_57() { return &___BoxColliderVerts_57; }
	inline void set_BoxColliderVerts_57(Vector3U5BU5D_t215400611* value)
	{
		___BoxColliderVerts_57 = value;
		Il2CppCodeGenWriteBarrier(&___BoxColliderVerts_57, value);
	}

	inline static int32_t get_offset_of_capsuleCache_58() { return static_cast<int32_t>(offsetof(RecastGraph_t2197443166, ___capsuleCache_58)); }
	inline List_1_t3400845858 * get_capsuleCache_58() const { return ___capsuleCache_58; }
	inline List_1_t3400845858 ** get_address_of_capsuleCache_58() { return &___capsuleCache_58; }
	inline void set_capsuleCache_58(List_1_t3400845858 * value)
	{
		___capsuleCache_58 = value;
		Il2CppCodeGenWriteBarrier(&___capsuleCache_58, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
