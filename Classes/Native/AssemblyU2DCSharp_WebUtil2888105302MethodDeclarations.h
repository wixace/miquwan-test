﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.String
struct String_t;
// System.Action`1<System.String>
struct Action_1_t403047693;
// System.Action
struct Action_t3771233898;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "System_Core_System_Action3771233898.h"

// System.Collections.IEnumerator WebUtil::GetText(System.String,System.Action`1<System.String>,System.Action)
extern "C"  Il2CppObject * WebUtil_GetText_m3285714997 (Il2CppObject * __this /* static, unused */, String_t* ___url0, Action_1_t403047693 * ___success1, Action_t3771233898 * ___failure2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
