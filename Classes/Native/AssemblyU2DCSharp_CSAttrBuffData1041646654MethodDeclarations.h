﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CSAttrBuffData
struct CSAttrBuffData_t1041646654;

#include "codegen/il2cpp-codegen.h"

// System.Void CSAttrBuffData::.ctor()
extern "C"  void CSAttrBuffData__ctor_m1284861469 (CSAttrBuffData_t1041646654 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
