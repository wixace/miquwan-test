﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_ComputeShaderGenerated
struct UnityEngine_ComputeShaderGenerated_t834386267;
// JSVCall
struct JSVCall_t3708497963;
// System.Single[]
struct SingleU5BU5D_t2316563989;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"

// System.Void UnityEngine_ComputeShaderGenerated::.ctor()
extern "C"  void UnityEngine_ComputeShaderGenerated__ctor_m2879548832 (UnityEngine_ComputeShaderGenerated_t834386267 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ComputeShaderGenerated::ComputeShader_ComputeShader1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ComputeShaderGenerated_ComputeShader_ComputeShader1_m2241089888 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ComputeShaderGenerated::ComputeShader_Dispatch__Int32__Int32__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ComputeShaderGenerated_ComputeShader_Dispatch__Int32__Int32__Int32__Int32_m1171835237 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ComputeShaderGenerated::ComputeShader_FindKernel__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ComputeShaderGenerated_ComputeShader_FindKernel__String_m859104146 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ComputeShaderGenerated::ComputeShader_SetBuffer__Int32__String__ComputeBuffer(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ComputeShaderGenerated_ComputeShader_SetBuffer__Int32__String__ComputeBuffer_m2689235473 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ComputeShaderGenerated::ComputeShader_SetFloat__String__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ComputeShaderGenerated_ComputeShader_SetFloat__String__Single_m3559751710 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ComputeShaderGenerated::ComputeShader_SetFloats__String__Single_Array(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ComputeShaderGenerated_ComputeShader_SetFloats__String__Single_Array_m124879779 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ComputeShaderGenerated::ComputeShader_SetInt__String__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ComputeShaderGenerated_ComputeShader_SetInt__String__Int32_m3764484103 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ComputeShaderGenerated::ComputeShader_SetInts__String__Int32_Array(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ComputeShaderGenerated_ComputeShader_SetInts__String__Int32_Array_m3508046044 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ComputeShaderGenerated::ComputeShader_SetTexture__Int32__String__Texture(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ComputeShaderGenerated_ComputeShader_SetTexture__Int32__String__Texture_m1816576608 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ComputeShaderGenerated::ComputeShader_SetVector__String__Vector4(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ComputeShaderGenerated_ComputeShader_SetVector__String__Vector4_m2408676838 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ComputeShaderGenerated::__Register()
extern "C"  void UnityEngine_ComputeShaderGenerated___Register_m3229782823 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single[] UnityEngine_ComputeShaderGenerated::<ComputeShader_SetFloats__String__Single_Array>m__19C()
extern "C"  SingleU5BU5D_t2316563989* UnityEngine_ComputeShaderGenerated_U3CComputeShader_SetFloats__String__Single_ArrayU3Em__19C_m672925907 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] UnityEngine_ComputeShaderGenerated::<ComputeShader_SetInts__String__Int32_Array>m__19D()
extern "C"  Int32U5BU5D_t3230847821* UnityEngine_ComputeShaderGenerated_U3CComputeShader_SetInts__String__Int32_ArrayU3Em__19D_m3919258765 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_ComputeShaderGenerated::ilo_getInt321(System.Int32)
extern "C"  int32_t UnityEngine_ComputeShaderGenerated_ilo_getInt321_m3825342823 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_ComputeShaderGenerated::ilo_getObject2(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_ComputeShaderGenerated_ilo_getObject2_m2613411088 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_ComputeShaderGenerated::ilo_getArrayLength3(System.Int32)
extern "C"  int32_t UnityEngine_ComputeShaderGenerated_ilo_getArrayLength3_m3318265912 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_ComputeShaderGenerated::ilo_getObject4(System.Int32)
extern "C"  int32_t UnityEngine_ComputeShaderGenerated_ilo_getObject4_m3298012341 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_ComputeShaderGenerated::ilo_getElement5(System.Int32,System.Int32)
extern "C"  int32_t UnityEngine_ComputeShaderGenerated_ilo_getElement5_m3973731386 (Il2CppObject * __this /* static, unused */, int32_t ___id0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
