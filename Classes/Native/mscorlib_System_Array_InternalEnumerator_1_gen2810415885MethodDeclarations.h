﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2810415885.h"
#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DCSharp_AstarDebugger_PathTypeDebug4028073209.h"

// System.Void System.Array/InternalEnumerator`1<AstarDebugger/PathTypeDebug>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3683717694_gshared (InternalEnumerator_1_t2810415885 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m3683717694(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t2810415885 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m3683717694_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<AstarDebugger/PathTypeDebug>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1466027682_gshared (InternalEnumerator_1_t2810415885 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1466027682(__this, method) ((  void (*) (InternalEnumerator_1_t2810415885 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1466027682_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<AstarDebugger/PathTypeDebug>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m70506520_gshared (InternalEnumerator_1_t2810415885 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m70506520(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t2810415885 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m70506520_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<AstarDebugger/PathTypeDebug>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2679550357_gshared (InternalEnumerator_1_t2810415885 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m2679550357(__this, method) ((  void (*) (InternalEnumerator_1_t2810415885 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2679550357_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<AstarDebugger/PathTypeDebug>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m643862354_gshared (InternalEnumerator_1_t2810415885 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m643862354(__this, method) ((  bool (*) (InternalEnumerator_1_t2810415885 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m643862354_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<AstarDebugger/PathTypeDebug>::get_Current()
extern "C"  PathTypeDebug_t4028073209  InternalEnumerator_1_get_Current_m1670861287_gshared (InternalEnumerator_1_t2810415885 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1670861287(__this, method) ((  PathTypeDebug_t4028073209  (*) (InternalEnumerator_1_t2810415885 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1670861287_gshared)(__this, method)
