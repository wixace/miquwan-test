﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System_Collections_Generic_List77Generated/<ListA1_Exists__PredicateT1_T>c__AnonStorey7E
struct U3CListA1_Exists__PredicateT1_TU3Ec__AnonStorey7E_t4222894511;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void System_Collections_Generic_List77Generated/<ListA1_Exists__PredicateT1_T>c__AnonStorey7E::.ctor()
extern "C"  void U3CListA1_Exists__PredicateT1_TU3Ec__AnonStorey7E__ctor_m2730361292 (U3CListA1_Exists__PredicateT1_TU3Ec__AnonStorey7E_t4222894511 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System_Collections_Generic_List77Generated/<ListA1_Exists__PredicateT1_T>c__AnonStorey7E::<>m__A3()
extern "C"  Il2CppObject * U3CListA1_Exists__PredicateT1_TU3Ec__AnonStorey7E_U3CU3Em__A3_m3445726290 (U3CListA1_Exists__PredicateT1_TU3Ec__AnonStorey7E_t4222894511 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
