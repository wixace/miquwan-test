﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24287931429.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m733106853_gshared (KeyValuePair_2_t4287931429 * __this, uint64_t ___key0, int32_t ___value1, const MethodInfo* method);
#define KeyValuePair_2__ctor_m733106853(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t4287931429 *, uint64_t, int32_t, const MethodInfo*))KeyValuePair_2__ctor_m733106853_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>::get_Key()
extern "C"  uint64_t KeyValuePair_2_get_Key_m2024648546_gshared (KeyValuePair_2_t4287931429 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m2024648546(__this, method) ((  uint64_t (*) (KeyValuePair_2_t4287931429 *, const MethodInfo*))KeyValuePair_2_get_Key_m2024648546_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m3521839806_gshared (KeyValuePair_2_t4287931429 * __this, uint64_t ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m3521839806(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t4287931429 *, uint64_t, const MethodInfo*))KeyValuePair_2_set_Key_m3521839806_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>::get_Value()
extern "C"  int32_t KeyValuePair_2_get_Value_m2323422163_gshared (KeyValuePair_2_t4287931429 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m2323422163(__this, method) ((  int32_t (*) (KeyValuePair_2_t4287931429 *, const MethodInfo*))KeyValuePair_2_get_Value_m2323422163_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m4285188030_gshared (KeyValuePair_2_t4287931429 * __this, int32_t ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m4285188030(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t4287931429 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Value_m4285188030_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m1712840292_gshared (KeyValuePair_2_t4287931429 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m1712840292(__this, method) ((  String_t* (*) (KeyValuePair_2_t4287931429 *, const MethodInfo*))KeyValuePair_2_ToString_m1712840292_gshared)(__this, method)
